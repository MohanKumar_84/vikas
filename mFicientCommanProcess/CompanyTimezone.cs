﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace mFicientCommonProject
{
    class CompanyTimezone
    {
        private static volatile CompanyTimezone obj;
        private static string _timeZoneId;
        private static TimeZoneInfo _cmpTimeZoneInfo;

        public static TimeZoneInfo CmpTimeZoneInfo
        {
            get { return CompanyTimezone._cmpTimeZoneInfo; }
            private set { CompanyTimezone._cmpTimeZoneInfo = value; }
        }
        public static string TimeZoneId
        {
            get { return CompanyTimezone._timeZoneId; }
            private set { CompanyTimezone._timeZoneId = value; }
        }

        private CompanyTimezone()
        {
            //httpRequest = new HttpRequestJson();
        }
        private static void checkCreateSingleton()
        {
            if (obj == null)
                obj = new CompanyTimezone();
        }


        private static string   getTimezoneIdString(string companyId, SqlConnection con,out string result)
        {
            string timezonestring = "";
            GetCompanyDetails objCmpDtls = new GetCompanyDetails(companyId,con);

            if (objCmpDtls.ResultTable.Rows.Count > 0)
            {
                timezonestring = Convert.ToString(objCmpDtls.ResultTable.Rows[0]["TIMEZONE_ID"]);
            }

            result = timezonestring;
            return result;
        }
        /// <summary>
        /// Returns the timezone info of the company.
        /// returns TimeZoneInfo.Local if the saved Timezone Id is not found.
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static TimeZoneInfo getTimezoneInfo(string companyId,SqlConnection CONNNECTION)
        {
            string timezone = "";
            string strTimezoneIdString = getTimezoneIdString(companyId,CONNNECTION,out timezone);
            TimeZoneId = strTimezoneIdString;
            TimeZoneInfo tzi;
            try
            {
                tzi = TimeZoneInfo.FindSystemTimeZoneById(strTimezoneIdString);
            }
            catch (TimeZoneNotFoundException)
            {
                tzi = TimeZoneInfo.Local;
            }
            catch (InvalidTimeZoneException)
            {
                tzi = TimeZoneInfo.Local;
            }
            CmpTimeZoneInfo = tzi;
            return tzi;
        }
    }
}

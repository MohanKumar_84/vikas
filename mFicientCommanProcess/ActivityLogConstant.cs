﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace mFicientCommonProcess
{
    public enum SOURCE
    {
        CP = 1,
        WEBSERVICE = 2,
        MBUZZ = 3,
        MGRAM = 4,
        MPLUGIN = 5,
    }

    public enum ACTIVITYENUM
    {
        APP_SAVE_MODIFIED = 101,
        APP_PUBLISH = 102,
        APP_ROLLBACK = 103,
        APP_DELETE = 124,

        CATEGORY_ADDED = 104,
        CATEGORY_RENAMED = 105,
        CATEGORY_REMOVED = 106,
        CATEGORY_MODIFIED_APPADDED = 107,
        CATEGORY_MODIFIED_APP_DELETED = 108,

        GROUP_ADDED = 109,
        GROUP_RENAMED = 110,
        GROUP_REMOVED = 111,
        GROUP_MODIFIED_APP_ADDED = 112,
        GROUP_MODIFIED_APP_DELETED = 113,
        GROUP_MODIFIED_USER_ADDED = 114,
        GROUP_MODIFIED_USER_DELETED = 115,

        DEVICE_APPROVED = 116,
        DEVICE_DENY = 117,
        DEVICE_DELETE = 118,

        USER_CREATED = 119,
        USER_MODIFIED = 120,
        USER_DELETE = 121,
        USER_BLOCKED = 122,
        USER_UNBLOCK = 123,
        USER_LOGIN = 201,
        USER_LOGOUT = 202,

        SESSTION_RENEW = 203,
        APP_USED = 204,
        OBJECT_EXECUTE = 205,
        PUSHMESSAGE_SEND = 206,

        MPLUGIN_CONNECTED = 301,
        MPLUGIN_DISCONNECTED = 302,
        MPLUGIN_CREATE=303,
        MPLUGIN_DELETE = 304,
        MPLUGIN_CHANGE = 305,

        SUBADMIN_CREATED = 141,
        SUBADMIN_MODIFIED = 142,
        SUBADMIN_DELETE = 143,
        SUBADMIN_BLOCKED = 144,
        SUBADMIN_UNBLOCK = 145,
    }
    public enum Category
    {
        APPS = 1,
        Groups = 2,
        Users = 3,
        Category = 4,
        mPlugin = 5,
        Device = 6,
    }


}

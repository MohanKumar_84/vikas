﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCommonProcess
{
    public class mGarmUserdetails
    {
        public mGarmUserdetails(string _name, string _companyId, string _devicetype, List<List<string>> _deviceIds)
        {
            Name = _name;
            CompanyId = _companyId;
            Devicetype = _devicetype;
            DeviceIds = _deviceIds;
        }
        //public string Id { get; set; }
        public string Name { get; set; }
        public string CompanyId { get; set; }
        public string Devicetype { get; set; }
        public List<List<string>> DeviceIds { get; set; }
    }
    public class SendNotification
    {
        public static void SaveMgramMessage(List<mGarmUserdetails> Usernames, SqlConnection con, SqlTransaction transaction, string Category, string Message, long sendTime, int days)
        {
            string strQuery = getSqlQuery();
            foreach (var item in Usernames)
            {
                string strUniqueId = Utility.GetMd5Hash(item.CompanyId + item.Name +
                    DateTime.UtcNow.Ticks.ToString());
                SqlCommand cmd;
                if (transaction != null)
                    cmd = new SqlCommand(strQuery, con, transaction);
                else
                    cmd = new SqlCommand(strQuery, con);
                // = new SqlCommand(strQuery, con, transaction);
                cmd.CommandText = strQuery;
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("MGRAM_MSG_ID", strUniqueId);
                cmd.Parameters.AddWithValue("MSG_ID", strUniqueId);
                cmd.Parameters.AddWithValue("@ENTERPRISE_ID", item.CompanyId);
                cmd.Parameters.AddWithValue("@PICK_UP_TIME", DateTime.UtcNow.Ticks);
                cmd.Parameters.AddWithValue("@CATEGORY", Category);
                cmd.Parameters.AddWithValue("@USERNAME", item.Name);
                cmd.Parameters.AddWithValue("@EXPIRATION_DATETIME", getExpirationDateInTicks(sendTime, days));
                cmd.Parameters.AddWithValue("@SEND_DATETIME", DateTime.UtcNow.Ticks);
                cmd.Parameters.AddWithValue("@STATUS", 0);
                cmd.Parameters.AddWithValue("@MESSAGE", Message);
                cmd.Parameters.AddWithValue("@SENT_DATETIME", 0);
                cmd.Parameters.AddWithValue("@IS_NOTIFICATION_SENT", 1);
                cmd.Parameters.AddWithValue("@DEVICE_TYPE", item.Devicetype);
                cmd.ExecuteNonQuery();
                foreach (List<string> deviceid in item.DeviceIds)
                {
                    SaveNotificationTable(con, transaction, item.CompanyId, item.Name,"",0, NOTIFICATION_TYPE.MF_MGRAM, deviceid[0], deviceid[1], deviceid[2], "",1);
                }
            }
        }

        static string getSqlQuery()
        {
            return @"INSERT INTO [TBL_MGRAM_MSG] ([MGRAM_MSG_ID],[MSG_ID],[ENTERPRISE_ID]
                    ,[PICK_UP_TIME],[CATEGORY] ,[USERNAME],[SEND_DATETIME] ,[STATUS],[MESSAGE]
                    ,[EXPIRATION_DATETIME],[SENT_DATETIME],[IS_NOTIFICATION_SENT],[DEVICE_TYPE])
                    VALUES (@MGRAM_MSG_ID,@MSG_ID,@ENTERPRISE_ID ,@PICK_UP_TIME,@CATEGORY
                    ,@USERNAME,@SEND_DATETIME ,@STATUS,@MESSAGE ,@EXPIRATION_DATETIME,@SENT_DATETIME,
                    @IS_NOTIFICATION_SENT, @DEVICE_TYPE)";
        }
        public static int SaveNotificationTable(SqlConnection con, SqlTransaction trn, string _enterpriseId, string _userName, string _fromName,int source, NOTIFICATION_TYPE _eNotificationType, string _devTokenID, string _devType, string _deviceID, string _info, int badgeCount)
        {
            string query = @"INSERT INTO [TBL_PUSHMESSAGE_OUTBOX]
                ([PUSH_NOTIFICATION_ID],[ENTERPRISE_ID],[BADGE_COUNT],[USERNAME]
                ,[FROM_USER_FIRSTNAME],[SOURCE],[NOTIFICATION_TYPE],[PUSH_DATETIME],
                [DEVICE_TOKEN_ID],[DEVICE_TYPE],[DEVICE_ID],[INFO])
           VALUES
                (@PUSH_NOTIFICATION_ID,@ENTERPRISE_ID,@BADGE_COUNT,@USERNAME
                ,@FROM_USER_FIRSTNAME,@SOURCE,@NOTIFICATION_TYPE, @PUSH_DATETIME,
                @DEVICE_TOKEN_ID,@DEVICE_TYPE,@DEVICE_ID,@INFO);";
            SqlCommand cmd;
            if (trn != null)
                cmd = new SqlCommand(query, con, trn);
            else
                cmd = new SqlCommand(query, con);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue(
                "@PUSH_NOTIFICATION_ID",
                Utility.GetMd5Hash(_enterpriseId + DateTime.UtcNow.Ticks + _userName)
                );
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", _enterpriseId);
            cmd.Parameters.AddWithValue("@BADGE_COUNT", badgeCount);
            cmd.Parameters.AddWithValue("@USERNAME", _userName);
            cmd.Parameters.AddWithValue("@FROM_USER_FIRSTNAME", _fromName);
            cmd.Parameters.AddWithValue("@SOURCE", 0);
            cmd.Parameters.AddWithValue("@NOTIFICATION_TYPE", Convert.ToInt16((int)_eNotificationType));
            cmd.Parameters.AddWithValue("@PUSH_DATETIME", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@DEVICE_TOKEN_ID", _devTokenID);
            cmd.Parameters.AddWithValue("@DEVICE_TYPE", _devType);
            cmd.Parameters.AddWithValue("@DEVICE_ID", _deviceID);
            cmd.Parameters.AddWithValue("@INFO", _info);

            return cmd.ExecuteNonQuery();
        }
        static long getExpirationDateInTicks(long SendDatetime, int Days)
        {
            DateTime dtSendTime = DateTime.UtcNow;
            DateTime dtExpiryTime = DateTime.UtcNow;
            long lngSendTime = dtSendTime.Ticks;
            if (SendDatetime != 0)
            {
                dtSendTime = new DateTime(SendDatetime, DateTimeKind.Utc);
            }
            dtExpiryTime = dtSendTime.AddDays(Days);
            return dtExpiryTime.Ticks;
        }
        public static void ResetUserNotificationBadgeCount(SqlConnection con, SqlTransaction trn, string _enterpriseId, string _userName)
        {
            string query = @"if exist (select true TBL_USER_NOTIFICATION_COUNT from where ENTERPRISE_ID=@ENTERPRISE_ID and USERNAME=@USERNAME)BEGIN
                        UPDATE TBL_USER_NOTIFICATION_COUNT SET MGRAM_COUNT = 0,MBUZZ_COUNT = 0 where ENTERPRISE_ID=@ENTERPRISE_ID and USERNAME=@USERNAME END
                        else insert into TBL_USER_NOTIFICATION_COUNT(ENTERPRISE_ID,USERNAME,MGRAM_COUNT,MBUZZ_COUNT)values (@ENTERPRISE_ID,@USERNAME,0,0)";
            SqlCommand cmd;
            if (trn != null)
                cmd = new SqlCommand(query, con, trn);
            else
                cmd = new SqlCommand(query, con);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@USERNAME", _userName);
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", _enterpriseId);
        }
    }
}
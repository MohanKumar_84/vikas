﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCommonProcess
{
    class msSqlClient
    {
        /// <summary>
        /// Description :SqlConnection Open Function
        /// </summary>
        public static void SqlConnectionOpen(out SqlConnection Conn,string ConnectionString)
        {
            try
            {
                Conn = new SqlConnection(ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch 
            {
                Conn = null;
            }
        }

        /// <summary>
        /// Description :SqlConnection Close Function
        /// </summary>
        public static void SqlConnectionClose(SqlConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open))
                {
                    Conn.Close();
                    Conn.Dispose();
                    Conn = null;
                }
            }
            catch
            {
                Conn.Dispose();
                Conn = null;
            }
        }

        /// <summary>
        /// Description :Select Data According To SqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static DataSet SelectDataFromSQlCommand(SqlCommand _SqlCommand, SqlConnection objSqlConnection, SqlTransaction objSqlTransaction)
        {
            try
            {
                _SqlCommand.Connection = objSqlConnection;
                if (objSqlTransaction != null)
                    _SqlCommand.Transaction = objSqlTransaction;
                DataSet ObjDataSet = new DataSet();

                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

                objSqlDataAdapter.Fill(ObjDataSet);
                return ObjDataSet;
            }
            catch
            {
                return null;
            }
            finally
            {
                //SqlConnectionClose(objSqlConnection);
            }
        }

        /// <summary>
        /// Description :Data Process (Insert, Update, Delete ) According To SqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static int ExecuteNonQueryRecord(SqlCommand _SqlCommand, SqlConnection objSqlConnection, SqlTransaction objSqlTransaction)
        {
            _SqlCommand.Connection = objSqlConnection;
            if (objSqlTransaction != null)
                _SqlCommand.Transaction = objSqlTransaction;
            int i = _SqlCommand.ExecuteNonQuery();

            SqlConnectionClose(objSqlConnection);

            return i;
        }
    }
}

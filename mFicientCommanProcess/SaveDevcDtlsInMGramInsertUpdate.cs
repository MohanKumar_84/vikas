﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCommonProcess
{
    public class SaveDevcDtlsInMGramInsertUpdate
    {
        public static void SaveDeviceDetailInMgram(string companyId, string deviceId, string deviceType,string userName, string devcPushMsgId,SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(InsertUpdateQuery(), con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@EnterpriseId", companyId);
            cmd.Parameters.AddWithValue("@DeviceId", deviceId);
            cmd.Parameters.AddWithValue("@DeviceType", deviceType);
            cmd.Parameters.AddWithValue("@UserName", userName);
            cmd.Parameters.AddWithValue("@DevicePushMsgId", devcPushMsgId);
            int iRowsEffected = cmd.ExecuteNonQuery();
        }

        static string InsertUpdateQuery()
        {
            return @"IF EXISTS (SELECT USERNAME 
                    FROM TBL_USER_DEVICE 
                    WHERE ENTERPRISE_ID = @EnterpriseId
                    AND DEVICE_ID = @DeviceId
                    AND DEVICE_TYPE = @DeviceType
                    AND USERNAME = @UserName)

                    UPDATE TBL_USER_DEVICE
                    SET DEVICE_PUSH_MESSAGE_ID = @DevicePushMsgId
                    WHERE ENTERPRISE_ID =@EnterpriseID 
                    AND DEVICE_ID = @DeviceId
                    AND DEVICE_TYPE = @DeviceType
                    AND USERNAME = @UserName

                    else IF EXISTS (SELECT USERNAME  FROM TBL_USER_DEVICE 
                    WHERE  DEVICE_PUSH_MESSAGE_ID = @DevicePushMsgId)


                    UPDATE TBL_USER_DEVICE
                    SET ENTERPRISE_ID =@EnterpriseID ,DEVICE_ID = @DeviceId,DEVICE_TYPE = @DeviceType, USERNAME = @UserName
                    WHERE DEVICE_PUSH_MESSAGE_ID = @DevicePushMsgId

                    ELSE

                    INSERT INTO TBL_USER_DEVICE
                    VALUES(@EnterpriseId,@DeviceId,@DeviceType,@UserName,@DevicePushMsgId);";

        }

        public static void deleteDeviceDtls(string companyId, string deviceId, string deviceType, string userName,  SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(DeleteQuery(), con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", companyId);
            cmd.Parameters.AddWithValue("@DEVICE_ID", deviceId);
            cmd.Parameters.AddWithValue("@DEVICE_TYPE", deviceType);
            cmd.Parameters.AddWithValue("@USER_NAME", userName);
            int iRowsEffected = cmd.ExecuteNonQuery();
        }

        static string DeleteQuery()
        {
            return @"DELETE FROM TBL_USER_DEVICE
                    WHERE ENTERPRISE_ID = @ENTERPRISE_ID
                    AND DEVICE_ID = @DEVICE_ID
                    AND DEVICE_TYPE = @DEVICE_TYPE
                    AND USERNAME = @USER_NAME";
        }
    }
}

﻿<%@ Page Title="mFicient | My Account" Language="C#" MasterPageFile="~/master/Canvas.master"
    AutoEventWireup="true" CodeBehind="companyDetails.aspx.cs" Inherits="mFicientCP.companyDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <style type="text/css">
        .column1
        {
            margin-left: 3px;
            float: left;
            width: 80px;
            color: #6F6F6F;
        }
        .column2
        {
            margin-left: 3px;
            margin-right: 4px;
            float: left;
        }
        .column3
        {
            clear: both;
        }
        #divThumbNailImg
        {
            cursor: pointer;
            margin: auto;
            float: left;
            width: 50px;
            height: 50px;
            position: relative; /*top: -15px;*/
        }
        .hide{display:none}
         .SubProcborderDiv{margin-top:10px;width:100%;float:left;clear:both;border-top:solid 1px #e0e0e0;}
   .SubProcBtnDiv {text-align:center; padding:7px;}
   .InputStyle{
	width:100%;
	border:1px solid;
	padding:4px 2px;
	margin:0 1px;
	border-color: #bbbbbb;
	-webkit-border-radius:4px;
	-moz-border-radius:4px;
	border-radius:4px;
	background-color: #ffffff;
}
    </style>
    <script type="text/javascript">
        var companyLogoImageLoaded = false; //for firefox the resizeImageMaintainingAspectRatio($(strLogoSrc).val()); was called again and again in jquery
        var cmpThumbNailLogoImageLoaded = false; //for firefox the resizeImageMaintainingAspectRatio($(strLogoSrc).val()); was called again and again in jquery
        function clearControlsUsedInEdit() {
            var hidDesignationIdForEdit = document.getElementById('<%=hidDesignationIdForEdit.ClientID %>');
            var hidPopUpMode = document.getElementById('<%=hidPopUpMode.ClientID %>');
            hidDesignationIdForEdit.Value = "";
            hidPopUpMode.Value = "";
        }
        function loadCompanyLogo() {
            var strLogoSrc = $('#' + '<%= hidCompanyLogoUrl.ClientID %>');
            var imgLogoId = $('#imgLogo');
            var imgThumbNailLogoId = $('#imgThumbNailCmpLogo');
            $(imgLogoId).attr('src', $(strLogoSrc).val());
            $(imgThumbNailLogoId).attr('src', $(strLogoSrc).val());
            var originalImgWidth = 0;
            var originalImgHeight = 0;
            var strImageDim = getImgOriginalSizeFromHidField();
            var objImageDim = {};
            if (strImageDim) {
                objImageDim = JSON.parse(strImageDim);
            }
            if (objImageDim) {
                originalImgWidth = objImageDim.Width;
                originalImgHeight = objImageDim.Height;
            }
            $(imgLogoId).bind('load', function () {
                if (companyLogoImageLoaded) {
                    return;
                }
                else {
                    //$("#modalLogoPopUpContent>div.imgLiquidFill").imgLiquid();
                    //companyLogoImageLoaded = true;
                    companyLogoImageLoaded = resizeImageMaintainingAspectRatio($(strLogoSrc).val(), imgLogoId, 500, 500, originalImgWidth, originalImgHeight);
                }
            });
            $(imgThumbNailLogoId).bind('load', function () {
                if (cmpThumbNailLogoImageLoaded) {
                    return;
                }
                else {
                    //$("#divThumbNailImg.imgLiquidFill").imgLiquid();
                    //cmpThumbNailLogoImageLoaded = true;
                    cmpThumbNailLogoImageLoaded = resizeImageMaintainingAspectRatio($(strLogoSrc).val(), imgThumbNailLogoId, 50, 50, originalImgWidth, originalImgHeight);
                }
            });
            $("#divThumbNailImg").click(function () {
                showCompanyLogo(); return false;
            });
        }
        function getImgOriginalSizeFromHidField(imgSrc) {
            var strLogoDim = $('#' + '<%= hidCmpLogoDim.ClientID %>');
            if (strLogoDim) {
                return $(strLogoDim).val();
            }
            else {
                return "";
            }
        }
        $(document).ready(function () {
            try {
                loadCompanyLogo();
              // backgroundtitle();
            }
            catch (Error)
            { }
        });
        function showCompanyLogo() {
            try {
                showModalPopUp('divLogoModalContainer', 'Company Logo', 600, false);
            }
            catch (err)
            { }

        }
        function resizeImageMaintainingAspectRatio(src, imgLogoId, maxHeightReq, maxWidthReq, originalWidth, originalHeight) {
            var imgwidth = originalWidth;
            var imgheight = originalHeight;
            if (maxHeightReq < imgheight || maxWidthReq < imgwidth) {
                if (maxHeightReq < imgheight && imgwidth < imgheight) {
                    imgwidth = (imgwidth / imgheight) * maxWidthReq;
                    imgheight = 50;
                }
                else {
                    imgheight = (imgheight / imgwidth) * maxHeightReq;
                    imgwidth = 50;
                }
                $(imgLogoId).width(imgwidth);
                $(imgLogoId).height(imgheight);
                $(imgLogoId).attr('src', src);

            }
            else {
                $(imgLogoId).width(imgwidth);
                $(imgLogoId).height(imgheight);
                $(imgLogoId).attr('src', src);

            }
        }
        
    </script>
    <script  type="text/javascript">
        function backgroundtitle() {
            $('input[type="text"]').each(function () {

                this.value = $(this).attr('title');
                $(this).addClass('text-label');

                $(this).focus(function () {
                    if (this.value == $(this).attr('title')) {
                        this.value = '';
                        $(this).removeClass('text-label');
                    }
                });

                $(this).blur(function () {
                    if (this.value == '') {
                        this.value = $(this).attr('title');
                        $(this).addClass('text-label');
                    }
                });
            });
        }


        function openUpdateAdmin() {
            $('#divAdminUpdateInformation').show();
            $('#divAdminInformation').hide();
            
        }

        function cancelUpdateAdmin() {
            $('#divAdminUpdateInformation').hide();
            $('#divAdminInformation').show();
        }

        function showMessage(message, optionForScript/*DialogType*/) {
            var strOption = '', dialogImageType = '';
            switch (optionForScript) {
                case DialogType.Error:
                    strOption = $.alert.Error;
                    dialogImageType = DialogType.Error
                    break;
                case DialogType.Info:
                    strOption = $.alert.Information;
                    dialogImageType = DialogType.Info
                    break;
            }
            $.alert(message, strOption); showDialogImage(dialogImageType);
        }

        function cancelDetail() {
            var completedtls = '';
            var res = '';
            $('[id$=txtfirstname]').val('');
            $('[id$=txtmiddlename]').val('');
            $('[id$=txtlastname]').val('');
            $('[id$=txtmobile]').val('');
            $('[id$=txtEmail]').val('');
            completedtls = $('[id$=hidadmincomplete]').val()
            res = completedtls.split(",");
            $('[id$=txtfirstname]').val(res[0]);
            $('[id$=txtmiddlename]').val(res[1]);
            $('[id$=txtlastname]').val(res[2]);
            $('[id$=txtmobile]').val(res[3]);
            $('[id$=txtEmail]').val(res[4]);
        }



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="updCompanyDetails" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>
        <ContentTemplate>
            <div id="PageCanvasContent">
                <div id="divCompanyInformation">
                    <div id="divAlert">
                    </div>
                    <div class="g12 ">
                        <div class="widget">
                            <h3 class="handle">
                                Company Details</h3>
                            <div>
                                <span class="column1">
                                    <asp:Label ID="lblCompanyId" runat="server" Text="Company Id" class="BasicDetailHead"></asp:Label>
                                </span><span class="column2">
                                    <asp:Label ID="lblSeparator1" runat="server" Text=":"></asp:Label>
                                </span><span class="column3">
                                    <asp:Label ID="lblCompIdValue" runat="server"></asp:Label>
                                </span>
                                <br />
                                <br />
                                <span class="column1">
                                    <asp:Label ID="lblCompanyName" runat="server" Text="Name" class="BasicDetailHead"></asp:Label>
                                </span><span class="column2">
                                    <asp:Label ID="lblSeparator2" runat="server" Text=":"></asp:Label>
                                </span><span class="column3">
                                    <asp:Label ID="lblCompNameValue" runat="server"></asp:Label>
                                </span>
                                <br />
                                <br />
                                <span class="column1">
                                    <asp:Label ID="lblAddress" runat="server" Text="Address" class="BasicDetailHead"></asp:Label>
                                </span><span class="column2">
                                    <asp:Label ID="lblSeparator4" runat="server" Text=":"></asp:Label>
                                </span><span class="column3">
                                    <asp:Label ID="lblAddressValue" runat="server"></asp:Label>
                                </span>
                                <br />
                                <br />
                                <div style="margin-top: 10px;">
                                    <span class="column1">
                                        <asp:Label ID="lblLogo" runat="server" Text="Logo Name" class="BasicDetailHead"></asp:Label>
                                    </span><span class="column2">
                                        <asp:Label ID="lblSeparator5" runat="server" Text=":"></asp:Label>
                                    </span><span>
                                        <asp:Label ID="lblLogoValue" runat="server"></asp:Label>
                                    </span><span style="margin-left: 6px; float: left;">
                                        <asp:PlaceHolder ID="phLnkAddLogo" runat="server">
                                            <asp:LinkButton ID="lnkAddLogo" runat="server" Text="add" ToolTip="Add Logo" OnClientClick="showModalPopUpWithOutHeader('divImageUploadModal',' Add Company Logo ',400,false);return false;"></asp:LinkButton></asp:PlaceHolder>
                                        <asp:PlaceHolder ID="phLnkViewAndChange" runat="server">
                                            <div id="divThumbNailImg" class="imageContainer" title="zoom">
                                                <img id="imgThumbNailCmpLogo" width="50" height="50" alt="Company Logo" src="" title="zoom"
                                                    style="cursor: pointer" />
                                            </div>
                                            <asp:LinkButton ID="lnkChangeLogo" class="fl" Style="margin-left: 20px;" runat="server"
                                                Text="[change]" OnClientClick="showModalPopUpWithOutHeader('divImageUploadModal',' Add Company Logo ',400,false);return false;"></asp:LinkButton>
                                                
          <asp:LinkButton ID="lnkRemoveLogo" class="fl" Style="margin-left: 20px;" runat="server"
                                                Text="[remove]" OnClientClick="showModalPopUpWithOutHeader('divImageRemove',' Remove  Logo ',300,false);return false;"></asp:LinkButton>
                                        </asp:PlaceHolder>
                                    </span>
                                </div>
                                <div style="height: 0px; clear: both">
                                </div>
                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
                <br />

                <div id="divAdminInformation">
                 <div id="div1">
                    </div>
                    <div class="g12 ">
                        <div class="widget">
                            <h3 class="handle">
                                Admin Details</h3>
                                <div>
                                <span class="column1">
                                    <asp:Label ID="Label1" runat="server" Text="Name" class="BasicDetailHead"></asp:Label>
                                </span><span class="column2">
                                    <asp:Label ID="Label2" runat="server" Text=":"></asp:Label>
                                </span><span class="column3">
                                    <asp:Label ID="lbladminname" runat="server"></asp:Label>
                                </span>
                                <br />
                                <br />
                                <span class="column1">
                                    <asp:Label ID="Label4" runat="server" Text="Email" class="BasicDetailHead"></asp:Label>
                                </span><span class="column2">
                                    <asp:Label ID="Label5" runat="server" Text=":"></asp:Label>
                                </span><span class="column3">
                                    <asp:Label ID="lbladminemail" runat="server"></asp:Label>
                                </span>
                                <br />
                                <br />
                                <span class="column1">
                                    <asp:Label ID="Label7" runat="server" Text="Mobile" class="BasicDetailHead"></asp:Label>
                                </span><span class="column2">
                                    <asp:Label ID="Label8" runat="server" Text=":"></asp:Label>
                                </span><span class="column3">
                                    <asp:Label ID="lblmobile" runat="server"></asp:Label>
                                </span>
                                <br />
                                <br />
                                  <span class="column1">
                                    <asp:Label ID="Label9" runat="server" Text="" class="BasicDetailHead"></asp:Label>
                                </span><span class="column2">
                                    <asp:Label ID="Label12" runat="server" Text=""></asp:Label>
                                </span><span class="column3">
                                    <asp:Button ID="btnEditsubmit" runat="server" OnClientClick="openUpdateAdmin();return false;" style="float:right" CssClass="btn" Text="Edit" />
                                </span>
                                <br />
                                <br />
                            </div>
                </div>
             
            </div>
        
            </div>
               <br />
               <div id="divAdminUpdateInformation" class="hide">
               <div id="div2">
                    </div>
                    <div class="g12 ">
                       <div class="widget">
                            <h3 class="handle">
                            Update  Admin Details</h3>
                         
                                <div>
                                <span class="column1">
                                    <asp:Label ID="Label3" runat="server" Text="Name" class="BasicDetailHead"></asp:Label>
                                </span><span class="column2">
                                    <asp:Label ID="Label6" runat="server" Text=":"></asp:Label>
                                </span><span class="column3">
                                <asp:TextBox ID="txtfirstname" title="First Name" runat="server" style="width:25%"></asp:TextBox>
                                <asp:TextBox ID="txtmiddlename" title="Middle Name" runat="server" style="width:25%"></asp:TextBox>
                                <asp:TextBox ID="txtlastname" title="Last Name" runat="server" style="width:25%"></asp:TextBox>
                                <asp:HiddenField ID="hidadmincomplete" runat="server" />
                                             </span>
                                <br />
                                <br />
                                <span class="column1">
                                    <asp:Label ID="Label10" runat="server" Text="Email" class="BasicDetailHead"></asp:Label>
                                </span><span class="column2">
                                    <asp:Label ID="Label11" runat="server" Text=":"></asp:Label>
                                </span><span class="column3">
                                    <asp:TextBox ID="txtEmail" title="Email" runat="server" style="width:25%"></asp:TextBox>
                                </span>
                                <br />
                                <br />
                                <span class="column1">
                                    <asp:Label ID="Label13" runat="server" Text="Mobile" class="BasicDetailHead"></asp:Label>
                                </span><span class="column2">
                                    <asp:Label ID="Label14" runat="server" Text=":"></asp:Label>
                                </span><span class="column3">
                                     <asp:TextBox ID="txtmobile" title="Mobile" runat="server" style="width:25%" ></asp:TextBox>
                                </span>
                                <br />
                                <br />
                                   <span class="column1">
                                    <asp:Label ID="Label15" runat="server" Text="" class="BasicDetailHead"></asp:Label>
                                </span><span class="column2">
                                    <asp:Label ID="Label16" runat="server" Text=""></asp:Label>
                                </span><span class="column3">
                                  <asp:Button ID="btnupdateCancel" runat="server" OnClientClick="cancelDetail();cancelUpdateAdmin();return false;" style="float:right"  CssClass="btn" Text="Cancel" />
                                    <asp:Button ID="btnUpdate" runat="server" OnClick="UpdateAdmin_Click"  CssClass="btn" style="float:right" Text="Update" />&nbsp;

                                </span>
                                <br />
                                <br />
                             
                              
                            </div>
                           
                           
                </div>
               
               </div>
            
                </div>

                <div>
                    <asp:HiddenField ID="hidDesignationIdForEdit" runat="server" />
                    <asp:HiddenField ID="hidPopUpMode" runat="server" />
                    <asp:HiddenField ID="hidCompanyLogoUrl" runat="server" />
                    <asp:HiddenField ID="hidUploadTypeNewOrEdit" runat="server" />
                    <asp:HiddenField ID="hidCmpLogoDim" runat="server" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divLogoModalContainer" style="display: none">
        <div id="divCompanyLogoContainer" style="display: block">
            <div id="modalLogoPopUpContent" style="height: 100%;">
                <div id="divPopUpImgCont" class="imageContainer" style="margin: auto; width:auto;height:auto">
                    <img id="imgLogo" alt="Company Logo" src="" style="display:block;margin: auto;" />
                </div>
                <div style="height: 10px;">
                </div>
                <div style="text-align: center;">
                    <asp:Button ID="btnImagePopUpClose" runat="server" Text="cancel" CssClass="aspButton"
                        OnClientClick="closeModalPopUp('divLogoModalContainer');return false;" />
                </div>
            </div>
        </div>
    </div>
    <section>
        <div id="divImageUploadModal" style="display: none">
            <asp:UpdatePanel ID="updCompanyImgUpload" runat="server">
                <ContentTemplate>
                    <div id="divFileUploadContainer" style="display: block">
                        <div id="divFileUploadFields" style="margin: auto; width: 320px;">
                            <div style="float: left; margin-top: 15px; margin-left: 15px;">
                                <asp:FileUpload ID="upImage" runat="server" />
                            </div>
                            <div style="clear: both">
                            </div>
                            <div class="modalPopUpAction">
                                <div id="divPopUpActionCont" class="popUpActionCont" style="width: 60%;">
                                    <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btn aspButton"
                                        OnClick="btnUpload_Click" Style="position: relative; top: 2px;" OnClientClick="isCookieCleanUpRequired('false');"
                                        CommandArgument="New" />
                                    <asp:Button ID="btnImgPopUpCancel" runat="server" Text="Cancel" CssClass="btn aspButton"
                                        Style="position: relative; top: 2px;" OnClientClick="$('#divImageUploadModal').dialog('close');return false;" />
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField ID="hidErrorOnUpload" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
         <div id="divImageRemove"  style="display:none">
        <div class="modalPopUpAction44">
        <div style="margin-top:10px">
                        <a id="aMsgCmdDelCmf">Do you want to Remove  Company Logo?</a>
                    </div>
                    <div class="MarginT10">
                    </div>
                    <div class="SubProcborderDiv" style="margin-top:30px">
                    <div class="SubProcBtnDiv" align="center">
                         <asp:Button ID="btnremove" runat="server" Text="Yes" CssClass="InputStyle" OnClick="btnUpload_Click1"
                                         OnClientClick="isCookieCleanUpRequired('false');" />

                                        <asp:Button ID="btncancel" runat="server" Text="No" CssClass="InputStyle"
                                         OnClientClick="$('#divImageRemove').dialog('close');return false;" />
    
        </div>
        </div>
        </div>
        </div>
    </section>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
            isCookieCleanUpRequired('true');
            loadCompanyLogo();
        }
    </script>
</asp:Content>

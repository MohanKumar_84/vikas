﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="CngAcc.aspx.cs" Inherits="mFicientCP.CngAcc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="PageCanvasContent">
                <section>
                    <asp:Panel ID="pnlChangePassword" runat="server">
                        <div class="g12">
                            <div id="formDiv">
                                <fieldset>
                                    <section>
                                        <div>
                                            <span style="font-style: italic">
                                                <asp:Label ID="lblPasswordInfo" runat="server" Text="Password should be atleast 6 character long"></asp:Label></span>
                                        </div>
                                    </section>
                                    <section>
                                        <label for="txtOldPassword">
                                            Old Password</label>
                                        <div>
                                            <asp:TextBox ID="txtOldPassword" runat="server" Width="250" TextMode="Password"></asp:TextBox>
                                        </div>
                                    </section>
                                    <section>
                                        <label for="txtNewPassword">
                                            New Password</label>
                                        <div>
                                            <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" Width="250"></asp:TextBox>
                                        </div>
                                    </section>
                                    <section>
                                        <label for="txtReEnterPassword">
                                            Re Type Password</label>
                                        <div>
                                            <asp:TextBox ID="txtReEnterPassword" runat="server" TextMode="Password" Width="250"></asp:TextBox>
                                        </div>
                                    </section>
                                </fieldset>
                                <section id="buttons">
                                    <asp:Button ID="btnSave" runat="server" CssClass="aspButton" OnClick="btnSave_Click"
                                        Text="Save" />
                                </section>
                            </div>
                        </div>
                    </asp:Panel>
                </section>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div>
        <%--<asp:HiddenField ID="hfs" runat="server" />--%>
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientCP
{
    public partial class adminGroupList : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;
        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!Page.IsPostBack)
            {
                if (hfsValue != string.Empty)
                    BindGroupsRepeater();
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    BindGroupsRepeater();
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, String.Empty);
                    return;
                }
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, String.Empty);
            }
        }

        protected void BindGroupsRepeater()
        {
            GetSubAdminGroups getSubadminGroups = new GetSubAdminGroups(hfsPart3);
            getSubadminGroups.GetAdminGroupList();
            rptGroupDtls.DataSource = getSubadminGroups.GroupDetails;
            rptGroupDtls.DataBind();
        }

        protected void fillGroupUsersInHiddenField(string _GroupId)
        {
            GetUsersofGroup getUsers = new GetUsersofGroup();
            getUsers.GroupId = _GroupId;
            getUsers.Process();
            if (getUsers.StatusCode == 0)
            {
                DataView dvGroupUsers = getUsers.GroupUsers.DefaultView;
                dvGroupUsers.Sort = "USER_NAME";
                DataTable dtblGroupUsers = dvGroupUsers.ToTable();

                hidExistingUsers.Value = "";
                if (dtblGroupUsers.Rows.Count > 0)
                {
                    foreach (DataRow row in dtblGroupUsers.Rows)
                    {
                        if (string.IsNullOrEmpty(hidExistingUsers.Value))
                        {
                            hidExistingUsers.Value = Convert.ToString(row["USER_ID"]) + "," + Convert.ToString(row["USER_NAME"]) + "," + Convert.ToString(row["FIRST_NAME"]) + "," + Convert.ToString(row["LAST_NAME"]);
                        }
                        else
                        {
                            hidExistingUsers.Value += "|" + Convert.ToString(row["USER_ID"]) + "," + Convert.ToString(row["USER_NAME"]) + "," + Convert.ToString(row["FIRST_NAME"]) + "," + Convert.ToString(row["LAST_NAME"]);
                        }
                    }
                }
            }
        }

        private void userList(string groupId, string groupName)
        {
            GetUsersofGroup getUsersofGroup = new GetUsersofGroup();
            hfdGroupId.Value = getUsersofGroup.GroupId = groupId;
            fillGroupUsersInHiddenField(hfdGroupId.Value);
            updAddRemoveUsers.Update();
            lblGroupNme.Text = groupName;
            Utilities.showModalPopup("divAddRemoveUsers", updRepeater, "Group : " + groupName, "400", true, false);
            updAddRemoveUsers.Update();
        }

        protected void lnkBackToList_Click(object sender, EventArgs e)
        {
            pnlEditGroup.Visible = false;
            pnlRepeaterBox.Visible = true;
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            Utilities.closeModalPopUp("divAddRemoveUsers", updAddRemoveUsers);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Utilities.closeModalPopUp("divAddRemoveUsers", updAddRemoveUsers);
        }

        private void showTheHTMLComboBoxAfterPostback()
        {
            ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "Html Combo box Rendering", "formTheHTMLForComboBox();$(\"input\").uniform();", true);
        }

        protected void btnRowClickPostBack_Click(object sender, EventArgs e)
        {
            if (hidRowValuesUsedAfterPostBack.Value.Trim().Length != 0)
            {
                string[] aryValuesFromHidField = hidRowValuesUsedAfterPostBack.Value.Split(';');
                userList(aryValuesFromHidField[0], aryValuesFromHidField[1]);
                hidRowValuesUsedAfterPostBack.Value = "";
                showTheHTMLComboBoxAfterPostback();
            }

        }
    }
}
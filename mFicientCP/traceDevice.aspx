﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="traceDevice.aspx.cs" Inherits="mFicientCP.deviceLocationTrack" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">

 <link href="css/IdeDialog.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery.datatables.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script src="Scripts/mfSetting.js" type="text/javascript"></script>
    <script type="text/javascript">
        var ExtraProperties;
        function makeUsergroupAutoComplete(_lstAlreadySelectdgroup) {
            var _hidAutoCmpltAllGroup = $('#' + '<%=hidgrouplist.ClientID %>');
            if (_hidAutoCmpltAllGroup && $(_hidAutoCmpltAllGroup).val()) {
                var lstgrpDetail = JSON.parse($(_hidAutoCmpltAllGroup).val());
            }
            var _alreadyGroupsNew = [];
            if (_lstAlreadySelectdgroup) {
                var _alreadyGroups = _lstAlreadySelectdgroup.split(',');
                for (var i = 0; i < _alreadyGroups.length; i++)
                    for (var j = 0; j < lstgrpDetail.length; j++)
                        if (_alreadyGroups[i] == lstgrpDetail[j].id) {
                            _alreadyGroupsNew.push(lstgrpDetail[j]);
                            break;
                        }
            }
            if (lstgrpDetail) {
                $("#" + "<% = txtLocGroup.ClientID %>").tokenInput(lstgrpDetail, {
                    theme: "facebook",
                    preventDuplicates: true,
                    prePopulate: _alreadyGroupsNew
                });
            }
        }
     
        function setTraceLocation(_isSetControl) {
            ExtraProperties = $('#' + '<%=hidPropperties.ClientID %>').val();
            var _hidtrackLocDetail = $('#' + '<%=hidTrackLocDetail.ClientID %>');
            var groups = '';
            var _resetflag = true;
            if (_hidtrackLocDetail) {
                var _trackLocDetail = _hidtrackLocDetail.val();
                if (_trackLocDetail && _trackLocDetail.length > 2) {
                    _resetflag = false;
                    var _objTrackLoc = JSON.parse(_trackLocDetail);
                    var _options = document.getElementById('<%= clkdays.ClientID %>').getElementsByTagName('input');
                    for (var i = 0; i < 7; i++) {
                        if (_objTrackLoc.dy[i] == '1') 
                            _options[i].checked = true;
                        else
                            _options[i].checked = false;
                    }
                    groups = _objTrackLoc.groups;
                    selectDropdown(_objTrackLoc.ftm.substring(0, 2), 'ddlFoHr');
                    selectDropdown(_objTrackLoc.ftm.substring(2), 'ddlFoMin');
                    selectDropdown(_objTrackLoc.ttm.substring(0, 2), 'ddlToHr');
                    selectDropdown(_objTrackLoc.ttm.substring(2), 'ddlToMin');
                    selectDropdown(_objTrackLoc.freq, 'freq');
                    $('#ddlSyncCommandType').val(_objTrackLoc.ctyp);
                    mfTrackLocation.ddlSyncCommandTypeChange(_objTrackLoc.ctyp);
                    $('#ddlObject').val(_objTrackLoc.cmd);
                    getColOptions(_objTrackLoc.lp, true);                    
                }
            }

            if (_resetflag || !_isSetControl) {
                var options = document.getElementById('<%= clkdays.ClientID %>').getElementsByTagName('input');
                for (var i = 0; i < 7; i++) {
                    options[i].checked = false;
                }
                selectDropdown('0', 'ddlFoHr');
                selectDropdown('0', 'ddlFoMin');
                selectDropdown('0', 'ddlToHr');
                selectDropdown('0', 'ddlToMin');
                selectDropdown('5', 'freq');
                selectDropdown("-1", 'ddlSyncCommandType');
                selectDropdown("-1", 'ddlObject');
                getColOptions([], true);
                mfTrackLocation.drawCustomPropertiesTable();
            }
            if (_isSetControl) makeUsergroupAutoComplete(groups);
            else $("#" + "<% = txtLocGroup.ClientID %>").tokenInput("clear");
            $("input").uniform();
            $('#' + '<%=hidTrackLocDetail.ClientID %>').val('')       
        }

        function selectDropdown(val, controlId) {
            $('[id$=' + controlId + ']').val(val);
            $('select[id$=' + controlId + ']').parent().children("span").text($('select[id$=' + controlId + ']').children('option:selected').text());
        }
        function getTrackLocationJSON() {

            $('#formDiv').find('.alert').remove();
            var _errorMsg = '';
            var _trackLocJSON = {};
            _trackLocJSON.groups = document.getElementById('<% = txtLocGroup.ClientID %>').value.toString();
            var days = '';
            var options = document.getElementById('<%= clkdays.ClientID %>').getElementsByTagName('input');
            for (var i = 0; i < options.length; i++) {
                if (options[i].checked) days += '1';
                else days += '0';
            }
            if (days == '0000000') _errorMsg = "Atleast select one day.<br/>";

            _trackLocJSON.dy = days;
            _trackLocJSON.ttm = document.getElementById('<% = ddlToHr.ClientID %>').value + document.getElementById('<% = ddlToMin.ClientID %>').value;
            _trackLocJSON.ftm = document.getElementById('<% = ddlFoHr.ClientID %>').value + document.getElementById('<% = ddlFoMin.ClientID %>').value;
            _trackLocJSON.freq = $('#freq').val();
            _trackLocJSON.ctyp = $('#ddlSyncCommandType').val();
            _trackLocJSON.cmd = $('#ddlObject').val();
            if (_trackLocJSON.cmd == '-1') _errorMsg += "Select data object.<br/>";
            var lp = [], _propError = [],prop=[];
            if (_colOptions && _colOptions.length > 0) {
                $.each(_colOptions, function (index, cval) {
                    var propValue = $('#_selProp_' + cval).val();
                    lp.push({ para: cval, val: propValue });                    
                    if (prop.indexOf(propValue) >= 0 && _propError.indexOf(propValue) < 0) {
                        _errorMsg += "Property '" + propValue.substring(1) + "' is used multiple.<br/>";
                        _propError.push(propValue);
                    }
                    prop.push(propValue);
                });
            }

            _trackLocJSON.lp = lp;
            $('[id$=hidTrackLocDetail]').val(JSON.stringify(_trackLocJSON));
            if (_errorMsg.length > 0) {
                $.wl_Alert(_errorMsg, 'info', '#formDiv');
                return false;
            }
            return true;
        }
        function Uniform() {
            CallUnformCss($('#ddlFoHr'));
            CallUnformCss($('#ddlFoMin'));
            CallUnformCss($('#ddlToHr'));
            CallUnformCss($('#ddlToMin'));
            CallUnformCss($('#ddlSyncCommandType'));
            CallUnformCss($('#ddlObject'));
        }
        function ConfirmMessage() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Are you sure! you want to reset it?")) {
                confirm_value.value = "Yes";
                return true;
            } else {
                confirm_value.value = "No";
                return false;
            }
            document.forms[0].appendChild(confirm_value);
        }
        var RowCount = 0;
        var _colOptions = [], _porpOptions = '';
        function getCommandList() {
            mfTrackLocation.ddlSyncCommandTypeChange($('#ddlSyncCommandType').val());
            CallUnformCss($('#ddlObject'));
        }
        function getColOptions(lp,isNew) {
            isNewForm = isNew;
            mfTrackLocation.ddlObjectChange($('#ddlObject').val(), lp);            
        }
        
    </script>
    <style type="text/css">
        li.token-input-input-token-facebook input[id^="token-input"]
        {
            border:0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divTrackLocation">
                <div style="margin: auto; width: 95%; margin-top: 0px;">
                    <div class="g12">
                        <div id="formDiv">
                            <fieldset>
                                <section>
                                    <label>
                                        Group(s)</label>
                                    <div>
                                        <asp:TextBox ID="txtLocGroup" runat="server" Width="50%" Border="0px"></asp:TextBox>
                                    </div>
                                </section>
                                <section>
                                    <label>
                                        Days</label>
                                    <div>
                                        <asp:CheckBoxList ID="clkdays" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Monday</asp:ListItem>
                                            <asp:ListItem>Tuesday</asp:ListItem>
                                            <asp:ListItem>wednesday</asp:ListItem>
                                            <asp:ListItem>Thursday</asp:ListItem>
                                            <asp:ListItem>Firday</asp:ListItem>
                                            <asp:ListItem>Saturday</asp:ListItem>
                                            <asp:ListItem>Sunday</asp:ListItem>
                                        </asp:CheckBoxList>
                                    </div>
                                </section>
                                <section>
                                    <label>
                                        From Time</label>
                                    <div>
                                        <div style="float: left;width:auto;clear:none;">
                                            <asp:DropDownList ID="ddlFoHr" runat="server" Width="25px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="float: left;width:auto;clear:none;margin-top:10px;">
                                            :</div>
                                        <div style="float: left;width:auto;clear:none;">
                                            <asp:DropDownList ID="ddlFoMin" runat="server" Width="25px">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </section>
                                <section>
                                    <label>
                                        To Time</label>
                                    <div>
                                        <div style="float: left;width:auto;clear:none;">
                                            <asp:DropDownList ID="ddlToHr" runat="server" Width="25px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="float: left;width:auto;clear:none;margin-top:10px;">
                                            :</div>
                                        <div style="float: left;width:auto;clear:none;">
                                            <asp:DropDownList ID="ddlToMin" runat="server" Width="25px">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </section>
                                <section>
                                    <label>Frequency (min)</label>
                                    <div>
                                        <div>
                                            <select id="freq" >
                                                <option value="5">5</option>
                                                <option value="10">10</option>
                                                 <option value="15">15</option>
                                                <option value="20">20</option>
                                                <option value="25">25</option>
                                                <option value="30">30</option>
                                                <option value="35">35</option>
                                                <option value="40">40</option>
                                                 <option value="45">45</option>
                                                <option value="50">50</option>
                                                <option value="55">55</option>
                                                <option value="60">60</option>
                                             </select>
                                        </div>
                                    </div>
                                </section>
                                <section>
                                    <label>
                                        Source Type</label>
                                    <div>
                                        <select id="ddlSyncCommandType" onchange="getCommandList();">
                                            <option value="-1">Select</option>
                                            <option value="1">Database</option>
                                            <option value="2">Webservice(WSDL)</option>
                                            <option value="3">Webservice(HTTP)</option>
                                            <option value="4">Webservice(XML-RPC)</option>
                                            <option value="5">OData</option>
                                        </select>
                                    </div>
                                </section>
                                <section>
                                    <label>
                                        Data Object</label>
                                    <div>
                                        <select id="ddlObject" onchange="getColOptions([],false);">
                                            <option value="-1">Select</option>
                                        </select>
                                    </div>
                                </section>
                            </fieldset>
                            <fieldset id="fidsetCustomUserproperty">
                                <div id="divProperties">
                                </div>
                            </fieldset> 
                            <asp:UpdatePanel ID="updTrackLoc" runat="server">
                            <ContentTemplate>   
                                <fieldset id="fldsetMessenger">
                                    <section>                     
                                        <div id="buttons" style="width: 100%; border: none; padding-left: 0px; padding-right: 0px;
                                                            text-align: center;">       
                                            <asp:Button ID="btnSaveTrackLocation" runat="server" Text="Save" OnClientClick="return getTrackLocationJSON();"
                                                OnClick="btnSaveTrackLocation_Click" class="aspButton"/>
                                            &nbsp;&nbsp;<asp:Button ID="btnResetTrackLocation" runat="server" Text="Reset" OnClientClick="return ConfirmMessage();"
                                                OnClick="btnResetTrackLocation_Click" class="aspButton" />
                                            
                                            <asp:HiddenField ID="hidAllDbCommands" runat="server" Value="" />
                                            <asp:HiddenField ID="hidAllWsCommands" runat="server" Value="" />
                                            <asp:HiddenField ID="hidAllOdataCommands" runat="server" Value="" />
                                            <asp:HiddenField ID="hidPropperties" runat="server" />
                                            <asp:HiddenField ID="hidTrackLocDetail" runat="server" />
                                            <asp:HiddenField ID="hidgrouplist" runat="server" />
                                        </div>
                                    </section>
                                </fieldset>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                        </div> 
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            //$("input").uniform();
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest(sender, args) {
            if (args.get_error() != undefined) {

                var msg = args.get_error().message.replace("Sys.WebForms.PageRequestManagerServerErrorException: ", "");
                //alert(msg);
                args.set_errorHandled(true);
            }

            //$("input").uniform();
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>

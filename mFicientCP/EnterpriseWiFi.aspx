﻿<%@ Page Title="mFicient | Enterprise WiFi" Language="C#" MasterPageFile="~/master/Canvas.master"
    AutoEventWireup="true" CodeBehind="EnterpriseWiFi.aspx.cs" Inherits="mFicientCP.EnterpriseWiFi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script src="Scripts/EnterpriseWiFi.js" type="text/javascript"></script>
    <style type="text/css">
        
    </style>
    <script type="text/javascript">
        var postBackByHtmlProcess = {
            "AddNewWiFiSSID": "1",
            "UpdateWiFiSSID": "2",
            "DeleteWiFiSSID": "3"
        };
    </script>
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="divEnterpriseWiFiListCont" style="width:50%;margin:auto;">
        <table id="tblEnterpriseWiFiInfo">
            <thead>
                <tr>
                    <th>
                        WiFi SSID
                    </th>
                    <th>
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div id="divUpdateWiFiSSID" style="display:none;">
        <asp:UpdatePanel ID="updUpdateWiFiSSID" runat="server">
            <ContentTemplate>
                <div class="g12">
                    <div class="g3">
                        WiFi SSID :</div>
                    <div class="g8">
                        <input type="text" id="txtUpdateWiFiSSIDName" autocomplete="false" />
                    </div>
                </div>
                <div class="g12" style="text-align: center">
                    <asp:Button ID = "btnUpdateName" Text="Ok" runat="server" OnClick="btnUpdateName_Click" OnClientClick="enterpriseWiFi.updateWiFiName();"/>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divHiddenFields">
        <asp:UpdatePanel ID="updHiddenFields" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:HiddenField ID="hidWiFiInfos" runat="server" Value="" />
                <asp:HiddenField ID="hidDtlsForPostBackByHtmlCntrl" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!--
    POST BACK BUTTON FOR HTML CONTROLS-->
    <div style="display: none;">
        <asp:UpdatePanel ID="updPostbackByHtmlCntrl" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnPostbackByHtmlCntrl" runat="server" Text="" OnClick="btnPostbackByHtmlCntrl_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!--End POST BACK BUTTON FOR HTML CONTROLS -->
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            //$("input").uniform();
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            //$("input").uniform();
            hideWaitModal();
            isCookieCleanUpRequired('true');
            //loadCompanyLogo();
        }
    </script>
</asp:Content>

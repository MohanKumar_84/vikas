﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using System.Collections;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;

namespace mFicientCP
{
    public partial class AppManager1 : System.Web.UI.Page
    {

        string fsValue = string.Empty;
        string fbidValue = string.Empty;
        string strUserId = string.Empty; // Subadmin ID
        string strSessionId = string.Empty;
        string fsPart3 = string.Empty;
        string fsPart4 = string.Empty;// Company ID
        string hfsPart5 = string.Empty;
        string hfsPart6 = string.Empty;

        #region ID PREPEND STRING CONSTANTS FOR FORMING HTML USING HTML WRITER

        const string SPAN_WORKFLOW_NAME_ID_PREPEND_STRING = @"WF_NAME";
        const string SPAN_WORKFLOW_ID_PREPEND_STRING = "@WF_ID";
        const string SPAN_CATEGORY_ID_PREPEND_STRING = "@CATEGORY_ID";
        const string LINK_ADD_REMOVE_ID_PREPEND_STRING = @"lnkUser";
        const string SPAN_GROUP_NAME_ID_PREPEND_STRING = @"GROUP_NAME";
        const string SPAN_GROUP_ID_PREPEND_STRING = @"GROUP_ID";
        const string SPAN_GROUP_SUBADMIN_ID_PREPEND_STRING = @"SUBADMIN_ID";
        #endregion

        public static DataTable MenuCategoryCollection = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            Literal ltFullNAME = (Literal)this.Master.Master.FindControl("ltFullNAME");
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                fsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                fbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }
                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;
                fsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                fbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }
            if (string.IsNullOrEmpty(fsValue) || string.IsNullOrEmpty(fbidValue)) return;

            context.Items["hfs"] = fsValue;
            context.Items["hfbid"] = fbidValue;
            string[] hfsParts = Utilities.DecryptString(fsValue).Split(',');
            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            fsPart3 = hfsParts[2];
            fsPart4 = hfsParts[3];
            int lengthA = hfsParts.Length;
            if (lengthA > 4)
            {
                hfsPart5 = hfsParts[4];
                hfsPart6 = hfsParts[5];
                context.Items["hfs5"] = hfsPart5;
                context.Items["hfs6"] = hfsPart6;
                ltFullNAME.Text = hfsPart5 + " (" + hfsPart6 + ")";
            }
            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = fsPart3;
            context.Items["hfs4"] = fsPart4;
            if (!Page.IsPostBack)
            {
                rptMenuCategoryDtlsbind();
                hidTabSelected.Value = "1";
            }
            Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback2", true,
                    "makeTabAfterPostBack();showSelectedTabOnPostBack();");
        }


        List<AppAndCategoryLink> getExistingAppsFromHiddenField()
        {
            List<AppAndCategoryLink> codelst1 = new List<AppAndCategoryLink>();
            if (!String.IsNullOrEmpty(hidExistingUsers.Value))
            {
                string[] userIdsToSave = hidExistingUsers.Value.Split('|');
                if (userIdsToSave.Length > 0)
                {
                    for (int i = 0; i <= userIdsToSave.Length - 1; i++)
                    {
                        string[] aryUserDtl = userIdsToSave[i].Split(',');
                        codelst1.Add(new AppAndCategoryLink { WORKFLOW_NAME = aryUserDtl[1], WORKFLOW_ID = aryUserDtl[0], Category_ID = aryUserDtl[2] });
                    }
                }
            }
            return codelst1;
        }



        //    hidexistingapp.value

        DataTable getOtherAppsFromHiddenField()
        {
            DataTable dtblUsersList = new DataTable();
            dtblUsersList.Columns.Add("WF_Name", typeof(string));
            dtblUsersList.Columns.Add("WF_ID", typeof(string));
            dtblUsersList.Columns.Add("Category_ID", typeof(string));
            DataRow row = null;
            if (!String.IsNullOrEmpty(hidOtherAvailableUsers.Value))
            {
                string[] userIdsToRemove = hidOtherAvailableUsers.Value.Split('|');
                if (userIdsToRemove.Length > 0)
                {
                    for (int i = 0; i <= userIdsToRemove.Length - 1; i++)
                    {
                        string[] aryUserDtl = userIdsToRemove[i].Split(',');
                        row = dtblUsersList.NewRow();
                        row["WF_NAME"] = aryUserDtl[1];
                        row["WF_ID"] = aryUserDtl[0];
                        row["Category_ID"] = aryUserDtl[2];
                        dtblUsersList.Rows.Add(row);
                    }
                }
            }
            return dtblUsersList;
        }

        #region PublishApplication

        protected void rptWFPublishAppRepeter()
        {

            GetAppGroupsByCategory objGetAppGroupByCategory = new GetAppGroupsByCategory(true, "", strUserId, fsPart4);
            objGetAppGroupByCategory.Process();
            if (objGetAppGroupByCategory.StatusCode == 0)
            {
                rptWFPublishApp.DataSource = objGetAppGroupByCategory.Resultdataset.Tables[0];
                rptWFPublishApp.DataBind();

            }
            else
            {
                Utilities.showMessage("No Record Found", updGrpsInformation, DIALOG_TYPE.Info);

            }
        }

        protected void repetergroupbind()
        {
            UserGroupDetails objusergroupdetails = new UserGroupDetails(fsPart4);
            objusergroupdetails.Process();
            if (objusergroupdetails.StatusCode == 0)
            {
                DataTable dtblSource = makeDatasourceForRptGroup(objusergroupdetails.ResultTable, objusergroupdetails.AllGroupsInCompany);
                repetergroup.DataSource = dtblSource;
                repetergroup.DataBind();
            }
            else
            {
                pnlEmptyPublishApps.Visible = true;
            }

        }
        DataTable makeDatasourceForRptGroup(DataTable groupDtls, DataTable allGroupIds)
        {
            DataTable dtblSourceForBinding = new DataTable();
            dtblSourceForBinding.Columns.Add("GROUP_ID", typeof(string));
            dtblSourceForBinding.Columns.Add("GROUP_NAME", typeof(string));
            dtblSourceForBinding.Columns.Add("WF_IDS", typeof(string));
            dtblSourceForBinding.Columns.Add("WF_NAMES", typeof(string));
            if (allGroupIds != null && groupDtls != null)
            {
                foreach (DataRow row in allGroupIds.Rows)
                {
                    string strGroupId = Convert.ToString(row["GROUP_ID"]);
                    string strGroupName = "";
                    DataRow[] rowsByGroupId = groupDtls.Select("GROUP_ID = '" + strGroupId + "'");
                    string strWFIds = "", strWfNames = "";
                    if (rowsByGroupId.Length > 0)
                    {
                        strGroupName = Convert.ToString(rowsByGroupId[0]["GROUP_NAME"]);
                        for (int i = 0; i <= rowsByGroupId.Length - 1; i++)
                        {
                            if (String.IsNullOrEmpty(strWFIds))
                            {
                                strWFIds = Convert.ToString(rowsByGroupId[i]["WF_ID"]);
                                strWfNames = Convert.ToString(rowsByGroupId[i]["WF_NAME"]);
                            }
                            else
                            {
                                strWFIds += "," + "&nbsp;" + Convert.ToString(rowsByGroupId[i]["WF_ID"]);
                                strWfNames += "," + "&nbsp;" + Convert.ToString(rowsByGroupId[i]["WF_NAME"]);
                            }
                        }
                        DataRow rowToAdd = dtblSourceForBinding.NewRow();
                        rowToAdd["GROUP_ID"] = strGroupId;
                        rowToAdd["GROUP_NAME"] = strGroupName;
                        rowToAdd["WF_IDS"] = strWFIds;
                        rowToAdd["WF_NAMES"] = strWfNames;
                        dtblSourceForBinding.Rows.Add(rowToAdd);

                    }

                }
            }
            return dtblSourceForBinding;

        }


        DataTable makeDatasourceForRptMenuCategory(DataTable menuDtls, DataTable ExistingApp, DataTable AvilabelApp)
        {
            DataTable dtblSourceForBinding = new DataTable();
            dtblSourceForBinding.Columns.Add("MENU_CATEGORY_ID", typeof(string));
            dtblSourceForBinding.Columns.Add("CATEGORY", typeof(string));
            dtblSourceForBinding.Columns.Add("WF_IDS", typeof(string));
            dtblSourceForBinding.Columns.Add("WF_NAME", typeof(string));
            dtblSourceForBinding.Columns.Add("NOTWF_NAME", typeof(string));
            dtblSourceForBinding.Columns.Add("DISPLAY_INDEX", typeof(string));
            dtblSourceForBinding.Columns.Add("WF_ID", typeof(string));
            if (menuDtls != null)
            {
                foreach (DataRow row in menuDtls.Rows)
                {
                    string strCatId = Convert.ToString(row["MENU_CATEGORY_ID"]);
                    string strCatergoryName = Convert.ToString(row["CATEGORY"]);
                    string strDisplayIndex = Convert.ToString(row["DISPLAY_INDEX"]);
                    DataRow[] rowsByGroupId = ExistingApp.Select("CATEGORY_ID = '" + strCatId + "'");
                    DataRow[] rowsByGroupId1 = AvilabelApp.Select("MENU_CATEGORY_ID = '" + strCatId + "'");
                    string strWFIds = "", strWfNames = "", strWFNotApp = "", strWFNotAppWfID = "";
                    if (strCatergoryName != "")
                    {
                        if (rowsByGroupId.Length > 0)
                        {
                            for (int i = 0; i <= rowsByGroupId.Length - 1; i++)
                            {
                                if (String.IsNullOrEmpty(strWFIds))
                                {
                                    strWFIds = Convert.ToString(rowsByGroupId[i]["WF_ID"]);
                                    strWfNames = Convert.ToString(rowsByGroupId[i]["WF_NAME"]);

                                }
                                else
                                {
                                    strWFIds += "," + Convert.ToString(rowsByGroupId[i]["WF_ID"]);
                                    strWfNames += "," + "&nbsp;" + Convert.ToString(rowsByGroupId[i]["WF_NAME"]);

                                }
                            }
                        }
                        else
                        {
                            strWFIds = "";
                            strWfNames = "";
                        }
                    }

                    if (strCatergoryName != "")
                    {
                        if (rowsByGroupId1.Length > 0)
                        {
                            for (int j = 0; j <= rowsByGroupId1.Length - 1; j++)
                            {
                                if (String.IsNullOrEmpty(strWFNotApp))
                                {
                                    strWFNotAppWfID = Convert.ToString(rowsByGroupId1[j]["WF_ID"]);
                                    strWFNotApp = Convert.ToString(rowsByGroupId1[j]["WF_NAME"]);

                                }
                                else
                                {
                                    strWFNotAppWfID += "," + Convert.ToString(rowsByGroupId1[j]["WF_ID"]);
                                    strWFNotApp += "," + "&nbsp;" + Convert.ToString(rowsByGroupId1[j]["WF_NAME"]);
                                }
                            }
                        }
                        else
                        {
                            strWFNotAppWfID = "";
                            strWFNotApp = "";
                        }
                    }

                    DataRow rowToAdd = dtblSourceForBinding.NewRow();
                    rowToAdd["MENU_CATEGORY_ID"] = strCatId;
                    rowToAdd["CATEGORY"] = strCatergoryName;
                    rowToAdd["WF_IDS"] = strWFIds;
                    rowToAdd["WF_NAME"] = strWfNames;
                    rowToAdd["DISPLAY_INDEX"] = strDisplayIndex;
                    rowToAdd["NOTWF_NAME"] = strWFNotApp;
                    rowToAdd["WF_ID"] = strWFNotAppWfID;
                    dtblSourceForBinding.Rows.Add(rowToAdd);
                }
            }
            return dtblSourceForBinding;
        }




        protected void btnPulishAppSave_Click(object sender, EventArgs e)
        {
            string strverstion = hdfselectedVersion.Value;
            string strPreviousVersion = "";
            if (String.IsNullOrEmpty(hdfPrvVersion.Value)) strPreviousVersion = strverstion;
            else strPreviousVersion = hdfPrvVersion.Value;
            PublishApplication objPublishApp = new PublishApplication(hdfPubAppWfId.Value, strUserId, strPreviousVersion, strverstion, fsPart4);
            objPublishApp.Process();
            if (objPublishApp.StatusCode == 0)
            {
                rptWFPublishAppRepeter();
                rptMenuCategoryDtlsbind();
                hdfPubAppWfId.Value = "";
                hdfselectedVersion.Value = "";
                Utilities.SendNotificationMenuUpdate(fsPart4, "1");
                Utilities.showMessage("App published successfully", updSubProcPublichApp, DIALOG_TYPE.Info);
                Utilities.closeModalPopUp("SubProcPublishApp", updSubProcPublichApp, "CloseGroupChangePopUp");
            }
            else
            {
                hdfPubAppWfId.Value = "";
                hdfselectedVersion.Value = "";
                Utilities.closeModalPopUp("SubProcPublishApp", updSubProcPublichApp, "CloseGroupChangePopUp");
                Utilities.showMessage("Internal server error.", updSubProcPublichApp, DIALOG_TYPE.Error);
            }
            updSubProcPublichApp.Update();
        }
        protected void btnUnpublish_Click(object sender, EventArgs e)
        {
            AppUnpublish(hdfPubAppWfId.Value, fsPart4, strUserId);
            Utilities.closeModalPopUp("SubProcPublishApp", updPublishApp);
            hdfPubAppWfId.Value = "";
        }

        private void AppUnpublish(string _workflowid, string _companyId, string _SubAdminId)
        {
            AppUnpublish objUnpublish = new AppUnpublish(_workflowid, _companyId, _SubAdminId, hdfselectedVersion.Value);
            objUnpublish.Process();
            if (objUnpublish.StatusCode == 0)
            {
                rptWFPublishAppRepeter();
                Utilities.showMessage("Version Unpublish successfully", updPublishApp, DIALOG_TYPE.Info);
                Utilities.SendNotificationMenuUpdate(fsPart4, "2");
            }
            else
            {
                Utilities.showMessage("Internal server error.", updGrpsInformation, DIALOG_TYPE.Error);
            }
            updPublishApp.Update();
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "_showUpDownArrowInTable();", true);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int displayIndex;
            int z = 0;
            string strexistingapp = "";
            string straddnewapps = "";

            if (!string.IsNullOrEmpty(hidOtherAvailableUsers.Value))
            {
                JObject jo = JObject.Parse(hidOtherAvailableUsers.Value);
                strexistingapp = jo["remove"].ToString();
                if (strexistingapp.Length > 0) strexistingapp = strexistingapp.Substring(1);
                straddnewapps = jo["add"].ToString();
                if (straddnewapps.Length > 0) straddnewapps = straddnewapps.Substring(1);
            }

            List<AppAndCategoryLink> codelst1 = new List<AppAndCategoryLink>();
            if (txtupdatectegryinfo.Text == "")
                Utilities.showMessage("* Please Enter valid category", updSubProcCat, DIALOG_TYPE.Error);

            else if (txtupdatectegryinfo.Text != "" && (txtupdatectegryinfo.Text != hidupdatecategoryinfoname.Value))
            {
                UpdateDisplayIndex();
                displayIndex = getdisplayIndex(hidupdatecategoryinfoid.Value, out z);
                updatecategory(displayIndex);

                codelst1 = getExistingAppsFromHiddenField();

                SaveManuAppCategory objSaveMenuCategory = new SaveManuAppCategory(fsPart4, txtupdatectegryinfo.Text, displayIndex, "", true, hidupdatecategoryinfoid.Value, strUserId, codelst1, strexistingapp, straddnewapps);
                objSaveMenuCategory.Process();
                if (objSaveMenuCategory.StatusCode == 0)
                {
                    Utilities.SendNotificationMenuUpdate(fsPart4, "3");
                    Utilities.showMessage("App in this Menu category saved successfully.", updSubProcCat, "ShowSuccessMessageOnMenuSave", DIALOG_TYPE.Info);
                    if (hidMenuCategoryForEdit.Value != "")
                        hidupdatecategoryinfoid.Value = "";
                    Utilities.closeModalPopUp("SubProcMenuCategory", updSubProcCat);
                    rptMenuCategoryDtlsbind();
                    updSubProcCat.Visible = true;
                    updMenuCat.Update();
                }
            }
            else
            {
                UpdateDisplayIndex();
                displayIndex = getdisplayIndex(hidupdatecategoryinfoid.Value, out z);
                codelst1 = getExistingAppsFromHiddenField();

                SaveManuAppCategory objSaveMenuCategory = new SaveManuAppCategory(fsPart4, txtupdatectegryinfo.Text, displayIndex, "", true, hidupdatecategoryinfoid.Value, strUserId, codelst1, straddnewapps, strexistingapp);
                objSaveMenuCategory.Process();
                if (objSaveMenuCategory.StatusCode == 0)
                {
                    Utilities.showMessage("App  in this Menu category saved successfully", updSubProcCat, "ShowSuccessMessageOnMenuSave", DIALOG_TYPE.Info);
                    if (hidMenuCategoryForEdit.Value != "")
                        hidupdatecategoryinfoid.Value = "";

                    Utilities.closeModalPopUp("SubProcMenuCategory", updSubProcCat);
                    Utilities.SendNotificationMenuUpdate(fsPart4, "3");
                    rptMenuCategoryDtlsbind();
                    updSubProcCat.Visible = true;
                    updMenuCat.Update();
                    ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"_showUpDownArrowInTable();cleardiv();", true);
                }
            }
        }
        private int getdisplayIndex(string catid, out int dispIndex)
        {
            int displayIndex = 0;
            // string groups = "";
            GetMenuCategoryNew objGetMenuCategory = new GetMenuCategoryNew(fsPart4, catid);
            
            if (objGetMenuCategory.ResultTable != null)
            {
                if (objGetMenuCategory.ResultTable.Rows.Count > 0)
                {
                    if (catid == "") displayIndex = Convert.ToInt32(objGetMenuCategory.ResultTable.Select("DISPLAY_INDEX=MAX(DISPLAY_INDEX)")[0]["DISPLAY_INDEX"]) + 1;
                    else
                    {
                        string strFilter = String.Format("MENU_CATEGORY_ID = '{0}'", catid);
                        DataRow[] row = objGetMenuCategory.ResultTable.Select(strFilter);
                        displayIndex = (int)row[0]["DISPLAY_INDEX"];
                    }
                }
                else
                {
                    displayIndex = 1;
                }
            }
            dispIndex = displayIndex;
            return dispIndex;
        }
        #endregion

        #region MenuCategories

        private void rptMenuCategoryDtlsbind()
        {
            GetMenuCategoryNew objGetMenuCategory = new GetMenuCategoryNew(fsPart4);
            objGetMenuCategory.Process();
            if (objGetMenuCategory.StatusCode == 0)
            {
                MenuCategoryCollection = objGetMenuCategory.ResultTable;
                DataTable dt = makeDatasourceForRptMenuCategory(objGetMenuCategory.ResultTable, objGetMenuCategory.ResultTable1, objGetMenuCategory.ResultTable2);
                rptMenuCategoryDtls.DataSource = dt;
                rptMenuCategoryDtls.DataBind();
                rptWFPublishAppRepeter();
            }
            else
            {
                pnlEmptyMenuCat.Visible = true;
                rptMenuCategoryDtls.DataSource = null;
                rptMenuCategoryDtls.DataBind();
            }
            repetergroupbind();
        }

        void processDeleteCategory(RepeaterCommandEventArgs e)
        {
            Label lblMenuCategoryId = (Label)e.Item.FindControl("lblMenuCategoryId");
            Label lblCategoryNm = (Label)e.Item.FindControl("lblCategoryNm");
            DeleteMenuCategory(lblMenuCategoryId.Text, lblCategoryNm.Text);
            rptMenuCategoryDtlsbind();
        }

        private void DeleteMenuCategory(string _Id, string _name)
        {
            DeleteMenuCategory objDeleteMenuCategory = new DeleteMenuCategory(fsPart4, _Id, strUserId, _name);
            objDeleteMenuCategory.Process();
            if (objDeleteMenuCategory.StatusCode == 0)
            {
                updMenuCat.Update();
                Utilities.SendNotificationMenuUpdate(fsPart4, "3");
                Utilities.showMessage("Delete Sucessfully", updMenuCat, "message", DIALOG_TYPE.Info);
            }
            else
            {
                Utilities.showMessage("Internal server error.Please try again", updMenuCat, "message", DIALOG_TYPE.Error);
            }
        }

        DataTable getIndexHiddenField()
        {
            DataTable dtblUsersList = new DataTable();
            dtblUsersList.Columns.Add("MENU_CATEGORY_ID", typeof(string));
            dtblUsersList.Columns.Add("DISPLAY_INDEX", typeof(string));
            dtblUsersList.Columns.Add("COMPANY_ID", typeof(string));

            DataRow row = null;
            if (!String.IsNullOrEmpty(hidFinalDisplayIndexOfCat.Value))
            {
                string[] userIdsToSave = hidFinalDisplayIndexOfCat.Value.Split(';');
                if (userIdsToSave.Length > 0)
                {
                    for (int i = 0; i <= userIdsToSave.Length - 1; i++)
                    {
                        row = dtblUsersList.NewRow();

                        row["MENU_CATEGORY_ID"] = userIdsToSave[i];
                        row["DISPLAY_INDEX"] = i;
                        row["COMPANY_ID"] = fsPart4;


                        dtblUsersList.Rows.Add(row);
                    }
                }
            }
            return dtblUsersList;
        }

        protected void btnMenuCatIconClear_Click(object sender, EventArgs e)
        {
            hdfIconname.Value = "";
            UpdIconname.Update();
            ScriptManager.RegisterStartupScript(UpdIconname, typeof(UpdatePanel), Guid.NewGuid().ToString(), "setMenuIconOnClear();", true);
        }

        #endregion

        #region WorkFlowCategories
        private void EditAppFlowSettings(string _WorkFlowId, UpdatePanel _Upd)
        {
            string _CategoryId = "";
            GetCategoryAndGroupDtlsByWorkFlowId objGetCatAndGroupDtls = new GetCategoryAndGroupDtlsByWorkFlowId(_WorkFlowId);
            objGetCatAndGroupDtls.Process();
            DataSet dsCatAndGroupDtls = objGetCatAndGroupDtls.ResultTables;
            if (dsCatAndGroupDtls != null && dsCatAndGroupDtls.Tables.Count > 0)
            {
                if (dsCatAndGroupDtls.Tables[0] != null && dsCatAndGroupDtls.Tables[0].Rows.Count > 0)
                {
                    _CategoryId = (string)dsCatAndGroupDtls.Tables[0].Rows[0]["CATEGORY_ID"];
                }
                else { }
                if (dsCatAndGroupDtls.Tables[0] != null && dsCatAndGroupDtls.Tables[0].Rows.Count > 0)
                {
                    hdfIconname.Value = Convert.ToString(dsCatAndGroupDtls.Tables[0].Rows[0]["WF_ICON"]);
                    UpdIconname.Update();

                }
                else
                {
                    hdfIconname.Value = "";
                    UpdIconname.Update();
                }
            }

        }

        protected void rptMenuCategoryDtls_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Delete":
                    processDeleteCategory(e);
                    break;

            }

        }

        protected void btnProcessIconClear_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "setMenuIconOnClear();", true);
            hdfIconname.Value = "";
            UpdIconname.Update();
        }

        #endregion

        protected void Add_Click(object sender, EventArgs e)
        {
            string strErrorMessage = "";
            int displayIndex;
            string MenuId = string.Empty;
            if (txtCategoryName.Text.Trim().Length == 0) strErrorMessage = "* Please enter category name.<br />";
            if (strErrorMessage.Length > 0)
            {
                ScriptManager.RegisterStartupScript(updMenuCat, typeof(UpdatePanel), Guid.NewGuid().ToString(), "ShowMenuCatEdit();$('#aMessage').html('" + strErrorMessage + "');SubProcBoxMessage(true);", true);
                return;
            }
            else
            {
                GetMenuCategoryNew objGetMenuCategory = new GetMenuCategoryNew(fsPart4);
                objGetMenuCategory.GetCategoryList();
                if (objGetMenuCategory.ResultTable != null)
                {
                    if (objGetMenuCategory.ResultTable.Rows.Count > 0)
                    {
                        if (hidMenuCategoryForEdit123.Value == "") displayIndex = Convert.ToInt32(objGetMenuCategory.ResultTable.Select("DISPLAY_INDEX=MAX(DISPLAY_INDEX)")[0]["DISPLAY_INDEX"]) + 1;
                        else
                        {
                            string strFilter = String.Format("MENU_CATEGORY_ID = '{0}'", hidMenuCategoryForEdit123.Value);

                            DataRow[] row = objGetMenuCategory.ResultTable.Select(strFilter);
                            displayIndex = (int)row[0]["DISPLAY_INDEX"];

                        }

                    }
                    else
                    {
                        displayIndex = 1;
                    }
                    SaveMenuCategory objSaveMenuCategory = new SaveMenuCategory(fsPart4, txtCategoryName.Text, displayIndex,  true, hidMenuCategoryForEdit123.Value, strUserId, hdfIconname.Value, hidupdatecategoryinfoname.Value);
                    objSaveMenuCategory.Process();
                    if (objSaveMenuCategory.StatusCode == 0)
                    {
                        pnlEmptyMenuCat.Visible = false;
                        if (hidMenuCategoryForEdit123.Value == "")
                        {
                            Utilities.showMessage("Category Added  successfully", updMenuCat, "Successfull", DIALOG_TYPE.Info);

                        }
                        else
                        {
                            Utilities.showMessage("Updated successfully", updMenuCat, "Successfull Update", DIALOG_TYPE.Info);

                            hidMenuCategoryForEdit123.Value = "";
                        }
                        Utilities.closeModalPopUp("EditMenuCatDetailsDiv123", updMenuCat, "CloseMenuSavePopUp");
                        rptMenuCategoryDtlsbind();
                        updMenuCat.Update();
                        updPublishApp.Update();

                    }
                }
            }
        }

        private void UpdateDisplayIndex()
        {
            List<SelectetdDisplayIndex> lsist = new List<SelectetdDisplayIndex>();
            DataTable objupdated = getIndexHiddenField();
            foreach (DataRow row in objupdated.Rows)
            {
                lsist.Add(new SelectetdDisplayIndex { MenuCategory_ID = row["MENU_CATEGORY_ID"].ToString(), Display_ID = row["DISPLAY_INDEX"].ToString(), Company_ID = row["COMPANY_ID"].ToString() });
            }
            UpdatedDisplayIndex objUpdatedDisplayIndex = new UpdatedDisplayIndex(lsist);
            objUpdatedDisplayIndex.Process();
            int intStatusCode = objUpdatedDisplayIndex.StatusCode;

            if (intStatusCode == 0)
            {

                hidFinalDisplayIndexOfCat.Value = string.Empty;

            }
            else
            {
                Utilities.showMessage("Display Index Not Updated", updMenuCat, DIALOG_TYPE.Info);
            }
        }

        private void updatecategory(int displayIndex)
        {
            string groups = "";
            string MenuId = string.Empty;
            SaveMenuCategory objSaveMenuCategory = new SaveMenuCategory(fsPart4, txtupdatectegryinfo.Text, displayIndex,  true, hidupdatecategoryinfoid.Value, strUserId, hdfIconname.Value, hidupdatecategoryinfoname.Value);
            objSaveMenuCategory.Process();
            if (objSaveMenuCategory.StatusCode == 0)
            {
            }
            else
            {
                Utilities.showMessage("Internal server error", updSubProcCat, DIALOG_TYPE.Error);
            }
        }

        #region GroupInformation

        void formHTMLAssignedAppsInGroup(DataTable usersInGroupDtbl)
        {
            if (usersInGroupDtbl == null) throw new ArgumentNullException();
            StringWriter stringWriter = new StringWriter();
            using (HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter))
            {
                //header part
                htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "comboHeader");
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);




                htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
                htmlWriter.Write("Assigned Apps");
                htmlWriter.RenderEndTag();
                htmlWriter.RenderEndTag();

                //User List
                int iLoopCount = 0;
                StringBuilder sb = new StringBuilder();
                foreach (DataRow row in usersInGroupDtbl.Rows)
                {
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);

                    //first span tag for WorkFlow name
                    sb = new StringBuilder();
                    sb.Append(SPAN_GROUP_NAME_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["WF_NAME"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");

                    //Second span tag Group ID span
                    sb = new StringBuilder();
                    sb.Append(SPAN_GROUP_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Style, "display:none");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["GROUP_ID"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");
                    ////Third span Tag SubAdmin ID
                    sb = new StringBuilder();
                    sb.Append(SPAN_GROUP_SUBADMIN_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Style, "display:none");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["Group_Name"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");

                    //Fourth span tag WorkFlow ID span
                    sb = new StringBuilder();
                    sb.Append(SPAN_WORKFLOW_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Style, "display:none");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["WF_ID"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");

                    sb = new StringBuilder();
                    sb.Append(LINK_ADD_REMOVE_ID_PREPEND_STRING).Append("_");
                    sb.Append(Convert.ToString(row["WF_NAME"])).Append("_");
                    sb.Append(Convert.ToString(row["GROUP_ID"])).Append("_");
                    sb.Append(Convert.ToString(row["Group_Name"])).Append("_");
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "remove");
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Onclick, "moveappliedToOtherAvailableapp(this);");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.A);
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");
                    htmlWriter.RenderEndTag();
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "clear");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                    htmlWriter.RenderEndTag();
                    iLoopCount++;
                }
                divavailable.InnerHtml = stringWriter.ToString();
            }

        }

        void formHTMLForAvilableInGroup(DataTable usersNotInGroupDtbl)
        {

            if (usersNotInGroupDtbl == null) throw new ArgumentNullException();

            StringWriter stringWriter = new StringWriter();
            using (HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter))
            {
                //header part
                htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "comboHeader");
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
                htmlWriter.Write("Available Apps");
                htmlWriter.RenderEndTag();
                htmlWriter.RenderEndTag();
                //User List

                int iLoopCount = 0;
                StringBuilder sb = new StringBuilder();
                foreach (DataRow row in usersNotInGroupDtbl.Rows)
                {
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);


                    //first span tag for WorkFlow name
                    sb = new StringBuilder();
                    sb.Append(SPAN_GROUP_NAME_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["WF_NAME"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");

                    //Second span tag Group Id span
                    sb = new StringBuilder();
                    sb.Append(SPAN_GROUP_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Style, "display:none");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["GROUP_ID"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");
                    //Fourth span tag WorkFlow ID span
                    sb = new StringBuilder();
                    sb.Append(SPAN_WORKFLOW_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Style, "display:none");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["WF_ID"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");
                    //Third span Tag Groupname ID
                    sb = new StringBuilder();
                    sb.Append(SPAN_GROUP_SUBADMIN_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Style, "display:none");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["Group_Name"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");


                    //A tag
                    sb = new StringBuilder();
                    sb.Append(LINK_ADD_REMOVE_ID_PREPEND_STRING).Append("_");

                    sb.Append(Convert.ToString(row["WF_NAME"])).Append("_");
                    sb.Append(Convert.ToString(row["GROUP_ID"])).Append("_");
                    sb.Append(Convert.ToString(row["Group_Name"])).Append("_");
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "add");
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Onclick, "moveAvilableAppToExistingApps(this);");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.A);
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");

                    htmlWriter.RenderEndTag();
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "clear");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                    htmlWriter.RenderEndTag();
                    iLoopCount++;
                }
                divassigned.InnerHtml = stringWriter.ToString();
            }
        }

        protected void repetergroup_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Edit":
                    Label imggroup = (Label)e.Item.FindControl("lblgroupiddetails");
                    Label labname = (Label)e.Item.FindControl("lblgroupname1");
                    hgorupid.Value = imggroup.Text;
                    GetGroupAndAppDetails objGetGroupAndAppDetails = new GetGroupAndAppDetails(((Label)e.Item.FindControl("lblgroupiddetails")).Text, fsPart4, strUserId);
                    objGetGroupAndAppDetails.Process();

                    if (objGetGroupAndAppDetails.StatusCode == 0)
                    {
                        hidGrpIdInEditMode.Value = ((Label)e.Item.FindControl("lblgroupiddetails")).Text;
                        hidgroupname.Value = ((Label)e.Item.FindControl("lblgroupname1")).Text;
                        hidexistingappingroup.Value = ((Label)e.Item.FindControl("lblasignApps1")).Text;
                        hidexistingappingroup.Value = hidexistingappingroup.Value.Replace("&nbsp;", "");
                        formTheHTMLForMultiSelectGroupsListBoxes(objGetGroupAndAppDetails.ResultTables.Tables[0], objGetGroupAndAppDetails.ResultTables.Tables[1]);
                        Utilities.showModalPopup("divgroupsinformation", updAppInfoGroup, labname.Text, "515", true);
                        updAppInfoGroup.Update();
                        updGrpsInformation.Update();
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "_showUpDownArrowInTable();", true);
                    }
                    else
                    {
                        Utilities.showModalPopup("divgroupsinformation", updAppInfoGroup, labname.Text, "500", false);
                        ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "_showUpDownArrowInTable();", true);
                    }
                    break;

            }

        }


        protected void btnApply_Click(object sender, EventArgs e)
        {
            List<SelectetdDisplayIndex> lsist = new List<SelectetdDisplayIndex>();
            DataTable objupdated = getIndexHiddenField();
            foreach (DataRow row in objupdated.Rows)
            {
                lsist.Add(new SelectetdDisplayIndex { MenuCategory_ID = row["MENU_CATEGORY_ID"].ToString(), Display_ID = row["DISPLAY_INDEX"].ToString(), Company_ID = row["COMPANY_ID"].ToString() });
            }
            UpdatedDisplayIndex objUpdatedDisplayIndex = new UpdatedDisplayIndex(lsist);
            objUpdatedDisplayIndex.Process();
            int intStatusCode = objUpdatedDisplayIndex.StatusCode;
            string strStatusDesscription = objUpdatedDisplayIndex.StatusDescription;

            if (intStatusCode == 0)
            {
                //SendNotificationMenuUpdate(fsPart4);
                Utilities.showMessage("Updated successfully", updMenuCat, "Successfull Update", DIALOG_TYPE.Info);
                hidFinalDisplayIndexOfCat.Value = string.Empty;
                rptMenuCategoryDtlsbind();
            }
            else
            {
                Utilities.showMessage("No App Details find this Groups.", updGrpsInformation, DIALOG_TYPE.Info);

            }
            updMenuCat.Update();
        }

        List<string> getappliedAppsFromHiddenFieldForSaving()
        {
            List<string> codelst1 = new List<string>();
            if (!String.IsNullOrEmpty(hfappliedgroup.Value))
            {
                string[] userIdsToRemove = hfappliedgroup.Value.Split('|');
                if (userIdsToRemove.Length > 0)
                {
                    for (int i = 0; i <= userIdsToRemove.Length - 1; i++)
                    {
                        string[] aryUserDtl = userIdsToRemove[i].Split(',');
                        codelst1.Add(aryUserDtl[0]);
                    }
                }
            }
            return codelst1;
        }
        protected void btnSavegroup_Click(object sender, EventArgs e)
        {
            List<string> codelst1 = getappliedAppsFromHiddenFieldForSaving();
            string strexistingapp = "", straddnewapps = "";
            if (!string.IsNullOrEmpty(hidOtherAvailableUsers.Value))
            {
                JObject jo = JObject.Parse(hidOtherAvailableUsers.Value);
                strexistingapp = jo["remove"].ToString();
                if (strexistingapp.Length > 0) strexistingapp = strexistingapp.Substring(1);
                straddnewapps = jo["add"].ToString();
                if (straddnewapps.Length > 0) straddnewapps = straddnewapps.Substring(1);
            }

            UpdateUserGroupDetails objDetails = new UpdateUserGroupDetails(fsPart4, strUserId, codelst1, hidGrpIdInEditMode.Value, hidgroupname.Value, straddnewapps, strexistingapp);
            objDetails.Process();
            if (objDetails.StatusCode == 0)
            {
                repetergroupbind();
                hidGrpIdInEditMode.Value = "";
                Utilities.SendNotificationMenuUpdate(fsPart4, "4");
                Utilities.showMessage("Groups assigned successfully", updGrpsInformation, DIALOG_TYPE.Info);
                Utilities.closeModalPopUp("divgroupsinformation", updGrpsInformation, "CloseGroupChangePopUp");
            }
            else
            {
                Utilities.showMessage("Internal server error.", updGrpsInformation, DIALOG_TYPE.Error);
            }
            updGrpsInformation.Update();
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "_showUpDownArrowInTable();", true);
        }

        void showTheHTMLGROUPComboBoxAfterPostback(DataTable selectedForAppliedGroup, DataTable unselectedGroups)
        {
            formTheHTMLForMultiSelectGroupsListBoxes(selectedForAppliedGroup, unselectedGroups);
            ScriptManager.RegisterClientScriptBlock(updAppInfoGroup, typeof(UpdatePanel), "Html Combo box Rendering", "$(\"input\").uniform();", true);
        }

        void formTheHTMLForMultiSelectGroupsListBoxes(DataTable usersInGroupDtbl, DataTable otherUserAvailableDtbl)
        {
            try
            {
                formHTMLAssignedAppsInGroup(usersInGroupDtbl);
                formHTMLForAvilableInGroup(otherUserAvailableDtbl);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        protected void rptWFPublishApp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string strstatus = string.Empty;
                Label lblversion = (Label)e.Item.FindControl("lblversion");


                if (String.IsNullOrEmpty(lblversion.Text))
                {
                    strstatus = "Not published";
                    lblversion.Text = strstatus;

                }
            }
        }

        protected void rptMenuCategoryDtls_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ScriptManager.RegisterStartupScript(updMenuCat, typeof(UpdatePanel), Guid.NewGuid().ToString(), "_showUpDownArrowInTable();", true);

        }
    }
}
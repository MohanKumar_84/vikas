﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
namespace mFicientCP
{
    public partial class EnterpriseWiFi : System.Web.UI.Page
    {
        string SubAdminid = "", CompanyId = "", Sessionid = "", Adminid = "";
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        enum POST_BACK_BY_HTML_PROCESS
        {
            AddNewWiFiSSID = 1,
            UpdateWiFiSSID =2,
            DeleteWiFiSSID =3
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            this.SubAdminid = strUserId;
            this.CompanyId =hfsPart3 ;
            this.Adminid = hfsPart4;
            if (!Page.IsPostBack)
            {
                bindWiFiDtlsTable();

                Utilities.runPageStartUpScript(this.Page, "Add new App Dialog", @"enterpriseWiFi.setUI();");
            }
            else
            {
                //if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                //{
                    //Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, "");
                    //return;
                //}
                //Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, String.Empty);
            }
        }

        void bindWiFiDtlsTable()
        {
            EnterpriseWiFiProcess objEnterpriseWiFi = new EnterpriseWiFiProcess(this.CompanyId);
            List<MFEEnterpriseWiFi> lstWiFiInfos = objEnterpriseWiFi.GetWiFiDtlsByCompanyId();
            if (lstWiFiInfos != null)
            {
                hidWiFiInfos.Value = Utilities.SerializeJson<List<MFEEnterpriseWiFi>>(lstWiFiInfos);
            }
            else
            {
                lstWiFiInfos = new List<MFEEnterpriseWiFi>();
                hidWiFiInfos.Value = Utilities.SerializeJson<List<MFEEnterpriseWiFi>>(lstWiFiInfos);
            }
        }
        protected void btnPostbackByHtmlCntrl_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(hidDtlsForPostBackByHtmlCntrl.Value))
                {
                    PostbackByHtmlControlData objPostbackData =
                            Utilities.DeserialiseJson<PostbackByHtmlControlData>(hidDtlsForPostBackByHtmlCntrl.Value);
                    if (objPostbackData != null)
                    {
                        POST_BACK_BY_HTML_PROCESS eProcFor = (POST_BACK_BY_HTML_PROCESS)Enum.Parse(typeof(POST_BACK_BY_HTML_PROCESS),
                                    objPostbackData.processFor);
                        switch (eProcFor)
                        {
                            case POST_BACK_BY_HTML_PROCESS.AddNewWiFiSSID:
                                addNewWiFi(objPostbackData);
                                bindWiFiDtlsTable();
                                updHiddenFields.Update();
                                Utilities.runPostBackScript(@"enterpriseWiFi.setUI();", updPostbackByHtmlCntrl, "RebindListAfterAddingNewSetting");
                                break;
                            case POST_BACK_BY_HTML_PROCESS.UpdateWiFiSSID:
                                break;
                            case POST_BACK_BY_HTML_PROCESS.DeleteWiFiSSID:
                                deleteWiFi(objPostbackData);
                                bindWiFiDtlsTable();
                                updHiddenFields.Update();
                                Utilities.runPostBackScript(@"enterpriseWiFi.setUI();", updPostbackByHtmlCntrl, "RebindAfterDeletingWiFi");
                                break;
                        }
                    }
                }
            }
            catch (MficientException mfEx)
            {
                Utilities.showMessage(mfEx.Message, updPostbackByHtmlCntrl, DIALOG_TYPE.Error);
            }
            catch
            {
                Utilities.showMessage("Internal server error.", updPostbackByHtmlCntrl, DIALOG_TYPE.Error);
            }
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "setExpiryDateOfCookie();", true);
        }
        protected void btnUpdateName_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(hidDtlsForPostBackByHtmlCntrl.Value))
                {
                    PostbackByHtmlControlData objPostbackData =
                            Utilities.DeserialiseJson<PostbackByHtmlControlData>(hidDtlsForPostBackByHtmlCntrl.Value);
                    if (objPostbackData != null)
                    {
                        updateWiFi(objPostbackData);
                        bindWiFiDtlsTable();
                        updHiddenFields.Update();
                        Utilities.runPostBackScript(@"enterpriseWiFi.closeUpdateWiFiName();enterpriseWiFi.setUI();", updUpdateWiFiSSID, "RebindAfterUpdatingWiFi");
                    }
                }
            }
            catch (MficientException mfEx)
            {
                Utilities.showMessage(mfEx.Message, updPostbackByHtmlCntrl, DIALOG_TYPE.Error);
            }
            catch
            {
                Utilities.showMessage("Internal server error.", updPostbackByHtmlCntrl, DIALOG_TYPE.Error);
            }
        }
        void addNewWiFi(PostbackByHtmlControlData postBackData)
        {
            string strName = postBackData.data[0];
            if (String.IsNullOrEmpty(strName))
            {
                throw new MficientException("Please provide a name of the WiFi Network");
            }
            int iRecInserted = 0;
            EnterpriseWiFiProcess objEnterpriseWiFiProcess = new EnterpriseWiFiProcess(this.CompanyId);
            if (objEnterpriseWiFiProcess.doesWiFiNameExistsForCompany(strName))
            {
                throw new MficientException("A WiFi setting with name already exists");
            }
            else
            { 
                MFEEnterpriseWiFi objMfeEnterpriseWiFi = new MFEEnterpriseWiFi();
                objMfeEnterpriseWiFi.Id= Utilities.GetMd5Hash(this.CompanyId+DateTime.UtcNow.Ticks.ToString()+strName);
                objMfeEnterpriseWiFi.CompanyId = this.CompanyId;
                objMfeEnterpriseWiFi.Name = strName;
                objMfeEnterpriseWiFi.CreatedOn = DateTime.UtcNow.Ticks.ToString();
                iRecInserted = objEnterpriseWiFiProcess.InsertWiFiDtl(objMfeEnterpriseWiFi);
                if (objEnterpriseWiFiProcess.StatusCode != 0)
                {
                    throw new MficientException(objEnterpriseWiFiProcess.StatusDescription);
                }
                else if (iRecInserted == 0)
                {
                    throw new MficientException("Internal server error.");
                }
            }
        }
        void deleteWiFi(PostbackByHtmlControlData postBackData)
        {
            string strId = postBackData.data[0];
            int iRecUpdated = 0;
            EnterpriseWiFiProcess objEnterpriseWiFiProcess = new EnterpriseWiFiProcess(this.CompanyId);
            iRecUpdated = objEnterpriseWiFiProcess.DeleteWiFiDtlById(strId);
            if (objEnterpriseWiFiProcess.StatusCode != 0)
            {
                throw new MficientException(objEnterpriseWiFiProcess.StatusDescription);
            }
        }
        void updateWiFi(PostbackByHtmlControlData postBackData)
        {
            string strWiFiId = postBackData.data[0];
            string strWiFiName = postBackData.data[1];
            int iRecUpdated = 0;
            if (String.IsNullOrEmpty(strWiFiName))
            {
                throw new MficientException("Please provide a name of the WiFi Network");
            }
            EnterpriseWiFiProcess objEnterpriseWiFiProcess = new EnterpriseWiFiProcess(this.CompanyId);
            if (objEnterpriseWiFiProcess.doesWiFiNameExistsForCompany(strWiFiName, strWiFiId))
            {
                throw new MficientException("A WiFi setting with name already exists");
            }
            else 
            {
                iRecUpdated = objEnterpriseWiFiProcess.UpdateWiFiDtlById(strWiFiName, strWiFiId);
                if (objEnterpriseWiFiProcess.StatusCode != 0)
                {
                    throw new MficientException(objEnterpriseWiFiProcess.StatusDescription);
                }
            }
        }
   }
}
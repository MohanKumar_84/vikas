﻿<%@ Page Title="mFicient | My Plan" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="currentPlanDetails.aspx.cs" Inherits="mFicientCP.currentPlanDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <style type="text/css">
        .threeColumnDetail > div
        {
            clear: both;
        }
        .threeColumnDetail > div.spacer
        {
            margin: 0px 0px 15px 0px;
        }
        .threeColumnDetail > div label
        {
            float: left;
            width: 20%;
            color: #6F6F6F;
            clear: none;
            text-shadow: 0 -1px 0 white;
            font-size: 11px;
            font-style: normal;
            font-weight: 700;
            margin-left: 10px;
        }
        .threeColumnDetail.lblWidth40Prcnt > div label
        {
            width: 40%;
        }
        .threeColumnDetail > div label.forInputControl
        {
            position: relative;
            top: 8px;
        }
        .threeColumnDetail > div .labelColon
        {
            margin-right: 4px;
            float: left;
            width: auto;
        }
        .threeColumnDetail > div .labelColon.forInputControl
        {
            position: relative;
            top: 8px;
        }
        .threeColumnDetail > div .contentColumn
        {
            float: left;
            width: 50%;
            margin-left: 3px;
            word-wrap: break-word;
        }
    </style>
    <script type="text/javascript">
        function CheckDecimal(inputtxt) {
            var decimal = /^[0-9]+(\.[0-9]+)+$/;
            if (inputtxt.value.match(decimal)) {
                //alert('The number has a decimal part...');
                return true;
            }
            else {
                //alert('The number has no decimal part...');
                return false;
            }
        }
        function clearTextFields() {
            var txtAddUserCount = document.getElementById('<%=txtAddUserCount.ClientID %>');
            txtAddUserCount.value = "";
        }
        function confirmationForRemovingUser(sender) {
            clearTextFields();
            var returnOfConfirmation = confirm('No amount would be refunded.The price, for the no of users provided, will be considered only in next billing cycle.Would you like to continue?');
            if (returnOfConfirmation) {
                showModalPopUp('divRemoveUsersContainer', 'Remove Users', '300', false); return false;
            }
            else {
                return false;
            }
            return false;
        }

        CONTAINER_PANELS =
        {
            CurrentPlanDetails: 0,
            PurchasePlan: 1
        }
        function hideShowContainerPanels(ContainerPanelToShow) {
            var divCurrentPlanDetails = document.getElementById('<%=CurrentPlanDetailsDiv.ClientID %>');
            var divPurchasePlan = document.getElementById('<%= divPurchasePlan.ClientID%>');
            switch (ContainerPanelToShow) {
                case CONTAINER_PANELS.CurrentPlanDetails:
                    $(divCurrentPlanDetails).show();
                    $(divPurchasePlan).hide();
                    break;
                case CONTAINER_PANELS.PurchasePlan:
                    $(divCurrentPlanDetails).hide();
                    $(divPurchasePlan).show();
                    break;
            }
        }
        function shouldDdlPurchaseCurrencyPostBack() {
            var hidPlanCode = document.getElementById('<%= hfdPlanCode.ClientID%>');
            if (hidPlanCode && $(hidPlanCode).val() !== "") {
                return true;
            }
            else {
                return false;
            }
        }
        function defaultsOfPurchasePlanForm() {
            $('#' + '<%=hfdPlanCode.ClientID %>').val("");
            setTextAndCmdArgumentOfLnkPurchase();
            $('#' + '<%=lblPurchasePrice.ClientID %>').html("please select a plan first");
            $('#' + '<%=lblPurchaseCurrency.ClientID %>').html("");
            $('#' + '<%=lblPurchasePlanName.ClientID %>').html("");
            $('#' + '<%=txtPurchaseNoOfUsers.ClientID %>').val("");
            var ddlPurchaseMonths = $('#' + '<%=ddlPurchaseMonths.ClientID %>');
            $(ddlPurchaseMonths)
            .children('option:selected').
            removeAttr('selected');
            $($(ddlPurchaseMonths).children('option')[0]).attr('selected', 'selected');
            $(ddlPurchaseMonths).siblings('span').html($($(ddlPurchaseMonths).children('option')[0]).html());
            showHideLnkCalculateTotal('false');
        }

        function setTextAndCmdArgumentOfLnkPurchase() {
            var hidPlanCodeValue = $('#' + '<%=hfdPlanCode.ClientID %>').val("");
            var lnkPurchasePlanChange = $('#' + '<%=lnkPurchasePlnChange.ClientID %>');
            if ($(hidPlanCodeValue).val() === "") {
                $(lnkPurchasePlanChange).attr('CommandArgument', 'Select');
                $(lnkPurchasePlanChange).html("[select]");
            }
            else {
                $(lnkPurchasePlanChange).attr('CommandArgument', 'change');
                $(lnkPurchasePlanChange).html("[change]");
            }
        }
        function validateFormWhenPostBack(isPlanSelected, isFromLnkButton) {
            var strError = "";
            var ddlPurchaseMonths = $('#' + '<%=ddlPurchaseMonths.ClientID %>');
            var txtPurchaseNoOfUsers = $('#' + '<%=txtPurchaseNoOfUsers.ClientID %>');
            var hfdPlanCode = $('#' + '<%=hfdPlanCode.ClientID %>');
            if (ddlPurchaseMonths) {
                if ($(ddlPurchaseMonths).val() === "-1") {
                    strError += "Please select a month.<br/>";
                }
            }
            if (txtPurchaseNoOfUsers) {
                if ($(txtPurchaseNoOfUsers).val() === "") {
                    strError += "Please enter valid no of users.<br/>";
                }
            }
            if (isPlanSelected) {
                if (hfdPlanCode) {
                    if (isPlanSelected.toString().toLowerCase() == "true") {
                        if ($(hfdPlanCode).val() == "") {
                            strError += "Please select a plan.<br/>";
                        }
                    }
                }
            }
            if (strError === "") {
                if (isFromLnkButton) {
                    if (isFromLnkButton.toString().toLowerCase() == "true") {
                        isCookieCleanUpRequired(false);
                    }
                }
                return true;
            }
            else {
                $.wl_Alert(strError, 'info', '#divPurchaseDetails');
                return false;
            }
        }
        function showHideLnkCalculateTotal(show) {
            var lnkCalculateAmnt = $('#' + '<%=lnkCalculateAmnt.ClientID %>');
            if (lnkCalculateAmnt) {
                if (show && show.toString().toLowerCase() == "true") {
                    $(lnkCalculateAmnt).show();
                }
                else if (show && show.toString().toLowerCase() == "false") {
                    $(lnkCalculateAmnt).hide();
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="updContainer" runat="server">
        <ContentTemplate>
            <div class="widget g7 " id="CurrentPlanDetailsDiv" runat="server" style="float: left;
                padding: 0px;" visible="true">
                <h3 class="handle">
                    Current Plan Details
                </h3>
                <div class="customWidgetDirectDiv" style="padding-top: 10px">
                    <div id="divAlert">
                    </div>
                    <div>
                        <div style="float: left; width: 150px;">
                            <span class="BasicDetailHead">Plan</span>:<span class="BasicDetail"><asp:Label ID="lblCurrentPlanName"
                                runat="server"></asp:Label>
                                <asp:Label ID="lblPlanTrialText" runat="server" Text="(Trial)" Style="margin-left: 3px;"></asp:Label></span>
                        </div>
                        <asp:Panel ID="pnlNextMonthPlan" runat="server" Style="float: left; margin-left: 10px;">
                            <span class="BasicDetailHead">Next Month Plan</span>:<span class="BasicDetail"><asp:Label
                                ID="lblNextMnthPlanName" runat="server"></asp:Label>&nbsp;(
                                <asp:LinkButton ID="lnkChangeNxtMnthPlan" runat="server" OnClientClick="isCookieCleanUpRequired('false');"
                                    OnClick="lnkChangeNxtMnthPlan_Click" Text="change"></asp:LinkButton>&nbsp;)</span>
                        </asp:Panel>
                        <div class="clear">
                        </div>
                        <br />
                        <div style="float: left; width: 150px;">
                            <span class="BasicDetailHead">Max Workflow</span>:<span class="BasicDetail"><asp:Label
                                ID="lblWorkflow" runat="server"></asp:Label></span>
                        </div>
                        <asp:Panel ID="pnlWorkFlowUsed" runat="server" Style="float: left;">
                            <span class="BasicDetailHead" style="margin-left: 10px">Workflow Used</span>:<span
                                class="BasicDetail"><asp:Label ID="lblWorkflowUsed" runat="server"></asp:Label></span>
                        </asp:Panel>
                        <div class="clear">
                        </div>
                        <br />
                        <asp:Panel ID="pnlUserDetailPurchased" runat="server">
                            <div style="float: left; width: 100px;">
                                <asp:Label ID="labelMinUsers" runat="server" Text="Min Users :" CssClass="BasicDetailHead"></asp:Label><span
                                    class="BasicDetail"><asp:Label ID="lblMinUsers" runat="server"></asp:Label></span>
                            </div>
                            <asp:Panel ID="pnlMaxUsers" runat="server" Style="float: left; margin-left: 61px;">
                                <span class="BasicDetailHead">Max Users</span>:<span class="BasicDetail"><asp:Label
                                    ID="lblMaxUsers" runat="server"></asp:Label></span>
                            </asp:Panel>
                            <asp:Panel ID="pnlCurrentUsers" runat="server" Style="float: left;">
                                <span class="BasicDetailHead" style="margin-left: 40px">Current Users</span>:<span
                                    class="BasicDetail"><asp:Label ID="lblCurrentUsersCount" runat="server"></asp:Label></span>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel ID="pnlUserDetailTrial" runat="server">
                            <div style="float: left; width: 150px;">
                                <asp:Label ID="lblTrialMaxUsers" runat="server" Text="Max Users :" CssClass="BasicDetailHead"></asp:Label><span
                                    class="BasicDetail"><asp:Label ID="lblTrialMaxUsersCount" runat="server"></asp:Label></span>
                            </div>
                            <asp:Panel ID="Panel3" runat="server" Style="float: left;">
                                <span class="BasicDetailHead" style="margin-left: 10px">Current Users</span>:<span
                                    class="BasicDetail"><asp:Label ID="lblTrialCurrentUsers" runat="server"></asp:Label></span>
                            </asp:Panel>
                        </asp:Panel>
                        <div class="clear">
                        </div>
                        <br />
                        <span class="BasicDetailHead">Charge Per User/Per Month</span>:<span class="BasicDetail">
                            <asp:Label ID="lblChargePerMonth" runat="server"></asp:Label>&nbsp;
                            <asp:Label ID="lblChargeType" runat="server"></asp:Label>
                        </span>
                        <br />
                        <br />
                        <span class="BasicDetailHead">Balance Amount</span>:<span class="BasicDetail"><asp:Label
                            ID="lblBalanceAmt" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblBalanceAmntCurrType"
                                runat="server"></asp:Label></span><br />
                        <br />
                        <div class="BasicDetail" style="margin-bottom:10px;">
                            <asp:Button ID="btnNewPlan" runat="server" Text="Buy Now" OnClick="btnNewPlan_Click"
                                OnClientClick="defaultsOfPurchasePlanForm();hideShowContainerPanels(CONTAINER_PANELS.PurchasePlan);return false;" />
                            <asp:Button ID="btnAddUser" runat="server" Text="Add User" OnClientClick="clearTextFields();showModalPopUp('divAddUsersContainer','Add Users','300',false);return false;" />&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnRemoveUser" runat="server" Text="Remove User" OnClientClick="return confirmationForRemovingUser();" />&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget" id="divPurchasePlan" runat="server" style="float: left; width: 100%">
                <h3 class="handle">
                    Purchase Plan <span style="float: right; position: relative; top: 10px;">
                        <asp:LinkButton ID="LinkButton6" runat="server" Text="back" OnClick="lnkBack_Click"
                            OnClientClick="defaultsOfPurchasePlanForm();hideShowContainerPanels(CONTAINER_PANELS.CurrentPlanDetails);return false;">
                        </asp:LinkButton></span>
                </h3>
                <div id="divPurchaseDetails" class="customWidgetDirectDiv  threeColumnDetail">
                    <div class="spacer" style="margin-top: 5px;">
                    </div>
                    <div>
                        <label class="forInputControl">
                            Currency Type</label>
                        <span class="labelColon forInputControl">:</span>
                        <div class="contentColumn">
                            <asp:DropDownList runat="server" ID="ddlPurchaseCurrencyType" CssClass="Fld" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlPurchaseCurrencyType_SelectedIndexChanged" AppendDataBoundItems="true">
                                <asp:ListItem Text="USD" Value="USD" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="INR" Value="INR"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="spacer">
                    </div>
                    <div>
                        <label class="forInputControl">
                            Months</label>
                        <span class="labelColon forInputControl">:</span>
                        <div class="contentColumn">
                            <asp:DropDownList runat="server" ID="ddlPurchaseMonths" CssClass="Fld">
                                <asp:ListItem Value="-1" Text="Select" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                <asp:ListItem Value="12" Text="12"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="spacer">
                    </div>
                    <div>
                        <label class="forInputControl" style="top: 4px;">
                            No. of Users</label>
                        <span class="labelColon forInputControl" style="top: 4px;">:</span>
                        <div class="contentColumn" style="padding-left: 3px;">
                            <asp:TextBox ID="txtPurchaseNoOfUsers" runat="server" Width="80px"></asp:TextBox><asp:LinkButton
                                ID="lnkCalculateAmnt" runat="server" Text="[calculate total]" OnClientClick="return validateFormWhenPostBack('true','true');"
                                OnClick="lnkCalculateAmnt_Click"></asp:LinkButton>
                        </div>
                    </div>
                    <div class="spacer">
                    </div>
                    <div>
                        <label>
                            Plan Name</label>
                        <span class="labelColon">:</span>
                        <div class="contentColumn" style="padding-left: 3px;">
                            <asp:Label ID="lblPurchasePlanName" runat="server"></asp:Label>&nbsp;
                            <asp:LinkButton ID="lnkPurchasePlnChange" runat="server" Text="[change]" OnClick="lnkPurchasePlnChange_Click"
                                OnClientClick="return validateFormWhenPostBack('false','true');">
                            </asp:LinkButton>
                        </div>
                    </div>
                    <div class="spacer">
                    </div>
                    <div>
                        <label>
                            Charge per user per month</label>
                        <span class="labelColon">:</span>
                        <div class="contentColumn" style="padding-left: 3px;">
                            <asp:Label ID="lblPurchasePrice" runat="server"></asp:Label>
                            <asp:Label ID="lblPurchaseCurrency" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="spacer">
                    </div>
                    <div style="padding-left: 10px;">
                        <asp:Button ID="btnPurchasePlanSave" runat="server" Text="Save" Style="margin: 0px 0px 5px 2px;"
                            OnClick="btnPurchasePlanSave_Click" OnClientClick="return validateFormWhenPostBack('true','false');" />
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlCurrentPlanOrder" runat="server" Style="margin-top: 5px;" Visible="false">
                <div class="widget g7 " id="divCurrentPlanOrder" runat="server" style="float: left;
                    padding: 0px;">
                    <h3 class="handle">
                        Current Plan Order
                    </h3>
                    <div id="divPurchaseOrderContainer" class="customWidgetDirectDiv  threeColumnDetail lblWidth40Prcnt">
                        <div id="divCurrPlanOrderError">
                        </div>
                        <div class="spacer" style="margin-top: 5px;">
                        </div>
                        <div>
                            <label>
                                Plan Name</label>
                            <span class="labelColon">:</span>
                            <div class="contentColumn">
                                <asp:Label ID="lblCurrPlnOdrPlanName" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="spacer">
                        </div>
                        <div>
                            <label>
                                Request</label>
                            <span class="labelColon">:</span>
                            <div class="contentColumn">
                                <asp:Label ID="lblCurrPlnOdrRequestType" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="spacer">
                        </div>
                        <div>
                            <label>
                                Charge per user per month</label>
                            <span class="labelColon">:</span>
                            <div class="contentColumn">
                                <asp:Label ID="lblCurrPlnOdrCharge" runat="server"></asp:Label>
                                <asp:Label ID="lblCurrPlnOdrCurrType" runat="server" Style="margin-left: 3px;"></asp:Label>
                            </div>
                        </div>
                        <div class="spacer">
                        </div>
                        <div>
                            <label>
                                No. of Users</label>
                            <span class="labelColon">:</span>
                            <div class="contentColumn">
                                <asp:Label ID="lblCurrPlnOdrNoOfUsers" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="spacer">
                        </div>
                        <div>
                            <label>
                                Months</label>
                            <span class="labelColon">:</span>
                            <div class="contentColumn">
                                <asp:Label ID="lblCurrPlnOdrMonths" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="spacer">
                        </div>
                        <div>
                            <label>
                                Requested Date</label>
                            <span class="labelColon">:</span>
                            <div class="contentColumn">
                                <asp:Label ID="lblCurrPlnOdrRequestedDate" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="spacer">
                        </div>
                        <div>
                            <label>
                                Status</label>
                            <span class="labelColon">:</span>
                            <div class="contentColumn">
                                <asp:Label ID="lblCurrPlnOdrStatus" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="spacer">
                        </div>
                        <div id="div2" style="padding-left: 8px;">
                            <asp:Button ID="btnCurrPlnOdrCancel" runat="server" Text="Cancel Order" OnClick="btnCurrPlnOdrCancel_Click"
                                OnClientClick="return confirm('Are you sure, you want to cancel the order');"
                                Style="margin-bottom: 6px;" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div id="divHiddenFields">
                <asp:HiddenField ID="hfdPlanCode" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hidPriceAmountPayable" runat="server" />
                <asp:HiddenField ID="hidChargePerMnthPerUsr" runat="server" />
                <asp:HiddenField ID="hidPlanChangeType" runat="server" />
                <asp:HiddenField ID="hidUpgrdDowngrdPlanCode" runat="server" />
                <asp:HiddenField ID="hidNewPurchasePlanCode" runat="server" />
                <asp:HiddenField ID="hidPlanRadButtonFormed" runat="server" />
                <asp:HiddenField ID="hidIsTrial" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <div id="divModalContainer" style="display: none">
        <div id="divPlanDetailsRpt" style="display: block">
            <asp:UpdatePanel ID="updModalContainer" runat="server">
                <ContentTemplate>
                    <div id="modalPopUpContent" style="overflow: hidden">
                        <div id="divRepeater" style="margin-top: 5px;">
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server" ScrollBars="Vertical"
                                Height="300">
                                <asp:Repeater ID="rptPlanDetails" runat="server" OnItemCommand="rptPlanDetails_ItemCommand"
                                    OnItemDataBound="rptPlanDetails_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th style="display: none;">
                                                        PLAN CODE
                                                    </th>
                                                    <th>
                                                        Name
                                                    </th>
                                                    <th>
                                                        Workflows
                                                    </th>
                                                    <th>
                                                        Push Message *
                                                    </th>
                                                    <th>
                                                        Price
                                                    </th>
                                                    <th>
                                                        Currency
                                                    </th>
                                                    <th>
                                                        Min Users
                                                    </th>
                                                    <th>
                                                        Months
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td style="display: none;">
                                                    <asp:Label ID="lblPlanCode" runat="server" Text='<%# Eval("PLAN_CODE") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPlanName" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbWorkFlow" runat="server" Text='<%# Eval("MAX_WORKFLOW") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPushMessagePerDay" runat="server" Text='<%# Eval("PUSHMESSAGE_PERDAY") %>'></asp:Label>&nbsp;|&nbsp;
                                                    <asp:Label ID="lblPushMessagePerMonth" runat="server" Text='<%# Eval("PUSHMESSAGE_PERMONTH") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("PRICE") %>'></asp:Label>
                                                </td>
                                                <td class="radioColumn">
                                                    <asp:Label ID="lblCurrencyType" runat="server" Style="margin-left: 3px;"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblMinUsers" runat="server" Text='<%# Eval("MIN_USERS") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblMonths" runat="server" Text='<%# Eval("MONTHS") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkBuy" runat="server" Text="Select" CssClass="repeaterLink"
                                                        CommandName="Select" OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                <td style="display: none;">
                                                    <asp:Label ID="lblPlanCode" runat="server" Text='<%# Eval("PLAN_CODE") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPlanName" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbWorkFlow" runat="server" Text='<%# Eval("MAX_WORKFLOW") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPushMessagePerDay" runat="server" Text='<%# Eval("PUSHMESSAGE_PERDAY") %>'></asp:Label>&nbsp;|&nbsp;
                                                    <asp:Label ID="lblPushMessagePerMonth" runat="server" Text='<%# Eval("PUSHMESSAGE_PERMONTH") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("PRICE") %>'></asp:Label>
                                                </td>
                                                <td class="radioColumn">
                                                    <asp:Label ID="lblCurrencyType" runat="server" Style="margin-left: 3px;"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblMinUsers" runat="server" Text='<%# Eval("MIN_USERS") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblMonths" runat="server" Text='<%# Eval("MONTHS") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkBuy" runat="server" Text="Select" CssClass="repeaterLink"
                                                        CommandName="Select" OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                        <div class="RptFooterMessage">
                                            * Per day | Per month</div>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                            <div id="divPlanDtlsAction" class="modalPopUpAction" style="margin-top: 3px;">
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click"
                                    OnClientClick="$('#divModalContainer').dialog('close');return false;" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="divAddUsersContainer" style="display: none; min-height: 0px;">
        <asp:UpdatePanel ID="updAddUsers" runat="server">
            <ContentTemplate>
                <div id="divAddUsers" style="width: 100%; margin: 5px 0px 5px 0px;">
                    <div style="margin-left: 30px;">
                        <div style="float: left; margin-right: 3px; position: relative; top: 5px;">
                            <asp:Label ID="lblNoOfUserCount" runat="server" Text="No of Users :"></asp:Label></div>
                        <div style="float: left; margin-right: 3px;">
                            <asp:TextBox ID="txtAddUserCount" runat="server" Width="50"></asp:TextBox>
                            &nbsp;(&nbsp;<asp:LinkButton ID="lnkAddUserCheckBalance" runat="server" Text="check balance"
                                OnClick="lnkAddUserCheckBalance_Click" OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>&nbsp;)</div>
                        <div class="clear">
                        </div>
                    </div>
                    <div id="divAction" style="width: 100%; margin-top: 30px; text-align: center;">
                        <asp:Button ID="btnAddUsersSave" runat="server" Text="save" CssClass="aspButton"
                            OnClick="btnAddUsersSave_Click" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divRemoveUsersContainer" style="display: none; min-height: 0px;">
        <asp:UpdatePanel ID="updRemoveUsers" runat="server">
            <ContentTemplate>
                <div id="div3" style="width: 100%; margin: 5px 0px 5px 0px;">
                    <div style="margin-left: 30px;">
                        <div style="float: left; margin-right: 3px; position: relative; top: 5px;">
                            <asp:Label ID="lblRemoveNoOfUserCount" runat="server" Text="No of Users :"></asp:Label></div>
                        <div style="float: left; margin-right: 3px;">
                            <asp:TextBox ID="txtRemoveUserCount" runat="server" Width="50"></asp:TextBox>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div id="div4" style="width: 100%; margin-top: 30px; text-align: center;">
                        <asp:Button ID="btnRemoveUsersSave" runat="server" Text="save" CssClass="aspButton"
                            OnClick="btnRemoveUsersSave_Click" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divPostBackConfirm" style="display: none;">
        <asp:UpdatePanel ID="updConfirmation" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="modalPopUpConfirm">
                    <div>
                        <asp:Label ID="lblConfirmationMessage" runat="server" Text="Do you really like to confirm this"></asp:Label></div>
                    <div style="margin-top: 15px; margin-bottom: 5px; float: right;">
                        <div style="float: right;">
                            <span class="ui-button-text">
                                <asp:Button ID="btnConfirmYes" runat="server" Text='Yes' CssClass="aspButton" OnClick="btnConfirmYes_Click" /></span>&nbsp;
                            <span class="ui-button-text">
                                <asp:Button ID="btnConfirmNo" runat="server" Text='No' CssClass="aspButton" OnClick="btnConfirmNo_Click"
                                    OnClientClick="$('#divPostBackConfirm').dialog('close');removeDialogImage('divPostBackConfirm');return false;" /></span>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest(sender, args) {
            $("input").uniform();
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>

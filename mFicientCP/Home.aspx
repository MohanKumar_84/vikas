﻿<%@ Page Title="mFicient | Control Panel" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="Home.aspx.cs" Inherits="mFicientCP.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
<script type="text/javascript">
    function setHomePageDeviceLinkClicked(senderValue) {
        var hidHomePgDeviceLinkClicked = document.getElementById('<%=hidHomePgDeviceLinkClicked.ClientID %>');
        hidHomePgDeviceLinkClicked.value = senderValue;
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="divMain" runat="server">
        <div id="divImportantAnnouncements" runat="server">
            <div class="alert note">
                <asp:Literal ID="ltAnnouncement" runat="server" Text="Announcement"></asp:Literal>
            </div>
        </div>
        <div class="g8">
            <div class="widget" id="ajax_widget" style="float: left; width: 100%">
                <h3 class="handle">
                    
                    Enterprise <span style="margin-left: 8px;">(&nbsp;
                        <asp:LinkButton ID="lbCompany" runat="server" PostBackUrl="~/companyDetails.aspx" OnClientClick="isCookieCleanUpRequired('false');showWaitModal();"> details </asp:LinkButton>&nbsp;)</span>
                </h3>
                <div>
                    <span class="BasicDetailHead">Enterprise Name</span>:<span class="BasicDetail"><asp:Literal
                        ID="ltCompany" runat="server"></asp:Literal></span>
                    <br />
                    <br />
                    <span class="BasicDetailHead">Enterprise ID</span>:<span class="BasicDetail"><asp:Literal
                        ID="ltcode" runat="server"></asp:Literal></span>
                    <br />
                    <br />
                    
                    <span class="BasicDetailHead">Administrator </span>:<span class="BasicDetail">
                        <asp:Literal ID="ltAdmin" runat="server"></asp:Literal></span><br />
                    <br />
                </div>
            </div>
            <div style="clear: both; height: 0px; margin: 0px; padding: 0px;">
            </div>
            <div class="widget" id="Div1" style="float: left; width: 100%">
                <h3 class="handle">
                    Current Plan <span style="margin-left: 8px;">(&nbsp;
                        <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/currentPlanDetails.aspx" OnClientClick="isCookieCleanUpRequired('false');showWaitModal();">details</asp:LinkButton>
                        &nbsp;)</span></h3>
                <div>
                    <span class="BasicDetailHead">Plan</span>:<span class="BasicDetail"><asp:Literal
                        ID="ltPlanCode" runat="server">hh</asp:Literal></span><br />
                    <br />
                    <span class="BasicDetailHead">Account Valid Till</span>:<span class="BasicDetail"><asp:Literal
                        ID="ltpVali" runat="server">hh</asp:Literal></span><br />
                    <br />
                    <span class="BasicDetailHead">MAX Mobiles</span>:<span class="BasicDetail"><asp:Literal
                        ID="ltpMobile" runat="server">hh</asp:Literal></span><br />
                    <br />
                    <span class="BasicDetailHead">Work Flows</span>:<span class="BasicDetail"><asp:Literal
                        ID="ltpWork" runat="server">hh</asp:Literal></span><br />
                    <br />
                </div>
            </div>
        </div>
        <div class="g4">
            <div class="widget number-widget" id="number_widget">
                <h3 class="handle">
                   Overview</h3>
                <div>
                    <ul>
                        <li>
                            <asp:LinkButton ID="lbCatCnt" runat="server" PostBackUrl="~/adminAppMenu.aspx" OnClientClick="isCookieCleanUpRequired('false');showWaitModal();">
                                <span>
                                    <asp:Literal ID="ltrlDocCategoryCount"  runat="server" Text="0"></asp:Literal></span>Menu
                                Categories</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbsubAdmin" runat="server" PostBackUrl="~/addSubadminDetails.aspx" OnClientClick="isCookieCleanUpRequired('false');showWaitModal();">
                                <span>
                                    <asp:Literal ID="ltrlSubAdmin" runat="server" Text="0"></asp:Literal></span>Sub
                                Admins</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbuser" runat="server" PostBackUrl="~/MobileUserList.aspx" OnClientClick="isCookieCleanUpRequired('false');showWaitModal();">
                                <span>
                                    <asp:Literal ID="ltrlUser" runat="server" Text="0"></asp:Literal></span>Users
                            </asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lnkRegisteredDevices" runat="server" PostBackUrl="~/registeredDeviceList.aspx" OnClientClick="isCookieCleanUpRequired('false');showWaitModal();">
                                <span>
                                    <asp:Literal ID="ltrlRegisteredDevices" runat="server" Text="0"></asp:Literal></span>Registered
                                Devices</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lbForm" runat="server" PostBackUrl="~/manageApps.aspx"  OnClientClick="isCookieCleanUpRequired('false');showWaitModal();">
                                <span>
                                    <asp:Literal ID="ltrlFormReportCount" runat="server" Text="0"></asp:Literal></span>Apps</asp:LinkButton></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="divSubadmin" runat="server">
        <div class="g8">
            <div class="widget" id="Div3" style="float: left; width: 100%">
                <h3 class="handle">
                    Overview
                </h3>
                <div>
                    <span class="BasicDetailHead">Enterprise Name </span>:<span class="BasicDetail"><asp:Literal
                        ID="ltComs" runat="server"></asp:Literal></span>
                    <br />
                    <br />
                    <span class="BasicDetailHead">Enterprise ID</span>:<span class="BasicDetail"><asp:Literal
                        ID="ltComcds" runat="server"></asp:Literal></span>
                    <br />
                    <br />
                    <span class="BasicDetailHead">Administrator</span>:<span class="BasicDetail">
                        <asp:Literal ID="ltAdmins" runat="server"></asp:Literal></span>
                    <br />
                    <br />
                    <span class="BasicDetailHead">Email</span>:<span class="BasicDetail">
                        <asp:Literal ID="ltEmail" runat="server"></asp:Literal></span><br />
                    <br />
                </div>
            </div>
        </div>
        <div class="g4">
            <div class="widget number-widget" id="Div2">
                <h3 class="handle">
                   Mobile Devices</h3>
                <div>
                    <ul>
                        <li>
                            <asp:LinkButton ID="lnkAll" runat="server" PostBackUrl="~/registerDevice.aspx" OnClientClick="isCookieCleanUpRequired('false');setHomePageDeviceLinkClicked(1);showWaitModal();">
                                <span>
                                    <asp:Literal ID="ltall" runat="server" Text="0"></asp:Literal></span>Allowed
                                Devices</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lnkalap" runat="server" PostBackUrl="~/registerDevice.aspx" OnClientClick="isCookieCleanUpRequired('false');setHomePageDeviceLinkClicked(2);showWaitModal();">
                                <span>
                                    <asp:Literal ID="ltalap" runat="server" Text="0"></asp:Literal></span>Total Approved Devices</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lnkApp" runat="server" PostBackUrl="~/registerDevice.aspx" OnClientClick="isCookieCleanUpRequired('false');setHomePageDeviceLinkClicked(3);showWaitModal();">
                                <span>
                                    <asp:Literal ID="ltapp" runat="server" Text="0"></asp:Literal></span>Devices Approved by Me
 
                            </asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="lnkpnd" runat="server" PostBackUrl="~/registerDevice.aspx" OnClientClick="isCookieCleanUpRequired('false');setHomePageDeviceLinkClicked(4);showWaitModal();">
                                <span>
                                    <asp:Literal ID="ltpnd" runat="server" Text="0"></asp:Literal></span>Pending
                                Devices</asp:LinkButton></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div >
        <asp:HiddenField ID="hidHomePgDeviceLinkClicked" runat="server" />
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
</asp:Content>

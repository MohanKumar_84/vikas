﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Threading;
using System.Net;
using System.Data.SqlClient;
namespace mFicientCP
{
    public partial class addUserDetails : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        string hfsPart5 = string.Empty;
        string hfsPart6 = string.Empty;
        enum PanelToShowHide
        {
            Repeater = 0,
            Form = 1
        }
        enum REPEATER_NAME
        {
            UserDetail = 1,
        }
        enum USER_DETAIL_COLUMN_NAME
        {
            Name = 1,
            Email = 2
        }

        #region Events

        TimeZoneInfo tzi;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            Literal ltFullNAME = (Literal)this.Master.Master.FindControl("ltFullNAME");
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;
            int lengthA = hfsParts.Length;
            if (lengthA > 4)
            {
                hfsPart5 = hfsParts[4];
                hfsPart6 = hfsParts[5];
                context.Items["hfs5"] = hfsPart5;
                context.Items["hfs6"] = hfsPart6;
                ltFullNAME.Text = hfsPart5 + " (" + hfsPart6 + ")";
            }
            tzi = CompanyTimezone.getTimezoneInfo(hfsPart4);
            if (!Page.IsPostBack)
            {
                BindReapeater(null);
                bindGroupChkLst();
                EnterpriseDefinistion(hfsPart4);
                #region setting the sort of repeater first time
                hidSortingDetail.Value = "1,1,0";
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignOnPageLoadAndSortOrder", false,
                    "changeImageOfSortedCol();clearSortingDtlHiddenField();setWidthOfDropDown();doProcessForFormHelp();CustomProperty();CredentialProperty();");
                #endregion
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    BindReapeater(null);
                    bindGroupChkLst();
                    EnterpriseDefinistion(hfsPart4);
                    enableDisablePanelsForAddAndEdit("", false);
                    #region setting the sort of repeater first time
                    hidSortingDetail.Value = "1,1,0";
                    #endregion
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page,
                        "changeImageOfSortedCol();clearSortingDtlHiddenField();hideShowPanelForCancel(Panels.Repeater);setWidthOfDropDown();doProcessForFormHelp();CustomProperty();CredentialProperty();");
                    return;
                }
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback2", true,
                    "setHeightOfUserTypeAndDomainInEditMode();setWidthOfDropDown();");
                fillPasswordOnPostBack();
            }
            ucUserDetails.OnEditClick += new UserControls.UserDetails.ActionClick(ucUserDetails_Edit);
            ucUserDetails.OnBlockClick += new UserControls.UserDetails.ActionClick(ucUserDetails_Block);
            ucUserDetails.OnUnBlockClick += new UserControls.UserDetails.ActionClick(ucUserDetails_UnBlock);
        }
        private void EnterpriseDefinistion(string CompanyId)
        {
            GetEnterpriseAdditionalDefinition objGetWsCommand = new GetEnterpriseAdditionalDefinition(CompanyId);

            string str = objGetWsCommand.GetUserAdditionalProperties();
            if (objGetWsCommand.StatusCode == 0)
            {
                if (objGetWsCommand.ResultTable != null)
                {
                    //DataTable dt = objGetWsCommand.ResultTable.Tables[0];
                    if (objGetWsCommand.ResultTable.Rows.Count > 0)
                    {
                        hidData.Value = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["ADDITIONAL_PARAMETERS_META"]);
                        hidAuthenticationData.Value = Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["AUTHENTICATION_META"]);
                    }
                }
                else
                {
                    hidData.Value = "";
                    hidAuthenticationData.Value = "";
                }
            }
        }
        void ucUserDetails_Edit(object sender, EventArgs e)
        {
            //command argument value userId,userName
            processEdit(((LinkButton)sender).CommandArgument.Split(',')[1]);
            Utilities.closeModalPopUp("divUserDtlsModal", upUserDtlsModal);
        }
        void ucUserDetails_Block(object sender, EventArgs e)
        {
            //command argument value userId,userName
            processBlockUnblock(((LinkButton)sender).CommandArgument.Split(',')[0], "Block", upUserDtlsModal);
            Utilities.closeModalPopUp("divUserDtlsModal", upUserDtlsModal);
            enableDisablePanelsForAddAndEdit("", false);
        }
        void ucUserDetails_UnBlock(object sender, EventArgs e)
        {
            //command argument value userId,userName
            processBlockUnblock(((LinkButton)sender).CommandArgument.Split(',')[0], "UnBlock", upUserDtlsModal);
            Utilities.closeModalPopUp("divUserDtlsModal", upUserDtlsModal);
            enableDisablePanelsForAddAndEdit("", false);
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string strStatusDescription;
            //hidScrollPositionHelpForError.Value = String.Empty;


            if (hidDetailsFormMode.Value.ToLower() == "edit")
            {
                //Edit
                GetUserDetail getuserdetail = new GetUserDetail(false, false, strUserId, "", txtUserName.Text, hfsPart4);
                getuserdetail.Process();
                DataTable dt = getuserdetail.ResultTable;
                if (dt.Rows.Count > 0)
                {
                    UpdateUserDetails(hfsPart4, strUserId, Convert.ToString(dt.Rows[0]["USER_ID"]), out strStatusDescription);
                    if (String.IsNullOrEmpty(strStatusDescription))
                    {
                        clearFilterTextBox();
                        bindUserDetailRptAfterPostBack(updRepeater, String.Empty);
                    }
                    else
                    {
                        enableDisablePanelsForAddAndEdit("edit", true);
                    }
                    ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                }
            }
            else
            {
                //new user
                SaveUserDetails(hfsPart3, strUserId, hfsPart4, out strStatusDescription);
                if (String.IsNullOrEmpty(strStatusDescription))
                {
                    clearFilterTextBox();
                    bindUserDetailRptAfterPostBack(updRepeater, String.Empty);
                }
                else
                {
                    enableDisablePanelsForAddAndEdit("Add", true);
                }
                ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearControls();
            enableDisablePanelsForAddAndEdit("", false);

        }
        #endregion
        #region Private methods
        void bindUserTypeDropDown(bool allowActiveDirUser)
        {
            ddlUserType.Items.Clear();
            if (allowActiveDirUser)
            {
                ddlUserType.Items.Add(new ListItem("Cloud Authentication", "0"));
                ddlUserType.Items.Add(new ListItem("Active Directory Authentication", "1"));
            }
            else
            {
                ddlUserType.Items.Add(new ListItem("Cloud Authentication", "0"));
            }
        }
        protected void BindReapeater(UpdatePanel updPanel)
        {
            GetUserDetail getuserdetail = new GetUserDetail(true, false, strUserId, "", "", hfsPart4);
            getuserdetail.Process();
            if (getuserdetail.StatusCode == 0)
            {
                List<List<string>> users = new List<List<string>>();
                foreach (DataRow dr in getuserdetail.ResultTable.Rows)
                {
                    List<string> user = new List<string>();
                    //lblNameAndUserName.Text = (string)DataBinder.Eval(e.Item.DataItem, "FIRST_NAME") + " " + (string)DataBinder.Eval(e.Item.DataItem, "LAST_NAME") + " ( " + (string)DataBinder.Eval(e.Item.DataItem, "USER_NAME") + " ) ";
                    user.Add(dr["FIRST_NAME"].ToString() + " " + dr["LAST_NAME"].ToString() + " (" + dr["USER_NAME"].ToString() + ")");
                    user.Add(dr["USER_NAME"].ToString());
                    users.Add(user);
                }
                hidusers.Value = Utilities.SerializeJson<List<List<string>>>(users);
                bindUserDetailRpt(getuserdetail, updPanel);
            }
            else
            {
                if (updPanel != null)
                {
                    Utilities.showMessage("Could not get the user details.Please try again.",
                        updPanel,
                        DIALOG_TYPE.Error);
                }
                else
                {
                    Utilities.showMessageUsingPageStartUp(
                        this.Page, "ErrorMsgUserNotFound",
                        "Could not get the user details.Please try again.",
                        DIALOG_TYPE.Error);
                }
            }
        }

        void bindUserDetailRpt(GetUserDetail userDtl, UpdatePanel updPanel)
        {
            DataTable dtblUserDtl = userDtl.ResultTable;
            if (dtblUserDtl != null)
            {
                if (dtblUserDtl.Rows.Count > 0)
                {
                    if (chkShowBlockedUser.Checked)
                    {
                        DataTable dtBlockedUsers = getBlockedOrUnBlockedUsers(dtblUserDtl, true);
                        if (dtBlockedUsers != null && dtBlockedUsers.Rows.Count > 0)
                        {
                            dtblUserDtl = dtBlockedUsers;
                        }
                        else
                        {
                            dtblUserDtl = getBlockedOrUnBlockedUsers(dtblUserDtl, false);
                            //show message no blokced users
                            if (updPanel != null)
                            {
                                Utilities.showAlert("There are no blocked users", "divInformation", updPanel);
                            }
                            else
                            {
                                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, @"$.wl_Alert(" + "'" + "There are no blocked users" + "'" + ",'info','#" + "divInformation" + "'" + ");", true);
                            }
                            chkShowBlockedUser.Checked = false;
                        }
                    }
                    else
                    {
                        DataTable dtUnBlockedUsers = getBlockedOrUnBlockedUsers(dtblUserDtl, false);
                        if (dtUnBlockedUsers != null && dtUnBlockedUsers.Rows.Count > 0)
                        {
                            dtblUserDtl = dtUnBlockedUsers;
                        }
                        else
                        {
                            dtblUserDtl = getBlockedOrUnBlockedUsers(dtblUserDtl, true);
                            if (updPanel != null)
                            {
                                //show message no unblocked uses
                                Utilities.showAlert("There are no unblocked users", "divInformation", updPanel);
                            }
                            else
                            {
                                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, @"$.wl_Alert(" + "'" + "There are no blocked users" + "'" + ",'info','#" + "divInformation" + "'" + ");", true);
                            }
                            chkShowBlockedUser.Checked = true;
                        }
                    }
                    rptUserDetails.DataSource = dtblUserDtl;
                    rptUserDetails.DataBind();
                    showRptHdrInfoAndEnableDisableAdd("User List", true);
                    if (userDtl.IsSelectAll && (userDtl.UserCount == userDtl.MaxUsers))
                    {
                        lnkAddNewMobileUser.Visible = false;
                        if (Page.IsPostBack)
                        {
                            if (updPanel != null)
                                Utilities.showAlert("You have reached the maximum user limit.No more users could be added until you delete some users", "divInformation", updPanel);
                            else
                                Utilities.showAlert("You have reached the maximum user limit.No more users could be added until you delete some users", "divInformation",
                                    this.Page, "InfoOnPostBack");

                        }
                        else
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, @"$.wl_Alert(" + "'" + "You have reached the maximum user limit.No more users could be added" + "'" + ",'info','#divInformation'" + ");", true);
                        }
                    }
                    else
                    {
                        lnkAddNewMobileUser.Visible = true;
                    }
                }
                else
                {
                    rptUserDetails.DataSource = null;
                    rptUserDetails.DataBind();
                    showRptHdrInfoAndEnableDisableAdd("No user added yet", true);
                }
            }
            else
            {
                rptUserDetails.DataSource = null;
                rptUserDetails.DataBind();
                showRptHdrInfoAndEnableDisableAdd("No user added yet", true);
            }
        }
        void bindUserDetailRptAfterPostBack(UpdatePanel updPanel, string filter)
        {
            string[] aryValuesFromHidField = hidCurrentSortOrder.Value.Split(',');
            GetUserDetail objUserDtl = new GetUserDetail();
            try
            {
                objUserDtl.getUserDetailsForSort(hfsPart4, strUserId
                    , (GetUserDetail.ORDER_BY)Enum.Parse(typeof(GetUserDetail.ORDER_BY), aryValuesFromHidField[1])
                    , (GetUserDetail.SORT_TYPE)Enum.Parse(typeof(GetUserDetail.SORT_TYPE), aryValuesFromHidField[2]), filter);
                if (objUserDtl.StatusCode == 0)
                {
                    bindUserDetailRpt(objUserDtl, updPanel);
                    Utilities.runPostBackScript("changeImageOfSortedCol();", updPanel, "Change Sort Image");
                    enableDisablePanelsForAddAndEdit("", false);
                }
                else
                {
                    throw new Exception("Internal server error");
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error", updPanel, DIALOG_TYPE.Error);
            }
        }
        DataTable getBlockedOrUnBlockedUsers(DataTable usersList, bool getBlockedUsers)
        {
            string filter = "";
            if (getBlockedUsers)
            {
                filter = String.Format("IS_BLOCKED = '{0}'", true);
                DataRow[] rows = usersList.Select(filter);
                if (rows.Length > 0)
                {
                    return rows.CopyToDataTable<DataRow>();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                filter = String.Format("IS_BLOCKED = '{0}'", false);
                DataRow[] rows = usersList.Select(filter);
                if (rows.Length > 0)
                {
                    return rows.CopyToDataTable<DataRow>();
                }
                else
                {
                    return null;
                }
            }
        }
        void fillPasswordOnPostBack()
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "FillPasswordAfterPostBack", "fillPasswordEntered();", true);
        }
        void showRptHdrInfoAndEnableDisableAdd(string headerInfo, bool enableAddButton)
        {
            lblHeaderInfo.Text = "<h1>" + headerInfo + "</h1>";
            if (enableAddButton)
                lnkAddNewMobileUser.Visible = true;
            else
                lnkAddNewMobileUser.Visible = false;
        }
        protected void UpdateUserDetails(string _CompanyId, string _SubAdminId, string _UserId, out string statusDescription)
        {
            #region Set Group List If Changed
            //Group list is set using javascript hence the value is not available after postback.So set it here before the page is rendered again.
            //string strGroupNamesSelected;
            //setGroupValsSelectedInLabelOnPostBack(out strGroupNamesSelected);

            string strUserGroupNamesSelected;
            setUserGroupValsSelectedInLabelOnPostBack(out strUserGroupNamesSelected);
            #endregion;

            //string strMessage = validateForm(_SubAdminId, true); ;//Validate(_SubAdminId, out strFieldBorder, true);
            statusDescription = "";
            string strBasicInfoError = "", strOtherInfoError = "", strMessengerError = "";

            string strProp = hidData.Value;
            string strAuth = hidAuthenticationData.Value;


            validateForm(_SubAdminId, true, out strBasicInfoError, out strOtherInfoError, out strMessengerError);
            if (strBasicInfoError.Contains("<li>") || strOtherInfoError.Contains("<li>") || strMessengerError.Contains("<li>"))
            {
                showUserDetSaveErrorMsg(strBasicInfoError, strOtherInfoError, strMessengerError);
                statusDescription = "validation error";
                return;
            }
            else if (strProp == "Error" || strAuth == "Error")
            {
                statusDescription = "validation error";
                return;
            }
            else
            {
                Boolean IsActive = true;
                string strStartULTag = @"<ul style=""position:relative;right:-3px;"">";
                string strLITag = "<li>", strLIEndTag = "</li>";
                strBasicInfoError = strStartULTag; strOtherInfoError = strStartULTag; strMessengerError = strStartULTag;
                //check if emailId and employee no already exists
                CheckForEmailAndEmpNoInEnterprise objCheckEmailAndEmpNo =
                    new CheckForEmailAndEmpNoInEnterprise(txtEmailId.Text.Trim()
                            , hfsPart4
                             , CheckForEmailAndEmpNoInEnterprise.SEARCH_FOR.ExistingUser
                             , _UserId
                         );
                objCheckEmailAndEmpNo.Process();
                if (objCheckEmailAndEmpNo.StatusCode == 0)
                {
                    if (objCheckEmailAndEmpNo.EmailExists && !objCheckEmailAndEmpNo.EmailExistsForUserIdToSearch)
                    {
                        strBasicInfoError = strStartULTag;
                        if (!objCheckEmailAndEmpNo.EmailExistsForUserIdToSearch)
                            strBasicInfoError += strLITag + @"Email exists for some other user." + strLIEndTag;
                        //Utilities.showAlert(strMessage, "formDiv", updRepeater);
                        showUserDetSaveErrorMsg(strBasicInfoError, strOtherInfoError, strMessengerError);
                        statusDescription = "Error in MailId and Employee No";
                        return;
                    }
                }
                else
                {
                    Utilities.showMessage(objCheckEmailAndEmpNo.StatusDescription, updRepeater, DIALOG_TYPE.Error);
                    return;
                }

                List<string> lstUserGroupSelected = getUserGroupIdsSelected(out strUserGroupNamesSelected);
                string strDomainId = String.Empty;
                MFICIENT_USER_TYPE userType = MFICIENT_USER_TYPE.CloudUser;
                switch (getUserTypeSelected(ddlUserType.SelectedItem.Value))
                {
                    case MFICIENT_USER_TYPE.CloudUser:
                        userType = MFICIENT_USER_TYPE.CloudUser;
                        strDomainId = string.Empty;
                        break;
                    case MFICIENT_USER_TYPE.ActiveDirectoryUser:
                        userType = MFICIENT_USER_TYPE.ActiveDirectoryUser;
                        strDomainId = ddlDomainList.SelectedValue;
                        break;

                }
               // string Requested_by = getGroupIdsSelected(out strGroupNamesSelected);
                UpdateUserCompleteDetail updateusercompletedetail = new UpdateUserCompleteDetail(txtEmailId.Text, txtFirstName.Text,
                           txtLastName.Text, txtMobile.Text, _CompanyId, "0", _SubAdminId, "", "",
                           "", "", "", IsActive,  Convert.ToByte(0),Convert.ToByte(0), _UserId
                           ,  Convert.ToByte(0), "",
                           userType, strDomainId, lstUserGroupSelected, txtUserName.Text, strProp, strAuth);
                updateusercompletedetail.Process();

                int intStatusCode = updateusercompletedetail.StatusCode;
                string strDescription = updateusercompletedetail.StatusDescription;
                if (intStatusCode == 0)
                {
                    enableDisablePanelsForAddAndEdit("", false);
                    setValOfHidPasswordEntered(String.Empty);
                    setValOfHidRetypePassEntered(String.Empty);
                    //hidPasswordEntered.Value = "";
                    clearControls();
                    Utilities.showMessage("User details updated successfully", updRepeater, DIALOG_TYPE.Info);
                }
                else
                {
                    Utilities.showMessage("Internal Error.Please try again", updRepeater, DIALOG_TYPE.Error);
                    statusDescription = "Internal server error";
                }
            }

        }
        protected void SaveUserDetails(string _AdminId, string _SubAdminId, string _CompanyId, out string statusDescription)
        {
            statusDescription = "";
            #region Set Group List If Changed
            //Group list and User Group List is set using javascript hence the value is not available after postback.So set it here before the page is rendered again.
            //string strGroupNamesSelected;
            //setGroupValsSelectedInLabelOnPostBack(out strGroupNamesSelected);

            string strUserGroupNamesSelected;
            setUserGroupValsSelectedInLabelOnPostBack(out strUserGroupNamesSelected);
            #endregion
            switch (this.getUserTypeSelected(ddlUserType.SelectedValue))
            {
                case MFICIENT_USER_TYPE.CloudUser:
                    break;
                case MFICIENT_USER_TYPE.ActiveDirectoryUser:
                    if (!Convert.ToBoolean(hidGotUserDtls.Value))
                    {
                        // Utilities.showAlert("You have to get the details of the user first before saving.",
                        //   "divInformation", updRepeater);
                        Utilities.showAlert("You have to get the details of the user first before saving.", "formDiv", updRepeater);
                        statusDescription = "You have to get the details of the user first before saving.";
                        return;
                    }
                    break;
            }
            string strBasicInfoError = "", strOtherInfoError = "", strMessengerError = "";
            string strProp = hidData.Value;
            string strAuth = hidAuthenticationData.Value;

            validateForm(_SubAdminId,
                         false,
                         out strBasicInfoError,
                         out strOtherInfoError,
                         out strMessengerError);
            if (strBasicInfoError.Contains("<li>") ||
                strOtherInfoError.Contains("<li>") ||
                strMessengerError.Contains("<li>")
                )
            {
                showUserDetSaveErrorMsg(strBasicInfoError,
                    strOtherInfoError,
                    strMessengerError
                    );
                statusDescription = "validation error";
                return;
            }
            else if (strProp == "Error" || strAuth == "Error")
            {
                statusDescription = "validation error";
                return;
            }
            else
            {
                Boolean IsActive = true;
                string strStartULTag = @"<ul style=""position:relative;right:-3px;"">";
                string strLITag = "<li>", strLIEndTag = "</li>";
                strBasicInfoError = strStartULTag; strOtherInfoError = strStartULTag; strMessengerError = strStartULTag;
                //check if emailId and employee no already exists
                CheckForEmailAndEmpNoInEnterprise objCheckEmailAndEmpNo = new CheckForEmailAndEmpNoInEnterprise(txtEmailId.Text.Trim()
                                                                            , hfsPart4
                                                                            , CheckForEmailAndEmpNoInEnterprise.SEARCH_FOR.NewUser, String.Empty);
                objCheckEmailAndEmpNo.Process();
                if (objCheckEmailAndEmpNo.StatusCode == 0)
                {
                    if (objCheckEmailAndEmpNo.EmailExists)
                    {
                        strBasicInfoError = strStartULTag;
                        if (objCheckEmailAndEmpNo.EmailExists)
                            strBasicInfoError += strLITag + @"Email exists for some other user." + strLIEndTag;
                        //Utilities.showAlert(strBasicInfoError, "divBasicInfoError", updRepeater);
                        showUserDetSaveErrorMsg(strBasicInfoError, strOtherInfoError, strMessengerError);
                        statusDescription = "Error in MailId and Employee No";
                        return;
                    }
                }
                else
                {
                    Utilities.showMessage(objCheckEmailAndEmpNo.StatusDescription, updRepeater, DIALOG_TYPE.Error);
                    return;
                }

                //string Requested_by = getGroupIdsSelected(out strGroupNamesSelected);
                List<string> lstUserGroupSelected = getUserGroupIdsSelected(out strUserGroupNamesSelected);
                string strPassword = String.Empty;
                string strDomainId = String.Empty, strMpluginId = String.Empty, strDomainName = String.Empty;

                switch (this.getUserTypeSelected(ddlUserType.SelectedValue))
                {
                    case MFICIENT_USER_TYPE.CloudUser:
                        strPassword = txtPassword.Text;
                        strDomainId = string.Empty;
                        break;
                    case MFICIENT_USER_TYPE.ActiveDirectoryUser:
                        strPassword = String.Empty;
                        getDataFromDomainDrpDwnValue(out strDomainId, out strDomainName, out strMpluginId);
                        break;

                }

                AddNewUserCompleteDetail addnewuser = new AddNewUserCompleteDetail(txtEmailId.Text, strPassword, txtFirstName.Text,
                txtLastName.Text, txtMobile.Text, _CompanyId, "0", _SubAdminId, "", "", "", "",
                txtUserName.Text, "", IsActive,  Convert.ToByte(0),
                Convert.ToByte(0),  Convert.ToByte(0),
                "", strDomainId, hfsPart3, this.Context, lstUserGroupSelected, strProp, strAuth);
                addnewuser.Process();                

                int intStatusCode = addnewuser.StatusCode;
                string strDescription = addnewuser.StatusDescription;
                if (intStatusCode == 0)
                {
                    enableDisablePanelsForAddAndEdit("", false);
                    //hidPasswordEntered.Value = "";
                    setValOfHidPasswordEntered(String.Empty);
                    setValOfHidRetypePassEntered(String.Empty);
                    clearControls();
                    Utilities.showMessage("Mobile user added  successfully.", updRepeater, DIALOG_TYPE.Info);
                }
                else if (intStatusCode == 1)
                {
                    Utilities.showMessage(strDescription, updRepeater, DIALOG_TYPE.Error);
                    statusDescription = strDescription;

                }
                else
                {
                    Utilities.showMessage("Internal Error.Please try again", updRepeater, DIALOG_TYPE.Error);
                    statusDescription = "Internal server error.";
                }
            }

        }
        void showUserDetSaveErrorMsg(string basicInfoError,
            string otherInfoError,
            string messengerError
            )
        {
            bool blnErrorExists = false;
            if (basicInfoError.Contains("<li>"))
            {
                Utilities.showAlert(basicInfoError, "divBasicInfoError", updRepeater, "BasicInfoError");
                blnErrorExists = true;
            }
            if (otherInfoError.Contains("<li>"))
            {
                Utilities.showAlert(otherInfoError, "divOtherDtlError", updRepeater, "OtherInfoError");
                blnErrorExists = true;
            }
            if (messengerError.Contains("<li>"))
            {
                Utilities.showAlert(messengerError, "divMessengerError", updRepeater, "MessengerError");
                blnErrorExists = true;
            }
            if (blnErrorExists)
            {
                Utilities.runPostBackScript("scrollToErrorMsg();", updRepeater, "ScrollToFirstErrorMsg");
            }
        }

        //string getGroupIdsSelected(out string groupName)
        //{
        //    string strGroupIdSelected = "";
        //    groupName = String.Empty;
        //    int iNoOfItems = chkLstGroups.Items.Count;
        //    int iIndexOfItem = 0;
        //    foreach (ListItem item in chkLstGroups.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            if (String.IsNullOrEmpty(strGroupIdSelected))
        //            {
        //                strGroupIdSelected += item.Value;
        //                groupName += item.Text;
        //            }
        //            else if (iIndexOfItem != iNoOfItems)
        //            {
        //                strGroupIdSelected += "," + item.Value;
        //                groupName += "," + item.Text;
        //            }
        //        }
        //        iIndexOfItem += 1;
        //    }
        //    return strGroupIdSelected;
        //}
        List<string> getUserGroupIdsSelected(out string userGroupName)
        {
            List<string> lstGroupIdSelected = new List<string>();
            userGroupName = String.Empty;
            int iNoOfItems = chkLstUserGroups.Items.Count;
            int iIndexOfItem = 0;
            foreach (ListItem item in chkLstUserGroups.Items)
            {
                if (item.Selected)
                {
                    lstGroupIdSelected.Add(item.Value);
                    if (String.IsNullOrEmpty(userGroupName))
                    {
                        userGroupName += item.Text;
                    }
                    else if (iIndexOfItem != iNoOfItems)
                    {
                        userGroupName += "," + item.Text;
                    }
                }
                iIndexOfItem += 1;
            }
            return lstGroupIdSelected;
        }
        protected void FillUserDetails(string _SubAdminId, string _UserName)
        {
            GetUserDetail getuserdetail = new GetUserDetail(false, false, _SubAdminId, "", _UserName, hfsPart4);
            getuserdetail.Process();
            DataTable dt = getuserdetail.ResultTable;
            if (dt.Rows.Count > 0)
            {
                DateTime dtDateOfBirth = new DateTime(Convert.ToInt64(dt.Rows[0]["DATE_OF_BIRTH"]));
                txtEmailId.Text = Convert.ToString(dt.Rows[0]["EMAIL_ID"]);
                txtFirstName.Text = Convert.ToString(dt.Rows[0]["FIRST_NAME"]);
                txtLastName.Text = Convert.ToString(dt.Rows[0]["LAST_NAME"]);


                lblHeaderUserName.Text = "( " + Convert.ToString(dt.Rows[0]["USER_NAME"]) + " " + ")";

                txtMobile.Text = Convert.ToString(dt.Rows[0]["MOBILE"]);
                txtUserName.Text = Convert.ToString(dt.Rows[0]["USER_NAME"]);
                bindUserTypeDropDown(isActiveDirectoryUsrAllowed(hfsPart4));
                if (!String.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["DOMAIN_ID"])))
                {
                    try
                    {
                        bindDomainListDropDown();
                        ddlDomainList.SelectedIndex = ddlDomainList.Items.IndexOf(
                        ddlDomainList.Items.FindByValue(Convert.ToString(dt.Rows[0]["DOMAIN_ID"])));
                        lblEditDomain.Text = ddlDomainList.SelectedItem.Text;
                    }
                    catch { }

                    ddlUserType.SelectedIndex = ddlUserType.Items.IndexOf(
                        ddlUserType.Items.FindByValue(Convert.ToString(getUserTypeByUserDetail(true))));

                    lblEditUserType.Text = ddlUserType.SelectedItem.Text;
                    showHideDomainPanel(true);
                }
                else
                {

                    ddlUserType.SelectedIndex = ddlUserType.Items.IndexOf(
                       ddlUserType.Items.FindByValue(Convert.ToString(getUserTypeByUserDetail(false))));
                    lblEditUserType.Text = ddlUserType.SelectedItem.Text;
                    showHideDomainPanel(false);
                }
                enableDisableDdlUserType(false);                

                //checkGroupListSelected(Convert.ToString(dt.Rows[0]["REQUESTED_BY"]));
                checkUserGroupSelected(getUserGrpLinkAndGrpDtl(Convert.ToString(dt.Rows[0]["USER_ID"])));

                //showGroupListInLabelAndFillGroupId();
                showUserGroupListInLabelAndFillGroupId();

                //this is required fot the block and unblock user and delete user in the edit section 21/8/2012
                if (!Convert.ToBoolean(dt.Rows[0]["IS_BLOCKED"]))
                {
                    lnkBlockUnblock.CommandName = "Block";
                    lnkBlockUnblock.CommandArgument = Convert.ToString(dt.Rows[0]["USER_ID"]);
                    lnkBlockUnblock.Text = "block";
                    lnkResetPassword.Visible = true;
                    lblSeparatorTwo.Visible = true;
                }
                else
                {
                    lnkBlockUnblock.CommandName = "Unblock";
                    lnkBlockUnblock.CommandArgument = Convert.ToString(dt.Rows[0]["USER_ID"]);
                    lnkBlockUnblock.Text = "unblock";
                    //if the user is blocked don't allow the reset password option
                    lnkResetPassword.Visible = false;
                    lblSeparatorTwo.Visible = false;
                }
                lnkDelete.CommandArgument = Convert.ToString(dt.Rows[0]["USER_ID"]);
                lnkDelete.CommandName = Convert.ToString(dt.Rows[0]["USER_NAME"]);

                List<CredentialProperties> Crds = Utilities.DeserialiseJson<List<CredentialProperties>>(Convert.ToString(dt.Rows[0]["CREDENTIAL_DETAIL"]));
                foreach (CredentialProperties Cr in Crds)
                {
                    Cr.unm = AesEncryption.AESDecrypt(hfsPart4.ToUpper(), Cr.unm);
                    if (!string.IsNullOrEmpty(Cr.pwd))
                        Cr.pwd = AesEncryption.AESDecrypt(hfsPart4.ToUpper(), Cr.pwd);
                }
                string strCredential = Utilities.SerializeJson<List<CredentialProperties>>(Crds);
                ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"CustomOrCredentialControlOnEdit(" + Convert.ToString(dt.Rows[0]["CUSTOM_PROPERTIES"]) + "," + strCredential + ");", true);
            }
        }
        int getUserTypeByUserDetail(bool hasDomainId)
        {
            if (hasDomainId) return (int)MFICIENT_USER_TYPE.ActiveDirectoryUser;
            else return (int)MFICIENT_USER_TYPE.CloudUser;
        }
        protected void clearControls()
        {
            txtEmailId.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtMobile.Text = "";
            txtUserName.Text = "";
            txtPassword.Text = "";
            txtRetypePassword.Text = "";
            hidDetailsFormMode.Value = "";
        }
        //private void ClearControls()
        //{
        //    foreach (Control c in Page.Controls)
        //    {
        //        foreach (Control ctrl in c.Controls)
        //        {
        //            if (ctrl is TextBox)
        //            {
        //                ((TextBox)ctrl).Text = string.Empty;
        //            }
        //        }
        //    }
        //}
        #endregion

        private void validateForm(string _SubAdminId,
            Boolean _IsEdit,
            out string basicInfoError,
            out string otherInfoError,
            out string messengerError)
        {
            string strScrollPositionHelp = String.Empty;
            string strStartUl = @"<ul style=""position:relative;right:-3px;"">";
            basicInfoError = strStartUl;
            otherInfoError = strStartUl;
            messengerError = strStartUl;
            string strLITag = "<li>", strLIEndTag = "</li>";
            MFICIENT_USER_TYPE eUserType = this.getUserTypeSelected(ddlUserType.SelectedValue);
            bool isValidUserName = false;
            switch (eUserType)
            {
                case MFICIENT_USER_TYPE.CloudUser:
                    isValidUserName = Utilities.isValidUserName(txtUserName.Text, 50, false);
                    break;
                case MFICIENT_USER_TYPE.ActiveDirectoryUser:
                    isValidUserName = Utilities.isValidUserName(txtUserName.Text, 50, true);
                    break;
            }
            if (!isValidUserName)
            {
                basicInfoError += strLITag + "Please enter valid user name." + strLIEndTag;
            }
            else
            {
                if (!_IsEdit)
                {
                    if (IsUserNameExists(txtUserName.Text, _SubAdminId))
                    {
                        basicInfoError += strLITag + "Username exists for another user." + strLIEndTag;
                    }
                }

            }
            if (!_IsEdit)
            {
                switch (eUserType)
                {
                    case MFICIENT_USER_TYPE.CloudUser:
                        if (!Utilities.IsValidString(txtPassword.Text, true, true, true, @"!@#$%^&*()_-", 6, 50, false, false))
                        {
                            if (_IsEdit)
                            { }
                            else
                            {
                                basicInfoError += strLITag + "Please enter valid password." + strLIEndTag;
                            }
                        }
                        if (txtPassword.Text != txtRetypePassword.Text)
                        {
                            basicInfoError += strLITag + "Password entered does not match." + strLIEndTag;
                        }
                        break;
                    case MFICIENT_USER_TYPE.ActiveDirectoryUser:
                        break;

                }
            }

            if (!Utilities.IsValidString(txtFirstName.Text, true, true, false, "- ", 1, 50, false, false))
            {
                basicInfoError += strLITag + "Please enter valid first name." + strLIEndTag;
            }
            if (!Utilities.IsValidString(txtLastName.Text, true, true, false, "- ", 0, 50, false, false))
            {
                basicInfoError += strLITag + "Please enter valid last name." + strLIEndTag;
            }

            if (!Utilities.IsValidEmail(txtEmailId.Text.TrimEnd()) || txtEmailId.Text.TrimEnd().Length > 50)
            {
                basicInfoError += strLITag + "Please enter valid email." + strLIEndTag;
            }
            if (!String.IsNullOrEmpty(validateMobileNo(txtMobile.Text)))
            {
                basicInfoError += strLITag + "Please enter valid mobile number." + strLIEndTag;
            }

            switch (this.getUserTypeSelected(ddlUserType.SelectedValue))
            {
                case MFICIENT_USER_TYPE.CloudUser:
                    break;
                case MFICIENT_USER_TYPE.ActiveDirectoryUser:
                    if (ddlDomainList.Items.Count == 0)
                    {
                        basicInfoError += strLITag + "Domain list is empty." + strLIEndTag;
                    }
                    break;
            }
            basicInfoError += "</ul>";
            messengerError += "</ul>";
            otherInfoError += "</ul>";

        }
        void callFormHelperJscriptFunction(bool isPostBack, UpdatePanel updPanel)
        {
            if (isPostBack)
            {
                if (updPanel != null)
                {
                    Utilities.runPostBackScript(
                         "doProcessForFormHelp();",
                         updPanel,
                         "FormHelperFunction");
                }
                else
                {
                    Utilities.runPostBackScript(
                         "doProcessForFormHelp();",
                         this.Page,
                         "FormHelperFunction");
                }
            }
            else
            {
                Utilities.runPageStartUpScript(
                    this.Page,
                    "FormHelperFunctionONPageStatup",
                    "doProcessForFormHelp();");
            }
        }

        protected Boolean IsUserNameExists(string _UserName, string _SubAdminId)
        {
            Boolean bolUserExist = false;
            GetUserDetail getuserdetail = new GetUserDetail(false, false, _SubAdminId, "", _UserName, hfsPart4);
            getuserdetail.Process();
            DataTable dt = getuserdetail.ResultTable;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    bolUserExist = true;
                }
            }

            return bolUserExist;
        }

        protected void lnkAddNewMobileUser_Click(object sender, EventArgs e)
        {
            try
            {
                clearControls();
                enableDisablePanelsForAddAndEdit("Add", false);
                hidDetailsFormMode.Value = "add";
                lblUserGroupList.Text = "No group selected yet";
                lblHeaderUserName.Visible = false;
                showHidePasswordPanel(true);
                showHideRetypePasswordPanel(true);
                showHidePwdContainerPanel(true);
                showHideUsernamePanel(true);

                foreach (ListItem item in chkLstUserGroups.Items)
                    item.Selected = false;

                bindUserTypeDropDown(isActiveDirectoryUsrAllowed(hfsPart4));
                ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), 
                    "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date(); setHeightOfUserTypeAndDomainInEditMode(); doProcessForFormHelp();", true);
            }
            catch
            {
                Utilities.showMessage("Internal server error.", updRepeater, DIALOG_TYPE.Error);
                enableDisablePanelsForAddAndEdit(String.Empty, false);
            }
        }
        bool isActiveDirectoryUsrAllowed(string companyId)
        {
            GetAccountSettingsByCompanyId objAccountSettings = new GetAccountSettingsByCompanyId(companyId);
            objAccountSettings.Process();
            if (objAccountSettings.StatusCode != 0)
                throw new MficientException("Internal server error.");
            else
            {
                if (objAccountSettings.AccountSettings.Tables[0].Rows.Count > 0)
                    return objAccountSettings.AllowActiveDirectoryUser;
                else
                    return false;
            }
        }

        protected void rptUserDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblNameAndUserName = (Label)e.Item.FindControl("lblNameAndUserName");
                lblNameAndUserName.Text = (string)DataBinder.Eval(e.Item.DataItem, "FIRST_NAME") + " " + (string)DataBinder.Eval(e.Item.DataItem, "LAST_NAME") + " ( " + (string)DataBinder.Eval(e.Item.DataItem, "USER_NAME") + " ) ";
                Label lblIsBlocked = (Label)e.Item.FindControl("lblIsBlocked");
                if (!(bool)DataBinder.Eval(e.Item.DataItem, "IS_BLOCKED"))
                {
                    lblIsBlocked.Text = "False";
                    lblNameAndUserName.Attributes.Remove("class");
                }
                else
                {
                    lblIsBlocked.Text = "True";
                    lblNameAndUserName.Attributes.Add("class", "repeaterAttentionLabel");
                    lblNameAndUserName.ToolTip = "Blocked.Click for details";
                }
                Label lblLogon = (Label)e.Item.FindControl("lblLogon");
                long datatime=Convert.ToInt64(DataBinder.Eval(e.Item.DataItem, "ulon"));

                lblLogon.Text = datatime > 0 ? Utilities.getCompanyLocalFormattedDateTime(tzi, datatime) : "";
            }
        }

        void processEdit(string loginName)
        {
            hidDetailsFormMode.Value = "Edit";
            enableDisablePanelsForAddAndEdit("Edit", false);
            FillUserDetails(strUserId, loginName);
            lblHeaderUserName.Visible = true;
            showHidePasswordPanel(false);
            showHideRetypePasswordPanel(false);
            showHideUsernamePanel(false);
            showHidePwdContainerPanel(false);
            Utilities.runPostBackScript("setHeightOfUserTypeAndDomainInEditMode();", upUserDtlsModal, "SetHeightOfUserDomainTypeDiv");
        }

        void processDeleteUser(string loginName, string userId)
        {
            GetUserCompleteDetails userDetail = new GetUserCompleteDetails(userId, hfsPart4);
            userDetail.Process();
            string strUserName = userDetail.UserName;

            //MBuzzServerConfigDtls objMBuzzSetting = ServerMappingManager.getMbuzzConfigDtls(hfsPart4);

            GetRegisteredDeviceDetailsByUserId objRegtDeviceDtls = new
                GetRegisteredDeviceDetailsByUserId(hfsPart4, strUserId
                , userId);

            objRegtDeviceDtls.Process();

            List<MFEDevice> lstRegDev = objRegtDeviceDtls.RegDeviceDtlsDevIdAndDevType;

            DeleteUser objDeleteUser = new DeleteUser(strUserId, loginName, userId, hfsPart4);
            objDeleteUser.Process();

            if (objDeleteUser.StatusCode == 0)
            {
                try
                {
                    if (objRegtDeviceDtls.StatusCode == 0)
                    {
                        foreach (MFEDevice device in lstRegDev)
                        {
                            //Have to send this MF_USER_BLOCK push message in this as well.
                            SavePushMsgOutboxInMgram objSavePshMsgOutbox = new
                                                    SavePushMsgOutboxInMgram(hfsPart4,
                                                    strUserName,
                                                    device.DeviceId,
                                                    device.DevicePushMsgId,
                                                    device.DeviceType,
                                                    SavePushMsgOutboxInMgram.NOTIFICATION_TYPE.MF_USER_BLOCK, "");
                            objSavePshMsgOutbox.Process();
                        }
                    }
                }
                catch
                { }
                clearFilterTextBox();
                bindUserDetailRptAfterPostBack(updRepeater, String.Empty);
                Utilities.showMessage("User deleted successfully", updRepeater, DIALOG_TYPE.Info);
            }
            else
            {
                Utilities.showMessage("Internal server error.", updRepeater, DIALOG_TYPE.Error);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="blockOrUnblock">pass block or unblock</param>
        void processBlockUnblock(string userId, string blockOrUnblock, UpdatePanel updPanelUpdated)
        {
            GetUserCompleteDetails userDetail = new GetUserCompleteDetails(userId, hfsPart4);
            userDetail.getUsrDtlDeptDivAndGroup();
            DataSet dsUserDetails = userDetail.UserDtlWithDivDeptAndGroups;

            GetRegisteredDeviceDetailsByUserId objRegtDeviceDtls = new
                            GetRegisteredDeviceDetailsByUserId(hfsPart4, strUserId
                            , userId);

            objRegtDeviceDtls.Process();

            List<MFEDevice> lstRegDev = objRegtDeviceDtls.RegDeviceDtlsDevIdAndDevType;


            BlockOrUnBlockUser objBlockUnblock = new BlockOrUnBlockUser(strUserId, userId, blockOrUnblock, hfsPart4);
            objBlockUnblock.Process();

            if (objBlockUnblock.StatusCode == 0)
            {

            //    try
            //    {
            //        if (dsUserDetails != null)
            //        {
                        //MBuzzServerConfigDtls objMBuzzSetting =  ServerMappingManager.getMbuzzConfigDtls(hfsPart4);
                        //if (!String.IsNullOrEmpty(objMBuzzSetting.ServerIP))
                        //{
                            //if (blockOrUnblock.ToLower() == "block")
                            //{
                            //    UserDeletedorBlockedRequest userDeletedorBlocked = new UserDeletedorBlockedRequest(Utilities.GetMsgRefID(), Convert.ToString(dsUserDetails.Tables[0].Rows[0]["USER_NAME"]), ((int)MessageCode.CONTACT_BLOCKED).ToString(), hfsPart4);
                            //    mBuzzClient objClient = new mBuzzClient(userDeletedorBlocked.RequestJson, objMBuzzSetting.ServerIP, objMBuzzSetting.PortNumber);
                            //    ThreadPool.QueueUserWorkItem(new WaitCallback(objClient.Connect), null);
                            //}
                            //else if (blockOrUnblock.ToLower() == "unblock")
                            //{
                            //    UserData user = new UserData();
                            //    user.unm = Convert.ToString(dsUserDetails.Tables[0].Rows[0]["USER_NAME"]);
                            //    user.fnm = Convert.ToString(dsUserDetails.Tables[0].Rows[0]["FIRST_NAME"]) + " " + Convert.ToString(dsUserDetails.Tables[0].Rows[0]["LAST_NAME"]);
                            //    user.dep = Convert.ToString(dsUserDetails.Tables[1].Rows[0]["Departments"]);
                            //    user.des = Convert.ToString(dsUserDetails.Tables[0].Rows[0]["DESIGNATION_NAME"]);
                            //    user.em = Convert.ToString(dsUserDetails.Tables[0].Rows[0]["EMAIL_ID"]);
                            //    GetUserDetail obj = new GetUserDetail();
                            //    user.status = "1";
                            //    AddNewContactRequest addNewContact = new AddNewContactRequest(Utilities.GetMsgRefID(), user, hfsPart4, obj.GetUserContactsForSendRequest(Convert.ToString(dsUserDetails.Tables[0].Rows[0]["REQUESTED_BY"])));
                            //    mBuzzClient objClient = new mBuzzClient(addNewContact.RequestJson, objMBuzzSetting.ServerIP, objMBuzzSetting.PortNumber);
                            //    ThreadPool.QueueUserWorkItem(new WaitCallback(objClient.Connect), null);
                            //}
                        //}
                    //}
                //}
                //catch { }
                if (blockOrUnblock.ToLower() == "block")
                {
                    try
                    {
                        if (objRegtDeviceDtls.StatusCode == 0)
                        {
                            foreach (MFEDevice device in lstRegDev)
                            {
                                //Have to send this MF_USER_BLOCK push message in this as well.
                                SavePushMsgOutboxInMgram objSavePshMsgOutbox = new
                                                        SavePushMsgOutboxInMgram(hfsPart4,
                                                        Convert.ToString(dsUserDetails.Tables[0].Rows[0]["USER_NAME"]),
                                                        device.DeviceId,
                                                        device.DevicePushMsgId,
                                                        device.DeviceType,
                                                        SavePushMsgOutboxInMgram.NOTIFICATION_TYPE.MF_USER_BLOCK, "");
                                objSavePshMsgOutbox.Process();
                            }
                        }
                    }
                    catch
                    { }
                }

                clearFilterTextBox();
                bindUserDetailRptAfterPostBack(updPanelUpdated, String.Empty);
                mFicientCommonProcess.ACTIVITYENUM process = mFicientCommonProcess.ACTIVITYENUM.USER_BLOCKED;
                if (blockOrUnblock.ToLower() == "block")
                {
                    Utilities.showMessage("User blocked successfully", updPanelUpdated, DIALOG_TYPE.Info);
                }
                else
                {
                   process = mFicientCommonProcess.ACTIVITYENUM.USER_UNBLOCK;
                    Utilities.showMessage("User Unblocked successfully", updPanelUpdated, DIALOG_TYPE.Info);
                }
                Utilities.saveActivityLog(null, process, hfsPart3, strUserId, userId, Convert.ToString(dsUserDetails.Tables[0].Rows[0]["USER_NAME"]), Convert.ToString(dsUserDetails.Tables[0].Rows[0]["FIRST_NAME"]) + " " + Convert.ToString(dsUserDetails.Tables[0].Rows[0]["LAST_NAME"]), "", "", "", "", "");
            }
            else
            {
                Utilities.showMessage("Internal errror.Please try again", updPanelUpdated, DIALOG_TYPE.Error);
            }
        }

        protected void lnkResetPassword_Click(object sender, EventArgs e)
        {
            //txtUserName panel is invisible but the value is still filled in the edit mode.So there is no need to change the process.
            ResetPasword objResetPassword =
                new ResetPasword(
                    txtUserName.Text,
                    hfsPart4,
                    strUserId,
                    this.Context);
            objResetPassword.ProcessForMobileUser();
            if (objResetPassword.StatusCode == 0)
            {
                Utilities.showMessage("Password has been reset.", updRepeater, DIALOG_TYPE.Info);
                enableDisablePanelsForAddAndEdit("edit", false);
            }
            else
            {
                Utilities.showMessage("Internal Error.Please try again", updRepeater, DIALOG_TYPE.Error);
            }
            enableDisablePanelsForAddAndEdit("edit", false);
        }


        protected void lnkShowDepartmentPopUp_Click(object sender, EventArgs e)
        {
            Utilities.showModalPopup("divDepartmentModalPopUp", updRepeater, "Departments", "350", false);
        }

        protected void lnkShowGroupList_Click(object sender, EventArgs e)
        {
            Utilities.showModalPopup("divGroupModalPopUp", updRepeater, "Groups", "350", false);
        }

        //void showGroupListInLabelAndFillGroupId()
        //{
        //    lblGroupList.Text = "";
        //    int iNoOfItems = chkLstGroups.Items.Count;
        //    int iIndexOfItem = 0;
        //    foreach (ListItem item in chkLstGroups.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            if (String.IsNullOrEmpty(lblGroupList.Text))
        //            {
        //                lblGroupList.Text += item.Text;
        //                lblGroupListId.Text += item.Value;
        //            }
        //            else if (iIndexOfItem != iNoOfItems)
        //            {
        //                lblGroupList.Text += "," + " " + item.Text;
        //                lblGroupListId.Text += "," + item.Value;
        //            }
        //        }
        //        iIndexOfItem += 1;
        //    }
        //    if (String.IsNullOrEmpty(lblGroupList.Text))
        //    {
        //        lblGroupList.Text = "No group selected yet";
        //    }
        //}
        void showUserGroupListInLabelAndFillGroupId()
        {
            lblUserGroupList.Text = "";
            int iNoOfItems = chkLstUserGroups.Items.Count;
            int iIndexOfItem = 0;
            foreach (ListItem item in chkLstUserGroups.Items)
            {
                if (item.Selected)
                {
                    if (String.IsNullOrEmpty(lblUserGroupList.Text))
                    {
                        lblUserGroupList.Text += item.Text;
                        lblUserGroupIds.Text += item.Value;
                    }
                    else if (iIndexOfItem != iNoOfItems)
                    {
                        lblUserGroupList.Text += "," + " " + item.Text;
                        lblUserGroupIds.Text += "," + item.Value;
                    }
                }
                iIndexOfItem += 1;
            }
            if (String.IsNullOrEmpty(lblUserGroupList.Text))
            {
                lblUserGroupList.Text = "No group selected yet";
            }
        }
        void bindGroupChkLst()
        {
            GetGroupDetails objGroupDtls = new GetGroupDetails(hfsPart4);
            objGroupDtls.Process();
            if (objGroupDtls != null)
            {
                hidgroups.Value = Utilities.SerializeJson<List<List<string>>>(objGroupDtls.Groups);
                //if (objGroupDtls.ResultTable.Rows.Count > 0)
                //{
                //    chkLstGroups.DataSource = objGroupDtls.ResultTable;
                //    chkLstGroups.DataTextField = "GROUP_NAME";
                //    chkLstGroups.DataValueField = "GROUP_ID";
                //    chkLstGroups.DataBind();
                //}
                //else
                //{
                //    lblGroupList.Text = "No groups added for the company yet";
                //}
                if (objGroupDtls.ResultTable.Rows.Count > 0)
                {
                    chkLstUserGroups.DataSource = objGroupDtls.ResultTable;
                    chkLstUserGroups.DataTextField = "GROUP_NAME";
                    chkLstUserGroups.DataValueField = "GROUP_ID";
                    chkLstUserGroups.DataBind();
                }
                else
                {
                    lblUserGroupList.Text = "No groups added for the company yet";
                }
            }
            else
            {
                lblUserGroupList.Text = "Internal server error.Please try again";
            }
        }
        //void checkGroupListSelected(string requestBy)
        //{
        //    string[] straryRequestBy = requestBy.Split(',');
        //    foreach (ListItem item in chkLstGroups.Items)
        //    {
        //        for (int i = 0; i < straryRequestBy.Length; i++)
        //        {
        //            if (item.Value.ToLower().Trim() == straryRequestBy[i].ToLower().Trim())
        //            {
        //                item.Selected = true;
        //                break;
        //            }
        //            else
        //            {
        //                item.Selected = false;
        //            }
        //        }
        //    }
        //}

        List<MFEUserGroupLinkAndGroup> getUserGrpLinkAndGrpDtl(string userId)
        {
            GetUserCompleteDetails objUsrCmpltDtl =
                new GetUserCompleteDetails(userId, hfsPart4);
            return objUsrCmpltDtl.GetUserGroupLinkAndGrpDtl();
        }
        void checkUserGroupSelected(List<MFEUserGroupLinkAndGroup> usrGrpLinkAndGrp)
        {
            if (usrGrpLinkAndGrp != null)
            {
                foreach (ListItem item in chkLstUserGroups.Items)
                {
                    bool blnItemExists = false;
                    foreach (MFEUserGroupLinkAndGroup usrGrp in usrGrpLinkAndGrp)
                    {
                        if (usrGrp.UserGroupLink.GroupId.ToLower().Trim() == item.Value.ToLower().Trim())
                        {
                            blnItemExists = true;
                            break;
                        }
                    }
                    if (blnItemExists)
                    {
                        item.Selected = true;
                    }
                    else
                    {
                        item.Selected = false;
                    }
                }
            }
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            LinkButton delete = (LinkButton)sender;
            //command name has user name and command argument has userid
            processDeleteUser(delete.CommandName, delete.CommandArgument);
            clearFilterTextBox();
            bindUserDetailRptAfterPostBack(updRepeater, String.Empty);
            enableDisablePanelsForAddAndEdit("", false);
        }

        protected void lnkBlockUnblock_Click(object sender, EventArgs e)
        {
            LinkButton blockUnblock = (LinkButton)sender;
            if (blockUnblock.CommandName.ToLower() == "block")
            {
                processBlockUnblock(blockUnblock.CommandArgument, "block", updRepeater);
            }
            else
            {
                processBlockUnblock(blockUnblock.CommandArgument, "unblock", updRepeater);
            }
            clearFilterTextBox();
            bindUserDetailRptAfterPostBack(updRepeater, String.Empty);
            enableDisablePanelsForAddAndEdit("", false);
        }
        /// <summary>
        /// date in dd-mm-yyyy
        /// </summary>
        /// <param name="date"></param>
        long convertDateToTicks(string date, out string errorDescription)
        {
            errorDescription = "";
            string[] aryDateParts = date.Split('-');
            try
            {
                DateTime dtDateSelected = new DateTime(Convert.ToInt32(aryDateParts[2]), Convert.ToInt32(aryDateParts[1]), Convert.ToInt32(aryDateParts[0]));
                return dtDateSelected.Ticks;
            }
            catch (FormatException e)
            {
                errorDescription = "Date not in valid format.Please enter valid date";
                return -1000;
            }
            catch 
            {
                errorDescription = "Please enter a valid date";
                return -1000;
            }
        }

        /// <summary>
        /// date in dd-mm-yyyy
        /// </summary>
        /// <param name="date"></param>
        bool isValidDate(string dateOfBirth)
        {
            if (!string.IsNullOrEmpty(dateOfBirth))
            {
                try
                {
                    string[] aryDateParts = dateOfBirth.Split('-');
                    DateTime dt = new DateTime(int.Parse(aryDateParts[2]), int.Parse(aryDateParts[1]), int.Parse(aryDateParts[0]));
                    if (dt < DateTime.UtcNow.Date)
                    {
                        return true;
                    }
                }
                catch (FormatException)
                {
                    return false;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        protected void chkShowBlockedUser_CheckedChanged(object sender, EventArgs e)
        {
            clearFilterTextBox();
            showHideShowAllLink(false);
            bindUserDetailRptAfterPostBack(updRepeater, txtFilter.Text);
        }

        protected void btnRowClickPostBack_Click(object sender, EventArgs e)
        {
            if (hidSortingDetail.Value.Trim().Length == 0 && hidRowValuesUsedAfterPostBack.Value.Trim().Length != 0)
            {
                string[] aryValuesFromHidField = hidRowValuesUsedAfterPostBack.Value.Split(';');
                ucUserDetails.UserId = aryValuesFromHidField[0];
                ucUserDetails.UserName = aryValuesFromHidField[2];
                ucUserDetails.IsBlocked = Convert.ToBoolean(aryValuesFromHidField[1]);
                ucUserDetails.rememberUserId();

                ucUserDetails.getUserDetailsAndFillData(aryValuesFromHidField[0], hfsPart4);
                Utilities.showModalPopup("divUserDtlsModal", updRepeater, "User Detail", Convert.ToString(Utilities.getModalPopUpWidth(MODAL_POP_UP_NAME.USER_DETAIL)), true);
                upUserDtlsModal.Update();
                hidRowValuesUsedAfterPostBack.Value = "";
                enableDisablePanelsForAddAndEdit("", false);
            }
            else if (hidSortingDetail.Value.Trim().Length != 0)//for sorting
            {
                string[] aryValuesFromHidField = hidSortingDetail.Value.Split(',');
                GetUserDetail objUserDtl = new GetUserDetail();
                try
                {
                    objUserDtl.getUserDetailsForSort(hfsPart4, strUserId
                        , (GetUserDetail.ORDER_BY)Enum.Parse(typeof(GetUserDetail.ORDER_BY), aryValuesFromHidField[1])
                        , (GetUserDetail.SORT_TYPE)Enum.Parse(typeof(GetUserDetail.SORT_TYPE), aryValuesFromHidField[2]), txtFilter.Text);
                    if (objUserDtl.StatusCode == 0)
                    {
                        if (objUserDtl.TotalUsersCount > 0)
                        {
                            bindUserDetailRpt(objUserDtl, updRowPostBack);
                            Utilities.runPostBackScript("changeImageOfSortedCol();clearSortingDtlHiddenField();", updRepeater, "Change Sort Image");
                            enableDisablePanelsForAddAndEdit("", false);
                            if (objUserDtl.ResultTable.Rows.Count > 0 && !String.IsNullOrEmpty(txtFilter.Text))
                            {
                                showHideShowAllLink(true);
                            }
                            else
                            {
                                showHideShowAllLink(false);
                            }
                        }
                        else
                        {
                            hidSortingDetail.Value = "";//so that the original sort order is not changed.as no record is found.
                        }
                    }
                    else
                    {
                        hidSortingDetail.Value = "";
                        throw new Exception("Internal server error");
                    }

                }
                catch
                {
                    hidSortingDetail.Value = "";
                    Utilities.showMessage("Internal server error", updRowPostBack, DIALOG_TYPE.Error);
                }
                enableDisablePanelsForAddAndEdit("", false);
            }
        }
        void showHidePanelUsingJavascript(PanelToShowHide panelName, UpdatePanel updPanel)
        {
            string strScript = "";
            switch (panelName)
            {
                case PanelToShowHide.Repeater:
                    strScript = "hideShowPanelForCancel(" + (int)PanelToShowHide.Repeater + ")";
                    break;
                case PanelToShowHide.Form:
                    strScript = "hideShowPanelForCancel(" + (int)PanelToShowHide.Form + ")";
                    break;
            }
            if (!(updPanel == null))
            {
                Utilities.runPostBackScript(strScript, updPanel, "Show Hide Panel");
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "onload", strScript, true);
            }
        }
        protected void imgSearch_Click(object sender, EventArgs e)
        {
            if (txtFilter.Text == "")
            {
                Utilities.showAlert("Please enter some data to search", "divInformation", updRepeater);
                enableDisablePanelsForAddAndEdit("", false);
                return;
            }
            try
            {
                int iTotalUserCount = 0;
                GetUserDetail objUserDetail = getUserDetailForSearch(out iTotalUserCount);
                if (iTotalUserCount > 0)
                {
                    bindUserDetailRpt(objUserDetail, updRepeater);
                    Utilities.runPostBackScript("changeImageOfSortedCol();", updRepeater, "Change Sort Image");
                    enableDisablePanelsForAddAndEdit("", false);
                    showHideShowAllLink(true);
                }
                else
                {
                    Utilities.showMessage("No user found", updRepeater, DIALOG_TYPE.Info);
                    enableDisablePanelsForAddAndEdit("", false);
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error", updRepeater, DIALOG_TYPE.Error);
            }
            enableDisablePanelsForAddAndEdit("", false);
        }
        GetUserDetail getUserDetailForSearch(out int totalUserCount)
        {
            GetUserDetail objUserDetail = new GetUserDetail(false, true, true, strUserId, hfsPart4, txtFilter.Text);
            objUserDetail.Process();
            if (objUserDetail.StatusCode == 0)
            {
                totalUserCount = objUserDetail.TotalUsersCount;
                return objUserDetail;
            }
            else
            {
                totalUserCount = 0;
                return objUserDetail;
            }
        }

        protected void lnkShowCompleteList_Click(object sender, EventArgs e)
        {
            clearFilterTextBox();
            bindUserDetailRptAfterPostBack(updRepeater, String.Empty);
            showHideShowAllLink(false);
        }
        void clearFilterTextBox()
        {
            txtFilter.Text = "";
        }

        void showHideShowAllLink(bool show)
        {
            if (show)
            {
                lnkShowCompleteList.Visible = true;
            }
            else
            {
                lnkShowCompleteList.Visible = false;
            }
        }

        void bindDomainListDropDown()
        {
            GetActvDirDomainSettings objActvDirSettings =
                            new GetActvDirDomainSettings(hfsPart4);
            objActvDirSettings.Process();
            if (objActvDirSettings.StatusCode == 0)
            {
                if (objActvDirSettings.ActDirDomainSettings.Rows.Count == 0)
                {
                    throw new MficientException( "Could not get Domain list." + "You have to add domains before adding active directory user." );
                }
                else
                {
                    ddlDomainList.DataSource = objActvDirSettings.ActDirDomainSettings;
                    ddlDomainList.DataTextField = "DOM_MPLUGIN_NAME";
                    ddlDomainList.DataValueField = "DOMAIN_ID";
                    ddlDomainList.DataBind();
                }
            }
            else
            {
                throw new Exception("Internal server error.");
            }
        }

        protected void lnkActvDirUserDetail_Click(object sender, EventArgs e)
        {
            try
            {
                #region Set Group List If Changed
                //Group list is set using javascript hence the value is not available after postback.So set it here before the page is rendered again.
               // string strGroupnameSelected;
               // setGroupValsSelectedInLabelOnPostBack(out strGroupnameSelected);
                #endregion;
                if (ddlDomainList.Items.Count == 0 || ddlDomainList.SelectedValue == "-1")
                {
                    Utilities.showAlert("Please select a domain.",
                        "divInformation", updRepeater);
                    return;
                }
                if (String.IsNullOrEmpty(txtUserName.Text))
                {
                    Utilities.showAlert("Please enter a username.",
                       "divInformation", updRepeater);
                    return;
                }
                string strRqst, strUrl = "";
                string strMpluginAgntName, strDomainName, strDomainId;
                this.getDataFromDomainDrpDwnValue(out strDomainId,
                     out strDomainName, out strMpluginAgntName);

                string strTicks = Convert.ToString(DateTime.Now.Ticks);
                mPluginAgents objAgent = new mPluginAgents();
                string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);

                strRqst = "{\"req\":{\"rid\":\"" + strRqtId + "\",\"eid\":\"" + hfsPart4 +"\",\"agtnm\":\"" + strMpluginAgntName.Trim() +
                    "\",\"agtpwd\":\"" +objAgent.GetMpluginAgentPassword(hfsPart4, strMpluginAgntName.Trim()) +"\",\"dmnm\":\"" + strDomainName.Trim() +
                    "\",\"unm\":\"" + txtUserName.Text.Trim() + "\"}}";

                GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(hfsPart4);
                objServerUrl.Process();
                if (objServerUrl.StatusCode == 0)
                {
                    strUrl = objServerUrl.ServerUrl;
                    if (strUrl.Length > 0)
                    {
                        strUrl = strUrl + "/MPGetActiveDirectoryUserDetails.aspx?d=" + Utilities.UrlEncode(strRqst);
                        HTTP oHttp = new HTTP(strUrl);
                        oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                        HttpResponseStatus oResponse = oHttp.Request();
                        if (oResponse.StatusCode == HttpStatusCode.OK)
                        {
                            List<string> lstDomains = new List<string>();
                            GetActiveDirectoryUserDetailsResp obj = new GetActiveDirectoryUserDetailsResp(oResponse.ResponseText);
                            if (obj.Code == "0")
                            {
                                txtEmailId.Text = obj.Data.email;
                                txtFirstName.Text = obj.Data.fnm;
                                txtLastName.Text = obj.Data.lnm;
                                txtMobile.Text = obj.Data.mbl;
                                hidGotUserDtls.Value = "true";
                                hidGotDtlsOfUsrNm.Value = txtUserName.Text;
                            }

                            else
                                Utilities.showMessage("Could not get the user details.", updRepeater, DIALOG_TYPE.Error);
                        }
                        else if (oResponse.StatusCode == HttpStatusCode.NotFound)
                            Utilities.showMessage("mPlugin server not found.", updRepeater, DIALOG_TYPE.Info);
                        else
                            throw new Exception();
                    }
                    else
                        throw new Exception();
                }
                else
                    throw new Exception();
            }
            catch 
            {
                Utilities.showMessage("Internal server error",
                                   updRepeater, DIALOG_TYPE.Error);
            }
        }
        void getDataFromDomainDrpDwnValue(out string domainId,
            out string domainName,
            out string mPluginAgentName)
        {
            //Value DomainId ,Text DomainName ( mplugin )
            if (ddlDomainList.SelectedValue == "-1") throw new Exception();
            domainId = ddlDomainList.SelectedValue;
            string strItemText = ddlDomainList.SelectedItem.Text.Trim();
            //domain name can contain only . or -
            domainName = strItemText.Substring(0, strItemText.IndexOf('(')).Trim();
            //agent name does not have any special character.
            mPluginAgentName =
                strItemText.Substring(
                strItemText.IndexOf('(') + 1,
                strItemText.IndexOf(')') - strItemText.IndexOf('(') - 1
                ).Trim();
        }
        protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region Set Group List If Changed
            //Group list is set using javascript hence the value is not available after postback.So set it here before the page is rendered again.
            //string strGroupNameselected;
            //setGroupValsSelectedInLabelOnPostBack(out strGroupNameselected);
            #endregion;
            try
            {
                switch (getUserTypeSelected(ddlUserType.SelectedValue))
                {
                    case MFICIENT_USER_TYPE.CloudUser:
                        showHideControlsByDdlUserType(MFICIENT_USER_TYPE.CloudUser,
                            true);
                        setValOfHidGotUserDtls("false");
                        break;
                    case MFICIENT_USER_TYPE.ActiveDirectoryUser:
                        bindDomainListDropDown();
                        showHideControlsByDdlUserType(MFICIENT_USER_TYPE.ActiveDirectoryUser,
                            true);
                        setValOfHidGotUserDtls("false");
                        setValOfHidPasswordEntered(String.Empty);
                        setValOfHidRetypePassEntered(String.Empty);
                        break;
                }
            }
            catch (MficientException ex)
            {
                ddlUserType.SelectedIndex =
                    ddlUserType.Items.IndexOf(
                    ddlUserType.Items.FindByValue(
                    ((int)MFICIENT_USER_TYPE.CloudUser).ToString()
                    )
                    );

                Utilities.showMessage(ex.Message,
                    updRepeater, DIALOG_TYPE.Error);
            }
            catch
            {
                Utilities.showMessage("Iternal server error.",
                    updRepeater, DIALOG_TYPE.Error);
            }
        }
        MFICIENT_USER_TYPE getUserTypeSelected(string value)
        {
            return (MFICIENT_USER_TYPE)Enum.Parse(typeof(MFICIENT_USER_TYPE), value);
        }
        /// <summary>
        /// Group list is set using javascript hence the value is not available after postback.So set it here before the page is rendered again.
        /// </summary>
        //void setGroupValsSelectedInLabelOnPostBack(out string strGroupNamesSelected)
        //{
        //    getGroupIdsSelected(out strGroupNamesSelected);
        //    if (!String.IsNullOrEmpty(strGroupNamesSelected))
        //    {
        //        lblGroupList.Text = strGroupNamesSelected;
        //    }
        //    else
        //    {
        //        lblGroupList.Text = "No group selected yet";
        //    }
        //}
        void setUserGroupValsSelectedInLabelOnPostBack(out string strUserGroupNamesSelected)
        {
            getUserGroupIdsSelected(out strUserGroupNamesSelected);
            if (!String.IsNullOrEmpty(strUserGroupNamesSelected))
            {
                lblUserGroupList.Text = strUserGroupNamesSelected;
            }
            else
            {
                lblUserGroupList.Text = "No group selected yet";
            }
        }
        string validateMobileNo(string mobileNo)
        {
            string strErrorMsg = "Invalid mobile number.";
            try
            {
                if (mobileNo.Length < 7 || mobileNo.Length > 20) throw new MficientException(strErrorMsg);
                char chrFirstChar = Convert.ToChar(mobileNo.Substring(0, 1));
                if (chrFirstChar != '+') throw new MficientException(strErrorMsg);
                char chrSecondChar = Convert.ToChar(mobileNo.Substring(1, 1));
                int iSecondChar = 0;
                try
                {
                    //check if it is a number.
                    iSecondChar = int.Parse(chrSecondChar.ToString());
                }
                catch
                {
                    throw new MficientException(strErrorMsg);
                }
                if (iSecondChar == 0) throw new MficientException(strErrorMsg);
                string strNoFromThirdChar = mobileNo.Substring(2, mobileNo.Length - 2);
                try
                {
                    ulong.Parse(strNoFromThirdChar);
                }
                catch
                {
                    throw new MficientException(strErrorMsg);
                }

                strErrorMsg = String.Empty;
                return strErrorMsg;
            }
            catch
            {
                return strErrorMsg;
            }
        }

        #region Show Hide Controls And Panels
        void showHideUsernamePanel(bool show)
        {
            if (show) pnlUserName.Visible = true;
            else pnlUserName.Visible = false;
        }
        void showHidePasswordPanel(bool show)
        {
            if (show) pnlPassword.Visible = true;
            else pnlPassword.Visible = false;
        }
        void showHideRetypePasswordPanel(bool show)
        {
            if (show) pnlRetypePassword.Visible = true;
            else pnlRetypePassword.Visible = false;
        }
        void showHideDomainPanel(bool show)
        {
            if (show) pnlDomain.Visible = true;
            else pnlDomain.Visible = false;
        }
        void showHideActiveDirUserCheckButton(bool show)
        {
            if (show)
            {
                lnkActvDirUserDetail.Visible = true;
            }
            else
            {
                lnkActvDirUserDetail.Visible = false;
            }
        }
        void showHideLnkActvDirGetDetails(bool show)
        {
            if (show) lnkActvDirUserDetail.Visible = true;
            else lnkActvDirUserDetail.Visible = false;
        }
        void showHideControlsByDdlUserType(MFICIENT_USER_TYPE userType
            , bool isForAddingUser)
        {
            switch (userType)
            {
                case MFICIENT_USER_TYPE.CloudUser:
                    if (isForAddingUser)
                    {
                        showHidePasswordPanel(true);
                        showHideRetypePasswordPanel(true);
                        showHideUsernamePanel(true);
                        showHideDomainPanel(false);
                        showHidePwdContainerPanel(true);
                        showHideActiveDirUserCheckButton(false);
                        showHideLnkActvDirGetDetails(false);
                    }
                    else
                    {
                        //The check box will be disabled 
                        //when in edit mode
                        //so this situation is not required
                    }
                    break;
                case MFICIENT_USER_TYPE.ActiveDirectoryUser:
                    showHidePasswordPanel(false);
                    showHideRetypePasswordPanel(false);
                    showHideUsernamePanel(true);
                    showHideDomainPanel(true);
                    showHidePwdContainerPanel(false);
                    showHideActiveDirUserCheckButton(true);
                    showHideLnkActvDirGetDetails(true);
                    break;

            }

        }
        void showHideDdlUserType(bool show)
        {
            if (show) ddlUserType.Visible = true;
            else ddlUserType.Visible = false;
        }
        void showHideDdlDomainList(bool show)
        {
            if (show) ddlDomainList.Visible = true;
            else ddlDomainList.Visible = false;
        }
        void showHideEditLblDomianName(bool show)
        {
            if (show) lblEditDomain.Visible = true;
            else lblEditDomain.Visible = false;
        }
        void showHideEditLblUserType(bool show)
        {
            if (show) lblEditUserType.Visible = true;
            else lblEditUserType.Visible = false;
        }
        void showHidePwdContainerPanel(bool show)
        {
            if (show)
            {
                pnlPasswordContainer.Visible = true;
            }
            else
            {
                pnlPasswordContainer.Visible = false;
            }
        }
        #endregion
        #region Enable Disable Controls
        void enableDisableDdlUserType(bool enable)
        {
            if (enable) ddlUserType.Enabled = true;
            else ddlUserType.Enabled = false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode">Pass add ,edit or empty string(for showing repeater page)</param>
        /// <param name="isSaving">When saving passing true or else false.</param>
        void enableDisablePanelsForAddAndEdit(string mode,
            bool isSaving)
        {

            if (mode.ToLower() == "Add".ToLower())
            {
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = true;
                showHidePanelUsingJavascript(PanelToShowHide.Form, updRepeater);
                txtPassword.Visible = true;
                txtRetypePassword.Visible = true;
                txtUserName.Enabled = true;
                lnkResetPassword.Visible = false;
                lnkBlockUnblock.Visible = false;
                lnkDelete.Visible = false;
                lblSeparatorOne.Visible = false;
                lblSeparatorTwo.Visible = false;
                enableDisableDdlUserType(true);
                showHideEditLblDomianName(false);
                showHideEditLblUserType(false);
                showHideDdlDomainList(true);
                showHideDdlUserType(true);
                if (isSaving)
                {
                    if (this.getUserTypeSelected(ddlUserType.SelectedValue) == MFICIENT_USER_TYPE.CloudUser)
                    {
                        showHideLnkActvDirGetDetails(false);
                        showHideDomainPanel(false);
                    }
                    else
                    {
                        showHideLnkActvDirGetDetails(true);
                        showHideDomainPanel(true);
                    }
                }
                else
                {
                    ddlUserType.SelectedIndex = ddlUserType.Items.IndexOf(
                        ddlUserType.Items.FindByValue(
                        Convert.ToString((int)MFICIENT_USER_TYPE.CloudUser)
                        )
                        );
                    showHideLnkActvDirGetDetails(false);
                    showHideDomainPanel(false);
                }
            }
            else if (mode.ToLower() == "Edit".ToLower())
            {
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = true;
                showHidePanelUsingJavascript(PanelToShowHide.Form, updRepeater);
                txtPassword.Visible = false;
                txtRetypePassword.Visible = false;
                txtUserName.Enabled = false;
                lnkResetPassword.Visible = true;
                lnkBlockUnblock.Visible = true;
                lnkDelete.Visible = true;
                lblSeparatorOne.Visible = true;
                lblSeparatorTwo.Visible = true;
                showHideLnkActvDirGetDetails(false);
                showHideEditLblDomianName(true);
                showHideEditLblUserType(true);
                showHideDdlDomainList(false);
                showHideDdlUserType(false);
            }
            else
            {
                clearControls();
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = true;
                showHidePanelUsingJavascript(PanelToShowHide.Repeater, updRepeater);
            }
        }

        #endregion
        #region Set Value of Hidden Field
        void setValOfHidGotUserDtls(string value)
        {
            hidGotDtlsOfUsrNm.Value = value;
        }
        void setValOfHidPasswordEntered(string value)
        {
            hidPasswordEntered.Value = value;
        }
        void setValOfHidRetypePassEntered(string value)
        {
            hidReTypePasswordEntered.Value = value;
        }
        #endregion
    }
}
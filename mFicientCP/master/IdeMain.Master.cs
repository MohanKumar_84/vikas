﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mFicientCP
{
    public partial class IdeMain : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //hdfControl.Value = "lnkDbConnector";
            }
            HiddenField hidBackButtonClientId = (HiddenField)this.MainCanvas.FindControl("hidBackButtonClientId");
            hidBackButtonClientId.Value = lnkBackButton.ClientID;
        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
           // ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"makeFormUniform()", false);
        }

        protected void lnkDbConnector_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkDbConnector";
            //System.Web.UI.HtmlControls.HtmlGenericControl DatabaseConnectorDiv = (System.Web.UI.HtmlControls.HtmlGenericControl)(MainCanvas.FindControl("DatabaseConnectorDiv"));
            //DatabaseConnectorDiv.Visible = true;
        }

        protected void lnkDbCommand_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkDbCommand";
            //System.Web.UI.HtmlControls.HtmlGenericControl DatabaseCommandDiv = (System.Web.UI.HtmlControls.HtmlGenericControl)(MainCanvas.FindControl("DatabaseCommandDiv"));
            //DatabaseCommandDiv.Visible = true;
        }

        protected void lnkWsConnector_Click(object sender, EventArgs e)
        {
            //formdesigner fr = new formdesigner();
            //fr.LeftNavActive();
            //System.Web.UI.HtmlControls.HtmlGenericControl DatabaseConnectorDiv = (System.Web.UI.HtmlControls.HtmlGenericControl)(MainCanvas.FindControl("DatabaseConnectorDiv"));
         //  var v= MainCanvas.FindControl("DatabaseConnectorDiv");
            hdfControl.Value = "lnkWsConnector";

            //System.Web.UI.HtmlControls.HtmlGenericControl WebserviceConnectorDiv = (System.Web.UI.HtmlControls.HtmlGenericControl)(MainCanvas.FindControl("WebserviceConnectorDiv"));
            //WebserviceConnectorDiv.Visible = true;
        }

        protected void lnkWsCommand_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkWsCommand";
        }

        protected void lnkMenuDefnition_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkMenuDefnition";
        }

        protected void lnkMediaFiles_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkMediaFiles";
        }

        protected void lnkMaster_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkMaster";

        }

        protected void lnkForm_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkForm";
        }

        protected void lnkReports_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkReports";
        }
        protected void lnkProcess_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkProcess";
        }

        protected void lnkProcessSettings_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkProcessSettings";
        }
        protected void lnkPublishApp_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkPublishApp";
        }
        protected void lnkApplication_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkApplication";
        }
        protected void lnkBackButton_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkBackButton";
            //lnkBackButton.PostBackUrl = "~/UiDetail.aspx";
            //updBackButton.Update();
        }
        protected void lnkOdataConn_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkOdataConn";
        }
        protected void lnkOdataCmd_Click(object sender, EventArgs e)
        {
            hdfControl.Value = "lnkOdataCmd";
        }
    }
}
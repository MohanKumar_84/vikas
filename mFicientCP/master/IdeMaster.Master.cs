﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mFicientCP.master
{
    public partial class IdeMaster : System.Web.UI.MasterPage
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty, hfsPart3 = string.Empty, hfsPart4 = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string strCompanyId = string.Empty;
        string strAdminId = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            try
            {
                if (context.Items.Contains("hfs") && context.Items.Contains("hfbid"))
                {
                    hfsValue = context.Items["hfs"].ToString();
                    hfbidValue = context.Items["hfbid"].ToString();

                    strUserId = context.Items["hfs1"].ToString();
                    strSessionId = context.Items["hfs2"].ToString();
                    strCompanyId = context.Items["hfs4"].ToString();
                    strAdminId = context.Items["hfs3"].ToString();
                }
                //if (Page.AppRelativeVirtualPath != "~/ideNewStandardApp.aspx")
                //{
                //    try
                //    {
                //        bool deviceIdExistsInCookie, cookieExists;
                //        if (String.IsNullOrEmpty(hfsValue))
                //        {
                //            Utilities.removeCurrentCookie(String.Empty);
                //            Response.Redirect(@"Default.aspx");
                //            return;
                //        }
                //        bool sessionExists = Utilities.AuthenticateUserSessionFromCache(strUserId, strAdminId
                //                                                        , strCompanyId, strSessionId
                //                                                        , hfbidValue, Cache, out deviceIdExistsInCookie, out cookieExists);
                //        /**
                //         * Session      DeviceId
                //         * n            n    //unlikely no need to do anything
                //         * n            y    // remove all the cookie and redirect
                //         * y            n   // some other user is logged in.Don't remove cookie but redirect,there is no need to remove anything as device already does not exist
                //         *                  //even if the cookie does not esits then also this condition (sessionExists && !deviceIdExistsInCookie) will do teh work.
                //         * y            y   //every things perfect
                //         * **/
                //        if (sessionExists && !deviceIdExistsInCookie)
                //        {
                //            //Response.Redirect(@"Default.aspx");
                //        }
                //        if (!sessionExists)
                //        {
                //            //Utilities.removeCurrentCookie(String.Empty);
                //            //Response.Redirect(@"Default.aspx");
                //        }
                //    }
                //    catch (IndexOutOfRangeException ex)//thrown(possibly) if hfs value is tampered with at client end
                //    {
                //        //Utilities.removeCurrentCookie(String.Empty);
                //        //Response.Redirect(@"Default.aspx");
                //    }
                //}
            }
            catch (IndexOutOfRangeException ex)//thrown(possibly) if hfs value is tampered with at client end
            {
                //Utilities.removeCurrentCookie(String.Empty);
                //Response.Redirect(@"Default.aspx");
            }
        }
        protected void lbLogout_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAdmin = true;


                string[] hfsParts = Utilities.DecryptString(hfs.Value).Split(',');
                strUserId = hfsParts[0];
                strSessionId = hfsParts[1];
                strAdminId = hfsParts[2];
                strCompanyId = hfsParts[3];



                if (hfsPart4.Trim().ToUpper() == strUserId.Trim().ToUpper()) isAdmin = true;
                else isAdmin = false;

                UserLogOut objUserLogOut = null;
                if (isAdmin) objUserLogOut = new UserLogOut(strUserId, strSessionId, strAdminId, strCompanyId, Cache, LogOutReason.NORMAL);
                else objUserLogOut = new UserLogOut(strUserId, strSessionId, strAdminId, strCompanyId, Cache, LogOutReason.NORMAL);

                if (objUserLogOut != null) objUserLogOut.Process();


                Utilities.removeCurrentCookie(String.Empty);
                Response.Redirect("~/Default.aspx");
            }
            catch
            {
            }
        }


            }
        }
  


          //  HttpContext context = HttpContext.Current;
            //try
            //{
            //    hfsValue = context.Items["hfs"].ToString();
            //    hfbidValue = context.Items["hfbid"].ToString();

            //    if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            //    strUserId = context.Items["hfs1"].ToString();
            //    strSessionId = context.Items["hfs2"].ToString();
            //    hfsPart3 = context.Items["hfs3"].ToString();
            //    hfsPart4 = context.Items["hfs4"].ToString();
            //}
            //catch (Exception ex)
            //{
            //    return;
            //}
            //try
            //{
            //    if (context.Items.Contains("hfs") && context.Items.Contains("hfbid"))
            //    {
            //        hfsValue = context.Items["hfs"].ToString();
            //        hfbidValue = context.Items["hfbid"].ToString();

            //        //hfs.Value = hfsValue;
            //        //hfbid.Value = hfbidValue;

            //        strUserId = context.Items["hfs1"].ToString();
            //        strSessionId = context.Items["hfs2"].ToString();
            //        strCompanyId = context.Items["hfs4"].ToString();
            //        strAdminId = context.Items["hfs3"].ToString();
            //    }
            //    if (Page.AppRelativeVirtualPath != "~/ideNewStandardApp.aspx")
            //    {
            //        bool deviceIdExistsInCookie, cookieExists;
            //        if (String.IsNullOrEmpty(hfsValue))
            //        {
            //            Utilities.removeCurrentCookie(String.Empty);
            //            Response.Redirect(@"Default.aspx");
            //            return;
            //        }
            //        bool sessionExists = Utilities.AuthenticateUserSessionFromCache(strUserId, strAdminId
            //                                                        , strCompanyId, strSessionId
            //                                                        , hfbidValue, Cache, out deviceIdExistsInCookie, out cookieExists);
            //        /**
            //         * Session      DeviceId
            //         * n            n    //unlikely no need to do anything
            //         * n            y    // remove all the cookie and redirect
            //         * y            n   // some other user is logged in.Don't remove cookie but redirect,there is no need to remove anything as device already does not exist
            //         *                  //even if the cookie does not esits then also this condition (sessionExists && !deviceIdExistsInCookie) will do teh work.
            //         * y            y   //every things perfect
            //         * **/
            //        //if (sessionExists && !deviceIdExistsInCookie)
            //        //{
            //        //    Response.Redirect(@"Default.aspx");
            //        //}
            //        //if (!sessionExists)
            //        //{
            //        //    Utilities.removeCurrentCookie(String.Empty);
            //        //    Response.Redirect(@"Default.aspx");
            //        //}
            //        try
            //        {
            //            if (!Page.IsPostBack) Page.ClientScript.RegisterStartupScript(this.GetType(), "onload", @"setExpiryDateOfCookie();", true);
            //            else ScriptManager.RegisterStartupScript(Page, typeof(Page), "Alert", @"setExpiryDateOfCookie();", true);
            //        }
            //        catch (Exception ex)
            //        {
            //        }
            //    }

                
            //}
            //catch (IndexOutOfRangeException ex)//thrown(possibly) if hfs value is tampered with at client end
            //{
            //    Utilities.removeCurrentCookie(String.Empty);
            //    Response.Redirect(@"Default.aspx");
            //}

            //try
            // {
            //     if (!Page.IsPostBack)
            //     {
            //         Page.ClientScript.RegisterStartupScript(this.GetType(), "onload", @"setExpiryDateOfCookie();", true);
            //     }
            //     else
            //     {
            //         ScriptManager.RegisterStartupScript(Page, typeof(Page), "Alert", @"setExpiryDateOfCookie();", true);
            //     }
            //}
            // catch (Exception ex)
            //{
            //}

       
//    }
//}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace mFicientCP
{
    public partial class Canvas : System.Web.UI.MasterPage
    {
        string pageFileName = "";
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;

        string strCompanyId = "", strAdminId = "";

        DataSet _dsNavDtls = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            try
            {
                hfsValue = context.Items["hfs"].ToString();
                hfbidValue = context.Items["hfbid"].ToString();

                if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

                strUserId = context.Items["hfs1"].ToString();
                strSessionId = context.Items["hfs2"].ToString();
                hfsPart3 = context.Items["hfs3"].ToString();
                hfsPart4 = context.Items["hfs4"].ToString();
            }
            catch (Exception ex)
            {
                return;
            }

            if (!IsPostBack)
            {
                if (context.Items.Contains("hfs") && context.Items.Contains("hfbid"))
                {
                    pageFileName = System.IO.Path.GetFileName(HttpContext.Current.Request.CurrentExecutionFilePath).Trim().ToLower();
                }
                else
                {
                    System.Web.UI.Page previousPage = Page.PreviousPage;
                    if (previousPage == null)
                    {
                        Response.End();
                        return;
                    }
                    string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                    prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);

                    if (prvPageName.ToLower() == "addform.aspx") pageFileName = System.IO.Path.GetFileName(Request.CurrentExecutionFilePath).Trim().ToLower();
                }

            }

            if (!IsPostBack)
            {
                Literal ltlUserType = (Literal)this.Master.FindControl("ltUserType");
                try
                {
                    if (!string.IsNullOrEmpty(hfsValue.Trim()) && hfsPart4.Trim().ToUpper() == strUserId.Trim().ToUpper()) ltlUserType.Text = "Admin";
                    else ltlUserType.Text = "Sub Admin";
                }
                catch (Exception ex)  //thrown(possibly) if hfs value is tampered with at client end
                {
                    Utilities.removeCurrentCookie(String.Empty);
                    Response.Redirect(@"Default.aspx");
                }
                BindRoleMenu(pageFileName);
                //used for making the sub menu item active after post back
                ((HiddenField)this.Master.FindControl("hidIsMenuNew")).Value = "true";
            }
            else
            {
                //used for making the sub menu item active after post back
                ((HiddenField)this.Master.FindControl("hidIsMenuNew")).Value = "false";
            }


            if (!String.IsNullOrEmpty(hfsValue))
            {
                Literal ltrlUserType = (Literal)this.Master.FindControl("ltUserType");
                if (ltrlUserType.Text.ToLower() == "admin".ToLower())
                {
                    strCompanyId = hfsPart3;
                    strAdminId = hfsPart4;
                }
                else
                {
                    strCompanyId = hfsPart4;
                    strAdminId = hfsPart3;
                }
            }

            try
            {
                bool deviceIdExistsInCookie, cookieExists;
                if (String.IsNullOrEmpty(hfsValue))
                {
                    Utilities.removeCurrentCookie(String.Empty);
                    Response.Redirect(@"Default.aspx");
                    return;
                }
                bool sessionExists = Utilities.AuthenticateUserSessionFromCache(strUserId, strAdminId
                                                                , strCompanyId, strSessionId
                                                                , hfbidValue, Cache, out deviceIdExistsInCookie, out cookieExists);
                /**
                 * Session      DeviceId
                 * n            n    //unlikely no need to do anything
                 * n            y    // remove all the cookie and redirect
                 * y            n   // some other user is logged in.Don't remove cookie but redirect,there is no need to remove anything as device already does not exist
                 *                  //even if the cookie does not esits then also this condition (sessionExists && !deviceIdExistsInCookie) will do teh work.
                 * y            y   //every things perfect
                 * **/
                if (sessionExists && !deviceIdExistsInCookie)
                {
                    Response.Redirect(@"Default.aspx");
                }
                if (!sessionExists)
                {
                    Utilities.removeCurrentCookie(String.Empty);
                    Response.Redirect(@"Default.aspx");
                }
            }
            catch (IndexOutOfRangeException ex)//thrown(possibly) if hfs value is tampered with at client end
            {
                Utilities.removeCurrentCookie(String.Empty);
                Response.Redirect(@"Default.aspx");
            }

        }

        protected void BindRoleMenu(string _pageFileName)
        {
            string strRoles = Utilities.SelectForm(strUserId);
            if (strRoles.Trim() == "") strRoles = "A";

            HiddenField hidField = (HiddenField)this.Master.FindControl("hidRoleArray");
            hidField.Value = strRoles;
            bool blnGotDtlsFromCache;
            DataSet dsNavDtls = Utilities.GetMainMenuList(strRoles, out blnGotDtlsFromCache);
            if (!blnGotDtlsFromCache)
            {
                addNavigationDtlsByRoleInCache(strRoles == "A" ? MficientConstants.CACHE_ADMIN_ROLE : MficientConstants.CACHE_SUB_ADMIN_ROLE,
                    dsNavDtls);
            }
            if (dsNavDtls != null)
            {
                _dsNavDtls = dsNavDtls;
                DataTable dt = dsNavDtls.Tables[0];
                rptMenuCats.DataSource = this.getMainMenuListFromNavDtlDs(dt);
                rptMenuCats.DataBind();
            }
        }

        protected void rptMenuCats_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            RepeaterItem menuItem = e.Item;
            if (menuItem.ItemType != ListItemType.Item && menuItem.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dtItemNode = (DataRowView)menuItem.DataItem;
            LinkButton lb = (LinkButton)menuItem.FindControl("lnk1");
            string linkPage = dtItemNode["PAGE"].ToString().Trim();
            if (!String.IsNullOrEmpty(linkPage))
            {
                lb.PostBackUrl = "~/" + dtItemNode["PAGE"].ToString();
                lb.OnClientClick = "isCookieCleanUpRequired();showWaitModal();";

                Repeater rptItem = (Repeater)menuItem.FindControl("rptMenuItem");
                rptItem.Visible = false;
                if (pageFileName.ToLower() == linkPage.ToLower())
                {
                    lb.CssClass = "active";
                    if (String.IsNullOrEmpty(dtItemNode["HEADER"].ToString().Trim()))
                    {
                        ltHead.Visible = false;
                        ltlHelp.Visible = false;
                    }
                    else
                    {
                        ltHead.Text = dtItemNode["HEADER"].ToString();

                        if (String.IsNullOrEmpty(dtItemNode["HELP_TEXT"].ToString().Trim()))
                            ltlHelp.Visible = false;
                        else
                            ltlHelp.Text = dtItemNode["HELP_TEXT"].ToString();
                    }
                }
            }
            else
            {
                lb.OnClientClick = "return false;";
               // DataSet ds = Utilities.GetSubMenuList(pageFileName, dtItemNode["id"].ToString());
                DataSet ds = getSubMenuListFromNavDetailsDs(_dsNavDtls.Tables[0], Convert.ToInt32(dtItemNode["id"]), pageFileName);
                if (ds != null && ds.Tables.Count > 0)
                {
                    Repeater rptItem = (Repeater)menuItem.FindControl("rptMenuItem");
                    rptItem.DataSource = ds.Tables[0];
                    rptItem.DataBind();
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        lb.CssClass = "active";
                    }
                }
            }
        }

        protected void rptMenuItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            RepeaterItem menuItem = e.Item;
            if (menuItem.ItemType != ListItemType.Item && menuItem.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dtItemNode = (DataRowView)menuItem.DataItem;
            if (!String.IsNullOrEmpty(dtItemNode["PAGE"].ToString().Trim()))
            {
                LinkButton lb = (LinkButton)menuItem.FindControl("lnk1");
                lb.PostBackUrl = "~/" + dtItemNode["PAGE"].ToString();
                lb.OnClientClick = "setHidSubMenuClicked(this);isCookieCleanUpRequired();showWaitModal();";
                if (pageFileName.ToLower() == dtItemNode["PAGE"].ToString().ToLower())
                {
                    lb.CssClass = "active";
                    if (String.IsNullOrEmpty(dtItemNode["HEADER"].ToString().Trim()))
                    {
                        ltHead.Visible = false;
                        ltlHelp.Visible = false;
                    }
                    else
                    {
                        ltHead.Text = dtItemNode["HEADER"].ToString();

                        if (String.IsNullOrEmpty(dtItemNode["HELP_TEXT"].ToString().Trim()))
                            ltlHelp.Visible = false;
                        else
                            ltlHelp.Text = dtItemNode["HELP_TEXT"].ToString();
                    }
                }
            }
        }

        void addNavigationDtlsByRoleInCache(string role, DataSet navDtlDs)
        {
            CacheManager.Insert<DataSet>(CacheManager.GetKey(CacheManager.CacheType.mFicientNavigation,
                                          String.Empty, role, String.Empty, String.Empty, String.Empty, String.Empty),
                                          navDtlDs,
                                          DateTime.UtcNow,
                                          TimeSpan.FromSeconds(MficientConstants.SESSION_VALIDITY_SECONDS));
        }
        DataSet getSubMenuListFromNavDetailsDs(DataTable navDtlDtbl,int parentId,string pageName)
        {
            DataSet dsSubMenuDtl = new DataSet();
            DataTable dtblToAddToDS= new DataTable();
            string filter = String.Format("PARENT_ID = {0}", parentId);
            DataView dtView = new DataView(navDtlDtbl,
                                    filter,
                                    "ID",
                                    DataViewRowState.CurrentRows);
            
            if (dtView != null)
            {
                dtblToAddToDS = dtView.ToTable();
                dtblToAddToDS.TableName = "Table1";
                dsSubMenuDtl.Tables.Add(dtblToAddToDS);
            }
            else return null;

            filter = String.Format("PAGE= '{0}' AND PARENT_ID = {1}",pageName, parentId);
            dtView = new DataView(navDtlDtbl,
                                    filter,
                                    "ID",
                                    DataViewRowState.CurrentRows);
            
            if (dtView != null)
            {
                dtblToAddToDS = dtView.ToTable();
                dtblToAddToDS.TableName = "Table2";
                dsSubMenuDtl.Tables.Add(dtblToAddToDS);
            }

            return dsSubMenuDtl;

        }
        DataTable getMainMenuListFromNavDtlDs(DataTable navDtlDtbl)
        {
            DataTable dtblMainMenu = new DataTable();
            string filter = String.Format("PARENT_ID = {0}", 0);
            DataView dtView = new DataView(navDtlDtbl,
                                    filter,
                                    "ID",
                                    DataViewRowState.CurrentRows);

            if (dtView != null)
            {
                return dtView.ToTable();
                //DataTable dt = dtView.ToTable();
                //dt.Rows.Add();
                //int index = dt.Rows.Count - 1;
                //dt.Rows[index]["ROLE"] = "A";
                //dt.Rows[index]["LINK"] = "Trace Device";
                //dt.Rows[index]["PAGE_TYPE"] = "1";
                //dt.Rows[index]["PAGE"] = "traceDevice.aspx";
                //dt.Rows[index]["ID"] = "37";
                //dt.Rows[index]["PARENT_ID"] = "0";
                //dt.Rows[index]["CLASS"] = "i_settings";
                //dt.Rows[index]["HEADER"] = "Trace Device Setting";
                //dt.Rows[index]["HELP_TEXT"] = "";
                //return dt;
            }
            else return null;
        }


    }

}
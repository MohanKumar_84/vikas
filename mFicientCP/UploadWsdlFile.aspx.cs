﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using System.Xml;

namespace mFicientCP
{
    public partial class UploadWsdlFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                hdf.Value = Convert.ToString(Request.QueryString["id"]);
                hdfCid.Value = Convert.ToString(Request.QueryString["cid"]);
            }
        }
        protected void btnImportFile_Click(object sender, EventArgs e)
        {
            lblResult.Text = "";
            if (!fupd.HasFile)
            {
                return;
            }
            
            if (fupd.FileName.Split('.')[1] == "xml" || fupd.FileName.Split('.')[1] == "txt")
            {
                //File size grater tha 500kb can not be uploaded.
                if (fupd.PostedFile.ContentLength > (500 * 1024))
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.CommonErrorMessages(9);", true);
                    //lblResult.Text = "File can not be uploaded.File size greater than 500KB."; 
                    return;
                }
                string strContent = "";
                using (StreamReader stRead = new StreamReader(fupd.PostedFile.InputStream))
                {
                    string  line1;
                    while ((line1 = stRead.ReadLine()) != null)
                    {
                            strContent += line1;
                        //    strContent += "\n" + line1;
                    }
                }
                Guid gid;
                gid= Guid.NewGuid();
                WsdlFileContent objWsdlFileContent = new WsdlFileContent(true, gid.ToString(), strContent, "wsdl",hdf.Value,hdfCid.Value);
                objWsdlFileContent.Process();

               
                //System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                //xmlDoc.LoadXml(strContent);

                //foreach (XmlNode node in xmlDoc)
                //if (node.NodeType == XmlNodeType.XmlDeclaration)
                //{
                //    xmlDoc.RemoveChild(node);
                //    //doc.RemoveChild(doc.FirstChild);
                //}

                //string strXML = "";
                //if (xmlDoc != null)
                //{
                //    StringWriter writer = new StringWriter();
                //    xmlDoc.Save(writer);
                //    strXML = writer.ToString();
                //}
                //xmlDoc = null;

                if (objWsdlFileContent.StatusCode != 0)
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.CommonErrorMessages(8);", true);
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.SubProcBoxUldWsdlCfm(false);parent.ChangeWsdlFileId('" + gid + "');", true);
                }
                                
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.CommonErrorMessages(7);", true);
                       // lblResult.Text = "Only xml or txt file format is allowed.";
            }
        }
    }
}
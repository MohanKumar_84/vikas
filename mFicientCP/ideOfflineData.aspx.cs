﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using System.Collections;
using Newtonsoft.Json.Linq;

namespace mFicientCP
{
    public partial class ideOfflineData : System.Web.UI.Page
    {
        string SubAdminid = "", CompanyId = "", Sessionid = "", Adminid = "";

        enum POST_BACK_BY_HTML_PROCESS
        {
            SaveNewOfflineDt = 1,
            UpdateOfflineDt = 2,
            DeleteOfflineDt = 3,
            SaveAndOpenNewTbl = 4,
            SaveAndOpenSameTbl = 5
        }

        string strhfs, strhfbid, strPageMode = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            MasterPage mainMaster = Page.Master;
            if (Page.IsPostBack)
            {
                strhfs = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                //System.Web.UI.Page previousPage = Page.PreviousPage;
                if (Page.PreviousPage == null)
                {
                    Response.End();
                    return;
                }
                strPageMode = ((HiddenField)Page.PreviousPage.Master.FindControl("hidAppNewOrEdit")).Value;
                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                strhfs = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;

                ((HiddenField)mainMaster.FindControl("hfs")).Value = strhfs;
                ((HiddenField)mainMaster.FindControl("hfbid")).Value = strhfbid;

                bindDataCacheDropdowns();
            }

            if (string.IsNullOrEmpty(strhfs) || string.IsNullOrEmpty(strhfbid)) return;

            context.Items["hfs"] = strhfs;
            context.Items["hfbid"] = strhfbid;
            string str = Utilities.DecryptString(strhfs);
            SubAdminid = str.Split(',')[0];
            CompanyId = str.Split(',')[3].ToLower();
            Sessionid = str.Split(',')[1];
            Adminid = str.Split(',')[2];

            context.Items["hfs1"] = SubAdminid;
            context.Items["hfs2"] = Sessionid;
            context.Items["hfs3"] = Adminid;
            context.Items["hfs4"] = CompanyId;

            try
            {
                if (!Page.IsPostBack || (Page.IsPostBack && Utilities.getPostBackControlName(this.Page).ToLower() == "lnkofflinedb"))
                {
                    try
                    {
                        setOfflineRptDataDtl();
                        setAllDbCmdHidField(getJsonOfAllDbCommands());
                        setAllWsCmdHidField(getJsonOfAllWsCommands());
                        setAllOdataHidField(getJsonOfAllOdataCommands());
                        setAllDbConnsHidField(getJsonOfAllDbConnections());
                        setAllWsConnsHidField(getJsonOfAllWsConnections());
                        setAllOdataConnsHidField(getJsonOfAllOdataConnections());
                        Utilities.runPageStartUpScript(this.Page, "bindOfflineDatatables", @"bindOfflineDtRpt(); makeControlsUniform(); MFODB.processAddNewTableInUI();");
                    }
                    catch (MficientException ex)
                    {
                        Utilities.showMessage(ex.Message, this.Page, "Page Load Error Meaage", DIALOG_TYPE.Error);
                    }
                }
            }
            catch (Exception)
            {
                Utilities.showMessage("Error loading Offline data page.Please try again later.", this.Page, "Page Load Error Message", DIALOG_TYPE.Error);
            }
        }
        protected void btnPostbackByHtmlCntrl_Click(object sender, EventArgs e)
        {
            try
            {
                UpdatePanel thisUpdPanel = updPostbackByHtmlCntrl;
                if (!String.IsNullOrEmpty(hidDtlsForPostBackByHtmlCntrl.Value))
                {
                    PostbackByHtmlControlData objPostbackData =
                                Utilities.DeserialiseJson<PostbackByHtmlControlData>(hidDtlsForPostBackByHtmlCntrl.Value);
                    if (objPostbackData != null)
                    {
                        POST_BACK_BY_HTML_PROCESS eProcFor = (POST_BACK_BY_HTML_PROCESS)Enum.Parse(typeof(POST_BACK_BY_HTML_PROCESS),
                                    objPostbackData.processFor);
                        string strTableJson = "";
                        string strSyncJson = "";                       
                        OfflineTables objOfflineTable = null;
                        MFEOfflineDataTable objMfeOfflineDt = null;
                        GetOfflineDataTableDtl objGetOfflineDtbl = null;
                        switch (eProcFor)
                        {
                            case POST_BACK_BY_HTML_PROCESS.SaveNewOfflineDt:

                                strTableJson = hidFinalTableJsonForSaving.Value;
                                strSyncJson = hidFinalSyncJsonForSavning.Value;

                                if (!String.IsNullOrEmpty(strTableJson))
                                {
                                    JObject jobj = JObject.Parse(strTableJson);
                                    JObject jobjTbl = (JObject)jobj["tbl"];
                                    objOfflineTable = new OfflineTables(this.CompanyId, this.SubAdminid);
                                    objMfeOfflineDt = new MFEOfflineDataTable();
                                    objMfeOfflineDt.CompanyId = this.CompanyId;
                                    objMfeOfflineDt.TableName = Convert.ToString(jobjTbl["nm"]);
                                    objMfeOfflineDt.TableType = Convert.ToInt32(jobjTbl["typ"]);
                                    objMfeOfflineDt.TableJson = strTableJson;
                                    objMfeOfflineDt.SyncJson = strSyncJson;
                                    objMfeOfflineDt.SubadminId = this.SubAdminid;
                                    
                                    objOfflineTable.InsertRecordInOfflineTables(objMfeOfflineDt);
                                    if (objOfflineTable.StatusCode == 0)
                                    {
                                        Utilities.showMessage("Offline table saved successfully.", updPostbackByHtmlCntrl, DIALOG_TYPE.Info);
                                        setOfflineRptDataDtl();
                                        hidFinalTableJsonForSaving.Value = "";
                                        hidFinalSyncJsonForSavning.Value = "";
                                        updHiddenFields.Update();
                                        Utilities.runPostBackScript(@"bindOfflineDtRpt();MFODB.processAddNewTableInUI();",
                                            updPostbackByHtmlCntrl, "ResetDetailsAgfterSave");
                                    }
                                    else
                                    {
                                        throw new MficientException(objOfflineTable.StatusDescription);
                                    }
                                }
                                else
                                {
                                    throw new MficientException("No table data available for saving.Please try again.");
                                }
                                break;
                            case POST_BACK_BY_HTML_PROCESS.UpdateOfflineDt:
                                updateDatatable();
                                Utilities.showMessage("Offline table updated successfully.", updPostbackByHtmlCntrl, DIALOG_TYPE.Info);
                                setOfflineRptDataDtl();
                                hidFinalTableJsonForSaving.Value = "";
                                hidFinalSyncJsonForSavning.Value = "";
                                updHiddenFields.Update();
                                Utilities.runPostBackScript(@"bindOfflineDtRpt();MFODB.processAddNewTableInUI();",
                                    updPostbackByHtmlCntrl, "ResetDetailsAgfterUpdate");
                                break;
                            case POST_BACK_BY_HTML_PROCESS.DeleteOfflineDt:
                                string tableId = objPostbackData.data[0];
                                objOfflineTable = new OfflineTables(this.CompanyId, this.SubAdminid);
                                objOfflineTable.DeleteOfflineTables(tableId, this.CompanyId);
                                if (objOfflineTable.StatusCode == 0)
                                {
                                    setOfflineRptDataDtl();
                                    updHiddenFields.Update();
                                    Utilities.showMessage("Offline table deleted successfully.", updPostbackByHtmlCntrl, DIALOG_TYPE.Info);
                                    Utilities.runPostBackScript(@"bindOfflineDtRpt();
                                            MFODB.processAddNewTableInUI();",
                                           updPostbackByHtmlCntrl, "ResetDetailsAgfterUpdate");
                                }
                                else
                                {
                                    throw new MficientException(objOfflineTable.StatusDescription);
                                }
                                break;
                            case POST_BACK_BY_HTML_PROCESS.SaveAndOpenNewTbl:
                            case POST_BACK_BY_HTML_PROCESS.SaveAndOpenSameTbl:
                                bool isUpdateSuccessfull = false;
                                string strMessageForUI = "";
                                try
                                {
                                    updateDatatable();
                                    isUpdateSuccessfull = true;
                                    strMessageForUI = "Offline table was updated successfully.<br/>";
                                    hidFinalTableJsonForSaving.Value = "";
                                    hidFinalSyncJsonForSavning.Value = "";
                                    objGetOfflineDtbl = new GetOfflineDataTableDtl(this.CompanyId);
                                    JObject joTblDtlsForSaveAndOpen = JObject.Parse(hidOldNewTblDtlForSaveAndOpenNew.Value);
                                    objMfeOfflineDt = objGetOfflineDtbl.ProcessGetOfflineDTDtlByTblId(Convert.ToString(joTblDtlsForSaveAndOpen["newTableId"]));
                                    if (objGetOfflineDtbl.StatusCode != 0)
                                    {
                                        throw new MficientException(objGetOfflineDtbl.StatusDescription);
                                    }
                                    hidTblDataToOpenAfterSaveAndOpenNew.Value = Utilities.SerializeJson<MFEOfflineDataTable>(objMfeOfflineDt);
                                    setOfflineRptDataDtl();
                                    updHiddenFields.Update();
                                    Utilities.showMessage(strMessageForUI, updPostbackByHtmlCntrl, DIALOG_TYPE.Info);
                                    Utilities.runPostBackScript(@"bindOfflineDtRpt();MFODB.doAfterSaveAndOpenNewTblSuccess();",
                                            updPostbackByHtmlCntrl, "Open New Datatable After Updating Current Datatable");
                                }
                                catch (MficientException ex)
                                {
                                    if (isUpdateSuccessfull == true)
                                    {
                                        strMessageForUI = "Offline Table changes were updated successfully.<br/>The Offline Table selected for editing was not found.";
                                        Utilities.runPostBackScript(@"bindOfflineDtRpt();MFODB.doAfterSaveAndOpenNewTblOnNewTblFetchFailure();",
                                                updPostbackByHtmlCntrl, "Do On Failure Of Fetching Table Details");
                                    }
                                    else
                                    {
                                        strMessageForUI = ex.Message;
                                        Utilities.runPostBackScript(@"MFODB.doAfterSaveAndOpenNewTblOnUpdateFailure();",
                                                updPostbackByHtmlCntrl, "Do On Failure Of Updating Table Record");
                                    }
                                    Utilities.showMessage(strMessageForUI, updPostbackByHtmlCntrl, DIALOG_TYPE.Error);
                                }
                                catch
                                {
                                    if (isUpdateSuccessfull == true)
                                    {
                                        strMessageForUI = "Offline Table changes were updated successfully.<br/>There was an error while opening the selected Offline Table.Please try again.";
                                        Utilities.runPostBackScript(@"bindOfflineDtRpt();MFODB.doAfterSaveAndOpenNewTblOnNewTblFetchFailure();",
                                                updPostbackByHtmlCntrl, "Do On Failure Of Fetching Table Details");
                                    }
                                    else
                                    {
                                        strMessageForUI = "Internal server error.";
                                        Utilities.runPostBackScript(@"MFODB.doAfterSaveAndOpenNewTblOnUpdateFailure();",
                                                updPostbackByHtmlCntrl, "Do On Failure Of Updating Table Record.");
                                    }
                                    Utilities.showMessage(strMessageForUI, updPostbackByHtmlCntrl, DIALOG_TYPE.Error);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (MficientException mfEx)
            {
                Utilities.showMessage(mfEx.Message, updPostbackByHtmlCntrl, DIALOG_TYPE.Error);
            }
            catch
            {
                Utilities.showMessage("Internal server error.", updPostbackByHtmlCntrl, DIALOG_TYPE.Error);
            }
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "setExpiryDateOfCookie();", true);
        }
        
        #region Set Values in Hidden Fields
        void setAllOdataHidField(string value)
        {
            hidAllOdataCommands.Value = value;
        }
        void setAllDbCmdHidField(string value)
        {
            hidAllDbCommands.Value = value;
        }
        void setAllWsCmdHidField(string value)
        {
            hidAllWsCommands.Value = value;
        }

        void setAllDbConnsHidField(string value)
        {
            hidAllDbConnections.Value = value;
        }
        void setAllWsConnsHidField(string value)
        {
            hidAllWsConnections.Value = value;
        }
        void setAllOdataConnsHidField(string value)
        {
            hidAllOdataConnections.Value = value;
        }
        void setOfflineDtListHidField(string value)
        {
            hidOfflineDtList.Value = value;
        }
        #endregion
        #region Private Members
        void updateDatatable()
        {
            string strTableJson = hidFinalTableJsonForSaving.Value;
            string strSyncJson = hidFinalSyncJsonForSavning.Value;
            OfflineDataTableJson objTblJson = null;
            OfflineDataTableSyncCont objTblSyncDetails = null;
            OfflineTables objOfflineTable = null;
            if (!String.IsNullOrEmpty(strTableJson))
            {
                objTblJson = Utilities.DeserialiseJson<OfflineDataTableJson>(strTableJson);
            }
            if (!String.IsNullOrEmpty(strSyncJson))
            {
                objTblSyncDetails = Utilities.DeserialiseJson<OfflineDataTableSyncCont>(strSyncJson);
            }
            if (objTblJson != null && objTblSyncDetails != null)
            {
                objOfflineTable = new OfflineTables(this.CompanyId, this.SubAdminid);
                MFEOfflineDataTable objMfeOfflineDt = new MFEOfflineDataTable();
                objMfeOfflineDt.TableId = objTblJson.tbl.id;
                objMfeOfflineDt.CompanyId = this.CompanyId;
                objMfeOfflineDt.TableName = objTblJson.tbl.nm;
                objMfeOfflineDt.TableType = Convert.ToInt32(objTblJson.tbl.typ);
                objMfeOfflineDt.TableJson = strTableJson;
                objMfeOfflineDt.SyncJson = strSyncJson;
                objMfeOfflineDt.SubadminId = this.SubAdminid;

                objOfflineTable.UpdateRecordInOfflineTables(objMfeOfflineDt);
                if (objOfflineTable.StatusCode == 0)
                {
                }
                else
                {
                    throw new MficientException(objOfflineTable.StatusDescription);
                }
            }

        }       

        string getJsonOfAllDbCommands()
        {
            string strJson = "";
            GetDbCommand objDbCommand = new GetDbCommand(true, false, this.SubAdminid, "", "", this.CompanyId);
            DataTable dtblDbCmds = objDbCommand.ResultTable;
            strJson = Utilities.SerializeJson<List<IdeDbCmdsForJson>>(objDbCommand.getCommondDetail());
            return strJson;
        }
        string getJsonOfAllWsCommands()
        {
            GetWsCommand objWsCommand = new GetWsCommand(true, false, this.SubAdminid, "", "", this.CompanyId);
            objWsCommand.Process();            
            return Utilities.SerializeJson<List<IdeWsCmdsForJson>>(objWsCommand.GetWsCommandDetail());
        }
        string getJsonOfAllOdataCommands()
        {
            GetOdataCommand objOdataCmds = new GetOdataCommand(true, false, this.SubAdminid, "", "", this.CompanyId);
            return Utilities.SerializeJson<List<IdeOdataCmdsForJson>>(objOdataCmds.getOdataCommandDetail()); 
        }

        string getJsonOfAllDbConnections()
        {
            string strJson = "";
            GetDatabaseConnection objGetDBConn = new GetDatabaseConnection(true, "", "", this.CompanyId);
            if (objGetDBConn.StatusCode == 0)
            {
                DataTable dtblDbConns = objGetDBConn.ResultTable;
                if (dtblDbConns != null)
                {
                    List<MFEDatabaseConnection> lstDbConns = new List<MFEDatabaseConnection>();
                    foreach (DataRow row in dtblDbConns.Rows)
                    {
                        MFEDatabaseConnection objMFEDbConn = new MFEDatabaseConnection();
                        objMFEDbConn.ConnectorId = Convert.ToString(row["DB_CONNECTOR_ID"]);
                        objMFEDbConn.ConnectionName = Convert.ToString(row["CONNECTION_NAME"]);
                        lstDbConns.Add(objMFEDbConn);
                    }
                    strJson = Utilities.SerializeJson<List<MFEDatabaseConnection>>(lstDbConns);
                    //string test = getInputOutputParamJsonForCommand(COMMAND_TYPE.DatabaseCommand, dtblDbCmds);
                }
            }
            return strJson;
        }
        string getJsonOfAllWsConnections()
        {
            string strJson = "";
            GetWebserviceConnection objGetWSConn = new GetWebserviceConnection(true, this.SubAdminid, "", "", this.CompanyId);
            objGetWSConn.Process1();
            if (objGetWSConn.StatusCode == 0)
            {
                DataTable dtblWsConns = objGetWSConn.ResultTable;
                if (dtblWsConns != null)
                {
                    List<MFEWebserviceConnection> lstWsConn = new List<MFEWebserviceConnection>();
                    foreach (DataRow row in dtblWsConns.Rows)
                    {
                        MFEWebserviceConnection objMFEWsConn = new MFEWebserviceConnection();
                        objMFEWsConn.ConnectorId = Convert.ToString(row["WS_CONNECTOR_ID"]);
                        objMFEWsConn.ConnectionName = Convert.ToString(row["CONNECTION_NAME"]);
                        objMFEWsConn.WebserviceType = Convert.ToString(row["WEBSERVICE_TYPE"]);
                        lstWsConn.Add(objMFEWsConn);
                    }
                    strJson = Utilities.SerializeJson<List<MFEWebserviceConnection>>(lstWsConn);
                }
            }
            return strJson;
        }
        string getJsonOfAllOdataConnections()
        {
            string strJson = "";
            GetODataSrviceConnector objGetOdataConns = new GetODataSrviceConnector(true, this.SubAdminid, "", "", this.CompanyId, Int32.MinValue);
            if (objGetOdataConns.StatusCode == 0)
            {
                DataTable dtblOdataConns = objGetOdataConns.ResultTable;
                if (dtblOdataConns != null)
                {
                    List<MFEOdataConnection> lstOdataConn = new List<MFEOdataConnection>();
                    foreach (DataRow row in dtblOdataConns.Rows)
                    {
                        MFEOdataConnection objMFEOdataConn = new MFEOdataConnection();
                        objMFEOdataConn.ConnectorId = Convert.ToString(row["ODATA_CONNECTOR_ID"]);
                        objMFEOdataConn.ConnectionName = Convert.ToString(row["CONNECTION_NAME"]);
                        lstOdataConn.Add(objMFEOdataConn);
                    }
                    strJson = Utilities.SerializeJson<List<MFEOdataConnection>>(lstOdataConn);
                }
            }
            return strJson;
        }
        /// <summary>
        /// Bind the Offline main view Repeater
        /// </summary>
        void setOfflineRptDataDtl()
        {
            List<MFEOfflineDataTable> objOflnDts = new List<MFEOfflineDataTable>();
            GetOfflineDataTableDtl getOfflineData = new GetOfflineDataTableDtl(this.CompanyId);
            getOfflineData.Process();
            if (getOfflineData.StatusCode == 0)
            {
                objOflnDts = getOfflineData.offlineDatatables;
                if (objOflnDts == null) objOflnDts = new List<MFEOfflineDataTable>();
                setOfflineDtListHidField(Utilities.SerializeJson<List<MFEOfflineDataTable>>(objOflnDts));
            }
            else
            {
                throw new MficientException("Internal server error.");
            }
        }
        private void bindDataCacheDropdowns()
        {
            ddl_AbsoluteDayHour.Items.Clear();
            ddl_AbsoluteDayMin.Items.Clear();
            ddl_AbsoluteDate.Items.Clear();
            ddl_AbsoluteYear_Day.Items.Clear();
            ddl_RelativeMinute.Items.Clear();
            ddl_RelativeHour.Items.Clear();
            ddl_RelativeDay.Items.Clear();
            ddl_RelativeWeek.Items.Clear();
            ddl_RelativeMonth.Items.Clear();

            for (int abHr = 0; abHr <= 23; abHr++)
            {
                ddl_AbsoluteDayHour.Items.Add(new ListItem(abHr.ToString(), abHr.ToString()));
            }

            for (int abMin = 0; abMin <= 59; abMin++)
            {
                ddl_AbsoluteDayMin.Items.Add(new ListItem(abMin.ToString(), abMin.ToString()));
            }

            for (int abDate = 1; abDate <= 31; abDate++)
            {
                ddl_AbsoluteDate.Items.Add(new ListItem(abDate.ToString(), abDate.ToString()));
            }

            for (int abYrDay = 1; abYrDay <= 30; abYrDay++)
            {
                ddl_AbsoluteYear_Day.Items.Add(new ListItem(abYrDay.ToString(), abYrDay.ToString()));
            }

            for (int relMin = 0; relMin <= 59; relMin++)
            {
                ddl_RelativeMinute.Items.Add(new ListItem(relMin.ToString(), relMin.ToString()));
            }

            for (int relHr = 0; relHr <= 23; relHr++)
            {
                ddl_RelativeHour.Items.Add(new ListItem(relHr.ToString(), relHr.ToString()));
            }

            for (int relDay = 0; relDay <= 31; relDay++)
            {
                ddl_RelativeDay.Items.Add(new ListItem(relDay.ToString(), relDay.ToString()));
            }

            for (int relWeek = 0; relWeek <= 52; relWeek++)
            {
                ddl_RelativeWeek.Items.Add(new ListItem(relWeek.ToString(), relWeek.ToString()));
            }

            for (int relMnth = 0; relMnth <= 12; relMnth++)
            {
                ddl_RelativeMonth.Items.Add(new ListItem(relMnth.ToString(), relMnth.ToString()));
            }
        }
        //MOHAN19/3/2014
        //MOHAN19/3/2014
        #endregion
    }
}
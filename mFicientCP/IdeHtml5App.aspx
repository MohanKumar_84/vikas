﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/IdeMaster.Master" AutoEventWireup="true"
    CodeBehind="IdeHtml5App.aspx.cs" Inherits="mFicientCP.IdeHtml5App" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/IdeHtml5App.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="jsTree/themes/default/style.min.css" />
    <script src="Scripts/IdeHtml5App.js" type="text/javascript"></script>
    <script src="ace/ace.js" type="text/javascript" charset="utf-8"></script>
    <script src="jsTree/jstree.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var selectedFile;
            var box;
            box = document.getElementById("dragandDrop");
            box.addEventListener("dragenter", OnDragEnter, false);
            box.addEventListener("dragover", OnDragOver, false);
            box.addEventListener("drop", OnDrop, false);

            $('#filesDiv').removeClass('ui-tabs-panel');
            $('#propertiesDiv').removeClass('ui-tabs-panel');

            $('#filePropDiv').resizable({
                handles: "e",
                maxWidth: 300,
                minWidth: 100,
                stop: function (event, ui) { alert('testing resinzing'); }
            });
        });

        function OnDragEnter(e) {
            e.stopPropagation();
            e.preventDefault();
        }

        function OnDragOver(e) {
            e.stopPropagation();
            e.preventDefault();
        }

        function OnDrop(e) {
            e.stopPropagation();
            e.preventDefault();
            selectedFile = e.dataTransfer.files[0];
            var reader = new FileReader();
            handleFiles(selectedFile);
            //            reader.onload = (function (file) {
            //                return function (event) {
            //                    $('#<%=hdFileName.ClientID %>').val(file.name);
            //                    $('#<%=hdFileBytes.ClientID %>').val(event.target.result.split(',')[1]);
            //                 };
            //            })(selectedFile);
            //            reader.readAsDataURL(selectedFile);

            $("#txtFilePath").on('change', function () {
                isChildClicked = false;
            });
        }
        function handleFiles(file) {
            var imageType = /image.*/;
            var reader = new FileReader();
            reader.onload = (function (file) {
                return function (e) {
                    $('#<%=hdFileName.ClientID %>').val(file.name);
                    $('#<%=hdFileBytes.ClientID %>').val(e.target.result.split(',')[1]);
                };
            })(file);
            reader.onerror = function () {
                alert('An error encountered while reading the file.Please try again.');
            }
            reader.onloadend = function () {
                if (reader.readyState === 2) {
                    $('#<%=btnPostbackByHtmlCntrl.ClientID %>').click();
                }
            }
            reader.readAsDataURL(file);
        }

        function formAppTree() {
            var treeData = $('[id$="hidAppTreeView"]').val();
            if (treeData) {
                treeData = $.parseJSON(treeData);
            }
            $('#jstree_demo_div').on("changed.jstree", function (e, data) {
                console.log(data.selected);
            }).jstree({ 'core': {
                'data': treeData
            }
            });
        }
        function testSetAceEditorValue() {
            editor.setValue($('[id$="hidSelectedFileContent"]').val());
            document.getElementById('editor').style.fontSize = '12px';
        }
    </script>
    <style type="text/css" media="screen">
        #editor
        {
            width: 100%;
            height: 615px;
        }
        .ctn
        {
            background-color: #F00;
            margin-left: auto;
            margin-right: auto;
            position: relative;
            overflow: hidden;
        }
        .ctn div
        {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }
        .ctn div img
        {
            width: 150px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainCanvas" runat="server">
    <div id="pageContent">
        <div id="filePropDiv">
            <div id="divTab" class="tab" style="overflow: hidden; border: 0;">
                <ul id="tabList" style="overflow: hidden; border: 0;">
                    <li id="lstTabFiles"><a id="lnktabCategory" href="#filesDiv" onclick="storeSelectedTabIndex(TabEnum.FILES);">
                        Files</a></li>
                    <li id="lstTabProperties"><a id="lnkDataCommandSetting" href="#propertiesDiv" onclick="storeSelectedTabIndex(TabEnum.PROPERTIES);">
                        Properties</a></li>
                </ul>
                <asp:UpdatePanel ID="updFilesDiv" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="ui-tabs-hide" id="filesDiv">
                            <%--<ul id="lstAppFiles" style="width: 100%; height: 520px;">
                            </ul>--%>
                        </div>
                        <div class="ui-tabs-hide" id="propertiesDiv">
                            <table style="width: 100%;">
                                <tr>
                                    <td>
                                        Name
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAppName" runat="server" TextMode="SingleLine" Width="150px" CssClass="propTextbox"
                                            Height="30px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 80px;">
                                        Description
                                    </td>
                                    <td style="height: 80px;">
                                        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Width="150px"
                                            CssClass="propTextbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        HomeView
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtHomeView" runat="server" TextMode="SingleLine" Width="150px"
                                            CssClass="propTextbox" Height="30px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Icon
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Created By
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCreateBy" runat="server" TextMode="SingleLine" Width="150px"
                                            CssClass="propTextbox" Height="30px" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Last Modified
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLastModifiedDate" runat="server" TextMode="SingleLine" Width="150px"
                                            CssClass="propTextbox" Height="30px" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Modified By
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtModifiedBy" runat="server" TextMode="SingleLine" Width="150px"
                                            CssClass="propTextbox" Height="30px" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Show on
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlShowOn" runat="server" Width="140px">
                                            <asp:ListItem Value="1" Text="Mobile"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Tablet"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div style="height: 48px; border: 1px solid #bbbbbb;">
                <asp:UpdatePanel ID="updAddFile" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Button ID="btnAddNewFile" runat="server" Text="Add File(s)" CssClass="button"
                            OnClientClick="showHtml5AddNewFileDiv(true);return false;" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="canvasDiv">
            <div style="height: 31px; border-bottom: 1px solid #bbbbbb;">
                <div style="float: left; width: 40%; line-height: 31px; vertical-align: middle; text-align: center;">
                    <div style="float: left; width: 20%; font-size: small;">
                        File Path:</div>
                    <div style="float: left; width: 80%;">
                        <asp:TextBox ID="txtFilePath" runat="server" CssClass="txtFilePath"></asp:TextBox></div>
                </div>
                <div id="fileEdit" style="float: right; width: 40%; line-height: 31px; vertical-align: middle;
                    text-align: center; display: none;">
                    <asp:UpdatePanel ID="updEditPanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ul style="list-style: none; width: 90%;">
                                <li style="float: right; width: 20%; margin-top: -10px; line-height: 31px; font-size: small;">
                                    <asp:LinkButton ID="lnkDelete" runat="server">Delete</asp:LinkButton></li>
                                <li style="float: right; width: 2%; margin-top: -10px; line-height: 31px; font-size: small;">
                                    |</li>
                                <li style="float: right; width: 20%; margin-top: -10px; line-height: 31px; font-size: small;">
                                    <asp:LinkButton ID="lnkEditFile" runat="server">Edit</asp:LinkButton></li>
                            </ul>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="fileSave" style="float: right; width: 40%; line-height: 31px; vertical-align: middle;
                    text-align: center; display: block;">
                    <asp:UpdatePanel ID="updSavePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ul style="list-style: none; width: 90%;">
                                <li style="float: right; width: 20%; margin-top: -10px; line-height: 31px; font-size: small;">
                                    <asp:LinkButton ID="lnkCloseApp" runat="server">Close</asp:LinkButton></li>
                                <li style="float: right; width: 2%; margin-top: -10px; line-height: 31px; font-size: small;">
                                    |</li>
                                <li style="float: right; width: 20%; margin-top: -10px; line-height: 31px; font-size: small;">
                                    <asp:LinkButton ID="lnkSaveApp" runat="server" OnClick="lnkSaveApp_Click">Save</asp:LinkButton></li>
                            </ul>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div id="over" class="fl aceContainer" style="width: 100%; height: 100%; position: relative;">
                <pre id="editor"></pre>
                <div id="statusBar">
                    ace rocks!</div>
                <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                       <asp:Image ID="Image1" runat="server" Width="800px" Height="500px"></asp:Image>
                    </ContentTemplate>
                </asp:UpdatePanel>--%>
            </div>
        </div>
        <div id="SubProcAddNewFile" class="hide">
            <div class="radioButtonList">
                <asp:RadioButtonList ID="rdbFileTypes" runat="server" RepeatDirection="Vertical"
                    RepeatLayout="Table">
                    <asp:ListItem Value="IMG" Text="Image File"></asp:ListItem>
                    <asp:ListItem Value="HTML" Text="HTML File"></asp:ListItem>
                    <asp:ListItem Value="JS" Text="JS (Javascript) File"></asp:ListItem>
                    <asp:ListItem Value="CSS" Text="CSS (Stylesheet) File"></asp:ListItem>
                    <asp:ListItem Value="TXT" Text="Text File"></asp:ListItem>
                    <asp:ListItem Value="XML" Text="XML File"></asp:ListItem>
                    <asp:ListItem Value="ZIP" Text="Application Zip File (drag and drop)" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <asp:UpdatePanel runat="server" ID="updAddNewFile" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="InputStyle ButtonStyle"
                                OnClientClick="$('#SubProcAddNewFile').dialog('close');return false;" />
                            <asp:Button ID="btnAddFile" runat="server" Text="Add" CssClass="InputStyle ButtonStyle"
                                OnClientClick="showHtml5AddNewFileDiv(false);showCreateNewFileDiv(true);return false;" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div id="SubProcCreateNewFile" class="hide">
            <asp:UpdatePanel runat="server" ID="updCreateFile" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="radioButtonList">
                        <asp:RadioButtonList ID="rdbCreateMode" runat="server" RepeatDirection="Vertical"
                            onchange="createNewFileModeonChange(this);" RepeatLayout="Table">
                            <asp:ListItem Value="UPL" Text="Upload File" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="CRT" Text="Create File"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div id="divCreateFile" class="DbCmdRow hide" style="margin-left: 10px;">
                        <div style="float: left; padding-top: 5px; padding-left: 5px; width: 14%; margin-top: 5px;
                            cursor: default; margin-left: 8px;">
                            <b>FileName</b>
                        </div>
                        <div style="float: left; padding-top: 5px; padding-left: 1px; width: 4%; margin-top: 5px;
                            cursor: default;">
                            <b>:</b>
                        </div>
                        <div style="float: left; padding-top: 5px; padding-left: 5px; width: 40%; margin-top: 5px;
                            cursor: default;">
                            <asp:TextBox ID="txtFileName" runat="server" CssClass="InputStyle"></asp:TextBox>
                        </div>
                        <div style="float: left; padding-top: 5px; padding-left: 5px; width: 20%; margin-top: 5px;
                            cursor: default; margin-left: 10px;">
                            <asp:Label ID="lblFileExt" runat="server" Text=".zip"></asp:Label>
                        </div>
                    </div>
                    <div class="SubProcborderDiv">
                        <div class="SubProcBtnDiv" align="center">
                            <asp:Button ID="btnCancelNewFile" runat="server" Text="Cancel" CssClass="InputStyle ButtonStyle"
                                OnClientClick="$('#SubProcCreateNewFile').dialog('close');return false;" />
                            <asp:Button ID="btnCreateNewFile" runat="server" Text="Add" CssClass="InputStyle ButtonStyle"
                                OnClientClick="showFileUploader(true);return false;" /></div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="SubProcFileUploader" class="hide">
            <div id="dragandDrop">
                Drag and Drop</div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <asp:Button ID="Button1" runat="server" Text="Cancel" CssClass="InputStyle ButtonStyle"
                        OnClientClick="$('#SubProcFileUploader').dialog('close');return false;" />
                </div>
            </div>
            <div class="hide">
                <asp:UpdatePanel runat="server" ID="updDragAndDrop" UpdateMode="Conditional">
                    <ContentTemplate>
                        <%--<asp:Button ID="btnHidden" runat="server" OnClick="btnHidden_Click" OnClientClick="addAppId();"/>--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="SubProcErrorBox" class="hide">
            <div id="InvalidFilesDiv" class="hide">
                <span></span>
                <div>
                    Only(.jpg, .jpeg, .png, .txt, .xml, .htm, .html, .js, .css) files are allowed.
                    <hr />
                </div>
            </div>
            <br />
            <div id="InvalidFilePathDiv" class="hide">
                <span></span>
            </div>
            <br />
            <div>
                <span>These files will not be uploaded. Do you still want to continue?</span>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <asp:UpdatePanel runat="server" ID="updErrorBox" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Button ID="btnContinue" runat="server" Text="Yes" CssClass="InputStyle ButtonStyle"
                                OnClick="btnContinue_Click" OnClientClick="addAppId();showErrorBox(false,'','');" />
                            <asp:Button ID="btnCancelErrorBox" runat="server" Text="No" CssClass="InputStyle ButtonStyle"
                                OnClientClick="showErrorBox(false,'','');showFileUploader(false);return false;" />
                            <asp:HiddenField ID="hdHtml5AppObj" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div id="SubProcErrorMessageBox" class="hide">
            <asp:UpdatePanel runat="server" ID="updErrorMsg" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="ErrorMsgDiv">
                        <span></span>
                    </div>
                    <div class="SubProcborderDiv">
                        <div class="SubProcBtnDiv" align="center">
                            <asp:Button ID="btnOk" runat="server" Text="OK" CssClass="InputStyle ButtonStyle"
                                OnClientClick="showErrorMsgBox(false,'');" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="divGetApplicationName" class="hide">
            <div style="float: left; width: 100%">
                <div class="DbCmdRow" style="margin-left: 5px">
                    <div class="DbConnLeftDiv">
                        Application Name
                    </div>
                    <div class="DbConnRightDiv">
                        <input type="text" id="txtNewAppName" class="InputStyle w_80" />
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="SubProcborderDiv">
                    <div class="SubProcBtnDiv" align="center">
                        <input id="btnAddNewApp" type="button" value="  Create  " class="InputStyle" />
                    </div>
                </div>
            </div>
        </div>
        <div id="shadow" style="width: 100%; position: absolute; top: 0px; left: 0px; opacity: 0.6  !important;
            filter: alpha(opacity=60); background-color: #333 !important; z-index: 1000;
            min-height: 1000px; height: auto !important; height: 100%; display: none;">
            <div style="margin-left: 45%; margin-top: 22%; background-color: #333 !important;">
                <div id="progress">
                </div>
                <br />
                <div style="color: White; margin-left: 110px; margin-top: 6px; font-size: small;">
                    <span>Please Wait.........</span></div>
                <div id="progressValue" style="color: White; margin-left: 70px; margin-top: 6px;
                    font-size: small;">
                    <span></span>
                </div>
            </div>
        </div>
        <div id="divWaitBox" class="waitModal">
            <div id="WaitAnim">
                <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                    AlternateText="Please Wait" BorderWidth="0px" />
            </div>
        </div>
        <div>
            <asp:HiddenField ID="hidIsCleanUpRequired" runat="server" />
        </div>
        <!-- Hidden Fields -->
        <div id="divHiddenFields" style="display: none">
            <asp:UpdatePanel ID="updHiddenFields" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:HiddenField ID="hdFileBytes" runat="server" />
                    <asp:HiddenField ID="hdFileName" runat="server" />
                    <asp:HiddenField ID="hidTabSelected" runat="server" />
                    <asp:HiddenField ID="hidAppTreeView" runat="server" />
                    <asp:HiddenField ID="hidAppFileSelected" runat="server" />
                    <asp:HiddenField ID="hidSelectedFileContent" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <!-- End Hidden Fields -->
        <div class="hide">
            <asp:UpdatePanel ID="updClick" runat="server">
                <ContentTemplate>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <!-- POST BACK BUTTON FOR HTML CONTROLS-->
        <div class="hide">
            <asp:UpdatePanel ID="updPostbackByHtmlCntrl" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btnPostbackByHtmlCntrl" runat="server" Text="" OnClick="btnPostbackByHtmlCntrl_Click" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <!--End POST BACK BUTTON FOR HTML CONTROLS -->
    </div>
    <script type="text/javascript">
        var editor = ace.edit("editor");
        editor.setTheme("ace/theme/crimson_editor");
        editor.getSession().setMode("ace/mode/javascript");
        //editor.$blockScrolling = Infinity
        //$(function () { $('#jstree_demo_div').jstree(); });
</script>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }

        function isCookieCleanUpRequired(value) {
            if (typeof value === "undefined") {
                $('#' + '<%=hidIsCleanUpRequired.ClientID %>').val('false'); ;
            }
            else {
                $('#' + '<%=hidIsCleanUpRequired.ClientID %>').val(value);
            }
        }
    </script>
</asp:Content>

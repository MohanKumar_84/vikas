﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Newtonsoft.Json.Linq;
using System.Collections;
using Newtonsoft.Json;

namespace mFicientCP
{
    public partial class TransferApps : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        Hashtable hstLinkCmds = new Hashtable();
        Hashtable hstLinkCon = new Hashtable();

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!IsPostBack)
            {
                bindAppsDropdown();
                bindCompaniesDropdown();
                bindSubabminsDropdown();
            }
        }

        protected void ddlCompanies_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindSubabminsDropdown();
            ScriptManager.RegisterStartupScript(updAppTransfer, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"UniformControlDesign();", true);
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            string strArryOfflineDataConJson = string.Empty, strArryDbConJson = string.Empty, strArryWsConJson = string.Empty, strArryODataConJson = string.Empty;
            getDbConnections();
            getWsConnections();
            getOdataConnections();
            //getOfflineDataTables();
            DataTable dtWFDetail = GetWfDetails.getWorkflowDetailByWfId(ddlApps.SelectedValue, hfsPart3);

            if (dtWFDetail != null && dtWFDetail.Rows.Count > 0)
            {
                hdfWfDetails.Value = HttpContext.Current.Server.HtmlEncode(Utilities.ConvertdatetabletoString(dtWFDetail));
                List<string> lstAppJson = new List<string>();
                foreach (DataRow row in dtWFDetail.Rows)
                {
                    lstAppJson.Add(row["APP_JS_JSON"].ToString());
                }
                ExtractAppfromJson extractJson = new ExtractAppfromJson(lstAppJson, hfsPart3);
                if (extractJson.DbConnections.Count > 0)
                {
                    foreach (DataBaseConnector dbCon in extractJson.DbConnections.Values)
                    {
                        if (strArryDbConJson.Trim().Length > 0)
                            strArryDbConJson += ",";
                        strArryDbConJson += "{ \"id\" : \"" + dbCon.ConnectorId + "\", \"name\" : \"" + dbCon.ConnectionName + "\"}";
                    }
                }
                if (extractJson.WSConnections.Count > 0)
                {
                    foreach (WebServiceConnector wsCon in extractJson.WSConnections.Values)
                    {
                        if (strArryWsConJson.Trim().Length > 0)
                            strArryWsConJson += ",";
                        strArryWsConJson += "{ \"id\" : \"" + wsCon.ConnectorId + "\", \"name\" : \"" + wsCon.ConnectionName + "\"}";
                    }
                }
                if (extractJson.ODataConnections.Count > 0)
                {
                    foreach (ODataConnector oDataCon in extractJson.ODataConnections.Values)
                    {
                        if (strArryODataConJson.Trim().Length > 0)
                            strArryODataConJson += ",";
                        strArryODataConJson += "{ \"id\" : \"" + oDataCon.ConnectorId + "\", \"name\" : \"" + oDataCon.ConnectionName + "\"}";
                    }
                }
                if (extractJson.OfflineDataTables.Count > 0)
                {
                    foreach (MFEOfflineDataTable offlineTbl in extractJson.OfflineDataTables.Values)
                    {
                        if (strArryOfflineDataConJson.Trim().Length > 0)
                            strArryOfflineDataConJson += ",";
                        strArryOfflineDataConJson += "{ \"id\" : \"" + offlineTbl.TableId + "\", \"name\" : \"" + offlineTbl.TableName + "\"}";
                    }
                }

                hdfDbCommands.Value = extractJson.DBCommandsJson;
                hdfWsCommands.Value = extractJson.WSCommandsJson;
                hdfODataCommands.Value = extractJson.ODataCommandsJson;
                hdfOfflineCommands.Value = extractJson.OfflineDataCommandsJson;

                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#divLinkConnections').createDataConLinksHtml({ DbConnections: [" + strArryDbConJson + "], WsConnections: [" + strArryWsConJson + "], ODataConnections : [" + strArryODataConJson + "], OfflineDataConnections : [" + strArryOfflineDataConJson + "]  });UniformControlDesign();", true);
            }

            ScriptManager.RegisterStartupScript(updAppTransfer, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"UniformControlDesign();", true);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            transferWsCommands();
            transferDbCommands();
            transferODataCommands();
            transferWf();
        }

        private void bindAppsDropdown()
        {
            ddlApps.Items.Clear();
            DataTable dtblWfDtls = GetWfDetails.getWorkflowDetailByCompId(hfsPart3);
            if (dtblWfDtls != null)
            {
                ddlApps.DataSource = dtblWfDtls;

                ddlApps.DataValueField = "WF_ID";
                ddlApps.DataTextField = "WF_NAME";
                ddlApps.DataBind();
                ddlApps.Items.Insert(0, new ListItem("Select App", "-1"));
            }
            else
            {
                ddlApps.Items.Insert(0, new ListItem("Select App", "-1"));
            }
        }

        private void bindCompaniesDropdown()
        {
            ddlCompanies.Items.Clear();
            GetCompanyList getCompList = new GetCompanyList();
            getCompList.Process();
            DataTable dtblCompDtls = getCompList.ResultTable;
            if (getCompList.StatusCode == 0 && dtblCompDtls != null)
            {
                ddlCompanies.DataSource = dtblCompDtls;

                ddlCompanies.DataValueField = "COMPANY_ID";
                ddlCompanies.DataTextField = "COMPANY_NAME";
                ddlCompanies.DataBind();
                ddlCompanies.Items.Insert(0, new ListItem("Select Company", "-1"));
            }
            else
            {
                ddlCompanies.Items.Insert(0, new ListItem("Select Company", "-1"));
            }
        }

        private void bindSubabminsDropdown()
        {
            ddlSubadmins.Items.Clear();
            GetSubAdminDetail getSubadminList = new GetSubAdminDetail(ddlCompanies.SelectedValue);
            getSubadminList.getAllSubAdminsOfCmp();
            DataTable dtblSubadminDtls = getSubadminList.ResultTable;
            if (getSubadminList.StatusCode == 0 && dtblSubadminDtls != null)
            {
                ddlSubadmins.DataSource = dtblSubadminDtls;

                ddlSubadmins.DataValueField = "SUBADMIN_ID";
                ddlSubadmins.DataTextField = "FULL_NAME";
                ddlSubadmins.DataBind();
                ddlSubadmins.Items.Insert(0, new ListItem("Select Subadmin", "-1"));
            }
            else
            {
                ddlSubadmins.Items.Insert(0, new ListItem("Select Subadmin", "-1"));
            }
        }

        private void getDbConnections()
        {
            GetDatabaseConnection dbConnections = new GetDatabaseConnection(true, "", "", ddlCompanies.SelectedItem.Value);
            if (dbConnections.StatusCode == 0 && dbConnections.ResultTable != null && dbConnections.ResultTable.Rows.Count > 0)
            {
                string strDbConJson = "";
                foreach (DataRow dbRow in dbConnections.ResultTable.Rows)
                {
                    if (strDbConJson.Trim().Length > 0)
                        strDbConJson += ",";
                    strDbConJson += "{ \"id\" : \"" + dbRow["DB_CONNECTOR_ID"] + "\", \"name\" : \"" + dbRow["CONNECTION_NAME"] + "\"}";
                }

                if (strDbConJson.Trim().Length > 0)
                    hdfDbCon.Value = "[" + strDbConJson + "]";
                else
                    hdfDbCon.Value = string.Empty;
            }
        }

        private void getWsConnections()
        {
            GetWebserviceConnection wsConnections = new GetWebserviceConnection(true, "", "", "", ddlCompanies.SelectedItem.Value);
            wsConnections.Process();
            if (wsConnections.StatusCode == 0 && wsConnections.ResultTable != null && wsConnections.ResultTable.Rows.Count > 0)
            {
                string strWsConJson = "";
                foreach (DataRow wsRow in wsConnections.ResultTable.Rows)
                {
                    if (strWsConJson.Trim().Length > 0)
                        strWsConJson += ",";
                    strWsConJson += "{ \"id\" : \"" + wsRow["WS_CONNECTOR_ID"] + "\", \"name\" : \"" + wsRow["CONNECTION_NAME"] + "\"}";
                }

                if (strWsConJson.Trim().Length > 0)
                    hdfWsCon.Value = "[" + strWsConJson + "]";
                else
                    hdfWsCon.Value = string.Empty;
            }
        }

        private void getOdataConnections()
        {
            GetODataSrviceConnector oDataConnections = new GetODataSrviceConnector(true, "", "", "", ddlCompanies.SelectedItem.Value);
            if (oDataConnections.StatusCode == 0 && oDataConnections.ResultTable != null && oDataConnections.ResultTable.Rows.Count > 0)
            {
                string strODataConJson = "";
                foreach (DataRow oDataRow in oDataConnections.ResultTable.Rows)
                {
                    if (strODataConJson.Trim().Length > 0)
                        strODataConJson += ",";
                    strODataConJson += "{ \"id\" : \"" + oDataRow["ODATA_CONNECTOR_ID"] + "\", \"name\" : \"" + oDataRow["CONNECTION_NAME"] + "\"}";
                }

                if (strODataConJson.Trim().Length > 0)
                    hdfODataCon.Value = "[" + strODataConJson + "]";
                else
                    hdfODataCon.Value = string.Empty;
            }
        }

        //private void getOfflineDataTables()
        //{
        //    GetOfflineDataTableDtl offlineDataTbls = new GetOfflineDataTableDtl(hfsPart4);
        //    if (offlineDataTbls.StatusCode == 0 && offlineDataTbls.offlineDatatables != null && offlineDataTbls.offlineDatatables.Count > 0)
        //    {
        //        string strOfflineDataConJson = "";
        //        foreach (MFEOfflineDataTable offlineTbl in offlineDataTbls.offlineDatatables)
        //        {
        //            if (strOfflineDataConJson.Trim().Length > 0)
        //                strOfflineDataConJson += ",";
        //            strOfflineDataConJson += "{ \"id\" : \"" + offlineTbl.TableId + "\", \"upDataCmdId\" : \"" + offlineTbl.UploadCommandId + "\", \"downDataCmdId\" : \"" + offlineTbl.DownloadCommandId + "\"}";
        //        }

        //        if (strOfflineDataConJson.Trim().Length > 0)
        //            hdfOfflineDataTbl.Value = "[" + strOfflineDataConJson + "]";
        //        else
        //            hdfOfflineDataTbl.Value = string.Empty;
        //    }
        //}

        private void transferDbCommands()
        {
            if (!string.IsNullOrEmpty(hdfDbLinks.Value))
            {
                Hashtable hstDbCon = new Hashtable();
                JArray dbConArray = JArray.Parse(hdfDbLinks.Value);
                foreach (JObject obj in dbConArray.Children<JObject>())
                {
                    string conId = string.Empty;
                    hstDbCon[obj["con"].ToString()] = obj["link"].ToString();
                }

                string newConId = string.Empty;
                if (!string.IsNullOrEmpty(hdfDbCommands.Value))
                {
                    JArray dbCmdArray = JArray.Parse(hdfDbCommands.Value);
                    List<DatabaseCommand> lstDbCmds = new List<DatabaseCommand>();
                    foreach (var obj in dbCmdArray)
                    {
                        string s = obj.ToString();
                        lstDbCmds.Add((DatabaseCommand)JsonConvert.DeserializeObject(obj.ToString(), typeof(DatabaseCommand)));
                    }

                    foreach (DatabaseCommand dbCmd in lstDbCmds)
                    {
                        newConId = getValueFromCollection(hstDbCon, dbCmd.ConnectorId);
                        if (!hstLinkCon.Contains(dbCmd.ConnectorId))
                            hstLinkCon.Add(dbCmd.ConnectorId, newConId);
                        TransferAppCommands transferDbCmd = new TransferAppCommands(dbCmd, hfsPart3, hfsPart4, newConId);
                        if (!hstLinkCmds.Contains(dbCmd.CommandId))
                            hstLinkCmds.Add(dbCmd.CommandId, transferDbCmd.NewCommandId);
                    }
                }
            }
        }

        private void transferWsCommands()
        {
            if (!string.IsNullOrEmpty(hdfWsLinks.Value))
            {
                Hashtable hstWsCon = new Hashtable();
                JArray wsConArray = JArray.Parse(hdfWsLinks.Value);
                foreach (JObject obj in wsConArray.Children<JObject>())
                {
                    string conId = string.Empty;
                    hstWsCon[obj["con"].ToString()] = obj["link"].ToString();
                }

                string newConId = string.Empty;
                if (!string.IsNullOrEmpty(hdfWsCommands.Value))
                {
                    JArray wsCmdArray = JArray.Parse(hdfWsCommands.Value);
                    List<WebServiceCommand> lstWsCmds = new List<WebServiceCommand>();
                    foreach (var obj in wsCmdArray)
                    {
                        lstWsCmds.Add((WebServiceCommand)JsonConvert.DeserializeObject(obj.ToString(), typeof(WebServiceCommand)));
                    }

                    foreach (WebServiceCommand wsCmd in lstWsCmds)
                    {
                        newConId = getValueFromCollection(hstWsCon, wsCmd.ConnectorId);
                        if (!hstLinkCon.Contains(wsCmd.ConnectorId))
                            hstLinkCon.Add(wsCmd.ConnectorId, newConId);
                        TransferAppCommands transferWsCmd = new TransferAppCommands(wsCmd, hfsPart3, hfsPart4, newConId);
                        if (!hstLinkCmds.Contains(wsCmd.CommandId))
                            hstLinkCmds.Add(wsCmd.CommandId, transferWsCmd.NewCommandId);
                    }
                }
            }
        }

        private void transferODataCommands()
        {
            if (!string.IsNullOrEmpty(hdfODataLinks.Value))
            {
                Hashtable hstODataCon = new Hashtable();
                JArray oDataConArray = JArray.Parse(hdfODataLinks.Value);
                foreach (JObject obj in oDataConArray.Children<JObject>())
                {
                    string conId = string.Empty;
                    hstODataCon[obj["con"].ToString()] = obj["link"].ToString();
                }

                string newConId = string.Empty;
                if (!string.IsNullOrEmpty(hdfODataCommands.Value))
                {
                    JArray oDataCmdArray = JArray.Parse(hdfODataCommands.Value);
                    List<OdataCommand> lstODataCmds = new List<OdataCommand>();
                    foreach (var obj in oDataCmdArray)
                    {
                        lstODataCmds.Add((OdataCommand)JsonConvert.DeserializeObject(obj.ToString(), typeof(OdataCommand)));
                    }

                    foreach (OdataCommand oDataCmd in lstODataCmds)
                    {
                        newConId = getValueFromCollection(hstODataCon, oDataCmd.ConnectorId);
                        if (!hstLinkCon.Contains(oDataCmd.ConnectorId))
                            hstLinkCon.Add(oDataCmd.ConnectorId, newConId);
                        TransferAppCommands transferODataCmd = new TransferAppCommands(oDataCmd, hfsPart3, hfsPart4, newConId);
                        if (!hstLinkCmds.Contains(oDataCmd.CommandId))
                            hstLinkCmds.Add(oDataCmd.CommandId, transferODataCmd.NewCommandId);
                    }
                }
            }
        }

        private void transferOfflineDataCommands()
        {
            if (!string.IsNullOrEmpty(hdfOfflineLinks.Value))
            {
                Hashtable hstOfflineDataCon = new Hashtable();
                JArray dbConArray = JArray.Parse(hdfOfflineLinks.Value);
                foreach (JObject obj in dbConArray.Children<JObject>())
                {
                    string conId = string.Empty;
                    foreach (JProperty prop in obj.Properties())
                    {
                        if (prop.Name.Trim() == "con")
                        {
                            if (!hstOfflineDataCon.Contains(prop.Value))
                            {
                                hstOfflineDataCon.Add(Convert.ToString(prop.Value), string.Empty);
                                conId = Convert.ToString(prop.Value);
                            }
                        }
                        else if (prop.Name.Trim() == "link")
                        {
                            hstOfflineDataCon[conId] = prop.Value;
                        }
                    }
                }

                string newConId = string.Empty;
                if (!string.IsNullOrEmpty(hdfOfflineCommands.Value))
                {
                    JArray offlineDataCmdArray = JArray.Parse(hdfOfflineCommands.Value);
                    List<OfflineDataObject> lstOfflineDataCmds = new List<OfflineDataObject>();
                    foreach (var obj in offlineDataCmdArray)
                    {
                        lstOfflineDataCmds.Add((OfflineDataObject)JsonConvert.DeserializeObject(obj.ToString(), typeof(OfflineDataObject)));
                    }

                    foreach (OfflineDataObject offlineDataObj in lstOfflineDataCmds)
                    {
                        newConId = getValueFromCollection(hstOfflineDataCon, offlineDataObj.ID);
                        if (!hstLinkCon.Contains(offlineDataObj.ID))
                            hstLinkCon.Add(offlineDataObj.ID, newConId);
                        TransferAppCommands transferOfflineDataCmd = new TransferAppCommands(offlineDataObj, hfsPart3, hfsPart4, newConId);
                        if (!hstLinkCmds.Contains(offlineDataObj.ID))
                            hstLinkCmds.Add(offlineDataObj.ID, transferOfflineDataCmd.NewCommandId);
                    }
                }
            }
        }

        private string getValueFromCollection(Hashtable hst, string key)
        {
            string value = string.Empty;
            if (hst != null && !string.IsNullOrEmpty(key))
            {
                value = Convert.ToString(hst[key]);
            }

            return value;
        }

        private void transferWf()
        {
            if (!string.IsNullOrEmpty(hdfWfDetails.Value))
            {
                DataTable dt = (DataTable)JsonConvert.DeserializeObject<DataTable>(hdfWfDetails.Value);

                string[] strAppJson = new string[dt.Rows.Count];
                if (dt != null && dt.Rows.Count > 0)
                {
                    int count =  0;
                    foreach (DataRow row in dt.Rows)
                    {
                        strAppJson[count] = deserializeAppJsonAndCreateWf(Convert.ToString(row["APP_JS_JSON"]));
                        count ++;
                    }

                    SaveCompleteApp saveApp = new SaveCompleteApp();
                    saveApp.ProcessSaveAsCompleteApp(ddlSubadmins.SelectedValue, ddlCompanies.SelectedValue, ddlApps.SelectedValue, strAppJson);
                }
            }
        }

        private string deserializeAppJsonAndCreateWf(string _appJson)
        {
            JObject appJsonObj = JObject.Parse(_appJson);

            var objStartupTasks = JObject.Parse(Convert.ToString(appJsonObj["startUpTasks"]));
            Tasks startupTasks = objStartupTasks.ToObject<Tasks>();

            appJsonObj["startUpTasks"] = JsonConvert.SerializeObject(processTask(startupTasks));

            JArray array = JArray.Parse(Convert.ToString(appJsonObj["views"]));
            List<Appviews> views = new List<Appviews>();
            JArray newArrViews = new JArray();
            foreach (var obj in array)
            {
                views.Add(obj.ToObject<Appviews>());
            }

            foreach (Appviews view in views)
            {
                view.onExitCmds = processTask(view.onExitCmds);
                view.rowClickTasks = processTask(view.rowClickTasks);
                view.cancelTasks = processTask(view.cancelTasks);
                view.actionBtn1Tasks = processTask(view.actionBtn1Tasks);
                view.actionBtn2Tasks = processTask(view.actionBtn2Tasks);
                view.actionBtn3Tasks = processTask(view.actionBtn3Tasks);
                getViewActionBtnsTasks(view.viewActionBtns);

                newArrViews.Add(JObject.FromObject(view));

            }

            appJsonObj["views"] = JsonConvert.SerializeObject(newArrViews);

            return JsonConvert.SerializeObject(appJsonObj);
        }

        private List<ViewActionButton> getViewActionBtnsTasks(List<ViewActionButton> lstViewActionButtons)
        {
            List<ViewActionButton> newLstActionBtns = null;
            if (lstViewActionButtons != null && lstViewActionButtons.Count > 0)
            {
                foreach (ViewActionButton viewActnBtn in lstViewActionButtons)
                {
                   viewActnBtn.onExitTasks = processTask(viewActnBtn.onExitTasks);
                   newLstActionBtns.Add(viewActnBtn);
                }
            }
            return newLstActionBtns;
        }

        private Tasks processTask(Tasks task)
        {
            Tasks newTasks = new Tasks();
            newTasks.databindingObjs = processCommandsFromTask(task);
            return newTasks;
        }

        private List<AppCommands> processCommandsFromTask(Tasks task)
        {
            AppCommands newAppcmdObj;
            List<AppCommands> appCommands = new List<AppCommands>();
            if (task.databindingObjs != null && task.databindingObjs.Count > 0)
            {
                foreach (AppCommands appCmd in task.databindingObjs)
                {
                    newAppcmdObj = replaceCmdId(appCmd);
                    if (!appCommands.Contains(newAppcmdObj))
                        appCommands.Add(newAppcmdObj);
                }
            }
            return appCommands;
        }

        private AppCommands replaceCmdId(AppCommands cmd)
        {
            if (cmd != null)
            {
                cmd.id = getValueFromCollection(hstLinkCmds, cmd.id);
                return cmd;
            }
            else
            {
                return null;
            }
        }
    }
}
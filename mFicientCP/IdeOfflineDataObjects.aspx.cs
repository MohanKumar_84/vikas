﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using DataQueryParser;
using System.Collections;

namespace mFicientCP
{
    public partial class IdeOfflineDataObjects : System.Web.UI.Page
    {
        #region Private Members

        private string strhfs, strhfbid, subAdminId = "", companyId = "", sessionId = "", adminId = "", strPageMode = "", strPostbackPageMode = "";

        #endregion

        #region Page Control Events

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            MasterPage mainMaster = Page.Master;
            if (Page.IsPostBack)
            {
                strhfs = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
                strPostbackPageMode = ((HiddenField)mainMaster.FindControl("hidAppNewOrEdit")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;
                strPostbackPageMode = ((HiddenField)previousPage.Master.FindControl("hidAppNewOrEdit")).Value;

                strhfs = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
                ((HiddenField)mainMaster.FindControl("hfs")).Value = strhfs;
                ((HiddenField)mainMaster.FindControl("hfbid")).Value = strhfbid;
            }

            if (string.IsNullOrEmpty(strhfs) || string.IsNullOrEmpty(strhfbid)) return;
            context.Items["hfs"] = strhfs;
            context.Items["hfbid"] = strhfbid;
            Utilities.SaveInContext(context, strhfs, strhfbid, out subAdminId, out sessionId, out companyId, out adminId);

            //if (!string.IsNullOrEmpty(strhfs))
            //{
            //    string str = Utilities.DecryptString(strhfs);
            //    subAdminId = str.Split(',')[0];
            //    companyId = str.Split(',')[3].ToLower();
            //    sessionId = str.Split(',')[1];
            //    adminId = str.Split(',')[2];
            //}
            //if (!IsPostBack == true)
            //{
            //}
            if (!IsPostBack || (IsPostBack && !string.IsNullOrEmpty(strPostbackPageMode)))
            {
                bindDataObjects();
                getAllOfllineDatatables();
            }
        }

        protected void btnDataObj_Save_Click(object sender, EventArgs e)
        {
            try
            {
                bool allowRetry = ddlAllowRetry.SelectedValue == "0" ? false : true;
                bool exitOnError = ddlExitOnError.SelectedValue == "0" ? false : true;

                if (string.IsNullOrEmpty(hdfDataObjectId.Value))
                {
                    AddOfflineDataObject addOfflineDataObj = new AddOfflineDataObject(companyId, subAdminId, txtDataObj_Name.Text.Trim(), txtDataObj_Desc.Text.Trim(),
                                                                                     txtDataObj_Query.Text.Trim(), hdfDbCmdPara.Value, hdfOfflineColumns.Value.Trim(), hdfOfflineTablesUsed.Value.Trim(),
                                                                                     (DatabaseCommandType)(Convert.ToInt32(ddl_DataObjType.SelectedValue)), allowRetry, exitOnError, txtErrorMessages.Text.Trim());
                    if (addOfflineDataObj.StatusCode == 0)
                    {
                        bindDataObjects();
                        ScriptManager.RegisterStartupScript(updAddNewDataObject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aCFmessage').html('" + "* Offline Data Object Saved Succsessfully." + "');SubProcConfirmBoxMessageDB(true,'Saved', 300);initializeDataObjects();UniformControlDesign();", true);
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html('* Error adding offline data object');SubProcBoxMessage(true,'Error', 300);UniformControlDesign();", true);
                }
                else
                {
                    UpdateOfflineDataObject updateOfflineDataObj = new UpdateOfflineDataObject(hdfDataObjectId.Value, companyId, subAdminId, txtDataObj_Desc.Text.Trim(), txtDataObj_Query.Text.Trim(),
                                                                                               hdfDbCmdPara.Value, hdfOfflineColumns.Value.Trim(), hdfOfflineTablesUsed.Value.Trim(), allowRetry, exitOnError, txtErrorMessages.Text.Trim());
                    if (updateOfflineDataObj.StatusCode == 0)
                    {
                        bindDataObjects();
                        ScriptManager.RegisterStartupScript(updAddNewDataObject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aCFmessage').html('" + "* Offline Data Object Updated Succsessfully." + "');SubProcConfirmBoxMessageDB(true,'Updated', 300);initializeDataObjects();UniformControlDesign();", true);
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html('* Error updating offline data object');SubProcBoxMessage(true,'Error', 300);UniformControlDesign(); $('[id$=btnDataObj_Cancel]').show();", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html(" + ex.Message + ");SubProcBoxMessage(true,'Error', 300);UniformControlDesign();", true);
            }
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteOfflineDataObject deleteObj = new DeleteOfflineDataObject(hdfDataObjectId.Value.Trim(), companyId);
                if (deleteObj.StatusCode == 0)
                {
                    bindDataObjects();
                    ScriptManager.RegisterStartupScript(updAddNewDataObject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aCFmessage').html('" + "* Offline Data Object Deleted Succsessfully." + "');SubProcConfirmBoxMessageDB(true,'Deleted', 300);initializeDataObjects();UniformControlDesign();addNewOfflineDataObject();", true);
                }
                else
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html('* Error deleting offline data object');SubProcBoxMessage(true,'Error', 300);UniformControlDesign();", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(updAddNewDataObject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aMessage').html(" + ex.Message + ");SubProcBoxMessage(true);UniformControlDesign();", true);
            }
        }

        protected void CommandCopy_Save_Click(object sender, EventArgs e)
        {
            try
            {
                GetOfflineDataObject getOfflineDataObj = new GetOfflineDataObject(hdfDataObjectId.Value, companyId);
                if (getOfflineDataObj.DataObject != null)
                {
                    AddOfflineDataObject addNewOfflineDataObj = new AddOfflineDataObject(getOfflineDataObj.DataObject, txt_CopyObject.Text.Trim());

                    if (addNewOfflineDataObj.StatusCode == 0)
                    {
                        bindDataObjects();
                        ScriptManager.RegisterStartupScript(updSaveCopiedObject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"initializeDataObjects();UniformControlDesign();$('#aCFmessage').html('" + "* Offline Data Object Saved Succsessfully." + "');SubProcConfirmBoxMessageDB(true,'Saved', 300);", true);
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#aMessage').html('* Error adding offline data object');SubProcBoxMessage(true,'Error', 300);UniformControlDesign();", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(updSaveCopiedObject, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aMessage').html(" + ex.Message + ");SubProcBoxMessage(true);UniformControlDesign();", true);
            }

        }

        #endregion

        #region Private Methods

        private void bindDataObjects()
        {
            try
            {
                GetOfflineDataObject objGetOfflineDataObjects = new GetOfflineDataObject(companyId);
                hdfCompleteData.Value = JsonConvert.SerializeObject(objGetOfflineDataObjects.DataObjects);
                hdfSelectedData.Value = getSelectedDataofObjects(objGetOfflineDataObjects.DataObjects);
                updAddNewDataObject.Update();
                ScriptManager.RegisterStartupScript(updDataObjectsList, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"initializeDataObjects();UniformControlDesign();", true);
            }
            catch 
            {

            }
        }

        private void getAllOfllineDatatables()
        {
            GetOfflineDataTableDtl getOfflineData = new GetOfflineDataTableDtl(companyId);
            try
            {
                getOfflineData.Process();
                if (getOfflineData.StatusCode == 0)
                {
                    hdfOfflineTables.Value = JsonConvert.SerializeObject(getOfflineData.offlineDatatables);
                }
            }
            catch 
            {
                hdfOfflineTables.Value = string.Empty;
            }
        }

        private string getSelectedDataofObjects(List<OfflineDataObject> lstOfflineDataObjects)
        {
            string serializedData = string.Empty;
            List<List<string>> lstDataObjects = new List<List<string>>();
            try
            {
                if (lstOfflineDataObjects != null && lstOfflineDataObjects.Count > 0)
                {
                    List<string> dataObj = new List<string>();
                    foreach (OfflineDataObject obj in lstOfflineDataObjects)
                    {
                        dataObj = new List<string>();
                        dataObj.Add(obj.ID);
                        dataObj.Add(obj.Name);
                        dataObj.Add(obj.Description);
                        dataObj.Add(obj.QueryType.ToString());
                        lstDataObjects.Add(dataObj);
                    }
                }

                serializedData = JsonConvert.SerializeObject(lstDataObjects);
            }
            catch
            {
                serializedData = string.Empty;
            }
            return serializedData;
        }

        #endregion
    }
}
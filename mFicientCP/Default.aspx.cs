﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Web.Caching;
//using mficientCP;
namespace mFicientCP
{
    public partial class _Default : System.Web.UI.Page
    {
        const string errorMsg = @"Invalid Username and / or Password and / or Company Id";
        const string errorMsgCompanyBlocked = @"Account is blocked";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                txtCompanyId.Focus();
                //hidQueryStrFromEmail.Value = Utilities.EncryptString("DB4EE0FB295332EE13B513591301C357"+"@" + "05B28BBB8B4C8823" + "@" + "shalini10");
                //For registration of user devices we will send email to the sub admin,from there he will be allowed to click a link
                //where we have provided the option for first login and go to the specific page
                //the following is for the above purpose
                //query string structure ?d1=encrypted useridandcompanyidandsubadminUserName&d2=page where we have to redirect
                if (Request.QueryString.Count > 0)
                {
                    hidQueryStrFromEmail.Value = Request.QueryString.ToString();
                    if (!String.IsNullOrEmpty(Request.QueryString.Get("d1")))
                    {
                        string[] companyIdAndSubAdminName = Utilities.DecryptString(Request.QueryString.Get("d1")).Split('@');
                        txtCompanyId.Text = companyIdAndSubAdminName[1];
                        txtUserName.Text = companyIdAndSubAdminName[2];
                    }
                }

                if (Request.Cookies[MficientConstants.COOKIE_MUEID] != null)
                {
                    HttpCookie userNameCompanyCookie = Request.Cookies.Get(MficientConstants.COOKIE_MUEID);
                    string userName = Convert.ToString(userNameCompanyCookie.Values[MficientConstants.USER_NAME_IN_COOKIE]);
                    string companyId = Convert.ToString(userNameCompanyCookie.Values[MficientConstants.COMPANY_NAME_IN_COOKIE]);
                    if (!String.IsNullOrEmpty(userName))
                    {
                        txtUserName.Text = userName;
                        chkRememberUserName.Checked = true;
                    }
                    if (!String.IsNullOrEmpty(companyId))
                    {
                        txtCompanyId.Text = companyId;
                        chkRememberCompanyId.Checked = true;
                    }
                }

                if (Page.PreviousPage == null && Request.Cookies[MficientConstants.COOKIE_MSID] != null)
                {
                    HttpCookie reqCookie = Request.Cookies[MficientConstants.COOKIE_MSID];

                    if (String.IsNullOrEmpty(reqCookie.Value) || reqCookie.Value == "null") return;

                    hfs.Value = reqCookie.Value.Length > 0 ? HttpUtility.UrlDecode(reqCookie.Value).Split('|')[0] : String.Empty;//if cookie is present this should have a value
                    if (!String.IsNullOrEmpty(hfs.Value))//if it is empty then the pages get redirected again and again.Not a valid condition
                    {
                        hfbid.Value = Utilities.getBrowserWindowUniqueId();
                        reqCookie.Value =
                           reqCookie.Value + "|" + hfbid.Value;
                        HttpContext.Current.Items.Add("hfs", hfs.Value);
                        HttpContext.Current.Items.Add("hfbid", hfbid.Value);
                        HttpContext.Current.Response.Cookies[MficientConstants.COOKIE_MSID].Value = reqCookie.Value;

                        hfp.Value = @"~/Home.aspx";
                        Server.Transfer(hfp.Value, true);
                        return;
                    }
                    else
                    {
                        //throw new Exception("Invalid condition reached.If cookie was present then it should have an hfs value");
                        Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, @"eraseCookie(COOKIE_MSID);", true);
                        return;
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(hfs.Value) && !string.IsNullOrEmpty(hfp.Value))
                {
                    Utilities.runPostBackScript("generateUniqueBrowserId()", TransferUpdatePanel, "Generate browser id");
                    HttpContext.Current.Items.Add("hfs", hfs.Value);
                    HttpContext.Current.Items.Add("hfbid", hfbid.Value);
                    Server.Transfer(hfp.Value, true);
                    txtCompanyId.Focus();
                }
            }
        }

        void showAlert(string message, string divIdToShowAlert)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowAlert + "'" + ");", true);
        }

        protected void UserLogin(string _userName, string _password)
        {
            Boolean bolIsAdmin = true;
            string strMessage = validateForm();
            if (String.IsNullOrEmpty(strMessage))
            {

                /***
                 * First check if company is blocked or not
                 * ***/
                MFECompanyInfo objCmpInfo = this.getCompanyInfo();
                if (!String.IsNullOrEmpty(objCmpInfo.CompanyId))
                {
                    if (!isCompanyBlocked(objCmpInfo))
                    {
                        try
                        {
                            UserLogin userlogin = new UserLogin(bolIsAdmin, txtCompanyId.Text, _userName, _password, "web", "", "", "");
                            userlogin.Process();
                            if (userlogin.StatusCode == 0)
                            {
                                rememberUserNameAndCompanyId();
                                hfs.Value = Utilities.EncryptString(
                                    userlogin.UserId +
                                    "," +
                                    userlogin.SessionId +
                                    "," +
                                    userlogin.DBCompanyId.ToLower() +
                                    "," +
                                    userlogin.AdminId +
                                    "," + userlogin.StrSubAdminName +
                                     "," +
                                     userlogin.StrSubAdminUserName.ToLower()
 
                                    );
                                hfbid.Value = userlogin.BrowserWindowId;
                                string ScriptToRun = @"xferBack();";
                                if (String.IsNullOrEmpty(hidQueryStrFromEmail.Value))
                                {
                                    hfp.Value = @"~/Home.aspx";
                                }
                                else
                                {
                                    string[] valuesFromQueryString = hidQueryStrFromEmail.Value.Split('&');
                                    hfp.Value = @"~/" + valuesFromQueryString[1].Substring(valuesFromQueryString[1].IndexOf('=') + 1) + ".aspx" + "?" + valuesFromQueryString[0];
                                }
                                ScriptManager.RegisterClientScriptBlock(TransferUpdatePanel, typeof(UpdatePanel), TransferUpdatePanel.ClientID, ScriptToRun, true);

                            }
                            else
                            {
                                txtPassword.Text = "";
                                hideWaitModal();
                                showAlert(errorMsg, "content");
                            }
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message == ((int)DATABASE_ERRORS.USER_ISNOT_ADMIN).ToString())
                            {
                                bolIsAdmin = false;
                            }
                        }
                        if (!bolIsAdmin)
                        {
                            UserLogin userlogin = new UserLogin(false, txtCompanyId.Text, _userName, _password, "web", "", "", "");
                            userlogin.Process();

                            if (userlogin.StatusCode == 0)
                            {
                                rememberUserNameAndCompanyId();
                                hfs.Value = Utilities.EncryptString(
                                    userlogin.UserId +
                                    "," +
                                    userlogin.SessionId +
                                    "," +
                                    userlogin.AdminId +
                                    "," +
                                    userlogin.DBCompanyId +
                                     "," +
                                     userlogin.StrSubAdminName +
                                     "," +
                                     userlogin.StrSubAdminUserName
                                    );
                                hfbid.Value = userlogin.BrowserWindowId;
                                string ScriptToRun = @"xferBack();";
                                if (String.IsNullOrEmpty(hidQueryStrFromEmail.Value))
                                {
                                    hfp.Value = @"~/Home.aspx";
                                }
                                else
                                {
                                    string[] valuesFromQueryString = hidQueryStrFromEmail.Value.Split('&');
                                    hfp.Value = @"~/" + valuesFromQueryString[1].Substring(valuesFromQueryString[1].IndexOf('=') + 1) + ".aspx" + "?" + valuesFromQueryString[0];
                                }
                                ScriptManager.RegisterClientScriptBlock(TransferUpdatePanel, typeof(UpdatePanel), TransferUpdatePanel.ClientID, ScriptToRun, true);
                                return;
                            }
                            else
                            {
                                txtPassword.Text = "";
                                hideWaitModal();
                                showAlert(errorMsg, "content");
                            }

                        }
                    }
                    else
                    {
                        txtPassword.Text = "";
                        hideWaitModal();
                        showAlert(errorMsgCompanyBlocked, "content");
                    }
                }
                else
                {
                    txtPassword.Text = "";
                    hideWaitModal();
                    showAlert(errorMsg, "content");
                }
            }
            else
            {
                txtPassword.Text = "";
                hideWaitModal();
                showAlert(strMessage, "content");
            }
        }
        bool isCompanyBlocked(MFECompanyInfo cmpInfo)
        {
            if (!String.IsNullOrEmpty(cmpInfo.CompanyId))
            {
                if (cmpInfo.IsBlocked) return true;
                else return false;
            }
            else
            {
                return false;
            }

        }
        MFECompanyInfo getCompanyInfo()
        {
            GetCompanyDetails objCmpDtls = new
            GetCompanyDetails(txtCompanyId.Text);
            objCmpDtls.Process();
            if (objCmpDtls.StatusCode == 0)
            {
                return objCmpDtls.CmpInfo;
            }
            else
            {
                return new MFECompanyInfo();
            }
        }
        void hideWaitModal()
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "HideWaitModal", "hideWaitModal();", true);
        }
        public string validateForm()
        {
            string strMessage = "";
            if (txtCompanyId.Text.Trim() == "")
            {
                strMessage += "Please enter company id." + "<br />";
            }
            if (txtUserName.Text.Trim() == "")
            {
                strMessage += "Please enter username." + "<br />";
            }
            if (txtPassword.Text.Trim() == "")
            {
                strMessage += "Please enter password." + "<br />";
            }
            return strMessage;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            UserLogin(txtUserName.Text, txtPassword.Text);
        }

        protected void lnkForgotPassword_Click(object sender, EventArgs e)
        {
            Utilities.showModalPopup("divModalContainer", UpdatePanel1, "Reset password", "300", false);
            hideWaitModal();
        }

        void closeModalPopUp()
        {
            clearControlsForForgotPassword();
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "CloseModal", @"$('#divModalContainer').dialog('close')", true);
        }
        void showModalPopUp(string modalPopUpHeader)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "ShowPopUp", @"showModalPopUp('divModalContainer','" + modalPopUpHeader + "',400);", true);
        }
        void clearControlsForForgotPassword()
        {
            txtForgotPwdCompanyId.Text = "";
            txtForgotPwdUserName.Text = "";
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            string strMessage = ValidateResetPassword();
            if (String.IsNullOrEmpty(strMessage))
            {
                ResetPasword obj = new ResetPasword(
                    txtForgotPwdUserName.Text,
                    txtForgotPwdCompanyId.Text, this.Context);
                //obj.UserName = txtForgotPwdUserName.Text;
                //obj.CompanyID = txtForgotPwdCompanyId.Text;
                obj.Process();
                if (obj.StatusCode == 0)
                {
                    closeModalPopUp();
                    hideWaitModal();
                    Utilities.showMessage("Password has been reset.Please check your email for the new password", updModalContainer, DIALOG_TYPE.Info);
                }
                else
                {
                    showAlert("Invalid company name or username", "divForgotPwdAlertMsgs");
                    hideWaitModal();
                }
            }
            else
            {
                showAlert(strMessage, "divForgotPwdAlertMsgs");
                hideWaitModal();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            closeModalPopUp();
            hideWaitModal();
        }
        public string ValidateResetPassword()
        {
            string strMessage = "";
            if (txtForgotPwdCompanyId.Text.Trim() == "")
            {
                strMessage += "Please enter company id." + "<br />";
            }
            if (txtForgotPwdUserName.Text.Trim() == "")
            {
                strMessage += "Please enter user name." + "<br />";
            }

            return strMessage;
        }
        void rememberUserNameAndCompanyId()
        {
            HttpCookie myCookie = new HttpCookie(MficientConstants.COOKIE_MUEID);
            Response.Cookies.Remove(MficientConstants.COOKIE_MUEID);
            Response.Cookies.Add(myCookie);
            if (chkRememberUserName.Checked)
            {
                myCookie.Values.Add(MficientConstants.USER_NAME_IN_COOKIE, txtUserName.Text.ToString());
            }
            if (chkRememberCompanyId.Checked)
            {
                myCookie.Values.Add(MficientConstants.COMPANY_NAME_IN_COOKIE, txtCompanyId.Text.ToString());
            }
            myCookie.Expires = DateTime.Now.AddDays(15);
        }

    }
}
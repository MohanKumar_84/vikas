﻿<%@ Page Title="mFicient | Apps Statistics" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="WFStatisticsDetail.aspx.cs" Inherits="mFicientCP.WFStatisticsDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <link rel="stylesheet" href="css/themes/jquery.tipsy.css" id="tipsyLink" />
    <script src='Scripts/flot.js' type="text/javascript"></script>
    <script src='Scripts/wl_Chart.js' type="text/javascript"></script>
    <script type="text/javascript">
        function makeDatePickerWithMonthYear() {
            var datePickerFrom = document.getElementById('<%=txtFromDate.ClientID %>');
            var datePickerTo = document.getElementById('<%=txtToDate.ClientID %>');
            $(datePickerFrom).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                maxDate: "+0D",
                onClose: function (selectedDate) {
                    $(datePickerTo).datepicker("option", "minDate", selectedDate);
                }
            });
            //$.datepicker.formatDate('yy-mm-dd', new Date(2007, 1 - 1, 26));
            if (datePickerFrom) {
                stopDefualtOfDateText(datePickerFrom);
            }

            $(datePickerTo).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                maxDate: "+0D",
                onClose: function (selectedDate) {
                    $(datePickerFrom).datepicker("option", "maxDate", selectedDate);
                }
            });
            //$.datepicker.formatDate('yy-mm-dd', new Date(2007, 1 - 1, 26));
            if (datePickerTo) {
                stopDefualtOfDateText(datePickerTo);
            }
        }
        function setDateAfterPostBack() {
            var datePickerFrom = document.getElementById('<%=txtFromDate.ClientID %>');
            var datePickerTo = document.getElementById('<%=txtToDate.ClientID %>');
            var toCurrentDate = $(datePickerTo).datepicker("getDate");
            var fromCurrentDate = $(datePickerFrom).datepicker("getDate");
            if (toCurrentDate) {
                $(datePickerFrom).datepicker("option", "maxDate", toCurrentDate);
            }
            if (fromCurrentDate) {
                $(datePickerTo).datepicker("option", "minDate", fromCurrentDate);
            }
        }
        function stopDefualtOfDateText(txtDateField) {
            if (txtDateField) {
                txtDateField.addEventListener(
                                        'keydown', stopDefAction, false
                                    );
            }
        }
        function makeTheChart() {
            $('#' + '<%=divChartContainer.ClientID %>').find('table.chart').wl_Chart({
                tableBefore: true,
                hideTable: false,
                type: 'pie'
            });
        }
        function makeLegendTableIfOverFlowing() {
            var divSiblingOfTblLegend = $('.legend').children('div');
            if (divSiblingOfTblLegend) {
                if ($('.legend').children('div').height() > 300) {
                    var tblLegend = $('.legend').children('table');

                    if (tblLegend) {
                        $($(tblLegend))
                    .wrap($('<div style="height: 300px;opacity: 0.85;overflow: scroll;position: absolute;right: 5px;top: 5px;width: 130px;"></div>'));
                    }
                    $($(tblLegend)).removeAttr('style');
                    $($(tblLegend)).attr('style', 'font-size:smaller;color:#666;');
                    $($(divSiblingOfTblLegend)).removeAttr('style');
                }
            }
        }
        $(document).ready(function () { makeTheChart(); makeLegendTableIfOverFlowing(); });
    </script>
    <style type="text/css">
    .searchRow div.selector span
    {
        width:180px;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <div class="widget" id="divInfoAndSelectionWidget">
            <div class="customWidgetDirectDiv">
                <asp:UpdatePanel ID="updWFStatistics" runat="server">
                    <ContentTemplate>
                        <div id="divAlert">
                            <div id="divErrorAlert">
                            </div>
                            <div class="searchRow g12" style="padding: 0px;">
                                <div class="g12">
                                    <div>
                                        <asp:Label ID="lblForDdlMenu" runat="server" Text="Menu :" CssClass="label" AssociatedControlID="ddlMenu"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:DropDownList ID="ddlMenu" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Text="All" Value="ALL"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="g5">
                                    <div style="float: left;">
                                        <asp:Label ID="lblForFromDate" runat="server" Text="From :" CssClass="label" AssociatedControlID="txtFromDate"></asp:Label>
                                    </div>
                                    <div style="float: left; margin-left:4px;">
                                        <asp:TextBox ID="txtFromDate" runat="server" Width="200"></asp:TextBox></div>
                                </div>
                                <div class="g5">
                                    <div style="float: left;">
                                        <asp:Label ID="lblForToDate" runat="server" Text="To :" CssClass="label" AssociatedControlID="txtToDate"></asp:Label>
                                    </div>
                                    <div style="float: left; margin-left: 7px;">
                                        <asp:TextBox ID="txtToDate" runat="server" Width="200"></asp:TextBox></div>
                                </div>
                                <div class="g1" style="margin-left: 30px; text-align: right;">
                                    <asp:Button ID="btnShow" runat="server" Text="Show Statistics" OnClick="btnShow_Click" />
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div>
                                <div id="divChartContainer" style="margin: 5px;" runat="server">
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            hideWaitModal();
            isCookieCleanUpRequired('true');
            setDateAfterPostBack();
        }
    </script>
</asp:Content>

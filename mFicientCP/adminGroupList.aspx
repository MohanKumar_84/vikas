﻿<%@ Page Title="mFicient | Groups" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="adminGroupList.aspx.cs" Inherits="mFicientCP.adminGroupList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script type="text/javascript">
        var idPrependString = { "checkbox": "chkUser", "spanFirst": "lblFrstName", "spanLast": "lblLastName", "spanUserName": "lblUsrName", "a": "lnkUser" };
        var idOfTemplate = { "checkbox": "@checkbox", "spanFirstid": "@spanFirstid", "spanLastid": "@spanLastid", "spanUserNmid": "@spanUserNmid", "a": "@atag", "spanFirstInnerHtml": "@spanFirstInnerHtml", "spanLastInnerHtml": "@spanLastInnerHtml", "spanUsrNmInnerHtml": "@spanUserNmInnerHtml" };
        var templateForAddingUsers = "<div><span id=\"@spanFirstid\">@spanFirstInnerHtml</span>&nbsp;<span id=\"@spanLastid\">@spanLastInnerHtml</span>&nbsp;(&nbsp;<span id=\"@spanUserNmid\">@spanUserNmInnerHtml</span> &nbsp;)</div><div class=\"clear\"></div>";
        var templateForDeletingUsers = "<div><span id=\"@spanFirstid\">@spanFirstInnerHtml</span>&nbsp;<span id=\"@spanLastid\">@spanLastInnerHtml</span>&nbsp;(&nbsp;<span id=\"@spanUserNmid\">@spanUserNmInnerHtml</span> &nbsp;)</div><div class=\"clear\"></div>";

        // This is a factory function that returns a new range object.
        function UserDetail(userId, userName, firstName, lastName) {
            var User = Object.create(Object.prototype); ;
            User.userId = userId;
            User.userName = userName;
            User.firstName = firstName;
            User.lastName = lastName;
            // Finally return the new object
            return User;
        }
        //this function is used at starting when the html for combo box is formed
        function getExistingUsers() {
            var aryObjUserList = new Array();
            var hidExistingUsers = document.getElementById('<%=hidExistingUsers.ClientID %>');
            if (hidExistingUsers.value.toString().length > 0) {
                var aryUsersFromPostBack = hidExistingUsers.value.toString().split('|');
                for (var i = 0; i <= aryUsersFromPostBack.length - 1; i++) {
                    var aryUserIdandName = aryUsersFromPostBack[i].split(',');
                    var objUsers = UserDetail(aryUserIdandName[0], aryUserIdandName[1], aryUserIdandName[2], aryUserIdandName[3]);
                    aryObjUserList.push(objUsers);
                }
            }
            return aryObjUserList;
        }
        //this function is used at starting when the html for combo box is formed
        function getOtherAvailableUsers() {
            var aryObjUserList = new Array();
            var hidOtherAvailableUsers = document.getElementById('<%=hidOtherAvailableUsers.ClientID %>');
            if (hidOtherAvailableUsers.value.toString().length > 0) {
                var aryUsersFromPostBack = hidOtherAvailableUsers.value.toString().split('|');
                for (var i = 0; i <= aryUsersFromPostBack.length - 1; i++) {
                    var aryUserIdandName = aryUsersFromPostBack[i].split(',');
                    var objUsers = UserDetail(aryUserIdandName[0], aryUserIdandName[1], aryUserIdandName[2], aryUserIdandName[3]); ;
                    aryObjUserList.push(objUsers);
                }
            }
            return aryObjUserList
        }
        function formTheHTMLForComboBox() {
            var iCountOfLoop = 0;
            var strIdOfUICheckBox, strIdOfUISpan, strIdOfUIAnchor;
            var aryExistingUserList = getExistingUsers();
            var aryOtherAvailableUsers = getOtherAvailableUsers();
            if (aryExistingUserList && aryExistingUserList.length > 0) {
                for (var i = 0; i <= aryExistingUserList.length - 1; i++) {
                    var htmlToInsert = templateForAddingUsers;
                    strIdOfFrstNameSpan = idPrependString.spanFirst + "_" + iCountOfLoop;
                    strIdOfLstNameSpan = idPrependString.spanLast + "_" + iCountOfLoop;
                    strIdOfUserNmSpan = idPrependString.spanUserName + "_" + iCountOfLoop;
                    strIdOfUIAnchor = idPrependString.a + "_" + aryExistingUserList[i].userId + "_" + aryExistingUserList[i].userName + "_" + aryExistingUserList[i].firstName + "_" + aryExistingUserList[i].lastName + "_" + iCountOfLoop;
                    var finalHtmlToInsert = new String(htmlToInsert.replace(idOfTemplate.checkbox, strIdOfUICheckBox));
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.spanFirstid, strIdOfFrstNameSpan);
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.spanLastid, strIdOfLstNameSpan);
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.spanUserNmid, strIdOfUserNmSpan);
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.a, strIdOfUIAnchor);
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.spanFirstInnerHtml, aryExistingUserList[i].firstName);
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.spanLastInnerHtml, aryExistingUserList[i].lastName);
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.spanUsrNmInnerHtml, aryExistingUserList[i].userName);
                    $('#divExistingUsers').append(finalHtmlToInsert);
                    iCountOfLoop++;
                }
            }
            iCountOfLoop = 0;
            if (aryOtherAvailableUsers && aryOtherAvailableUsers.length > 0) {
                for (var i = 0; i <= aryOtherAvailableUsers.length - 1; i++) {
                    var htmlToInsert = templateForDeletingUsers;
                    strIdOfFrstNameSpan = idPrependString.spanFirst + "_" + iCountOfLoop;
                    strIdOfLstNameSpan = idPrependString.spanLast + "_" + iCountOfLoop;
                    strIdOfUserNmSpan = idPrependString.spanUserName + "_" + iCountOfLoop;
                    strIdOfUIAnchor = idPrependString.a + "_" + aryOtherAvailableUsers[i].userId + "_" + aryOtherAvailableUsers[i].userName + "_" + aryOtherAvailableUsers[i].firstName + "_" + aryOtherAvailableUsers[i].lastName + "_" + iCountOfLoop;
                    var finalHtmlToInsert = new String(htmlToInsert.replace(idOfTemplate.checkbox, strIdOfUICheckBox));
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.spanFirstid, strIdOfFrstNameSpan);
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.spanLastid, strIdOfLstNameSpan);
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.spanUserNmid, strIdOfUserNmSpan);
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.a, strIdOfUIAnchor);
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.spanFirstInnerHtml, aryOtherAvailableUsers[i].firstName);
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.spanLastInnerHtml, aryOtherAvailableUsers[i].lastName);
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.spanUsrNmInnerHtml, aryOtherAvailableUsers[i].userName);
                    $('#divOtherAvailableUsers').append(finalHtmlToInsert);
                    iCountOfLoop++;
                }
            }
        }
        //this is used for save button for final saving of the users
        function getExistingUsersForSaving() {
            var hidExistingUsers = document.getElementById('<%=hidExistingUsers.ClientID %>');
            hidExistingUsers.value = "";
            $('#divExistingUsers').each(function (index) {
                $(this).children().each(function (index) {
                    var anchorTag = $(this).children('a'); ;
                    if (!(anchorTag.length === 0)) {
                        if (hidExistingUsers) {
                            var aryAnchorTagValues = anchorTag.attr("id").split('_');
                            if (hidExistingUsers.value === "") {
                                hidExistingUsers.value = aryAnchorTagValues[1] + "," + aryAnchorTagValues[2] + "," + aryAnchorTagValues[3] + "," + aryAnchorTagValues[4];
                            }
                            else {
                                hidExistingUsers.value += "|" + aryAnchorTagValues[1] + "," + aryAnchorTagValues[2] + "," + aryAnchorTagValues[3] + "," + aryAnchorTagValues[4];
                            }
                        }
                    }
                });
            });
        }
        //this is used for save button for final saving of the users
        function getOtherAvailableUsersForSaving() {
            var hidOtherExistingUsers = document.getElementById('<%=hidOtherAvailableUsers.ClientID %>');
            hidOtherExistingUsers.value = "";
            $('#divOtherAvailableUsers').each(function (index) {
                $(this).children().each(function (index) {
                    var anchorTag = $(this).children('a'); ;
                    if (!(anchorTag.length === 0)) {
                        var aryAnchorTagValues = anchorTag.attr("id").split('_');
                        if (hidOtherExistingUsers) {
                            if (hidOtherExistingUsers.value === "") {
                                hidOtherExistingUsers.value = aryAnchorTagValues[1] + "," + aryAnchorTagValues[2] + "," + aryAnchorTagValues[3] + "," + aryAnchorTagValues[4];
                            }
                            else {
                                hidOtherExistingUsers.value += "|" + aryAnchorTagValues[1] + "," + aryAnchorTagValues[2] + "," + aryAnchorTagValues[3] + "," + aryAnchorTagValues[4];
                            }
                        }
                    }
                });
            });
        }
        //this is used in the a tag of the div 
        function moveUserToOtherAvailableUser(anchorTag) {
            var divToTransfer = anchorTag.parentElement;
            var divClearSibling = undefined;
            if (divToTransfer.nextElementSibling)
                divClearSibling = divToTransfer.nextElementSibling;
            $(anchorTag).attr("onclick", "moveUserToExistingUser(this)");
            $(anchorTag).attr("class", "add");
            var chkbox = $(divToTransfer).find('input:checkbox');
            var chkBoxParent = $(chkbox).parent();
            if ($(chkBoxParent).is("span")) {
                chkBoxParent.removeAttr('class');
            }
            $('#divOtherAvailableUsers').append(divToTransfer);
            if (divClearSibling != undefined)
                $('#divOtherAvailableUsers').append(divClearSibling);

        }
        //this is used in the a tag of the div
        function moveUserToExistingUser(anchorTag) {
            var divToTransfer = anchorTag.parentElement;
            var divClearSibling = undefined;
            if (divToTransfer.nextElementSibling)
                divClearSibling = divToTransfer.nextElementSibling;
            $(anchorTag).attr("onclick", "moveUserToOtherAvailableUser(this)");
            $(anchorTag).attr("class", "remove");
            var chkbox = $(divToTransfer).find('input:checkbox');
            var chkBoxParent = $(chkbox).parent();
            if ($(chkBoxParent).is("span")) {
                chkBoxParent.removeAttr('class');
            }
            $('#divExistingUsers').append(divToTransfer);
            if (divClearSibling != undefined)
                $('#divExistingUsers').append(divClearSibling);

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <div style="margin: auto; width: 500px;">
                    <asp:UpdatePanel ID="updRepeater" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlGroupRptHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblGroupRptHeader" runat="server" Text="<h1>Group List </h1>"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptGroupDtls" runat="server" Visible="true">
                                    <HeaderTemplate>
                                        <table class="repeaterTable rptWithoutImageAndButton rowClickable" style="width: 98%">
                                            <thead>
                                                <tr>
                                                    <th style="display: none">
                                                        GROUP ID
                                                    </th>
                                                    <th>
                                                        Group Name
                                                    </th>
                                                    <th>
                                                        No Of Users
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem" onclick="rptRowClick(this,0);">
                                                <td style="display: none" title="id">
                                                    <asp:Label ID="lblGroupId" runat="server" Text='<%#Eval("GROUP_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblGrpName" runat="server" Text='<%# Eval("GROUP_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblGroupName" runat="server" Text='<%# Eval("GROUP_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblUsersCount" runat="server" Text='<%# Eval("USER_COUNT") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem" onclick="rptRowClick(this,0);">
                                                <td style="display: none" title="id">
                                                    <asp:Label ID="lblGroupId" runat="server" Text='<%#Eval("GROUP_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblGrpName" runat="server" Text='<%# Eval("GROUP_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblGroupName" runat="server" Text='<%# Eval("GROUP_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblUsersCount" runat="server" Text='<%# Eval("USER_COUNT") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                            <div class="clear">
                            </div>
                            <asp:Panel ID="pnlEditGroup" runat="server" Visible="false">
                            </asp:Panel>
                            <div id="updRepeaterHidVal">
                                <asp:HiddenField ID="hidExistingUsers" runat="server" />
                                <asp:HiddenField ID="hidOtherAvailableUsers" runat="server" />
                                <asp:HiddenField ID="hfdGroupId" runat="server" />
                                <asp:HiddenField ID="hidRowValuesUsedAfterPostBack" runat="server" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </section>
    </div>
    <div id="divAddRemoveUsers" style="display: none;width : 200px;">
        <asp:UpdatePanel runat="server" ID="updAddRemoveUsers" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divAddRemoveUsersDelete" style="float: right">
                </div>
                <div class="clear" style="height: 0px;">
                </div>
                <div id="divAddRemoveUsersError">
                </div>
                <div style="margin-bottom: 10px;">
                    <div style="float: left; margin-right: 10px; margin-left: 30px; position: relative;
                        top: 5px;">
                        <asp:Label ID="lblGroupName" runat="server" Text="Name :"></asp:Label>
                   
                        <asp:Label ID="lblGroupNme" runat="server"></asp:Label>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div style="margin-bottom: 20px;">
                    <div class="comboselectbox searchable" style="height: 250px; margin-left: 30px;">
                        <div class="comwraper">
                            <div id="divExistingUsers" class="comboRepeater">
                                <div class="comboHeader">
                                    <h5>
                                        Users in Group</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div style="text-align: center">
                        <asp:Button ID="btnOK" runat="server" Text="OK" OnClick="btnOK_Click" OnClientClick="$('#divAddRemoveUsers').dialog('close');return false;" />&nbsp;&nbsp;
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="$('#divAddRemoveUsers').dialog('close');return false;" />&nbsp;&nbsp;
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div id="divWaitBox" class="waitModal">
            <div id="WaitAnim">
                <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                    AlternateText="Please Wait" BorderWidth="0px" />
            </div>
        </div>
    </div>
    <div>
        <asp:UpdatePanel runat="server" ID="UpdatePanel11" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRowClickPostBack" EventName="Click" />
            </Triggers>
            <ContentTemplate>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div>
        <asp:Button ID="btnRowClickPostBack" runat="server" OnClick="btnRowClickPostBack_Click"
            Width="1px" Height="1px" Style="display: none;" />
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>

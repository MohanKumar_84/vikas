﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;
namespace mFicientCP
{
    public partial class groupList : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        string hfsPart5 = string.Empty;
        string hfsPart6 = string.Empty;


        #region ID PREPEND STRING CONSTANTS FOR FORMING HTML USING HTML WRITER

        const string SPAN_FRST_NAME_ID_PREPEND_STRING = @"lblFrstName";
        const string SPAN_LAST_NAME_ID_PREPEND_STRING = @"lblLastName";
        const string SPAN_USER_NAME_ID_PREPEND_STRING = @"lblUsrName";
        const string LINK_ADD_REMOVE_ID_PREPEND_STRING = @"lnkUser";
        const string SPAN_WORKFLOW_NAME_ID_PREPEND_STRING = @"WF_NAME";
        const string SPAN_WORKFLOW_ID_PREPEND_STRING = "@WF_ID";

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            Literal ltFullNAME = (Literal)this.Master.Master.FindControl("ltFullNAME");
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;
            int lengthA = hfsParts.Length;
            if (lengthA > 4)
            {
                hfsPart5 = hfsParts[4];
                hfsPart6 = hfsParts[5];
                context.Items["hfs5"] = hfsPart5;
                context.Items["hfs6"] = hfsPart6;
                ltFullNAME.Text = hfsPart5 + " (" + hfsPart6 + ")";
            }
            if (!Page.IsPostBack)
            {
                if (hfsValue != string.Empty)
                    BindGroupsRepeater();
                btnUpdateGroupName.Text = "Save";
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    BindGroupsRepeater();
                    btnUpdateGroupName.Text = "Save";
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, String.Empty);
                    return;
                }
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, "makeTabAfterPostBack();");
            }

        }
        protected void BindGroupsRepeater()
        {
            GetSubAdminGroups getSubadminGroups = new GetSubAdminGroups(hfsPart4, strUserId);
            getSubadminGroups.ProcessByCompanyId();//show all the group to any sub admin of the company.
            rptGroupDtls.DataSource = getSubadminGroups.GroupDetails;
            rptGroupDtls.DataBind();
        }

        protected void btnUpdateGroupName_Click(object sender, EventArgs e)
        {
            if (txtGroup.Text == "")
            {
                Utilities.showAlert("Please enter a valid group name.</br>Min 3 Characters , No numbers are allowed.", "divGroupAdd", upAddEditModel, "Error Text");
                BindGroupsRepeater();
                upAddEditModel.Update();
                return;
            }
            if (txtGroup.Text != "")
            {
                if (!Utilities.IsValidString(txtGroup.Text, true, true, false, " ", 3, 50, false, false))
                {
                    Utilities.showAlert("Please enter a valid group name.</br>Min 3 Characters , No numbers are allowed.", "divGroupAdd", upAddEditModel, "Error Text");
                    BindGroupsRepeater();
                    upAddEditModel.Update();
                    return;
                }
                if (btnUpdateGroupName.Text == "Update")
                {
                    UpdateGroup updateGroup = new UpdateGroup();
                    updateGroup.GroupId = hfdGroupId.Value;
                    updateGroup.SubAdminId = strUserId;
                    updateGroup.GroupName = txtGroup.Text;
                    updateGroup.CompanyId = hfsPart4;
                    updateGroup.OldGroupName = hidgroupid.Value;
                    updateGroup.Process();
                    if (updateGroup.StatusCode == 0)
                    {
                        Utilities.showMessage("Group Name updated", upAddEditModel, DIALOG_TYPE.Info);
                    }
                    else
                    {
                        Utilities.showMessage("Internal server error", upAddEditModel, DIALOG_TYPE.Error);
                        return;
                    }
                }
                else
                {
                    AddGroup addUserGroup = new AddGroup();
                    addUserGroup.GroupName = txtGroup.Text;
                    addUserGroup.SubAdminId = strUserId;
                    addUserGroup.CompanyId = hfsPart4;
                    addUserGroup.Process();
                    if (addUserGroup.StatusCode != 0)
                    {
                        Utilities.showMessage("Internal server error", upAddEditModel, DIALOG_TYPE.Error);
                        return;
                    }
                }
                btnUpdateGroupName.Text = "Save";
                BindGroupsRepeater();
                Utilities.closeModalPopUp("AddEditModel", upAddEditModel);

            }
        }
        protected void btnUpdateGroupName1_Click(object sender, EventArgs e)
        {

            if (txtGroup1.Text != "")
            {
                if (!Utilities.IsValidString(txtGroup1.Text, true, true, false, " ", 3, 50, false, false))
                {
                    Utilities.showAlert("Please enter a valid group name.</br>Min 3 Characters , No numbers are allowed.", "div1", UpdateEditModel1, "Error Text");
                    UpdateEditModel1.Update();
                    return;
                }
                else
                {
                    UpdateGroup updateGroup = new UpdateGroup();
                    updateGroup.GroupId = hidgroupname.Value;
                    updateGroup.SubAdminId = strUserId;
                    updateGroup.GroupName = txtGroup1.Text;
                    updateGroup.CompanyId = hfsPart4;
                    updateGroup.OldGroupName = hidgroupid.Value;
                    updateGroup.Process();
                    if (updateGroup.StatusCode == 0)
                    {
                        BindGroupsRepeater();

                        Utilities.showMessage("Group Name updated", UpdateEditModel1, DIALOG_TYPE.Info);
                        UpdateEditModel1.Update();
                    }
                    else
                    {
                        Utilities.showMessage("Internal server error", UpdateEditModel1, DIALOG_TYPE.Error);
                        return;
                    }

                    Utilities.closeModalPopUp("UpdateEditModel", UpdateEditModel1);

                }
            }

        }

        protected void rptAddUsers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "AddUser":
                    processAddUser(e);
                    break;
            }
        }



        void processAddUser(RepeaterCommandEventArgs e)
        {
            Label lblUserID = (Label)e.Item.FindControl("lblUserID");

        }

        DataTable getGroupUsersFromHiddenField()
        {
            DataTable dtblUsersList = new DataTable();
            dtblUsersList.Columns.Add("USER_ID", typeof(string));
            dtblUsersList.Columns.Add("USER_NAME", typeof(string));
            dtblUsersList.Columns.Add("FIRST_NAME", typeof(string));
            dtblUsersList.Columns.Add("LAST_NAME", typeof(string));

            DataRow row = null;
            if (!String.IsNullOrEmpty(hidExistingUsers.Value))
            {
                string[] userIdsToSave = hidExistingUsers.Value.Split('|');
                //If HidExistingUsers has value then hidAllUsers should have value as well.
                UsersOfCompanyForJson objAllUsersOfCmp = Utilities.DeserialiseJson<UsersOfCompanyForJson>(hidAllUsersListInCmp.Value);
                List<UserDtlForJson> lstUserDtls = objAllUsersOfCmp.usrs;
                if (userIdsToSave.Length > 0)
                {
                    for (int i = 0; i <= userIdsToSave.Length - 1; i++)
                    {
                        row = dtblUsersList.NewRow();
                        //string[] aryUserDtl = userIdsToSave[i].Split(',');
                        string userId = userIdsToSave[i];
                        row["USER_ID"] = userId;
                        UserDtlForJson objUserDtl = lstUserDtls.Find(usr => usr.id == userId);
                        //row["USER_NAME"] = aryUserDtl[1];
                        //row["FIRST_NAME"] = aryUserDtl[2];
                        //row["LAST_NAME"] = aryUserDtl[3];

                        row["USER_NAME"] = objUserDtl.usrnm;
                        row["FIRST_NAME"] = objUserDtl.frstName;
                        row["LAST_NAME"] = objUserDtl.lstName;

                        dtblUsersList.Rows.Add(row);
                    }
                }
            }
            return dtblUsersList;
        }
        DataTable getOtherUsersFromHiddenField()
        {
            DataTable dtblUsersList = new DataTable();
            dtblUsersList.Columns.Add("USER_ID", typeof(string));
            dtblUsersList.Columns.Add("USER_NAME", typeof(string));
            dtblUsersList.Columns.Add("FIRST_NAME", typeof(string));
            dtblUsersList.Columns.Add("LAST_NAME", typeof(string));


            DataRow row = null;
            if (!String.IsNullOrEmpty(hidOtherAvailableUsers.Value))
            {
                string[] userIdsToRemove = hidOtherAvailableUsers.Value.Split('|');
                //If HidExistingUsers has value then hidAllUsers should have value as well.
                UsersOfCompanyForJson objAllUsersOfCmp = Utilities.DeserialiseJson<UsersOfCompanyForJson>(hidAllUsersListInCmp.Value);
                List<UserDtlForJson> lstUserDtls = objAllUsersOfCmp.usrs;
                if (userIdsToRemove.Length > 0)
                {
                    for (int i = 0; i <= userIdsToRemove.Length - 1; i++)
                    {
                        //string[] aryUserDtl = userIdsToRemove[i].Split(',');
                        string userId = userIdsToRemove[i];
                        row = dtblUsersList.NewRow();
                        row["USER_ID"] = userId;
                        UserDtlForJson objUserDtl = lstUserDtls.Find(usr => usr.id == userId);
                        row["USER_NAME"] = objUserDtl.usrnm;
                        row["FIRST_NAME"] = objUserDtl.frstName;
                        row["LAST_NAME"] = objUserDtl.lstName;
                        dtblUsersList.Rows.Add(row);
                    }
                }
            }
            return dtblUsersList;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string strDeleteingapp = GetDeletedList(hidexistinguseringroup.Value, hidaddnewuseringroup.Value);
            string straddnewapps = hidaddnewuseringroup.Value;
            DataTable dtblGrpUsersFromHiddenField = new DataTable(), dtblOtherUsersFromHiddenField = new DataTable();
            try
            {
                dtblGrpUsersFromHiddenField = getGroupUsersFromHiddenField();
                dtblOtherUsersFromHiddenField = getOtherUsersFromHiddenField();
            }
            catch
            {
                //if the datatable is not formed from hid field create combo box again.
                processShowAddRemoveUserPopUp(hfdGroupId.Value, txtGroupName.Text, hidGrpSubAdminId.Value);
                Utilities.showMessage("Internal server error.Please try again", updAddRemoveUsers, "message", DIALOG_TYPE.Error);
                return;
            }
            try
            {
                if (txtGroupName.Text == "")
                {
                    Utilities.showAlert("Please enter group name.", "divAddRemoveUsersError", updAddRemoveUsers, "Error Text");
                    showTheHTMLComboBoxAfterPostbackForUser(dtblGrpUsersFromHiddenField, dtblOtherUsersFromHiddenField);
                    return;
                }
                else
                {
                    if (!Utilities.IsValidString(txtGroupName.Text, true, true, false, " ", 3, 50, false, false))
                    {
                        Utilities.showAlert("Please enter a valid group name.</br>Min 3 Characters , No numbers are allowed.", "divAddRemoveUsersError", updAddRemoveUsers, "Error Text");
                        showTheHTMLComboBoxAfterPostbackForUser(dtblGrpUsersFromHiddenField, dtblOtherUsersFromHiddenField);
                        return;
                    }

                    UpdateGroupDetailsNameAndUsers objUpdateGroupDetails = new UpdateGroupDetailsNameAndUsers(hfdGroupId.Value, txtGroupName.Text.Trim(),
                        dtblGrpUsersFromHiddenField, dtblOtherUsersFromHiddenField, strUserId, hfsPart4, hidGrpSubAdminId.Value, straddnewapps, strDeleteingapp);
                    objUpdateGroupDetails.Process();

                    if (objUpdateGroupDetails.StatusCode == 0)
                    {
                        Utilities.closeModalPopUp("divAddRemoveUsers", updAddRemoveUsers);
                        Utilities.showMessage("Group details updated successfully", updAddRemoveUsers, "message", DIALOG_TYPE.Info);
                        setHidGroupId(String.Empty);
                        setHidGrpSubAdmin(String.Empty);
                        BindGroupsRepeater();
                    }
                    else
                    {
                        Utilities.showMessage("Internal server error.Please try again", updAddRemoveUsers, "message", DIALOG_TYPE.Error);
                        showTheHTMLComboBoxAfterPostbackForUser(dtblGrpUsersFromHiddenField, dtblOtherUsersFromHiddenField);
                    }
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error.Please try again", updAddRemoveUsers, "message", DIALOG_TYPE.Error);
                showTheHTMLComboBoxAfterPostbackForUser(dtblGrpUsersFromHiddenField, dtblOtherUsersFromHiddenField);
            }
            setHidGroupUsersUsedAfterPostBack(String.Empty);
            setHidOtherUsersUsedAfterPostBack(String.Empty);
        }


        void showTheHTMLComboBoxAfterPostbackForUser(DataTable selectedUsersForGroup, DataTable unselectedUsers)
        {
            formTheHTMLForMultiSelectListBoxes(selectedUsersForGroup, unselectedUsers);

            ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "Html Combo box Rendering", "$(\"input\").uniform();", true);

        }
        protected void btnRowClickPostBack_Click(object sender, EventArgs e)
        {
            if (hidRowValuesUsedAfterPostBack.Value.Trim().Length != 0)
            {
                //values groupId,groupName
                string[] aryValuesFromHidField = hidRowValuesUsedAfterPostBack.Value.Split(';');
                processShowAddRemoveUserPopUp(aryValuesFromHidField[0], aryValuesFromHidField[1], aryValuesFromHidField[2]);
                setHidRowValuesAfterPostBack(String.Empty);

            }

        }
        //protected void btnDeleteGroup_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DeleteGroupandGroupUsers deleteGroup = new DeleteGroupandGroupUsers();
        //        deleteGroup.GroupId = hfdGroupId.Value;
        //        deleteGroup.SubAdminIdWhoCreatedGrp = hidGrpSubAdminId.Value;
        //        deleteGroup.SubAdminIdDeletingGrp = strUserId;
        //        deleteGroup.CompanyId = hfsPart4;
        //        deleteGroup.GroupName = hidInitialGrpNameInEditMode.Value;
        //        deleteGroup.DeleteGroup();
        //        if (deleteGroup.StatusCode == 0)
        //        {
        //            BindGroupsRepeater();
        //            pnlRepeaterBox.Visible = true;
        //            pnlEditGroup.Visible = false;
        //            setHidGroupUsersUsedAfterPostBack(String.Empty);
        //            setHidOtherUsersUsedAfterPostBack(String.Empty);
        //            setHidGrpSubAdmin(String.Empty);
        //            setHidGroupId(String.Empty);
        //            setHidInitialGrpName(String.Empty);
        //            Utilities.closeModalPopUp("divAddRemoveUsers", updAddRemoveUsers);
        //            Utilities.showMessage("Deleted successfully", updAddRemoveUsers, "message", DIALOG_TYPE.Info);
        //        }
        //        else
        //        {
        //            throw new Exception("Internal server error");
        //        }
        //    }
        //    catch
        //    {
        //        showTheHTMLComboBoxAfterPostback(getGroupUsersFromHiddenField(), getOtherUsersFromHiddenField());
        //        Utilities.showMessage("Internal server error", updAddRemoveUsers, DIALOG_TYPE.Error);
        //    }
        //}

        /// <summary>
        /// Shows the Pop Up For editing Group details
        /// Forms the html for combo boxes
        /// set the hid field Group Id
        /// And Shows the group name in the pop up.
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="groupName"></param>
        void processShowAddRemoveUserPopUp(string groupId, string groupName, string grpSubAdminId)
        {

            try
            {
                DataTable dtblUsersInGroup = new DataTable(),
                    dtblOtherAvailableUsers = new DataTable();
                GetGroupAndAppDetails objGetGroupAndAppDetails = new GetGroupAndAppDetails(groupId, hfsPart4, strUserId);
                objGetGroupAndAppDetails.Process();
                if (objGetGroupAndAppDetails.StatusCode == 0)
                {
                    formTheHTMLForMultiSelectListBoxesForApp(objGetGroupAndAppDetails.ResultTables.Tables[0], objGetGroupAndAppDetails.ResultTables.Tables[1]);

                }

                else
                {
                    throw new Exception();
                }

                GetUsersWithGroupDtlsByCmpId objUserDtl =
                    new GetUsersWithGroupDtlsByCmpId(hfsPart4);
                objUserDtl.Process(groupId, out dtblUsersInGroup, out dtblOtherAvailableUsers);
                setAllUsersDtlOfCmpJsonInHidField(dtblUsersInGroup, dtblOtherAvailableUsers);
                if (objUserDtl.StatusCode == 0)
                {
                    formTheHTMLForMultiSelectListBoxes(dtblUsersInGroup, dtblOtherAvailableUsers);
                    txtGroupName.Text = groupName;
                    hfdGroupId.Value = groupId;
                    hidInitialGrpNameInEditMode.Value = groupName;
                    hidGrpSubAdminId.Value = grpSubAdminId;

                    Utilities.showModalPopup("divAddRemoveUsers", updRepeater, "Group : " + groupName, "750", true, false);

                    updAddRemoveUsers.Update();
                    updSubProcCat.Update();
                }
                else throw new Exception();

            }
            catch
            {
                Utilities.showMessage("Internal server error",
                    updRepeater, DIALOG_TYPE.Error);
            }

        }
        void setAllUsersDtlOfCmpJsonInHidField(DataTable usersInGroup, DataTable otherAvailableUsers)
        {
            if (usersInGroup == null || otherAvailableUsers == null) throw new ArgumentNullException();
            DataTable dtblAllUsersInCmp = new DataTable();

            dtblAllUsersInCmp.Columns.Add("USER_ID", typeof(string));
            dtblAllUsersInCmp.Columns.Add("USER_NAME", typeof(string));
            dtblAllUsersInCmp.Columns.Add("FIRST_NAME", typeof(string));
            dtblAllUsersInCmp.Columns.Add("LAST_NAME", typeof(string));

            DataRow row = null;

            foreach (DataRow usr in usersInGroup.Rows)
            {
                row = dtblAllUsersInCmp.NewRow();
                row["USER_ID"] = Convert.ToString(usr["USER_ID"]);
                row["USER_NAME"] = Convert.ToString(usr["USER_NAME"]);
                row["FIRST_NAME"] = Convert.ToString(usr["FIRST_NAME"]);
                row["LAST_NAME"] = Convert.ToString(usr["LAST_NAME"]);
                dtblAllUsersInCmp.Rows.Add(row);
            }
            foreach (DataRow usr in otherAvailableUsers.Rows)
            {
                row = dtblAllUsersInCmp.NewRow();
                row["USER_ID"] = Convert.ToString(usr["USER_ID"]);
                row["USER_NAME"] = Convert.ToString(usr["USER_NAME"]);
                row["FIRST_NAME"] = Convert.ToString(usr["FIRST_NAME"]);
                row["LAST_NAME"] = Convert.ToString(usr["LAST_NAME"]);
                dtblAllUsersInCmp.Rows.Add(row);
            }
            if (dtblAllUsersInCmp.Rows.Count > 0)
            {
                UsersOfCompanyForJson objUsersOfCmp = new UsersOfCompanyForJson();
                List<UserDtlForJson> lstUsrDtl = new List<UserDtlForJson>();
                foreach (DataRow usr in dtblAllUsersInCmp.Rows)
                {
                    UserDtlForJson objUserDtl = new UserDtlForJson();
                    objUserDtl.id = Convert.ToString(usr["USER_ID"]);
                    objUserDtl.usrnm = Convert.ToString(usr["USER_NAME"]);
                    objUserDtl.frstName = Convert.ToString(usr["FIRST_NAME"]);
                    objUserDtl.lstName = Convert.ToString(usr["LAST_NAME"]);
                    lstUsrDtl.Add(objUserDtl);
                }
                objUsersOfCmp.usrs = lstUsrDtl;
                hidAllUsersListInCmp.Value = Utilities.SerializeJson<UsersOfCompanyForJson>(objUsersOfCmp);
            }
            else
            {
                hidAllUsersListInCmp.Value = "";
            }
        }
        void formTheHTMLForMultiSelectListBoxes(DataTable usersInGroupDtbl, DataTable otherUserAvailableDtbl)
        {
            try
            {
                formHTMLForUsersInGroup(usersInGroupDtbl);
                formHTMLForUsersNotInGroup(otherUserAvailableDtbl);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void formHTMLForUsersInGroup(DataTable usersInGroupDtbl)
        {
            if (usersInGroupDtbl == null) throw new ArgumentNullException();
            /***
             * Required html
             * <div class="comboHeader">
                <h5>
                    Users in Group</h5>
                </div>
             * <div>
                <input type=\"checkbox\" id=\"@checkbox\">
                <span id=\"@spanFirstid\">@spanFirstInnerHtml</span>&nbsp;
                <span id=\"@spanLastid\">@spanLastInnerHtml</span>&nbsp;(&nbsp;<span id=\"@spanUserNmid\">@spanUserNmInnerHtml</span> &nbsp;)
                <a class=\"remove\" id=\"@atag\" onclick=\"moveUserToOtherAvailableUser(this);\"></a>
                </div>
                <div class=\"clear\"></div>
             ***/
            StringWriter stringWriter = new StringWriter();
            using (HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter))
            {
                //header part
                htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "comboHeader");
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
                htmlWriter.Write("Assigned Users");
                htmlWriter.RenderEndTag();
                htmlWriter.RenderEndTag();

                //User List
                int iLoopCount = 0;
                StringBuilder sb = new StringBuilder();
                foreach (DataRow row in usersInGroupDtbl.Rows)
                {
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                    //Check box


                    //first span tag for first name
                    sb = new StringBuilder();
                    sb.Append(SPAN_FRST_NAME_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["FIRST_NAME"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");
                    //second span tag for last name
                    sb = new StringBuilder();
                    sb.Append(SPAN_LAST_NAME_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["LAST_NAME"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;(&nbsp;");

                    //Third span tag userName And Id span
                    sb = new StringBuilder();
                    sb.Append(SPAN_USER_NAME_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "spantesting");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["USER_NAME"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;)&nbsp;");

                    //A tag
                    sb = new StringBuilder();
                    sb.Append(LINK_ADD_REMOVE_ID_PREPEND_STRING).Append("|");
                    sb.Append(Convert.ToString(row["USER_ID"])).Append("|").Append(iLoopCount);

                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "remove");
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Onclick, "moveUserToOtherAvailableUser(this);");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.A);
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");

                    htmlWriter.RenderEndTag();

                    //div clear 
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "clear");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                    htmlWriter.RenderEndTag();
                    iLoopCount++;
                    // string Check=Convert.ToString(row["USER_NAME"]);
                    hidexistinguseringroup.Value = "";
                    if (Convert.ToString(row["USER_NAME"]) != "")
                    {
                        if (hidexistinguseringroup.Value == "") hidexistinguseringroup.Value = Convert.ToString(row["USER_NAME"]);
                        else hidexistinguseringroup.Value += "," + Convert.ToString(row["USER_NAME"]);
                    }
                }
                divExistingUsers.InnerHtml = stringWriter.ToString();

            }

        }
        void formHTMLForUsersNotInGroup(DataTable usersNotInGroupDtbl)
        {

            if (usersNotInGroupDtbl == null) throw new ArgumentNullException();

            StringWriter stringWriter = new StringWriter();
            using (HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter))
            {
                //header part
                htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "comboHeader");
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
                htmlWriter.Write("Available Users");
                htmlWriter.RenderEndTag();
                htmlWriter.RenderEndTag();

                //User List
                int iLoopCount = 0;
                StringBuilder sb = new StringBuilder();
                foreach (DataRow row in usersNotInGroupDtbl.Rows)
                {
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);

                    //first span tag for first name
                    sb = new StringBuilder();
                    sb.Append(SPAN_FRST_NAME_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["FIRST_NAME"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");

                    //second span tag for last name
                    sb = new StringBuilder();
                    sb.Append(SPAN_LAST_NAME_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["LAST_NAME"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;(&nbsp;");

                    //Third span tag userName And Id span
                    sb = new StringBuilder();
                    sb.Append(SPAN_USER_NAME_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "spantesting");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["USER_NAME"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;)&nbsp;");

                    //A tag
                    sb = new StringBuilder();
                    sb.Append(LINK_ADD_REMOVE_ID_PREPEND_STRING).Append("|");
                    sb.Append(Convert.ToString(row["USER_ID"])).Append("|").Append(iLoopCount);

                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "add");
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Onclick, "moveUserToExistingUser(this);");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.A);
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");

                    htmlWriter.RenderEndTag();

                    //div clear 
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "clear");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                    htmlWriter.RenderEndTag();
                    iLoopCount++;
                }

                //insert the html
                divOtherAvailableUsers.InnerHtml = stringWriter.ToString();
            }
        }
        #region Set Value Of Controls
        void setHidGroupId(string value)
        {
            hfdGroupId.Value = value;
        }
        void setHidRowValuesAfterPostBack(string value)
        {
            hidRowValuesUsedAfterPostBack.Value = value;
        }
        void setHidGroupUsersUsedAfterPostBack(string value)
        {
            hidExistingUsers.Value = value;
        }
        void setHidOtherUsersUsedAfterPostBack(string value)
        {
            hidOtherAvailableUsers.Value = value;
        }
        void setHidGrpSubAdmin(string value)
        {
            hidGrpSubAdminId.Value = value;
        }
        void setHidInitialGrpName(string value)
        {
            hidGrpSubAdminId.Value = value;
        }
        #endregion

        protected void rptGroupDtls_ItemCommand(object source, RepeaterCommandEventArgs e)
        {


            switch (e.CommandName)
            {
                case "Delete":
                    Label lab_GId = (Label)e.Item.FindControl("lblGroupId");
                    Label lab_GroupName = (Label)e.Item.FindControl("lblGrpName");
                    ProcessDeleteGroup(lab_GId.Text, hfsPart4, strUserId, lab_GroupName.Text);
                    break;
                case "Discard":
                    break;
            }
        }

        private void ProcessDeleteGroup(string GroupId, string CompanyId, string SubadminId, string groupName)
        {
            DeleteGroupViaGroupId deleteGroup = new DeleteGroupViaGroupId(GroupId, CompanyId);
            deleteGroup.SubAdminIdDeletingGrp = SubadminId;
            deleteGroup.GroupName = groupName;
            deleteGroup.Process();
            {

                if (deleteGroup.StatusCode == 0)
                {
                    BindGroupsRepeater();
                    Utilities.showMessage("Delete Group successfully", updRepeater, DIALOG_TYPE.Info);
                }
                else
                {
                    Utilities.showMessage("Internal server error.", updRepeater, DIALOG_TYPE.Error);
                }

            }
        }

        void formHTMLForAppInGroup(DataTable usersInGroupDtbl)
        {
            if (usersInGroupDtbl == null) throw new ArgumentNullException();

            StringWriter stringWriter = new StringWriter();
            using (HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter))
            {
                //header part
                htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "comboHeader");
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);
                htmlWriter.Write("Available Apps");
                htmlWriter.RenderEndTag();
                htmlWriter.RenderEndTag();

                //User List
                int iLoopCount = 0;
                StringBuilder sb = new StringBuilder();
                foreach (DataRow row in usersInGroupDtbl.Rows)
                {
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);

                    //first span tag for WorkFlow name
                    sb = new StringBuilder();
                    sb.Append(SPAN_WORKFLOW_NAME_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "spantestingdetails");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["WF_NAME"]));
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");

                    //Second span tag WorkFlow ID span
                    sb = new StringBuilder();
                    sb.Append(SPAN_WORKFLOW_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Style, "display:none");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["WF_ID"]));
                    htmlWriter.RenderEndTag();

                    sb = new StringBuilder();
                    sb.Append(LINK_ADD_REMOVE_ID_PREPEND_STRING).Append("_");
                    sb.Append(Convert.ToString(row["WF_NAME"])).Append("_");
                    sb.Append(Convert.ToString(row["WF_ID"])).Append("_");
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "add");
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Onclick, "moveUserToOtherAvailableUser1(this);");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.A);
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");
                    htmlWriter.RenderEndTag();
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "clear");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                    htmlWriter.RenderEndTag();

                    iLoopCount++;
                }
                divavilableapps.InnerHtml = String.Empty;
                divavilableapps.InnerHtml = stringWriter.ToString();
            }

        }

        void formHTMLForAppNotInGroup(DataTable usersNotInGroupDtbl)
        {

            if (usersNotInGroupDtbl == null) throw new ArgumentNullException();

            StringWriter stringWriter = new StringWriter();
            using (HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter))
            {
                //header part
                htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "comboHeader");
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.H3);

                htmlWriter.Write("Assigned Apps");

                htmlWriter.RenderEndTag();
                htmlWriter.RenderEndTag();
                //User List

                int iLoopCount = 0;
                StringBuilder sb = new StringBuilder();
                hidgroupaexistapp.Value = "";
                foreach (DataRow row in usersNotInGroupDtbl.Rows)
                {
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);


                    //first span tag for WorkFlow name
                    sb = new StringBuilder();
                    sb.Append(SPAN_WORKFLOW_NAME_ID_PREPEND_STRING).Append("_").Append(iLoopCount);
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "spantestingdetails");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["WF_NAME"]));


                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");
                    //Second span tag WorkFlow Id span
                    sb = new StringBuilder();

                    sb.Append(SPAN_WORKFLOW_ID_PREPEND_STRING).Append("_").Append(iLoopCount);

                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Style, "display:none");

                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Span);
                    htmlWriter.Write(Convert.ToString(row["WF_ID"]));
                    htmlWriter.RenderEndTag();

                    //A tag
                    sb = new StringBuilder();
                    sb.Append(LINK_ADD_REMOVE_ID_PREPEND_STRING).Append("_");
                    sb.Append(Convert.ToString(row["WF_NAME"])).Append("_");
                    sb.Append(Convert.ToString(row["WF_ID"])).Append("_");
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, sb.ToString());
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "remove");
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Onclick, "moveUserToExistingUser1(this);");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.A);
                    htmlWriter.RenderEndTag();
                    htmlWriter.Write("&nbsp;");

                    htmlWriter.RenderEndTag();
                    htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "clear");
                    htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                    htmlWriter.RenderEndTag();
                    string strcheck = "";
                    strcheck = Convert.ToString(row["WF_NAME"]);

                    if (strcheck != "")
                    {
                        if (hidgroupaexistapp.Value == "") hidgroupaexistapp.Value = Convert.ToString(row["WF_NAME"]);
                        else hidgroupaexistapp.Value += "," + Convert.ToString(row["WF_NAME"]);
                    }
                    iLoopCount++;
                }
                divexistingApps.InnerHtml = String.Empty;
                divexistingApps.InnerHtml = stringWriter.ToString();
            }
        }

        void showTheHTMLComboBoxAfterPostbackForApp(DataTable selectedUsersForGroup, DataTable unselectedUsers)
        {
            formTheHTMLForMultiSelectListBoxesForApp(selectedUsersForGroup, unselectedUsers);
        }

        void formTheHTMLForMultiSelectListBoxesForApp(DataTable otherUserAvailableDtbl, DataTable usersInGroupDtbl)
        {
            try
            {

                formHTMLForAppNotInGroup(otherUserAvailableDtbl);
                formHTMLForAppInGroup(usersInGroupDtbl);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private string GetDeletedList(string OldList, string NewList)
        {
            string strexistingapp = "";
            if (hidgroupaexistapp.Value.Length > 0)
            {
                string[] strOldApps = OldList.Split(',');
                string[] straddnewapps = NewList.Split(',');
                foreach (string s in strOldApps)
                {
                    if (!straddnewapps.Contains(s))
                        strexistingapp = strexistingapp + "," + s;
                }
            }
            if (strexistingapp.Length > 0) strexistingapp = strexistingapp.Substring(1);
            return strexistingapp;
        }

        protected void btnSavegroup_Click(object sender, EventArgs e)
        {
            string strDeleteingapp = GetDeletedList(hidgroupaexistapp.Value, hidgroupaddnewapps.Value);
            string straddnewapps = hidgroupaddnewapps.Value;
            List<string> codelst1 = getExistingWFsFromHiddenFieldForSaving();
            UpdateUserGroupDetails objDetails = new UpdateUserGroupDetails(hfsPart4, strUserId, codelst1, hfdGroupId.Value, hidInitialGrpNameInEditMode.Value, straddnewapps, strDeleteingapp);
            objDetails.Process();

            if (objDetails.StatusCode == 0)
            {

                hfdGroupId.Value = "";
                Utilities.showMessage("Groups assigned successfully", updSubProcCat, DIALOG_TYPE.Info);
                Utilities.closeModalPopUp("divAddRemoveUsers", updSubProcCat, "CloseGroupChangePopUp");
            }
            else
            {
                Utilities.showMessage("Internal server error.", updSubProcCat, DIALOG_TYPE.Error);
            }
            BindGroupsRepeater();
            updSubProcCat.Update();
        }

        DataTable getAvailableWFFromHiddenFieldForSaving()
        {
            DataTable dtblUsersList = new DataTable();
            dtblUsersList.Columns.Add("WF_ID", typeof(string));
            dtblUsersList.Columns.Add("GROUP_ID", typeof(string));
            dtblUsersList.Columns.Add("COMPANY_ID", typeof(string));
            DataRow row = null;
            if (!String.IsNullOrEmpty(hfappliedapps.Value))
            {
                string[] userIdsToSave = hfappliedapps.Value.Split('|');
                if (userIdsToSave.Length > 0)
                {
                    for (int i = 0; i <= userIdsToSave.Length - 1; i++)
                    {
                        row = dtblUsersList.NewRow();
                        string[] aryUserDtl = userIdsToSave[i].Split(',');
                        row["WF_ID"] = aryUserDtl[0];
                        row["GROUP_ID"] = hfdGroupId.Value;
                        row["COMPANY_ID"] = hfsPart4;

                        dtblUsersList.Rows.Add(row);
                    }
                }
            }
            return dtblUsersList;
        }

        List<string> getExistingWFsFromHiddenFieldForSaving()
        {
            List<string> codelst1 = new List<string>();
            if (!String.IsNullOrEmpty(hfappliedapps.Value))
            {
                string[] userIdsToRemove = hfappliedapps.Value.Split('|');
                if (userIdsToRemove.Length > 0)
                {
                    for (int i = 0; i <= userIdsToRemove.Length - 1; i++)
                    {
                        string[] aryUserDtl = userIdsToRemove[i].Split(',');
                        codelst1.Add(aryUserDtl[0]);
                    }
                }
            }
            return codelst1;
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Description;
using System.Xml;
using System.Xml.Schema;
using System.Text;

namespace mFicientCP
{
    [Serializable]
    public class WSDL
    {
        private BindingCollection bindings;
        private PortTypeCollection portTypes;
        private ServiceCollection services;
        private ComplexDataBuilderCollection complexTypeBuffer;
        private SimpleDataBuilderCollection simpleTypeBuffer;
        private ComplexDataCollection complexTypes;
        private SimpleDataCollection simpleTypes;
        private bool parsingDone = false;

        public WSDL(string _wsdl)
        {
            simpleTypes = new SimpleDataCollection();
            complexTypes = new ComplexDataCollection();

            simpleTypeBuffer = new SimpleDataBuilderCollection(simpleTypes);
            complexTypeBuffer = new ComplexDataBuilderCollection(complexTypes);

            bindings = new BindingCollection();
            portTypes = new PortTypeCollection();
            services = new ServiceCollection();

            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(_wsdl);
            System.IO.MemoryStream ms = new System.IO.MemoryStream(bytes);
            ServiceDescription serviceDescription = GetAllSchema(ServiceDescription.Read(ms), ms);

            ReadAllSimpleTypes(serviceDescription);

            while (!parsingDone)
            {
                parsingDone = true;
                ReadAllComplexTypes(serviceDescription);
                ReadAllSimpleElements(serviceDescription);
                ReadAllComplexElements(serviceDescription);
            }

            ReadAllBindings(serviceDescription);
            ReadAllPortTypes(serviceDescription);
            ReadAllServices(serviceDescription);
        }

        public BindingCollection Bindings
        {
            get
            {
                return bindings;
            }
        }
        public PortTypeCollection PortTypes
        {
            get
            {
                return portTypes;
            }
        }
        public ServiceCollection Services
        {
            get
            {
                return services;
            }
        }
        public SimpleDataCollection SimpleDataTypes
        {
            get
            {
                return simpleTypes;
            }
        }
        public ComplexDataCollection ComplexDataTypes
        {
            get
            {
                return complexTypes;
            }
        }
        private ServiceDescription GetAllSchema(ServiceDescription sd, System.IO.MemoryStream ms)
        {
            try
            {
                List<System.Xml.Schema.XmlSchema> lstXmlSchema = new List<System.Xml.Schema.XmlSchema>();
                foreach (XmlSchema wsdlSchema in sd.Types.Schemas)
                {
                    foreach (XmlSchemaObject externalSchema in wsdlSchema.Includes)
                    {
                        if (externalSchema is XmlSchemaImport)
                        {
                            System.Xml.Schema.XmlSchema schema = XmlSchema.Read(ms, null);
                            lstXmlSchema.Add(schema);
                        }
                    }
                }

                if (lstXmlSchema.Count > 0)
                {
                    for (int i = 0; i < lstXmlSchema.ToArray().Length; i++)
                    {
                        sd.Types.Schemas.Add(lstXmlSchema.ToArray()[i]);
                    }
                }
            }
            catch
            {
            }

            return sd;
        }

        private void ReadAllSimpleTypes(ServiceDescription sd)
        {
            XmlSchemaSimpleType simpleType;

            foreach (XmlSchema xmlSchema in sd.Types.Schemas)
            {
                foreach (object schemaItem in xmlSchema.Items)
                {
                    if (schemaItem.GetType().Equals(typeof(XmlSchemaSimpleType)))
                    {
                        simpleType = (XmlSchemaSimpleType)schemaItem;
                        SimpleDataBuilder sdb = simpleTypeBuffer.AddNew(simpleType.Name);

                        if (simpleType.Content.GetType().Equals(typeof(XmlSchemaSimpleTypeRestriction)))
                        {
                            sdb.Restrictions = GetSimpleTypeRestrictions(sd, (XmlSchemaSimpleTypeRestriction)simpleType.Content);
                            sdb.DataType = sdb.Restrictions.BaseType;
                        }
                        else
                        {
                            // code for list and union
                        }

                        simpleTypeBuffer.Complete(sdb);
                    }
                }
            }
        }

        private Restrictions GetSimpleTypeRestrictions(ServiceDescription sd, XmlSchemaSimpleTypeRestriction SimpleTypeRestriction)
        {
            Restrictions objRestrictions = new Restrictions();

            List<object> objFacetsItem = new List<object>();
            if (SimpleTypeRestriction.BaseTypeName.Namespace != sd.TargetNamespace)
            {
                if (SimpleTypeRestriction.Facets != null)
                {
                    objRestrictions.BaseType = SimpleTypeRestriction.BaseTypeName.Name;

                    foreach (object facetsItem in SimpleTypeRestriction.Facets)
                    {
                        switch (facetsItem.GetType().Name)
                        {
                            case "XmlSchemaMinExclusiveFacet":
                                objRestrictions.Add(new Restriction(RestrictionType.MinExclusive, ((XmlSchemaMinExclusiveFacet)facetsItem).Value));
                                break;
                            case "XmlSchemaMinInclusiveFacet":
                                objRestrictions.Add(new Restriction(RestrictionType.MinInclusive, ((XmlSchemaMinInclusiveFacet)facetsItem).Value));
                                break;
                            case "XmlSchemaMaxExclusiveFacet":
                                objRestrictions.Add(new Restriction(RestrictionType.MaxExclusive, ((XmlSchemaMaxExclusiveFacet)facetsItem).Value));
                                break;
                            case "XmlSchemaMaxInclusiveFacet":
                                objRestrictions.Add(new Restriction(RestrictionType.MaxInclusive, ((XmlSchemaMaxInclusiveFacet)facetsItem).Value));
                                break;
                            case "XmlSchemaFractionDigitsFacet":
                                objRestrictions.Add(new Restriction(RestrictionType.FractionDigits, ((XmlSchemaFractionDigitsFacet)facetsItem).Value));
                                break;
                            case "XmlSchemaLengthFacet":
                                objRestrictions.Add(new Restriction(RestrictionType.Length, ((XmlSchemaLengthFacet)facetsItem).Value));
                                break;
                            case "XmlSchemaMinLengthFacet":
                                objRestrictions.Add(new Restriction(RestrictionType.MinLength, ((XmlSchemaMinLengthFacet)facetsItem).Value));
                                break;
                            case "XmlSchemaEnumerationFacet":
                                objRestrictions.Add(new Restriction(RestrictionType.Enumeration, ((XmlSchemaEnumerationFacet)facetsItem).Value));
                                break;
                            case "XmlSchemaWhiteSpaceFacet":
                                objRestrictions.Add(new Restriction(RestrictionType.WhiteSpace, ((XmlSchemaWhiteSpaceFacet)facetsItem).Value));
                                break;
                            case "XmlSchemaPatternFacet":
                                objRestrictions.Add(new Restriction(RestrictionType.Pattern, ((XmlSchemaPatternFacet)facetsItem).Value));
                                break;
                        }
                    }
                }
            }
            return objRestrictions;
        }

        private void ReadAllComplexTypes(ServiceDescription sd)
        {
            XmlSchemaComplexType complexType;

            foreach (XmlSchema xmlSchema in sd.Types.Schemas)
            {
                foreach (object schemaItem in xmlSchema.Items)
                {
                    if (schemaItem.GetType().Equals(typeof(XmlSchemaComplexType)))
                    {
                        complexType = (XmlSchemaComplexType)schemaItem;
                        if (!complexTypes.Contains(complexType.Name))
                        {
                            ComplexDataBuilder cdb = complexTypeBuffer.AddNew(complexType.Name);
                            if (ParseWithinComplexData(cdb, complexType, sd)) complexTypeBuffer.Complete(cdb);
                            else
                            {
                                parsingDone = false;
                                complexTypeBuffer.Remove(cdb);
                            }
                        }
                    }
                }
            }
        }

        private void ReadAllSimpleElements(ServiceDescription sd)
        {
            XmlSchemaElement schemaElement;
            XmlSchemaSimpleType simpleType;
            SimpleDataBuilder sdb = null;

            foreach (XmlSchema xmlSchema in sd.Types.Schemas)
            {
                foreach (object schemaItem in xmlSchema.Items)
                {
                    if (schemaItem.GetType().Equals(typeof(XmlSchemaElement)))
                    {
                        schemaElement = (XmlSchemaElement)schemaItem;
                        if (schemaElement.SchemaType != null)
                        {
                            if (schemaElement.SchemaType.GetType().Equals(typeof(XmlSchemaSimpleType)))
                            {
                                simpleType = (XmlSchemaSimpleType)schemaElement.SchemaType;

                                if (!string.IsNullOrEmpty(simpleType.Name)) sdb = simpleTypeBuffer.AddNew(simpleType.Name);
                                else sdb = simpleTypeBuffer.AddNew(schemaElement.Name);

                                if (simpleType.Content.GetType().Equals(typeof(XmlSchemaSimpleTypeRestriction)))
                                {
                                    sdb.Restrictions = GetSimpleTypeRestrictions(sd, (XmlSchemaSimpleTypeRestriction)simpleType.Content);
                                    sdb.DataType = sdb.Restrictions.BaseType;
                                }
                                else
                                {
                                    // code for list and union
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ReadAllComplexElements(ServiceDescription sd)
        {
            XmlSchemaElement schemaElement;
            XmlSchemaComplexType complexType;
            ComplexDataBuilder cdb = null;

            foreach (XmlSchema xmlSchema in sd.Types.Schemas)
            {
                foreach (object schemaItem in xmlSchema.Items)
                {
                    if (schemaItem.GetType().Equals(typeof(XmlSchemaElement)))
                    {
                        schemaElement = (XmlSchemaElement)schemaItem;
                        if (schemaElement.SchemaType != null)
                        {
                            if (schemaElement.SchemaType.GetType().Equals(typeof(XmlSchemaComplexType)))
                            {
                                complexType = (XmlSchemaComplexType)schemaElement.SchemaType;

                                if (!string.IsNullOrEmpty(complexType.Name)) cdb = complexTypeBuffer.AddNew(complexType.Name);
                                else cdb = complexTypeBuffer.AddNew(schemaElement.Name);

                                if (ParseWithinComplexData(cdb, complexType, sd)) complexTypeBuffer.Complete(cdb);
                                else
                                {
                                    parsingDone = false;
                                    complexTypeBuffer.Remove(cdb);
                                }
                            }
                        }
                        else
                        {
                            SimpleDataBuilder sdb = simpleTypeBuffer.AddNew(schemaElement.Name);
                            sdb.DataType = schemaElement.SchemaTypeName.Name;
                            sdb.IsElement = true;
                            cdb.SimpleDataTypes.Add(simpleTypeBuffer.Complete(sdb));
                            simpleTypes.Remove(sdb.Name);
                        }
                    }
                }
            }
        }

        private bool ParseWithinComplexData(ComplexDataBuilder complexDataBuilder, XmlSchemaComplexType schemaComplexType, ServiceDescription sd)
        {
            if (schemaComplexType.Particle == null)
            {
                return ParseWithinComplexDataWhenParticleNull(complexDataBuilder, schemaComplexType, sd);
            }
            else
            {
                return ParseWithinComplexDataWhenParticleNotNull(complexDataBuilder, schemaComplexType.Particle, sd);
            }
        }

        private bool ParseWithinComplexDataWhenParticleNull(ComplexDataBuilder complexDataBuilder, XmlSchemaComplexType schemaComplexType, ServiceDescription sd)
        {
            if (schemaComplexType.ContentModel == null)
            {
                foreach (object attribute in schemaComplexType.Attributes)
                {
                    SimpleDataBuilder sdb = simpleTypeBuffer.AddNew(((XmlSchemaAttribute)(attribute)).Name);
                    sdb.DataType = ((XmlSchemaAttribute)(attribute)).SchemaTypeName.Name;
                    if (((XmlSchemaAttribute)(attribute)).SchemaTypeName.Namespace == sd.TargetNamespace)
                        sdb.IsStandard = false;
                    sdb.IsAttribute = true;
                    complexDataBuilder.SimpleDataTypes.Add(simpleTypeBuffer.Complete(sdb));
                    simpleTypes.Remove(sdb.Name);
                }
            }
            else
            {
                if (schemaComplexType.ContentModel.GetType().Equals(typeof(XmlSchemaComplexContent)))
                {
                    XmlSchemaComplexContent xmlScCpx = (XmlSchemaComplexContent)(schemaComplexType.ContentModel);
                    if (xmlScCpx.Content.GetType().Equals(typeof(XmlSchemaComplexContentExtension)))
                    {
                        XmlSchemaComplexContentExtension schemaComplexContentExt = (XmlSchemaComplexContentExtension)(xmlScCpx.Content);
                        if (schemaComplexContentExt.Particle != null)
                        {
                            if (!ParseWithinComplexDataWhenParticleNotNull(complexDataBuilder, schemaComplexContentExt.Particle, sd))
                            {
                                return false;
                            }
                        }
                        if (schemaComplexContentExt.Attributes.Count > 0)
                        {
                            foreach (var attribute in schemaComplexContentExt.Attributes)
                            {
                                SimpleDataBuilder sdb = simpleTypeBuffer.AddNew(((XmlSchemaAttribute)(attribute)).Name);
                                sdb.DataType = ((XmlSchemaAttribute)(attribute)).SchemaTypeName.Name;
                                sdb.IsAttribute = true;
                                complexDataBuilder.SimpleDataTypes.Add(simpleTypeBuffer.Complete(sdb));
                                simpleTypes.Remove(sdb.Name);
                            }
                        }
                    }
                    else if (xmlScCpx.Content.GetType().Equals(typeof(XmlSchemaComplexContentRestriction)))
                    {
                        XmlSchemaComplexContentRestriction schemaComplexContentRxt = (XmlSchemaComplexContentRestriction)(xmlScCpx.Content);
                        if (schemaComplexContentRxt.Particle != null)
                        {
                            if (!ParseWithinComplexDataWhenParticleNotNull(complexDataBuilder, schemaComplexContentRxt.Particle, sd))
                            {
                                return false;
                            }
                        }
                        foreach (var attribute in schemaComplexContentRxt.Attributes)
                        {
                            if (((System.Xml.Schema.XmlSchemaAttribute)(attribute)).Name == null)
                            {
                                if (((System.Xml.Schema.XmlSchemaAnnotated)(attribute)).UnhandledAttributes.Length > 0)
                                {
                                    if ((((System.Xml.Schema.XmlSchemaAnnotated)(attribute)).UnhandledAttributes[0]).Value.Contains(sd.TargetNamespace))
                                    {
                                        string schemaTypeName = (((System.Xml.Schema.XmlSchemaAnnotated)(attribute)).UnhandledAttributes[0]).Value.Replace(sd.TargetNamespace + ":", "").Replace("[]", ""); ;
                                        if (simpleTypes.Contains(schemaTypeName))
                                        {
                                            ComplexDataBuilder cdb = complexTypeBuffer.AddNew(schemaComplexType.Name + "[]");
                                            cdb.SimpleDataTypes.Add(simpleTypes.findKey(schemaTypeName));
                                            complexDataBuilder.ComplexDataTypes.Add(complexTypeBuffer.Complete(cdb));
                                            complexTypes.Remove(cdb.Name);
                                        }
                                        else if (complexTypes.Contains(schemaTypeName))
                                        {
                                            ComplexDataBuilder cdb = complexTypeBuffer.AddNew(schemaComplexType.Name + "[]");
                                            cdb.ComplexDataTypes.Add(complexTypes.findKey(schemaTypeName));
                                            complexDataBuilder.ComplexDataTypes.Add(complexTypeBuffer.Complete(cdb));
                                            complexTypes.Remove(cdb.Name);
                                        }
                                        else
                                        {
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                        string strType = (((System.Xml.Schema.XmlSchemaAnnotated)(attribute)).UnhandledAttributes[0]).Value;
                                        SimpleDataBuilder sdb = simpleTypeBuffer.AddNew(schemaComplexType.Name);
                                        sdb.DataType = strType.Substring(strType.LastIndexOf(':') + 1, strType.Length - strType.LastIndexOf(':') - 1);
                                        complexDataBuilder.SimpleDataTypes.Add(simpleTypeBuffer.Complete(sdb));
                                        //simpleTypes.Remove(sdb.Name);
                                        simpleTypes.Remove(sdb.Name);
                                    }
                                }
                                else
                                {
                                    SimpleDataBuilder sdb = simpleTypeBuffer.AddNew(((XmlSchemaAttribute)(attribute)).Name);
                                    sdb.DataType = ((XmlSchemaAttribute)(attribute)).SchemaTypeName.Name;
                                    if (((XmlSchemaAttribute)(attribute)).SchemaTypeName.Namespace == sd.TargetNamespace)
                                        sdb.IsStandard = false;
                                    sdb.IsAttribute = true;
                                    complexDataBuilder.SimpleDataTypes.Add(simpleTypeBuffer.Complete(sdb));
                                    simpleTypes.Remove(sdb.Name);
                                }
                            }
                            //((System.Xml.Schema.XmlSchemaAttribute)(attribute)).RefName.Namespace
                            //attribute objAttribute = new attribute();
                            //objAttribute.name = ((System.Xml.Schema.XmlSchemaAttribute)(attribute)).Name;
                            //objAttribute.DataType = ((System.Xml.Schema.XmlSchemaAttribute)(attribute)).SchemaTypeName.Name;
                        }
                    }
                }
            }
            return true;
        }

        private bool ParseWithinComplexDataWhenParticleNotNull(ComplexDataBuilder complexDataBuilder, XmlSchemaParticle schemaParticle, ServiceDescription sd)
        {
            Type particleType = schemaParticle.GetType();
            if (particleType.Equals(typeof(XmlSchemaSequence)))
            {
                XmlSchemaSequence schemaSequence = (XmlSchemaSequence)schemaParticle;
                for (int i = 0; i < schemaSequence.Items.Count; i++)
                {
                    XmlSchemaObject schemaObject = schemaSequence.Items[i];
                    if (schemaObject.GetType().Equals(typeof(XmlSchemaElement)))
                    {
                        XmlSchemaElement schemaElement = (XmlSchemaElement)schemaObject;
                        if (schemaElement.SchemaTypeName.Namespace == sd.TargetNamespace)
                        {
                            string schemaTypeName = schemaElement.SchemaTypeName.Name;
                            if (simpleTypes.Contains(schemaTypeName))
                            {
                                ComplexDataBuilder cdb = complexTypeBuffer.AddNew(schemaElement.Name);
                                cdb.SimpleDataTypes.Add(simpleTypes.findKey(schemaTypeName));
                                cdb.IsElement = true;
                                complexDataBuilder.ComplexDataTypes.Add(complexTypeBuffer.Complete(cdb));
                                complexTypes.Remove(cdb.Name);
                            }
                            else if (complexTypes.Contains(schemaTypeName))
                            {
                                ComplexDataBuilder cdb = complexTypeBuffer.AddNew(schemaElement.Name);
                                cdb.ComplexDataTypes.Add(complexTypes.findKey(schemaTypeName));
                                cdb.IsElement = true;
                                complexDataBuilder.ComplexDataTypes.Add(complexTypeBuffer.Complete(cdb));
                                complexTypes.Remove(cdb.Name);
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            SimpleDataBuilder sdb = simpleTypeBuffer.AddNew(schemaElement.Name);
                            sdb.DataType = schemaElement.SchemaTypeName.Name;
                            sdb.IsElement = true;
                            sdb.IsStandard = true;
                            complexDataBuilder.SimpleDataTypes.Add(simpleTypeBuffer.Complete(sdb));
                            simpleTypes.Remove(sdb.Name);
                        }
                    }
                }
                return true;
            }
            else if (particleType.Equals(typeof(XmlSchemaChoice)))
            {
                XmlSchemaChoice schemaChoice = (XmlSchemaChoice)schemaParticle;

                for (int i = 0; i < schemaChoice.Items.Count; i++)
                {
                    XmlSchemaObject schemaObject = schemaChoice.Items[i];
                    if (schemaObject.GetType().Equals(typeof(XmlSchemaElement)))
                    {
                        XmlSchemaElement schemaElement = (XmlSchemaElement)schemaObject;

                        if (schemaElement.SchemaTypeName.Namespace == sd.TargetNamespace)
                        {
                            string schemaTypeName = schemaElement.SchemaTypeName.Name;
                            if (simpleTypes.Contains(schemaTypeName))
                            {
                                ComplexDataBuilder cdb = complexTypeBuffer.AddNew(schemaElement.Name);
                                cdb.SimpleDataTypes.Add(simpleTypes.findKey(schemaTypeName));
                                cdb.IsElement = true;
                                complexDataBuilder.ComplexDataTypes.Add(complexTypeBuffer.Complete(cdb));
                                complexTypes.Remove(cdb.Name);
                            }
                            else if (complexTypes.Contains(schemaTypeName))
                            {
                                ComplexDataBuilder cdb = complexTypeBuffer.AddNew(schemaElement.Name);
                                cdb.ComplexDataTypes.Add(complexTypes.findKey(schemaTypeName));
                                cdb.IsElement = true;
                                complexDataBuilder.ComplexDataTypes.Add(complexTypeBuffer.Complete(cdb));
                                complexTypes.Remove(cdb.Name);
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            SimpleDataBuilder sdb = simpleTypeBuffer.AddNew(schemaElement.Name);
                            sdb.DataType = schemaElement.SchemaTypeName.Name;
                            sdb.IsStandard = true;
                            sdb.IsElement = true;
                            complexDataBuilder.SimpleDataTypes.Add(simpleTypeBuffer.Complete(sdb));
                            simpleTypes.Remove(sdb.Name);
                        }
                    }
                }
                return true;
            }
            else if (particleType.Equals(typeof(XmlSchemaAll)))
            {
                XmlSchemaAll schemaAll = (XmlSchemaAll)schemaParticle;
                for (int i = 0; i < schemaAll.Items.Count; i++)
                {
                    XmlSchemaObject schemaObject = schemaAll.Items[i];
                    if (schemaObject.GetType().Equals(typeof(XmlSchemaElement)))
                    {
                        XmlSchemaElement schemaElement = (XmlSchemaElement)schemaObject;

                        if (schemaElement.SchemaTypeName.Namespace == sd.TargetNamespace)
                        {
                            string schemaTypeName = schemaElement.SchemaTypeName.Name;
                            if (simpleTypes.Contains(schemaTypeName))
                            {
                                ComplexDataBuilder cdb = complexTypeBuffer.AddNew(schemaElement.Name);
                                cdb.SimpleDataTypes.Add(simpleTypes.findKey(schemaTypeName));
                                cdb.IsElement = true;
                                complexDataBuilder.ComplexDataTypes.Add(complexTypeBuffer.Complete(cdb));
                                complexTypes.Remove(cdb.Name);
                            }
                            else if (complexTypes.Contains(schemaTypeName))
                            {
                                ComplexDataBuilder cdb = complexTypeBuffer.AddNew(schemaElement.Name);
                                cdb.ComplexDataTypes.Add(complexTypes.findKey(schemaTypeName));
                                cdb.IsElement = true;
                                complexDataBuilder.ComplexDataTypes.Add(complexTypeBuffer.Complete(cdb));
                                complexTypes.Remove(cdb.Name);
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            SimpleDataBuilder sdb = simpleTypeBuffer.AddNew(schemaElement.Name);
                            sdb.DataType = schemaElement.SchemaTypeName.Name;
                            sdb.IsStandard = true;
                            sdb.IsElement = true;
                            complexDataBuilder.SimpleDataTypes.Add(simpleTypeBuffer.Complete(sdb));
                            simpleTypes.Remove(sdb.Name);
                        }
                    }
                }
                return true;
            }
            return true;
        }

        private void ReadAllBindings(ServiceDescription sd)
        {
            Binding binding = null;
            OperationSoapAction operationSoapAction = null;
            OperationSoapActionCollection operationSoapActions = null;
            foreach (System.Web.Services.Description.Binding objBinding in sd.Bindings)
            {
                operationSoapActions = new OperationSoapActionCollection();
                foreach (System.Web.Services.Description.OperationBinding objOperation in objBinding.Operations)
                {
                    if (objOperation.Extensions.Count > 0)
                    {
                        if (objBinding.Extensions[0].GetType().Equals(typeof(SoapBinding)))
                        {
                            operationSoapAction = new OperationSoapAction(objOperation.Name, ((SoapOperationBinding)((new ArrayList(((CollectionBase)(objOperation.Extensions))))[0])).SoapAction);
                            operationSoapActions.Add(operationSoapAction);
                        }
                    }
                }
                if (objBinding.Extensions.Count > 0)
                {
                    if (objBinding.Extensions[0].GetType().Equals(typeof(SoapBinding)) || objBinding.Extensions[0].GetType().Equals(typeof(Soap12Binding)))
                    {
                        binding = new Binding(objBinding.Name, objBinding.Type.Name, "Soap", ((System.Web.Services.Description.SoapBinding)(objBinding.Extensions[0])).Style.ToString(),sd.TargetNamespace.ToString());
                    }
                    if (objBinding.Extensions[0].GetType().Equals(typeof(HttpBinding))) binding = new Binding(objBinding.Name, objBinding.Type.Name, "http", null, sd.TargetNamespace.ToString());
                }
                binding.OperationSoapActions = operationSoapActions;
                bindings.Add(binding);
            }
        }

        private void ReadAllPortTypes(ServiceDescription sd)
        {
            PartCollection parts;
            Operation operation;
            OperationCollection operations;
            Part part;
            PortType portType;

            foreach (System.Web.Services.Description.PortType objPortType in sd.PortTypes)
            {
                portType = new PortType(objPortType.Name);
                operations = new OperationCollection();
                foreach (System.Web.Services.Description.Operation ObjOperation in objPortType.Operations)
                {
                    operation = new Operation(ObjOperation.Name, ObjOperation.Messages.Input.Message.Name, ObjOperation.Messages.Output.Message.Name);
                    if (sd.Messages[ObjOperation.Messages.Input.Message.Name].Parts.Count > 0)
                    {
                        parts = new PartCollection();
                        foreach (MessagePart itemPart in sd.Messages[ObjOperation.Messages.Input.Message.Name].Parts)
                        {
                            if (itemPart.Element.Name.Length > 0)
                            {
                                if (itemPart.Element.Namespace == sd.TargetNamespace) part = new Part(itemPart.Name, itemPart.Element.Name, true, this.SimpleDataTypes, this.ComplexDataTypes);
                                else part = new Part(itemPart.Name, itemPart.Element.Name, true, this.SimpleDataTypes, this.ComplexDataTypes);
                            }
                            else
                            {
                                if (itemPart.Type.Namespace == sd.TargetNamespace) part = new Part(itemPart.Name, itemPart.Type.Name, true, this.SimpleDataTypes, this.ComplexDataTypes);
                                else part = new Part(itemPart.Name, itemPart.Type.Name, false, this.SimpleDataTypes, this.ComplexDataTypes);
                            }
                            parts.Add(part);
                        }
                        operation.InputMessageParts = parts;
                    }

                    if (sd.Messages[ObjOperation.Messages.Output.Message.Name].Parts.Count > 0)
                    {
                        parts = new PartCollection();
                        foreach (MessagePart itemPart in sd.Messages[ObjOperation.Messages.Output.Message.Name].Parts)
                        {
                            if (itemPart.Element.Name.Length > 0)
                            {
                                if (itemPart.Element.Namespace == sd.TargetNamespace) part = new Part(itemPart.Name, itemPart.Element.Name, true, this.SimpleDataTypes, this.ComplexDataTypes);
                                else part = new Part(itemPart.Name, itemPart.Element.Name, false, this.SimpleDataTypes, this.ComplexDataTypes);
                            }
                            else
                            {
                                if (itemPart.Type.Namespace == sd.TargetNamespace) part = new Part(itemPart.Name, itemPart.Type.Name, true, this.SimpleDataTypes, this.ComplexDataTypes);
                                else part = new Part(itemPart.Name, itemPart.Type.Name, false, this.SimpleDataTypes, this.ComplexDataTypes);
                            }
                            parts.Add(part);
                        }
                        operation.OutputMessageParts = parts;
                    }
                    operations.Add(operation);
                }
                portType.Operations = operations;
                portTypes.Add(portType);
            }
        }

        private void ReadAllServices(ServiceDescription sd)
        {
            PortCollection ports;
            Port port;
            Service service;
            foreach (System.Web.Services.Description.Service objService in sd.Services)
            {
                service = new Service(objService.Name);
                ports = new PortCollection();
                foreach (System.Web.Services.Description.Port objPort in objService.Ports)
                {
                    if ((new ArrayList(((CollectionBase)(objPort.Extensions)))[0]).GetType().Equals(typeof(System.Web.Services.Description.SoapAddressBinding)))
                    {
                        port = new Port(objPort.Name, objPort.Binding.Name, ((System.Web.Services.Description.SoapAddressBinding)(new ArrayList(((CollectionBase)(objPort.Extensions)))[0])).Location);
                        ports.Add(port);
                    }
                    else if ((new ArrayList(((CollectionBase)(objPort.Extensions)))[0]).GetType().Equals(typeof(System.Web.Services.Description.HttpAddressBinding)))
                    {
                        port = new Port(objPort.Name, objPort.Binding.Name, ((System.Web.Services.Description.HttpAddressBinding)(new ArrayList(((CollectionBase)(objPort.Extensions)))[0])).Location);
                        ports.Add(port);
                    }
                }
                service.Ports = ports;
                services.Add(service);
            }
        }

        #region SoapGenerationRegion

        public string getSoapRequest(string operationName, string portName, string targetNamespace, bool isRPC, PortTypeCollection portTypes, SimpleDataCollection simpleTypes)
        {
            //<?xml version=""1.0"" encoding=""utf-8""?>
            string strSoapEnvelope = @"
                            <soap:Envelope
                            xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
                            xmlns:xsd='http://www.w3.org/2001/XMLSchema'
                            xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
                        <soap:Body></soap:Body></soap:Envelope>";
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(strSoapEnvelope);

            //KeywordRequest
            //getInputParameters(portName, operationName, complexTypes, elementTypes, simpleTypes, portTypes, attributeTypes, bindingTypes);

            //Use this for testing
            //XmlDocumentFragment xmlDocFragForSoapBody = getBodyOfSoapRequest(getParametersForSoapRequest("ProductInfo", operationName), operationName, soapEnvelopeXml, targetNameSpace, true);

            XmlDocumentFragment xmlDocFragForSoapBody = getBodyOfSoapRequest(getSoapParameters(operationName, portName, portTypes, simpleTypes), operationName, soapEnvelopeXml, targetNamespace, isRPC);

            if (xmlDocFragForSoapBody != null)
            {
                XmlNode xmlEnvelopeBodyNode = soapEnvelopeXml.FirstChild.FirstChild;
                //XmlNodeList xmlNodeList = soapEnvelopeXml.GetElementsByTagName("Envelope");
                xmlEnvelopeBodyNode.AppendChild(xmlDocFragForSoapBody);
                return "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + soapEnvelopeXml.InnerXml.ToString();
            }
            else
            {
                return "";
            }

            //return "";
        }

        private XmlDocumentFragment getBodyOfSoapRequest(Parameter[] inputParameters, string operationName, XmlDocument xmlDocMain, string targetNameSpaceOfShema, bool isRPC)
        {
            if (inputParameters != null)
            {
                List<string> lstTypesWithNoParentTag = new List<string>();
                for (int i = 0; i < inputParameters.Length; i++)
                {
                    if (inputParameters[i].ParentTagName == "")
                    {

                    }
                }
                List<string> lstParentTags = new List<string>();
                lstParentTags.Add(operationName);
                for (int i = 0; i < inputParameters.Length; i++)
                {
                    if (lstParentTags.Contains(inputParameters[i].ParentTagName))
                    {
                        continue;
                    }
                    else
                    {
                        lstParentTags.Add(inputParameters[i].ParentTagName);
                    }
                }

                //we had to add elements which were children of some inner tags.
                //we had to find that node first but it was not possible to find 
                //the node using the selectSingleNode because it could have only found element which was directly 
                //under the main tag that was passed.Since it was not possible to find out how deep it could get,using XPath was not
                //possible,so all the parent tag when formed, their object was stored in the hashtable(defined below hsInnerParentTagsNode)
                //so when we had to add elements of the innerParents we could use the object directly from the hashtable
                Hashtable hsInnerParentTagsNode = new Hashtable();

                XmlDocumentFragment xmlFragForSoapBody = xmlDocMain.CreateDocumentFragment();
                XmlNode xmlNode;
                xmlNode = xmlDocMain.CreateElement(operationName);
               // XmlAttribute attribute = xmlDocMain.CreateAttribute("targetNamespace");
                XmlAttribute attribute = xmlDocMain.CreateAttribute("xmlns");
                attribute.Value = targetNameSpaceOfShema;
                xmlNode.Attributes.Append(attribute);
                xmlFragForSoapBody.AppendChild(xmlNode);
                for (int i = 0; i < inputParameters.Length; i++)
                {
                    //attributes will be of some parent tag,and if it is a parent tag then it must be in the hash table of parent tags
                    //so no other checks are required.
                    //But check for other examples as well
                    if (inputParameters[i].IsAttribute)
                    {
                        attribute = xmlDocMain.CreateAttribute(inputParameters[i].Name);
                        attribute.Value = "@@" + inputParameters[i].Name + "@@";
                        xmlNode = (XmlNode)hsInnerParentTagsNode[inputParameters[i].ParentTagName];
                        xmlNode.Attributes.Append(attribute);
                    }
                    //changes made for accomodating array type with simple data types
                    //The tag will be of the data type and not of the name of the parameter
                    else
                    {
                        if (!inputParameters[i].IsArray)
                        {
                            xmlNode = xmlDocMain.CreateElement(inputParameters[i].Name);
                        }
                        else
                        {
                            xmlNode = xmlDocMain.CreateElement(inputParameters[i].Type);
                        }
                        try
                        {
                            XmlNode xmlParentNode;
                            if (operationName == inputParameters[i].ParentTagName)
                            {
                                //this will work for the tag which has the operation tag
                                xmlParentNode = xmlFragForSoapBody.SelectSingleNode(inputParameters[i].ParentTagName);
                            }
                            else
                            {
                                xmlParentNode = xmlFragForSoapBody.FirstChild.SelectSingleNode(inputParameters[i].ParentTagName);
                                if (xmlParentNode == null)
                                {
                                    xmlParentNode = (XmlNode)hsInnerParentTagsNode[inputParameters[i].ParentTagName];
                                }
                                //xmlParentNode = xmlFragOperationName.
                            }
                            if (!lstParentTags.Contains(inputParameters[i].Name))
                            {
                                xmlNode.InnerText = "@@" + inputParameters[i].Name + "@@";
                                //changed for accomodating array types which are of simple types && !inputParameters[i].IsArray
                                //for array of simple types then the tag will be  of the data type and not of the element name and will not require any attribute if rpc
                                if (isRPC && !inputParameters[i].IsArray)
                                {
                                    //attribute = xmlDocMain.CreateAttribute("xsi:type=\"xsd:",inputParameters[i].Type,"http://www.w3.org/2001/XMLSchema");
                                    //xmlNode.Attributes.Append(
                                    attribute = xmlDocMain.CreateAttribute("xsi", "type", "http://www.w3.org/2001/XMLSchema-instance");
                                    attribute.Value = "xsd:" + inputParameters[i].Type;
                                    xmlNode.Attributes.Append(attribute);
                                }
                            }
                            else
                            {
                                hsInnerParentTagsNode.Add(xmlNode.Name, xmlNode);
                            }
                            xmlParentNode.AppendChild(xmlNode);
                        }
                        catch //(System.Xml.XPath.XPathException e)
                        {
                            // xmlNode = xmlDocMain.CreateElement(inputParameters[i].ParentTagName);

                        }
                        //catch (Exception e)
                        //{
                            //
                        //}
                    }
                }
                return xmlFragForSoapBody;
            }
            else
            {
                return null;
            }


        }
        
        private XmlDocument createEmptySoapEnvelope(string content, string soapEnvelope)
        {
            StringBuilder sb = new StringBuilder(soapEnvelope);
            //sb.Insert(sb.ToString().IndexOf("</soap:Body>"), content);
            // create an empty soap envelope
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(sb.ToString());
            return soapEnvelopeXml;
        }
        
        private Parameter[] getSoapParameters(string operationName, string portName, PortTypeCollection portTypes, SimpleDataCollection simpleTypes)
        {
            PortType port = PortTypes.findKey(portName);
            List<Parameter> lstParameters = new List<Parameter>();
            if (port != null)
            {
                PartCollection partColl = port.Operations.findKey(operationName).InputMessageParts;
                if (partColl != null)
                {
                    //this is required because there may be times when the message  itself contains the type,name of 
                    //the parameter
                    //<wsdl:message name="ConversionRateHttpPostIn">
                    //<wsdl:part name="FromCurrency" type="s:string"/>
                    //<wsdl:part name="ToCurrency" type="s:string"/>
                    //</wsdl:message>
                    //other wise most probably there will be only one item in the collection
                    foreach (Part prt in partColl)
                    {
                        if (prt.ExistsInCollection)
                        {
                            getSoapParametersIfInCollection(operationName, lstParameters, prt.Type);
                        }
                        else
                        {
                            lstParameters.Add(new Parameter(prt.Name, prt.TypeName, operationName, false, false));
                        }
                    }
                }
            }
            return lstParameters.ToArray();
        }
        
        private List<Parameter> getSoapParametersIfInCollection(string operationName, List<Parameter> parameters, Object messagePartTypeDataCollection)
        {
            //string productInfo = "ProductInfo";
            // List<Parameter> lstParameters = new List<Parameter>();

            if (messagePartTypeDataCollection.GetType().Equals(typeof(ComplexData)))
            {
                ComplexData objComplexData = (ComplexData)messagePartTypeDataCollection;
                string parentTag = operationName;
                SimpleDataCollection objSimpleDataColl = objComplexData.SimpleDataTypes;
                //foreach (SimpleData simpleData in objSimpleDataColl)
                //{
                //    lstParameters.Add(new Parameter(simpleData.Name,simpleData.DataType,parentTag));
                //}
                if (objSimpleDataColl != null && objSimpleDataColl.Count > 0)
                {
                    getParametersFromSimpleTypes(objSimpleDataColl, parentTag, parameters);
                }
                ComplexDataCollection objComplexDataColl = objComplexData.ComplexDataTypes;
                foreach (ComplexData cmplxdata in objComplexDataColl)
                {
                    SimpleDataCollection objInnerSimpleDataColl = cmplxdata.SimpleDataTypes;
                    if (objInnerSimpleDataColl != null && objInnerSimpleDataColl.Count > 0)
                    {
                        getParametersFromSimpleTypes(objInnerSimpleDataColl, parentTag, parameters);
                    }
                    if (cmplxdata.ComplexDataTypes != null && cmplxdata.ComplexDataTypes.Count > 0)
                    {
                        if (cmplxdata.IsElement)
                        {
                            //a complex type will not be an attribute of any tags
                            parameters.Add(new Parameter(cmplxdata.Name, String.Empty, parentTag, cmplxdata.IsArray, false));
                            //parentTag = cmplxdata.Name;
                            getParametersFromComplexData(cmplxdata, cmplxdata.Name, parameters);
                        }
                        else
                        {
                            getParametersFromComplexData(cmplxdata, parentTag, parameters);
                        }
                    }
                }
            }
            else if (messagePartTypeDataCollection.GetType().Equals(typeof(SimpleData)))
            {
                SimpleData simpleData = (SimpleData)messagePartTypeDataCollection;
                //simpleData.
            }
            return parameters;
        }
        
        private void getParametersFromComplexData(ComplexData complexData, string parentTag, List<Parameter> parameters)
        {
            ComplexDataCollection objComplexDataColl = complexData.ComplexDataTypes;
            //SimpleDataCollection objSimpleDataColl = complexData.SimpleDataTypes;
            //if (objSimpleDataColl != null && objSimpleDataColl.Count > 0)
            //{
            //    getParametersFromSimpleTypes(objSimpleDataColl, parentTag, parameters);
            //}
            foreach (ComplexData cmplxdata in objComplexDataColl)
            {
                SimpleDataCollection objInnerSimpleDataColl = cmplxdata.SimpleDataTypes;
                if (objInnerSimpleDataColl != null && objInnerSimpleDataColl.Count > 0)
                {
                    getParametersFromSimpleTypes(objInnerSimpleDataColl, parentTag, parameters);
                }
                if (cmplxdata.ComplexDataTypes != null && cmplxdata.ComplexDataTypes.Count > 0)
                {
                    if (cmplxdata.IsElement)
                    {
                        //a complex type will not be an attribute of any tags
                        parameters.Add(new Parameter(cmplxdata.Name, String.Empty, parentTag, cmplxdata.IsArray, false));
                        //parentTag = cmplxdata.Name;
                        getParametersFromComplexData(cmplxdata, cmplxdata.Name, parameters);
                    }
                    else
                    {
                        getParametersFromComplexData(cmplxdata, parentTag, parameters);
                    }
                }
            }
        }
        
        private void getParametersFromSimpleTypes(SimpleDataCollection simpleDataCollection, string parentTag, List<Parameter> parameters)
        {
            foreach (SimpleData simpleData in simpleDataCollection)
            {
                //any type cannot simultaneosly be an array type or attribute
                parameters.Add(new Parameter(simpleData.Name, simpleData.DataType, parentTag, simpleData.IsArray, simpleData.IsAttribute));
            }
        }
        [Serializable]
        public struct Parameter
        {
            /// <summary>
            /// constructor
            /// </summary>
            /// <param name="name">
            /// <param name="type">
            public Parameter(string name, string type, string parentTag, bool isArray, bool isAttribute)
            {
                this.Name = name;
                this.Type = type;
                this.ParentTagName = parentTag;
                this.IsArray = isArray;
                this.IsAttribute = isAttribute;
            }

            /// <summary>
            /// Name
            /// </summary>
            public string Name;
            /// <summary>
            /// Type
            /// </summary>
            public string Type;

            /// <summary>
            /// This Parameter will be the child of this tag
            /// </summary>
            public string ParentTagName;


            public bool IsArray;

            public bool IsAttribute;
        }
        #endregion
        [Serializable]
        public class Restriction
        {
            private RestrictionType restrictionType;
            private object restrictionValue;
            private List<object> enumList;

            public Restriction(RestrictionType type)
            {
                restrictionType = type;
            }

            public Restriction(RestrictionType type, object value)
            {
                restrictionType = type;
                if (restrictionType == RestrictionType.Enumeration)
                {
                    if (enumList == null) enumList = new List<object>();
                    enumList.Add(value);
                }
                else
                {
                    restrictionValue = value;
                }
            }

            public RestrictionType Type
            {
                get
                {
                    return restrictionType;
                }
                set
                {
                    restrictionType = value;
                }
            }

            public object Value
            {
                get
                {
                    if (restrictionType == RestrictionType.Enumeration)
                    {
                        return enumList;
                    }
                    return restrictionValue;
                }
                set
                {
                    if (restrictionType == RestrictionType.Enumeration)
                    {
                        if (enumList == null) enumList = new List<object>();
                        enumList.Add(value);
                    }
                    else
                    {
                        restrictionValue = value;
                    }
                }
            }
        }

        [Serializable]
        public class SimpleData
        {
            private string strName, strDataType;
            private Restrictions restrictions;
            private bool isStandardDataType;
            private bool isArray = false;
            private bool isAttribute = false;
            private bool isElement = false;
            public SimpleData(string _name)
            {
                strName = _name;
                if (_name.StartsWith("ArrayOf"))
                {
                    isArray = true;
                }
            }
            public string Name
            {
                get
                {
                    return strName;
                }
            }
            public string DataType
            {
                get
                {
                    return strDataType;
                }
                set
                {
                    if (value.EndsWith("[]"))
                    {
                        isArray = true;
                        strDataType = value.Substring(0, value.Length - 2);
                    }
                    else
                    {
                        strDataType = value;
                    }
                }
            }
            public bool IsArray
            {
                get
                {
                    return isArray;
                }
            }
            public bool IsStandard
            {
                get
                {
                    return isStandardDataType;
                }
                set
                {
                    isStandardDataType = value;
                }
            }
            public bool IsAttribute
            {
                get
                {
                    return isAttribute;
                }
                set
                {
                    isAttribute = value;
                }
            }
            public bool IsElement
            {
                get
                {
                    return isElement;
                }
                set
                {
                    isElement = value;
                }
            }
            public Restrictions Restrictions
            {
                get
                {
                    return restrictions;
                }
                set
                {
                    restrictions = value;
                }
            }
        }

        [Serializable]
        public class ComplexData
        {
            private string strName;
            private SimpleDataCollection simpleDataTypes;
            private ComplexDataCollection complexDataTypes;
            private bool isArray = false;
            private bool isElement = false;
            public ComplexData(string _name)
            {
                simpleDataTypes = new SimpleDataCollection();
                complexDataTypes = new ComplexDataCollection();
                strName = _name;
                if (_name.StartsWith("ArrayOf"))
                {
                    isArray = true;
                }
                
            }

            public string Name
            {
                get
                {
                    return strName;
                }
                set
                {
                    //if (value.EndsWith("[]"))
                    //{
                    //    isArray = true;
                    //    strName = value.Substring(0, value.Length - 3);
                    //}
                    //else
                    //{
                    strName = value;
                    //}
                }
            }
            public bool IsArray
            {
                get
                {
                    return isArray;
                }
            }
            public bool IsElement
            {
                get
                {
                    return isElement;
                }
                set
                {
                    isElement = value;
                }
            }
            public SimpleDataCollection SimpleDataTypes
            {
                get
                {
                    return simpleDataTypes;
                }
            }
            public ComplexDataCollection ComplexDataTypes
            {
                get
                {
                    return complexDataTypes;
                }
            }
        }

        [Serializable]
        public class SimpleDataBuilder
        {
            private SimpleData data;
            private bool isCompleted;
            public SimpleDataBuilder(string _name)
            {
                data = new SimpleData(_name);
                isCompleted = false;
            }

            public SimpleData Data
            {
                get
                {
                    return data;
                }
            }
            public string Name
            {
                get
                {
                    return data.Name;
                }
            }
            public string DataType
            {
                get
                {
                    return data.DataType;
                }
                set
                {
                    data.DataType = value;
                }
            }
            public bool IsStandard
            {
                get
                {
                    return data.IsStandard;
                }
                set
                {
                    data.IsStandard = value;
                }
            }
            public bool IsAttribute
            {
                get
                {
                    return data.IsAttribute;
                }
                set
                {
                    data.IsAttribute = value;
                }
            }
            public bool IsElement
            {
                get
                {
                    return data.IsElement;
                }
                set
                {
                    data.IsElement = value;
                }
            }
            public Restrictions Restrictions
            {
                get
                {
                    return data.Restrictions;
                }
                set
                {
                    data.Restrictions = value;
                }
            }
            public bool IsCompleted
            {
                get
                {
                    return isCompleted;
                }
            }
            public void Complete()
            {
                isCompleted = true;
            }
        }

        [Serializable]
        public class ComplexDataBuilder
        {
            private ComplexData data;
            private bool isCompleted;

            public ComplexDataBuilder(string _name)
            {
                data = new ComplexData(_name);
                isCompleted = false;
            }

            public ComplexData Data
            {
                get
                {
                    return data;
                }
            }
            public string Name
            {
                get
                {
                    return data.Name;
                }
                set
                {
                    data.Name = value;
                }
            }
            public SimpleDataCollection SimpleDataTypes
            {
                get
                {
                    return data.SimpleDataTypes;
                }
            }
            public ComplexDataCollection ComplexDataTypes
            {
                get
                {
                    return data.ComplexDataTypes;
                }
            }
            public bool IsCompleted
            {
                get
                {
                    return isCompleted;
                }
            }
            public bool IsElement
            {
                get
                {
                    return data.IsElement;
                }
                set
                {
                    data.IsElement = value;
                }
            }
            public void Complete()
            {
                isCompleted = true;
            }
        }

        [Serializable]
        public class PortType
        {
            private string strName;
            private OperationCollection operations;

            public PortType(string _name)
            {
                strName = _name;
            }
            public string Name
            {
                get
                {
                    return strName;
                }
            }
            public OperationCollection Operations
            {
                get
                {
                    return operations;
                }
                set
                {
                    operations = value;
                }
            }
        }

        [Serializable]
        public class Operation
        {
            private string strName, strInputMessageName, strOutputMessageName;
            private PartCollection inputMessageParts, outputMessageParts;

            public Operation(string _name, string _inputMessageName, string _outputMessageName)
            {
                strName = _name;
                strInputMessageName = _inputMessageName;
                strOutputMessageName = _outputMessageName;
            }

            public string Name
            {
                get
                {
                    return strName;
                }
            }
            public string InputMessageName
            {
                get
                {
                    return strInputMessageName;
                }
            }
            public string OutputMessageName
            {
                get
                {
                    return strOutputMessageName;
                }
            }
            public PartCollection InputMessageParts
            {
                get
                {
                    return inputMessageParts;
                }
                set
                {
                    inputMessageParts = value;
                }
            }
            public PartCollection OutputMessageParts
            {
                get
                {
                    return outputMessageParts;
                }
                set
                {
                    outputMessageParts = value;
                }
            }
        }

        [Serializable]
        public class Part
        {
            private string strName, strTypeName;
            private object objType;
            private bool existsInCollection;

            public Part(string _name, string _type, bool _isExistInCollection, SimpleDataCollection _sdc, ComplexDataCollection _cdc)
            {
                strName = _name;
                strTypeName = _type;
                if (_isExistInCollection) objType = GetType(_sdc, _cdc);
                existsInCollection = _isExistInCollection;
            }
            public string Name
            {
                get
                {
                    return strName;
                }
            }
            public string TypeName
            {
                get
                {
                    return strTypeName;
                }
            }
            public object Type
            {
                get
                {
                    return objType;
                }
            }
            public bool ExistsInCollection
            {
                get
                {
                    return existsInCollection;
                }
            }
            private object GetType(SimpleDataCollection _sdc, ComplexDataCollection _cdc)
            {
                if (_sdc.Contains(TypeName)) return ((object)(_sdc.findKey(TypeName)));
                else if (_cdc.Contains(TypeName)) return ((object)(_cdc.findKey(TypeName)));
                else return null;
            }
        }

        [Serializable]
        public class Binding
        {
            private string strName, strPortType, strStyle, strBindingType, strTargetNameSpace;
            private OperationSoapActionCollection operationSoapActions;

            public Binding(string _name, string _portType, string _bindingType, string _style,string _targetNameSpace)
            {
                strName = _name;
                strPortType = _portType;
                strBindingType = _bindingType;
                strStyle = _style;
                strTargetNameSpace = _targetNameSpace;
            }
            public string Name
            {
                get
                {
                    return strName;
                }
            }
            public string PortType
            {
                get
                {
                    return strPortType;
                }
            }
            public string Style
            {
                get
                {
                    return strStyle;
                }
            }
            public string BindingType
            {
                get
                {
                    return strBindingType;
                }
            }
            public string TargetNameSpace
            {
                get
                {
                    return strTargetNameSpace;
                }
            }
            public OperationSoapActionCollection OperationSoapActions
            {
                get
                {
                    return operationSoapActions;
                }
                set
                {
                    operationSoapActions = value;
                }
            }
        }

        [Serializable]
        public class Service
        {
            private string strName;
            private PortCollection ports;

            public Service(string _name)
            {
                strName = _name;
            }

            public string Name
            {
                get
                {
                    return strName;
                }
            }
            public PortCollection Ports
            {
                get
                {
                    return ports;
                }
                set
                {
                    ports = value;
                }
            }
        }

        [Serializable]
        public class Port
        {
            private string strName, strBinding, strAddress;

            public Port(string _name, string _binding, string _Address)
            {
                strName = _name;
                strBinding = _binding;
                strAddress = _Address;
            }

            public string Name
            {
                get
                {
                    return strName;
                }
            }
            public string Binding
            {
                get
                {
                    return strBinding;
                }
            }
            public string Address
            {
                get
                {
                    return strAddress;
                }
            }
        }

        [Serializable]
        public class OperationSoapAction
        {
            private string strOperationName, strSoapAction;

            public OperationSoapAction(string operationName, string soapAction)
            {
                strOperationName = operationName;
                strSoapAction = soapAction;
            }
            public string OperationName
            {
                get
                {
                    return strOperationName;
                }
            }
            public string SoapAction
            {
                get
                {
                    return strSoapAction;
                }
            }

        }

        [Serializable]
        public class SimpleDataCollection : CollectionBase, IEnumerable
        {
            private Hashtable _values = new Hashtable();
            private Hashtable _keys = new Hashtable();

            public SimpleDataCollection()
            {
            }

            public SimpleData Add(SimpleData data)
            {
                if (string.IsNullOrEmpty(data.Name)) return null;
                if (findKey(data.Name) != null) Remove(data.Name);
                return Add(data.Name, data);
            }

            private SimpleData Add(string key, SimpleData data)
            {
                Guid gid = Guid.NewGuid();
                _values.Add(gid, data);
                _keys.Add(gid, key);
                List.Add(gid);
                return data;
            }

            public void Remove(string key)
            {
                RemoveData(key);
            }

            public void Remove(SimpleData data)
            {
                RemoveData(data);
            }

            private void RemoveData(object data)
            {
                if (data.GetType().Equals(typeof(string)))
                {
                    List<Guid> guidList = getGuid((string)data);
                    while (guidList.Count > 0)
                    {
                        foreach (Guid gid in guidList)
                        {
                            removeGuid(gid);
                            break;
                        }
                        guidList = getGuid((string)data);
                    }
                }
                else
                {
                    foreach (Guid gid in _values.Keys)
                    {
                        if (object.ReferenceEquals(_values[gid], data))
                        {
                            removeGuid(gid);
                            break;
                        }
                    }
                }
            }

            public bool Contains(string key)
            {
                if (findKey(key) == null) return false;
                return true;
            }

            public SimpleData findKey(string key)
            {
                List<object> objectList = findListFromKey(key);
                if (objectList.Count <= 0) return null;
                return (SimpleData)objectList[0];
            }

            private List<object> findListFromKey(string key)
            {
                List<object> objectList = new List<object>();
                List<Guid> guidList = getGuid(key);
                if (guidList.Count <= 0) return objectList;
                foreach (Guid gid in guidList)
                {
                    object obj = findGuid(gid);
                    if (obj != null) objectList.Add(obj);
                }
                return objectList;
            }

            private object findGuid(Guid gid)
            {
                if (_values.ContainsKey(gid)) return _values[gid];
                return null;
            }

            private void removeGuid(Guid gid)
            {
                _values.Remove(gid);
                _keys.Remove(gid);
                if (List.Contains(gid)) List.Remove(gid);
            }

            private List<Guid> getGuid(string key)
            {
                List<Guid> guidList = new List<Guid>();
                foreach (Guid gid in _keys.Keys)
                {
                    if (_keys[gid].ToString() == key) guidList.Add(gid);
                }
                //foreach (Guid gid in _keys.Keys)
                //{
                //    if (object.ReferenceEquals(_keys[gid], key)) guidList.Add(gid);
                //}
                return guidList;
            }

            public SimpleData this[int index]
            {
                get
                {
                    return (SimpleData)findGuid((Guid)List[index]);
                }
            }

            public new SimpleDataEnumerator GetEnumerator()
            {
                return new SimpleDataEnumerator(this);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public class SimpleDataEnumerator : IEnumerator
            {
                private SimpleDataCollection sd;
                private int index;

                public SimpleDataEnumerator(SimpleDataCollection _sd)
                {
                    sd = _sd;
                    index = -1;
                }
                public SimpleData Current
                {
                    get
                    {
                        return sd[index];
                    }
                }
                public bool MoveNext()
                {
                    index++;
                    return (index < sd.Count);
                }
                public void Reset()
                {
                    index = -1;
                }
                object IEnumerator.Current
                {
                    get
                    {
                        return this.Current;
                    }
                }
            }
        }

        [Serializable]
        public class SimpleDataBuilderCollection : CollectionBase
        {
            private Hashtable _values = new Hashtable();
            private Hashtable _keys = new Hashtable();

            private SimpleDataCollection _sdc;

            public SimpleDataBuilderCollection(SimpleDataCollection sdc)
            {
                _sdc = sdc;
            }

            public SimpleDataBuilder AddNew(string key)
            {
                return Add(key, new SimpleDataBuilder(key));
            }

            public SimpleDataBuilder Add(SimpleDataBuilder data)
            {
                if (string.IsNullOrEmpty(data.Name)) return null;
                return Add(data.Name, data);
            }

            private SimpleDataBuilder Add(string key, SimpleDataBuilder data)
            {
                Guid gid = Guid.NewGuid();
                _values.Add(gid, data);
                _keys.Add(gid, key);
                List.Add(gid);
                return data;
            }

            public void Remove(string key)
            {
                RemoveData(key);
            }

            public void Remove(SimpleDataBuilder data)
            {
                RemoveData(data);
            }

            private void RemoveData(object data)
            {
                if (data.GetType().Equals(typeof(string)))
                {
                    List<Guid> guidList = getGuid((string)data);
                    while (guidList.Count > 0)
                    {
                        foreach (Guid gid in guidList)
                        {
                            removeGuid(gid);
                            break;
                        }
                        guidList = getGuid((string)data);
                    }
                }
                else
                {
                    foreach (Guid gid in _values.Keys)
                    {
                        if (object.ReferenceEquals(_values[gid], data))
                        {
                            removeGuid(gid);
                            break;
                        }
                    }
                }
            }

            public bool Contains(string key)
            {
                List<SimpleDataBuilder> objectList = findKey(key);
                if (objectList.Count <= 0) return false;
                return true;
            }

            public List<SimpleDataBuilder> findKey(string key)
            {
                List<object> objectList = findListFromKey(key);
                List<SimpleDataBuilder> dataList = new List<SimpleDataBuilder>();
                foreach (object obj in objectList)
                {
                    dataList.Add((SimpleDataBuilder)obj);
                }
                return dataList;
            }

            private List<object> findListFromKey(string key)
            {
                List<object> objectList = new List<object>();
                List<Guid> guidList = getGuid(key);
                if (guidList.Count <= 0) return objectList;
                foreach (Guid gid in guidList)
                {
                    object obj = findGuid(gid);
                    if (obj != null) objectList.Add(obj);
                }
                return objectList;
            }

            private object findGuid(Guid gid)
            {
                if (_values.ContainsKey(gid)) return _values[gid];
                return null;
            }

            private void removeGuid(Guid gid)
            {
                _values.Remove(gid);
                _keys.Remove(gid);
                if (List.Contains(gid)) List.Remove(gid);
            }

            private List<Guid> getGuid(string key)
            {
                List<Guid> guidList = new List<Guid>();
                foreach (Guid gid in _keys.Keys)
                {
                    if (_keys[gid].ToString() == key) guidList.Add(gid);
                }
                //foreach (Guid gid in _keys.Keys)
                //{
                //    if (object.ReferenceEquals(_keys[gid], key)) guidList.Add(gid);
                //}
                return guidList;
            }

            public SimpleData Complete(SimpleDataBuilder sdb)
            {
                List<SimpleDataBuilder> objectList = findKey(sdb.Name);
                if (objectList.Count <= 0) return null;
                foreach (SimpleDataBuilder sdBuilder in objectList)
                {
                    if (object.ReferenceEquals(sdBuilder, sdb))
                    {
                        sdb.Complete();
                        Remove(sdb.Name);
                        return _sdc.Add(sdb.Data);
                    }
                }
                return null;
            }
        }

        [Serializable]
        public class ComplexDataCollection : CollectionBase, IEnumerable
        {
            private Hashtable _values = new Hashtable();
            private Hashtable _keys = new Hashtable();

            public ComplexDataCollection()
            {
            }

            public ComplexData Add(ComplexData data)
            {
                if (string.IsNullOrEmpty(data.Name)) return null;
                if (findKey(data.Name) != null) Remove(data.Name);
                return Add(data.Name, data);
            }

            private ComplexData Add(string key, ComplexData data)
            {
                Guid gid = Guid.NewGuid();
                _values.Add(gid, data);
                _keys.Add(gid, key);
                List.Add(gid);
                return data;
            }

            public void Remove(string key)
            {
                RemoveData(key);
            }

            public void Remove(ComplexData data)
            {
                RemoveData(data);
            }

            private void RemoveData(object data)
            {
                if (data.GetType().Equals(typeof(string)))
                {
                    List<Guid> guidList = getGuid((string)data);
                    while (guidList.Count > 0)
                    {
                        foreach (Guid gid in guidList)
                        {
                            removeGuid(gid);
                            break;
                        }
                        guidList = getGuid((string)data);
                    }
                }
                else
                {
                    foreach (Guid gid in _values.Keys)
                    {
                        if (object.ReferenceEquals(_values[gid], data))
                        {
                            removeGuid(gid);
                            break;
                        }
                    }
                }
            }

            public bool Contains(string key)
            {
                if (findKey(key) == null) return false;
                return true;
            }

            public new void Clear()
            {
                _values.Clear();
                _keys.Clear();
                base.Clear();
            }

            public new void RemoveAt(int index)
            {
                Remove(this[index]);
            }

            public ComplexData findKey(string key)
            {
                List<object> objectList = findListFromKey(key);
                if (objectList.Count <= 0) return null;
                return (ComplexData)objectList[0];
            }

            private List<object> findListFromKey(string key)
            {
                List<object> objectList = new List<object>();
                List<Guid> guidList = getGuid(key);
                if (guidList.Count <= 0) return objectList;
                foreach (Guid gid in guidList)
                {
                    object obj = findGuid(gid);
                    if (obj != null) objectList.Add(obj);
                }
                return objectList;
            }

            private object findGuid(Guid gid)
            {
                if (_values.ContainsKey(gid)) return _values[gid];
                return null;
            }

            //public ComplexData findName(string name)
            //{

            //}

            private void removeGuid(Guid gid)
            {
                _values.Remove(gid);
                _keys.Remove(gid);
                if (List.Contains(gid)) List.Remove(gid);
            }

            private List<Guid> getGuid(string key)
            {
                List<Guid> guidList = new List<Guid>();

                foreach (Guid gid in _keys.Keys)
                {
                    //if (object.ReferenceEquals(_keys[gid], key)) guidList.Add(gid);
                    if (_keys[gid].ToString() == key) guidList.Add(gid);
                }
                return guidList;
            }

            public ComplexData this[int index]
            {
                get
                {
                    return (ComplexData)findGuid((Guid)List[index]);
                }
            }

            public new ComplexDataEnumerator GetEnumerator()
            {
                return new ComplexDataEnumerator(this);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public class ComplexDataEnumerator : IEnumerator
            {
                private ComplexDataCollection cd;
                private int index;

                public ComplexDataEnumerator(ComplexDataCollection _cd)
                {
                    cd = _cd;
                    index = -1;
                }
                public ComplexData Current
                {
                    get
                    {
                        return cd[index];
                    }
                }
                public bool MoveNext()
                {
                    index++;
                    return (index < cd.Count);
                }
                public void Reset()
                {
                    index = -1;
                }
                object IEnumerator.Current
                {
                    get
                    {
                        return this.Current;
                    }
                }
            }
        }

        [Serializable]
        public class ComplexDataBuilderCollection : CollectionBase
        {
            private Hashtable _values = new Hashtable();
            private Hashtable _keys = new Hashtable();

            private ComplexDataCollection _cdc;

            public ComplexDataBuilderCollection(ComplexDataCollection cdc)
            {
                _cdc = cdc;
            }

            public ComplexDataBuilder AddNew(string key)
            {
                return Add(key, new ComplexDataBuilder(key));
            }

            public ComplexDataBuilder Add(ComplexDataBuilder data)
            {
                if (string.IsNullOrEmpty(data.Name)) return null;
                return Add(data.Name, data);
            }

            private ComplexDataBuilder Add(string key, ComplexDataBuilder data)
            {
                Guid gid = Guid.NewGuid();
                _values.Add(gid, data);
                _keys.Add(gid, key);
                List.Add(gid);
                return data;
            }

            public void Remove(string key)
            {
                RemoveData(key);
            }

            public void Remove(ComplexDataBuilder data)
            {
                RemoveData(data);
            }

            private void RemoveData(object data)
            {
                if (data.GetType().Equals(typeof(string)))
                {
                    List<Guid> guidList = getGuid((string)data);
                    while (guidList.Count > 0)
                    {
                        foreach (Guid gid in guidList)
                        {
                            removeGuid(gid);
                            break;
                        }
                        guidList = getGuid((string)data);
                    }
                }
                else
                {
                    foreach (Guid gid in _values.Keys)
                    {
                        if (object.ReferenceEquals(_values[gid], data))
                        {
                            removeGuid(gid);
                            break;
                        }
                    }
                }
            }

            public bool Contains(string key)
            {
                List<ComplexDataBuilder> objectList = findKey(key);
                if (objectList.Count <= 0) return false;
                return true;
            }

            public List<ComplexDataBuilder> findKey(string key)
            {
                List<object> objectList = findListFromKey(key);
                List<ComplexDataBuilder> dataList = new List<ComplexDataBuilder>();
                foreach (object obj in objectList)
                {
                    dataList.Add((ComplexDataBuilder)obj);
                }
                return dataList;
            }

            private List<object> findListFromKey(string key)
            {
                List<object> objectList = new List<object>();
                List<Guid> guidList = getGuid(key);
                if (guidList.Count <= 0) return objectList;
                foreach (Guid gid in guidList)
                {
                    object obj = findGuid(gid);
                    if (obj != null) objectList.Add(obj);
                }
                return objectList;
            }

            private object findGuid(Guid gid)
            {
                if (_values.ContainsKey(gid)) return _values[gid];
                return null;
            }

            private void removeGuid(Guid gid)
            {
                _values.Remove(gid);
                _keys.Remove(gid);
                if (List.Contains(gid)) List.Remove(gid);
            }

            private List<Guid> getGuid(string key)
            {
                List<Guid> guidList = new List<Guid>();

                foreach (Guid gid in _keys.Keys)
                {
                    //if (object.ReferenceEquals(_keys[gid], key)) guidList.Add(gid);
                    if (_keys[gid].ToString() == key) guidList.Add(gid);
                }
                return guidList;
            }

            public ComplexData Complete(ComplexDataBuilder cdb)
            {
                List<ComplexDataBuilder> objectList = findKey(cdb.Name);
                if (objectList.Count <= 0) return null;
                foreach (ComplexDataBuilder cdBuilder in objectList)
                {
                    if (object.ReferenceEquals(cdBuilder, cdb))
                    {
                        cdb.Complete();
                        Remove(cdb.Name);
                        return _cdc.Add(cdb.Data);
                    }
                }
                return null;
            }
        }

        [Serializable]
        public class Restrictions : CollectionBase
        {
            string baseType;

            public Restrictions()
            {
            }

            public Restrictions(string base_type)
            {
                baseType = base_type;
            }

            public string BaseType
            {
                get
                {
                    return baseType;
                }
                set
                {
                    baseType = value;
                }
            }

            public Restriction this[int index]
            {
                get
                {
                    return ((Restriction)List[index]);
                }
                //set  
                //{
                //    List[index] = value;
                //}
            }

            public void Add(Restriction restriction)
            {
                if ((restriction.Type != RestrictionType.Enumeration) || (List.Count <= 0))
                {
                    List.Add(restriction);
                    return;
                }

                foreach (Restriction r in List)
                {
                    if (r.Type == RestrictionType.Enumeration)
                    {
                        r.Value = restriction.Value;
                        return;
                    }
                }

                List.Add(restriction);
            }
        }

        [Serializable]
        public class BindingCollection : CollectionBase, IEnumerable
        {
            private Hashtable _values = new Hashtable();
            private Hashtable _keys = new Hashtable();

            public BindingCollection()
            {
            }

            public void Add(Binding data)
            {
                if (string.IsNullOrEmpty(data.Name)) return;
                if (findKey(data.Name) != null) Remove(data.Name);
                //return Add(data.Name, data);
                Add(data.Name, data);
            }

            private void Add(string key, Binding data)
            {
                Guid gid = Guid.NewGuid();
                _values.Add(gid, data);
                _keys.Add(gid, key);
                List.Add(gid);
                //return data;
            }

            public void Remove(string key)
            {
                RemoveData(key);
            }

            public void Remove(Binding data)
            {
                RemoveData(data);
            }

            private void RemoveData(object data)
            {
                if (data.GetType().Equals(typeof(string)))
                {
                    List<Guid> guidList = getGuid((string)data);
                    while (guidList.Count > 0)
                    {
                        foreach (Guid gid in guidList)
                        {
                            removeGuid(gid);
                            break;
                        }
                        guidList = getGuid((string)data);
                    }
                }
                else
                {
                    foreach (Guid gid in _values.Keys)
                    {
                        if (object.ReferenceEquals(_values[gid], data))
                        {
                            removeGuid(gid);
                            break;
                        }
                    }
                }
            }

            public bool Contains(string key)
            {
                if (findKey(key) == null) return false;
                return true;
            }

            public Binding findKey(string key)
            {
                List<object> objectList = findListFromKey(key);
                if (objectList.Count <= 0) return null;
                return (Binding)objectList[0];
            }

            private List<object> findListFromKey(string key)
            {
                List<object> objectList = new List<object>();
                List<Guid> guidList = getGuid(key);
                if (guidList.Count <= 0) return objectList;
                foreach (Guid gid in guidList)
                {
                    object obj = findGuid(gid);
                    if (obj != null) objectList.Add(obj);
                }
                return objectList;
            }

            private object findGuid(Guid gid)
            {
                if (_values.ContainsKey(gid)) return _values[gid];
                return null;
            }

            private void removeGuid(Guid gid)
            {
                _values.Remove(gid);
                _keys.Remove(gid);
                if (List.Contains(gid)) List.Remove(gid);
            }

            private List<Guid> getGuid(string key)
            {
                List<Guid> guidList = new List<Guid>();
                foreach (Guid gid in _keys.Keys)
                {
                    if (_keys[gid].ToString() == key) guidList.Add(gid);
                }
                //foreach (Guid gid in _keys.Keys)
                //{
                //    if (object.ReferenceEquals(_keys[gid], key)) guidList.Add(gid);
                //}
                return guidList;
            }

            public Binding this[int index]
            {
                get
                {
                    return (Binding)findGuid((Guid)List[index]);
                }
            }

            public new BindingEnumerator GetEnumerator()
            {
                return new BindingEnumerator(this);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public class BindingEnumerator : IEnumerator
            {
                private BindingCollection sd;
                private int index;

                public BindingEnumerator(BindingCollection _sd)
                {
                    sd = _sd;
                    index = -1;
                }
                public Binding Current
                {
                    get
                    {
                        return sd[index];
                    }
                }
                public bool MoveNext()
                {
                    index++;
                    return (index < sd.Count);
                }
                public void Reset()
                {
                    index = -1;
                }
                object IEnumerator.Current
                {
                    get
                    {
                        return this.Current;
                    }
                }
            }
        }

        [Serializable]
        public class PortTypeCollection : CollectionBase, IEnumerable
        {
            private Hashtable _values = new Hashtable();
            private Hashtable _keys = new Hashtable();

            public PortTypeCollection()
            {
            }

            public void Add(PortType data)
            {
                if (string.IsNullOrEmpty(data.Name)) return;
                if (findKey(data.Name) != null) Remove(data.Name);
                Add(data.Name, data);
            }

            private void Add(string key, PortType data)
            {
                Guid gid = Guid.NewGuid();
                _values.Add(gid, data);
                _keys.Add(gid, key);
                List.Add(gid);
            }

            public void Remove(string key)
            {
                RemoveData(key);
            }

            public void Remove(PortType data)
            {
                RemoveData(data);
            }

            private void RemoveData(object data)
            {
                if (data.GetType().Equals(typeof(string)))
                {
                    List<Guid> guidList = getGuid((string)data);
                    while (guidList.Count > 0)
                    {
                        foreach (Guid gid in guidList)
                        {
                            removeGuid(gid);
                            break;
                        }
                        guidList = getGuid((string)data);
                    }
                }
                else
                {
                    foreach (Guid gid in _values.Keys)
                    {
                        if (object.ReferenceEquals(_values[gid], data))
                        {
                            removeGuid(gid);
                            break;
                        }
                    }
                }
            }

            public bool Contains(string key)
            {
                if (findKey(key) == null) return false;
                return true;
            }

            public PortType findKey(string key)
            {
                List<object> objectList = findListFromKey(key);
                if (objectList.Count <= 0) return null;
                return (PortType)objectList[0];
            }

            private List<object> findListFromKey(string key)
            {
                List<object> objectList = new List<object>();
                List<Guid> guidList = getGuid(key);
                if (guidList.Count <= 0) return objectList;
                foreach (Guid gid in guidList)
                {
                    object obj = findGuid(gid);
                    if (obj != null) objectList.Add(obj);
                }
                return objectList;
            }

            private object findGuid(Guid gid)
            {
                if (_values.ContainsKey(gid)) return _values[gid];
                return null;
            }

            private void removeGuid(Guid gid)
            {
                _values.Remove(gid);
                _keys.Remove(gid);
                if (List.Contains(gid)) List.Remove(gid);
            }

            private List<Guid> getGuid(string key)
            {
                List<Guid> guidList = new List<Guid>();
                foreach (Guid gid in _keys.Keys)
                {
                    if (_keys[gid].ToString() == key) guidList.Add(gid);
                }
                //foreach (Guid gid in _keys.Keys)
                //{
                //    if (object.ReferenceEquals(_keys[gid], key)) guidList.Add(gid);
                //}
                return guidList;
            }

            public PortType this[int index]
            {
                get
                {
                    return (PortType)findGuid((Guid)List[index]);
                }
            }

            public new PortTypeEnumerator GetEnumerator()
            {
                return new PortTypeEnumerator(this);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public class PortTypeEnumerator : IEnumerator
            {
                private PortTypeCollection pt;
                private int index;

                public PortTypeEnumerator(PortTypeCollection _pt)
                {
                    pt = _pt;
                    index = -1;
                }
                public PortType Current
                {
                    get
                    {
                        return pt[index];
                    }
                }
                public bool MoveNext()
                {
                    index++;
                    return (index < pt.Count);
                }
                public void Reset()
                {
                    index = -1;
                }
                object IEnumerator.Current
                {
                    get
                    {
                        return this.Current;
                    }
                }
            }
        }

        [Serializable]
        public class ServiceCollection : CollectionBase, IEnumerable
        {
            private Hashtable _values = new Hashtable();
            private Hashtable _keys = new Hashtable();

            public ServiceCollection()
            {
            }

            public void Add(Service data)
            {
                if (string.IsNullOrEmpty(data.Name)) return;
                if (findKey(data.Name) != null) Remove(data.Name);
                Add(data.Name, data);
            }

            private void Add(string key, Service data)
            {
                Guid gid = Guid.NewGuid();
                _values.Add(gid, data);
                _keys.Add(gid, key);
                List.Add(gid);
            }

            public void Remove(string key)
            {
                RemoveData(key);
            }

            public void Remove(Service data)
            {
                RemoveData(data);
            }

            private void RemoveData(object data)
            {
                if (data.GetType().Equals(typeof(string)))
                {
                    List<Guid> guidList = getGuid((string)data);
                    while (guidList.Count > 0)
                    {
                        foreach (Guid gid in guidList)
                        {
                            removeGuid(gid);
                            break;
                        }
                        guidList = getGuid((string)data);
                    }
                }
                else
                {
                    foreach (Guid gid in _values.Keys)
                    {
                        if (object.ReferenceEquals(_values[gid], data))
                        {
                            removeGuid(gid);
                            break;
                        }
                    }
                }
            }

            public bool Contains(string key)
            {
                if (findKey(key) == null) return false;
                return true;
            }

            public Service findKey(string key)
            {
                List<object> objectList = findListFromKey(key);
                if (objectList.Count <= 0) return null;
                return (Service)objectList[0];
            }

            private List<object> findListFromKey(string key)
            {
                List<object> objectList = new List<object>();
                List<Guid> guidList = getGuid(key);
                if (guidList.Count <= 0) return objectList;
                foreach (Guid gid in guidList)
                {
                    object obj = findGuid(gid);
                    if (obj != null) objectList.Add(obj);
                }
                return objectList;
            }

            private object findGuid(Guid gid)
            {
                if (_values.ContainsKey(gid)) return _values[gid];
                return null;
            }

            private void removeGuid(Guid gid)
            {
                _values.Remove(gid);
                _keys.Remove(gid);
                if (List.Contains(gid)) List.Remove(gid);
            }

            private List<Guid> getGuid(string key)
            {
                List<Guid> guidList = new List<Guid>();
                foreach (Guid gid in _keys.Keys)
                {
                    if (_keys[gid].ToString() == key) guidList.Add(gid);
                }
                //foreach (Guid gid in _keys.Keys)
                //{
                //    if (object.ReferenceEquals(_keys[gid], key)) guidList.Add(gid);
                //}
                return guidList;
            }

            public Service this[int index]
            {
                get
                {
                    return (Service)findGuid((Guid)List[index]);
                }
            }

            public new ServiceEnumerator GetEnumerator()
            {
                return new ServiceEnumerator(this);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public class ServiceEnumerator : IEnumerator
            {
                private ServiceCollection sv;
                private int index;

                public ServiceEnumerator(ServiceCollection _sv)
                {
                    sv = _sv;
                    index = -1;
                }
                public Service Current
                {
                    get
                    {
                        return sv[index];
                    }
                }
                public bool MoveNext()
                {
                    index++;
                    return (index < sv.Count);
                }
                public void Reset()
                {
                    index = -1;
                }
                object IEnumerator.Current
                {
                    get
                    {
                        return this.Current;
                    }
                }
            }
        }

        [Serializable]
        public class OperationCollection : CollectionBase, IEnumerable
        {
            private Hashtable _values = new Hashtable();
            private Hashtable _keys = new Hashtable();

            public OperationCollection()
            {
            }

            public void Add(Operation data)
            {
                if (string.IsNullOrEmpty(data.Name)) return;
                if (findKey(data.Name) != null) Remove(data.Name);
                Add(data.Name, data);
            }

            private void Add(string key, Operation data)
            {
                Guid gid = Guid.NewGuid();
                _values.Add(gid, data);
                _keys.Add(gid, key);
                List.Add(gid);
            }

            public void Remove(string key)
            {
                RemoveData(key);
            }

            public void Remove(Service data)
            {
                RemoveData(data);
            }

            private void RemoveData(object data)
            {
                if (data.GetType().Equals(typeof(string)))
                {
                    List<Guid> guidList = getGuid((string)data);
                    while (guidList.Count > 0)
                    {
                        foreach (Guid gid in guidList)
                        {
                            removeGuid(gid);
                            break;
                        }
                        guidList = getGuid((string)data);
                    }
                }
                else
                {
                    foreach (Guid gid in _values.Keys)
                    {
                        if (object.ReferenceEquals(_values[gid], data))
                        {
                            removeGuid(gid);
                            break;
                        }
                    }
                }
            }

            public bool Contains(string key)
            {
                if (findKey(key) == null) return false;
                return true;
            }

            public Operation findKey(string key)
            {
                List<object> objectList = findListFromKey(key);
                if (objectList.Count <= 0) return null;
                return (Operation)objectList[0];
            }

            private List<object> findListFromKey(string key)
            {
                List<object> objectList = new List<object>();
                List<Guid> guidList = getGuid(key);
                if (guidList.Count <= 0) return objectList;
                foreach (Guid gid in guidList)
                {
                    object obj = findGuid(gid);
                    if (obj != null) objectList.Add(obj);
                }
                return objectList;
            }

            private object findGuid(Guid gid)
            {
                if (_values.ContainsKey(gid)) return _values[gid];
                return null;
            }

            private void removeGuid(Guid gid)
            {
                _values.Remove(gid);
                _keys.Remove(gid);
                if (List.Contains(gid)) List.Remove(gid);
            }

            private List<Guid> getGuid(string key)
            {
                List<Guid> guidList = new List<Guid>();
                foreach (Guid gid in _keys.Keys)
                {
                    if (_keys[gid].ToString() == key) guidList.Add(gid);
                }
                //foreach (Guid gid in _keys.Keys)
                //{
                //    if (object.ReferenceEquals(_keys[gid], key)) guidList.Add(gid);
                //}
                return guidList;
            }

            public Operation this[int index]
            {
                get
                {
                    return (Operation)findGuid((Guid)List[index]);
                }
            }

            public new OperationEnumerator GetEnumerator()
            {
                return new OperationEnumerator(this);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public class OperationEnumerator : IEnumerator
            {
                private OperationCollection op;
                private int index;

                public OperationEnumerator(OperationCollection _op)
                {
                    op = _op;
                    index = -1;
                }
                public Operation Current
                {
                    get
                    {
                        return op[index];
                    }
                }
                public bool MoveNext()
                {
                    index++;
                    return (index < op.Count);
                }
                public void Reset()
                {
                    index = -1;
                }
                object IEnumerator.Current
                {
                    get
                    {
                        return this.Current;
                    }
                }
            }
        }

        [Serializable]
        public class PartCollection : CollectionBase, IEnumerable
        {
            private Hashtable _values = new Hashtable();
            private Hashtable _keys = new Hashtable();

            public PartCollection()
            {
            }

            public void Add(Part data)
            {
                if (string.IsNullOrEmpty(data.Name)) return;
                if (findKey(data.Name) != null) Remove(data.Name);
                Add(data.Name, data);
            }

            private void Add(string key, Part data)
            {
                Guid gid = Guid.NewGuid();
                _values.Add(gid, data);
                _keys.Add(gid, key);
                List.Add(gid);
            }

            public void Remove(string key)
            {
                RemoveData(key);
            }

            public void Remove(Service data)
            {
                RemoveData(data);
            }

            private void RemoveData(object data)
            {
                if (data.GetType().Equals(typeof(string)))
                {
                    List<Guid> guidList = getGuid((string)data);
                    while (guidList.Count > 0)
                    {
                        foreach (Guid gid in guidList)
                        {
                            removeGuid(gid);
                            break;
                        }
                        guidList = getGuid((string)data);
                    }
                }
                else
                {
                    foreach (Guid gid in _values.Keys)
                    {
                        if (object.ReferenceEquals(_values[gid], data))
                        {
                            removeGuid(gid);
                            break;
                        }
                    }
                }
            }

            public bool Contains(string key)
            {
                if (findKey(key) == null) return false;
                return true;
            }

            public Part findKey(string key)
            {
                List<object> objectList = findListFromKey(key);
                if (objectList.Count <= 0) return null;
                return (Part)objectList[0];
            }

            private List<object> findListFromKey(string key)
            {
                List<object> objectList = new List<object>();
                List<Guid> guidList = getGuid(key);
                if (guidList.Count <= 0) return objectList;
                foreach (Guid gid in guidList)
                {
                    object obj = findGuid(gid);
                    if (obj != null) objectList.Add(obj);
                }
                return objectList;
            }

            private object findGuid(Guid gid)
            {
                if (_values.ContainsKey(gid)) return _values[gid];
                return null;
            }

            private void removeGuid(Guid gid)
            {
                _values.Remove(gid);
                _keys.Remove(gid);
                if (List.Contains(gid)) List.Remove(gid);
            }

            private List<Guid> getGuid(string key)
            {
                List<Guid> guidList = new List<Guid>();
                foreach (Guid gid in _keys.Keys)
                {
                    if (_keys[gid].ToString() == key) guidList.Add(gid);
                }
                //foreach (Guid gid in _keys.Keys)
                //{
                //    if (object.ReferenceEquals(_keys[gid], key)) guidList.Add(gid);
                //}
                return guidList;
            }

            public Part this[int index]
            {
                get
                {
                    return (Part)findGuid((Guid)List[index]);
                }
            }

            public new PartEnumerator GetEnumerator()
            {
                return new PartEnumerator(this);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public class PartEnumerator : IEnumerator
            {
                private PartCollection pt;
                private int index;

                public PartEnumerator(PartCollection _pt)
                {
                    pt = _pt;
                    index = -1;
                }
                public Part Current
                {
                    get
                    {
                        return pt[index];
                    }
                }
                public bool MoveNext()
                {
                    index++;
                    return (index < pt.Count);
                }
                public void Reset()
                {
                    index = -1;
                }
                object IEnumerator.Current
                {
                    get
                    {
                        return this.Current;
                    }
                }
            }
        }

        [Serializable]
        public class PortCollection : CollectionBase, IEnumerable
        {
            private Hashtable _values = new Hashtable();
            private Hashtable _keys = new Hashtable();

            public PortCollection()
            {
            }

            public void Add(Port data)
            {
                if (string.IsNullOrEmpty(data.Name)) return;
                if (findKey(data.Name) != null) Remove(data.Name);
                Add(data.Name, data);
            }

            private void Add(string key, Port data)
            {
                Guid gid = Guid.NewGuid();
                _values.Add(gid, data);
                _keys.Add(gid, key);
                List.Add(gid);
            }

            public void Remove(string key)
            {
                RemoveData(key);
            }

            public void Remove(Port data)
            {
                RemoveData(data);
            }

            private void RemoveData(object data)
            {
                if (data.GetType().Equals(typeof(string)))
                {
                    List<Guid> guidList = getGuid((string)data);
                    while (guidList.Count > 0)
                    {
                        foreach (Guid gid in guidList)
                        {
                            removeGuid(gid);
                            break;
                        }
                        guidList = getGuid((string)data);
                    }
                }
                else
                {
                    foreach (Guid gid in _values.Keys)
                    {
                        if (object.ReferenceEquals(_values[gid], data))
                        {
                            removeGuid(gid);
                            break;
                        }
                    }
                }
            }

            public bool Contains(string key)
            {
                if (findKey(key) == null) return false;
                return true;
            }

            public Port findKey(string key)
            {
                List<object> objectList = findListFromKey(key);
                if (objectList.Count <= 0) return null;
                return (Port)objectList[0];
            }

            private List<object> findListFromKey(string key)
            {
                List<object> objectList = new List<object>();
                List<Guid> guidList = getGuid(key);
                if (guidList.Count <= 0) return objectList;
                foreach (Guid gid in guidList)
                {
                    object obj = findGuid(gid);
                    if (obj != null) objectList.Add(obj);
                }
                return objectList;
            }

            private object findGuid(Guid gid)
            {
                if (_values.ContainsKey(gid)) return _values[gid];
                return null;
            }

            private void removeGuid(Guid gid)
            {
                _values.Remove(gid);
                _keys.Remove(gid);
                if (List.Contains(gid)) List.Remove(gid);
            }

            private List<Guid> getGuid(string key)
            {
                List<Guid> guidList = new List<Guid>();
                foreach (Guid gid in _keys.Keys)
                {
                    if (_keys[gid].ToString() == key) guidList.Add(gid);
                }
                //foreach (Guid gid in _keys.Keys)
                //{
                //    if (object.ReferenceEquals(_keys[gid], key)) guidList.Add(gid);
                //}
                return guidList;
            }

            public Port this[int index]
            {
                get
                {
                    return (Port)findGuid((Guid)List[index]);
                }
            }

            public new PortEnumerator GetEnumerator()
            {
                return new PortEnumerator(this);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public class PortEnumerator : IEnumerator
            {
                private PortCollection pt;
                private int index;

                public PortEnumerator(PortCollection _pt)
                {
                    pt = _pt;
                    index = -1;
                }
                public Port Current
                {
                    get
                    {
                        return pt[index];
                    }
                }
                public bool MoveNext()
                {
                    index++;
                    return (index < pt.Count);
                }
                public void Reset()
                {
                    index = -1;
                }
                object IEnumerator.Current
                {
                    get
                    {
                        return this.Current;
                    }
                }
            }
        }

        [Serializable]
        public class OperationSoapActionCollection : CollectionBase, IEnumerable
        {
            private Hashtable _values = new Hashtable();
            private Hashtable _keys = new Hashtable();

            public OperationSoapActionCollection()
            {
            }

            public void Add(OperationSoapAction data)
            {
                if (string.IsNullOrEmpty(data.OperationName)) return;
                if (findKey(data.OperationName) != null) Remove(data.OperationName);
                Add(data.OperationName, data);
            }

            private void Add(string key, OperationSoapAction data)
            {
                Guid gid = Guid.NewGuid();
                _values.Add(gid, data);
                _keys.Add(gid, key);
                List.Add(gid);
            }

            public void Remove(string key)
            {
                RemoveData(key);
            }

            public void Remove(Port data)
            {
                RemoveData(data);
            }

            private void RemoveData(object data)
            {
                if (data.GetType().Equals(typeof(string)))
                {
                    List<Guid> guidList = getGuid((string)data);
                    while (guidList.Count > 0)
                    {
                        foreach (Guid gid in guidList)
                        {
                            removeGuid(gid);
                            break;
                        }
                        guidList = getGuid((string)data);
                    }
                }
                else
                {
                    foreach (Guid gid in _values.Keys)
                    {
                        if (object.ReferenceEquals(_values[gid], data))
                        {
                            removeGuid(gid);
                            break;
                        }
                    }
                }
            }

            public bool Contains(string key)
            {
                if (findKey(key) == null) return false;
                return true;
            }

            public OperationSoapAction findKey(string key)
            {
                List<object> objectList = findListFromKey(key);
                if (objectList.Count <= 0) return null;
                return (OperationSoapAction)objectList[0];
            }

            private List<object> findListFromKey(string key)
            {
                List<object> objectList = new List<object>();
                List<Guid> guidList = getGuid(key);
                if (guidList.Count <= 0) return objectList;
                foreach (Guid gid in guidList)
                {
                    object obj = findGuid(gid);
                    if (obj != null) objectList.Add(obj);
                }
                return objectList;
            }

            private object findGuid(Guid gid)
            {
                if (_values.ContainsKey(gid)) return _values[gid];
                return null;
            }

            private void removeGuid(Guid gid)
            {
                _values.Remove(gid);
                _keys.Remove(gid);
                if (List.Contains(gid)) List.Remove(gid);
            }

            private List<Guid> getGuid(string key)
            {
                List<Guid> guidList = new List<Guid>();
                foreach (Guid gid in _keys.Keys)
                {
                    if (_keys[gid].ToString() == key) guidList.Add(gid);
                }
                //foreach (Guid gid in _keys.Keys)
                //{
                //    if (object.ReferenceEquals(_keys[gid], key)) guidList.Add(gid);
                //}
                return guidList;
            }

            public OperationSoapAction this[int index]
            {
                get
                {
                    return (OperationSoapAction)findGuid((Guid)List[index]);
                }
            }

            public new OperationSoapActionEnumerator GetEnumerator()
            {
                return new OperationSoapActionEnumerator(this);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public class OperationSoapActionEnumerator : IEnumerator
            {
                private OperationSoapActionCollection pt;
                private int index;

                public OperationSoapActionEnumerator(OperationSoapActionCollection _pt)
                {
                    pt = _pt;
                    index = -1;
                }
                public OperationSoapAction Current
                {
                    get
                    {
                        return pt[index];
                    }
                }
                public bool MoveNext()
                {
                    index++;
                    return (index < pt.Count);
                }
                public void Reset()
                {
                    index = -1;
                }
                object IEnumerator.Current
                {
                    get
                    {
                        return this.Current;
                    }
                }
            }
        }

        [Serializable]
        public enum RestrictionType
        {
            MinExclusive = 0,
            MinInclusive = 1,
            MaxExclusive = 2,
            MaxInclusive = 3,
            FractionDigits = 4,
            Length = 5,
            MinLength = 6,
            Enumeration = 7,
            WhiteSpace = 8,
            Pattern = 9
        }
    }
}
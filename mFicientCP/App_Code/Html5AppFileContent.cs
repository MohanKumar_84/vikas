﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace mFicientCP
{
    public static class Html5AppFileContent
    {
        public static FileType GetFileTypeinZippedFile(string fileExtension)
        {
            FileType fileType = FileType.Other;
            switch (fileExtension)
            {
                case ".png":
                case ".jpeg":
                case "jpg":
                case "gif":
                    fileType = FileType.Image;
                    break;
                case ".js":
                    fileType = FileType.Script;
                    break;
                case ".css":
                    fileType = FileType.Style;
                    break;
                case ".htm":
                case ".html":
                case ".xml":
                case ".txt":
                    fileType = FileType.Root;
                    break;
            }
            return fileType;
        }

        public static FileType GetFileType(string fileExtension)
        {
            FileType fileType = FileType.Other;
            switch (fileExtension)
            {
                case "png":
                case "jpeg":
                case "jpg":
                case "gif":
                    fileType = FileType.Image;
                    break;
                case "js":
                    fileType = FileType.Script;
                    break;
                case "css":
                    fileType = FileType.Style;
                    break;
                case "htm":
                case "html":
                case "xml":
                case "txt":
                    fileType = FileType.Root;
                    break;
                case "zip":
                    fileType = FileType.Zip;
                    break;
            }
            return fileType;
        }

        public static string GetFileContent(string filePath, string appId, string enterpriseId)
        {
            string fileContent = string.Empty;
            string query = string.Empty;
            try
            {
                switch (GetFileTypeinZippedFile(Path.GetExtension(filePath)))
                {
                    case FileType.Image:
                        query = @"SELECT * FROM TBL_HTML5_IMAGE_FILES WHERE APP_ID = @APP_ID AND COMPANY_ID = @COMPANY_ID AND FILENAME = @FILENAME";
                        break;
                    case FileType.Script:
                        query = @"SELECT * FROM TBL_HTML5_SCRIPT_FILES WHERE APP_ID = @APP_ID AND COMPANY_ID = @COMPANY_ID AND FILENAME = @FILENAME";
                        break;
                    case FileType.Style:
                        query = @"SELECT * FROM TBL_HTML5_STYLE_FILES WHERE APP_ID = @APP_ID AND COMPANY_ID = @COMPANY_ID AND FILENAME = @FILENAME";
                        break;
                    case FileType.Root:
                        query = @"SELECT * FROM TBL_HTML5_ROOT_FILES WHERE APP_ID = @APP_ID AND COMPANY_ID = @COMPANY_ID AND FILENAME = @FILENAME";
                        break;
                }
                try
                {
                    SqlCommand cmd = new SqlCommand(query);
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@APP_ID", appId);
                    cmd.Parameters.AddWithValue("@FILENAME", Path.GetFileName(filePath));
                    cmd.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);

                    DataTable dt = MSSqlClient.SelectDataFromSQlCommand(cmd).Tables[0];
                    if (dt != null && dt.Rows.Count > 1)
                    {
                        fileContent = Convert.ToString(dt.Rows[0]["CONTENT"]);
                    }
                }
                catch
                {
                }
            }
            catch
            {

            }
            return fileContent;
        }

        public static void SaveApp(Html5App app)
        {
            try
            {

            }
            catch
            { 
            }
        }

        public static string GetAppId()
        {
            return Utilities.GetMd5Hash(DateTime.UtcNow.Ticks.ToString());
        }
    }
}
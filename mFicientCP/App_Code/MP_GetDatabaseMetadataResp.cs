﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
namespace mFicientCP
{
    public class MP_GetDatabseMetadataResp
    {
        //{resp:{\"cd\":\"" + _statusCode + "\",\"desc\":\"" + _description + "\",\"data\":\"" + _data + "\"}}
        string _code, _description;

        public string Description
        {
            get { return _description; }
        }

        public string Code
        {
            get { return _code; }
        }
        DataSet _data;
        public DataSet Data
        {
            get { return _data; }
        }
        public int StatusCode
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public MP_GetDatabseMetadataResp(string json)
        {
            this.StatusCode = -1000;
            try
            {
                MPGetMetaDataResponse objGetMetaDataResponseParsing = DeserialiseJson<MPGetMetaDataResponse>(json);
                _code = objGetMetaDataResponseParsing.resp.cd;
                _description = objGetMetaDataResponseParsing.resp.desc;
                if (Convert.ToInt32(_code) == 0)
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(objGetMetaDataResponseParsing.resp.data);
                    MemoryStream stream = new MemoryStream(byteArray);
                    _data = new DataSet();
                    _data.ReadXml(stream);
                }
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }


        T DeserialiseJson<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer serialiser = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serialiser.ReadObject(ms);
            ms.Close();
            return obj;
        }
    }

    [DataContract]
    public class MPGetMetaDataResponse
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public MPGetMetaDataRespFields resp { get; set; }
    }

    [DataContract]
    public class MPGetMetaDataRespFields
    {

        [DataMember]
        public string cd { get; set; }

        [DataMember]
        public string desc { get; set; }

        [DataMember]
        public string data { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetRoleList
    {
        public GetRoleList()
        {

        }

        public void Process()
        {
            try
            {
                StatusCode = -1000;
                string strQuery = "SELECT * FROM TBL_MST_ROLE;";
              
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    StatusCode = 0;
                    StatusDescription = "";
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    StatusDescription = "No roles found";
                }
            }
            catch
            {
                StatusDescription = "Internal server error";
            }
        }

        public DataTable ResultTable
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }



    //public class EntityCollection : IEnumerable<Entity>
    //{ 

    //}
}
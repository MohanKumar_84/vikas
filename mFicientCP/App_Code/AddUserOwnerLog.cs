﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class ChangeSubAdminOfUsers
    {
        //public ChangeSubAdminOfUsers(string _CompanyId, string _SubAdminId,)
        //{
        //    CompanyId = _CompanyId;
        //    SubAdminId = _SubAdminId;
        //    UserId = _UserId;
        //}
        public ChangeSubAdminOfUsers(string companyId, string newSubAdminId)
        {
            CompanyId = companyId;
            SubAdminId = newSubAdminId;
        }
        public void Process( string _UserId)
        {
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            try
            {
                this.UserId = _UserId;
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                string query = @"INSERT INTO TBL_USER_OWNER_LOG(USER_OWNER_ID,COMPANY_ID,USER_ID,CHANGE_DATETIME)
                                VALUES(@USER_OWNER_ID,@COMPANY_ID,@USER_ID,@CHANGE_DATETIME);";

                objSqlTransaction = objSqlConnection.BeginTransaction();
                SqlCommand objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_OWNER_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@CHANGE_DATETIME", DateTime.UtcNow.Ticks);
                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    objSqlTransaction.Rollback();
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;

                }
                string query2 = @"UPDATE TBL_USER_DETAIL SET SUBADMIN_ID = @SUBADMIN_ID WHERE USER_ID = @USER_ID";
                objSqlCommand = new SqlCommand(query2, objSqlConnection, objSqlTransaction);

                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@USER_ID", UserId);
                if (objSqlCommand.ExecuteNonQuery() > 0)
                {

                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    objSqlTransaction.Rollback();
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                objSqlTransaction.Commit();
            }
            catch (Exception ex)
            {
                objSqlTransaction.Rollback();
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }
        public void ProcessForMultipleUsers(List<MFEMobileUser> usersToChangeSubAdmin)
        {
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        foreach (MFEMobileUser userDtl in usersToChangeSubAdmin)
                        {
                            insertUserOwnerLog(con, transaction, userDtl.UserId);
                            updateUserSubAdmin(con, transaction, userDtl.UserId);
                        }
                        transaction.Commit();
                        this.StatusCode = 0;
                        this.StatusDescription = String.Empty;
                    }
                }

            }
            catch (SqlException)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
            #region Previous Code
            //SqlConnection objSqlConnection = null;
            //SqlTransaction objSqlTransaction = null;
            //try
            //{
            //    MSSqlClient.SqlConnectionOpen(out objSqlConnection);
            //    objSqlTransaction = objSqlConnection.BeginTransaction();
            //    if (objSqlCommand.ExecuteNonQuery() > 0)
            //    {
            //        this.StatusCode = 0;
            //        this.StatusDescription = "";
            //    }
            //    else
            //    {
            //        objSqlTransaction.Rollback();
            //        this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;

            //    }

            //    if ( > 0)
            //    {

            //        this.StatusCode = 0;
            //        this.StatusDescription = "";
            //    }
            //    else
            //    {
            //        objSqlTransaction.Rollback();
            //        this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            //        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            //    }
            //    objSqlTransaction.Commit();
            //}
            //catch (Exception ex)
            //{
            //    objSqlTransaction.Rollback();
            //    if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
            //    {
            //        this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            //    }
            //    this.StatusCode = -1000;
            //    this.StatusDescription = "Internal server error";
            //}
            //finally
            //{
            //    MSSqlClient.SqlConnectionClose(objSqlConnection);
            //}
            #endregion
        }
        int insertUserOwnerLog(SqlConnection con, SqlTransaction transaction, string userId)
        {
            string query = @"INSERT INTO TBL_USER_OWNER_LOG(USER_OWNER_ID,COMPANY_ID,USER_ID,CHANGE_DATETIME)
                                VALUES(@USER_OWNER_ID,@COMPANY_ID,@USER_ID,@CHANGE_DATETIME);";


            SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@USER_OWNER_ID", this.SubAdminId);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@USER_ID", userId);
            objSqlCommand.Parameters.AddWithValue("@CHANGE_DATETIME", DateTime.UtcNow.Ticks);
            return objSqlCommand.ExecuteNonQuery();
        }
        int updateUserSubAdmin(SqlConnection con, SqlTransaction transaction,
             string userId)
        {
            string strQuery = @"UPDATE TBL_USER_DETAIL SET SUBADMIN_ID = @SUBADMIN_ID WHERE USER_ID = @USER_ID";
            SqlCommand objSqlCommand = new SqlCommand(strQuery, con, transaction);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand = new SqlCommand(strQuery, con, transaction);

            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", SubAdminId);
            objSqlCommand.Parameters.AddWithValue("@USER_ID", userId);
            return objSqlCommand.ExecuteNonQuery();
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string UserId
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public List<MFEMobileUser> UserDetails
        {
            private set;
            get;
        }
    }
}
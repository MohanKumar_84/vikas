﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientCP
{
    [DataContract]
    public class MpluginTestConnResp
    {
        public MpluginTestConnResp() { }
        [DataMember]
        public Reps resp { get; set; }
    }

    [DataContract]
    public class Reps
    {
        [DataMember]
        public string rid { get; set; }
        [DataMember]
        public string eid { get; set; }
        [DataMember]
        public MpluginTestConnData data { get; set; }
    }

    [DataContract]
    public class MpluginTestConnData
    {
        [DataMember]
        public string error { get; set; }
        [DataMember]
        public string errormsg { get; set; }
        [DataMember]
        public string inerror { get; set; }
        [DataMember]
        public string inerrormsg { get; set; }
        [DataMember]
        public string result { get; set; }
    }
}
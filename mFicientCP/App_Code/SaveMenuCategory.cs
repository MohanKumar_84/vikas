﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
namespace mFicientCP
{
    public class SaveMenuCategory
    {
        public SaveMenuCategory(string companyId, string menuCategoryName, int categoryDisplayIndex,  bool isForAllGroups, string menuCategoryId, string subAdminId, string _CategoryIcon, string oldcategoryname)
        {
            this.CompanyId = companyId;
            this.MenuCategoryName = menuCategoryName;
            this.CategoryDisplayIndex = categoryDisplayIndex;
            this.IsForAllGroups = isForAllGroups;
            this.MenuCategoryId = menuCategoryId;
            this.SubAdminId = subAdminId;
            this.CategoryIcon = _CategoryIcon;
            this.OldCategory = oldcategoryname;
        }

        public void Process()
        {
            StatusCode = -1000;
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);
                //List<mFicientCommonProcess.Activity> objActivity = new List<mFicientCommonProcess.Activity>();
                mFicientCommonProcess.ACTIVITYENUM objType;
                using (con)
                {

                    using (transaction = con.BeginTransaction())
                    {
                        if (String.IsNullOrEmpty(MenuCategoryId))
                        {
                            insertMenuCategoryDetails(transaction, con);
                            this.OldCategory = "";
                            objType = mFicientCommonProcess.ACTIVITYENUM.CATEGORY_ADDED;
                        }
                        else
                        {
                            updateMenuCategoryDetails(transaction, con);
                            objType = mFicientCommonProcess.ACTIVITYENUM.CATEGORY_RENAMED;
                        }
                        transaction.Commit();

                        Utilities.saveActivityLog(con, objType, this.CompanyId, this.SubAdminId, this.MenuCategoryId, this.MenuCategoryName, this.OldCategory, "", "",  "", "", "");
                    }
                    AddIntoUpdationRequiredForMenu obj_up = new AddIntoUpdationRequiredForMenu();
                    obj_up.CategoryUpdationForAllGroup(CompanyId);
                    StatusCode = 0;
                    StatusDescription = "";
                }
            }
            catch
            {
                if (transaction != null) transaction.Rollback();
                StatusCode = -1000;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }
        /// <summary>
        /// Delete selected group
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="connection"></param>
//        void deleteSelectedGroups(SqlTransaction transaction, SqlConnection connection)
//        {
//            string strQuery = @"DELETE FROM TBL_MENU_AND_GROUP_LINK
//                                WHERE MENU_CATEGORY_ID = @MenuCategoryId AND COMPANY_ID=@CompanyId;";
//            SqlCommand cmd = new SqlCommand(strQuery, connection, transaction);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@MenuCategoryId", MenuCategoryId);
//            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
//            cmd.ExecuteNonQuery();
//        }

        void insertMenuCategoryDetails(SqlTransaction transaction, SqlConnection connection)
        {
            this.MenuCategoryId = Utilities.GetMd5Hash(this.CompanyId + this.MenuCategoryName + this.SubAdminId + DateTime.UtcNow.Ticks.ToString());
            string strQuery = @"INSERT INTO TBL_MENU_CATEGORY
                                VALUES(@MenuCategoryId,@Category,@DisplayIndex,@SubAdminId,@CompanyId,@AllGroups,@UpdatedOn,@CATEGORY_ICON);";
            SqlCommand cmd = new SqlCommand(strQuery, connection, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@MenuCategoryId", MenuCategoryId);
            cmd.Parameters.AddWithValue("@Category", this.MenuCategoryName);
            cmd.Parameters.AddWithValue("@DisplayIndex", this.CategoryDisplayIndex);
            cmd.Parameters.AddWithValue("@SubAdminId", this.SubAdminId);
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            cmd.Parameters.AddWithValue("@AllGroups", IsForAllGroups ? 1 : 0);
            cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@CATEGORY_ICON", this.CategoryIcon);

            cmd.ExecuteNonQuery();
        }

        void updateMenuCategoryDetails(SqlTransaction transaction, SqlConnection connection)
        {
            string strQuery = @"UPDATE [TBL_MENU_CATEGORY]
                                SET[CATEGORY] = @Category
                                    ,[DISPLAY_INDEX] = @DisplayIndex
                                    ,[SUBADMIN_ID] = @SubAdminId
                                    ,[COMPANY_ID] = @CompanyId
                                    ,[ALL_GROUPS] = @AllGroups
                                    ,[UPDATED_ON] = @UpdatedOn
                                    ,[CATEGORY_ICON] = @CATEGORY_ICON
                                WHERE MENU_CATEGORY_ID =@MenuCategoryId";
            SqlCommand cmd = new SqlCommand(strQuery, connection, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@MenuCategoryId", MenuCategoryId);
            cmd.Parameters.AddWithValue("@Category", this.MenuCategoryName);
            cmd.Parameters.AddWithValue("@DisplayIndex", this.CategoryDisplayIndex);
            cmd.Parameters.AddWithValue("@SubAdminId", this.SubAdminId);
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            cmd.Parameters.AddWithValue("@AllGroups", IsForAllGroups ? 1 : 0);
            cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@CATEGORY_ICON", this.CategoryIcon);
            cmd.ExecuteNonQuery();
        }

//        void insertGroupsSelected(SqlTransaction transaction, SqlConnection connection)
//        {
//            //if it is new then MenuCategoryId is set in the insert function
//            //other wise if it is edit then MenuCategoryId is set in the constructor
//            string[] groupIds = Groups.Split(',');
//            for (int i = 0; i < groupIds.Length; i++)
//            {
//                string strQuery = @"INSERT INTO TBL_MENU_AND_GROUP_LINK
//                                    VALUES(@COMPANY_ID,@MenuCategoryId,@GroupId);";
//                SqlCommand cmd = new SqlCommand(strQuery, connection, transaction);
//                cmd.CommandType = CommandType.Text;
//                cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
//                cmd.Parameters.AddWithValue("@MenuCategoryId", MenuCategoryId);
//                cmd.Parameters.AddWithValue("@GroupId", groupIds[i]);
//                cmd.ExecuteNonQuery();

//            }
//        }

        public string CompanyId
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string MenuCategoryName
        {
            set;
            get;
        }

        public int CategoryDisplayIndex
        {
            set;
            get;
        }

        public bool IsForAllGroups
        {
            set;
            get;
        }

        public string MenuCategoryId
        {
            set;
            get;
        }

        public string StatusDescription
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }

        public string OldCategory
        {
            get;
            set;
        }


        public string CategoryIcon { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetReportPageDetails
    {
        public GetReportPageDetails()
        {

        }

        public GetReportPageDetails(Boolean _IsSelectAll, string _ReportId, string _SubAdminId, string _ReportName)
        {
            this.ReportId = _ReportId;
            this.ReportName = _ReportName;
            this.SubAdminId = _SubAdminId;
            this.IsSelectAll = _IsSelectAll;
        }
        public void Process()
        {  
            try
            {
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                if (this.IsSelectAll == true)
                {
                    query = @"SELECT  * FROM  TBL_REPORT_DETAILS rd INNER JOIN TBL_REPORT_OWNER ro ON ro.REPORT_ID=rd.REPORT_ID 
                                    WHERE ro.SUBADMIN_ID=@SUBADMIN_ID ORDER BY rd.REPORT_NAME;";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                }
                else if (this.ReportName.Length == 0)
                {
                    query = @"SELECT  * FROM  TBL_REPORT_DETAILS WHERE REPORT_ID =@REPORT_ID  ORDER BY REPORT_NAME;";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@REPORT_ID", this.ReportId);
                }
                else
                {
                    if (this.ReportId.Length == 0)
                    {
                        query = @"SELECT  * FROM  TBL_REPORT_DETAILS WHERE REPORT_NAME =@REPORT_NAME;";
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@REPORT_NAME", this.ReportName);
                    }
                    else
                    {
                        query = @"SELECT  * FROM  TBL_REPORT_DETAILS rd INNER JOIN TBL_REPORT_OWNER ro ON ro.REPORT_ID=rd.REPORT_ID  WHERE rd.REPORT_ID !=@REPORT_ID AND rd.REPORT_NAME =@REPORT_NAME AND ro.SUBADMIN_ID=@SUBADMIN_ID;";
                        
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@REPORT_ID", this.ReportId);
                        objSqlCommand.Parameters.AddWithValue("@REPORT_NAME", this.ReportName);
                        objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                    }

                }
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }

        public string ReportId
        {
            set;
            get;
        }
        public string ReportName
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }

        public Boolean IsSelectAll
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

    }



    //public class EntityCollection : IEnumerable<Entity>
    //{ 

    //}
}
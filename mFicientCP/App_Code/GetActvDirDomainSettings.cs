﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetActvDirDomainSettings
    {
        #region Private members
        string _companyId, _domainName, _mPluginId;
        DataTable _ActDirDomainSettings;
        GET_RESULTS_BY _getResultsBy;


        enum GET_RESULTS_BY
        {
            CompanyId,
            MplugInAndDomainName
        }
        #endregion


        #region Public Properties

        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }

        public DataTable ActDirDomainSettings
        {
            get { return _ActDirDomainSettings; }
            private set { _ActDirDomainSettings = value; }
        }
        public string StatusDescription
        {
            get;
            private set;
        }
        public int StatusCode
        {
            get;
            private set;
        }
        private GET_RESULTS_BY GetResultsBy
        {
            get { return _getResultsBy; }
             set { _getResultsBy = value; }
        }
        public string MPluginId
        {
            get { return _mPluginId; }
            private set { _mPluginId = value; }
        }

        public string DomainName
        {
            get { return _domainName; }
            private set { _domainName = value; }
        }
        #endregion


        #region Constructor
        public GetActvDirDomainSettings(string companyId)
        {
            _companyId = companyId;
            this.GetResultsBy = GET_RESULTS_BY.CompanyId;
        }
        public GetActvDirDomainSettings(string domainName,
            string mpluginId,
            string companyId)
        {
            _companyId = companyId;
            _mPluginId = mpluginId;
            _companyId = companyId;
            _domainName = domainName;
            this.GetResultsBy = GET_RESULTS_BY.MplugInAndDomainName;
        }
        #endregion


        #region Public Methods
        public void Process()
        {
            try
            {
                DataSet dsActDirDomainSettings = new DataSet();

                switch (this.GetResultsBy)
                {
                    case GET_RESULTS_BY.CompanyId:
                        dsActDirDomainSettings =getSettingByCompanyId();
                        break;
                    case GET_RESULTS_BY.MplugInAndDomainName:
                        dsActDirDomainSettings = getSettingByMpluginIdAndDomainName();
                        break;
                }
                if (dsActDirDomainSettings == null 
                    || dsActDirDomainSettings.Tables.Count == 0)
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());

                this.ActDirDomainSettings = dsActDirDomainSettings.Tables[0];
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;

            }
            catch
            {
                this.StatusDescription = "Internal server error";
                this.StatusCode = -1000;
            }
        }
        #endregion


        #region Private Methods

        string getQuery(GET_RESULTS_BY eGetResultsBy)
        {
            string strQuery = String.Empty;
            switch (eGetResultsBy)
            {
                case GET_RESULTS_BY.CompanyId:
                    strQuery = @"SELECT *,
                                (Domain.DOMAIN_NAME+' ( '+Agent.MP_AGENT_NAME+' )') AS DOM_MPLUGIN_NAME,
                                (Domain.DOMAIN_ID+'_'+Domain.DOMAIN_NAME+'_'+Agent.MP_AGENT_NAME) AS DomID_DomNm_MPluginNm
                                FROM TBL_ENTERPRISE_DOMAINS AS Domain
                                INNER JOIN TBL_MPLUGIN_AGENT_DETAIL AS Agent
                                ON Domain.MPLUGIN_ID = Agent.MP_AGENT_ID
                                WHERE Domain.COMPANY_ID = @COMPANY_ID";
                    break;
                case GET_RESULTS_BY.MplugInAndDomainName:
                    strQuery = @"SELECT *,
                                (Domain.DOMAIN_NAME+' ( '+Agent.MP_AGENT_NAME+' )') AS DOM_MPLUGIN_NAME,
                                (Domain.DOMAIN_ID+'_'+Domain.DOMAIN_NAME+'_'+Agent.MP_AGENT_NAME) AS DomID_DomNm_MPluginNm 
                                FROM TBL_ENTERPRISE_DOMAINS AS Domain
                                INNER JOIN TBL_MPLUGIN_AGENT_DETAIL AS Agent
                                ON Domain.MPLUGIN_ID = Agent.MP_AGENT_ID
                                 WHERE Domain.COMPANY_ID = @COMPANY_ID
                                 AND Domain.DOMAIN_NAME =@DOMAIN_NAME
                                 AND Domain.MPLUGIN_ID = @MPLUGIN_ID";
                    break;

            }

            return strQuery;
        }

        DataSet getSettingByCompanyId()
        {
            //set at constructor
            string strQuery = getQuery(GET_RESULTS_BY.CompanyId);
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            return MSSqlClient.SelectDataFromSQlCommand(cmd);
        }
        DataSet getSettingByMpluginIdAndDomainName()
        {
            //set at constructor
            string strQuery = getQuery(GET_RESULTS_BY.MplugInAndDomainName);
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            cmd.Parameters.AddWithValue("@DOMAIN_NAME", this.DomainName);
            cmd.Parameters.AddWithValue("@MPLUGIN_ID", this.MPluginId);
            return MSSqlClient.SelectDataFromSQlCommand(cmd);
        }
        #endregion



    }
}
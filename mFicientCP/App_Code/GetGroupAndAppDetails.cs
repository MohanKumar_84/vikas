﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCP
{
    public class GetGroupAndAppDetails
    {
        public  GetGroupAndAppDetails(string _GroupId, string _CompanyId, string _SubadminId)
        {
            this.CompanyId = _CompanyId;
            this.SubdminId = _SubadminId;
            this.GroupId = _GroupId;
        }

        public void Process()
        {
            try
            {
                string strQuery = @"SELECT t.COMPANY_ID,t.Group_ID,t.Group_Name,t2.WF_ID,t2.WF_NAME
                from dbo.TBL_USER_GROUP as t  
                inner  join 
                dbo.TBL_WORKFLOW_AND_GROUP_LINK as w 
                on  w.GROUP_ID=t.GROUP_ID   
                inner  join  
                dbo.TBL_WORKFLOW_DETAIL as t2  on   t2.WF_ID=w.WORKFLOW_ID 
                where t.Company_Id=@CompanyId and t.GROUP_ID=@Group_ID  
                Group by t.COMPANY_ID,t.Group_ID,t.Group_Name,t2.WF_ID,t2.WF_NAME  ORDER BY  t2.WF_NAME asc;

               select distinct b.wf_name,'' Group_id,b.WF_ID,'' GROUP_NAME  from dbo.TBL_WORKFLOW_DETAIL as b
           where WF_ID not in(select a.WORKFLOW_ID from TBL_WORKFLOW_AND_GROUP_LINK as a where a.GROUP_ID=@Group_ID and a.COMPANY_ID=@CompanyId)
              and b.COMPANY_ID=@CompanyId ;";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                
                objSqlCommand.Parameters.AddWithValue("@CompanyId",this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@Group_ID", this.GroupId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    ResultTables = objDataSet;
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }   
            }

            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";

            }
        }
        public DataSet ResultTables
        {
            set;
            get;
        }
         public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string SubdminId
        {
            get;
            set;
        }

        public string CompanyId
        {
            get;
            set;
        }
        public string GroupId
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public string StatusDiscription
        {
            get;
            set;
        }

            
           
    }
  
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetReportDatabindDetails
    {
        public GetReportDatabindDetails()
        {

        }
        public GetReportDatabindDetails(string _DbbindId, string _ReportId, string _CompanyId,Boolean _IsDatabaseBinding)
        {
            this.DbbindId = _DbbindId;
            this.ReportId = _ReportId;
            this.CompanyId = _CompanyId;
            this.IsDatabaseBinding = _IsDatabaseBinding;
        }
        public void Process()
        {
            try
            {
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                if (IsDatabaseBinding)
                {
                    query = @"SELECT  * FROM  TBL_REPORT_DB_DATABINDING WHERE REPORT_ID=@REPORT_ID AND COMPANY_ID=@COMPANY_ID AND REPORT_DB_BINDING_ID=@REPORT_DB_BINDING_ID;";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@REPORT_ID", this.ReportId);
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    objSqlCommand.Parameters.AddWithValue("@REPORT_DB_BINDING_ID", this.DbbindId);
                }
                else
                {
                    query = @"SELECT  * FROM TBL_REPORT_WS_DATABINDING WHERE REPORT_ID=@REPORT_ID AND COMPANY_ID=@COMPANY_ID AND REPORT_WS_DATABINDING_ID=@REPORT_WS_DATABINDING_ID;";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@REPORT_ID", this.ReportId);
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    objSqlCommand.Parameters.AddWithValue("@REPORT_WS_DATABINDING_ID", this.DbbindId);
                }
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }
        public string DbbindId
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string ReportId
        {
            set;
            get;
        }
        public Boolean IsDatabaseBinding
        {
            set;
            get;
        }
        
        public DataTable ResultTable
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }

}
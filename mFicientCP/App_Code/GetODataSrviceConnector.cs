﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetODataSrviceConnector
    {
        public GetODataSrviceConnector()
        {

        }

        public GetODataSrviceConnector(Boolean _IsSelectAll, string _SubAdminId, string _ConnectionId, string _ConnectionName, string _CompanyID)
        {
            this.IsSelectAll = _IsSelectAll;
            this.SubAdminId = _SubAdminId;
            this.ConnectionId = _ConnectionId;
            this.ConnectionName = _ConnectionName;
            this.CompanyId = _CompanyID;
            Process();
        }
        public GetODataSrviceConnector(Boolean _IsSelectAll, string _SubAdminId, string _ConnectionId, string _ConnectionName, string _CompanyID,int newide)
        {
            this.IsSelectAll = _IsSelectAll;
            this.SubAdminId = _SubAdminId;
            this.ConnectionId = _ConnectionId;
            this.ConnectionName = _ConnectionName;
            this.CompanyId = _CompanyID;
            Process1();
        }
        private void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                if (this.IsSelectAll == true)
                {
               query = @"Select isnull(ODATA_OBJ,'') as ODATA_OBJ,SS.Company_ID,SS.ODATA_CONNECTOR_ID,SS.ODATA_ENDPOINT,SS.AUTHENTICATION_TYPE
                            ,SS.SUBADMIN_ID,SS.CONNECTION_NAME,SS.USER_NAME,PASSWORD,SS.ODATA_CONTENT,SS.CREDENTIAL_PROPERTY,
                             isnull(SS.CREATED_BY,'') as CREATED_BY,isnull(SS.CREATED_ON,0) as CREATED_ON,isnull(SS.UPDATED_ON,0) as UPDATED_ON,SC.FULL_NAME,isnull(st.FULL_NAME,'') as CreatedBY,ODATA_CONTENT, 
                           CASE MPLUGIN_AGENT  WHEN '' THEN 'None' ELSE MPLUGIN_AGENT  END  AS  MPLUGIN_AGENT ,VERSION FROM 
                          TBL_ODATA_CONNECTION AS SS LEFT OUTER JOIN 
                           (select ss.ODATA_CONNECTOR_ID,STUFF((select ',' + T.ODATA_COMMAND_NAME  from dbo.TBL_ODATA_COMMAND as T where ss.ODATA_CONNECTOR_ID=T.ODATA_CONNECTOR_ID for xml path('')),1,1,'') as ODATA_OBJ
                             from dbo.TBL_ODATA_COMMAND as ss WHERE Company_ID=@Company_ID group by 
                              ss.ODATA_CONNECTOR_ID )T ON T.ODATA_CONNECTOR_ID=SS.ODATA_CONNECTOR_ID 
                           Left outer  join dbo.TBL_SUB_ADMIN as SC on SC.SUBADMIN_ID=SS.SUBADMIN_ID 
                         Left outer  join dbo.TBL_SUB_ADMIN as st on SS.CREATED_BY = st.SUBADMIN_ID 
                          WHERE  SS.COMPANY_ID=@Company_ID ORDER BY CONNECTION_NAME;";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                }
                else
                {
                    if (this.ConnectionName.Length == 0)
                    {
                        query = @"SELECT  * FROM  TBL_ODATA_CONNECTION WHERE ODATA_CONNECTOR_ID=@ODATA_CONNECTOR_ID AND COMPANY_ID=@COMPANY_ID;";
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                        objSqlCommand.Parameters.AddWithValue("@ODATA_CONNECTOR_ID", this.ConnectionId);
                    }
                    else
                    {
                        query = @"SELECT  * FROM  TBL_ODATA_CONNECTION WHERE  CONNECTION_NAME=@CONNECTION_NAME AND COMPANY_ID=@COMPANY_ID";
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    }
                }
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
                if (this.ResultTable.Rows.Count > 0)
                    this.StatusCode = 0;
            }
            catch 
            {
                this.StatusCode = -1000;
            }
        }
        private void Process1()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                if (this.IsSelectAll == true)
                {
                    query = @"Select isnull(ODATA_OBJ,'') as ODATA_OBJ,SS.Company_ID,SS.ODATA_CONNECTOR_ID,SS.ODATA_ENDPOINT,SS.AUTHENTICATION_TYPE
                            ,SS.SUBADMIN_ID,SS.CONNECTION_NAME,SS.ODATA_CONTENT,SS.CREDENTIAL_PROPERTY,
                             isnull(SS.CREATED_BY,'') as CREATED_BY,isnull(SS.CREATED_ON,0) as CREATED_ON,isnull(SS.UPDATED_ON,0) as UPDATED_ON,SC.FULL_NAME,isnull(st.FULL_NAME,'') as CreatedBY,ODATA_CONTENT, 
                           CASE MPLUGIN_AGENT  WHEN '' THEN 'None' ELSE MPLUGIN_AGENT  END  AS  MPLUGIN_AGENT ,VERSION FROM 
                          TBL_ODATA_CONNECTION AS SS LEFT OUTER JOIN 
                           (select ss.ODATA_CONNECTOR_ID,STUFF((select ',' + T.ODATA_COMMAND_NAME  from dbo.TBL_ODATA_COMMAND as T where ss.ODATA_CONNECTOR_ID=T.ODATA_CONNECTOR_ID for xml path('')),1,1,'') as ODATA_OBJ
                             from dbo.TBL_ODATA_COMMAND as ss WHERE Company_ID=@COMPANY_ID group by 
                              ss.ODATA_CONNECTOR_ID )T ON T.ODATA_CONNECTOR_ID=SS.ODATA_CONNECTOR_ID 
                           Left outer  join dbo.TBL_SUB_ADMIN as SC on SC.SUBADMIN_ID=SS.SUBADMIN_ID 
                         Left outer  join dbo.TBL_SUB_ADMIN as st on SS.CREATED_BY = st.SUBADMIN_ID 
                          WHERE  SS.COMPANY_ID=@COMPANY_ID ORDER BY CONNECTION_NAME";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                }
                else
                {
                    if (this.ConnectionName.Length == 0)
                    {
                        query = @"SELECT  * FROM  TBL_ODATA_CONNECTION WHERE ODATA_CONNECTOR_ID=@ODATA_CONNECTOR_ID AND COMPANY_ID=@COMPANY_ID;";
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

                        objSqlCommand.Parameters.AddWithValue("@ODATA_CONNECTOR_ID", this.ConnectionId);
                    }
                    else
                    {
                        query = @"SELECT  * FROM  TBL_ODATA_CONNECTION WHERE  CONNECTION_NAME=@CONNECTION_NAME AND COMPANY_ID=@COMPANY_ID";
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    }
                }
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }

        public string SubAdminId
        {
            set;
            get;
        }
        public string ConnectionId
        {
            set;
            get;
        }
        public Boolean IsSelectAll
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public string ConnectionName { get; set; }

        public string CompanyId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
namespace mFicientCP
{
    public class GetUsersWithGroupDtlsByCmpId
    {
        string _companyId;
        DataTable _resultSet;

        
        public GetUsersWithGroupDtlsByCmpId(string companyId)
        {
            this.CompanyId = companyId;
        }
        public void Process()
        {
            this.StatusCode = 0;
            this.StatusDescription = String.Empty;
            try
            {
                string strQuery = getSqlQuery();
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                DataSet dsUsers = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsUsers == null) throw new Exception();
                this.ResultSet = dsUsers.Tables[0];
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        public void Process(string groupId,out DataTable usersInGroup,out DataTable otherAvailableUsers)
        {
            this.StatusCode = 0;
            this.StatusDescription = String.Empty;
            usersInGroup = new DataTable();
            otherAvailableUsers = new DataTable();
            try
            {
                Process();
                if (this.StatusCode == 0)
                {
                    StringBuilder sbUserInGroup = new StringBuilder();
                    string filter = String.Format("GROUP_ID = '{0}'", groupId);
                    DataView dtView = new DataView(this.ResultSet,
                                        filter,
                                        "USER_ID",
                                        DataViewRowState.CurrentRows);

                    if (dtView != null)
                    {
                        usersInGroup = dtView.ToTable();
                        foreach (DataRow row in usersInGroup.Rows)
                        {
                            if (sbUserInGroup.Length == 0)
                                sbUserInGroup.Append('\'').Append(Convert.ToString(row["USER_ID"])).Append('\'');
                            else
                                sbUserInGroup.Append(",").Append('\'').Append(Convert.ToString(row["USER_ID"])).Append('\'');
                        }
                    }
                    if (sbUserInGroup.Length == 0)
                    {
                        filter = String.Format("(GROUP_ID <> '{0}' OR GROUP_ID IS NULL)", groupId, sbUserInGroup.ToString());
                    }
                    else
                    {
                        filter = String.Format("(GROUP_ID <> '{0}' OR GROUP_ID IS NULL)  AND USER_ID NOT IN ( {1} )", groupId, sbUserInGroup.ToString());
                    }
                     
                     dtView = new DataView(this.ResultSet,
                                        filter,
                                        "USER_ID",
                                        DataViewRowState.CurrentRows);

                    if (dtView != null)
                    {
                        otherAvailableUsers = dtView.ToTable(true,"USER_ID","FIRST_NAME"
                            ,"LAST_NAME","USER_NAME");
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        string getSqlQuery()
        {
            //get all the users in the company
            return @"SELECT usrDtl.*,usrInGroups.*
                    FROM TBL_USER_DETAIL AS usrDtl 
                    LEFT OUTER JOIN
                    (SELECT usrGrp.*,usrGrpLnk.USER_ID
                    FROM TBL_USER_GROUP_LINK AS usrGrpLnk
                    INNER JOIN TBL_USER_GROUP AS usrGrp
                    ON usrGrp.GROUP_ID = usrGrpLnk.GROUP_ID) usrInGroups
                    ON usrInGroups.USER_ID = usrDtl.USER_ID
                    AND usrInGroups.COMPANY_ID = usrDtl.COMPANY_ID
                    WHERE usrDtl.COMPANY_ID = @COMPANY_ID
                    ORDER BY usrDtl.FIRST_NAME";
        }
        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }
        public string StatusDescription
        {
            get;
            private set;
        }
        public int StatusCode
        {
            get;
            private set;
        }
        public DataTable ResultSet
        {
            get { return _resultSet; }
            private set { _resultSet = value; }
        }
    }
}
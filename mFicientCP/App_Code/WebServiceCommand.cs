﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Text;

namespace mFicientCP
{
    public class WebServiceCommand
    {
        #region Private Members

        private string commandId, connectorId, enterpriseId, tag, returnType, url, recordPath, datasetPath, commandName,
            service, method, soapRequest, httpRequest, operationAction, portType, mPluginAgent, mPluginAgentPwd ,_rpcObject, _inputXmlRpcParam, _outputXmlRpcParam, expirycondition, xreferences, description,parameter;

        private List<IdeWsParam> output;
        private HTTP_TYPE httpType;
        private WebServiceConnector wsConnector;
        WebserviceType _webServiceType;
        private int cache, expiryfrequency;

        #endregion

        #region Constructor

        public WebServiceCommand()
        { 
        }

        internal WebServiceCommand(string _commandId, string _enterpriseId)
        {
            commandId = _commandId;
            enterpriseId = _enterpriseId;
            output = new List<IdeWsParam>();
        }

        #endregion

        #region Public Properties
        public string CommandId
        {

            get
            {
                return commandId;
            }
            set
            {
                commandId = value;
            }
            
        }
        public string ConnectorId
        {
            
            get
            {
                return connectorId;
            }
            set
            {
                connectorId = value;
            }

         
        }
        public string Description
        {
           
             get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }
        public string Parameter
        {
            get
            {
                return parameter;
            }
            set
            {
                parameter = value;
            }
        }

        public WebServiceConnector WSConnector
        {
            get
            {
                try
                {
                    wsConnector = new WebServiceConnector(connectorId, enterpriseId);
                    wsConnector.GetConnector();
                }
                catch
                {
                }
                return wsConnector;
            }
        }

        public HTTP_TYPE HTTPType
        {
            get
            {
                return httpType;
            }
            set
            {
                httpType = value;
            }

        }

        public string Tag
        {
            get
            {
                return tag;
            }
            set
            {
                tag = value;
            }
        }

        public string ReturnType
        {

           get
            {
                return returnType;
            }
            set
            {
                returnType = value;
            }
        }

        public string EnterpriseId
        {
            get
            {
                return enterpriseId;
            }
            set
            {
                enterpriseId = value;
            }

        }

        public string Url
        {
            get
            {
                return url;
            }
            set
            {
                url = value;
            }
        }

        public string RecordPath
        {
            get
            {
                return recordPath;
            }
            set
            {
                recordPath = value;
            }
        }

        public string DataSetPath
        {
           
             get
            {
                return datasetPath;
            }
            set
            {
                datasetPath = value;
            }



        }

        public string CommandName
        {
            get
            {
                return commandName;
            }
            set
            {
                commandName = value;
            }
        }

        public string Method
        {
           
            get
            {
                return method;
            }
            set
            {
                method = value;
            }
        }

        public string Service
        {
             get
            {
                return service;
            }
            set
            {
                service = value;
            }
        }

        public string PortType
        {
             get
            {
                return portType;
            }
            set
            {
                portType = value;
            }

        }

        public string SoapRequest
        {
            get
            {
                return soapRequest;
            }
            set
            {
                soapRequest = value;
            }
        }

        public string HttpRequest
        {
            get
            {
                return httpRequest;
            }
            set
            {
                httpRequest = value;
            }
            
        }

        public string OperationAction
        {
            get
            {
                return operationAction;
            }
            set
            {
                operationAction = value;
            }
        }

        
        public string MPluginAgent
        {
            get
            {
                return mPluginAgent;
            }
            set
            {
                mPluginAgent = value;
            }
        }
        public string MPluginAgentPwd
        {
            get
            {
                return mPluginAgentPwd;
            }
            set
            {
                mPluginAgentPwd = value;
            }
        }

        public string XMLRPCObject
        {
             get
            {
                return _rpcObject;
            }
            set
            {
                _rpcObject = value;
            }
        }

        public WebserviceType WSType
        {
             get
            {
                return _webServiceType;
            }
            set
            {
                _webServiceType = value;
            }
        }

        public string OutputXmlRpcParam
        {
            get
            {
                return _outputXmlRpcParam;
            }
            set
            {
                _outputXmlRpcParam = value;
            }
        }

        public string InputXmlRpcParam
        {
             get
            {
                return _inputXmlRpcParam;
            }
            set
            {
                _inputXmlRpcParam = value;
            }
        }
        public List<IdeWsParam> OutPutTagPaths
        {
        
            get
            {
                return output;
            }
            set
            {
                output = value;
            }
        }
        public int Cache
        {
            get
            {
                return cache;
            }
            set
            {
                cache = value;
            }

        }
        public int Expiryfrequency
        {
             get
            {
                return expiryfrequency;
            }
            set
            {
                expiryfrequency = value;
            }
        }

        public string ExpiryCondtion
        {
            get
            {
                return expirycondition;
            }
            set
            {
                expirycondition = value;
            }

        }

        public string Xreference
        {
            get
            {
                return xreferences;
            }
            set
            {
                xreferences = value;
            }
        }

        #endregion

        #region Public Method

        public void GetCommandDetails()
        {
            string strQuery = @"SELECT cmd.*,isnull(ag.MP_AGENT_PASSWORD, '') as MP_AGENT_PASSWORD,isnull(con.MPLUGIN_AGENT,'') as MPLUGIN_AGENT,con.* FROM TBL_WS_COMMAND as cmd inner join TBL_WEBSERVICE_CONNECTION as con on cmd.WS_CONNECTOR_ID=con.WS_CONNECTOR_ID and cmd.COMPANY_ID=con.COMPANY_ID 
            left outer join TBL_MPLUGIN_AGENT_DETAIL as ag on ag.MP_AGENT_NAME=con.MPLUGIN_AGENT and ag.COMPANY_ID=con.COMPANY_ID 
            WHERE WS_COMMAND_ID = @WS_COMMAND_ID AND con.COMPANY_ID = @COMPANY_ID;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@WS_COMMAND_ID", commandId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);
            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                connectorId = ds.Tables[0].Rows[0]["WS_CONNECTOR_ID"].ToString();
                mPluginAgentPwd = ds.Tables[0].Rows[0]["MP_AGENT_PASSWORD"].ToString();
                mPluginAgent = ds.Tables[0].Rows[0]["MPLUGIN_AGENT"].ToString();
                url = ds.Tables[0].Rows[0]["URL"].ToString();
                returnType = ds.Tables[0].Rows[0]["RETURN_TYPE"].ToString();
                httpType = (HTTP_TYPE)Convert.ToInt32(ds.Tables[0].Rows[0]["HTTP_TYPE"].ToString());
                commandName = ds.Tables[0].Rows[0]["WS_COMMAND_NAME"].ToString();
                recordPath = ds.Tables[0].Rows[0]["RECORD_PATH"].ToString();
                datasetPath = ds.Tables[0].Rows[0]["DATASET_PATH"].ToString();
                tag = ds.Tables[0].Rows[0]["TAG"].ToString();
                xreferences = ds.Tables[0].Rows[0]["X_REFERENCES"].ToString();
                description = ds.Tables[0].Rows[0]["DESCRIPTION"].ToString();
                cache = Convert.ToInt32(ds.Tables[0].Rows[0]["CACHE"].ToString());
                expiryfrequency = Convert.ToInt32(ds.Tables[0].Rows[0]["EXPIRY_FREQUENCY"].ToString());
                expirycondition = ds.Tables[0].Rows[0]["EXPIRY_CONDITION"].ToString();
                parameter = ds.Tables[0].Rows[0]["PARAMETER"].ToString();

                service = ds.Tables[0].Rows[0]["SERVICE"].ToString();
                method = ds.Tables[0].Rows[0]["METHOD"].ToString();
                httpRequest = ds.Tables[0].Rows[0]["HTTP_REQUEST"].ToString();
                soapRequest = HttpContext.Current.Server.HtmlEncode(ds.Tables[0].Rows[0]["SOAP_REQUEST"].ToString());
                operationAction = ds.Tables[0].Rows[0]["OPERATION_ACTION"].ToString();
                portType = ds.Tables[0].Rows[0]["PORT_TYPE"].ToString();
                commandId = ds.Tables[0].Rows[0]["WS_COMMAND_ID"].ToString();
                string strWebServiceType = ds.Tables[0].Rows[0]["WEBSERVICE_TYPE"].ToString();
                switch (ds.Tables[0].Rows[0]["WEBSERVICE_TYPE"].ToString().Trim().ToLower())
                {
                    case "wsdl":
                        _webServiceType = WebserviceType.WSDL_SOAP;
                        break;
                    case "http":
                        _webServiceType = WebserviceType.HTTP;
                        break;
                    case "rpc":
                        _webServiceType = WebserviceType.RPC_XML;
                        break;
                    default:
                        _webServiceType = WebserviceType.HTTP;
                        break;

                }
                if (_webServiceType == WebserviceType.RPC_XML)
                {
                    //_rpcObject = getXMLRPCSerialisedObject();
                    _inputXmlRpcParam = ds.Tables[0].Rows[0]["PARAMETER"].ToString();
                    _outputXmlRpcParam = ds.Tables[0].Rows[0]["TAG"].ToString();
                }
                this.wsConnector = new WebServiceConnector(connectorId, enterpriseId);

                wsConnector.ConnectionName = ds.Tables[0].Rows[0]["CONNECTION_NAME"].ToString();
                wsConnector.WebserviceUrl = ds.Tables[0].Rows[0]["WEBSERVICE_URL"].ToString();
            }
        }

        string getXMLRPCSerialisedObject()
        {
            string strXMLSerialisedObject = String.Empty;
            string strQuery = @"SELECT * FROM TBL_SERIALIZE_OBJECT
                                  WHERE WS_COMMAND_ID=@WS_COMMAND_ID
                                  AND COMPANY_ID=@COMPANY_ID";

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@WS_COMMAND_ID", this.commandId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.enterpriseId);
            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                strXMLSerialisedObject = Convert.ToString(ds.Tables[0].Rows[0]["SERIALIZE_OBJECT"]);
            }
            return strXMLSerialisedObject;
        }
        #endregion
    }
}
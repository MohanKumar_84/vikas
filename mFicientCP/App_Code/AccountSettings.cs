﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class AccountSettings
    {
        public enum RECORD_TO_CHANGE
        {
            MaxDevicePerUser,
            AllowDeleteCmd,
            AllowActvDirUser,
            AutoLogin
        }
        RECORD_TO_CHANGE _recordToChange;


        //public AccountSettings(string companyId, string maxDevicePerUser, bool allowDeleteCommand, bool allowActiveDirectoryUser)
        //{
        //    this.CompanyId = companyId;
        //    this.MaxDevicePerUser = maxDevicePerUser;
        //    this.AllowDeleteCommand = allowDeleteCommand;
        //    this.AllowActiveDirectoryUser = allowActiveDirectoryUser;
        //}
        public AccountSettings(string companyId, int maxDevicePerUser)
        {
            this.CompanyId = companyId;
            this.MaxDevicePerUser = maxDevicePerUser;
            this._recordToChange = RECORD_TO_CHANGE.MaxDevicePerUser;
        }
        public AccountSettings(string companyId, RECORD_TO_CHANGE recordToChange, bool value)
        {
            if (recordToChange == RECORD_TO_CHANGE.MaxDevicePerUser) throw new ArgumentException();
            this.CompanyId = companyId;
            switch (recordToChange)
            {
                case RECORD_TO_CHANGE.AllowDeleteCmd:
                    this.AllowDeleteCommand = value;
                    break;
                case RECORD_TO_CHANGE.AllowActvDirUser:
                    this.AllowActiveDirectoryUser = value;
                    break;
                case RECORD_TO_CHANGE.AutoLogin:
                    this.AutoLogin = value;
                    break;
            }
            this._recordToChange = recordToChange;

        }
        public void Process()
        {
            try
            {
                switch (this.RecordToChange)
                {
                    case RECORD_TO_CHANGE.MaxDevicePerUser:
                        updateOrInsertMaxDevPerUser();
                        break;
                    case RECORD_TO_CHANGE.AllowDeleteCmd:
                        updateOrInsertAllowDeleteCmd();
                        break;
                    case RECORD_TO_CHANGE.AllowActvDirUser:
                        updateOrInsertAllowActiveDirUser();
                        break;
                    case RECORD_TO_CHANGE.AutoLogin:
                        updateOrInsertAutoLogin();
                        break;
                }

            }
            catch (Exception ex)
            {
                StatusCode = -1000;
                StatusDescription = ex.Message;
            }
        }
        void updateOrInsertMaxDevPerUser()
        {
            if (!anyNonExceptionUserAlreadyHasMoreDevicesRegistered())
            {
                string query = @"IF EXISTS(SELECT COMPANY_ID FROM TBL_ACCOUNT_SETTINGS WHERE COMPANY_ID = @CompanyId)
                                UPDATE TBL_ACCOUNT_SETTINGS
                                SET MAX_DEVICE_PER_USER = @MaxDevice
                                WHERE COMPANY_ID = @CompanyId
                                ELSE
                                INSERT INTO TBL_ACCOUNT_SETTINGS (COMPANY_ID,MAX_DEVICE_PER_USER, MAX_MPLUGIN_AGENTS, ALLOW_DELETE_COMMAND, ALLOW_ACTIVE_DIRECTORY_USER, REMOVE_OLDEST_DEVICE_ON_AUTOREGISTRATION)
                                VALUES(@CompanyId,@MaxDevice,1,1,0,1)";

                SqlCommand cmd = new SqlCommand(query);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.Parameters.AddWithValue("@MaxDevice", this.MaxDevicePerUser);
                if (MSSqlClient.ExecuteNonQueryRecord(cmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception("Internal server error");
                }

            }
            else
            {
                throw new Exception("There are already users who have more devices registered than the max device selected.");
            }
        }
        void updateOrInsertAllowDeleteCmd()
        {
            string query = @"IF EXISTS(SELECT COMPANY_ID FROM TBL_ACCOUNT_SETTINGS WHERE COMPANY_ID = @CompanyId)
                                UPDATE TBL_ACCOUNT_SETTINGS
                                SET ALLOW_DELETE_COMMAND = @AllowDeleteCommand
                                WHERE COMPANY_ID = @CompanyId
                                ELSE
                                INSERT INTO TBL_ACCOUNT_SETTINGS (COMPANY_ID,MAX_DEVICE_PER_USER,MAX_MPLUGIN_AGENTS,ALLOW_DELETE_COMMAND,ALLOW_ACTIVE_DIRECTORY_USER,REMOVE_OLDEST_DEVICE_ON_AUTOREGISTRATION)
                                VALUES(@CompanyId,1,1,@AllowDeleteCommand,0,1)";

            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            cmd.Parameters.AddWithValue("@AllowDeleteCommand", this.AllowDeleteCommand);
            if (MSSqlClient.ExecuteNonQueryRecord(cmd) > 0)
            {
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            else
            {
                throw new Exception("Internal server error");
            }
        }
        void updateOrInsertAllowActiveDirUser()
        {
            string query = @"IF EXISTS(SELECT COMPANY_ID FROM TBL_ACCOUNT_SETTINGS WHERE COMPANY_ID = @CompanyId)
                                UPDATE TBL_ACCOUNT_SETTINGS
                                SET ALLOW_ACTIVE_DIRECTORY_USER = @AllowActiveDirectoryUser
                                WHERE COMPANY_ID = @CompanyId
                                ELSE
                                INSERT INTO TBL_ACCOUNT_SETTINGS (COMPANY_ID,MAX_DEVICE_PER_USER,MAX_MPLUGIN_AGENTS,ALLOW_DELETE_COMMAND,ALLOW_ACTIVE_DIRECTORY_USER,REMOVE_OLDEST_DEVICE_ON_AUTOREGISTRATION)
                                VALUES(@CompanyId,1,1,1,@AllowActiveDirectoryUser,1)";

            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            cmd.Parameters.AddWithValue("@AllowActiveDirectoryUser", this.AllowActiveDirectoryUser);
            if (MSSqlClient.ExecuteNonQueryRecord(cmd) > 0)
            {
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            else
            {
                throw new Exception("Internal server error");
            }
        }
        void updateOrInsertAutoLogin()
        {
            string query = @"IF EXISTS(SELECT COMPANY_ID FROM TBL_ACCOUNT_SETTINGS WHERE COMPANY_ID = @CompanyId)
                                UPDATE TBL_ACCOUNT_SETTINGS
                                SET AUTO_LOGIN = @AUTO_LOGIN
                                WHERE COMPANY_ID = @CompanyId
                                ELSE
                                INSERT INTO TBL_ACCOUNT_SETTINGS (COMPANY_ID,MAX_DEVICE_PER_USER, MAX_MPLUGIN_AGENTS, ALLOW_DELETE_COMMAND, ALLOW_ACTIVE_DIRECTORY_USER, REMOVE_OLDEST_DEVICE_ON_AUTOREGISTRATION,AUTO_LOGIN)
                                VALUES(@CompanyId,1,1,1,0,1,@AUTO_LOGIN)";

            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            cmd.Parameters.AddWithValue("@AUTO_LOGIN", this.AutoLogin ? 1 : 0);
            if (MSSqlClient.ExecuteNonQueryRecord(cmd) > 0)
            {
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            else
            {
                throw new Exception("Internal server error");
            }
        }
        bool anyNonExceptionUserAlreadyHasMoreDevicesRegistered()
        {
            string strQuery = @"SELECT COUNT(RegDev.USER_ID) AS ALREADY_REGISTERED, RegDev.USER_ID,RegDev.COMPANY_ID,DevSetngs.MAX_DEVICE,UsrDtl.USER_NAME
                                FROM TBL_REGISTERED_DEVICE AS RegDev
                                LEFT OUTER JOIN TBL_USER_DEVICE_SETTINGS AS DevSetngs
                                ON RegDev.USER_ID = DevSetngs.USER_ID
                                INNER JOIN TBL_USER_DETAIL UsrDtl
                                ON UsrDtl.USER_ID = RegDev.USER_ID
                                WHERE RegDev.COMPANY_ID = @CompanyId GROUP BY RegDev.USER_ID,RegDev.COMPANY_ID,DevSetngs.MAX_DEVICE,UsrDtl.USER_NAME";

            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            DataSet dsDeviseSetngsForUser = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            string strFilter = String.Format("ALREADY_REGISTERED > {0} AND MAX_DEVICE IS NULL", this.MaxDevicePerUser);//users who are not VIP(Exceptional users).
            DataRow[] rows = dsDeviseSetngsForUser.Tables[0].Select(strFilter);
            if (rows.Length > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string CompanyId
        {
            set;
            get;
        }

        public int MaxDevicePerUser
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public bool AllowDeleteCommand
        {
            get;
            set;
        }
        public bool AllowActiveDirectoryUser
        {
            get;
            set;
        }
        private RECORD_TO_CHANGE RecordToChange
        {
            get { return _recordToChange; }
            set { _recordToChange = value; }
        }

        public bool AutoLogin
        {
            get;
            private set;
        }

        //        public void Process()
        //        {
        //            try
        //            {
        //                this.StatusCode = -1000;//for error
        //                if (!anyNonExceptionUserAlreadyHasMoreDevicesRegistered())
        //                {
        //                    string query = @"IF EXISTS(SELECT COMPANY_ID FROM TBL_ACCOUNT_SETTINGS WHERE COMPANY_ID = @CompanyId)
        //                                UPDATE TBL_ACCOUNT_SETTINGS
        //                                SET MAX_DEVICE_PER_USER = @MaxDevice
        //                                ,ALLOW_DELETE_COMMAND = @AllowDeleteCommand,
        //                                ALLOW_ACTIVE_DIRECTORY_USER = @AllowActiveDirectoryUser
        //                                WHERE COMPANY_ID = @CompanyId
        //                                ELSE
        //                                INSERT INTO TBL_ACCOUNT_SETTINGS
        //                                VALUES(@CompanyId,@MaxDevice,1,@AllowDeleteCommand,@AllowActiveDirectoryUser)";

        //                    SqlCommand objSqlCommand = new SqlCommand(query);
        //                    objSqlCommand.CommandType = CommandType.Text;
        //                    objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
        //                    objSqlCommand.Parameters.AddWithValue("@MaxDevice", this.MaxDevicePerUser);
        //                    objSqlCommand.Parameters.AddWithValue("@AllowDeleteCommand", this.AllowDeleteCommand ? 1 : 0);
        //                    objSqlCommand.Parameters.AddWithValue("@AllowActiveDirectoryUser", this.AllowActiveDirectoryUser ? 1 : 0);
        //                    if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
        //                    {
        //                        this.StatusCode = 0;
        //                        this.StatusDescription = "";
        //                    }
        //                    else
        //                    {
        //                        throw new Exception("Internal server error");
        //                    }
        //                }
        //                else
        //                {
        //                    throw new Exception("There are already users who have more devices registered than the max device selected.");
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                StatusCode = -1000;
        //                StatusDescription = ex.Message;
        //            }
        //        }
    }
}
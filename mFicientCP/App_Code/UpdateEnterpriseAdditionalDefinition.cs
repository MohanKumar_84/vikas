﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Data;
//using System.Data.SqlClient;

//namespace mFicientCP
//{
//    public class UpdateEnterpriseAdditionalDefinition
//    {

//        public UpdateEnterpriseAdditionalDefinition()
//        {

//        }
//        public UpdateEnterpriseAdditionalDefinition(string _CompanyId, string _AdditionalParameterDefinition, string _CredentialsDefinition)
//        {
//            this.AdditionalParameterDefinitionJson = _AdditionalParameterDefinition;
//            this.CredentialsDefinitionJson = _CredentialsDefinition;
//            this.CompanyId = CompanyId;
//            Process();
//        }
//        public void Process()
//        {
//            this.StatusCode = -1000;
//            SqlConnection objSqlConnection = null;
//            SqlTransaction objSqlTransaction = null;
//            SqlCommand objSqlCommand;
//            string query;
//            try
//            {
//                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
//                query = @"Update dbo.TBL_ENTERPRISE_ADDITIONAL_DEFINITION  set ADDITIONAL_PARAMETERS_META=@ADDITIONAL_PARAMETERS_META AND  AUTHENTICATION_META=@AUTHENTICATION_META  where COMPANY_ID=@CompanyId";
//                objSqlTransaction = objSqlConnection.BeginTransaction();
//                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
//                objSqlCommand.Parameters.AddWithValue("@ADDITIONAL_PARAMETERS_META", this.AdditionalParameterDefinitionJson);
//                objSqlCommand.Parameters.AddWithValue("@AUTHENTICATION_META", this.CredentialsDefinitionJson);
//                if (objSqlCommand.ExecuteNonQuery() > 0)
//                {
//                    objSqlTransaction.Commit();
//                    this.StatusCode = 0;
//                }
//                else
//                {
//                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
//                }
//            }
//            catch (Exception ex)
//            {
//                this.StatusCode = -1000;
//                this.StatusDescription = "Internal server error";

//            }
//            finally
//            {
//                MSSqlClient.SqlConnectionClose(objSqlConnection);
//            }
//        }
//        public string AdditionalParameterDefinitionJson { get; set; }
//        public string CredentialsDefinitionJson { get; set; }
//        public string CompanyId { get; set; }
//        public string StatusDescription { get; set; }
//        public int StatusCode { get; set; }
//    }
//}
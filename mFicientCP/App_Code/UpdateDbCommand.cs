﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class UpdateDbCommand
    {
        public UpdateDbCommand(string _CommandId, string _SubAdminId, string _CommandName, string _ConnactorId, string _Query, string _Parameter, string _ReturnType, string _DbCommandType, string _TableName, string _SelectFrom, string _ParaJson, string _Columns, string _CompanyId, string _Description, int _Cache, int _Expfrequency, string _ExpCondtion, long _UpdatedOn, string _ImageColumn, string _Noexecutecondtion)
        {
            this.CommandId = _CommandId;
            this.SubAdminId = _SubAdminId;
            this.CommandName = _CommandName;
            this.ConnactorId = _ConnactorId;
            this.Query = _Query;
            this.Parameter = _Parameter;
            this.ReturnType = _ReturnType;
            this.DbCommandType = _DbCommandType;
            this.TableName = _TableName;
            this.SelectFrom = _SelectFrom;
            this.ParaJson = _ParaJson;
            this.Columns = _Columns;
            this.CompanyId = _CompanyId;
            this.Description = _Description;
            this.Cache = _Cache;
            this.Expfrequency = _Expfrequency;
            this.ImageColumn = _ImageColumn;
            this.ExpCondtion = _ExpCondtion;
            this.UpdatedOn = _UpdatedOn;
            this.NoExcuteCondtion = _Noexecutecondtion;
            Process2();
        }

        private void Process2()
        {
            try
            {
                string query = @"UPDATE TBL_DATABASE_COMMAND SET
                                SQL_QUERY=@SQL_QUERY,
                                PARAMETER=@PARAMETER,
                                RETURN_TYPE=@RETURN_TYPE,
                                DB_COMMAND_NAME=@DB_COMMAND_NAME,
                                DB_CONNECTOR_ID=@DB_CONNECTOR_ID,
                                DB_COMMAND_TYPE=@DB_COMMAND_TYPE,
                                TABLE_NAME=@TABLE_NAME,
                                QUERY_TYPE=@QUERY_TYPE,
                                PARAMETER_JSON=@PARAMETER_JSON,
                                COLUMNS=@COLUMNS,
                                DESCRIPTION = @DESCRIPTION,
                                CACHE = @CACHE,
                                EXPIRY_FREQUENCY=@EXPIRY_FREQUENCY,
                                EXPIRY_CONDITION=@EXPIRY_CONDITION,
                                IMAGE_COLUMN=@IMAGE_COLUMN,
                                UPDATED_ON=@UPDATED_ON,
                                SUBADMIN_ID=@SUBADMIN_ID,
                                NO_EXECUTE_CONDITION=@NO_EXECUTE_CONDITION
                                WHERE
                                DB_COMMAND_ID=@DB_COMMAND_ID
                                AND COMPANY_ID=@COMPANY_ID;";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_ID", this.CommandId);
                objSqlCommand.Parameters.AddWithValue("@DB_CONNECTOR_ID", this.ConnactorId);
                objSqlCommand.Parameters.AddWithValue("@SQL_QUERY", this.Query);
                objSqlCommand.Parameters.AddWithValue("@PARAMETER", this.Parameter);
                objSqlCommand.Parameters.AddWithValue("@RETURN_TYPE", this.ReturnType);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_NAME", this.CommandName);
                objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_TYPE", this.DbCommandType);
                objSqlCommand.Parameters.AddWithValue("@TABLE_NAME", this.TableName);
                objSqlCommand.Parameters.AddWithValue("@QUERY_TYPE", this.SelectFrom);
                objSqlCommand.Parameters.AddWithValue("@PARAMETER_JSON", this.ParaJson);
                objSqlCommand.Parameters.AddWithValue("@COLUMNS", this.Columns);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", this.Description);
                objSqlCommand.Parameters.AddWithValue("@CACHE", this.Cache);
                objSqlCommand.Parameters.AddWithValue("@EXPIRY_FREQUENCY", this.Expfrequency);
                objSqlCommand.Parameters.AddWithValue("@EXPIRY_CONDITION", this.ExpCondtion);
                objSqlCommand.Parameters.AddWithValue("@IMAGE_COLUMN", this.ImageColumn);
                objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", this.UpdatedOn);
                objSqlCommand.Parameters.AddWithValue("@NO_EXECUTE_CONDITION", this.NoExcuteCondtion);

                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
            }
        }

        public int StatusCode { get; set; }
        private string SubAdminId { get; set; }
        private string CommandName { get; set; }
        private string ReturnType { get; set; }
        private string Parameter { get; set; }
        private string Query { get; set; }
        private string ConnactorId { get; set; }
        private string CommandId { get; set; }
        private string StatusDescription { get; set; }
        private string DbCommandType { get; set; }
        private string TableName { get; set; }
        private string SelectFrom { get; set; }
        private string ParaJson { get; set; }
        private string Columns { get; set; }
        private string CompanyId { get; set; }
        private string Description { get; set; }
        private int Cache { get; set; }
        private int Expfrequency { get; set; }
        private string ImageColumn { get; set; }
        private string ExpCondtion { get; set; }
        private long UpdatedOn { get; set; }

        private string NoExcuteCondtion { get; set; }


    }
}
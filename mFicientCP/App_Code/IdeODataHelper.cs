﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace mFicientCP
{
    public class IdeODataHelper
    {
        public IdeODataHelper()
        {
        }

        #region ODataConnector

        
        public bool SaveOdataConnector(string _CompanyId, string _SubAdminId, string _ConnId, string _ConnName, bool _Auth, string _HttpUserName, string _HttpPassword, string _HttpAutenticationtype, string _Url, string _Mplugin, string CREDENTIAL_PROPERTY,  out string _Script)
        {
            _Script = "";
            bool isValid = false;
            if (_SubAdminId.Length == 0) return false;
            string strOdatContent = "", strErrorDesc = "", strVersion = "";
            StringBuilder sbScript = new StringBuilder();
            try
            {
                if (_Mplugin.Trim() == "-1")
                {
                    strOdatContent = GetOdataOnline(_Auth, _HttpUserName.Trim(), _HttpPassword, _HttpAutenticationtype, _Url, out strVersion);
                }
                else if (_HttpUserName != "" && _HttpPassword != "")
                {
                    strOdatContent = GetODataMetaData(_CompanyId, _Mplugin, _Url, _HttpUserName, _HttpPassword, _HttpAutenticationtype, out strErrorDesc);
                }
            }
            catch
            {
                strVersion = "";
                strErrorDesc = (strErrorDesc.Length == 0 ? "Odata metadata is not valid." : strErrorDesc);
                _Script = @"SubProcBoxMessage(true);$('#aMessage').html('" + strErrorDesc + "');CallUniformCssForOdataConn()";
            }
            if (strOdatContent.Length == 0)
            {
                strErrorDesc = (strErrorDesc.Length == 0 ? "Odata metadata does not exists." : strErrorDesc);
                _Script = @"SubProcBoxMessage(true);$('#aMessage').html('" + strErrorDesc + "');";
            }
            else
            {
                strVersion = (strVersion.Trim().Length == 0 ? "2" : strVersion);

                 if (_ConnId.Length == 0 && _Mplugin != "-1")
                {
                    AddODataServiceConnector obj = new AddODataServiceConnector(_CompanyId, _SubAdminId, _ConnName, _Url,
                                                                               (_Auth ? "" : ""),
                                                                               (_Auth ? "" : ""),
                                                                                (_Auth ? _HttpAutenticationtype : ""),
                                                                                strOdatContent, (_Mplugin == "-1" ? "" : _Mplugin), (strVersion.Contains('.') ? strVersion : strVersion + ".0"), _SubAdminId, DateTime.UtcNow.Ticks, DateTime.UtcNow.Ticks, CREDENTIAL_PROPERTY);
                    if (obj.StatusCode == 0)
                    {
                        _Script = @"$('#aCFmessage').html('OData service connector saved successfully.');SubProcConfirmBoxMessage(true,'  Saved',250);SubProcAddOdataConn(false); ";
                        isValid = true;
                    }
                    else
                    {
                        _Script = @"SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.'); ";
                    }
                }
               
                 else if (_ConnId.Length == 0)
                  {
                    AddODataServiceConnector obj = new AddODataServiceConnector(_CompanyId, _SubAdminId, _ConnName, _Url,
                                                                               (_Auth ? "": ""),
                                                                               (_Auth ? "" : ""),
                                                                                (_Auth ? _HttpAutenticationtype : ""),
                                                                                strOdatContent, (_Mplugin == "-1" ? "" : _Mplugin), (strVersion.Contains('.') ? strVersion : strVersion + ".0"), _SubAdminId, DateTime.UtcNow.Ticks, DateTime.UtcNow.Ticks, CREDENTIAL_PROPERTY);
                    if (obj.StatusCode == 0)
                    {
                        _Script = @"$('#aCFmessage').html('OData service connector saved successfully.');SubProcConfirmBoxMessage(true,'  Saved',250);SubProcAddOdataConn(false); ";
                        isValid = true;
                    }
                    else
                    {
                        _Script = @"SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.'); ";
                    }
                }
                 else if (_ConnId.Length > 0 && _Mplugin != "-1")
                 {
                     UpdateODataServiceConnector obj = new UpdateODataServiceConnector(_CompanyId, _SubAdminId, _ConnId, _Url,
                                                                                       (_Auth ? "" : ""),
                                                                                       (_Auth ? "" : ""),
                                                                                        (_Auth ? _HttpAutenticationtype : ""),
                                                                                       strOdatContent, (_Mplugin == "-1" ? "" : _Mplugin), (strVersion.Contains('.') ? strVersion : strVersion + ".0"), DateTime.UtcNow.Ticks, CREDENTIAL_PROPERTY);
                     if (obj.StatusCode == 0)
                     {
                         _Script = "$('#aCFmessage').html('Odata service connector updated successfully.');SubProcConfirmBoxMessage(true,'  Saved',350);SubProcAddOdataConn(false); ";
                         isValid = true;
                     }
                     else
                     {
                         _Script = @"ShowODataConnEdit();SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.'); ";
                     }
                 }
                else if (_ConnId.Length > 0)
                {
                    UpdateODataServiceConnector obj = new UpdateODataServiceConnector(_CompanyId, _SubAdminId, _ConnId, _Url,
                                                                                      (_Auth ? "" : ""),
                                                                                      (_Auth ? "" : ""),
                                                                                       (_Auth ? _HttpAutenticationtype : ""),
                                                                                      strOdatContent, (_Mplugin == "-1" ? "" : _Mplugin), (strVersion.Contains('.') ? strVersion : strVersion + ".0"), DateTime.UtcNow.Ticks, CREDENTIAL_PROPERTY);
                    if (obj.StatusCode == 0)
                    {
                        _Script = "$('#aCFmessage').html('Odata service connector updated successfully.');SubProcConfirmBoxMessage(true,'  Saved',350);SubProcAddOdataConn(false); ";
                        isValid = true;
                    }
                    else
                    {
                        _Script = @"ShowODataConnEdit();SubProcBoxMessage(true);$('#aMessage').html('Data cannot be saved.Internal server error.'); ";
                    }
                }
            }
            return isValid;
        }

        private string GetODataMetaData(string _CompanyId, string _AgentName, string _Url, string _UserName, string _PassWord, out string _Desc)
        {
            string strRqst, strUrl = "", stOData = "";
            _Desc = "";
            try
            {
                string strTicks = Convert.ToString(DateTime.Now.Ticks);
                string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);
                mPluginAgents objAgent = new mPluginAgents();
                strRqst = "{\"req\":{\"rid\":\"" + strRqtId + "\",\"eid\":\"" + _CompanyId + "\",\"agtnm\":\"" + _AgentName + "\",\"agtpwd\":\"" + objAgent.GetMpluginAgentPassword(_CompanyId, _AgentName) + "\",\"wsurl\":\"" + _Url + "\",\"hunm\":\"" + _UserName + "\",\"hpwd\":\"" + _PassWord + "\"}}";

                GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(_CompanyId);
                objServerUrl.Process();
                if (objServerUrl.StatusCode == 0)
                {
                    strUrl = objServerUrl.ServerUrl;
                    if (strUrl.Length > 0)
                    {
                        strUrl = strUrl + "/MPGetOdataMetadataInfo.aspx?d=" + Utilities.UrlEncode(strRqst);
                        HTTP oHttp = new HTTP(strUrl);
                        oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                        HttpResponseStatus oResponse = oHttp.Request();
                        if (oResponse.StatusCode == HttpStatusCode.OK)
                        {
                            MP_GetWsdlResp obj = new MP_GetWsdlResp(oResponse.ResponseText);
                            if (obj.StatusCode == 0)
                            {
                                stOData = obj.Data;
                            }
                            else
                            {
                                _Desc = obj.StatusDescription;
                                return stOData;

                            }
                            return stOData;
                        }
                        else
                        {
                            return stOData;
                        }
                    }
                    else
                    {
                        return stOData;
                    }
                }
                else
                {
                    return stOData;
                }
            }
            catch
            {
                return "";
            }
        }
        private string GetODataMetaData(string _CompanyId, string _AgentName, string _Url, string _UserName, string _PassWord, string _HttpAutenticationtype, out string _Desc)
        {
            string strRqst, strUrl = "", stOData = "";
            _Desc = "";
            try
            {
                string strTicks = Convert.ToString(DateTime.Now.Ticks);
                string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);
                mPluginAgents objAgent = new mPluginAgents();
                strRqst = "{\"req\":{\"rid\":\"" + strRqtId + "\",\"eid\":\"" + _CompanyId + "\",\"agtnm\":\"" + _AgentName + "\",\"agtpwd\":\"" + objAgent.GetMpluginAgentPassword(_CompanyId, _AgentName) + "\",\"wsurl\":\"" + _Url + "\",\"hunm\":\"" + _UserName + "\",\"hpwd\":\"" + _PassWord + "\"}}";

                GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(_CompanyId);
                objServerUrl.Process();
                if (objServerUrl.StatusCode == 0)
                {
                    strUrl = objServerUrl.ServerUrl;
                    if (strUrl.Length > 0)
                    {
                        strUrl = strUrl + "/MPGetOdataMetadataInfo.aspx?d=" + Utilities.UrlEncode(strRqst);
                        HTTP oHttp = new HTTP(strUrl);
                        oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                        HttpResponseStatus oResponse = oHttp.Request();
                        if (oResponse.StatusCode == HttpStatusCode.OK)
                        {
                            MP_GetWsdlResp obj = new MP_GetWsdlResp(oResponse.ResponseText);
                            if (obj.StatusCode == 0)
                            {
                                stOData = obj.Data;
                            }
                            else
                            {
                                _Desc = obj.StatusDescription;
                                return stOData;

                            }
                            return stOData;
                        }
                        else
                        {
                            return stOData;
                        }
                    }
                    else
                    {
                        return stOData;
                    }
                }
                else
                {
                    return stOData;
                }
            }
            catch
            {
                return "";
            }
        }
        private string GetOdataOnline(Boolean _AddCredentials, string _UserId, string _Password, string _Url, out string _Version)
        {
            _Version = "";
            try
            {
                ODataHTTP oDataHttp = new ODataHTTP(_Url + "$metadata");
                oDataHttp.SetHttpCredentials(_UserId, _Password);
                oDataHttp.HttpRequestMethod = ODataHTTP.EnumHttpMethod.HTTP_GET;
                oDataHttp.RequestFormat = ResponseType.XML;
                HttpResponseStatus oResponse = oDataHttp.Request();

                _Version = GetOdataVersion(oResponse.ResponseText);
                return oResponse.ResponseText;
            }
            catch 
            {
                return "";
            }
        }
        private string GetOdataOnline(Boolean _AddCredentials, string _UserId, string _Password, string _HttpAutenticationtype, string _Url, out string _Version)
        {
            _Version = "";
            try
            {
                ODataHTTP oDataHttp = new ODataHTTP(_Url + "$metadata");
                oDataHttp.SetHttpCredentials(_UserId, _Password);
                oDataHttp.HttpRequestMethod = ODataHTTP.EnumHttpMethod.HTTP_GET;
                oDataHttp.RequestFormat = ResponseType.XML;
                HttpResponseStatus oResponse = oDataHttp.Request();

                _Version = GetOdataVersion(oResponse.ResponseText);
                return oResponse.ResponseText;
            }
            catch 
            {
                return "";
            }
        }


        private string GetOdataVersion(string _MetadataString)
        {
            ODATA obj = new ODATA(_MetadataString);
            return obj.MajorVersion;
        }

        public bool DeleteODataConnector(string _CompanyId, string _SubAdminId, string _ConnectionId)
        {
            DeleteODataConnector obj = new DeleteODataConnector(_CompanyId, _SubAdminId, _ConnectionId);
            obj.Process();
            if (obj.StatusCode == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void BindOdataConnDp(DataTable _dt, DropDownList _ddl, UpdatePanel _Upd)
        {
            if (_dt != null)
            {
                _ddl.DataSource = _dt;
                _ddl.DataTextField = "CONNECTION_NAME";
                _ddl.DataValueField = "ODATA_CONNECTOR_ID";
                _ddl.DataBind();
                _ddl.Items.Insert(0, new ListItem("Select Odata Connector", "-1"));
                _Upd.Update();
            }
            else
            {
                _ddl.Items.Insert(0, new ListItem("Select Odata Connector", "-1"));
                _Upd.Update();
            }
        }


        public void BindOdataConnDp(string _CompanyId, string _SubAdminId, DropDownList _ddl, UpdatePanel _Upd)
        {
            GetODataSrviceConnector obj = new GetODataSrviceConnector(true, _SubAdminId, "", "", _CompanyId,1);
            if (obj.StatusCode == 0)
            {
                _ddl.DataSource = obj.ResultTable;
                _ddl.DataTextField = "CONNECTION_NAME";
                _ddl.DataValueField = "ODATA_CONNECTOR_ID";
                _ddl.DataBind();
                _ddl.Items.Insert(0, new ListItem("Select Odata Connector", "-1"));
            }
            else
            {
                _ddl.Items.Insert(0, new ListItem("Select Odata Connector", "-1"));
            }
            _Upd.Update();
        }

        #endregion

        #region ODataCommand
        public void BindOdataCmdDp(string _CompanyId, string _SubAdminId, List<DropDownList> _LstDdl)
        {
            GetOdataCommand obj = new GetOdataCommand(true, false, _SubAdminId, "", "", _CompanyId);
            if (obj.StatusCode == 0)
            {

                foreach (DropDownList ddl in _LstDdl)
                {
                    ddl.DataSource = obj.ResultTable;
                    ddl.DataTextField = "ODATA_COMMAND_NAME";
                    ddl.DataValueField = "ODATA_COMMAND_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem("Select Odata Object", "-1"));
                }
            }
            else
            {
                foreach (DropDownList ddl in _LstDdl)
                {
                    ddl.Items.Clear();
                    ddl.Items.Insert(0, new ListItem("Select Odata Object", "-1"));
                }
            }
        }

        public string getJsonOfAllOdataCommands(string SubAdminid, string CompanyId)
        {
            string strJson = "";
            List<IdeOdataCmdsForJson> lstOdataCmds = new List<IdeOdataCmdsForJson>();
            GetOdataCommand objOdataCmds = new GetOdataCommand(true, false, SubAdminid, "", "", CompanyId);
            if (objOdataCmds.StatusCode == 0)
            {
                DataTable dtblOdataCmds = objOdataCmds.ResultTable;
                if (dtblOdataCmds != null)
                {
                    foreach (DataRow row in dtblOdataCmds.Rows)
                    {
                        IdeOdataCmdsForJson objOdataCmd = new IdeOdataCmdsForJson();
                        objOdataCmd.compId = Convert.ToString(row["COMPANY_ID"]);
                        objOdataCmd.connectorId = Convert.ToString(row["ODATA_CONNECTOR_ID"]);
                        objOdataCmd.connectionName = Convert.ToString(row["CONNECTION_NAME"]);
                        objOdataCmd.endPoint = Convert.ToString(row["ODATA_ENDPOINT"]);
                        objOdataCmd.serviceVer = Convert.ToString(row["VERSION"]);
                        objOdataCmd.commandId = Convert.ToString(row["ODATA_COMMAND_ID"]);
                        objOdataCmd.commandName = Convert.ToString(row["ODATA_COMMAND_NAME"]);
                        objOdataCmd.resourceType = Convert.ToString(row["RESOURCE_TYPE"]);
                        objOdataCmd.resourcePath = Convert.ToString(row["RESOURCE_PATH"]);
                        objOdataCmd.httpType = Convert.ToString(row["HTTP_TYPE"]);
                        objOdataCmd.httpData = Convert.ToString(row["HTTP_DATA"]);
                        objOdataCmd.queryOption = Convert.ToString(row["QUERY_OPTION"]);
                        objOdataCmd.inputParamJson = Convert.ToString(row["INPUT_PARAMETER_JSON"]);
                        objOdataCmd.outputParamJson = Convert.ToString(row["OUTPUT_PARAMETER_JSON"]);
                        objOdataCmd.functionType = Convert.ToString(row["FUNCTION_TYPE"]);
                        objOdataCmd.responseFormat = Convert.ToString(row["RESPONSE_FORMAT"]);
                        objOdataCmd.dataJson = Convert.ToString(row["DATA_JSON"]);
                        objOdataCmd.datasetPath = Convert.ToString(row["DATASET_PATH"]);
                        objOdataCmd.returnType = Convert.ToString(row["RETURN_TYPE"]);
                        objOdataCmd.additionalResourcePath = Convert.ToString(row["ADDITIONAL_RESOURCE_PATH"]);
                        objOdataCmd.description = Convert.ToString(row["DESCRIPTION"]);
                        lstOdataCmds.Add(objOdataCmd);
                    }
                }
            }
            strJson = Utilities.SerializeJson<List<IdeOdataCmdsForJson>>(lstOdataCmds);
            return strJson;
        }
        #endregion
    }

}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetRequestForRegisteringDevice
    {
        //public enum SORT_TYPE
        //{
        //    ASC,
        //    DESC
        //}
        //public enum ORDER_BY
        //{
        //    Name = 1,
        //    OS = 2,
        //    Devices = 3,
        //    RequestDate = 4,
        //    RequestType = 5
        //}

        public GetRequestForRegisteringDevice(string companyId, string subAdminId,PENDING_DEVICES_ORDER_BY orderBy,SORT_TYPE sortType)
        {
            SubAdminId = subAdminId;
            CompanyId = companyId;
            this.OrderBy = orderBy;
            this.SortType = sortType;
        }
        public GetRequestForRegisteringDevice(string companyId, string subAdminId, string userId, PENDING_DEVICES_ORDER_BY orderBy, SORT_TYPE sortType)
        {
            SubAdminId = subAdminId;
            CompanyId = companyId;
            this.UserId = userId;
            this.OrderBy = orderBy;
            this.SortType = sortType;
        }
        public void Process()
        {
            try
            {
                //for error
                //if process is successfull then value will be 0
               
                
                //this was query used(the last one below) previously.we only needed to know how many registered devices left 
                //outer join with tbl_device_registration_request is not required

                /*
                 * SELECT COUNT(*) AS REGISTERED_DEVICE_COUNT,RegDev.USER_ID,
                    UsrDevSetng.MAX_DEVICE,AccSetng.MAX_DEVICE_PER_USER
                    FROM TBL_REGISTERED_DEVICE AS RegDev
                  LEFT OUTER JOIN TBL_DEVICE_REGISTRATION_REQUEST AS DevReq
                    ON RegDev.USER_ID = RegDev.USER_ID
                    LEFT OUTER JOIN TBL_USER_DEVICE_SETTINGS AS UsrDevSetng
                    ON UsrDevSetng.USER_ID=DevReq.USER_ID
                    LEFT OUTER JOIN TBL_ACCOUNT_SETTINGS AS AccSetng
                    ON AccSetng.COMPANY_ID = RegDev.COMPANY_ID
                    WHERE RegDev.COMPANY_ID =  @CompanyId
                    AND RegDev.SUBADMIN_ID =@SubAdminId
                    GROUP BY RegDev.USER_ID,UsrDevSetng.MAX_DEVICE,AccSetng.MAX_DEVICE_PER_USER;"
                 * 
                 * 
                 * */
                StatusCode = -1000;
                string strQuery = getUserAndDevicesDetailQuery() +


                                   @" SELECT * FROM TBL_MST_REQUEST_TYPE_FOR_DEVICE_REGISTRATION;

                                    SELECT COUNT(*) AS Total,CurrPlan.MAX_USER,AccSetng.MAX_DEVICE_PER_USER
                                                    FROM TBL_REGISTERED_DEVICE AS RegDev
                                                    INNER JOIN TBL_COMPANY_CURRENT_PLAN AS CurrPlan
                                                    ON RegDev.COMPANY_ID = CurrPlan.COMPANY_ID
													LEFT OUTER JOIN TBL_ACCOUNT_SETTINGS AS AccSetng
                                    ON AccSetng.COMPANY_ID = RegDev.COMPANY_ID
                                                    WHERE RegDev.COMPANY_ID = @CompanyId
                                                    GROUP BY CurrPlan.MAX_USER ,MAX_DEVICE_PER_USER;

                                    SELECT COUNT(*) AS Total FROM
                                    TBL_DEVICE_REGISTRATION_REQUEST  request
                                    INNER JOIN TBL_USER_DETAIL usr
                                    ON request.USER_ID = usr.USER_ID
                                    WHERE request.COMPANY_ID =@CompanyId
                                    AND STATUS = 0;
                                    
                                    SELECT MAX_USER
                                   FROM TBL_COMPANY_CURRENT_PLAN
                                    WHERE COMPANY_ID = @CompanyId;

                                    SELECT COUNT(*) AS REGISTERED_DEVICE_COUNT,RegDev.USER_ID,
                                    UsrDevSetng.MAX_DEVICE,AccSetng.MAX_DEVICE_PER_USER
                                    FROM TBL_REGISTERED_DEVICE AS RegDev
                                    LEFT OUTER JOIN TBL_USER_DEVICE_SETTINGS AS UsrDevSetng
                                    ON UsrDevSetng.USER_ID=RegDev.USER_ID
                                    LEFT OUTER JOIN TBL_ACCOUNT_SETTINGS AS AccSetng
                                    ON AccSetng.COMPANY_ID = RegDev.COMPANY_ID
                                    WHERE RegDev.COMPANY_ID = @CompanyId
                                    GROUP BY RegDev.USER_ID,UsrDevSetng.MAX_DEVICE,AccSetng.MAX_DEVICE_PER_USER;

                                    SELECT COUNT(*) AS REGISTERED_DEVICE_COUNT FROM TBL_REGISTERED_DEVICE AS RegDev WHERE RegDev.COMPANY_ID = @CompanyId;
                                    
                                    SELECT COUNT(*) AS REQUEST_COUNT,USER_ID
                                    FROM TBL_DEVICE_REGISTRATION_REQUEST
                                    WHERE COMPANY_ID = @CompanyId
                                    AND STATUS =0
                                    GROUP BY USER_ID;"; //this last query added on 22/8/2012.Change in registerDevice.
                

                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                if (!String.IsNullOrEmpty(UserId))
                {
                    objSqlCommand.Parameters.AddWithValue("@UserId", this.UserId);
                }
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    if (objDataSet.Tables.Count > 0)
                    {
                        ResultTables = objDataSet;
                        StatusCode = 0;
                        StatusDescription = "";
                    }
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }
        }

        string getUserAndDevicesDetailQuery()
        {
            string strQuery;
            if (String.IsNullOrEmpty(UserId))
            {
                strQuery = @"SELECT Request.*,usr.SUBADMIN_ID,(usr.FIRST_NAME +' '+usr.LAST_NAME)AS FULL_NAME ,usr.USER_NAME,usr.FIRST_NAME,usr.LAST_NAME,reqtype.REQUEST_DESCRIPTION,device.DEVICE_ID as 'RegDeviceId'
                                    ,(SELECT REQUEST_ID FROM TBL_MST_REQUEST_TYPE_FOR_DEVICE_REGISTRATION WHERE REQUEST_DESCRIPTION = 'DENY')AS 'DenyId'
                                    FROM TBL_DEVICE_REGISTRATION_REQUEST AS request
                                    
                                    INNER JOIN TBL_USER_DETAIL AS usr
                                    ON request.USER_ID = usr.USER_ID
                                    
                                    INNER JOIN TBL_MST_REQUEST_TYPE_FOR_DEVICE_REGISTRATION AS reqtype
                                    ON reqtype.REQUEST_ID = request.REQUEST_TYPE
                                    
                                    LEFT OUTER JOIN TBL_REGISTERED_DEVICE as device
                                    ON device.DEVICE_ID = request.DEVICE_ID
                                    
                                    WHERE request.COMPANY_ID = @CompanyId
                                    AND STATUS =0 " + " " +"ORDER BY"+" "+ getOrderByColumnName() + " " + getSortTypeInString()+";";
            }

            else
            {
                strQuery = @"SELECT Request.*,usr.SUBADMIN_ID,(usr.FIRST_NAME +' '+usr.LAST_NAME)AS FULL_NAME ,usr.USER_NAME,usr.FIRST_NAME,usr.LAST_NAME ,reqtype.REQUEST_DESCRIPTION,device.DEVICE_ID as 'RegDeviceId'
                                    ,(SELECT REQUEST_ID FROM TBL_MST_REQUEST_TYPE_FOR_DEVICE_REGISTRATION WHERE REQUEST_DESCRIPTION = 'DENY')AS 'DenyId'
                                    FROM TBL_DEVICE_REGISTRATION_REQUEST AS request
                                    
                                    INNER JOIN TBL_USER_DETAIL AS usr
                                    ON request.USER_ID = usr.USER_ID
                                    
                                    INNER JOIN TBL_MST_REQUEST_TYPE_FOR_DEVICE_REGISTRATION AS reqtype
                                    ON reqtype.REQUEST_ID = request.REQUEST_TYPE
                                    
                                    LEFT OUTER JOIN TBL_REGISTERED_DEVICE as device
                                    ON device.DEVICE_ID = request.DEVICE_ID
                                    
                                    WHERE request.COMPANY_ID = @CompanyId
                                    AND request.USER_ID = @UserId
                                    AND STATUS =0"+" "+"ORDER BY"+" " + getOrderByColumnName() + " " + getSortTypeInString()+";";
            }
            return strQuery;
        }

        string getOrderByColumnName()
        {
            string strOrderByName = "";
            switch (this.OrderBy)
            {
                case PENDING_DEVICES_ORDER_BY.Name:
                    strOrderByName = "FULL_NAME";
                    break;
                case PENDING_DEVICES_ORDER_BY.OS:
                    strOrderByName = "DEVICE_TYPE";
                    break;
                case PENDING_DEVICES_ORDER_BY.RequestDate:
                    strOrderByName = "REQUEST_DATETIME";
                    break;
                case PENDING_DEVICES_ORDER_BY.RequestType:
                    strOrderByName = "REQUEST_DESCRIPTION";
                    break;
            }
            return strOrderByName;
        }
        string getSortTypeInString()
        {
            string strSortType = "";
            switch (this.SortType)
            {
                case SORT_TYPE.ASC:
                    strSortType = "ASC";
                    break;
                case SORT_TYPE.DESC:
                    strSortType = "DESC";
                    break;
            }
            return strSortType;
        }


        public DataSet ResultTables
        {
            set;
            get;
        }
        /// <summary>
        /// Status code if 0 then process is success else some internal error
        /// </summary>
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string UserId
        {
            set;
            get;
        }

        public PENDING_DEVICES_ORDER_BY OrderBy
        {
            get;
            set;
        }
        public SORT_TYPE SortType
        {
            get;
            set;
        }
    }
}
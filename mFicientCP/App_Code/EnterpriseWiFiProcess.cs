﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCP
{
    public class EnterpriseWiFiProcess
    {
        public EnterpriseWiFiProcess(string companyId)
        {
            this.CompanyId = companyId;
        }
        public List<MFEEnterpriseWiFi> GetWiFiDtlsByCompanyId()
        {
            string query;
            DataSet objDataSet;
            SqlCommand objSqlCommand;
            List<MFEEnterpriseWiFi> lstWiFiDtls = null;
            try
            {
                lstWiFiDtls = new List<MFEEnterpriseWiFi>();
                query = @"SELECT * FROM TBL_WIFI_MASTER WHERE COMPANY_ID = @COMPANY_ID";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet == null) throw new Exception("Internal server error.");
                lstWiFiDtls = _getWiFiDetails(objDataSet.Tables[0]);
            }
            catch (MficientException ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            return lstWiFiDtls;
        }
        public MFEEnterpriseWiFi GetWiFiDtlByName(string name)
        {
            return _getWiFiDtlByName(name);
        }
        public int UpdateWiFiDtlById(string name, string id)
        {
            int iRecUpdated = 0;
            this.StatusCode = 0;
            this.StatusDescription = "";
            try
            {
                string query = @"UPDATE TBL_WIFI_MASTER
                            SET WIFI_NAME = @WIFI_NAME
                            WHERE WIFI_ID = @WIFI_ID
                            AND COMPANY_ID = @COMPANY_ID";
                SqlCommand objCommand = new SqlCommand(query);
                objCommand.CommandType = CommandType.Text;
                objCommand.Parameters.AddWithValue("@WIFI_NAME", name);
                objCommand.Parameters.AddWithValue("@WIFI_ID", id);
                objCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                iRecUpdated = MSSqlClient.ExecuteNonQueryRecord(objCommand);
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            return iRecUpdated;
        }
        public int InsertWiFiDtl(MFEEnterpriseWiFi enterpriseWiFi)
        {
            int iRecUpdated = 0;
            this.StatusCode = 0;
            this.StatusDescription = "";
            try
            {
                string query = @"INSERT INTO TBL_WIFI_MASTER(WIFI_ID,WIFI_NAME,COMPANY_ID,CREATED_ON)
                             VALUES (@WIFI_ID,@WIFI_NAME,@COMPANY_ID,@CREATED_ON)";
                SqlCommand objCommand = new SqlCommand(query);
                objCommand.CommandType = CommandType.Text;
                objCommand.Parameters.AddWithValue("@WIFI_ID", enterpriseWiFi.Id);
                objCommand.Parameters.AddWithValue("@WIFI_NAME", enterpriseWiFi.Name);
                objCommand.Parameters.AddWithValue("@COMPANY_ID", enterpriseWiFi.CompanyId);
                objCommand.Parameters.AddWithValue("@CREATED_ON", enterpriseWiFi.CreatedOn);
                iRecUpdated = MSSqlClient.ExecuteNonQueryRecord(objCommand);
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            return iRecUpdated;
        }
        public int DeleteWiFiDtlById(string id)
        {
            int iRecChanged = 0;
            this.StatusCode = 0;
            this.StatusDescription = "";
            try
            {

                string query = @"DELETE FROM TBL_WIFI_MASTER
                                WHERE WIFI_ID = @WIFI_ID
                                AND COMPANY_ID = @COMPANY_ID";
                SqlCommand objCommand = new SqlCommand(query);
                objCommand.CommandType = CommandType.Text;
                objCommand.Parameters.AddWithValue("@WIFI_ID", id);
                objCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                return MSSqlClient.ExecuteNonQueryRecord(objCommand);
            }
            catch (Exception)
            { 
                this.StatusCode=0;
                this.StatusDescription = "Internal server error.";
            }
            return iRecChanged;
        }
        public bool doesWiFiNameExistsForCompany(string name)
        {
            MFEEnterpriseWiFi objWiFi = _getWiFiDtlByName(name);
            if (objWiFi == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool doesWiFiNameExistsForCompany(string name,string idToIgnore)
        {
            MFEEnterpriseWiFi objWiFi = _getWiFiDtlByName(name,idToIgnore);
            if (objWiFi == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #region Private Methods
        /// <summary>
        /// Gets the WiFi Dtl from the datarow provided.row should match the details of the Database table
        /// </summary>
        /// <param name="row"></param>
        /// <returns>MFEEnterpriseWiFi</returns>
        /// <exception cref="ArgumentNullException">If Row Provided is null</exception>
        MFEEnterpriseWiFi _getWiFiDetail(DataRow row)
        {
            if (row == null) throw new ArgumentNullException();

            MFEEnterpriseWiFi objWiFiDtl = new MFEEnterpriseWiFi();
            objWiFiDtl.CompanyId = Convert.ToString(row["COMPANY_ID"]);
            objWiFiDtl.CreatedOn = Convert.ToString(row["CREATED_ON"]);
            objWiFiDtl.Id = Convert.ToString(row["WIFI_ID"]);
            objWiFiDtl.Name = Convert.ToString(row["WIFI_NAME"]);
            return objWiFiDtl;
        }
        /// <summary>
        /// Get the wifi details from the datatable --> Columns should match the names as that of database.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        List<MFEEnterpriseWiFi> _getWiFiDetails(DataTable dt)
        {
            if (dt == null) throw new ArgumentNullException();
            List<MFEEnterpriseWiFi> lstWiFiDtls = new List<MFEEnterpriseWiFi>();
            foreach (DataRow row in dt.Rows)
            {
                lstWiFiDtls.Add(_getWiFiDetail(row));
            }
            return lstWiFiDtls;
        }
        MFEEnterpriseWiFi _getWiFiDtlByName(string name)
        {
            string query;
            DataSet objDataSet = null ;
            SqlCommand objSqlCommand = null;
            MFEEnterpriseWiFi objWiFi = null;
            DataTable dt = null;
            
            query = @"SELECT * FROM TBL_WIFI_MASTER WHERE COMPANY_ID = @COMPANY_ID AND WIFI_NAME = @WIFI_NAME";
            objSqlCommand = new SqlCommand(query);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@WIFI_NAME",name);
            objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            if (objDataSet == null) throw new Exception("Internal server error.");
            dt = objDataSet.Tables[0];
            if (dt != null && dt.Rows.Count>0)
            {
                objWiFi = _getWiFiDetail(dt.Rows[0]);
            }
            return objWiFi;
        }
        MFEEnterpriseWiFi _getWiFiDtlByName(string name,string idToIgnore)
        {
            string query;
            DataSet objDataSet = null;
            SqlCommand objSqlCommand = null;
            MFEEnterpriseWiFi objWiFi = null;
            DataTable dt = null;

            query = @"SELECT * FROM TBL_WIFI_MASTER WHERE COMPANY_ID = @COMPANY_ID AND WIFI_NAME = @WIFI_NAME AND WIFI_ID != @WIFI_ID";
            objSqlCommand = new SqlCommand(query);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@WIFI_NAME", name);
            objSqlCommand.Parameters.AddWithValue("@WIFI_ID", idToIgnore);
            objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            if (objDataSet == null) throw new Exception("Internal server error.");
            dt = objDataSet.Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                objWiFi = _getWiFiDetail(dt.Rows[0]);
            }
            return objWiFi;
        }
        #endregion
        #region Properties
        public string SubAdmidId
        {
            get;
            private set;
        }
        public string CompanyId
        {
            get;
            private set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public int StatusCode
        {
            get;
            set;
        }
        #endregion
    }
}
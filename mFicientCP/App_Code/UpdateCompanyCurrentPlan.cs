﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class UpdateCompanyCurrentPlan
    {
        public UpdateCompanyCurrentPlan(int noOfUsers, CompanyPlan companyPlanDtl,PLAN_RENEW_OR_CHANGE_REQUESTS updateFor)
        {
            this.UpdateFor = updateFor;
            this.NoOfUsers = noOfUsers;
            this.CompanyId = companyPlanDtl.CompanyId;
            this.CompanyPlanDtl = companyPlanDtl;
        }
        public UpdateCompanyCurrentPlan(string nextMonthPlanCode, string nextMonthPlanName,string companyId, PLAN_RENEW_OR_CHANGE_REQUESTS updateFor)
        {
            this.UpdateFor = updateFor;
            this.NextMonthPlanCode = nextMonthPlanCode;
            this.NextMonthPlanName = nextMonthPlanName;
            this.CompanyId = companyId;
            this.NoOfUsers = int.MinValue;
        }
        public void Process()
        {
            try
            {
                switch (this.UpdateFor)
                {
                    case PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED:
                    case PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_REMOVED:
                        processUpdateUser();
                        break;
                    case PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_CHANGE:
                        processUpdateNextMonthPlan();
                        break;
                }
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "";
            }
        }
        void processUpdateUser()
        {
            SqlTransaction sqlTransaction = null;
            SqlConnection con=null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);
                if (this.NoOfUsers <= 0)
                {
                    throw new Exception();
                }
                using (con)
                {
                    using (sqlTransaction = con.BeginTransaction())
                    {
                        updateUsersForCompany(sqlTransaction, con);
                        insertLogForUsersUpdate(sqlTransaction, con);
                        updateUsersInAdminTable(sqlTransaction, con);
                        //commit
                        sqlTransaction.Commit();
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (sqlTransaction != null)
                {
                    sqlTransaction.Dispose();
                }
            }
        }
        void processUpdateNextMonthPlan()
        {
            SqlTransaction sqlTransaction = null;
            SqlConnection con= null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (sqlTransaction = con.BeginTransaction())
                    {
                        updateNextMonthPlanInCurrPlan(sqlTransaction, con);
                        updateNextMonthPlanInAdminCurrPlan(sqlTransaction, con);
                        //commit
                        sqlTransaction.Commit();
                        this.StatusCode = 0;
                        this.StatusDescription = "";
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (sqlTransaction != null)
                {
                    sqlTransaction.Dispose();
                }
            }
        }
        void insertLogForUsersUpdate(SqlTransaction transaction, SqlConnection con)
        {
            string sqlQuery = @"INSERT INTO [TBL_COMPANY_PLAN_PURCHASE_HISTORY]
                                       ([COMPANY_ID],[PLAN_CODE],[MAX_WORKFLOW]
                                       ,[MAX_USER],[USER_CHARGE_PM],[CHARGE_TYPE]
                                       ,[VALIDITY],[PRICE_WITHOUT_DISCOUNT],[DISCOUNT_PER]
                                       ,[ACTUAL_PRICE],[PURCHASE_DATE],[TRANSACTION_ID]
                                       ,[TRANSACTION_TYPE],[PLAN_NAME],[QUANTITY_CHANGED])
                                 VALUES
                                       (@COMPANY_ID
                                        ,@PLAN_CODE
                                       ,@MAX_WORKFLOW
                                       ,@MAX_USER
                                        ,@USER_CHARGE_PM
                                       ,@CHARGE_TYPE
                                       ,@VALIDITY
                                       ,@PRICE_WITHOUT_DISCOUNT
                                       ,@DISCOUNT_PER
                                       ,@ACTUAL_PRICE
                                       ,@PURCHASE_DATE
                                       ,@TRANSACTION_ID
                                       ,@TRANSACTION_TYPE
                                       ,@PLAN_NAME
                                        ,@QUANTITY_CHANGED)";
            SqlCommand cmd = new SqlCommand(sqlQuery, con, transaction);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyPlanDtl.CompanyId);
            cmd.Parameters.AddWithValue("@PLAN_CODE", this.CompanyPlanDtl.PlanCode);
            cmd.Parameters.AddWithValue("@MAX_WORKFLOW", this.CompanyPlanDtl.MaxWorkFlow);
            cmd.Parameters.AddWithValue("@MAX_USER", (this.CompanyPlanDtl.MaxUsers + this.NoOfUsers));
            cmd.Parameters.AddWithValue("@USER_CHARGE_PM", this.CompanyPlanDtl.UserChargePerMonth);
            cmd.Parameters.AddWithValue("@CHARGE_TYPE", this.CompanyPlanDtl.ChargeType);
            cmd.Parameters.AddWithValue("@VALIDITY", this.CompanyPlanDtl.Validity);
            cmd.Parameters.AddWithValue("@PRICE_WITHOUT_DISCOUNT",this.UpdateFor==PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED?this.CompanyPlanDtl.getUserChargeForRemainingMonth(this.NoOfUsers):0);
            cmd.Parameters.AddWithValue("@DISCOUNT_PER",0);
            cmd.Parameters.AddWithValue("@ACTUAL_PRICE", this.UpdateFor == PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED ? this.CompanyPlanDtl.getUserChargeForRemainingMonth(this.NoOfUsers) : 0);
            cmd.Parameters.AddWithValue("@PURCHASE_DATE",DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@TRANSACTION_ID",Utilities.GetMd5Hash(this.CompanyPlanDtl.CompanyId+DateTime.UtcNow.Ticks));
            cmd.Parameters.AddWithValue("@TRANSACTION_TYPE", (int)this.UpdateFor);
            cmd.Parameters.AddWithValue("@PLAN_NAME", this.CompanyPlanDtl.PlanName);
            cmd.Parameters.AddWithValue("@QUANTITY_CHANGED",this.UpdateFor==PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED || this.UpdateFor == PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_REMOVED
                                                            ?this.NoOfUsers:0);
            cmd.ExecuteNonQuery();
        }
        void updateUsersForCompany(SqlTransaction transaction, SqlConnection con)
        {
            string strSqlQuery = @"UPDATE TBL_COMPANY_CURRENT_PLAN
                                    SET MAX_USER = @MAX_USERS
                                    , BALANCE_AMOUNT = @BALANCE_AMOUNT
                                    WHERE COMPANY_ID = @COMPANY_ID";
            SqlCommand cmd = new SqlCommand(strSqlQuery, con, transaction);
            if (this.UpdateFor == PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED)
            {
                cmd.Parameters.AddWithValue("@MAX_USERS", (this.CompanyPlanDtl.MaxUsers + NoOfUsers));
            }
            else
            {
                cmd.Parameters.AddWithValue("@MAX_USERS", (this.CompanyPlanDtl.MaxUsers - NoOfUsers));
            }
            cmd.Parameters.AddWithValue("@BALANCE_AMOUNT", (this.CompanyPlanDtl.BalanceAmount - Convert.ToDecimal((this.CompanyPlanDtl.getUserChargeForRemainingMonth(this.NoOfUsers)))));
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyPlanDtl.CompanyId);
            cmd.ExecuteNonQuery();
        }
        void updateUsersInAdminTable(SqlTransaction transaction, SqlConnection con)
        {
            string strQuery = @"UPDATE ADMIN_TBL_COMPANY_CURRENT_PLAN
                                SET MAX_USER = @MAX_USERS
                                , BALANCE_AMOUNT = @BALANCE_AMOUNT
                                WHERE COMPANY_ID = @COMPANY_ID";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            if (this.UpdateFor == PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED)
            {
                cmd.Parameters.AddWithValue("@MAX_USERS", (this.CompanyPlanDtl.MaxUsers + NoOfUsers));
            }
            else
            {
                cmd.Parameters.AddWithValue("@MAX_USERS", (this.CompanyPlanDtl.MaxUsers - NoOfUsers));
            }
            cmd.Parameters.AddWithValue("@BALANCE_AMOUNT", (this.CompanyPlanDtl.BalanceAmount - Convert.ToDecimal((this.CompanyPlanDtl.getUserChargeForRemainingMonth(this.NoOfUsers)))));
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyPlanDtl.CompanyId);
            cmd.ExecuteNonQuery();
        }

        void updateNextMonthPlanInCurrPlan(SqlTransaction transaction, SqlConnection con)
        {
            string strSqlQuery = @"UPDATE TBL_COMPANY_CURRENT_PLAN
                                    SET NEXT_MONTH_PLAN = @NEXT_MONTH_PLAN
                                    WHERE COMPANY_ID = @COMPANY_ID";
            SqlCommand cmd = new SqlCommand(strSqlQuery, con, transaction);
            cmd.Parameters.AddWithValue("@NEXT_MONTH_PLAN", this.NextMonthPlanCode);
            cmd.Parameters.AddWithValue("@COMPANY_ID",this.CompanyId);
            cmd.ExecuteNonQuery();
        }
        void updateNextMonthPlanInAdminCurrPlan(SqlTransaction transaction, SqlConnection con)
        {
            string strSqlQuery = @"UPDATE ADMIN_TBL_COMPANY_CURRENT_PLAN
                                    SET NEXT_MONTH_PLAN = @NEXT_MONTH_PLAN
                                    WHERE COMPANY_ID = @COMPANY_ID";
            SqlCommand cmd = new SqlCommand(strSqlQuery, con, transaction);
            cmd.Parameters.AddWithValue("@NEXT_MONTH_PLAN", this.NextMonthPlanCode);
            cmd.Parameters.AddWithValue("@COMPANY_ID",this.CompanyId);
            cmd.ExecuteNonQuery();
        }
        public PLAN_RENEW_OR_CHANGE_REQUESTS UpdateFor
        {
            get;
            set;
        }
        public int NoOfUsers
        {
            get;
            set;
        }
        public int StatusCode
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public CompanyPlan CompanyPlanDtl
        {
            get;
            set;
        }

        public string NextMonthPlanCode
        {
            get;
            set;
        }
        public string NextMonthPlanName
        {
            get;
            set;
        }
        public string CompanyId
        {
            get;
            set;
        }
    }
}
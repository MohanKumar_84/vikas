﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
namespace mFicientCP
{
    public class SaveGrpDtlsInMGramInsertUpdate
    {
        string _enterpriseId, _groupName, _groupId;
        List<string> _userNames;
        int _statusCode;
        string _statusDescription;

        public SaveGrpDtlsInMGramInsertUpdate(string companyId, string groupName,
            string groupId,
            List<string> userNames)
        {
            this.EnterpriseId = companyId;
            this.UserNames = userNames;
            this.GroupName = groupName;
            this.GroupId = groupId;
        }
        public void Process()
        {
            try
            {
                StringBuilder sbCommaSepUsrNames = new StringBuilder();
                int iCountofLoop =0;
                foreach (string usrName in this.UserNames)
                {
                    if (iCountofLoop == this.UserNames.Count - 1)
                    {
                        sbCommaSepUsrNames.Append(usrName);
                    }
                    else
                    {
                        sbCommaSepUsrNames.Append(usrName).Append(',');
                    }
                    iCountofLoop++;
                }
                saveGroupDtls(sbCommaSepUsrNames.ToString());
            }
            catch (Exception ex)
            {
                this.StatusCode = -1000;
                if (ex.Message == (((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR)).ToString())
                {
                    this.StatusDescription = ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
                }
                this.StatusDescription = "Internal server error.";
            }
        }
        void saveGroupDtls(string commaSepUserNames)
        {
            string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
            string strQuery = getQuery();
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID",this.EnterpriseId);
            cmd.Parameters.AddWithValue("@GROUP_ID",this.GroupId);
            cmd.Parameters.AddWithValue("@GROUP_NAME",this.GroupName);
            cmd.Parameters.AddWithValue("@USERS", commaSepUserNames);

            int iRowsEffected = MSSqlClient.ExecuteNonQueryRecord(cmd, strConnectionString);
            if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
        }

        string getQuery()
        {
            return @"IF EXISTS (SELECT GROUP_ID 
                    FROM TBL_ENTERPRISE_GROUPS 
                    WHERE ENTERPRISE_ID = @ENTERPRISE_ID
                    AND GROUP_ID = @GROUP_ID)

                    UPDATE TBL_ENTERPRISE_GROUPS
                    SET GROUP_NAME = @GROUP_NAME,
                    USERS = @USERS
                    WHERE ENTERPRISE_ID = @ENTERPRISE_ID
                    AND GROUP_ID = @GROUP_ID

                    ELSE

                    INSERT INTO TBL_ENTERPRISE_GROUPS(ENTERPRISE_ID,GROUP_NAME,USERS,GROUP_ID)
                    VALUES(@ENTERPRISE_ID,@GROUP_NAME,@USERS,@GROUP_ID);";

        }
        #region Public Properties
        public string EnterpriseId
        {
            get { return _enterpriseId; }
            set { _enterpriseId = value; }
        }
        public string GroupName
        {
            get { return _groupName; }
            set { _groupName = value; }
        }
        public List<string> UserNames
        {
            get { return _userNames; }
            set { _userNames = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            set { _statusCode = value; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }
        public string GroupId
        {
            get { return _groupId; }
            private set { _groupId = value; }
        }
        #endregion
    }
}
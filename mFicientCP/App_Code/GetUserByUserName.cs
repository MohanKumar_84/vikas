﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
namespace mFicientCP
{
    public class GetUserByUserName
    {
        #region member variables
        string _userName, _userId, _emailId, _firstName, _lastName, _companyId, _fullName, _statusDescription;
        int _statusCode;
        DataSet _resultTables;
        bool _isAdmin = false, _isSubAdmin = false, _isUser = false;
        #endregion
        #region Public Properties
        public string LastName
        {
            get { return _lastName; }
        }

        public string FirstName
        {
            get { return _firstName; }
        }

        public string EmailId
        {
            get { return _emailId; }
        }

        public string UserId
        {
            get { return _userId; }
        }

        public string UserName
        {
            get { return _userName; }
        }

        public string CompanyId
        {
            get { return _companyId; }
        }

        public DataSet ResultTables
        {
            get { return _resultTables; }
        }

        public bool IsUser
        {
            get { return _isUser; }
        }

        public bool IsSubAdmin
        {
            get { return _isSubAdmin; }
        }

        public bool IsAdmin
        {
            get { return _isAdmin; }
        }
        public string FullName
        {
            get { return _fullName; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
        }
        #endregion
        public GetUserByUserName(string userName, string companyId)
        {
            _userName = userName;
            _companyId = companyId;
        }
        public void Process()
        {
            try
            {
                string strQuery = @"SELECT * FROM TBL_COMPANY_ADMINISTRATOR
                                WHERE EMAIL_ID = @UserName;
                                SELECT * FROM TBL_SUB_ADMIN
                                WHERE USER_NAME=@UserName;
                                SELECT * FROM TBL_USER_DETAIL
                                WHERE USER_NAME = @UserName;";

                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@UserName", _userName);
                cmd.Parameters.AddWithValue("@CompanyId", _companyId);
                DataSet dsUserDetail = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsUserDetail != null)
                {
                    _resultTables = dsUserDetail;
                    if (dsUserDetail.Tables[0] != null && dsUserDetail.Tables[0].Rows.Count > 0)
                    {
                        _isAdmin = true;
                        _emailId = Convert.ToString(dsUserDetail.Tables[0].Rows[0]["EMAIL_ID"]);
                        _userId = Convert.ToString(dsUserDetail.Tables[0].Rows[0]["ADMIN_ID"]);
                        _lastName = Convert.ToString(dsUserDetail.Tables[0].Rows[0]["LAST_NAME"]);
                        _firstName = Convert.ToString(dsUserDetail.Tables[0].Rows[0]["FIRST_NAME"]);
                    }
                    else if (dsUserDetail.Tables[1] != null && dsUserDetail.Tables[1].Rows.Count > 0)
                    {
                        _isSubAdmin = true;
                        _emailId = Convert.ToString(dsUserDetail.Tables[1].Rows[0]["EMAIL"]);
                        _userId = Convert.ToString(dsUserDetail.Tables[1].Rows[0]["SUBADMIN_ID"]);
                        _fullName = Convert.ToString(dsUserDetail.Tables[1].Rows[0]["FULL_NAME"]);
                    }
                    else if (dsUserDetail.Tables[2] != null && dsUserDetail.Tables[2].Rows.Count > 0)
                    {
                        _isUser = true;
                        _emailId = Convert.ToString(dsUserDetail.Tables[0].Rows[2]["EMAIL_ID"]);
                        _userId = Convert.ToString(dsUserDetail.Tables[0].Rows[2]["USER_ID"]);
                        _lastName = Convert.ToString(dsUserDetail.Tables[0].Rows[2]["LAST_NAME"]);
                        _firstName = Convert.ToString(dsUserDetail.Tables[0].Rows[2]["FIRST_NAME"]);
                    }
                    _statusCode = 0;
                    _statusDescription = "";
                }
                else
                {
                    _statusCode = 0;
                    _statusDescription = "Internal server error";
                }
            }
            catch
            {
                _statusCode =0;
                _statusDescription="Internal server error";
            }
        }
    }
}
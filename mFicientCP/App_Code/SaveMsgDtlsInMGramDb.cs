﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class SaveMsgDtlsInMGramDb
    {
        string _enterpriseId, _category,
         _message, _statusDescription;


        short _status;
        long _pickUpTime, _sendDatetime;
        int _expirationAfter, _statusCode;


        List<mFicientCommonProcess.mGarmUserdetails> _usernames;

        public SaveMsgDtlsInMGramDb(string companyId,
                    string category, string message,
                    long sendDatetime,
                    int expirationAfter, List<mFicientCommonProcess.mGarmUserdetails> userNames)
        {
            this.EnterpriseId = companyId;
            this.Category = category;
            this.Message = message;
            this.SendDatetime = sendDatetime;
            this.Usernames = userNames;
            this.ExpirationAfter = expirationAfter;
        }
        public void Process()
        {
            try
            {
                insertMsgDtls();
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        void insertMsgDtls()
        {
            string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
            SqlTransaction transaction = null;
            SqlConnection con;
            MSSqlClient.SqlConnectionOpen(out con, strConnectionString);
            try
            {
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        mFicientCommonProcess.SendNotification.SaveMgramMessage(this.Usernames, con, transaction, this.Category, this.Message, this.SendDatetime, this.ExpirationAfter);

                        transaction.Commit();
                    }
                }

            }
            catch (SqlException e)
            {
                this.StatusCode = -1000;
                if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusDescription = "There was error in connection";
                }
                this.StatusDescription = "Internal server error.";
            }
            catch (Exception e)
            {
                this.StatusCode = -1000;
                if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusDescription = "There was error in connection";
                }
                this.StatusDescription = "Internal server error.";
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }


        }
        string getSqlQuery()
        {

            return @"INSERT INTO [TBL_MGRAM_MSG]
                       ([MGRAM_MSG_ID],[MSG_ID],[ENTERPRISE_ID]
                       ,[PICK_UP_TIME],[CATEGORY]
                       ,[USERNAME],[SEND_DATETIME]
                       ,[STATUS],[MESSAGE]
                       ,[EXPIRATION_DATETIME],[SENT_DATETIME])
                 VALUES
                       (@MGRAM_MSG_ID,@MSG_ID,@ENTERPRISE_ID
                       ,@PICK_UP_TIME,@CATEGORY
                       ,@USERNAME,@SEND_DATETIME
                       ,@STATUS,@MESSAGE
                       ,@EXPIRATION_DATETIME,@SENT_DATETIME
                       )";
        }
        long getExpirationDateInTicks()
        {

            DateTime dtSendTime = DateTime.UtcNow;
            DateTime dtExpiryTime = DateTime.UtcNow;
            long lngSendTime = dtSendTime.Ticks;
            if (this.SendDatetime != 0)
            {
                dtSendTime = new DateTime(this.SendDatetime, DateTimeKind.Utc);
            }
            dtExpiryTime = dtSendTime.AddDays(this.ExpirationAfter);
            return dtExpiryTime.Ticks;
        }
        void savePushMsgOutboxDtls(string _enterpriseId, string _userName, string notificationType, string _devTokenID, string _devType, string _deviceID, SqlConnection con, SqlTransaction transaction)
        {
            SqlCommand cmd = new SqlCommand(getQuery(), con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@PUSH_NOTIFICATION_ID", Utilities.GetMd5Hash(_enterpriseId + DateTime.UtcNow.Ticks + _userName));
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", _enterpriseId);
            cmd.Parameters.AddWithValue("@BADGE_COUNT", 1);
            cmd.Parameters.AddWithValue("@USERNAME", _userName);
            cmd.Parameters.AddWithValue("@FROM_USER_FIRSTNAME", String.Empty);
            cmd.Parameters.AddWithValue("@SOURCE", 0);
            cmd.Parameters.AddWithValue("@NOTIFICATION_TYPE", notificationType);
            cmd.Parameters.AddWithValue("@PUSH_DATETIME", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@DEVICE_TOKEN_ID", _devTokenID);
            cmd.Parameters.AddWithValue("@DEVICE_TYPE", _devType);
            cmd.Parameters.AddWithValue("@DEVICE_ID", _deviceID);
            cmd.Parameters.AddWithValue("@INFO", "");
            int iRowsEffected = cmd.ExecuteNonQuery();
        }
        string getQuery()
        {
            return @"INSERT INTO [TBL_PUSHMESSAGE_OUTBOX]
           ([PUSH_NOTIFICATION_ID],[ENTERPRISE_ID]
           ,[BADGE_COUNT],[USERNAME]
           ,[FROM_USER_FIRSTNAME],[SOURCE]
           ,[NOTIFICATION_TYPE],[PUSH_DATETIME],
            [DEVICE_TOKEN_ID],[DEVICE_TYPE],[DEVICE_ID],[INFO])
     VALUES
           (@PUSH_NOTIFICATION_ID,@ENTERPRISE_ID
           ,@BADGE_COUNT,@USERNAME
           ,@FROM_USER_FIRSTNAME,@SOURCE
           ,@NOTIFICATION_TYPE,
           @PUSH_DATETIME
           ,@DEVICE_TOKEN_ID
           ,@DEVICE_TYPE,@DEVICE_ID,@INFO);";

        }
        #region Public properties
        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }
        public int ExpirationAfter
        {
            get { return _expirationAfter; }
            private set { _expirationAfter = value; }
        }
        public string Message
        {
            get { return _message; }
            private set { _message = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }

        public string Category
        {
            get { return _category; }
            private set { _category = value; }
        }
        public long SendDatetime
        {
            get { return _sendDatetime; }
            private set { _sendDatetime = value; }
        }

        public long PickUpTime
        {
            get { return _pickUpTime; }
            private set { _pickUpTime = value; }
        }


        public short Status
        {
            get { return _status; }
            private set { _status = value; }
        }


        public string EnterpriseId
        {
            get { return _enterpriseId; }
            set { _enterpriseId = value; }
        }
        public List<mFicientCommonProcess.mGarmUserdetails> Usernames
        {
            get { return _usernames; }
            private set { _usernames = value; }
        }
        

        #endregion
    }
}
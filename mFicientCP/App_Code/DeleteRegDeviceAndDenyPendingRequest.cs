﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Data.SqlClient;
//using System.Data;

//namespace mFicientCP
//{
//    public class DeleteRegDeviceAndDenyPendingRequest
//    {
//        //Delete Device: deviceId,deviceType,deviceModel,IMEI
//        //Pending Request:deviceId,deviceType,deviceModel,IMEI
//        public DeleteRegDeviceAndDenyPendingRequest(string companyId, string subAdminId, string userId, List<string> devicesToDelete, List<string> devicesToDeny)
//        {
//            this.CompanyId = companyId;
//            this.SubAdminId = subAdminId;
//            this.UserId = userId;
//            this.DevicesToDelete = devicesToDelete;
//            this.DevicesToDeny = devicesToDeny;
//        }

//        //public void Process()
//        //{
//        //    StatusCode =-1000;//For error
//        //    SqlTransaction sqlTransaction = null;
//        //    SqlConnection con = null;
//        //    int iRowsAffected = 0;
//        //    try
//        //    {
//        //        MSSqlClient.SqlConnectionOpen(out con);
//        //        using (con)
//        //        {
//        //            using (sqlTransaction = con.BeginTransaction())
//        //            {
//        //                //delete registered devices
//        //                foreach (string devicesDtls in DevicesToDelete)
//        //                {
//        //                    //Delete Device: deviceId,deviceType,deviceModel,IMEI
//        //                    string[] aryDeviceDtl = devicesDtls.Split(',');
//        //                    //logDeletedDeviceDataByDeviceIdAndType(sqlTransaction, con, aryDeviceDtl[0], aryDeviceDtl[1], aryDeviceDtl[2], (DEVICE_OR_ICON_SIZE)Enum.Parse(typeof(DEVICE_OR_ICON_SIZE), aryDeviceDtl[3]));
//        //                    iRowsAffected = deleteRegisteredDeviceOfUser(sqlTransaction, con, aryDeviceDtl[0], aryDeviceDtl[1], aryDeviceDtl[2], (DEVICE_OR_ICON_SIZE)Enum.Parse(typeof(DEVICE_OR_ICON_SIZE), aryDeviceDtl[3]));
//        //                }
//        //                //deny pending request
//        //                foreach (string devicesDtls in DevicesToDeny)
//        //                {
//        //                    //Pending Request:deviceId,deviceType,deviceModel,IMEI
//        //                    string[] aryDeviceDtl = devicesDtls.Split(',');
//        //                    denyUserRequest(sqlTransaction, con, aryDeviceDtl[0], aryDeviceDtl[1], aryDeviceDtl[2], (DEVICE_OR_ICON_SIZE)Enum.Parse(typeof(DEVICE_OR_ICON_SIZE), aryDeviceDtl[3]));

//        //                }

//        //                sqlTransaction.Commit();
//        //                StatusCode = 0;
//        //                StatusDescription = "";
//        //            }
//        //        }
//        //    }
//        //    catch (SqlException ex)
//        //    {
                
//        //        if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
//        //        {
//        //            StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
//        //            StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
//        //        }
//        //        StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
//        //        StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
//        //    }
//        //    catch (Exception ex)
//        //    {
                
//        //        if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
//        //        {
//        //            StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
//        //            StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
//        //        }
//        //        StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
//        //        StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
//        //    }
//        //    finally
//        //    {
//        //        if (con != null)
//        //        {
//        //            con.Dispose();
//        //        }
//        //        if (sqlTransaction != null)
//        //        {
//        //            sqlTransaction.Dispose();
//        //        }
//        //    }
            
//        //}
////        int logDeletedDeviceDataByDeviceIdAndType(SqlTransaction transaction, SqlConnection con, string deviceId, string deviceType, string deviceModel,DEVICE_OR_ICON_SIZE deviceSize)
////        {
////            string strSqlQuery = @"INSERT INTO TBL_DELETED_DEVICE(COMPANY_ID,USER_ID,DEVICE_TYPE,DEVICE_ID,DELETED_DATE,DEVICE_MODEL,DEVICE_SIZE)
////                                SELECT COMPANY_ID,USER_ID,DEVICE_TYPE,DEVICE_ID,@DateTimeNow,DEVICE_MODEL,DEVICE_SIZE
////                                FROM TBL_REGISTERED_DEVICE
////                                WHERE USER_ID = @UserId
////                                AND SUBADMIN_ID = @SubAdminId
////                                AND DEVICE_ID = @DeviceId
////                                AND DEVICE_TYPE = @DeviceType
////                                AND DEVICE_MODEL =@DeviceModel
////                                AND DEVICE_SIZE = @DEVICE_SIZE";

////            SqlCommand cmd = new SqlCommand(strSqlQuery, con, transaction);
////            cmd.CommandType = CommandType.Text;
////            cmd.Parameters.AddWithValue("@UserId", UserId);
////            cmd.Parameters.AddWithValue("@SubAdminId", SubAdminId);
////            cmd.Parameters.AddWithValue("@DeviceId", deviceId);
////            cmd.Parameters.AddWithValue("@DeviceType", deviceType);
////            cmd.Parameters.AddWithValue("@DateTimeNow", DateTime.UtcNow.Ticks);
////            cmd.Parameters.AddWithValue("@DeviceModel", deviceModel);
////            cmd.Parameters.AddWithValue("@DEVICE_SIZE", (int)deviceSize);

////            return cmd.ExecuteNonQuery();

////        }
//        int deleteRegisteredDeviceOfUser(SqlTransaction transaction, SqlConnection con, string deviceId, string deviceType, string deviceModel,DEVICE_OR_ICON_SIZE deviceSize)
//        {
//            string strQuery = @"DELETE FROM TBL_REGISTERED_DEVICE
//                                WHERE COMPANY_ID = @CompanyId
//                                AND USER_ID = @UserId
//                                AND SUBADMIN_ID = @SubAdminId
//                                AND DEVICE_ID = @DeviceId
//                                AND DEVICE_TYPE = @DeviceType
//                                AND DEVICE_MODEL = @DeviceModel
//                                AND DEVICE_SIZE = @DEVICE_SIZE";
//            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
//            cmd.CommandType = CommandType.Text;

//            cmd.Parameters.AddWithValue("@CompanyId", CompanyId);
//            cmd.Parameters.AddWithValue("@UserId", UserId);
//            cmd.Parameters.AddWithValue("@SubAdminId", SubAdminId);
//            cmd.Parameters.AddWithValue("@DeviceId", deviceId);
//            cmd.Parameters.AddWithValue("@DeviceType", deviceType);
//            cmd.Parameters.AddWithValue("@DeviceModel", deviceModel);
//            cmd.Parameters.AddWithValue("@DEVICE_SIZE", (int)deviceSize);
//            //execute the query

//            return cmd.ExecuteNonQuery();


//        }
//        void denyUserRequest(SqlTransaction transaction, SqlConnection con, string deviceId, string deviceType, string deviceModel, DEVICE_OR_ICON_SIZE deviceSize)
//        {
//            string strSqlQuery = @" UPDATE TBL_DEVICE_REGISTRATION_REQUEST
//                                    SET STATUS = 2,
//                                    STATUS_DATETIME = @StatusDate
//                                    WHERE COMPANY_ID = @CompanyId
//                                    AND USER_ID = @UserId
//                                    AND DEVICE_TYPE = @DeviceType
//                                    AND DEVICE_ID = @DeviceId
//                                    AND DEVICE_MODEL = @DeviceModel
//                                    AND DEVICE_SIZE = @DEVICE_SIZE";
//            StatusCode = -1000;
//            try
//            {
//                SqlCommand cmd = new SqlCommand(strSqlQuery, con, transaction);
//                cmd.CommandType = CommandType.Text;

//                cmd.Parameters.AddWithValue("@CompanyId", CompanyId);
//                cmd.Parameters.AddWithValue("@UserId", UserId);
//                cmd.Parameters.AddWithValue("@DeviceType", deviceType);
//                cmd.Parameters.AddWithValue("@DeviceId", deviceId);
//                cmd.Parameters.AddWithValue("@StatusDate", DateTime.UtcNow.Ticks);
//                cmd.Parameters.AddWithValue("@DeviceModel", deviceModel);
//                cmd.Parameters.AddWithValue("@DEVICE_SIZE", deviceSize);

//                //if (MSSqlClient.ExecuteNonQueryRecord(cmd) > 0)
//                //{
//                //    StatusCode = 0;
//                //}
//                cmd.ExecuteNonQuery();
//            }
//            catch (Exception ex)
//            {
//                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
//                {
//                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
//                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
//                }
//                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
//                StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
//            }
//        }
//        /// <summary>
//        /// Status code if 0 then process is success else some internal error
//        /// </summary>
//        public int StatusCode
//        {
//            set;
//            get;
//        }
//        public string StatusDescription
//        {
//            set;
//            get;
//        }
//        public string SubAdminId
//        {
//            set;
//            get;
//        }
//        public string CompanyId
//        {
//            set;
//            get;
//        }
//        public string UserId
//        {
//            get;
//            set;
//        }
//        public int RequestType
//        {
//            get;
//            set;
//        }
//        public List<string> DevicesToDelete
//        {
//            get;
//            set;
//        }
//        public List<string> DevicesToDeny
//        {
//            get;
//            set;
//        }
//    }
//}
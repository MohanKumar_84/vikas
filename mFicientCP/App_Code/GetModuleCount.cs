﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetModuleCount
    {

        public GetModuleCount(string adminId,string companyId)
        {
           this.AdminId = adminId;
           this.CompanyId = companyId;
        }

        public void Process()
        {
            try
            {
                string strQuery = @"SELECT  COUNT(*) as subAdminCount FROM TBL_SUB_ADMIN where COMPANY_ID =  @CompanyId;
                                   
                                    SELECT COUNT(Usr.USER_ID) AS USER_COUNT,COUNT(DEVICE_ID) AS DEVICE_COUNT
                                    FROM TBL_USER_DETAIL AS Usr LEFT OUTER JOIN TBL_REGISTERED_DEVICE AS regDev
                                    ON Usr.USER_ID = regDev.USER_ID WHERE Usr.COMPANY_ID = @CompanyId AND Usr.IS_ACTIVE = 1;
                    
                                    SELECT COUNT(*) AS WORK_FLOW_COUNT FROM TBL_WORKFLOW_DETAIL
                                    WHERE COMPANY_ID = @CompanyId;

                                    SELECT COUNT(DISTINCT MENU_CATEGORY_ID)  as MENU_CATEGORY_cnt from TBL_MENU_CATEGORY where COMPANY_ID= @CompanyId";


                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    ResultTables = objDataSet;
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

     
        public DataSet ResultTables
        {
            set;
            get;
        }
        
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string AdminId
        {
            get;
            set;
        }

        public string CompanyId
        {
            get;
            set;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class SaveActvDirSettings
    {
        #region Private members
        string _companyId, _mplugInId, _domainName;


        #endregion

        #region Public Properties
        public string DomainName
        {
            get { return _domainName; }
            private set { _domainName = value; }
        }

        public string MplugInId
        {
            get { return _mplugInId; }
            private set { _mplugInId = value; }
        }

        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }
        public string StatusDescription
        {
            get;
            private set;
        }
        public int StatusCode
        {
            get;
            private set;
        }
        #endregion

        #region Constructor

        public SaveActvDirSettings(string domainName, string mpluginId,
            string companyId)
        {
            _domainName = domainName;
            _mplugInId = mpluginId;
            _companyId = companyId;
        }
        #endregion

        #region Public Methods
        public void Process()
        {
            try
            {
                string strQuery = getQuery();
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@DOMAIN_ID",
                    Utilities.GetMd5Hash(DateTime.UtcNow.Ticks.ToString() + this.DomainName));
                cmd.Parameters.AddWithValue("@DOMAIN_NAME", this.DomainName);
                cmd.Parameters.AddWithValue("@MPLUGIN_ID", this.MplugInId);
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                int iRowsEffected = MSSqlClient.ExecuteNonQueryRecord(cmd);
                if (iRowsEffected == 0) throw new Exception("Internal server error");
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        #endregion

        #region Private Methods
        string getQuery()
        {
            string strQuery = @"INSERT INTO TBL_ENTERPRISE_DOMAINS
                                VALUES (@DOMAIN_ID,@DOMAIN_NAME,@MPLUGIN_ID,@COMPANY_ID)";
            return strQuery;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetWebserviceConnection
    {
        public GetWebserviceConnection()
        {

        }

        public GetWebserviceConnection(Boolean _IsSelectAll, string _SubAdminId, string _ConnectionId, string _ConnectionName, string _CompanyID)
        {
            this.IsSelectAll = _IsSelectAll;
            this.SubAdminId = _SubAdminId;
            this.ConnectionId = _ConnectionId;
            this.ConnectionName = _ConnectionName;
            this.CompanyId = _CompanyID;
        }
        public void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                if (this.IsSelectAll == true)
                {
                    query = @"SELECT P.COMPANY_ID,WEBSERVICE_TYPE,WEBSERVICE_URL,isnull(t.WS_OBJ,'') as WS_OBJ,P.WS_CONNECTOR_ID,
                    CASE WEBSERVICE_TYPE WHEN 'http' then 'rest' when 'rpc' then 'rpc' else  'wsdl' end  WEBSERVICE_TYPEALIES,
                    P.SUBADMIN_ID,RESPONSE_TYPE,CONNECTION_NAME,WSDL_URL,WSDL_CONTENT,P.USER_NAME,PASSWORD,AUTHENTICATION_TYPE,
                    CASE MPLUGIN_AGENT  WHEN '' THEN 'None' ELSE MPLUGIN_AGENT  END  AS  MPLUGIN_AGENT,
                    isnull(st.FULL_NAME,'') as FULL_NAME,isnull(CREATED_ON,0) as CREATED_ON,isnull(UPDATED_ON,0) as UPDATED_ON,isnull(ss.FULL_NAME,'') as  CreatedBY,CREDENTIAL_PROPERTY
                    FROM TBL_WEBSERVICE_CONNECTION AS P
                    left outer join(select ss.WS_CONNECTOR_ID,STUFF((select ',' + T.WS_COMMAND_NAME from TBL_WS_COMMAND  as T 
                    where ss.WS_CONNECTOR_ID=T.WS_CONNECTOR_ID for xml path('')),1,1,'') as WS_OBJ
                    from TBL_WS_COMMAND as ss group by ss.WS_CONNECTOR_ID)t 
                    ON t.WS_CONNECTOR_ID=P.WS_CONNECTOR_ID 
                    Left outer join dbo.TBL_SUB_ADMIN as ss on ss.SUBADMIN_ID=P.SUBADMIN_ID 
                    Left outer join dbo.TBL_SUB_ADMIN as st on P.CREATED_BY = st.SUBADMIN_ID WHERE P.COMPANY_ID=@COMPANY_ID ORDER BY CONNECTION_NAME ASC;";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                }
                else
                {
                    if (this.ConnectionName.Length == 0)
                    {
      
                        query = @"SELECT  * FROM  TBL_WEBSERVICE_CONNECTION WHERE  WS_CONNECTOR_ID=@CONNECTION_ID AND COMPANY_ID=@COMPANY_ID;";
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                     
                        objSqlCommand.Parameters.AddWithValue("@CONNECTION_ID", this.ConnectionId);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    }
                    else
                    {
            
                        query = @"SELECT  * FROM  TBL_WEBSERVICE_CONNECTION WHERE  CONNECTION_NAME=@CONNECTION_NAME AND COMPANY_ID=@COMPANY_ID";
                        
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                  
                        objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    }

                }
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
                if (this.ResultTable.Rows.Count > 0)
                    this.StatusCode = 0;

            }
            catch 
            {
                this.StatusCode = -1000;
            }
        }
        public void Process1()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                if (this.IsSelectAll == true)
                {
                    query = @"SELECT P.COMPANY_ID,WEBSERVICE_TYPE,WEBSERVICE_URL,isnull(t.WS_OBJ,'') as WS_OBJ,P.WS_CONNECTOR_ID,
                    CASE WEBSERVICE_TYPE WHEN 'http' then 'REST' when 'rpc' then 'RPC' else  'WSDL' end  WEBSERVICE_TYPEALIES,
                    P.SUBADMIN_ID,RESPONSE_TYPE,CONNECTION_NAME,WSDL_URL,WSDL_CONTENT,AUTHENTICATION_TYPE,
                    CASE MPLUGIN_AGENT  WHEN '' THEN 'None' ELSE MPLUGIN_AGENT  END  AS  MPLUGIN_AGENT,
                    isnull(st.FULL_NAME,'') as FULL_NAME,isnull(CREATED_ON,0) as CREATED_ON,isnull(UPDATED_ON,0) as UPDATED_ON,isnull(ss.FULL_NAME,'') as  CreatedBY,CREDENTIAL_PROPERTY
                    FROM TBL_WEBSERVICE_CONNECTION AS P
                    left outer join(select ss.WS_CONNECTOR_ID,STUFF((select ',' + T.WS_COMMAND_NAME from TBL_WS_COMMAND  as T 
                    where ss.WS_CONNECTOR_ID=T.WS_CONNECTOR_ID for xml path('')),1,1,'') as WS_OBJ
                    from TBL_WS_COMMAND as ss group by ss.WS_CONNECTOR_ID)t 
                    ON t.WS_CONNECTOR_ID=P.WS_CONNECTOR_ID 
                    Left outer join dbo.TBL_SUB_ADMIN as ss on ss.SUBADMIN_ID=P.SUBADMIN_ID 
                    Left outer join dbo.TBL_SUB_ADMIN as st on P.CREATED_BY = st.SUBADMIN_ID WHERE P.COMPANY_ID=@COMPANY_ID ORDER BY CONNECTION_NAME ASC;";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                }
                else
                {
                    if (this.ConnectionName.Length == 0)
                    {

                        query = @"SELECT  * FROM  TBL_WEBSERVICE_CONNECTION WHERE  WS_CONNECTOR_ID=@CONNECTION_ID AND COMPANY_ID=@COMPANY_ID;";
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;

                        objSqlCommand.Parameters.AddWithValue("@CONNECTION_ID", this.ConnectionId);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    }
                    else
                    {

                        query = @"SELECT  * FROM  TBL_WEBSERVICE_CONNECTION WHERE  CONNECTION_NAME=@CONNECTION_NAME AND COMPANY_ID=@COMPANY_ID";

                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;

                        objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    }

                }
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;

            }
            catch
            {
                this.StatusCode = -1000;
            }
        }


        public void Process2()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                    query = @"SELECT  * FROM  TBL_WEBSERVICE_CONNECTION WHERE  WS_CONNECTOR_ID=@CONNECTION_ID AND COMPANY_ID=@COMPANY_ID;";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;

                    objSqlCommand.Parameters.AddWithValue("@CONNECTION_ID", this.ConnectionName);
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
                if (this.ResultTable.Rows.Count > 0)
                    this.StatusCode = 0;

            }
            catch
            {
                this.StatusCode = -1000;
            }
        }



        public string SubAdminId
        {
            set;
            get;
        }
        public string ConnectionId
        {
            set;
            get;
        }
        public Boolean IsSelectAll
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public string ConnectionName { get; set; }

        public string CompanyId { get; set; }
    }
}
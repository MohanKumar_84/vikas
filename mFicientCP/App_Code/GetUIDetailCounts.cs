﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetUIDetailCounts
    {
        public GetUIDetailCounts(string subAdminId, string companyId)
        {
            this.CompanyId = companyId;
            this.SubAdminId = subAdminId;
        }
        public GetUIDetailCounts(string companyId)
        {
            this.CompanyId = companyId;
        }

        #region previous query
        //        string strQuery = @"SELECT COUNT(*) AS TOTAL
        //                                    FROM TBL_DATABASE_CONNECTION DbConn
        //                                    INNER JOIN TBL_SUB_ADMIN AS SbAdm
        //                                    ON DbConn.SUBADMIN_ID = SbAdm.SUBADMIN_ID
        //                                    INNER JOIN TBL_COMPANY_ADMINISTRATOR AS Adm
        //                                    ON SbAdm.ADMIN_ID = Adm.ADMIN_ID
        //                                    INNER JOIN TBL_COMPANY_DETAIL AS CompDtl
        //                                    ON Adm.ADMIN_ID = CompDtl.ADMIN_ID 
        //                                    WHERE COMPANY_ID=@CompanyId;
        //
        //
        //                                    SELECT COUNT(*) AS TOTAL
        //                                    FROM TBL_WEBSERVICE_CONNECTION DbConn
        //                                    INNER JOIN TBL_SUB_ADMIN AS SbAdm
        //                                    ON DbConn.SUBADMIN_ID = SbAdm.SUBADMIN_ID
        //                                    INNER JOIN TBL_COMPANY_ADMINISTRATOR AS Adm
        //                                    ON SbAdm.ADMIN_ID = Adm.ADMIN_ID
        //                                    INNER JOIN TBL_COMPANY_DETAIL AS CompDtl
        //                                    ON Adm.ADMIN_ID = CompDtl.ADMIN_ID 
        //                                    WHERE COMPANY_ID=@CompanyId;
        //                                    
        //
        //                                    SELECT COUNT(*) AS TOTAL FROM TBL_MASTER_PAGE_DETAILS AS MstPage
        //                                    INNER JOIN TBL_MASTER_PAGE_OWNER AS MstOnr
        //                                    ON MstPage.MASTER_PAGE_ID = MstOnr.MASTER_PAGE_ID
        //                                    WHERE MstOnr.COMPANY_ID =@CompanyId;
        //
        //                                    SELECT COUNT(*) AS TOTAL FROM TBL_FORM_DETAILS AS FrmDtls
        //                                    INNER JOIN TBL_FORM_OWNER AS FrmOnr
        //                                    ON FrmDtls.FORM_ID = FrmOnr.FORM_ID
        //                                    WHERE FrmOnr.COMPANY_ID =@CompanyId;
        //
        //
        //
        //                                    SELECT COUNT(*) AS TOTAL FROM TBL_REPORT_DETAILS AS RptDtls
        //                                    INNER JOIN TBL_REPORT_OWNER AS RptOnr
        //                                    ON RptDtls.REPORT_ID = RptOnr.REPORT_ID
        //                                    WHERE RptOnr.COMPANY_ID = @CompanyId;
        //
        //                                    SELECT COUNT(*) AS TOTAL FROM TBL_WORKFLOW_DETAIL AS WrkFlow
        //                                    INNER JOIN TBL_SUB_ADMIN AS SubAdm
        //                                    ON WrkFlow.SUBADMIN_ID = SubAdm.SUBADMIN_ID
        //                                    INNER JOIN TBL_COMPANY_ADMINISTRATOR AS Adm
        //                                    ON Adm.ADMIN_ID = SubAdm.ADMIN_ID
        //                                    INNER JOIN TBL_COMPANY_DETAIL AS CompDtl
        //                                    ON Adm.ADMIN_ID = CompDtl.ADMIN_ID
        //                                    WHERE CompDtl.COMPANY_ID = @CompanyId;
        //
        //                                    SELECT COUNT(*) FROM TBL_MENU_CATEGORY
        //                                    WHERE COMPANY_ID = @CompanyId";
        #endregion
        public void Process()
        {
            this.StatusCode = -1000;//for error
            try
            {
                string strQuery = @"SELECT COUNT(*) AS TOTAL FROM TBL_DATABASE_CONNECTION DbConn WHERE COMPANY_ID=@CompanyId;

                                    SELECT COUNT(*) AS TOTAL FROM TBL_WEBSERVICE_CONNECTION DbConn WHERE COMPANY_ID=@CompanyId;
                                    
                                    SELECT COUNT(*) AS TOTAL FROM (select distinct WrkFlow.WF_ID from TBL_WORKFLOW_DETAIL AS WrkFlow WHERE COMPANY_ID =  @CompanyId) as tbl;

                                    SELECT COUNT(*) AS TOTAL FROM TBL_MENU_CATEGORY WHERE COMPANY_ID = @CompanyId;

                                    SELECT COUNT(*) AS TOTAL FROM TBL_DATABASE_COMMAND AS DBComm WHERE COMPANY_ID =@CompanyId;

                                    SELECT COUNT(*) AS TOTAL FROM TBL_WS_COMMAND AS WSComm WHERE COMPANY_ID =@CompanyId;";


                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.Parameters.AddWithValue("@CompanyId", CompanyId);
                objSqlCommand.CommandType = CommandType.Text;
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null && objDataSet.Tables.Count > 0)
                {
                    ResultTables = objDataSet;
                    this.ApplicationCount = Convert.ToInt32(ResultTables.Tables[2].Rows[0]["TOTAL"]);
                    this.DatabaseConnectionCount = Convert.ToInt32(ResultTables.Tables[0].Rows[0]["TOTAL"]);
                    this.WebServiceConnectionCount = Convert.ToInt32(ResultTables.Tables[1].Rows[0]["TOTAL"]);
                    this.WebServiceCommandCount = Convert.ToInt32(ResultTables.Tables[5].Rows[0]["TOTAL"]);
                    this.DatabaseCommandCount = Convert.ToInt32(ResultTables.Tables[4].Rows[0]["TOTAL"]);
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Record not found";
            }
        }

        public DataSet ResultTables
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public int ApplicationCount
        {
            get;
            set;
        }
        public int DatabaseConnectionCount
        {
            get;
            set;
        }
        public int WebServiceConnectionCount
        {
            get;
            set;
        }
        public int WebServiceCommandCount
        {
            get;
            set;
        }
        public int DatabaseCommandCount
        {
            get;
            set;
        }
    }
}
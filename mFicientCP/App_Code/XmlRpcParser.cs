﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientCP
{
    [DataContract]
    public class XmlRpcParser
    {
        [DataMember]
        public List<param> param { get; set; }
    }
    [DataContract]
    public class param
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string typ { get; set; }
        [DataMember]
        public List<param> inparam { get; set; }
    }
}
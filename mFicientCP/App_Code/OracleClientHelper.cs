﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
using System.Configuration;
using Oracle.ManagedDataAccess.Client;

namespace mFicientCP
{
    public class OracleClientHelper
    {
        /// <summary>
        /// Description :Test connction ( Sql Query And Parameter )
        /// </summary>
        public static bool TestOracleConnection(string connectionString)
        {
            bool isConnected = false;
            OracleConnection Conn = null;
            try
            {
                Conn = new OracleConnection(connectionString);
            }
            catch
            {
                throw new Exception(@"Invalid Connection");
            }
            try
            {
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    if (Conn != null)
                    {
                        Conn.Dispose();
                        Conn = null;
                    }
                }
                else
                {
                    isConnected = true;
                    Conn.Close();
                }
            }
            catch
            {
                Conn = null;
            }
            return isConnected;
        }
        /// <summary>
        /// Description :Select Data According To MySqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static DataSet Select(OracleCommand _OracleCommand, string connectionString)
        {
            OracleConnection objOracleConnection = null;
            try
            {
                OpenConnection(out objOracleConnection, connectionString);
                _OracleCommand.Connection = objOracleConnection;

                DataSet ObjDataSet = new DataSet();

                OracleDataAdapter objOracleDataAdapter = new OracleDataAdapter(_OracleCommand);

                objOracleDataAdapter.Fill(ObjDataSet);

                //SqlConnectionClose(objSqlConnection);

                return ObjDataSet;
            }
            catch
            {
                return null;
            }
            finally
            {
                CloseConnection(objOracleConnection);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_SqlCommand"></param>
        /// <param name="con">pass a open connection</param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static DataSet SelectUsingTransaction(OracleCommand _OracleCommand, OracleConnection connection, OracleTransaction transaction)
        {
            _OracleCommand.Connection = connection;
            _OracleCommand.Transaction = transaction;
            DataSet ObjDataSet = new DataSet();

            OracleDataAdapter objOracleDataAdapter = new OracleDataAdapter(_OracleCommand);

            objOracleDataAdapter.Fill(ObjDataSet);

            return ObjDataSet;

        }
        /// <summary>
        /// Description :MySqlConnection Open Function
        /// </summary>
        public static void OpenConnection(out OracleConnection Conn, string connectionString)
        {
            try
            {
                Conn = new OracleConnection(connectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }

        /// <summary>
        /// Description :MySqlConnection Close Function
        /// </summary>
        public static void CloseConnection(OracleConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open))
                {
                    Conn.Close();
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Description :Data Process (Insert, Update, Delete ) According To MySqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static int ExecuteNonQuery(OracleCommand _OracleCommand, string connectionString)
        {
            OracleConnection objOracleConnection = null;
            OpenConnection(out objOracleConnection, connectionString);
            _OracleCommand.Connection = objOracleConnection;

            int i = _OracleCommand.ExecuteNonQuery();

            CloseConnection(objOracleConnection);

            return i;
        }

        public static int ExecuteNonQueryUsingTransaction(OracleCommand _OracleCommand, OracleConnection connection, OracleTransaction transaction)
        {
            _OracleCommand.Connection = connection;
            _OracleCommand.Transaction = transaction;
            return _OracleCommand.ExecuteNonQuery();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="metadataType"></param>
        /// <param name="connectionString"></param>
        /// <param name="tableName">If MetadataType is for a table</param>
        /// <returns>Dataset</returns>
        /// <exception cref="System.Exception">Thrown when there is some internal error</exception>
        public static DataSet getMetaDataByType(MetadataType metadataType
            , string connectionString,
            string tableName,
           out string errorMsg)
        {

            //            try
            //            {
            //                string strQuery = string.Empty;
            //                orac cmd = new SqlCommand();
            //                switch (metadataType)
            //                {
            //                    case MetadataType.TABLES:
            //                        strQuery = @"SELECT TABLE_SCHEMA,TABLE_NAME, OBJECTPROPERTY(object_id(TABLE_NAME), N'IsUserTable') 
            //                                     AS type FROM INFORMATION_SCHEMA.TABLES;";
            //                        break;

            //                    case MetadataType.COLUMNS:
            //                        strQuery = @" SELECT OBJECT_NAME(c.OBJECT_ID) table_name,
            //                                        c.name column_name,t.name column_type,c.is_nullable FROM
            //                                        sys.columns AS c JOIN
            //                                        sys.types AS t ON
            //                                        c.user_type_id=t.user_type_id JOIN
            //                                         sysobjects ON
            //                                        c.OBJECT_ID = sysobjects.id  WHERE
            //                                        sysobjects.xtype='U' AND
            //                                        OBJECT_NAME(c.OBJECT_ID)= @TABLE_NAME ORDER BY c.OBJECT_ID;";
            //                        break;

            //                    case MetadataType.ALL:
            //                        strQuery = @" SELECT TABLE_SCHEMA,TABLE_NAME, OBJECTPROPERTY(object_id(TABLE_NAME), N'IsUserTable') AS type FROM INFORMATION_SCHEMA.TABLES;
            //                                      SELECT OBJECT_NAME(c.OBJECT_ID) table_name, c.name column_name,t.name column_type,c.is_nullable FROM sys.columns AS c
            //                                    JOIN sys.types AS t ON c.user_type_id=t.user_type_id
            //                                      JOIN sysobjects ON c.OBJECT_ID = sysobjects.id 
            //                                    WHERE sysobjects.xtype='U' 
            //                                    ORDER BY c.OBJECT_ID;";
            //                        break;
            //                }

            //                cmd.CommandText = strQuery;
            //                cmd.CommandType = CommandType.Text;
            //                //Add Parameters
            //                switch (metadataType)
            //                {
            //                    case MetadataType.TABLES:
            //                        break;
            //                    case MetadataType.COLUMNS:
            //                        cmd.Parameters.AddWithValue("@TABLE_NAME", tableName);
            //                        break;
            //                    case MetadataType.ALL:
            //                        break;
            //                }
            //                return ExecuteSelectQuery(strQuery, connectionString, queryParameters);
            //            }
            //            catch (Exception ex)
            //            {
            //                throw ex;
            //            }








            //            DataSet dsMetaData = new DataSet();
            //            errorMsg = String.Empty;
            //            try
            //            {
            //                string strQuery = string.Empty;

            //                switch (metadataType)
            //                {
            //                    case MetadataType.TABLES:
            //                        strQuery = @"SELECT TABLE_SCHEMA,TABLE_NAME, OBJECTPROPERTY(object_id(TABLE_NAME), N'IsUserTable') AS type FROM INFORMATION_SCHEMA.TABLES;";
            //                        break;

            //                    case MetadataType.COLUMNS:
            //                        strQuery = @" SELECT OBJECT_NAME(c.OBJECT_ID) table_name, c.name column_name,t.name column_type,c.is_nullable FROM sys.columns AS c JOIN sys.types AS t ON c.user_type_id=t.user_type_id
            //                                      JOIN sysobjects ON c.OBJECT_ID = sysobjects.id WHERE sysobjects.xtype='U' AND OBJECT_NAME(c.OBJECT_ID)= @TABLE_NAME ORDER BY c.OBJECT_ID;";
            //                        break;

            //                    case MetadataType.ALL:
            //                        strQuery = @" SELECT TABLE_SCHEMA,TABLE_NAME, OBJECTPROPERTY(object_id(TABLE_NAME), N'IsUserTable') AS type FROM INFORMATION_SCHEMA.TABLES;
            //                                      SELECT OBJECT_NAME(c.OBJECT_ID) table_name, c.name column_name,t.name column_type,c.is_nullable FROM sys.columns AS c JOIN sys.types AS t ON c.user_type_id=t.user_type_id
            //                                      JOIN sysobjects ON c.OBJECT_ID = sysobjects.id WHERE sysobjects.xtype='U' ORDER BY c.OBJECT_ID;";
            //                        break;
            //                }

            //                dsMetaData = SelectDataFromDifferentConnection(connectionString, cmd);
            //                return dsMetaData;
            //            }
            //            catch (Exception ex)
            //            {
            //                dsMetaData = null;
            //                if (ex.Message.ToLower() == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString().ToLower())
            //                {
            //                    errorMsg = "There was a error in connection.";
            //                }
            //                else
            //                {
            //                    errorMsg = "Internal server error.";
            //                }
            //                return dsMetaData;
            //            }
            errorMsg = "Database not supported.";
            return null;
        }
    }
}
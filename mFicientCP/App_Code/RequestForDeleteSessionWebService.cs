﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientCP
{
    public class RequestForDeleteSessionWebService
    {
        [DataMember]
        public CPDeleteSessionReqJsonFields req { get; set; }
    }
    public class CPDeleteSessionReqJsonFields
    {
        /// <summary>
        /// request id
        /// </summary>
        [DataMember]
        public string rid { get; set; }

        /// <summary>
        /// data
        /// </summary>
        [DataMember]
        public CPDeleteSessionReqData data { get; set; }
    }
    public class CPDeleteSessionReqData
    {
        [DataMember]
        public string unm { get; set; }

        [DataMember]
        public string enid { get; set; }

        [DataMember]
        public List<DeviceDetail> devc { get; set; }
    }
    public class DeviceDetail
    {
        /// <summary>
        /// DeviceId
        /// </summary>
        [DataMember]
        public string did { get; set; }

        /// <summary>
        /// Device Type
        /// </summary>
        [DataMember]
        public string dtyp { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class DeleteGroupViaGroupId
    {

        public DeleteGroupViaGroupId(string _groupId, string _companyId)
        {
            this.GroupId = _groupId;
            this.CompanyID = _companyId;
        }

        public void Process()
        {
            this.StatusCode = -1000;
            try
            {
                SqlTransaction transaction = null;
                SqlConnection con;
                MSSqlClient.SqlConnectionOpen(out con);
                try
                {
                    using (con)
                    {
                        using (transaction = con.BeginTransaction())
                        {
                            DeleteWorkFlowGroupLinkTblFormDetail(this.GroupId, this.CompanyID, con, transaction);
                            DeleteTBLUSER_GROUPLINK(this.GroupId, this.CompanyID, con, transaction);
                            DeleteTBLUSER_GROUP(this.GroupId, this.CompanyID, con, transaction);
                           
                            transaction.Commit();

                            Utilities.saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.GROUP_REMOVED, this.CompanyID, this.SubAdminIdDeletingGrp, this.GroupId, this.GroupName, "", "", "", "", "", "");
                            this.StatusCode = 0;
                            this.StatusDescription = String.Empty;

                        }
                    }

                }
                catch (SqlException e)
                {
                    //transaction.Rollback();
                    if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                    }
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                catch (Exception e)
                {
                    //transaction.Rollback();
                    if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                    }
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                finally
                {
                    if (con != null)
                    {
                        con.Dispose();
                    }
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }

        }

        void DeleteWorkFlowGroupLinkTblFormDetail(string GroupId, string CompanyId, SqlConnection con, SqlTransaction transaction)
        {
            string strQuery = String.Empty;
             strQuery = @"DELETE FROM TBL_WORKFLOW_AND_GROUP_LINK  WHERE GROUP_ID=@GROUP_ID  and COMPANY_ID=@COMPANY_ID;";
           SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
               cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@GROUP_ID", this.GroupId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyID);
            cmd.ExecuteNonQuery();
        }

        void DeleteTBLUSER_GROUPLINK(string GroupId, string CompanyId, SqlConnection con, SqlTransaction transaction)
        {
            string strQuery = String.Empty;
            strQuery = @"Delete from  TBL_USER_GROUP_LINK WHERE GROUP_ID=@GROUP_ID  and COMPANY_ID=@COMPANY_ID;";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@GROUP_ID", this.GroupId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyID);
            cmd.ExecuteNonQuery();
        }

        void DeleteTBLUSER_GROUP( string GroupId, string CompanyId, SqlConnection con, SqlTransaction transaction)
        {
            string strQuery = String.Empty;
            strQuery = @"Delete from  TBL_USER_GROUP WHERE GROUP_ID=@GROUP_ID  and COMPANY_ID=@COMPANY_ID;";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@GROUP_ID", this.GroupId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyID);
            cmd.ExecuteNonQuery();
        }

        public string CompanyID
        {
            get;
            set;
        }
        public string GroupId
        {
            get;
            set;
        }
        public string GroupName
        {
            get;
            set;
        }
        public string SubAdminIdDeletingGrp
        {
            get;
            set;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.Caching;
using System.Web.SessionState;
namespace mFicientCP
{
    public class UserLogin
    {
        private string strSessionId, _browserWindowId;

        public UserLogin(Boolean _IsAdmin, string _CompanyId, string _Email, string _Password, string _DeviceTokenTypeCode, string _DeviceToken, string _HostName, string _IpAddress)
        {
            this.IsAdmin = _IsAdmin;
            this.CompanyId = _CompanyId;
            this.UserName = _Email;
            this.Password = _Password;
            this.DeviceTokenTypeCode = _DeviceTokenTypeCode;
            this.DeviceToken = _DeviceToken;
            this.HostName = _HostName;
            this.IpAddress = _IpAddress;
        }
        /// <summary>
        /// For renew Session
        /// </summary>
        /// <param name="_IsAdmin"></param>
        /// <param name="_CompanyId"></param>
        /// <param name="userId"></param>
        /// <param name="_Password"></param>
        /// <param name="cache"></param>
        public UserLogin(string companyId, string userId, string adminId, string password, string oldSessionId)
        {
            this.CompanyId = companyId;
            this.Password = password;
            this.UserId = userId;
            this.AdminId = adminId;
            this.OldSessionId = oldSessionId;
        }
       
        public void Process()
        {
            try
            {
                strSessionId = "";
                string strUserId, strAdminId = "", strEmailId,strCompanyId,strSubAdminName="",StrSubAdminUserName="" ;
                
                this.StatusCode = 0;
                Boolean blnIsAuthorizedUser = 
                    IsAuthorizedUser(out strUserId,
                    out strAdminId,
                    out strEmailId,
                    out strCompanyId, out strSubAdminName, out StrSubAdminUserName);

                this.UserId = strUserId;
                this.AdminId = strAdminId;
                this.UserEmail = strEmailId;
                this.DBCompanyId = strCompanyId;
                this.StrSubAdminName = strSubAdminName;
                this.StrSubAdminUserName = StrSubAdminUserName;
               

                if (blnIsAuthorizedUser)
                {
                    loginUser(string.Empty);
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception("Not an Authorised User");
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (((DATABASE_ERRORS)Enum.Parse(typeof(DATABASE_ERRORS), ex.Message)) == DATABASE_ERRORS.USER_ISNOT_ADMIN)
                    {
                        throw ex;
                    }
                }
                catch (ArgumentException)//if some other exception.
                {
                    StatusCode = -1000;
                    StatusDescription = "";
                }
                StatusCode = -1000;
                StatusDescription = "";
            }

        }

        public void RenewSession(string tabBrowserID)
        {
            try
            {
                if (isAuthorisedSubAdminForRenewSession(this.UserId, this.Password, this.CompanyId))
                {
                    loginUser(tabBrowserID);
                    CacheManager.Remove(CacheManager.GetKey(CacheManager.CacheType.mFicientIDE, this.CompanyId, this.UserId, MficientConstants.WEB_DEVICE_TYPE_AND_ID, MficientConstants.WEB_DEVICE_TYPE_AND_ID, this.OldSessionId, ""));
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception("User Not Authorised");
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "";
            }

        }
        void loginUser(string tabBrowserId)
        {
            string strCacheSessKey = "",strPrevCookieValue="";
            try
            {
                string strBrowserWindowId = Utilities.getBrowserWindowUniqueId();
                string strBrowserId=String.Empty;//we are using the browserid as the deviceId.
                bool isLoggedBefore = false;
                if (HttpContext.Current.Request.Cookies[MficientConstants.COOKIE_MSID] != null)
                {
                    HttpCookie reqCookie = HttpContext.Current.Request.Cookies.Get(MficientConstants.COOKIE_MSID);
                    if (!String.IsNullOrEmpty(reqCookie.Value) && !(reqCookie.Value.ToLower() == "null"))//after logout the value of the cookie stored earlier is set as "null" string.
                    {
                        /** New Changes
                         * 1 position is for browserid
                         * split value '|' if ary.count >2(which means more than one browser tab is open for the user) then same user is logging in again or some other user is trying to login
                         * if userId is same then using same browser but different window(decode the value and get userid)
                         * add the new Device Id to cookie value concat '| new strDeviceId'
                         * return the sessionId in the cookie
                         * else
                         * if user id does not match then same browser different user so have to remove the existing cookie
                         * set isDifferentUser = true and isLoggedBefore as false.
                        **/
                        strPrevCookieValue =HttpUtility.UrlDecode(Convert.ToString(reqCookie.Value));
                        string[] aryCookieValue = strPrevCookieValue.Split('|');
                        if (aryCookieValue.Length > 2)//change hfs|uniqueBrowserId|uniqueBrowserWindowId
                        {
                            //value stored as UserId;SessionId+"|"+strBrowserId+"|"+strDeviceId+("|"+strDeviceId(for different browser window))
                            string userIdFromCookie = Utilities.DecryptString(aryCookieValue[0]).Split(',')[0];
                            strBrowserId = aryCookieValue[1];
                            if(!String.IsNullOrEmpty(tabBrowserId))
                                strBrowserWindowId = tabBrowserId;
                            if (this.UserId == userIdFromCookie)
                            {
                                isLoggedBefore = true;
                                //previous value set above
                            }
                            else
                            {
                                isLoggedBefore = false;
                            }
                        }

                        
                    }
                    else
                    {
                        HttpCookie mfCookie = new HttpCookie(MficientConstants.COOKIE_MSID, "");
                        mfCookie.Expires = DateTime.UtcNow.AddSeconds(MficientConstants.SESSION_TIMEOUT_SECONDS);
                        HttpContext.Current.Response.Cookies.Add(mfCookie);
                    }
                }
                else
                {
                    /** New Changes
                     * Adding value to cookie
                     * Encrypt UserId;SessionId+"|"+"strBrowserId"+"|"+strDeviceId+("|"+strDeviceId(if different browser window))
                     * **/
                    HttpCookie mfCookie = new HttpCookie(MficientConstants.COOKIE_MSID, "");
                    mfCookie.Expires = DateTime.UtcNow.AddSeconds(MficientConstants.SESSION_TIMEOUT_SECONDS);
                    HttpContext.Current.Response.Cookies.Add(mfCookie);
                }

                if (String.IsNullOrEmpty(strBrowserId))
                {
                    strBrowserId = Utilities.getBrowserWindowUniqueId();
                }
                strCacheSessKey = CacheManager.GetKey(CacheManager.CacheType.mFicientSession, this.CompanyId, this.UserId
                                                      , strBrowserId, MficientConstants.WEB_DEVICE_TYPE_AND_ID, strSessionId, "");
                if (!isLoggedBefore)
                {
                    SessionUser objSession = new SessionUser(this.CompanyId, this.UserId, MficientConstants.WEB_DEVICE_TYPE_AND_ID
                                                            , strBrowserId, this.UserId, this.UserEmail);
                    strSessionId = objSession.SessionId;
                    CacheManager.Insert<SessionUser>(strCacheSessKey, objSession, DateTime.UtcNow, TimeSpan.FromSeconds(MficientConstants.SESSION_VALIDITY_SECONDS));
                    updateSaveLoginHistory(1, this.UserId, false);
                    /**
                     * Above the value of the cookie is not set.set It here
                     * Encrypt UserId;SessionId+"|"+"strBrowserId"+"|"+strDeviceId+("|"+strDeviceId(if different browser window))
                     * **/
                    addValueForResponseCookie(true,strBrowserId, strBrowserWindowId, strPrevCookieValue);
                    
                }
                else
                {
                    strSessionId = "";
                    SessionUser objSessionUser = CacheManager.Get<SessionUser>(strCacheSessKey);
                    if (objSessionUser != null && String.IsNullOrEmpty(strSessionId))
                    {
                        strSessionId = objSessionUser.SessionId;
                        addValueForResponseCookie(false,strBrowserId, strBrowserWindowId, strPrevCookieValue);
                    }
                    else//the cookie has not expired yet
                    {
                        SessionUser objSession = new SessionUser(this.CompanyId, this.UserId, MficientConstants.WEB_DEVICE_TYPE_AND_ID
                                                                , strBrowserId, this.UserId, this.UserEmail);
                        strSessionId = objSession.SessionId;
                        CacheManager.Insert<SessionUser>(strCacheSessKey, objSession, DateTime.UtcNow, TimeSpan.FromSeconds(MficientConstants.SESSION_VALIDITY_SECONDS));
                        updateSaveLoginHistory(1, this.UserId, false);

                        if (!String.IsNullOrEmpty(tabBrowserId))
                            addValueForResponseCookie(true,strBrowserId, strBrowserWindowId, strPrevCookieValue);
                    }

                }
                _browserWindowId = strBrowserWindowId;
            }
            catch
            {
                CacheManager.Remove(strCacheSessKey);
                saveLoginHistoryForUnsuccessfullLogin();
                throw new Exception();
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="addNew"></param>
        /// <param name="browserWindowId"></param>
        /// <param name="previousCookieValue">If not new then send the previous value</param>
        void addValueForResponseCookie(bool addNew,string browserId, string browserWindowId, string previousCookieValue)
        {
            //for admin encrypt string userId,sessionid,companyid,adminId;
            //for subAdmin encrypt string userId,sessionId,AdminId,companyId
            HttpCookie sessionCookie = new HttpCookie(MficientConstants.COOKIE_MSID);

            if (addNew)
            {
                if (this.UserId == this.AdminId)//is Admin
                {
                    sessionCookie.Value = HttpUtility.UrlEncode(Utilities.EncryptString(this.UserId + "," + strSessionId + "," + this.CompanyId + "," + this.AdminId) + "|" + browserId + "|" + browserWindowId);
           }
                else
                {
                    sessionCookie.Value = HttpUtility.UrlEncode(Utilities.EncryptString(this.UserId + "," + strSessionId + "," + this.AdminId + "," + this.CompanyId) + "|" + browserId + "|" + browserWindowId);
                }
            }
            else
            {
                sessionCookie.Value = HttpUtility.UrlEncode(previousCookieValue + "|" + browserWindowId);
            }

            sessionCookie.Expires = DateTime.Now.AddMinutes(30.0);
            HttpContext.Current.Response.Cookies.Add(sessionCookie);
        }
        
        /// <summary>

        /// Description :Check user is authorized 
        /// Created By: Rahul Chaturvedi
        /// Created On :23/01/2011
        /// </summary>
        private Boolean IsAuthorizedUser(out string _UserId,
            out string _AdminID,
            out string emailId,
            out string dbCompanyId,out string StrSubAdminName ,out string StrSubAdminUserName)
        {
            Boolean blnIsAuthorizeUser = false;
            _UserId = "";
            _AdminID = "";
            emailId = "";
            StrSubAdminName = "";
            StrSubAdminUserName = "";
            dbCompanyId = String.Empty;
            string strQuery;
            string strPassword = Utilities.GetMd5Hash(this.Password);
            DataSet objDataSet;
            if (this.IsAdmin == true)
            {
                strQuery = @"SELECT a.FIRST_NAME,a.LAST_NAME, a.EMAIL_ID, a.ADMIN_ID,c.COMPANY_ID FROM 
                        TBL_COMPANY_ADMINISTRATOR a inner join TBL_COMPANY_DETAIL c 
                        on a.ADMIN_ID=c.ADMIN_ID where a.EMAIL_ID=@EMAIL_ID AND 
                        a.ACCESS_CODE=@ACCESS_CODE AND c.COMPANY_ID=@COMPANY_ID;";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@EMAIL_ID", this.UserName.ToLower());
                objSqlCommand.Parameters.AddWithValue("@ACCESS_CODE", strPassword);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    _UserId = Convert.ToString(objDataSet.Tables[0].Rows[0]["ADMIN_ID"]);
                    _AdminID = _UserId;
                    
                    blnIsAuthorizeUser = true;
                    emailId = Convert.ToString(objDataSet.Tables[0].Rows[0]["EMAIL_ID"]);
                    dbCompanyId = Convert.ToString(objDataSet.Tables[0].Rows[0]["COMPANY_ID"]);
                    StrSubAdminName = Convert.ToString(objDataSet.Tables[0].Rows[0]["FIRST_NAME"]);
                    StrSubAdminUserName = Convert.ToString(objDataSet.Tables[0].Rows[0]["LAST_NAME"]);
                }
                else
                {                    
                    throw new Exception(((int)DATABASE_ERRORS.USER_ISNOT_ADMIN).ToString());
                }
            }
            else
            {
                strQuery = @"SELECT a.USER_NAME,a.FULL_NAME,a.EMAIL,SUBADMIN_ID,b.COMPANY_ID,a.ADMIN_ID FROM TBL_SUB_ADMIN a inner join 
                                TBL_COMPANY_DETAIL b on a.ADMIN_ID=b.ADMIN_ID WHERE USER_NAME=@USER_NAME 
                                AND ACCESS_CODE=@ACCESS_CODE  AND b.COMPANY_ID =@COMPANY_ID and a.ISACTIVE=1 and a.IS_BLOCKED='false';";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_NAME", this.UserName.ToLower());
                objSqlCommand.Parameters.AddWithValue("@ACCESS_CODE", strPassword);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    _UserId = Convert.ToString(objDataSet.Tables[0].Rows[0]["SUBADMIN_ID"]);
                    _AdminID = Convert.ToString(objDataSet.Tables[0].Rows[0]["ADMIN_ID"]);
                    blnIsAuthorizeUser = true;
                    emailId = Convert.ToString(objDataSet.Tables[0].Rows[0]["EMAIL"]);
                    dbCompanyId = Convert.ToString(objDataSet.Tables[0].Rows[0]["COMPANY_ID"]);
                    StrSubAdminName = Convert.ToString(objDataSet.Tables[0].Rows[0]["FULL_NAME"]);
                    StrSubAdminUserName = Convert.ToString(objDataSet.Tables[0].Rows[0]["USER_NAME"]);
                }

            }

            return blnIsAuthorizeUser;
        }

        private void updateSaveLoginHistory(int _LoginSuccess, string _UserId, Boolean _IsAlreadyLogin)
        {
            try
            {
                SqlTransaction sqlTransaction = null;
                SqlConnection con;
                MSSqlClient.SqlConnectionOpen(out con);
                try
                {
                    using (con)
                    {
                        using (sqlTransaction = con.BeginTransaction())
                        {
                            updateLoginHistory(con, sqlTransaction);
                            saveLoginHistory(con, sqlTransaction);
                            //commit
                            sqlTransaction.Commit();
                        }
                    }
                }
                //catch (SqlException ex)
                //{
                //    this.StatusCode = -1001;
                //}
                catch 
                {
                    this.StatusCode = -1001;
                }
                finally
                {
                    if (con != null)
                    {
                        con.Dispose();
                    }
                    if (sqlTransaction != null)
                    {
                        sqlTransaction.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void updateLoginHistory(SqlConnection con, SqlTransaction transaction)
        {
            string strQuery = @"UPDATE TBL_LOGIN_HISTORY
                                SET LOGOUT_DATETIME = @LogOutTime,
                                LOGOUT_TYPE = 'LOGGED AGAIN'
                                WHERE LOGIN_SUCCESS = 1
                                AND LOGOUT_DATETIME = 0
                                AND USER_ID = @USER_ID
                                AND COMPANY_ID = @COMPANY_ID;";

            SqlCommand objSqlCommand = new SqlCommand(strQuery, con, transaction);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@LogOutTime", DateTime.UtcNow.Ticks);

            objSqlCommand.ExecuteNonQuery();
        }

        void saveLoginHistory(SqlConnection con, SqlTransaction transaction)
        {
            string strQuery = @"INSERT INTO TBL_LOGIN_HISTORY(COMPANY_ID,USER_ID,LOGOUT_TYPE,LOGIN_DATETIME,LOGOUT_DATETIME,LOGIN_DEVICE_TOKEN,LOGIN_DEVICE_TYPE,LOGIN_SUCCESS,LOGIN_HOST_NAME,LOGIN_HOST_IP,LOGIN_DEVICE_TYPE_CODE)
                                VALUES(@COMPANY_ID,@USER_ID,@LOGOUT_TYPE,@LOGIN_DATETIME,@LOGOUT_DATETIME,@DEVICE_TOKEN,@LOGIN_DEVICE_TYPE,@LOGIN_SUCCESS,@LOGIN_HOST_NAME,@LOGIN_HOST_IP,@DEVICE_TYPE_CODE);";

            SqlCommand objSqlCommand = new SqlCommand(strQuery, con, transaction);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@LOGIN_DATETIME", DateTime.UtcNow.Ticks);
            objSqlCommand.Parameters.AddWithValue("@LOGOUT_DATETIME", 0);
            objSqlCommand.Parameters.AddWithValue("@LOGIN_DEVICE_TYPE", "Normal");
            objSqlCommand.Parameters.AddWithValue("@LOGOUT_TYPE", 0);
            objSqlCommand.Parameters.AddWithValue("@LOGIN_SUCCESS", 1);
            objSqlCommand.Parameters.AddWithValue("@LOGIN_HOST_NAME", this.HostName);
            objSqlCommand.Parameters.AddWithValue("@LOGIN_HOST_IP", this.IpAddress);
            objSqlCommand.Parameters.AddWithValue("@DEVICE_TOKEN", this.DeviceToken);
            objSqlCommand.Parameters.AddWithValue("@DEVICE_TYPE_CODE", this.DeviceTokenTypeCode);
            objSqlCommand.ExecuteNonQuery();
        }

        void saveLoginHistoryForUnsuccessfullLogin()
        {
            string strQuery = @"INSERT INTO TBL_LOGIN_HISTORY(COMPANY_ID,USER_ID,LOGOUT_TYPE,LOGIN_DATETIME,LOGOUT_DATETIME,LOGIN_DEVICE_TOKEN,LOGIN_DEVICE_TYPE,LOGIN_SUCCESS,LOGIN_HOST_NAME,LOGIN_HOST_IP,LOGIN_DEVICE_TYPE_CODE)
                                VALUES(@COMPANY_ID,@USER_ID,@LOGOUT_TYPE,@LOGIN_DATETIME,@LOGOUT_DATETIME,@DEVICE_TOKEN,@LOGIN_DEVICE_TYPE,@LOGIN_SUCCESS,@LOGIN_HOST_NAME,@LOGIN_HOST_IP,@DEVICE_TYPE_CODE);";

            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@LOGIN_DATETIME", DateTime.UtcNow.Ticks);
            objSqlCommand.Parameters.AddWithValue("@LOGOUT_DATETIME", 0);
            objSqlCommand.Parameters.AddWithValue("@LOGIN_DEVICE_TYPE", "Normal");
            objSqlCommand.Parameters.AddWithValue("@LOGOUT_TYPE", 0);
            objSqlCommand.Parameters.AddWithValue("@LOGIN_SUCCESS", 1);
            objSqlCommand.Parameters.AddWithValue("@LOGIN_HOST_NAME", this.HostName);
            objSqlCommand.Parameters.AddWithValue("@LOGIN_HOST_IP", this.IpAddress);
            objSqlCommand.Parameters.AddWithValue("@DEVICE_TOKEN", this.DeviceToken);
            objSqlCommand.Parameters.AddWithValue("@DEVICE_TYPE_CODE", this.DeviceTokenTypeCode);
            MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
        }
        
        private bool isAuthorisedSubAdminForRenewSession(string userId, string password, string companyId)
        {
            string strQuery = @"SELECT * FROM TBL_SUB_ADMIN
                                WHERE SUBADMIN_ID = @SubAdminId
                                AND ACCESS_CODE = @AccessCode
                                AND COMPANY_ID = @CompanyId";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@SubAdminId", userId);
            cmd.Parameters.AddWithValue("@AccessCode", password);
            cmd.Parameters.AddWithValue("@CompanyId", companyId);
            DataSet dsSubAdminDetails = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (dsSubAdminDetails != null && dsSubAdminDetails.Tables[0] != null && dsSubAdminDetails.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean IsAdmin
        {
            get;
            set;
        }
        public string AdminId
        {
            get;
            set;
        }
        public string CompanyId
        {
            get;
            set;
        }
        public string UserName
        {
            get;
            set;
        }
        public string Password
        {
            get;
            set;
        }
        public string UserId
        {
            get;
            set;
        }
        public string DeviceTokenTypeCode
        {
            get;
            set;
        }
        public string DeviceToken
        {
            get;
            set;
        }
        public string HostName
        {
            get;
            set;
        }
        public string IpAddress
        {
            get;
            set;
        }
        public string SessionId
        {
            get
            {
                return strSessionId;
            }
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string OldSessionId
        {
            set;
            get;
        }
        public string UserEmail
        {
            get;
            set;
        }
        public string BrowserWindowId
        {
            get { return _browserWindowId; }
        }
        /// <summary>
        /// No check is applied for letter casing in the companyID.
        /// This gives the CompanyId that is stored in the Database.
        /// </summary>
        public string DBCompanyId
        {
            get;
            private set;
        }


        public string StrSubAdminName
        {
            get;
            private set;
        }

        public string StrSubAdminUserName
        {
            get;
            private set;
        }
    }
}
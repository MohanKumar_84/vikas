﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
namespace mFicientCP
{
    public class UpdateCompanyLogo
    {

        public UpdateCompanyLogo()
        { }

        public UpdateCompanyLogo(string companyId, string logoName)
        {
            CompanyId = companyId;
            LogoName = logoName;
        }

        public void Process()
        {
            SqlTransaction sqlTransaction = null;
            SqlConnection con = null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);

                using (con)
                {
                    using (sqlTransaction = con.BeginTransaction())
                    {
                        updateCompanyLogo(con, sqlTransaction);
                        int status = deletePreviousUpdateReqRecords(this.CompanyId, sqlTransaction, con);
                        if (status == -1000)
                        {
                            throw new Exception();
                        }
                        else
                        {
                            status = saveUpdateRequiredInfo(this.CompanyId, sqlTransaction, con);
                            if (status == -1000)
                            {
                                throw new Exception();
                            }
                        }
                        sqlTransaction.Commit();
                        StatusCode = 0;
                        StatusDescription = "";
                    }
                }
            }
            catch (SqlException ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (sqlTransaction != null)
                {
                    sqlTransaction.Dispose();
                }
            }
        }
        public static int saveUpdateRequiredInfo(string companyId, SqlTransaction transaction, SqlConnection con)
        {
            //int StatusCode = -1000,for error.
            try
            {
                SqlCommand cmd = new SqlCommand(@"INSERT INTO TBL_UPDATION_REQUIRED_DETAIL(COMPANY_ID,USER_ID,DEVICE_ID,DEVICE_TYPE,UPDATED_ON,UPDATION_TYPE)
                                                (
                                                SELECT usr.COMPANY_ID,usr.USER_ID,regDev.DEVICE_ID,regDev.DEVICE_TYPE,@UpdatedOn,@UpdateType FROM TBL_USER_DETAIL AS usr
                                                INNER JOIN TBL_REGISTERED_DEVICE AS regDev
                                                ON usr.USER_ID = regDEV.USER_ID
                                                WHERE usr.COMPANY_ID = @CompanyId
                                                );", con, transaction);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@UpdatedOn", DateTime.UtcNow.Ticks);
                cmd.Parameters.AddWithValue("@UpdateType", UPDATE_TYPE.COMPANY_LOGO_UPDATE);
                cmd.Parameters.AddWithValue("@CompanyId", companyId);
                return cmd.ExecuteNonQuery();

            }
            catch
            {
                return -1000;
            }
        }

        int deletePreviousUpdateReqRecords(string companyId, SqlTransaction transaction, SqlConnection con)
        {
            //int StatusCode = -1000,for error.
            try
            {
                SqlCommand cmd = new SqlCommand(@"DELETE FROM TBL_UPDATION_REQUIRED_DETAIL WHERE COMPANY_ID = @CompanyId AND UPDATION_TYPE=@UpdateType", con, transaction);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@UpdateType", UPDATE_TYPE.COMPANY_LOGO_UPDATE);
                cmd.Parameters.AddWithValue("@CompanyId", companyId);
                return cmd.ExecuteNonQuery();
            }
            catch
            {
                return -1000;
            }
        }

        void updateCompanyLogo(SqlConnection con, SqlTransaction transaction)
        {
            
            string query = @"UPDATE TBL_COMPANY_DETAIL 
                                 SET LOGO_IMAGE_NAME = @ImageName
                                  WHERE COMPANY_ID=@COMPANY_ID ";

            SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);

            objSqlCommand.CommandType = CommandType.Text;

            objSqlCommand.Parameters.AddWithValue("@ImageName", LogoName);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", CompanyId);

            objSqlCommand.ExecuteNonQuery();
            
        }

        public string CompanyId
        {
            get;
            set;
        }

        public string LogoName
        {
            get;
            set;
        }

        public int StatusCode
        {
            get;
            set;
        }

        public string StatusDescription
        {
            get;
            set;
        }
    }
}
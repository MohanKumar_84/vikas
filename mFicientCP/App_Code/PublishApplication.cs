﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;
using System.Runtime.Serialization;
using System.IO;
using System.Text;

namespace mFicientCP
{
    public class PublishApplication
    {
        public PublishApplication()
        {
        }

        public PublishApplication(string _WfId, string _SubAdminId, string _PrvVersion, string _CurrentVersion, string Company_ID)
        {
            this.WfId = _WfId;
            this.SubAdminId = _SubAdminId;
            this.CompanyId = Company_ID;
            this.PrvVersion = _PrvVersion;
            this.CurrentVersion = _CurrentVersion;
            //this.CurrentVersionDetails = _CurrentVersionDetails;
        }
        public void Process()
        {
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            SqlCommand objSqlCommand;
            string query;
            string wf_name = "";
            try
            {
                StatusCode = -1000;
                query = @"Select * from dbo.TBL_WORKFLOW_DETAIL where WF_ID=@WF_ID and COMPANY_ID=@COMPANY_ID";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WfId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                DataSet ds = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (ds != null && ds.Tables[0].Rows.Count > 0) wf_name = Convert.ToString(ds.Tables[0].Rows[0]["WF_NAME"]);
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);

                objSqlTransaction = objSqlConnection.BeginTransaction();

                query = @"DELETE FROM TBL_CURR_WORKFLOW_DETAIL WHERE WF_ID=@WF_ID  and COMPANY_ID=@COMPANY_ID;";
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WfId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

                if (objSqlCommand.ExecuteNonQuery() > 0)
                    this.StatusCode = 0;

                this.PublishOn = DateTime.UtcNow.Ticks;

                if (this.CurrentVersion != "-1")
                {
                    query = @"INSERT INTO TBL_CURR_WORKFLOW_DETAIL(COMPANY_ID,WF_ID,WF_NAME,WF_DESCRIPTION,WF_JSON,VERSION,WORKED_BY,PUBLISH_ON,SUBADMIN_ID,FORMS,PARENT_ID
                    ,VERSION_HISTORY, MODEL_TYPE,COMMAND_IDS,APP_JS_JSON,ICON,APP_TYPE)
                    SELECT COMPANY_ID,WF_ID,WF_NAME,WF_DESCRIPTION,WF_JSON,VERSION,WORKED_BY,@PUBLISH_ON,SUBADMIN_ID,FORMS,PARENT_ID,
                    VERSION_HISTORY,MODEL_TYPE,'' as COMMAND_IDS,APP_JS_JSON,ICON,APP_TYPE FROM TBL_PRV_WORKFLOW_DETAIL pw WHERE pw.WF_ID=@WF_ID AND pw.VERSION=@VERSION AND COMPANY_ID=@COMPANY_ID;";

                    objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WfId);
                    objSqlCommand.Parameters.AddWithValue("@VERSION", this.CurrentVersion);
                    objSqlCommand.Parameters.AddWithValue("@PUBLISH_ON", this.PublishOn);
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

                    if (objSqlCommand.ExecuteNonQuery() > 0)
                        this.StatusCode = 0;
                    else
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                if (this.PrvVersion != "-1")
                {
                    UpdatePublishOnInPrvTable(objSqlCommand, objSqlConnection, objSqlTransaction, 0);
                }
                if (this.CurrentVersion != "-1")
                {
                    UpdatePrvVersionTable(objSqlCommand, objSqlConnection, objSqlTransaction, this.CurrentVersion, this.PublishOn);
                }

                this.StatusCode = 0;
                objSqlTransaction.Commit();

                Utilities.saveActivityLog(objSqlConnection, mFicientCommonProcess.ACTIVITYENUM.APP_PUBLISH, this.CompanyId, this.SubAdminId, WfId, wf_name, this.CurrentVersion, "", "", "", "", "");

                AddIntoUpdationRequiredForMenu objUpdation = new AddIntoUpdationRequiredForMenu();
                objUpdation.AppFlowUpdation(WfId, CompanyId, null);
                try
                {
                    UpdateObjectXreference obj = new UpdateObjectXreference();
                    obj.UpdateObjectXreferenceForPublishApp(this.CompanyId, this.WfId);
                }
                catch { }
            }
            catch
            {
                this.StatusCode = -1000;
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }
        private void UpdatePublishOnInPrvTable(SqlCommand _SqlCommand, SqlConnection _SqlConnection, SqlTransaction _SqlTransaction, Int64 _PublishOn)
        {
            string query = @"UPDATE TBL_PRV_WORKFLOW_DETAIL SET PUBLISH_ON=@PUBLISH_ON WHERE WF_ID=@WF_ID AND COMPANY_ID=@COMPANY_ID;";
            _SqlCommand = new SqlCommand(query, _SqlConnection, _SqlTransaction);
            _SqlCommand.CommandType = CommandType.Text;
            _SqlCommand.Parameters.AddWithValue("@WF_ID", this.WfId);
            //_SqlCommand.Parameters.AddWithValue("@VERSION", _Version);
            _SqlCommand.Parameters.AddWithValue("@PUBLISH_ON", _PublishOn);
            _SqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

            if (_SqlCommand.ExecuteNonQuery() > 0)
                this.StatusCode = 0;
        }
        private void UpdatePrvVersionTable(SqlCommand _SqlCommand, SqlConnection _SqlConnection, SqlTransaction _SqlTransaction, string _Version, Int64 _PublishOn)
        {
            string query = @"UPDATE TBL_PRV_WORKFLOW_DETAIL SET PUBLISH_ON=@PUBLISH_ON WHERE WF_ID=@WF_ID AND VERSION=@VERSION AND COMPANY_ID=@COMPANY_ID;";
            _SqlCommand = new SqlCommand(query, _SqlConnection, _SqlTransaction);
            _SqlCommand.CommandType = CommandType.Text;
            _SqlCommand.Parameters.AddWithValue("@WF_ID", this.WfId);
            _SqlCommand.Parameters.AddWithValue("@VERSION", _Version);
            _SqlCommand.Parameters.AddWithValue("@PUBLISH_ON", _PublishOn);
            _SqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

            if (_SqlCommand.ExecuteNonQuery() > 0)
                this.StatusCode = 0;
        }

        public string UserId
        {
            set;
            get;
        }

        public int StatusCode { get; set; }

        public string WfId { get; set; }

        public string PrvVersion { get; set; }

        public string CurrentVersion { get; set; }

        public DataRow CurrentVersionDetails { get; set; }

        public string SubAdminId { get; set; }

        public string CompanyId { get; set; }

        public long PublishOn { get; set; }
    }

}
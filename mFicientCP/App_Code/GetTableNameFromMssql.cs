﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetTableNameFromMssql
    {
       public GetTableNameFromMssql()
        { 

        }

       public GetTableNameFromMssql(string _ConnectionString)
        {
            this.ConnectionString = _ConnectionString;
            //this.IsSelectAll = _IsSelectAll;
            //this.SubAdminId = _SubAdminId;
            //this.ConnectionId = _ConnectionId;
            //this.ConnectionName = ConnectionName;
        }
        public void Process()
        {
            this.StatusCode = -1000;
            SqlConnection conn = new SqlConnection();
            try
            {
                conn = new SqlConnection(this.ConnectionString);
                conn.Open();
                string query = @"SELECT TABLE_SCHEMA,TABLE_NAME, OBJECTPROPERTY(object_id(TABLE_NAME), N'IsUserTable') AS type 
                                         FROM INFORMATION_SCHEMA.TABLES;";
                SqlCommand objSqlCommand = new SqlCommand(query, conn);
                DataSet ObjDataSet = new DataSet();
                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(ObjDataSet);
                this.ResultTable = ObjDataSet.Tables[0];
                conn.Close();
                this.StatusCode = 0;
            }
            catch 
            {
                this.StatusCode = -1000;
                conn.Close();
            }
        }

        public string ConnectionString
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
      
        public int StatusCode
        {
            set;
            get;
        }
    }
}
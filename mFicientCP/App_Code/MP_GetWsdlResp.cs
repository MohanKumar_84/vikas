﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace mFicientCP
{
    public class MP_GetWsdlResp
    {
        string _data;
        public string Data
        {
            get { return _data; }
        }
        public int StatusCode
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public MP_GetWsdlResp(string json)
        {
            this.StatusCode = -1000;
            try
            {
                MPGetWSDLInformationResponse objWsdlResp = DeserialiseJson<MPGetWSDLInformationResponse>(json);
                StatusCode = Convert.ToInt32(objWsdlResp.resp.cd);
                StatusDescription = objWsdlResp.resp.desc;
                _data = objWsdlResp.resp.data;
            }
            catch
            {
            }
        }

        private T DeserialiseJson<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer serialiser = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serialiser.ReadObject(ms);
            ms.Close();
            return obj;
        }
    }
    [DataContract]
    public class MPGetWSDLInformationResponse
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public MPGetWSDLDataRespFields resp { get; set; }
    }
    public class MPGetWSDLDataRespFields
    {

        [DataMember]
        public string cd { get; set; }

        [DataMember]
        public string desc { get; set; }

        [DataMember]
        public string data { get; set; }
    }
}
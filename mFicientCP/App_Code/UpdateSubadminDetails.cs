﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class UpdateSubadminDetails
    {
        public UpdateSubadminDetails()
        { 

        }
        public UpdateSubadminDetails(string _SubAdminId, string _AdminId, string _FullName, string _UserName, string _AccessCode, string _Email,
                            string _DivisionId, string _RoleId,byte _PushMessage,string companyId,string user)
        {
            this.SubAdminId = _SubAdminId;
            this.AdminId = _AdminId;
            this.FullName = _FullName;
            this.UserName = _UserName;
            this.AccessCode = _AccessCode;
            this.Email = _Email;
            this.DivisionId = _DivisionId;
            this.RoleId = _RoleId;
            this.PushMessage = _PushMessage;
            this.CompanyId = companyId;
            this.User = user;
        }
        
        public void Process()
        {
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                string query = @"UPDATE TBL_SUB_ADMIN SET 
                                        FULL_NAME=@FULL_NAME,
                                        PUSH_MESSAGE = @PUSH_MESSAGE,
                                         MOBILE_USER=@MOBILE_USER
                                        WHERE SUBADMIN_ID =@SUBADMIN_ID
                                            AND  ADMIN_ID=@ADMIN_ID;";
                objSqlTransaction = objSqlConnection.BeginTransaction();
                string strPassword = Utilities.GetMd5Hash(this.AccessCode);
                SqlCommand objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@PUSH_MESSAGE", this.PushMessage);
                objSqlCommand.Parameters.AddWithValue("@FULL_NAME", this.FullName);
                objSqlCommand.Parameters.AddWithValue("@MOBILE_USER", this.User);
                
                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                query = @"DELETE FROM TBL_SUBADMIN_ROLE WHERE SUBADMIN_ID=@SUBADMIN_ID AND ADMIN_ID=@ADMIN_ID;";
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                objSqlCommand.ExecuteNonQuery();
              

                string[] strArrayRoleId = this.RoleId.Split('&');
                foreach (string str in strArrayRoleId)
                {
                    if (str.Length != 0)
                    {
                        query = @"INSERT INTO TBL_SUBADMIN_ROLE(SUBADMIN_ID,ROLE_ID,ADMIN_ID,COMPANY_ID) values(@SUBADMIN_ID,@ROLE_ID,@ADMIN_ID,@COMPANY_ID);";
                        objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                        objSqlCommand.Parameters.AddWithValue("@ROLE_ID", str);
                        objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                        if (objSqlCommand.ExecuteNonQuery() > 0)
                        {
                            this.StatusCode = 0;
                            this.StatusDescription = "";
                        }
                        else
                        {
                            throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                        }
                    }
                }
                query = @"DELETE FROM TBL_SUBADMIN_DIVISION_DEP WHERE SUBADMIN_ID=@SUBADMIN_ID;";
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.ExecuteNonQuery();

                string[] strArrayDivDepId = this.DivisionId.Split('&');
                foreach (string str in strArrayDivDepId)
                {
                    if (str.Length != 0)
                    {
                        string strDivId = str.Split('@')[1];
                        string strRelatedDepId = str.Split('@')[0];
                        string[] strArrayDepId = strRelatedDepId.Split('#');
                        foreach (string strDepId in strArrayDepId)
                        {
                            if (strDepId.Length != 0)
                            {
                                query = @"INSERT INTO TBL_SUBADMIN_DIVISION_DEP(SUBADMIN_ID,DIVISION_ID,DEPARTMENT_ID,COMPANY_ID) values(@SUBADMIN_ID,@DIVISION_ID,@DEPARTMENT_ID,@COMPANY_ID);";

                                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                                objSqlCommand.CommandType = CommandType.Text;
                                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                                objSqlCommand.Parameters.AddWithValue("@DIVISION_ID", strDivId);
                                objSqlCommand.Parameters.AddWithValue("@DEPARTMENT_ID", strDepId);
                                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                                if (objSqlCommand.ExecuteNonQuery() > 0)
                                {
                                    this.StatusCode = 0;
                                    this.StatusDescription = "";
                                }
                                else
                                {
                                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                                }
                            }
                        }
                    }
                }
                objSqlTransaction.Commit();
            }
            catch (Exception ex)
            {
                objSqlTransaction.Rollback();
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                this.StatusCode = -1000;
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }
        public string AdminId
        {
            get;
            set;
        }
        public string SubAdminId
        {
            get;
            set;
        }
        public string FullName
        {
            get;
            set;
        }
        public string UserName
        {
            get;
            set;
        }
        public string AccessCode
        {
            get;
            set;
        }
        public string Email
        {
            get;
            set;
        }
        public string DivisionId
        {
            get;
            set;
        }
        public string RoleId
        {
            get;
            set;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public byte PushMessage
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string User
        {
            set;
            get;
        }



    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
namespace mFicientCP
{
    public class UpdateMgrmEnterpriseGroupsByGrpId
    {
        public UpdateMgrmEnterpriseGroupsByGrpId()
        {

        }
        public void Process(MFEMgramEnterpriseGroups mGramEnterpriseGrps)
        {
            updateGroupDtls(mGramEnterpriseGrps);
        }
        public void Process(SqlConnection con, SqlTransaction transaction, MFEMgramEnterpriseGroups mGramEnterpriseGrps)
        {
            updateGroupDtls(con, transaction, mGramEnterpriseGrps);
        }
        void updateGroupDtls(MFEMgramEnterpriseGroups mGramEnterpriseGrps)
        {
            string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
            string strQuery = getQuery();
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", mGramEnterpriseGrps.EnterpriseId);
            cmd.Parameters.AddWithValue("@GROUP_ID", mGramEnterpriseGrps.GroupId);
            cmd.Parameters.AddWithValue("@GROUP_NAME", mGramEnterpriseGrps.GroupName);
            cmd.Parameters.AddWithValue("@USERS", mGramEnterpriseGrps.Users);

            int iRowsEffected = MSSqlClient.ExecuteNonQueryRecord(cmd, strConnectionString);
            if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
        }
        void updateGroupDtls(SqlConnection con,
            SqlTransaction transaction,
            MFEMgramEnterpriseGroups mGramEnterpriseGrps)
        {
            string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
            string strQuery = getQuery();
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", mGramEnterpriseGrps.EnterpriseId);
            cmd.Parameters.AddWithValue("@GROUP_ID", mGramEnterpriseGrps.GroupId);
            cmd.Parameters.AddWithValue("@GROUP_NAME", mGramEnterpriseGrps.GroupName);
            cmd.Parameters.AddWithValue("@USERS", mGramEnterpriseGrps.Users);

            int iRowsEffected = cmd.ExecuteNonQuery();
            if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
        }


        public void Process(List<MFEMgramEnterpriseGroups> mGramEnterpriseGrps)
        {
            updateGroupDtls(mGramEnterpriseGrps);
        }
        public void Process(SqlConnection con, SqlTransaction transaction, List<MFEMgramEnterpriseGroups> mGramEnterpriseGrps)
        {
            updateGroupDtls(con, transaction, mGramEnterpriseGrps);
        }
        void updateGroupDtls(List<MFEMgramEnterpriseGroups> mGramEnterpriseGrps)
        {
            string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
            string strQuery = getQuery();
            foreach (MFEMgramEnterpriseGroups mgramEnterpriseGrp in mGramEnterpriseGrps)
            {
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ENTERPRISE_ID", mgramEnterpriseGrp.EnterpriseId);
                cmd.Parameters.AddWithValue("@GROUP_ID", mgramEnterpriseGrp.GroupId);
                cmd.Parameters.AddWithValue("@GROUP_NAME", mgramEnterpriseGrp.GroupName);
                cmd.Parameters.AddWithValue("@USERS", mgramEnterpriseGrp.Users);

                int iRowsEffected = MSSqlClient.ExecuteNonQueryRecord(cmd, strConnectionString);
                if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
        }
        void updateGroupDtls(SqlConnection con,
            SqlTransaction transaction,
            List<MFEMgramEnterpriseGroups> mGramEnterpriseGrps)
        {
            string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
            string strQuery = getQuery();
            foreach (MFEMgramEnterpriseGroups mgramEnterpriseGrp in mGramEnterpriseGrps)
            {
                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ENTERPRISE_ID", mgramEnterpriseGrp.EnterpriseId);
                cmd.Parameters.AddWithValue("@GROUP_ID", mgramEnterpriseGrp.GroupId);
                cmd.Parameters.AddWithValue("@GROUP_NAME", mgramEnterpriseGrp.GroupName);
                cmd.Parameters.AddWithValue("@USERS", mgramEnterpriseGrp.Users);

                int iRowsEffected = cmd.ExecuteNonQuery();
                if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
        }



        string getQuery()
        {
            return @"UPDATE TBL_ENTERPRISE_GROUPS
                    SET GROUP_NAME = @GROUP_NAME,
                    USERS = @USERS
                    WHERE ENTERPRISE_ID = @ENTERPRISE_ID
                    AND GROUP_ID = @GROUP_ID";

        }
        #region Public Properties
        //public MFEMgramEnterpriseGroups MgramEnterpriseGroups
        //{
        //    get;
        //    private set;
        //}
        #endregion
    }
}
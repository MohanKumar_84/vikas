﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class DeleteDbCommand
    {
       public DeleteDbCommand()
        { 

        }

       public DeleteDbCommand( string _CommandId, string _SubAdminId, string _CompanyId)
        {

            this.CommandId = _CommandId;
            this.SubAdminId = _SubAdminId;
            this.CompanyId = _CompanyId;
        }
        public void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query = @"DELETE FROM TBL_DATABASE_COMMAND WHERE DB_COMMAND_ID=@DB_COMMAND_ID AND COMPANY_ID=@COMPANY_ID";
                
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_ID", this.CommandId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
            }
            catch 
            {
                this.StatusCode = -1000;
            }
        }
        

        public string CommandId { get; set; }
        public string ConnactorId { get; set; }
        public Boolean IsDbConnection { get; set; }
        public int StatusCode { get; set; }
        public string SubAdminId { get; set; }
        public string CompanyId { get; set; }
        public string DBConnectorId { get; set; }
    }
}
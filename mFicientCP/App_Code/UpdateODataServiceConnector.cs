﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class UpdateODataServiceConnector
    {
       public UpdateODataServiceConnector()
        { 

        }

       public UpdateODataServiceConnector(string _CompanyId, string _SubAdminId, string _ConnectionId, string _EndPointUrl, string _UserName, string _Password, string _ODataContent, string _MpluginAgent, string _Version)
       {
           this.CompanyId = _CompanyId;
           this.SubAdminId = _SubAdminId;
           this.ConnectionId = _ConnectionId;
           this.UserName = _UserName;
           this.Password = _Password;
           this.EndPointUrl = _EndPointUrl;
           this.ODataContent = _ODataContent;
           this.MpluginAgent = _MpluginAgent;
           this.Version = _Version;
           Process();
       }

       public UpdateODataServiceConnector(string _CompanyId, string _SubAdminId, string _ConnectionId, string _EndPointUrl, string _UserName, string _Password, string _HttpAutenticationtype, string _ODataContent, string _MpluginAgent, string _Version, long _UpdatedOn, string CredentialProperty)
       {
           this.CompanyId = _CompanyId;
           this.SubAdminId = _SubAdminId;
           this.ConnectionId = _ConnectionId;
           this.UserName = _UserName;
           this.Password = _Password;
           this.EndPointUrl = _EndPointUrl;
           this.ODataContent = _ODataContent;
           this.MpluginAgent = _MpluginAgent;
           this.Version = _Version;
           this.HttpAutenticationtype = _HttpAutenticationtype;
           this.UpdatedOn = _UpdatedOn;
           this.CREDENTIAL_PROPERTY = CredentialProperty;
           Process3();
       }
       
       private void Process3()
       {
           try
           {

               this.StatusCode = -1000;
               string query;

               query = @"UPDATE TBL_ODATA_CONNECTION SET 
                                ODATA_ENDPOINT=@ODATA_ENDPOINT,
	                            ODATA_CONTENT=@ODATA_CONTENT,
                                MPLUGIN_AGENT=@MPLUGIN_AGENT,
                                SUBADMIN_ID=@SUBADMIN_ID,
                               AUTHENTICATION_TYPE=@AUTHENTICATION_TYPE,
                                CREDENTIAL_PROPERTY=@CREDENTIAL_PROPERTY,
                                VERSION=@VERSION, 
                                UPDATED_ON=@UPDATED_ON 
                            WHERE
                            ODATA_CONNECTOR_ID=@ODATA_CONNECTOR_ID AND 
                            COMPANY_ID=@COMPANY_ID;";
               SqlCommand objSqlCommand = new SqlCommand(query);
               objSqlCommand.CommandType = CommandType.Text;
               objSqlCommand.Parameters.AddWithValue("@ODATA_CONNECTOR_ID", this.ConnectionId);
               objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
               objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
               objSqlCommand.Parameters.AddWithValue("@AUTHENTICATION_TYPE", this.HttpAutenticationtype);
               objSqlCommand.Parameters.AddWithValue("@ODATA_ENDPOINT", this.EndPointUrl);
               objSqlCommand.Parameters.AddWithValue("@ODATA_CONTENT", this.ODataContent);
               objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgent);
               objSqlCommand.Parameters.AddWithValue("@VERSION", this.Version);
               objSqlCommand.Parameters.AddWithValue("@UPDATED_ON ", this.UpdatedOn);
               objSqlCommand.Parameters.AddWithValue("@CREDENTIAL_PROPERTY", this.CREDENTIAL_PROPERTY);
               if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;


           }
           catch 
           {
               this.StatusCode = -1000;
           }
       }
        private void Process()
        {
            try
            {

                this.StatusCode = -1000;
                string query;
              
                query = @"UPDATE TBL_ODATA_CONNECTION SET 
                                ODATA_ENDPOINT=@ODATA_ENDPOINT,
                                USER_NAME=@USER_NAME,
                                PASSWORD=@PASSWORD,
	                            ODATA_CONTENT=@ODATA_CONTENT,
                                MPLUGIN_AGENT=@MPLUGIN_AGENT,
                                VERSION=@VERSION   
                            WHERE
                            ODATA_CONNECTOR_ID=@ODATA_CONNECTOR_ID AND 
                            SUBADMIN_ID=@SUBADMIN_ID AND 
                            COMPANY_ID=@COMPANY_ID;";
                

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@ODATA_CONNECTOR_ID", this.ConnectionId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@USER_NAME", this.UserName);
                objSqlCommand.Parameters.AddWithValue("@PASSWORD", this.Password);
                objSqlCommand.Parameters.AddWithValue("@ODATA_ENDPOINT", this.EndPointUrl);
                objSqlCommand.Parameters.AddWithValue("@ODATA_CONTENT", this.ODataContent);
                objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgent);
                objSqlCommand.Parameters.AddWithValue("@VERSION", this.Version);
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;


            }
            catch 
            {
                this.StatusCode = -1000;
            }
        }

        public string CompanyId { get; set; }
        public string SubAdminId { get; set; }
        public string ConnectionId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EndPointUrl { get; set; }
        public string ODataContent { get; set; }
        public string MpluginAgent { get; set; }
        public int StatusCode { get; set; }
        public string Version { get; set; }
        public string HttpAutenticationtype { get; set; }
        public long UpdatedOn { get; set; }
        public string CREDENTIAL_PROPERTY { get; set; }
        public string typeofcredential { get; set; }


        
    }
}
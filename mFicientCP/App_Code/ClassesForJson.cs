﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientCP
{
    public class ClassesForJson
    {
    }
    [DataContract]
    public class MessageCategoryForPushMessage
    {
        public MessageCategoryForPushMessage() { }
        [DataMember]
        public string txt { get; set; }
        [DataMember]
        public string id { get; set; }
    }

    public class UserDetailResponseForAutoCompleteTextBox
    {
        /// <summary>
        /// User Details
        /// </summary>
        [DataMember]
        public List<UserDetailMembersForAutoCmptTextBox> data { get; set; }
    }

    public class UserDetailMembersForAutoCmptTextBox
    {
        /// <summary>
        /// User Id
        /// </summary>
        [DataMember]
        public string id { get; set; }

        /// <summary>
        /// User Name
        /// </summary>
        [DataMember]
        public string name { get; set; }

        /// <summary>
        /// User Name
        /// </summary>
        [DataMember]
        public string fnm { get; set; }

        /// <summary>
        /// User Name
        /// </summary>
        [DataMember]
        public string lnm { get; set; }
    }

    public class OfflineInputOutPutParamByCommand
    {
        [DataMember]
        public List<OfllineParam> inptOut { get; set; }
        //[DataMember]
        //public OfflineOutputParam outpt { get; set; }
    }
    public class OfflineInputParam
    {
        [DataMember]
        public List<OfllineParam> inpt { get; set; }
    }
    public class OfflineOutputParam
    {
        [DataMember]
        public List<OfllineParam> outpt { get; set; }
    }
    public class OfllineParam
    {
        /// <summary>
        /// Command Name
        /// </summary>
        [DataMember]
        public string cmdnm { get; set; }

        /// <summary>
        ///Command Type
        /// </summary>
        [DataMember]
        public string cmdType { get; set; }

        /// <summary>
        /// Command Id
        /// </summary>
        [DataMember]
        public string cmdid { get; set; }

        /// <summary>
        /// Webservice Type
        /// </summary>
        [DataMember]
        public string wstype { get; set; }

        /// <summary>
        /// Input Parma
        /// </summary>
        [DataMember]
        public string inputprm { get; set; }

        /// <summary>
        /// Output Praram
        /// </summary>
        [DataMember]
        public string outputprm { get; set; }
    }
    public class OfflineCommandIdsAlreadySeleted
    {
        /// <summary>
        /// Command Download
        /// </summary>
        [DataMember]
        public string cmdDown { get; set; }
        /// <summary>
        /// Command Upload
        /// </summary>
        [DataMember]
        public string cmdUp { get; set; }

        /// <summary>
        /// Command Out
        /// </summary>
        [DataMember]
        public string cmdOut { get; set; }
    }
    [DataContract]
    public class UserDtlForJson
    {
        public UserDtlForJson() { }
        [DataMember]
        public string usrnm { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string frstName { get; set; }
        [DataMember]
        public string lstName { get; set; }
    }
    [DataContract]
    public class UsersOfCompanyForJson
    {
        public UsersOfCompanyForJson() { }
        [DataMember]
        public List<UserDtlForJson> usrs { get; set; }
    }

    #region Offline Table
    [DataContract]
    public class OfflineSyncCompltDetForUIFormation
    {
        [DataMember]
        OfflineSavedSyncCompleteDetailForUI syncUI { get; set; }
    }
    [DataContract]
    public class OfflineSavedSyncCompleteDetailForUI
    {
        [DataMember]
        public string cmdType { get; set; }
        [DataMember]
        public string conVal { get; set; }
        [DataMember]
        public string downCmdVal { get; set; }
        [DataMember]
        public string upCmdVal { get; set; }
        [DataMember]
        public List<TextValuePair> cmdConns { get; set; }
        [DataMember]
        public List<TextValuePair> cmdDownloadCmds { get; set; }
        [DataMember]
        public List<TextValuePair> cmdUploadCmds { get; set; }
        [DataMember]
        public List<string> appIdsSelected { get; set; }
        [DataMember]
        public string inOutParamForSelect { get; set; }
        [DataMember]
        public string inOutParamForInsert { get; set; }
    }
    [DataContract]
    public class TextValuePair
    {
        [DataMember]
        public string text { get; set; }
        [DataMember]
        public string value { get; set; }
    }
    [DataContract]
    public class OfflineSaveSyncTabConCmdDtl
    {
        [DataMember]
        public string conn { get; set; }
        [DataMember]
        public string dwnCmd { get; set; }
        [DataMember]
        public string upCmd { get; set; }
    }
    [DataContract]
    public class OfflineDataTableJson
    {
        public OfflineDataTableJson() { }
        [DataMember]
        public OfflineTable tbl { get; set; }
    }
    [DataContract]
    public class OfflineDataTableOutPutJson
    {
        public OfflineDataTableOutPutJson() { }
        [DataMember]
        public List<ListParameters> sync { get; set; }
    }
    [DataContract]
    public class Offline_object_Col
    {
        [DataMember]
        public string col { get; set; }
        [DataMember]
        public string para { get; set; }
    }

    [DataContract]
    public class OfflineDataTableSyncCont
    {
        public OfflineDataTableSyncCont() { }
        [DataMember]
        public OfflineTableSyncDtls sync { get; set; }
    }

    [DataContract]
    public class OfflineTableSyncDtls
    {
        public OfflineTableSyncDtls() { }
        [DataMember]
        public string cmdType { get; set; }
        [DataMember]
        public string conVal { get; set; }
        [DataMember]
        public string downCmdVal { get; set; }
        [DataMember]
        public string upCmdVal { get; set; }
        [DataMember]
        public List<ListParameters> uploadInParam { get; set; }
        [DataMember]
        public List<ListParameters> downloadInParam { get; set; }
        [DataMember]
        public List<ListParameters> downloadOutParam { get; set; }
    }
    [DataContract]
    public class OfflineTable
    {
        public OfflineTable() { }
        [DataMember]
        public string nm { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string typ { get; set; }
        [DataMember]
        public string sync { get; set; }
        [DataMember]
        public string seq { get; set; }
        [DataMember]
        public string prmt { get; set; }
        /// <summary>
        /// No of records
        /// </summary>
        [DataMember]
        public string nor { get; set; }
        //[DataMember]
        //public List<ListParameters> down { get; set; }
        //[DataMember]
        //public List<ListParameters> up { get; set; }
        [DataMember]
        public List<Off_columns> col { get; set; }
    }
    [DataContract]
    public class ListParameters
    {
        [DataMember]
        public string para { get; set; }
        [DataMember]
        public string val { get; set; }
    }
    [DataContract]
    public class Off_columns
    {
        [DataMember]
        public string cnm { get; set; }
        [DataMember]
        public string ctyp { get; set; }
        [DataMember]
        public string dpnt { get; set; }
        [DataMember]
        public string min { get; set; }
        [DataMember]
        public string max { get; set; }
        [DataMember]
        public string dval { get; set; }
    }


    #endregion
    #region OfflineInputOutputParamForXmlRpc
    public class OfflineInputOutputParamForXmlRpc
    {
        [DataMember]
        public List<OfflineInputOutputParamDetail> param { get; set; }
    }
    public class OfflineInputOutputParamDetail
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string typ { get; set; }
    }
    #endregion
    #region Apps
    [DataContract]
    public class IdeAppsForJson
    {
        public IdeAppsForJson() { }
        [DataMember]
        public string compId { get; set; }
        [DataMember]
        public string wfid { get; set; }
        [DataMember]
        public string wfName { get; set; }
        [DataMember]
        public string wfDescription { get; set; }
        [DataMember]
        public string wfJson { get; set; }
        [DataMember]
        public string parentId { get; set; }
        [DataMember]
        public string modelType { get; set; }
        [DataMember]
        public string icon { get; set; }
        [DataMember]
        public string noOfRecordsInDb { get; set; }
        [DataMember]
        public string runsOn { get; set; }
        [DataMember]
        public string diffInterfaceForTab { get; set; }
        [DataMember]
        public string designInterface { get; set; }
        [DataMember]
        public jqueryAppClass appJsJson { get; set; }


    }
    [DataContract]
    public class IdeFormsForJson
    {
        public IdeFormsForJson() { }
        [DataMember]
        public string compId { get; set; }
        [DataMember]
        public string formId { get; set; }
        [DataMember]
        public string formName { get; set; }
        [DataMember]
        public string isActive { get; set; }
        [DataMember]
        public string formJson { get; set; }
        [DataMember]
        public string formDesc { get; set; }
        [DataMember]
        public string cmdIds { get; set; }
        [DataMember]
        public string masterid { get; set; }
        [DataMember]
        public string parentId { get; set; }
        [DataMember]
        public string rptRowClick { get; set; }
        [DataMember]
        public string modelType { get; set; }
    }
    [DataContract]
    public class IdeMasterPagesForJson
    {
        public IdeMasterPagesForJson() { }
        [DataMember]
        public string compId { get; set; }
        [DataMember]
        public string masterPageId { get; set; }
        [DataMember]
        public string masterName { get; set; }
        [DataMember]
        public string isActive { get; set; }
        [DataMember]
        public string parentId { get; set; }
    }
    [DataContract]
    public class IdeDbCmdsForJson
    {
        public IdeDbCmdsForJson() { }
        [DataMember]
        public string compId { get; set; }
        [DataMember]
        public string commandId { get; set; }
        [DataMember]
        public string connectorId { get; set; }
        [DataMember]
        public string connectionName { get; set; }
        [DataMember]
        public string sqlQuery { get; set; }
        [DataMember]
        public string parameter { get; set; }
        [DataMember]
        public string commandName { get; set; }
        [DataMember]
        public string returnType { get; set; }
        [DataMember]
        public string commandType { get; set; }
        [DataMember]
        public string tableName { get; set; }
        [DataMember]
        public string queryType { get; set; }
        [DataMember]
        public string paramJson { get; set; }
        [DataMember]
        public string columns { get; set; }
        [DataMember]
        public string description { get; set; }
    }
    [DataContract]
    public class IdeWsCmdsForJson
    {
        public IdeWsCmdsForJson() { }
        [DataMember]
        public string compId { get; set; }
        [DataMember]
        public string commandId { get; set; }
        [DataMember]
        public string connectorId { get; set; }
        [DataMember]
        public string connectionName { get; set; }
        [DataMember]
        public string url { get; set; }
        [DataMember]
        public string parameter { get; set; }
        [DataMember]
        public string returnType { get; set; }
        [DataMember]
        public string recordPath { get; set; }
        [DataMember]
        public string datasetPath { get; set; }
        [DataMember]
        public string tag { get; set; }
        [DataMember]
        public string commandName { get; set; }
        [DataMember]
        public string service { get; set; }
        [DataMember]
        public string portType { get; set; }
        [DataMember]
        public string method { get; set; }
        [DataMember]
        public string webserviceType { get; set; }
        [DataMember]
        public string httpType { get; set; }
        [DataMember]
        public string description { get; set; }
    }
    [DataContract]
    public class IdeOdataCmdsForJson
    {
        public IdeOdataCmdsForJson() { }
        [DataMember]
        public string compId { get; set; }
        [DataMember]
        public string connectorId { get; set; }
        [DataMember]
        public string connectionName { get; set; }
        [DataMember]
        public string endPoint { get; set; }
        [DataMember]
        public string serviceVer { get; set; }
        [DataMember]
        public string commandId { get; set; }
        [DataMember]
        public string commandName { get; set; }
        [DataMember]
        public string resourceType { get; set; }
        [DataMember]
        public string resourcePath { get; set; }
        [DataMember]
        public string httpType { get; set; }
        [DataMember]
        public string httpData { get; set; }
        [DataMember]
        public string queryOption { get; set; }
        [DataMember]
        public string inputParamJson { get; set; }
        [DataMember]
        public string outputParamJson { get; set; }
        [DataMember]
        public string functionType { get; set; }
        [DataMember]
        public string responseFormat { get; set; }
        [DataMember]
        public string dataJson { get; set; }
        [DataMember]
        public string datasetPath { get; set; }
        [DataMember]
        public string returnType { get; set; }
        [DataMember]
        public string additionalResourcePath { get; set; }
        [DataMember]
        public string description { get; set; }
    }
    [DataContract]
    public class MfHtml5App
    {
        public MfHtml5App()
        {
            this.name = String.Empty;
            this.id = String.Empty;
            this.description = String.Empty;
            this.icon = MficientConstants.IDE_APPLICATION_DEFAULT_ICON;
            this.startupPage = String.Empty;
        }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string icon { get; set; }
        [DataMember]
        public string startupPage { get; set; }
    }
    #endregion



    #region EntepriseAddtionalDefination



    //public class ParaData
    //{
    //    public List<EnterpriseDefination>[] ParaData { get; set; }
    //}


    public class EnterpriseDefination
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string tag { get; set; }
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public List<Validationsetting> validationSetting { get; set; }
    }

    public class Validationsetting
    {
        [DataMember]
        public int req { get; set; }

        [DataMember]
        public string vtype { get; set; }
        [DataMember]
        public string allowNumStart { get; set; }

        [DataMember]
        public string allowSpace { get; set; }

        [DataMember]
        public string apecialchar { get; set; }
        [DataMember]
        public string[] selectopt { get; set; }

    }



    #endregion


    public class PostbackByHtmlControlData
    {
        public PostbackByHtmlControlData() { }
        [DataMember]
        public List<string> data { get; set; }
        [DataMember]
        public string processFor { get; set; }
    }

    public class Html5AppNodeForTreeView
    {
        public Html5AppNodeForTreeView()
        {
            this.isDirectory = false;
        }
        public string id
        {
            get;
            set;
        }
        public string text
        {
            get;
            set;
        }
        public string icon
        {
            get;
            set;
        }
        public Html5AppNodeForTreeViewState state
        {
            get;
            set;
        }
        public List<Html5AppNodeForTreeView> children
        {
            get;
            set;
        }
        public bool isDirectory
        {
            get;
            set;
        }
        public int fileType
        {
            get;
            set;
        }
    }
    public class Html5AppNodeForTreeViewState
    {
        public Html5AppNodeForTreeViewState()
        {
            this.opened = false;
            this.disabled = false;
            this.selected = false;
        }
        public bool opened
        {
            get;
            set;
        }
        public bool disabled
        {
            get;
            set;
        }
        public bool selected
        {
            get;
            set;
        }
    }
    public class Html5AppFileContentTxt
    {
        public string content
        {
            get;
            set;
        }
    }
    public class Html5AppFileDtlForUIPropSheet
    {
        public Html5AppFileDtlForUIPropSheet()
        {
            this.name = "";
            this.size = "";
            this.lastEditedDate = "";
            this.editedBy = "";
            this.dimensions = "";
            this.fileType = "";
        }
        public string name
        {
            get;
            set;
        }
        public string size
        {
            get;
            set;
        }
        public string lastEditedDate
        {
            get;
            set;
        }
        public string editedBy
        {
            get;
            set;
        }
        public string dimensions
        {
            get;
            set;
        }
        public string fileType
        {
            get;
            set;
        }
    }
    public class Html5AppEntryMetaData
    {
        public Html5AppEntryMetaData()
        {
            this.Id = String.Empty;
            this.Name = String.Empty;
            this.FilePath = String.Empty;
            this.FileType = Int32.MinValue;
            this.FileContent = String.Empty;
            this.IsDirectory = false;
            this.FileSize = 0;
            this.LastEditedDate = 0;
            this.LastEditedBy = String.Empty;
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public string FilePath { get; set; }
        public int FileType { get; set; }
        public string FileContent { get; set; }
        public bool IsDirectory { get; set; }
        public long FileSize { get; set; }
        public long LastEditedDate { get; set; }
        public string LastEditedBy { get; set; }
    }
    public class Html5AppTempFileKeyForUI
    {
        public Html5AppTempFileKeyForUI()
        {

            this.S = String.Empty;
        }
        /// <summary>
        /// Session
        /// </summary>
        public string S
        {
            get;
            set;
        }
    }
    public class Html5AppFileMoveDataForUI
    {
        public Html5AppFileMoveDataForUI()
        {
            this.newPath = String.Empty;
            this.oldPath = String.Empty;
            this.metaData = String.Empty;
        }
        public string oldPath { get; set; }
        public string newPath { get; set; }
        public string metaData { get; set; }
    }

    public class GroupDetails
    {
        /// <summary>
        /// User Id
        /// </summary>
        [DataMember]
        public string id { get; set; }

        /// <summary>
        /// User Name
        /// </summary>
        [DataMember]
        public string name { get; set; }

        /// <summary>
        /// User Name
        /// </summary>
        [DataMember]
        public string fnm { get; set; }

        /// <summary>
        /// User Name
        /// </summary>
        [DataMember]
        public string lnm { get; set; }
    }
    public class GroupDetailsAutoCompleteTextBox
    {
        /// <summary>
        /// User Details
        /// </summary>
        [DataMember]
        public List<GroupDetails> data { get; set; }
    }

    [DataContract]
    public class IdeArchivedAppsForListingUI
    {
        public IdeArchivedAppsForListingUI() { }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Versions { get; set; }
        [DataMember]
        public string ModelTypes { get; set; }
    }


}
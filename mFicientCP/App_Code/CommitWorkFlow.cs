﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class CommitWorkFlow
    {
        public CommitWorkFlow()
        {

        }

        public CommitWorkFlow(DataTable _WsDataTable, string _Version, string _VersionHistroy, Boolean _IsDeleteLastVer, string _DelVersion)
        {
            this.WsDataTable = _WsDataTable;
            this.Version = _Version;
            this.VersionHistroy = _VersionHistroy;
            this.IsDeleteLastVer = _IsDeleteLastVer;
            this.DelVersion = _DelVersion;
        }
        public void NewProcess()
        {
            this.StatusCode = -1000;
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            SqlCommand objSqlCommand;
            string query;
            try
            {
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                objSqlTransaction = objSqlConnection.BeginTransaction();
                foreach (DataRow dr in WsDataTable.Rows)
                {
                    query = @"INSERT INTO TBL_PRV_WORKFLOW_DETAIL(COMPANY_ID,WF_ID,WF_NAME,WF_DESCRIPTION,WF_JSON,VERSION,WORKED_BY,PUBLISH_ON,SUBADMIN_ID,FORMS,COMMITED_ON,PARENT_ID,VERSION_HISTORY,MODEL_TYPE,APP_JS_JSON,ICON,APP_TYPE)
                    VALUES(@COMPANY_ID,@WF_ID,@WF_NAME,@WF_DESCRIPTION,@WF_JSON,@VERSION,@WORKED_BY,@PUBLISH_ON,@SUBADMIN_ID,@FORMS,@COMMITED_ON,@PARENT_ID,@VERSION_HISTORY,@MODEL_TYPE,@APP_JS_JSON,@ICON,@APP_TYPE);";
                    objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                    objSqlCommand.CommandType = CommandType.Text;

                    objSqlCommand.Parameters.AddWithValue("@WF_ID", Convert.ToString(dr["WF_ID"]));
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", Convert.ToString(dr["COMPANY_ID"]));
                    objSqlCommand.Parameters.AddWithValue("@WF_NAME", Convert.ToString(dr["WF_NAME"]));
                    objSqlCommand.Parameters.AddWithValue("@WF_DESCRIPTION", Convert.ToString(dr["WF_DESCRIPTION"]));
                    objSqlCommand.Parameters.AddWithValue("@WF_JSON", Convert.ToString(dr["WF_JSON"]));
                    objSqlCommand.Parameters.AddWithValue("@VERSION", this.Version);
                    objSqlCommand.Parameters.AddWithValue("@WORKED_BY", Convert.ToString(dr["WORKED_BY"]));
                    objSqlCommand.Parameters.AddWithValue("@PUBLISH_ON", "0");
                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", Convert.ToString(dr["SUBADMIN_ID"]));
                    objSqlCommand.Parameters.AddWithValue("@FORMS", Convert.ToString(dr["FORMS"]));
                    objSqlCommand.Parameters.AddWithValue("@COMMITED_ON", DateTime.UtcNow.Ticks);
                    objSqlCommand.Parameters.AddWithValue("@PARENT_ID", Convert.ToString(dr["PARENT_ID"]));
                    objSqlCommand.Parameters.AddWithValue("@VERSION_HISTORY", this.VersionHistroy);
                    objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", Convert.ToInt32(dr["MODEL_TYPE"]));
                    objSqlCommand.Parameters.AddWithValue("@APP_JS_JSON", Convert.ToString(dr["APP_JS_JSON"]));
                    objSqlCommand.Parameters.AddWithValue("@ICON", Convert.ToString(dr["ICON"]));
                    objSqlCommand.Parameters.AddWithValue("@APP_TYPE", Convert.ToByte(dr["APP_TYPE"]));
                    if (objSqlCommand.ExecuteNonQuery() > 0)
                        this.StatusCode = 0;
                    else
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                if (IsDeleteLastVer && !string.IsNullOrEmpty(this.DelVersion))
                {
                    query = @"DELETE FROM TBL_PRV_WORKFLOW_DETAIL
                            WHERE WF_ID=@WF_ID AND SUBADMIN_ID=@SUBADMIN_ID AND VERSION in ( " + this.DelVersion + " ) AND COMPANY_ID=@COMPANY_ID;";

                    objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@WF_ID", Convert.ToString(WsDataTable.Rows[0]["WF_ID"]));
                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", Convert.ToString(WsDataTable.Rows[0]["SUBADMIN_ID"]));
                    // objSqlCommand.Parameters.AddWithValue("@VERSION", this.DelVersion);
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", Convert.ToString(WsDataTable.Rows[0]["COMPANY_ID"]));
                    int affectedRow = objSqlCommand.ExecuteNonQuery();
                    if (affectedRow > 0) this.StatusCode = 0;
                }
                objSqlTransaction.Commit();
            }
            catch
            {
                this.StatusCode = -1000;
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
            }
            finally
            {
                if (objSqlTransaction != null)
                    objSqlTransaction.Dispose();
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }

        public void commitAppForTester(string _subAdminId, string _CompanyId, string _WF_Id, List<jqueryAppClass> lstApps)
        {
            SqlCommand objSqlCommand;
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            this.StatusCode = -1000;
            string strAppObj = string.Empty;
            string query;
            string version = "1.0";
            DataTable dtblPrevVersion = null;
            try
            {

                query = @"Select WF_NAME from TBL_WORKFLOW_DETAIL_FOR_TESTER where WF_ID=@WF_ID and COMPANY_ID=@COMPANY_ID;
                          SELECT top 1 ""VERSION"" FROM TBL_PRV_WORKFLOW_DETAIL
                          where WF_ID = @WF_ID
                          ORDER BY ""VERSION"" DESC; ";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _CompanyId);
                objSqlCommand.Parameters.AddWithValue("@WF_ID", _WF_Id);
                DataSet appTbl = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);

                if (appTbl != null && appTbl.Tables.Count >= 2)
                {
                    dtblPrevVersion = appTbl.Tables[1];
                    if (dtblPrevVersion.Rows.Count > 0)
                    {
                        version = Convert.ToString(dtblPrevVersion.Rows[0]["VERSION"]);
                    }
                }

                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                objSqlTransaction = objSqlConnection.BeginTransaction();

                string InterfaceModelType = "";
                foreach (jqueryAppClass app in lstApps)
                {
                    query = @"if exists (Select 'true' from TBL_WORKFLOW_DETAIL_FOR_TESTER where  COMPANY_ID=@COMPANY_ID and MODEL_TYPE=@MODEL_TYPE and WF_ID=@WF_ID)
                    update TBL_WORKFLOW_DETAIL_FOR_TESTER set [COMPANY_ID]=@COMPANY_ID,[VERSION]=@VERSION,[WF_NAME]=@WF_NAME,[WF_DESCRIPTION]=@WF_DESCRIPTION,[WORKED_BY]=@SUBADMIN_ID,
                    [PUBLISH_ON]=@PUBLISH_ON,[APP_JS_JSON]=@APP_JS_JSON,[ICON]=@ICON,VERSION_HISTORY=@VERSION_HISTORY where COMPANY_ID=@COMPANY_ID and MODEL_TYPE=@MODEL_TYPE and WF_ID=@WF_ID 
                    else INSERT INTO TBL_WORKFLOW_DETAIL_FOR_TESTER([COMPANY_ID],[WF_ID],[WF_NAME],[WF_DESCRIPTION],[WF_JSON],[VERSION],[WORKED_BY],[SUBADMIN_ID],[MODEL_TYPE],[APP_JS_JSON],
                    [ICON],[PARENT_ID],[FORMS],[PUBLISH_ON],[VERSION_HISTORY]) 
                    VALUES (@COMPANY_ID,@WF_ID,@WF_NAME,@WF_DESCRIPTION,@WF_JSON,@VERSION,@SUBADMIN_ID,@SUBADMIN_ID,@MODEL_TYPE,@APP_JS_JSON,@ICON,@WF_ID,'',@PUBLISH_ON,@VERSION_HISTORY);";

                    objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _CompanyId);
                    objSqlCommand.Parameters.AddWithValue("@WF_ID", _WF_Id);
                    objSqlCommand.Parameters.AddWithValue("@WF_NAME", app.name);
                    objSqlCommand.Parameters.AddWithValue("@WF_DESCRIPTION", app.description);
                    objSqlCommand.Parameters.AddWithValue("@WF_JSON", "");
                    objSqlCommand.Parameters.AddWithValue("@VERSION", version);
                    objSqlCommand.Parameters.AddWithValue("@PUBLISH_ON", DateTime.UtcNow.Ticks);
                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", _subAdminId);
                    objSqlCommand.Parameters.AddWithValue("@ICON", app.icon);
                    objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", app.modelType);
                    objSqlCommand.Parameters.AddWithValue("@VERSION_HISTORY", "");
                    objSqlCommand.Parameters.AddWithValue("@APP_JS_JSON", Utilities.SerializeJson<jqueryAppClass>(app));

                    if (objSqlCommand.ExecuteNonQuery() > 0)
                    {
                    }
                    else
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }

                if (appTbl != null && appTbl.Tables[0].Rows.Count > 0 && lstApps.Count() == 1)
                {
                    query = "delete  from TBL_WORKFLOW_DETAIL where WF_ID=@WF_ID and COMPANY_ID=@COMPANY_ID and MODEL_TYPE!=@MODEL_TYPE;";
                    objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _CompanyId);
                    objSqlCommand.Parameters.AddWithValue("@WF_ID", _WF_Id);
                    objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", InterfaceModelType);
                    if (objSqlCommand.ExecuteNonQuery() > 0)
                    {
                    }
                }
                objSqlTransaction.Commit();
                this.StatusCode = 0;
            }
            catch
            {
                this.StatusCode = -1000;
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
            }
            finally
            {
                if (objSqlTransaction != null)
                    objSqlTransaction.Dispose();
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }
        public DataTable WsDataTable { get; set; }

        public int StatusCode
        {
            set;
            get;
        }

        public string VersionHistroy { get; set; }

        public string Version { get; set; }

        public bool IsDeleteLastVer { get; set; }

        public string DelVersion { get; set; }
    }
}
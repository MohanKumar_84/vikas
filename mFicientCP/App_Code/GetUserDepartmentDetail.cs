﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetUserDepartmentDetail
    {
        public GetUserDepartmentDetail()
        { 

        }

        public GetUserDepartmentDetail(Boolean _IsSelectAll, string _AdminId, string _DepartmentId,string _UserId)
        {
            this.IsSelectAll = _IsSelectAll;
            this.AdminId = _AdminId;
            this.DepartmentId = _DepartmentId;
            this.UserId = _UserId;
        }
        public void Process()
        {
            try
            {
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                if (this.IsSelectAll == true)
                {
                    query = @"SELECT  * FROM  TBL_USER_DEPARTMENT WHERE 
		                                ADMIN_ID=@ADMIN_ID AND  USER_ID=@USER_ID";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                    objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                }
                else
                {
                    query = @"SELECT  * FROM  TBL_USER_DEPARTMENT WHERE 
		                                ADMIN_ID=@ADMIN_ID AND  USER_ID=@USER_ID AND DEPARTMENT_ID=@DEPARTMENT_ID;";

                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                    objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                    objSqlCommand.Parameters.AddWithValue("@DEPARTMENT_ID", this.DepartmentId);
                }

                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }

            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
               // throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
        }
        
        public string AdminId
        {
            set;
            get;
        }
        public string UserId
        {
            set;
            get;
        }
        public string DepartmentId
        {
            set;
            get;
        }
        public Boolean IsSelectAll
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
        //public string LocationName
        //{
        //    set;
        //    get;
        //}
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
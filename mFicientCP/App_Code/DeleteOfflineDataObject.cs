﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class DeleteOfflineDataObject
    {
        private string id, companyId;

        public DeleteOfflineDataObject(string _id, string _companyId)
        {
            id = _id;
            companyId = _companyId;

            try
            {
                process();
                StatusCode = 0;
            }
            catch 
            {
                StatusCode = -1;
            }
        }

        public int StatusCode
        {
            get;
            set;
        }

        private void process()
        {
            try
            {
                string strQuery = @"DELETE FROM TBL_OFFLINE_OBJECTS WHERE
                                    OFFLINE_OBJECT_ID = @OFFLINE_OBJECT_ID AND COMPANY_ID = @COMPANY_ID";

                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@OFFLINE_OBJECT_ID", id);
                cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
                int result = MSSqlClient.ExecuteNonQueryRecord(cmd);
                if (result <= 0)
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace mFicientCP
{
    public class SavePushMessageDtls
    {

        public SavePushMessageDtls(string dbType, string companyId, string connectionName,
            string hostName, string databaseName, string userName,
            string password, string tableName,
            string mpluginAgentName,
            string additionalInfo)
        {
            this.DBType = dbType;
            this.CompanyId = companyId.ToUpper();
            this.ConnectionName = connectionName;
            this.HostName = hostName;
            this.DatabaseName = databaseName;
            this.UserName = userName;
            this.Password = password;
            this.TableName = tableName;
            this.MessageId = String.Empty;
            this.Message = String.Empty;
            this.Users = String.Empty;
            this.Groups = String.Empty;
            this.DeleteAfterSending = false;
            this.Status = String.Empty;
            this.MpluginAgentName = mpluginAgentName;
            this.AdditionalInfo = additionalInfo;
        }

        public void Process()
        {
            try
            {
                StatusCode = -1000;
                SqlTransaction transaction = null;
                SqlConnection con;
                MSSqlClient.SqlConnectionOpen(out con);
                try
                {
                    using (con)
                    {
                        using (transaction = con.BeginTransaction())
                        {
                            saveDtlsInMficientDb(con, transaction);
                            //saveDtlsInMGramDb();
                            transaction.Commit();
                        }
                    }
                    this.StatusCode = 0;
                    this.StatusDescription = String.Empty;
                }
                catch (SqlException e)
                {
                    if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                    }
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                catch (Exception e)
                {
                    if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                    }
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                finally
                {
                    if (con != null)
                    {
                        con.Dispose();
                    }
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }
                }

            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        void saveDtlsInMficientDb(SqlConnection con, SqlTransaction transaction)
        {
            string query = @" IF EXISTS(SELECT * FROM TBL_PULL_DATA_SETTINGS WHERE COMPANY_ID = @COMPANY_ID)
                                    
                                   UPDATE [TBL_PULL_DATA_SETTINGS]
                                   SET [COMPANY_ID] = @COMPANY_ID
                                      ,[DATABASE_TYPE] = @DATABASE_TYPE
                                      ,[CONNECTION_NAME] = @CONNECTION_NAME
                                      ,[HOST_NAME] = @HOST_NAME
                                      ,[DATABASE_NAME] = @DATABASE_NAME
                                      ,[USER_NAME] = @USER_NAME
                                      ,[PASSWORD] = @PASSWORD
                                      ,[TABLE_NAME] = @TABLE_NAME
                                      ,[MESSAGE_ID] = @MESSAGE_ID
                                      ,[MESSAGE] = @MESSAGE
                                      ,[USERS] = @USERS
                                      ,[GROUPS] = @GROUPS
                                      ,[DELETE_AFTER_SENDING] = @DELETE_AFTER_SENDING
                                      ,[STATUS] = @STATUS
                                      ,[MPLUGIN_AGENT] = @MPLUGIN_AGENT
                                      ,[ADDITIONAL] = @ADDITIONAL
                                 WHERE COMPANY_ID = @COMPANY_ID
                                    
                                ELSE
                                
                                INSERT INTO [TBL_PULL_DATA_SETTINGS]
                                ([COMPANY_ID]
                                ,[DATABASE_TYPE]
                                ,[CONNECTION_NAME]
                                ,[HOST_NAME]
                                ,[DATABASE_NAME]
                                ,[USER_NAME]
                                ,[PASSWORD]
                                ,[TABLE_NAME]
                                ,[MESSAGE_ID]
                                ,[MESSAGE]
                                ,[USERS]
                                ,[GROUPS]
                                ,[DELETE_AFTER_SENDING]
                                ,[STATUS]
                                ,[MPLUGIN_AGENT]
                                ,[ADDITIONAL])
                            VALUES
                                (@COMPANY_ID,
                                @DATABASE_TYPE,
                                @CONNECTION_NAME,
                                @HOST_NAME,
                                @DATABASE_NAME,
                                @USER_NAME,
                                @PASSWORD,
                                @TABLE_NAME,
                                @MESSAGE_ID,
                                @MESSAGE,
                                @USERS,
                                @GROUPS,
                                @DELETE_AFTER_SENDING,
                                @STATUS,
                                @MPLUGIN_AGENT,
                                @ADDITIONAL)";

            SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@DATABASE_TYPE", this.DBType.ToString());
            objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
            objSqlCommand.Parameters.AddWithValue("@HOST_NAME", this.HostName);
            
            objSqlCommand.Parameters.AddWithValue("@DATABASE_NAME",
                AesEncryption.AESEncrypt(this.CompanyId.ToUpper(), this.DatabaseName));
            objSqlCommand.Parameters.AddWithValue("@USER_NAME",
                AesEncryption.AESEncrypt(this.CompanyId.ToUpper(), this.UserName));
            objSqlCommand.Parameters.AddWithValue("@PASSWORD",
                AesEncryption.AESEncrypt(this.CompanyId.ToUpper(), this.Password));

            objSqlCommand.Parameters.AddWithValue("@TABLE_NAME", this.TableName);
            objSqlCommand.Parameters.AddWithValue("@MESSAGE_ID", this.MessageId);
            objSqlCommand.Parameters.AddWithValue("@MESSAGE", this.Message);
            objSqlCommand.Parameters.AddWithValue("@USERS", this.Users);
            objSqlCommand.Parameters.AddWithValue("@GROUPS", this.Groups);
            objSqlCommand.Parameters.AddWithValue("@DELETE_AFTER_SENDING", this.DeleteAfterSending);
            objSqlCommand.Parameters.AddWithValue("@STATUS", this.Status);
            objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgentName);
            objSqlCommand.Parameters.AddWithValue("@ADDITIONAL", this.AdditionalInfo);
            int iRowsEffected = objSqlCommand.ExecuteNonQuery();
            if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
        }
        void saveDtlsInMGramDb()
        {
            string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
            string strQuery = @"IF EXISTS(SELECT * FROM [TBL_DATA_OUTBOX_CONNECTIONS] WHERE ENTERPRISE_ID = @ENTERPRISE_ID)
                                UPDATE [TBL_DATA_OUTBOX_CONNECTIONS]
                                   SET [ENTERPRISE_ID] = @ENTERPRISE_ID
                                      ,[CONNECTION_STRING] =@CONNECTION_STRING
                                      ,[DATABASE_TYPE] =@DATABASE_TYPE
                                      ,[TABLE_NAME] = @TABLE_NAME
                                 WHERE ENTERPRISE_ID = @ENTERPRISE_ID
                                 ELSE
                                 INSERT INTO [TBL_DATA_OUTBOX_CONNECTIONS]
                                           ([ENTERPRISE_ID]
                                           ,[CONNECTION_STRING]
                                           ,[DATABASE_TYPE]
                                           ,[TABLE_NAME]
                                           ,[DAY_LIMIT]
                                           ,[MONTH_LIMIT])
                                     VALUES
                                           (@ENTERPRISE_ID
                                           ,@CONNECTION_STRING
                                           ,@DATABASE_TYPE
                                           ,@TABLE_NAME
                                           ,0
                                           ,0)";

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", this.CompanyId);
            cmd.Parameters.AddWithValue("@CONNECTION_STRING",
                getConnectionStringToSaveInDB());
            cmd.Parameters.AddWithValue("@DATABASE_TYPE", this.DBType);
            cmd.Parameters.AddWithValue("@TABLE_NAME", this.TableName);
            int iRowsEffected = MSSqlClient.ExecuteNonQueryRecord(cmd, strConnectionString);
            if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
        }
        DatabaseType getDbTypeByValueInDdlDbType()
        {
            return (DatabaseType)Enum.Parse(typeof(DatabaseType), this.DBType);
        }
        string getConnectionStringToSaveInDB()
        {
            string strConnString = String.Empty;
            switch (getDbTypeByValueInDdlDbType())
            {
                case DatabaseType.MSSQL:
                   strConnString= MSSqlClient.getConnectionString(this.HostName, this.DatabaseName, this.UserName, this.Password, 0);
                    break;
                case DatabaseType.ORACLE:
                    break;
                case DatabaseType.MYSQL:
                    break;
                case DatabaseType.POSTGRESQL:
                    break;
                case DatabaseType.ODBCDSN:
                    break;
                default:
                    break;
            }
            return strConnString;
        }
        public string DBType
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string ConnectionName
        {
            set;
            get;
        }
        public string HostName
        {
            set;
            get;
        }
        public string DatabaseName
        {
            set;
            get;
        }
        public string UserName
        {
            set;
            get;
        }
        public string Password
        {
            set;
            get;
        }
        public string TableName
        {
            set;
            get;
        }
        public string MessageId
        {
            set;
            get;
        }
        public string Message
        {
            set;
            get;
        }
        public string Users
        {
            set;
            get;
        }
        public string Groups
        {
            set;
            get;
        }
        public bool DeleteAfterSending
        {
            set;
            get;
        }
        public string Status
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string MpluginAgentName
        {
            set;
            get;
        }
        public string AdditionalInfo
        {
            set;
            get;
        }
    }
}
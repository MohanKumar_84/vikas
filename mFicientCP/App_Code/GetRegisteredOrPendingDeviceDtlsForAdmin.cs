﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetRegisteredOrPendingDeviceDtlsForAdmin
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="adminId"></param>
        /// <param name="requestType">Whether Pending devices for approval(Pass "Pending") or Approved devices (Pass "Registered")</param>
        public GetRegisteredOrPendingDeviceDtlsForAdmin(string companyId, string adminId, string requestType)
        {
            AdminId = adminId;
            CompanyId = companyId;
            RequestType = requestType;
        }

        public GetRegisteredOrPendingDeviceDtlsForAdmin()
        {

        }
        public void Process()
        {
            try
            {
                StatusCode = -1000;
               
                string strQuery = getSqlQuery();
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@AdminId", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    ResultTables = objDataSet;
                    StatusCode = 0;
                    StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
        }

        private string getSqlQuery()
        {
            string strQuery = "";
            if (RequestType == "Pending")
            {
                strQuery = @"SELECT Request.*,usr.SUBADMIN_ID,usr.USER_NAME,usr.FIRST_NAME,usr.LAST_NAME,reqtype.REQUEST_DESCRIPTION,device.DEVICE_ID as 'RegDeviceId'
                            ,(SELECT REQUEST_ID FROM TBL_MST_REQUEST_TYPE_FOR_DEVICE_REGISTRATION WHERE REQUEST_DESCRIPTION = 'DENY')AS 'DenyId'
                            FROM TBL_DEVICE_REGISTRATION_REQUEST AS request
                            INNER JOIN TBL_USER_DETAIL AS usr
                            ON request.USER_ID = usr.USER_ID
                            INNER JOIN TBL_MST_REQUEST_TYPE_FOR_DEVICE_REGISTRATION AS reqtype
                            ON reqtype.REQUEST_ID = request.REQUEST_TYPE
                            INNER JOIN TBL_COMPANY_ADMINISTRATOR AS CmpAdmin
							ON CmpAdmin.COMPANY_ID = request.COMPANY_ID
                            LEFT OUTER JOIN TBL_REGISTERED_DEVICE as device
                            ON device.DEVICE_ID = request.DEVICE_ID
                            WHERE request.COMPANY_ID = @CompanyId
                            AND CmpAdmin.ADMIN_ID = @AdminId
                            AND request.STATUS =0 Order by REQUEST_DATETIME desc;
                            
                            SELECT * FROM TBL_MST_REQUEST_TYPE_FOR_DEVICE_REGISTRATION;
                            
                            SELECT COUNT(*) AS Total,CurrPlan.MAX_USER
                            FROM TBL_REGISTERED_DEVICE AS RegDev
                            INNER JOIN TBL_COMPANY_CURRENT_PLAN AS CurrPlan
                            ON RegDev.COMPANY_ID = CurrPlan.COMPANY_ID
                            WHERE RegDev.COMPANY_ID = @CompanyId
                            GROUP BY CurrPlan.MAX_USER ;

                            SELECT COUNT(*) AS Total FROM
                            TBL_DEVICE_REGISTRATION_REQUEST  request
                            INNER JOIN TBL_USER_DETAIL usr
                            ON request.USER_ID = usr.USER_ID
							INNER JOIN TBL_COMPANY_ADMINISTRATOR AS CmpAdmin
							ON CmpAdmin.COMPANY_ID = request.COMPANY_ID
                            WHERE request.COMPANY_ID =@CompanyId
                            AND CmpAdmin.ADMIN_ID = @AdminId
                            AND STATUS = 0;

                            SELECT MAX_USER
                            FROM TBL_COMPANY_CURRENT_PLAN AS CurrPlan
							INNER JOIN TBL_COMPANY_ADMINISTRATOR AS CmpAdmin
							ON CmpAdmin.COMPANY_ID = CurrPlan.COMPANY_ID
                            WHERE CurrPlan.COMPANY_ID = @CompanyId
                            AND CmpAdmin.ADMIN_ID = @AdminId;

                            SELECT COUNT(*) AS REGISTERED_DEVICE_COUNT,RegDev.USER_ID,
                            UsrDevSetng.MAX_DEVICE,AccSetng.MAX_DEVICE_PER_USER
                            FROM TBL_REGISTERED_DEVICE AS RegDev
                            LEFT OUTER JOIN TBL_USER_DEVICE_SETTINGS AS UsrDevSetng
                            ON UsrDevSetng.USER_ID=RegDev.USER_ID
                            LEFT OUTER JOIN TBL_ACCOUNT_SETTINGS AS AccSetng
                            ON AccSetng.COMPANY_ID = RegDev.COMPANY_ID
							INNER JOIN TBL_COMPANY_ADMINISTRATOR AS CmpAdmin
							ON CmpAdmin.COMPANY_ID = RegDev.COMPANY_ID
                            WHERE RegDev.COMPANY_ID =  @CompanyId
                            AND CmpAdmin.ADMIN_ID = @AdminId
                            GROUP BY RegDev.USER_ID,UsrDevSetng.MAX_DEVICE,AccSetng.MAX_DEVICE_PER_USER;

                            SELECT COUNT(*) AS REGISTERED_DEVICE_COUNT FROM TBL_REGISTERED_DEVICE AS RegDev
							INNER JOIN TBL_COMPANY_ADMINISTRATOR AS CmpAdmin
							ON CmpAdmin.COMPANY_ID = RegDev.COMPANY_ID
                            WHERE RegDev.COMPANY_ID = @CompanyId
                            AND CmpAdmin.ADMIN_ID = @AdminId;

                            SELECT COUNT(*) AS REQUEST_COUNT,USER_ID
                            FROM TBL_DEVICE_REGISTRATION_REQUEST request
							INNER JOIN TBL_COMPANY_ADMINISTRATOR AS CmpAdmin
							ON CmpAdmin.COMPANY_ID = request.COMPANY_ID
                            WHERE request.COMPANY_ID = @CompanyId
                            AND request.STATUS =0
                            AND CmpAdmin.ADMIN_ID = @AdminId
                            GROUP BY request.USER_ID;
                            ";
            }
            else
            {
                strQuery = @"
                            SELECT device.*,usr.USER_ID,usr.USER_NAME,(usr.FIRST_NAME +' '+usr.LAST_NAME)AS FULL_NAME 
                            FROM TBL_REGISTERED_DEVICE device
                            INNER JOIN TBL_USER_DETAIL as usr
                            ON usr.USER_ID = device.USER_ID
                            WHERE device.COMPANY_ID =@CompanyId
                            AND usr.SUBADMIN_ID IN (SELECT SUBADMIN_ID FROM TBL_SUB_ADMIN
                            WHERE ADMIN_ID = @AdminId AND device.COMPANY_ID =@CompanyId) Order by device.REGISTRATION_DATE desc;

                            SELECT COUNT(*) AS Total,CurrPlan.MAX_USER FROM TBL_REGISTERED_DEVICE AS RegDev INNER JOIN TBL_COMPANY_CURRENT_PLAN AS CurrPlan
                                    ON RegDev.COMPANY_ID = CurrPlan.COMPANY_ID WHERE RegDev.COMPANY_ID = @CompanyId AND RegDev.SUBADMIN_ID IN (SELECT SUBADMIN_ID
									FROM TBL_SUB_ADMIN WHERE ADMIN_ID = @AdminId AND RegDev.COMPANY_ID =@CompanyId) GROUP BY CurrPlan.MAX_USER ;
                
                            SELECT COUNT(*) AS Total FROM TBL_DEVICE_REGISTRATION_REQUEST  request INNER JOIN TBL_USER_DETAIL usr
                                    ON request.USER_ID = usr.USER_ID WHERE request.COMPANY_ID =@CompanyId AND usr.SUBADMIN_ID IN (SELECT SUBADMIN_ID
									FROM TBL_SUB_ADMIN WHERE ADMIN_ID = @AdminId AND request.COMPANY_ID =@CompanyId);

                            SELECT MAX_USER FROM TBL_COMPANY_CURRENT_PLAN WHERE COMPANY_ID = @CompanyId;";
            }

            return strQuery;
        }
        public DataSet ResultTables
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string AdminId
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string RequestType
        {
            set;
            get;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class UpdateGroup
    {
        public UpdateGroup()
        {

        }
        public void Process()
        {
            try
            {
                string query = @"UPDATE TBL_USER_GROUP SET GROUP_NAME=@GROUP_NAME WHERE GROUP_ID =@GROUP_ID AND SUBADMIN_ID=@SUBADMIN_ID;";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@GROUP_ID", this.GroupId);
                objSqlCommand.Parameters.AddWithValue("@GROUP_NAME", this.GroupName);

                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    Utilities.saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.GROUP_RENAMED, this.CompanyId, this.SubAdminId, this.GroupId, this.GroupName, this.OldGroupName, "", "", "", "", "");
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        public string SubAdminId
        {
            set;
            get;
        }
        public string GroupId
        {
            set;
            get;
        }
        public string GroupName
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string CompanyId
        {
            get;
            set;
        }

        public string OldGroupName
        {
            set;
            get;
        }

    }
}
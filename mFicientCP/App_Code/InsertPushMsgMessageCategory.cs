﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class InsertPushMsgMessageCategory
    {
        public InsertPushMsgMessageCategory(string companyId, string userId, string msgCategory)
        {
            this.CompanyId = companyId;
            this.UserId = userId;
            this.MessageCategory = msgCategory;
        }
        public void Process()
        {
            try
            {
                string strQuery = @"INSERT INTO TBL_PUSH_MSG_MESSAGE_CATEGORY(MSG_CATEGORY_ID,MSG_CATEGORY,USER_ID,COMPANY_ID)
                                    VALUES(@MSG_CATEGORY_ID,@MSG_CATEGORY,@USER_ID,@COMPANY_ID)";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@MSG_CATEGORY_ID",Utilities.GetMd5Hash(DateTime.UtcNow.Ticks+this.UserId+this.CompanyId));
                cmd.Parameters.AddWithValue("@MSG_CATEGORY",this.MessageCategory);
                cmd.Parameters.AddWithValue("@USER_ID", this.UserId);
                cmd.Parameters.AddWithValue("@COMPANY_ID",this.CompanyId);
                if (MSSqlClient.ExecuteNonQueryRecord(cmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                }
            }
            catch
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
            }

        }


        public string CompanyId
        {
            get;
           private set;
        }
        public string StatusDescription
        {
            get;
            private set;
        }
        public int StatusCode
        {
            get;
            private set;
        }
        
        public string MessageCategory
        {
            get;
            private set;
        }
        public string UserId
        {
            get;
            private set;
        }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
namespace mFicientCP
{
    #region Enums
    public enum TRIAL_ACCOUNT_VALIDATION
    {
        MAX_SUBADMIN = 1,
        MAX_USER = 2,
        MAX_FORM = 5,
        MAX_WORKFLOW = 2,
        VALIDITY = 30,
    }
    public enum REGISTRATION_DEVICE_REQUEST_TYPE
    {
        ALLOW_USER_DEVICE = 1,
        DELETE_AND_APPROVE = 2,
        DELETE_DEVICES_OF_OTHER_USER_AND_ALLOW_NEW = 3,
        DENY = 4,
        DELETE_REGISTERED_USER_DEVICE = 5,
    }
    public enum DATABASE_ERRORS//9000000
    {
        DATABASE_CONNECTION_ERROR = 9000001,
        RECORD_NOT_FOUND_ERROR = 9000002,
        RECORD_INSERT_ERROR = 9000003,
        RECORD_DELETE_ERROR = 9000004,
        USER_ISNOT_ADMIN = 9000005,
    }
    public enum USERLOGIN_ERRORS
    {
        USER_ALREADY_LOGGED_ERROR = 1100201,
        EMAIL_OR_PASSWORD_ERROR = 1100202,
        PASSWORD_ERROR = 1100203,
    }
    public enum UPDATE_TYPE
    {
        COMPANY_LOGO_UPDATE = 1,
        USER_DETAILS_UPDATE = 2,
        MENU_CATEGORY_UPDATE = 3,
    }
    public enum SYSTEM_ERRORS//9100000
    {
        VALIDATION_ERROR = 9100000
    }
    public enum REGISTERUSER_ERRORS
    {
        EMAIL_ALREADY_EXISTS = 1000101
    }
    public enum USER_ROLES
    {
        ADMIN = 1,
        SUBADMIN = 2
    }
    public enum PLAN_RENEW_OR_CHANGE_REQUESTS
    {
        RENEW = 1,
        USERS_ADDED = 2,
        PLAN_UPGRADE = 3,
        PLAN_DOWNGRADE = 4,
        PLAN_PURCHASE = 5,
        TRIAL = 6,
        USERS_REMOVED = 7,
        PLAN_CHANGE = 8,
        ADD_BALANCE_AMOUNT = 9,
    }
    public enum PLAN_REQUEST_STATUS
    {
        UNAPPROVED = 0,
        RESELLER_APPROVED = 1,
        SALES_APPROVED = 2,
        ACCOUNTS_APPROVED = 3,
        CANCELLED = 4,
        RESELLER_DENIED = 5,
        SALES_DENIED = 6,
        ACCOUNTS_DENIED = 7,
    }
    public enum DIALOG_TYPE
    {
        None,
        Error,
        Warning,
        Info
    }
    public enum USER_LOG_OUT_TYPE
    {
        NORMAL = 1,
        LOGGED_IN_AGAIN,
        USER_DELETED,
        USER_BLOCKED,
        DEVICE_DELETED
    }
    public enum MODAL_POP_UP_NAME
    {
        //getModalPopUpWidth is used to return the width of the pop up
        USER_DETAIL,
        DEVICE_DETAILS_REPEATER,
        SUB_ADMIN_DETAILS,
        MANAGE_GROUP_POPUPS,
        POST_BACK_CONFIRMATION,
        CHANGE_PASSWORD,
    }
    public enum HTTP_TYPE
    {
        GET = 1,
        POST = 2,
        PUT = 3,
        PATCH = 4,
        MERGE = 5,
        DELETE = 6
    }
    public enum MPlugInChangesType
    {
        REFRESH_CONNECTORS = 4,
        CHANGE_PASSWORD = 5
    }
    //public enum SUBADMIN_ACTIVITY_LOG
    //{
    //    ALL = 0,
    //    MOBILE_USER_ADD = 1,
    //    MOBILE_USER_UPDATE = 2,
    //    MOBILE_USER_DELETE = 3,
    //    MOBILE_DEVICE_APPROVE = 4,
    //    MOBILE_DEVICE_DELETE = 5,
    //    MOBILE_DEVICE_DENY = 6,
    //    MOBILE_DEVICE_DELETE_APPROVE = 12,
    //    ADD_GROUP = 7,
    //    UPDATE_GROUP = 8,
    //    ADD_MPLUGIN = 9,
    //    UPDATE_MPLUGIN = 10,
    //    PUBLISH_APP = 11,
    //    UNPUBLISH_APP = 14,
    //    DELETE_GROUP = 13,
    //}
   
    public enum SUBADMIN_Device_LOG
    {
        ALL = 14,
        MOBILE_DEVICE_APPROVE = 4,
        MOBILE_DEVICE_DELETE = 5,
        MOBILE_DEVICE_DENY = 6,
        MOBILE_DEVICE_DELETE_APPROVE = 12,
    }
 
    public enum DatabaseType
    {
        MSSQL = 1,
        ORACLE = 2,
        MYSQL = 3,
        POSTGRESQL = 4,
        ODBCDSN = 5,
    }
    public enum LogOutReason
    {
        NORMAL,
        SESSION_EXPIRED,
        LOGGED_AGAIN,
    }

    public enum DEVICE_OR_ICON_SIZE
    {
        SMALL = 0,
        MEDIUM = 1,
        LARGE = 2,

    }

    public enum CURRENCY_TYPE
    {
        INR,
        USD
    }

    //public enum COOKIE_TYPES
    //{ 
    //    COMPANY_ID_USER_NAME=1,
    //    SESSION_VALUE=2,
    //}

    public enum MBUZZ_FUNCTION_CODES
    {
        ADD_UPDATE_COMPANY = 11001,
        REMOVE_COMPANY = 11002,
        DELETE_USERS_PENDING_MESSAGE_AND_REPORT = 11003,
    }
    public enum MFICIENT_USER_TYPE
    {
        CloudUser = 0,
        ActiveDirectoryUser = 1,
    }
    public enum MetadataType
    {
        TABLES,
        COLUMNS,
        ALL
    }
    public enum SUPPORT_QUERIES_COMMENTER
    {
        User = 1,
        SubAdmin = 2,
        Admin = 3
    }
    public enum RESET_PASSWORD_REQUEST_BY
    {
        SubAdmin,
        Admin,
        User
    }
    public enum COMMAND_TYPE
    {
        DatabaseCommand,
        Wsdl,
        Http,
        XmlRpc,
        Odata
    }
    public enum WebserviceType
    {
        HTTP = 1,
        WSDL_SOAP = 2,
        RPC_XML = 3
    }

    public enum FileType
    {
        Other = -1,
        Root = 0,
        Script = 1,
        Style = 2,
        Image = 3,
        Zip = 4
    }

    public enum DatabaseCommandType
    {
        SELECT = 1,
        INSERT = 2,
        UPDATE = 3,
        DELETE = 4
    }

    public enum APP_MODEL_TYPE
    {
        Phone =0,
        Tablet=1
    }
    public enum APP_RUNS_ON
    {
        PhoneAndTablet = 0,
        Tablet = 1
    }
    public enum APP_DESIGN_MODE
    {
        Phone = 0,
        Tablet = 1,
        Both = 2
    }

    public enum OFFLINE_DT_SYNC_ON_TYPE
    {
        OnLogin,
        OnAppStartUp,
        Manual
    }
    public enum OFFLINE_TABLE_TYPE
    {
        UploadData,
        DownloadData,
        DownloadAndUploadData
    }
    public enum DB_COMMAND_TYPE
    {
        Select = 1,
        Insert = 2,
        Update = 3
    }

    public enum Different_Type_Meta
    {
        ALL = 0,
        TABLES = 1,
        TABLE_COLUMNS = 2,
        TABLE_AND_COLUMNS = 3,
        VIEWS = 4,
        VIEW_COLUMNS = 5,
        VIEWS_AND_COLUMNS = 6,
        STORED_PROCEDURES = 7,
        STORED_PROCEDURE_PARAMS = 8
    }

    public enum DisplayView
    {
        Mobile = 1,
        Tablet = 2,
    }
    public enum DATABINDING_OBJECT_TYPE
    {
        None  = 0,
        Database = 1,
        Wsdl= 2,
        Http = 3,
        XmlRpc = 4,
        OData = 5,
        Offline_Data_Obj = 6,
        Offline_Tables = 7
    }

    public enum BucketType
    {
        MFICIENT_WEBSERVICE = 1,
        MPLUGIN_AGENT = 2,
        MBUZZ_DESKTOP = 3,
        MFICIENT_ANDROID = 4,
        IOS_DEVICE = 5,
    }
    public enum HTML5_FILE_TYPES
    {
        Image = 1,
        Html = 2,
        Js = 3,
        Css = 4,
        Txt = 5,
        Xml = 6,
        Zip = 7,
        Htm=8
    }
    public enum MF_APP_TYPE
    {
        StandardApp = 1,
        Html5App = 2
    }
    public enum MF_STANDARD_APP_TYPE
    {
        Online = 0,
        Offline = 1
    }
    public enum Html5AppFileType
    {
        Image = 1,
        Html,
        Js,
        Css,
        Txt,
        Xml,
        Zip
    }
    #endregion
    public class MficientConstants
    {
        internal const int EMAIL_TEMPLATE_CACHE_SECONDS = 3600;
        internal const int SESSION_VALIDITY_SECONDS = 5400;//900
        internal const int SESSION_TIMEOUT_SECONDS = 7200;//1800
        internal const string ENTERPRISE_URL = @"https://enterprise.mficient.com";//1800
        internal const string IDE_APPLICATION_ICON_PATH = "//enterprise.mficient.com/images/icon";
        internal const string IDE_APPLICATION_DEFAULT_ICON = "GRAY0207.png";
        internal const Int32 IDE_MODEL_TYPE_PHONE = 0;
        internal const Int32 IDE_MODEL_TYPE_TABLET = 1;
        internal const string WEB_DEVICE_TYPE_AND_ID = "WEB";
        internal const int MAXIMUM_COMITTED_VERSIONS = 15;
        internal const string MBUZZ_SERVER_URL = @"mbuzz.mficient.net";
        internal const string COOKIE_MSID = @"MFMSID";
        internal const string COOKIE_MUEID = @"MFMUEID";
        internal const string USER_NAME_IN_COOKIE = @"unm";
        internal const string COMPANY_NAME_IN_COOKIE = @"cmp";
        internal const string COMPANY_LOGO_IMAGE_URL_PREFIX = @"https://enterprise.mficient.com/CompanyImages/";
        internal const string CACHE_ADMIN_ROLE = @"ADMIN";
        internal const string CACHE_SUB_ADMIN_ROLE = @"SUBADMIN";
        internal const string MF_S3_PUBLIC_BUCKET_NAME = @"mficient-public";
        internal const string MF_S3_MEDIA_FILES_BUCKET_NAME = @"mficient-images";


        internal const string ERROR_DATABASE_CONNECTION = @"Database server connection fail";
    }
    public class MficientStringsUsed 
    {
        internal const string AppDesignInterfacePhone = "Phone";
        internal const string AppDesignInterfaceTablet = "Tablet";
        internal const string AppDesignInterfacePhoneAndTablet = "Phone & Tablet";
    }
    public enum Amzondropdown
    {
        Unknown = 0,
        USEast1 = 1,
        USWest1 = 2,
        USWest2 = 3,
        EUWest1 = 4,
        EUCentral1 = 5,
        APSoutheast1 = 6,
        APNortheast1 = 7,
        APSoutheast2 = 8,
        SAEast1 = 9,
        CNNorth1 = 10
    }
    public enum AmzonType
    {
        NormalUser = 1,
        CognitoPool = 2
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientCP
{
    public class GetActiveDirectoryUserDetailsResp
    {
        string _code, _description;
        ActiveDirectoryUser _data;

        public string Description
        {
            get { return _description; }
        }

        public string Code
        {
            get { return _code; }
        }

        public ActiveDirectoryUser Data
        {
            get { return _data; }
        }

        public int StatusCode
        {
            get;
            set;
        }

        public string StatusDescription
        {
            get;
            set;
        }

        public GetActiveDirectoryUserDetailsResp(string json)
        {
            this.StatusCode = -1000;
            try
            {
                ActiveDirectoryUserDetailsResponse objActiveDirectoryUserDetailsResponseParsing = Utilities.DeserialiseJson<ActiveDirectoryUserDetailsResponse>(json);
                _code = objActiveDirectoryUserDetailsResponseParsing.resp.cd;
                _description = objActiveDirectoryUserDetailsResponseParsing.resp.desc;
                if (Convert.ToInt32(_code) == 0)
                {
                    _data = objActiveDirectoryUserDetailsResponseParsing.resp.data;
                }
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }
    }

    [DataContract]
    public class ActiveDirectoryUserDetailsResponse
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public ActiveDirectoryUserDetailsRespFields resp { get; set; }
    }

    [DataContract]
    public class ActiveDirectoryUserDetailsRespFields
    {

        [DataMember]
        public string cd { get; set; }

        [DataMember]
        public string desc { get; set; }

        [DataMember]
        public ActiveDirectoryUser data { get; set; }
    }

    [DataContract]
    public class ActiveDirectoryUser
    {

        internal ActiveDirectoryUser()
        {
        }

        [DataMember]
        public string fnm
        {
            get;
            set;
        }

        [DataMember]
        public string lnm
        {
            get;
            set;
        }

        [DataMember]
        public string unm
        {
            get;
            set;
        }

        [DataMember]
        public string dpt
        {
            get;
            set;
        }

        [DataMember]
        public string dsg
        {
            get;
            set;
        }

        [DataMember]
        public string email
        {
            get;
            set;
        }

        [DataMember]
        public string mbl
        {
            get;
            set;
        }
    }
}
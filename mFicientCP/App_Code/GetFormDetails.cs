﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Data.SqlClient;
//using System.Data;

//namespace mFicientCP
//{
//    public class GetFormDetails
//    {
//        public GetFormDetails()
//        {

//        }

//        public GetFormDetails(Boolean _IsSelectAll, Boolean _IsCheckIdAndName, string _FormId, string _SubAdminId, string _FormName,string _ParentId, string _CompanyID)
//        {
//            this.FormId = _FormId;
//            this.FormName = _FormName;
//            this.UserId = _SubAdminId;
//            this.IsSelectAll = _IsSelectAll;
//            this.IsCheckIdAndName = _IsCheckIdAndName;
//            this.CompanyId = _CompanyID;
//            this.ParentId = _ParentId;
//        }
//        public void Process()
//        {
//            try
//            {
//                this.StatusCode = -1000;
//                string query;
//                DataSet objDataSet;
//                SqlCommand objSqlCommand;
//                if (this.IsSelectAll == true)
//                {
//                    query = @"SELECT  * FROM  TBL_FORM_DETAILS fd WHERE fd.CREATED_BY=@SUBADMIN_ID AND fd.PARENT_ID=@PARENT_ID AND COMPANY_ID=@COMPANY_ID ORDER BY fd.FORM_NAME;";
//                    objSqlCommand = new SqlCommand(query);
//                    objSqlCommand.CommandType = CommandType.Text;
//                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.UserId);
//                    objSqlCommand.Parameters.AddWithValue("@PARENT_ID", this.ParentId);
//                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    
//                }
//                else if (IsCheckIdAndName)
//                {
//                    query = @"SELECT  * FROM  TBL_FORM_DETAILS fd  WHERE fd.FORM_ID !=@FORM_ID AND fd.FORM_NAME =@FORM_NAME AND fd.CREATED_BY=@SUBADMIN_ID  AND fd.PARENT_ID=@PARENT_ID AND COMPANY_ID=@COMPANY_ID ORDER BY fd.FORM_NAME;";
//                    objSqlCommand = new SqlCommand(query);
//                    objSqlCommand.CommandType = CommandType.Text;
//                    objSqlCommand.Parameters.AddWithValue("@FORM_ID", this.FormId);
//                    objSqlCommand.Parameters.AddWithValue("@FORM_NAME", this.FormName);
//                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.UserId);
//                    objSqlCommand.Parameters.AddWithValue("@PARENT_ID", this.ParentId);
//                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                }
//                else if (this.FormName.Length == 0)
//                {
//                    query = @"SELECT  * FROM  TBL_FORM_DETAILS fd  WHERE fd.FORM_ID =@FORM_ID AND fd.CREATED_BY=@SUBADMIN_ID AND fd.PARENT_ID=@PARENT_ID AND COMPANY_ID=@COMPANY_ID  ORDER BY fd.FORM_NAME;";
//                    objSqlCommand = new SqlCommand(query);
//                    objSqlCommand.CommandType = CommandType.Text;
//                    objSqlCommand.Parameters.AddWithValue("@FORM_ID", this.FormId);
//                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.UserId);
//                    objSqlCommand.Parameters.AddWithValue("@PARENT_ID", this.ParentId);
//                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                }

//                else
//                {
//                    if (this.FormId.Length == 0)
//                    {
//                        query = @"SELECT  * FROM  TBL_FORM_DETAILS fd WHERE fd.FORM_NAME =@FORM_NAME AND fd.CREATED_BY=@SUBADMIN_ID  AND fd.PARENT_ID=@PARENT_ID AND COMPANY_ID=@COMPANY_ID;";
//                        objSqlCommand = new SqlCommand(query);
//                        objSqlCommand.CommandType = CommandType.Text;
//                        objSqlCommand.Parameters.AddWithValue("@FORM_NAME", this.FormName);
//                        objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.UserId);
//                        objSqlCommand.Parameters.AddWithValue("@PARENT_ID", this.ParentId);
//                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                    }
//                    else
//                    {
//                        query = @"SELECT  * FROM  TBL_FORM_DETAILS fd WHERE fd.FORM_ID !=@FORM_ID AND fd.FORM_NAME =@FORM_NAME AND fd.CREATED_BY=@SUBADMIN_ID AND fd.PARENT_ID=@PARENT_ID AND COMPANY_ID=@COMPANY_ID ";
//                        objSqlCommand = new SqlCommand(query);
//                        objSqlCommand.CommandType = CommandType.Text;
//                        objSqlCommand.Parameters.AddWithValue("@FORM_ID", this.FormId);
//                        objSqlCommand.Parameters.AddWithValue("@FORM_NAME", this.FormName);
//                        objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.UserId);
//                        objSqlCommand.Parameters.AddWithValue("@PARENT_ID", this.ParentId);
//                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                    }

//                }
//                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
//                this.ResultTable = objDataSet.Tables[0];
//                if (objDataSet.Tables[0].Rows.Count > 0) 
//                    this.StatusCode = 0;
//                else  
//                    this.StatusCode = -1000;
//            }
//            catch 
//            {
//                this.StatusCode = -1000;
//            }
//        }
//        public static DataTable getAllFormDetailsForCmp(string subAdminId, string companyId)
//        {
//            DataTable dtblFormDtl = new DataTable();
//            try
//            {
//                string query = @"SELECT * FROM TBL_FORM_DETAILS
//                                WHERE CREATED_BY =@SUBADMIN_ID AND COMPANY_ID =@COMPANY_ID";
//                SqlCommand objSqlCommand = new SqlCommand(query);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", subAdminId);
//                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
//                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
//                if (objDataSet != null)
//                {
//                    dtblFormDtl = objDataSet.Tables[0];
//                }
//                else
//                {
//                    dtblFormDtl = null;
//                }

//            }
//            catch
//            {
//                dtblFormDtl = null;
//            }
//            return dtblFormDtl;
//        }
//        public string FormId
//        {
//            set;
//            get;
//        }
//        public string FormName
//        {
//            set;
//            get;
//        }
//        public string UserId
//        {
//            set;
//            get;
//        }

//        public Boolean IsSelectAll
//        {
//            set;
//            get;
//        }
//        public DataTable ResultTable
//        {
//            set;
//            get;
//        }
//        public int StatusCode
//        {
//            set;
//            get;
//        }

//        public bool IsCheckIdAndName { get; set; }

//        public string ParentId { get; set; }

//        public string CompanyId { get; set; }
//    }



//    //public class EntityCollection : IEnumerable<Entity>
//    //{ 

//    //}
//}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class AddOdataCommand
    {
       //public AddOdataCommand()
       // { 

       // }

       //public AddOdataCommand(string _CompanyId, string _SubAdminId, string _ConnectorId, string _CommandId, string _CommandName,
       //                        string _ResourceType, string _ResourcePath, string _additionalSingleValuePath, string _HttpType, string _HttpData, string _QueryOption,
       //                        string _InputParamJson, string _OutputParamJson, string _FunctionType, string _ResponseFormat, string _DataJson, string _ReturnType, string _DsPath, string _Description)
       //{
       //    this.CompanyId = _CompanyId;
       //    this.SubAdminId = _SubAdminId;
       //    this.ConnectorId = _ConnectorId;
       //    this.CommandId = _CommandId;
       //    this.CommandName = _CommandName;
       //    this.ResourceType = _ResourceType;
       //    this.ResourcePath = _ResourcePath;
       //    this.AdditionalSingleValuePath = _additionalSingleValuePath;
       //    this.HttpType = _HttpType;
       //    this.HttpData = _HttpData;
       //    this.QueryOption = _QueryOption;
       //    this.InputParamJson = _InputParamJson;
       //    this.OutputParamJson = _OutputParamJson;
       //    this.FunctionType = _FunctionType;
       //    this.ResponseFormat = _ResponseFormat;
       //    this.DataJson = _DataJson;
       //    this.ReturnType = _ReturnType;
       //    this.DsPath = _DsPath;
       //    this.Description = _Description;
       //    Process();
       //}

       public AddOdataCommand(string _CompanyId, string _SubAdminId, string _ConnectorId, string _CommandName,
                                string _ResourceType, string _ResourcePath, string _additionalSingleValuePath, string _HttpType, string _HttpData, string _QueryOption,
                                string _InputParamJson, string _OutputParamJson, string _FunctionType, string _ResponseFormat, string _DataJson, string _ReturnType, string _DsPath, string _Description, int _Cache, int _Expfrequency, string _ExpCondtion, string _CreatedBy, long _CreatedOn, long _UpdatedOn, string _Xreferences)
        {
            this.CompanyId = _CompanyId;
            this.SubAdminId = _SubAdminId;
            this.ConnectorId = _ConnectorId;
            this.CommandName = _CommandName;
            this.ResourceType = _ResourceType;
            this.ResourcePath = _ResourcePath;
            this.AdditionalSingleValuePath = _additionalSingleValuePath;
            this.HttpType = _HttpType;
            this.HttpData = _HttpData;
            this.QueryOption = _QueryOption;
            this.InputParamJson = _InputParamJson;
            this.OutputParamJson = _OutputParamJson;
            this.FunctionType = _FunctionType;
            this.ResponseFormat = _ResponseFormat;
            this.DataJson = _DataJson;
            this.ReturnType = _ReturnType;
            this.DsPath = _DsPath;
            this.Description = _Description;
            this.Cache = _Cache;
            this.Expfrequency = _Expfrequency;
            this.ExpCondtion = _ExpCondtion;
            this.CreatedBy = _CreatedBy;
            this.CreatedOn = _CreatedOn;
            this.UpdatedOn = _UpdatedOn;
            this.Xreference = _Xreferences;
            Process1();
        }



       public AddOdataCommand(string _SubAdminId, string _CommandName, string DbCommandId, string _CompanyId, long _UpdatedOn)
       {
           this.SubAdminId = _SubAdminId;
           this.CommandName = _CommandName;
           this.CommandId = DbCommandId;
           this.CompanyId= _CompanyId;
           this.UpdatedOn = _UpdatedOn;
           CopyOdataObjectProcess();

       }

       public void CopyOdataObjectProcess()
       {

           try
           {
               this.StatusCode = -1000;

               this.CopyCommandId = Utilities.GetMd5Hash(this.SubAdminId + this.CommandName + DateTime.UtcNow.Ticks.ToString());
               string query = @"INSERT INTO  TBL_ODATA_COMMAND (COMPANY_ID,SUBADMIN_ID,ODATA_CONNECTOR_ID,ODATA_COMMAND_ID,ODATA_COMMAND_NAME,RESOURCE_TYPE,RESOURCE_PATH,HTTP_TYPE,HTTP_DATA,QUERY_OPTION,INPUT_PARAMETER_JSON,OUTPUT_PARAMETER_JSON,FUNCTION_TYPE,RESPONSE_FORMAT,DATA_JSON,DATASET_PATH,RETURN_TYPE,ADDITIONAL_RESOURCE_PATH,DESCRIPTION,CACHE,EXPIRY_FREQUENCY,EXPIRY_CONDITION,X_REFERENCES,CREATED_BY,CREATED_ON,UPDATED_ON)
                select COMPANY_ID,SUBADMIN_ID,ODATA_CONNECTOR_ID,@COPYDB_COMMAND_ID,@ODATA_COMMAND_NAME,RESOURCE_TYPE,RESOURCE_PATH,HTTP_TYPE,HTTP_DATA,QUERY_OPTION,INPUT_PARAMETER_JSON,OUTPUT_PARAMETER_JSON,FUNCTION_TYPE,RESPONSE_FORMAT,DATA_JSON,DATASET_PATH,RETURN_TYPE,ADDITIONAL_RESOURCE_PATH,DESCRIPTION,CACHE,EXPIRY_FREQUENCY,EXPIRY_CONDITION,X_REFERENCES,CREATED_BY,@CREATED_ON,@CREATED_ON from TBL_ODATA_COMMAND   where ODATA_COMMAND_ID=@ODATA_COMMAND_ID;";
//UPDATE TOP(1)  TBL_ODATA_COMMAND  SET ODATA_COMMAND_NAME=@ODATA_COMMAND_NAME,ODATA_COMMAND_ID=@COPYDB_COMMAND_ID,UPDATED_ON=UPDATED_ON,CREATED_ON=@CREATED_ON where ODATA_COMMAND_ID=@ODATA_COMMAND_ID";
               SqlCommand objSqlCommand = new SqlCommand(query);
               objSqlCommand.CommandType = CommandType.Text;
               objSqlCommand.Parameters.AddWithValue("@ODATA_COMMAND_ID", this.CommandId);
               objSqlCommand.Parameters.AddWithValue("@COPYDB_COMMAND_ID", this.CopyCommandId);
               objSqlCommand.Parameters.AddWithValue("@ODATA_COMMAND_NAME", this.CommandName);
               objSqlCommand.Parameters.AddWithValue("@CREATED_ON", this.UpdatedOn);

               if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                   this.StatusCode = 0;
               else
                   this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
           }
           catch (Exception ex)
           {
               if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                   this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
           }



       }





       private void Process1()
       {
           try
           {
               this.StatusCode = -1000;
               this.CommandId = Utilities.GetMd5Hash(this.SubAdminId + this.CommandName + DateTime.UtcNow.Ticks.ToString());
               string query = @"INSERT INTO TBL_ODATA_COMMAND(
                                                    COMPANY_ID,SUBADMIN_ID,ODATA_CONNECTOR_ID,
                                                    ODATA_COMMAND_ID,ODATA_COMMAND_NAME,RESOURCE_TYPE,
                                                    RESOURCE_PATH, HTTP_TYPE,HTTP_DATA,
                                                    QUERY_OPTION,INPUT_PARAMETER_JSON,OUTPUT_PARAMETER_JSON,
                                                    FUNCTION_TYPE,RESPONSE_FORMAT,DATA_JSON,RETURN_TYPE,DATASET_PATH,ADDITIONAL_RESOURCE_PATH, DESCRIPTION,CACHE,EXPIRY_FREQUENCY,EXPIRY_CONDITION,CREATED_BY,CREATED_ON,UPDATED_ON,X_REFERENCES
                                                    )
                                                    VALUES(
                                                    @COMPANY_ID,@SUBADMIN_ID,@ODATA_CONNECTOR_ID,
                                                    @ODATA_COMMAND_ID,@ODATA_COMMAND_NAME,@RESOURCE_TYPE,
                                                    @RESOURCE_PATH,@HTTP_TYPE,@HTTP_DATA,
                                                    @QUERY_OPTION,@INPUT_PARAMETER_JSON,@OUTPUT_PARAMETER_JSON,
                                                    @FUNCTION_TYPE,@RESPONSE_FORMAT,@DATA_JSON,@RETURN_TYPE,@DATASET_PATH, @ADDITIONAL_RESOURCE_PATH, @DESCRIPTION,@CACHE,@EXPIRY_FREQUENCY,@EXPIRY_CONDITION,@CREATED_BY,@CREATED_ON,@UPDATED_ON,@X_REFERENCES);";

               SqlCommand objSqlCommand = new SqlCommand(query);
               objSqlCommand.CommandType = CommandType.Text;
               objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
               objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
               objSqlCommand.Parameters.AddWithValue("@ODATA_CONNECTOR_ID", this.ConnectorId);
               objSqlCommand.Parameters.AddWithValue("@ODATA_COMMAND_ID", this.CommandId);
               objSqlCommand.Parameters.AddWithValue("@ODATA_COMMAND_NAME", this.CommandName);
               objSqlCommand.Parameters.AddWithValue("@RESOURCE_TYPE", this.ResourceType);
               objSqlCommand.Parameters.AddWithValue("@RESOURCE_PATH", this.ResourcePath);
               objSqlCommand.Parameters.AddWithValue("@HTTP_TYPE", this.HttpType);
               objSqlCommand.Parameters.AddWithValue("@HTTP_DATA", this.HttpData);
               objSqlCommand.Parameters.AddWithValue("@QUERY_OPTION", this.QueryOption);
               objSqlCommand.Parameters.AddWithValue("@INPUT_PARAMETER_JSON", this.InputParamJson);
               objSqlCommand.Parameters.AddWithValue("@OUTPUT_PARAMETER_JSON", this.OutputParamJson);
               objSqlCommand.Parameters.AddWithValue("@FUNCTION_TYPE", this.FunctionType);
               objSqlCommand.Parameters.AddWithValue("@RESPONSE_FORMAT", this.ResponseFormat);
               objSqlCommand.Parameters.AddWithValue("@DATA_JSON", this.DataJson);
               objSqlCommand.Parameters.AddWithValue("@RETURN_TYPE", this.ReturnType);
               objSqlCommand.Parameters.AddWithValue("@DATASET_PATH", this.DsPath);
               objSqlCommand.Parameters.AddWithValue("@ADDITIONAL_RESOURCE_PATH", this.AdditionalSingleValuePath);
               objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", this.Description);
               objSqlCommand.Parameters.AddWithValue("@CACHE", this.Cache);
               objSqlCommand.Parameters.AddWithValue("@EXPIRY_FREQUENCY", this.Expfrequency);
               objSqlCommand.Parameters.AddWithValue("@EXPIRY_CONDITION", this.ExpCondtion);
               objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.CreatedBy);
               objSqlCommand.Parameters.AddWithValue("@CREATED_ON", this.CreatedOn);
               objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", this.UpdatedOn);
               objSqlCommand.Parameters.AddWithValue("@X_REFERENCES", this.Xreference);


               if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
           }
           catch 
           {
               this.StatusCode = -1000;
           }
       }

        private void Process()
        {
            try
            {
                this.StatusCode = -1000;
                this.CommandId = Utilities.GetMd5Hash(this.SubAdminId + this.CommandName + DateTime.UtcNow.Ticks.ToString());
                string query = @"INSERT INTO TBL_ODATA_COMMAND(
                                                    COMPANY_ID,SUBADMIN_ID,ODATA_CONNECTOR_ID,
                                                    ODATA_COMMAND_ID,ODATA_COMMAND_NAME,RESOURCE_TYPE,
                                                    RESOURCE_PATH, HTTP_TYPE,HTTP_DATA,
                                                    QUERY_OPTION,INPUT_PARAMETER_JSON,OUTPUT_PARAMETER_JSON,
                                                    FUNCTION_TYPE,RESPONSE_FORMAT,DATA_JSON,RETURN_TYPE,DATASET_PATH,ADDITIONAL_RESOURCE_PATH, DESCRIPTION
                                                    )
                                                    VALUES(
                                                    @COMPANY_ID,@SUBADMIN_ID,@ODATA_CONNECTOR_ID,
                                                    @ODATA_COMMAND_ID,@ODATA_COMMAND_NAME,@RESOURCE_TYPE,
                                                    @RESOURCE_PATH,@HTTP_TYPE,@HTTP_DATA,
                                                    @QUERY_OPTION,@INPUT_PARAMETER_JSON,@OUTPUT_PARAMETER_JSON,
                                                    @FUNCTION_TYPE,@RESPONSE_FORMAT,@DATA_JSON,@RETURN_TYPE,@DATASET_PATH, @ADDITIONAL_RESOURCE_PATH, @DESCRIPTION);";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@ODATA_CONNECTOR_ID", this.ConnectorId);
                objSqlCommand.Parameters.AddWithValue("@ODATA_COMMAND_ID", this.CommandId);
                objSqlCommand.Parameters.AddWithValue("@ODATA_COMMAND_NAME", this.CommandName);
                objSqlCommand.Parameters.AddWithValue("@RESOURCE_TYPE", this.ResourceType);
                objSqlCommand.Parameters.AddWithValue("@RESOURCE_PATH", this.ResourcePath);
                objSqlCommand.Parameters.AddWithValue("@HTTP_TYPE", this.HttpType);
                objSqlCommand.Parameters.AddWithValue("@HTTP_DATA", this.HttpData);
                objSqlCommand.Parameters.AddWithValue("@QUERY_OPTION", this.QueryOption);
                objSqlCommand.Parameters.AddWithValue("@INPUT_PARAMETER_JSON", this.InputParamJson);
                objSqlCommand.Parameters.AddWithValue("@OUTPUT_PARAMETER_JSON", this.OutputParamJson);
                objSqlCommand.Parameters.AddWithValue("@FUNCTION_TYPE", this.FunctionType);
                objSqlCommand.Parameters.AddWithValue("@RESPONSE_FORMAT", this.ResponseFormat);
                objSqlCommand.Parameters.AddWithValue("@DATA_JSON", this.DataJson);
                objSqlCommand.Parameters.AddWithValue("@RETURN_TYPE", this.ReturnType);
                objSqlCommand.Parameters.AddWithValue("@DATASET_PATH", this.DsPath);
                objSqlCommand.Parameters.AddWithValue("@ADDITIONAL_RESOURCE_PATH", this.AdditionalSingleValuePath);
                objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", this.Description);

                
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
            }
            catch 
            {
                this.StatusCode = -1000;
            }
        }

        public string CompanyId { get; set; }
        public string SubAdminId { get; set; }
        public string ConnectorId { get; set; }
        public string CommandId { get; set; }
        public string CopyCommandId
        {
            set;
            get;
        }
        public string CommandName { get; set; }
        public string ResourceType { get; set; }
        public string ResourcePath { get; set; }
        public string AdditionalSingleValuePath { get; set; }

        public string HttpType { get; set; }
        public string HttpData { get; set; }

        public string QueryOption { get; set; }
        public string InputParamJson { get; set; }

        public string OutputParamJson { get; set; }
        public string FunctionType { get; set; }

        public string ResponseFormat { get; set; }
        public string DataJson { get; set; }

        
        public int StatusCode { get; set; }

        public string DsPath { get; set; }

        public string ReturnType { get; set; }

        public string Description { get; set; }

         public int Cache { get; set; }

        public int Expfrequency { get; set; }

        public string ExpCondtion { get; set; }
        public string CreatedBy { get; set; }

        public long CreatedOn { get; set; }

        public long UpdatedOn { get; set; }

        public string Xreference { get; set; }

    }
}
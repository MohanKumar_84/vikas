﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class InsertUserGroupLink
    {
        public InsertUserGroupLink()
        {
        }

        //public void Process(MFEUserGroupLink userGroupLink)
        //{
        //    insertGroupDtls(userGroupLink);
        //}
        //public void Process(SqlConnection con, SqlTransaction transaction, MFEUserGroupLink userGroupLink)
        //{
        //    insertGroupDtls(con, transaction, userGroupLink);
        //}
        //void insertGroupDtls(MFEUserGroupLink userGroupLink)
        //{
        //    string strQuery = getQuery();
        //    SqlCommand cmd = new SqlCommand(strQuery);
        //    cmd.CommandType = CommandType.Text;
        //    cmd.Parameters.AddWithValue("@COMPANY_ID", userGroupLink.EnterpriseId);
        //    cmd.Parameters.AddWithValue("@GROUP_ID", userGroupLink.GroupId);
        //    cmd.Parameters.AddWithValue("@USER_ID", userGroupLink.UserId);
        //    cmd.Parameters.AddWithValue("@CREATED_ON", DateTime.UtcNow.Ticks);

        //    int iRowsEffected = MSSqlClient.ExecuteNonQueryRecord(cmd);
        //    if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
        //}
        //void insertGroupDtls(SqlConnection con, SqlTransaction transaction, MFEUserGroupLink userGroupLink)
        //{
        //    string strQuery = getQuery();
        //    SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
        //    cmd.CommandType = CommandType.Text;
        //    cmd.Parameters.AddWithValue("@COMPANY_ID", userGroupLink.EnterpriseId);
        //    cmd.Parameters.AddWithValue("@GROUP_ID", userGroupLink.GroupId);
        //    cmd.Parameters.AddWithValue("@USER_ID", userGroupLink.UserId);
        //    cmd.Parameters.AddWithValue("@CREATED_ON", DateTime.UtcNow.Ticks);

        //    int iRowsEffected = cmd.ExecuteNonQuery();
        //    if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
        //}

        //public void Process(List<MFEUserGroupLink> userGroupLinks)
        //{
        //    insertGroupDtls(userGroupLinks);
        //}
        public void Process(SqlConnection con,
            SqlTransaction transaction,
            List<MFEUserGroupLink> userGroupLinks)
        {
            insertGroupDtls(con, transaction, userGroupLinks);
        }
                //void insertGroupDtls(List<MFEUserGroupLink> userGroupLinks)
                //{
                //    string strQuery = getQuery();
                //    foreach (MFEUserGroupLink usrGrpLink in userGroupLinks)
                //    {
                //        SqlCommand cmd = new SqlCommand(strQuery);
                //        cmd.CommandType = CommandType.Text;
                //        cmd.Parameters.AddWithValue("@COMPANY_ID", usrGrpLink.EnterpriseId);
                //        cmd.Parameters.AddWithValue("@GROUP_ID", usrGrpLink.GroupId);
                //        cmd.Parameters.AddWithValue("@USER_ID", usrGrpLink.UserId);
                //        cmd.Parameters.AddWithValue("@CREATED_ON", DateTime.UtcNow.Ticks);

                //        int iRowsEffected = MSSqlClient.ExecuteNonQueryRecord(cmd);
                //    }
                //}
        void insertGroupDtls(SqlConnection con,
            SqlTransaction transaction,
            List<MFEUserGroupLink> userGroupLinks)
        {
            string strQuery = @"INSERT INTO TBL_USER_GROUP_LINK(GROUP_ID,USER_ID,CREATED_ON,COMPANY_ID) VALUES(@GROUP_ID,@USER_ID,@CREATED_ON,@COMPANY_ID);";
            foreach (MFEUserGroupLink usrGrpLink in userGroupLinks)
            {
                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", usrGrpLink.EnterpriseId);
                cmd.Parameters.AddWithValue("@GROUP_ID", usrGrpLink.GroupId);
                cmd.Parameters.AddWithValue("@USER_ID", usrGrpLink.UserId);
                cmd.Parameters.AddWithValue("@CREATED_ON", DateTime.UtcNow.Ticks);

                int iRowsEffected = cmd.ExecuteNonQuery();
            }
        }
        #region Public Properties
        //public MFEUserGroupLink UserGroupLink
        //{
        //    get;
        //    private set;
        //}
        //public List<MFEUserGroupLink> UserGroupLinks
        //{
        //    get;
        //    private set;
        //}
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class AppUnpublish
    {
        public AppUnpublish(string _workflowId, string _companyId, string _SubAdminId, string _CurrentVersion)
        {
            this.CompanyId = _companyId;
            this.WorkFlowId = _workflowId;
            this.CurrentVersion = _CurrentVersion;
            this.SubAdminId = _SubAdminId;
        }
        public void Process()
        {
            string wf_name = "";
            try
            {
                StatusCode = -1000;
                string query = @"Select * from dbo.TBL_CURR_WORKFLOW_DETAIL where WF_ID=@WF_ID and COMPANY_ID=@COMPANY_ID";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WorkFlowId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                DataSet ds = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (ds != null && ds.Tables[0].Rows.Count > 0) wf_name = Convert.ToString(ds.Tables[0].Rows[0]["WF_NAME"]);

                query = @"delete from dbo.TBL_CURR_WORKFLOW_DETAIL where WF_ID=@WF_ID and COMPANY_ID=@COMPANY_ID";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WorkFlowId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    Utilities.saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.APP_ROLLBACK, this.CompanyId, this.SubAdminId, this.WorkFlowId, wf_name, this.CurrentVersion, "", "", "", "", "");

                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
                //  throw new Exception(((int)DATABASE_ERRORS.RECORD_Delete_ERROR).ToString());
                this.StatusDescription = "Internal server error";
                this.StatusCode = -1000;
            }
        }

        string WorkFlowId, SubAdminId, CurrentVersion, CompanyId;

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}

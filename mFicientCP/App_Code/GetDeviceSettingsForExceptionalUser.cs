﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetDeviceSettingsForExceptionalUser
    {
        public GetDeviceSettingsForExceptionalUser(string userId, string companyId)
        {
            this.UserId = userId;
            this.CompanyId = companyId;
        }

        public void Process()
        {
            StatusCode = -1000;//for error
            try
            {
                DataSet dsDeviceSettingsForUser = getDeviceStngsForExceptionalUser();
                if (dsDeviceSettingsForUser == null && dsDeviceSettingsForUser.Tables[0].Rows.Count==0)
                {
                    throw new Exception();
                }
                ResultTable = dsDeviceSettingsForUser.Tables[0];
                StatusCode = 0;
                StatusDescription = "";
            }
            catch (Exception e)
            {
                StatusCode = -1000;
                StatusDescription = e.Message;
            }
        }

        DataSet getDeviceStngsForExceptionalUser()
        {
            string strQuery = @"SELECT COUNT(RegDev.USER_ID) AS ALREADY_REGISTERED, RegDev.USER_ID,RegDev.COMPANY_ID,DevSetngs.MAX_DEVICE,UsrDtl.USER_NAME
                                    FROM TBL_REGISTERED_DEVICE AS RegDev
                                    LEFT OUTER JOIN TBL_USER_DEVICE_SETTINGS AS DevSetngs
                                    ON RegDev.USER_ID = DevSetngs.USER_ID
                                    INNER JOIN TBL_USER_DETAIL UsrDtl
                                    ON UsrDtl.USER_ID = RegDev.USER_ID
                                    WHERE RegDev.USER_ID =@UserId
                                    AND RegDev.COMPANY_ID = @CompanyId GROUP BY RegDev.USER_ID,RegDev.COMPANY_ID,DevSetngs.MAX_DEVICE,UsrDtl.USER_NAME";

            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@UserId", this.UserId);
            DataSet dsDeviseSetngsForUser = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            return dsDeviseSetngsForUser;
        }

        public string CompanyId
        {
            get;
            set;
        }
        public string UserId
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public int StatusCode
        {
            get;
            set;
        }
        public DataTable ResultTable
        {
            get;
            set;
        }
    }
}
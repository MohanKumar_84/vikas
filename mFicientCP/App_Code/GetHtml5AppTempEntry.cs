﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
namespace mFicientCP
{
    public class GetHtml5AppTempEntry
    {
        public GetHtml5AppTempEntry(string subAdminId, string companyId)
        {
            this.SubadminId = subAdminId;
            this.CompanyId = companyId;
        }
        /// <summary>
        /// Sets the Result Table and Html5AppTempFile if App exists
        /// </summary>
        /// <param name="appName"></param>
        public void GetHtml5AppTempFileByPath(string path,string appId,string sessionId)
        {
            try
            {
                DataTable dtblTempFile = getHtml5AppTempFile(appId,sessionId,path);
                this.ResultTable = dtblTempFile;
                MFEHtml5AppTempFileDetail objAppTempFileDtl = null;
                if (this.ResultTable.Rows.Count > 0)
                {
                    DataRow row = this.ResultTable.Rows[0];//should get only one record.
                    objAppTempFileDtl = this.getHtml5AppTempFileFromDataRow(row);
                }
                this.Html5AppTempFile = objAppTempFileDtl;
            }
            catch (DataSetNullException)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appName"></param>
        /// <returns></returns>
        /// <exception cref="MficientException">Thrown when there is some internal error</exception>
//        public bool DoesHtml5AppWithAppNameExists(string appName)
//        {
//            try
//            {
//                DataTable dtblAppDtl = this.getHtml5AppByName(appName);
//                if (dtblAppDtl == null) throw new DataSetNullException();
//                if (dtblAppDtl.Rows.Count > 0)
//                {
//                    return true;
//                }
//                else
//                {
//                    return false;
//                }
//            }
//            catch
//            {
//                throw new MficientException("Internal server error.");
//            }
//        }
//        public bool DoesHtml5AppWithAppNameExists(string appName,List<string>ignoreIds)
//        {
//            try
//            {
//                string strQuery = "";
//                SqlCommand cmd = new SqlCommand();
//                if (ignoreIds != null && ignoreIds.Count>0)
//                {
//                    string[] parameters = new string[ignoreIds.Count];
//                    for (int i = 0; i < ignoreIds.Count; i++)
//                    {
//                        parameters[i] = "@p" + i;
//                        cmd.Parameters.AddWithValue(parameters[i], ignoreIds[i]);
//                    }
//                    strQuery = @"SELECT * FROM TBL_HTML5_APP_DETAIL
//                                WHERE APP_NAME = @APP_NAME 
//                                AND SUBADMIN_ID = @SUBADMIN_ID
//                                AND COMPANY_ID = @COMPANY_ID
//                                AND APP_ID NOT IN (";
//                    strQuery += string.Join(",", parameters) + ")";
//                    cmd.CommandText = strQuery;

//                }
//                else
//                {
//                     strQuery = @"SELECT * FROM TBL_HTML5_APP_DETAIL
//                                WHERE APP_NAME = @APP_NAME 
//                                AND SUBADMIN_ID = @SUBADMIN_ID
//                                AND COMPANY_ID = @COMPANY_ID";
//                }
//                cmd.CommandType = CommandType.Text;
//                cmd.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
//                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                cmd.Parameters.AddWithValue("@APP_NAME", appName);
//                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(cmd);
//                if (objDataSet == null) throw new DataSetNullException();
//                //this.ResultTable = objDataSet.Tables[0];
//                if (objDataSet.Tables[0].Rows.Count > 0)
//                {
//                    return true;
//                }
//                else
//                {
//                    return false;
//                }
//            }
//            catch
//            {
//                throw new MficientException("Internal server error.");
//            }
//        }
        #region Private Member Region
        DataTable getHtml5AppTempFile(string appId,string sessionId,string filePath)
        {
            string strQuery = @"SELECT * FROM [TBL_TEMP_HTML5_APP]
                                  WHERE COMPANY_ID = @COMPANY_ID
                                  AND SUBADMIN_ID = @SUBADMIN_ID
                                  AND SESSION_ID = @SESSION_ID
                                  AND APP_ID = @APP_ID
                                  AND FILE_PATH = @FILE_PATH
                                  ORDER BY FILE_PATH DESC;";

            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
            objSqlCommand.Parameters.AddWithValue("@SESSION_ID", sessionId);
            objSqlCommand.Parameters.AddWithValue("@FILE_PATH", filePath);
            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            if (objDataSet == null) throw new DataSetNullException();
            //this.ResultTable = objDataSet.Tables[0];
            return objDataSet.Tables[0];
            
        }
//        DataTable getHtml5AppById(string appId)
//        {
//            string strQuery = @"SELECT * FROM TBL_HTML5_APP_DETAIL
//                                WHERE APP_ID = @APP_ID 
//                                AND SUBADMIN_ID = @SUBADMIN_ID
//                                AND COMPANY_ID = @COMPANY_ID;";

//            SqlCommand objSqlCommand = new SqlCommand(strQuery);
//            objSqlCommand.CommandType = CommandType.Text;
//            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
//            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//            objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
//            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
//            if (objDataSet == null) throw new DataSetNullException();
//            //this.ResultTable = objDataSet.Tables[0];
//            return objDataSet.Tables[0];
//        }

        MFEHtml5AppTempFileDetail getHtml5AppTempFileFromDataRow(DataRow dr)
        {
            if (dr == null) throw new ArgumentNullException();
            MFEHtml5AppTempFileDetail objTempFile = new MFEHtml5AppTempFileDetail();
            objTempFile.AppId = Convert.ToString(dr["APP_ID"]);
            objTempFile.CompanyId = Convert.ToString(dr["COMPANY_ID"]);
            objTempFile.SubAdminId = Convert.ToString(dr["SUBADMIN_ID"]);
            objTempFile.LastOpenedDate = Convert.ToString(dr["OPEN_DATETIME"]);
            objTempFile.CdnPath = Convert.ToString(dr["CDN_PATH"]);
            objTempFile.FilePath = Convert.ToString(dr["FILE_PATH"]);
            objTempFile.FileMeta = Convert.ToString(dr["FILE_META"]);
            objTempFile.FileContent = Convert.ToString(dr["FILE_CONTENT"]);
            return objTempFile;
        }
        #endregion
        public string SubadminId
        {
            private set;
            get;
        }
        public string CompanyId
        {
            private set;
            get;
        }
        public int StatusCode
        {
            private set;
            get;
        }
        public string StatusDescription
        {
            private set;
            get;
        }
        public DataTable ResultTable
        {
            get;
            private set;
        }
        public MFEHtml5AppTempFileDetail Html5AppTempFile
        {
            get;
            private set;
        }
    }
    public class GetHtml5AppTempEntries
    {
        public GetHtml5AppTempEntries(string subAdminId, string companyId)
        {
            this.SubadminId = subAdminId;
            this.CompanyId = companyId;
        }
        /// <summary>
        /// Sets the Result Table and Html5AppTempFile if App exists
        /// </summary>
        /// <param name="appName"></param>
        public void GetHtml5AppTempFiles(string appId, string sessionId)
        {
            try
            {
                DataTable dtblTempFile = getHtml5AppTempFiles(appId, sessionId);
                this.ResultTable = dtblTempFile;
                MFEHtml5AppTempFileDetail objAppTempFileDtl = null;
                List<MFEHtml5AppTempFileDetail> lstHtml5AppFiles = new List<MFEHtml5AppTempFileDetail>();
                if (this.ResultTable.Rows.Count > 0)
                {
                    foreach (DataRow row in this.ResultTable.Rows)
                    {
                        objAppTempFileDtl = this.getHtml5AppTempFileFromDataRow(row);
                        lstHtml5AppFiles.Add(objAppTempFileDtl);
                    }
                }
                this.Html5AppTempFiles = lstHtml5AppFiles;
            }
            catch (DataSetNullException)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }

       #region Private Member Region
        DataTable getHtml5AppTempFiles(string appId, string sessionId)
        {
            string strQuery = @"SELECT * FROM [TBL_TEMP_HTML5_APP]
                                WHERE COMPANY_ID = @COMPANY_ID
                                AND SUBADMIN_ID = @SUBADMIN_ID
                                AND SESSION_ID = @SESSION_ID
                                AND APP_ID = @APP_ID;";

            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
            objSqlCommand.Parameters.AddWithValue("@SESSION_ID", sessionId);
            
            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            if (objDataSet == null) throw new DataSetNullException();
            //this.ResultTable = objDataSet.Tables[0];
            return objDataSet.Tables[0];

        }
        
        MFEHtml5AppTempFileDetail getHtml5AppTempFileFromDataRow(DataRow dr)
        {
            if (dr == null) throw new ArgumentNullException();
            MFEHtml5AppTempFileDetail objTempFile = new MFEHtml5AppTempFileDetail();
            objTempFile.AppId = Convert.ToString(dr["APP_ID"]);
            objTempFile.CompanyId = Convert.ToString(dr["COMPANY_ID"]);
            objTempFile.SubAdminId = Convert.ToString(dr["SUBADMIN_ID"]);
            objTempFile.LastOpenedDate = Convert.ToString(dr["OPEN_DATETIME"]);
            objTempFile.CdnPath = Convert.ToString(dr["CDN_PATH"]);
            objTempFile.FilePath = Convert.ToString(dr["FILE_PATH"]);
            objTempFile.FileMeta = Convert.ToString(dr["FILE_META"]);
            objTempFile.FileContent = Convert.ToString(dr["FILE_CONTENT"]);
            return objTempFile;
        }
        #endregion
        public string SubadminId
        {
            private set;
            get;
        }
        public string CompanyId
        {
            private set;
            get;
        }
        public int StatusCode
        {
            private set;
            get;
        }
        public string StatusDescription
        {
            private set;
            get;
        }
        public DataTable ResultTable
        {
            get;
            private set;
        }
        public List<MFEHtml5AppTempFileDetail> Html5AppTempFiles
        {
            get;
            private set;
        }
    }
}
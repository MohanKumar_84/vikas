﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetCategoryAndGroupDtlsByWorkFlowId
    {
        public GetCategoryAndGroupDtlsByWorkFlowId(string workFlowId)
        {
            this.WorkFlowId = workFlowId;
        }
        public void Process()
        {
            try
            {
                StatusCode = -1000;
                string query = @"SELECT * FROM TBL_WORKFLOW_AND_CATEGORY_LINK
                                WHERE WORKFLOW_ID = @WorkFlowId;

                                SELECT * FROM TBL_WORKFLOW_AND_GROUP_LINK
                                WHERE WORKFLOW_ID = @WorkFlowId;

                                SELECT * FROM TBL_WORKFLOW_DETAIL a inner join TBL_WORKFLOW_ADDITIONAL_DETAIL b on a.WF_ID=b.WORKFLOW_ID
                                WHERE WF_ID = @WorkFlowId";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WorkFlowId", this.WorkFlowId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null && objDataSet.Tables.Count > 0)
                {
                    this.ResultTables = objDataSet;
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }

            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }
//        public DataTable GetAppUsersList(string companyId)
//        {
//            try
//            {
//                StatusCode = -1000;
//                string query = @"if exists(select * from TBL_WORKFLOW_AND_CATEGORY_LINK wc inner join TBL_MENU_CATEGORY mc on wc.CATEGORY_ID=mc.MENU_CATEGORY_ID where wc.ALL_GROUPS='true' and mc.ALL_GROUPS='true' and WORKFLOW_ID = @WORKFLOW_ID)   
//                                 select distinct ud.USER_NAME,ud.FIRST_NAME +' '+ud.LAST_NAME as fullname from TBL_USER_DETAIL ud inner join TBL_USER_GROUP_LINK ug on ud.USER_ID=ug.USER_ID and ud.COMPANY_ID=@COMPANY_ID
//
//                else if exists(select 'true' from TBL_WORKFLOW_AND_CATEGORY_LINK wc inner join TBL_MENU_CATEGORY mc on wc.CATEGORY_ID=mc.MENU_CATEGORY_ID where wc.ALL_GROUPS='false' and mc.ALL_GROUPS='true' and WORKFLOW_ID = @WORKFLOW_ID )                                
//                select distinct ud.USER_NAME,ud.FIRST_NAME +' '+ud.LAST_NAME as fullname from TBL_USER_DETAIL ud inner join TBL_USER_GROUP_LINK ug on ud.USER_ID=ug.USER_ID 
//	                inner join TBL_WORKFLOW_AND_GROUP_LINK wg on ug.GROUP_ID=wg.GROUP_ID WHERE WORKFLOW_ID = @WORKFLOW_ID and ud.COMPANY_ID=@COMPANY_ID
//	
//                else if exists(select 'true' from TBL_WORKFLOW_AND_CATEGORY_LINK wc inner join TBL_MENU_CATEGORY mc on wc.CATEGORY_ID=mc.MENU_CATEGORY_ID where wc.ALL_GROUPS='true' and mc.ALL_GROUPS='false' and WORKFLOW_ID = @WORKFLOW_ID) 	
//                select distinct ud.USER_NAME,ud.FIRST_NAME +' '+ud.LAST_NAME as fullname from TBL_USER_DETAIL ud inner join TBL_USER_GROUP_LINK ug on ud.USER_ID=ug.USER_ID and ud.COMPANY_ID=ug.COMPANY_ID
//                inner join TBL_MENU_AND_GROUP_LINK mg on mg.GROUP_ID=ug.GROUP_ID and mg.COMPANY_ID=ug.COMPANY_ID inner join TBL_WORKFLOW_AND_CATEGORY_LINK wc
//                on wc.CATEGORY_ID=mg.MENU_CATEGORY_ID and ud.COMPANY_ID=ug.COMPANY_ID  WHERE WORKFLOW_ID = @WORKFLOW_ID and ud.COMPANY_ID=@COMPANY_ID 
// 
//                 else 
//                  select distinct ud.USER_NAME,ud.FIRST_NAME +' '+ud.LAST_NAME as fullname from TBL_USER_DETAIL ud inner join TBL_USER_GROUP_LINK ug on ud.USER_ID=ug.USER_ID and ud.COMPANY_ID=ug.COMPANY_ID
//                 inner join TBL_MENU_AND_GROUP_LINK mg on mg.GROUP_ID=ug.GROUP_ID and mg.COMPANY_ID=ug.COMPANY_ID inner join TBL_WORKFLOW_AND_CATEGORY_LINK wc on wc.CATEGORY_ID=mg.MENU_CATEGORY_ID and ud.COMPANY_ID=ug.COMPANY_ID
//                 inner join TBL_WORKFLOW_AND_GROUP_LINK wg on wg.GROUP_ID=ug.GROUP_ID and wg.COMPANY_ID=ug.COMPANY_ID WHERE wc.WORKFLOW_ID = @WORKFLOW_ID and ud.COMPANY_ID=@COMPANY_ID ";

//                SqlCommand objSqlCommand = new SqlCommand(query);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@WORKFLOW_ID", this.WorkFlowId);
//                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
//                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
//                if (objDataSet != null && objDataSet.Tables.Count > 0)
//                {
//                    return objDataSet.Tables[0];
//                }
//            }
//            catch (Exception ex)
//            {
//                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
//                {
//                    throw ex;
//                }
//            }
//            return null;
//        }
        public string WorkFlowId
        {
            set;
            get;
        }

        public DataSet ResultTables
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
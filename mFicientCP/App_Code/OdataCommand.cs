﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Data;

namespace mFicientCP
{
    public class OdataCommand
    {
        #region Private Members

        private string commandId, connectorId, endPoint, enterpriseId, resourcePath, singleValueResourcePath, httpData, returnType, datasetPath, commandName, connectionName, dataServiceVersion, addtionalresourcepath,
            queryOptions, inputParaJson, outputParaJson, mPluginAgent, mPluginAgentPwd, userName, password, expirycondition, xreferences, description, datajson, selectType, createdBy, updatedBy, updatedOn, completeHttpType;
        private ResponseType responseType;
        private HTTP_TYPE httpType;
        private ODataConnector oDataConnector;
        private FunctionType functionType;
        private ODataRequestType resourceType;
        List<IdeWsParam> _outputTagPath;
        List<QueryParametersType> lstInputParameters;
        private int cache, expiryfrequency, responseformat = 0;


        #endregion

        #region Constructor

        public OdataCommand()
        {
        }

        public OdataCommand(string _commandId, string _enterpriseId)
        {
            commandId = _commandId;
            enterpriseId = _enterpriseId;
            _outputTagPath = new List<IdeWsParam>();
            lstInputParameters = new List<QueryParametersType>();
        }

        #endregion

        #region Public Properties

        public string Datajson
        {
            get
            {
                return datajson;
            }
            set
            {
                datajson = value;
            }

        }

        public string ConnectorId
        {
            get
            {
                return connectorId;
            }
            set
            {
                connectorId = value;
            }
        }

        public string CommandId
        {
            get
            {
                return commandId;
            }
            set
            {
                commandId = value;
            }
        }

        public string AddtionalResourcepath
        {
            get
            {
                return addtionalresourcepath;
            }
            set
            {
                addtionalresourcepath = value;
            }
        }

        public string InputParajson
        {
            get
            {
                return inputParaJson;
            }
            set
            {
                inputParaJson = value;
            }
        }

        public string EndPoint
        {
            get
            {
                return endPoint;
            }
            set
            {
                endPoint = value;
            }
        }

        public string EnterpriseId
        {
            get
            {
                return enterpriseId;
            }
            set
            {
                enterpriseId = value;
            }

        }

        public string DataServiceVersion
        {
            get
            {
                return dataServiceVersion;
            }
            set
            {
                dataServiceVersion = value;
            }

        }

        public string ResourcePath
        {
            get
            {
                return resourcePath;
            }
            set
            {
                resourcePath = value;
            }

        }

        public string SingleValueResourcePath
        {
            get
            {
                return singleValueResourcePath;
            }
            set
            {
                singleValueResourcePath = value;
            }
        }

        public string HttpData
        {
            get
            {
                return httpData;
            }
            set
            {
                httpData = value;
            }
        }

        public string CommandName
        {
            get
            {
                return commandName;
            }
            set
            {
                commandName = value;
            }
        }

        public string ConnectionName
        {
            get
            {
                return connectionName;
            }
            set
            {
                connectionName = value;
            }
        }

        public string QueryOptions
        {
            get
            {
                return queryOptions;
            }
            set
            {
                queryOptions = value;
            }
        }

        public List<QueryParametersType> InputParaTypes
        {
            get
            {
                return lstInputParameters;
            }
            set
            {
                lstInputParameters = value;
            }
        }

        public string OutputParaJson
        {
            get
            {
                return outputParaJson;
            }
            set
            {
                outputParaJson = value;
            }
        }

        public string MPluginAgent
        {
            get
            {
                return mPluginAgent;
            }
            set
            {
                mPluginAgent = value;
            }
        }

        public string MPluginAgentPwd
        {
            get
            {
                return mPluginAgentPwd;
            }
            set
            {
                mPluginAgentPwd = value;
            }
        }

        public string Xreference
        {
            get
            {
                return xreferences;
            }
            set
            {
                xreferences = value;
            }
        }

        public string Usage
        {
            get;
            set;
        }

        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }

        public ResponseType Responsetype
        {
            get
            {
                return responseType;
            }
            set
            {
                responseType = value;
            }
        }

        public HTTP_TYPE Httptype
        {
            get
            {
                return httpType;
            }
            set
            {
                httpType = value;
            }
        }

        public string SelectType
        {
            get
            {
                return selectType;
            }
            set
            {
                selectType = value;
            }
        }

        public ODataConnector OdataConnector
        {
            get
            {
                try
                {
                    oDataConnector = new ODataConnector(connectorId, enterpriseId);
                    oDataConnector.GetConnector();
                }
                catch
                {
                }
                return oDataConnector;
            }
        }

        public FunctionType Functiontype
        {
            get
            {
                return functionType;
            }
            set
            {
                functionType = value;
            }

        }

        public ODataRequestType Resourcetype
        {
            get
            {
                return resourceType;
            }
            set
            {
                resourceType = value;
            }
        }

        public string ReturnType
        {
            get
            {
                return returnType;
            }
            set
            {
                returnType = value;
            }
        }

        public string DataSetPath
        {
            get
            {
                return datasetPath;
            }
            set
            {
                datasetPath = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public int Cache
        {
            get
            {
                return cache;
            }
            set
            {
                cache = value;
            }

        }

        public int Expiryfrequency
        {
            get
            {
                return expiryfrequency;
            }
            set
            {
                expiryfrequency = value;
            }
        }

        public string ExpiryCondtion
        {
            get
            {
                return expirycondition;
            }
            set
            {
                expirycondition = value;
            }

        }

        public int Responseformat
        {
            get
            {
                return responseformat;
            }
            set
            {
                responseformat = value;
            }
        }

        public List<IdeWsParam> OutputTagPath
        {
            get { return _outputTagPath; }
            private set { _outputTagPath = value; }
        }

        public string CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                createdBy = value;
            }

        }

        public string UpdatedBy
        {
            get
            {
                return updatedBy;
            }
            set
            {
                updatedBy = value;
            }

        }

        public string UpdatedOn
        {
            get
            {
                return updatedOn;
            }
            set
            {
                updatedOn = value;
            }

        }

        public string CompleteHttpType
        {
            get
            {
                return completeHttpType;
            }
            set
            {
                completeHttpType = value;
            }

        }

        #endregion

        #region Public Methods

        public void GetCommandDetails()
        {
            string strQuery = @"SELECT  cmd.*,con.*,isnull(ag.MP_AGENT_PASSWORD, '') as MP_AGENT_PASSWORD,isnull(con.MPLUGIN_AGENT,'') as MPLUGIN_AGENT,
            ss.FULL_NAME,st.FULL_NAME as CreatedBY FROM TBL_ODATA_COMMAND as cmd LEFT JOIN 
            TBL_ODATA_CONNECTION as con on cmd.ODATA_CONNECTOR_ID=con.ODATA_CONNECTOR_ID and cmd.COMPANY_ID=con.COMPANY_ID 
            left outer join TBL_MPLUGIN_AGENT_DETAIL as ag on ag.MP_AGENT_NAME=con.MPLUGIN_AGENT and ag.COMPANY_ID=con.COMPANY_ID
            left outer join  dbo.TBL_SUB_ADMIN as ss on ss.SUBADMIN_ID=cmd.SUBADMIN_ID 
            left outer join dbo.TBL_SUB_ADMIN as st on cmd.CREATED_BY = st.SUBADMIN_ID 
            WHERE ODATA_COMMAND_ID = @ODATA_COMMAND_ID AND con.COMPANY_ID = @COMPANY_ID;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@ODATA_COMMAND_ID", commandId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);
            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    connectorId = ds.Tables[0].Rows[0]["ODATA_CONNECTOR_ID"].ToString();
                    mPluginAgentPwd = ds.Tables[0].Rows[0]["MP_AGENT_PASSWORD"].ToString();
                    mPluginAgent = ds.Tables[0].Rows[0]["MPLUGIN_AGENT"].ToString();
                    endPoint = ds.Tables[0].Rows[0]["ODATA_ENDPOINT"].ToString();
                    dataServiceVersion = ds.Tables[0].Rows[0]["VERSION"].ToString();
                    resourcePath = ds.Tables[0].Rows[0]["RESOURCE_PATH"].ToString();
                    singleValueResourcePath = ds.Tables[0].Rows[0]["ADDITIONAL_RESOURCE_PATH"].ToString();
                    completeHttpType = ds.Tables[0].Rows[0]["HTTP_TYPE"].ToString().Trim();
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["HTTP_TYPE"].ToString().Trim()))
                    {
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["HTTP_TYPE"].ToString().Trim().Split('_')[0]))
                            httpType = (HTTP_TYPE)Convert.ToInt32(ds.Tables[0].Rows[0]["HTTP_TYPE"].ToString().Trim().Split('_')[0]);
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["HTTP_TYPE"].ToString().Trim().Split('_')[1]))
                            selectType = ds.Tables[0].Rows[0]["HTTP_TYPE"].ToString().Trim().Split('_')[1];
                    }
                    resourceType = (ODataRequestType)Convert.ToInt32(ds.Tables[0].Rows[0]["RESOURCE_TYPE"].ToString().Trim());
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["FUNCTION_TYPE"].ToString().Trim())) functionType = (FunctionType)Convert.ToInt32(ds.Tables[0].Rows[0]["FUNCTION_TYPE"].ToString());
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["RESPONSE_FORMAT"].ToString().Trim())) responseType = (ResponseType)Convert.ToInt32(ds.Tables[0].Rows[0]["RESPONSE_FORMAT"].ToString());
                    commandName = ds.Tables[0].Rows[0]["ODATA_COMMAND_NAME"].ToString();
                    connectionName = ds.Tables[0].Rows[0]["CONNECTION_NAME"].ToString();
                    httpData = ds.Tables[0].Rows[0]["HTTP_DATA"].ToString();
                    queryOptions = ds.Tables[0].Rows[0]["QUERY_OPTION"].ToString();
                    inputParaJson = ds.Tables[0].Rows[0]["INPUT_PARAMETER_JSON"].ToString();
                    outputParaJson = ds.Tables[0].Rows[0]["OUTPUT_PARAMETER_JSON"].ToString();
                    returnType = ds.Tables[0].Rows[0]["RETURN_TYPE"].ToString();
                    datasetPath = ds.Tables[0].Rows[0]["DATASET_PATH"].ToString();
                    xreferences = ds.Tables[0].Rows[0]["X_REFERENCES"].ToString();
                    description = ds.Tables[0].Rows[0]["DESCRIPTION"].ToString();
                    cache = Convert.ToInt32(ds.Tables[0].Rows[0]["CACHE"].ToString());
                    expiryfrequency = Convert.ToInt32(ds.Tables[0].Rows[0]["EXPIRY_FREQUENCY"].ToString());
                    expirycondition = ds.Tables[0].Rows[0]["EXPIRY_CONDITION"].ToString();
                    datajson = ds.Tables[0].Rows[0]["DATA_JSON"].ToString();
                    responseformat = Convert.ToInt32(ds.Tables[0].Rows[0]["RESPONSE_FORMAT"].ToString());
                    addtionalresourcepath = ds.Tables[0].Rows[0]["ADDITIONAL_RESOURCE_PATH"].ToString();
                    updatedOn = Utilities.getCompanyLocalFormattedDateTime(CompanyTimezone.CmpTimeZoneInfo, Convert.ToInt64(ds.Tables[0].Rows[0]["UPDATED_ON"]));
                    createdBy = ds.Tables[0].Rows[0]["CreatedBY"].ToString();
                    updatedBy = ds.Tables[0].Rows[0]["FULL_NAME"].ToString();

                    if (!string.IsNullOrEmpty(outputParaJson.Trim()))
                    {
                        MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(outputParaJson));
                        System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(OutputTagPath.GetType());
                        OutputTagPath = serializer.ReadObject(ms) as List<IdeWsParam>;
                    }

                    if (!string.IsNullOrEmpty(inputParaJson.Trim()))
                    {
                        List<QueryParametersType> objRequestParaJsonParsing = Utilities.DeserialiseJson<List<QueryParametersType>>(inputParaJson);
                        lstInputParameters = objRequestParaJsonParsing;
                    }
                    this.oDataConnector = new ODataConnector(connectorId, enterpriseId);
                    oDataConnector.ConnectionName = ds.Tables[0].Rows[0]["CONNECTION_NAME"].ToString();
                    oDataConnector.ServiceEndPoint = ds.Tables[0].Rows[0]["ODATA_ENDPOINT"].ToString();
                    oDataConnector.DataServiceVersion = ds.Tables[0].Rows[0]["VERSION"].ToString();
                    oDataConnector.HttpAuthenticationType = Convert.ToString(ds.Tables[0].Rows[0]["AUTHENTICATION_TYPE"]);
                }

            }
        }

        #endregion

    }

    [DataContract]
    public class QueryParametersType
    {
        [DataMember]
        public string para { get; set; }

        [DataMember]
        public string typ { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;

namespace mFicientCP
{
    public class GetEnterpriseAdditionalDefinition
    {
        string CompanyId, SubAdmin_id;
        public GetEnterpriseAdditionalDefinition(string _CompanyId)
        {
            this.CompanyId = _CompanyId;
            this.SubAdmin_id = string.Empty;
            GetEnterpriseDefinitionProcess();
        }
        public GetEnterpriseAdditionalDefinition(string _CompanyId,string subAdminId)
        {
            this.CompanyId = _CompanyId;
            this.SubAdmin_id = subAdminId;
            GetEnterpriseDefinitionProcess();
        }

        public void GetEnterpriseDefinitionProcess()
        {
            try
            {
                StatusCode = 0;
                SqlCommand objSqlCommand;
                if (this.SubAdmin_id != string.Empty)
                {
                    string query = @"Select * FROM dbo.TBL_ENTERPRISE_ADDITIONAL_DEFINITION WHERE  COMPANY_ID=@COMPANY_ID;
                        SELECT FIRST_NAME+' '+LAST_NAME+' ('+[USER_NAME]+')' as usernm FROM TBL_USER_DETAIL WHERE  COMPANY_ID=@COMPANY_ID order by usernm;
                        SELECT GROUP_NAME FROM TBL_USER_GROUP  WHERE  COMPANY_ID=@COMPANY_ID order by GROUP_NAME;
                        SELECT [SUBADMIN_ID] ,[FULL_NAME] FROM [TBL_SUB_ADMIN] WHERE  COMPANY_ID=@COMPANY_ID and SUBADMIN_ID  !=  @SUBADMIN_ID order by FULL_NAME;";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdmin_id);
                }
                else
                {
                    string query = @"Select * FROM dbo.TBL_ENTERPRISE_ADDITIONAL_DEFINITION WHERE  COMPANY_ID=@COMPANY_ID;
                        SELECT FIRST_NAME+' '+LAST_NAME+' ('+[USER_NAME]+')' as usernm FROM TBL_USER_DETAIL WHERE  COMPANY_ID=@COMPANY_ID order by usernm;
                        SELECT GROUP_NAME FROM TBL_USER_GROUP  WHERE  COMPANY_ID=@COMPANY_ID order by GROUP_NAME;
                        SELECT [SUBADMIN_ID] ,[FULL_NAME] FROM [TBL_SUB_ADMIN] WHERE  COMPANY_ID=@COMPANY_ID order by FULL_NAME;";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                }

                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet!=null)
                {
                    ResultTable = objDataSet.Tables[0];
                    tblUserList = objDataSet.Tables[1];
                    tblUserGroup = objDataSet.Tables[2];
                    tblSubadmin = objDataSet.Tables[3];
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        public List<UserAdditionalProperties> GetUserAmazonAWSProperties()
        {
            List<UserAdditionalProperties> props = new List<UserAdditionalProperties>();
            try
            {
                if (this.StatusCode == 0)
                {
                    if (this.ResultTable != null && this.ResultTable.Rows.Count > 0)
                    {
                        JObject jo = JObject.Parse(Convert.ToString(this.ResultTable.Rows[0]["AMAZON_ATHENTICATION"]));
                        JArray jaAcr = JArray.Parse(jo["acr"].ToString());
                        JArray jaAcp = JArray.Parse(jo["acp"].ToString());
                        foreach (JObject acr in jaAcr)
                        {
                            UserAdditionalProperties obj = new UserAdditionalProperties();
                            obj.name = acr["name"].ToString();
                            obj.tag = acr["id"].ToString();
                            props.Add(obj);
                        }
                        foreach (JObject acp in jaAcp)
                        {
                            UserAdditionalProperties obj = new UserAdditionalProperties();
                            obj.name = acp["name"].ToString();
                            obj.tag = acp["id"].ToString();
                            props.Add(obj);
                        }
                    }
                }
            }
            catch
            {
            }
            return props;
        }
        public string GetUserAdditionalProperties()
        {
            List<string> props = new List<string>();
            try
            {
                if (this.StatusCode == 0)
                {
                    if (this.ResultTable != null && this.ResultTable.Rows.Count > 0)
                    {
                        List<UserAdditionalProperties> objUaps = Utilities.DeserialiseJson<List<UserAdditionalProperties>>(Convert.ToString(this.ResultTable.Rows[0]["ADDITIONAL_PARAMETERS_META"]));
                        foreach (UserAdditionalProperties uap in objUaps)
                        {
                            props.Add("USER_"+uap.tag);
                        }
                    }
                }
            }
            catch
            {
            }
            return Utilities.SerializeJson<List<string>>(props);
        }
        public string GetUserList()
        {
            List<string> lstUser = new List<string>();
            if (this.StatusCode == 0)
            {
                if (this.tblUserList != null && this.tblUserList.Rows.Count > 0)
                {
                    foreach (DataRow dr in tblUserList.Rows)
                    {
                        lstUser.Add(Convert.ToString(dr["usernm"]));
                    }
                }
            }
            return Utilities.SerializeJson<List<string>>(lstUser);
        }
        public string GetUserGroupsList()
        {
            List<string> lstUserGrp = new List<string>();
            if (this.StatusCode == 0)
            {
                if (this.tblUserGroup != null && this.tblUserGroup.Rows.Count > 0)
                {
                    foreach (DataRow dr in tblUserGroup.Rows)
                    {
                        lstUserGrp.Add(Convert.ToString(dr["GROUP_NAME"]));
                    }
                }
            }
            return Utilities.SerializeJson<List<string>>(lstUserGrp);
        }
        public string GetSubadminList(string _SubAdmin_id)
        {
            List<subadminClass> lstSubAdmin = new List<subadminClass>();
            if (this.StatusCode == 0)
            {
                if (this.tblSubadmin != null && this.tblSubadmin.Rows.Count > 0)
                {
                    subadminClass objsubadminClass;
                    foreach (DataRow dr in tblSubadmin.Rows)
                    {
                        if (Convert.ToString(dr["SUBADMIN_ID"]).ToUpper() != _SubAdmin_id.ToUpper())
                        {
                            objsubadminClass = new subadminClass();
                            objsubadminClass.id = Convert.ToString(dr["SUBADMIN_ID"]);
                            objsubadminClass.name = Convert.ToString(dr["FULL_NAME"]);
                            lstSubAdmin.Add(objsubadminClass);
                        }
                    }
                }
            }
            return Utilities.SerializeJson<List<subadminClass>>(lstSubAdmin);
        }
        public DataTable ResultTable
        {
            get;
            set;
        }
        public DataTable tblUserList
        {
            get;
            set;
        }
        public DataTable tblUserGroup
        {
            get;
            set;
        }
        public DataTable tblSubadmin
        {
            get;
            set;
        }
        public string StatusDescription { get; set; }
        public int StatusCode { get; set; }



        [DataContract]
        public class subadminClass
        {
            [DataMember]
            public string id { get; set; }
            [DataMember]
            public string name { get; set; }
        } 

    }

    [DataContract]
    public class UserAdditionalProperties
    {
        [DataMember]
        public string tag { get; set; }
        [DataMember]
        public string name { get; set; }
    }
}
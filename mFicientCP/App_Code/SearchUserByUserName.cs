﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class SearchUserByUserName
    {
        //public enum SORT_TYPE
        //{
        //    ASC,
        //    DESC
        //}
        //public enum REGISTERED_ORDER_BY
        //{
        //    Name = 1,
        //    OS = 2,
        //    RequestDate = 3,
        //}
        //public enum PENDING_ORDER_BY
        //{
        //    Name = 1,
        //    OS = 2,
        //    Devices = 3,
        //    RequestDate = 4,
        //    RequestType = 5
        //}
        public enum REQUEST_TYPE
        {
            Pending,
            Registered
        }
        public SearchUserByUserName()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="subAdminId"></param>
        /// <param name="userName"></param>
        /// <param name="requestType">Pass "Pending" for pending request else pass "Registered"</param>
        public SearchUserByUserName(string companyId, string subAdminId, string searchText, REQUEST_TYPE requestType,REGISTERED_DEVICES_ORDER_BY registeredOrderBy
            ,PENDING_DEVICES_ORDER_BY pendingOrderBy,SORT_TYPE sortType)
        {
            SubAdminId = subAdminId;
            SearchText = searchText;
            CompanyId = companyId;
            RequestType = requestType;
            this.PendingOrderBy = pendingOrderBy;
            this.RegisteredOrderBy = registeredOrderBy;
            this.SortType = sortType;
        }
        public void Process()
        {
            try
            {
                StatusCode = -1000;
                string strQuery = getSqlQueryByRequestType() +
                                    @"
                                                                                

                                    SELECT COUNT(*) AS Total,CurrPlan.MAX_USER
                                    FROM TBL_REGISTERED_DEVICE AS RegDev
                                    INNER JOIN TBL_COMPANY_CURRENT_PLAN AS CurrPlan
                                    ON RegDev.COMPANY_ID = CurrPlan.COMPANY_ID
                                    WHERE RegDev.COMPANY_ID = @CompanyId
                                    AND RegDev.SUBADMIN_ID = @SubAdminId 
                                    GROUP BY CurrPlan.MAX_USER ;

                                    SELECT COUNT(*) AS Total FROM
                                    TBL_DEVICE_REGISTRATION_REQUEST  request
                                    INNER JOIN TBL_USER_DETAIL usr
                                    ON request.USER_ID = usr.USER_ID
                                    WHERE request.COMPANY_ID =@CompanyId
                                    AND usr.SUBADMIN_ID=@SubAdminId
                                    AND STATUS = 0;
                                    
                                    SELECT MAX_USER
                                   FROM TBL_COMPANY_CURRENT_PLAN
                                    WHERE COMPANY_ID = @CompanyId;

                                    SELECT COUNT(*) AS REGISTERED_DEVICE_COUNT,RegDev.USER_ID,
                                    UsrDevSetng.MAX_DEVICE,AccSetng.MAX_DEVICE_PER_USER
                                    FROM TBL_REGISTERED_DEVICE AS RegDev
                                    LEFT OUTER JOIN TBL_DEVICE_REGISTRATION_REQUEST AS DevReq
                                    ON RegDev.USER_ID = DevReq.USER_ID
                                    LEFT OUTER JOIN TBL_USER_DEVICE_SETTINGS AS UsrDevSetng
                                    ON UsrDevSetng.USER_ID=DevReq.USER_ID
                                    LEFT OUTER JOIN TBL_ACCOUNT_SETTINGS AS AccSetng
                                    ON AccSetng.COMPANY_ID = RegDev.COMPANY_ID
                                    WHERE RegDev.COMPANY_ID =  @CompanyId
                                    AND RegDev.SUBADMIN_ID =@SubAdminId
                                    GROUP BY RegDev.USER_ID,UsrDevSetng.MAX_DEVICE,AccSetng.MAX_DEVICE_PER_USER;
                                    
                                    SELECT COUNT(*) AS REGISTERED_DEVICE_COUNT FROM TBL_REGISTERED_DEVICE AS RegDev WHERE RegDev.COMPANY_ID = @CompanyId;
                                     
                                    SELECT COUNT(*) AS REQUEST_COUNT,USER_ID
                                    FROM TBL_DEVICE_REGISTRATION_REQUEST
                                    WHERE COMPANY_ID = @CompanyId
                                    GROUP BY USER_ID;";//this last query added on 22/8/2012.Change in registerDevice.
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SubAdminId", this.SubAdminId);
                //objSqlCommand.Parameters.AddWithValue("@UserName", this.UserName);
                objSqlCommand.Parameters.AddWithValue("@SearchText", this.SearchText);
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                DataSet dsUserDetails = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (dsUserDetails != null)
                {

                    UserDetails = dsUserDetails;
                    StatusCode = 0;
                    StatusDescription = "";

                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }

            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        string getSqlQueryByRequestType()
        {
            string strQuery="";
            switch (this.RequestType)
            { 
                case REQUEST_TYPE.Pending:
                    strQuery= @"SELECT Request.*,usr.SUBADMIN_ID,usr.USER_NAME,usr.FIRST_NAME,usr.LAST_NAME,reqtype.REQUEST_DESCRIPTION,device.DEVICE_ID as 'RegDeviceId'
                                    ,(SELECT REQUEST_ID FROM TBL_MST_REQUEST_TYPE_FOR_DEVICE_REGISTRATION WHERE REQUEST_DESCRIPTION = 'DENY')AS 'DenyId'
                                    
                                    FROM TBL_DEVICE_REGISTRATION_REQUEST AS request
                                    
                                    INNER JOIN TBL_USER_DETAIL AS usr
                                    ON request.USER_ID = usr.USER_ID
                                    
                                    INNER JOIN TBL_MST_REQUEST_TYPE_FOR_DEVICE_REGISTRATION AS reqtype
                                    ON reqtype.REQUEST_ID = request.REQUEST_TYPE
                                    
                                    LEFT OUTER JOIN TBL_REGISTERED_DEVICE as device
                                    ON device.DEVICE_ID = request.DEVICE_ID
                                    
                                    WHERE request.COMPANY_ID = @CompanyId
                                    AND usr.SUBADMIN_ID = @SubAdminId
                                    AND (usr.FIRST_NAME+' '+usr.LAST_NAME LIKE +'%'+ @SearchText+'%' OR request.DEVICE_TYPE+'/'+request.DEVICE_MODEL LIKE +'%'+ @SearchText+'%')
                                    AND STATUS =0 " + " " + "ORDER BY" + " " + getPendingOrderByColumnName()+" "+getSortTypeInString()+";" + "SELECT * FROM TBL_MST_REQUEST_TYPE_FOR_DEVICE_REGISTRATION;";
                    break;
                case REQUEST_TYPE.Registered:
                    strQuery= @"SELECT device.*,usr.USER_NAME,(usr.FIRST_NAME +' '+usr.LAST_NAME)AS FULL_NAME  FROM TBL_REGISTERED_DEVICE device
                                    INNER JOIN TBL_USER_DETAIL as usr
                                    ON usr.USER_ID = device.USER_ID
                                    WHERE device.COMPANY_ID =@CompanyId
                                    AND device.SUBADMIN_ID=@SubAdminId
                                    AND (usr.FIRST_NAME+' '+usr.LAST_NAME LIKE +'%'+ @SearchText+'%' OR device.DEVICE_TYPE+'/'+device.DEVICE_MODEL LIKE +'%'+ @SearchText+'%')
                                    " + " "+"ORDER BY"+" "+getRegisteredOrderByColumnName()+" "+getSortTypeInString()+";";
                    break;
            }
            return strQuery;
        }
        string getRegisteredOrderByColumnName()
        {
            string strOrderByName = "";
            switch (this.RegisteredOrderBy)
            {
                case REGISTERED_DEVICES_ORDER_BY.Name:
                    strOrderByName = "FULL_NAME";
                    break;
                case REGISTERED_DEVICES_ORDER_BY.OS:
                    strOrderByName = "DEVICE_TYPE";
                    break;
                case REGISTERED_DEVICES_ORDER_BY.RequestDate:
                    strOrderByName = "REGISTRATION_DATE";
                    break;
            }
            return strOrderByName;
        }
        string getPendingOrderByColumnName()
        {
            string strOrderByName = "";
            switch (this.PendingOrderBy)
            {
                case PENDING_DEVICES_ORDER_BY.Name:
                    strOrderByName = "FULL_NAME";
                    break;
                case PENDING_DEVICES_ORDER_BY.OS:
                    strOrderByName = "DEVICE_TYPE";
                    break;
                case PENDING_DEVICES_ORDER_BY.RequestDate:
                    strOrderByName = "REQUEST_DATETIME";
                    break;
                case PENDING_DEVICES_ORDER_BY.RequestType:
                    strOrderByName = "REQUEST_DESCRIPTION";
                    break;
            }
            return strOrderByName;
        }
        string getSortTypeInString()
        {
            string strSortType = "";
            switch (this.SortType)
            {
                case SORT_TYPE.ASC:
                    strSortType = "ASC";
                    break;
                case SORT_TYPE.DESC:
                    strSortType = "DESC";
                    break;
            }
            return strSortType;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string SearchText
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public DataSet UserDetails
        {
            set;
            get;
        }
        public REQUEST_TYPE RequestType
        {
            set;
            get;
        }

        public PENDING_DEVICES_ORDER_BY PendingOrderBy
        {
            set;
            get;
        }
        public REGISTERED_DEVICES_ORDER_BY RegisteredOrderBy
        {
            get;
            set;
        }
        public SORT_TYPE SortType
        {
            get;
            set;
        }
    }
}
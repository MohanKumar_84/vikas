﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetOrderedPlanRequests
    {
        string _statusDescription, _companyId;
        int _statusCode;

        public GetOrderedPlanRequests(string companyId)
        {
            this.CompanyId = companyId;
        }
        public MFEPlanOrderRequest getLatestPlanOrderRequest()
        {
            MFEPlanOrderRequest objMfePlanOrderReq = null;
            try
            {
                SqlCommand cmd = cmdForGetLatestPlanOferReq();
                DataSet dsPlanOrderReq = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsPlanOrderReq == null) throw new Exception("Could not get Plan Order Request");
                else
                {
                    this.StatusCode = 0;
                    this.StatusDescription = String.Empty;
                    if (dsPlanOrderReq.Tables[0].Rows.Count > 0)
                    {
                        objMfePlanOrderReq = fillPlanOrderReqFromDataRow(dsPlanOrderReq.Tables[0].Rows[0]);
                    }
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return objMfePlanOrderReq;
        }
        string queryForGetLatestPlanOrderReq()
        {
            return @"SELECT TOP 1 * FROM ADMIN_TBL_PLAN_ORDER_REQUEST AS Req
                    LEFT OUTER JOIN ADMIN_TBL_PLAN_ORDER_REMARKS AS Remark
                    ON Req.REMARKS = Remark.REMARK_CODE
                    WHERE COMPANY_ID = @COMPANY_ID
                    ORDER BY REQUEST_DATETIME
                    DESC";
        }
        SqlCommand cmdForGetLatestPlanOferReq()
        {
            SqlCommand cmd = new SqlCommand(queryForGetLatestPlanOrderReq());
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            return cmd;
        }

        MFEPlanOrderRequest fillPlanOrderReqFromDataRow(DataRow rowDet)
        {
            if (rowDet == null) throw new ArgumentNullException();
            MFEPlanOrderRequest objPlanOrderReq = new MFEPlanOrderRequest();
            objPlanOrderReq.CompanyId = Convert.ToString(rowDet["COMPANY_ID"]);
            objPlanOrderReq.RequestType= Convert.ToString(rowDet["REQUEST_TYPE"]);
            objPlanOrderReq.PlanCode = Convert.ToString(rowDet["PLAN_CODE"]);
            objPlanOrderReq.NoOfUsers = Convert.ToInt32(rowDet["MOBILE_USERS"]);
            objPlanOrderReq.Months = Convert.ToInt32(rowDet["NUMBER_MONTH"]);
            objPlanOrderReq.RequestDateTime = Convert.ToInt64(rowDet["REQUEST_DATETIME"]);
            objPlanOrderReq.Status = Convert.ToInt32(rowDet["STATUS"]);
            objPlanOrderReq.CurrencyType = Convert.ToString(rowDet["CURRENCY_TYPE"]);
            objPlanOrderReq.RemarksCode = Convert.ToString(rowDet["REMARKS"]);
            objPlanOrderReq.RemarksDescription= Convert.ToString(rowDet["REMARK"]);
            return objPlanOrderReq;
        }
        #region Public Properties
        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }
        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }
        #endregion
    }
}
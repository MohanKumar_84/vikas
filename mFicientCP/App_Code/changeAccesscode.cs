﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class changeAccesscode
    {
        int intStatusCode;
        public string UserId
        {
            set;
            get;
        }
        public string OldAccessCode
        {
            set;
            get;
        }
        public string NewAccessCode
        {
            set;
            get;
        }
        public void process()
        {
            try
            {
                SqlCommand objSqlCommand = new SqlCommand(@"if exists (select * from TBL_COMPANY_ADMINISTRATOR WHERE ADMIN_ID=@ADMIN_ID  AND ACCESS_CODE=@ACCESS_CODE)
	            UPDATE TBL_COMPANY_ADMINISTRATOR SET ACCESS_CODE=@NEWACCESSCODE WHERE  ADMIN_ID=@ADMIN_ID  AND ACCESS_CODE=@ACCESS_CODE
                else if exists (select * from TBL_SUB_ADMIN WHERE SUBADMIN_ID=@ADMIN_ID  AND ACCESS_CODE=@ACCESS_CODE)
	            UPDATE TBL_SUB_ADMIN SET ACCESS_CODE=@NEWACCESSCODE WHERE  SUBADMIN_ID=@ADMIN_ID  AND ACCESS_CODE=@ACCESS_CODE;");
                objSqlCommand.CommandType = CommandType.Text;

                objSqlCommand.Parameters.AddWithValue("@ACCESS_CODE", Utilities.GetMd5Hash(OldAccessCode));
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", UserId);
                objSqlCommand.Parameters.AddWithValue("@NEWACCESSCODE", Utilities.GetMd5Hash(NewAccessCode));

                int intExecuteRecordsCount = MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
                if (intExecuteRecordsCount > 0)
                {
                    intStatusCode = 0;
                }
                else
                {
                    intStatusCode = (int)USERLOGIN_ERRORS.PASSWORD_ERROR;
                }
            }
            catch
            {
                intStatusCode = (int)USERLOGIN_ERRORS.PASSWORD_ERROR;
            }
        }
        public int StatusCode
        {
            get
            {
                return intStatusCode;
            }
        }
    }
}
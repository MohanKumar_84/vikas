﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class AddGroup
    {
        public AddGroup()
        {

        }
        public void Process()
        {
            try
            {
                StatusCode = -1000;
                this.GroupId = Utilities.GetMd5Hash(this.SubAdminId + this.GroupName + DateTime.UtcNow.Ticks.ToString());

                string query = @"INSERT INTO TBL_USER_GROUP(GROUP_ID,GROUP_NAME,SUBADMIN_ID,CREATED_ON,COMPANY_ID) VALUES(@GROUP_ID,@GROUP_NAME,@SUBADMIN_ID,@CREATED_ON,@COMPANY_ID);";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@GROUP_ID", this.GroupId);
                objSqlCommand.Parameters.AddWithValue("@GROUP_NAME", this.GroupName);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@CREATED_ON", DateTime.UtcNow.Ticks);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    Utilities.saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.GROUP_ADDED, this.CompanyId, this.SubAdminId, this.GroupId, this.GroupName, "", "", "", "", "", "");
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;

                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        public string GroupId
        {
            set;
            get;
        }
        public string GroupName
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string CompanyId
        {
            get;
            set;
        }
    }
}
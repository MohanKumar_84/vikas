﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
namespace mFicientCP
{
    public class S3Bucket
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bucketType"></param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Thrown when there is some internal error</exception>
        public static MFES3BucketDetails GetS3BucketDetails(BucketType bucketType)
        {
            string strQuery = @"SELECT * FROM TBL_BUCKET_MASTER WHERE ID = @ID";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ID", (Int16)bucketType);
            MFES3BucketDetails objS3Bucket = null;
            try
            {
                DataSet dsBucketDtl =  MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsBucketDtl != null)
                {
                    objS3Bucket = new MFES3BucketDetails();
                    DataTable dtblS3Bucket = dsBucketDtl.Tables[0];
                    if (dtblS3Bucket.Rows.Count > 0)
                    {
                        DataRow row = dtblS3Bucket.Rows[0];
                        objS3Bucket.BucketUserName = Convert.ToString(row["USER_NAME"]);
                        objS3Bucket.AccessKey = Convert.ToString(row["ACCESS_KEY"]);
                        objS3Bucket.SecretAccessKey = Convert.ToString(row["SECRET_ACCESS_KEY"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objS3Bucket;
        }
        public static string GetCurrentHtml5AppZipFolderPath(string subadminId, string companyId, string appId)
        {
            //return companyId + "/apps/html/" + subadminId + "/" + appId + "/working/";
            return companyId + appId + "/current/";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="subadminId"></param>
        /// <param name="companyId"></param>
        /// <param name="appId"></param>
        /// <param name="fileName">complete file name with extension</param>
        /// <returns></returns>
        public static string GetCurrentHtml5AppZipFolderPath(string subadminId, string companyId, string appId,string fileName)
        {
            return GetCurrentHtml5AppZipFolderPath(subadminId, companyId, appId) + fileName;
        }

        public static string GetCompanyMediaFilesFolderPath(string companyId)
        {
            return "companyImages/"+companyId+"/mediaFiles/";
        }
        public static string GetCompanyMediaFilesFolderPath(string companyId,string fileName)
        {
            return GetCompanyMediaFilesFolderPath(companyId) + fileName;
        }
        public static string GetCompleteAmazonS3Path(string bucketName,string uploadKey)
        {
            return @"https://s3-ap-southeast-1.amazonaws.com/" + bucketName+"/" + uploadKey;
        }
    }
}
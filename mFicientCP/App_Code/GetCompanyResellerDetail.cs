﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Data;
//using System.Data.SqlClient;
//namespace mFicientCP
//{
//    public class GetCompanyResellerDetail
//    {
//        #region members
//        string _resellerId, _statusDescription, _emailId;
//        int _statusCode;
//        DataTable _resultTable;
//        #endregion
//        #region Properties
//        public string ResellerId
//        {
//            get { return _resellerId; }
//        }
//        public string StatusDescription
//        {
//            get { return _statusDescription; }
//        }
//        public int StatusCode
//        {
//            get { return _statusCode; }
//        }
//        public string EmailId
//        {
//            get { return _emailId; }
//        }
//        public DataTable ResultTable
//        {
//            get { return _resultTable; }
//        }
//        #endregion
//        public GetCompanyResellerDetail(string resellerId)
//        {
//            _resellerId = resellerId;
//        }

//        public void Process()
//        {
//            try
//            {
//                DataSet dsResellerDetail = getResellerDetails();
//                if (dsResellerDetail == null || dsResellerDetail.Tables[0] == null || dsResellerDetail.Tables[0].Rows.Count == 0)
//                {
//                    _statusCode = -1000;
//                    _statusDescription = "No record found";
//                    return;
//                }
//                _resultTable = dsResellerDetail.Tables[0];
//                _emailId = Convert.ToString(_resultTable.Rows[0]["EMAIL"]);
//            }
//            catch
//            {
//                _statusCode = -1000;
//                _statusDescription = "Internal server error";
//            }
//        }
//        DataSet getResellerDetails()
//        {
//            string strQuery = @"SELECT * FROM ADMIN_TBL_RESELLER_DETAIL
//                                WHERE RESELLER_ID = @RESELLER_ID";

//            SqlCommand cmd = new SqlCommand(strQuery);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@RESELLER_ID", _resellerId);

//            return MSSqlClient.SelectDataFromSQlCommand(cmd);
//        }
//    }
//}
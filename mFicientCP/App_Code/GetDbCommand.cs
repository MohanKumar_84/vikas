﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetDbCommand
    {
        public GetDbCommand()
        {

        }

       public GetDbCommand(Boolean _IsSelectAll, Boolean _IsCmdEdit, string _SubAdminId, string _CommandId, string _CommandName, string _CompanyId)
       {
           this.IsSelectAll = _IsSelectAll;
           this.SubAdminId = _SubAdminId;
           this.CommandId = _CommandId;
           this.CommandName = _CommandName;
           this.IsCmdEdit = _IsCmdEdit;
           this.CompanyId = _CompanyId;
           //his.FindConnectedApp = _FindConnectedApp;
           Process();
       }

       public List<IdeDbCmdsForJson> getCommondDetail()
       {
           List<IdeDbCmdsForJson> lstDbCmds = new List<IdeDbCmdsForJson>();
               
           if (this.ResultTable != null)
           {
               foreach (DataRow row in this.ResultTable.Rows)
               {
                   IdeDbCmdsForJson objDbCmd = new IdeDbCmdsForJson();
                   objDbCmd.compId = Convert.ToString(row["COMPANY_ID"]);
                   objDbCmd.commandName = Convert.ToString(row["DB_COMMAND_NAME"]);
                   objDbCmd.commandType = Convert.ToString(row["DB_COMMAND_TYPE"]);
                   objDbCmd.connectorId = Convert.ToString(row["DB_CONNECTOR_ID"]);
                   objDbCmd.connectionName = Convert.ToString(row["CONNECTION_NAME"]);
                   objDbCmd.connectorId = Convert.ToString(row["DB_CONNECTOR_ID"]);
                   objDbCmd.parameter = Convert.ToString(row["PARAMETER"]);
                   objDbCmd.paramJson = Convert.ToString(row["PARAMETER_JSON"]);
                   objDbCmd.queryType = Convert.ToString(row["QUERY_TYPE"]);
                   objDbCmd.returnType = Convert.ToString(row["RETURN_TYPE"]);
                   objDbCmd.sqlQuery = Convert.ToString(row["SQL_QUERY"]);
                   objDbCmd.tableName = Convert.ToString(row["TABLE_NAME"]);
                   objDbCmd.columns = Convert.ToString(row["COLUMNS"]).Replace(";", "");
                   objDbCmd.description = Convert.ToString(row["DESCRIPTION"]);
                   objDbCmd.commandId = Convert.ToString(row["DB_COMMAND_ID"]);
                   lstDbCmds.Add(objDbCmd);
               }               
           }
           return lstDbCmds;
       }

       public GetDbCommand(Boolean _IsSelectAll, Boolean _IsCmdEdit, string _SubAdminId, string _CommandId, string _CommandName, string _CompanyId, string _NewId)
       {
           this.IsSelectAll = _IsSelectAll;
           this.SubAdminId = _SubAdminId;
           this.CommandId = _CommandId;
           this.CommandName = _CommandName;
           this.IsCmdEdit = _IsCmdEdit;
           this.CompanyId = _CompanyId;
           //this.FindConnectedApp = _FindConnectedApp;
           this.NewId = _NewId;
           Process1();
       }
       private void Process()
       {
           try
           {
               this.StatusCode = -1000;
               string query;
               DataSet objDataSet;
               SqlCommand objSqlCommand;
               if (this.IsSelectAll == true)
               {
                   query = @"SELECT  cn.CONNECTION_NAME, cd.DB_COMMAND_ID,cd.COMPANY_ID,cd.DB_COMMAND_NAME,cd.DB_COMMAND_TYPE,cd.DB_CONNECTOR_ID,cd.PARAMETER,cd.DB_COMMAND_TYPE,CASE  cd.DB_COMMAND_TYPE WHEN '1' THEN 'SELECT' WHEN'2' THEN ' INSERT' WHEN '3' THEN 'UPDATE'  ELSE  'DELETE'
                    END AS DB_COMMAND,cd.PARAMETER_JSON,cd.QUERY_TYPE,cd.RETURN_TYPE,cd.SQL_QUERY,cd.SUBADMIN_ID,cd.TABLE_NAME,cd.COLUMNS,cd.DESCRIPTION,isnull(cd.CREATED_BY,'') as CREATED_BY,isnull(cd.CREATED_ON,0) as CREATED_ON,isnull(cd.UPDATED_ON,0) as UPDATED_ON,
                    ss.FULL_NAME,isnull(st.FULL_NAME,'') as CreatedBY,
                    cn.CONNECTION_NAME FROM  TBL_DATABASE_COMMAND cd INNER JOIN TBL_DATABASE_CONNECTION cn on cn.DB_CONNECTOR_ID=cd.DB_CONNECTOR_ID
                    left outer join  dbo.TBL_SUB_ADMIN as ss on ss.SUBADMIN_ID=cd.SUBADMIN_ID left outer join dbo.TBL_SUB_ADMIN as st on cd.CREATED_BY = st.SUBADMIN_ID
                                  WHERE  cd.COMPANY_ID=@COMPANY_ID ORDER BY cn.CONNECTION_NAME,cd.DB_COMMAND_NAME;";
                   objSqlCommand = new SqlCommand(query);
                   objSqlCommand.CommandType = CommandType.Text;
                   objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

               }
               else
               {
                   if (this.CommandName.Length == 0)
                   {


                       query = @"SELECT  cd.*,cn.CONNECTION_NAME FROM  TBL_DATABASE_COMMAND cd 
                                    LEFT JOIN TBL_DATABASE_CONNECTION cn on cn.DB_CONNECTOR_ID=cd.DB_CONNECTOR_ID
                                        WHERE  cd.DB_COMMAND_ID=@DB_COMMAND_ID AND cd.COMPANY_ID=@COMPANY_ID ORDER BY cd.DB_COMMAND_NAME;";

                       objSqlCommand = new SqlCommand(query);
                       objSqlCommand.CommandType = CommandType.Text;

                       objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_ID", this.CommandId);
                       objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

                   }
                   else
                   {
                       if (IsCmdEdit)
                       {

                           query = @"SELECT  * FROM  TBL_DATABASE_COMMAND WHERE  DB_COMMAND_NAME=@DB_COMMAND_NAME AND DB_COMMAND_ID !=@DB_COMMAND_ID AND COMPANY_ID=@COMPANY_ID ORDER BY DB_COMMAND_NAME;";
                           objSqlCommand = new SqlCommand(query);
                           objSqlCommand.CommandType = CommandType.Text;
                           objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_NAME", this.CommandName);
                           objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_ID", this.CommandId);
                           objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                       }
                       else
                       {

                           //      query = @"SELECT  * FROM  TBL_DATABASE_COMMAND WHERE  DB_COMMAND_NAME=@DB_COMMAND_NAME AND COMPANY_ID=@COMPANY_ID ORDER BY DB_COMMAND_NAME;";  
                           query = @"SELECT cd.*,ss.FULL_NAME,st.FULL_NAME as CreatedBY FROM  TBL_DATABASE_COMMAND as cd  
                                     inner join  dbo.TBL_SUB_ADMIN as ss on ss.SUBADMIN_ID=cd.SUBADMIN_ID
                         inner join dbo.TBL_SUB_ADMIN as st on cd.CREATED_BY = st.SUBADMIN_ID
                          WHERE  cd.DB_COMMAND_NAME=@DB_COMMAND_NAME AND cd.COMPANY_ID=@COMPANY_ID ORDER BY DB_COMMAND_NAME";
                           objSqlCommand = new SqlCommand(query);
                           objSqlCommand.CommandType = CommandType.Text;

                           objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_NAME", this.CommandName);
                           objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                       }
                   }
               }

               objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
               this.ResultTable = objDataSet.Tables[0];
               if (this.ResultTable != null)
                   if (this.ResultTable.Rows.Count != 0)
                       this.StatusCode = 0;

           }
           catch
           {
               this.StatusCode = -1000;
           }
       }

       private void Process1()
       {
           try
           {
               this.StatusCode = -1000;
               string query;
               DataSet objDataSet;
               SqlCommand objSqlCommand;
               if (this.IsSelectAll == true)
               {
                   query = @"SELECT  cn.CONNECTION_NAME, cd.DB_COMMAND_ID,cd.COMPANY_ID,cd.DB_COMMAND_NAME,cd.DB_COMMAND_TYPE,cd.DB_CONNECTOR_ID,cd.PARAMETER,cd.DB_COMMAND_TYPE,CASE  cd.DB_COMMAND_TYPE WHEN '1' THEN 'SELECT' WHEN'2' THEN ' INSERT' WHEN '3' THEN 'UPDATE' WHEN '5' THEN 'STORE PROCEDURE'  ELSE  'DELETE'
                    END AS DB_COMMAND,cd.PARAMETER_JSON,cd.QUERY_TYPE,cd.RETURN_TYPE,cd.SQL_QUERY,cd.SUBADMIN_ID,cd.TABLE_NAME,cd.COLUMNS,cd.DESCRIPTION,isnull(cd.CREATED_BY,'') as CREATED_BY,isnull(cd.CREATED_ON,0) as CREATED_ON,isnull(cd.UPDATED_ON,0) as UPDATED_ON,
                    cd.IMAGE_COLUMN,ss.FULL_NAME,isnull(st.FULL_NAME,'') as CreatedBY,cd.CACHE,cd.EXPIRY_FREQUENCY,cd.EXPIRY_CONDITION,
                    cn.CONNECTION_NAME,cn.DATABASE_TYPE FROM  TBL_DATABASE_COMMAND cd INNER JOIN TBL_DATABASE_CONNECTION cn on cn.DB_CONNECTOR_ID=cd.DB_CONNECTOR_ID
                    left outer join  dbo.TBL_SUB_ADMIN as ss on ss.SUBADMIN_ID=cd.SUBADMIN_ID left outer join dbo.TBL_SUB_ADMIN as st on cd.CREATED_BY = st.SUBADMIN_ID
                                  WHERE  cd.COMPANY_ID=@COMPANY_ID ORDER BY cn.CONNECTION_NAME,cd.DB_COMMAND_NAME;";
                   objSqlCommand = new SqlCommand(query);
                   objSqlCommand.CommandType = CommandType.Text;
                   objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

               }
               else
               {
                   if (this.CommandName.Length == 0)
                   {
                       query = @"SELECT  cd.*,cn.CONNECTION_NAME,cn.DATABASE_TYPE  FROM  TBL_DATABASE_COMMAND cd 
                                    LEFT JOIN TBL_DATABASE_CONNECTION cn on cn.DB_CONNECTOR_ID=cd.DB_CONNECTOR_ID
                                        WHERE  cd.DB_COMMAND_ID=@DB_COMMAND_ID AND cd.COMPANY_ID=@COMPANY_ID ORDER BY cd.DB_COMMAND_NAME;";
                       
                       objSqlCommand = new SqlCommand(query);
                       objSqlCommand.CommandType = CommandType.Text;

                       objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_ID", this.CommandId);
                       objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                   }
                   else
                   {
                       if (IsCmdEdit)
                       {

                           query = @"SELECT  cd.*,cn.CONNECTION_NAME,cn.DATABASE_TYPE  FROM  TBL_DATABASE_COMMAND cd 
                                    LEFT JOIN TBL_DATABASE_CONNECTION cn on cn.DB_CONNECTOR_ID=cd.DB_CONNECTOR_ID  WHERE  DB_COMMAND_NAME=@DB_COMMAND_NAME AND DB_COMMAND_ID !=@DB_COMMAND_ID AND COMPANY_ID=@COMPANY_ID ORDER BY DB_COMMAND_NAME;";
                           objSqlCommand = new SqlCommand(query);
                           objSqlCommand.CommandType = CommandType.Text;
                           objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_NAME", this.CommandName);
                           objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_ID", this.CommandId);
                           objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                       }
                       else
                       {
                           query = @"SELECT cd.*,ss.FULL_NAME,st.FULL_NAME as CreatedBY,cn.CONNECTION_NAME,cn.DATABASE_TYPE   FROM  TBL_DATABASE_COMMAND as cd  
                                     left outer join  dbo.TBL_SUB_ADMIN as ss on ss.SUBADMIN_ID=cd.SUBADMIN_ID
                         left outer join dbo.TBL_SUB_ADMIN as st on cd.CREATED_BY = st.SUBADMIN_ID
                          inner join
                         dbo.TBL_DATABASE_CONNECTION as cn on
                          cn.DB_CONNECTOR_ID=cd.DB_CONNECTOR_ID
                          WHERE  cd.DB_COMMAND_NAME=@DB_COMMAND_NAME AND cd.COMPANY_ID=@COMPANY_ID ORDER BY DB_COMMAND_NAME";
                           objSqlCommand = new SqlCommand(query);
                           objSqlCommand.CommandType = CommandType.Text;

                           objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_NAME", this.CommandName);
                           objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                       }
                   }
               }

               objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
               this.ResultTable = objDataSet.Tables[0];
               if (this.ResultTable != null)
                       this.StatusCode = 0;               

           }
           catch{
               this.StatusCode = -1000;
           }
       }

       private Boolean IsSelectAll { get; set; }

       public DataTable ResultTable { get; set; }


       //public DataTable FormDetails { get; set; }
       //public DataTable AppDetails { get; set; }

       public int StatusCode { get; set; }

       private string SubAdminId { get; set; }

       private string CommandId { get; set; }

       private string CommandName { get; set; }

       private bool IsCmdEdit { get; set; }

       private string CompanyId { get; set; }

      
       public string DBCommandId { get; set; }

       public string NewId { get; set; }
    }
}
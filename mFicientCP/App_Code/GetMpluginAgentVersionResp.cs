﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Data;
using System.Text;


namespace mFicientCP
{
     
    public class GetMpluginAgentVersionResp
    {
        private string _requestId, _CompanyId;
        string _minorVersion, _majorVersion;
      
        public string CompanyId
        {
            get { return _CompanyId; }  
        }
        public string RequestId
        {
            get { return _requestId; } 
        }
        public string Minversion
        {
            get { return _minorVersion; }
        }
        public string Majversion
        {
            get { return _majorVersion; }
        }
        public GetMpluginAgentVersionResp(string Json)
        {
            try
            {
               
                GetMpluginAgentVersionInformationResp objGetMpluginAgentVersionInformation = Utilities.DeserialiseJson<GetMpluginAgentVersionInformationResp>(Json);
                _CompanyId = objGetMpluginAgentVersionInformation.resp.enid;
                _majorVersion = objGetMpluginAgentVersionInformation.resp.data.majver;
                _minorVersion = objGetMpluginAgentVersionInformation.resp.data.minver;
                _requestId = objGetMpluginAgentVersionInformation.resp.rid;
               
            }
            catch 
            {
            }

        }
    

    }
    [DataContract]
    public class GetMpluginAgentVersionInformationResp
    {
        [DataMember]
        public GetMpluginAgentVersionRespJson resp { get; set; }
    }
    [DataContract]
    public class GetMpluginAgentVersionRespJson 
    {
        [DataMember]
        public GetMpluginAgentVersionData data { get; set; }
        /// <summary>
        /// Company Id
        /// </summary>
        [DataMember]
        public string enid { get; set; }
        /// <summary>
        /// Request Id
        /// </summary>
        [DataMember]
        public string rid { get; set; }
       
       
    }
    [DataContract]
    public class GetMpluginAgentVersionData
    {
        [DataMember]
        public string minver { get; set; }
        [DataMember]
        public string majver { get; set; }
    }
   
}
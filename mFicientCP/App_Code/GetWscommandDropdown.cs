﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCP
{
    public class GetWscommandDropdown
    {
        public GetWscommandDropdown()
        { 

        }

        public GetWscommandDropdown(string _CompanyId)
        {
            this.CompanyId = _CompanyId;
            Process();
        }


        public void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                query = @"select CONNECTION_NAME,DB_CONNECTOR_ID,DATABASE_TYPE from TBL_DATABASE_CONNECTION WHERE COMPANY_ID=@COMPANY_ID";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
                if (this.ResultTable != null)
                    if (this.ResultTable.Rows.Count != 0)
                        this.StatusCode = 0;
               
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }
       
        public DataTable ResultTable
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public string CompanyId { get; set; }
    }
}
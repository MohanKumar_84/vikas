﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;

namespace mFicientCP
{
    public class SaveCompleteApp
    {
        public SaveCompleteApp()
        {
        }
        //----New app create -------
        public string ProcessSaveCompleteApp(string _wfName, string _subAdminId, string _CompanyId, APP_RUNS_ON runsOn, APP_DESIGN_MODE designMode, MF_STANDARD_APP_TYPE appType)
        {
            jqueryAppClass objApp;
            SqlCommand objSqlCommand;
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            this.StatusCode = -1000;
            string strAppObj = string.Empty;
            string query;
            string _WFId = "";
            APP_MODEL_TYPE eAppModelType = APP_MODEL_TYPE.Phone;
            try
            {
                query = @"Select WF_NAME from TBL_WORKFLOW_DETAIL where WF_NAME=@WF_NAME and COMPANY_ID=@COMPANY_ID;";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _CompanyId);
                objSqlCommand.Parameters.AddWithValue("@WF_NAME", _wfName);
                DataSet appTbl = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);

                if (appTbl.Tables[0].Rows.Count <= 0)
                {
                    MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                    objSqlTransaction = objSqlConnection.BeginTransaction();

                    _WFId = Utilities.GetMd5Hash(_subAdminId + _wfName + DateTime.UtcNow.Ticks.ToString());
                    string _MasterPageId = Utilities.GetMd5Hash(_subAdminId + _wfName + "Default" + DateTime.UtcNow.Ticks.ToString());

                    int iNoOfAppsToCreate = 1;
                    objApp = new jqueryAppClass();
                    //jqApps.id = _WFId;
                    //jqApps.name = _wfName;

                    if (runsOn == APP_RUNS_ON.PhoneAndTablet && designMode == APP_DESIGN_MODE.Both)
                    {
                        iNoOfAppsToCreate = 2;
                    }
                    if (iNoOfAppsToCreate == 1)
                    {
                        if (designMode == APP_DESIGN_MODE.Phone)
                        {
                            objApp = new jqueryAppClass(runsOn, designMode, APP_MODEL_TYPE.Phone, appType);
                            eAppModelType = APP_MODEL_TYPE.Phone;
                        }
                        else if (designMode == APP_DESIGN_MODE.Tablet)
                        {
                            objApp = new jqueryAppClass(runsOn, designMode, APP_MODEL_TYPE.Tablet, appType);
                            eAppModelType = APP_MODEL_TYPE.Tablet;
                        }
                        else
                        { }
                        //will not get any other type
                        objApp.id = _WFId;
                        objApp.name = _wfName;
                        objApp.icon = "GRAY0064.png";
                        insertApp(_CompanyId, _subAdminId, _WFId, _wfName, _MasterPageId, eAppModelType, appType, objApp, objSqlConnection, objSqlTransaction);
                    }
                    else if (iNoOfAppsToCreate == 2)
                    {
                        objApp = new jqueryAppClass(runsOn, designMode, APP_MODEL_TYPE.Phone, appType);
                        eAppModelType = APP_MODEL_TYPE.Phone;
                        objApp.id = _WFId;
                        objApp.name = _wfName;
                        objApp.icon = "GRAY0064.png";
                        insertApp(_CompanyId, _subAdminId, _WFId, _wfName, _MasterPageId, eAppModelType, appType, objApp, objSqlConnection, objSqlTransaction);

                        objApp = new jqueryAppClass(runsOn, designMode, APP_MODEL_TYPE.Tablet, appType);
                        eAppModelType = APP_MODEL_TYPE.Tablet;
                        objApp.id = _WFId;
                        objApp.name = _wfName;
                        objApp.icon = "GRAY0064.png";
                        insertApp(_CompanyId, _subAdminId, _WFId, _wfName, _MasterPageId, eAppModelType, appType, objApp, objSqlConnection, objSqlTransaction);
                    }

                    objSqlTransaction.Commit();
                    this.StatusCode = 0;
                }
                else
                {
                    this.StatusCode = 1001;
                }
            }
            catch
            {
                this.StatusCode = -1000;
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
            return _WFId;
        }

        void insertApp(
                    string companyId,
                    string subAdminId,
                    string appId,
                    string appName,
                    string masterPageId,
                    APP_MODEL_TYPE modelType,
                    MF_STANDARD_APP_TYPE appType,
                    jqueryAppClass app,
                    SqlConnection connection,
                    SqlTransaction transaction
                    )
        {
            string query = @"INSERT INTO TBL_WORKFLOW_DETAIL
                            ([COMPANY_ID],[WF_ID],[WF_NAME],[WF_DESCRIPTION],[WF_JSON],[WORKED_BY],[UPDATE_ON],
                             [SUBADMIN_ID],[MODEL_TYPE],[APP_JS_JSON],[ICON],[PARENT_ID],[FORMS],[APP_TYPE]) VALUES
                            (@COMPANY_ID,@WF_ID,@WF_NAME,
                             @WF_DESCRIPTION,@WF_JSON,@WORKED_BY,@UPDATE_ON,
                            @SUBADMIN_ID,@MODEL_TYPE,@APP_JS_JSON,@ICON,@PARENT_ID,@FORMS,@APP_TYPE);";

            SqlCommand objSqlCommand = new SqlCommand(query);
            objSqlCommand = new SqlCommand(query, connection, transaction);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
            objSqlCommand.Parameters.AddWithValue("@WF_ID", appId);
            objSqlCommand.Parameters.AddWithValue("@WF_NAME", appName);
            objSqlCommand.Parameters.AddWithValue("@WF_DESCRIPTION", "");
            objSqlCommand.Parameters.AddWithValue("@WF_JSON", "");
            objSqlCommand.Parameters.AddWithValue("@WORKED_BY", subAdminId);
            objSqlCommand.Parameters.AddWithValue("@UPDATE_ON", DateTime.UtcNow.Ticks);
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", subAdminId);
            objSqlCommand.Parameters.AddWithValue("@ICON", app.icon);
            objSqlCommand.Parameters.AddWithValue("@PARENT_ID", appId);
            objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", modelType);
            objSqlCommand.Parameters.AddWithValue("@APP_JS_JSON", Utilities.SerializeJson<jqueryAppClass>(app));
            objSqlCommand.Parameters.AddWithValue("@FORMS", "");
            objSqlCommand.Parameters.AddWithValue("@APP_TYPE", (int)appType);
            objSqlCommand.ExecuteNonQuery();

            //            query = @"INSERT INTO TBL_MASTER_PAGE_DETAILS(COMPANY_ID,MASTER_PAGE_ID,MASTER_NAME,MASTER_DESCRIPTION,
            //                                MASTER_JSON,CREATED_BY,MODIFIED_BY,
            //                                MODIFIED_DATE,IS_ACTIVE,HEADER_HTML,FOOTER_HTML,PARENT_ID)
            //                                VALUES(@COMPANY_ID,@MASTER_PAGE_ID,@MASTER_NAME,@MASTER_DESCRIPTION,@MASTER_JSON,@CREATED_BY,@MODIFIED_BY,
            //                                @MODIFIED_DATE,@IS_ACTIVE,@HEADER_HTML,@FOOTER_HTML,@PARENT_ID);";

            //            objSqlCommand = new SqlCommand(query, connection, transaction);
            //            objSqlCommand.CommandType = CommandType.Text;
            //            objSqlCommand.Parameters.AddWithValue("@MASTER_PAGE_ID", masterPageId);
            //            objSqlCommand.Parameters.AddWithValue("@MASTER_NAME", "Master");
            //            objSqlCommand.Parameters.AddWithValue("@MASTER_DESCRIPTION", "");
            //            objSqlCommand.Parameters.AddWithValue("@MASTER_JSON", "");
            //            objSqlCommand.Parameters.AddWithValue("@CREATED_BY", subAdminId);
            //            objSqlCommand.Parameters.AddWithValue("@MODIFIED_BY", subAdminId);
            //            objSqlCommand.Parameters.AddWithValue("@MODIFIED_DATE", DateTime.UtcNow.Ticks);
            //            objSqlCommand.Parameters.AddWithValue("@IS_ACTIVE", true);
            //            objSqlCommand.Parameters.AddWithValue("@HEADER_HTML", "");
            //            objSqlCommand.Parameters.AddWithValue("@FOOTER_HTML", "");
            //            objSqlCommand.Parameters.AddWithValue("@PARENT_ID", appId);
            //            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);

            //            objSqlCommand.ExecuteNonQuery();
        }

        public string ProcessUpdateCompleteApp(string _subAdminId, string _CompanyId, string _WF_Id, string[] AppsJsonObj)
        {
            string AppName = "";
            SqlCommand objSqlCommand;
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            this.StatusCode = -1000;
            string strAppObj = string.Empty;
            string query;
            try
            {
                query = @"Select WF_NAME from TBL_WORKFLOW_DETAIL where WF_ID=@WF_ID and COMPANY_ID=@COMPANY_ID;";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _CompanyId);
                objSqlCommand.Parameters.AddWithValue("@WF_ID", _WF_Id);
                DataSet appTbl = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);

                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                objSqlTransaction = objSqlConnection.BeginTransaction();
                string InterfaceModelType = "";
                byte byteAppType = 0;
                foreach (string app in AppsJsonObj)
                {
                    if (!String.IsNullOrEmpty(app))
                    {
                        //jqueryAppBasicClass jqApps = new jqueryAppBasicClass();
                        //MemoryStream ms1 = new MemoryStream(Encoding.Unicode.GetBytes(app));
                        //System.Runtime.Serialization.Json.DataContractJsonSerializer serializer1 = new System.Runtime.Serialization.Json.DataContractJsonSerializer(jqApps.GetType());
                        //jqApps = serializer1.ReadObject(ms1) as jqueryAppBasicClass;
                        //ms1.Close();
                        JObject jqApps = JObject.Parse(app);

                        InterfaceModelType = Convert.ToString(jqApps["modelType"]);
                        byteAppType = Convert.ToByte(jqApps["type"].ToString());
                        query = @"if exists (Select 'true' from TBL_WORKFLOW_DETAIL where  COMPANY_ID=@COMPANY_ID and MODEL_TYPE=@MODEL_TYPE and WF_ID=@WF_ID)
                        update TBL_WORKFLOW_DETAIL set [COMPANY_ID]=@COMPANY_ID,[WF_NAME]=@WF_NAME,[WF_DESCRIPTION]=@WF_DESCRIPTION,[WORKED_BY]=@SUBADMIN_ID,[UPDATE_ON]=@UPDATE_ON,
                        [APP_JS_JSON]=@APP_JS_JSON,[ICON]=@ICON where COMPANY_ID=@COMPANY_ID and MODEL_TYPE=@MODEL_TYPE and WF_ID=@WF_ID 
                        else INSERT INTO TBL_WORKFLOW_DETAIL([COMPANY_ID],[WF_ID],[WF_NAME],[WF_DESCRIPTION],[WF_JSON],[WORKED_BY],[UPDATE_ON],[SUBADMIN_ID],[MODEL_TYPE],[APP_JS_JSON],
                        [ICON],[PARENT_ID],[FORMS],[APP_TYPE]) VALUES (@COMPANY_ID,@WF_ID,@WF_NAME,@WF_DESCRIPTION,@WF_JSON,@SUBADMIN_ID,@UPDATE_ON,@SUBADMIN_ID,@MODEL_TYPE,@APP_JS_JSON,@ICON,@WF_ID,'',@APP_TYPE);";

                        objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _CompanyId);
                        objSqlCommand.Parameters.AddWithValue("@WF_ID", _WF_Id);
                        objSqlCommand.Parameters.AddWithValue("@WF_NAME", Convert.ToString(jqApps["name"]));
                        objSqlCommand.Parameters.AddWithValue("@WF_DESCRIPTION", Convert.ToString(jqApps["description"]));
                        objSqlCommand.Parameters.AddWithValue("@WF_JSON", "");
                        objSqlCommand.Parameters.AddWithValue("@UPDATE_ON", DateTime.UtcNow.Ticks);
                        objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", _subAdminId);
                        objSqlCommand.Parameters.AddWithValue("@ICON", Convert.ToString(jqApps["icon"]));
                        objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", Convert.ToString(jqApps["modelType"]));
                        objSqlCommand.Parameters.AddWithValue("@APP_JS_JSON", app);
                        objSqlCommand.Parameters.AddWithValue("@APP_TYPE", byteAppType);

                        AppName = Convert.ToString(jqApps["name"]);
                        //throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                        if (objSqlCommand.ExecuteNonQuery() > 0)
                        {
                        }
                        else
                            throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                    }
                }

                if (appTbl != null && appTbl.Tables[0].Rows.Count > 0 && AppsJsonObj.Count() == 1)
                {
                    query = "delete  from TBL_WORKFLOW_DETAIL where WF_ID=@WF_ID and COMPANY_ID=@COMPANY_ID and MODEL_TYPE!=@MODEL_TYPE;";
                    objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _CompanyId);
                    objSqlCommand.Parameters.AddWithValue("@WF_ID", _WF_Id);
                    objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", InterfaceModelType);
                    if (objSqlCommand.ExecuteNonQuery() > 0)
                    {
                    }
                }
                Utilities.saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.APP_SAVE_MODIFIED, _CompanyId, _subAdminId, _WF_Id, AppName, "", "", "", "", "", "");

                objSqlTransaction.Commit();
                this.StatusCode = 0;
            }
            catch
            {
                this.StatusCode = -1000;
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
            }
            finally
            {
                if (objSqlTransaction != null)
                    objSqlTransaction.Dispose();
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
            return strAppObj;
        }
        public void ProcessUpdateAppFromArchivedApp(string subadminId, string companyId, string wfId, string version)
        {
            SqlCommand objSqlCommand;
            this.StatusCode = -1000;
            string query;
            try
            {
                query = @"UPDATE wf
                          SET
                              wf.APP_JS_JSON = prvWf.APP_JS_JSON
                          FROM
                             TBL_WORKFLOW_DETAIL wf
	                            INNER JOIN
                             TBL_PRV_WORKFLOW_DETAIL prvWf
	                            ON
                                wf.WF_ID = prvWf.WF_ID
	                            AND
	                            wf.MODEL_TYPE = prvWf.MODEL_TYPE
	                            WHERE prvWf.VERSION = @VERSION
	                            AND prvWf.WF_ID = @WF_ID
	                            AND prvWf.COMPANY_ID = @COMPANY_ID";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
                objSqlCommand.Parameters.AddWithValue("@WF_ID", wfId);
                objSqlCommand.Parameters.AddWithValue("@VERSION", version);
                int iRecordCount = MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
                this.StatusCode = 0;
            }
            catch (Exception)
            {
                this.StatusCode = -1000;

            }
        }
        //-----app save as---------
        public string ProcessSaveAsCompleteApp(string _subAdminId, string _CompanyId, string appName, string[] AppsJsonObj)
        {
            SqlCommand objSqlCommand;
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            this.StatusCode = -1000;
            string strAppObj = string.Empty;
            string _WFId = "";
            string query;
            _WFId = Utilities.GetMd5Hash(_subAdminId + appName + DateTime.UtcNow.Ticks.ToString()); ;
            byte byteAppType = 0;
            MSSqlClient.SqlConnectionOpen(out objSqlConnection);
            try
            {

                using (objSqlConnection)
                {
                    using (objSqlTransaction = objSqlConnection.BeginTransaction())
                    {
                        foreach (string app in AppsJsonObj)
                        {
                            if (!String.IsNullOrEmpty(app))
                            {
                                //jqueryAppClass jqApp = new jqueryAppClass();
                                //MemoryStream ms1 = new MemoryStream(Encoding.Unicode.GetBytes(app));
                                //System.Runtime.Serialization.Json.DataContractJsonSerializer serializer1 =
                                //        new System.Runtime.Serialization.Json.DataContractJsonSerializer(jqApp.GetType());
                                //jqApp = serializer1.ReadObject(ms1) as jqueryAppClass;
                                //ms1.Close();
                                JObject jqApp = JObject.Parse(app);

                                jqApp["id"] = _WFId;
                                jqApp["name"] = appName;
                                byteAppType = Convert.ToByte(jqApp["type"]);
                                query = @"INSERT INTO TBL_WORKFLOW_DETAIL([COMPANY_ID],[WF_ID],[WF_NAME],[WF_DESCRIPTION],[WF_JSON],[WORKED_BY],[UPDATE_ON],[SUBADMIN_ID],[MODEL_TYPE],[APP_JS_JSON],[ICON],[PARENT_ID],[FORMS],[APP_TYPE]) VALUES
                                    (@COMPANY_ID,@WF_ID,@WF_NAME,@WF_DESCRIPTION,@WF_JSON,@WORKED_BY,@UPDATE_ON,@SUBADMIN_ID,@MODEL_TYPE,@APP_JS_JSON,@ICON,@PARENT_ID,@FORMS,@APP_TYPE);";

                                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                                objSqlCommand.CommandType = CommandType.Text;
                                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _CompanyId);
                                objSqlCommand.Parameters.AddWithValue("@WF_ID", _WFId);
                                objSqlCommand.Parameters.AddWithValue("@WF_NAME", appName);
                                objSqlCommand.Parameters.AddWithValue("@WF_DESCRIPTION", Convert.ToString(jqApp["description"]));
                                objSqlCommand.Parameters.AddWithValue("@WF_JSON", "");
                                objSqlCommand.Parameters.AddWithValue("@WORKED_BY", _subAdminId);
                                objSqlCommand.Parameters.AddWithValue("@UPDATE_ON", DateTime.UtcNow.Ticks);
                                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", _subAdminId);
                                objSqlCommand.Parameters.AddWithValue("@ICON", Convert.ToString(jqApp["icon"]));
                                objSqlCommand.Parameters.AddWithValue("@PARENT_ID", _WFId);
                                objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", Convert.ToString(jqApp["modelType"]));
                                objSqlCommand.Parameters.AddWithValue("@APP_JS_JSON", Convert.ToString(jqApp));
                                objSqlCommand.Parameters.AddWithValue("@FORMS", "");
                                objSqlCommand.Parameters.AddWithValue("@APP_TYPE", byteAppType);
                                //throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                                if (objSqlCommand.ExecuteNonQuery() > 0)
                                {
                                }
                                else
                                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                            }
                        }

                        objSqlTransaction.Commit();
                        this.StatusCode = 0;
                    }
                }

            }
            catch (SqlException)
            {
                this.StatusCode = -1000;
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
            }
            finally
            {
                if (objSqlConnection != null)
                {
                    objSqlConnection.Dispose();
                }
                if (objSqlTransaction != null)
                {
                    objSqlTransaction.Dispose();
                }
            }
            return _WFId;
        }
        //----create form copy------
        //        public string ProcessUpdateCompleteApp(string _subAdminId, string _CompanyId, string _WF_Id, List<jqueryAppClass> lstApps)
        //        {
        //            SqlCommand objSqlCommand;
        //            SqlConnection objSqlConnection = null;
        //            SqlTransaction objSqlTransaction = null;
        //            this.StatusCode = -1000;
        //            string strAppObj = string.Empty;
        //            string query;
        //            try
        //            {

        //                query = @"Select WF_NAME from TBL_WORKFLOW_DETAIL where WF_ID=@WF_ID and COMPANY_ID=@COMPANY_ID;";
        //                objSqlCommand = new SqlCommand(query);
        //                objSqlCommand.CommandType = CommandType.Text;
        //                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _CompanyId);
        //                objSqlCommand.Parameters.AddWithValue("@WF_ID", _WF_Id);
        //                DataSet appTbl = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);


        //                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
        //                objSqlTransaction = objSqlConnection.BeginTransaction();

        //                string InterfaceModelType = "";
        //                //byte byteAppType = 0;
        //                foreach (jqueryAppClass app in lstApps)
        //                {
        //                    query = @"if exists (Select 'true' from TBL_WORKFLOW_DETAIL where  COMPANY_ID=@COMPANY_ID and MODEL_TYPE=@MODEL_TYPE and WF_ID=@WF_ID)
        //                    update TBL_WORKFLOW_DETAIL set [COMPANY_ID]=@COMPANY_ID,[WF_NAME]=@WF_NAME,[WF_DESCRIPTION]=@WF_DESCRIPTION,[WORKED_BY]=@SUBADMIN_ID,[UPDATE_ON]=UPDATE_ON,
        //                    [APP_JS_JSON]=@APP_JS_JSON,[ICON]=@ICON where COMPANY_ID=@COMPANY_ID and MODEL_TYPE=@MODEL_TYPE and WF_ID=@WF_ID 
        //                    else INSERT INTO TBL_WORKFLOW_DETAIL([COMPANY_ID],[WF_ID],[WF_NAME],[WF_DESCRIPTION],[WF_JSON],[WORKED_BY],[UPDATE_ON],[SUBADMIN_ID],[MODEL_TYPE],[APP_JS_JSON],
        //                    [ICON],[PARENT_ID],[FORMS]) VALUES (@COMPANY_ID,@WF_ID,@WF_NAME,@WF_DESCRIPTION,@WF_JSON,@SUBADMIN_ID,@UPDATE_ON,@SUBADMIN_ID,@MODEL_TYPE,@APP_JS_JSON,@ICON,@WF_ID,'',@APP_TYPE);";

        //                    //byteAppType = 
        //                    objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
        //                    objSqlCommand.CommandType = CommandType.Text;
        //                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _CompanyId);
        //                    objSqlCommand.Parameters.AddWithValue("@WF_ID", _WF_Id);
        //                    objSqlCommand.Parameters.AddWithValue("@WF_NAME", app.name);
        //                    objSqlCommand.Parameters.AddWithValue("@WF_DESCRIPTION", app.description);
        //                    objSqlCommand.Parameters.AddWithValue("@WF_JSON", "");
        //                    objSqlCommand.Parameters.AddWithValue("@UPDATE_ON", DateTime.UtcNow.Ticks);
        //                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", _subAdminId);
        //                    objSqlCommand.Parameters.AddWithValue("@ICON", app.icon);
        //                    objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", app.modelType);
        //                    objSqlCommand.Parameters.AddWithValue("@APP_JS_JSON", Utilities.SerializeJson<jqueryAppClass>(app));
        //                    //throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
        //                    if (objSqlCommand.ExecuteNonQuery() > 0)
        //                    {
        //                    }
        //                    else
        //                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
        //                }

        //                if (appTbl != null && appTbl.Tables[0].Rows.Count > 0 && lstApps.Count() == 1)
        //                {
        //                    query = "delete  from TBL_WORKFLOW_DETAIL where WF_ID=@WF_ID and COMPANY_ID=@COMPANY_ID and MODEL_TYPE!=@MODEL_TYPE;";
        //                    objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
        //                    objSqlCommand.CommandType = CommandType.Text;
        //                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _CompanyId);
        //                    objSqlCommand.Parameters.AddWithValue("@WF_ID", _WF_Id);
        //                    objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", InterfaceModelType);
        //                    if (objSqlCommand.ExecuteNonQuery() > 0)
        //                    {
        //                    }
        //                }
        //                objSqlTransaction.Commit();
        //                this.StatusCode = 0;
        //            }
        //            catch
        //            {
        //                this.StatusCode = -1000;
        //                if (objSqlTransaction != null)
        //                    objSqlTransaction.Rollback();
        //            }
        //            finally
        //            {
        //                if (objSqlTransaction != null)
        //                    objSqlTransaction.Dispose();
        //                MSSqlClient.SqlConnectionClose(objSqlConnection);
        //            }
        //            return strAppObj;
        //        }

        public string TransferAppToAnotherSubadmin(string _subAdminId, string _CompanyId, string _WF_Id)
        {
            SqlCommand objSqlCommand;
            this.StatusCode = -1000;
            string strAppObj = string.Empty;
            string query;
            try
            {

                query = @"update TBL_WORKFLOW_DETAIL set [WORKED_BY]=@SUBADMIN_ID where COMPANY_ID=@COMPANY_ID and WF_ID=@WF_ID ";

                objSqlCommand = new SqlCommand(query);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _CompanyId);
                objSqlCommand.Parameters.AddWithValue("@WF_ID", _WF_Id);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", _subAdminId);

                //throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                {
                }
                else
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());

                this.StatusCode = 0;
            }
            catch
            {
                this.StatusCode = -1000;
            }
            finally
            {
            }
            return strAppObj;
        }
        //public string WFId { get; set; }
        //public string MasterPageId { get; set; }
        public int StatusCode { get; set; }
    }
}
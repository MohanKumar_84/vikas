﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
using System.Configuration;
namespace mFicientCP
{
    public class MySQLClient
    {
        /// <summary>
        /// Description :Test connction ( Sql Query And Parameter )
        /// </summary>
        public static bool TestMySQLConnection(string connectionString)
        {
            bool isConnected = false;
            MySqlConnection mySqlConn = null;
            try
            {
                mySqlConn = new MySqlConnection(connectionString);
            }
            catch
            {
                throw new Exception(@"Invalid Connection");
            }
            try
            {
                mySqlConn.Open();
                if (mySqlConn.State != ConnectionState.Open)
                {
                    if (mySqlConn != null)
                    {
                        mySqlConn.Dispose();
                        mySqlConn = null;
                    }
                }
                else
                {
                    isConnected = true;
                    mySqlConn.Close();
                }
            }
            catch
            {
                mySqlConn = null;
            }
            return isConnected;
        }
        /// <summary>
        /// Description :Select Data According To MySqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static DataSet Select(MySqlCommand _SqlCommand, string connectionString)
        {
            MySqlConnection objSqlConnection = null;
            DataSet ObjDataSet = new DataSet();
            try
            {
                OpenConnection(out objSqlConnection, connectionString);
                _SqlCommand.Connection = objSqlConnection;
                MySqlDataAdapter objSqlDataAdapter = new MySqlDataAdapter(_SqlCommand);

                objSqlDataAdapter.Fill(ObjDataSet);

                //SqlConnectionClose(objSqlConnection);

                return ObjDataSet;
            }
            catch
            {
                ObjDataSet = null;
                throw;

            }
            finally
            {
                CloseConnection(objSqlConnection);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_SqlCommand"></param>
        /// <param name="con">pass a open connection</param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static DataSet SelectUsingTransaction(MySqlCommand _SqlCommand, MySqlConnection connection, MySqlTransaction transaction)
        {
            _SqlCommand.Connection = connection;
            _SqlCommand.Transaction = transaction;
            DataSet ObjDataSet = new DataSet();

            MySqlDataAdapter objSqlDataAdapter = new MySqlDataAdapter(_SqlCommand);

            objSqlDataAdapter.Fill(ObjDataSet);

            return ObjDataSet;

        }
        /// <summary>
        /// Description :MySqlConnection Open Function
        /// </summary>
        public static void OpenConnection(out MySqlConnection Conn, string connectionString)
        {
            try
            {
                Conn = new MySqlConnection(connectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }

        /// <summary>
        /// Description :MySqlConnection Close Function
        /// </summary>
        public static void CloseConnection(MySqlConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open))
                {
                    Conn.Close();
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Description :Data Process (Insert, Update, Delete ) According To MySqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static int ExecuteNonQuery(MySqlCommand _SqlCommand, string connectionString)
        {
            MySqlConnection objSqlConnection = null;
            OpenConnection(out objSqlConnection, connectionString);
            _SqlCommand.Connection = objSqlConnection;

            int i = _SqlCommand.ExecuteNonQuery();

            CloseConnection(objSqlConnection);

            return i;
        }

        public static int ExecuteNonQueryUsingTransaction(MySqlCommand _SqlCommand, MySqlConnection connection, MySqlTransaction transaction)
        {
            _SqlCommand.Connection = connection;
            _SqlCommand.Transaction = transaction;
            return _SqlCommand.ExecuteNonQuery();
        }

        public static DataSet getMetaDataByType(MetadataType metadataType,
            string connectionString,
            string databaseName,
            string tableName,
           out string errorMsg)
        {
            DataSet dsMetaData = new DataSet();
            errorMsg = String.Empty;
            try
            {
                string strQuery = string.Empty;
                strQuery = getMetaDataQuery(metadataType,
                    databaseName);
                MySqlCommand cmd = new MySqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                switch (metadataType)
                {
                    case MetadataType.TABLES:
                        break;
                    case MetadataType.COLUMNS:
                        cmd.Parameters.AddWithValue("@TABLE_NAME", tableName);
                        break;
                    case MetadataType.ALL:
                        break;
                }
                dsMetaData = MySQLClient.Select(cmd, connectionString);
                return dsMetaData;
            }
            catch (Exception ex)
            {
                dsMetaData = null;
                if (ex.Message.ToLower() == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString().ToLower())
                {
                    errorMsg = "There was some error in connection.";
                }
                else
                {
                    errorMsg = "Internal server error.";
                }
                return dsMetaData;
            }
        }
        public static string getMetaDataQuery(MetadataType metadataType,
            string databaseName)
        {
            string strQuery = string.Empty;
            switch (metadataType)
            {
                case MetadataType.TABLES:
                    strQuery = @"SELECT TABLE_SCHEMA,TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE table_schema ='"
                        + databaseName + "';";
                    break;

                case MetadataType.COLUMNS:
                    strQuery = @"SELECT COLUMN_NAME, DATA_TYPE as column_type, IS_NULLABLE, TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @TABLE_NAME AND table_schema ='"
                        + databaseName + "' ;";
                    break;

                case MetadataType.ALL:
                    strQuery = @"SELECT TABLE_SCHEMA,TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE table_schema ='" + databaseName + @"';
                                SELECT COLUMN_NAME, DATA_TYPE as column_type, IS_NULLABLE, TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema ='" +
                                databaseName + "' ;";
                    break;
            }
            return strQuery;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostName"></param>
        /// <param name="databaseName"></param>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <param name="timeout">For default timeout pass 0 or value less than 0</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public static string getConnectionString(string hostName,
            string databaseName,
            string userId,
            string password,
            uint timeout)
        {
            if (String.IsNullOrEmpty(hostName) ||
               String.IsNullOrEmpty(databaseName) ||
                String.IsNullOrEmpty(userId) ||
                String.IsNullOrEmpty(password)) throw new ArgumentNullException();

            MySqlConnectionStringBuilder conStringBuilder = new MySqlConnectionStringBuilder();
            conStringBuilder.Server = hostName;
            conStringBuilder.Database = databaseName;
            conStringBuilder.UserID = userId;
            conStringBuilder.Password = password;
            if (timeout > 0)
                conStringBuilder.ConnectionTimeout = timeout;
            return conStringBuilder.ConnectionString;

        }
    }
}
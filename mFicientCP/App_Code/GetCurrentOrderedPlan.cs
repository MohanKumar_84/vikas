﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetCurrentOrderedPlan
    {
        public GetCurrentOrderedPlan(string companyId)
        {
            this.CompanyId = companyId;
        }
        public GetCurrentOrderedPlan(string companyId, string currency, int months)
        {
            this.CompanyId = companyId;
            this.Currency = currency;
            this.Months = months;
        }
        public void Process()
        {
            StatusCode = -1000;//for error
            try
            {
                //in the table it should be either in state 3 0r 4 means the process is complete.
                //if its 0,1,2 then process is pending for approval
                //second inner join added on 26/10/2012 changes in database
                string strQuery = "";
                /******
                 * For all the queries if we just find the request which has status == 0.
                 * Then also we can get the desired result.
                 * As 0 means no one has approved the request.
                 * The above consideration would be wrong.
                 * There are other records in the table as well.with status code other than zero.
                 * The status include Cancelled by user,Cancelled by resller,cancelled by sales,cancelled by accounts
                 * Or Accounts approved which is the final stage in the process.
                 * if we can get a record that is having diff status other than above mentioned or zero 
                 * then the process is still in process.
                 * ***/
                if (String.IsNullOrEmpty(this.Currency) && this.Months == 0)
                {

                    strQuery = @"SELECT * FROM ADMIN_TBL_PLAN_ORDER_REQUEST AS Request
                                    INNER JOIN ADMIN_TBL_PLANS AS Plans
                                    ON Plans.PLAN_CODE = Request.PLAN_CODE
                                    INNER JOIN ADMIN_TBL_PLAN_PRICE AS PlanPrice
                                    ON PlanPrice.PLAN_CODE = Plans.PLAN_CODE
                                    WHERE COMPANY_ID = @CompanyId AND 
                                    STATUS !=@AccountsApproved AND 
                                    STATUS != @Cancelled AND 
                                    STATUS !=@ResellerCancelled AND 
                                    STATUS !=@SalesCancelled AND 
                                    STATUS != @AccCancelled;";
                }
                else if (!String.IsNullOrEmpty(this.Currency) && this.Months == 0)
                {
                    strQuery = @"SELECT * FROM ADMIN_TBL_PLAN_ORDER_REQUEST AS Request
                                    INNER JOIN ADMIN_TBL_PLANS AS Plans
                                    ON Plans.PLAN_CODE = Request.PLAN_CODE
                                    INNER JOIN ADMIN_TBL_PLAN_PRICE AS PlanPrice
                                    ON PlanPrice.PLAN_CODE = Plans.PLAN_CODE
                                    WHERE COMPANY_ID = @CompanyId AND
                                    CURRENCY = @Currency AND
                                    STATUS !=@AccountsApproved  AND
                                    STATUS != @Cancelled AND 
                                    STATUS !=@ResellerCancelled AND 
                                    STATUS !=@SalesCancelled AND 
                                    STATUS != @AccCancelled;";
                }
                else
                {


                    strQuery = @"SELECT * FROM ADMIN_TBL_PLAN_ORDER_REQUEST AS Request
                                    INNER JOIN ADMIN_TBL_PLANS AS Plans
                                    ON Plans.PLAN_CODE = Request.PLAN_CODE
                                    INNER JOIN ADMIN_TBL_PLAN_PRICE AS PlanPrice
                                    ON PlanPrice.PLAN_CODE = Plans.PLAN_CODE
                                    WHERE COMPANY_ID = @CompanyId
                                    AND @Month BETWEEN FROM_MONTH AND TO_MONTH
                                    AND 
                                    CURRENCY = @Currency
                                    AND 
                                    STATUS !=@AccountsApproved AND 
                                    STATUS != @Cancelled AND 
                                    STATUS !=@ResellerCancelled AND 
                                    STATUS !=@SalesCancelled AND 
                                    STATUS != @AccCancelled;";
                }
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                // there will be only one record of this if exists.other wise no data will be present
                cmd.Parameters.AddWithValue("@AccountsApproved", (int)PLAN_REQUEST_STATUS.ACCOUNTS_APPROVED);
                cmd.Parameters.AddWithValue("@Cancelled", (int)PLAN_REQUEST_STATUS.CANCELLED);
                cmd.Parameters.AddWithValue("@ResellerCancelled", (int)PLAN_REQUEST_STATUS.RESELLER_DENIED);
                cmd.Parameters.AddWithValue("@SalesCancelled", (int)PLAN_REQUEST_STATUS.SALES_DENIED);
                cmd.Parameters.AddWithValue("@AccCancelled", (int)PLAN_REQUEST_STATUS.ACCOUNTS_DENIED);
                if (!String.IsNullOrEmpty(this.Currency) && this.Months != 0)
                {
                    cmd.Parameters.AddWithValue("@Month", this.Months);
                    cmd.Parameters.AddWithValue("@Currency", this.Currency);
                }
                if (!String.IsNullOrEmpty(this.Currency) && this.Months == 0)
                {
                    //cmd.Parameters.AddWithValue("@Month", this.Months);
                    cmd.Parameters.AddWithValue("@Currency", this.Currency);
                }
                OrderRequest = MSSqlClient.SelectDataFromSQlCommand(cmd).Tables[0];

                if (OrderRequest != null)
                {
                    StatusCode = 0;
                    StatusDescription = "";
                    if (OrderRequest.Rows.Count > 0)
                    {
                        //TODO This step may not be required.It is checking the number of months
                        // <= of that of the data in database,the result that we have got,
                        //this will always give the same row as record.
                        //Test it then remove it.
                        string filter = String.Format(
                                                    "FROM_MONTH <= {0} AND TO_MONTH>={1} AND CURRENCY_TYPE = CURRENCY",
                                                    Convert.ToInt32(OrderRequest.Rows[0]["NUMBER_MONTH"]),
                                                    Convert.ToInt32(OrderRequest.Rows[0]["NUMBER_MONTH"])
                                                    );
                        DataRow[] rows = OrderRequest.Select(filter);
                        if (rows != null && rows.Length > 0)
                        {
                            this.RequestType = Convert.ToString(rows[0]["REQUEST_TYPE"]);
                            this.PlanCode = Convert.ToString(rows[0]["PLAN_CODE"]);
                            this.NoOfUSers = Convert.ToInt32(rows[0]["MOBILE_USERS"]);
                            this.RequestDateTimeTicks = Convert.ToInt64(rows[0]["REQUEST_DATETIME"]);
                            this.RequestPlanName = Convert.ToString(rows[0]["PLAN_NAME"]);
                            this.Currency = Convert.ToString(rows[0]["CURRENCY_TYPE"]);
                            this.UserChargePerMonth = Convert.ToString(rows[0]["PRICE"]);
                            this.Months = Convert.ToInt32(rows[0]["NUMBER_MONTH"]);
                            this.RequestStatus = Convert.ToInt32(rows[0]["STATUS"]);
                        }
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
            }
        }
        public string CompanyId
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }

        public int StatusCode
        {
            get;
            set;
        }
        public DataTable OrderRequest
        {
            get;
            set;
        }
        public string Currency
        {
            get;
            set;
        }
        public int Months
        {
            get;
            set;
        }


        public string RequestType
        {
            get;
            set;
        }
        public string PlanCode
        {
            get;
            set;
        }
        public int NoOfUSers
        {
            get;
            set;
        }
        public long RequestDateTimeTicks
        {
            get;
            set;
        }
        public string RequestPlanName
        {
            get;
            set;
        }
        public string UserChargePerMonth
        {
            get;
            set;
        }
        public int RequestStatus
        {
            get;
            private set;
        }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Runtime.Serialization;

namespace mFicientCP
{
    public class GetDomainListResp
    {
        string _code, _description;
        List<string> _data;

        public string Description
        {
            get { return _description; }
        }

        public string Code
        {
            get { return _code; }
        }

        public List<string> Data
        {
            get { return _data; }
        }

        public int StatusCode
        {
            get;
            set;
        }

        public string StatusDescription
        {
            get;
            set;
        }

        public GetDomainListResp(string json)
        {
            this.StatusCode = -1000;
            try
            {
                DomainListResponse objDomainListResponseParsing = Utilities.DeserialiseJson<DomainListResponse>(json);
                _code = objDomainListResponseParsing.resp.cd;
                _description = objDomainListResponseParsing.resp.desc;
                if (Convert.ToInt32(_code) == 0)
                {
                    _data = objDomainListResponseParsing.resp.data;
                }
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }
    }

    [DataContract]
    public class DomainListResponse
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public DomainListRespFields resp { get; set; }
    }

    [DataContract]
    public class DomainListRespFields
    {

        [DataMember]
        public string cd { get; set; }

        [DataMember]
        public string desc { get; set; }

        [DataMember]
        public List<string> data { get; set; }
    }
}
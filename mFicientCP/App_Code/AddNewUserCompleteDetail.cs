﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;
using System.Runtime.Serialization;

namespace mFicientCP
{
    public class AddNewUserCompleteDetail
    {
        private string strUserId, strEmail, strAccessCode, strFirstName, strLastName, strMobile, strCompanyId
                             , strDateOfBirth, strAdminId, strRegionId, strLocationId,
                             strDivisionId, strDesignationId, strEmployeeNo, strUserName, strRequestedBy, strCedentiailProperties, strCustomproperties,
                             strDomainId;
        private byte blAllowMessenger, blAllowOfflineWork, blAllowDesktopWork;
        private Boolean blIsactive;
        private string _adminIdOfSubAdmin;
        HttpContext _appContext;


        public AddNewUserCompleteDetail(
           string _Email, string _AccessCode, string _FirstName,
          string _LastName, string _Mobile, string _CompanyId,
           string _DateOfBirth, string _SubAdminId, string _RegionId,
           string _LocationId, string _DesignationId, string _EmployeeNo,
           string _UserName, string _DivisionId, Boolean _IsActive,
           byte _AllowMessenger, byte _AllowOfflineWork, byte _AllowDesktopMessenger
           , string _RequestedBy, string domainId,
           string adminId,
           HttpContext context,
           List<string> userGroupIds, string _CustomJson, string _CredentialJson)
        {
            this.strEmail = _Email;
            this.strAccessCode = _AccessCode;
            this.strFirstName = _FirstName;
            this.strLastName = _LastName;
            this.strMobile = _Mobile;
            this.strCompanyId = _CompanyId;
            this.strDateOfBirth = _DateOfBirth;
            this.strAdminId = _SubAdminId;
            this.strRegionId = _RegionId == "-1" ? "" : _RegionId;
            this.strLocationId = _LocationId == "-1" ? "" : _LocationId;
            this.strDivisionId = _DivisionId == "-1" ? "" : _DivisionId;
            this.strDesignationId = _DesignationId == "-1" ? "" : _DesignationId;
            this.strEmployeeNo = _EmployeeNo;
            this.strUserName = _UserName;
            this.blIsactive = _IsActive;
            this.blAllowMessenger = _AllowMessenger;
            this.blAllowOfflineWork = _AllowOfflineWork;
            this.blAllowDesktopWork = _AllowDesktopMessenger;
            this.strRequestedBy = _RequestedBy;
            this.strDomainId = domainId;
            this._adminIdOfSubAdmin = adminId;
            this._appContext = context;
            this.UserGroupIds = userGroupIds;

            List<CredentialProperties> Crds = Utilities.DeserialiseJson<List<CredentialProperties>>(_CredentialJson);
            foreach (CredentialProperties Cr in Crds)
            {
                Cr.unm = AesEncryption.AESEncrypt(_CompanyId.ToUpper(), Cr.unm);
                if (!string.IsNullOrEmpty(Cr.pwd))
                    Cr.pwd = AesEncryption.AESEncrypt(_CompanyId.ToUpper(), Cr.pwd);
            }

            this.strCedentiailProperties = Utilities.SerializeJson<List<CredentialProperties>>(Crds);
            this.strCustomproperties = _CustomJson;
        }
        public void Process()
        {
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            StatusCode = -1000;

            try
            {

                DataTable dtblSubAdminDtl = getSubAdminDetails();
                long dateTimeOfAddition = DateTime.UtcNow.Ticks;
                string fullDate = Utilities.getFullCompanyLocalFormattedDate(
                            CompanyTimezone.getTimezoneInfo(this.CompanyId),
                            dateTimeOfAddition
                            );
                List<MFEMgramEnterpriseGroups> lstAllMgrmEntprsGrps = getMgrmEnterpriseGroup();
                List<MFEMgramEnterpriseGroups> lstExistingGrps;
                List<MFEMgramEnterpriseGroups> lstNonExistingGrpsINMgram;
                compareIfUserGroupSelectedAlreadyExistInMGram(
                    lstAllMgrmEntprsGrps,
                    out lstExistingGrps,
                    out lstNonExistingGrpsINMgram);

                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                this.UserId = Utilities.GetMd5Hash(this.SubAdminId + this.UserName + DateTime.UtcNow.Ticks.ToString());
                string query = @"INSERT INTO TBL_USER_DETAIL
                                        (USER_ID,EMAIL_ID,ACCESS_CODE,FIRST_NAME,LAST_NAME,MOBILE,
                                        COMPANY_ID,DATE_OF_BIRTH,REGISTRATION_DATETIME,SUBADMIN_ID,
                                        REGION_ID,LOCATION_ID,DESIGNATION_ID,EMPLOYEE_NO,
                                        USER_NAME,IS_ACTIVE,ALLOW_MESSENGER,IS_BLOCKED,IS_OFFLINE_WORK,UPDATE_ON,DESKTOP_MESSENGER,REQUESTED_BY,
                                        DOMAIN_ID,CUSTOM_PROPERTIES,CREDENTIAL_DETAIL)
                                        VALUES
                                        (@USER_ID,@EMAIL_ID,@ACCESS_CODE,@FIRST_NAME,@LAST_NAME,@MOBILE,
                                        @COMPANY_ID,@DATE_OF_BIRTH,@REGISTRATION_DATETIME,@SUBADMIN_ID,
                                        @REGION_ID,@LOCATION_ID,@DESIGNATION_ID,@EMPLOYEE_NO,
                                        @USER_NAME,@IS_ACTIVE,@ALLOW_MESSENGER,@IS_BLOCKED,@ALLOW_OFFLINE_WORK,@UPDATE_ON,@DESKTOP_MESSENGER,@REQUESTED_BY,
                                        @DOMAIN_ID,@CUSTOM_PROPERTIES,@CREDENTIAL_DETAIL)";
                objSqlTransaction = objSqlConnection.BeginTransaction();
                string strPassword = Utilities.GetMd5Hash(this.AccessCode);
                SqlCommand objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@EMAIL_ID", this.Email);
                objSqlCommand.Parameters.AddWithValue("@ACCESS_CODE", strPassword);
                objSqlCommand.Parameters.AddWithValue("@FIRST_NAME", this.FirstName);
                objSqlCommand.Parameters.AddWithValue("@LAST_NAME", this.LastName);
                objSqlCommand.Parameters.AddWithValue("@MOBILE", this.Mobile);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@DATE_OF_BIRTH", this.DateOfBirth);
                objSqlCommand.Parameters.AddWithValue("@REGISTRATION_DATETIME", dateTimeOfAddition);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@REGION_ID", this.RegionId);
                objSqlCommand.Parameters.AddWithValue("@LOCATION_ID", this.LocationId);
                objSqlCommand.Parameters.AddWithValue("@DESIGNATION_ID", this.DesignationId);
                objSqlCommand.Parameters.AddWithValue("@EMPLOYEE_NO", this.EmployeeNo);
                objSqlCommand.Parameters.AddWithValue("@USER_NAME", this.UserName);
                objSqlCommand.Parameters.AddWithValue("@IS_ACTIVE", this.IsActive ? 1 : 0);
                objSqlCommand.Parameters.AddWithValue("@ALLOW_MESSENGER", this.AllowMessenger);
                objSqlCommand.Parameters.AddWithValue("@IS_BLOCKED", false);
                objSqlCommand.Parameters.AddWithValue("@ALLOW_OFFLINE_WORK", this.AllowOfflineWork);
                objSqlCommand.Parameters.AddWithValue("@UPDATE_ON", dateTimeOfAddition);
                objSqlCommand.Parameters.AddWithValue("@DESKTOP_MESSENGER", this.AllowDesktopWork);
                objSqlCommand.Parameters.AddWithValue("@REQUESTED_BY", this.RequestedBy);
                objSqlCommand.Parameters.AddWithValue("@DOMAIN_ID", this.DomainId);
                objSqlCommand.Parameters.AddWithValue("@CREDENTIAL_DETAIL", this.strCedentiailProperties);
                objSqlCommand.Parameters.AddWithValue("@CUSTOM_PROPERTIES", this.strCustomproperties);
                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }

                string[] strArrayDivDepId = this.DivisionId.Split('&');
                foreach (string str in strArrayDivDepId)
                {
                    if (str.Length != 0)
                    {
                        string strDivId = str.Split('@')[1];
                        string strRelatedDepId = str.Split('@')[0];
                        string[] strArrayDepId = strRelatedDepId.Split('#');
                        foreach (string strDepId in strArrayDepId)
                        {
                            if (strDepId.Length != 0)
                            {
                                query = @"INSERT INTO TBL_USER_DIVISION_DEP(USER_ID,SUBADMIN_ID,DIVISION_ID,DEPARTMENT_ID,COMPANY_ID) VALUES(@USER_ID,@SUBADMIN_ID,@DIVISION_ID,@DEPARTMENT_ID,@COMPANY_ID);";

                                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                                objSqlCommand.CommandType = CommandType.Text;
                                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                                objSqlCommand.Parameters.AddWithValue("@DIVISION_ID", strDivId);
                                objSqlCommand.Parameters.AddWithValue("@DEPARTMENT_ID", strDepId);
                                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                                objSqlCommand.ExecuteNonQuery();
                            }
                        }
                    }
                }
                insertUserGroupLinkDetails(objSqlConnection, objSqlTransaction);
                insertMgramGroups(lstExistingGrps, lstNonExistingGrpsINMgram);

                //SubAdminLog.saveSubAdminActivityLog(
                //    "S", this.SubAdminId,
                //    this.UserId, this.CompanyId,
                //    SUBADMIN_ACTIVITY_LOG.MOBILE_USER_ADD,
                //    dateTimeOfAddition, "", "", "", "",
                //    objSqlConnection, objSqlTransaction);

                //try
                //{
                //    if (dtblSubAdminDtl != null &&
                //        dtblSubAdminDtl.Rows.Count > 0)
                //    {
                //        SaveEmailInfo.cpUserAdded(
                //            Convert.ToString(dtblSubAdminDtl.Rows[0]["FULL_NAME"]),
                //            this.UserName,
                //            this.FirstName + " " + this.LastName,
                //            fullDate,
                //            this.CompanyId,
                //            this.Email,
                //            this.AppContext
                //            );
                //    }
                //}
                //catch
                //{ }

                objSqlTransaction.Commit();
                Utilities.saveActivityLog(objSqlConnection, mFicientCommonProcess.ACTIVITYENUM.USER_CREATED, this.CompanyId, this.SubAdminId, this.UserId, this.UserName, this.FirstName + ' ' + this.LastName, "", "", "", "", "");
            }
            catch (Exception ex)
            {
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
            //}
        }


        #region Process Function
        void insertUserGroupLinkDetails(SqlConnection con, SqlTransaction transaction)
        {
            if (this.UserGroupIds != null)
            {
                List<MFEUserGroupLink> lstUserGrpLink = new List<MFEUserGroupLink>();
                foreach (string groupId in this.UserGroupIds)
                {
                    MFEUserGroupLink objUserGrpLink =
                        new MFEUserGroupLink();
                    objUserGrpLink.EnterpriseId = this.CompanyId;
                    objUserGrpLink.UserId = this.UserId;
                    objUserGrpLink.GroupId = groupId;
                    objUserGrpLink.CreatedOn = DateTime.UtcNow.Ticks;

                    lstUserGrpLink.Add(objUserGrpLink);
                }
                InsertUserGroupLink objInsertUsrGrpLink =
                        new InsertUserGroupLink();
                objInsertUsrGrpLink.Process(con, transaction, lstUserGrpLink);
            }
        }
        void insertMgramGroups(
            List<MFEMgramEnterpriseGroups> lstExistingGrps,
            List<MFEMgramEnterpriseGroups> lstNonExistingGrpsInMgram)
        {
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                string strConnectionString = MSSqlClient.getConnectionStringFromWebConfig(
                MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM);
                MSSqlClient.SqlConnectionOpen(out con, strConnectionString);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        //Add this Username to existing list of user name in mGram Database
                        if (lstExistingGrps != null)
                        {
                            foreach (MFEMgramEnterpriseGroups mGramEnterPriseGrps in lstExistingGrps)
                            {
                                //Check if user name already exist.If not then add.
                                if (!String.IsNullOrEmpty(mGramEnterPriseGrps.Users) &&
                                    !mGramEnterPriseGrps.Users.Contains(this.UserName))
                                {

                                    mGramEnterPriseGrps.Users = mGramEnterPriseGrps.Users + "," + this.UserName;
                                }
                                else
                                {
                                    mGramEnterPriseGrps.Users = this.UserName;
                                }
                            }

                            UpdateMgrmEnterpriseGroupsByGrpId objUpdateMgrmEntrprseGrps =
                                new UpdateMgrmEnterpriseGroupsByGrpId();
                            objUpdateMgrmEntrprseGrps.Process(con, transaction,
                                lstExistingGrps
                                );
                        }
                        if (lstNonExistingGrpsInMgram != null)
                        {
                            InsertMgrmEnterpriseGroups objInsertMgrmEnterpriseGrps =
                                new InsertMgrmEnterpriseGroups();
                            objInsertMgrmEnterpriseGrps.Process(
                                con, transaction,
                                lstNonExistingGrpsInMgram
                                );
                        }

                        transaction.Commit();

                    }
                }

            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }

        }
        #endregion

        DataTable getSubAdminDetails()
        {
            DataTable dtblSubAdminDtl = new DataTable();
            GetSubAdminCompleteDetail objSubAdmin =
                        new GetSubAdminCompleteDetail(this.AdminId,
                            this.SubAdminId,
                            this.CompanyId);
            objSubAdmin.Process();
            if (objSubAdmin.StatusCode == 0)
            {
                dtblSubAdminDtl = objSubAdmin.ResultTables.Tables["Sub Admin Details"];
            }
            return dtblSubAdminDtl;
        }
        #region Helper Functions also used in AddNewUserCompleteDetail
        List<MFEMgramEnterpriseGroups> getMgrmEnterpriseGroup()
        {
            GetAllMgrmEnterpriseGrps objMgrmEnterpriseGroups =
                new GetAllMgrmEnterpriseGrps(this.CompanyId);
            objMgrmEnterpriseGroups.Process();
            return objMgrmEnterpriseGroups.MgramEnterpriseGroups;
        }

        List<MFEGroupDetail> getGroupDtlbyCompany()
        {
            GetGroupDetails objGrpDtls =
                new GetGroupDetails(this.CompanyId);
            objGrpDtls.Process();
            if (objGrpDtls.StatusCode == 0)
            {
                return objGrpDtls.GroupList;
            }
            else
            {
                throw new Exception();
            }
        }

        void compareIfUserGroupSelectedAlreadyExistInMGram(
            List<MFEMgramEnterpriseGroups> mGramEnterpriseGroups
            , out List<MFEMgramEnterpriseGroups> groupsExistInMgram
            , out List<MFEMgramEnterpriseGroups> groupsNotInMgram)
        {
            groupsExistInMgram = new List<MFEMgramEnterpriseGroups>();
            groupsNotInMgram = new List<MFEMgramEnterpriseGroups>();
            List<MFEGroupDetail> lstAllGroupsDtl = getGroupDtlbyCompany();
            foreach (string groupId in this.UserGroupIds)
            {
                bool blnExists = false;
                MFEMgramEnterpriseGroups objMgramEnterpriseGroup =
                        new MFEMgramEnterpriseGroups();
                if (mGramEnterpriseGroups != null)
                {
                    foreach (MFEMgramEnterpriseGroups mgramEnterpriseGroup in mGramEnterpriseGroups)
                    {
                        if (mgramEnterpriseGroup.GroupId == groupId)
                        {
                            blnExists = true;
                            objMgramEnterpriseGroup = mgramEnterpriseGroup;
                            break;
                        }
                    }
                }
                if (blnExists)
                {
                    groupsExistInMgram.Add(objMgramEnterpriseGroup);
                }
                else
                {
                    objMgramEnterpriseGroup =
                       new MFEMgramEnterpriseGroups();
                    foreach (MFEGroupDetail grpDtl in lstAllGroupsDtl)
                    {
                        if (grpDtl.GroupId == groupId)
                        {
                            objMgramEnterpriseGroup.EnterpriseId = grpDtl.EnterpriseId;
                            objMgramEnterpriseGroup.GroupId = grpDtl.GroupId;
                            objMgramEnterpriseGroup.GroupName = grpDtl.GroupName;
                            objMgramEnterpriseGroup.Users = this.UserName;
                            break;
                        }
                    }

                    groupsNotInMgram.Add(objMgramEnterpriseGroup);
                }
            }
        }
        #endregion

        public Boolean IsActive
        {
            get
            {
                return blIsactive;
            }
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public string UserId
        {
            get
            {
                return strUserId;
            }
            set
            {
                strUserId = value;
            }
        }

        public string SubAdminId
        {
            get
            {
                return strAdminId;
            }
        }
        public string RegionId
        {
            get
            {
                return strRegionId;
            }
        }
        public string DivisionId
        {
            get
            {
                return strDivisionId;
            }
        }
        public string LocationId
        {
            get
            {
                return strLocationId;
            }
        }
        public string DesignationId
        {
            get
            {
                return strDesignationId;
            }
        }
        public string EmployeeNo
        {
            get
            {
                return strEmployeeNo;
            }
        }
        public string UserName
        {
            get
            {
                return strUserName;
            }
        }
        public string Email
        {
            get
            {
                return strEmail;
            }
        }
        public string AccessCode
        {
            get
            {
                return strAccessCode;
            }
        }
        public string FirstName
        {
            get
            {
                return strFirstName;
            }
        }
        public string LastName
        {
            get
            {
                return strLastName;
            }
        }
        //public string Gender
        //{
        //    get
        //    {
        //        return str;
        //    }
        //}
        public string Mobile
        {
            get
            {
                return strMobile;
            }

        }
        public string CompanyId
        {
            get
            {
                return strCompanyId;
            }
        }
        public string DateOfBirth
        {
            get
            {
                return strDateOfBirth;
            }
        }
        public byte AllowMessenger
        {
            get
            {
                return blAllowMessenger;
            }
        }
        public byte AllowOfflineWork
        {
            get
            {
                return blAllowOfflineWork;
            }
        }
        public string RequestedBy
        {
            get
            {
                return strRequestedBy;
            }
        }
        public byte AllowDesktopWork
        {
            get
            {
                return blAllowDesktopWork;
            }
        }
        public string DomainId
        {
            get
            {
                return strDomainId;
            }
        }
        public string AdminId
        {
            get
            {
                return _adminIdOfSubAdmin;
            }
        }
        public HttpContext AppContext
        {
            get { return _appContext; }
            private set { _appContext = value; }
        }
        public List<string> UserGroupIds
        {
            get;
            private set;
        }
    }
    [DataContract]
    public class CredentialProperties
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string unm { get; set; }
        [DataMember]
        public string pwd { get; set; }
    }
}
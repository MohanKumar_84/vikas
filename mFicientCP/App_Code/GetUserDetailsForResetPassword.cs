﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetUserDetailsForResetPassword
    {
        public GetUserDetailsForResetPassword(string userName, string companyId, RESET_PASSWORD_REQUEST_BY pwdResetRequestedBy)
        {
            this.UserName = userName;
            this.CompanyId = companyId;
            this.PwdResetRequestedBy = pwdResetRequestedBy;
        }

        public void Process()
        {
            try
            {
                StatusCode = -1000;
                
                SqlCommand cmd = getCommandToRun();
                cmd.CommandType = CommandType.Text;
                
                DataSet dsResetPwdDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsResetPwdDtls == null) throw new MficientException("Internal server error.");
                if (dsResetPwdDtls.Tables[0].Rows.Count > 0)
                {
                    ResultTable = dsResetPwdDtls.Tables[0];
                    //getting the latest record.
                    fillPropertiesByUserType(ResultTable);
                }
                StatusCode = 0;
                this.StatusDescription = String.Empty;

            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = "Record not found.";
                StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
        }
        void fillPropertiesByUserType(DataTable newPasswordDetails)
        {
            if (newPasswordDetails == null) throw new ArgumentNullException();
            DataRow drLatestRecord = newPasswordDetails.Rows[0];
            switch (PwdResetRequestedBy)
            {
                case RESET_PASSWORD_REQUEST_BY.SubAdmin:
                    this.UserID = Convert.ToString(drLatestRecord["SUBADMIN_ID"]);
                    this.IsUserActive = Convert.ToBoolean(drLatestRecord["ISACTIVE"]);
                    this.IsUserBlocked = Convert.ToBoolean(drLatestRecord["IS_BLOCKED"]);
                    this.EmailID = Convert.ToString(drLatestRecord["EMAIL"]);
                    break;
                case RESET_PASSWORD_REQUEST_BY.Admin:
                    this.UserID = Convert.ToString(drLatestRecord["ADMIN_ID"]);
                    this.IsUserActive = true;
                    this.IsUserBlocked = false;
                    this.EmailID = Convert.ToString(drLatestRecord["EMAIL_ID"]);
                    break;
                case RESET_PASSWORD_REQUEST_BY.User:
                    this.UserID = Convert.ToString(drLatestRecord["USER_ID"]);
                    this.IsUserActive = Convert.ToBoolean(drLatestRecord["IS_ACTIVE"]);
                    this.IsUserBlocked = Convert.ToBoolean(drLatestRecord["IS_BLOCKED"]);
                    this.EmailID = Convert.ToString(drLatestRecord["EMAIL_ID"]);
                    break;
            }
            this.IsExpired = Convert.ToBoolean(drLatestRecord["IS_EXPIRED"]);
            this.NewAccessCode = Convert.ToString(drLatestRecord["NEW_ACCESSCODE"]);
            this.AccessCodeResetDatetime = Convert.ToInt64(drLatestRecord["ACCESSCODE_RESET_DATETIME"]);
        }

        string getQueryByUserType()
        {
            string strQuery = string.Empty;
            switch (this.PwdResetRequestedBy)
            {
                case RESET_PASSWORD_REQUEST_BY.SubAdmin:
                    strQuery = @"SELECT top 1 * FROM TBL_SUB_ADMIN AS SubAdmin 
                                INNER JOIN TBL_RESET_PASSWORD_LOG AS RstLog
                                ON  SubAdmin.SUBADMIN_ID = RstLog.User_ID 
                                WHERE  SubAdmin.USER_NAME=@USER_NAME
                                and SubAdmin.COMPANY_ID=@COMPANY_ID 
                                order by RstLog.ACCESSCODE_RESET_DATETIME desc";
                    break;
                case RESET_PASSWORD_REQUEST_BY.Admin:
                    strQuery = @"SELECT top 1 * FROM TBL_COMPANY_ADMINISTRATOR AS CmpAdmin 
                                INNER JOIN TBL_RESET_PASSWORD_LOG AS RstLog
                                ON  CmpAdmin.ADMIN_ID= RstLog.User_ID 
                                WHERE   CmpAdmin.EMAIL_ID=@USER_NAME
                                and CmpAdmin.COMPANY_ID=@COMPANY_ID 
                                order by RstLog.ACCESSCODE_RESET_DATETIME desc";
                    break;
                case RESET_PASSWORD_REQUEST_BY.User:
                    strQuery = @"SELECT top 1 * FROM TBL_USER_DETAIL AS UsrDet 
                                INNER JOIN TBL_RESET_PASSWORD_LOG AS RstLog
                                ON  UsrDet.USER_ID= RstLog.User_ID 
                                WHERE UsrDet.USER_NAME=@USER_NAME
                                and UsrDet.COMPANY_ID=@COMPANY_ID 
                                order by RstLog.ACCESSCODE_RESET_DATETIME desc";
                    break;
            }
            return strQuery;
        }
        SqlCommand getCommandToRun()
        {
            SqlCommand cmd = new SqlCommand(getQueryByUserType());
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@USER_NAME", UserName);
            cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
            return cmd;
        }

        #region Public Properties
        public DataTable ResultTable
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string UserName
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public RESET_PASSWORD_REQUEST_BY PwdResetRequestedBy
        {
            get;
            private set;
        }

        public string NewAccessCode
        {
            get;
            private set;
        }

        public bool IsExpired
        {
            get;
            private set;
        }

        public long AccessCodeResetDatetime
        {
            get;
            private set;
        }
        public string UserID
        {
            get;
            private set;
        }
        public string EmailID
        {
            get;
            private set;
        }
        /// <summary>
        /// This value is set for User And SubAdmin Only.
        /// </summary>
        public bool IsUserActive
        {
            get;
            private set;
        }
        /// <summary>
        /// This value is set for User And SubAdmin Only.
        /// </summary>
        public bool IsUserBlocked
        {
            get;
            private set;
        }
        #endregion
    }
}
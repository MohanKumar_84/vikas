﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class mPluginAgents
    {
        #region Private Members

        private string _statusDescription,_mPluginAgentId;
        private int _statusCode ;

        #endregion

        #region Public Properties

        public int StatusCode
        {
            get
            {
                return _statusCode;
            }
        }

        public string StatusDescription
        {
            get
            {
                return _statusDescription;
            }
        }
        public string mPluginAgentId
        {
            get
            {
                return _mPluginAgentId;
            }
        }

        #endregion

        #region Public Methods

        public void AddMpluginAgent(string enterpriseId, string mPluginAgentName, string mPluginAgentPassword)
        {
            _mPluginAgentId = Utilities.GetMd5Hash(enterpriseId + mPluginAgentName.Trim() + DateTime.UtcNow.Ticks.ToString());

            string query = "";

            try
            {
                query = @"SELECT * FROM TBL_MPLUGIN_AGENT_DETAIL WHERE MP_AGENT_NAME = @MP_AGENT_NAME AND COMPANY_ID= @COMPANY_ID";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@MP_AGENT_NAME", mPluginAgentName.Trim().ToLower());
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", enterpriseId.Trim());

                DataSet dsMpluginAgents = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (dsMpluginAgents != null && dsMpluginAgents.Tables[0].Rows.Count <= 0)
                {
                    query = @"INSERT INTO TBL_MPLUGIN_AGENT_DETAIL(COMPANY_ID,MP_AGENT_ID,MP_AGENT_NAME,MP_AGENT_PASSWORD)
                        VALUES(@COMPANY_ID,@MP_AGENT_ID,@MP_AGENT_NAME,@MP_AGENT_PASSWORD);";

                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", enterpriseId.Trim());
                    objSqlCommand.Parameters.AddWithValue("@MP_AGENT_ID", mPluginAgentId.Trim());
                    objSqlCommand.Parameters.AddWithValue("@MP_AGENT_NAME", mPluginAgentName.Trim());
                    objSqlCommand.Parameters.AddWithValue("@MP_AGENT_PASSWORD", mPluginAgentPassword.Trim());
                    if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                    {
                        _statusCode = 0;
                        _statusDescription = "";
                    }
                    else
                    {
                        _statusCode = -1000;
                        _statusDescription = "Cannot add Agent";
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                    }
                }
                else
                {
                    _statusCode = -1000;
                    throw new Exception(@"mPlugin agent Name already exist");
                }
            }
            catch (Exception ex)
            {
                _statusCode = -1000;
                _statusDescription = ex.Message;
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }

        public void UpdateMpluginAgent(string mPluginAgentId, string mPluginAgentPassword, string enterpriseId)
        {
            try
            {
                string query = @"UPDATE TBL_MPLUGIN_AGENT_DETAIL SET MP_AGENT_PASSWORD=@MP_AGENT_PASSWORD WHERE MP_AGENT_ID=@MP_AGENT_ID AND COMPANY_ID= @COMPANY_ID;";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@MP_AGENT_ID", mPluginAgentId.Trim());
                objSqlCommand.Parameters.AddWithValue("@MP_AGENT_PASSWORD", mPluginAgentPassword.Trim());
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", enterpriseId.Trim());

                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                {
                    _statusCode = 0;
                    _statusDescription = "";
                }
                else
                {
                    _statusCode = -1000;
                    _statusDescription = "Agent not updated";
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                _statusCode = -1000;
                _statusDescription = ex.Message;
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }

        public void DeleteMpluginAgent(string mPluginAgentId, string enterpriseId)
        {
            try
            {
                string query = @"DELETE FROM TBL_MPLUGIN_AGENT_DETAIL WHERE MP_AGENT_ID=@MP_AGENT_ID AND COMPANY_ID= @COMPANY_ID;";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@MP_AGENT_ID", mPluginAgentId.Trim());
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", enterpriseId.Trim());

                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                {
                    _statusCode = 0;
                    _statusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                _statusCode = -1000;
                _statusDescription = ex.Message;
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }

        public DataSet GetMpluginAgents(string enterpriseId)
        {
            DataSet dsMpluginAgents = new DataSet();
            try
            {
                string query = @"SELECT * FROM TBL_MPLUGIN_AGENT_DETAIL WHERE COMPANY_ID = @COMPANY_ID";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);

                dsMpluginAgents = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (dsMpluginAgents == null) throw new Exception();
                _statusCode = 0;
                _statusDescription = "";
            }
            catch (Exception ex)
            {
                _statusCode = -1000;
                _statusDescription = ex.Message;
                //if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                //{
                //    throw ex;
                //}
            }
            return dsMpluginAgents;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="enterpriseId"></param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Thrown when there is some internal error</exception>
        //public int GetCountOfMpluginAgents(string enterpriseId)
        //{
        //    DataSet dsMpluginAgents = new DataSet();
        //    string query = @"SELECT COUNT(MP_AGENT_ID) AS TOTAL  FROM TBL_MPLUGIN_AGENT_DETAIL WHERE COMPANY_ID = @COMPANY_ID";

        //    SqlCommand objSqlCommand = new SqlCommand(query);
        //    objSqlCommand.CommandType = CommandType.Text;
        //    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);

        //    dsMpluginAgents = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
        //    if (dsMpluginAgents == null) throw new Exception();
        //    return Convert.ToInt32(dsMpluginAgents.Tables[0].Rows[0]["TOTAL"]);
        //}
        public string GetMpluginAgentPassword(string enterpriseId, string agentName)
        {
            DataSet dsMpluginAgents = new DataSet();
            string password = string.Empty;

            try
            {
                string query = @"SELECT MP_AGENT_PASSWORD FROM TBL_MPLUGIN_AGENT_DETAIL WHERE COMPANY_ID = @COMPANY_ID AND MP_AGENT_NAME = @MP_AGENT_NAME";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);
                objSqlCommand.Parameters.AddWithValue("@MP_AGENT_NAME", agentName);

                dsMpluginAgents = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (dsMpluginAgents.Tables[0].Rows.Count > 0)
                {
                    password = dsMpluginAgents.Tables[0].Rows[0][@"MP_AGENT_PASSWORD"].ToString();
                    _statusCode = 0;
                    _statusDescription = "";
                }
                else
                {
                    throw new Exception(@"Agent not found");
                }
            }
            catch (Exception ex)
            {
                _statusCode = -1000;
                _statusDescription = ex.Message;
                //if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                //{
                //    throw ex;
                //}
            }
            return password;
        }

        public bool AllowAgentsToAdd(string enterpriseId)
        {
            try
            {
                string query = @"SELECT * FROM TBL_ACCOUNT_SETTINGS WHERE COMPANY_ID = @COMPANY_ID";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);

                DataSet dsMpluginAgents = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                _statusCode = 0;
                _statusDescription = "";

                DataSet dsAgents = GetMpluginAgents(enterpriseId);

                if (dsMpluginAgents != null && dsMpluginAgents.Tables[0].Rows.Count > 0)
                {
                    if (dsAgents != null && dsAgents.Tables[0].Rows.Count < Convert.ToInt32(dsMpluginAgents.Tables[0].Rows[0]["MAX_MPLUGIN_AGENTS"].ToString()))
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                _statusCode = -1000;
                _statusDescription = ex.Message;
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
            return false;
        }

        #endregion
    }
}
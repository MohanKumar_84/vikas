﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


namespace mFicientCP
{
    public class UpdateUserGroupDetails
    {
        public UpdateUserGroupDetails(string companyId, string subAdminId, List<string> _lsit, string gropuIdInEdit, string groupname, string addappingroup, string removeappingroup)
        {
            this.CompanyId = companyId;
            this.SubAdminId = subAdminId;
            this.Slst1 = _lsit;
            this.GroupIdToEdit = gropuIdInEdit;
            this.AddappinGroup = addappingroup;
            this.RemoveappinGroup = removeappingroup;
            this.Groupname = groupname;
        }

        public void Process()
        {
            StatusCode = -1000;
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {

                        RemoveAllExistingWFAndGroupsLinkByGropuId(transaction, con);
                        foreach (string wfID in Slst1)
                        {
                            UpdateNewGroups(transaction, con, wfID);
                        }
                        transaction.Commit();
                    }
                    StatusCode = 0;
                    StatusDescription = "";

                    if (this.AddappinGroup != "")
                        Utilities.saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.GROUP_MODIFIED_APP_ADDED, this.CompanyId, this.SubAdminId, this.GroupIdToEdit, this.Groupname, "", "", "", "", "", this.AddappinGroup);
                    if (this.RemoveappinGroup != "")
                        Utilities.saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.GROUP_MODIFIED_APP_DELETED, this.CompanyId, this.SubAdminId, this.GroupIdToEdit, this.Groupname, "", "", "", "", "", this.RemoveappinGroup);
                }
            }
            catch
            {
                if (transaction != null) transaction.Rollback();
                StatusCode = -1000;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }

        void RemoveExistingGroups(SqlTransaction transaction, SqlConnection connection, SelectetdGroupList appSelected)
        {
            string strQuery = @"delete from  dbo.TBL_WORKFLOW_AND_GROUP_LINK where WORKFLOW_ID=@WORKFLOW_ID";
            SqlCommand cmd = new SqlCommand(strQuery, connection, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@WORKFLOW_ID", appSelected.COMPANY_ID);
            cmd.ExecuteNonQuery();
        }
        void RemoveAllExistingWFAndGroupsLinkByGropuId(SqlTransaction transaction, SqlConnection connection)
        {
            string strQuery = @"delete from  dbo.TBL_WORKFLOW_AND_GROUP_LINK where GROUP_ID =@GROUP_ID";
            SqlCommand cmd = new SqlCommand(strQuery, connection, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@GROUP_ID", this.GroupIdToEdit);
            cmd.ExecuteNonQuery();
        }
        void UpdateNewGroups(SqlTransaction transaction, SqlConnection connection, string appSelected1)
        {
            string strQuery = @"insert into dbo.TBL_WORKFLOW_AND_GROUP_LINK (COMPANY_ID,GROUP_ID,WORKFLOW_ID) VALUES(@COMPANY_ID,@GROUP_ID,@WORKFLOW_ID);";
            SqlCommand cmd = new SqlCommand(strQuery, connection, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
            cmd.Parameters.AddWithValue("@GROUP_ID", GroupIdToEdit);
            cmd.Parameters.AddWithValue("@WORKFLOW_ID", appSelected1);

            cmd.ExecuteNonQuery();
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string GroupIdToEdit
        {
            get;
            set;
        }

        public string AddappinGroup
        {
            get;
            set;
        }

        public string RemoveappinGroup
        {
            get;
            set;
        }

        public string Groupname
        {
            get;
            set;
        }

        public List<string> Slst1 { get; set; }

    }
    public class SelectetdGroupList
    {
        public string GROUP_ID { get; set; }
        public string COMPANY_ID { get; set; }
        public string WORKFLOW_ID { get; set; }

    }
}
﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Data.SqlClient;
//using System.Data;

//namespace mFicientCP
//{
//    public class GetSubAdminDivision
//    {
//        public GetSubAdminDivision()
//        { 

//        }

//       public GetSubAdminDivision(string _SubAdminId, string _DivisionId)
//       {
//           this.SubAdminId = _SubAdminId;
//           this.DivisionId = _DivisionId;
//        }
//        public void Process()
//        {
//            try
//            {
//                string query;
//                DataSet objDataSet;
//                SqlCommand objSqlCommand;
//                if (this.DivisionId.Length == 0)
//                {
//                    query = @"SELECT  DISTINCT tsd.DIVISION_ID,td.DIVISION_NAME FROM  TBL_SUBADMIN_DIVISION_DEP tsd
//                                INNER JOIN TBL_DIVISION td ON tsd.DIVISION_ID=td.DIVISION_ID
//                                    WHERE tsd.SUBADMIN_ID=@SUBADMIN_ID";
//                    objSqlCommand = new SqlCommand(query);
//                    objSqlCommand.CommandType = CommandType.Text;
//                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
//                }
//                else {
//                    //28/8/2012 order by department_name
//                    query = @"SELECT  *,td.DEPARTMENT_NAME FROM  TBL_SUBADMIN_DIVISION_DEP tsd 
//                                    INNER JOIN TBL_DEPARTMENT td ON tsd.DEPARTMENT_ID=td.DEPARTMENT_ID
//                                WHERE tsd.SUBADMIN_ID=@SUBADMIN_ID AND tsd.DIVISION_ID=@DIVISION_ID
//                                ORDER BY td.DEPARTMENT_NAME;";
//                    objSqlCommand = new SqlCommand(query);
//                    objSqlCommand.CommandType = CommandType.Text;
//                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
//                    objSqlCommand.Parameters.AddWithValue("@DIVISION_ID", this.DivisionId);
//                }
//                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
//                if (objDataSet.Tables[0].Rows.Count > 0)
//                {
//                    this.ResultTable = objDataSet.Tables[0];
//                    this.StatusCode = 0;
//                    this.StatusDescription = "";
//                }
//                else
//                {
//                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
//                }

//            }
//            catch 
//            {
//                this.StatusCode = -1000;
//                this.StatusDescription = "Internal server error";
//            }
//        }
//        public string SubAdminId
//        {
//            set;
//            get;
//        }
//        public string DivisionId
//        {
//            set;
//            get;
//        }
//        public DataTable ResultTable
//        {
//            set;
//            get;
//        }
//        public int StatusCode
//        {
//            set;
//            get;
//        }
//        public string StatusDescription
//        {
//            set;
//            get;
//        }
//    }
//}
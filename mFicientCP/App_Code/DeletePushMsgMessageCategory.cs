﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class DeletePushMsgMessageCategory
    {
        public DeletePushMsgMessageCategory(string companyId, string userId, string msgCategoryId)
        {
            this.CompanyId = companyId;
            this.UserId = userId;
            this.MsgCategoryId = msgCategoryId;
        }
        public void Process()
        {
            try
            {
                string strQuery = @"DELETE FROM TBL_PUSH_MSG_MESSAGE_CATEGORY
                                    WHERE MSG_CATEGORY_ID = @MSG_CATEGORY_ID
                                    AND USER_ID= @USER_ID
                                    AND COMPANY_ID=@COMPANY_ID
                                    ";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@MSG_CATEGORY_ID", this.MsgCategoryId);
                cmd.Parameters.AddWithValue("@USER_ID", this.UserId);
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                if (MSSqlClient.ExecuteNonQueryRecord(cmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
                }
            }
            catch
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
            }

        }


        public string CompanyId
        {
            get;
            private set;
        }
        public string StatusDescription
        {
            get;
            private set;
        }
        public int StatusCode
        {
            get;
            private set;
        }

        public string MessageCategory
        {
            get;
            private set;
        }
        public string UserId
        {
            get;
            private set;
        }
        public string MsgCategoryId
        {
            get;
            private set;
        }
    }
}
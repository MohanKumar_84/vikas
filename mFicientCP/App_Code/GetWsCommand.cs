﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetWsCommand
    {
       public GetWsCommand()
        { 

        }
       public GetWsCommand(Boolean _IsSelectAll, Boolean _FindConnectedApp, string _SubAdminId, string _CommandId, string _CommandName, string _CompanyId)
        {
            this.IsSelectAll = _IsSelectAll;
            this.SubAdminId = _SubAdminId;
            this.CommandId = _CommandId;
            this.CommandName = _CommandName;
            this.FindConnectedApp = _FindConnectedApp;
            this.CompanyId = _CompanyId;
        }

       public GetWsCommand(Boolean _IsSelectAll, Boolean _FindConnectedApp, string _SubAdminId, string _CommandId, string _CommandName, string _CompanyId,string _NewIde)
       {
           this.IsSelectAll = _IsSelectAll;
           this.SubAdminId = _SubAdminId;
           this.CommandId = _CommandId;
           this.CommandName = _CommandName;
           this.FindConnectedApp = _FindConnectedApp;
           this.CompanyId = _CompanyId;
           this.NewIde = _NewIde;
       }
         public GetWsCommand(string _CommandId, string _CommandName)
        {
           this.CommandId = _CommandId;
          this.CommandName = _CommandName;
          GetProcess();
            
        }

         public void  GetProcess()
         {
             try
             {
                 this.StatusCode = -1000;
                 string query;
                 DataSet objDataSet;
                 SqlCommand objSqlCommand;
                 query = @"select * from dbo.TBL_WS_COMMAND where WS_COMMAND_NAME =@WS_COMMAND_NAME";
                 objSqlCommand = new SqlCommand(query);
                 objSqlCommand.CommandType = CommandType.Text;
                 objSqlCommand.Parameters.AddWithValue("@WS_COMMAND_NAME", this.CommandName);
                 objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
               if (this.ResultTable != null)
                   if (this.ResultTable.Rows.Count != 0)
                       this.StatusCode = 0;
              }
           catch
           {
               this.StatusCode = -1000;
           }
         }




        public void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                if (this.IsSelectAll == true)
                {
                    query = @"SELECT  cd.RETURN_TYPE,cd.COMPANY_ID,cd.WS_COMMAND_ID,cd.WS_CONNECTOR_ID,cd.SUBADMIN_ID,cd.URL,cd.PARAMETER,cd.RETURN_TYPE RECORD_PATH,cd.DATASET_PATH,cd.TAG,cd.WS_COMMAND_NAME,cd.SERVICE,cd.PORT_TYPE,cd.METHOD,cd.WEBSERVICE_TYPE SOAP_REQUEST,cd.HTTP_REQUEST,cd.OPERATION_ACTION,cd.HTTP_TYPE
                    ,cd.DESCRIPTION,cd.CACHE EXPIRY_FREQUENCY,cd.EXPIRY_CONDITION,cd.X_REFERENCES,cd.HEADERS,isnull(cd.CREATED_BY,'') as CREATED_BY,isnull(cd.CREATED_ON,0) as CREATED_ON
                    ,isnull(cd.UPDATED_ON,0) as UPDATED_ON,cn.CONNECTION_NAME,SC.FULL_NAME,isnull(st.FULL_NAME,'') as CreatedBY,cn.WEBSERVICE_URL,cn.WEBSERVICE_TYPE  FROM 
                      TBL_WS_COMMAND cd  INNER JOIN TBL_WEBSERVICE_CONNECTION cn ON cn.WS_CONNECTOR_ID=cd.WS_CONNECTOR_ID 
                      left outer join dbo.TBL_SUB_ADMIN as SC on SC.SUBADMIN_ID=cd.SUBADMIN_ID 
                 left outer join dbo.TBL_SUB_ADMIN as st on cd.CREATED_BY = st.SUBADMIN_ID  collate SQL_Latin1_General_CP1_CI_AS 
                   WHERE  cd.COMPANY_ID=@COMPANY_ID ORDER BY cd.WS_COMMAND_NAME";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

                }
                else
                {
                    if (this.CommandName.Length == 0)
                    {
                            query = @"SELECT  cd.*,cn.CONNECTION_NAME FROM  TBL_WS_COMMAND cd 
	                                                        LEFT JOIN TBL_WEBSERVICE_CONNECTION cn ON cn.WS_CONNECTOR_ID=cd.WS_CONNECTOR_ID 
	                                WHERE  cd.COMPANY_ID=@COMPANY_ID  AND cd.WS_COMMAND_ID=@WS_COMMAND_ID ORDER BY cd.WS_COMMAND_NAME;";


                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.Parameters.AddWithValue("@WS_COMMAND_ID", this.CommandId);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    }
                    else
                    {
                        query = @"SELECT cd.*,SC.FULL_NAME,st.FULL_NAME as CreatedBY FROM  TBL_WS_COMMAND as cd

                          inner join dbo.TBL_SUB_ADMIN as SC on SC.SUBADMIN_ID=cd.SUBADMIN_ID 
                     inner join dbo.TBL_SUB_ADMIN as st on cd.CREATED_BY = st.SUBADMIN_ID WHERE  cd.COMPANY_ID=@COMPANY_ID  AND cd.WS_COMMAND_NAME=@WS_COMMAND_NAME ORDER BY WS_COMMAND_NAME;";
                        
                        
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@WS_COMMAND_NAME", this.CommandName);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    }
                }

                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
                if(this.ResultTable!=null)
                            this.StatusCode = 0;
                if (FindConnectedApp)
                {
                    FormDetails = objDataSet.Tables[1];
                    AppDetails = objDataSet.Tables[2];
                }
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }

        public List<IdeWsCmdsForJson> GetWsCommandDetail()
        {
            List<IdeWsCmdsForJson> lstWsCmds = new List<IdeWsCmdsForJson>();
            if (this.ResultTable != null)
            {
                foreach (DataRow row in this.ResultTable.Rows)
                {
                    IdeWsCmdsForJson objWsCmd = new IdeWsCmdsForJson();
                    objWsCmd.compId = Convert.ToString(row["COMPANY_ID"]);
                    objWsCmd.commandName = Convert.ToString(row["WS_COMMAND_NAME"]).Trim();
                    objWsCmd.commandId = Convert.ToString(row["WS_COMMAND_ID"]).Trim();
                    objWsCmd.connectorId = Convert.ToString(row["WS_CONNECTOR_ID"]).Trim();
                    objWsCmd.connectionName = Convert.ToString(row["CONNECTION_NAME"]).Trim();
                    objWsCmd.url = Convert.ToString(row["URL"]).Trim();
                    objWsCmd.parameter = Convert.ToString(row["PARAMETER"]).Trim();
                    objWsCmd.returnType = Convert.ToString(row["RETURN_TYPE"]).Trim();
                    objWsCmd.recordPath = Convert.ToString(row["RECORD_PATH"]).Trim();
                    objWsCmd.datasetPath = Convert.ToString(row["DATASET_PATH"]).Trim();
                    objWsCmd.tag = Convert.ToString(row["TAG"]).Trim();
                    objWsCmd.service = Convert.ToString(row["SERVICE"]).Trim();
                    objWsCmd.portType = Convert.ToString(row["PORT_TYPE"]).Trim();
                    objWsCmd.method = Convert.ToString(row["METHOD"]).Trim();
                    objWsCmd.webserviceType = Convert.ToString(row["WEBSERVICE_TYPE"]).Trim();
                    objWsCmd.httpType = Convert.ToString(row["HTTP_TYPE"]).Trim();
                    objWsCmd.description = Convert.ToString(row["DESCRIPTION"]).Trim();
                    lstWsCmds.Add(objWsCmd);
                }
                DataTable dtblFilteredTable = null;
                string filterType = "rpc";
                string filter = String.Format("WEBSERVICE_TYPE = '{0}'", filterType);
                DataView dtView = new DataView(this.ResultTable,
                                        filter,
                                        "WEBSERVICE_TYPE",
                                        DataViewRowState.CurrentRows);

                if (dtView != null)
                {
                    dtblFilteredTable = dtView.ToTable();
                    //string test = getInputOutputParamJsonForCommand(COMMAND_TYPE.XmlRpc, dtblFilteredTable);
                }
                filterType = "wsdl";
                filter = String.Format("WEBSERVICE_TYPE = '{0}'", filterType);
                dtView = new DataView(this.ResultTable,
                                       filter,
                                       "WEBSERVICE_TYPE",
                                       DataViewRowState.CurrentRows);

                if (dtView != null)
                {
                    dtblFilteredTable = dtView.ToTable();
                    //string test2 = getInputOutputParamJsonForCommand(COMMAND_TYPE.Wsdl, dtblFilteredTable);
                }
                filterType = "http";
                filter = String.Format("WEBSERVICE_TYPE = '{0}'", filterType);
                dtView = new DataView(this.ResultTable,
                                       filter,
                                       "WEBSERVICE_TYPE",
                                       DataViewRowState.CurrentRows);

                if (dtView != null)
                {
                    dtblFilteredTable = dtView.ToTable();
                    //string test3 = getInputOutputParamJsonForCommand(COMMAND_TYPE.Http, dtblFilteredTable);
                }
            }
            return lstWsCmds;
        }

        public void Process1()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                if (this.IsSelectAll == true)
                {
                    query = @"SELECT  cd.RETURN_TYPE,cd.COMPANY_ID,cd.WS_COMMAND_ID,cd.WS_CONNECTOR_ID,cd.SUBADMIN_ID,cd.URL,cd.PARAMETER,cd.RETURN_TYPE RECORD_PATH,cd.DATASET_PATH,cd.TAG,cd.WS_COMMAND_NAME,cd.SERVICE,cd.PORT_TYPE,cd.METHOD,cd.WEBSERVICE_TYPE SOAP_REQUEST,cd.HTTP_REQUEST,cd.OPERATION_ACTION,cd.HTTP_TYPE
                    ,cd.DESCRIPTION,cd.CACHE EXPIRY_FREQUENCY,cd.EXPIRY_CONDITION,cd.X_REFERENCES,isnull(cd.CREATED_BY,'') as CREATED_BY,isnull(cd.CREATED_ON,0) as CREATED_ON
                    ,isnull(cd.UPDATED_ON,0) as UPDATED_ON,cn.CONNECTION_NAME,SC.FULL_NAME,cd.HEADERS,isnull(st.FULL_NAME,'') as CreatedBY,cn.WEBSERVICE_URL,cn.WEBSERVICE_TYPE  FROM 
                      TBL_WS_COMMAND cd  INNER JOIN TBL_WEBSERVICE_CONNECTION cn ON cn.WS_CONNECTOR_ID=cd.WS_CONNECTOR_ID 
                      left outer join dbo.TBL_SUB_ADMIN as SC on SC.SUBADMIN_ID=cd.SUBADMIN_ID 
                 left outer join dbo.TBL_SUB_ADMIN as st on cd.CREATED_BY = st.SUBADMIN_ID  collate SQL_Latin1_General_CP1_CI_AS 
                   WHERE  cd.COMPANY_ID=@COMPANY_ID ORDER BY cd.WS_COMMAND_NAME";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

                }
                else
                {
                    if (this.CommandName.Length == 0)
                    {
                        query = @"SELECT  cd.*,cn.CONNECTION_NAME FROM  TBL_WS_COMMAND cd 
                                    LEFT JOIN TBL_WEBSERVICE_CONNECTION cn ON cn.WS_CONNECTOR_ID=cd.WS_CONNECTOR_ID 
	                                WHERE  cd.COMPANY_ID=@COMPANY_ID  AND cd.WS_COMMAND_ID=@WS_COMMAND_ID ORDER BY cd.WS_COMMAND_NAME;";
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.Parameters.AddWithValue("@WS_COMMAND_ID", this.CommandId);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    }
                    else
                    {
                        query = @"SELECT cd.*,SC.FULL_NAME,st.FULL_NAME as CreatedBY FROM  TBL_WS_COMMAND as cd
                                    inner join dbo.TBL_SUB_ADMIN as SC on SC.SUBADMIN_ID=cd.SUBADMIN_ID 
                                    inner join dbo.TBL_SUB_ADMIN as st on cd.CREATED_BY = st.SUBADMIN_ID WHERE  cd.COMPANY_ID=@COMPANY_ID  AND cd.WS_COMMAND_NAME=@WS_COMMAND_NAME ORDER BY WS_COMMAND_NAME;";


                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@WS_COMMAND_NAME", this.CommandName);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    }
                }

                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
                if (this.ResultTable != null)
                    if (this.ResultTable.Rows.Count != 0)
                        this.StatusCode = 0;
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }

       
        public Boolean IsSelectAll
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
      
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public string SubAdminId { get; set; }

        public string CommandId { get; set; }

        public string CommandName { get; set; }

        public bool FindConnectedApp { get; set; }

        public DataTable FormDetails { get; set; }

        public DataTable AppDetails { get; set; }

        public string CompanyId { get; set; }

        public string NewIde { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class AddDatabaseConnection
    {
       //public AddDatabaseConnection()
       // { 

       // }

       //public AddDatabaseConnection(string _SubAdminId, string _ConnectionName, string _HostName, string _Database, string _UserId, string _Password, string _DataBaseType, string _TimeOut, string _AdditinalString, string _MpluginAgent, string _CompanyID)
       // {
       //     this.SubAdminId = _SubAdminId;
       //     this.UserId = _UserId;
       //     this.Password = _Password;
       //     this.DataBaseType = _DataBaseType;
       //     this.ConnectionName = _ConnectionName;
       //     this.HostName = _HostName;
       //     this.Database = _Database;
       //     this.AdditinalString = _AdditinalString;
       //     this.TimeOut = _TimeOut;
       //     this.MpluginAgent = _MpluginAgent;
       //     this.CompanyID = _CompanyID;
       // }
       public AddDatabaseConnection(string _SubAdminId, string _ConnectionName, string _HostName, string _Database,  string _DataBaseType, string _TimeOut, string _AdditinalString, string _MpluginAgent, string _CompanyID, string _CreatedBy, long _CreatedOn, long _UpdatedOn, string _Credential)
       {
           this.SubAdminId = _SubAdminId;
           this.DataBaseType = _DataBaseType;
           this.ConnectionName = _ConnectionName;
           this.HostName = _HostName;
           this.Database = _Database;
           this.AdditinalString = _AdditinalString;
           this.TimeOut = _TimeOut;
           this.MpluginAgent = _MpluginAgent;
           this.CompanyID = _CompanyID;
           this.CreatedBy = _CreatedBy;
           this.CreatedOn = _CreatedOn;
           this.UpdatedOn = _UpdatedOn;
           this.Credential = _Credential;
       }
//        public void Process()
//        {
//            try
//            {
//                this.StatusCode = -1000;
//                this.ConnectionId = Utilities.GetMd5Hash(this.SubAdminId + this.ConnectionName + DateTime.UtcNow.Ticks.ToString());

//                string query = @"INSERT INTO TBL_DATABASE_CONNECTION(COMPANY_ID,SUBADMIN_ID,CONNECTION_NAME,HOST_NAME,DATABASE_NAME,USER_ID,PASSWORD,DB_CONNECTOR_ID,DATABASE_TYPE,ADDITIONAL_STRING,TIME_OUT,MPLUGIN_AGENT)
//                        VALUES(@COMPANY_ID,@SUBADMIN_ID,@CONNECTION_NAME,@HOST_NAME,@DATABASE_NAME,@USER_ID,@PASSWORD,@CONNECTION_ID,@DATABASE_TYPE,@ADDITIONAL_STRING,@TIME_OUT,@MPLUGIN_AGENT);";

//                SqlCommand objSqlCommand = new SqlCommand(query);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyID);
//                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
//                objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
//                objSqlCommand.Parameters.AddWithValue("@HOST_NAME", this.HostName);
//                objSqlCommand.Parameters.AddWithValue("@DATABASE_NAME", this.Database);
//                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
//                objSqlCommand.Parameters.AddWithValue("@PASSWORD", this.Password);
//                objSqlCommand.Parameters.AddWithValue("@CONNECTION_ID", this.ConnectionId);
//                objSqlCommand.Parameters.AddWithValue("@DATABASE_TYPE", this.DataBaseType);
//                objSqlCommand.Parameters.AddWithValue("@TIME_OUT", this.TimeOut);
//                objSqlCommand.Parameters.AddWithValue("@ADDITIONAL_STRING", this.AdditinalString);
//                objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgent);
//                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
//                    this.StatusCode = 0;
//            }
//            catch 
//            {
//                this.StatusCode = -1000;
//            }
//        }
        public void Process1()
        {
            try
            {
                this.StatusCode = -1000;
                this.ConnectionId = Utilities.GetMd5Hash(this.SubAdminId + this.ConnectionName + DateTime.UtcNow.Ticks.ToString());

                string query = @"INSERT INTO TBL_DATABASE_CONNECTION(COMPANY_ID,SUBADMIN_ID,CONNECTION_NAME,HOST_NAME,DATABASE_NAME,DB_CONNECTOR_ID,DATABASE_TYPE,ADDITIONAL_STRING,TIME_OUT,MPLUGIN_AGENT,CREATED_BY,CREATED_ON,UPDATED_ON,CREDENTIAL_PROPERTY)
                        VALUES(@COMPANY_ID,@SUBADMIN_ID,@CONNECTION_NAME,@HOST_NAME,@DATABASE_NAME,@CONNECTION_ID,@DATABASE_TYPE,@ADDITIONAL_STRING,@TIME_OUT,@MPLUGIN_AGENT,@CREATED_BY,@CREATED_ON,@UPDATED_ON,@CREDENTIAL_PROPERTY);";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyID);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
                objSqlCommand.Parameters.AddWithValue("@HOST_NAME", this.HostName);
                objSqlCommand.Parameters.AddWithValue("@DATABASE_NAME", this.Database);
                objSqlCommand.Parameters.AddWithValue("@CONNECTION_ID", this.ConnectionId);
                objSqlCommand.Parameters.AddWithValue("@DATABASE_TYPE", this.DataBaseType);
                objSqlCommand.Parameters.AddWithValue("@TIME_OUT", this.TimeOut);
                objSqlCommand.Parameters.AddWithValue("@ADDITIONAL_STRING", this.AdditinalString);
                objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgent);
                objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.CreatedBy);
                objSqlCommand.Parameters.AddWithValue("@CREATED_ON", this.CreatedOn);
                objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", this.UpdatedOn);
                objSqlCommand.Parameters.AddWithValue("@CREDENTIAL_PROPERTY", this.Credential);
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                    this.StatusCode = 0;
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string Database
        {
            set;
            get;
        }
        public string UserId
        {
            set;
            get;
        }
        public string Password
        {
            set;
            get;
        }
        public string DataBaseType
        {
            set;
            get;
        }
        public string ConnectionName
        {
            set;
            get;
        }
        public string HostName
        {
            set;
            get;
        }
        public string ConnectionId
        {
            set;
            get;
        }
        public string Credential
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }

        public string AdditinalString { get; set; }

        public string TimeOut { get; set; }

        public bool UseMplugin { get; set; }

        public string CompanyID { get; set; }

        public string MpluginAgent { get; set; }

        public string CreatedBy { get; set; }

        public long CreatedOn { get; set; }

        public long UpdatedOn { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetCompanyAdministrator
    {
        string _adminId, _emailId, _firstName, _lastName, _statusDescription
            , _resellerId;
        bool _isTrial;
        int _statusCode = -1000;
        public GetCompanyAdministrator(string adminId)
        {
            _adminId = adminId;
        }
               
        public void ProcessUsingCpTable(string companyId)
        {
            try
            {
                string strQuery = @"SELECT * FROM TBL_COMPANY_ADMINISTRATOR
                                WHERE ADMIN_ID = @AdminId
                                AND COMPANY_ID = @COMPANY_ID";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@AdminId", _adminId);
                cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);

                DataSet dsAdminDetail = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsAdminDetail != null && dsAdminDetail.Tables[0] != null && dsAdminDetail.Tables[0].Rows.Count > 0)
                {
                    DataTable dtblAdminDtl = dsAdminDetail.Tables[0];
                    _firstName = Convert.ToString(dtblAdminDtl.Rows[0]["FIRST_NAME"]);
                    _emailId = Convert.ToString(dtblAdminDtl.Rows[0]["EMAIL_ID"]);
                    _isTrial = Convert.ToBoolean(dtblAdminDtl.Rows[0]["IS_TRIAL"]);
                    _lastName = Convert.ToString(dtblAdminDtl.Rows[0]["LAST_NAME"]);
                    _statusCode = 0;
                    _statusDescription = "";

                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                _statusCode = -1000;
                _statusDescription = "Internal server error";
            }

        }

        public int StatusCode
        {
            get { return _statusCode; }
        }
        public bool IsTrial
        {
            get { return _isTrial; }
        }
        public string LastName
        {
            get { return _lastName; }
        }

        public string FirstName
        {
            get { return _firstName; }
        }

        public string EmailId
        {
            get { return _emailId; }
        }
        public string AdminId
        {
            get { return _adminId; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
        }
        public string ResellerId
        {
            get { return _resellerId; }
        }
    }
}
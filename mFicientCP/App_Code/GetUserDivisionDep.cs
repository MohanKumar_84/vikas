﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetUserDivisionDep
    {
        public GetUserDivisionDep()
        { 

        }

        public GetUserDivisionDep(string _SubAdminId, string _UserId, string _DivisionId, string _CompanyId)
       {
           this.SubAdminId = _SubAdminId;
           this.DivisionId = _DivisionId;
           this.UserId = _UserId;
           this.CompanyId = _CompanyId;
        }
        public void Process()
        {
            try
            {
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                if (this.DivisionId.Length == 0)
                {
                    //changed on 12/6/2012 Mohan original query below
                    //query = @"SELECT  DISTINCT DIVISION_ID FROM  TBL_USER_DIVISION_DEP WHERE USER_ID=@USER_ID AND SUBADMIN_ID=@SUBADMIN_ID;";
                    query = @"SELECT  * FROM  TBL_USER_DIVISION_DEP WHERE USER_ID=@USER_ID AND COMPANY_ID=@COMPANY_ID;";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                     objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                     objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                }
                else {
                    query = @"SELECT  * FROM  TBL_USER_DIVISION_DEP WHERE USER_ID=@USER_ID AND COMPANY_ID=@COMPANY_ID AND DIVISION_ID=@DIVISION_ID;";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    //objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                    objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                    objSqlCommand.Parameters.AddWithValue("@DIVISION_ID", this.DivisionId);
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                }
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }

            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string DivisionId
        {
            set;
            get;
        }
        public string UserId
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Data;
//using System.Data.SqlClient;
//using System.Text;
//namespace mFicientCP
//{
//    public class SubAdminLog
//    {
//        private static volatile SubAdminLog objAddSubAdminLog;
//        private const string SPACE_SEPARATOR = " ";
//        private SubAdminLog()
//        { }

//        private static void checkCreateSingleton()
//        {
//            if (objAddSubAdminLog == null)
//                objAddSubAdminLog = new SubAdminLog();
//        }
//        public static int saveSubAdminActivityLog(string userRole, string subAdminId, string userId, string companyId, SUBADMIN_ACTIVITY_LOG processType, long dateTimeTicks
//            , string deviceId, string deviceType, string groupId, string appId)
//        {
//            //checkCreateSingleton();

//            SqlCommand cmd = new SqlCommand(@"INSERT INTO TBL_ACTIVITY_LOG(USER_ROLE,USER_ID,COMPANY_ID,PROCESS_TYPE,PROCESS_ID,ACTIVITY_DATETIME,PARA1,PARA2,PARA3,PARA4,PARA5)
//                            VALUES(@USER_ROLE,@USER_ID,@COMPANY_ID,@PROCCESS_TYPE,@PROCCESS_ID,@ACTIVITY_DATETIME,@PARA1,@PARA2,@PARA3,@PARA4,@PARA5)");
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@USER_ROLE", userRole);
//            cmd.Parameters.AddWithValue("@USER_ID", subAdminId);
//            cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
//            cmd.Parameters.AddWithValue("@PROCCESS_TYPE", (int)processType);
//            //cmd.Parameters.AddWithValue("@PROCCESS_LOG", processLog);
//            cmd.Parameters.AddWithValue("@PROCCESS_ID", Utilities.GetMd5Hash(userId + companyId + DateTime.UtcNow.Ticks.ToString()));
//            cmd.Parameters.AddWithValue("@PARA1", userId);
//            cmd.Parameters.AddWithValue("@PARA2", deviceId);
//            cmd.Parameters.AddWithValue("@PARA3", deviceType);
//            cmd.Parameters.AddWithValue("@PARA4", groupId);
//            cmd.Parameters.AddWithValue("@PARA5", appId);
//            cmd.Parameters.AddWithValue("@ACTIVITY_DATETIME", dateTimeTicks);
//            try
//            {
//                return MSSqlClient.ExecuteNonQueryRecord(cmd);
//            }
//            catch
//            {
//                return 0;
//            }

//        }

////        public static int saveSubAdminActivityLog(string userRole, string subAdminId, string userIdPara1, string companyId, SUBADMIN_ACTIVITY_LOG processType, long dateTimeTicks
////            , string deviceIdPara2, string deviceTypePara3, string groupIdPara4, string appIdPara5, SqlConnection con, SqlTransaction transaction)
////        {
////            //checkCreateSingleton();

////            SqlCommand cmd = new SqlCommand(@"INSERT INTO TBL_ACTIVITY_LOG(USER_ROLE,USER_ID,COMPANY_ID,PROCESS_TYPE,PROCESS_ID,ACTIVITY_DATETIME,PARA1,PARA2,PARA3,PARA4,PARA5)
////                            VALUES(@USER_ROLE,@USER_ID,@COMPANY_ID,@PROCCESS_TYPE,@PROCCESS_ID,@ACTIVITY_DATETIME,@PARA1,@PARA2,@PARA3,@PARA4,@PARA5)", con, transaction);
////            cmd.CommandType = CommandType.Text;
////            cmd.Parameters.AddWithValue("@USER_ROLE", userRole);
////            cmd.Parameters.AddWithValue("@USER_ID", subAdminId);
////            cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
////            cmd.Parameters.AddWithValue("@PROCCESS_TYPE", (int)processType);
////            cmd.Parameters.AddWithValue("@PROCCESS_ID", Utilities.GetMd5Hash(userIdPara1 + companyId + DateTime.UtcNow.Ticks.ToString()));
////            cmd.Parameters.AddWithValue("@PARA1", userIdPara1);
////            cmd.Parameters.AddWithValue("@PARA2", deviceIdPara2);
////            cmd.Parameters.AddWithValue("@PARA3", deviceTypePara3);
////            cmd.Parameters.AddWithValue("@PARA4", groupIdPara4);
////            cmd.Parameters.AddWithValue("@PARA5", appIdPara5);
////            cmd.Parameters.AddWithValue("@ACTIVITY_DATETIME", dateTimeTicks);
////            return cmd.ExecuteNonQuery();

////        }
//        /// <summary>
//        /// Get activity log according to process type
//        /// </summary>
//        /// <param name="subAdminId">Pass empty string to get all the list</param>
//        /// <param name="companyId"></param>
//        /// <param name="processType"></param>
//        /// <param name="fromTime"></param>
//        /// <param name="toTime"></param>
//        /// <returns></returns>
//        public static DataSet getSubAdminActivityLogRaw(string subAdminId, string companyId, SUBADMIN_ACTIVITY_LOG processType, long fromTime, long toTime)
//        {
//            string strQuery = "";
//            if (String.IsNullOrEmpty(subAdminId) && !(processType == SUBADMIN_ACTIVITY_LOG.ALL))
//            {
//                strQuery = @"SELECT [USER_ROLE],[USER_ID],[COMPANY_ID],[PROCESS_TYPE]
//                                ,[PROCESS_ID],[ACTIVITY_DATETIME],[PARA1],[PARA2],[PARA3],[PARA4],[PARA5],SubAdmin.USER_NAME,SubAdmin.FULL_NAME AS SA_FULL_NAME
//                                FROM [TBL_ACTIVITY_LOG] AS Activity
//                                INNER JOIN TBL_SUB_ADMIN SubAdmin
//                                ON Activity.USER_ID = SubAdmin.SUBADMIN_ID
//                                WHERE Activity.COMPANY_ID = @CompanyId
//                                AND Activity.USER_ID = @UserId
//                                AND Activity.PROCESS_TYPE = @ProcessType
//                                AND Activity.ACTIVITY_DATETIME BETWEEN @FromTime AND @ToTime ORDER BY [ACTIVITY_DATETIME] DESC";
//            }
//            else if (!String.IsNullOrEmpty(subAdminId) && processType == SUBADMIN_ACTIVITY_LOG.ALL)
//            {

//                strQuery = @"SELECT [USER_ROLE],[USER_ID],[COMPANY_ID],[PROCESS_TYPE]
//                                ,[PROCESS_ID],[ACTIVITY_DATETIME],[PARA1],[PARA2],[PARA3],[PARA4],[PARA5],SubAdmin.USER_NAME,SubAdmin.FULL_NAME AS SA_FULL_NAME
//                                FROM [TBL_ACTIVITY_LOG] AS Activity
//                                INNER JOIN TBL_SUB_ADMIN SubAdmin
//                                ON Activity.USER_ID = SubAdmin.SUBADMIN_ID
//                                WHERE Activity.COMPANY_ID = @CompanyId
//                                AND Activity.USER_ID = @UserId
//                                AND Activity.ACTIVITY_DATETIME BETWEEN @FromTime AND @ToTime ORDER BY [ACTIVITY_DATETIME] DESC";

//            }
//            else if (!String.IsNullOrEmpty(subAdminId) && !(processType == SUBADMIN_ACTIVITY_LOG.ALL))
//            {
//                strQuery = @"SELECT [USER_ROLE],[USER_ID],[COMPANY_ID],[PROCESS_TYPE]
//                                ,[PROCESS_ID],[ACTIVITY_DATETIME],[PARA1],[PARA2],[PARA3],[PARA4],[PARA5],SubAdmin.USER_NAME,SubAdmin.FULL_NAME  AS SA_FULL_NAME
//                                FROM [TBL_ACTIVITY_LOG] AS Activity
//                                INNER JOIN TBL_SUB_ADMIN SubAdmin
//                                ON Activity.USER_ID = SubAdmin.SUBADMIN_ID
//                                WHERE Activity.COMPANY_ID = @CompanyId
//                                AND Activity.USER_ID = @UserId
//                                AND Activity.PROCESS_TYPE = @ProcessType
//                                AND Activity.ACTIVITY_DATETIME BETWEEN @FromTime AND @ToTime ORDER BY [ACTIVITY_DATETIME] DESC";
//            }
//            else if (String.IsNullOrEmpty(subAdminId) && processType == SUBADMIN_ACTIVITY_LOG.ALL)
//            {
//                strQuery = @"SELECT [USER_ROLE],[USER_ID],[COMPANY_ID],[PROCESS_TYPE]
//                                ,[PROCESS_ID],[ACTIVITY_DATETIME],[PARA1],[PARA2],[PARA3],[PARA4],[PARA5],SubAdmin.USER_NAME,SubAdmin.FULL_NAME AS SA_FULL_NAME
//                                FROM [TBL_ACTIVITY_LOG] AS Activity
//                                INNER JOIN TBL_SUB_ADMIN SubAdmin
//                                ON Activity.USER_ID = SubAdmin.SUBADMIN_ID
//                                WHERE Activity.COMPANY_ID = @CompanyId
//                                AND Activity.ACTIVITY_DATETIME BETWEEN @FromTime AND @ToTime ORDER BY [ACTIVITY_DATETIME] DESC";
//            }


//            SqlCommand cmd = new SqlCommand(strQuery);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@CompanyId", companyId);
//            if (!String.IsNullOrEmpty(subAdminId))
//            {
//                cmd.Parameters.AddWithValue("@UserId", subAdminId);
//            }
//            if (!(processType == SUBADMIN_ACTIVITY_LOG.ALL))
//            {
//                cmd.Parameters.AddWithValue("@ProcessType", processType);
//            }
//            cmd.Parameters.AddWithValue("@FromTime", fromTime);
//            cmd.Parameters.AddWithValue("@ToTime", toTime);

//            return MSSqlClient.SelectDataFromSQlCommand(cmd);
//        }

//        public static DataSet getSubAdminActivityLogModified(string subAdminId, string companyId, SUBADMIN_ACTIVITY_LOG processType, long fromTime, long toTime)
//        {
//            string strQuery  = @"SELECT ActivityLog.*, UserDtl.USER_NAME,WrkFlwDtl.WF_NAME,UsrGrp.GROUP_NAME,
//            SubAdmin.USER_NAME AS SUBADMIN_NAME,case WHEN ActivityLog.user_id='-1' THEN 'Auto'
//            WHEN SubAdmin.FULL_NAME=null THEN 'sub-admin' else SubAdmin.FULL_NAME end AS SA_FULL_NAME
//            FROM [TBL_ACTIVITY_LOG] AS ActivityLog LEFT OUTER  JOIN TBL_SUB_ADMIN SubAdmin
//            ON ActivityLog.USER_ID = SubAdmin.SUBADMIN_ID LEFT OUTER JOIN TBL_USER_DETAIL AS  UserDtl ON ActivityLog.PARA1 = UserDtl.USER_ID
//            LEFT OUTER JOIN TBL_WORKFLOW_DETAIL AS WrkFlwDtl ON ActivityLog.PARA5 = WrkFlwDtl.WF_ID
//            LEFT OUTER JOIN TBL_USER_GROUP AS UsrGrp ON ActivityLog.PARA4 = UsrGrp.GROUP_ID WHERE ActivityLog.COMPANY_ID = @CompanyId 
//            AND ActivityLog.ACTIVITY_DATETIME BETWEEN @FromTime AND @ToTime ";

//            if (!String.IsNullOrEmpty(subAdminId) && (processType == SUBADMIN_ACTIVITY_LOG.ALL))
//            {
//                strQuery += @" AND ActivityLog.USER_ID = @SubAdminId
//                            AND ActivityLog.PROCESS_TYPE = @ProcessType";
//            }
//            else if (!String.IsNullOrEmpty(subAdminId) && processType == SUBADMIN_ACTIVITY_LOG.ALL)
//            {
//                strQuery += @" AND ActivityLog.USER_ID = @SubAdminId ";
//            }
//            else if (String.IsNullOrEmpty(subAdminId) && !(processType == SUBADMIN_ACTIVITY_LOG.ALL))
//            {
//                strQuery += @" AND ActivityLog.PROCESS_TYPE = @ProcessType ";
//            }

//            strQuery += @" ORDER BY [ACTIVITY_DATETIME] DESC";
//            SqlCommand cmd = new SqlCommand(strQuery);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@CompanyId", companyId);
//            if (!String.IsNullOrEmpty(subAdminId))
//            {
//                cmd.Parameters.AddWithValue("@SubAdminId", subAdminId);
//            }
//            if (!(processType == SUBADMIN_ACTIVITY_LOG.ALL ))
//            {
//                cmd.Parameters.AddWithValue("@ProcessType", processType);
//            }
           
//            cmd.Parameters.AddWithValue("@FromTime", fromTime);
//            cmd.Parameters.AddWithValue("@ToTime", toTime);

//            return modifiedDataset(MSSqlClient.SelectDataFromSQlCommand(cmd), companyId);
//        }

//        static DataSet modifiedDataset(DataSet rawSubAdminActivityLog, string companyId)
//        {
//           StringBuilder sbLog=new StringBuilder();
//            TimeZoneInfo tzi = CompanyTimezone.getTimezoneInfo(companyId);
//            DataTable dtblModifiedActivityLog = new DataTable();
//            dtblModifiedActivityLog.Columns.Add("LOG_ID", typeof(string));
//            dtblModifiedActivityLog.Columns.Add("LOG", typeof(string));
//            dtblModifiedActivityLog.Columns.Add("DATE", typeof(string));
//            if (rawSubAdminActivityLog != null)
//            {
//                DataTable dtblSubAdminLog = rawSubAdminActivityLog.Tables[0];
//                string strSubAdminUsername ="", strSubAdminFullName="";
//                foreach (DataRow row in dtblSubAdminLog.Rows)
//                {
//                    sbLog = new StringBuilder();
//                    string strUserName = Convert.ToString(row["USER_NAME"]);
//                    strSubAdminFullName = Convert.ToString(row["SA_FULL_NAME"]);
//                    strSubAdminUsername = Convert.ToString(row["SUBADMIN_NAME"]);
                    
//                    SUBADMIN_ACTIVITY_LOG eSubAdminLogType = (SUBADMIN_ACTIVITY_LOG)Enum.Parse(typeof(SUBADMIN_ACTIVITY_LOG)
//                        , Convert.ToInt32(row["PROCESS_TYPE"]).ToString());
//                    switch (eSubAdminLogType)
//                    {
//                        case SUBADMIN_ACTIVITY_LOG.ALL:
//                            break;
//                        case SUBADMIN_ACTIVITY_LOG.MOBILE_USER_ADD:
//                            sbLog.Append(strSubAdminFullName).Append(SPACE_SEPARATOR)
//                                .Append("added a user").Append(SPACE_SEPARATOR).Append(strUserName);
//                            break;
//                        case SUBADMIN_ACTIVITY_LOG.MOBILE_USER_UPDATE:
//                            sbLog.Append(strSubAdminFullName).Append(SPACE_SEPARATOR)
//                                .Append("updated").Append(SPACE_SEPARATOR)
//                                .Append(strUserName).Append(SPACE_SEPARATOR).Append("detail");
//                            break;
//                        case SUBADMIN_ACTIVITY_LOG.MOBILE_USER_DELETE:
//                             sbLog.Append(strSubAdminFullName).Append(SPACE_SEPARATOR)
//                                .Append("deleted user :").Append(SPACE_SEPARATOR).Append(row["PARA5"]);
//                            break;
//                        case SUBADMIN_ACTIVITY_LOG.MOBILE_DEVICE_APPROVE:
//                            sbLog.Append(strSubAdminFullName).Append(SPACE_SEPARATOR)
//                                .Append("approved device DeviceId:").Append(SPACE_SEPARATOR).Append(row["PARA2"])
//                                .Append(" / Device Type: ").Append(row["PARA3"]).Append(" of user : ").Append(strUserName);
//                            break;
//                        case SUBADMIN_ACTIVITY_LOG.MOBILE_DEVICE_DELETE:
//                            sbLog.Append(strSubAdminFullName).Append(SPACE_SEPARATOR)
//                                .Append("deleted device DeviceId:").Append(SPACE_SEPARATOR).Append(row["PARA2"])
//                                .Append(" / Device Type: ").Append(row["PARA3"]).Append(" of user : ").Append(strUserName);
//                            break;
//                        case SUBADMIN_ACTIVITY_LOG.MOBILE_DEVICE_DENY:
//                            sbLog.Append(strSubAdminFullName).Append(SPACE_SEPARATOR)
//                                .Append("denied device DeviceId:").Append(SPACE_SEPARATOR).Append(row["PARA2"])
//                                .Append(" / Device Type: ").Append(row["PARA3"]).Append(" of user : ").Append(strUserName);
//                            break;
//                        case SUBADMIN_ACTIVITY_LOG.MOBILE_DEVICE_DELETE_APPROVE:
//                            break;
//                        case SUBADMIN_ACTIVITY_LOG.ADD_GROUP:
//                            sbLog.Append(strSubAdminFullName).Append(SPACE_SEPARATOR)
//                                .Append("added a group :").Append(SPACE_SEPARATOR)
//                                .Append(String.IsNullOrEmpty(Convert.ToString(row["GROUP_NAME"])) ? row["PARA1"]:row["GROUP_NAME"] );
//                            break;
//                        case SUBADMIN_ACTIVITY_LOG.UPDATE_GROUP:
//                            sbLog.Append(strSubAdminFullName).Append(SPACE_SEPARATOR)
//                                .Append("updated the group :").Append(SPACE_SEPARATOR)
//                                .Append(String.IsNullOrEmpty(Convert.ToString(row["GROUP_NAME"]))?row["PARA1"]:row["GROUP_NAME"]);
//                            break;
//                        case SUBADMIN_ACTIVITY_LOG.ADD_MPLUGIN:
//                            break;
//                        case SUBADMIN_ACTIVITY_LOG.UPDATE_MPLUGIN:
//                            break;
//                        case SUBADMIN_ACTIVITY_LOG.PUBLISH_APP:
//                            sbLog.Append(strSubAdminFullName).Append(SPACE_SEPARATOR)
//                                .Append("published an app :").Append(SPACE_SEPARATOR).Append(Convert.ToString(row["WF_NAME"]));
//                            break;
//                        case SUBADMIN_ACTIVITY_LOG.DELETE_GROUP:
//                            sbLog.Append(strSubAdminFullName).Append(SPACE_SEPARATOR)
//                                .Append("deleted group :").Append(SPACE_SEPARATOR).Append(Convert.ToString(row["PARA1"]));
//                            break;
                   
//                    }
                    
//                    DataRow newRow = dtblModifiedActivityLog.NewRow();
//                    newRow["LOG_ID"] = Convert.ToString(row["PROCESS_ID"]);
//                    newRow["LOG"] = sbLog.ToString();
//                    newRow["DATE"] = Utilities.getCompanyLocalFormattedDateTime(tzi, Convert.ToInt64(row["ACTIVITY_DATETIME"]));
//                    dtblModifiedActivityLog.Rows.Add(newRow);
//                }
//                DataSet dsModifiedActivityLog = new DataSet();
//                dsModifiedActivityLog.Tables.Add(dtblModifiedActivityLog);
//                return dsModifiedActivityLog;
//            }
//            else
//            {
//                return null;
//            }
//        }

//    }
//}
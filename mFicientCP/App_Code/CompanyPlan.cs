﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace mFicientCP
{
    public class CompanyPlan
    {

        enum Month
        {
            January = 1,
            February = 2,
            March = 3,
            April = 4,
            May = 5,
            June = 6,
            July = 7,
            August = 8,
            September = 9,
            October = 10,
            November = 11,
            December = 12
        }
        string _companyId, _planCode, _maxWorkFlow, _chargeType, _validity;
        string _feature3, _feature4, _feature5, _feature6, _feature7, _feature8, _feature9, _feature10;
        string _nextMonthPlan, _planName, _nextMonthPlanName;
        long _purchaseDate, _nextMonthPlanLastUpdated;
        decimal _balanceAmount = decimal.MinValue, _userChargePerMonth = decimal.MinValue;
        int _pushMessagePerDay, _pushMessagePerMonth, _year, _maxUsers;
        bool _isTrial;

        
        /// <summary>
        /// Pass the data table of the current plan detail of the company(complete database row since all the properties are set here based on the table)
        /// </summary>
        /// <param name="companyCurrPlanDetail"></param>
        public CompanyPlan(DataTable companyCurrPlanDetail,string planName,bool isTrial)
        {
            this.CompanyId = Convert.ToString(companyCurrPlanDetail.Rows[0]["COMPANY_ID"]);
            this.PlanCode = Convert.ToString(companyCurrPlanDetail.Rows[0]["PLAN_CODE"]);
            this.MaxUsers = Convert.ToInt32(companyCurrPlanDetail.Rows[0]["MAX_USER"]);
            this.MaxWorkFlow = Convert.ToString(companyCurrPlanDetail.Rows[0]["MAX_WORKFLOW"]);
            this.UserChargePerMonth = Convert.ToDecimal(companyCurrPlanDetail.Rows[0]["USER_CHARGE_PM"]);
            this.ChargeType = Convert.ToString(companyCurrPlanDetail.Rows[0]["CHARGE_TYPE"]);
            this.Validity = Convert.ToString(companyCurrPlanDetail.Rows[0]["VALIDITY"]);
            this.PurchaseDate = Convert.ToInt64(companyCurrPlanDetail.Rows[0]["PURCHASE_DATE"]);
            this.PushMessagePerDay = Convert.ToInt32(companyCurrPlanDetail.Rows[0]["PUSHMESSAGE_PERDAY"]);
            this.PushMessagePerMonth = Convert.ToInt32(companyCurrPlanDetail.Rows[0]["PUSHMESSAGE_PERMONTH"]);
            this.Feature3 = Convert.ToString(companyCurrPlanDetail.Rows[0]["FEATURE3"]);
            this.Feature4 = Convert.ToString(companyCurrPlanDetail.Rows[0]["FEATURE4"]);
            this.Feature5 = Convert.ToString(companyCurrPlanDetail.Rows[0]["FEATURE5"]);
            this.Feature6 = Convert.ToString(companyCurrPlanDetail.Rows[0]["FEATURE6"]);
            this.Feature7 = Convert.ToString(companyCurrPlanDetail.Rows[0]["FEATURE7"]);
            this.Feature8 = Convert.ToString(companyCurrPlanDetail.Rows[0]["FEATURE8"]);
            this.Feature9 = Convert.ToString(companyCurrPlanDetail.Rows[0]["FEATURE9"]);
            this.Feature10 = Convert.ToString(companyCurrPlanDetail.Rows[0]["FEATURE10"]);
            this.BalanceAmount = Convert.ToDecimal(companyCurrPlanDetail.Rows[0]["BALANCE_AMOUNT"]);
            this.NextMonthPlan = Convert.ToString(companyCurrPlanDetail.Rows[0]["NEXT_MONTH_PLAN"]);
            this.NextMonthPlanLastUpdated = Convert.ToInt64(companyCurrPlanDetail.Rows[0]["NEXT_MONTH_PLAN_LAST_UPDATED"]);
            this.PlanName = planName;
            this.IsTrial = isTrial;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="noOfUsers"></param>
        /// <returns></returns>
        ///<exception cref="System.Exception">Thrown when the public property used is not set...and the plan selected is trial</exception>
        ///<exception cref="System.ArgumentException"></exception>
        public bool isBalanceAmountValidForAddingUsers(int noOfUsers)
        {
            if (this.BalanceAmount == decimal.MinValue || this.UserChargePerMonth == decimal.MinValue ||  this.IsTrial== true)
            {
                throw new Exception();
            }
            if (noOfUsers <= 0)
            {
                throw new ArgumentException("No of Users cannot be less than or equal to zero");
            }
            if (Convert.ToDecimal(this.getUserChargeForRemainingMonth(noOfUsers)) < this.BalanceAmount)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="noOfUsers"></param>
        /// <param name="balanceAfterAddition"></param>
        /// <returns></returns>
        ///<exception cref="System.Exception">Thrown when the public property used is not set...and the plan selected is trial</exception>
        ///<exception cref="System.ArgumentException"></exception>
        public bool isBalanceAmountValidForAddingUsers(int noOfUsers, out decimal balanceAfterAddition)
        {
            balanceAfterAddition = 0;
            if (this.BalanceAmount == decimal.MinValue || this.UserChargePerMonth == decimal.MinValue || String.IsNullOrEmpty(this.ChargeType) || this.IsTrial == true)
            {
                throw new Exception();
            }
            if (noOfUsers <= 0)
            {
                throw new ArgumentException("No of Users cannot be less than or equal to zero");
            }

            decimal decUserChargeForRemainingMonth = Convert.ToDecimal(this.getUserChargeForRemainingMonth(noOfUsers));
            if (decUserChargeForRemainingMonth < this.BalanceAmount)
            {
                balanceAfterAddition = Convert.ToDecimal(Utilities.roundOfAmountByCurrencyType(Convert.ToDouble(this.BalanceAmount - decUserChargeForRemainingMonth), (CURRENCY_TYPE)Enum.Parse(typeof(CURRENCY_TYPE), this.ChargeType)));
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ///<exception cref="System.Exception">Thrown when the public property used is not set...and the plan selected is trial</exception>
        ///<exception cref="System.ArgumentException"></exception>
        public Double getUserChargeForRemainingMonth(int noOfUsersAdded, out double chargePerDay)
        {
            if (this.UserChargePerMonth == decimal.MinValue || this.IsTrial == true)
            {
                throw new Exception();
            }
            if (noOfUsersAdded <= 0)
            {
                throw new ArgumentException("No of Users cannot be less than or equal to zero");
            }
            DateTime dtNow = DateTime.UtcNow;
            this.Year = dtNow.Year;
            Month month = (Month)Enum.Parse(typeof(Month), Convert.ToString(dtNow.Month));
            int iNoOfDays = getNoOfDaysInMonth(month);
            DateTime dtLastDayOfMonth = new DateTime(this.Year, dtNow.Month, iNoOfDays, 23, 59, 59).ToUniversalTime();
            TimeSpan tsRemaining = dtLastDayOfMonth - dtNow;
            chargePerDay = Convert.ToDouble(this.UserChargePerMonth / iNoOfDays);
            return Utilities.roundOfAmountByCurrencyType((chargePerDay * tsRemaining.TotalDays * (noOfUsersAdded)), (CURRENCY_TYPE)Enum.Parse(typeof(CURRENCY_TYPE), this.ChargeType));
            //return (chargePerDay * (Convert.ToInt32(this.MaxUsers) + noOfUsersAdded));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="noOfUsersAdded"></param>
        /// <returns></returns>
        ///<exception cref="System.Exception">Thrown when the public property used is not set...and the plan selected is trial</exception>
        ///<exception cref="System.ArgumentException"></exception>
        public Double getUserChargeForRemainingMonth(int noOfUsersAdded)
        {
            if (this.UserChargePerMonth == decimal.MinValue || this.IsTrial == true)
            {
                throw new Exception();
            }
            if (noOfUsersAdded <= 0)
            {
                throw new ArgumentException("No of Users cannot be less than or equal to zero");
            }
            DateTime dtNow = DateTime.UtcNow;
            this.Year = dtNow.Year;
            Month month = (Month)Enum.Parse(typeof(Month), Convert.ToString(dtNow.Month));
            int iNoOfDays = getNoOfDaysInMonth(month);
            DateTime dtLastDayOfMonth = new DateTime(this.Year, dtNow.Month, iNoOfDays, 23, 59, 59).ToUniversalTime();
            TimeSpan tsRemaining = dtLastDayOfMonth - dtNow;
            double dblChargePerDay = Convert.ToDouble(this.UserChargePerMonth / iNoOfDays);
            return Utilities.roundOfAmountByCurrencyType((dblChargePerDay * tsRemaining.TotalDays * (noOfUsersAdded)), (CURRENCY_TYPE)Enum.Parse(typeof(CURRENCY_TYPE), this.ChargeType));
            //return (dblChargePerDay*tsRemaining.TotalDays * (noOfUsersAdded));
            //return (dblChargePerDay * (Convert.ToInt32(this.MaxUsers) + noOfUsersAdded));
        }
        int getNoOfDaysInMonth(Month month)
        {
            int iNoOfDays = 0;
            switch (month)
            {
                case Month.January:
                    iNoOfDays = 31;
                    break;
                case Month.February:
                    if (System.DateTime.IsLeapYear(this.Year))
                    {
                        iNoOfDays = 29;
                    }
                    else
                    {
                        iNoOfDays = 28;
                    }
                    break;
                case Month.March:
                    iNoOfDays = 31;
                    break;
                case Month.April:
                    iNoOfDays = 30;
                    break;
                case Month.May:
                    iNoOfDays = 31;
                    break;
                case Month.June:
                    iNoOfDays = 30;
                    break;
                case Month.July:
                    iNoOfDays = 31;
                    break;
                case Month.August:
                    iNoOfDays = 31;
                    break;
                case Month.September:
                    iNoOfDays = 30;
                    break;
                case Month.October:
                    iNoOfDays = 31;
                    break;
                case Month.November:
                    iNoOfDays = 30;
                    break;
                case Month.December:
                    iNoOfDays = 31;
                    break;
            }
            return iNoOfDays;
        }
        public DateTime validTill()
        {
            DateTime dtNow = DateTime.UtcNow;
            this.Year = dtNow.Year;
            Month month = (Month)Enum.Parse(typeof(Month), Convert.ToString(dtNow.Month));
            int iNoOfDays = getNoOfDaysInMonth(month);
            DateTime dtLastDayOfMonth = new DateTime(this.Year, dtNow.Month, iNoOfDays, 23, 59, 59).ToUniversalTime();
            return dtLastDayOfMonth;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="noOfUsers"></param>
        /// <returns></returns>
        /// ///<exception cref="System.Exception">Thrown when there is some internal error</exception>
        public bool isNoOfUsersValidForDeletion(int noOfUsers)
        {
            GetPlanList objPlanList = new GetPlanList(this.PlanCode, this.ChargeType, Convert.ToInt32(this.Validity),false);
            objPlanList.Process();
            if (objPlanList.MinNoOfUsers == 0)//this will be an error
            {
                throw new Exception();
            }
            if (objPlanList.MinNoOfUsers > (this.MaxUsers - noOfUsers))
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        public bool isAccountBalanceValidForNextMonth()
        {
            if (this.UserChargePerMonth == decimal.MinValue || this.MaxUsers == 0 || this.IsTrial == true)
            {
                throw new Exception();
            }
            decimal decAmountRequiredForNextMnth = 
                Convert.ToDecimal(
                Utilities.roundOfAmountByCurrencyType(
                Convert.ToDouble(this.UserChargePerMonth * this.MaxUsers),
                (CURRENCY_TYPE)Enum.Parse(typeof(CURRENCY_TYPE),this.ChargeType)
                )
                );
            if (this.BalanceAmount < decAmountRequiredForNextMnth)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        #region Public Properties
        public string Validity
        {
            get { return _validity; }
           private set { _validity = value; }
        }

        public string ChargeType
        {
            get { return _chargeType; }
            private set { _chargeType = value; }
        }

        public decimal UserChargePerMonth
        {
            get { return _userChargePerMonth; }
            private set { _userChargePerMonth = value; }
        }

        public int MaxUsers
        {
            get { return _maxUsers; }
            private set { _maxUsers = value; }
        }

        public string MaxWorkFlow
        {
            get { return _maxWorkFlow; }
            private set { _maxWorkFlow = value; }
        }

        public string PlanCode
        {
            get { return _planCode; }
            private set { _planCode = value; }
        }

        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }


        public string Feature10
        {
            get { return _feature10; }
            private set { _feature10 = value; }
        }

        public string Feature9
        {
            get { return _feature9; }
            private set { _feature9 = value; }
        }

        public string Feature8
        {
            get { return _feature8; }
            private set { _feature8 = value; }
        }

        public string Feature7
        {
            get { return _feature7; }
            private set { _feature7 = value; }
        }

        public string Feature6
        {
            get { return _feature6; }
            private set { _feature6 = value; }
        }

        public string Feature5
        {
            get { return _feature5; }
            private set { _feature5 = value; }
        }

        public string Feature4
        {
            get { return _feature4; }
            private set { _feature4 = value; }
        }

        public string Feature3
        {
            get { return _feature3; }
            private set { _feature3 = value; }
        }


        public string NextMonthPlan
        {
            get { return _nextMonthPlan; }
            private set { _nextMonthPlan = value; }
        }


        public long NextMonthPlanLastUpdated
        {
            get { return _nextMonthPlanLastUpdated; }
            private set { _nextMonthPlanLastUpdated = value; }
        }

        public long PurchaseDate
        {
            get { return _purchaseDate; }
            private set { _purchaseDate = value; }
        }


        public int PushMessagePerMonth
        {
            get { return _pushMessagePerMonth; }
            private set { _pushMessagePerMonth = value; }
        }

        public int PushMessagePerDay
        {
            get { return _pushMessagePerDay; }
            private set { _pushMessagePerDay = value; }
        }


        public decimal BalanceAmount
        {
            get { return _balanceAmount; }
            private set { _balanceAmount = value; }
        }

        public int Year
        {
            get { return _year; }
            private set { _year = value; }
        }
        public string PlanName
        {
            get { return _planName; }
            private set { _planName = value; }
        }

        public string NextMonthPlanName
        {
            get { return _nextMonthPlanName; }
            set { _nextMonthPlanName = value; }
        }
        public bool IsTrial
        {
            get { return _isTrial; }
            private set { _isTrial = value; }
        }
        #endregion
    }
}
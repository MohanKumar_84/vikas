﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class UpdateWsConnection
    {
       public UpdateWsConnection()
        { 

        }

       public UpdateWsConnection(string _SubAdminId, string _ConnectionId, string _Url, string _UserName, string _Password, string _ResponseType, string _WsdlUrl, string _WsdContent, Boolean _ChangeWsdlContent, string _MpluginAgent, string _CompanyID)
        {
            this.SubAdminId = _SubAdminId;
            this.Url = _Url;
            this.UserName = _UserName;
            this.Password = _Password;
            this.WsdlUrl = _WsdlUrl;
            this.WsdContent = _WsdContent;
            this.ConnectionId = _ConnectionId;
            this.ResponseType = _ResponseType;
            this.ChangeWsdlContent = _ChangeWsdlContent;
            this.MpluginAgent = _MpluginAgent;
            this.CompanyId = _CompanyID;
        }
       

       public UpdateWsConnection(string _SubAdminId, string _ConnectionId, string _Url, string _UserName, string _Password, string _Autenticationtype, string _ResponseType, string _WsdlUrl, string _WsdContent, Boolean _ChangeWsdlContent, string _MpluginAgent, string _CompanyID,  string _Credentiailproperty)
       {
           this.SubAdminId = _SubAdminId;
           this.Url = _Url;
           this.UserName = _UserName;
           this.Password = _Password;
           this.WsdlUrl = _WsdlUrl;
           this.WsdContent = _WsdContent;
           this.ConnectionId = _ConnectionId;
           this.ResponseType = _ResponseType;
           this.ChangeWsdlContent = _ChangeWsdlContent;
           this.MpluginAgent = _MpluginAgent;
           this.CompanyId = _CompanyID;
           this.HttpAutenticationtype = _Autenticationtype;
           this.CredentialProperty = _Credentiailproperty;
       }


        public void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                if (ChangeWsdlContent)
                {
                    query = @"UPDATE TBL_WEBSERVICE_CONNECTION SET 
	                                WEBSERVICE_URL=@WEBSERVICE_URL,
	                                USER_NAME=@USER_NAME,
	                                PASSWORD=@PASSWORD,
	                                RESPONSE_TYPE=@RESPONSE_TYPE,
	                                WSDL_URL=@WSDL_URL,
                                	WSDL_CONTENT=@WSDL_CONTENT,
                                    MPLUGIN_AGENT=@MPLUGIN_AGENT
                                WHERE WS_CONNECTOR_ID=@WS_CONNECTOR_ID AND SUBADMIN_ID=@SUBADMIN_ID AND COMPANY_ID=@COMPANY_ID;";
                }
                else
                {
                    query = @"UPDATE TBL_WEBSERVICE_CONNECTION SET 
	                                WEBSERVICE_URL=@WEBSERVICE_URL,
	                                USER_NAME=@USER_NAME,
	                                PASSWORD=@PASSWORD,
                                    MPLUGIN_AGENT=@MPLUGIN_AGENT
                                WHERE WS_CONNECTOR_ID=@WS_CONNECTOR_ID AND SUBADMIN_ID=@SUBADMIN_ID AND COMPANY_ID=@COMPANY_ID;";
                }

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WS_CONNECTOR_ID", this.ConnectionId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@WEBSERVICE_URL", this.Url);
                objSqlCommand.Parameters.AddWithValue("@USER_NAME", this.UserName);
                objSqlCommand.Parameters.AddWithValue("@PASSWORD", this.Password);
                objSqlCommand.Parameters.AddWithValue("@RESPONSE_TYPE", this.ResponseType);
                objSqlCommand.Parameters.AddWithValue("@WSDL_URL", this.WsdlUrl);
                objSqlCommand.Parameters.AddWithValue("@WSDL_CONTENT", this.WsdContent);
                objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgent);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }


         public void Process2()
          {
            try
            {
                this.StatusCode = -1000;
                string query;
                if (ChangeWsdlContent)
                {
                    query = @"UPDATE TBL_WEBSERVICE_CONNECTION SET 
	                                WEBSERVICE_URL=@WEBSERVICE_URL,
	                                RESPONSE_TYPE=@RESPONSE_TYPE,
	                                WSDL_URL=@WSDL_URL,
                                   SUBADMIN_ID=@SUBADMIN_ID,
                                	WSDL_CONTENT=@WSDL_CONTENT,
                                    MPLUGIN_AGENT=@MPLUGIN_AGENT,
                                    AUTHENTICATION_TYPE=@AUTHENTICATION_TYPE,
                                    CREDENTIAL_PROPERTY=@CREDENTIAL_PROPERTY,
                                   UPDATED_ON=@UPDATED_ON
                                WHERE WS_CONNECTOR_ID=@WS_CONNECTOR_ID  AND COMPANY_ID=@COMPANY_ID;";
                }
                else
                {

                    query = @"UPDATE TBL_WEBSERVICE_CONNECTION SET 
	                                WEBSERVICE_URL=@WEBSERVICE_URL,
                               AUTHENTICATION_TYPE=@AUTHENTICATION_TYPE,
                                   SUBADMIN_ID=@SUBADMIN_ID,
                                    MPLUGIN_AGENT=@MPLUGIN_AGENT,
                                    CREDENTIAL_PROPERTY=@CREDENTIAL_PROPERTY
                                WHERE WS_CONNECTOR_ID=@WS_CONNECTOR_ID  AND COMPANY_ID=@COMPANY_ID;";
                }

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WS_CONNECTOR_ID", this.ConnectionId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@WEBSERVICE_URL", this.Url);
                objSqlCommand.Parameters.AddWithValue("@RESPONSE_TYPE", this.ResponseType);
                objSqlCommand.Parameters.AddWithValue("@WSDL_URL", this.WsdlUrl);
                objSqlCommand.Parameters.AddWithValue("@WSDL_CONTENT", this.WsdContent);
                objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgent);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@AUTHENTICATION_TYPE", this.HttpAutenticationtype);
                objSqlCommand.Parameters.AddWithValue("@CREDENTIAL_PROPERTY", this.CredentialProperty);
                objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", DateTime.UtcNow.Ticks);
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }

        public string SubAdminId { get; set; }
        public string Password { get; set; }
        public string ConnectionId { get; set; }
        public int StatusCode { get; set; }
        public string Url { get; set; }
        public string UserName { get; set; }
        public string ResponseType { get; set; }
        public string WsdlUrl { get; set; }
        public string WsdContent { get; set; }
        public bool ChangeWsdlContent { get; set; }
        public bool UseMplugin { get; set; }
        public string CompanyId { get; set; }
        public string MpluginAgent { get; set; }
        public string HttpAutenticationtype { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        //public long UpdatedOn { get; set; }
        public string CredentialProperty { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCP
{
    public class GetRegisteredDeviceDetails
    {
       //public enum SORT_TYPE
       // {
       //     ASC,
       //     DESC
       // }
       //public enum ORDER_BY
       // {
       //     Name = 1,
       //     OS = 2,
       //     RequestDate = 3,
       // }
        public GetRegisteredDeviceDetails(string companyId, string subAdminId, REGISTERED_DEVICES_ORDER_BY orderBy, SORT_TYPE sortType)
        {
            SubAdminId = subAdminId;
            CompanyId = companyId;
            this.OrderBy = orderBy;
            this.SortType = sortType;
        }
        public GetRegisteredDeviceDetails()
        {
            this.OrderBy = REGISTERED_DEVICES_ORDER_BY.RequestDate;
            this.SortType = SORT_TYPE.ASC;
        }
        public void Process()
        {
            try
            {
                
                StatusCode = -1000;
                
                string strQuery = @" SELECT device.*,usr.USER_ID,usr.USER_NAME,(usr.FIRST_NAME +' '+usr.LAST_NAME)AS FULL_NAME 
                                     FROM TBL_REGISTERED_DEVICE device
                                     INNER JOIN TBL_USER_DETAIL as usr
                                     ON usr.USER_ID = device.USER_ID
                                     WHERE device.COMPANY_ID =@CompanyId " + " " + @"ORDER BY" + " " + getOrderByColumnName() + " " + getSortTypeInString()+" "+";"+
                                    
                                     @" SELECT COUNT(*) AS Total,CurrPlan.MAX_USER
                                                    FROM TBL_REGISTERED_DEVICE AS RegDev
                                                    INNER JOIN TBL_COMPANY_CURRENT_PLAN AS CurrPlan
                                                    ON RegDev.COMPANY_ID = CurrPlan.COMPANY_ID
                                                    WHERE RegDev.COMPANY_ID = @CompanyId
                                                    GROUP BY CurrPlan.MAX_USER ;
                
                                    SELECT COUNT(*) AS Total FROM
                                                    TBL_DEVICE_REGISTRATION_REQUEST  request
                                                    INNER JOIN TBL_USER_DETAIL usr
                                                    ON request.USER_ID = usr.USER_ID
                                                    WHERE request.COMPANY_ID =@CompanyId
                                                    AND STATUS = 0;

                                    SELECT MAX_USER
                                   FROM TBL_COMPANY_CURRENT_PLAN
                                    WHERE COMPANY_ID = @CompanyId;";

                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    ResultTables = objDataSet;
                    StatusCode = 0;
                    StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }
        }

        public void GetAdminRegisteredDevice()
        {
            try
            {
                StatusCode = -1000;
                string strQuery = @"SELECT * FROM TBL_REGISTERED_DEVICE WHERE COMPANY_ID = @COMPANY_ID
                                    AND USER_ID = @USER_ID;";


                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    ResultTables = objDataSet;
                    StatusCode = 0;
                    StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }
        }

        string getOrderByColumnName()
        {
            string strOrderByName = "";
            switch (this.OrderBy)
            {
                case REGISTERED_DEVICES_ORDER_BY.Name:
                    strOrderByName = "FULL_NAME";
                    break;
                case REGISTERED_DEVICES_ORDER_BY.OS:
                    strOrderByName = "DEVICE_TYPE";
                    break;
                case REGISTERED_DEVICES_ORDER_BY.RequestDate:
                    strOrderByName = "REGISTRATION_DATE";
                    break;
            }
            return strOrderByName;
        }
        string getSortTypeInString()
        {
            string strSortType="";
            switch (this.SortType)
            { 
                case SORT_TYPE.ASC:
                    strSortType = "ASC";
                    break;
                case SORT_TYPE.DESC:
                    strSortType = "DESC";
                    break;
            }
            return strSortType;
        }
        public DataSet ResultTables
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string AdminId
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public REGISTERED_DEVICES_ORDER_BY OrderBy
        {
            get;
            set;
        }
        public SORT_TYPE SortType
        {
            get;
            set;
        }
    }
}
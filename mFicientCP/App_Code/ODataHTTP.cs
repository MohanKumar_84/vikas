﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Threading;

namespace mFicientCP
{
    public enum ResponseType
    {
        ATOM_XML = 0,
        JSON = 1,
        XML = 2
    }

    public class ODataHTTP
    {
        internal String strURL = "";
        private String UseURL = "";
        internal int Retry = 3, Timeout = 10000;
        internal int RetryDelay = 50;
        internal String ResponseText = "", ValidResponse = "";
        internal Boolean blnRequestSuccess = false;
        internal String ErrorMessage = "";
        private Byte[] ContentData;
        private String strRequestMethod = "GET", strUsername = "", strPassword = "";
        private Boolean blnWaitForResponse = true;
        private string postString = string.Empty;
        private ResponseType requestFormat;
        private FunctionType functionType;
        private ODataRequestType oDataRequestType;
        private string maxDataServiceVersion = string.Empty;

        internal ODataHTTP(String URL)
        {
            strURL = URL;
            UseURL = strURL;
        }

        internal String URL
        {
            get
            {
                URL = strURL;
                return URL;
            }
            set
            {
                strURL = value;
                UseURL = strURL;
            }
        }

        internal string MaxDataServiceVersion
        {
            get
            {
                return maxDataServiceVersion;
            }
            set
            {
                maxDataServiceVersion = value;
            }
        }

        internal string PostString
        {
            get
            {
                return postString;
            }
            set
            {
                postString = value;
            }
        }

        internal ResponseType RequestFormat
        {
            get
            {
                return requestFormat;
            }
            set
            {
                requestFormat = value;
            }
        }

        internal FunctionType Functiontype
        {
            get
            {
                return functionType;
            }
            set
            {
                functionType = value;
            }
        }

        internal ODataRequestType ODataRequesttype
        {
            get
            {
                return oDataRequestType;
            }
            set
            {
                oDataRequestType = value;
            }
        }

        internal void SetHttpCredentials(String UID, String PWD)
        {
            strUsername = UID;
            strPassword = PWD;
        }

        internal enum EnumHttpMethod
        {
            HTTP_POST = 1,
            HTTP_GET = 2,
            HTTP_PUT = 3,
            HTTP_PATCH = 4,
            HTTP_MERGE = 5,
            HTTP_DELETE = 6
        };

        internal EnumHttpMethod HttpRequestMethod
        {
            get
            {
                switch (strRequestMethod.Trim().ToUpper())
                {
                    case "POST":
                        HttpRequestMethod = EnumHttpMethod.HTTP_POST;
                        break;
                    case "GET":
                        HttpRequestMethod = EnumHttpMethod.HTTP_GET;
                        break;
                    case "PUT":
                        HttpRequestMethod = EnumHttpMethod.HTTP_PUT;
                        break;
                    case "PATCH":
                        HttpRequestMethod = EnumHttpMethod.HTTP_PATCH;
                        break;
                    case "MERGE":
                        HttpRequestMethod = EnumHttpMethod.HTTP_MERGE;
                        break;
                    case "DELETE":
                        HttpRequestMethod = EnumHttpMethod.HTTP_DELETE;
                        break;
                }
                return HttpRequestMethod;
            }
            set
            {
                switch (value)
                {
                    case EnumHttpMethod.HTTP_GET:
                        strRequestMethod = "GET";
                        break;
                    case EnumHttpMethod.HTTP_POST:
                        strRequestMethod = "POST";
                        break;
                    case EnumHttpMethod.HTTP_PUT:
                        strRequestMethod = "PUT";
                        break;
                    case EnumHttpMethod.HTTP_PATCH:
                        strRequestMethod = "PATCH";
                        break;
                    case EnumHttpMethod.HTTP_MERGE:
                        strRequestMethod = "MERGE";
                        break;
                    case EnumHttpMethod.HTTP_DELETE:
                        strRequestMethod = "DELETE";
                        break;
                }
            }
        }

        public HttpResponseStatus Request()
        {
            HttpStatusCode StatusCode = HttpStatusCode.RequestTimeout;
            blnRequestSuccess = false;
            try
            {
                int i = 0;
                ResponseText = "";
                ErrorMessage = "";
                if (UseURL.Trim().Length <= 0)
                {
                    ErrorMessage = "URL not defined for HTTP request";
                    blnRequestSuccess = false;
                }
                HttpWebRequest myHttpWebRequest = null;
                HttpWebResponse myHttpWebResponse = null;
                StreamReader myHttpWebReader = null;
                String myHttpWebResult = "";
                Stream myHttpRequestStream = null;
                NetworkCredential myHttpCredentials = null;
                for (i = 0; i <= Retry; i++)
                {
                    ErrorMessage = "";
                    ResponseText = "";
                    try
                    {
                        //Creates an HttpWebRequest with the specified URL.
                        myHttpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(UseURL));
                        switch (requestFormat)
                        {
                            case ResponseType.ATOM_XML:
                                myHttpWebRequest.Accept = "application/atom+xml";
                                break;
                            case ResponseType.JSON:
                                myHttpWebRequest.Accept = "application/json;odata=verbose";
                                break;
                            case ResponseType.XML:
                                myHttpWebRequest.Accept = "application/xml";
                                break;
                        }
                        myHttpWebRequest.Timeout = Timeout;
                        myHttpWebRequest.AllowAutoRedirect = true;
                        myHttpWebRequest.MaximumAutomaticRedirections = 10;
                        myHttpWebRequest.KeepAlive = true;
                        myHttpWebRequest.Method = strRequestMethod;
                        if (!string.IsNullOrEmpty(maxDataServiceVersion)) myHttpWebRequest.Headers.Add("MaxDataServiceVersion", maxDataServiceVersion.Trim());

                        if (strUsername.Trim().Length > 0 || strPassword.Trim().Length > 0)
                        {
                            myHttpCredentials = new NetworkCredential(strUsername.Trim(), strPassword.Trim());
                            myHttpWebRequest.Credentials = myHttpCredentials;
                            myHttpWebRequest.PreAuthenticate = true;
                        }

                        if (strRequestMethod != "GET" && strRequestMethod != "DELETE" && oDataRequestType == ODataRequestType.EntityType)
                        {
                            ContentData = System.Text.Encoding.UTF8.GetBytes(postString);
                            myHttpWebRequest.ContentLength = ContentData.Length;
                            myHttpRequestStream = myHttpWebRequest.GetRequestStream();
                            myHttpRequestStream.Write(ContentData, 0, ContentData.Length);
                            myHttpRequestStream.Close();
                        }
                        else if (strRequestMethod != "GET" && strRequestMethod != "DELETE" && oDataRequestType == ODataRequestType.Function)
                        {
                            if (functionType == FunctionType.ACTION)
                            {
                                ContentData = System.Text.Encoding.UTF8.GetBytes(postString);
                                myHttpWebRequest.ContentLength = ContentData.Length;
                                myHttpRequestStream = myHttpWebRequest.GetRequestStream();
                                myHttpRequestStream.Write(ContentData, 0, ContentData.Length);
                                myHttpRequestStream.Close();
                            }
                        }
                        try
                        {
                            myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                        }
                        catch (WebException wex)
                        {
                            StatusCode = ((System.Net.HttpWebResponse)wex.Response).StatusCode;
                            ErrorMessage = ((System.Net.HttpWebResponse)wex.Response).StatusDescription;
                        }
                        catch (Exception ex)
                        {
                            StatusCode = HttpStatusCode.BadRequest;
                            ErrorMessage = ex.Message;
                        }
                        finally
                        {
                        }
                        // wait for Response 
                        if (blnWaitForResponse)
                        {
                            if (StatusCode == HttpStatusCode.OK)
                            {

                                myHttpWebReader = new StreamReader(myHttpWebResponse.GetResponseStream());
                                myHttpWebResult = myHttpWebReader.ReadToEnd();
                                ResponseText = myHttpWebResult;
                                blnRequestSuccess = true;
                            }
                            else
                            {
                                try
                                {
                                    myHttpWebReader = new StreamReader(myHttpWebResponse.GetResponseStream());
                                    myHttpWebResult = myHttpWebReader.ReadToEnd();
                                    ResponseText = myHttpWebResult;
                                }
                                catch
                                {
                                }
                                if (ResponseText.Trim().Length <= 0)
                                {
                                    ResponseText = myHttpWebResponse.StatusDescription;
                                }
                                if (ResponseText.Trim().Length <= 0)
                                    ResponseText = "ERROR " + myHttpWebResponse.StatusCode.ToString();
                                ErrorMessage = ResponseText;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        if (myHttpRequestStream != null)
                        {
                            myHttpRequestStream.Close();
                            myHttpRequestStream = null;
                        }
                        if (myHttpWebReader != null)
                        {
                            myHttpWebReader.Close();
                            myHttpWebReader = null;
                        }
                        if (myHttpWebResponse != null)
                        {
                            myHttpWebResponse.Close();
                            myHttpWebResponse = null;
                        }
                        if (myHttpWebRequest != null) myHttpWebRequest = null;
                        if (myHttpCredentials != null) myHttpCredentials = null;
                    }

                    if (blnRequestSuccess || !blnWaitForResponse)
                    {
                        break;
                    }
                    if (i < Retry) Thread.Sleep(RetryDelay);
                    Thread.Sleep(1);
                }
                if (!blnRequestSuccess && ErrorMessage.Trim().Length <= 0)
                {
                    ErrorMessage = "HTTP Request Failed";
                }
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }

            return new HttpResponseStatus(blnRequestSuccess, StatusCode, ErrorMessage, ResponseText);
        }
    }
}
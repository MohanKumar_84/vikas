﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
namespace mFicientCP
{
    public class DeleteHtml5AppTempFiles
    {
        public DeleteHtml5AppTempFiles(string subAdminId, string companyId)
        {
            this.SubadminId = subAdminId;
            this.CompanyId = companyId;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appName"></param>
        /// <returns></returns>
        public void DeleteTempFiles(string appId, string sessionId)
        {
            try
            {
                string strQuery = @"DELETE FROM [TBL_TEMP_HTML5_APP]
                                    WHERE COMPANY_ID = @COMPANY_ID
                                    AND SUBADMIN_ID = @SUBADMIN_ID
                                    AND SESSION_ID = @SESSION_ID
                                    AND APP_ID = @APP_ID";

                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
                objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
                objSqlCommand.Parameters.AddWithValue("@SESSION_ID", sessionId);
                MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        public void DeleteTempFile(string appId, string sessionId,string fileId)
        {
            try
            {
                string strQuery = @"DELETE FROM [TBL_TEMP_HTML5_APP]
                                    WHERE COMPANY_ID = @COMPANY_ID
                                    AND SUBADMIN_ID = @SUBADMIN_ID
                                    AND SESSION_ID = @SESSION_ID
                                    AND APP_ID = @APP_ID";

                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
                objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
                objSqlCommand.Parameters.AddWithValue("@SESSION_ID", sessionId);
                MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }

        public string SubadminId
        {
            private set;
            get;
        }
        public string CompanyId
        {
            private set;
            get;
        }

        public int StatusCode
        {
            private set;
            get;
        }

        public string StatusDescription
        {
            private set;
            get;
        }
    }
}
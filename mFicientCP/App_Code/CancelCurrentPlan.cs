﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class CancelCurrentPlan
    {

        public CancelCurrentPlan(string companyId)
        {
            CompanyId = companyId;
        }

        public void Process()
        {
            try
            {
                string strQuery = "";
                this.StatusCode = -1000;// for error
                strQuery = @"UPDATE ADMIN_TBL_PLAN_ORDER_REQUEST 
                             SET STATUS =@Status
                             WHERE COMPANY_ID = @CompanyId
                             AND STATUS =0";
                //see enum PLAN_REQUEST_STATUS status 2 means cancelled//Only those plan can be cancelled which are not in process which means status =0

                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.Parameters.AddWithValue("@Status", (int)PLAN_REQUEST_STATUS.CANCELLED);
                this.NoOfRowsAffected= MSSqlClient.ExecuteNonQueryRecordAdmin(cmd);
                this.StatusCode = 0;
                this.StatusDescription = "";
                
            }
            catch 
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
            }
        }

        
        public string CompanyId
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public DataTable CompanyDetail
        {
            set;
            get;
        }
        public int NoOfRowsAffected
        {
            get;
            private set;
        }
    }
}
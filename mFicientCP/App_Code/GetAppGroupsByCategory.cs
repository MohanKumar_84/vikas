﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCP
{
    public class GetAppGroupsByCategory
    {

        public GetAppGroupsByCategory(Boolean _IsGroupBy, string _CategoryId, string _SubAdminId, string _CompanyId)
        {
            this.IsGroupBy = _IsGroupBy;
            this.CategoryId = _CategoryId;
            this.ComapanyId = _CompanyId;
            this.SubAdminId = _SubAdminId;
        }
        public void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;

                query = @"select DISTINCT z.CATEGORY,  p.lastallversion, d3.Device,w.ICON as WF_ICON,  l.VERSION as preversion, w.WF_ID,
                        w.WF_NAME,cw.VERSION from dbo.TBL_WORKFLOW_DETAIL  as w 
                        left join dbo.TBL_CURR_WORKFLOW_DETAIL as  cw on cw.WF_ID=w.WF_ID  
                         and  cw.Company_ID=w.Company_ID
                        left join dbo.TBL_WORKFLOW_ADDITIONAL_DETAIL as t 
                         on t.WORKFLOW_ID=w.WF_ID and  t.Company_ID=w.Company_ID
                        left join (select WF_ID,Version from 
                        (select WF_ID,VERSION,COMMITED_ON ,ROW_NUMBER() OVER (PARTITION BY WF_ID
                                    ORDER BY COMMITED_ON desc) as t1 from dbo.tbl_prv_workflow_detail 
                                    where COMPANY_ID=@COMPANY_ID
                                     group by WF_ID,WF_NAME,VERSION ,COMMITED_ON)d  where t1='1')l
                                    on l.WF_ID=w.WF_ID   
                        left join  (select WF_ID,WF_NAME,(case WHEN ICON IS NULL then 'GRAY0207.png' 
                        else ICON end)WF_ICON ,device=(case Device when '0,1,'  
                                    then 'Phone and tablet' when  '1,0,' then 'Tablet and Phone' when '0,' then 'Phone' when '1,' then 'Tablet'
                                    else 'NA' end)   from ( select ICON ,WF_ID, WF_NAME,(select cast (MODEL_TYPE as VARCHAR(10))   
   	                                + ','  FROM TBL_WORKFLOW_DETAIL where WF_ID=d.WF_ID 
                                    for xml path('')  ) as Device from TBL_PRV_WORKFLOW_DETAIL  as d
                                    where COMPANY_ID=@COMPANY_ID)d12)d3 on d3.WF_ID=w.WF_ID
                        left join (select ss.WF_ID,ss.WF_NAME,
                         STUFF((select ',' + CAST (T.VERSION AS VARCHAR(70))  from   dbo.tbl_prv_workflow_detail as T where ss.WF_ID=T.WF_ID group by T.VERSION order by T.VERSION desc for xml path('')),1,1,'') as  lastallversion
                                    from   dbo.tbl_prv_workflow_detail as ss 
                                    where ss.COMPANY_ID=@COMPANY_ID 
                                    GROUP BY ss.WF_ID,WF_NAME)p on p.WF_ID=w.WF_ID 
                        left join (select DISTINCT  ss.WORKFLOW_ID,
                        STUFF((select ',' + T.CATEGORY from 
                        ( select m.CATEGORY,w2.WORKFLOW_ID 
                        from TBL_WORKFLOW_AND_CATEGORY_LINK as w2
                        inner join dbo.TBL_MENU_CATEGORY as m on 
                        m.MENU_CATEGORY_ID=w2.CATEGORY_ID where 
                        m.COMPANY_ID=@COMPANY_ID) as T 
                        where t.WORKFLOW_ID=ss.WORKFLOW_ID for xml path('')),1,1,'') as CATEGORY  from  (select m.CATEGORY,w2.WORKFLOW_ID from TBL_WORKFLOW_AND_CATEGORY_LINK as w2
                        inner join dbo.TBL_MENU_CATEGORY 
                        as m on  m.MENU_CATEGORY_ID=w2.CATEGORY_ID 
                         where m.COMPANY_ID=@COMPANY_ID) as ss )z on  z.WORKFLOW_ID=w.WF_ID
                        where  w.COMPANY_ID=@COMPANY_ID and z.CATEGORY  IS NOT NULL order by w.WF_NAME";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@Company_ID", this.ComapanyId);

                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                this.Resultdataset = objDataSet;
                this.StatusCode = 0;

            }
            catch
            {
                this.StatusCode = -1000;
            }
        }
        public DataSet Resultdataset
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }

        public bool IsGroupBy { get; set; }

        public string CategoryId { get; set; }

        public string SubAdminId { get; set; }
        public string ComapanyId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientCP
{
    public class OfflineDataObject
    {
        private string id, companyId, subadminId, name, description, query, inputParams, outputParams, offlineTable, errorMessage;
        private bool allowRetry, exitOnError;
        DatabaseCommandType queryType;

        public OfflineDataObject()
        {
        }

        public OfflineDataObject(string _id, string _companyId, string _subadminId, string _name, string _description, string _query, string _inputParams, string _outputParams, string _offlineTables, DatabaseCommandType _queryType, bool _allowRetry, bool _exitOnError, string _errorMessage)
        {
            id = _id;
            companyId = _companyId;
            subadminId = _subadminId;
            name = _name;
            description = _description;
            query = _query;
            inputParams = _inputParams;
            outputParams = _outputParams;
            offlineTable = _offlineTables;
            queryType = _queryType;
            allowRetry = _allowRetry;
            exitOnError = _exitOnError;
            errorMessage = _errorMessage;
        }

        public string ID
        {

            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public string CommandName
        {

            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public string CompanyId
        {
            get
            {
                return companyId;
            }
            set
            {
                companyId = value;
            }

        }

        public string SubadminId
        {
            get
            {
                return subadminId;
            }
            set
            {
                subadminId = value;
            }

        }

        public string Name
        {

            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public string Query
        {
            get
            {
                return query;
            }
            set
            {
                query = value;
            }
        }

        public string InputParameters
        {
            get
            {
                return inputParams;
            }
            set
            {
                inputParams = value;
            }
        }

        public string OutputParameters
        {
            get
            {
                return outputParams;
            }
            set
            {
                outputParams = value;
            }

        }

        public string OfflineTable
        {
            get
            {
                return offlineTable;
            }
            set
            {
                offlineTable = value;
            }

        }

        public DatabaseCommandType QueryType
        {
            get
            {
                return queryType;
            }
            set
            {
                queryType = value;
            }
        }

        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                errorMessage = value;
            }

        }

        public bool AllowRetry
        {
            get
            {
                return allowRetry;
            }
            set
            {
                allowRetry = value;
            }

        }

        public bool ExitOnError
        {
            get
            {
                return exitOnError;
            }
            set
            {
                exitOnError = value;
            }

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class DeleteGroupandGroupUsers
    {
        public DeleteGroupandGroupUsers()
        { 

        }
        //public void DeleteGroup()
        //{
        //    SqlConnection objSqlConnection = null;
        //    SqlTransaction objSqlTransaction = null;
        //    try
        //    {
        //        MSSqlClient.SqlConnectionOpen(out objSqlConnection);
        //        string query = @"DELETE FROM TBL_USER_GROUP WHERE GROUP_ID=@GROUP_ID AND SUBADMIN_ID=@SUBADMIN_ID AND  COMPANY_ID=@COMPANY_ID;";

        //        objSqlTransaction = objSqlConnection.BeginTransaction();
        //        SqlCommand objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
        //        objSqlCommand.CommandType = CommandType.Text;
        //        objSqlCommand.Parameters.AddWithValue("@GROUP_ID", this.GroupId);
        //        objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminIdWhoCreatedGrp);
        //        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
        //        if (objSqlCommand.ExecuteNonQuery() > 0)
        //        {
        //            this.StatusCode = 0;
        //            this.StatusDescription = "";
        //        }
        //        else
        //        {
        //            throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
        //        }

        //        string query2 = @"DELETE FROM TBL_USER_GROUP_LINK WHERE GROUP_ID=@GROUP_ID AND  COMPANY_ID=@COMPANY_ID;";

        //        objSqlCommand = new SqlCommand(query2, objSqlConnection, objSqlTransaction);
        //        objSqlCommand.CommandType = CommandType.Text;
        //        objSqlCommand.Parameters.AddWithValue("@GROUP_ID", this.GroupId);
        //        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
        //        objSqlCommand.ExecuteNonQuery();
                
        //        SubAdminLog.saveSubAdminActivityLog("S", this.SubAdminIdDeletingGrp, this.GroupName,
        //            this.CompanyId, SUBADMIN_ACTIVITY_LOG.DELETE_GROUP, DateTime.UtcNow.Ticks,
        //            "", "", this.GroupId, this.GroupName, objSqlConnection, objSqlTransaction
        //                                            );
                
        //        objSqlTransaction.Commit();
        //        this.StatusCode = 0;
        //        this.StatusDescription = "";
        //    }
        //    catch (Exception ex)
        //    {

        //        if (objSqlTransaction != null)
        //            objSqlTransaction.Rollback();
        //        if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
        //        {
        //        }
        //        StatusCode = -1000;
        //        StatusDescription = "Internal server error";
        //    }
        //    finally
        //    {
        //        MSSqlClient.SqlConnectionClose(objSqlConnection);
        //    }
        //}
        //public void DeleteUsersFromGroup(string userId,
        //    string groupId,
        //    string companyId)
        //{
        //    try
        //    {
        //        string query = @"DELETE FROM TBL_USER_GROUP_LINK WHERE GROUP_ID=@GROUP_ID AND USER_ID=@USER_ID AND  COMPANY_ID=@COMPANY_ID;";

        //        SqlCommand objSqlCommand = new SqlCommand(query);
        //        objSqlCommand.CommandType = CommandType.Text;
        //        objSqlCommand.Parameters.AddWithValue("@GROUP_ID",userId);
        //        objSqlCommand.Parameters.AddWithValue("@USER_ID", userId);
        //        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID",companyId);
        //        if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
        //        {
        //            this.StatusCode = 0;
        //            this.StatusDescription = "";
        //        }
        //        else
        //        {
        //            throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        this.StatusCode =-1000;
        //        this.StatusDescription = "Internal server error.";
        //    }
        //}
        //public void DeleteUsersFromGroup(SqlConnection con,
        //    SqlTransaction transaction,
        //    string userId,
        //    string groupId,
        //    string companyId)
        //{
        //    try
        //    {
        //        string query = @"DELETE FROM TBL_USER_GROUP_LINK WHERE GROUP_ID=@GROUP_ID AND USER_ID=@USER_ID AND  COMPANY_ID=@COMPANY_ID;";

        //        SqlCommand objSqlCommand = new SqlCommand(query,con,transaction);
        //        objSqlCommand.CommandType = CommandType.Text;
        //        objSqlCommand.Parameters.AddWithValue("@GROUP_ID",groupId);
        //        objSqlCommand.Parameters.AddWithValue("@USER_ID",userId);
        //        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID",companyId);
        //        objSqlCommand.ExecuteNonQuery();
        //        this.StatusCode = 0;
        //        this.StatusDescription = "";
        //    }
        //    catch (Exception)
        //    {
        //        this.StatusCode = -1000;
        //        this.StatusDescription = "Internal server error.";
        //    }
        //}
        public void DeleteAllUsersGroupLink(SqlConnection con,
            SqlTransaction transaction,
            string userId,
            string companyId)
        {
            try
            {
                string query = @"DELETE FROM TBL_USER_GROUP_LINK WHERE  USER_ID=@USER_ID AND  COMPANY_ID=@COMPANY_ID;";

                SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", userId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
                objSqlCommand.ExecuteNonQuery();
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        public string GroupId
        {
            set;
            get;
        }
        public string UserId
        {
            set;
            get;
        }
        public string SubAdminIdWhoCreatedGrp
        {
            set;
            get;
        }
        public string SubAdminIdDeletingGrp
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public string CompanyId
        {
            set;
            get;
        }

        public string GroupName
        {
            set;
            get;
        }
    }
}
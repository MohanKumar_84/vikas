﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetHtml5Apps
    {
        private string companyId = string.Empty, appId = string.Empty;
        public GetHtml5Apps(string _companyId)
        {
            companyId = _companyId;
            appId = string.Empty;
        }

        public GetHtml5Apps(string _companyId, string _appId)
        {
            companyId = _companyId;
            appId = _appId;
        }

        public void Process()
        {
            try
            {
                StatusCode = -1000;
                string query = string.Empty;
                if(string.IsNullOrEmpty(appId))
                    query = @"SELECT * FROM TBL_HTML5_APPS WHERE COMPANY_ID = @COMPANY_ID;";
                else 
                    query = @"SELECT * FROM TBL_HTML5_APPS WHERE COMPANY_ID = @COMPANY_ID  AND APP_ID = @APP_ID";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
                objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null && objDataSet.Tables.Count > 0)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }

            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }

        public DataTable ResultTable
        {
            set;
            get;
        }
        
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
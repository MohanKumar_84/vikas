﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class SaveHtml5App
    {
        private Html5App html5App;
        private string userId, homeView;
        SqlConnection con;
        SqlTransaction transaction = null;
        DisplayView displayView = DisplayView.Mobile;
        #region Previous Code
        public SaveHtml5App(Html5App _html5App, string _userId, DisplayView _displayView, string _homeView)
        {
            html5App = _html5App;
            userId = _userId;
            displayView = _displayView;
            homeView = _homeView;
        }

        public void Process()
        {
            try
            {
                StatusCode = -1000;
                MSSqlClient.SqlConnectionOpen(out con);
                try
                {
                    using (con)
                    {
                        using (transaction = con.BeginTransaction())
                        {
                            try
                            {
                                saveApp();
                                saveAppFiles();
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            transaction.Commit();
                        }
                    }
                    this.StatusCode = 0;
                    this.StatusDescription = String.Empty;
                }
                catch (SqlException e)
                {
                    if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                    }
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                catch (Exception e)
                {
                    if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                    }
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                finally
                {
                    if (con != null)
                    {
                        con.Dispose();
                    }
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }
                }

            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        private void saveApp()
        {
            try
            {
                string query = @"IF NOT EXISTS(SELECT * FROM TBL_HTML5_APPS WHERE APP_NAME = @APP_NAME AND COMPANY_ID = @COMPANY_ID) 
                             INSERT INTO TBL_HTML5_APPS (COMPANY_ID, APP_ID, APP_NAME, DESCRIPTION, CREATED_BY, CREATED_ON, MODIFIED_BY, DISPLAY_VIEW, LAST_MODIFIED, HOME_VIEW) VALUES (@COMPANY_ID, @APP_ID, @APP_NAME, @DESCRIPTION, @CREATED_BY, @CREATED_ON, @MODIFIED_BY, @DISPLAY_VIEW, @LAST_MODIFIED, @HOME_VIEW)";

                SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", html5App.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@APP_ID", html5App.AppId);
                objSqlCommand.Parameters.AddWithValue("@APP_NAME", html5App.AppName);
                objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", html5App.Description);
                objSqlCommand.Parameters.AddWithValue("@CREATED_BY", userId);
                objSqlCommand.Parameters.AddWithValue("@CREATED_ON", DateTime.UtcNow.Ticks);
                objSqlCommand.Parameters.AddWithValue("@MODIFIED_BY", userId);
                objSqlCommand.Parameters.AddWithValue("@DISPLAY_VIEW", displayView);
                objSqlCommand.Parameters.AddWithValue("@HOME_VIEW", homeView);
                objSqlCommand.Parameters.AddWithValue("@LAST_MODIFIED", DateTime.UtcNow.Ticks);
                int iRowsEffected = objSqlCommand.ExecuteNonQuery();
                if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void saveAppFiles()
        {
            SaveHtml5AppFile saveAppFile = null;
            foreach (HTML5AppFile appFile in html5App.Imagefiles.Files)
            {
                try
                {
                    saveAppFile = new SaveHtml5AppFile(appFile, FileType.Image, html5App.AppId, html5App.CompanyId);
                    saveAppFile.Process(con, transaction);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            foreach (HTML5AppFile appFile in html5App.Scriptfiles.Files)
            {
                try
                {
                    saveAppFile = new SaveHtml5AppFile(appFile, FileType.Script, html5App.AppId, html5App.CompanyId);
                    saveAppFile.Process(con, transaction);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            foreach (HTML5AppFile appFile in html5App.Stylefiles.Files)
            {
                try
                {
                    saveAppFile = new SaveHtml5AppFile(appFile, FileType.Style, html5App.AppId, html5App.CompanyId);
                    saveAppFile.Process(con, transaction);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            foreach (HTML5AppFile appFile in html5App.Rootfiles.Files)
            {
                try
                {
                    saveAppFile = new SaveHtml5AppFile(appFile, FileType.Root, html5App.AppId, html5App.CompanyId);
                    saveAppFile.Process(con, transaction);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion
        public SaveHtml5App(string subAdminId, string companyId)
        {
            this.SubadminId = subAdminId;
            this.CompanyId = companyId;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appName"></param>
        /// <returns></returns>
        public MfHtml5App SaveNewApp(string appName)
        {
            string strNewAppId = String.Empty;
            MfHtml5App objHtml5App = null;
            try
            {
                string strAppId = Utilities.GetMd5Hash(this.SubadminId + appName + this.CompanyId + DateTime.UtcNow.Ticks.ToString());
                objHtml5App = insertNewApp(strAppId, appName);
                strNewAppId = strAppId;
                StatusCode = 0;
                this.StatusDescription = "";
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            return objHtml5App;
        }

        public void UpdateApp(MFEHtml5AppDetail appDetail)
        {
            string strQuery = @"UPDATE [TBL_HTML5_APP_DETAIL]
                               SET [APP_NAME] = @APP_NAME
                                  ,[DESCRIPTION] = @DESCRIPTION
                                  ,[APP_JSON] = @APP_JSON
                                  ,[ICON] = @ICON
                                  ,[UPDATE_ON] = @UPDATE_ON
      
                             WHERE APP_ID = @APP_ID
                             AND COMPANY_ID = @COMPANY_ID
                             AND SUBADMIN_ID = @SUBADMIN_ID";

            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@APP_NAME", appDetail.AppName);
            objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", appDetail.Description);
            objSqlCommand.Parameters.AddWithValue("@APP_JSON", appDetail.AppJson);
            objSqlCommand.Parameters.AddWithValue("@ICON", appDetail.Icon);
            objSqlCommand.Parameters.AddWithValue("@UPDATE_ON", DateTime.UtcNow.Ticks);
            objSqlCommand.Parameters.AddWithValue("@APP_ID", appDetail.AppId);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
            MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);

        }
        public MFEHtml5AppDetail SaveAsApp(string appName, MfHtml5App appToCopy)
        {
            //string strNewAppId = String.Empty;
            MfHtml5App objHtml5App = null;
            MFEHtml5AppDetail objMfeHtml5AppDtl = null;
            try
            {
                string strAppId = Utilities.GetMd5Hash(this.SubadminId + appName + this.CompanyId + DateTime.UtcNow.Ticks.ToString());
                objHtml5App = new MfHtml5App();
                objHtml5App.id = strAppId;
                objHtml5App.name = appName;
                objHtml5App.icon = appToCopy.icon;
                objHtml5App.startupPage = appToCopy.startupPage;
                objHtml5App.description = appToCopy.description;
                objMfeHtml5AppDtl = insertNewApp(objHtml5App);
                //strNewAppId = strAppId;
                StatusCode = 0;
                this.StatusDescription = "";
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            return objMfeHtml5AppDtl;
        }

        MFEHtml5AppDetail insertNewApp(MfHtml5App html5App)
        {
            string strQuery = @"INSERT INTO [TBL_HTML5_APP_DETAIL]
                                ([COMPANY_ID],[APP_ID],[APP_NAME]
                                ,[DESCRIPTION],[APP_JSON],[ICON]
                                ,[SUBADMIN_ID],[CREATED_BY],[UPDATE_ON]
                                ,[BUCKET_FILE_PATH])
                                VALUES
                                (@COMPANY_ID,@APP_ID,@APP_NAME,
                                    @DESCRIPTION,@APP_JSON,@ICON,
                                    @SUBADMIN_ID,@CREATED_BY,@UPDATE_ON,
                                    @BUCKET_FILE_PATH)";



            string strS3BucketFilePath = String.Empty;
            strS3BucketFilePath = S3Bucket.GetCurrentHtml5AppZipFolderPath(this.SubadminId, this.CompanyId, html5App.id, html5App.id + ".zip");
            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@APP_ID", html5App.id);
            objSqlCommand.Parameters.AddWithValue("@APP_NAME", html5App.name);
            objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", html5App.description);
            objSqlCommand.Parameters.AddWithValue("@APP_JSON", Utilities.SerializeJson<MfHtml5App>(html5App));
            objSqlCommand.Parameters.AddWithValue("@ICON", html5App.icon);
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
            objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.SubadminId);
            objSqlCommand.Parameters.AddWithValue("@UPDATE_ON", DateTime.UtcNow.Ticks);
            objSqlCommand.Parameters.AddWithValue("@BUCKET_FILE_PATH", strS3BucketFilePath);

            MFEHtml5AppDetail objMfeHtml5AppDtl = new MFEHtml5AppDetail();
            objMfeHtml5AppDtl.AppId = html5App.id;
            objMfeHtml5AppDtl.AppName = html5App.name;
            objMfeHtml5AppDtl.AppJson = Utilities.SerializeJson<MfHtml5App>(html5App);
            objMfeHtml5AppDtl.BucketFilePath = strS3BucketFilePath;
            objMfeHtml5AppDtl.CompanyId = this.CompanyId;
            objMfeHtml5AppDtl.CreatedBy = this.SubadminId;
            objMfeHtml5AppDtl.Description = html5App.description;
            objMfeHtml5AppDtl.Icon = html5App.icon;
            objMfeHtml5AppDtl.SubadminId = this.SubadminId;
            objMfeHtml5AppDtl.UpdatedOn = (DateTime.UtcNow.Ticks).ToString();

            MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);

            return objMfeHtml5AppDtl;
        }
        MfHtml5App insertNewApp(string appId, string appName)
        {
            string strQuery = @"INSERT INTO [TBL_HTML5_APP_DETAIL]
                                ([COMPANY_ID],[APP_ID],[APP_NAME]
                                ,[DESCRIPTION],[APP_JSON],[ICON]
                                ,[SUBADMIN_ID],[CREATED_BY],[UPDATE_ON]
                                ,[BUCKET_FILE_PATH])
                                VALUES
                                (@COMPANY_ID,@APP_ID,@APP_NAME,
                                    @DESCRIPTION,@APP_JSON,@ICON,
                                    @SUBADMIN_ID,@CREATED_BY,@UPDATE_ON,
                                    @BUCKET_FILE_PATH)";

            MfHtml5App objHtml5App = new MfHtml5App();
            objHtml5App.id = appId;
            objHtml5App.name = appName;
            objHtml5App.icon = MficientConstants.IDE_APPLICATION_DEFAULT_ICON;
            objHtml5App.startupPage = String.Empty;
            objHtml5App.description = String.Empty;

            string strS3BucketFilePath = String.Empty;
            strS3BucketFilePath = S3Bucket.GetCurrentHtml5AppZipFolderPath(this.SubadminId, this.CompanyId, appId, appId + ".zip");
            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            objSqlCommand.Parameters.AddWithValue("@APP_ID", appId);
            objSqlCommand.Parameters.AddWithValue("@APP_NAME", appName);
            objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", "");
            objSqlCommand.Parameters.AddWithValue("@APP_JSON", Utilities.SerializeJson<MfHtml5App>(objHtml5App));
            objSqlCommand.Parameters.AddWithValue("@ICON", MficientConstants.IDE_APPLICATION_DEFAULT_ICON);
            objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubadminId);
            objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.SubadminId);
            objSqlCommand.Parameters.AddWithValue("@UPDATE_ON", DateTime.UtcNow.Ticks);
            objSqlCommand.Parameters.AddWithValue("@BUCKET_FILE_PATH", strS3BucketFilePath);
            MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);

            return objHtml5App;
        }



        public string SubadminId
        {
            private set;
            get;
        }
        public string CompanyId
        {
            private set;
            get;
        }

        public int StatusCode
        {
            private set;
            get;
        }

        public string StatusDescription
        {
            private set;
            get;
        }
    }
}
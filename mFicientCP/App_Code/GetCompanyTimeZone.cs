﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetCompanyTimeZone
    {
        public GetCompanyTimeZone(string companyId)
        {
            this.CompanyId = companyId;
        }
        public void Process()
        {
            StatusCode = -1000;//for error
            try
            {
                Timezone = getTimezone((string)getCompanyCountryCode().Rows[0]["COUNTRY_CODE"]);
                StatusCode = 0;
                StatusDescription = "";
            }
            catch(Exception e)
            {
                StatusCode = -1000;
                StatusDescription = e.Message;
            }
        }
        DataTable getCompanyCountryCode()
        {
            try
            {
                string strQuery = @"SELECT COUNTRY_CODE FROM ADMIN_TBL_COMPANY_DETAIL
                                    WHERE COMPANY_ID = @CompanyId;";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                DataSet dsCountryCode = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (dsCountryCode.Tables[0].Rows.Count > 0)
                {
                    return dsCountryCode.Tables[0];
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        string getTimezone(string countryCode)
        {
            try
            {
                string strQuery = @"SELECT * FROM ADMIN_TBL_MST_COUNTRY
                                    WHERE COUNTRY_CODE = @CountryCode;";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CountryCode", countryCode);
                DataSet dsCountryMaster = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (dsCountryMaster.Tables[0].Rows.Count > 0)
                {
                    return (string)dsCountryMaster.Tables[0].Rows[0]["TIMEZONE_STRING"];
                }
                else
                {
                    throw new Exception("Country code does not exist");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string CompanyId
        {
            get;
            set;
        }
        public string Timezone
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public int StatusCode
        {
            get;
            set;
        }
    }
}
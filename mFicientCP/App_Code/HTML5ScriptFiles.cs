﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;

namespace mFicientCP
{
    public class HTML5ScriptFiles : ICollection<HTML5AppFile>
    {
        private List<HTML5AppFile> scriptFiles;
        private object collectionLock;

        public HTML5ScriptFiles()
        {
            scriptFiles = new List<HTML5AppFile>();
            collectionLock = new object();
        }

        int ICollection<HTML5AppFile>.Count
        {
            get
            {
                return scriptFiles.Count;
            }
        }

        public List<HTML5AppFile> Files
        {
            get
            {
                return scriptFiles;
            }
        }

        public void Add(HTML5AppFile item)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    if (!scriptFiles.Contains(item))
                        scriptFiles.Add(item);
                }
                catch
                {
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        public void AddRange(List<HTML5AppFile> files)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    scriptFiles.AddRange(files);
                }
                catch
                {

                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        bool ICollection<HTML5AppFile>.Remove(HTML5AppFile item)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    if (scriptFiles.Contains(item))
                    {
                        scriptFiles.Remove(item);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
            return false;
        }

        public void RemoveAt(int index)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    scriptFiles.RemoveAt(index);
                }
                catch
                {

                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        public void RemoveAll(Predicate<HTML5AppFile> files)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    scriptFiles.RemoveAll(files);
                }
                catch
                {
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        public void Clear()
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    scriptFiles.Clear();
                }
                catch
                {

                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        public bool Contains(HTML5AppFile item)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    if (scriptFiles.Contains(item))
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
            return false;
        }

        public HTML5AppFile GetFile(string filename)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    foreach (HTML5AppFile file in scriptFiles)
                    {
                        if (file.FileName == filename)
                        {
                            return file;
                        }
                    }
                }
                catch
                {
                    return null;
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
            return null;
        }

        public void CopyTo(HTML5AppFile[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public IEnumerator<HTML5AppFile> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
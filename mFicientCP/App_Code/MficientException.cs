﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientCP
{
    [Serializable]
    public class MficientException : ApplicationException
    {
        public MficientException() { }
        public MficientException(string message) : base(message) { }
        public MficientException(string message, Exception inner) : base(message, inner) { }
        protected MficientException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }

    [Serializable]
    public class UnSupportedHtml5AppFileException : ApplicationException
    {
        public UnSupportedHtml5AppFileException() { }
        public UnSupportedHtml5AppFileException(string message) : base(message) { }
        public UnSupportedHtml5AppFileException(string message, Exception inner) : base(message, inner) { }
        protected UnSupportedHtml5AppFileException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }

    [Serializable]
    public class DataSetNullException : ApplicationException
    {
        public DataSetNullException() { }
        public DataSetNullException(string message) : base(message) { }
        public DataSetNullException(string message, Exception inner) : base(message, inner) { }
        protected DataSetNullException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
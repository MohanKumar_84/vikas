﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class DeleteWfDetails
    {
        public DeleteWfDetails()
        {

        }
        //new
        public DeleteWfDetails(string _WorkFlowId, string _SubAdminId, string _CompanyId)
        {
            this.WorkflowId = _WorkFlowId;
            this.SubAdminId = _SubAdminId;
            this.CompanyId = _CompanyId;
        }
        //old
        //public DeleteWfDetails(string _WorkFlowId, string _SubAdminId, string _CompanyId,Boolean _DeleteTabletOnly,int _ModalType)
        //{
        //    this.WorkflowId = _WorkFlowId;
        //    this.SubAdminId = _SubAdminId;
        //    this.CompanyId = _CompanyId;
        //    this.DeleteTabletOnly = _DeleteTabletOnly;
        //    this.ModalType = _ModalType;
        //}

        public void Process()
        {
            this.StatusCode = -1000;
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            SqlCommand objSqlCommand;
            try
            {
                string query = "";
                AddIntoUpdationRequiredForMenu objUpdation = new AddIntoUpdationRequiredForMenu();
                objUpdation.AppFlowUpdation(WorkflowId, CompanyId,null);

                MSSqlClient.SqlConnectionOpen(out objSqlConnection);

                query = @"DELETE FROM TBL_WORKFLOW_DETAIL WHERE WF_ID=@WF_ID AND SUBADMIN_ID=@SUBADMIN_ID AND COMPANY_ID=@COMPANY_ID;";
                objSqlTransaction = objSqlConnection.BeginTransaction();
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WorkflowId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

                if (objSqlCommand.ExecuteNonQuery() > 0) this.StatusCode = 0;
                else throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());

                query = @"DELETE FROM TBL_WORKFLOW_AND_CATEGORY_LINK WHERE WORKFLOW_ID=@WF_ID AND COMPANY_ID=@COMPANY_ID;
                          DELETE FROM TBL_WORKFLOW_AND_GROUP_LINK WHERE WORKFLOW_ID=@WF_ID AND COMPANY_ID=@COMPANY_ID;
                          DELETE FROM TBL_CURR_WORKFLOW_DETAIL WHERE WF_ID=@WF_ID  AND SUBADMIN_ID=@SUBADMIN_ID AND COMPANY_ID=@COMPANY_ID;
                          DELETE FROM TBL_PRV_WORKFLOW_DETAIL WHERE WF_ID=@WF_ID  AND SUBADMIN_ID=@SUBADMIN_ID AND COMPANY_ID=@COMPANY_ID;
                          DELETE FROM TBL_WORKFLOW_DETAIL_FOR_TESTER WHERE WF_ID = @WF_ID AND SUBADMIN_ID = @SUBADMIN_ID AND COMPANY_ID = @COMPANY_ID";
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WorkflowId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.ExecuteNonQuery();

                objSqlTransaction.Commit();
                this.StatusCode = 0;
                
            }
            catch
            {
                this.StatusCode = -1000; 
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }

        public string WorkflowId
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }

        public bool DeleteTabletOnly { get; set; }

        public int ModalType { get; set; }
    }
}
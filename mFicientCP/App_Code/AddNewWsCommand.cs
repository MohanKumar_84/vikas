﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class AddNewWsCommand
    {
        public AddNewWsCommand(string _SubAdminId, string _CommandName, string _ConnactorId, string _Url,
         string _Parameter, string _ReturnType, string _RecordPath, string _DataSetPath, string _Tag, string _Service, string _PortType, string _Method, 
            string _WebserviceType, string _SoapRequest, string _OperationAction, string _XmlRpc, Int32 _HttpType, string _HttpRequest, string _Company_id, 
            string _Description, int _Cache, int _Expfrequency, string _ExpCondtion, string _Xreference, string _CreatedBy, long _CreatedOn, long _UpdatedOn,
            string _Headers)
        {
            this.SubAdminId = _SubAdminId;
            this.CommandName = _CommandName;
            this.ConnactorId = _ConnactorId;
            this.Url = _Url;
            this.Parameter = _Parameter;
            this.ReturnType = _ReturnType;
            this.RecordPath = _RecordPath;
            this.DataSetPath = _DataSetPath;
            this.Tag = _Tag;
            this.Service = _Service;
            this.PortType = _PortType;
            this.Method = _Method;
            this.WebserviceType = _WebserviceType;
            this.SoapRequest = _SoapRequest;
            this.OperationAction = _OperationAction;
            this.XmlRpc = _XmlRpc;
            this.HttpType = _HttpType;
            this.HttpRequest = _HttpRequest;
            this.Company_id = _Company_id;
            this.Description = _Description;
            this.Cache = _Cache;
            this.Expfrequency = _Expfrequency;
            this.ExpCondtion = _ExpCondtion;
            this.Xreference = _Xreference;
            this.CreatedBy = _CreatedBy;
            this.CreatedOn = _CreatedOn;
            this.UpdatedOn = _UpdatedOn;
            this.Headers = _Headers;
        }
      public AddNewWsCommand(string _SubAdminId, string _CommandName, string DbCommandId, string _CompanyId, long _UpdatedOn)
       {
           this.SubAdminId = _SubAdminId;
           this.CommandName = _CommandName;
           this.CommandId = DbCommandId;
           this.Company_id = _CompanyId;
           this.UpdatedOn = _UpdatedOn;
           CopyWsObjectProcess();
       }

        public void Process2()
        {
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            SqlCommand objSqlCommand;
            try
            {
                this.StatusCode = -1000;
                this.CommandId = Utilities.GetMd5Hash(this.SubAdminId + this.CommandName + DateTime.UtcNow.Ticks.ToString());
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                string query = @"INSERT INTO TBL_WS_COMMAND(WS_COMMAND_ID,WS_CONNECTOR_ID,SUBADMIN_ID,URL,PARAMETER,RETURN_TYPE,RECORD_PATH,DATASET_PATH,TAG,WS_COMMAND_NAME,
                    SERVICE,PORT_TYPE,METHOD,WEBSERVICE_TYPE,SOAP_REQUEST,OPERATION_ACTION,HTTP_TYPE,HTTP_REQUEST,COMPANY_ID, DESCRIPTION,CACHE,EXPIRY_FREQUENCY,EXPIRY_CONDITION,
                    X_REFERENCES,CREATED_BY,CREATED_ON,UPDATED_ON,HEADERS)
                            VALUES(@WS_COMMAND_ID,@WS_CONNECTOR_ID,@SUBADMIN_ID,@URL,@PARAMETER,@RETURN_TYPE,@RECORD_PATH,@DATASET_PATH,@TAG,@WS_COMMAND_NAME,
                    @SERVICE,@PORT_TYPE,@METHOD,@WEBSERVICE_TYPE,@SOAP_REQUEST,@OPERATION_ACTION,@HTTP_TYPE,@HTTP_REQUEST,@COMPANY_ID, @DESCRIPTION,@CACHE,
                    @EXPIRY_FREQUENCY,@EXPIRY_CONDITION,@X_REFERENCES,@CREATED_BY,@CREATED_ON,@UPDATED_ON,@HEADERS);";

                objSqlTransaction = objSqlConnection.BeginTransaction();
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WS_COMMAND_ID", this.CommandId);
                objSqlCommand.Parameters.AddWithValue("@WS_CONNECTOR_ID", this.ConnactorId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@URL", this.Url);
                objSqlCommand.Parameters.AddWithValue("@PARAMETER", this.Parameter);
                objSqlCommand.Parameters.AddWithValue("@RETURN_TYPE", this.ReturnType);
                objSqlCommand.Parameters.AddWithValue("@RECORD_PATH", this.RecordPath);
                objSqlCommand.Parameters.AddWithValue("@DATASET_PATH", this.DataSetPath);
                objSqlCommand.Parameters.AddWithValue("@TAG", this.Tag);
                objSqlCommand.Parameters.AddWithValue("@WS_COMMAND_NAME", this.CommandName);
                objSqlCommand.Parameters.AddWithValue("@SERVICE", this.Service);
                objSqlCommand.Parameters.AddWithValue("@PORT_TYPE", this.PortType);
                objSqlCommand.Parameters.AddWithValue("@METHOD", this.Method);
                objSqlCommand.Parameters.AddWithValue("@WEBSERVICE_TYPE", this.WebserviceType);
                objSqlCommand.Parameters.AddWithValue("@SOAP_REQUEST", this.SoapRequest);
                objSqlCommand.Parameters.AddWithValue("@HTTP_REQUEST", this.HttpRequest);
                objSqlCommand.Parameters.AddWithValue("@OPERATION_ACTION", this.OperationAction);
                objSqlCommand.Parameters.AddWithValue("@HTTP_TYPE", this.HttpType);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
                objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", this.Description);
                objSqlCommand.Parameters.AddWithValue("@CACHE", this.Cache);
                objSqlCommand.Parameters.AddWithValue("@EXPIRY_FREQUENCY", this.Expfrequency);
                objSqlCommand.Parameters.AddWithValue("@EXPIRY_CONDITION", this.ExpCondtion);
                objSqlCommand.Parameters.AddWithValue("@X_REFERENCES", this.Xreference);
                objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.CreatedBy);
                objSqlCommand.Parameters.AddWithValue("@CREATED_ON", this.CreatedOn);
                objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", this.UpdatedOn);
                objSqlCommand.Parameters.AddWithValue("@HEADERS", this.Headers);

                if (objSqlCommand.ExecuteNonQuery() > 0)
                    this.StatusCode = 0;
                else
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
             
                objSqlTransaction.Commit();
            }
            catch 
            {
                this.StatusCode = -1000;
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }


        public void Process3()
        {
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            SqlCommand objSqlCommand;
            try
            {
                this.StatusCode = -1000;
                this.CommandId = Utilities.GetMd5Hash(this.SubAdminId + this.CommandName + DateTime.UtcNow.Ticks.ToString());
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                string query = @"INSERT INTO TBL_WS_COMMAND(WS_COMMAND_ID,WS_CONNECTOR_ID,SUBADMIN_ID,URL,PARAMETER,RETURN_TYPE,RECORD_PATH,DATASET_PATH,TAG,WS_COMMAND_NAME,
                                SERVICE,PORT_TYPE,METHOD,WEBSERVICE_TYPE,SOAP_REQUEST,OPERATION_ACTION,HTTP_TYPE,COMPANY_ID, DESCRIPTION,CACHE,EXPIRY_FREQUENCY,EXPIRY_CONDITION,
                                X_REFERENCES,CREATED_BY,CREATED_ON,UPDATED_ON,HTTP_REQUEST,HEADERS)
                                    VALUES(@WS_COMMAND_ID,@WS_CONNECTOR_ID,@SUBADMIN_ID,@URL,@PARAMETER,@RETURN_TYPE,@RECORD_PATH,@DATASET_PATH,@TAG,@WS_COMMAND_NAME,
                                @SERVICE,@PORT_TYPE,@METHOD,@WEBSERVICE_TYPE,@SOAP_REQUEST,@OPERATION_ACTION,@HTTP_TYPE,@COMPANY_ID, @DESCRIPTION,@CACHE,@EXPIRY_FREQUENCY,@EXPIRY_CONDITION,
                                @X_REFERENCES,@CREATED_BY,@CREATED_ON,@UPDATED_ON,@HTTP_REQUEST,@HEADERS);";

                objSqlTransaction = objSqlConnection.BeginTransaction();
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WS_COMMAND_ID", this.CommandId);
                objSqlCommand.Parameters.AddWithValue("@WS_CONNECTOR_ID", this.ConnactorId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@URL", this.Url);
                objSqlCommand.Parameters.AddWithValue("@PARAMETER", this.Parameter);
                objSqlCommand.Parameters.AddWithValue("@RETURN_TYPE", this.ReturnType);
                objSqlCommand.Parameters.AddWithValue("@RECORD_PATH", this.RecordPath);
                objSqlCommand.Parameters.AddWithValue("@DATASET_PATH", this.DataSetPath);
                objSqlCommand.Parameters.AddWithValue("@TAG", this.Tag);
                objSqlCommand.Parameters.AddWithValue("@WS_COMMAND_NAME", this.CommandName);
                objSqlCommand.Parameters.AddWithValue("@SERVICE", this.Service);
                objSqlCommand.Parameters.AddWithValue("@PORT_TYPE", this.PortType);
                objSqlCommand.Parameters.AddWithValue("@METHOD", this.Method);
                objSqlCommand.Parameters.AddWithValue("@WEBSERVICE_TYPE", this.WebserviceType);
                objSqlCommand.Parameters.AddWithValue("@SOAP_REQUEST", this.SoapRequest);
                objSqlCommand.Parameters.AddWithValue("@HTTP_REQUEST", this.HttpRequest);
                objSqlCommand.Parameters.AddWithValue("@OPERATION_ACTION", this.OperationAction);
                objSqlCommand.Parameters.AddWithValue("@HTTP_TYPE", this.HttpType);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
                objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", this.Description);
                objSqlCommand.Parameters.AddWithValue("@CACHE", this.Cache);
                objSqlCommand.Parameters.AddWithValue("@EXPIRY_FREQUENCY", this.Expfrequency);
                objSqlCommand.Parameters.AddWithValue("@EXPIRY_CONDITION", this.ExpCondtion);
                objSqlCommand.Parameters.AddWithValue("@X_REFERENCES", this.Xreference);
                objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.CreatedBy);
                objSqlCommand.Parameters.AddWithValue("@CREATED_ON", this.CreatedOn);
                objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", this.UpdatedOn);
                objSqlCommand.Parameters.AddWithValue("@HEADERS", this.Headers);
           
                if (objSqlCommand.ExecuteNonQuery() > 0)
                    this.StatusCode = 0;
                else
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());

                objSqlTransaction.Commit();
            }
            catch 
            {
                this.StatusCode = -1000;
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }

        public void CopyWsObjectProcess()
        {

            try
            {
                this.StatusCode = -1000;
              
                this.CopyCommandId = Utilities.GetMd5Hash(this.SubAdminId + this.CommandName + DateTime.UtcNow.Ticks.ToString());
                string query = @"INSERT INTO dbo.TBL_WS_COMMAND (COMPANY_ID,WS_COMMAND_ID,WS_CONNECTOR_ID,SUBADMIN_ID,URL,PARAMETER,RETURN_TYPE,RECORD_PATH,DATASET_PATH,TAG,WS_COMMAND_NAME,SERVICE,PORT_TYPE,METHOD,WEBSERVICE_TYPE,SOAP_REQUEST,HTTP_REQUEST,OPERATION_ACTION,HTTP_TYPE,DESCRIPTION,CACHE,EXPIRY_FREQUENCY,EXPIRY_CONDITION,X_REFERENCES,CREATED_BY,CREATED_ON,UPDATED_ON,HEADERS)
                select COMPANY_ID,@COPYDB_COMMAND_ID,WS_CONNECTOR_ID,SUBADMIN_ID,URL,PARAMETER,RETURN_TYPE,RECORD_PATH,DATASET_PATH,TAG,@WS_COMMAND_NAME,SERVICE,PORT_TYPE,METHOD,WEBSERVICE_TYPE,SOAP_REQUEST,HTTP_REQUEST,OPERATION_ACTION,HTTP_TYPE,DESCRIPTION,CACHE,EXPIRY_FREQUENCY,EXPIRY_CONDITION,X_REFERENCES,CREATED_BY,@CREATED_ON,@CREATED_ON,HEADERS from TBL_WS_COMMAND  where WS_COMMAND_ID=@WS_COMMAND_ID";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WS_COMMAND_ID", this.CommandId);
                objSqlCommand.Parameters.AddWithValue("@COPYDB_COMMAND_ID", this.CopyCommandId);
                objSqlCommand.Parameters.AddWithValue("@WS_COMMAND_NAME", this.CommandName);
                objSqlCommand.Parameters.AddWithValue("@CREATED_ON", this.UpdatedOn);

                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                    this.StatusCode = 0;
                else
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }



        }
        public string CopyCommandId
        {
            set;
            get;
        }
        public string SubAdminId { get; set; }

        public string CommandName { get; set; }

        public string ReturnType { get; set; }

        public string Parameter { get; set; }

        public string ConnactorId { get; set; }

        public string CommandId { get; set; }

        public int StatusCode { get; set; }

        public string Url { get; set; }

        public string Tag { get; set; }

        public string DataSetPath { get; set; }

        public string RecordPath { get; set; }

        public string Service { get; set; }

        public string PortType { get; set; }

        public string Method { get; set; }

        public string WebserviceType { get; set; }


        public string SoapRequest { get; set; }

        public string OperationAction { get; set; }

        public string XmlRpc { get; set; }

        public Int32 HttpType { get; set; }

        public string HttpRequest { get; set; }//Company_id

        public string Company_id { get; set; }

        public string Description { get; set; }

        public int Cache { get; set; }

        public int Expfrequency { get; set; }

        public string ExpCondtion { get; set; }

        public string CreatedBy { get; set; }
        public long CreatedOn { get; set; }
        public long UpdatedOn { get; set; }

        //public string Httpdatatemplate { get; set; }

        public string Xreference { get; set; }

        public string Headers { get; set; }

    }
}
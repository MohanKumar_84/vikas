﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetPushMessageDtls
    {
        string _databaseType, _connectionName, _hostName, _dbName;
        string _userId, _password, _tableName,_mpluginAgntName;

        public GetPushMessageDtls(string companyId)
        {
            this.CompanyId = companyId;
        }
        public void Process()
        {
            try
            {
                DataSet objDataSet;
                SqlCommand objSqlCommand;

                string query = @"SELECT  * FROM  TBL_PULL_DATA_SETTINGS WHERE COMPANY_ID = @CompanyId;";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    if (objDataSet.Tables[0].Rows.Count > 0)
                    {
                        this.DatabaseType = Convert.ToString(this.ResultTable.Rows[0]["DATABASE_TYPE"]);
                        this.ConnectionName = Convert.ToString(this.ResultTable.Rows[0]["CONNECTION_NAME"]);
                        this.HostName = Convert.ToString(this.ResultTable.Rows[0]["HOST_NAME"]);
                        this.DbName =
                            AesEncryption.AESDecrypt(this.CompanyId,
                           Convert.ToString(
                           this.ResultTable.Rows[0]["DATABASE_NAME"]
                           )
                           );
                            
                        
                        this.UserId =
                           AesEncryption.AESDecrypt(this.CompanyId,
                           Convert.ToString(this.ResultTable.Rows[0]["USER_NAME"]
                           )
                           );
                        
                        this.Password =
                            AesEncryption.AESDecrypt(this.CompanyId,
                            Convert.ToString(
                            this.ResultTable.Rows[0]["PASSWORD"]
                            )
                            );
                        
                        this.TableName = Convert.ToString(this.ResultTable.Rows[0]["TABLE_NAME"]);
                        this.MpluginAgntName = Convert.ToString(this.ResultTable.Rows[0]["MPLUGIN_AGENT"]);
                    }
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }

            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        public string CompanyId
        {
           private set;
            get;
        }
        public int StatusCode
        {
            private set;
            get;
        }
        public string StatusDescription
        {
            private set;
            get;
        }
        public DataTable ResultTable
        {
            private set;
            get;
        }
        public string TableName
        {
            get { return _tableName; }
            private set { _tableName = value; }
        }

        public string Password
        {
            get { return _password; }
            private set { _password = value; }
        }
        public string UserId
        {
            get { return _userId; }
            private set { _userId = value; }
        }
        public string DbName
        {
            get { return _dbName; }
            private set { _dbName = value; }
        }
        public string HostName
        {
            get { return _hostName; }
            private set { _hostName = value; }
        }
        public string ConnectionName
        {
            get { return _connectionName; }
            private set { _connectionName = value; }
        }
        public string DatabaseType
        {
            get { return _databaseType; }
            private set { _databaseType = value; }
        }
        public string MpluginAgntName
        {
            get { return _mpluginAgntName; }
            private set { _mpluginAgntName = value; }
        }
    }
}
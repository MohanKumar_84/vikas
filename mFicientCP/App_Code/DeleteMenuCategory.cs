﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class DeleteMenuCategory
    {
        public DeleteMenuCategory(string companyId, string menuCategoryId, string subadminId, string menucategoryname)
        {
            this.CompanyId = companyId;
            this.MenuCategoryId = menuCategoryId;
            this.SubadminId = subadminId;
            this.MenuCategoryName = menucategoryname;
        }

        public void Process()
        {
            StatusCode = -1000;
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                GetMenuCategoryDtlsByMenuCatId objGetMenuCategorydtl = new GetMenuCategoryDtlsByMenuCatId(this.CompanyId, this.MenuCategoryId);
                objGetMenuCategorydtl.Process();
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        deleteMenuCategory(transaction, con);
                        deleteMenuCategoryAndGroupLink(transaction, con);

                        transaction.Commit();
                        Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.CATEGORY_REMOVED, this.CompanyId, this.SubadminId, this.MenuCategoryId, this.MenuCategoryName, "", "", "", "", "", "");

                        StatusCode = 0;
                        StatusDescription = "";
                    }
                }
                if (objGetMenuCategorydtl.StatusCode == 0)
                {
                    AddIntoUpdationRequiredForMenu obj_up = new AddIntoUpdationRequiredForMenu();
                    obj_up.CategoryUpdationForSelectedGroup(CompanyId, objGetMenuCategorydtl.ResultTables);
                }
            }
            catch (Exception e)
            {
                StatusCode = -1000;
                if (transaction != null) transaction.Rollback();
                //if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                //{
                //    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                //}
                //throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }
        void deleteMenuCategoryAndGroupLink(SqlTransaction transaction, SqlConnection connection)
        {
            string strQuery = @"DELETE FROM TBL_MENU_AND_GROUP_LINK
                                WHERE MENU_CATEGORY_ID = @MenuCategoryId AND COMPANY_ID=@CompanyId;";
            SqlCommand cmd = new SqlCommand(strQuery, connection, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@MenuCategoryId", MenuCategoryId);
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            cmd.ExecuteNonQuery();
        }

        void deleteMenuCategory(SqlTransaction transaction, SqlConnection connection)
        {
            string strQuery = @"DELETE FROM TBL_MENU_CATEGORY
                                WHERE MENU_CATEGORY_ID = @MenuCategoryId
                                AND COMPANY_ID = @CompanyId;";
            SqlCommand cmd = new SqlCommand(strQuery, connection, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@MenuCategoryId", MenuCategoryId);
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId);
            cmd.ExecuteNonQuery();
        }
        public string CompanyId
        {
            set;
            get;
        }

        public string SubadminId
        {
            set;
            get;
        }
        public string MenuCategoryId
        {
            set;
            get;
        }
        public string MenuCategoryName
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
    }
}
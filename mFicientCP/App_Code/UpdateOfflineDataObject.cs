﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class UpdateOfflineDataObject
    {
        private string id, companyId, subadminId, description, query, inputParams, outputParams, tablesUsed, errorMessage;
        private bool allowRetry, exitOnError;

        public UpdateOfflineDataObject(string _id, string _companyId, string _subadminId, string _description, string _query, string _inputParams, string _outputParams, string _tablesUsed, bool _allowRetry, bool _exitOnError, string _errorMessage)
        {
            id = _id;
            companyId = _companyId;
            subadminId = _subadminId;
            description = _description;
            query = _query;
            inputParams = _inputParams;
            outputParams = _outputParams;
            tablesUsed = _tablesUsed;
            allowRetry = _allowRetry;
            exitOnError = _exitOnError;
            errorMessage = _errorMessage;

            try
            {
                process();
                StatusCode = 0;
            }
            catch (Exception ex)
            {
                StatusCode = -1;
            }
        }

        public int StatusCode
        {
            get;
            set;
        }

        private void process()
        {
            try
            {
                string strQuery = @"UPDATE TBL_OFFLINE_OBJECTS SET
                                    DESCRIPTION = @DESCRIPTION,
                                    INPUT_PARAMETERS = @INPUT_PARAMETERS, QUERY = @QUERY,
                                    OUTPUTS = @OUTPUTS, OFFLINE_TABLES = @OFFLINE_TABLES,
                                    UPDATED_ON = @UPDATED_ON, SUBADMIN_ID = @SUBADMIN_ID,
                                    ERROR_MESSAGE = @ERROR_MESSAGE, ALLOW_RETRY = @ALLOW_RETRY,
                                    EXIT_ON_ERROR = @EXIT_ON_ERROR
                                    WHERE OFFLINE_OBJECT_ID = @OFFLINE_OBJECT_ID AND COMPANY_ID = @COMPANY_ID";

                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@OFFLINE_OBJECT_ID", id);
                cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
                cmd.Parameters.AddWithValue("@SUBADMIN_ID", subadminId);
                cmd.Parameters.AddWithValue("@DESCRIPTION", description);
                cmd.Parameters.AddWithValue("@QUERY", query);
                cmd.Parameters.AddWithValue("@INPUT_PARAMETERS", inputParams);
                cmd.Parameters.AddWithValue("@OUTPUTS", outputParams);
                cmd.Parameters.AddWithValue("@OFFLINE_TABLES", tablesUsed);
                cmd.Parameters.AddWithValue("@UPDATED_ON", DateTime.UtcNow.Ticks);
                cmd.Parameters.AddWithValue("@ERROR_MESSAGE", errorMessage);
                cmd.Parameters.AddWithValue("@ALLOW_RETRY", allowRetry);
                cmd.Parameters.AddWithValue("@EXIT_ON_ERROR", exitOnError);
                int result = MSSqlClient.ExecuteNonQueryRecord(cmd);
                if (result <= 0)
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
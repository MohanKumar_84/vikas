﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCP
{
    public class AddIntoUpdationRequiredForMenu
    {
       
        public Boolean CategoryUpdationForAllGroup(string CompanyId)
        {
            try
            {
                string query = @"delete from  TBL_UPDATION_REQUIRED_DETAIL  where user_id in (select user_id from tbl_user_detail where company_id='" + CompanyId + @"');
                INSERT INTO [TBL_UPDATION_REQUIRED_DETAIL] ([COMPANY_ID],[USER_ID],[DEVICE_ID],[DEVICE_TYPE],[UPDATED_ON],[UPDATION_TYPE]) 
                select r.company_id,r.user_id,r.device_id,r.device_type," + DateTime.UtcNow.Ticks + @"," + (int)UPDATE_TYPE.MENU_CATEGORY_UPDATE + @" from
                TBL_REGISTERED_DEVICE as r inner join tbl_user_detail as u on r.user_id=u.user_id where r.company_id='" + CompanyId + "'";
                SqlCommand cmd = new SqlCommand(query);
                cmd.CommandType = CommandType.Text;
                MSSqlClient.ExecuteNonQueryRecord(cmd);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public int CategoryUpdationForSelectedGroup(string CompanyId,  DataTable PreviousMenuDtl)
        {
            int val = -1;
            try
            {
                if (PreviousMenuDtl.Rows.Count > 0)
                {
                    CategoryUpdationForAllGroup(CompanyId);
                    val = 1;
                }
            }
            catch
            {
                // return val;
            }
            return val;
                
        }
        public Boolean AppFlowUpdation(string WF_id,string company_id, DataSet dtWF_Previous_detail)
        {
            try
            {
                bool previousWFForAllGroup = false, previousCategoryForAllGroup = false; string previousCategoryId = "";
                bool CurrentWFForAllGroup = false, CurrentCategoryForAllGroup = false; string CurrentCategoryId = "";
                DataTable dtPreviouWFGroups = new DataTable();
                DataTable dtPreviouCatGroups = new DataTable();
                DataTable dtCurrentWFGroups = new DataTable();
                DataTable dtCurrentCatGroups = new DataTable();
                if (dtWF_Previous_detail != null && dtWF_Previous_detail.Tables[0].Rows.Count > 0)
                {
                    previousWFForAllGroup = Convert.ToBoolean(dtWF_Previous_detail.Tables[0].Rows[0]["WORKFLOW_ALL_GROUPS"]);
                    previousCategoryId = Convert.ToString(dtWF_Previous_detail.Tables[0].Rows[0]["CATEGORY_ID"]);
                    previousCategoryForAllGroup = Convert.ToBoolean(dtWF_Previous_detail.Tables[0].Rows[0]["CATEGORY_ALL_GROUPS"]);
                    dtPreviouWFGroups=dtWF_Previous_detail.Tables[1];
                    dtPreviouCatGroups=dtWF_Previous_detail.Tables[2];
                }

                string query = @"select c.ALL_GROUPS as CATEGORY_ALL_GROUPS,w.CATEGORY_ID,wfad.ALL_GROUPS as WORKFLOW_ALL_GROUPS,cd.company_id from TBL_WORKFLOW_AND_CATEGORY_LINK as w 
                inner join TBL_MENU_CATEGORY as c on w.CATEGORY_ID=c.MENU_CATEGORY_ID inner join TBL_CURR_WORKFLOW_DETAIL as wd on wd.wf_ID=w.WORKFLOW_ID
                inner join dbo.TBL_SUB_ADMIN as sa on sa.subadmin_id=wd.Subadmin_id inner join tbl_company_detail as cd on cd.admin_id=sa.admin_id
                inner JOIN TBL_WORKFLOW_ADDITIONAL_DETAIL wfad ON wfad.WORKFLOW_ID = wd.WF_ID where w.WORKFLOW_ID=@WORKFLOW_ID;
                Select Group_ID from TBL_WORKFLOW_AND_GROUP_LINK where WORKFLOW_ID=@WORKFLOW_ID and COMPANY_ID=@COMPANY_ID;
                Select Group_ID from TBL_MENU_AND_GROUP_LINK as ml inner join TBL_WORKFLOW_AND_CATEGORY_LINK as wc on wc.CATEGORY_ID=ml.MENU_CATEGORY_ID where wc.WORKFLOW_ID=@WORKFLOW_ID and wc.COMPANY_ID=@COMPANY_ID;";

                SqlCommand _SqlCommand = new SqlCommand(query);
                _SqlCommand.CommandType = CommandType.Text;
                _SqlCommand.Parameters.AddWithValue("@WORKFLOW_ID", WF_id);
                _SqlCommand.Parameters.AddWithValue("@COMPANY_ID", company_id);
                DataSet dsCurrentWF = MSSqlClient.SelectDataFromSQlCommand(_SqlCommand);

                if (dsCurrentWF != null && dsCurrentWF.Tables[0].Rows.Count > 0)
                {
                    CurrentWFForAllGroup = Convert.ToBoolean(dsCurrentWF.Tables[0].Rows[0]["WORKFLOW_ALL_GROUPS"]);
                    CurrentCategoryId = Convert.ToString(dsCurrentWF.Tables[0].Rows[0]["CATEGORY_ID"]);
                    CurrentCategoryForAllGroup = Convert.ToBoolean(dsCurrentWF.Tables[0].Rows[0]["CATEGORY_ALL_GROUPS"]);
                    dtCurrentWFGroups = dsCurrentWF.Tables[1];
                    dtCurrentCatGroups = dsCurrentWF.Tables[2];

                    if ((CurrentCategoryForAllGroup || previousWFForAllGroup) && (CurrentWFForAllGroup || previousWFForAllGroup))
                    {
                        query = @" delete from TBL_UPDATION_REQUIRED_DETAIL where USER_ID in (select distinct u.USER_ID from tbl_user_detail as u inner join TBL_USER_GROUP_LINK as gl on gl.user_id=u.user_id inner join TBL_MENU_AND_GROUP_LINK as ml on ml.GROUP_ID=gl.GROUP_ID  where u.company_id=@COMPANY_ID) ;
                                INSERT INTO [TBL_UPDATION_REQUIRED_DETAIL]([COMPANY_ID],[USER_ID],[DEVICE_ID],[DEVICE_TYPE],[UPDATED_ON],[UPDATION_TYPE]) 
                                (select r.company_id,r.user_id,r.device_id,r.device_type,@UPDATED_ON,@UPDATION_TYPE from
                                TBL_REGISTERED_DEVICE as r inner join tbl_user_detail as u on r.user_id=u.user_id where r.company_id=@COMPANY_ID)";
                        _SqlCommand = new SqlCommand(query);
                        _SqlCommand.CommandType = CommandType.Text;
                        _SqlCommand.Parameters.AddWithValue("@COMPANY_ID", company_id);
                        _SqlCommand.Parameters.AddWithValue("@UPDATED_ON", DateTime.UtcNow.Ticks);
                        _SqlCommand.Parameters.AddWithValue("@UPDATION_TYPE", (int)UPDATE_TYPE.MENU_CATEGORY_UPDATE);
                        MSSqlClient.ExecuteNonQueryRecord(_SqlCommand);
                    }
                    else
                    {
                        string group = "";
                        if (previousCategoryId != CurrentCategoryId && !CurrentCategoryForAllGroup && !previousWFForAllGroup)
                        {
                            foreach (DataRow dr in dtPreviouCatGroups.Rows)
                                if (!group.Contains(dr["Group_ID"].ToString()))
                                    group += ",'" + dr["Group_ID"].ToString() + "'";
                            foreach (DataRow dr in dtCurrentCatGroups.Rows)
                                if (!group.Contains(dr["Group_ID"].ToString()))
                                    group += ",'" + dr["Group_ID"].ToString() + "'";

                        }

                        if (!CurrentWFForAllGroup && !previousWFForAllGroup)
                        {
                            foreach (DataRow dr in dtPreviouWFGroups.Rows)
                                if (!group.Contains(dr["Group_ID"].ToString()))
                                    group += ",'" + dr["Group_ID"].ToString() + "'";
                            foreach (DataRow dr in dtCurrentWFGroups.Rows)
                                if (!group.Contains(dr["Group_ID"].ToString()))
                                    group += ",'" + dr["Group_ID"].ToString() + "'";
                        }
                        if (!string.IsNullOrEmpty(group))
                        {
                            group = group.Substring(1);
                            query = @"delete from TBL_UPDATION_REQUIRED_DETAIL where USER_ID in(select distinct u.user_id from tbl_user_detail as u inner join 
                                TBL_USER_GROUP_LINK as gl on gl.user_id=u.user_id where u.COMPANY_ID=@COMPANY_ID and gl.group_id in (" + group + @"));
                                INSERT INTO [TBL_UPDATION_REQUIRED_DETAIL]([COMPANY_ID],[USER_ID],[DEVICE_ID],[DEVICE_TYPE],[UPDATED_ON],[UPDATION_TYPE]) 
                                (select distinct r.company_id,r.user_id,r.device_id,r.device_type,@UPDATED_ON,@UPDATION_TYPE from TBL_REGISTERED_DEVICE as r
                                inner join tbl_user_detail as u on r.user_id=u.user_id inner join TBL_USER_GROUP_LINK as gl on gl.user_id=u.user_id 
                                where gl.group_id in (" + group + @"))";

                            _SqlCommand = new SqlCommand(query);
                            _SqlCommand.CommandType = CommandType.Text;
                            _SqlCommand.Parameters.AddWithValue("@COMPANY_ID", company_id);
                            _SqlCommand.Parameters.AddWithValue("@UPDATED_ON", DateTime.UtcNow.Ticks);
                            _SqlCommand.Parameters.AddWithValue("@UPDATION_TYPE", (int)UPDATE_TYPE.MENU_CATEGORY_UPDATE);
                            MSSqlClient.ExecuteNonQueryRecord(_SqlCommand);
                        }
                    }
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
        public DataSet GetPreviousDetailOfWF(string WF_id, string company_id)
        {
            string query = @" select c.ALL_GROUPS as CATEGORY_ALL_GROUPS,w.CATEGORY_ID,wfad.ALL_GROUPS as WORKFLOW_ALL_GROUPS,cd.company_id from TBL_WORKFLOW_AND_CATEGORY_LINK as w 
                inner join TBL_MENU_CATEGORY as c on w.CATEGORY_ID=c.MENU_CATEGORY_ID inner join TBL_CURR_WORKFLOW_DETAIL as wd on wd.wf_ID=w.WORKFLOW_ID
                inner join dbo.TBL_SUB_ADMIN as sa on sa.subadmin_id=wd.Subadmin_id inner join tbl_company_detail as cd on cd.admin_id=sa.admin_id 
                inner JOIN TBL_WORKFLOW_ADDITIONAL_DETAIL wfad ON wfad.WORKFLOW_ID = wd.WF_ID where w.WORKFLOW_ID=@WORKFLOW_ID;
                Select Group_ID from TBL_WORKFLOW_AND_GROUP_LINK where WORKFLOW_ID=@WORKFLOW_ID and COMPANY_ID=@COMPANY_ID;
                Select Group_ID from TBL_MENU_AND_GROUP_LINK as ml inner join TBL_WORKFLOW_AND_CATEGORY_LINK as wc on wc.CATEGORY_ID=ml.MENU_CATEGORY_ID where wc.WORKFLOW_ID=@WORKFLOW_ID and wc.COMPANY_ID=@COMPANY_ID;";
            SqlCommand _SqlCommand = new SqlCommand(query);
            _SqlCommand.CommandType = CommandType.Text;
            _SqlCommand.Parameters.AddWithValue("@WORKFLOW_ID", WF_id);
            _SqlCommand.Parameters.AddWithValue("@COMPANY_ID", company_id);
            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(_SqlCommand);
            if (ds != null)
                return ds;
            return null;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
namespace mFicientCP
{
    public class SavePushMessageInfoAndLogs
    {
        string _companyId, _statusDescription, _adminId, _message,
            _subject;
        List<string> _userIdsSelected;
       // List<string> _userIsSelected;


        int _statusCode, _userRole;
        long _msgSheduleTimeInTicks;
        int _expireAfterDays;
        int _noOfUsersToSendMsg;



        const int PUSH_MESSAGE_SOURCE_MFICIENT = 2;
        const int PUSH_MESSAGE_STATUS = 0;
        const int PUSH_MESSAGE_BADGE_COUNT = 1;


        public SavePushMessageInfoAndLogs(string companyId, string adminId
         , int userRole, string message,
         string subject,
         int expiryAfterDays,
         long msgSheduleTime,
            List<string> userIdsSelected)
        {
            _companyId = companyId;
            _adminId = adminId;
            _userRole = userRole;
            _message = message;
            _msgSheduleTimeInTicks = msgSheduleTime;
            _expireAfterDays = expiryAfterDays;
            _subject = subject;
            _userIdsSelected = userIdsSelected;
                          
        }

        public void Process()
        {
            SqlConnection con = null;
            SqlTransaction transaction = null;
            try
            {

                string strPushMessageId = Utilities.GetMd5Hash(_companyId + _adminId + DateTime.UtcNow.Ticks.ToString());
                List<mFicientCommonProcess.mGarmUserdetails> lstUserNamesToSendPushMsg = new List<mFicientCommonProcess.mGarmUserdetails>();
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        lstUserNamesToSendPushMsg = getUserListAfterProcessing(this.UserIdsSelected);
                        if (_userIdsSelected.Count > 0)
                        {
                            this.NoOfUsersToSendMsg = lstUserNamesToSendPushMsg.Count;
                            SaveMsgDtlsInMGramDb objSaveInMGramDB =
                                 new SaveMsgDtlsInMGramDb(this.CompanyId, this.Subject,
                                     this.Message, this.MsgShedulingTime,
                                     this.ExpirationAfter, lstUserNamesToSendPushMsg);
                            objSaveInMGramDB.Process();
                            if (objSaveInMGramDB.StatusCode != 0) throw new Exception();
                            // }

                            transaction.Commit();
                            _statusDescription = "";
                            _statusCode = 0;
                        }
                    }
                    _statusCode = 0;
                    _statusDescription = "";

                }
            }
            catch
            {
                _statusCode = -1000;
                _statusDescription = "Internal server error";
            }
            finally
            {
                if (con != null) con.Dispose();
                if (transaction != null) transaction.Dispose();
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Exception">Thrown when there is some internal error</exception>
        DataSet getRegisterDeviceListOfCompany()
        {
            string strQuery = @"Select d.*,u.user_name from TBL_REGISTERED_DEVICE as  d  inner join tbl_user_detail as u on u.user_id=d.user_id and u.company_id=d.company_id where d.company_id=@CompanyId;";

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", _companyId);
            return MSSqlClient.SelectDataFromSQlCommand(cmd);
        }



        List<mFicientCommonProcess.mGarmUserdetails> getUserListAfterProcessing(List<string> lstuser)
        {
            DataSet dtblAllUserDetl = getRegisterDeviceListOfCompany();
            List<mFicientCommonProcess.mGarmUserdetails> UsersToSendPushMsg = new List<mFicientCommonProcess.mGarmUserdetails>();
            foreach (string commaSeparatedLocDept in lstuser)
            {
                List<List<string>> devs = new List<List<string>>();
                DataRow[] dr = dtblAllUserDetl.Tables[0].Select("USER_ID='" + commaSeparatedLocDept + "'");
                string username = "";
                foreach (DataRow deviceid in dr)
                {
                    List<string> dev = new List<string>();
                    dev.Add(Convert.ToString(deviceid["DEVICE_PUSH_MESSAGE_ID"]));
                    dev.Add(Convert.ToString(deviceid["DEVICE_TYPE"]));
                    dev.Add(Convert.ToString(deviceid["DEVICE_ID"]));
                    username = Convert.ToString(deviceid["USER_NAME"]);
                    devs.Add(dev);
                }
                if (!string.IsNullOrEmpty(username))
                    UsersToSendPushMsg.Add(new mFicientCommonProcess.mGarmUserdetails(username, this.CompanyId, "", devs));
            }
            return UsersToSendPushMsg;
        }

        public string CompanyId
        {
            get { return _companyId; }
        }
        
        public string StatusDescription
        {
            get { return _statusDescription; }
        }


        public int StatusCode
        {
            get { return _statusCode; }
        }
        public string AdminId
        {
            get { return _adminId; }
        }
        public string Message
        {
            get { return _message; }
        }
        public int UserRole
        {
            get { return _userRole; }
        }
        public long MsgShedulingTime
        {
            get { return _msgSheduleTimeInTicks; }
        }
        public string Subject
        {
            get { return _subject; }
            private set { _subject = value; }
        }
        public int ExpirationAfter
        {
            get { return _expireAfterDays; }
            private set { _expireAfterDays = value; }
        }
        /// <summary>
        /// For the location and department selected
        /// whehter there were any users saved.
        /// </summary>
        public int NoOfUsersToSendMsg
        {
            get { return _noOfUsersToSendMsg; }
            private set { _noOfUsersToSendMsg = value; }
        }
        public List<string> UserIdsSelected
        {
            get { return _userIdsSelected; }
            private set { _userIdsSelected = value; }
        }
    }
}
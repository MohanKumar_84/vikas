﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class UpdatedAdminDetails
    {
        public UpdatedAdminDetails()
        {

        }
        public UpdatedAdminDetails(string _firstname, string _middlename, string _lastname, string _email, string _mobileno, string _companyId, string _adminId)
        {
            this.Firstname = _firstname;
            this.MiddleName = _middlename;
            this.LastName = _lastname;
            this.Emailid = _email;
            this.Mobile = _mobileno;
            this.AdminId = _adminId;
            this.CompanyId = _companyId;
            Process();

        }
        public void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query = @"UPDATE dbo.TBL_COMPANY_ADMINISTRATOR SET 
                                            FIRST_NAME=@FIRST_NAME,
                                            MIDDLE_NAME=@MIDDLE_NAME,
                                           LAST_NAME=@LAST_NAME,
                                           MOBILE=@MOBILE,
                                           EMAIL_ID=@EMAIL_ID
                                       WHERE COMPANY_ID=@COMPANY_ID AND ADMIN_ID=ADMIN_ID";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@FIRST_NAME", this.Firstname);
                objSqlCommand.Parameters.AddWithValue("@MIDDLE_NAME", this.MiddleName);
                objSqlCommand.Parameters.AddWithValue("@LAST_NAME", this.LastName);
                objSqlCommand.Parameters.AddWithValue("@MOBILE", this.Mobile);
                objSqlCommand.Parameters.AddWithValue("@EMAIL_ID", this.Emailid);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                {
                  
                    StatusCode = 0;
                }
                else
                {
                    StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }


            }
            catch 
            {
                this.StatusCode = -1000;
            }
        }
        public string Firstname { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string CompanyId { get; set; }
        public string Mobile { get; set; }
        public string Emailid { get; set; }
        public string AdminId { get; set; }
        public int StatusCode { get; set; }
    }
     
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
namespace mFicientCP
{
    public class CommitHtml5App
    {

        public CommitHtml5App(string subadminId, string companyId)
        {
            this.SubadminId = subadminId;
            this.CompanyId = companyId;
        }
        public void Commit(MFEHtml5AppPublishDetail appPublishDtl, bool deleteOldestVersion, string oldestVersionVal)
        {
            this.StatusCode = -1000;
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            SqlCommand objSqlCommand;
            string query;
            try
            {
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                objSqlTransaction = objSqlConnection.BeginTransaction();
                query = @"INSERT INTO TBL_HTML5_APP_PRV_WF_DETAIL(COMPANY_ID,APP_ID,[APP_NAME],
                          APP_DESCRIPTION,APP_JSON,[VERSION],WORKED_BY,PUBLISHED_ON
                          ,SUBADMIN_ID,COMMITTED_ON,VERSION_HISTORY,ICON,
                          BUCKET_FILE_PATH)
                          VALUES
                          (@COMPANY_ID,@APP_ID,@APP_NAME,
                          @APP_DESCRIPTION,@APP_JSON,@VERSION,@WORKED_BY,@PUBLISHED_ON
                          ,@SUBADMIN_ID,@COMMITTED_ON,@VERSION_HISTORY,@ICON,
                          @BUCKET_FILE_PATH);";
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;

                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", appPublishDtl.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@APP_ID", appPublishDtl.AppId);
                objSqlCommand.Parameters.AddWithValue("@APP_NAME", appPublishDtl.AppName);
                objSqlCommand.Parameters.AddWithValue("@APP_DESCRIPTION", appPublishDtl.Description);
                objSqlCommand.Parameters.AddWithValue("@APP_JSON", appPublishDtl.AppJson);
                objSqlCommand.Parameters.AddWithValue("@VERSION", appPublishDtl.Version);
                objSqlCommand.Parameters.AddWithValue("@WORKED_BY", appPublishDtl.WorkedBy);
                objSqlCommand.Parameters.AddWithValue("@PUBLISHED_ON", appPublishDtl.PublishedOn);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", appPublishDtl.SubadminId);
                objSqlCommand.Parameters.AddWithValue("@COMMITTED_ON", appPublishDtl.CommittedOn);
                objSqlCommand.Parameters.AddWithValue("@VERSION_HISTORY", appPublishDtl.VersionHistory);
                objSqlCommand.Parameters.AddWithValue("@ICON", appPublishDtl.Icon);
                objSqlCommand.Parameters.AddWithValue("@BUCKET_FILE_PATH", appPublishDtl.BucketFilePath);
                int iRowsAffected = objSqlCommand.ExecuteNonQuery();
                if (iRowsAffected == 0)
                {
                    throw new Exception("Record Insert Error.");
                }



                if (deleteOldestVersion)
                {
                    query = @"DELETE FROM TBL_HTML5_APP_PRV_WF_DETAIL
                            WHERE APP_ID=@APP_ID AND SUBADMIN_ID=@SUBADMIN_ID AND VERSION=@VERSION AND COMPANY_ID=@COMPANY_ID;";

                    objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@APP_ID", appPublishDtl.AppId);
                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", appPublishDtl.SubadminId);
                    objSqlCommand.Parameters.AddWithValue("@VERSION", oldestVersionVal);
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", appPublishDtl.CompanyId);
                    objSqlCommand.ExecuteNonQuery();
                }
                objSqlTransaction.Commit();
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch
            {
                this.StatusCode = -1000;
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
            }
            finally
            {
                if (objSqlTransaction != null)
                    objSqlTransaction.Dispose();
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }

        public void CommitAppForTester(MFEHtml5AppPublishDetail appPublishDtl)
        {

            this.StatusCode = -1000;
            string strAppObj = string.Empty;
            string query;
            try
            {

                query = @"IF EXISTS (Select 'true' from TBL_HTML5_APP_FOR_TESTER where  COMPANY_ID=@COMPANY_ID AND APP_ID=@APP_ID)
                    
                          UPDATE [TBL_HTML5_APP_FOR_TESTER]
                           SET [COMPANY_ID] = @COMPANY_ID
                              ,[APP_ID] = @APP_ID
                              ,[APP_NAME] = @APP_NAME
                              ,[APP_DESCRIPTION] = @APP_DESCRIPTION
                              ,[APP_JSON] = @APP_JSON
                              ,[VERSION] = @VERSION
                              ,[WORKED_BY] = @WORKED_BY
                              ,[PUBLISHED_ON] = @PUBLISHED_ON
                              ,[SUBADMIN_ID] = @SUBADMIN_ID
                              ,[COMMITTED_ON] = @COMMITTED_ON
                              ,[VERSION_HISTORY] = @VERSION_HISTORY
                              ,[ICON] = @ICON
                              ,[BUCKET_FILE_PATH] = @BUCKET_FILE_PATH
                         WHERE COMPANY_ID = @COMPANY_ID
		                        AND APP_ID = @APP_ID

                    else INSERT INTO [TBL_HTML5_APP_FOR_TESTER]
                               ([COMPANY_ID],[APP_ID],[APP_NAME]
                               ,[APP_DESCRIPTION],[APP_JSON],[VERSION]
                               ,[WORKED_BY],[PUBLISHED_ON],[SUBADMIN_ID]
                               ,[COMMITTED_ON],[VERSION_HISTORY],[ICON]
                               ,[BUCKET_FILE_PATH])
                         VALUES
                               (@COMPANY_ID,@APP_ID,@APP_NAME
                               ,@APP_DESCRIPTION,@APP_JSON,@VERSION
                               ,@WORKED_BY ,@PUBLISHED_ON,@SUBADMIN_ID
                               ,@COMMITTED_ON,@VERSION_HISTORY,@ICON
                               ,@BUCKET_FILE_PATH);";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", appPublishDtl.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@APP_ID", appPublishDtl.AppId);
                objSqlCommand.Parameters.AddWithValue("@APP_NAME", appPublishDtl.AppName);
                objSqlCommand.Parameters.AddWithValue("@APP_DESCRIPTION", appPublishDtl.Description);
                objSqlCommand.Parameters.AddWithValue("@APP_JSON", appPublishDtl.AppJson);
                objSqlCommand.Parameters.AddWithValue("@VERSION", appPublishDtl.Version);
                objSqlCommand.Parameters.AddWithValue("@WORKED_BY", appPublishDtl.WorkedBy);
                objSqlCommand.Parameters.AddWithValue("@PUBLISHED_ON", appPublishDtl.PublishedOn);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", appPublishDtl.SubadminId);
                objSqlCommand.Parameters.AddWithValue("@COMMITTED_ON", appPublishDtl.CommittedOn);
                objSqlCommand.Parameters.AddWithValue("@VERSION_HISTORY", appPublishDtl.VersionHistory);
                objSqlCommand.Parameters.AddWithValue("@ICON", appPublishDtl.Icon);
                objSqlCommand.Parameters.AddWithValue("@BUCKET_FILE_PATH", appPublishDtl.BucketFilePath);
                MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            finally
            {
            }
        }


        public int StatusCode
        {
            private set;
            get;
        }

        public string StatusDescription
        {
            get;
            private set;
        }

        public string SubadminId { get; private set; }

        public string CompanyId { get; private set; }
    }
}
﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.IO;
//using System.Text;
//using System.Data;
//using System.Runtime.Serialization;
//using System.Runtime.Serialization.Json;
//using System.Web.UI;
//using System.Xml;
//using System.Text.RegularExpressions;

//namespace mFicientCP
//{
//    public class GetFormHtml
//    {
//        #region Constructor
//        public GetFormHtml(string _formJson, verfile objfiles, bool _isChild, DataTable _drDataobjects)
//        {
//            formJson = _formJson;
//            isChild = _isChild;
//            drDataObjects = _drDataobjects;
//            initialize();
//            formJsontoHtml(objfiles, "");
//        }

//        public GetFormHtml(string _formJson, bool _isChild, verfile objfiles, string _view_id, DataTable _drDataObjects)
//        {
//            formJson = _formJson;
//            isChild = _isChild;
//            drDataObjects = _drDataObjects;
//            initialize();
//            formJsontoHeadAndBody(_view_id, objfiles);
//        }

//        #endregion
//        #region Enumerations

//        protected enum ControlEvents
//        {
//            ON_BLUR = 1,
//            ON_CLICK = 2,
//            ON_CHANGE = 3,
//        }

//        protected enum JqueryValidationType
//        {
//            Email = 1,
//            Integer = 2,
//            Decimal = 3,
//            AlphaNumeric = 4,
//            Alphabetical = 5,
//            CustumRegx = 6,
//            IntegerLessThan = 7,
//            IntegerGreaterThan = 8,
//            IntegerInRange = 9,
//            DecimalLessThan = 10,
//            DecimalGreaterThan = 11,
//            DecimalInRange = 12,
//            AlphabeticalLengthLessThan = 13,
//            AlphabeticalLengthGreaterThan = 14,
//            AlphabeticalLengthInRange = 15,
//            AlphaNumericLengthLessThan = 16,
//            AlphaNumericLengthGreaterThan = 17,
//            AlphaNumericLengthInRange = 18,
//            IntegerDivisible = 19,
//            DecimalDivisible = 20
//        }

//        protected enum DataBindType
//        {
//            Manual = 0,
//            Database = 1,
//            webservice = 2,
//        }

//        public enum Code
//        {
//            ZERO = 0,
//            ONE = 1,
//            TWO = 2,
//            THREE = 3
//        }

//        private enum CallNativeFunctionCodes
//        {
//            CALL_NATIVE_HTML_READY = 1,
//            CALL_NATIVE_DO_TASK_RUN = 2,
//            CALL_NATIVE_SCAN_BARCODE = 3,
//            CALL_NATIVE_SHOW_LOCATION_IN_MAP = 4,
//            CALL_NATIVE_LIST_ROW_CLICKED = 5,
//            CALL_NATIVE_HYPERLINK_CLICKED = 6,
//            CALL_NATIVE_FOR_RESPONSE = 7,
//            CALL_NATIVE_VIEW_IN_EDITABLE_LIST = 8,
//            CALL_NATIVE_EDIT_IN_EDITABLE_LIST = 9,
//            CALL_NATIVE_DELETE_IN_EDITABLE_LIST = 10,
//            CALL_NATIVE_LIST_PAGE_BIND = 11,
//            CALL_NATIVE_FORCE_DO_TASK_RUN = 17,
//        }

//        private enum CallHtmlFromNativeFunctionCodes
//        {
//            CALL_FROM_NATIVE_SET_CONROL_VALUE = 101,
//            CALL_FROM_NATIVE_GET_CONROL_VALUE = 102,
//            CALL_FROM_NATIVE_BIND_DROPDOWN = 103,
//            CALL_FROM_NATIVE_GET_ALL_CONROL_VALUE = 104,
//            CALL_FROM_NATIVE_BIND_LIST = 105,
//            CALL_FROM_NATIVE_BIND_BAR = 106,
//            CALL_FROM_NATIVE_BIND_LINE = 107,
//            CALL_FROM_NATIVE_BIND_PIE = 108,
//            CALL_FROM_NATIVE_BIND_TABLE = 109,
//            CALL_FROM_NATIVE_SET_BAR_CODE = 110,
//            CALL_FROM_NATIVE_SET_LOCATION = 111,
//            CALL_FROM_NATIVE_BIND_EDITABLE_LIST = 112,
//            CALL_FROM_NATIVE_BIND_TEXT_CONTROL = 116,
//        }

//        #endregion
//        #region Private properties

//        private AppForm form;
//        private List<ConditionPlugin> adv;
//        private int LocExists = 0;
//        private bool IsjqPlotJSUsed = false, isChild;
//        private string formJson,
//            coditionalControls = string.Empty,
//            prevChkfield = string.Empty,
//            strValidationJquery = string.Empty,
//            setMapOnReady = string.Empty,
//            setControlOnLoadScript = string.Empty,
//            scriptForDateTimePckr = string.Empty,
//        strControlsEvents = string.Empty,
//        setEditableListItemOnReady = string.Empty,
//        setEditableChildbindingOnReady = string.Empty,
//        strTablePropertySet = string.Empty,
//        strOrentationChange = string.Empty,
//        ListActionButtonMethod = string.Empty,
//        strTablePropertyOnReady = string.Empty,
//        ControlsOnReady = string.Empty;
//        private DataTable dtCtrlsEvents, dtCtrlsdofuntionjson, drDataObjects;
//        List<string> _htmlImagesName = new List<string>();
//        string _HtmlHead, _HtmlBody, _HtmlFileLinks;
//        private void initialize()
//        {
//            form = new AppForm();
//            adv = new List<ConditionPlugin>();
//            dtCtrlsEvents = new DataTable();
//            dtCtrlsdofuntionjson = new DataTable();

//            dtCtrlsEvents.Columns.Add("CtrlId");
//            dtCtrlsEvents.Columns.Add("CtrlEventFunction");

//            dtCtrlsdofuntionjson.Columns.Add("CtrlId");
//            dtCtrlsdofuntionjson.Columns.Add("cmdjson");
//            dtCtrlsdofuntionjson.Columns.Add("parajson");

//            _returnHtml = string.Empty;
//            _HtmlHead = string.Empty;
//            _HtmlBody = string.Empty;
//        }
//        #endregion
//        #region Private methods Overall Html
//        private string formJsontoHtml(verfile objfiles, string _view_id)
//        {
//            string _returnHtml = "";
//            try
//            {
//                HtmlTextWriter writer;

//                if (formJson != string.Empty)
//                {
//                    formJsontoHeadAndBody(_view_id, objfiles);
//                    string strContentHtlm;
//                    HtmlInnerContentDiv(out strContentHtlm);

//                    StringWriter strOuterWriter = new StringWriter();
//                    using (writer = new HtmlTextWriter(strOuterWriter))
//                    {
//                        writer.Write(@"<!DOCTYPE html>" + "\r\n");
//                        writer.RenderBeginTag(HtmlTextWriterTag.Html);
//                        writer.RenderBeginTag(HtmlTextWriterTag.Head);
//                        writer.RenderBeginTag(HtmlTextWriterTag.Title);
//                        writer.RenderEndTag();//Title
//                        writer.Write(_HtmlFileLinks);
//                        writer.Write(_HtmlHead);
//                        writer.RenderEndTag();//Head

//                        writer.RenderBeginTag(HtmlTextWriterTag.Body);

//                        writer.Write(_HtmlBody);
//                        writer.RenderEndTag();//body
//                        writer.RenderEndTag();//Html

//                        _returnHtml = strOuterWriter.ToString();
//                    }
//                }
//            }
//            catch
//            {
//                _returnHtml = "";
//            }
//            return _returnHtml;
//        }

//        private HtmlFileParts formJsontoHeadAndBody(string _view_id, verfile verFileList)
//        {
//            HtmlFileParts objhtml = null;
//            try
//            {
//                if (formJson != string.Empty)
//                {
//                    string strContentHtlm;
//                    HtmlInnerContentDiv(out strContentHtlm);
//                    HtmlInnerHead(_view_id, verFileList);
//                    HtmlInnerBody(strContentHtlm);
//                    objhtml = new HtmlFileParts(_htmlImagesName, _HtmlHead, _HtmlBody, _HtmlFileLinks);
//                }
//            }
//            catch
//            {

//            }
//            return objhtml;
//        }

//        private void HtmlInnerContentDiv(out string strContentHtlm)
//        {
//            strContentHtlm = string.Empty;

//            MemoryStream ms1 = new MemoryStream(Encoding.Unicode.GetBytes(formJson));
//            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer1 = new System.Runtime.Serialization.Json.DataContractJsonSerializer(form.GetType());
//            form = serializer1.ReadObject(ms1) as AppForm;
//            ms1.Close();

//            HtmlTextWriter writer;
//            StringWriter stringWriter = new StringWriter();
//            using (writer = new HtmlTextWriter(stringWriter))
//            {
//                foreach (var grow in form.rowPanels)
//                {
//                    create grid
//                    writer.AddAttribute(HtmlTextWriterAttribute.Class, getHtmlForGridDiv(grow.type));
//                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
//                    foreach (var gcol in grow.colmnPanels)
//                    {
//                        create block
//                        writer.AddAttribute(HtmlTextWriterAttribute.Id, gcol.id);
//                        if (getDisplayType(gcol.conditionalDisplay))
//                        {
//                            coditionalControls += "'" + gcol.id + "',";
//                        }
//                        writer.AddAttribute(HtmlTextWriterAttribute.Class, getClassForBlocks(Convert.ToInt32(gcol.colType), grow.type));
//                        writer.RenderBeginTag(HtmlTextWriterTag.Div);

//                        foreach (var Gctrl in gcol.controls)
//                        {
//                            getHtml(Gctrl, gcol.id, writer);
//                        }
//                        if (!string.IsNullOrEmpty(prevChkfield))
//                        {
//                            writer.RenderEndTag();
//                            prevChkfield = string.Empty;
//                        }
//                         end
//                        writer.RenderEndTag();//end block
//                    }
//                    writer.RenderEndTag();//end grid
//                }
//            }
//            strContentHtlm = stringWriter.ToString();
//        }

//        private string HtmlInnerBody(string strContentHtlm)
//        {
//            HtmlTextWriter writer;
//            StringWriter strOuterWriter = new StringWriter();
//            using (writer = new HtmlTextWriter(strOuterWriter))
//            {
//                writer.AddAttribute("data-role", "page");
//                writer.RenderBeginTag(HtmlTextWriterTag.Div);

//                writer.AddAttribute("data-role", "content");
//                writer.RenderBeginTag(HtmlTextWriterTag.Div);
//                writer.RenderBeginTag(HtmlTextWriterTag.Form);
//                writer.Write(strContentHtlm);
//                writer.RenderEndTag();//Form
//                writer.RenderEndTag();//content
//                writer.RenderEndTag();//page
//            }
//            _HtmlBody = strOuterWriter.ToString();
//            return _HtmlBody;
//        }

//        private string HtmlInnerHead(string _view_id, verfile verFileList)
//        {
//            HtmlTextWriter writer;
//            StringWriter strOuterWriter = new StringWriter();
//            using (writer = new HtmlTextWriter(strOuterWriter))
//            {
//                string BarCodeChangeEvent = string.Empty;//if (ctrlId = '') {};
//                BarCodeChangeEvent += CreateEvents();
//                BarCodeChangeEvent = "function BarCodeChange(ctrlId) {" + BarCodeChangeEvent + "}";
//                string StrScripts = scriptForDateTimePckr.Trim();
//                if (string.IsNullOrEmpty(StrScripts)) StrScripts = HideControlsFunctionCall();
//                StrScripts += (StrScripts.Trim().Length > 0 ? "\r\n" + strControlsEvents.Trim() : strControlsEvents.Trim()) +
//                        (StrScripts.Trim().Length > 0 ? "\r\n" + strValidationJquery.Trim() : strValidationJquery.Trim()) +
//                        (StrScripts.Trim().Length > 0 ? "\r\n" + strTablePropertySet.Trim() : strTablePropertySet.Trim());
//                if (!string.IsNullOrEmpty(StrScripts.Trim()))
//                {
//                    StrScripts = " $(document).ready(function() {" + "  $('form').submit(function (event) { return SubmitForm();} ); " + StrScripts + "});";

//                    StrScripts += (strOrentationChange.Length > 0 ? "$(window).on('orientationchange', function (event) {" + strOrentationChange + "});" : "");
//                }
//                string strVar = "";
//                if (!string.IsNullOrEmpty(_view_id))
//                    strVar = "var LINK_" + _view_id.Replace('-', '_') + "='" + (IsjqPlotJSUsed ? "1" : "0") + (LocExists >= 2 ? "1" : "0") +
//                        (scriptForDateTimePckr.Length > 0 ? "1" : "0") + "0" + (!string.IsNullOrEmpty(strTablePropertyOnReady) ? "1" : "0") + "';";
//                else
//                    GetallPlaceHolderTags(verFileList);
//                writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/javascript");
//                writer.RenderBeginTag(HtmlTextWriterTag.Script);
//                writer.Write(strVar + ListActionButtonMethod + BarCodeChangeEvent + StrScripts + getPageLoadFunction());
//                writer.RenderEndTag();//Script
//            }
//            _HtmlHead = strOuterWriter.ToString();
//            return _HtmlHead;
//        }

//        string getHtmlForGridDiv(string RowType)
//        {
//            if (RowType.ToUpper() == "COL-100")
//                return "ui-grid-solo";

//            else if (RowType.ToUpper() == "COL-33-34-33")
//                return "ui-grid-b";

//            else
//                return "ui-grid-a";

//        }

//        private string getClassForBlocks(int columnNo, string RowType)
//        {
//            string strClass = string.Empty;
//            switch (columnNo)
//            {
//                case 1:
//                    strClass = "ui-block-a";
//                    if (RowType.ToUpper() == "COL-33-67")
//                        strClass += " left-33";
//                    else if (RowType.ToUpper() == "COL-67-33")
//                        strClass += " left-67";
//                    break;
//                case 2:
//                    strClass = "ui-block-b";
//                    if (RowType.ToUpper() == "COL-33-67")
//                        strClass += " right-67";
//                    else if (RowType.ToUpper() == "COL-67-33")
//                        strClass += " right-33";
//                    break;
//                case 3:
//                    strClass = "ui-block-c";
//                    break;
//                case 4:
//                    strClass = "ui-block-d";
//                    break;
//            }
//            return strClass;
//        }

//        private bool getDisplayType(string _Enum)
//        {
//            if (_Enum == "2")
//            {
//                return true;
//            }
//            return false;
//        }

//        private void GetallPlaceHolderTags(verfile verFileList)
//        {
//            string strjQueryMobileScripts = string.Empty;
//            if (verFileList != null)
//            {
//                string strfile = "COM";
//                if (IsjqPlotJSUsed) strfile += ",JQP";
//                if (!string.IsNullOrEmpty(strTablePropertyOnReady)) strfile += ",TBL";
//                if (scriptForDateTimePckr.Length > 0)
//                {
//                    scriptForDateTimePckr = @"DateTimeTextBoxClearEvent();" + scriptForDateTimePckr;
//                    strfile += ",DT";
//                }
//                if (LocExists >= 2) strfile += ",MAP";
//                strjQueryMobileScripts = @"<!--<PLACEHOLDER_META_VIEWPORT>-->" + "\r\n";
//                try
//                {
//                    foreach (VerFiles xmln in verFileList.files)
//                    {
//                        if (strfile.ToUpper().Contains(xmln.typ.ToUpper()))
//                        {
//                            strjQueryMobileScripts += @"<!--<PLACEHOLDER_" + xmln.pf + ">-->" + "\r\n";
//                            if (xmln.typ.ToUpper() == "MAP")
//                                strjQueryMobileScripts += @"<script type=""text/javascript"" src=""http://maps.google.com/maps/api/js?sensor=false""></script> ";
//                        }
//                    }
//                }
//                catch { }
//            }
//            _HtmlFileLinks = strjQueryMobileScripts;
//        }

//        #endregion
//        #region private method Control html
//        private void getHtml(Controls gctrl, string groupID, HtmlTextWriter writer)
//        {
//            string controlType = gctrl.id.Split('_')[0];
//            if (!string.IsNullOrEmpty(prevChkfield) && controlType != "CHK")
//            {
//                writer.RenderEndTag();
//                prevChkfield = string.Empty;
//            }
//            switch (controlType)
//            {
//                case "TXT":
//                    getHtmlForTextbox(gctrl, writer);
//                    break;
//                case "LBL":
//                    getHtmlForLabel(gctrl, writer);
//                    break;
//                case "DTP":
//                    getHtmlForDateTimePicker(gctrl, writer);
//                    break;
//                case "SLD":
//                    getHtmlForSlider(gctrl, writer);
//                    break;
//                case "IMG":
//                    getHtmlForImage(gctrl, writer);
//                    break;
//                case "CHK":
//                    if (string.IsNullOrEmpty(prevChkfield))
//                    {
//                        writer.AddAttribute("data-role", "controlgroup");
//                        writer.AddAttribute(HtmlTextWriterAttribute.Id, gctrl.id);
//                        writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);
//                    }
//                    prevChkfield = gctrl.id;
//                    getHtmlForCheckBox(gctrl, writer);
//                    break;
//                case "PIE":
//                    getHtmlForPieChart(gctrl, writer);
//                    break;
//                case "BAR":
//                    getHtmlForBarChart(gctrl, writer);
//                    break;
//                case "LINE":
//                    getHtmlForLineChart(gctrl, writer);
//                    break;
//                case "SEP":
//                    getHtmlForSeprator(gctrl, writer);
//                    break;
//                case "ELT":
//                    getHtmlForEditableListView(gctrl, writer);
//                    break;
//                case "BCODE":
//                    getHtmlForBarCodeReaderControl(gctrl, writer);
//                    break;
//                case "RDB":
//                    getHtmlForRadioButton(gctrl, writer);
//                    break;
//                case "TGL":
//                    getHtmlFortoggleButton(gctrl, writer);
//                    break;
//                case "DDL":
//                    getHtmlForDropDown(gctrl, writer);
//                    break;
//                case "HDF":
//                    getHtmlForHiddenField(gctrl, writer);
//                    break;
//                case "RPT":
//                    getHtmlForListview(gctrl, writer);
//                    break;
//                case "LOC":
//                    getHtmlForLocationControl(gctrl, writer);
//                    break;
//                case "TBL":
//                    getHtmlForTable(gctrl, writer);
//                    break;
//            }
//            throw new NotImplementedException();
//        }

//        private void getHtmlForListview(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
//            writer.RenderBeginTag(HtmlTextWriterTag.Div);

//            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);
//            if (_Gctrl.headerText.Trim() != string.Empty)
//            {
//                writer.RenderBeginTag(HtmlTextWriterTag.Legend);
//                writer.Write(_Gctrl.headerText);
//                writer.RenderEndTag();
//            }
//            writer.RenderEndTag();

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
//            writer.AddAttribute("data-role", "listview");

//            if (_Gctrl.dataInset == "1")
//            {
//                writer.AddAttribute("data-inset", "true");
//            }
//            else
//            {
//                writer.AddAttribute("data-inset", "false");
//            }
//            if (_Gctrl.actionButton == "1")
//            {
//                writer.AddAttribute("data-split-icon", "gear");
//            }
//            if (_Gctrl.dataGrouping == "1")
//                writer.AddAttribute("data-autodividers", "true");
//            switch (_Gctrl.listType)
//            {
//                case "1":
//                    writer.RenderBeginTag(HtmlTextWriterTag.Ul);
//                    break;
//                case "2":
//                    writer.RenderBeginTag(HtmlTextWriterTag.Ul);
//                    break;
//                case "3":
//                    writer.RenderBeginTag(HtmlTextWriterTag.Ol);
//                    break;
//                case "4":
//                    writer.RenderBeginTag(HtmlTextWriterTag.Ul);
//                    break;
//                case "5":
//                    writer.RenderBeginTag(HtmlTextWriterTag.Ul);
//                    break;
//            }

//            writer.RenderEndTag();

//            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);
//            if (_Gctrl.footerText.Trim() != string.Empty)
//            {
//                writer.RenderBeginTag(HtmlTextWriterTag.Label);
//                writer.Write(_Gctrl.footerText);
//                writer.RenderEndTag();
//            }
//            writer.RenderEndTag();
//            string rtTag = @"{""i"":""" + _Gctrl.imageName + @""",""b"":""" + _Gctrl.title + @""",""s"":""" + _Gctrl.subtitle + @""",""c"":""" + _Gctrl.countBubble + @""",""r"":""" + _Gctrl.rowId + @""",""n"":""" + _Gctrl.information + @""",""an"":""" + _Gctrl.additionalInfo + @"""}";
//            string strComdJson = createCommandJson(_Gctrl.userDefinedName, _Gctrl.bindType, _Gctrl.databindObjs, (int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_LIST, rtTag, _Gctrl.countBubble);

//            if (_Gctrl.actionButton == "1" || _Gctrl.rowClickable == "1")
//            {
//                writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName + "_RowId");
//                writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
//                writer.AddAttribute(HtmlTextWriterAttribute.Value, string.Empty);
//                writer.RenderBeginTag(HtmlTextWriterTag.Input);
//                writer.RenderEndTag();

//                writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName + "_Title");
//                writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
//                writer.AddAttribute(HtmlTextWriterAttribute.Value, string.Empty);
//                writer.RenderBeginTag(HtmlTextWriterTag.Input);
//                writer.RenderEndTag();
//            }
//            List<ActionButtonSetting> Abs = new List<ActionButtonSetting>();
//            if (_Gctrl.actionButton == "1")
//            {
//                if (!string.IsNullOrEmpty(_Gctrl.actionBtn1Settings.text)) Abs.Add(_Gctrl.actionBtn1Settings);
//                if (!string.IsNullOrEmpty(_Gctrl.actionBtn2Settings.text)) Abs.Add(_Gctrl.actionBtn2Settings);
//                if (!string.IsNullOrEmpty(_Gctrl.actionBtn3Settings.text)) Abs.Add(_Gctrl.actionBtn3Settings);
//            }
//            AddInControlsString(_Gctrl);


//            strControlsEvents += (strControlsEvents.Trim().Length > 1 ? "\r\n" : "") + " PushListPropInArray({'ctrl' : '" + _Gctrl.userDefinedName + "','rcdpg': '" + _Gctrl.countBubble + "', 'clk' : '" + _Gctrl.rowClickable + "', 'tmp' : '" + _Gctrl.listType + "','rname' : '" + _Gctrl.userDefinedName + "_RowId',  'bname' : '" + _Gctrl.userDefinedName + "_Title', 'cntbl' :'" + _Gctrl.countBubble + "', 'acbtn' : '" + _Gctrl.actionButton + "','btnlst':[" + SerializeJson<List<ActionButtonSetting>>(Abs) + "],'divider':'" + _Gctrl.dataGrouping + "','rt':" + rtTag.Replace(@"""", "'") + "});";
//            if (_Gctrl.databindObjs != null)
//            {
//                ForCommandargumentadd(_Gctrl.userDefinedName, strComdJson, _Gctrl.databindObjs.cmdParams);
//            }
//            writer.RenderEndTag();
//        }

//        private void getHtmlForTable(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);

//            string dotaskRtTag = "";

//            foreach (TableColumns tc in _Gctrl.columns)
//            {
//                dotaskRtTag += @",{""dtcol"":""" + tc.column + @""",""name"":""" + tc.header + @"""}";
//                strTablePropertyOnReady += @",{""idx"":""" + tc.index + @""",""chead"":""" + tc.header + @""",""dtyp"":""" + tc.datatype + @""",""disp"":""" + tc.dataFormat +
//                    @""",""frmt"":""" + tc.displayFormat + @""",""pfix"":""" + tc.prefix + @""",""sfix"":""" + tc.suffix + @""",""mfact"":""" + tc.factor + @"""}";
//            }
//            strTablePropertySet = "$('#TBL_" + _Gctrl.userDefinedName + "').parent().parent().addClass('tableForm');";
//            strOrentationChange = "TableHeightSetOnRotation('" + _Gctrl.userDefinedName + "');";
//            strTablePropertyOnReady = @"""ctrlid"":""" + _Gctrl.userDefinedName + @""",""issort"":""" + _Gctrl.allowSorting + @""",
//            ""isplot"":""" + _Gctrl.allowPlotting + @"""""isgroup"":""" + _Gctrl.allowGrouping + @"""""isfliter"":""" + _Gctrl.allowFilter + @"""
//            ""isrowno"":""" + _Gctrl.showRowNo + @""",""cols"":[" + strTablePropertyOnReady.Substring(1) + @"]";
//            if (_Gctrl.databindObjs != null)
//            {
//                ForCommandargumentadd(_Gctrl.userDefinedName, createCommandJson(_Gctrl.userDefinedName, _Gctrl.bindType, _Gctrl.databindObjs, (int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TABLE, @"{""prop"":[" + dotaskRtTag.Substring(1) + "]}", "-1"), _Gctrl.databindObjs.cmdParams);
//            }
//            writer.AddAttribute(HtmlTextWriterAttribute.Id, "TBL_" + _Gctrl.userDefinedName);
//            writer.RenderBeginTag(HtmlTextWriterTag.Div);
//            writer.RenderEndTag();//div 
//        }

//        private void getHtmlForLocationControl(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);
//            LocExists = 1;
//            setMapOnReady = setMapOnReady + @"""ctrlid"":""" + _Gctrl.userDefinedName + @""",""rtfn"":""" + ((int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_SET_LOCATION).ToString() + @""",""gpsign"":""" + _Gctrl.allowUsages + @"""";

//            writer.AddAttribute("data-role", "controlgroup");
//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
//            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
//            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
//            writer.AddAttribute(HtmlTextWriterAttribute.Value, "");
//            writer.RenderBeginTag(HtmlTextWriterTag.Input);
//            writer.RenderEndTag();

//            if (_Gctrl.isHidden == "1")
//            {
//                LocExists = 2;
//                setControlOnLoadScript += (setControlOnLoadScript.Trim().Length > 1 ? "\r\n" : "") + "MapType=2;";
//                writer.AddAttribute(HtmlTextWriterAttribute.Id, "MAP_CANVAS_" + _Gctrl.userDefinedName);
//                writer.AddAttribute(HtmlTextWriterAttribute.Class, "page-map");
//                writer.RenderBeginTag(HtmlTextWriterTag.Div);
//                writer.RenderEndTag();
//            }

//            writer.RenderEndTag();
//            CreateLinkItemJsonForChildForm(_Gctrl);
//        }

//        private void getHtmlForDropDown(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);

//            writer.AddAttribute("data-role", "controlgroup");
//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
//            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

//            writer.RenderBeginTag(HtmlTextWriterTag.Legend);
//            writer.Write(_Gctrl.labelText);
//            writer.RenderEndTag();

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
//            writer.RenderBeginTag(HtmlTextWriterTag.Select);
//            setControlOnLoadScript += (setControlOnLoadScript.Trim().Length > 1 ? "\r\n" : "") + "SetSizeOfControl('" + _Gctrl.userDefinedName + "','" + getControlWidth(_Gctrl.widthInPercent) + "','" + _Gctrl.minWidth + "',true);";
//            if (_Gctrl.promptText.Length > 0)
//            {
//                writer.AddAttribute(HtmlTextWriterAttribute.Value, _Gctrl.promptValue);
//                writer.AddAttribute(HtmlTextWriterAttribute.Selected, "selected");
//                writer.RenderBeginTag(HtmlTextWriterTag.Option);
//                writer.Write(_Gctrl.promptText);
//                writer.RenderEndTag();
//                strControlsEvents += (strControlsEvents.Trim().Length > 1 ? "\r\n" : "") + " PushSelectArray('" + _Gctrl.userDefinedName + "', '" + Convert.ToString(_Gctrl.promptText) + "', '" + Convert.ToString(_Gctrl.promptValue) + "');";
//            }
//            if (_Gctrl.bindType == ((int)DataBindType.Manual).ToString())
//            {
//                foreach (ControlOption op in _Gctrl.options)
//                {
//                    writer.AddAttribute(HtmlTextWriterAttribute.Value, op.value);
//                    writer.RenderBeginTag(HtmlTextWriterTag.Option);
//                    writer.Write(op.text);
//                    writer.RenderEndTag();
//                }
//            }
//            else
//            {
//                ForCommandargumentadd(_Gctrl.userDefinedName, createCommandJson(_Gctrl.userDefinedName, _Gctrl.bindType, _Gctrl.databindObjs, (int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_DROPDOWN, @"{""t"":""" + _Gctrl.displayText + @""",""v"":""" + _Gctrl.displayText + @"""}", "-1"), _Gctrl.databindObjs.cmdParams);
//            }
//            writer.RenderEndTag();
//            writer.RenderEndTag();
//            CreateLinkItemJsonForChildForm(_Gctrl);
//            AddInControlsString(_Gctrl);
//        }

//        private void getHtmlForRadioButton(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);

//            writer.AddAttribute("data-role", "controlgroup");
//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
//            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);
//            writer.RenderBeginTag(HtmlTextWriterTag.Legend);
//            writer.Write(_Gctrl.labelText);
//            writer.RenderEndTag();//Legend

//            foreach (ControlOption op in _Gctrl.options)
//            {
//                writer.AddAttribute(HtmlTextWriterAttribute.Id, op.id);
//                writer.AddAttribute(HtmlTextWriterAttribute.Name, _Gctrl.userDefinedName);
//                writer.AddAttribute(HtmlTextWriterAttribute.Type, "radio");
//                writer.AddAttribute(HtmlTextWriterAttribute.Value, op.value);
//                if (op.isDefault.Trim() == "1")
//                    writer.AddAttribute("checked", "checked");
//                writer.RenderBeginTag(HtmlTextWriterTag.Input);
//                writer.RenderEndTag();//Input

//                writer.AddAttribute("for", op.id);
//                writer.RenderBeginTag(HtmlTextWriterTag.Label);
//                writer.Write(op.text);
//                writer.RenderEndTag();//Label
//            }
//            writer.RenderEndTag();//Fieldset
//            CreateLinkItemJsonForChildForm(_Gctrl);
//            AddInControlsString(_Gctrl);
//        }

//        private void getHtmlForBarCodeReaderControl(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);
//            strControlsEvents += (strControlsEvents.Trim().Length > 1 ? "\r\n" : "") + "$('#" + "btn_" + _Gctrl.id + @"').bind('click',function(){  Scan('" + _Gctrl.userDefinedName + "','" + (string.IsNullOrEmpty(_Gctrl.barcodeType) ? "0" : _Gctrl.barcodeType) + "'); });";

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
//            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

//            writer.RenderBeginTag(HtmlTextWriterTag.Legend);
//            writer.Write(_Gctrl.labelText);
//            writer.RenderEndTag();//Legend

//            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-grid-a");
//            writer.RenderBeginTag(HtmlTextWriterTag.Div);
//            if (!string.IsNullOrEmpty(_Gctrl.placeholder))
//                writer.AddAttribute("placeholder", _Gctrl.placeholder);
//            if (_Gctrl.rowsCount > 1)
//            {
//                if (string.IsNullOrEmpty(_Gctrl.txtRowsCount)) _Gctrl.txtRowsCount = "2";
//                writer.AddAttribute(HtmlTextWriterAttribute.Rows, _Gctrl.txtRowsCount);
//                writer.RenderBeginTag(HtmlTextWriterTag.Textarea);
//            }
//            else
//            {
//                writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-block-a scan-Input");
//            }
//            writer.RenderBeginTag(HtmlTextWriterTag.Div);

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
//            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");
//            writer.AddAttribute("placeholder", "Scan Barcode");
//            if (_Gctrl.manualEntry == Convert.ToInt32(Code.ZERO).ToString()) writer.AddAttribute(HtmlTextWriterAttribute.ReadOnly, "readonly");
//            writer.RenderBeginTag(HtmlTextWriterTag.Input);
//            writer.RenderEndTag();
//            writer.RenderEndTag();//block a

//            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-block-b scan-Button");
//            writer.RenderBeginTag(HtmlTextWriterTag.Div);
//            writer.AddAttribute(HtmlTextWriterAttribute.Class, "custom-icon-border-radius");
//            writer.RenderBeginTag(HtmlTextWriterTag.Div);
//            writer.AddAttribute(HtmlTextWriterAttribute.Id, "btn_" + _Gctrl.id);
//            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-btn ui-icon-barcode ui-btn-icon-notext ui-corner-all ui-nodisc-icon");
//            writer.RenderBeginTag(HtmlTextWriterTag.A);
//            writer.RenderEndTag();//a
//            writer.RenderEndTag();//div

//            writer.RenderEndTag();//block b
//            writer.RenderEndTag();//grid a
//            writer.RenderEndTag();//fieldset
//            CreateLinkItemJsonForChildForm(_Gctrl);
//            AddInControlsString(_Gctrl);
//        }

//        private void getHtmlForSeprator(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            writer.RenderBeginTag(HtmlTextWriterTag.Hr);
//            writer.RenderEndTag();
//        }

//        private void getHtmlForTextbox(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);
//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
//            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

//            writer.RenderBeginTag(HtmlTextWriterTag.Legend);
//            writer.Write(_Gctrl.labelText);
//            writer.RenderEndTag();

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
//            if (string.IsNullOrEmpty(_Gctrl.txtRowsCount) || Convert.ToInt32(_Gctrl.txtRowsCount) <= 1)
//            {
//                if (!string.IsNullOrEmpty(_Gctrl.maskValue) && _Gctrl.maskValue == "1")
//                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "password");
//                else
//                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");

//                writer.AddAttribute(HtmlTextWriterAttribute.Maxlength, _Gctrl.maxLength);
//                if (!string.IsNullOrEmpty(_Gctrl.placeholder))
//                    writer.AddAttribute("placeholder", _Gctrl.placeholder);
//                writer.AddAttribute("data-clear-btn", "true");
//                writer.AddAttribute(HtmlTextWriterAttribute.Value, _Gctrl.value);
//                writer.RenderBeginTag(HtmlTextWriterTag.Input);
//            }
//            else
//            {
//                if (string.IsNullOrEmpty(_Gctrl.txtRowsCount)) _Gctrl.txtRowsCount = "2";
//                writer.AddAttribute(HtmlTextWriterAttribute.Rows, _Gctrl.txtRowsCount);
//                if (!string.IsNullOrEmpty(_Gctrl.placeholder))
//                    writer.AddAttribute("placeholder", _Gctrl.placeholder);
//                if (!string.IsNullOrEmpty(_Gctrl.value))
//                    writer.Write(_Gctrl.value);
//                writer.RenderBeginTag(HtmlTextWriterTag.Textarea);
//            }

//            writer.RenderEndTag();
//            writer.RenderEndTag();

//            setControlOnLoadScript += (setControlOnLoadScript.Trim().Length > 1 ? "\r\n" : "") + "SetSizeOfControl('" + _Gctrl.userDefinedName + "','" + getControlWidth(_Gctrl.widthInPercent) + "','" + _Gctrl.minWidth + "',false);";
//            CreateLinkItemJsonForChildForm(_Gctrl);
//            AddInControlsString(_Gctrl);
//            if (_Gctrl.databindObjs != null && !string.IsNullOrEmpty(_Gctrl.databindObjs.id))
//                ForCommandargumentadd(_Gctrl.userDefinedName, createCommandJson(_Gctrl.userDefinedName, _Gctrl.bindType, _Gctrl.databindObjs, (int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TEXT_CONTROL, @"{""t"":""" + _Gctrl.displayText + @"""}", "-1"), _Gctrl.databindObjs.cmdParams);
//        }

//        private void getHtmlForLabel(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);
//            writer.AddAttribute("data-role", "controlgroup");

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);

//            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

//            if (!string.IsNullOrEmpty(_Gctrl.title) && !string.IsNullOrEmpty(_Gctrl.titlePosition))
//            {
//                if (Convert.ToInt32(_Gctrl.titlePosition) != 1)
//                {
//                    writer.RenderBeginTag(HtmlTextWriterTag.Legend);
//                    writer.Write(_Gctrl.title);
//                    writer.RenderEndTag();
//                    _Gctrl.title = string.Empty;
//                }
//                else
//                    _Gctrl.title = "<b>" + _Gctrl.title + "</b> ";
//            }
//            else
//            {
//                _Gctrl.title = string.Empty;
//            }

//            if (!string.IsNullOrEmpty(_Gctrl.title))
//                strControlsEvents += (strControlsEvents.Trim().Length > 1 ? "\r\n" : "") + " PushLabelArray('" + _Gctrl.userDefinedName + "', '', '','','" + _Gctrl.title + "','" + _Gctrl.text + "');";


//            string ControlName = string.Empty;

//            ControlName = _Gctrl.userDefinedName;

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, ControlName);

//            string Classes = string.Empty;
//            if (!string.IsNullOrEmpty(_Gctrl.fontSize)) Classes = getFontSize(_Gctrl.fontSize) + " ";
//            if (!string.IsNullOrEmpty(_Gctrl.fontStyle)) Classes += getFontStyle(_Gctrl.fontStyle) + " ";
//            if (!string.IsNullOrEmpty(_Gctrl.fontAlign)) Classes += getTextAlignment(_Gctrl.fontAlign);
//            if (_Gctrl.wrap == ((int)Code.ONE).ToString()) Classes += " " + "styleword-wrap";
//            writer.AddAttribute(HtmlTextWriterAttribute.Class, Classes);

//            writer.RenderBeginTag(HtmlTextWriterTag.P);
//            writer.Write(_Gctrl.title + _Gctrl.text);
//            writer.RenderEndTag();//p tag

//            writer.RenderEndTag();//Fieldset CHANDRASHEKHAR
//            CreateLinkItemJsonForChildForm(_Gctrl);
//            AddInControlsString(_Gctrl);
//            if (_Gctrl.databindObjs != null && !string.IsNullOrEmpty(_Gctrl.databindObjs.id))
//                ForCommandargumentadd(_Gctrl.userDefinedName, createCommandJson(_Gctrl.userDefinedName, _Gctrl.bindType, _Gctrl.databindObjs, (int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TEXT_CONTROL, @"{""MapParameters"":""" + SerializeJson<List<LabelDataObectOutPuts>>(_Gctrl.labelDataObjs) + @"""}", "-1"), _Gctrl.databindObjs.cmdParams);
//        }

//        private void getHtmlForDateTimePicker(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            scriptForDateTimePckr += "\r\n" + "ShowDateTimePicker('" + _Gctrl.userDefinedName + "','" + _Gctrl.datePickType + "');";

//            SetControlDisplayCondition(_Gctrl);

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
//            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

//            writer.RenderBeginTag(HtmlTextWriterTag.Legend);
//            writer.Write(_Gctrl.labelText);
//            writer.RenderEndTag();//Legend

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
//            writer.AddAttribute("data-clear-btn", "true");
//            writer.AddAttribute(HtmlTextWriterAttribute.ReadOnly, "readonly");
//            writer.RenderBeginTag(HtmlTextWriterTag.Input);
//            writer.RenderEndTag();

//            writer.RenderEndTag();
//            CreateLinkItemJsonForChildForm(_Gctrl);
//            AddInControlsString(_Gctrl);
//        }

//        private void getHtmlForSlider(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
//            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

//            writer.RenderBeginTag(HtmlTextWriterTag.Legend);
//            writer.Write(_Gctrl.labelText);
//            writer.RenderEndTag();

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
//            writer.AddAttribute(HtmlTextWriterAttribute.Name, "slider");
//            writer.AddAttribute(HtmlTextWriterAttribute.Type, "range");
//            writer.AddAttribute(HtmlTextWriterAttribute.Value, _Gctrl.value);
//            writer.AddAttribute("data-highlight", "true");
//            writer.AddAttribute("min", _Gctrl.minVal);
//            writer.AddAttribute("max", _Gctrl.maxVal);
//            writer.AddAttribute("step", _Gctrl.incrementOnScroll);
//            writer.RenderBeginTag(HtmlTextWriterTag.Input);
//            writer.RenderEndTag();

//            writer.RenderEndTag();
//            CreateLinkItemJsonForChildForm(_Gctrl);
//            AddInControlsString(_Gctrl);

//            if (_Gctrl.databindObjs != null && !string.IsNullOrEmpty(_Gctrl.databindObjs.id))
//                ForCommandargumentadd(_Gctrl.userDefinedName, createCommandJson(_Gctrl.userDefinedName, _Gctrl.bindType, _Gctrl.databindObjs, (int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TEXT_CONTROL, @"{""t"":""" + _Gctrl.displayText + @"""}", "-1"), _Gctrl.databindObjs.cmdParams);
//        }

//        private void getHtmlForImage(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);
//            string width = "null";

//            if (!(_htmlImagesName.Contains(_Gctrl.imageSource)))
//            {
//                _htmlImagesName.Add(_Gctrl.imageSource);
//            }

//            writer.AddAttribute("data-role", "controlgroup");

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
//            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

//            if (_Gctrl.useOriginalSize == "1" || _Gctrl.fitToWidth == "0")
//            {
//                writer.AddAttribute(HtmlTextWriterAttribute.Class, getImgAlignment(_Gctrl.imgAlignment));
//            }
//            writer.RenderBeginTag(HtmlTextWriterTag.Div);

//            string ControlName = string.Empty;

//            ControlName = _Gctrl.userDefinedName;

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, ControlName);
//            writer.RenderBeginTag(HtmlTextWriterTag.Img);
//            writer.RenderEndTag();

//            writer.RenderEndTag();//div
//            writer.RenderEndTag();//fieldset
//            if (string.IsNullOrEmpty(_Gctrl.bindType.id) || Convert.ToInt32(_Gctrl.bindType.id) == 0)
//            {
//                try
//                {
//                    if (_Gctrl.fitToWidth == "1")
//                        width = "'100%'";
//                    else width = "'" + _Gctrl.imageWidth + "px'";
//                }
//                catch { }
//                if (_Gctrl.imageSource.Contains("."))
//                    strControlsEvents += (strControlsEvents.Trim().Length > 1 ? "\r\n" : "") + " SetImages('" + ControlName + "','" + _Gctrl.imageSource.Substring(0, _Gctrl.imageSource.LastIndexOf('.')).ToUpper() + _Gctrl.imageSource.Substring(_Gctrl.imageSource.LastIndexOf('.')) + "'," + width + ");";
//            }
//            else
//            {
//                strControlsEvents += (strControlsEvents.Trim().Length > 1 ? "\r\n" : "") + "PushImageBindType('" + ControlName + "'," + (Convert.ToInt32(_Gctrl.imageBindType) - 1).ToString() + ");";
//            }
//            if (Convert.ToInt32(_Gctrl.fitToWidth) == 1)
//            {
//                strControlsEvents += (strControlsEvents.Trim().Length > 1 ? "\r\n" : "") + "$('#" + ControlName + "').load(function () {$('#" + ControlName + "').parent().width($('#" + ControlName + "').width());});";
//            }

//            CreateLinkItemJsonForChildForm(_Gctrl);
//            AddInControlsString(_Gctrl);
//        }

//        private void getHtmlFortoggleButton(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);
//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
//            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);

//            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-grid-a");
//            writer.RenderBeginTag(HtmlTextWriterTag.Div);

//            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-block-a left-67");
//            writer.RenderBeginTag(HtmlTextWriterTag.Div);
//            writer.AddAttribute("for", _Gctrl.userDefinedName);
//            writer.RenderBeginTag(HtmlTextWriterTag.Label);
//            writer.Write(_Gctrl.labelText);
//            writer.RenderEndTag();

//            writer.RenderEndTag();//Div block-a

//            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-block-b right-33");
//            writer.RenderBeginTag(HtmlTextWriterTag.Div);
//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
//            writer.AddAttribute(HtmlTextWriterAttribute.Name, "slider");
//            writer.AddAttribute("data-role", "slider");
//            writer.AddAttribute("data-mini", "true");
//            writer.RenderBeginTag(HtmlTextWriterTag.Select);

//            writer.AddAttribute(HtmlTextWriterAttribute.Value, "0");
//            if (_Gctrl.defaultState == "0")
//                writer.AddAttribute(HtmlTextWriterAttribute.Selected, "selected");
//            writer.RenderBeginTag(HtmlTextWriterTag.Option);
//            if (_Gctrl.switchText == "0")
//                writer.Write("Off");
//            else
//                writer.Write("No");
//            writer.RenderEndTag();//Option

//            writer.AddAttribute(HtmlTextWriterAttribute.Value, "1");
//            if (_Gctrl.defaultState == "1")
//                writer.AddAttribute(HtmlTextWriterAttribute.Selected, "selected");
//            writer.RenderBeginTag(HtmlTextWriterTag.Option);
//            if (_Gctrl.switchText == "0")
//                writer.Write("On");
//            else
//                writer.Write("Yes");

//            writer.RenderEndTag();//Option
//            writer.RenderEndTag();//Select
//            writer.RenderEndTag();//Div block-b
//            writer.RenderEndTag();//Div
//            writer.RenderEndTag();//Fieldset
//            CreateLinkItemJsonForChildForm(_Gctrl);
//            AddInControlsString(_Gctrl);
//        }

//        private void getHtmlForCheckBox(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
//            writer.AddAttribute(HtmlTextWriterAttribute.Name, _Gctrl.id);
//            writer.AddAttribute(HtmlTextWriterAttribute.Type, "checkbox");
//            writer.RenderBeginTag(HtmlTextWriterTag.Input);
//            writer.RenderEndTag();//Input

//            writer.AddAttribute("for", _Gctrl.userDefinedName);
//            writer.RenderBeginTag(HtmlTextWriterTag.Label);
//            writer.Write(_Gctrl.labelText);
//            writer.RenderEndTag();//Label
//            CreateLinkItemJsonForChildForm(_Gctrl);
//            AddInControlsString(_Gctrl);
//        }

//        private void getHtmlForPieChart(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);
//            IsjqPlotJSUsed = true;
//            insertChartRenderContainerWithZoom(_Gctrl, writer);
//            if (Convert.ToInt32(_Gctrl.lblPo) == 0)
//                _Gctrl.lblPo = "7";

//            strControlsEvents += (strControlsEvents.Trim().Length > 1 ? "\r\n" : "") + @" PushChartCssInArray({""ctrl"": """ + _Gctrl.userDefinedName + @""",""thrshld"" : " + _Gctrl.dataLblThreshold + @",""title"" : """ + _Gctrl.chartTitle + @"""});";

//            string strCommandjson = createCommandJson(_Gctrl.userDefinedName, _Gctrl.bindType, _Gctrl.databindObjs, (int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_PIE, @"{""t"":""" + _Gctrl.title + @""",""v"":""" + _Gctrl.value + @"""}", "-1");
//            if (_Gctrl.databindObjs != null)
//            {
//                ForCommandargumentadd(_Gctrl.userDefinedName, strCommandjson, _Gctrl.databindObjs.cmdParams);
//            }
//            refreshButtonClickInHtml(_Gctrl.userDefinedName, strCommandjson, getFinalParaJson(SerializeJson<List<DatabindingObjParams>>(_Gctrl.databindObjs.cmdParams)));
//        }

//        private void getHtmlForBarChart(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);
//            IsjqPlotJSUsed = true;

//            insertChartRenderContainerWithZoom(_Gctrl, writer);

//            string strAxis = string.Empty;

//            string series = getLabelColorforLineAndBar(_Gctrl);
//            strControlsEvents += (strControlsEvents.Trim().Length > 1 ? "\r\n" : "") + @" PushChartCssInArray({""ctrl"": """ + _Gctrl.userDefinedName + @""",""noOfSeries"" : " + _Gctrl.noOfSeries + @", ""seriesValue"":" + getSeriesValueDependencies(_Gctrl) +
//            @",""sdpnt"" : """ + _Gctrl.noOfDecimalPoint + @""", ""xname"" : """ + _Gctrl.xAxisLabel + @""", ""yname"" : """ + _Gctrl.yAxisLabel + @""",""series"" : " + series + @", ""dir"" : """ + _Gctrl.barGraphDir + @""", ""btyp"" : """ + _Gctrl.barGraphType + @""",""title"" : """ + _Gctrl.chartTitle + @"""});";

//            string strCommandjson = createCommandJson(_Gctrl.userDefinedName, _Gctrl.bindType, _Gctrl.databindObjs, (int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_BAR, getSeriesValue(_Gctrl).Replace("'", @""""), "-1");
//            if (_Gctrl.databindObjs != null)
//            {
//                ForCommandargumentadd(_Gctrl.userDefinedName, strCommandjson, _Gctrl.databindObjs.cmdParams);
//            }
//            refreshButtonClickInHtml(_Gctrl.userDefinedName, strCommandjson, getFinalParaJson(SerializeJson<List<DatabindingObjParams>>(_Gctrl.databindObjs.cmdParams)));
//        }

//        private void getHtmlForLineChart(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);
//            IsjqPlotJSUsed = true;

//            insertChartRenderContainerWithZoom(_Gctrl, writer);

//            string series = getLabelColorforLineAndBar(_Gctrl);
//            strControlsEvents += (strControlsEvents.Trim().Length > 1 ? "\r\n" : "") + @" PushChartCssInArray({""ctrl"": """ + _Gctrl.userDefinedName + @""",""noOfSeries"" : " + _Gctrl.noOfSeries +
//            @", ""seriesValue"":" + getSeriesValueDependencies(_Gctrl) +
// @",""sdpnt"" : """ + _Gctrl.noOfDecimalPoint + @""", ""xname"" : """ + _Gctrl.xAxisLabel + @""", ""yname"" : """ + _Gctrl.yAxisLabel + @""",""series"" : " + series + @", ""dir"" : """ + _Gctrl.barGraphDir + @""", ""btyp"" : """ + _Gctrl.barGraphType + @""",""title"" : """ + _Gctrl.chartTitle + @"""});";
//            string strCommandjson = createCommandJson(_Gctrl.userDefinedName, _Gctrl.bindType, _Gctrl.databindObjs, (int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_LINE, getSeriesValue(_Gctrl).Replace("'", @""""), "-1");
//            if (_Gctrl.databindObjs != null)
//            {
//                ForCommandargumentadd(_Gctrl.userDefinedName, strCommandjson, _Gctrl.databindObjs.cmdParams);
//            }
//            refreshButtonClickInHtml(_Gctrl.userDefinedName, strCommandjson, getFinalParaJson(SerializeJson<List<DatabindingObjParams>>(_Gctrl.databindObjs.cmdParams)));
//        }

//        private void getHtmlForEditableListView(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            SetControlDisplayCondition(_Gctrl);

//            if (string.IsNullOrEmpty(_Gctrl.emptyListMsg)) _Gctrl.emptyListMsg = "List is Empty";
//            string EditButtonName = "ELT_EDIT_" + _Gctrl.userDefinedName;
//            string CancelButtonName = "ELT_CANCEL_" + _Gctrl.userDefinedName;
//            string DeleteButtonName = "ELT_DELETE_" + _Gctrl.userDefinedName;
//            string ActionPopupName = "ELT_ACTION_" + _Gctrl.userDefinedName;

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.id);
//            writer.RenderBeginTag(HtmlTextWriterTag.Div);

//            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);
//            writer.RenderEndTag();
//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
//            writer.AddAttribute("data-role", "listview");
//            writer.AddAttribute("data-inset", "true");
//            writer.AddAttribute("data-split-icon", "delete");

//            writer.RenderBeginTag(HtmlTextWriterTag.Ul);
//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName + "li1");
//            writer.RenderBeginTag(HtmlTextWriterTag.Li);
//            writer.AddAttribute(HtmlTextWriterAttribute.Style, "white-space:normal;margin:0.0em;");
//            writer.RenderBeginTag(HtmlTextWriterTag.P);
//            writer.RenderBeginTag(HtmlTextWriterTag.Strong);
//            writer.Write(_Gctrl.emptyListMsg);
//            writer.RenderEndTag();
//            writer.RenderEndTag();
//            writer.RenderEndTag();
//            writer.RenderEndTag();

//            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);
//            writer.RenderEndTag();
//            writer.RenderEndTag();

//            if (string.IsNullOrEmpty(_Gctrl.addBtnText)) _Gctrl.addBtnText = "Add";
//            string tnum = "-1", atnum = "-1", dnum = "-1", anum = "-1";
//            string prop = "";
//            foreach (EditableListItem et in _Gctrl.properties)
//            {
//                prop += @",{""dtcol"":""" + et.mappedDataCol + @""",""name"":""" + et.name + @"""}";
//                et.mappedDataCol = "";
//                if (et.name == _Gctrl.title)
//                {
//                    et.disp = "1";
//                    tnum = et.type;
//                }
//                else if (et.name == _Gctrl.additionalTitle)
//                {
//                    et.disp = "4";
//                    atnum = et.type;
//                }
//                else if (et.name == _Gctrl.subTitle)
//                {
//                    et.disp = "2";
//                    dnum = et.type;
//                }
//                else if (et.name == _Gctrl.additionalSubTitle)
//                {
//                    et.disp = "3";
//                    anum = et.type;
//                }
//                else
//                    et.disp = "0";
//            }

//            setEditableListItemOnReady += "\"ctrlid\":\"" + _Gctrl.userDefinedName + "\",\"perm\":\"" + _Gctrl.allowItemAddition + _Gctrl.allowItemEditing + _Gctrl.allowItemDelete + "\",\"min\":\"" + _Gctrl.minItems + "\",\"max\":\"" + _Gctrl.maxItems +
//                "\",\"addcfm\":\"" + _Gctrl.addConfirmation + "\",\"addbt\":\"" + _Gctrl.addBtnText + "\",\"anext\":\"" + _Gctrl.addNextBtnText + "\",\"enext\":\"" + _Gctrl.editNextBtnText + "\",\"atitle\":\"" + _Gctrl.addFormTitle +
//                "\",\"etitle\":\"" + _Gctrl.editFormTitle + "\",\"vtitle\":\"" + _Gctrl.viewFormTitle + "\",\"prop\":" + Utilities.SerializeJson<List<EditableListItem>>(_Gctrl.properties);

//            strControlsEvents += (strControlsEvents.Trim().Length > 1 ? "\r\n" : "") + "SetListOrTable('{\"req\":{\"isdel\":\"" + _Gctrl.allowItemDelete + "\",\"dsptxt\":\"" + _Gctrl.emptyListMsg + "\",\"tnum\":\"" + tnum + "\",\"atnum\":\"" + atnum + "\",\"dnum\":\"" + dnum + "\",\"anum\":\"" + anum + "\",\"tpre\":\"" + _Gctrl.titlePrefix + "\",\"tsuf\":\"" + _Gctrl.titleSuffix + "\",\"tfct\":\"" + _Gctrl.titleMultFactor + "\",";
//            if (!string.IsNullOrEmpty(_Gctrl.additionalTitle))
//            {
//                strControlsEvents += "\"atpre\":\"" + _Gctrl.additionalTitlePrefix + "\",\"atsuf\":\"" + _Gctrl.additionalTitleSuffix + "\",\"atfct\":\"" + _Gctrl.additionalTitleMultFactor + "\",";
//            }
//            strControlsEvents += "\"dpre\":\"" + _Gctrl.subTitlePrefix + "\",\"dsuf\":\"" + _Gctrl.subTitlePrefixSuffix + "\",\"dfct\":\"" + _Gctrl.subTitleMultFactor + "\"";
//            if (!string.IsNullOrEmpty(_Gctrl.additionalSubTitle))
//            {
//                strControlsEvents += ",\"apre\":\"" + _Gctrl.additionalSubTitlePrefix + "\",\"asuf\":\"" + _Gctrl.additionalSubTitleSuffix + "\",\"afct\":\"" + _Gctrl.additionalSubTitleMultFactr + "\"";
//            }
//            strControlsEvents += "}}');";

//            if (_Gctrl.databindObjs != null)
//            {
//                ForCommandargumentadd(_Gctrl.userDefinedName, createCommandJson(_Gctrl.userDefinedName, _Gctrl.bindType, _Gctrl.databindObjs, (int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_EDITABLE_LIST, @"{""prop"":[" + prop.Substring(1) + "]}", "-1"), _Gctrl.databindObjs.cmdParams);
//            }
//            AddInControlsString(_Gctrl);
//        }

//        private void getHtmlForHiddenField(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            strControlsEvents += (strControlsEvents.Trim().Length > 1 ? "\r\n" : "") + " PushLabelArray('" + _Gctrl.userDefinedName + "', '', '','');";

//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
//            writer.AddAttribute(HtmlTextWriterAttribute.Type, "hidden");
//            writer.AddAttribute(HtmlTextWriterAttribute.Value, _Gctrl.value);
//            writer.RenderBeginTag(HtmlTextWriterTag.Input);
//            writer.RenderEndTag();
//            CreateLinkItemJsonForChildForm(_Gctrl);
//            AddInControlsString(_Gctrl);
//            if (_Gctrl.databindObjs != null && !string.IsNullOrEmpty(_Gctrl.databindObjs.id))
//                ForCommandargumentadd(_Gctrl.userDefinedName, createCommandJson(_Gctrl.userDefinedName, _Gctrl.bindType, _Gctrl.databindObjs, (int)CallHtmlFromNativeFunctionCodes.CALL_FROM_NATIVE_BIND_TEXT_CONTROL, @"{""t"":""" + _Gctrl.text + @"""}", "-1"), _Gctrl.databindObjs.cmdParams);
//        }

//        private void SetControlDisplayCondition(Controls _Gctrl)
//        {
//            if (getDisplayType(_Gctrl.conditionalDisplay))
//            {
//                coditionalControls += "'" + _Gctrl.id + "',";
//                adv = new List<ConditionPlugin>();
//                adv = _Gctrl.condDisplayCntrlProps;
//                foreach (ConditionPlugin con in _Gctrl.condDisplayCntrlProps)
//                {
//                    addOrUpdateControlEventDataTable(_Gctrl.id, con.ThirdCol);
//                }
//            }
//        }

//        void insertChartRenderContainerWithZoom(Controls _Gctrl, HtmlTextWriter writer)
//        {
//            Mohan
//            Div for chart container
//            writer.AddAttribute(HtmlTextWriterAttribute.Class, "chartContainer");
//            writer.RenderBeginTag(HtmlTextWriterTag.Div);
//            a tag for zoom
//            writer.AddAttribute(HtmlTextWriterAttribute.Class, "refresh");

//            writer.RenderBeginTag(HtmlTextWriterTag.Span);
//            writer.AddAttribute(HtmlTextWriterAttribute.Id, "refresh_" + _Gctrl.userDefinedName);
//            writer.RenderBeginTag(HtmlTextWriterTag.Img);
//            writer.RenderEndTag();
//            writer.RenderEndTag();


//            a tag for zoom
//            writer.AddAttribute(HtmlTextWriterAttribute.Class, "zoom");
//            writer.RenderBeginTag(HtmlTextWriterTag.Span);
//            writer.AddAttribute(HtmlTextWriterAttribute.Id, "zoom_" + _Gctrl.userDefinedName);
//            writer.RenderBeginTag(HtmlTextWriterTag.Img);
//            writer.RenderEndTag();
//            writer.RenderEndTag();
//            end a tag for zoom
//            Clear both div
//            writer.AddAttribute(HtmlTextWriterAttribute.Style, "clear:both;");
//            writer.RenderBeginTag(HtmlTextWriterTag.Div);
//            writer.RenderEndTag();
//            end clear both div
//            Div where chart is rendered
//            writer.AddAttribute(HtmlTextWriterAttribute.Id, _Gctrl.userDefinedName);
//            if (string.IsNullOrEmpty(_Gctrl.title))
//                writer.AddAttribute(HtmlTextWriterAttribute.Style, "height:250px;");
//            else
//                writer.AddAttribute(HtmlTextWriterAttribute.Style, "height:300px;");
//            writer.RenderBeginTag(HtmlTextWriterTag.Div);
//            writer.RenderEndTag();
//            end div where chart is rendered

//            writer.RenderEndTag();
//            end Div for chart container
//        }

//        void refreshButtonClickInHtml(string controlName, string strCommandjson, string lpjsaon)
//        {
//            strControlsEvents += "$('#refresh_" + controlName + @"').click(function () { DoHtmlToNative('" + ((int)CallNativeFunctionCodes.CALL_NATIVE_FORCE_DO_TASK_RUN).ToString() + "',['{" + strCommandjson + @",""idx"":""1""'],['" + lpjsaon + @"'],''); });";
//        }

//        private string getLabelColorforLineAndBar(Controls _Gctrl)
//        {
//            string strLabels = "[";
//            switch (_Gctrl.noOfSeries)
//            {
//                case "1":
//                    strLabels += "'" + _Gctrl.yAxisDataObjs[0].label + "'";
//                    break;
//                case "2":
//                    strLabels += "'" + _Gctrl.yAxisDataObjs[0].label + "','" + _Gctrl.yAxisDataObjs[1].label + "'";
//                    break;
//                case "3":
//                    strLabels += "'" + _Gctrl.yAxisDataObjs[0].label + "','" + _Gctrl.yAxisDataObjs[1].label + "','" + _Gctrl.yAxisDataObjs[2].label + "'";
//                    break;
//                case "4":
//                    strLabels += "'" + _Gctrl.yAxisDataObjs[0].label + "','" + _Gctrl.yAxisDataObjs[1].label + "','" + _Gctrl.yAxisDataObjs[2].label + "','" + _Gctrl.yAxisDataObjs[3].label + "'";
//                    break;
//                case "5":
//                    strLabels += "'" + _Gctrl.yAxisDataObjs[0].label + "','" + _Gctrl.yAxisDataObjs[1].label + "','" + _Gctrl.yAxisDataObjs[2].label + "','" + _Gctrl.yAxisDataObjs[3].label + "','" + _Gctrl.yAxisDataObjs[4].label + "'";
//                    break;
//            }
//            return strLabels + "]";
//        }

//        private string getSeriesValue(Controls _Gctrl)
//        {
//            string str = string.Empty;
//            switch (_Gctrl.noOfSeries)
//            {
//                case "1":
//                    str = "{'t':'" + _Gctrl.xAxisData + "','v1':'" + _Gctrl.yAxisDataObjs[0] + "'}";
//                    break;
//                case "2":
//                    str = "{'t':'" + _Gctrl.xAxisData + "','v1':'" + _Gctrl.yAxisDataObjs[0] + "','v2':'" + _Gctrl.yAxisDataObjs[1] + "'}";
//                    break;
//                case "3":
//                    str = "{'t':'" + _Gctrl.xAxisData + "','v1':'" + _Gctrl.yAxisDataObjs[0] + "','v2':'" + _Gctrl.yAxisDataObjs[1] + "','v3':'" + _Gctrl.yAxisDataObjs[2] + "'}";
//                    break;
//                case "4":
//                    str = "{'t':'" + _Gctrl.xAxisData + "','v1':'" + _Gctrl.yAxisDataObjs[0] + "','v2':'" + _Gctrl.yAxisDataObjs[1] + "','v3':'" + _Gctrl.yAxisDataObjs[2] + "','v4':'" + _Gctrl.yAxisDataObjs[3] + "'}";
//                    break;
//                case "5":
//                    str = "{'t':'" + _Gctrl.xAxisData + "','v1':'" + _Gctrl.yAxisDataObjs[0] + "','v2':'" + _Gctrl.yAxisDataObjs[1] + "','v3':'" + _Gctrl.yAxisDataObjs[2] + "','v4':'" + _Gctrl.yAxisDataObjs[3] + "','v5':'" + _Gctrl.yAxisDataObjs[4] + "'}";
//                    break;

//            }
//            return str;
//        }

//        private string getSeriesValueDependencies(Controls _Gctrl)
//        {
//            string strValues = "[";
//            switch (_Gctrl.noOfSeries)
//            {
//                case "1":
//                    strValues += "{ 'sfac': '" + _Gctrl.yAxisDataObjs[0].scaleFactor + "'}";
//                    break;
//                case "2":
//                    strValues += "{ 'sfac': '" + _Gctrl.yAxisDataObjs[0].scaleFactor + "'}"
//                                + ",{ 'sfac': '" + _Gctrl.yAxisDataObjs[1].scaleFactor + "'}";
//                    break;
//                case "3":
//                    strValues += "{ 'sfac': '" + _Gctrl.yAxisDataObjs[0].scaleFactor + "'}"
//                                + ",{ 'sfac': '" + _Gctrl.yAxisDataObjs[1].scaleFactor + "'}"
//                                + ",{ 'sfac': '" + _Gctrl.yAxisDataObjs[2].scaleFactor + "'}";
//                    break;
//                case "4":
//                    strValues += "{ 'sfac': '" + _Gctrl.yAxisDataObjs[0].scaleFactor + "'}"
//                                + ",{ 'sfac': '" + _Gctrl.yAxisDataObjs[1].scaleFactor + "'}"
//                                + ",{ 'sfac': '" + _Gctrl.yAxisDataObjs[2].scaleFactor + "'}"
//                                + ",{ 'sfac': '" + _Gctrl.yAxisDataObjs[3].scaleFactor + "'}";
//                    break;
//                case "5":
//                    strValues += "{ 'sfac': '" + _Gctrl.yAxisDataObjs[0].scaleFactor + "'}"
//                                + ",{ 'sfac': '" + _Gctrl.yAxisDataObjs[1].scaleFactor + "'}"
//                                + ",{ 'sfac': '" + _Gctrl.yAxisDataObjs[2].scaleFactor + "'}"
//                                + ",{ 'sfac': '" + _Gctrl.yAxisDataObjs[3].scaleFactor + "'}"
//                                + ",{ 'sfac': '" + _Gctrl.yAxisDataObjs[4].scaleFactor + "'}";
//                    break;
//            }
//            return strValues + "]";
//        }
//        #endregion
//        #region additional methods
//        private void addOrUpdateControlEventDataTable(string ConditionalCtrl, string controlId)
//        {
//            string strEvent = string.Empty;
//            foreach (sectionRowPanels gRow in form.rowPanels)
//            {
//                foreach (SectionColPanels ctrl in gRow.colmnPanels)
//                {
//                    bool flag1 = false;
//                    foreach (Controls gctrl in ctrl.controls)
//                    {
//                        if (gctrl.userDefinedName == controlId)
//                        {
//                            strEvent = callJavascriptFunc(gctrl.id.Split('_')[0], ConditionalCtrl, controlId);
//                            flag1 = true;
//                            break;
//                        }
//                    }
//                    if (flag1)
//                        break;
//                }

//            }

//            DataRow[] dr = dtCtrlsEvents.Select("CtrlId ='" + controlId + "'");

//            if (dr.Length > 0)
//                dr[0]["CtrlEventFunction"] = dr[0]["CtrlEventFunction"].ToString() + strEvent;
//            else
//                dtCtrlsEvents.Rows.Add(controlId, strEvent);
//        }

//        private string getControlWidth(string width)
//        {
//            string strWidth = "100";
//            try
//            {
//                switch ((Code)Convert.ToInt32(width))
//                {
//                    case Code.ZERO:
//                        strWidth = "25";
//                        break;
//                    case Code.ONE:
//                        strWidth = "50";
//                        break;
//                    case Code.TWO:
//                        strWidth = "75";
//                        break;
//                    case Code.THREE:
//                        strWidth = "100";
//                        break;
//                }
//            }
//            catch
//            {
//            }
//            return strWidth;
//        }

//        private string getImgAlignment(string value)
//        {
//            string strAlign = string.Empty;
//            try
//            {
//                switch ((Code)Convert.ToInt32(value))
//                {
//                    case Code.ZERO:
//                        strAlign = "";
//                        break;
//                    case Code.ONE:
//                        strAlign = "imgcenter";
//                        break;
//                    case Code.TWO:
//                        strAlign = "imgright";
//                        break;
//                }
//            }
//            catch
//            {
//            }
//            return strAlign;
//        }

//        private string getTextAlignment(string value)
//        {
//            string strAlign = "alignleft";
//            try
//            {
//                switch ((Code)Convert.ToInt32(value))
//                {
//                    case Code.ZERO:
//                        strAlign = "alignleft";
//                        break;
//                    case Code.ONE:
//                        strAlign = "aligncenter";
//                        break;
//                    case Code.TWO:
//                        strAlign = "alignright";
//                        break;
//                }
//            }
//            catch
//            {
//            }
//            return strAlign;
//        }

//        private string getFontSize(string value)
//        {
//            string strFont = "sizenormal";
//            try
//            {
//                switch ((Code)Convert.ToInt32(value))
//                {
//                    case Code.ZERO:
//                        strFont = "sizesmall";
//                        break;
//                    case Code.ONE:
//                        strFont = "sizenormal";
//                        break;
//                    case Code.TWO:
//                        strFont = "sizelarge";
//                        break;
//                }
//            }
//            catch
//            {
//            }
//            return strFont;
//        }

//        private string getFontStyle(string value)
//        {
//            string strFontStyle = string.Empty;
//            try
//            {
//                switch ((Code)Convert.ToInt32(value))
//                {
//                    case Code.ONE:
//                        strFontStyle = "stylebold";
//                        break;
//                    case Code.TWO:
//                        strFontStyle = "styleitalic";
//                        break;
//                    case Code.THREE:
//                        strFontStyle = "stylebold styleitalic";
//                        break;
//                }
//            }
//            catch
//            {
//            }
//            return strFontStyle;
//        }

//        private void CreateLinkItemJsonForChildForm(Controls _Gctrl)
//        {
//            if (isChild && (!string.IsNullOrEmpty(_Gctrl.EditablePropertiValue) || !string.IsNullOrEmpty(_Gctrl.EditablePropertiText)))//" + _Gctrl.lpre + "
//                setEditableChildbindingOnReady += (string.IsNullOrEmpty(setEditableChildbindingOnReady) ? "" : ",") + "{\"ctrlid\":\"" + _Gctrl.userDefinedName + "\",\"typ\":\"" +
//                    _Gctrl.id.Split('_')[0] + "\",\"vprop\":\"" + _Gctrl.EditablePropertiValue + "\",\"tprop\":\"" + _Gctrl.EditablePropertiText + "\"}";
//        }

//        private string ForCommandargumentadd(string Control_id, string strCmd, List<DatabindingObjParams> lstlp)
//        {
//            string CmdParaRun = getFinalParaJson(SerializeJson<List<DatabindingObjParams>>(lstlp));
//            dtCtrlsdofuntionjson.Rows.Add(Control_id, strCmd, CmdParaRun);
//            return string.Empty;
//        }

//        private string SerializeJson<T>(object obj)
//        {
//            T objResponse = (T)obj;
//            MemoryStream stream = new MemoryStream();
//            DataContractJsonSerializer serializer = new DataContractJsonSerializer(objResponse.GetType());
//            serializer.WriteObject(stream, objResponse);
//            stream.Position = 0;
//            StreamReader streamReader = new StreamReader(stream);
//            return streamReader.ReadToEnd();
//        }

//        private string getFinalParaJson(string jsonWithDetails)
//        {
//            string strFinalJson = @"{""lpjson"":" + ReplaceSingleCourseAndBackSlash(jsonWithDetails) + "}";
//            return strFinalJson;
//        }

//        private string ReplaceBackSlash(string _Input)
//        {
//            if (_Input == null)
//                return string.Empty;
//            else
//            {
//                return Regex.Replace(_Input, @"\\", @"\\");
//            }
//        }

//        private string ReplaceSingleCourse(string _Input)
//        {
//            if (_Input == null)
//                return string.Empty;
//            else
//            {
//                return Regex.Replace(_Input, @"'", @"\'");
//            }
//        }

//        private string ReplaceSingleCourseAndBackSlash(string _Input)
//        {
//            if (_Input == null)
//                return string.Empty;
//            else
//            {
//                return ReplaceSingleCourse(ReplaceBackSlash(_Input));
//            }
//        }

//        private void AddInControlsString(Controls _Gctrl)
//        {
//            ControlsOnReady += @",{""ctrlid"":""" + _Gctrl.userDefinedName + @""",""typ"":""" + _Gctrl.id.Split('_')[0] + @""",""lbl"":""" + (string.IsNullOrEmpty(_Gctrl.labelText) ? _Gctrl.placeholder : _Gctrl.labelText)
//                + @""",""req"":""";
//            if (_Gctrl.required != null && _Gctrl.required.ToUpper() == "TRUE")
//                ControlsOnReady += @"1"",";
//            else
//                ControlsOnReady += @"0"",";
//            if (!string.IsNullOrEmpty(_Gctrl.txtRowsCount) && Convert.ToInt32(_Gctrl.validationType) > 0)
//                ControlsOnReady += getValidationJquery(Convert.ToInt32(_Gctrl.validationType), _Gctrl.userDefinedName, _Gctrl.regularExp) + @",""verr"":""" + _Gctrl.errorMsg + @""",""rng"":""" + _Gctrl.validationRange + @"""}";
//            else
//                ControlsOnReady += @"""vdl"":""-1"",""reg"":"""",""verr"":""" + _Gctrl.errorMsg + @""",""rng"":""" + _Gctrl.validationRange + @"""}";
//        }

//        private string getValidationJquery(int intValidationType, string ctrlId, string reg)
//        {
//            switch (intValidationType)
//            {
//                case (int)JqueryValidationType.Email:
//                    return @"""vdl"":""" + intValidationType + @""",""reg"":""""";
//                case (int)JqueryValidationType.Integer:
//                case (int)JqueryValidationType.IntegerLessThan:
//                case (int)JqueryValidationType.IntegerGreaterThan:
//                case (int)JqueryValidationType.IntegerInRange:
//                case (int)JqueryValidationType.IntegerDivisible:
//                    strValidationJquery = strValidationJquery + "\r\n" + @"$(""#" + ctrlId + @""").keydown(function (e) {IntergerKeycheck(e);});";
//                    return @"""vdl"":""" + intValidationType + @""",""reg"":""""";
//                case (int)JqueryValidationType.Decimal:
//                case (int)JqueryValidationType.DecimalLessThan:
//                case (int)JqueryValidationType.DecimalGreaterThan:
//                case (int)JqueryValidationType.DecimalInRange:
//                case (int)JqueryValidationType.DecimalDivisible:
//                    strValidationJquery = strValidationJquery + "\r\n" + @"$(""#" + ctrlId + @""").keydown(function (e) {DecimalKeyCheck(e, $(""#" + ctrlId + @""").val()); });";
//                    return @"""vdl"":""" + intValidationType + @""",""reg"":""""";
//                case (int)JqueryValidationType.AlphaNumeric:
//                case (int)JqueryValidationType.AlphaNumericLengthGreaterThan:
//                case (int)JqueryValidationType.AlphaNumericLengthInRange:
//                case (int)JqueryValidationType.AlphaNumericLengthLessThan:
//                    strValidationJquery = strValidationJquery + "\r\n" + @"$(""#" + ctrlId + @""").keypress(function(e){AlfaNumericKeyCheck(e);});";
//                    return @"""vdl"":""" + intValidationType + @""",""reg"":""""";
//                case (int)JqueryValidationType.Alphabetical:
//                case (int)JqueryValidationType.AlphabeticalLengthGreaterThan:
//                case (int)JqueryValidationType.AlphabeticalLengthInRange:
//                case (int)JqueryValidationType.AlphabeticalLengthLessThan:
//                    strValidationJquery = strValidationJquery + "\r\n" + @"$(""#" + ctrlId + @""").keypress(function (e) {AlfaKeyCheck(e);});";
//                    return @"""vdl"":""" + intValidationType + @""",""reg"":""""";
//                case (int)JqueryValidationType.CustumRegx:
//                    return @"""vdl"":""" + intValidationType + @""",""reg"":""" + reg + @"""";
//                default:
//                    return @"""vdl"":""0"",""reg"":""""";
//            }
//        }

//        private string createCommandJson(string controlName, string Bindtype, DatabindingObject cmd, int returnFunc, string returnSeries, string pagercd)
//        {
//            DataRow[] result = drDataObjects.Select("COMMAND_ID = '" + cmd.id + "'");
//            if (result.Count() > 0)
//            {
//                string CACHE = Convert.ToString(result[0]["CACHE"]);
//                string EXPIRY_CONDITION = Convert.ToString(result[0]["EXPIRY_CONDITION"]);
//                string EXPIRY_FREQUENCY = Convert.ToString(result[0]["EXPIRY_FREQUENCY"]);
//                return @"""ctrlid"":""" + controlName + @""",""ctyp"":""" + Bindtype + @""",""cmd"":""" + cmd.id + @""",""cache"":""" + (string.IsNullOrEmpty(CACHE) ? "0" : CACHE) + @""",""efrq"":""" + (string.IsNullOrEmpty(EXPIRY_FREQUENCY) ? "-1" : EXPIRY_FREQUENCY)
//                    + @""",""econ"":""" + (string.IsNullOrEmpty(EXPIRY_CONDITION) ? "-1" : EXPIRY_CONDITION) + @""",""cr"":""" + Convert.ToString(result[0]["CREDENTIAL_PROPERTY"]) + @""",""rcdpg"":""" + pagercd + @""",""rtfn"":""" + returnFunc.ToString() + @""",""rt"":" + returnSeries;
//            }
//            return "";
//        }

//        private string callJavascriptFunc(string ctrlType, string ctrl, string conctrl)
//        {
//            string strValueArray = "new Array('";
//            if (adv != null)
//            {
//                for (int i = 0; i < adv.Count; i++)
//                {
//                    strValueArray = strValueArray + "" + adv[i].ThirdCol + ((i == adv.Count - 1) ? "'" : "','");
//                }
//            }
//            return "ShowControl('" + conctrl + "'," + strValueArray + ")" + ",'" + ctrl + "');";
//        }

//        string CreateEvents()
//        {
//            onChangeFunctions 
//            string srtEventsFunctions = string.Empty;
//            string srtFunctions = string.Empty;
//            string dotaskJson = string.Empty;
//            bool scriptFlag = false;
//            foreach (var grt in form.rowPanels)
//            {
//                foreach (var gcol in grt.colmnPanels)
//                {
//                    foreach (var gctrl in gcol.controls)
//                    {
//                        scriptFlag = false;
//                        dotaskJson = createdoHtmlToNativeJsonRefresh(gctrl.refreshControls);
//                        srtEventsFunctions = string.Empty;
//                        DataRow[] drEvent = dtCtrlsEvents.Select("CtrlId='" + gctrl.userDefinedName + "'");
//                        foreach (DataRow dr1 in drEvent)
//                        {
//                            srtEventsFunctions = srtEventsFunctions + " " + dr1["CtrlEventFunction"].ToString();
//                            scriptFlag = true;
//                        }
//                        bool IsControlEvent = false;
//                        switch (gctrl.id.Split('_')[0])
//                        {
//                            case "TXT":
//                                srtFunctions = "$('#" + gctrl.userDefinedName + @"').bind('blur',(function(){  ";
//                                IsControlEvent = true;
//                                break;
//                            case "BTN":
//                                srtFunctions = "$('#" + gctrl.userDefinedName + @"').bind('click',function(){  ";
//                                IsControlEvent = true;
//                                break;
//                            case "RDB":
//                                srtFunctions = "$('input[name=" + gctrl.userDefinedName + "]').bind('change',function(){  ";
//                                IsControlEvent = true;
//                                break;
//                            case "CHK":
//                                srtFunctions = "$('input[name=" + gctrl.userDefinedName + "]').bind('change',function(){  ";
//                                IsControlEvent = true;
//                                break;
//                            case "DDL":
//                                srtFunctions = "$('#" + gctrl.userDefinedName + @"').bind('change',function(){  ";
//                                IsControlEvent = true;
//                                break;
//                            case "TGL":
//                                srtFunctions = "$('#" + gctrl.userDefinedName + @"').bind('change',function(){  ";
//                                IsControlEvent = true;
//                                break;
//                            case "BCODE":
//                                return dotaskJson.Length > 0 ? ("if (ctrlId == '" + gctrl.userDefinedName + "') {" + dotaskJson + "}") : "";
//                        }

//                        if (scriptFlag || (!string.IsNullOrEmpty(dotaskJson.Trim()) && IsControlEvent))
//                        {
//                            strControlsEvents += srtFunctions + srtEventsFunctions + " " + dotaskJson + "});";
//                        }
//                    }
//                }
//            }
//            return string.Empty;
//        }

//        private string createdoHtmlToNativeJsonRefresh(List<string> controlId)
//        {
//            int index = 1;
//            string Cmdarraylist = string.Empty;
//            string Paraarraylist = string.Empty;
//            string RefreshControl = string.Empty;
//            if (controlId != null)
//                foreach (string s in controlId)
//                {
//                    DataRow[] rd = dtCtrlsdofuntionjson.Select("CtrlId ='" + s + "'");
//                    if (rd.Length > 0)
//                    {
//                        foreach (DataRow dr1 in rd)
//                        {
//                            Cmdarraylist = Cmdarraylist + "'{" + dr1["cmdjson"].ToString().Replace("'", @"""") + @",""idx"":""" + index.ToString() + @"""" + "',";
//                            Paraarraylist = Paraarraylist + "'" + dr1["parajson"].ToString() + "',";
//                            index++;
//                        }
//                    }
//                    else
//                    {
//                        RefreshControl += "SetCtrlValue('" + s + "','');";
//                    }
//                }
//            if (Cmdarraylist.Trim().Length > 0)//do task lists
//                Cmdarraylist = Cmdarraylist.Substring(0, Cmdarraylist.Length - 1);

//            if (Paraarraylist.Trim().Length > 0)
//                Paraarraylist = Paraarraylist.Substring(0, Paraarraylist.Length - 1);
//            string strReturn = string.Empty;
//            if (!string.IsNullOrEmpty(Cmdarraylist))
//                strReturn = @"DoHtmlToNative('" + ((int)CallNativeFunctionCodes.CALL_NATIVE_DO_TASK_RUN).ToString() + "',[" + Cmdarraylist + "],[" + Paraarraylist + @"],"""");";
//            if (!string.IsNullOrEmpty(RefreshControl)) strReturn += RefreshControl;
//            return strReturn;
//        }

//        private string getPageLoadFunction()
//        {
//            string strfunction = (setControlOnLoadScript.Trim().Length > 1 ? "\r\n" : "") + setControlOnLoadScript;
//            if (!string.IsNullOrEmpty(strfunction)) strfunction += "\r\n";
//            strfunction += createdoHtmlToNativeJson();
//            return "$(window).load(function() {" + strfunction + "\r\n" + " });";
//        }

//        private string createdoHtmlToNativeJson()
//        {
//            int index = 1;
//            string Cmdarraylist = string.Empty;
//            string Paraarraylist = string.Empty;
//            foreach (DataRow dr1 in dtCtrlsdofuntionjson.Rows)
//            {
//                Cmdarraylist = Cmdarraylist + "'{" + dr1["cmdjson"].ToString().Replace("'", @"""") + @",""idx"":""" + index.ToString() + @"""" + "',";
//                Paraarraylist = Paraarraylist + "'" + dr1["parajson"].ToString() + "',";
//                index++;
//            }

//            string strJsonForOnReady = "\"\"";
//            if (ControlsOnReady.Trim().Length > 0)//validate Control list in json on next click
//                ControlsOnReady = ControlsOnReady.Substring(1);

//            strJsonForOnReady = @"'""ctrls"":[" + ControlsOnReady + @"],""map"":{" + setMapOnReady + @"},""elst"":{" + setEditableListItemOnReady + @"},""elbind"":[" + setEditableChildbindingOnReady + @"],""dtbl"":{" + strTablePropertyOnReady + @"}'";
//            if (Cmdarraylist.Trim().Length > 0)//do task lists
//                Cmdarraylist = Cmdarraylist.Substring(0, Cmdarraylist.Length - 1);

//            if (Paraarraylist.Trim().Length > 0)
//                Paraarraylist = Paraarraylist.Substring(0, Paraarraylist.Length - 1);

//            return @"DoHtmlToNative('" + ((int)CallNativeFunctionCodes.CALL_NATIVE_HTML_READY).ToString() + "',[" + Cmdarraylist + "],[" + Paraarraylist + @"]," + strJsonForOnReady + ");";
//        }

//        string HideControlsFunctionCall()
//        {
//            if (coditionalControls.Trim().Length > 0)
//                return "\r\n" + "HideControls([" + coditionalControls.Remove(coditionalControls.LastIndexOf(',')) + "]);";
//            return string.Empty;
//        }
//        #endregion
//    }

//    [DataContract]
//    public class verfile
//    {
//        [DataMember]
//        public string ver { get; set; }
//        [DataMember]
//        public List<VerFiles> files { get; set; }
//    }

//    [DataContract]
//    public class VerFiles
//    {
//        [DataMember]
//        public int idx { get; set; }
//        [DataMember]
//        public string typ { get; set; }
//        [DataMember]
//        public string pf { get; set; }
//    }
//    public class HtmlFileParts
//    {
//        public HtmlFileParts(List<string> _htmlImagesName, string _HtmlHead, string _HtmlBody, string _HtmlFileLinks)
//        {
//            this.HtmlImagesName = _htmlImagesName;
//            this.FormHtmlHead = _HtmlHead;
//            this.FormHtmlBody = _HtmlBody;
//            this.FormHtmlLinks = _HtmlFileLinks;

//        }
//        [DataMember]
//        public List<string> HtmlImagesName { get; set; }
//        [DataMember]
//        public string FormHtmlHead { get; set; }
//        [DataMember]
//        public string FormHtmlBody { get; set; }
//        [DataMember]
//        public string FormHtmlLinks { get; set; }
//    }
//}
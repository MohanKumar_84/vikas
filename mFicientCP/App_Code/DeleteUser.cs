﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class DeleteUser
    {
        public DeleteUser()
        {

        }

        public DeleteUser(string _SubAdminId, string _UserName, string _UserId, string companyId)
        {
            this.SubAdminId = _SubAdminId;
            this.UserName = _UserName;
            this.UserId = _UserId;
            this.CompanyId = companyId;
        }
        public void Process()
        {
            StatusCode = -1000;
            //for deleting session from cache in the application we have to get the registered devices of the user
            GetRegisteredDeviceDetailsByUserId objRegisteredUserDevices = new GetRegisteredDeviceDetailsByUserId(this.CompanyId, this.SubAdminId, this.UserId);
            GetUserDetail objUserDetail = new GetUserDetail(false, true, this.SubAdminId, this.UserId, "", this.CompanyId);
            try
            {
                objRegisteredUserDevices.Process();
                objUserDetail.Process();
            }
            catch
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
                return;
            }
            //MBuzz User is deleleted from aspx page.
            //MBuzzServerConfigDtls objMBuzzSetting = new MBuzzServerConfigDtls();
            //objMBuzzSetting.ServerIP = String.Empty;
            //objMBuzzSetting.PortNumber = 0;

            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            SqlCommand objSqlCommand;
            try
            {
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                string query = @"DELETE FROM TBL_USER_DETAIL  WHERE  USER_ID=@USER_ID AND  COMPANY_ID=@COMPANY_ID;";
                objSqlTransaction = objSqlConnection.BeginTransaction();
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "Record deleted successFully";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
                }

                query = @"DELETE FROM TBL_USER_DIVISION_DEP   WHERE USER_ID=@USER_ID AND  COMPANY_ID=@COMPANY_ID;";
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.ExecuteNonQuery();

                query = @"DELETE FROM TBL_REGISTERED_DEVICE WHERE USER_ID=@USER_ID AND  COMPANY_ID=@COMPANY_ID;";
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.ExecuteNonQuery();


                query = @"DELETE FROM TBL_DEVICE_REGISTRATION_REQUEST WHERE USER_ID=@USER_ID AND  COMPANY_ID=@COMPANY_ID;";
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.ExecuteNonQuery();

                query = @"DELETE FROM TBL_USER_DEVICE_SETTINGS WHERE USER_ID=@USER_ID AND  COMPANY_ID=@COMPANY_ID;";
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.ExecuteNonQuery();
                //SubAdminLog.saveSubAdminActivityLog("S", this.SubAdminId, this.UserId, this.CompanyId,
                //    SUBADMIN_ACTIVITY_LOG.MOBILE_USER_DELETE, DateTime.UtcNow.Ticks, "", "", "", this.UserName, objSqlConnection, objSqlTransaction);

                DeleteMobileSessionOfUser objDeleteMobileSessAndMbuzzMsgs = new DeleteMobileSessionOfUser(this.UserId,
                    this.CompanyId, this.SubAdminId, USER_LOG_OUT_TYPE.USER_DELETED, DeleteMobileSessionOfUser.MBUZZ_PENDING_MSGS_DELETE_TYPE.User
                    , true, objSqlConnection, objSqlTransaction, null);
                objDeleteMobileSessAndMbuzzMsgs.ProcessInTransaction(objUserDetail, objRegisteredUserDevices, 0);


                DeleteDevcDtlsFromMGram objDeleteDvcFromMGram =
                                new DeleteDevcDtlsFromMGram(this.CompanyId,
                                    this.UserName);
                objDeleteDvcFromMGram.Process();
                if (objDeleteDvcFromMGram.StatusCode != 0) throw new Exception();
                objSqlTransaction.Commit();

                if(objUserDetail.StatusCode==0)
                    Utilities.saveActivityLog(objSqlConnection, mFicientCommonProcess.ACTIVITYENUM.USER_DELETE, this.CompanyId, this.SubAdminId, this.UserId, this.UserName, Convert.ToString(objUserDetail.ResultTable.Rows[0]["FIRST_NAME"]) + "" + Convert.ToString(objUserDetail.ResultTable.Rows[0]["LAST_NAME"]), "", "", "", "", "");
                StatusCode = 0;
                StatusDescription = "";

            }
            catch (Exception ex)
            {
                objSqlTransaction.Rollback();
                StatusCode = -1000;
                StatusDescription = "Internal server error";
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }

        public string SubAdminId
        {
            set;
            get;
        }
        public string UserName
        {
            set;
            get;
        }
        public string UserId
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
    }
}
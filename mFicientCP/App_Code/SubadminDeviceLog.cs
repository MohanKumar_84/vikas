﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace mFicientCP
{
    public class SubadminDeviceLog
    {
       
        private const string SPACE_SEPARATOR = " ";
        
        public static DataSet getSubAdminActivityLogRaw(string subAdminId, string companyId, SUBADMIN_Device_LOG processType, long fromTime, long toTime)
        {
            string strQuery = "";
            if (String.IsNullOrEmpty(subAdminId) && !(processType == SUBADMIN_Device_LOG.ALL))
            {
                strQuery = @"SELECT [USER_ROLE],[USER_ID],[COMPANY_ID],[PROCESS_TYPE]
                                ,[PROCESS_ID],[ACTIVITY_DATETIME],[PARA1],[PARA2],[PARA3],[PARA4],[PARA5],SubAdmin.USER_NAME,SubAdmin.FULL_NAME AS SA_FULL_NAME
                                FROM [TBL_ACTIVITY_LOG] AS Activity
                                INNER JOIN TBL_SUB_ADMIN SubAdmin
                                ON Activity.USER_ID = SubAdmin.SUBADMIN_ID
                                WHERE Activity.COMPANY_ID = @CompanyId
                                AND Activity.USER_ID = @UserId
                                AND Activity.PROCESS_TYPE = @ProcessType
                                AND Activity.ACTIVITY_DATETIME BETWEEN @FromTime AND @ToTime ORDER BY [ACTIVITY_DATETIME] DESC";
            }
            else if (!String.IsNullOrEmpty(subAdminId) && processType == SUBADMIN_Device_LOG.ALL)
            {

                strQuery = @"SELECT ActivityLog.*,
                            UserDtl.USER_NAME,SubAdmin.USER_NAME AS SUBADMIN_NAME,SubAdmin.FULL_NAME AS SA_FULL_NAME
                            FROM [TBL_ACTIVITY_LOG] AS ActivityLog
                            INNER JOIN TBL_SUB_ADMIN SubAdmin
                            ON ActivityLog.USER_ID = SubAdmin.SUBADMIN_ID
                            LEFT OUTER JOIN TBL_USER_DETAIL AS  UserDtl ON ActivityLog.PARA2 = UserDtl.USER_ID
                            WHERE ActivityLog.COMPANY_ID = @CompanyId
                            AND ActivityLog.ACTIVITY_DATETIME BETWEEN @FromTime  AND  @ToTime ORDER BY [ACTIVITY_DATETIME] DESC
                            ";

            }
            else if (!String.IsNullOrEmpty(subAdminId) && !(processType == SUBADMIN_Device_LOG.ALL))
            {
                strQuery = @"SELECT [USER_ROLE],[USER_ID],[COMPANY_ID],[PROCESS_TYPE]
                                ,[PROCESS_ID],[ACTIVITY_DATETIME],[PARA1],[PARA2],[PARA3],[PARA4],[PARA5],SubAdmin.USER_NAME,SubAdmin.FULL_NAME  AS SA_FULL_NAME
                                FROM [TBL_ACTIVITY_LOG] AS Activity
                                INNER JOIN TBL_SUB_ADMIN SubAdmin
                                ON Activity.USER_ID = SubAdmin.SUBADMIN_ID
                                WHERE Activity.COMPANY_ID = @CompanyId
                                AND Activity.USER_ID = @UserId
                                AND Activity.PROCESS_TYPE = @ProcessType
                                AND Activity.ACTIVITY_DATETIME BETWEEN @FromTime AND @ToTime ORDER BY [ACTIVITY_DATETIME] DESC";
            }
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", companyId);
            if (!String.IsNullOrEmpty(subAdminId))
            {
                cmd.Parameters.AddWithValue("@UserId", subAdminId);
            }
            if (!(processType == SUBADMIN_Device_LOG.ALL))
            {
                cmd.Parameters.AddWithValue("@ProcessType", processType);
            }
            cmd.Parameters.AddWithValue("@FromTime", fromTime);
            cmd.Parameters.AddWithValue("@ToTime", toTime);

            return MSSqlClient.SelectDataFromSQlCommand(cmd);
        }

        public static DataSet getSubAdminDeviceActivityLogModified(string subAdminId, string companyId, SUBADMIN_Device_LOG processType, long fromTime, long toTime)
        {
            string strQuery = "";
            if (!String.IsNullOrEmpty(subAdminId) && (processType == SUBADMIN_Device_LOG.ALL))
            {
                strQuery = @"SELECT ActivityLog.*,
                          UserDtl.USER_NAME,SubAdmin.USER_NAME AS SUBADMIN_NAME,SubAdmin.FULL_NAME AS SA_FULL_NAME
                         FROM [TBL_ACTIVITY_LOG] AS ActivityLog
                         INNER JOIN TBL_SUB_ADMIN SubAdmin
                          ON ActivityLog.USER_ID = SubAdmin.SUBADMIN_ID
                        LEFT OUTER JOIN TBL_USER_DETAIL AS  UserDtl ON ActivityLog.PARA2 = UserDtl.USER_ID
                         WHERE ActivityLog.COMPANY_ID = @CompanyId
                          AND (ActivityLog.ACTIVITY_DATETIME BETWEEN @FromTime  AND  @ToTime)  and (Process_Type='4' or Process_Type='5'  or PROCESS_TYPE='6' or PROCESS_TYPE='12' )  ORDER BY [ACTIVITY_DATETIME] DESC
                          ";
            }
            else if (!String.IsNullOrEmpty(subAdminId) && processType == SUBADMIN_Device_LOG.ALL)
            {

                strQuery = @"SELECT ActivityLog.*,
                          UserDtl.USER_NAME,SubAdmin.USER_NAME AS SUBADMIN_NAME,SubAdmin.FULL_NAME AS SA_FULL_NAME
                         FROM [TBL_ACTIVITY_LOG] AS ActivityLog
                         INNER JOIN TBL_SUB_ADMIN SubAdmin
                          ON ActivityLog.USER_ID = SubAdmin.SUBADMIN_ID
                        LEFT OUTER JOIN TBL_USER_DETAIL AS  UserDtl ON ActivityLog.PARA2 = UserDtl.USER_ID
                         WHERE ActivityLog.COMPANY_ID = @CompanyId
                          AND (ActivityLog.ACTIVITY_DATETIME BETWEEN @FromTime  AND  @ToTime)  and (Process_Type='4' or Process_Type='5'  or PROCESS_TYPE='6' or PROCESS_TYPE='12')   ORDER BY [ACTIVITY_DATETIME] DESC
                          ";

            }
            else if (String.IsNullOrEmpty(subAdminId) && !(processType == SUBADMIN_Device_LOG.ALL))
            {
                strQuery = @"SELECT ActivityLog.*,
                          UserDtl.USER_NAME,SubAdmin.USER_NAME AS SUBADMIN_NAME,SubAdmin.FULL_NAME AS SA_FULL_NAME
                         FROM [TBL_ACTIVITY_LOG] AS ActivityLog
                         INNER JOIN TBL_SUB_ADMIN SubAdmin
                          ON ActivityLog.USER_ID = SubAdmin.SUBADMIN_ID
                        LEFT OUTER JOIN TBL_USER_DETAIL AS  UserDtl ON ActivityLog.PARA2 = UserDtl.USER_ID
                         WHERE ActivityLog.COMPANY_ID = @CompanyId
                          AND ( ActivityLog.ACTIVITY_DATETIME BETWEEN @FromTime  AND  @ToTime  ) and (Process_Type='4' or Process_Type='5'  or PROCESS_TYPE='6' or PROCESS_TYPE='12')   ORDER BY [ACTIVITY_DATETIME] DESC
                          ";
            }
            else if (String.IsNullOrEmpty(subAdminId) && processType == SUBADMIN_Device_LOG.ALL)
            {
                strQuery = @"SELECT ActivityLog.*,
                          UserDtl.USER_NAME,SubAdmin.USER_NAME AS SUBADMIN_NAME,SubAdmin.FULL_NAME AS SA_FULL_NAME
                         FROM [TBL_ACTIVITY_LOG] AS ActivityLog
                         INNER JOIN TBL_SUB_ADMIN SubAdmin
                          ON ActivityLog.USER_ID = SubAdmin.SUBADMIN_ID
                        LEFT OUTER JOIN TBL_USER_DETAIL AS  UserDtl ON ActivityLog.PARA2 = UserDtl.USER_ID
                         WHERE ActivityLog.COMPANY_ID = @CompanyId
                          AND (ActivityLog.ACTIVITY_DATETIME BETWEEN @FromTime  AND  @ToTime)  and (ActivityLog.Process_Type='4' or ActivityLog. Process_Type='5'  or ActivityLog.PROCESS_TYPE='6' or ActivityLog.PROCESS_TYPE='12' )  ORDER BY [ACTIVITY_DATETIME] DESC ";
            }
          

          
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", companyId);
            if (!String.IsNullOrEmpty(subAdminId))
            {
                cmd.Parameters.AddWithValue("@SubAdminId", subAdminId);
            }
            if (!(processType == SUBADMIN_Device_LOG.ALL || processType == SUBADMIN_Device_LOG.ALL))
            {
                cmd.Parameters.AddWithValue("@ProcessType", processType);
            }

            cmd.Parameters.AddWithValue("@FromTime", fromTime);
            cmd.Parameters.AddWithValue("@ToTime", toTime);

            return modifiedDataset(MSSqlClient.SelectDataFromSQlCommand(cmd), companyId);
        }


        static DataSet modifiedDataset(DataSet rawSubAdminActivityLog, string companyId)
        {
            StringBuilder sbLog = new StringBuilder();
            TimeZoneInfo tzi = CompanyTimezone.getTimezoneInfo(companyId);
            DataTable dtblModifiedActivityLog = new DataTable();
            dtblModifiedActivityLog.Columns.Add("LOG_ID", typeof(string));
            dtblModifiedActivityLog.Columns.Add("LOG", typeof(string));
            dtblModifiedActivityLog.Columns.Add("DATE", typeof(string));
            if (rawSubAdminActivityLog != null)
            {
                DataTable dtblSubAdminLog = rawSubAdminActivityLog.Tables[0];
                string strSubAdminUsername = "", strSubAdminFullName = "";
                foreach (DataRow row in dtblSubAdminLog.Rows)
                {
                    sbLog = new StringBuilder();
                    string strUserName = Convert.ToString(row["USER_NAME"]);
                    strSubAdminFullName = Convert.ToString(row["SA_FULL_NAME"]);
                    strSubAdminUsername = Convert.ToString(row["SUBADMIN_NAME"]);

                    SUBADMIN_Device_LOG eSubAdminLogType = (SUBADMIN_Device_LOG)Enum.Parse(typeof(SUBADMIN_Device_LOG)
                        , Convert.ToInt32(row["PROCESS_TYPE"]).ToString());
                    switch (eSubAdminLogType)
                    {
                        case SUBADMIN_Device_LOG.ALL:
                            break;
                         case SUBADMIN_Device_LOG.MOBILE_DEVICE_APPROVE:
                            sbLog.Append(strSubAdminFullName).Append(SPACE_SEPARATOR)
                                .Append("approved device DeviceId:").Append(SPACE_SEPARATOR).Append(row["PARA2"])
                                .Append(" / Device Type: ").Append(row["PARA3"]).Append(" of user : ").Append(strUserName);
                            break;
                        case SUBADMIN_Device_LOG.MOBILE_DEVICE_DELETE:
                            sbLog.Append(strSubAdminFullName).Append(SPACE_SEPARATOR)
                                .Append("deleted device DeviceId:").Append(SPACE_SEPARATOR).Append(row["PARA2"])
                                .Append(" / Device Type: ").Append(row["PARA3"]).Append(" of user : ").Append(strUserName);
                            break;
                        case SUBADMIN_Device_LOG.MOBILE_DEVICE_DENY:
                            sbLog.Append(strSubAdminFullName).Append(SPACE_SEPARATOR)
                                .Append("denied device DeviceId:").Append(SPACE_SEPARATOR).Append(row["PARA2"])
                                .Append(" / Device Type: ").Append(row["PARA3"]).Append(" of user : ").Append(strUserName);
                            break;
                        case SUBADMIN_Device_LOG.MOBILE_DEVICE_DELETE_APPROVE:
                            break;
                       
                    }

                    DataRow newRow = dtblModifiedActivityLog.NewRow();
                    newRow["LOG_ID"] = Convert.ToString(row["PROCESS_ID"]);
                    newRow["LOG"] = sbLog.ToString();
                    newRow["DATE"] = Utilities.getCompanyLocalFormattedDateTime(tzi, Convert.ToInt64(row["ACTIVITY_DATETIME"]));
                    dtblModifiedActivityLog.Rows.Add(newRow);
                }
                DataSet dsModifiedActivityLog = new DataSet();
                dsModifiedActivityLog.Tables.Add(dtblModifiedActivityLog);
                return dsModifiedActivityLog;
            }
            else
            {
                return null;
            }
        }
    }
}
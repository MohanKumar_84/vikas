﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Runtime.Serialization;

//namespace mFicientCP
//{
//    public class UserDeletedorBlockedRequest
//    {
//        #region Private Members

//        private string strMsgRefId, strJson, strUserName, strMsgType, strEnterpriseId;

//        private UserDeletedorBlockedRequestJson userDeletedorBlockedRequestJson;

//        #endregion

//        #region Public Properties

//        public string RequestJson
//        {
//            get
//            {
//                return strJson;
//            }
//        }

//        #endregion

//        #region Constructor

//        public UserDeletedorBlockedRequest(string _msgRefId, string _userName, string _msgType, string _enterpriseId)
//        {
//            userDeletedorBlockedRequestJson = new UserDeletedorBlockedRequestJson();
//            strMsgRefId = _msgRefId;
//            strUserName = _userName;
//            strMsgType = _msgType;
//            strEnterpriseId = _enterpriseId;
//            strJson = CreateRequestJson();
//        }

//        #endregion

//        #region Private Methods

//        private string CreateRequestJson()
//        {
//            userDeletedorBlockedRequestJson.mrid = strMsgRefId;
//            userDeletedorBlockedRequestJson.type = strMsgType;
//            userDeletedorBlockedRequestJson.unm = strUserName;
//            userDeletedorBlockedRequestJson.enid = strEnterpriseId;

//            string json = Utilities.SerializeJson<UserDeletedorBlockedRequestJson>(userDeletedorBlockedRequestJson);
//            return Utilities.getRequestJson(json);
//        }

//        #endregion
//    }

//    #region Classes

//    [DataContract]
//    public class UserDeletedorBlockedRequestJson : RequestJson
//    {
//        public UserDeletedorBlockedRequestJson()
//        { }
//    }

//    #endregion
//}
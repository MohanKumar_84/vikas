﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetUserCompleteDetails
    {
        public GetUserCompleteDetails()
        {

        }
        public GetUserCompleteDetails(string userId, string companyId)
        {
            UserId = userId;
            this.CompanyId = companyId;
        }
        public void Process()
        {
            try
            {
                string strQuery = @"SELECT * FROM  TBL_USER_DETAIL userDetail left outer join
                                    TBL_DESIGNATION as userDesg on userDesg.DESIGNATION_ID = userDetail.DESIGNATION_ID left outer join
                                    TBL_SUB_ADMIN as subAdmin on subAdmin.SUBADMIN_ID = userDetail.SUBADMIN_ID left outer join
                                    TBL_REGION as region on region.REGION_ID = userDetail.REGION_ID left outer join
                                    TBL_LOCATION as location on location.LOCATION_ID = userDetail.LOCATION_ID and location.REGION_ID = userDetail.REGION_ID inner join
                                    TBL_COMPANY_DETAIL as company on company.COMPANY_ID = userDetail.COMPANY_ID
                                    WHERE userDetail.USER_ID=@USER_ID AND userDetail.COMPANY_ID = @CompanyId;";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@USER_ID", UserId);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.CommandType = CommandType.Text;
                DataSet dsUserDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsUserDtls != null && dsUserDtls.Tables[0].Rows.Count > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    UserDetails = dsUserDtls.Tables[0];
                    this.UserName = Convert.ToString(dsUserDtls.Tables[0].Rows[0]["USER_NAME"]);
                    this.FirstName = Convert.ToString(dsUserDtls.Tables[0].Rows[0]["FIRST_NAME"]);
                    this.LastName = Convert.ToString(dsUserDtls.Tables[0].Rows[0]["LAST_NAME"]);
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        public List<MFEUserGroupLinkAndGroup> GetUserGroupLinkAndGrpDtl()
        {
            List<MFEUserGroupLinkAndGroup> lstMfeUsrGrpLnkAndGrp =
                new List<MFEUserGroupLinkAndGroup>();
            try
            {
                string strQuery = @"SELECT * FROM TBL_USER_GROUP_LINK AS userGroupLink INNER JOIN 
                                    TBL_USER_GROUP AS userGroup ON userGroupLink.GROUP_ID = userGroup.GROUP_ID 
                                    where userGroupLink.USER_ID = @USER_ID AND userGroupLink.COMPANY_ID =@CompanyId;";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@USER_ID", UserId);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.CommandType = CommandType.Text;
                DataSet dsGroupsDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsGroupsDtls != null)
                {
                    UserGroups = dsGroupsDtls.Tables[0];
                    lstMfeUsrGrpLnkAndGrp = getUsrGrpLinkAndGrpFromDatatable(dsGroupsDtls.Tables[0]);
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            return lstMfeUsrGrpLnkAndGrp;
        }
        List<MFEUserGroupLinkAndGroup> getUsrGrpLinkAndGrpFromDatatable(DataTable usrGrpAndGrpDtble)
        {
            if (usrGrpAndGrpDtble == null) throw new ArgumentNullException();
            List<MFEUserGroupLinkAndGroup> lstUsrGrpLnkAndGrp = new List<MFEUserGroupLinkAndGroup>();
            foreach (DataRow row in usrGrpAndGrpDtble.Rows)
            {
                lstUsrGrpLnkAndGrp.Add(getUsrGrpLinkAndGrpFromDatarow(row));
            }
            return lstUsrGrpLnkAndGrp;
        }
        MFEUserGroupLinkAndGroup getUsrGrpLinkAndGrpFromDatarow(DataRow usrGrpAndGrpDrow)
        {
            if (usrGrpAndGrpDrow == null) throw new ArgumentNullException();
            MFEUserGroupLinkAndGroup objUsrGrpAndGrpDtl = new MFEUserGroupLinkAndGroup();
            MFEUserGroupLink objUsrGrpLink = new MFEUserGroupLink();
            MFEGroupDetail objGrpDtl = new MFEGroupDetail();

            objUsrGrpLink.EnterpriseId = Convert.ToString(usrGrpAndGrpDrow["COMPANY_ID"]);
            objUsrGrpLink.GroupId = Convert.ToString(usrGrpAndGrpDrow["GROUP_ID"]);
            objUsrGrpLink.UserId = Convert.ToString(usrGrpAndGrpDrow["USER_ID"]);
            objUsrGrpLink.CreatedOn = Convert.ToInt64(usrGrpAndGrpDrow["CREATED_ON"]);

            objUsrGrpAndGrpDtl.UserGroupLink = objUsrGrpLink;

            objGrpDtl.EnterpriseId = Convert.ToString(usrGrpAndGrpDrow["COMPANY_ID"]);
            objGrpDtl.GroupId = Convert.ToString(usrGrpAndGrpDrow["GROUP_ID"]);
            objGrpDtl.GroupName = Convert.ToString(usrGrpAndGrpDrow["GROUP_NAME"]);
            objGrpDtl.SubadminId = Convert.ToString(usrGrpAndGrpDrow["SUBADMIN_ID"]);
            objGrpDtl.CreatedOn = Convert.ToInt64(usrGrpAndGrpDrow["CREATED_ON"]);

            objUsrGrpAndGrpDtl.GroupDtl = objGrpDtl;

            return objUsrGrpAndGrpDtl;
        }
        public void getUsrDtlDeptDivAndGroup()
        {
            try
            {
                string strQuery = @"SELECT * FROM  TBL_USER_DETAIL userDetail Left outer join
                                    TBL_DESIGNATION as userDesg on userDesg.DESIGNATION_ID = userDetail.DESIGNATION_ID Left outer join
                                    TBL_REGION as region on region.REGION_ID = userDetail.REGION_ID Left outer join
                                    TBL_LOCATION as location on location.LOCATION_ID = userDetail.LOCATION_ID and location.REGION_ID = userDetail.REGION_ID inner join
                                    TBL_COMPANY_DETAIL as company on company.COMPANY_ID = userDetail.COMPANY_ID
                                    WHERE userDetail.USER_ID=@USER_ID AND userDetail.COMPANY_ID = @CompanyId;
                                    SELECT distinct SUBSTRING((select ' ,'+ d.DEPARTMENT_NAME from TBL_USER_DIVISION_DEP dd inner join 
                                    TBL_DEPARTMENT d on dd.DEPARTMENT_ID=d.DEPARTMENT_ID WHERE dd.USER_ID =u.USER_ID 
                                    FOR XML PATH('')),3,100)as Departments ,di.DIVISION_NAME
                                    FROM TBL_USER_DETAIL u inner join TBL_DESIGNATION desg on u.DESIGNATION_ID = desg.DESIGNATION_ID
                                    inner join TBL_USER_DIVISION_DEP de on  de.USER_ID =u.USER_ID 
                                    inner join TBL_DIVISION di on  di.DIVISION_ID=de.DIVISION_ID
                                     where  u.USER_ID=@USER_ID AND  u.COMPANY_ID = @CompanyId;

                                    SELECT distinct SUBSTRING((select ' ,'+ usrGrp.GROUP_NAME from TBL_USER_GROUP usrGrp inner join 
                                    TBL_USER_GROUP_LINK grpLnk on usrGrp.GROUP_ID=grpLnk.GROUP_ID
                                    WHERE grpLnk.USER_ID =usr.USER_ID 
                                    FOR XML PATH('')),3,100)as Groups
                                    FROM TBL_USER_DETAIL usr
                                    where  usr.USER_ID=@USER_ID AND  usr.COMPANY_ID = @CompanyId;

                            SELECT * INTO #TestTable from (
                                select  w.WF_Name,usrGrp.GROUP_NAME  from TBL_WORKFLOW_DETAIL as w
                                inner join TBL_WORKFLOW_AND_GROUP_LINK as w1  on  w1.WORKFLOW_ID=w.WF_ID
                                inner join  TBL_USER_GROUP_LINK as ut on ut.GROUP_ID=w1.GROUP_ID
                                INNER JOIN TBL_USER_GROUP AS usrGrp ON usrGrp.GROUP_ID=ut.GROUP_ID
                                where ut.USER_ID=@USER_ID and usrGrp.COMPANY_ID = @CompanyId
                                Group by w.WF_Name,ut.USER_ID,GROUP_NAME)w
                            select WF_NAME,STUFF((select ',' + T.GROUP_NAME from #TestTable as T where ss.WF_NAME=T.WF_NAME for xml path('')),1,1,'') as GROUP_NAME
                            from #TestTable as ss group by ss.WF_NAME DROP TABLE #TestTable";

                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@USER_ID", UserId);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.CommandType = CommandType.Text;
                DataSet dsUserDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsUserDtls != null)
                {

                    UserDtlWithDivDeptAndGroups = dsUserDtls;
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        public void GetUserDepartment()
        {
            try
            {
                string strQuery = @"SELECT * FROM TBL_USER_DIVISION_DEP as userDept INNER JOIN
                                    TBL_DEPARTMENT AS dept on dept.DEPARTMENT_ID = userDept.DEPARTMENT_ID INNER JOIN
                                    TBL_DIVISION AS division on division.DIVISION_ID = userDept.DIVISION_ID
                                    WHERE userDept.USER_ID = @USER_ID AND userDept.COMPANY_ID = @CompanyId;";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@USER_ID", UserId);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.CommandType = CommandType.Text;
                DataSet dsGroupsDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsGroupsDtls != null)
                {

                    UserDepartments = dsGroupsDtls.Tables[0];
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }
        public DataTable UserDetails
        {
            set;
            get;
        }
        public DataTable UserDepartments
        {
            set;
            get;
        }
        public DataTable UserGroups
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string UserId
        {
            get;
            set;
        }
        public DataSet UserDtlWithDivDeptAndGroups
        {
            set;
            get;
        }

        public string UserName
        {
            get;
            set;
        }
        public string CompanyId
        {
            get;
            set;
        }
        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }
    }
}
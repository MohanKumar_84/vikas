﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class CompanyTimezone
    {
        private static volatile CompanyTimezone obj;
        private static string _timeZoneId;
        private static TimeZoneInfo _cmpTimeZoneInfo;

        public static TimeZoneInfo CmpTimeZoneInfo
        {
            get { return CompanyTimezone._cmpTimeZoneInfo; }
            private set { CompanyTimezone._cmpTimeZoneInfo = value; }
        }
        public static string TimeZoneId
        {
            get { return CompanyTimezone._timeZoneId; }
            private set { CompanyTimezone._timeZoneId = value; }
        }

        private CompanyTimezone()
        {
            //httpRequest = new HttpRequestJson();
        }
        private static void checkCreateSingleton()
        {
            if (obj == null)
                obj = new CompanyTimezone();
        }


        private static string getTimezoneIdString(string companyId)
        {
            GetCompanyDetails objCmpDtls = new
            GetCompanyDetails(companyId);
            objCmpDtls.Process();

            if (objCmpDtls.StatusCode == 0)
            {
                return objCmpDtls.TimeZoneId;
            }
            else
            {
                return String.Empty;
            }
        }
        /// <summary>
        /// Returns the timezone info of the company.
        /// returns TimeZoneInfo.Local if the saved Timezone Id is not found.
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static TimeZoneInfo getTimezoneInfo(string companyId)
        {
            string strTimezoneIdString = getTimezoneIdString(companyId);
            TimeZoneId = strTimezoneIdString;
            TimeZoneInfo tzi;
            try
            {
                tzi = TimeZoneInfo.FindSystemTimeZoneById(strTimezoneIdString);
            }
            catch (TimeZoneNotFoundException)
            {
                tzi = TimeZoneInfo.Local;
            }
            catch (InvalidTimeZoneException)
            {
                tzi = TimeZoneInfo.Local;
            }
            CmpTimeZoneInfo = tzi;
            return tzi;
        }
        #region PreviousCode
        //private static string _timeZoneString;
        //private static int _timeZoneOffset;
        //public static int TimeZoneOffset
        //{
        //    get { return CompanyTimezone._timeZoneOffset; }
        //    set { CompanyTimezone._timeZoneOffset = value; }
        //}
        //public static string TimezoneString
        //{
        //    get { return _timeZoneString; }
        //    set { _timeZoneString = value; }
        //}

        //public static string getCompanyTimeZoneString(string companyId)
        //{
        //    try
        //    {
        //        string strTimezone = getTimezoneString((string)getCompanyCountryCode(companyId).Rows[0]["COUNTRY_CODE"]);
        //        return strTimezone;
        //    }
        //    catch
        //    {
        //        return String.Empty;
        //    }
        //}
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns>Timezone offset if successfull else int.minvalue for error</returns>
        //public static int getTimezoneIdString(string companyId)
        //{
        //    try
        //    {
        //        int iTimezone = getTimezoneOffset((string)getCompanyCountryCode(companyId).Rows[0]["COUNTRY_CODE"]);
        //        TimeZoneOffset = iTimezone;
        //        return iTimezone;
        //    }
        //    catch
        //    {
        //        return int.MinValue;//for error
        //    }
        //}
        //        static DataTable getCompanyCountryCode(string companyId)
        //        {
        //            try
        //            {
        //                string strQuery = @"SELECT COUNTRY_CODE FROM ADMIN_TBL_COMPANY_DETAIL
        //                                    WHERE COMPANY_ID = @CompanyId;";
        //                SqlCommand objSqlCommand = new SqlCommand(strQuery);
        //                objSqlCommand.CommandType = CommandType.Text;
        //                objSqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
        //                DataSet dsCountryCode = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
        //                if (dsCountryCode.Tables[0].Rows.Count > 0)
        //                {
        //                    return dsCountryCode.Tables[0];
        //                }
        //                else
        //                {
        //                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }
        //        }
        //        static string getTimezoneString(string countryCode)
        //        {
        //            try
        //            {
        //                string strQuery = @"SELECT * FROM ADMIN_TBL_MST_COUNTRY
        //                                    WHERE COUNTRY_CODE = @CountryCode;";
        //                SqlCommand objSqlCommand = new SqlCommand(strQuery);
        //                objSqlCommand.CommandType = CommandType.Text;
        //                objSqlCommand.Parameters.AddWithValue("@CountryCode", countryCode);
        //                DataSet dsCountryMaster = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
        //                if (dsCountryMaster.Tables[0].Rows.Count > 0)
        //                {
        //                    return (string)dsCountryMaster.Tables[0].Rows[0]["TIMEZONE_STRING"];
        //                }
        //                else
        //                {
        //                    throw new Exception("Country code does not exist");
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }
        //        }
        //        static int getTimezoneOffset(string countryCode)
        //        {
        //            try
        //            {
        //                string strQuery = @"SELECT * FROM TBL_MST_COUNTRY
        //                                    WHERE COUNTRY_CODE = @CountryCode;";
        //                SqlCommand objSqlCommand = new SqlCommand(strQuery);
        //                objSqlCommand.CommandType = CommandType.Text;
        //                objSqlCommand.Parameters.AddWithValue("@CountryCode", countryCode);
        //                DataSet dsCountryMaster = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
        //                if (dsCountryMaster.Tables[0].Rows.Count > 0)
        //                {
        //                    return Convert.ToInt32(dsCountryMaster.Tables[0].Rows[0]["TIMEZONE_OFFSET"]);
        //                }
        //                else
        //                {
        //                    throw new Exception("Country code does not exist");
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }
        //        }
        #endregion
    }
}
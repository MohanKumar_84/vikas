﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
namespace mFicientCP
{
    public enum EMAIL_FOR
    {
        CP_PLAN_ORDER_REQUEST,
        CP_DEVICE_REGISTRATION,
        CP_USER_CREATED,
        CP_DEVICE_DELETED,
        CP_PASSWORD_CHANGED,
        CP_RESET_PASSWORD,
        CP_QUERY_PROCESS,
        CP_SUB_ADMIN_CREATED,
        CP_DEVICE_DENIED,
        WEB_ENQUIRY_ID,
        ADMIN_COMPANY_CREATE,
        ADMIN_COMPANY_APPROVE,
        ADMIN_PLAN_ORDER_PROCESS,
        WS_DEVICE_REGISTRATION,
        MBUZZ_CONTACT_REQ_PROCESS,
        ADMIN_CHANGE_PASSWORD,
        WS_CHANGE_PASSWORD,
        ADMIN_RESET_PASSWORD,
        WS_RESET_PASSWORD,
        ADMIN_QUERY_PROCESS,
        WS_QUERY_PROCESS,
    }
    public class SaveEmailInfo
    {
        #region member variables
        //EMAIL_FOR _emailFor;
        //string _emailIdOfUser, _emailSubject, _statusDescription;
        //int _statusCode;
        //Hashtable _hashParamToReplace;

        #endregion
        #region Public Properties
        //public EMAIL_FOR EmailFor
        //{
        //    get { return _emailFor; }
        //}
        //public string EmailSubject
        //{
        //    get { return _emailSubject; }
        //}

        //public string EmailIdOfUser
        //{
        //    get { return _emailIdOfUser; }
        //}
        //public string StatusDescription
        //{
        //    get { return _statusDescription; }
        //}
        //public int StatusCode
        //{
        //    get { return _statusCode; }
        //}
        //public IEmailTemplateValues IEmailValues
        //{
        //    get { return _IEmailValues; }
        //}
        #endregion

        public enum RESET_PWD_USER_SEARCH_TYPE
        {
            UserName,
            UserId
        }
        public enum MGRAM_EMAIL_TYPE
        {
            NoReply = 0,
        }
        static string getEmailTemplateName(EMAIL_FOR emailFor)
        {
            string strEmailTemplateName = "";
            switch (emailFor)
            {
                case EMAIL_FOR.CP_DEVICE_DELETED:
                    strEmailTemplateName = "DeviceDeleted";
                    break;
                case EMAIL_FOR.CP_DEVICE_REGISTRATION:
                    strEmailTemplateName = "DeviceApproved";
                    break;
                case EMAIL_FOR.CP_PASSWORD_CHANGED:
                    strEmailTemplateName = "ChangePassword";
                    break;
                case EMAIL_FOR.CP_RESET_PASSWORD:
                    strEmailTemplateName = "ResetPassword";
                    break;
                case EMAIL_FOR.CP_PLAN_ORDER_REQUEST:
                    strEmailTemplateName = "Test Template";
                    break;
                case EMAIL_FOR.CP_QUERY_PROCESS:
                    strEmailTemplateName = "Test Template";
                    break;
                case EMAIL_FOR.CP_USER_CREATED:
                    strEmailTemplateName = "NewUserAdded";
                    break;
                case EMAIL_FOR.CP_SUB_ADMIN_CREATED:
                    strEmailTemplateName = "SubAdminAdded";
                    break;
                case EMAIL_FOR.CP_DEVICE_DENIED:
                    strEmailTemplateName = "DeviceDenied";
                    break;
            }
            return strEmailTemplateName;
        }

        static string getEmailSubject(EMAIL_FOR emailFor)
        {
            string strEmailSubject = "";
            switch (emailFor)
            {
                case EMAIL_FOR.CP_DEVICE_DELETED:
                    strEmailSubject = "Device deleted";
                    break;
                case EMAIL_FOR.CP_DEVICE_REGISTRATION:
                    strEmailSubject = "Device approved";
                    break;
                case EMAIL_FOR.CP_PASSWORD_CHANGED:
                    strEmailSubject = "mficient Credentials";
                    break;
                case EMAIL_FOR.CP_RESET_PASSWORD:
                    strEmailSubject = "mficient Credentials";
                    break;
                case EMAIL_FOR.CP_PLAN_ORDER_REQUEST:
                    strEmailSubject = "Test Subject";
                    break;
                case EMAIL_FOR.CP_QUERY_PROCESS:
                    strEmailSubject = "Test Subject";
                    break;
                case EMAIL_FOR.CP_USER_CREATED:
                    strEmailSubject = "User created";
                    break;
                case EMAIL_FOR.CP_SUB_ADMIN_CREATED:
                    strEmailSubject = "Subadmin created";
                    break;
                case EMAIL_FOR.CP_DEVICE_DENIED:
                    strEmailSubject = "Device denied";
                    break;
            }
            return strEmailSubject;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailIdOfUser"></param>
        /// <param name="emailSubject"></param>
        /// <param name="parametersToReplace"></param>
        /// <returns>True if successfull</returns>
        static bool saveEmailInfo(string emailIdOfUser,
            string emailSubject,
            Hashtable parametersToReplace, EMAIL_FOR emailFor,
            HttpContext appContext)
        {
            string strEmailBody = "";
            string strEmailDBUniqueID = Utilities.GetMd5Hash(emailIdOfUser + DateTime.UtcNow.Ticks.ToString());
            string strEmailTemplateKey = getEmailTemplateKey(emailFor);
            string strfileContent = getFileContentFromCache(strEmailTemplateKey);
            if (String.IsNullOrEmpty(strfileContent))
            {
                strfileContent = getFileContentFromTextFile(emailFor, appContext);
                if (!String.IsNullOrEmpty(strfileContent))
                    saveTemplateInCache(strEmailTemplateKey, strfileContent);
            }
            if (!String.IsNullOrEmpty(strfileContent))
            {
                strEmailBody = getEmailBodyAfterReplacingParams(parametersToReplace, strfileContent);
                /**THis is a completely different database,so if at any time the database is changed
                 * remember to see if the process has to be completed in transaction or not.
                 * if the process has to completed in transaction then make an overload of this method
                 * with connection and transaction.
                 * **/
                int iRowsAffected = insertInMgramTable(strEmailDBUniqueID,
                    emailIdOfUser, emailSubject, strEmailBody);
                if (iRowsAffected > 0)
                {
                    return true;
                }
                return false;
            }
            else
            {
                return false;
            }
        }
        static void saveTemplateInCache(string key, string value)
        {
            CacheManager.Insert<string>(key, value, DateTime.UtcNow, TimeSpan.FromSeconds(MficientConstants.SESSION_VALIDITY_SECONDS));
        }
        static string getEmailTemplateKey(EMAIL_FOR emailFor)
        {
            return CacheManager.GetKey(
                 CacheManager.CacheType.mFicientEmail,
                 String.Empty, String.Empty,
                 String.Empty, String.Empty,
                 String.Empty, ((int)emailFor).ToString()
                 );
        }
        static string getFileContentFromCache(string key)
        {
            return CacheManager.Get<string>(key);
        }
        static string getFileContentFromTextFile(EMAIL_FOR emailFor, HttpContext appContext)
        {
            string strFileContent = String.Empty;
            try
            {
                string strPath = appContext.Server.MapPath("EmailTemplates\\") + getEmailTemplateName(emailFor) + ".txt";
                FileInfo fileEmailTemplate = new FileInfo(strPath);
                if (fileEmailTemplate != null)
                {
                    using (StreamReader sreader = fileEmailTemplate.OpenText())
                    {
                        // Use the StreamReader object...
                        strFileContent = sreader.ReadToEnd();
                    }
                }
            }
            catch
            { }
            return strFileContent;
        }
        static string getEmailBodyAfterReplacingParams(Hashtable parametersToReplace,
            string fileContent)
        {
            string strEmailBody = String.Empty;
            Hashtable hashKeyPositions = new Hashtable();
            foreach (DictionaryEntry hashEntry in parametersToReplace)
            {
                int iIndexOFParam = getIndexOfParamToReplace(fileContent, Convert.ToString(hashEntry.Key));
                EmailTemplateParameterNameValue objParameterNameValue =
                    getValueForParam(parametersToReplace,
                        Convert.ToString(hashEntry.Key),
                        Convert.ToString(hashEntry.Value)
                     );
                fileContent = fileContent.Insert(iIndexOFParam, Convert.ToString(objParameterNameValue.ParameterValue));
                iIndexOFParam = getIndexOfParamToReplace(fileContent, Convert.ToString(hashEntry.Key));
                fileContent = fileContent.Remove(iIndexOFParam, objParameterNameValue.ParameterName.Length);
            }
            //if for some reason some of the parameters are not replaced then find that pattern and replace it with empty string.
            //string strPattern = @"\b<%[A-Za-z0-9 ]%>\b";
            string strPattern = @"<%[A-Za-z0-9 ]*%>";
            bool isLoopCompleted = false;
            do
            {
                foreach (DictionaryEntry hashEntry in hashKeyPositions)
                {
                    MatchCollection matches = System.Text.RegularExpressions.Regex.Matches(fileContent, strPattern);
                    EmailTemplateParameterNameValue parameterNameValue = (EmailTemplateParameterNameValue)hashEntry.Value;
                    foreach (Match match in matches)
                    {
                        if (parameterNameValue.ParameterName != match.Value)
                        {
                            fileContent = fileContent.Remove(match.Index, match.Value.Length);//because when thee characters are deleted then others position changes.
                            break;
                        }
                    }
                }
                int iCountOfMatchesAfterReplacing = 0;
                foreach (DictionaryEntry hashEntry in hashKeyPositions)
                {
                    MatchCollection matches = System.Text.RegularExpressions.Regex.Matches(fileContent, strPattern);
                    iCountOfMatchesAfterReplacing = iCountOfMatchesAfterReplacing + matches.Count;
                }
                if (iCountOfMatchesAfterReplacing > 0)
                {
                    isLoopCompleted = false;
                }
                else
                {
                    isLoopCompleted = true;
                }
            } while (isLoopCompleted == false);

            strEmailBody = fileContent;
            return strEmailBody;
        }
        static int getIndexOfParamToReplace(string fileContent, string paramName)
        {
            return fileContent.IndexOf("<%" + paramName + "%>");
        }
        static EmailTemplateParameterNameValue getValueForParam(Hashtable paramsWithValue, string paramName, string value)
        {
            EmailTemplateParameterNameValue objParameterNameValue = new EmailTemplateParameterNameValue();
            objParameterNameValue.ParameterName = "<%" + paramName + "%>";
            objParameterNameValue.ParameterValue = Convert.ToString(paramsWithValue[paramName]);
            return objParameterNameValue;
        }
        static int insertInMgramTable(string uniqueId,
            string emailId,
            string emailSubject,
            string emailBody)
        {
            SqlConnection con = new SqlConnection();
            MSSqlClient.SqlConnectionOpen(
                                out con,
                                MSSqlClient.getConnectionStringFromWebConfig(MSSqlClient.CONNECTION_STRING_FOR_DB.MGRAM));
            SqlCommand cmd = getSqlCommand(con, uniqueId, emailId, emailSubject, emailBody);

            return cmd.ExecuteNonQuery();
        }

        static string getSqlQuery()
        {
            return @"INSERT INTO ADMIN_TBL_EMAIL_OUTBOX
                   ([EMAIL_ID],[EMAIL]
                   ,[SUBJECT],[BODY]
                   ,[POST_TIME],[SEND_TIME]
                   ,[STATUS],[EMAIL_TYPE]
                   ,[NO_OF_ATTEMPTS],[NEXT_ATTEMPT_TIME])
             VALUES
                   (@EMAIL_ID,@EMAIL
                   ,@SUBJECT,@BODY
                   ,@POST_TIME,@SEND_TIME
                   ,@STATUS,@EMAIL_TYPE
                   ,@NO_OF_ATTEMPTS ,@NEXT_ATTEMPT_TIME)";
        }
        static SqlCommand getSqlCommand(SqlConnection con, string uniqueID,
            string emailID, string subject, string body)
        {
            SqlCommand cmd = new SqlCommand(getSqlQuery(), con);
            cmd.Parameters.AddWithValue("@EMAIL_ID", uniqueID);
            cmd.Parameters.AddWithValue("@EMAIL", emailID);
            cmd.Parameters.AddWithValue("@SUBJECT", subject);
            cmd.Parameters.AddWithValue("@BODY", body);
            cmd.Parameters.AddWithValue("@POST_TIME", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@SEND_TIME", 0);
            cmd.Parameters.AddWithValue("@STATUS", 0);
            cmd.Parameters.AddWithValue("@EMAIL_TYPE", ((int)MGRAM_EMAIL_TYPE.NoReply));//No Reply
            cmd.Parameters.AddWithValue("@NO_OF_ATTEMPTS", 0);
            cmd.Parameters.AddWithValue("@NEXT_ATTEMPT_TIME", 0);
            return cmd;
        }

        #region Various Processes
        public static void cpChangePassword(
            string emailId,
            string username, string password,
            long utcDateInTicks, TimeZoneInfo tzInfo,
            HttpContext context)
        {
            Hashtable hashParamToReplace = new Hashtable();
            hashParamToReplace.Add("username", username);
            hashParamToReplace.Add("ChangeDate", Utilities.getFullCompanyLocalFormattedDate(tzInfo, utcDateInTicks));
            //hashParamToReplace.Add("Password", password);
            saveEmailInfo(
                emailId,
                getEmailSubject(EMAIL_FOR.CP_PASSWORD_CHANGED),
                hashParamToReplace,
                EMAIL_FOR.CP_PASSWORD_CHANGED, context
                );
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="emailId"></param>
        /// <param name="companyId"></param>
        /// <param name="userType"></param>
        /// <param name="resetCode"></param>
        /// <param name="context"></param>
        /// <returns>True if process was successful</returns>
        public static bool cpResetPassword(
            string userName, string emailId,
            string companyId, string userType,
            string resetCode, HttpContext context)
        {
            Hashtable hashParamToReplace = new Hashtable();
            hashParamToReplace.Add("Username", userName);
            hashParamToReplace.Add("ResetCode", resetCode);
            hashParamToReplace.Add("Link", MficientConstants.ENTERPRISE_URL + "/resetPassword.aspx?d=" + Utilities.EncryptString(userName + "~" + companyId + "~" + userType));
            return saveEmailInfo(
                 emailId,
                 getEmailSubject(EMAIL_FOR.CP_RESET_PASSWORD),
                 hashParamToReplace,
                 EMAIL_FOR.CP_RESET_PASSWORD, context
                 );
        }
        public static bool cpUserAdded(
            string subAdminName, string username,
            string userFullName, string fullDate,
            string companyId,string userEmailId,
            HttpContext context)
        {
            Hashtable hashParamToReplace = new Hashtable();
            hashParamToReplace.Add("UserFullname", userFullName);
            hashParamToReplace.Add("SubAdminName", subAdminName);
            hashParamToReplace.Add("Date",fullDate);
            hashParamToReplace.Add("UsersUserName", username);
            return saveEmailInfo(
                 userEmailId,
                 getEmailSubject(EMAIL_FOR.CP_USER_CREATED),
                 hashParamToReplace,
                 EMAIL_FOR.CP_USER_CREATED, context
                 );
        }

        public static bool cpSubAdminAdded(
            string username,
            string saFullName, string fullDate,
            string companyId, string userEmailId,
            string adminName,string password,
            HttpContext context)
        {
            Hashtable hashParamToReplace = new Hashtable();
            hashParamToReplace.Add("SAFullname", saFullName);
            hashParamToReplace.Add("AdminName", adminName);
            hashParamToReplace.Add("Date", fullDate);
            hashParamToReplace.Add("EnterpriseId", companyId);
            hashParamToReplace.Add("SAUserName", username);
            hashParamToReplace.Add("Password", password);
            return saveEmailInfo(
                 userEmailId,
                 getEmailSubject(EMAIL_FOR.CP_SUB_ADMIN_CREATED),
                 hashParamToReplace,
                 EMAIL_FOR.CP_SUB_ADMIN_CREATED, context
                 );
        }

        public static bool cpDeviceApproved(
            string username,
            string saFullName, string fullDate,
            string companyId, string userEmailId,
            string deviceId,string deviceType,string osWithModel,
            HttpContext context)
        {
            Hashtable hashParamToReplace = new Hashtable();
            hashParamToReplace.Add("username", username);
            hashParamToReplace.Add("OS", osWithModel);
            hashParamToReplace.Add("DeviceId", deviceId);
            hashParamToReplace.Add("DeviceType", deviceType);
            hashParamToReplace.Add("subadminname", saFullName);
            hashParamToReplace.Add("date", fullDate);
            return saveEmailInfo(
                 userEmailId,
                 getEmailSubject(EMAIL_FOR.CP_DEVICE_REGISTRATION),
                 hashParamToReplace,
                 EMAIL_FOR.CP_DEVICE_REGISTRATION, context
                 );
        }

        public static bool cpDeviceDeleted(
            string username,
            string saFullName, string fullDate,
            string companyId, string userEmailId,
            string deviceId, string deviceType, string osWithModel,
            HttpContext context)
        {
            Hashtable hashParamToReplace = new Hashtable();
            hashParamToReplace.Add("username", username);
            hashParamToReplace.Add("OS", osWithModel);
            hashParamToReplace.Add("DeviceId", deviceId);
            hashParamToReplace.Add("DeviceType", deviceType);
            hashParamToReplace.Add("subadminname", saFullName);
            hashParamToReplace.Add("date", fullDate);
            return saveEmailInfo(
                 userEmailId,
                 getEmailSubject(EMAIL_FOR.CP_DEVICE_DELETED),
                 hashParamToReplace,
                 EMAIL_FOR.CP_DEVICE_DELETED, context
                 );
        }

        public static bool cpDeviceDenied(
            string username,
            string saFullName, string fullDate,
            string companyId, string userEmailId,
            string deviceId, string deviceType, string osWithModel,
            HttpContext context)
        {
            Hashtable hashParamToReplace = new Hashtable();
            hashParamToReplace.Add("username", username);
            hashParamToReplace.Add("OS", osWithModel);
            hashParamToReplace.Add("DeviceId", deviceId);
            hashParamToReplace.Add("DeviceType", deviceType);
            hashParamToReplace.Add("subadminname", saFullName);
            hashParamToReplace.Add("date", fullDate);
            return saveEmailInfo(
                 userEmailId,
                 getEmailSubject(EMAIL_FOR.CP_DEVICE_DENIED),
                 hashParamToReplace,
                 EMAIL_FOR.CP_DEVICE_DENIED, context
                 );
        }
        #endregion
        #region Get email id to send
        //static List<string> getPlanOrderReqEmailIds(string adminId, string companyId)
        //{
        //    List<string> emailIds = null;
        //    GetCompanyAdministrator objCmpAdmin = new GetCompanyAdministrator(adminId, companyId);
        //    objCmpAdmin.Process();
        //    if (objCmpAdmin.StatusCode == 0)
        //    {
        //        GetCompanyResellerDetail objReseller = new GetCompanyResellerDetail(objCmpAdmin.ResellerId);
        //        objReseller.Process();
        //        if (objReseller.StatusCode == 0)
        //        {
        //            emailIds = new List<string>();
        //            emailIds.Add(objReseller.EmailId);
        //        }
        //    }
        //    return emailIds;
        //}
        //static List<string> getDeviceRegisteredEmailIds(string userId, string companyId, string subAdminId, string adminId)
        //{
        //    List<string> emailIds = null;
        //    GetUserDetail objUserDetail = new GetUserDetail(false, false, subAdminId, userId, "", companyId);
        //    objUserDetail.Process();
        //    if (objUserDetail.StatusCode == 0)
        //    {
        //        emailIds.Add(objUserDetail.EmailId);
        //        GetSubAdminDetail objSubAdminDtl = new GetSubAdminDetail(false, true, adminId, "", "", companyId);
        //        objSubAdminDtl.Process();
        //        if (objSubAdminDtl.StatusCode == 0)
        //        {
        //            emailIds.Add(objSubAdminDtl.EmailId);
        //        }
        //    }
        //    return emailIds;
        //}
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="userId">Sub Admin or Admin Id</param>
        ///// <param name="companyId"></param>
        ///// <param name="adminId"></param>
        ///// <param name="isAdmin"></param>
        ///// <returns></returns>
        //static List<string> getChangePasswordEmailIds(string userId, string companyId, string adminId, bool isAdmin)
        //{
        //    List<string> emailIds = null;
        //    if (isAdmin)
        //    {
        //        GetCompanyAdministrator objAdmin = new GetCompanyAdministrator(userId, companyId);
        //        objAdmin.Process();
        //        if (objAdmin.StatusCode == 0)
        //        {
        //            emailIds.Add(objAdmin.EmailId);
        //        }
        //    }
        //    else
        //    {
        //        GetSubAdminDetail objSubAdminDtl = new GetSubAdminDetail(false, true, adminId, userId, "", companyId);
        //        objSubAdminDtl.Process();
        //        if (objSubAdminDtl.StatusCode == 0)
        //        {
        //            emailIds.Add(objSubAdminDtl.EmailId);
        //        }
        //    }
        //    return emailIds;
        //}
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="searchBy"></param>
        ///// <param name="userName"></param>
        ///// <param name="companyId"></param>
        ///// <param name="userId"></param>
        ///// <param name="isSubAdmin">Pass a value if search is by userId.If search by user id then the user is subadmin or not.</param>
        ///// <param name="subAdminId">Pass a value if userid to search is neither a subadmin or admin</param>
        ///// <param name="adminId">Pass Adminid if the user to search is subadmin</param>
        ///// <returns></returns>
        //static List<string> getResetPasswordEmailId(RESET_PWD_USER_SEARCH_TYPE searchBy, string userName, string companyId
        //                                            , string userId, bool isSubAdmin, string subAdminId, string adminId)
        //{
        //    List<string> emailIds = null;

        //    switch (searchBy)
        //    {
        //        case RESET_PWD_USER_SEARCH_TYPE.UserId:
        //            if (isSubAdmin)
        //            {
        //                GetSubAdminDetail objSubAdminDtl = getSubAdminDtl(adminId, subAdminId, companyId);
        //                emailIds.Add(objSubAdminDtl.EmailId);
        //            }
        //            else
        //            {
        //                GetUserDetail objUserDetail = getUserDetail(userId, subAdminId, companyId);
        //                emailIds.Add(objUserDetail.EmailId);
        //            }
        //            break;
        //        case RESET_PWD_USER_SEARCH_TYPE.UserName:
        //            GetUserByUserName objUserDetails = new GetUserByUserName(userName, companyId);
        //            objUserDetails.Process();
        //            emailIds.Add(objUserDetails.EmailId);
        //            break;
        //    }

        //    return emailIds;
        //}

        //static GetSubAdminDetail getSubAdminDtl(string adminId, string subAdminId, string companyId)
        //{
        //    try
        //    {
        //        GetSubAdminDetail objSubAdminDtl = new GetSubAdminDetail(false, true, adminId, subAdminId, "", companyId);
        //        objSubAdminDtl.Process();
        //        return objSubAdminDtl;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}

        //static GetUserDetail getUserDetail(string userId, string subAdminId, string companyId)
        //{
        //    try
        //    {
        //        GetUserDetail objUsrDetail = new GetUserDetail(false, false, subAdminId, userId, "", companyId);
        //        objUsrDetail.Process();
        //        return objUsrDetail;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}
        #endregion
    }

}
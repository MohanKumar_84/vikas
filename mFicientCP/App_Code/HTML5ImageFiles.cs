﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Collections;

namespace mFicientCP
{
    public class HTML5ImageFiles : ICollection<HTML5AppFile>
    {
        private List<HTML5AppFile> imageFiles;
        private object collectionLock;

        public HTML5ImageFiles()
        {
            imageFiles = new List<HTML5AppFile>();
            collectionLock = new object();
        }

        int ICollection<HTML5AppFile>.Count
        {
            get
            {
                return imageFiles.Count;
            }
        }

        public List<HTML5AppFile> Files
        {
            get
            {
                return imageFiles;
            }
        }

        public void Add(HTML5AppFile item)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    if (!imageFiles.Contains(item))
                        imageFiles.Add(item);
                }
                catch
                {
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        public void AddRange(List<HTML5AppFile> files)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    imageFiles.AddRange(files);
                }
                catch
                {

                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        bool ICollection<HTML5AppFile>.Remove(HTML5AppFile item)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    if (imageFiles.Contains(item))
                    {
                        imageFiles.Remove(item);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
            return false;
        }

        public void RemoveAt(int index)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    imageFiles.RemoveAt(index);
                }
                catch
                {

                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        public void RemoveAll(Predicate<HTML5AppFile> files)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    imageFiles.RemoveAll(files);
                }
                catch
                {
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        public void Clear()
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    imageFiles.Clear();
                }
                catch
                {

                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
        }

        public bool Contains(HTML5AppFile item)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    if (imageFiles.Contains(item))
                        return true;
                    else
                        return false;
                }
                catch
                {
                    return false;
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
            return false;
        }

        public HTML5AppFile GetFile(string filename)
        {
            if (Monitor.TryEnter(collectionLock, 3000))
            {
                try
                {
                    foreach (HTML5AppFile file in imageFiles)
                    {
                        if (file.FileName == filename)
                        {
                            return file;
                        }
                    }
                }
                catch
                {
                    return null;
                }
                finally
                {
                    Monitor.Exit(collectionLock);
                }
            }
            return null;
        }

        public void CopyTo(HTML5AppFile[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public IEnumerator<HTML5AppFile> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
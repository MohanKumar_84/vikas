﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class BlockOrUnBlockUser
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="subAdminId"></param>
        /// <param name="userId"></param>
        /// <param name="BlockOrUnblock">Block or UnBlock</param>
        public BlockOrUnBlockUser(string subAdminId, string userId, string blockOrUnblock, string companyId)
        {
            this.UserId = userId;
            this.SubAdminId = subAdminId;
            this.BlockOrUnblock = blockOrUnblock;
            this.CompanyId = companyId;
        }
        public void Process()
        {
            try
            {
                string UserName = "", Name = ""; ;
                GetRegisteredDeviceDetailsByUserId objRegisteredUserDevices = new GetRegisteredDeviceDetailsByUserId(this.CompanyId, this.SubAdminId, this.UserId);
                GetUserDetail objUserDetail = new GetUserDetail(false, true, this.SubAdminId, this.UserId, "", this.CompanyId);
                try
                {
                    objRegisteredUserDevices.Process();
                    objUserDetail.Process();
                    DataTable dt = objUserDetail.ResultTable;
                    UserName = dt.Rows[0]["USER_NAME"].ToString();
                    Name = dt.Rows[0]["FIRST_NAME"].ToString() + " " + dt.Rows[0]["LAST_NAME"].ToString();
                }
                catch
                {
                    StatusCode = -1000;
                    StatusDescription = "Internal server error";
                    return;
                }
                //MBuzz User is deleleted from aspx page.
                SqlTransaction sqlTransaction = null;
                SqlConnection con = null;
                try
                {
                    MSSqlClient.SqlConnectionOpen(out con);
                    using (con)
                    {
                        using (sqlTransaction = con.BeginTransaction())
                        {
                            blockUnBlockUser(con, sqlTransaction);
                            if (this.BlockOrUnblock.ToLower().Trim() == "block")
                            {
                                DeleteMobileSessionOfUser objDeleteMobileSessAndMbuzzMsgs = new DeleteMobileSessionOfUser(this.UserId, this.CompanyId, this.SubAdminId, USER_LOG_OUT_TYPE.USER_BLOCKED, DeleteMobileSessionOfUser.MBUZZ_PENDING_MSGS_DELETE_TYPE.User, true, con, sqlTransaction, null);
                                objDeleteMobileSessAndMbuzzMsgs.ProcessInTransaction(objUserDetail, objRegisteredUserDevices, 0);
                            }
                            //commit
                            sqlTransaction.Commit();

                            if (this.BlockOrUnblock == "Block")
                                Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.USER_BLOCKED, this.CompanyId, this.SubAdminId, this.UserId, UserName, Name, "", "", "", "", "");
                            else if (this.BlockOrUnblock == "UnBlock")
                                Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.USER_UNBLOCK, this.CompanyId, this.SubAdminId, this.UserId, UserName, Name, "", "", "", "", "");

                            StatusCode = 0;
                            StatusDescription = "";
                        }
                    }
                }
                catch (SqlException ex)
                {
                    if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                        StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                    }
                    StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;

                }
                catch (Exception ex)
                {
                    if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                        StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                    }
                    StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;

                }
                finally
                {
                    if (con != null)
                    {
                        con.Dispose();
                    }
                    if (sqlTransaction != null)
                    {
                        sqlTransaction.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
            }
        }

        private int blockUnBlockUser(SqlConnection con, SqlTransaction transaction)
        {
            string query = getSqlQuery();

            SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
            objSqlCommand.CommandType = CommandType.Text;

            objSqlCommand.Parameters.AddWithValue("@UserId", this.UserId);
            objSqlCommand.Parameters.AddWithValue("@SubAdminId", this.SubAdminId);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            return objSqlCommand.ExecuteNonQuery();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="process">Block or Unblock</param>
        private string getSqlQuery()
        {
            string sqlQuery;
            if (BlockOrUnblock.ToLower() == "block")
            {
                sqlQuery = @"UPDATE TBL_USER_DETAIL
                                SET IS_BLOCKED = 1
                                WHERE USER_ID = @UserId
                                AND SUBADMIN_ID = @SubAdminId AND  COMPANY_ID=@COMPANY_ID;";
            }
            else
            {
                sqlQuery = @"UPDATE TBL_USER_DETAIL
                                SET IS_BLOCKED = 0
                                WHERE USER_ID = @UserId
                                AND SUBADMIN_ID = @SubAdminId AND  COMPANY_ID=@COMPANY_ID;";
            }
            return sqlQuery;
        }
        //        private DataTable getRegisteredDeviceOfUser()
        //        {
        //            try
        //            {
        //                string strQuery = @"SELECT device.*,usr.USER_ID,usr.USER_NAME,(usr.FIRST_NAME +' '+usr.LAST_NAME)AS FULL_NAME 
        //                                     FROM TBL_REGISTERED_DEVICE device
        //                                     INNER JOIN TBL_USER_DETAIL as usr
        //                                     ON usr.USER_ID = device.USER_ID
        //                                     WHERE device.COMPANY_ID =@CompanyId
        //                                     AND device.SUBADMIN_ID=@SubAdminId;";
        //                SqlCommand objSqlCommand = new SqlCommand(strQuery);
        //                objSqlCommand.CommandType = CommandType.Text;
        //                objSqlCommand.Parameters.AddWithValue("@SubAdminId", this.SubAdminId);
        //                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
        //                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
        //                if (objDataSet.Tables.Count > 0 && objDataSet.Tables[0].Rows.Count > 0)
        //                {
        //                    return objDataSet.Tables[0];
        //                }
        //                else
        //                {
        //                    throw new Exception("Internal server error");
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                throw e;
        //            }

        //        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string UserId
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string BlockOrUnblock
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
    }
}
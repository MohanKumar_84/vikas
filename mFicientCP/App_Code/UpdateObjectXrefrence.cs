﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;

namespace mFicientCP
{
    public class UpdateObjectXreference
    {
        public UpdateObjectXreference()
        {
        }
        //string DataObjectsList = "", WsObjectsList = "", oDataObjectsList = "", odbObjectsList = "";
        List<ObjectXRefrence> lstRef = new List<ObjectXRefrence>();
        ObjectXRefrence ObjRef;
        List<string> containsList = new List<string>();
        public void UpdateObjectXreferenceForPublishApp(string companyId, string appId)
        {
            string Query = "select APP_JS_JSON from TBL_CURR_WORKFLOW_DETAIL where COMPANY_ID=@COMPANY_ID and WF_ID=@WF_ID";
            SqlCommand cmd1 = new System.Data.SqlClient.SqlCommand(Query);
            cmd1.Parameters.AddWithValue("COMPANY_ID", companyId);
            cmd1.Parameters.AddWithValue("WF_ID", appId);
            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(cmd1);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows )
                {
                    jqueryAppClass appJson = Utilities.DeserialiseJson<jqueryAppClass>(Convert.ToString(dr["APP_JS_JSON"]));
                    if (appJson.startUpTasks != null && appJson.startUpTasks.databindingObjs != null && appJson.startUpTasks.databindingObjs.Count > 0)
                    {
                        foreach (AppCommands DBobject in appJson.startUpTasks.databindingObjs)
                        {
                            if (!containsList.Contains(DBobject.id))
                            {
                                containsList.Add(DBobject.id);
                                ObjRef = new ObjectXRefrence();
                                //ObjRef.Type = DBobject.bindObjType;
                                ObjRef.ID = DBobject.id;
                                ObjRef.appStartup = true;
                                lstRef.Add(ObjRef);
                                if (DBobject.bindObjType == "1")
                                {
                                 //   DataObjectsList = "," + DBobject.id;
                                    ObjRef.ID ="DB"+ DBobject.id;
                                }
                                else if (DBobject.bindObjType == "5")
                                {
                                //    oDataObjectsList = "," + DBobject.id;
                                    ObjRef.ID = "OD" + DBobject.id;
                                }
                                else if (DBobject.bindObjType == "6")
                                {
                                //    odbObjectsList = "," + DBobject.id;
                                    ObjRef.ID = "OF" + DBobject.id;
                                }
                                else
                                {
                                 //   WsObjectsList = "," + DBobject.id;
                                    ObjRef.ID = "WS" + DBobject.id;
                                }
                            }
                        }
                    }
                    foreach (Appviews view in appJson.views)
                    {
                        //-------------------------Next button -----
                        if (Convert.ToInt32(view.nextButton) == 1)
                            if (view.onExitCmds != null && view.onExitCmds.databindingObjs != null && view.onExitCmds.databindingObjs.Count > 0)
                                foreach (AppCommands cmd in view.onExitCmds.databindingObjs)
                                    addUpdateXref(cmd, view.name, true);
                        //------------------cancel button-------
                        if (Convert.ToInt32(view.cancelButton) == 1)
                            if (view.cancelTasks != null && view.cancelTasks.databindingObjs != null && view.cancelTasks.databindingObjs.Count > 0)
                                foreach (AppCommands cmd in view.cancelTasks.databindingObjs)
                                    addUpdateXref(cmd, view.name, true);
                        //----------------row Click------
                        if (Convert.ToInt32(view.rowClickAction) == 1)
                            if (view.rowClickTasks != null && view.rowClickTasks.databindingObjs != null && view.rowClickTasks.databindingObjs.Count > 0)
                                foreach (AppCommands cmd in view.rowClickTasks.databindingObjs)
                                    addUpdateXref(cmd, view.name, true);
                        //---------------List Action button------
                        if (view.actionBtn1Tasks != null && view.actionBtn1Tasks.databindingObjs != null && view.actionBtn1Tasks.databindingObjs.Count > 0)
                            foreach (AppCommands cmd in view.actionBtn1Tasks.databindingObjs)
                                addUpdateXref(cmd, view.name, true);
                        if (view.actionBtn2Tasks != null && view.actionBtn2Tasks.databindingObjs != null && view.actionBtn2Tasks.databindingObjs.Count > 0)
                            foreach (AppCommands cmd in view.actionBtn2Tasks.databindingObjs)
                                addUpdateXref(cmd, view.name, true);
                        if (view.actionBtn3Tasks != null && view.actionBtn3Tasks.databindingObjs != null && view.actionBtn3Tasks.databindingObjs.Count > 0)
                            foreach (AppCommands cmd in view.actionBtn3Tasks.databindingObjs)
                                addUpdateXref(cmd, view.name, true);
                        if (view.formActionBtns != null && view.formActionBtns.Count>0)
                        {
                            foreach(ViewActionButton fb in view.formActionBtns  )
                            {
                                if (fb.onExitTasks != null && fb.onExitTasks.databindingObjs != null && fb.onExitTasks.databindingObjs.Count > 0)
                                    foreach (AppCommands cmd in fb.onExitTasks.databindingObjs)
                                        addUpdateXref(cmd, view.name, true);
                            }                            
                        }
                        if (view.customTriggers != null && view.customTriggers.Count > 0)
                        {
                            foreach (ViewCustomTrigger custom in view.customTriggers)
                            {
                                if (custom.onExitTasks!=null && custom.onExitTasks.databindingObjs != null && custom.onExitTasks.databindingObjs.Count > 0)
                                    foreach (AppCommands cmd in custom.onExitTasks.databindingObjs)
                                        addUpdateXref(cmd, view.name, true);
                            }
                        }
                        //-----------------view Action sheet-----------
                        if (!string.IsNullOrEmpty(view.enableViewActionBtns) && Convert.ToInt32(view.enableViewActionBtns) == 1)
                        {
                            if (view.viewActionBtns != null)
                                foreach (ViewActionButton objbtn in view.viewActionBtns)
                                    if (objbtn != null && objbtn.onExitTasks != null && objbtn.onExitTasks.databindingObjs.Count > 0)
                                        foreach (AppCommands cmd in view.cancelTasks.databindingObjs)
                                            addUpdateXref(cmd, view.name, true);
                        }
                    }

                    foreach (AppForm forms in appJson.forms)
                        foreach (sectionRowPanels row in forms.rowPanels)
                            foreach (SectionColPanels col in row.colmnPanels)
                                foreach (Controls ctrl in col.controls)
                                    if (ctrl.databindObjs != null)
                                        foreach (AppCommands cmd in ctrl.databindObjs)
                                            addUpdateXref(cmd, forms.name, false);
                }
            }

            Query = "Update TBL_CURR_WORKFLOW_DETAIL set Command_ids=@Command_ids where COMPANY_ID=@COMPANY_ID and WF_ID=@WF_ID";
            cmd1 = new SqlCommand(Query);
            cmd1.Parameters.AddWithValue("COMPANY_ID", companyId);
            cmd1.Parameters.AddWithValue("WF_ID", appId);
            cmd1.Parameters.AddWithValue("Command_ids", Utilities.SerializeJson <List<ObjectXRefrence>>(lstRef));
            MSSqlClient.ExecuteNonQueryRecord(cmd1);
        }
        void addUpdateXref( AppCommands cmd, string ViewId, bool isView)
        {
            if (cmd != null)
            {
                ObjectXRefrence ObjRef;
                string cmdid = "";
               
                if (!containsList.Contains(cmd.id))
                {
                    if (cmd.bindObjType == "1") cmdid = "DB_" + cmd.id;
                    else if (cmd.bindObjType == "5")cmdid = "OD_" + cmd.id;
                    else if (cmd.bindObjType == "6")cmdid = "OF_" + cmd.id;
                    else cmdid = "WS_" + cmd.id;

                    containsList.Add(cmd.id);
                    ObjRef = new ObjectXRefrence();
                    ObjRef.ID = cmdid;
                    if (isView) ObjRef.Views.Add(ViewId);
                    else ObjRef.Forms.Add(ViewId);
                    lstRef.Add(ObjRef);
                    //----------------------
                }
                else
                {
                    ObjRef = lstRef[containsList.IndexOf(cmd.id)];
                    if (isView)
                        if (!ObjRef.Views.Contains(ViewId))
                            ObjRef.Views.Add(ViewId);
                        else
                            if (!ObjRef.Forms.Contains(ViewId))
                                ObjRef.Forms.Add(ViewId);
                }
            }
        }
        public DataSet GetObjectXreference(string companyId, string ObjectId,int ObjectType)
        {
            string dataConnection = "";
            if (ObjectType == 1) dataConnection = "DB_";
            if (ObjectType == 2) dataConnection = "WS_";
            if (ObjectType == 3) dataConnection = "OD_";
            if (ObjectType == 4) dataConnection = "OF_";

            string Query = "Select WF_NAME,Command_ids from TBL_CURR_WORKFLOW_DETAIL where COMPANY_ID=@COMPANY_ID and Command_ids like '%" + dataConnection + ObjectId + "%'";
            SqlCommand cmd1 = new SqlCommand(Query);
            cmd1.Parameters.AddWithValue("COMPANY_ID", companyId);
            return MSSqlClient.SelectDataFromSQlCommand(cmd1);
        }

        public DataTable GetAppsListForObjectReff(string companyId)
        {
            string Query = "Select WF_NAME,Command_ids from TBL_CURR_WORKFLOW_DETAIL where COMPANY_ID=@COMPANY_ID ";
            SqlCommand cmd1 = new SqlCommand(Query);
            cmd1.Parameters.AddWithValue("COMPANY_ID", companyId);
            DataSet ds= MSSqlClient.SelectDataFromSQlCommand(cmd1);
            if (ds != null) return ds.Tables[0];
            else
            {
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("WF_NAME"));
                dt.Columns.Add(new DataColumn("Command_ids"));
                return dt;
            }
        }
    }

    [DataContract]
    public class ObjectXRefrence
    {
        public ObjectXRefrence()
        {
            Views = new List<string>();
            Forms = new List<string>();
        }
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public bool appStartup { get; set; }
        [DataMember]
        public List<string> Views { get; set; }
        [DataMember]
        public List<string> Forms { get; set; }
    }
}

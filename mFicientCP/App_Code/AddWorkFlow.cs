﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class AddWorkFlow
    {
        public AddWorkFlow()
        {

        }

        public AddWorkFlow(string _WFId, string _WfName, string _WfDescription, string _WfJson, string _Forms, string _SubAdminId, Boolean _IsMain, string _ParentId, string _CompanyId, Boolean _IsDiffForTablet, Boolean _AppForTablet,string _CommandIds)
        {
            this.WFId = _WFId;
            this.WfName = _WfName;
            this.WfDescription = _WfDescription;
            this.WfJson = _WfJson;
            this.Forms = _Forms;
            this.SubAdminId = _SubAdminId;
            this.ParentId = _ParentId;
            this.IsMain = _IsMain;
            this.CompanyId = _CompanyId;
            this.IsDiffForTablet = _IsDiffForTablet;
            this.AppForTablet = _AppForTablet;
            this.CommandIds = _CommandIds;
            
        }
        public void Process()
        {
            this.StatusCode = -1000;
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            SqlCommand objSqlCommand;
            string query;
            try
            {
                
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);

                query = @"INSERT INTO TBL_WORKFLOW_DETAIL(WF_ID,WF_NAME,WF_DESCRIPTION,WF_JSON,WORKED_BY,UPDATE_ON,SUBADMIN_ID,FORMS,PARENT_ID,COMPANY_ID,MODEL_TYPE,COMMAND_IDS)
                                    VALUES(@WF_ID,@WF_NAME,@WF_DESCRIPTION,@WF_JSON,@WORKED_BY,@UPDATE_ON,@SUBADMIN_ID,@FORMS,@PARENT_ID,@COMPANY_ID,@MODEL_TYPE,@COMMAND_IDS);
                            insert into TBL_WORKFLOW_ADDITIONAL_DETAIL (COMPANY_ID,WORKFLOW_ID,ALL_GROUPS,UPDATED_ON,WF_ICON) values (@COMPANY_ID,@WF_ID,0,@UPDATE_ON,'')"; 
                objSqlTransaction = objSqlConnection.BeginTransaction();
                if (!this.AppForTablet)
                {
                    this.WFId = Utilities.GetMd5Hash(this.SubAdminId + this.WfName + DateTime.UtcNow.Ticks.ToString());
                    if (this.IsMain)
                        this.ParentId = this.WFId;

                    objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WFId);
                    objSqlCommand.Parameters.AddWithValue("@WF_NAME", this.WfName);
                    objSqlCommand.Parameters.AddWithValue("@WF_DESCRIPTION", this.WfDescription);
                    objSqlCommand.Parameters.AddWithValue("@WF_JSON", this.WfJson);
                    objSqlCommand.Parameters.AddWithValue("@WORKED_BY", "");
                    objSqlCommand.Parameters.AddWithValue("@UPDATE_ON", DateTime.UtcNow.Ticks);
                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                    objSqlCommand.Parameters.AddWithValue("@FORMS", this.Forms);
                    objSqlCommand.Parameters.AddWithValue("@PARENT_ID", this.ParentId);
                    objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", MficientConstants.IDE_MODEL_TYPE_PHONE);
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    objSqlCommand.Parameters.AddWithValue("@COMMAND_IDS", this.CommandIds);
                    if (objSqlCommand.ExecuteNonQuery() > 0)
                        this.StatusCode = 0;
                    else
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                else
                {
                    if (this.IsMain)
                        this.ParentId = this.WFId;
                }
                if (IsDiffForTablet)
                {
                    objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@WF_ID", this.WFId);
                    objSqlCommand.Parameters.AddWithValue("@WF_NAME", this.WfName);
                    objSqlCommand.Parameters.AddWithValue("@WF_DESCRIPTION", this.WfDescription);
                    objSqlCommand.Parameters.AddWithValue("@WF_JSON", this.WfJson);
                    objSqlCommand.Parameters.AddWithValue("@WORKED_BY", "");
                    objSqlCommand.Parameters.AddWithValue("@UPDATE_ON", DateTime.UtcNow.Ticks);
                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                    objSqlCommand.Parameters.AddWithValue("@FORMS", this.Forms);
                    objSqlCommand.Parameters.AddWithValue("@PARENT_ID", this.ParentId);
                    objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", MficientConstants.IDE_MODEL_TYPE_TABLET);
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    objSqlCommand.Parameters.AddWithValue("@COMMAND_IDS", this.CommandIds);
                    if (objSqlCommand.ExecuteNonQuery() > 0)
                        this.StatusCode = 0;
                    else
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                if (!this.AppForTablet)
                {
                    if (this.IsMain)
                    {
//                        this.MasterPageId = Utilities.GetMd5Hash(this.SubAdminId + this.WfName + "Default" + DateTime.UtcNow.Ticks.ToString());
//                        query = @"INSERT INTO TBL_MASTER_PAGE_DETAILS(COMPANY_ID,MASTER_PAGE_ID,MASTER_NAME,MASTER_DESCRIPTION,
//                                MASTER_JSON,CREATED_BY,MODIFIED_BY,
//                                MODIFIED_DATE,IS_ACTIVE,HEADER_HTML,FOOTER_HTML,PARENT_ID)
//                                VALUES(@COMPANY_ID,@MASTER_PAGE_ID,@MASTER_NAME,@MASTER_DESCRIPTION,@MASTER_JSON,@CREATED_BY,@MODIFIED_BY,
//                                @MODIFIED_DATE,@IS_ACTIVE,@HEADER_HTML,@FOOTER_HTML,@PARENT_ID);";

//                        objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
//                        objSqlCommand.CommandType = CommandType.Text;
//                        objSqlCommand.Parameters.AddWithValue("@MASTER_PAGE_ID", this.MasterPageId);
//                        objSqlCommand.Parameters.AddWithValue("@MASTER_NAME", "Master");
//                        objSqlCommand.Parameters.AddWithValue("@MASTER_DESCRIPTION", "");
//                        objSqlCommand.Parameters.AddWithValue("@MASTER_JSON", "");
//                        objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.SubAdminId);
//                        objSqlCommand.Parameters.AddWithValue("@MODIFIED_BY", "");
//                        objSqlCommand.Parameters.AddWithValue("@MODIFIED_DATE", DateTime.UtcNow.Ticks);
//                        objSqlCommand.Parameters.AddWithValue("@IS_ACTIVE", true);
//                        objSqlCommand.Parameters.AddWithValue("@HEADER_HTML", "");
//                        objSqlCommand.Parameters.AddWithValue("@FOOTER_HTML", "");
//                        objSqlCommand.Parameters.AddWithValue("@PARENT_ID", this.ParentId);
//                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

//                        if (objSqlCommand.ExecuteNonQuery() > 0)
//                            this.StatusCode = 0;
//                        else
//                            throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());

                    }
                }
                objSqlTransaction.Commit();
            }
            catch
            {
                this.StatusCode = -1000;
                this.MasterPageId = "";
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }
        public string WFId
        {
            set;
            get;
        }
        public string MasterPageId
        {
            set;
            get;
        }
        public string WfName
        {
            set;
            get;
        }
        public string WfDescription
        {
            set;
            get;
        }
        public string WfJson
        {
            set;
            get;
        }
        public string Forms
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }

        public string ParentId { get; set; }

        public bool Main { get; set; }

        public bool IsMain { get; set; }
        public string CompanyId
        {
            set;
            get;
        }


        public Boolean IsDiffForTablet { get; set; }

        public bool OnlyForTablet { get; set; }

        public bool AppForTablet { get; set; }

        public string CommandIds { get; set; }
    }
}
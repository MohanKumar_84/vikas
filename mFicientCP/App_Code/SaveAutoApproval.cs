﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class SaveAutoApproval
    {
        public SaveAutoApproval(string companyId, string userIds, string groupIds, string removeautoregistration)
        {
            this.CompanyId = companyId;
            this.UserIds = userIds;
            this.GroupIds = groupIds;
            this.RemoveAutoregistration = removeautoregistration;
        }

        public void Process()
        {
            this.StatusCode = -1000;
            SqlTransaction transaction = null;
            SqlConnection con;
            MSSqlClient.SqlConnectionOpen(out con);
            try
            {
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        saveUserDeviceApproval(
                            transaction,
                            con,
                            this.CompanyId,
                            this.UserIds, this.GroupIds
                            );
                        saveDeviceApprovalSettings(transaction,
                            con,
                            this.CompanyId,
                            this.RemoveAutoregistration
                            );
                        transaction.Commit();
                    }
                }
            }
            catch
            {
                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }


        void saveUserDeviceApproval(
           SqlTransaction transaction,
           SqlConnection con,
           string companyId,
           string  userid,
           string groupid)
        {
            string query = @"IF NOT EXISTS (SELECT * FROM dbo.TBL_USER_DEVICE_AUTOAPPROVE_SETTING WHERE  COMPANY_ID=@COMPANY_ID)
                          BEGIN insert into TBL_USER_DEVICE_AUTOAPPROVE_SETTING(COMPANY_ID,USER_IDS,GROUP_IDS) VALUES(@COMPANY_ID,@USER_IDS,@GROUP_IDS) END
                          ELSE BEGIN UPDATE TBL_USER_DEVICE_AUTOAPPROVE_SETTING SET USER_IDS = @USER_IDS, GROUP_IDS=@GROUP_IDS WHERE COMPANY_ID=@COMPANY_ID END;";
            SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
            objSqlCommand.Parameters.AddWithValue("@USER_IDS", userid);
            objSqlCommand.Parameters.AddWithValue("@GROUP_IDS", groupid);
            objSqlCommand.ExecuteNonQuery(); this.StatusCode = 0;
        }


        void saveDeviceApprovalSettings(
           SqlTransaction transaction,
           SqlConnection con,
           string companyId,
           string autoregistration
           )
        {
            string query = @"UPDATE TBL_ACCOUNT_SETTINGS SET REMOVE_OLDEST_DEVICE_ON_AUTOREGISTRATION =@REMOVE_OLDEST_DEVICE_ON_AUTOREGISTRATION  WHERE COMPANY_ID=@COMPANY_ID;";
            SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", companyId);
            objSqlCommand.Parameters.AddWithValue("@REMOVE_OLDEST_DEVICE_ON_AUTOREGISTRATION", autoregistration);
            objSqlCommand.ExecuteNonQuery(); this.StatusCode = 0;
            
        }



        public string CompanyId
        {
            set;
            get;
        }
        public string UserIds
        {
            set;
            get;
        }
        public string GroupIds
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public string RemoveAutoregistration
        {
            set;
            get;
        }


    }
   
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetTableColumnNameFromMssql
    {
        public GetTableColumnNameFromMssql()
        {

        }

        public GetTableColumnNameFromMssql(string _ConnectionString, string _TableName)
        {
            this.ConnectionString = _ConnectionString;
            this.TableName = _TableName;
            //this.IsSelectAll = _IsSelectAll;
            //this.SubAdminId = _SubAdminId;
            //this.ConnectionId = _ConnectionId;
            //this.ConnectionName = ConnectionName;
        }
        public void Process()
        {
            try
            {
                SqlConnection conn = new SqlConnection(this.ConnectionString);
                conn.Open();
                string query = @"SELECT OBJECT_NAME(c.OBJECT_ID) table_name, c.name column_name,t.name column_type,c.is_nullable
                                         FROM sys.columns AS c
                                         JOIN sys.types AS t ON c.user_type_id=t.user_type_id
                                         JOIN sysobjects ON c.OBJECT_ID = sysobjects.id
                                         WHERE sysobjects.xtype='U' 
                                        AND OBJECT_NAME(c.OBJECT_ID)=@Table_Name
                                        ORDER BY c.OBJECT_ID;";
                SqlCommand objSqlCommand = new SqlCommand(query, conn);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@Table_Name", this.TableName);
                DataSet ObjDataSet = new DataSet();
                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(ObjDataSet);
                this.ResultTable = ObjDataSet.Tables[0];
                conn.Close();
            }
            catch (Exception ex)
            {

                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }

        public string ConnectionString
        {
            set;
            get;
        }
        public string TableName
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
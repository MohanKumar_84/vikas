﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Runtime.Serialization;

//namespace mFicientCP
//{
//    public class DeviceDeletedRequest
//    {
//        #region Private Members

//        private string strMsgRefId, strJson, strUserName, strDeviceId, strEnterpriseId;
//        private int deviceCount;

//        private DeviceDeletedRequestJson deviceDeletedRequestJson;

//        #endregion

//        #region Public Properties

//        public string RequestJson
//        {
//            get
//            {
//                return strJson;
//            }
//        }

//        #endregion

//        #region Constructor

//        public DeviceDeletedRequest(string _msgRefId, string _userName, string _deviceId, string _enterpriseId, int _deviceCount)
//        {
//            deviceDeletedRequestJson = new DeviceDeletedRequestJson();
//            strMsgRefId = _msgRefId;
//            strUserName = _userName;
//            strDeviceId = _deviceId;
//            strEnterpriseId = _enterpriseId;
//            deviceCount = _deviceCount;
//            strJson = CreateRequestJson();
//        }

//        #endregion

//        #region Private Methods

//        private string CreateRequestJson()
//        {
//            deviceDeletedRequestJson.mrid = strMsgRefId;
//            deviceDeletedRequestJson.type = ((int)MessageCode.DEVICE_DELETED).ToString();
//            deviceDeletedRequestJson.unm = strUserName;
//            deviceDeletedRequestJson.did = strDeviceId;
//            deviceDeletedRequestJson.enid = strEnterpriseId;
//            deviceDeletedRequestJson.dcnt = deviceCount.ToString();

//            string json = Utilities.SerializeJson<DeviceDeletedRequestJson>(deviceDeletedRequestJson);
//            return Utilities.getRequestJson(json);
//        }

//        #endregion
//    }

//    #region Classes

//    [DataContract]
//    public class DeviceDeletedRequestJson : RequestJson
//    {
//        public DeviceDeletedRequestJson()
//        { }

//        [DataMember]
//        public string did { get; set; }

//        [DataMember]
//        public string dcnt { get; set; }
//    }

//    #endregion
//}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientCP
{
    [DataContract]
    public class RequestJson
    {
        public RequestJson() { }

        [DataMember]
        public string enid { get; set; }

        [DataMember]
        public string unm { get; set; }

        [DataMember]
        public string mrid { get; set; }

        [DataMember]
        public string type { get; set; }
    }
}
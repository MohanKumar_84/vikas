﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class DeleteSubAdmin
    {
        public DeleteSubAdmin()
        {

        }

       public DeleteSubAdmin(string _AdminId, string _SubAdminId)
        {
            this.AdminId = _AdminId;
            this.SubAdminId = _SubAdminId;
           
        }
        public void Process()
        {
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            MSSqlClient.SqlConnectionOpen(out objSqlConnection);
             StatusCode = -1000;
            try
            {
                objSqlTransaction = objSqlConnection.BeginTransaction();
                string query = @"Delete from TBL_SUB_ADMIN WHERE SUBADMIN_ID=@SUBADMIN_ID AND ADMIN_ID=@ADMIN_ID;";
                SqlCommand objSqlCommand = new SqlCommand(query ,objSqlConnection, objSqlTransaction);
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@ISACTIVE",false);
                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
                }
                query = @"Delete from TBL_SUBADMIN_DIVISION_DEP WHERE SUBADMIN_ID=@SUBADMIN_ID;";
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.ExecuteNonQuery();

                query = @"Delete from TBL_SUBADMIN_ROLE WHERE SUBADMIN_ID=@SUBADMIN_ID;";
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.ExecuteNonQuery();

                objSqlTransaction.Commit();
               
            }
            catch
            {
                objSqlTransaction.Rollback();
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }
        public string AdminId
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
      
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
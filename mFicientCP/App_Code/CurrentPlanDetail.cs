﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCP
{
    public class CurrentPlanDetail
    {

        public CurrentPlanDetail(string _CompanyId)
        {
            CompanyId = _CompanyId;
        }
        public void Process()
        {
            this.StatusCode = -1000;//for error
            try
            {
                /*string strQuery = @"SELECT * FROM TBL_COMPANY_CURRENT_PLAN where COMPANY_ID=@COMPANY_ID;
                select IS_TRIAL,REGISTRATION_DATETIME from TBL_COMPANY_ADMINISTRATOR a inner join TBL_COMPANY_DETAIL c on a.ADMIN_ID=c.ADMIN_ID where c.COMPANY_ID=@COMPANY_ID";*/
                //changed on 17/9/2012
                bool blnIsTrial;
                string strQuery = @"SELECT *,CompCurrPlan.MAX_USER AS MaxUser,Plns.PLAN_CODE AS PlanCode,Plns.MAX_WORKFLOW AS MaxWorkFlow,Plns.PUSHMESSAGE_PERDAY AS PushMessagePerDay
                                ,Plns.PUSHMESSAGE_PERMONTH AS PushMessagePerMonth
                                FROM ADMIN_TBL_COMPANY_CURRENT_PLAN AS CompCurrPlan
                                INNER JOIN ADMIN_TBL_PLANS AS Plns
                                ON CompCurrPlan.PLAN_CODE = Plns.PLAN_CODE
                                WHERE CompCurrPlan.COMPANY_ID = @CompanyId;

                                SELECT CompCurrPlan.*,CmpAdmin.IS_TRIAL,CmpAdmin.REGISTRATION_DATETIME,Plns.PLAN_CODE AS PlanCode,Plns.MAX_WORKFLOW AS MaxWorkFlow,Plns.MAX_USER AS MaxUser,Plns.PUSHMESSAGE_PERDAY AS PushMessagePerDay
                                ,Plns.PUSHMESSAGE_PERMONTH AS PushMessagePerMonth,Plns.PLAN_NAME
                                FROM ADMIN_TBL_COMPANY_CURRENT_PLAN AS CompCurrPlan
                                INNER JOIN ADMIN_TBL_TRIAL_PLANS AS Plns
                                ON CompCurrPlan.PLAN_CODE = Plns.PLAN_CODE
                                INNER JOIN TBL_COMPANY_DETAIL AS CmpDtl
                                ON CmpDtl.COMPANY_ID  = CompCurrPlan.COMPANY_ID
                                INNER JOIN TBL_COMPANY_ADMINISTRATOR AS CmpAdmin
                                ON CmpDtl.ADMIN_ID = CmpAdmin.ADMIN_ID
                                WHERE CompCurrPlan.COMPANY_ID = @CompanyId;";


                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@CompanyId", CompanyId);

                cmd.CommandType = CommandType.Text;
                DataSet dsPriceDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsPriceDtls == null || (dsPriceDtls.Tables[0].Rows.Count == 0 && dsPriceDtls.Tables[1].Rows.Count == 0))
                {
                    throw new Exception();
                }
                if (dsPriceDtls.Tables[0].Rows.Count > 0)
                {
                    PlanDetails = dsPriceDtls.Tables[0];
                    PlanDetails.TableName = "Plan";
                    blnIsTrial = false;
                }
                else
                {
                    PlanDetails = dsPriceDtls.Tables[1];
                    PlanDetails.TableName = "Trial";
                    blnIsTrial = true;
                }

                this.CompanyPlanDetail = new CompanyPlan(PlanDetails, Convert.ToString(PlanDetails.Rows[0]["PLAN_NAME"]), blnIsTrial);
                GetPlanList objPlanList;
                DataRow[] rows = null;
                if (!blnIsTrial)
                {
                    objPlanList = new GetPlanList(Convert.ToString(PlanDetails.Rows[0]["CHARGE_TYPE"]));
                }
                else
                {
                    objPlanList = new GetPlanList(Convert.ToString(PlanDetails.Rows[0]["PLAN_CODE"]), "", 0, true);
                }
                objPlanList.Process();
                DataTable dtblPlanList = objPlanList.PlanDetails;
                if (dtblPlanList == null || dtblPlanList.Rows.Count == 0)
                {
                    throw new Exception();
                }
                if (!blnIsTrial)
                {
                    string filter = String.Format("PLAN_CODE LIKE '{0}'", Convert.ToString(PlanDetails.Rows[0]["PLAN_CODE"]));
                    rows = dtblPlanList.Select(filter);
                    if (rows.Length == 0)
                    {
                        throw new Exception();
                    }
                    this.CompanyPlanDetail.NextMonthPlanName = Convert.ToString(rows[0]["PLAN_NAME"]);
                    this.NextMonthPlanName = Convert.ToString(rows[0]["PLAN_NAME"]);
                    rows = dtblPlanList.Select("MAX_WORKFLOW=MAX(MAX_WORKFLOW)");
                    this.MinNoOfUsersForPlan = Convert.ToInt32(rows[0]["MIN_USERS"]);
                    this.HighestWorkFlowAvailable = Convert.ToInt32(rows[0]["MAX_WORKFLOW"]);
                    this.PlanNameWithHighestWF = Convert.ToString(rows[0]["PLAN_NAME"]);
                    this.PlanCodeWithHighestWF = Convert.ToString(rows[0]["PLAN_CODE"]);
                }
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch
            {
                //if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                //{
                //    throw ex;
                //}
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        #region Public Properties
        public DataTable PlanDetails
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string CompanyId
        {
            get;
            set;
        }
        public CompanyPlan CompanyPlanDetail
        {
            get;
            set;
        }
        public int HighestWorkFlowAvailable
        {
            get;
            set;
        }
        public string PlanNameWithHighestWF
        {
            get;
            set;
        }
        public string PlanCodeWithHighestWF
        {
            get;
            set;
        }
        public int MinNoOfUsersForPlan
        {
            get;
            set;
        }
        public string NextMonthPlanName
        {
            get;
            set;
        }

        #endregion
    }
}
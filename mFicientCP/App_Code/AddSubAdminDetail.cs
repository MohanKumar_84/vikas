﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;

namespace mFicientCP
{
    public class AddSubAdminDetail
    {
        HttpContext _context;

        
        public AddSubAdminDetail()
        {

        }
        
        public AddSubAdminDetail(string _AdminId, string _FullName, string _UserName, string _AccessCode, string _Email,
                            string _DivisionId, string _RoleId, byte _PushMessage, string _companyID,HttpContext context)
        {
            this.AdminId = _AdminId;
            this.FullName = _FullName;
            this.UserName = _UserName;
            this.AccessCode = _AccessCode;
            this.Email = _Email;
            this.DivisionId = _DivisionId;
            this.RoleId = _RoleId;
            this.PushMessage = _PushMessage;
            this.CompanyID = _companyID;
            this.Context = context;
        }

        public AddSubAdminDetail(string _AdminId, string _FullName, string _UserName, string _AccessCode, string _Email,
                               string _DivisionId, string _RoleId, byte _PushMessage, string _companyID,string _user, HttpContext context)
        {
            this.AdminId = _AdminId;
            this.FullName = _FullName;
            this.UserName = _UserName;
            this.AccessCode = _AccessCode;
            this.Email = _Email;
            this.DivisionId = _DivisionId;
            this.RoleId = _RoleId;
            this.PushMessage = _PushMessage;
            this.CompanyID = _companyID;
            this.Context = context;
            this.User = _user;
        }




        public void Process()
        {
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            //For Error.If the process succeeds then the status code is set to 0
            StatusCode = -1000;

            GetCompanyAdministrator objAdminDtl = getAdminDetailsAfterProcess();
            string fullDate = Utilities.getFullCompanyLocalFormattedDate(
                                CompanyTimezone.getTimezoneInfo(this.CompanyID),
                                DateTime.UtcNow.Ticks
                                );

            try
            {
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                this.SubAdminId = Utilities.GetMd5Hash(this.AdminId + this.UserName + DateTime.UtcNow.Ticks.ToString());
                string query = @" INSERT INTO TBL_SUB_ADMIN(COMPANY_ID,SUBADMIN_ID,ADMIN_ID,FULL_NAME,USER_NAME,ACCESS_CODE,EMAIL,ISACTIVE,PUSH_MESSAGE,IS_BLOCKED,MOBILE_USER)
                                        VALUES (@COMPANY_ID,@SUBADMIN_ID,@ADMIN_ID,@FULL_NAME,@USER_NAME,@ACCESS_CODE,@EMAIL,@ISACTIVE,@PUSH_MESSAGE,@IS_BLOCKED,@MOBILE_USER)";
                objSqlTransaction = objSqlConnection.BeginTransaction();
                string strPassword = Utilities.GetMd5Hash(this.AccessCode);
                SqlCommand objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@FULL_NAME", this.FullName);
                objSqlCommand.Parameters.AddWithValue("@USER_NAME", this.UserName.ToLower());
                objSqlCommand.Parameters.AddWithValue("@ACCESS_CODE", strPassword);
                objSqlCommand.Parameters.AddWithValue("@PUSH_MESSAGE", PushMessage);
                objSqlCommand.Parameters.AddWithValue("@EMAIL", this.Email.ToLower());
                objSqlCommand.Parameters.AddWithValue("@ISACTIVE", true);
                objSqlCommand.Parameters.AddWithValue("@IS_BLOCKED", false);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", CompanyID);
                objSqlCommand.Parameters.AddWithValue("@MOBILE_USER", this.User);

                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                string[] strArrayRoleId = this.RoleId.Split('&');
                foreach (string str in strArrayRoleId)
                {
                    if (str.Length != 0)
                    {
                        query = @"INSERT INTO TBL_SUBADMIN_ROLE(COMPANY_ID,SUBADMIN_ID,ROLE_ID,ADMIN_ID) values(@COMPANY_ID,@SUBADMIN_ID,@ROLE_ID,@ADMIN_ID);";
                        objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", CompanyID);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                        objSqlCommand.Parameters.AddWithValue("@ROLE_ID", str);
                        objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                        if (objSqlCommand.ExecuteNonQuery() > 0)
                        {
                            this.StatusCode = 0;
                            this.StatusDescription = "";
                        }
                        else
                        {
                            throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                        }
                    }
                }
                string[] strArrayDivDepId = this.DivisionId.Split('&');
                foreach (string str in strArrayDivDepId)
                {
                    if (str.Length != 0)
                    {
                        string strDivId = str.Split('@')[1];
                        string strRelatedDepId = str.Split('@')[0];
                        string[] strArrayDepId = strRelatedDepId.Split('#');
                        foreach (string strDepId in strArrayDepId)
                        {
                            if (strDepId.Length != 0)
                            {
                                query = @"INSERT INTO TBL_SUBADMIN_DIVISION_DEP(COMPANY_ID,SUBADMIN_ID,DIVISION_ID,DEPARTMENT_ID) values(@COMPANY_ID,@SUBADMIN_ID,@DIVISION_ID,@DEPARTMENT_ID);";

                                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                                objSqlCommand.CommandType = CommandType.Text;
                                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                                objSqlCommand.Parameters.AddWithValue("@DIVISION_ID", strDivId);
                                objSqlCommand.Parameters.AddWithValue("@DEPARTMENT_ID", strDepId);
                                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", CompanyID);

                                if (objSqlCommand.ExecuteNonQuery() > 0)
                                {
                                    this.StatusCode = 0;
                                    this.StatusDescription = "";
                                }
                                else
                                {
                                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                                }
                            }
                        }
                    }
                }
                try
                {
                    if (objAdminDtl.StatusCode == 0)
                    {
                        SaveEmailInfo.cpSubAdminAdded(
                            this.UserName,
                            this.FullName,
                            fullDate,
                            this.CompanyID,
                            this.Email, objAdminDtl.FirstName + " " + objAdminDtl.LastName,
                            this.AccessCode,
                            this.Context);

                    }
                }
                catch
                { }
                objSqlTransaction.Commit();
            }
            catch 
            {
                objSqlTransaction.Rollback();
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }



        GetCompanyAdministrator getAdminDetailsAfterProcess()
        {
            GetCompanyAdministrator objCmpAdmin =
                new GetCompanyAdministrator(this.AdminId);
            objCmpAdmin.ProcessUsingCpTable(this.CompanyID);
            return objCmpAdmin;
        }
        public string AdminId
        {
            get;
            set;
        }
        public string SubAdminId
        {
            get;
            set;
        }
        public string FullName
        {
            get;
            set;
        }
        public string UserName
        {
            get;
            set;
        }
        public string AccessCode
        {
            get;
            set;
        }
        public string Email
        {
            get;
            set;
        }
        public string DivisionId
        {
            get;
            set;
        }
        public string RoleId
        {
            get;
            set;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public byte PushMessage
        {
            set;
            get;
        }
        public string CompanyID
        {
            set;
            get;
        }

        public string User
        {
            set;
            get;
        }


        public HttpContext Context
        {
            get { return _context; }
            private set { _context = value; }
        }
    }
}
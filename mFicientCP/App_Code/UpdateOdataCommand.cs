﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class UpdateOdataCommand
    {
        public UpdateOdataCommand()
        { 
        }
        public UpdateOdataCommand(string _CompanyId, string _SubAdminId, string _ConnectorId, string _CommandId,
                                string _ResourceType, string _ResourcePath, string _additionalSingleValuePath, string _HttpType, string _HttpData, string _QueryOption,
                                string _InputParamJson, string _OutputParamJson, string _FunctionType, string _ResponseFormat, string _DataJson, string _ReturnType, string _DsPath, string _Description)
        {
            this.CompanyId = _CompanyId;
            this.SubAdminId = _SubAdminId;
            this.ConnectorId = _ConnectorId;
            this.CommandId = _CommandId;
            this.ResourceType = _ResourceType;
            this.ResourcePath = _ResourcePath;
            this.AdditionalSingleValuePath = _additionalSingleValuePath;
            this.HttpType = _HttpType;
            this.HttpData = _HttpData;
            this.QueryOption = _QueryOption;
            this.InputParamJson = _InputParamJson;
            this.OutputParamJson = _OutputParamJson;
            this.FunctionType = _FunctionType;
            this.ResponseFormat = _ResponseFormat;
            this.DataJson = _DataJson;
            this.ReturnType = _ReturnType;
            this.DsPath = _DsPath;
            this.Description = _Description;
            Process();
        }

       
        public UpdateOdataCommand(string _CompanyId, string _SubAdminId, string _ConnectorId, string _CommandId,
                                string _ResourceType, string _ResourcePath, string _additionalSingleValuePath, string _HttpType, string _HttpData, string _QueryOption,
                                string _InputParamJson, string _OutputParamJson, string _FunctionType, string _ResponseFormat, string _DataJson, string _ReturnType, string _DsPath, string _Description, int _Cache, int _Expfrequency, string _ExpCondtion, long _Updatedon, string _ReferenceJson)
        {
            this.CompanyId = _CompanyId;
            this.SubAdminId = _SubAdminId;
            this.ConnectorId = _ConnectorId;
            this.CommandId = _CommandId;
            this.ResourceType = _ResourceType;
            this.ResourcePath = _ResourcePath;
            this.AdditionalSingleValuePath = _additionalSingleValuePath;
            this.HttpType = _HttpType;
            this.HttpData = _HttpData;
            this.QueryOption = _QueryOption;
            this.InputParamJson = _InputParamJson;
            this.OutputParamJson = _OutputParamJson;
            this.FunctionType = _FunctionType;
            this.ResponseFormat = _ResponseFormat;
            this.DataJson = _DataJson;
            this.ReturnType = _ReturnType;
            this.DsPath = _DsPath;
            this.Description = _Description;
            this.Cache = _Cache;
            this.Expfrequency = _Expfrequency;
            this.ExpCondtion = _ExpCondtion;
            this.UpdatedOn = _Updatedon;
            this.Xreferences = _ReferenceJson;
            Process1();
        }

        public void Process1()
        {
            try
            {
                this.StatusCode = -1000;
                string query = @"UPDATE TBL_ODATA_COMMAND SET
                                        RESOURCE_TYPE=@RESOURCE_TYPE,
                                        RESOURCE_PATH=@RESOURCE_PATH, 
                                        HTTP_TYPE=@HTTP_TYPE,
                                        HTTP_DATA=@HTTP_DATA,
                                        QUERY_OPTION=@QUERY_OPTION,
                                        INPUT_PARAMETER_JSON=@INPUT_PARAMETER_JSON,
                                        OUTPUT_PARAMETER_JSON=@OUTPUT_PARAMETER_JSON,
                                        FUNCTION_TYPE=@FUNCTION_TYPE,
                                        RESPONSE_FORMAT=@RESPONSE_FORMAT,
                                        DATA_JSON=@DATA_JSON,
                                        RETURN_TYPE=@RETURN_TYPE,
                                        DATASET_PATH=@DATASET_PATH,
                                        ADDITIONAL_RESOURCE_PATH = @ADDITIONAL_RESOURCE_PATH,
                                        DESCRIPTION = @DESCRIPTION,
                                         X_REFERENCES=@X_REFERENCES,
                                        UPDATED_ON=@UPDATED_ON,
                                          CACHE = @CACHE,
                                         SUBADMIN_ID=@SUBADMIN_ID,
                                        EXPIRY_FREQUENCY=@EXPIRY_FREQUENCY,
                                        EXPIRY_CONDITION=@EXPIRY_CONDITION
                                        WHERE
                                        ODATA_COMMAND_ID=@ODATA_COMMAND_ID 
                                        AND COMPANY_ID=@COMPANY_ID;";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@ODATA_COMMAND_ID", this.CommandId);
                objSqlCommand.Parameters.AddWithValue("@RESOURCE_TYPE", this.ResourceType);
                objSqlCommand.Parameters.AddWithValue("@RESOURCE_PATH", this.ResourcePath);
                objSqlCommand.Parameters.AddWithValue("@HTTP_TYPE", this.HttpType);
                objSqlCommand.Parameters.AddWithValue("@HTTP_DATA", this.HttpData);
                objSqlCommand.Parameters.AddWithValue("@QUERY_OPTION", this.QueryOption);
                objSqlCommand.Parameters.AddWithValue("@INPUT_PARAMETER_JSON", this.InputParamJson);
                objSqlCommand.Parameters.AddWithValue("@OUTPUT_PARAMETER_JSON", this.OutputParamJson);
                objSqlCommand.Parameters.AddWithValue("@FUNCTION_TYPE", this.FunctionType);
                objSqlCommand.Parameters.AddWithValue("@RESPONSE_FORMAT", this.ResponseFormat);
                objSqlCommand.Parameters.AddWithValue("@DATA_JSON", this.DataJson);
                objSqlCommand.Parameters.AddWithValue("@RETURN_TYPE", this.ReturnType);
                objSqlCommand.Parameters.AddWithValue("@DATASET_PATH", this.DsPath);
                objSqlCommand.Parameters.AddWithValue("@ADDITIONAL_RESOURCE_PATH", this.AdditionalSingleValuePath);
                objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", this.Description);
                objSqlCommand.Parameters.AddWithValue("@CACHE", this.Cache);
                objSqlCommand.Parameters.AddWithValue("@EXPIRY_FREQUENCY", this.Expfrequency);
                objSqlCommand.Parameters.AddWithValue("@EXPIRY_CONDITION", this.ExpCondtion);
                objSqlCommand.Parameters.AddWithValue("@X_REFERENCES", this.Xreferences);
                objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", this.UpdatedOn);



                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
            }
            catch
            {
                this.StatusCode = -1000;

            }
            finally
            {
                //MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }
        public void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query = @"UPDATE TBL_ODATA_COMMAND SET
                                        RESOURCE_TYPE=@RESOURCE_TYPE,
                                        RESOURCE_PATH=@RESOURCE_PATH, 
                                        HTTP_TYPE=@HTTP_TYPE,
                                        HTTP_DATA=@HTTP_DATA,
                                        QUERY_OPTION=@QUERY_OPTION,
                                        INPUT_PARAMETER_JSON=@INPUT_PARAMETER_JSON,
                                        OUTPUT_PARAMETER_JSON=@OUTPUT_PARAMETER_JSON,
                                        FUNCTION_TYPE=@FUNCTION_TYPE,
                                        RESPONSE_FORMAT=@RESPONSE_FORMAT,
                                        DATA_JSON=@DATA_JSON,
                                        RETURN_TYPE=@RETURN_TYPE,
                                        DATASET_PATH=@DATASET_PATH,
                                        ADDITIONAL_RESOURCE_PATH = @ADDITIONAL_RESOURCE_PATH,
                                        DESCRIPTION = @DESCRIPTION,
                                            SUBADMIN_ID=@SUBADMIN_ID 
                                        WHERE
                                        ODATA_COMMAND_ID=@ODATA_COMMAND_ID 
                                        AND COMPANY_ID=@COMPANY_ID;";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@ODATA_COMMAND_ID", this.CommandId);
                objSqlCommand.Parameters.AddWithValue("@RESOURCE_TYPE", this.ResourceType);
                objSqlCommand.Parameters.AddWithValue("@RESOURCE_PATH", this.ResourcePath);
                objSqlCommand.Parameters.AddWithValue("@HTTP_TYPE", this.HttpType);
                objSqlCommand.Parameters.AddWithValue("@HTTP_DATA", this.HttpData);
                objSqlCommand.Parameters.AddWithValue("@QUERY_OPTION", this.QueryOption);
                objSqlCommand.Parameters.AddWithValue("@INPUT_PARAMETER_JSON", this.InputParamJson);
                objSqlCommand.Parameters.AddWithValue("@OUTPUT_PARAMETER_JSON", this.OutputParamJson);
                objSqlCommand.Parameters.AddWithValue("@FUNCTION_TYPE", this.FunctionType);
                objSqlCommand.Parameters.AddWithValue("@RESPONSE_FORMAT", this.ResponseFormat);
                objSqlCommand.Parameters.AddWithValue("@DATA_JSON", this.DataJson);
                objSqlCommand.Parameters.AddWithValue("@RETURN_TYPE", this.ReturnType);
                objSqlCommand.Parameters.AddWithValue("@DATASET_PATH", this.DsPath);
                objSqlCommand.Parameters.AddWithValue("@ADDITIONAL_RESOURCE_PATH", this.AdditionalSingleValuePath);
                objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", this.Description);           
              
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
            }
            catch
            {
                this.StatusCode = -1000;
                
            }
            finally
            {
                //MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }

        public string CompanyId { get; set; }
        public string SubAdminId { get; set; }
        public string ConnectorId { get; set; }
        public string CommandId { get; set; }

        public string ResourceType { get; set; }
        public string ResourcePath { get; set; }
        public string AdditionalSingleValuePath { get; set; }

        public string HttpType { get; set; }
        public string HttpData { get; set; }

        public string QueryOption { get; set; }
        public string InputParamJson { get; set; }

        public string OutputParamJson { get; set; }
        public string FunctionType { get; set; }

        public string ResponseFormat { get; set; }
        public string DataJson { get; set; }
        public int StatusCode { get; set; }
        public string ReturnType { get; set; }
        public string DsPath { get; set; }

        public string Description { get; set; }

        public int Cache { get; set; }

        public int Expfrequency { get; set; }

        public string ExpCondtion { get; set; }

        public long UpdatedOn {get;set;}


        public string CredentialProperty { get; set; }

        public string Xreferences { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetDatabaseConnection
    {
        public GetDatabaseConnection()
        {

        }
        public GetDatabaseConnection(Boolean _IsSelectAll, string _ConnectionId, string _ConnectionName, string _CompanyID)
        {
            this.IsSelectAll = _IsSelectAll;
            this.ConnectionId = _ConnectionId;
            this.ConnectionName = _ConnectionName;
            this.CompanyId = _CompanyID;
            this.Process();
        }
        //public GetDatabaseConnection(Boolean _IsSelectAll, string _ConnectionId, string _ConnectionName, string _CompanyID, int newid)
        //{
        //    this.IsSelectAll = _IsSelectAll;
        //    this.ConnectionId = _ConnectionId;
        //    this.ConnectionName = _ConnectionName;
        //    this.CompanyId = _CompanyID;
        //    this.Process1();
        //}

        private void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                if (this.IsSelectAll == true)
                {
                    query = @"select CV.DB_CONNECTOR_ID,isnull(DT_OBJ,'') as DT_OBJ, CV.COMPANY_ID,CV.SUBADMIN_ID,CONNECTION_NAME,HOST_NAME,DATABASE_NAME,USER_ID,PASSWORD,CREDENTIAL_PROPERTY,isnull(CV.CREATED_BY,'') as CREATED_BY,isnull(CV.CREATED_ON,0) as CREATED_ON,
                    isnull(CV.UPDATED_ON,0) as UPDATED_ON,ss.FULL_NAME,isnull(st.FULL_NAME,'') as CreatedBY,
                    DATABASE_TYPE,CASE DATABASE_TYPE   WHEN '1' THEN 'MS SQL' WHEN '2' THEN 'Oracle'  WHEN '3' THEN 'MySQL' WHEN '4' THEN 'PostgreSQL'
                    WHEN '5' THEN 'ODBC DSN' ELSE 'DATABASE TYPE NOT DEFINED'   END  AS DB ,TIME_OUT,ADDITIONAL_STRING,CASE MPLUGIN_AGENT 
                    WHEN '' THEN 'None' ELSE MPLUGIN_AGENT  END  AS  MPLUGIN_AGENT  from dbo.TBL_DATABASE_CONNECTION AS CV
                    LEFT JOIN (SELECT t.DB_CONNECTOR_ID, STUFF((SELECT ','  + space(1) + s.DB_COMMAND_NAME FROM (select DB_COMMAND_NAME,C.DB_CONNECTOR_ID FROM TBL_DATABASE_CONNECTION AS C
                    INNER JOIN dbo.TBL_DATABASE_COMMAND AS T ON C.DB_CONNECTOR_ID=T.DB_CONNECTOR_ID AND C.COMPANY_ID=T.COMPANY_ID AND  T.SUBADMIN_ID=C.SUBADMIN_ID) s
                    WHERE s.DB_CONNECTOR_ID = t.DB_CONNECTOR_ID FOR XML PATH('')),1,1,'') AS DT_OBJ FROM (select DB_COMMAND_NAME,C.DB_CONNECTOR_ID FROM TBL_DATABASE_CONNECTION AS C INNER JOIN
                    dbo.TBL_DATABASE_COMMAND AS T ON C.DB_CONNECTOR_ID=T.DB_CONNECTOR_ID AND C.COMPANY_ID=T.COMPANY_ID)t
                    GROUP BY t.DB_CONNECTOR_ID)DOC ON DOC.DB_CONNECTOR_ID=CV.DB_CONNECTOR_ID 
                    Left outer join dbo.TBL_SUB_ADMIN as ss on ss.SUBADMIN_ID=CV.SUBADMIN_ID 
                    Left outer join dbo.TBL_SUB_ADMIN as st on CV.CREATED_BY = st.SUBADMIN_ID where CV.COMPANY_ID=@COMPANY_ID ORDER BY CONNECTION_NAME";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                }
                else
                {
                    if (this.ConnectionName.Length == 0)
                    {
                        query = @"SELECT  * FROM  TBL_DATABASE_CONNECTION WHERE DB_CONNECTOR_ID=@DB_CONNECTOR_ID AND COMPANY_ID=@COMPANY_ID ORDER BY CONNECTION_NAME;";
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@DB_CONNECTOR_ID", this.ConnectionId);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    }
                    else
                    {
                        query = @"SELECT  * FROM  TBL_DATABASE_CONNECTION WHERE CONNECTION_NAME=@CONNECTION_NAME AND COMPANY_ID=@COMPANY_ID ORDER BY CONNECTION_NAME;";
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                    }
                }
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);

                if (objDataSet != null)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                }
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }

//        private void Process1()
//        {
//            try
//            {
//                this.StatusCode = -1000;
//                string query;
//                DataSet objDataSet;
//                SqlCommand objSqlCommand;
//                if (this.IsSelectAll == true)
//                {
//                    query = @"select CV.DB_CONNECTOR_ID,isnull(DT_OBJ,'') as DT_OBJ, CV.COMPANY_ID,CV.SUBADMIN_ID,CONNECTION_NAME,HOST_NAME,DATABASE_NAME,CREDENTIAL_PROPERTY,isnull(CV.CREATED_BY,'') as CREATED_BY,isnull(CV.CREATED_ON,0) as CREATED_ON,
//                    isnull(CV.UPDATED_ON,0) as UPDATED_ON,ss.FULL_NAME,isnull(st.FULL_NAME,'') as CreatedBY,
//                    DATABASE_TYPE,CASE DATABASE_TYPE   WHEN '1' THEN 'MS SQL' WHEN '2' THEN 'Oracle'  WHEN '3' THEN 'MySQL' WHEN '4' THEN 'PostgreSQL'
//                    WHEN '5' THEN 'ODBC DSN' ELSE 'DATABASE TYPE NOT DEFINED'   END  AS DB ,TIME_OUT,ADDITIONAL_STRING,CASE MPLUGIN_AGENT 
//                    WHEN '' THEN 'None' ELSE MPLUGIN_AGENT  END  AS  MPLUGIN_AGENT  from dbo.TBL_DATABASE_CONNECTION AS CV
//                    LEFT JOIN (SELECT t.DB_CONNECTOR_ID, STUFF((SELECT ','  + space(1) + s.DB_COMMAND_NAME FROM (select DB_COMMAND_NAME,C.DB_CONNECTOR_ID FROM TBL_DATABASE_CONNECTION AS C
//                    INNER JOIN dbo.TBL_DATABASE_COMMAND AS T ON C.DB_CONNECTOR_ID=T.DB_CONNECTOR_ID AND C.COMPANY_ID=T.COMPANY_ID AND  T.SUBADMIN_ID=C.SUBADMIN_ID) s
//                    WHERE s.DB_CONNECTOR_ID = t.DB_CONNECTOR_ID FOR XML PATH('')),1,1,'') AS DT_OBJ FROM (select DB_COMMAND_NAME,C.DB_CONNECTOR_ID FROM TBL_DATABASE_CONNECTION AS C INNER JOIN
//                    dbo.TBL_DATABASE_COMMAND AS T ON C.DB_CONNECTOR_ID=T.DB_CONNECTOR_ID AND C.COMPANY_ID=T.COMPANY_ID)t
//                    GROUP BY t.DB_CONNECTOR_ID)DOC ON DOC.DB_CONNECTOR_ID=CV.DB_CONNECTOR_ID 
//                    Left outer join dbo.TBL_SUB_ADMIN as ss on ss.SUBADMIN_ID=CV.SUBADMIN_ID 
//                    Left outer join dbo.TBL_SUB_ADMIN as st on CV.CREATED_BY = st.SUBADMIN_ID where CV.COMPANY_ID=@COMPANY_ID ORDER BY CONNECTION_NAME";
//                    objSqlCommand = new SqlCommand(query);
//                    objSqlCommand.CommandType = CommandType.Text;
//                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                }
//                else
//                {
//                    if (this.ConnectionName.Length == 0)
//                    {
//                        query = @"SELECT  * FROM  TBL_DATABASE_CONNECTION WHERE DB_CONNECTOR_ID=@DB_CONNECTOR_ID AND COMPANY_ID=@COMPANY_ID ORDER BY CONNECTION_NAME;";
//                        objSqlCommand = new SqlCommand(query);
//                        objSqlCommand.CommandType = CommandType.Text;
//                        objSqlCommand.Parameters.AddWithValue("@DB_CONNECTOR_ID", this.ConnectionId);
//                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                    }
//                    else
//                    {
//                        query = @"SELECT  * FROM  TBL_DATABASE_CONNECTION WHERE CONNECTION_NAME=@CONNECTION_NAME AND COMPANY_ID=@COMPANY_ID ORDER BY CONNECTION_NAME;";
//                        objSqlCommand = new SqlCommand(query);
//                        objSqlCommand.CommandType = CommandType.Text;
//                        objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
//                        objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                    }
//                }
//                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
//                this.ResultTable = objDataSet.Tables[0];
//                if (this.ResultTable != null)
//                        this.StatusCode = 0;
//            }
//            catch
//            {
//                this.StatusCode = -1000;
//            }
//        }

        private string ConnectionId
        {
            set;
            get;
        }
        private string ConnectionName
        {
            set;
            get;
        }
        private Boolean IsSelectAll
        {
            set;
            get;
        }

        private Boolean IsSelectDropdown
        {
            set;
            get;
        }
        internal DataTable ResultTable
        {
            set;
            get;
        }

        internal int StatusCode
        {
            set;
            get;
        }

        public string CompanyId { get; set; }
    }
}
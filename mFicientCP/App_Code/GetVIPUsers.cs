﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetVIPUsers
    {

        public GetVIPUsers(string companyId)
        {
            this.CompanyId = companyId;
        }

        public void Process()
        {
            try
            {
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                query = @"SELECT *
                        FROM TBL_USER_DEVICE_SETTINGS AS Settings
                        INNER JOIN TBL_USER_DETAIL AS Usr
                        ON Settings.USER_ID = Usr.USER_ID
                        WHERE Settings.COMPANY_ID = @CompanyId;

                        SELECT * FROM TBL_ACCOUNT_SETTINGS
                        WHERE COMPANY_ID = @CompanyId";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);


                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null && objDataSet.Tables.Count > 0)
                {
                    this.ResultTables = objDataSet.Tables[0];
                    if (objDataSet.Tables[1].Rows.Count > 0)
                    {
                        DefaultMaxUsers = Convert.ToString(objDataSet.Tables[1].Rows[0]["MAX_DEVICE_PER_USER"]);
                    }
                    else
                    {
                        DefaultMaxUsers = "1";
                    }
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        public string CompanyId
        {
            get;
            set;
        }
        public string DefaultMaxUsers
        {
            get;
            set;
        }
        public DataTable ResultTables
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
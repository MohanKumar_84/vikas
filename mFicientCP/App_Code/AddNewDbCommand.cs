﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class AddNewDbCommand
    {
        public AddNewDbCommand(string _SubAdminId, string _CommandName, string _ConnactorId, string _Query, string _Parameter, string _ReturnType, int _DbCommandType, string _TableName, string _SelectFrom, string _ParaJson, string _Columns, string _CompanyId, string _Description, int _Cache, int _Expfrequency, string _ExpCondtion, string _CreatedBy, long _CreatedOn, long _UpdatedOn, string _ImageColumn, string _NoExecute)
        {
            this.SubAdminId = _SubAdminId;
            this.CommandName = _CommandName;
            this.ConnactorId = _ConnactorId;
            this.Query = _Query;
            this.Parameter = _Parameter;
            this.ReturnType = _ReturnType;
            this.DbCommandType = _DbCommandType;
            this.TableName = _TableName;
            this.SelectFrom = _SelectFrom;
            this.ParaJson = _ParaJson;
            this.Columns = _Columns;
            this.CompanyId = _CompanyId;
            this.Description = _Description;
            this.Cache = _Cache;
            this.Expfrequency = _Expfrequency;
            this.ExpCondtion = _ExpCondtion;
            this.CreatedBy = _CreatedBy;
            this.CreatedOn = _CreatedOn;
            this.UpdatedOn = _UpdatedOn;
            this.ImageColumn = _ImageColumn;
            this.NoExecute = _NoExecute;
            Process2();
        }

        public AddNewDbCommand(string _SubAdminId, string _CommandName, string DbCommandId, string _CompanyId, long _UpdatedOn)
        {
            this.SubAdminId = _SubAdminId;
            this.CommandName = _CommandName;
            this.CommandId = DbCommandId;
            this.CompanyId = _CompanyId;
            this.UpdatedOn = _UpdatedOn;
            CopyObjectProcess();
        }

        private void Process2()
        {
            try
            {
                this.StatusCode = -1000;
                this.CommandId = Utilities.GetMd5Hash(this.SubAdminId + this.CommandName + DateTime.UtcNow.Ticks.ToString());

                string query = @"INSERT INTO TBL_DATABASE_COMMAND(COMPANY_ID,DB_COMMAND_ID,DB_CONNECTOR_ID,SQL_QUERY,PARAMETER,RETURN_TYPE,SUBADMIN_ID,DB_COMMAND_NAME,DB_COMMAND_TYPE,TABLE_NAME,QUERY_TYPE,PARAMETER_JSON,COLUMNS,DESCRIPTION,CACHE,EXPIRY_FREQUENCY,EXPIRY_CONDITION,CREATED_BY,CREATED_ON,UPDATED_ON,IMAGE_COLUMN,NO_EXECUTE_CONDITION)
                                VALUES(@COMPANY_ID,@DB_COMMAND_ID,@DB_CONNECTOR_ID,@SQL_QUERY,@PARAMETER,@RETURN_TYPE,@SUBADMIN_ID,@DB_COMMAND_NAME,@DB_COMMAND_TYPE,@TABLE_NAME,@QUERY_TYPE,@PARAMETER_JSON,@COLUMNS,@DESCRIPTION,@CACHE,@EXPIRY_FREQUENCY,@EXPIRY_CONDITION,@CREATED_BY,@CREATED_ON,@UPDATED_ON,@IMAGE_COLUMN,@NO_EXECUTE_CONDITION);";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_ID", this.CommandId);
                objSqlCommand.Parameters.AddWithValue("@DB_CONNECTOR_ID", this.ConnactorId);
                objSqlCommand.Parameters.AddWithValue("@SQL_QUERY", this.Query);
                objSqlCommand.Parameters.AddWithValue("@PARAMETER", this.Parameter);
                objSqlCommand.Parameters.AddWithValue("@RETURN_TYPE", this.ReturnType);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_NAME", this.CommandName);
                objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_TYPE", this.DbCommandType);
                objSqlCommand.Parameters.AddWithValue("@TABLE_NAME", this.TableName);
                objSqlCommand.Parameters.AddWithValue("@QUERY_TYPE", this.SelectFrom);
                objSqlCommand.Parameters.AddWithValue("@PARAMETER_JSON", this.ParaJson);
                objSqlCommand.Parameters.AddWithValue("@COLUMNS", this.Columns);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", this.Description);
                objSqlCommand.Parameters.AddWithValue("@CACHE", this.Cache);
                objSqlCommand.Parameters.AddWithValue("@EXPIRY_FREQUENCY", this.Expfrequency);
                objSqlCommand.Parameters.AddWithValue("@EXPIRY_CONDITION", this.ExpCondtion);
                objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.CreatedBy);
                objSqlCommand.Parameters.AddWithValue("@CREATED_ON", this.CreatedOn);
                objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", this.UpdatedOn);
                objSqlCommand.Parameters.AddWithValue("@IMAGE_COLUMN", this.ImageColumn);
                objSqlCommand.Parameters.AddWithValue("@NO_EXECUTE_CONDITION", this.NoExecute);
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                    this.StatusCode = 0;
                else
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }
        }

        private void CopyObjectProcess()
        {
            try
            {
                this.StatusCode = -1000;
                this.CopyCommandId = Utilities.GetMd5Hash(this.SubAdminId + this.CommandName + DateTime.UtcNow.Ticks.ToString());
                string query = @"insert into TBL_DATABASE_COMMAND(COMPANY_ID,DB_COMMAND_ID,DB_CONNECTOR_ID,SQL_QUERY,PARAMETER,RETURN_TYPE,
                SUBADMIN_ID,DB_COMMAND_NAME,DB_COMMAND_TYPE,TABLE_NAME,QUERY_TYPE,PARAMETER_JSON,COLUMNS,DESCRIPTION,
                CACHE,NO_EXECUTE_CONDITION,EXPIRY_FREQUENCY,EXPIRY_CONDITION,IMAGE_COLUMN,X_REFERENCES,CREATED_BY,CREATED_ON,UPDATED_ON)
                
                select COMPANY_ID,@COPYDB_COMMAND_ID,DB_CONNECTOR_ID,SQL_QUERY,PARAMETER,RETURN_TYPE,
                SUBADMIN_ID,@DB_COMMAND_NAME,DB_COMMAND_TYPE,TABLE_NAME,QUERY_TYPE,PARAMETER_JSON,COLUMNS,DESCRIPTION,
                CACHE,NO_EXECUTE_CONDITION,EXPIRY_FREQUENCY,EXPIRY_CONDITION,IMAGE_COLUMN,X_REFERENCES,@CREATED_BY,@UPDATED_ON,@UPDATED_ON
                from TBL_DATABASE_COMMAND  where DB_COMMAND_ID=@DB_COMMAND_ID;";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_ID", this.CommandId);
                objSqlCommand.Parameters.AddWithValue("@COPYDB_COMMAND_ID", this.CopyCommandId);
                objSqlCommand.Parameters.AddWithValue("@DB_COMMAND_NAME", this.CommandName);
                objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", this.UpdatedOn);

                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                    this.StatusCode = 0;
                else
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }
        }

        private string SubAdminId { get; set; }

        private string CommandName { get; set; }

        private string ReturnType { get; set; }

        private string Parameter { get; set; }

        private string Query { get; set; }

        private string ConnactorId { get; set; }

        public string CommandId { get; set; }

        public int StatusCode { get; set; }

        private string CopyCommandId { get; set; }

        private int DbCommandType { get; set; }

        private string TableName { get; set; }

        private string SelectFrom { get; set; }

        private string ParaJson { get; set; }

        private string Columns { get; set; }

        private string CompanyId { get; set; }

        private string Description { get; set; }

        private int Cache { get; set; }

        private int Expfrequency { get; set; }

        private string ExpCondtion { get; set; }

        private string CreatedBy { get; set; }

        private long CreatedOn { get; set; }

        private long UpdatedOn { get; set; }

        private string ImageColumn { get; set; }
        private string NoExecute { get; set; }
    }
}
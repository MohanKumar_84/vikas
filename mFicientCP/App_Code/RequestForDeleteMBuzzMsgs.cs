﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientCP
{
    public class RequestForDeleteMBuzzMsgs
    {
        [DataMember]
        public MBuzzDeleteUsrMsgsReqJsonFields req { get; set; }
    }

    public class MBuzzDeleteUsrMsgsReqJsonFields
    {
        /// <summary>
        /// request id
        /// </summary>
        [DataMember]
        public string rid { get; set; }

        /// <summary>
        /// request id
        /// </summary>
        [DataMember]
        public string func { get; set; }

        /// <summary>
        /// data
        /// </summary>
        [DataMember]
        public MBuzzDeleteUsrMsgsReqData data { get; set; }
    }
    public class MBuzzDeleteUsrMsgsReqData
    {
        /// <summary>
        /// User Name
        /// </summary>
        [DataMember]
        public string unm { get; set; }

        /// <summary>
        /// Enterprise Id
        /// </summary>
        [DataMember]
        public string enid { get; set; }

        /// <summary>
        /// DeviceId
        /// </summary>
        [DataMember]
        public List<string> devid { get; set; }

        /// <summary>
        /// Type Of request
        /// </summary>
        [DataMember]
        public string type { get; set; }
    }

}
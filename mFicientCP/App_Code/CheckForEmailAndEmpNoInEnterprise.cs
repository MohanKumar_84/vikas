﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class CheckForEmailAndEmpNoInEnterprise
    {
        public enum SEARCH_FOR
        {
            NewUser,
            ExistingUser,
        }
        #region Members
        string _userIdToSearch, _emailId, _userIdHavingEmail, _userNameHavingEmail;
        string _statusDescription, _companyId;
        int _statusCode;
        SEARCH_FOR _searchCondition;
        bool _emailExists, _emailExistsForUserIdToSearch;



        #endregion
        #region Public Properties
        public string EmailId
        {
            get { return _emailId; }
        }

        public string UserId
        {
            get { return _userIdToSearch; }
        }

        public SEARCH_FOR SearchCondition
        {
            get { return _searchCondition; }
        }
        

        public string UserIdHavingSimilarEmail
        {
            get { return _userIdHavingEmail; }
        }
        public bool EmailExistsForUserIdToSearch
        {
            get { return _emailExistsForUserIdToSearch; }
        }
        public bool EmailExists
        {
            get { return _emailExists; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }

        public string UserNameHavingEmail
        {
            get { return _userNameHavingEmail; }
        }
        #endregion
        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeNo"></param>
        /// <param name="emailId"></param>
        /// <param name="searchCondition"></param>
        /// <param name="userIdToSearch">Pass the existing user user id if search for existing user.</param>
        public CheckForEmailAndEmpNoInEnterprise(string emailId, string companyId, SEARCH_FOR searchCondition, string userIdToSearch)
        {
            _userIdToSearch = userIdToSearch;
            _emailId = emailId;
            _companyId = companyId;
            _searchCondition = searchCondition;
        }
        #endregion

        public void Process()
        {
            try
            {
                switch (_searchCondition)
                {
                    case SEARCH_FOR.NewUser:
                        checkEmailAndEmployeeNoForNewUser();
                        break;
                    case SEARCH_FOR.ExistingUser:
                        if (String.IsNullOrEmpty(_userIdToSearch))
                        {
                            throw new ArgumentException("Please provide a user Id");
                        }
                        checkEmailAndEmployeeNoForExistingUser();
                        break;
                }
            }
            catch (ArgumentException ex)
            {
                _statusCode = -1000;
                _statusDescription = ex.Message;
            }
            catch (Exception ex)
            {
                _statusCode = -1000;
                _statusDescription = ex.Message;
            }
        }

        void checkEmailAndEmployeeNoForNewUser()
        {
            GetUserDetail objUserDetails = new GetUserDetail(false, true, false, _companyId, String.Empty);
            objUserDetails.Process();
            if (objUserDetails.StatusCode == 0)
            {
                if (objUserDetails.ResultTable != null)
                {
                    DataRow[] rows = objUserDetails.ResultTable.Select(String.Format("EMAIL_ID = '{0}'", _emailId));
                    if (rows.Length > 0)
                    {
                        _emailExists = true;
                        _userIdHavingEmail = Convert.ToString(rows[0]["USER_ID"]);
                        _userNameHavingEmail = Convert.ToString(rows[0]["USER_NAME"]);
                    }
                }
            }
            else
            {
                throw new Exception("Internal server error");
            }
        }

        void checkEmailAndEmployeeNoForExistingUser()
        {
            GetUserDetail objUserDetails = new GetUserDetail(false, true, false, _companyId, String.Empty);
            objUserDetails.Process();
            if (objUserDetails.StatusCode == 0)
            {
                if (objUserDetails.ResultTable != null)
                {
                    DataRow[] rows = objUserDetails.ResultTable.Select(String.Format("EMAIL_ID = '{0}'", _emailId));
                    if (rows.Length > 0)
                    {
                        _emailExists = true;
                        _userIdHavingEmail = Convert.ToString(rows[0]["USER_ID"]);
                        _userNameHavingEmail = Convert.ToString(rows[0]["USER_NAME"]);
                        if (_userIdToSearch.ToLower() == _userIdHavingEmail.ToLower())
                        {
                            _emailExistsForUserIdToSearch = true;
                        }
                        else
                        {
                            _emailExistsForUserIdToSearch = false;
                        }
                    }
                    else
                    {
                        _emailExists = false;
                        _emailExistsForUserIdToSearch = false;
                    }
                }
            }
            else
            {
                throw new Exception("Internal server error");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class UserLogOut
    {

        //public UserLogOut()
        //{

        //}

        //public UserLogOut(string _UserId, string _SessionId)
        //{
        //    this.UserId = _UserId;
        //    this.SessionID = _SessionId;
        //}

        public UserLogOut(string _UserId, string _SessionId, string adminId, string companyId, System.Web.Caching.Cache cache,LogOutReason reason)
        {
            this.UserId = _UserId;
            this.SessionID = _SessionId;
            this.AdminId = adminId;
            this.CompanyId = companyId;
            this.AppCache = cache;
            this.LogOtReason = reason;
        }

        public void Process()
        {
            SqlCommand objSqlCommand;
            try
            {
                try
                {
                    string strLogOutReason = "";
                    switch (LogOtReason)
                    {
                        case LogOutReason.SESSION_EXPIRED:
                            strLogOutReason = "Session Expired";
                            break;
                        case LogOutReason.LOGGED_AGAIN:
                            strLogOutReason = "Logged Again";
                            break;
                        case LogOutReason.NORMAL:
                            strLogOutReason = "Normal";
                            break;
                    }
                    string query = @"update TBL_LOGIN_HISTORY set LOGOUT_DATETIME=@LOGOUT_DATETIME, LOGOUT_TYPE=@LOGOUT_TYPE where user_id=@user_id and LOGIN_DEVICE_TOKEN=login_device_token
                              and LOGIN_DEVICE_TYPE_CODE=@login_device_type_code AND LOGOUT_DATETIME=0 AND LOGIN_SUCCESS=1 AND COMPANY_ID = @CompanyId";//LOGIN_SUCCESS Added on 2/1/2012.

                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                    objSqlCommand.Parameters.AddWithValue("@login_device_type_code", "web");
                    objSqlCommand.Parameters.AddWithValue("@login_device_token", "");
                    objSqlCommand.Parameters.AddWithValue("@LOGOUT_DATETIME", DateTime.UtcNow.Ticks);
                    objSqlCommand.Parameters.AddWithValue("@LOGOUT_TYPE", strLogOutReason);
                    objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                    MSSqlClient.ExecuteNonQueryRecord(objSqlCommand);
                }
                catch { }//even if there is an error in the above process the session should be removed from the cache.
                
                //string strDeviceId = "";
                //if (HttpContext.Current.Request.Cookies[((int)COOKIE_TYPES.SESSION_VALUE).ToString()] != null)
                //{
                //    HttpCookie reqCookie = HttpContext.Current.Request.Cookies.Get(((int)COOKIE_TYPES.SESSION_VALUE).ToString());
                //    if (!String.IsNullOrEmpty(reqCookie.Value) && !(reqCookie.Value.ToLower() == "null"))//after logout the value of the cookie stored earlier is set as "null" string.
                //    {
                //        strDeviceId = Convert.ToString(reqCookie.Value);
                //    }
                //}
                CacheManager.Remove(CacheManager.GetKey(CacheManager.CacheType.mFicientSession, this.CompanyId, this.UserId, MficientConstants.WEB_DEVICE_TYPE_AND_ID, MficientConstants.WEB_DEVICE_TYPE_AND_ID, this.SessionID, ""));
                Utilities.removeCurrentCookie(String.Empty);
            }
            catch
            {
            }
        }

        public string UserId
        {
            set;
            get;
        }
        public string SessionID
        {
            set;
            get;
        }
        public string LocationName
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public System.Web.Caching.Cache AppCache
        {
            get;
            set;
        }
        public string AdminId
        {
            get;
            set;
        }

        public string CompanyId
        {
            get;
            set;
        }

        public LogOutReason LogOtReason
        {
            get;
            set;
        }
    }
}
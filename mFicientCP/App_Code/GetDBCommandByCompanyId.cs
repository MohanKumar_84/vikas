﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetDBCommandByCompanyId
    {

        string _companyId, _statusDescription;
        int _statusCode;
        DataTable _dBCommand = null;
        const int DB_COMMAND_TYPE_DELETE = 4;

        public GetDBCommandByCompanyId(string companyId)
        {
            _companyId = companyId;
        }


        public void Process()
        {
            try
            {
                SqlCommand cmd;
                string strQuery = @"SELECT * FROM TBL_DATABASE_COMMAND
                                    WHERE COMPANY_ID = @CompanyId";
                cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                DataSet dsDbCommand = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsDbCommand == null)
                {
                    throw new Exception();
                }
                _dBCommand = dsDbCommand.Tables[0];

                _statusCode = 0;
                _statusDescription = "";
            }
            catch
            {
                _statusCode = -1000;
                _statusDescription = "Internal server error";
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Exception">Thrown when there is some internal error</exception>
        public bool doesDeleteCommandExists()
        {
            try
            {
                Process();
                if (_statusCode == 0)
                {
                    DataRow[] rows = _dBCommand.Select(@"DB_COMMAND_TYPE = " + DB_COMMAND_TYPE_DELETE);
                    if (rows.Length > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    throw new Exception("Internal server error");
                }
            }
            catch
            {
                throw new Exception("Internal server error");
            }

        }

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
        }

        public int StatusCode
        {
            get { return _statusCode; }
        }
        public DataTable DBCommand
        {
            get { return _dBCommand; }
        }
    }
}
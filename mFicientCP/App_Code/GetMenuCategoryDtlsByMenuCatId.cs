﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetMenuCategoryDtlsByMenuCatId
    {
        public GetMenuCategoryDtlsByMenuCatId(string companyId, string menuCategoryId)
        {
            this.CompanyId = companyId;
            this.MenuCategoryId = menuCategoryId;

        }

        public void Process()
        {
            try
            {
                StatusCode = -1000;//for error
                string query = @"SELECT * FROM TBL_MENU_CATEGORY
                                WHERE COMPANY_ID = @CompanyId
                                AND MENU_CATEGORY_ID = @MenuCategoryId";
                                
                           //SELECT * FROM TBL_MENU_AND_GROUP_LINK
                           // WHERE MENU_CATEGORY_ID = @MenuCategoryId";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@MenuCategoryId", this.MenuCategoryId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet!=null && objDataSet.Tables.Count>0)
                {
                    this.ResultTables = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string MenuCategoryId
        {
            set;
            get;
        }
        public DataTable ResultTables
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
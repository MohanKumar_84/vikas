﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetSubAdminDetail
    {
        public enum SUB_ADMIN_ROLE
        { 
            USER_GROUPS_DEVICES_MANAGER,
            FORM_MANAGER
        }
        #region Previous Code
        //public GetSubAdminDetail(Boolean _IsSelectAll, Boolean _IsFindById, string _AdminId, string _SubAdminId, string _userName, string _Company_id)
        //{
        //    this.AdminId = _AdminId;
        //    this.IsFindByUserId = _IsFindById;
        //    this.IsSelectAll = _IsSelectAll;
        //    this.SubAdminId = _SubAdminId;
        //    this.UserName = _userName;
        //    this.Company_id = _Company_id;
        //}
        //public void Process()
        //{
        //    try
        //    {
        //        StatusCode = -1000;//for error
        //        string query;
        //        DataSet objDataSet;
        //        SqlCommand objSqlCommand;
        //        if (this.IsSelectAll)
        //        {
        //            query = @"SELECT  * FROM  TBL_SUB_ADMIN WHERE ADMIN_ID=@ADMIN_ID AND ISACTIVE=@ISACTIVE AND COMPANY_ID=@COMPANY_ID ORDER BY FULL_NAME;";
        //            objSqlCommand = new SqlCommand(query);
        //            objSqlCommand.CommandType = CommandType.Text;
        //            objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
        //            objSqlCommand.Parameters.AddWithValue("@ISACTIVE",true);
        //            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
        //        }
        //        else
        //        {
        //            if (this.IsFindByUserId)
        //            {
        //                query = @" SELECT  * FROM  TBL_SUB_ADMIN WHERE SUBADMIN_ID=@SUBADMIN_ID AND ADMIN_ID=@ADMIN_ID AND ISACTIVE=@ISACTIVE AND COMPANY_ID=@COMPANY_ID ORDER BY FULL_NAME;";
        //                objSqlCommand = new SqlCommand(query);
        //                objSqlCommand.CommandType = CommandType.Text;
        //                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
        //                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
        //               objSqlCommand.Parameters.AddWithValue("@ISACTIVE", true);
        //               objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
        //            }
        //            if (this.IsFindByRole)
        //            {
        //                query = @" SELECT * FROM TBL_SUBADMIN_ROLE WHERE ADMIN_ID=@ADMIN_ID AND ROLE_ID=@ROLE_ID AND COMPANY_ID=@COMPANY_ID ORDER BY FULL_NAME ;";
        //                objSqlCommand = new SqlCommand(query);
        //                objSqlCommand.CommandType = CommandType.Text;
        //                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
        //                objSqlCommand.Parameters.AddWithValue("@ROLE_ID", this.RoleId);
        //                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
        //            }
        //            else 
        //            {
        //                if (this.SubAdminId.Length == 0)
        //                {
        //                    query = @" SELECT  * FROM  TBL_SUB_ADMIN WHERE USER_NAME=@USER_NAME AND ADMIN_ID=@ADMIN_ID  AND COMPANY_ID=@COMPANY_ID ORDER BY FULL_NAME ;";
        //                    objSqlCommand = new SqlCommand(query);
        //                    objSqlCommand.CommandType = CommandType.Text;
        //                    objSqlCommand.Parameters.AddWithValue("@USER_NAME", this.UserName);
        //                    objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
        //                    objSqlCommand.Parameters.AddWithValue("@ISACTIVE", true);
        //                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
        //                }
        //                else
        //                {
        //                    query = @" SELECT  * FROM  TBL_SUB_ADMIN WHERE USER_NAME=@USER_NAME AND ADMIN_ID=@ADMIN_ID AND SUBADMIN_ID !=@SUBADMIN_ID  AND COMPANY_ID=@COMPANY_ID ORDER BY FULL_NAME;";
        //                    objSqlCommand = new SqlCommand(query);
        //                    objSqlCommand.CommandType = CommandType.Text;
        //                    objSqlCommand.Parameters.AddWithValue("@USER_NAME", this.UserName);
        //                    objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
        //                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
        //                    objSqlCommand.Parameters.AddWithValue("@ISACTIVE", true);
        //                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
        //                }
        //            }
        //        }
        //        objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
        //        if (objDataSet != null)
        //        {
        //            this.ResultTable = objDataSet.Tables[0];
        //            this.StatusCode = 0;
        //            this.StatusDescription = "";
        //            if (objDataSet.Tables[0].Rows.Count == 0)
        //            {
        //                StatusDescription = "No record found";
        //            }
        //            if (this.ResultTable.Rows.Count > 0)
        //            {
        //                this.EmailId = Convert.ToString(this.ResultTable.Rows[0]["EMAIL"]);
        //            }
        //        }
        //        else
        //        {
        //            this.StatusCode = -1000;
        //            this.StatusDescription = "Internal server error";
        //        }
        //    }

        //    catch 
        //    {
        //        StatusDescription = "Internal server error";
        //    }
        //}
        #endregion
        public GetSubAdminDetail(string adminId, string companyId)
        {
            this.AdminId = adminId;
            this.Company_id = companyId;
        }

        public GetSubAdminDetail(string companyId)
        {
            this.Company_id = companyId;
        }

        public void getSubAdminById(string subAdminId)
        {
            try
            {
                this.SubAdminId = subAdminId;
                StatusCode = -1000;//for error
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                query = @" SELECT  * FROM  TBL_SUB_ADMIN WHERE SUBADMIN_ID=@SUBADMIN_ID AND ADMIN_ID=@ADMIN_ID AND ISACTIVE=@ISACTIVE AND COMPANY_ID=@COMPANY_ID ORDER BY FULL_NAME;";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", subAdminId);
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@ISACTIVE", true);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    if (objDataSet.Tables[0].Rows.Count == 0)
                    {
                        StatusDescription = "No record found";
                    }
                    if (this.ResultTable.Rows.Count > 0)
                    {
                        this.EmailId = Convert.ToString(this.ResultTable.Rows[0]["EMAIL"]);
                    }
                }
                else
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        public void getAllSubAdminsOfCmpByAdminId()
        {
            try
            {
                StatusCode = -1000;//for error
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                query = @"SELECT  * FROM  TBL_SUB_ADMIN WHERE ADMIN_ID=@ADMIN_ID AND ISACTIVE=@ISACTIVE AND COMPANY_ID=@COMPANY_ID ORDER BY FULL_NAME;";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@ISACTIVE", true);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    if (objDataSet.Tables[0].Rows.Count == 0)
                    {
                        StatusDescription = "No record found";
                    }
                    if (this.ResultTable.Rows.Count > 0)
                    {
                        this.EmailId = Convert.ToString(this.ResultTable.Rows[0]["EMAIL"]);
                    }
                }
                else
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
            }
            catch
            {
                this.StatusDescription = "Internal server error.";
                this.StatusCode = -1000;
            }
        }

        //Tanika

        public void getAllSubAdminsOfCmp()
        {
            try
            {
                StatusCode = -1000;//for error
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                query = @"SELECT  * FROM  TBL_SUB_ADMIN WHERE ISACTIVE=@ISACTIVE AND COMPANY_ID=@COMPANY_ID ORDER BY FULL_NAME;";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@ISACTIVE", true);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    if (objDataSet.Tables[0].Rows.Count == 0)
                    {
                        StatusDescription = "No record found";
                    }
                    if (this.ResultTable.Rows.Count > 0)
                    {
                        this.EmailId = Convert.ToString(this.ResultTable.Rows[0]["EMAIL"]);
                    }
                }
                else
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
            }
            catch
            {
                this.StatusDescription = "Internal server error.";
                this.StatusCode = -1000;
            }
        }


        public void getSubAdminByRole(string roleId)
        {
            try
            {
                this.RoleId = roleId;
                StatusCode = -1000;//for error
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                query = @" SELECT * FROM TBL_SUBADMIN_ROLE WHERE ADMIN_ID=@ADMIN_ID AND ROLE_ID=@ROLE_ID AND COMPANY_ID=@COMPANY_ID ORDER BY FULL_NAME ;";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@ROLE_ID", roleId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    if (objDataSet.Tables[0].Rows.Count == 0)
                    {
                        StatusDescription = "No record found";
                    }
                    if (this.ResultTable.Rows.Count > 0)
                    {
                        this.EmailId = Convert.ToString(this.ResultTable.Rows[0]["EMAIL"]);
                    }
                }
                else
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        public void getSubAdminByUserName(string username)
        {
            try
            {
                this.UserName = username;
                StatusCode = -1000;//for error
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                query = @" SELECT  * FROM  TBL_SUB_ADMIN WHERE USER_NAME=@USER_NAME AND ADMIN_ID=@ADMIN_ID  AND COMPANY_ID=@COMPANY_ID ORDER BY FULL_NAME ;";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_NAME", username);
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@ISACTIVE", true);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    if (objDataSet.Tables[0].Rows.Count == 0)
                    {
                        StatusDescription = "No record found";
                    }
                    if (this.ResultTable.Rows.Count > 0)
                    {
                        this.EmailId = Convert.ToString(this.ResultTable.Rows[0]["EMAIL"]);
                    }
                }
                else
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        public void GetOtherSubAdmins(string subAdminId)
        {
            try
            {
                this.SubAdminId = subAdminId;
                StatusCode = -1000;//for error
                string strQuery = @"select * from TBL_SUB_ADMIN where SUBADMIN_ID != @SUBADMIN_ID and ADMIN_ID = @ADMIN_ID AND ISACTIVE=@ISACTIVE AND COMPANY_ID=@COMPANY_ID  ORDER BY FULL_NAME;";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", subAdminId);
                objSqlCommand.Parameters.AddWithValue("@ISACTIVE", true);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);

                objSqlCommand.CommandType = CommandType.Text;
                DataSet dsSubAdminDtls = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (dsSubAdminDtls != null)
                {
                    StatusCode = 0;
                    StatusDescription = "";
                    ResultTable = dsSubAdminDtls.Tables[0];
                }
                else
                {
                    StatusDescription = "No record found";
                }
            }
            catch
            {
                StatusDescription = "Internal server error";
            }
        }
        public void getSubAdminWithFormModificationRole()
        {
            try
            {
                StatusCode = -1000;//for error
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                query = @"SELECT  * FROM  TBL_SUB_ADMIN AS SA 
                        INNER JOIN TBL_SUBADMIN_ROLE AS ROLE ON SA.SUBADMIN_ID = ROLE.SUBADMIN_ID 
                        WHERE SA.ADMIN_ID=@ADMIN_ID AND SA.ISACTIVE=@ISACTIVE 
                        AND SA.COMPANY_ID=@COMPANY_ID 
                        AND ROLE.ROLE_ID = 'FM'
                        ORDER BY SA.FULL_NAME;";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@ISACTIVE", true);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    if (objDataSet.Tables[0].Rows.Count == 0)
                    {
                        StatusDescription = "No record found";
                    }
                    if (this.ResultTable.Rows.Count > 0)
                    {
                        this.EmailId = Convert.ToString(this.ResultTable.Rows[0]["EMAIL"]);
                    }
                }
                else
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
            }
            catch
            {
                this.StatusDescription = "Internal server error.";
                this.StatusCode = -1000;
            }
        }
        public void getSubAdminByRole(SUB_ADMIN_ROLE saRole)
        {
            try
            {
                StatusCode = -1000;//for error
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                query = @" SELECT * FROM TBL_SUBADMIN_ROLE AS SAROLE
                            INNER JOIN TBL_SUB_ADMIN AS SA
                            ON SAROLE.SUBADMIN_ID = SA.SUBADMIN_ID
                            WHERE SAROLE.ROLE_ID = @ROLE_ID
                            AND SAROLE.COMPANY_ID = @COMPANY_ID
                            AND SA.ISACTIVE = 1
                            ORDER BY SA.FULL_NAME";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@ROLE_ID",this.getRoleIdBySubAdminRole(saRole));
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    if (objDataSet.Tables[0].Rows.Count == 0)
                    {
                        StatusDescription = "No record found";
                    }
                    if (this.ResultTable.Rows.Count > 0)
                    {
                        this.EmailId = Convert.ToString(this.ResultTable.Rows[0]["EMAIL"]);
                    }
                }
                else
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        string getRoleIdBySubAdminRole(SUB_ADMIN_ROLE saRole)
        {
            string strSARole = String.Empty;
            switch (saRole)
            {
                case SUB_ADMIN_ROLE.USER_GROUPS_DEVICES_MANAGER:
                    strSARole = "UM";
                    break;
                case SUB_ADMIN_ROLE.FORM_MANAGER:
                    strSARole = "FM";
                    break;
            }
            return strSARole;
        }
        public string Company_id
        {
            set;
            get;
        }
        public string AdminId
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string RoleId
        {
            set;
            get;
        }
        public string UserName
        {
            set;
            get;
        }
        public Boolean IsFindByUserId
        {
            set;
            get;
        }
        public Boolean IsSelectAll
        {
            set;
            get;
        }
        public Boolean IsFindByRole
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string EmailId
        {
            get;
            set;
        }
    }
}
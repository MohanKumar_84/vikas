﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class DeleteODataConnector
    {
       public DeleteODataConnector()
        { 
        }

       public DeleteODataConnector(string _CompanyId, string _SubAdminId, string _ConnactorId)
        {

            this.ConnactorId = _ConnactorId;
            this.SubAdminId = _SubAdminId;
            this.CompanyId = _CompanyId;
        }

        public void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query = @"DELETE FROM TBL_ODATA_CONNECTION WHERE ODATA_CONNECTOR_ID=@ODATA_CONNECTOR_ID AND SUBADMIN_ID=@SUBADMIN_ID AND COMPANY_ID=@COMPANY_ID;";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@ODATA_CONNECTOR_ID", this.ConnactorId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);

                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
            }
            catch 
            {
                this.StatusCode = -1000;
            }
        }

        public string ConnactorId { get; set; }
        public int StatusCode { get; set; }
        public string SubAdminId { get; set; }
        public string CompanyId { get; set; }
    }
}
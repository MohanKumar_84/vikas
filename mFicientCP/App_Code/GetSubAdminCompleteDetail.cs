﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetSubAdminCompleteDetail
    {

        public GetSubAdminCompleteDetail(string adminId, string subAdminId,string _Company_id)
        {
            this.AdminId = adminId;
            this.SubAdminId = subAdminId;
            this.Company_id = _Company_id;
        }
        public void Process()
        {
            StatusCode = -1000;//for error
            try
            {
                string strQuery = @"SELECT  * FROM  TBL_SUB_ADMIN WHERE SUBADMIN_ID=@SUBADMIN_ID AND ADMIN_ID=@ADMIN_ID  AND COMPANY_ID=@COMPANY_ID;
                                
                                SELECT  *,td.DEPARTMENT_NAME FROM  TBL_SUBADMIN_DIVISION_DEP tsd 
                                INNER JOIN TBL_DEPARTMENT td ON tsd.DEPARTMENT_ID=td.DEPARTMENT_ID
                                WHERE tsd.SUBADMIN_ID=@SUBADMIN_ID  AND tsd.COMPANY_ID=@COMPANY_ID;

                                SELECT  * FROM  TBL_SUBADMIN_ROLE WHERE ADMIN_ID=@ADMIN_ID AND SUBADMIN_ID=@SUBADMIN_ID AND COMPANY_ID=@COMPANY_ID;
                                select FIRST_NAME  + SPACE(1) + LAST_NAME  + SPACE(1) +'(' + SPACE(1)+ USER_NAME  + SPACE(1)    +')'   AS username ,USER_ID from dbo.TBL_USER_DETAIL WHERE COMPANY_ID=@COMPANY_ID;";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                cmd.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
                DataSet dsSubAdminDetails = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsSubAdminDetails != null && dsSubAdminDetails.Tables.Count > 0)
                {

                    dsSubAdminDetails.Tables[0].TableName = "Sub Admin Details";
                    dsSubAdminDetails.Tables[1].TableName = "Sub Admin Division And Departement";
                    dsSubAdminDetails.Tables[2].TableName = "Sub Admin Role";
                    dsSubAdminDetails.Tables[3].TableName = "MAP USER";

                    ResultTables = dsSubAdminDetails;
                    StatusCode = 0;
                    StatusDescription = "";
                }
                else
                {
                    StatusDescription = "Results not found";
                }
            }
            catch (Exception e)
            {
                StatusCode = -1000;
                StatusDescription = e.Message;
            }
        }


        public string SubAdminId
        {
            set;
            get;
        }
        public string AdminId
        {
            set;
            get;
        }
        public string Company_id
        {
            set;
            get;
        }
        public DataSet ResultTables
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }



}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetSubAdminRoles
    {
       public GetSubAdminRoles()
        { 

        }

       public GetSubAdminRoles(string _AdminId, string _SubAdminId)
       {
            this.AdminId = _AdminId;
            this.SubAdminId = _SubAdminId;
        }
        public void Process()
        {
            try
            {
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                query = @"SELECT  * FROM  TBL_SUBADMIN_ROLE WHERE ADMIN_ID=@ADMIN_ID AND SUBADMIN_ID=@SUBADMIN_ID;";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", this.AdminId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        public string AdminId
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }

        
        public DataTable ResultTable
        {
            set;
            get;
        }
        //public string DivisionName
        //{
        //    set;
        //    get;
        //}
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;

using System.Web;

namespace mFicientCP
{
    public class UpdatedDisplayIndex
    {
        
        public UpdatedDisplayIndex(List<SelectetdDisplayIndex> _list)
        {
            this.lst = _list;
        }
        public void Process()
        {
            try
            {

                string query = @"update dbo.TBL_MENU_CATEGORY set  DISPLAY_INDEX =@DISPLAY_INDEX where COMPANY_ID=@COMPANY_ID and MENU_CATEGORY_ID=@MENU_CATEGORY_ID";
                foreach (SelectetdDisplayIndex objDisplayIndex in this.lst)
                {
                    SqlCommand objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@DISPLAY_INDEX", objDisplayIndex.Display_ID);
                    objSqlCommand.Parameters.AddWithValue("@MENU_CATEGORY_ID", objDisplayIndex.MenuCategory_ID);
                    objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", objDisplayIndex.Company_ID);
                    if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                    {
                        this.StatusCode = 0;
                        this.StatusDescription = "";
                    }
                    else
                    {
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                    }
                }

            }
            catch (SqlException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
           
            
    }



        public List<SelectetdDisplayIndex> lst { get; set; }
        public string StatusDescription
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }


    }
    public class SelectetdDisplayIndex
    {
        public string MenuCategory_ID { get; set; }
        public string Display_ID { get; set; }
        public string Company_ID { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Net;
namespace mFicientCP
{
    internal class DeleteMobileSessionOfUser
    {
        public enum MBUZZ_PENDING_MSGS_DELETE_TYPE
        {
            User,
            Device,
        }
        string _userId, _companyId, _subAdminId;
        USER_LOG_OUT_TYPE _eLogOutType;
        MBUZZ_PENDING_MSGS_DELETE_TYPE _mBuzzPendingMsgDelType;
        List<MFEDevice> _deviceDtlsToDelete;
        bool _useConnection;
        SqlConnection _con;
        SqlTransaction _transaction;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="companyId"></param>
        /// <param name="subAdminId"></param>
        /// <param name="logOutType"></param>
        /// <param name="mbuzzDeviceDelType"></param>
        /// <param name="useConnection"></param>
        /// <param name="con"></param>
        /// <param name="transaction"></param>
        /// <param name="deviceDtlsToDelete">Pass null in case logout type is blocked or deleted.</param>
        public DeleteMobileSessionOfUser(string userId, string companyId, string subAdminId, USER_LOG_OUT_TYPE logOutType, MBUZZ_PENDING_MSGS_DELETE_TYPE mbuzzDeviceDelType
            , bool useConnection, SqlConnection con, SqlTransaction transaction, List<MFEDevice> deviceDtlsToDelete)
        {
            if (logOutType == USER_LOG_OUT_TYPE.NORMAL || logOutType == USER_LOG_OUT_TYPE.LOGGED_IN_AGAIN)
            {
                throw new Exception();
            }
            _userId = userId;
            _companyId = companyId;
            _subAdminId = subAdminId;
            _eLogOutType = logOutType;
            _con = con;
            _transaction = transaction;
            _useConnection = useConnection;
            _deviceDtlsToDelete = deviceDtlsToDelete;
            _mBuzzPendingMsgDelType = mbuzzDeviceDelType;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userDetailAfterProcess"></param>
        /// <param name="registeredDeviceDtlAfterProcess"></param>
        public void ProcessInTransaction(GetUserDetail userDetailAfterProcess,  GetRegisteredDeviceDetailsByUserId registeredDeviceDtlAfterProcess,int approveDeviceCount)
        {
            if (userDetailAfterProcess.StatusCode != 0 || registeredDeviceDtlAfterProcess.StatusCode != 0)
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
                return;
            }

            try
            {
                DataSet dsRegisteredDevices = registeredDeviceDtlAfterProcess.ResultTables;

                DataTable dtblRegisteredDevices = dsRegisteredDevices.Tables[0];//check only for null because the user may not have any registered devices against his name as yet
                //_deviceDtls = registeredDeviceDtlAfterProcess.CommaSeparatedDevIdAndDevType;//if not device deleted then (_commaSeparatedDevIdAndDevType) would be null.
                List<MFEDevice> lstRegDevOfUser = registeredDeviceDtlAfterProcess.RegDeviceDtlsDevIdAndDevType;//if not device deleted then (_commaSeparatedDevIdAndDevType) would be null.

                DataRow[] aryUser = userDetailAfterProcess.ResultTable.Select(String.Format("USER_ID = '{0}'", _userId));
                if (aryUser.Length == 0)
                {
                    StatusCode = -1000;
                    StatusDescription = "Internal server error";
                    return;
                }

                if (_eLogOutType != USER_LOG_OUT_TYPE.DEVICE_DELETED)//for user_deleted or user_blocked
                {
                    //processDeleteExistingSession(String.Empty, String.Empty);
                }
                else
                {

                   // int iDesktopMessngr = Convert.ToBoolean(userDetailAfterProcess.ResultTable.Rows[0]["DESKTOP_MESSENGER"]) ? 1 : 0;
                    DataRow[] rows = new DataRow[0];

                    string strCommaSeparatedDevIdToDelete = String.Empty;
                    foreach (MFEDevice devDtls in _deviceDtlsToDelete)
                    {
                        if (String.IsNullOrEmpty(strCommaSeparatedDevIdToDelete))
                            strCommaSeparatedDevIdToDelete += "'" + devDtls.DeviceId + "'";
                        else
                            strCommaSeparatedDevIdToDelete += "," + "'" + devDtls.DeviceId + "'";
                    }
                    if (!String.IsNullOrEmpty(strCommaSeparatedDevIdToDelete))
                    {
                        string filter = String.Format("DEVICE_ID NOT IN ({0})", strCommaSeparatedDevIdToDelete);
                        rows = registeredDeviceDtlAfterProcess.ResultTables.Tables[0].Select(filter);
                    }
                    #region Previous Code
                    //find the count of all the registered device except the devices that are to be removed
                    //int iTotalDeviceCount = rows.Length + iDesktopMessngr + approveDeviceCount;
                    //foreach (MFEDevice devDtls in _deviceDtlsToDelete)
                    //{
                        //processDeleteExistingSession(devDtls.DeviceId, devDtls.DeviceType);
                        //try
                        //{
                        //    DeviceDeletedRequest deviceDelReq = new DeviceDeletedRequest(Utilities.GetMsgRefID(), aryUser[0]["USER_NAME"].ToString()
                        //                                        , devDtls.DeviceId, _companyId, iTotalDeviceCount);
                        //    mBuzzClient client = new mBuzzClient(deviceDelReq.RequestJson, mBuzzConfigDtls.ServerIP, mBuzzConfigDtls.PortNumber);
                        //    ThreadPool.QueueUserWorkItem(new WaitCallback(client.Connect), null);
                        //    /**
                        //     * Here we are not deleting the device.
                        //     * Only session is being deleted so total device count willno not change.
                        //     * **/
                        //    //iTotalDeviceCount = iTotalDeviceCount - 1;
                        //}
                        //catch
                        //{ }

                    //}
                   
                    //foreach (MFEDevice devDtls in _deviceDtls)
                    //{
                    //    processDeleteExistingSession(devDtls.DeviceId, devDtls.DeviceType);
                    //    try
                    //    {
                    //        DeviceDeletedRequest deviceDelReq = new DeviceDeletedRequest(Utilities.GetMsgRefID(), aryUser[0]["USER_NAME"].ToString()
                    //                                            , devDtls.DeviceId, _companyId, iTotalDeviceCount);
                    //        mBuzzClient client = new mBuzzClient(deviceDelReq.RequestJson, mBuzzConfigDtls.ServerIP, mBuzzConfigDtls.PortNumber);
                    //        ThreadPool.QueueUserWorkItem(new WaitCallback(client.Connect), null);
                    //        iTotalDeviceCount = iTotalDeviceCount - 1;
                    //    }
                    //    catch
                    //    { }
                    //}
                    #endregion
                }
                try
                {
                    callWebServiceToDeleteSession(_deviceDtlsToDelete, Utilities.GetMd5Hash(DateTime.Now.Ticks + _companyId + _userId)
                        , _companyId, Convert.ToString(aryUser[0]["USER_NAME"]));
                }
                catch { }
                StatusCode = 0;
                StatusDescription = "";
            }
            catch
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
            }
        }

        #region Session Clear on user is blocked or deleted
        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId">Pass empty if log out type is not Device deleted</param>
        /// <param name="deviceType">Pass empty if log out type is not Device deleted</param>
        //void processDeleteExistingSession(string deviceId, string deviceType)
        //{
        //    bool blnDeviceDeleted = false;
        //    try
        //    {
        //        string strLogOutType = "";
        //        switch (_eLogOutType)
        //        {
        //            case USER_LOG_OUT_TYPE.USER_DELETED:
        //                strLogOutType = "User Deleted";
        //                break;
        //            case USER_LOG_OUT_TYPE.USER_BLOCKED:
        //                strLogOutType = "User Blocked";
        //                break;
        //            case USER_LOG_OUT_TYPE.DEVICE_DELETED:
        //                strLogOutType = "Device Deleted";
        //                blnDeviceDeleted = true;
        //                break;
        //        }
        //        if (_useConnection)
        //        {
        //            if (!blnDeviceDeleted)
        //                updateLoginHistory(_con, _transaction, strLogOutType);
        //            else
        //                updateLoginHistory(_con, _transaction, strLogOutType, deviceId, deviceType);
        //        }
        //        else
        //        {
        //            if (!blnDeviceDeleted)
        //                updateLoginHistory(strLogOutType);
        //            else
        //                updateLoginHistory(strLogOutType, deviceId, deviceType);
        //        }
        //    }
        //    catch (SqlException ex)
        //    {
        //        throw ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// For removing existing session when user is deleted or blocked
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlTransaction"></param>
        /// <param name="logOutTime"></param>
        /// <returns></returns>
//        int updateLoginHistory(SqlConnection con, SqlTransaction transaction, string logoutType)
//        {

//            string strQuery = @"UPDATE TBL_MOBILE_USER_LOGIN_HISTORY
//                                SET LOGOUT_DATETIME = @LogOutDateTime,
//                                LOGOUT_TYPE = @LogOutType
//                                WHERE USER_ID = @UserId
//                                AND LOGOUT_DATETIME = 0 AND  COMPANY_ID=@COMPANY_ID";
//            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@UserId", _userId);
//            cmd.Parameters.AddWithValue("@LogOutDateTime", DateTime.UtcNow.Ticks);
//            cmd.Parameters.AddWithValue("@LogOutType", logoutType);
//            cmd.Parameters.AddWithValue("@COMPANY_ID", _companyId);
//            return cmd.ExecuteNonQuery();
//        }

//        int updateLoginHistory(string logoutType)
//        {

//            string strQuery = @"UPDATE TBL_MOBILE_USER_LOGIN_HISTORY
//                                SET LOGOUT_DATETIME = @LogOutDateTime,
//                                LOGOUT_TYPE = @LogOutType
//                                WHERE USER_ID = @UserId
//                                AND LOGOUT_DATETIME = 0 AND  COMPANY_ID=@COMPANY_ID";
//            SqlCommand cmd = new SqlCommand(strQuery);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@UserId", _userId);
//            cmd.Parameters.AddWithValue("@LogOutDateTime", DateTime.UtcNow.Ticks);
//            cmd.Parameters.AddWithValue("@LogOutType", logoutType);
//            cmd.Parameters.AddWithValue("@COMPANY_ID", _companyId);
//            return MSSqlClient.ExecuteNonQueryRecord(cmd);
//        }
        /// <summary>
        /// For removing existing session when user's device is deleted 
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlTransaction"></param>
        /// <param name="userId"></param>
        /// <param name="logoutType"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
//        int updateLoginHistory(SqlConnection con, SqlTransaction transaction, string logoutType, string deviceId, string deviceType)
//        {

//            string strQuery = @"UPDATE TBL_MOBILE_USER_LOGIN_HISTORY
//                                SET LOGOUT_DATETIME = @LogOutDateTime,
//                                LOGOUT_TYPE = @LogOutType
//                                WHERE USER_ID = @UserId
//                                AND  COMPANY_ID = @COMPANY_ID
//                                AND LOGOUT_DATETIME = 0
//                                AND LOGIN_DEVICE_TOKEN =@LoginDeviceToken
//                                AND LOGIN_DEVICE_TYPE_CODE =@LoginDeviceTypeCode";
//            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@UserId", _userId);
//            cmd.Parameters.AddWithValue("@COMPANY_ID", _companyId);
//            cmd.Parameters.AddWithValue("@LogOutDateTime", DateTime.UtcNow.Ticks);
//            cmd.Parameters.AddWithValue("@LoginDeviceToken", deviceId);
//            cmd.Parameters.AddWithValue("@LoginDeviceTypeCode", deviceType);
//            cmd.Parameters.AddWithValue("@LogOutType", logoutType);
//            return cmd.ExecuteNonQuery();
//        }

//        int updateLoginHistory(string logoutType, string deviceId, string deviceType)
//        {

//            string strQuery = @"UPDATE TBL_MOBILE_USER_LOGIN_HISTORY
//                                SET LOGOUT_DATETIME = @LogOutDateTime,
//                                LOGOUT_TYPE = @LogOutType
//                                WHERE USER_ID = @UserId
//                                AND  COMPANY_ID = @COMPANY_ID
//                                AND LOGOUT_DATETIME = 0
//                                AND LOGIN_DEVICE_TOKEN =@LoginDeviceToken
//                                AND LOGIN_DEVICE_TYPE_CODE =@LoginDeviceTypeCode";
//            SqlCommand cmd = new SqlCommand(strQuery);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@UserId", _userId);
//            cmd.Parameters.AddWithValue("@COMPANY_ID", _companyId);
//            cmd.Parameters.AddWithValue("@LogOutDateTime", DateTime.UtcNow.Ticks);
//            cmd.Parameters.AddWithValue("@LoginDeviceToken", deviceId);
//            cmd.Parameters.AddWithValue("@LoginDeviceTypeCode", deviceType);
//            cmd.Parameters.AddWithValue("@LogOutType", logoutType);
//            return MSSqlClient.ExecuteNonQueryRecord(cmd);
//        }
        #endregion

        void callWebServiceToDeleteSession(List<MFEDevice> devicesToRemove, string requestId, string companyId, string userName)
        {
            try
            {
                string requestJson = getRequestJsonForDeletingSession(devicesToRemove, requestId, companyId, userName);
                GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(companyId);
                objServerUrl.Process();
                string strUrl = "";
                if (objServerUrl.StatusCode == 0)
                {
                    strUrl = objServerUrl.ServerUrl;
                    if (strUrl.Length > 0)
                    {
                        strUrl = strUrl + "/CPDeleteExistingSession.aspx?d=" + Utilities.UrlEncode(requestJson);
                    }
                }
                //strUrl = "http://localhost:50995/CPDeleteExistingSession.aspx?d=" + Utilities.UrlEncode(requestJson);
                if (strUrl.Length != 0)
                {
                    HTTP oHttp = new HTTP(strUrl);
                    oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                    HttpResponseStatus oResponse = oHttp.Request();
                    if (oResponse.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception();
                    }
                }
            }
            catch
            {
                throw new Exception();
            }
        }
        void callMbuzzWSToDeletePendingMsgs(MBUZZ_PENDING_MSGS_DELETE_TYPE pendingMsgDeleteType, string companyId, string userName, List<string> deviceIds)
        {
            try
            {
                string requestJson = getRequestJsonForDelMBuzzMsgs(pendingMsgDeleteType, companyId, userName
                                    , Utilities.GetMd5Hash(DateTime.UtcNow.Ticks + userName + companyId).Substring(0, 10)
                                    , deviceIds);
                string strUrl = MficientConstants.MBUZZ_SERVER_URL + "?d=" + Utilities.UrlEncode(requestJson);
                // string strUrl = "http://localhost:51848?d=" + Utilities.UrlEncode(requestJson);

                HTTP oHttp = new HTTP(strUrl);
                oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                HttpResponseStatus oResponse = oHttp.Request();
            }
            catch
            {

            }
        }
        string getRequestJsonForDeletingSession(List<MFEDevice> devicesToRemove, string requestId, string companyId, string userName)
        {
            try
            {
                List<DeviceDetail> lstDeviceDetails = new List<DeviceDetail>();
                foreach (MFEDevice device in devicesToRemove)
                {
                    //string[] aryDeviceDetails = device.Split(',');
                    DeviceDetail objDeviceDetail = new DeviceDetail();
                    objDeviceDetail.did = device.DeviceId;
                    objDeviceDetail.dtyp = device.DeviceType;
                    lstDeviceDetails.Add(objDeviceDetail);
                }
                RequestForDeleteSessionWebService objRequest = new RequestForDeleteSessionWebService();//the request
                CPDeleteSessionReqJsonFields objRequestJsonFields = new CPDeleteSessionReqJsonFields();//the fields of the request class
                CPDeleteSessionReqData objRequestDataFields = new CPDeleteSessionReqData();//the fields ot the data tab in requst json fields
                objRequestDataFields.enid = companyId;
                objRequestDataFields.unm = userName;
                objRequestDataFields.devc = lstDeviceDetails;
                objRequestJsonFields.rid = requestId;
                objRequestJsonFields.data = objRequestDataFields;
                objRequest.req = objRequestJsonFields;

                return Utilities.SerializeJson<RequestForDeleteSessionWebService>(objRequest);
            }
            catch
            {
                throw new Exception();
            }
        }

        string getRequestJsonForDelMBuzzMsgs(MBUZZ_PENDING_MSGS_DELETE_TYPE deleteType, string companyId, string userName, string requestId, List<string> deviceIds)
        {
            RequestForDeleteMBuzzMsgs objRequest = new RequestForDeleteMBuzzMsgs();
            MBuzzDeleteUsrMsgsReqJsonFields objRequestJsonFields = new MBuzzDeleteUsrMsgsReqJsonFields();
            MBuzzDeleteUsrMsgsReqData objReqDataFields = new MBuzzDeleteUsrMsgsReqData();
            objReqDataFields.devid = deviceIds;
            objReqDataFields.enid = companyId;
            objReqDataFields.unm = userName;
            objReqDataFields.type = ((int)deleteType).ToString();

            objRequestJsonFields.rid = requestId;
            objRequestJsonFields.func = ((int)MBUZZ_FUNCTION_CODES.DELETE_USERS_PENDING_MESSAGE_AND_REPORT).ToString();
            objRequestJsonFields.data = objReqDataFields;


            objRequest.req = objRequestJsonFields;

            return Utilities.SerializeJson<RequestForDeleteMBuzzMsgs>(objRequest);

        }


        public string UserId
        {
            get { return _userId; }
        }
        public string SubAdminId
        {
            get { return _subAdminId; }
        }

        public string CompanyId
        {
            get { return _companyId; }
        }
        public int StatusCode
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public USER_LOG_OUT_TYPE ELogOutType
        {
            get { return _eLogOutType; }
        }
        public bool UseConnection
        {
            get { return _useConnection; }
        }


        public SqlConnection Con
        {
            get { return _con; }
        }


        public SqlTransaction Transaction
        {
            get { return _transaction; }
        }

        internal MBUZZ_PENDING_MSGS_DELETE_TYPE MBuzzPendingMsgDelType
        {
            get { return _mBuzzPendingMsgDelType; }
        }

        public List<MFEDevice> CommaSeparatedDevIdAndDevType
        {
            get { return _deviceDtlsToDelete; }
        }
    }
}
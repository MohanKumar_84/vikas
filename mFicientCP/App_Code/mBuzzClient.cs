﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using Plexus;
//using System.Threading;
//using System.Net.Sockets;

//namespace mFicientCP
//{
//    public enum MessageCode
//    {
//        NEW_CONTACT_ADDED_OR_UNBLOCKED = 124,
//        CONTACT_BLOCKED = 125,
//        CONTACT_DELETED = 126,
//        CONTACT_NO_DEVICE = 127,
//        DEVICE_DELETED = 128,
//        DESKTOP_MESSENGER_DELETED = 129, 
//    }

//    public enum DataSendStatus
//    {
//        PENDING = 0,
//        SENT = 1,
//        FAILED = 2
//    }

//    public class mBuzzClient
//    {
//        #region Private Members

//        private WinSocket objClient;

//        private int SERVER_BUFFER_SIZE = 8192;
//        private int CODEPAGE_1252 = 1252;
//        private int port ;//= 49001;
//        private int Retry = 3;
//        int reConnect = 0;

//        private bool taskCompleted;

//        private string END_TERMINATOR = "\r\n$END$\r\n", message, hostIPAddress;// = "108.166.71.129";
//        private object objDataSendLock = new object();
//        private DataSendStatus messageSendStatus = DataSendStatus.PENDING;

//        #endregion

//        #region Constructor

//        public mBuzzClient(string _message, string _hostIPAddress, int _port)
//        {
//            message = _message;
//            port = _port;
//            hostIPAddress = _hostIPAddress;
//            initWinSocketClient();
//        }

//        #endregion

//        #region Public Methods

//        public void Connect(object state)
//        {

//            try
//            {
//                if (objClient == null)
//                {
//                    initWinSocketClient();
//                }

//                objClient.Connect(hostIPAddress, port);

//                while (objClient.State != SocketStates.Connected)
//                {
//                    System.Threading.Thread.Sleep(100);
//                }
//            }
//            catch
//            {
//                int reConnect = 0;
//                if (reConnect < Retry)
//                {
//                    Connect(null);
//                    reConnect += 1;
//                }
//            }
//            finally
//            {
//            }
//        }

//        #endregion

//        #region Private Methods

//        private void initWinSocketClient()
//        {
//            try
//            {
//                objClient = new WinSocket();

//                if (objClient != null)
//                {
//                    objClient.ErrorReceived -= objClient_ErrorReceived;
//                    //objClient.StateChanged -= objClient_StateChanged;
//                    objClient.DataArrival -= objClient_DataArrival;
//                    objClient.Disconnected -= objClient_Disconnected;
//                    objClient.Connected -= objClient_Connected;
//                    objClient.SendComplete -= objClient_SendComplete;

//                    objClient.ErrorReceived += new IWinSocket.ErrorReceivedEventHandler(objClient_ErrorReceived);
//                    //objClient.StateChanged += new IWinSocket.StateChangedEventHandler(objClient_StateChanged);
//                    objClient.DataArrival += new IWinSocket.DataArrivalEventHandler(objClient_DataArrival);
//                    objClient.Disconnected += new IWinSocket.DisconnectedEventHandler(objClient_Disconnected);
//                    objClient.Connected += new IWinSocket.ConnectedEventHandler(objClient_Connected);
//                    objClient.SendComplete += new IWinSocket.SendCompleteEventHandler(objClient_SendComplete);
//                }

//                objClient.BufferSize = SERVER_BUFFER_SIZE;
//                objClient.LegacySupport = true;
//                objClient.Protocol = SocketProtocols.Tcp;
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }

//        private void send()
//        {
//            if (Monitor.TryEnter(objDataSendLock, 300000))
//            {
//                try
//                {
//                    messageSendStatus = DataSendStatus.PENDING;
//                    long sendTimeTicks = DateTime.Now.Ticks;
//                    if ((objClient == null) || (objClient.NetworkStream == null)) throw new Exception();
//                    objClient.Send(message + END_TERMINATOR);

//                    while (messageSendStatus == DataSendStatus.PENDING)
//                    {
//                        Thread.Sleep(10);
//                        TimeSpan elapsedTime = TimeSpan.FromTicks(DateTime.Now.Ticks - sendTimeTicks);
//                        if (elapsedTime > TimeSpan.FromTicks(TimeSpan.TicksPerMillisecond * 30000)) break;
//                    }
//                    if (messageSendStatus != DataSendStatus.SENT)
//                        throw new Exception(@"Message sending Failed");
//                }
//                catch
//                {
//                    messageSendStatus = DataSendStatus.FAILED;
//                    throw new Exception(@"Message sending Failed");
//                }
//                finally
//                {
//                    Monitor.Exit(objDataSendLock);
//                }
//            }
//        }

//        private void close()
//        {
//            try
//            {
//                if (objClient == null) return;
//                objClient.Close();
//            }
//            catch
//            {
//            }
//            finally
//            {
//                if (objClient != null) objClient.Dispose();
//            }
//        }

//        #endregion

//        #region Plexus Events

//        private void objClient_ErrorReceived(object sender, SocketErrorReceivedEventArgs e)
//        {
//            try
//            {
//                messageSendStatus = DataSendStatus.FAILED;
//                if (e.ErrorCode != SocketError.Success && messageSendStatus != DataSendStatus.SENT && reConnect < Retry)
//                {
//                    reConnect += 1;
//                    Connect(null);
//                }
//            }
//            catch
//            {
//            }
//        }

//        private void objClient_Connected(object sender, EventArgs e)
//        {
//            if (objClient == null)
//            {
//                return;
//            }
//            try
//            {
//                if (objClient.State == SocketStates.Connected)
//                {
//                    send();
//                }

//                while (!taskCompleted)
//                {
//                    System.Threading.Thread.Sleep(100);
//                }
//            }
//            catch
//            {
//            }
//        }

//        private void objClient_Disconnected(object sender, EventArgs e)
//        {
//            try
//            {
//                string state = objClient.State.ToString();
//            }
//            catch
//            {
//            }
//        }

//        private void objClient_DataArrival(object sender, SocketDataArrivalEventArgs e)
//        {
//            try
//            {
//                string stringData = "";
//                object inData = ((WinSocket)sender).Get();

//                if (inData.GetType() == typeof(System.String))
//                {
//                    stringData = inData.ToString();
//                }
//                else if (inData.GetType() == typeof(System.Byte[]))
//                {
//                    stringData = System.Text.Encoding.GetEncoding(CODEPAGE_1252).GetString((byte[])inData);
//                }
//                if (stringData == "OK")
//                {
//                    close();
//                    taskCompleted = true;
//                }
//            }
//            catch
//            {
//            }
//        }

//        //private void objClient_StateChanged(object sender, SocketStateChangedEventArgs e)
//        //{
//        //    switch (e.New_State)
//        //    {
//        //        case SocketStates.Connected:
//        //            break;

//        //        case SocketStates.Closing:
//        //            break;

//        //        case SocketStates.Closed:
//        //            break;
//        //    }
//        //}

//        private void objClient_SendComplete(object sender, SocketSendEventArgs e)
//        {
//            messageSendStatus = DataSendStatus.SENT;
//        }

//        #endregion
//    }
//}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class UpdateWsCommand
    {
        
        //public UpdateWsCommand(string _CommandId, string _SubAdminId, string _Url, string _Parameter, string _ReturnType, string _RecordPath, string _DataSetPath, 
        //    string _Tag, string _Service, string _PortType, string _Method, string _WebserviceType, string _SoapRequest, string _OperationAction, string _XmlRpc,
        //    Int32 _HttpType, string _Company_id, string _Description, int _Cache, int _Expfrequency, string _ExpCondtion, long _Updatedon, string _ReferenceJson,
        //    string _Headers)
        //{
        //    string _HttpRequest = "";
        //    this.CommandId = _CommandId;
        //    this.SubAdminId = _SubAdminId;
        //    this.Url = _Url;
        //    this.Parameter = _Parameter;
        //    this.ReturnType = _ReturnType;
        //    this.RecordPath = _RecordPath;
        //    this.DataSetPath = _DataSetPath;
        //    this.Tag = _Tag;
        //    this.Service = _Service;
        //    this.PortType = _PortType;
        //    this.Method = _Method;
        //    this.SoapRequest = _SoapRequest;
        //    this.HttpRequest = _HttpRequest;
        //    this.OperationAction = _OperationAction;
        //    this.WebserviceType = _WebserviceType;
        //    this.XmlRpc = _XmlRpc;
        //    this.HttpType = _HttpType;
        //    this.Company_id = _Company_id;
        //    this.Description = _Description;
        //    this.Cache = _Cache;
        //    this.Expfrequency = _Expfrequency;
        //    this.ExpCondtion = _ExpCondtion;
        //    this.UpdatedOn = _Updatedon;
        //    this.Xreferences = _ReferenceJson;
        //    this.Headers = _Headers;
        //}
        public UpdateWsCommand(string _CommandId, string _SubAdminId, string _Url, string _Parameter, string _ReturnType, string _RecordPath, string _DataSetPath, string _Tag,
            string _Service, string _PortType, string _Method, string _WebserviceType, string _SoapRequest, string _OperationAction, string _XmlRpc, Int32 _HttpType,
            string _Company_id, string _Description, int _Cache, int _Expfrequency, string _ExpCondtion, long _Updatedon, string _ReferenceJson,string _HttpDataTemplate,
            string _Headers)
        {
            string _HttpRequest = "";
            this.CommandId = _CommandId;
            this.SubAdminId = _SubAdminId;
            this.Url = _Url;
            this.Parameter = _Parameter;
            this.ReturnType = _ReturnType;
            this.RecordPath = _RecordPath;
            this.DataSetPath = _DataSetPath;
            this.Tag = _Tag;
            this.Service = _Service;
            this.PortType = _PortType;
            this.Method = _Method;
            this.SoapRequest = _SoapRequest;
            this.HttpRequest = _HttpRequest;
            this.OperationAction = _OperationAction;
            this.WebserviceType = _WebserviceType;
            this.XmlRpc = _XmlRpc;
            this.HttpType = _HttpType;
            this.Company_id = _Company_id;
            this.Description = _Description;
            this.Cache = _Cache;
            this.Expfrequency = _Expfrequency;
            this.ExpCondtion = _ExpCondtion;
            this.UpdatedOn = _Updatedon;
            this.Xreferences = _ReferenceJson;
            this.HttpDatatemplate = _HttpDataTemplate;

            
            this.Headers = _Headers;
        }

        public void Process1()
        {
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            SqlCommand objSqlCommand;
            try
            {
                this.StatusCode = -1000;
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                string query = @"UPDATE TBL_WS_COMMAND SET
                                        URL=@URL,
                                        PARAMETER=@PARAMETER,
                                        RETURN_TYPE=@RETURN_TYPE,
                                        RECORD_PATH=@RECORD_PATH,
                                        DATASET_PATH=@DATASET_PATH,
                                        TAG=@TAG,
                                        SERVICE=@SERVICE,
                                        PORT_TYPE=@PORT_TYPE,
                                        METHOD=@METHOD,
                                        WEBSERVICE_TYPE=@WEBSERVICE_TYPE,
                                        HTTP_TYPE=@HTTP_TYPE,
                                        OPERATION_ACTION=@OPERATION_ACTION,
                                        SOAP_REQUEST=@SOAP_REQUEST,
                                        HTTP_REQUEST=@HTTP_REQUEST,
                                        DESCRIPTION = @DESCRIPTION,
                                        CACHE = @CACHE,
                                        EXPIRY_FREQUENCY=@EXPIRY_FREQUENCY,
                                        EXPIRY_CONDITION=@EXPIRY_CONDITION,
                                        SUBADMIN_ID=@SUBADMIN_ID,
                                        X_REFERENCES=@X_REFERENCES,
                                        UPDATED_ON=@UPDATED_ON,
                                        HEADER=@HEADER
                                        WHERE WS_COMMAND_ID=@WS_COMMAND_ID  AND COMPANY_ID=@COMPANY_ID;";

                objSqlTransaction = objSqlConnection.BeginTransaction();
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WS_COMMAND_ID", this.CommandId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@URL", this.Url);
                objSqlCommand.Parameters.AddWithValue("@PARAMETER", this.Parameter);
                objSqlCommand.Parameters.AddWithValue("@RETURN_TYPE", this.ReturnType);
                objSqlCommand.Parameters.AddWithValue("@RECORD_PATH", this.RecordPath);
                objSqlCommand.Parameters.AddWithValue("@DATASET_PATH", this.DataSetPath);
                objSqlCommand.Parameters.AddWithValue("@TAG", this.Tag);
                objSqlCommand.Parameters.AddWithValue("@SERVICE", this.Service);
                objSqlCommand.Parameters.AddWithValue("@PORT_TYPE", this.PortType);
                objSqlCommand.Parameters.AddWithValue("@METHOD", this.Method);
                objSqlCommand.Parameters.AddWithValue("@WEBSERVICE_TYPE", this.WebserviceType);
                objSqlCommand.Parameters.AddWithValue("@SOAP_REQUEST", this.SoapRequest);
                objSqlCommand.Parameters.AddWithValue("@HTTP_REQUEST", this.HttpRequest);
                objSqlCommand.Parameters.AddWithValue("@OPERATION_ACTION", this.OperationAction);
                objSqlCommand.Parameters.AddWithValue("@HTTP_TYPE", this.HttpType);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
                objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", this.Description);
                objSqlCommand.Parameters.AddWithValue("@CACHE", this.Cache);
                objSqlCommand.Parameters.AddWithValue("@EXPIRY_FREQUENCY", this.Expfrequency);
                objSqlCommand.Parameters.AddWithValue("@EXPIRY_CONDITION", this.ExpCondtion);
                objSqlCommand.Parameters.AddWithValue("@X_REFERENCES", this.Xreferences);
                objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", this.UpdatedOn);
                objSqlCommand.Parameters.AddWithValue("@HEADER", this.Headers);

                if (objSqlCommand.ExecuteNonQuery() > 0)
                    this.StatusCode = 0;
                else
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                objSqlTransaction.Commit();
            }
            catch
            {
                this.StatusCode = -1000;
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }

        public void Process2()
        {
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            SqlCommand objSqlCommand;
            try
            {
                this.StatusCode = -1000;
                MSSqlClient.SqlConnectionOpen(out objSqlConnection);
                string query = @"UPDATE TBL_WS_COMMAND SET
                                    URL=@URL,
                                    PARAMETER=@PARAMETER,
                                    RETURN_TYPE=@RETURN_TYPE,
                                    RECORD_PATH=@RECORD_PATH,
                                    DATASET_PATH=@DATASET_PATH,
                                    TAG=@TAG,
                                    SERVICE=@SERVICE,
                                    PORT_TYPE=@PORT_TYPE,
                                    METHOD=@METHOD,
                                    WEBSERVICE_TYPE=@WEBSERVICE_TYPE,
                                    HTTP_TYPE=@HTTP_TYPE,
                                    OPERATION_ACTION=@OPERATION_ACTION,
                                    SOAP_REQUEST=@SOAP_REQUEST,
                                    DESCRIPTION = @DESCRIPTION,
                                    CACHE = @CACHE,
                                    EXPIRY_FREQUENCY=@EXPIRY_FREQUENCY,
                                    EXPIRY_CONDITION=@EXPIRY_CONDITION,
                                    SUBADMIN_ID=@SUBADMIN_ID,
                                    X_REFERENCES=@X_REFERENCES,
                                    UPDATED_ON=@UPDATED_ON,
                                    HTTP_REQUEST=@HTTP_REQUEST,
                                    HEADERS=@HEADERS
                                    WHERE WS_COMMAND_ID=@WS_COMMAND_ID AND COMPANY_ID=@COMPANY_ID;";

                objSqlTransaction = objSqlConnection.BeginTransaction();
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@WS_COMMAND_ID", this.CommandId);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@URL", this.Url);
                objSqlCommand.Parameters.AddWithValue("@PARAMETER", this.Parameter);
                objSqlCommand.Parameters.AddWithValue("@RETURN_TYPE", this.ReturnType);
                objSqlCommand.Parameters.AddWithValue("@RECORD_PATH", this.RecordPath);
                objSqlCommand.Parameters.AddWithValue("@DATASET_PATH", this.DataSetPath);
                objSqlCommand.Parameters.AddWithValue("@TAG", this.Tag);
                objSqlCommand.Parameters.AddWithValue("@SERVICE", this.Service);
                objSqlCommand.Parameters.AddWithValue("@PORT_TYPE", this.PortType);
                objSqlCommand.Parameters.AddWithValue("@METHOD", this.Method);
                objSqlCommand.Parameters.AddWithValue("@WEBSERVICE_TYPE", this.WebserviceType);
                objSqlCommand.Parameters.AddWithValue("@SOAP_REQUEST", this.SoapRequest);
                //objSqlCommand.Parameters.AddWithValue("@HTTP_REQUEST", this.HttpRequest);
                objSqlCommand.Parameters.AddWithValue("@OPERATION_ACTION", this.OperationAction);
                objSqlCommand.Parameters.AddWithValue("@HTTP_TYPE", this.HttpType);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.Company_id);
                objSqlCommand.Parameters.AddWithValue("@DESCRIPTION", this.Description);
                objSqlCommand.Parameters.AddWithValue("@CACHE", this.Cache);
                objSqlCommand.Parameters.AddWithValue("@EXPIRY_FREQUENCY", this.Expfrequency);
                objSqlCommand.Parameters.AddWithValue("@EXPIRY_CONDITION", this.ExpCondtion);
                objSqlCommand.Parameters.AddWithValue("@X_REFERENCES", this.Xreferences);
                objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", this.UpdatedOn);
                objSqlCommand.Parameters.AddWithValue("@HTTP_REQUEST", this.HttpDatatemplate);
                objSqlCommand.Parameters.AddWithValue("@HEADERS", this.Headers);
                if (objSqlCommand.ExecuteNonQuery() > 0)
                    this.StatusCode = 0;
                else
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                objSqlTransaction.Commit();
            }
            catch
            {
                this.StatusCode = -1000;
                if (objSqlTransaction != null)
                    objSqlTransaction.Rollback();
            }
            finally
            {
                MSSqlClient.SqlConnectionClose(objSqlConnection);
            }
        }



        public string SubAdminId { set; get; }

        public string ReturnType { set; get;}

        public string Parameter { set; get; }

        public string CommandId { set; get;}

        public string Url { get; set; }

        public string Tag { get; set; }

        public string DataSetPath { get; set; }

        public string RecordPath { get; set; }
        
        public int StatusCode { set; get; }

        public string Method { get; set; }

        public string PortType { get; set; }

        public string Service { get; set; }

        public string SoapRequest { get; set; }

        public string OperationAction { get; set; }

        public string WebserviceType { get; set; }

        public string XmlRpc { get; set; }

        public Int32 HttpType { get; set; }

        public string Company_id { get; set; }

        public string HttpRequest { get; set; }

        public string Description { get; set; }
        public int Cache { get; set; }

        public int Expfrequency { get; set; }

        public string ExpCondtion { get; set; }

        public long UpdatedOn { get; set; }
        public string Xreferences { get; set; }
        public string HttpDatatemplate { get; set; }
        public string Headers { get; set; }
    }
}
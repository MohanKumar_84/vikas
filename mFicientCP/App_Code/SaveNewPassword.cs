﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class SaveNewPassword
    {
        /// <summary>
        /// Pass
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <param name="companyId"></param>
        /// <param name="accessCode">Pass real access code.It is MD5'd while saving.</param>
        /// <param name="userType"></param>
        public SaveNewPassword(string userId,
            string userName,
            string companyId,
            string accessCode,
            string emailId,
            RESET_PASSWORD_REQUEST_BY userType,
            HttpContext context)
        {
            this.UserId = userId;
            this.UserName = userName;
            this.CompanyId = companyId;
            this.AccessCode = accessCode;
            this.EmailId = emailId;
            this.PwdResetRequestedBy = userType;
            this.Context = context;
        }

        public void Process()
        {
            try
            {
                StatusCode = -1000;
                SqlTransaction transaction = null;
                SqlConnection con = null;
                try
                {
                    MSSqlClient.SqlConnectionOpen(out con);
                    using (con)
                    {
                        using (transaction = con.BeginTransaction())
                        {
                            int iRowsAffected = 0;
                            switch (this.PwdResetRequestedBy)
                            {
                                case RESET_PASSWORD_REQUEST_BY.SubAdmin:
                                    iRowsAffected = updateSubAdminAccessCode(transaction, con);
                                    break;
                                case RESET_PASSWORD_REQUEST_BY.Admin:
                                    iRowsAffected = updateAdminAccessCodeInAdminTbl(transaction, con);
                                    if (iRowsAffected == 0)
                                    {
                                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                                    }
                                    iRowsAffected = updateAdminAccessCode(transaction, con);
                                    break;
                                case RESET_PASSWORD_REQUEST_BY.User:
                                    iRowsAffected = updateUserDetailAccessCode(transaction, con);
                                    break;
                            }
                            if (iRowsAffected > 0)
                            {
                                updatePwdLogIsUsedAndIsExpiredStatus(transaction, con);
                                SaveEmailInfo.cpChangePassword(
                                    this.EmailId,
                                    this.UserName,
                                    this.AccessCode,
                                    DateTime.UtcNow.Ticks,
                                    CompanyTimezone.getTimezoneInfo(this.CompanyId),
                                    Context
                                    );
                            }
                            else
                            {
                                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                            }
                            transaction.Commit();
                            StatusCode = 0;
                        }
                    }
                }
                catch
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
                finally
                {
                    if (con != null)
                    {
                        con.Dispose();
                    }
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        int updateUserDetailAccessCode(SqlTransaction transaction, SqlConnection con)
        {
            string strQuery = @"UPDATE TBL_USER_DETAIL
                                SET ACCESS_CODE = @AccessCode
                                WHERE USER_ID =@UserId";

            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@AccessCode", Utilities.GetMd5Hash(AccessCode).ToUpper());
            return cmd.ExecuteNonQuery();
        }
        int updateAdminAccessCode(SqlTransaction transaction, SqlConnection con)
        {
            string strQuery = @"UPDATE TBL_COMPANY_ADMINISTRATOR
                                SET ACCESS_CODE = @AccessCode
                                WHERE ADMIN_ID =@UserId";

            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@AccessCode", Utilities.GetMd5Hash(AccessCode).ToUpper());
            return cmd.ExecuteNonQuery();
        }
        int updateAdminAccessCodeInAdminTbl(SqlTransaction transaction, SqlConnection con)
        {
            string strQuery = @"UPDATE ADMIN_TBL_COMPANY_ADMINISTRATOR
                                SET ACCESS_CODE = @AccessCode
                                WHERE ADMIN_ID =@UserId";

            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@AccessCode", Utilities.GetMd5Hash(AccessCode).ToUpper());
            return cmd.ExecuteNonQuery();
        }
        int updateSubAdminAccessCode(SqlTransaction transaction, SqlConnection con)
        {
            string strQuery = @"UPDATE TBL_SUB_ADMIN
                                SET ACCESS_CODE = @AccessCode
                                WHERE SUBADMIN_ID =@UserId";

            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@AccessCode", Utilities.GetMd5Hash(AccessCode).ToUpper());
            return cmd.ExecuteNonQuery();
        }
        int updatePwdLogIsUsedAndIsExpiredStatus(SqlTransaction transaction, SqlConnection con)
        {
            string strQuery = @"UPDATE TBL_RESET_PASSWORD_LOG
                                SET IS_USED = 1
                                , IS_EXPIRED = 1
                                WHERE USER_ID = @UserId
                                AND IS_USED = 0
                                AND IS_EXPIRED = 0";

            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            return cmd.ExecuteNonQuery();
        }



        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string UserName
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string UserId
        {
            set;
            get;
        }
        public string AccessCode
        {
            set;
            get;
        }
        public RESET_PASSWORD_REQUEST_BY PwdResetRequestedBy
        {
            get;
            private set;
        }
        public string EmailId
        {
            get;
            private set;
        }
        public HttpContext Context
        {
            get;
            private set;
        }
    }



}
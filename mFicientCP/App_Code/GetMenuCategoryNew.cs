﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetMenuCategoryNew
    {
        public GetMenuCategoryNew(string companyId)
        {
            this.CompanyId = companyId;
        }
        public GetMenuCategoryNew(string companyId,string catgoryId)
        {
            this.CompanyId = companyId;
            this.CatId = catgoryId;
            GetcategoryDetail();
        }

        public void GetcategoryDetail()
        {
            try
            {
                StatusCode = -1000;
                string query = @"select *  from dbo.TBL_MENU_CATEGORY where COMPANY_ID=@CompanyId and MENU_CATEGORY_ID=@MENU_CATEGORY_ID;";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@MENU_CATEGORY_ID", this.CatId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                    this.StatusCode = 0;
                this.ResultTable = objDataSet.Tables[0];
            }
            catch
            {
                this.StatusCode = -1000;
            }
        }
        public void GetCategoryList()
        {
            try
            {
                StatusCode = -1000;//for error
                string query = @"SELECT * FROM TBL_MENU_CATEGORY
                                WHERE COMPANY_ID = @CompanyId
                                ORDER BY DISPLAY_INDEX;";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);

                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                    this.StatusCode = 0;
                this.ResultTable = objDataSet.Tables[0];
            }
            catch
            {
                this.StatusCode = -1000;

            }
        }

        public void Process()
        {
            try
            {
                StatusCode = -1000;//for 
                string query = @"select *  from dbo.TBL_MENU_CATEGORY where COMPANY_ID=@CompanyId order by DISPLAY_INDEX ASC;
                         
                         select c.CATEGORY_ID,w.WF_NAME,w.WF_ID from TBL_WORKFLOW_AND_CATEGORY_LINK as c
                         inner join TBL_WORKFLOW_DETAIL as w on c.WORKFLOW_ID=w.WF_ID where w.COMPANY_ID=@CompanyId
                         group by c.CATEGORY_ID,w.WF_NAME,w.WF_ID;
SELECT * INTO #newtable1 from (SELECT distinct W.WF_ID ,W.WF_NAME,W.COMPANY_ID AS COM ,M.MENU_CATEGORY_ID,M.CATEGORY,M.COMPANY_ID FROM TBL_WORKFLOW_DETAIL AS W INNER JOIN dbo.TBL_MENU_CATEGORY AS M
ON M.COMPANY_ID=W.COMPANY_ID WHERE W.COMPANY_ID=@CompanyId AND M.COMPANY_ID=@CompanyId AND NOT EXISTS ( SELECT * FROM dbo.TBL_WORKFLOW_AND_CATEGORY_LINK AS C
WHERE    WF_ID=C.WORKFLOW_ID AND M.MENU_CATEGORY_ID=C.CATEGORY_ID AND C.COMPANY_ID=@CompanyId ))D
select ss.CATEGORY,ss.MENU_CATEGORY_ID,STUFF((select ',' + T.WF_ID from #newtable1 as T where ss.MENU_CATEGORY_ID=T.MENU_CATEGORY_ID for xml path('')),1,1,'') as WF_ID,
STUFF((select ',' + T.WF_NAME from #newtable1 as T where ss.MENU_CATEGORY_ID=T.MENU_CATEGORY_ID for xml path('')),1,1,'') as WF_NAME
from #newtable1 as ss group by ss.CATEGORY,ss.MENU_CATEGORY_ID DROP TABLE #newtable1";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);

                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                    this.StatusCode = 0;
                this.ResultTable = objDataSet.Tables[0];
                this.ResultTable1 = objDataSet.Tables[1];
                this.ResultTable2 = objDataSet.Tables[2];
            }
            catch 
            {
                this.StatusCode = -1000;
              //  throw ex;
            }
        }
        public string CompanyId
        {
            set;
            get;
        }

        public string CatId
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
        public DataTable ResultTable1
        {
            set;
            get;
        }
        public DataTable ResultTable2
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetUserDetail
    {
        public enum ORDER_BY
        {
            Name = 1,
            Email = 2,
            Logon = 3
        }
        public enum SORT_TYPE
        {
            ASC = 0,
            DESC = 1,
            NONE = 3
        }
        public GetUserDetail()
        {

        }
       

        /// <summary>
        /// Added on 17/5/2012
        /// Mohan For addUserDetails change
        /// </summary>
        /// <param name="_IsSelectAll"></param>
        /// <param name="_IsFindByUserId"></param>
        /// <param name="_SubAdminId"></param>
        /// <param name="_UserId"></param>
        /// <param name="_userName"></param>
        /// <param name="_companyId"></param>
        public GetUserDetail(Boolean _IsSelectAll, Boolean _IsFindByUserId, string _SubAdminId, string _UserId, string _userName, string _companyId)
        {
            this.SubAdminId = _SubAdminId;
            this.IsFindByUserId = _IsFindByUserId;
            this.IsSelectAll = _IsSelectAll;
            this.UserId = _UserId;
            this.UserName = _userName;
            this.CompanyId = _companyId;
        }

        public GetUserDetail(Boolean _IsSelectAll, Boolean _FindByCompanyId, Boolean _IsFindByUserId, string _companyId, string _filter)
        {
            this.IsFindByUserId = _IsFindByUserId;
            this.IsSelectAll = _IsSelectAll;
            this.CompanyId = _companyId;
            this.IsFindByCompanyId = _FindByCompanyId;
            this.Filter = _filter;
        }
        public GetUserDetail(Boolean _IsSelectAll, Boolean _FindByCompanyId, Boolean _IsFindByUserId, string _SubAdminId, string _companyId, string _filter)
        {
            this.SubAdminId = _SubAdminId;
            this.IsFindByUserId = _IsFindByUserId;
            this.IsSelectAll = _IsSelectAll;
            this.CompanyId = _companyId;
            this.IsFindByCompanyId = _FindByCompanyId;
            this.Filter = _filter;
        }

        public GetUserDetail(string _companyId)
        {
            this.CompanyId = _companyId;
            Processgettesterlist();
        }

        public GetUserDetail(string  _mobileuser, string _companyId)
        {
            this.CompanyId = _companyId;
            this.Mobileuser = _mobileuser;
            Processgetsubadminuser();
        }



        public GetUserDetail(string _companyId, Boolean _IsFindByUserId)
        {
            this.CompanyId = _companyId;
            ProcessIstester();
        }

        public GetUserDetail(Boolean _IsFindByUserId, string _companyid)
        {
            this.CompanyId = _companyid;
            ProcesstogetAutoapprovedUserlist();
        }
        string query = @"SELECT  isnull(logon,0) as ulon,Usr.*,CurrPlan.MAX_USER,USER_COUNT,
                            (case when D.DEVICE IS  NULL then 'No Device Registered Yet' else D.Device end )DEVICE,
                            (case when D.DeviceVersion IS  NULL then 'No Version' else D.DeviceVersion end)DeviceVersion,
                            (case when D.DeviceType IS  NULL then 'No Device' else D.DeviceType end)DeviceType
                            FROM  TBL_USER_DETAIL AS Usr
                            RIGHT OUTER JOIN TBL_COMPANY_CURRENT_PLAN AS CurrPlan ON Usr.COMPANY_ID = CurrPlan.COMPANY_ID
                            INNER JOIN (SELECT COUNT(USER_ID) as USER_COUNT, COMPANY_ID  FROM TBL_USER_DETAIL WHERE COMPANY_ID =@CompanyId GROUP BY COMPANY_ID) Cnt on Cnt.COMPANY_ID=Usr.COMPANY_ID 
                            LEFT OUTER JOIN (select ss.USER_ID ,STUFF((select ',' + T.DEVICE_MODEL from dbo.TBL_REGISTERED_DEVICE as T where ss.USER_ID=T.USER_ID
                                    for xml path('')),1,1,'') as  DEVICE,STUFF((select ',' + T.OS_VERSION from dbo.TBL_REGISTERED_DEVICE as T where ss.USER_ID=T.USER_ID
                                    for xml path('')),1,1,'') as  DeviceVersion,STUFF((select ',' + T.DEVICE_TYPE from dbo.TBL_REGISTERED_DEVICE as T where ss.USER_ID=T.USER_ID
                                    for xml path('')),1,1,'') as  DeviceType from dbo.TBL_REGISTERED_DEVICE as ss WHERE ss.COMPANY_ID=@CompanyId group by ss.USER_ID)as D
                            ON  D.USER_ID=Usr.USER_ID
                            left outer join (SELECT distinct para1 as username,max(activity_datetime) as logon from TBL_ENTERPRISE_ACTIVITY_LOG
                                where activity_type=201 and company_id=@CompanyId group by para1) as ulog on ulog.username=Usr.user_name ";
        public void Process()
        {
            try
            {
                StatusCode = -1000;//for error
               
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                if (this.IsSelectAll)
                {
                    //Query changed for addUserDetails
                    //We needed count of number of users in a particular company
                    //old query query = @"SELECT  * FROM  TBL_USER_DETAIL WHERE SUBADMIN_ID=@SUBADMIN_ID ORDER BY FIRST_NAME;";
                    query += " ORDER BY FIRST_NAME";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    //objSqlCommand.Parameters.AddWithValue("@SubAdminId", this.SubAdminId);
                    objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                }
                else if (this.IsFindByCompanyId)
                {
                    if (!String.IsNullOrEmpty(Filter))
                    {
                        query +=@" WHERE Usr.COMPANY_ID = @CompanyID  AND (Usr.USER_NAME LIKE '%" + Filter + "%' OR Usr.EMAIL_ID LIKE '%" + Filter + "%' OR Usr.FIRST_NAME LIKE '%" + Filter + "%')";
                    }
                    else
                    {
                        query = "SELECT * FROM TBL_USER_DETAIL WHERE COMPANY_ID = @CompanyID";
                    }
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                }
                else
                {
                    if (this.IsFindByUserId)
                    {
                        //query = @"SELECT  * FROM  TBL_USER_DETAIL WHERE USER_ID=@USER_ID AND SUBADMIN_ID=@SUBADMIN_ID ORDER BY FIRST_NAME;";
                        query = @"SELECT  * FROM  TBL_USER_DETAIL WHERE USER_ID=@USER_ID and  COMPANY_ID = @CompanyID  ORDER BY FIRST_NAME;";
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                        // objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                        objSqlCommand.Parameters.AddWithValue("@CompanyID", this.CompanyId);
                    }
                    else
                    {
                        //query = @"SELECT  * FROM  TBL_USER_DETAIL WHERE USER_NAME=@USER_NAME AND SUBADMIN_ID=@SUBADMIN_ID ORDER BY FIRST_NAME;";
                        query = @"SELECT  * FROM  TBL_USER_DETAIL WHERE USER_NAME=@USER_NAME and  COMPANY_ID = @CompanyID ORDER BY FIRST_NAME;";
                        objSqlCommand = new SqlCommand(query);
                        objSqlCommand.CommandType = CommandType.Text;
                        objSqlCommand.Parameters.AddWithValue("@USER_NAME", this.UserName);
                        objSqlCommand.Parameters.AddWithValue("@CompanyID", this.CompanyId);
                        //objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                    }
                }

                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    if (ResultTable != null)
                    {
                        this.TotalUsersCount = ResultTable.Rows.Count;
                        if (TotalUsersCount > 0)
                        {
                            if (IsSelectAll)
                            {
                                this.UserCount = Convert.ToInt32(ResultTable.Rows[0]["USER_COUNT"]);
                                this.MaxUsers = Convert.ToInt32(ResultTable.Rows[0]["MAX_USER"]);
                            }
                            if (IsFindByCompanyId && !String.IsNullOrEmpty(Filter))
                                this.MaxUsers = Convert.ToInt32(ResultTable.Rows[0]["MAX_USER"]);
                        }
                    }
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }

            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
                //throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
        }

        public void Processgettesterlist()
        {
           
                StatusCode = -1000;//for error
                string query;
                DataSet objDataSet;
                query = @"select FIRST_NAME  +SPACE(1) + LAST_NAME  + SPACE(1) +'(' + SPACE(1)+ USER_NAME  + SPACE(1)    +')'   AS username ,USER_ID from dbo.TBL_USER_DETAIL WHERE COMPANY_ID=@CompanyId";
                SqlCommand cmd = new SqlCommand(query);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (objDataSet != null)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
            
                
        }


        public void Processgetsubadminuser()
        {
            StatusCode = -1000;//for error
            string query;
            DataSet objDataSet;
            query = @"select MOBILE_USER from dbo.TBL_SUB_ADMIN where COMPANY_ID=@CompanyId AND MOBILE_USER = @Mobileuser";
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            cmd.Parameters.AddWithValue("@Mobileuser", this.Mobileuser);
            objDataSet = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (objDataSet != null)
            {
                this.ResultTable = objDataSet.Tables[0];
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            else
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }



        }

        public void ProcessIstester()
        {

            StatusCode = -1000;//for error
            string query;
            DataSet objDataSet;
            query = @"select FIRST_NAME  +SPACE(1) + LAST_NAME  + SPACE(1) +'(' + SPACE(1)+ USER_NAME  + SPACE(1)    +')'   AS username ,USER_ID from dbo.TBL_USER_DETAIL WHERE COMPANY_ID=@CompanyId and IS_TESTER='1'";
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            objDataSet = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (objDataSet != null)
            {
                this.ResultTable = objDataSet.Tables[0];
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            else
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }


        }

        public void ProcesstogetAutoapprovedUserlist()
        {
           
            StatusCode = -1000;//for error
            string query;
            DataSet objDataSet;
            query = @"SELECT U.USER_IDS,U.GROUP_IDS,t.FIRST_NAME,U.COMPANY_ID ,t.LAST_NAME,t.USER_NAME FROM (SELECT 
                   Split.a.value('.', 'VARCHAR(100)') AS USER_IDS,GROUP_IDS,COMPANY_ID
                  FROM  (SELECT [USER_IDS],  
                   CAST ('<M>' + REPLACE(USER_IDS, ',', '</M><M>') + '</M>' AS XML) AS String,COMPANY_ID,GROUP_IDS 
                 FROM  TBL_USER_DEVICE_AUTOAPPROVE_SETTING) AS A CROSS APPLY String.nodes ('/M') AS Split(a))U
                  INNER JOIN TBL_USER_DETAIL as t
                      on t.USER_ID=U.USER_IDS WHERE U.COMPANY_ID=@CompanyId;";
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            objDataSet = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (objDataSet != null)
            {
                this.ResultTable = objDataSet.Tables[0];
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            else
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }


       
        }

        /// <summary>
        /// Sets the result table for final datatable of users.
        /// Sets the MaxUsers and User Count Properties
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="subadminId"></param>
        /// <param name="orderBy"></param>
        public void getUserDetailsForSort(string companyId, string subadminId, ORDER_BY orderBy, SORT_TYPE sortType, string filter)
        {
            this.IsSelectAll = true;
            string strQuery = getQueryForSorting(orderBy, sortType, filter);

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            //cmd.Parameters.AddWithValue("@SubAdminId", subadminId);
            cmd.Parameters.AddWithValue("@CompanyId", companyId);
            //cmd.Parameters.AddWithValue("@SortBy", strSortBy);

            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (objDataSet != null)
            {
                this.ResultTable = objDataSet.Tables[0];
                if (ResultTable != null)
                {
                    this.TotalUsersCount = ResultTable.Rows.Count;
                    if (TotalUsersCount > 0)
                    {
                        if (IsSelectAll)
                        {
                            this.UserCount = Convert.ToInt32(ResultTable.Rows[0]["USER_COUNT"]);
                        }
                        this.MaxUsers = Convert.ToInt32(ResultTable.Rows[0]["MAX_USER"]);

                    }
                }
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            else
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        string getQueryForSorting(ORDER_BY orderBy, SORT_TYPE sortType, string filter)
        {
            string strSortBy = "", strSortType = "";
            switch (orderBy)
            {
                case ORDER_BY.Name:
                default:
                    strSortBy = "FIRST_NAME";
                    break;
                case ORDER_BY.Email:
                    strSortBy = "EMAIL_ID";
                    break;
                case ORDER_BY.Logon:
                    strSortBy = "ulon";
                    break;
            }
            switch (sortType)
            {
                case SORT_TYPE.ASC:
                    strSortType = "ASC";
                    break;
                case SORT_TYPE.DESC:
                    strSortType = "DESC";
                    break;
            }
            string strQuery = query;

            if (!String.IsNullOrEmpty(filter))
            {
                strQuery = strQuery + " " + @"AND (Usr.USER_NAME LIKE '%" + filter + "%' OR Usr.EMAIL_ID LIKE '%" + filter + "%' OR Usr.FIRST_NAME LIKE '%" + filter + "%')";
            }
            strQuery = strQuery + " " + @"ORDER BY" + " " + strSortBy + " " + strSortType + ";";
            return strQuery;

        }

        public string GetUserContactsForSendRequest(string RequestedBy)
        {
            string strQuery = @"select user_name from  tbl_user_detail a inner join tbl_User_group_link b on a.user_id=b.user_id where b.Group_id in ('" + RequestedBy.Replace(",", "','") + "')";
            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;

            DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
            string Result = string.Empty;
            if (objDataSet != null && objDataSet.Tables.Count > 0 && objDataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in objDataSet.Tables[0].Rows)
                {
                    if (!string.IsNullOrEmpty(Result)) Result += ",";
                    Result += dr[0].ToString();
                }
            }
            return Result;

        }
        public static DataTable getUserDetlByDomainId(string domainId,
            string companyId, out bool userExists)
        {
            string strQuery = @"SELECT * FROM TBL_USER_DETAIL
                                WHERE DOMAIN_ID = @DOMAIN_ID
                                AND COMPANY_ID = @COMPANY_ID";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@DOMAIN_ID", domainId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (ds == null || ds.Tables.Count == 0) throw new Exception("Internal server error");
            if (ds.Tables[0].Rows.Count > 0)
                userExists = true;
            else
                userExists = false;
            return ds.Tables[0];

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="domainIds"></param>
        /// <param name="companyId"></param>
        /// <param name="userExists"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">Thrown when Domain id list is null</exception>
        /// <exception cref="MficientException">Thrown when there is some internal error</exception>
        public static DataTable getUserDetlByDomainId(List<string> domainIds,
            string companyId,
            out bool userExists)
        {
            if (domainIds == null) throw new ArgumentNullException();
            SqlCommand cmd = new SqlCommand();
            string[] parameters = new string[domainIds.Count];
            int iCountOfLoop = 0;
            foreach (string domainId in domainIds)
            {
                parameters[iCountOfLoop] = "@p" + iCountOfLoop;
                cmd.Parameters.AddWithValue(parameters[iCountOfLoop], domainId);
                iCountOfLoop++;
            }
            cmd.Parameters.AddWithValue("@CompanyId", companyId);
            string strQuery = @"SELECT * FROM TBL_USER_DETAIL
                                WHERE DOMAIN_ID IN (";

            strQuery += string.Join(",", parameters) + ")";
            strQuery += "AND COMPANY_ID = @CompanyId";
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            DataSet dsUserList = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (dsUserList == null || dsUserList.Tables.Count == 0) throw new Exception("Internal server error");
            if (dsUserList.Tables[0].Rows.Count > 0)
                userExists = true;
            else
                userExists = false;
            return dsUserList.Tables[0];

        }
        #region Get User Detail By Company and SubAdmin Id
        public List<MFEMobileUser> getUserDetail(string companyId, string subAdminId)
        {
            List<MFEMobileUser> lstUserDetails = new List<MFEMobileUser>();
            try
            {
                SqlCommand cmd = getSqlCommandForUserDetByCmpAndSubAdminId(companyId, subAdminId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (objDataSet != null)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    if (ResultTable != null)
                    {
                        if (ResultTable.Rows.Count > 0)
                        {
                            foreach (DataRow row in ResultTable.Rows)
                            {
                                lstUserDetails.Add(this.getUserFromDataRow(row));
                            }
                        }
                    }
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            return lstUserDetails;
        }
        string getQueryForUserDetByCmpAndSubAdminId()
        {
            return @"SELECT * FROM TBL_USER_DETAIL WHERE SUBADMIN_ID = @SUBADMIN_ID AND COMPANY_ID =@COMPANY_ID";
        }
        SqlCommand getSqlCommandForUserDetByCmpAndSubAdminId(
            string companyId,string subAdminId
            )
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            string strQuery = getQueryForUserDetByCmpAndSubAdminId();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
            cmd.Parameters.AddWithValue("@SUBADMIN_ID", subAdminId);
            return cmd;
        }
        #endregion
        #region Get User Detail By CompanyId
        public List<MFEMobileUser> getUserDetail(string companyId)
        {
            List<MFEMobileUser> lstUserDetails = new List<MFEMobileUser>();
            try
            {
                SqlCommand cmd = getSqlCommandForUserDetByCmp(companyId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (objDataSet != null)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    if (ResultTable != null)
                    {
                        if (ResultTable.Rows.Count > 0)
                        {
                            foreach (DataRow row in ResultTable.Rows)
                            {
                                lstUserDetails.Add(this.getUserFromDataRow(row));
                            }
                        }
                    }
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            return lstUserDetails;
        }
        string getQueryForUserDetByCmp()
        {
            return @"SELECT * FROM TBL_USER_DETAIL WHERE COMPANY_ID =@COMPANY_ID";
        }
        SqlCommand getSqlCommandForUserDetByCmp(string companyId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            string strQuery = getQueryForUserDetByCmp();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
            return cmd;
        }
        #endregion
        #region Get User List From UserIds
        public List<MFEMobileUser> getUserDtls(List<string> userIds, string companyId,out DataTable dtDevice)
        {
            dtDevice = new DataTable();
            List<MFEMobileUser> userDetails = new List<MFEMobileUser>();
            try
            {

                string strCommaSeparatedUserId = getCommaSeparatedUserIdFromUserIdList(userIds);
                SqlCommand cmd =
                    getSqlCommandForUserDtlOfManyUserById(userIds, companyId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (objDataSet != null)
                {
                    dtDevice = objDataSet.Tables[1];
                    this.ResultTable = objDataSet.Tables[0];
                    if (ResultTable != null)
                    {
                        if (ResultTable.Rows.Count > 0)
                        {
                            foreach (DataRow row in ResultTable.Rows)
                            {
                                userDetails.Add(this.getUserFromDataRow(row));
                            }
                        }
                    }
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            return userDetails;
        }
        SqlCommand getSqlCommandForUserDtlOfManyUserById(List<string> userIds, string companyId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            string[] parameters = new string[userIds.Count];
            int iLoopCount = 0;
            foreach (string userId in userIds)
            {
                parameters[iLoopCount] = "@p" + iLoopCount;
                cmd.Parameters.AddWithValue(parameters[iLoopCount], userId);
                iLoopCount++;
            }
            string users = string.Join(",", parameters);
             
            string strQuery = @"SELECT * FROM TBL_USER_DETAIL
                                WHERE USER_ID IN (" + users + @") AND COMPANY_ID = @COMPANY_ID; 
                            select * from tbl_registered_device as r inner join tbl_user_detail as u on r.user_id=u.user_id 
                               and r.company_id=u.Company_id  where r.USER_ID in ("+users+ ") and r.COMPANY_ID=@COMPANY_ID;";
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
            return cmd;
        }
        string getCommaSeparatedUserIdFromUserIdList(List<string> userIds)
        {
            if (userIds == null) throw new ArgumentNullException();
            string strCommaSeparatedUserId = String.Empty;
            foreach (string strUserId in userIds)
            {
                if (String.IsNullOrEmpty(strCommaSeparatedUserId))
                {
                    strCommaSeparatedUserId += strUserId;
                }
                else
                {
                    strCommaSeparatedUserId += "," + strUserId;
                }
            }
            return strCommaSeparatedUserId;
        }

        MFEMobileUser getUserFromDataRow(DataRow row)
        {
            if (row == null) throw new ArgumentNullException();
            MFEMobileUser objUser = new MFEMobileUser();
            objUser.UserId = Convert.ToString(row["USER_ID"]);
            objUser.Username = Convert.ToString(row["USER_NAME"]);
            objUser.EmailId = Convert.ToString(row["EMAIL_ID"]);
            objUser.FirstName = Convert.ToString(row["FIRST_NAME"]);
            objUser.LastName = Convert.ToString(row["LAST_NAME"]);
            return objUser;
        }
        #endregion

        #region Get User List Using First Name Or Last Name
        /// <summary>
        /// Get User Details by Name
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<MFEMobileUser> getUserDtlsByName(string searchTerm, string companyId)
        {
            List<MFEMobileUser> lstUserDetails = new List<MFEMobileUser>();
            try
            {
                SqlCommand cmd = getSqlCommandForUserDtlByName(searchTerm, companyId);
                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (objDataSet != null)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    if (ResultTable != null)
                    {
                        if (ResultTable.Rows.Count > 0)
                        {
                            foreach (DataRow row in ResultTable.Rows)
                            {
                                lstUserDetails.Add(this.getUserFromDataRow(row));
                            }
                        }
                    }
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            return lstUserDetails;
        }
        SqlCommand getSqlCommandForUserDtlByName(string searchTerm, string companyId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            string strQuery = @"SELECT *
                                FROM TBL_USER_DETAIL
                                WHERE ( FIRST_NAME LIKE '"+searchTerm+"%' OR LAST_NAME LIKE '"+searchTerm+"%' )";
            strQuery += " AND COMPANY_ID = @COMPANY_ID";
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
           cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
            return cmd;
        }

        #endregion

        public string CompanyId
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string UserId
        {
            set;
            get;
        }
        public string UserName
        {
            set;
            get;
        }
        public string LocationId
        {
            set;
            get;
        }
        public string DepartmentId
        {
            set;
            get;
        }
        public Boolean IsFindByUserId
        {
            set;
            get;
        }
        public Boolean IsFindByLocationIdandDepartmentId
        {
            set;
            get;
        }
        public Boolean IsSelectAll
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public Boolean IsFindByCompanyId
        {
            set;
            get;
        }
        public string Filter
        {
            set;
            get;
        }
        public int TotalUsersCount
        {
            get;
            set;
        }
        /// <summary>
        /// Available Count only in case of select all.
        /// </summary>
        public int UserCount
        {
            get;
            set;
        }
        public int MaxUsers
        {
            get;
            set;
        }
        public string EmailId
        {
            get;
            set;
        }
        public string Mobileuser
        {
            get;
            set;
        }
    }



    //public class EntityCollection : IEnumerable<Entity>
    //{ 

    //}
}
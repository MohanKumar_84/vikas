﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetWorkFlowDetails
    {

        public GetWorkFlowDetails(string _SubAdminId,string _CompanyId)
        {
            this.SubAdminId = _SubAdminId;
            this.CompanyId = _CompanyId;
        }

        public void Process()
        {
            try
            {
                StatusCode = -1000;//for error
                string query = @"SELECT DISTINCT(WF.WF_ID) As C_WF_ID,WF.WF_ID,WF.WF_NAME,wfad.WF_ICON,'NONE' AS CATEGORY_ID,'NONE' AS CATEGORY
                                FROM TBL_WORKFLOW_DETAIL AS WF LEFT OUTER JOIN TBL_WORKFLOW_ADDITIONAL_DETAIL wfad
                                ON wfad.WORKFLOW_ID = wf.WF_ID
                                WHERE WF.SUBADMIN_ID =@SubAdminId AND WF.COMPANY_ID =@COMPANY_ID AND WF.WF_ID=WF.PARENT_ID ORDER BY WF.WF_NAME;";
//@"SELECT DISTINCT(WF.WF_ID) As C_WF_ID,WF.WF_ID,WF.WF_NAME,WFCat.WF_ICON,
//                                (CASE WHEN WFCat.CATEGORY_ID IS NULL THEN 'NONE' ELSE WFCat.CATEGORY_ID END)AS CATEGORY_ID,
//                                (CASE WHEN MenuCat.CATEGORY IS NULL THEN 'NONE' ELSE MenuCat.CATEGORY END)AS CATEGORY
//                                FROM TBL_WORKFLOW_DETAIL AS WF
//                                LEFT OUTER JOIN TBL_WORKFLOW_AND_CATEGORY_LINK AS WFCat
//                                ON WF.WF_ID = WFCat.WORKFLOW_ID
//                                LEFT OUTER JOIN TBL_MENU_CATEGORY AS MenuCat
//                                ON WFCat.CATEGORY_ID = MenuCat.MENU_CATEGORY_ID
//                                WHERE WF.SUBADMIN_ID =@SubAdminId AND WF.COMPANY_ID =@COMPANY_ID AND WF.WF_ID=WF.PARENT_ID ORDER BY WF.WF_NAME;";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@SubAdminId", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                //objSqlCommand.Parameters.AddWithValue("@MODEL_TYPE", MficientConstants.IDE_MODEL_TYPE_PHONE);

                DataSet objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.ResultTable = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    StatusCode = -1000;
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }

            }
            catch
            {
                StatusCode = -1000;
            }
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public string CompanyId { get; set; }
    }
}
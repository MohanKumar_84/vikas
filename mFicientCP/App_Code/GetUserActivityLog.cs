﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCP
{
    public class GetUserActivityLog
    {
        public GetUserActivityLog()
        {

        }
        public void GetAdminLogs()
        {
            try
            {
                string strQuery = @"SELECT * FROM TBL_ACTIVITY_LOG WHERE COMPANY_ID = @COMPANY_ID and USER_ID = @USER_ID AND ACTIVITY_DATETIME > @FROM_DATE AND ACTIVITY_DATETIME < @TO_DATE;";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
                cmd.Parameters.AddWithValue("@USER_ID", UserId);
                cmd.Parameters.AddWithValue("@FROM_DATE", FromDate);
                cmd.Parameters.AddWithValue("@TO_DATE", ToDate);

                cmd.CommandType = CommandType.Text;
                DataSet dsActivityLogDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsActivityLogDtls != null)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    AdminActivityLogs = dsActivityLogDtls.Tables[0];
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        public void GetSubAdminLogs()
        {
            try
            {
                string strQuery = @"SELECT sub.SUBADMIN_ID, sub.ADMIN_ID, sub.USER_NAME, activityLog.* FROM TBL_SUB_ADMIN AS sub INNER JOIN TBL_ACTIVITY_LOG AS activityLog ON sub.SUBADMIN_ID = activityLog.USER_ID
                                    WHERE sub.ADMIN_ID = @ADMIN_ID and activityLog.COMPANY_ID = @COMPANY_ID AND activityLog.ACTIVITY_DATETIME > @FROM_DATE AND activityLog.ACTIVITY_DATETIME < @TO_DATE;";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
                cmd.Parameters.AddWithValue("@ADMIN_ID", AdminId);
                cmd.Parameters.AddWithValue("@FROM_DATE", FromDate);
                cmd.Parameters.AddWithValue("@TO_DATE", ToDate);

                cmd.CommandType = CommandType.Text;
                DataSet dsActivityLogDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsActivityLogDtls != null)
                {

                    SubAdminActivityLogs = dsActivityLogDtls.Tables[0];
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }

            }
        }

        public DataTable AdminActivityLogs
        {
            set;
            get;
        }
        public DataTable SubAdminActivityLogs
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string CompanyId
        {
            get;
            set;
        }
        public string UserId
        {
            get;
            set;
        }
        public string AdminId
        {
            get;
            set;
        }
        public long FromDate
        {
            get;
            set;
        }
        public long ToDate
        {
            get;
            set;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientCP
{
    public class UserGroupDetails
    {

        public UserGroupDetails(string companyId)
        {
            this.ComapanyId = companyId;
        }
        public void Process()
        {
            try
            {
                this.StatusCode = -1000;
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand;
                query = @"select DISTINCT u.Group_ID, u.GROUP_NAME,w.WF_Name,w.WF_ID,w.COMPANY_ID
                            from dbo.TBL_USER_GROUP as u
                             left outer join dbo.TBL_WORKFLOW_AND_GROUP_LINK as w1 
                            on u.GROUP_ID=w1.GROUP_ID   and  u.COMPANY_ID=w1.COMPANY_ID
                            left outer join dbo.TBL_WORKFLOW_DETAIL as w
                            on w1.WORKFLOW_ID=w.WF_ID and  u.COMPANY_ID=w.COMPANY_ID 
                             where  u.COMPANY_ID=@COMPANY_ID order by WF_Name asc;
              SELECT   DISTINCT GROUP_ID,COMPANY_ID FROM TBL_USER_GROUP WHERE COMPANY_ID = @COMPANY_ID;";
                objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@Company_ID", this.ComapanyId);

                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                
                if (objDataSet.Tables[0] != null && objDataSet.Tables[1] != null)
                {
                    this.StatusCode = 0;
                    this.ResultTable = objDataSet.Tables[0];
                    this.AllGroupsInCompany = objDataSet.Tables[1];
                }
                else
                {
                    this.StatusCode = -1000;
                }
            }
            catch
            {
                this.StatusCode = -1000;
            }

        }
        public DataTable ResultTable
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string ComapanyId { get; set; }
        public DataTable AllGroupsInCompany { get; set; }
    }
}
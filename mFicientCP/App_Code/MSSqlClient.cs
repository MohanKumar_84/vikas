﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace mFicientCP
{
    public class MSSqlClient
    {
        public enum CONNECTION_STRING_FOR_DB
        {
            MFICIENT,
            MGRAM,
            ADMINDB
        }
        /// <summary>
        /// Description :Test connction ( Sql Query And Parameter )
        /// </summary>
        public static bool TestMsSQLConnection(string connectionString)
        {
            SqlConnection Conn = null;
            bool isConnected = false;
            try
            {
                Conn = new SqlConnection(connectionString);
            }
            catch
            {
                throw new Exception(@"Invalid Connection");
            }
            try
            {
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    if (Conn != null)
                    {
                        Conn.Dispose();
                        Conn = null;
                    }
                }
                else
                {
                    isConnected = true;
                    Conn.Close();
                }
            }
            catch
            {
                Conn = null;
            }
            return isConnected;
        }
        /// <summary>
        /// Description :Select Data According To SqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static DataSet SelectDataFromSQlCommand(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            try
            {
                SqlConnectionOpen(out objSqlConnection);
                _SqlCommand.Connection = objSqlConnection;

                DataSet ObjDataSet = new DataSet();

                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

                objSqlDataAdapter.Fill(ObjDataSet);

                //SqlConnectionClose(objSqlConnection);

                return ObjDataSet;
            }
            catch
            {
                return null;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }
        }
        public static DataSet SelectDataFromSQlCommand(string connectionString, SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            try
            {
                SqlConnectionOpen(out objSqlConnection, connectionString);
                _SqlCommand.Connection = objSqlConnection;
                DataSet ObjDataSet = new DataSet();
                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);
                objSqlDataAdapter.Fill(ObjDataSet);
                return ObjDataSet;
            }
            catch
            {
                return null;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }
        }


        /// <summary>
        /// Description :SqlConnection Open Function
        /// </summary>
        public static void SqlConnectionOpen(out SqlConnection Conn)
        {
            try
            {
                Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }            
            catch 
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }
        public static void SqlConnectionOpen(out SqlConnection Conn, string ConnectionString)
        {
            try
            {
                Conn = new SqlConnection(ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }
        /// <summary>
        /// Description :SqlConnection Close Function
        /// </summary>
        public static void SqlConnectionClose(SqlConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open))
                {
                    Conn.Close();
                    Conn.Dispose();
                    Conn = null;
                }
            }
            catch
            {
                Conn.Dispose();
                Conn = null;
            }
        }

        /// <summary>
        /// Description :SqlConnection Open Function
        /// </summary>
        public static void SqlConnectionOpenAdmin(out SqlConnection Conn)
        {
            try
            {
                Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }
        /// <summary>
        /// Description :Select Data According To SqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static DataSet SelectDataFromSQlCommandAdminData(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            try
            {
                SqlConnectionOpenAdmin(out objSqlConnection);
                _SqlCommand.Connection = objSqlConnection;

                DataSet ObjDataSet = new DataSet();

                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

                objSqlDataAdapter.Fill(ObjDataSet);

                SqlConnectionClose(objSqlConnection);

                return ObjDataSet;

            }
            catch
            {
                return null;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }
        }

        /// <summary>
        /// Description :Data Process (Insert, Update, Delete ) According To SqlCommand ( Sql Query And Parameter ) Admin Database
        /// </summary>
        public static int ExecuteNonQueryRecordAdmin(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            SqlConnectionOpenAdmin(out objSqlConnection);
            _SqlCommand.Connection = objSqlConnection;

            int i = _SqlCommand.ExecuteNonQuery();

            SqlConnectionClose(objSqlConnection);

            return i;
        }

        /// <summary>
        /// Description :Data Process (Insert, Update, Delete ) According To SqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static int ExecuteNonQueryRecord(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            SqlConnectionOpen(out objSqlConnection);
            _SqlCommand.Connection = objSqlConnection;

            int i = _SqlCommand.ExecuteNonQuery();

            SqlConnectionClose(objSqlConnection);

            return i;
        }
        public static int ExecuteNonQueryRecord(SqlCommand _SqlCommand, string _connectionString)
        {
            SqlConnection objSqlConnection = null;
            SqlConnectionOpen(out objSqlConnection, _connectionString);
            _SqlCommand.Connection = objSqlConnection;
            int i = _SqlCommand.ExecuteNonQuery();

            SqlConnectionClose(objSqlConnection);

            return i;
        }
        public static DataSet SelectDataFromDifferentConnection(string ConnectionString, SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            try
            {
                try
                {
                    SqlConnectionOpen(out objSqlConnection, ConnectionString);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                _SqlCommand.Connection = objSqlConnection;

                DataSet ObjDataSet = new DataSet();

                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

                objSqlDataAdapter.Fill(ObjDataSet);

                //SqlConnectionClose(objSqlConnection);

                return ObjDataSet;
            }
            catch
            {
                return null;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="metadataType"></param>
        /// <param name="connectionString"></param>
        /// <param name="tableName">If MetadataType is for a table</param>
        /// <returns>Dataset</returns>
        /// <exception cref="System.Exception">Thrown when there is some internal error</exception>
        public static DataSet getMetaDataByType(MetadataType metadataType
            , string connectionString,
            string tableName,
           out string errorMsg)
        {
            DataSet dsMetaData = new DataSet();
            errorMsg = String.Empty;
            try
            {
                string strQuery = string.Empty;
                SqlCommand cmd = new SqlCommand();
                strQuery = getMetaDataQuery(metadataType);
                cmd.CommandText = strQuery;
                cmd.CommandType = CommandType.Text;
                //Add Parameters
                switch (metadataType)
                {
                    case MetadataType.TABLES:
                        break;
                    case MetadataType.COLUMNS:
                        cmd.Parameters.AddWithValue("@TABLE_NAME", tableName);
                        break;
                    case MetadataType.ALL:
                        break;
                }
                dsMetaData = SelectDataFromDifferentConnection(connectionString, cmd);
                return dsMetaData;
            }
            catch (Exception ex)
            {
                dsMetaData = null;
                if (ex.Message.ToLower() == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString().ToLower())
                {
                    errorMsg = "There was some error in connection.";
                }
                else
                {
                    errorMsg = "Internal server error.";
                }
                return dsMetaData;
            }
        }
        public static string getMetaDataQuery(MetadataType metaDataType)
        {
            string strQuery = String.Empty;
            switch (metaDataType)
            {
                case MetadataType.TABLES:
                    strQuery = @"SELECT TABLE_SCHEMA,TABLE_NAME, OBJECTPROPERTY(object_id(TABLE_NAME), N'IsUserTable') AS type FROM INFORMATION_SCHEMA.TABLES;";
                    break;

                case MetadataType.COLUMNS:
                    strQuery = @" SELECT OBJECT_NAME(c.OBJECT_ID) table_name, c.name column_name,t.name column_type,c.is_nullable FROM sys.columns AS c JOIN sys.types AS t ON c.user_type_id=t.user_type_id
                                      JOIN sysobjects ON c.OBJECT_ID = sysobjects.id WHERE sysobjects.xtype='U' AND OBJECT_NAME(c.OBJECT_ID)= @TABLE_NAME ORDER BY c.OBJECT_ID;";
                    break;

                case MetadataType.ALL:
                    strQuery = @" SELECT TABLE_SCHEMA,TABLE_NAME, OBJECTPROPERTY(object_id(TABLE_NAME), N'IsUserTable') AS type FROM INFORMATION_SCHEMA.TABLES;
                                      SELECT OBJECT_NAME(c.OBJECT_ID) table_name, c.name column_name,t.name column_type,c.is_nullable FROM sys.columns AS c JOIN sys.types AS t ON c.user_type_id=t.user_type_id
                                      JOIN sysobjects ON c.OBJECT_ID = sysobjects.id WHERE sysobjects.xtype='U' ORDER BY c.OBJECT_ID;";
                    break;
            }
            return strQuery;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostName"></param>
        /// <param name="databaseName"></param>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <param name="timeout">For default timeout pass 0 or value less than 0</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public static string getConnectionString(string hostName,
            string databaseName,
            string userId,
            string password,
            int timeout)
        {
            if (String.IsNullOrEmpty(hostName) ||
               String.IsNullOrEmpty(databaseName) ||
                String.IsNullOrEmpty(userId) ||
                String.IsNullOrEmpty(password)) throw new ArgumentNullException();
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = hostName;
            connectionStringBuilder.InitialCatalog = databaseName;
            connectionStringBuilder.UserID = userId;
            connectionStringBuilder.Password = password;
            if (timeout > 0)
                connectionStringBuilder.ConnectTimeout = timeout;

            return connectionStringBuilder.ConnectionString;
        }
        public static string getConnectionStringFromWebConfig(CONNECTION_STRING_FOR_DB db)
        {
            string strConnectionString = String.Empty;
            switch (db)
            {
                case CONNECTION_STRING_FOR_DB.MFICIENT:
                    strConnectionString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
                    break;
                case CONNECTION_STRING_FOR_DB.MGRAM:
                    strConnectionString = ConfigurationManager.ConnectionStrings["ConStrmGramDB"].ConnectionString;
                    break;
                case CONNECTION_STRING_FOR_DB.ADMINDB:
                    strConnectionString = ConfigurationManager.ConnectionStrings["ServerDtlsConString"].ConnectionString;
                    break;

            }
            return strConnectionString;
        }
    }
}
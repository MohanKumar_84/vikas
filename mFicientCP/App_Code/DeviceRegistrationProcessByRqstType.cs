﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
namespace mFicientCP
{
    public class DeviceRegistrationProcessByRqstType
    {
        public enum REGISTRATION_DEVICE_REQUEST_TYPE
        {
            ALLOW_USER_DEVICE = 1,
            DELETE_ALL_EXISTING_DEVICES_AND_ALLOW_NEW = 2,
            DELETE_DEVICES_OF_OTHER_USER_AND_ALLOW_NEW = 3,
            DENY = 4,
            DELETE_REGISTERED_USER_DEVICE = 5,
        }

        public DeviceRegistrationProcessByRqstType(string companyId, string subAdminId, string userId, string adminId,
            int requestType, string deviceType, string deviceId, string deviceModel, string devPushMessageId, string osVersion,
            DEVICE_OR_ICON_SIZE deviceSize, HttpContext context, string appVersion)
        {
            SubAdminId = subAdminId;
            CompanyId = companyId;
            UserId = userId;
            RequestType = requestType;
            DeviceType = deviceType;
            DeviceId = deviceId;
            DeviceModel = deviceModel;
            DeviceSize = deviceSize;
            AppVersion = string.IsNullOrEmpty(appVersion) ? "" : appVersion;
            this.OSVersion = osVersion;
            this.DevPushMsgId = devPushMessageId;
            this.AdminId = adminId;
            this.AppContext = context;
        }

        /// <summary>
        /// if request type is for deleting some existing device and approving new(Delete and approve)
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="subAdminId"></param>
        /// <param name="userId"></param>
        /// <param name="requestType"></param>
        /// <param name="deviceType"></param>
        /// <param name="deviceId"></param>
        /// <param name="deviceModel"></param>
        /// <param name="imei"></param>
        /// <param name="devicesToDelete"></param>
        public DeviceRegistrationProcessByRqstType(string companyId, string subAdminId, string userId, string adminId,
             int requestType, string deviceType, string deviceId, string deviceModel, string devPushMessageId, string osVersion
            , DEVICE_OR_ICON_SIZE deviceSize, List<MFEDevice> devicesToDelete, HttpContext context, string appVersion)
        {
            SubAdminId = subAdminId;
            CompanyId = companyId;
            UserId = userId;
            RequestType = requestType;
            DeviceType = deviceType;
            DeviceId = deviceId;
            DeviceModel = deviceModel;
            DeviceSize = deviceSize;
            this.DevicesToDelete = devicesToDelete;
            this.DevPushMsgId = devPushMessageId;
            this.OSVersion = osVersion;
            this.AdminId = adminId;
            this.AppContext = context;
            AppVersion = string.IsNullOrEmpty(appVersion) ? "" : appVersion;
        }
        public void Process()
        {


            switch (RequestType)
            {
                case (int)REGISTRATION_DEVICE_REQUEST_TYPE.ALLOW_USER_DEVICE:
                    processAllowUserDevice();
                    break;
                case (int)REGISTRATION_DEVICE_REQUEST_TYPE.DELETE_ALL_EXISTING_DEVICES_AND_ALLOW_NEW:
                    processDeleteAllExistingDevicesByIdAndAllowNew();
                    break;
                case (int)REGISTRATION_DEVICE_REQUEST_TYPE.DELETE_DEVICES_OF_OTHER_USER_AND_ALLOW_NEW:
                    processDeleteDevicesOfOtherUserAndAllowNew();
                    break;
                case (int)REGISTRATION_DEVICE_REQUEST_TYPE.DENY:
                    processDenyUserRequest();
                    break;
                case (int)REGISTRATION_DEVICE_REQUEST_TYPE.DELETE_REGISTERED_USER_DEVICE:
                    processDeleteRegisteredUserDevice();
                    break;
            }

        }

        void processAllowUserDevice()
        {
            string strDepartmentId = String.Empty, strUserName = String.Empty;
            
            SqlTransaction sqlTransaction = null;
            SqlConnection con = null;

            //for internal error 
            StatusCode = -1000;//set status code to 1 if device already exists for other user


            GetUserCompleteDetails objUserDetail = new GetUserCompleteDetails(this.UserId, this.CompanyId);
            objUserDetail.Process();
            if (objUserDetail.StatusCode != 0)
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
                return;
            }
            DataTable dtblUserDetail = objUserDetail.UserDetails;
            strUserName = objUserDetail.UserName;
            
            GetRegisteredDeviceDetailsByUserId objRegisteredDevOfUser = new GetRegisteredDeviceDetailsByUserId(this.CompanyId,
                                                                        this.SubAdminId, this.UserId);
            objRegisteredDevOfUser.Process();
            if (objRegisteredDevOfUser.StatusCode != 0)
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
                return;
            }
            GetSubAdminDetail objSubAdminDtl = new
            GetSubAdminDetail(this.AdminId, this.CompanyId);
            objSubAdminDtl.getSubAdminById(this.SubAdminId);
            if (objSubAdminDtl.StatusCode != 0)
            {

                StatusCode = -1000;
                StatusDescription = "Internal server error.";
                return;
            }

            try
            {
                long lngDateTimeOfProcess = DateTime.UtcNow.Ticks;
                string strFullDate = Utilities.getFullCompanyLocalFormattedDate(
                    CompanyTimezone.getTimezoneInfo(this.CompanyId), lngDateTimeOfProcess);
                MSSqlClient.SqlConnectionOpen(out con);
                //MBuzzServerConfigDtls objMBuzzSetting = new MBuzzServerConfigDtls();
                //try
                //{
                //    objMBuzzSetting = ServerMappingManager.getMbuzzConfigDtls(this.CompanyId);
                //}
                //catch { }
                using (con)
                {
                    using (sqlTransaction = con.BeginTransaction())
                    {
                        if (!isDeviceAlreadyRegisteredForOtherUser())
                        {
                            removeFromRequest(sqlTransaction, con);
                            allowDeviceToUser(sqlTransaction, con, lngDateTimeOfProcess);
                            processSaveUpdationReqInfo(sqlTransaction, con, lngDateTimeOfProcess);
                            if (!String.IsNullOrEmpty(this.DevPushMsgId))
                            {
                                SaveDevcDtlsInMGramInsertUpdate objSaveDevcInMGram = new SaveDevcDtlsInMGramInsertUpdate(this.CompanyId,
                                    this.DeviceId, this.DeviceType, strUserName, this.DevPushMsgId);
                                objSaveDevcInMGram.Process();
                                if (objSaveDevcInMGram.StatusCode != 0) throw new Exception();

                                try
                                {
                                    SavePushMsgOutboxInMgram objSavePshMsgOutbox = new SavePushMsgOutboxInMgram(this.CompanyId, strUserName,this.DeviceId, this.DevPushMsgId,this.DeviceType,SavePushMsgOutboxInMgram.NOTIFICATION_TYPE.MF_DEVICE_APPRV, "");
                                    objSavePshMsgOutbox.Process();
                                }
                                catch
                                { }
                            }

                            //if (objRegisteredDevOfUser.ResultTables.Tables[0].Rows.Count == 0 && !Convert.ToBoolean(dtblUserDetail.Rows[0]["DESKTOP_MESSENGER"]))
                            //{
                            //    try
                            //    {
                            //        UserData objUserData = new UserData();
                            //        objUserData.unm = strUserName;
                            //        objUserData.fnm = Convert.ToString(dtblUserDetail.Rows[0]["FIRST_NAME"]) + " " +Convert.ToString(dtblUserDetail.Rows[0]["LAST_NAME"]);
                            //        objUserData.dep = "";
                            //        objUserData.des = "";
                            //        objUserData.em = Convert.ToString(dtblUserDetail.Rows[0]["EMAIL_ID"]);
                            //        objUserData.status = "1";
                            //        GetUserDetail objGetUserDetail = new GetUserDetail();
                            //        AddNewContactRequest addNewContact = new AddNewContactRequest(Utilities.GetMsgRefID(), objUserData,this.CompanyId,objGetUserDetail.GetUserContactsForSendRequest(Convert.ToString(dtblUserDetail.Rows[0]["requested_by"])));
                            //        mBuzzClient objClient = new mBuzzClient(addNewContact.RequestJson, objMBuzzSetting.ServerIP, objMBuzzSetting.PortNumber);
                            //        ThreadPool.QueueUserWorkItem(new WaitCallback(objClient.Connect), null);
                            //    }
                            //    catch
                            //    { }
                            //}

                            try
                            {
                                SaveEmailInfo.cpDeviceApproved( strUserName,
                                    Convert.ToString(objSubAdminDtl.ResultTable.Rows[0]["FULL_NAME"]),
                                    strFullDate, this.CompanyId,
                                    Convert.ToString(dtblUserDetail.Rows[0]["EMAIL_ID"]),
                                    this.DeviceId,
                                    this.DeviceType,
                                    this.DeviceType + " " + this.OSVersion + " / " + this.DeviceModel,
                                    this.AppContext);

                            }
                            catch
                            { }

                            sqlTransaction.Commit(); 

                            Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.DEVICE_APPROVED, this.CompanyId, this.SubAdminId, this.UserId, objUserDetail.UserName, objUserDetail.FirstName + " " + objUserDetail.LastName, this.DeviceId, this.DeviceType, DeviceModel, OSVersion, AppVersion);
                          
                            StatusCode = 0;
                        }
                        else
                        {
                            StatusCode = 1;
                            StatusDescription = "Device already registered for other user";
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (sqlTransaction != null)
                {
                    sqlTransaction.Dispose();
                }
            }
        }

        void processDeleteAllExistingDevicesByIdAndAllowNew()
        {
            if (DevicesToDelete == null)
            {
                StatusCode = -1000;
                StatusDescription = "No device selected to delete";
                return;
            }

            DataTable dtblUserDetail;

            GetUserDetail objUserDetail = new GetUserDetail(false, true, this.SubAdminId, this.UserId, "", this.CompanyId);
            objUserDetail.Process();
            if (objUserDetail.StatusCode != 0)
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
                return;
            }

            GetUserCompleteDetails objUserCompleteDetail = new GetUserCompleteDetails(this.UserId, this.CompanyId);
            objUserCompleteDetail.Process();
            if (objUserCompleteDetail.StatusCode != 0)
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
                return;
            }
            
            dtblUserDetail = objUserCompleteDetail.UserDetails;
            string strUserName = objUserCompleteDetail.UserName;

            GetRegisteredDeviceDetailsByUserId objRegisteredDevOfUser = new GetRegisteredDeviceDetailsByUserId(this.CompanyId,
                                                                        this.SubAdminId, this.UserId);
            objRegisteredDevOfUser.Process();
            if (objRegisteredDevOfUser.StatusCode != 0)
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
                return;
            }

            //MBuzzServerConfigDtls objMBuzzSetting = new MBuzzServerConfigDtls();
            //try
            //{
            //    objMBuzzSetting = ServerMappingManager.getMbuzzConfigDtls(this.CompanyId);
            //}
            //catch
            //{
            //    objMBuzzSetting.ServerIP = String.Empty;
            //    objMBuzzSetting.PortNumber = 0;
            //}
            SqlTransaction sqlTransaction = null;
            SqlConnection con = null;
            int iRowsAffected = 0;
            try
            {
                long lngDateTimeOfProcess = DateTime.UtcNow.Ticks;
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (sqlTransaction = con.BeginTransaction())
                    {
                        //delete registered devices
                        foreach (MFEDevice devicesDtls in DevicesToDelete)
                        {
                            logDeletedDeviceDataByDeviceIdAndType(sqlTransaction, con,devicesDtls.DeviceId, devicesDtls.DeviceType, devicesDtls.DeviceModel,
                                (DEVICE_OR_ICON_SIZE)Enum.Parse(typeof(DEVICE_OR_ICON_SIZE), (devicesDtls.DeviceSize).ToString()));

                            iRowsAffected = deleteRegisteredDeviceOfUser( sqlTransaction, con,devicesDtls.DeviceId, devicesDtls.DeviceType, devicesDtls.DeviceModel,
                            (DEVICE_OR_ICON_SIZE)Enum.Parse(typeof(DEVICE_OR_ICON_SIZE), (devicesDtls.DeviceSize).ToString()));

                            DeleteDevcDtlsFromMGram objDeleteDvcFromMGram =new DeleteDevcDtlsFromMGram(this.CompanyId,devicesDtls.DeviceId, devicesDtls.DeviceType, strUserName);
                            objDeleteDvcFromMGram.Process();

                            if (objDeleteDvcFromMGram.StatusCode != 0) throw new Exception();
                        }
                        iRowsAffected = allowDeviceToUser( sqlTransaction, con, lngDateTimeOfProcess);
                        if (iRowsAffected > 0)
                        {
                            removeFromRequest(sqlTransaction, con);
                            processSaveUpdationReqInfo(sqlTransaction,con,lngDateTimeOfProcess);

                            try
                            {
                                DeleteMobileSessionOfUser objDeleteMobileSess = new DeleteMobileSessionOfUser(this.UserId,this.CompanyId, this.SubAdminId, USER_LOG_OUT_TYPE.DEVICE_DELETED
                                , DeleteMobileSessionOfUser.MBUZZ_PENDING_MSGS_DELETE_TYPE.Device, true, con, sqlTransaction, this.DevicesToDelete);

                                objDeleteMobileSess.ProcessInTransaction(objUserDetail, objRegisteredDevOfUser, 1);
                            }
                            catch
                            { }

                            if (!String.IsNullOrEmpty(this.DevPushMsgId))
                            {
                                SaveDevcDtlsInMGramInsertUpdate objSaveDevcInMGram = new SaveDevcDtlsInMGramInsertUpdate(this.CompanyId,this.DeviceId, this.DeviceType, strUserName, this.DevPushMsgId);
                                objSaveDevcInMGram.Process();
                                if (objSaveDevcInMGram.StatusCode != 0) throw new Exception();
                            }

                            //Send email for deleted devices
                            foreach (MFEDevice deviceDtls in DevicesToDelete)
                            {
                                try
                                {
                                    SavePushMsgOutboxInMgram objSavePshMsgOutbox = new SavePushMsgOutboxInMgram(this.CompanyId, strUserName, deviceDtls.DeviceId, deviceDtls.DevicePushMsgId,deviceDtls.DeviceType,  SavePushMsgOutboxInMgram.NOTIFICATION_TYPE.MF_USER_BLOCK, "");
                                    objSavePshMsgOutbox.Process();
                                }
                                catch
                                { }
                            }
                            //send email for approved device
                            try
                            {
                                SavePushMsgOutboxInMgram objSavePshMsgOutbox = new SavePushMsgOutboxInMgram(this.CompanyId, strUserName, this.DeviceId, this.DevPushMsgId, this.DeviceType,SavePushMsgOutboxInMgram.NOTIFICATION_TYPE.MF_DEVICE_APPRV, "");

                                objSavePshMsgOutbox.Process();
                            }
                            catch
                            { }

                            sqlTransaction.Commit();

                            Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.DEVICE_APPROVED, this.CompanyId, this.SubAdminId, this.UserId,
                               objUserCompleteDetail.UserName, objUserCompleteDetail.FirstName + " " + objUserCompleteDetail.LastName, DeviceId, DeviceType, DeviceModel, OSVersion, AppVersion);
                            foreach (MFEDevice devicesDtls in DevicesToDelete)
                            {
                                Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.DEVICE_DELETE, this.CompanyId, this.SubAdminId, this.UserId,
                               objUserCompleteDetail.UserName, objUserCompleteDetail.FirstName + " " + objUserCompleteDetail.LastName, devicesDtls.DeviceId, devicesDtls.DeviceType, devicesDtls.DeviceModel, devicesDtls.OsVersion, "");
                            }
                            StatusCode = 0;
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (sqlTransaction != null)
                {
                    sqlTransaction.Dispose();
                }
            }

        }

        void processDeleteDevicesOfOtherUserAndAllowNew()
        {

        }

        void processDenyUserRequest()
        {
            GetUserCompleteDetails objUserDetail = new
                GetUserCompleteDetails(this.UserId, this.CompanyId);
            objUserDetail.Process();
            if (objUserDetail.StatusCode != 0)
            {

                StatusCode = -1000;
                StatusDescription = "Internal server error.";
                return;
            }
            GetSubAdminDetail objSubAdminDtl = new
            GetSubAdminDetail(this.AdminId, this.CompanyId);
            objSubAdminDtl.getSubAdminById(this.SubAdminId);
            if (objSubAdminDtl.StatusCode != 0)
            {

                StatusCode = -1000;
                StatusDescription = "Internal server error.";
                return;
            }
            SqlTransaction sqlTransaction = null;
            SqlConnection con = null;
            try
            {
                long lngDateTimeOfProcess = DateTime.UtcNow.Ticks;
                string strFullDate = Utilities.getFullCompanyLocalFormattedDate(
                    CompanyTimezone.getTimezoneInfo(this.CompanyId), lngDateTimeOfProcess);
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (sqlTransaction = con.BeginTransaction())
                    {
                        deleteDeniedRequest(sqlTransaction, con);
                        deleteAnyPreviousReqInDeniedTable(sqlTransaction, con);
                        saveDeniedRequestRecord(lngDateTimeOfProcess, sqlTransaction, con);
                        try
                        {
                            if (objUserDetail.StatusCode == 0)
                            {
                                SavePushMsgOutboxInMgram objSavePshMsgOutbox = new
                                    SavePushMsgOutboxInMgram(this.CompanyId,
                                    objUserDetail.UserName,
                                    this.DeviceId,
                                    this.DevPushMsgId,
                                    this.DeviceType,
                                    SavePushMsgOutboxInMgram.NOTIFICATION_TYPE.MF_DEVICE_REJECT, "");
                                objSavePshMsgOutbox.Process();
                            }
                        }
                        catch
                        { }
                        //send email for denied device
                        try
                        {
                            SaveEmailInfo.cpDeviceDenied(
                                objUserDetail.UserName,
                                Convert.ToString(objSubAdminDtl.ResultTable.Rows[0]["FULL_NAME"]),
                                strFullDate, this.CompanyId,
                                Convert.ToString(objUserDetail.UserDetails.Rows[0]["EMAIL_ID"]),
                                this.DeviceId,
                                this.DeviceType,
                                this.DeviceType + " " + this.OSVersion + " /" + this.DeviceModel,
                                this.AppContext);

                        }
                        catch
                        { }
                        sqlTransaction.Commit();
                        StatusCode = 0;
                        StatusDescription = "";
                        Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.DEVICE_DENY, this.CompanyId, this.SubAdminId, this.UserId,
                               objUserDetail.UserName, objUserDetail.FirstName + " " + objUserDetail.LastName, this.DeviceId, this.DeviceType, DeviceModel, OSVersion, AppVersion);
                    }
                }
            }
            catch (SqlException ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (sqlTransaction != null)
                {
                    sqlTransaction.Dispose();
                }
            }
        }

        void processDeleteRegisteredUserDevice()
        {
            string strUserName = String.Empty, strFullName;
            GetUserDetail objGetUserDetail = new GetUserDetail(false, true, this.SubAdminId, this.UserId, "", this.CompanyId);
            objGetUserDetail.Process();
            DataTable dtblUserDetail = objGetUserDetail.ResultTable;
            if (dtblUserDetail == null && dtblUserDetail.Rows.Count <= 0)
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
                return;
            }
            strUserName = Convert.ToString(dtblUserDetail.Rows[0]["USER_NAME"]);
            strFullName = Convert.ToString(dtblUserDetail.Rows[0]["FIRST_NAME"]) + " " + Convert.ToString(dtblUserDetail.Rows[0]["LAST_NAME"]);
            GetRegisteredDeviceDetailsByUserId objRegisteredDevOfUser = new GetRegisteredDeviceDetailsByUserId(this.CompanyId,
                                                                        this.SubAdminId, this.UserId);
            objRegisteredDevOfUser.Process();
            if (objRegisteredDevOfUser.StatusCode != 0)
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
                return;
            }
        //    MBuzzServerConfigDtls objMBuzzSetting = new MBuzzServerConfigDtls();
        //    try
        //    {
        //        objMBuzzSetting =
        //ServerMappingManager.getMbuzzConfigDtls(this.CompanyId);
        //    }
        //    catch
        //    {
        //        objMBuzzSetting.ServerIP = String.Empty;
        //        objMBuzzSetting.PortNumber = 0;
        //    }
            SqlTransaction sqlTransaction = null;
            SqlConnection con = null;

            int iRowsAffected = 0;
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (sqlTransaction = con.BeginTransaction())
                    {

                        logDeletedDeviceDataByDeviceIdAndType(sqlTransaction, con);
                        iRowsAffected = deleteRegisteredDeviceOfUser(sqlTransaction, con);
                        if (iRowsAffected > 0)
                        {
                            List<MFEDevice> lstDevicesToDelete = new List<MFEDevice>();
                            MFEDevice objMfeDevice = new MFEDevice();
                            objMfeDevice.DeviceId = this.DeviceId;
                            objMfeDevice.DeviceType = this.DeviceType;
                            lstDevicesToDelete.Add(objMfeDevice);


                            DeleteMobileSessionOfUser objDeleteMobileSess = new DeleteMobileSessionOfUser(this.UserId, this.CompanyId, this.SubAdminId, USER_LOG_OUT_TYPE.DEVICE_DELETED
                                , DeleteMobileSessionOfUser.MBUZZ_PENDING_MSGS_DELETE_TYPE.Device, true, con, sqlTransaction, lstDevicesToDelete);

                            objDeleteMobileSess.ProcessInTransaction(objGetUserDetail,objRegisteredDevOfUser,0);
                            
                            DeleteDevcDtlsFromMGram objDeleteDvcFromMGram = new DeleteDevcDtlsFromMGram(this.CompanyId, this.DeviceId, this.DeviceType, strUserName);
                            objDeleteDvcFromMGram.Process();
                            if (objDeleteDvcFromMGram.StatusCode != 0) throw new Exception();

                            sqlTransaction.Commit();
                            StatusCode = 0;
                            Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.DEVICE_DELETE, this.CompanyId, this.SubAdminId, this.UserId, strUserName, strFullName, this.DeviceId, this.DeviceType, this.DeviceModel, this.OSVersion, this.AppVersion);
                            
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }

                }
            }
            catch (SqlException ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    StatusDescription = DATABASE_ERRORS.DATABASE_CONNECTION_ERROR.ToString();
                    StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                StatusDescription = DATABASE_ERRORS.RECORD_INSERT_ERROR.ToString();
                StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (sqlTransaction != null)
                {
                    sqlTransaction.Dispose();
                }
            }
        }

        bool isDeviceAlreadyRegisteredForOtherUser()
        {
            string strSqlQuery = @"SELECT * 
                                    FROM TBL_REGISTERED_DEVICE
                                    WHERE USER_ID != @UserId
                                    AND DEVICE_TYPE = @DeviceType
                                    AND DEVICE_ID = @DeviceId
                                    AND COMPANY_ID = @CompanyId
                                    AND DEVICE_MODEL =@DeviceModel
                                    AND DEVICE_SIZE = @DEVICE_SIZE";
            SqlCommand cmd = new SqlCommand(strSqlQuery);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@CompanyId", CompanyId);
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@DeviceType", DeviceType);
            cmd.Parameters.AddWithValue("@DeviceId", DeviceId);
            cmd.Parameters.AddWithValue("@DeviceModel", DeviceModel);
            cmd.Parameters.AddWithValue("@DEVICE_SIZE", (int)DeviceSize);

            DataSet dsRegisteredDevice = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (dsRegisteredDevice != null && dsRegisteredDevice.Tables.Count > 0)
            {
                if (dsRegisteredDevice.Tables[0] != null && dsRegisteredDevice.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        //int allowDeviceToUser()
        //{
        //    string strSqlQuery = getSqlQueryForAllowDevice();
        //    SqlCommand cmd = new SqlCommand(strSqlQuery);
        //    cmd.CommandType = CommandType.Text;

        //    cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
        //    cmd.Parameters.AddWithValue("@USER_ID", UserId);
        //    cmd.Parameters.AddWithValue("@DEVICE_TYPE", DeviceType);
        //    cmd.Parameters.AddWithValue("@DEVICE_ID", DeviceId);
        //    cmd.Parameters.AddWithValue("@REGISTRATION_DATE", DateTime.UtcNow.Ticks);
        //    cmd.Parameters.AddWithValue("@SUBADMIN_ID", SubAdminId);
        //    cmd.Parameters.AddWithValue("@MFICIENT_KEY", (Utilities.GetMd5Hash(CompanyId + UserId + DeviceId + DateTime.UtcNow.Ticks.ToString())).Substring(0, 9));
        //    cmd.Parameters.AddWithValue("@DEVICE_PUSH_MESSAGE_ID", this.DevPushMsgId);
        //    cmd.Parameters.AddWithValue("@OS_VERSION", this.OSVersion);
        //    cmd.Parameters.AddWithValue("@APP_VERSION", this.AppVersion);
        //    return cmd.ExecuteNonQuery();

        //}
        int allowDeviceToUser(
            SqlTransaction transaction,
            SqlConnection con,
            long datetimeOfProcess)
        {
            string strSqlQuery = getSqlQueryForAllowDevice();
            SqlCommand cmd = new SqlCommand(strSqlQuery, con, transaction);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
            cmd.Parameters.AddWithValue("@USER_ID", UserId);
            cmd.Parameters.AddWithValue("@DEVICE_TYPE", DeviceType);
            cmd.Parameters.AddWithValue("@DEVICE_ID", DeviceId);
            cmd.Parameters.AddWithValue("@REGISTRATION_DATE", datetimeOfProcess);
            cmd.Parameters.AddWithValue("@SUBADMIN_ID", SubAdminId);
            cmd.Parameters.AddWithValue("@DEVICE_MODEL", DeviceModel);
            cmd.Parameters.AddWithValue("@DEVICE_SIZE", DeviceSize);
            cmd.Parameters.AddWithValue("@MFICIENT_KEY", Utilities.GetMd5Hash(DeviceSize + DeviceId + CompanyId + UserId).Substring(0, 10));//28/9/2012
            cmd.Parameters.AddWithValue("@DEVICE_PUSH_MESSAGE_ID", this.DevPushMsgId);
            cmd.Parameters.AddWithValue("@OS_VERSION", this.OSVersion);
            cmd.Parameters.AddWithValue("@APP_VERSION", AppVersion);
            return cmd.ExecuteNonQuery();
        }
        string getSqlQueryForAllowDevice()
        {
            string strSqlQuery = @"INSERT INTO [TBL_REGISTERED_DEVICE]
                                    ([USER_ID]
                                    ,[COMPANY_ID]
                                    ,[DEVICE_TYPE]
                                    ,[DEVICE_ID]
                                    ,[REGISTRATION_DATE]
                                    ,[SUBADMIN_ID]
                                    ,[DEVICE_MODEL]
                                    ,[DEVICE_SIZE]
                                    ,[MFICIENT_KEY]
                                    ,[DEVICE_PUSH_MESSAGE_ID]
                                    ,[OS_VERSION],[APP_VERSION])
                                VALUES
                                    (@USER_ID
                                    ,@COMPANY_ID
                                    ,@DEVICE_TYPE
                                    ,@DEVICE_ID
                                    ,@REGISTRATION_DATE
                                    ,@SUBADMIN_ID
                                    ,@DEVICE_MODEL
                                    ,@DEVICE_SIZE
                                    ,@MFICIENT_KEY
                                    ,@DEVICE_PUSH_MESSAGE_ID
                                    ,@OS_VERSION,@APP_VERSION)";
            return strSqlQuery;
        }

        int deleteRegisteredDeviceOfUser(SqlTransaction transaction, SqlConnection con)
        {
            string strQuery = @"DELETE FROM TBL_REGISTERED_DEVICE
                                WHERE COMPANY_ID = @CompanyId
                                AND USER_ID = @UserId
                                AND DEVICE_ID = @DeviceId
                                AND DEVICE_TYPE = @DeviceType
                                AND DEVICE_MODEL = @DeviceModel
                                AND DEVICE_SIZE = @DEVICE_SIZE";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@CompanyId", CompanyId);
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@DeviceId", DeviceId);
            cmd.Parameters.AddWithValue("@DeviceType", DeviceType);
            cmd.Parameters.AddWithValue("@DeviceModel", DeviceModel);
            cmd.Parameters.AddWithValue("@DEVICE_SIZE", DeviceSize);
            //execute the query

            return cmd.ExecuteNonQuery();


        }

        void deleteDeniedRequest(SqlTransaction transaction, SqlConnection con)
        {

            string strQuery = @"DELETE FROM TBL_DEVICE_REGISTRATION_REQUEST
                                WHERE COMPANY_ID = @CompanyId
                                AND USER_ID = @UserId
                                AND DEVICE_ID = @DeviceId
                                AND DEVICE_TYPE = @DeviceType
                                AND DEVICE_MODEL = @DeviceModel
                                AND DEVICE_SIZE = @DEVICE_SIZE";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@CompanyId", CompanyId);
            cmd.Parameters.AddWithValue("@UserId", UserId);
            //cmd.Parameters.AddWithValue("@SubAdminId", SubAdminId);
            cmd.Parameters.AddWithValue("@DeviceId", DeviceId);
            cmd.Parameters.AddWithValue("@DeviceType", DeviceType);
            cmd.Parameters.AddWithValue("@DeviceModel", DeviceModel);
            cmd.Parameters.AddWithValue("@DEVICE_SIZE", DeviceSize);
            //execute the query
            cmd.ExecuteNonQuery();
        }
        void deleteAnyPreviousReqInDeniedTable(SqlTransaction transaction, SqlConnection con)
        {
            string strQuery = @"DELETE FROM TBL_DEVICE_REGISTRATION_REQ_DENIED
                                WHERE COMPANY_ID = @CompanyId
                                AND USER_ID = @UserId
                                AND DEVICE_TYPE = @DeviceType
                                AND DEVICE_MODEL = @DeviceModel
                                AND DEVICE_SIZE = @DEVICE_SIZE and DENIED_DATETIME<@DENIED_DATETIME";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@CompanyId", CompanyId);
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@DeviceType", DeviceType);
            cmd.Parameters.AddWithValue("@DeviceModel", DeviceModel);
            cmd.Parameters.AddWithValue("@DEVICE_SIZE", DeviceSize);
            cmd.Parameters.AddWithValue("@DENIED_DATETIME", DateTime.UtcNow.AddMonths(-6).Ticks);//DENIED_DATETIME
            //execute the query
            cmd.ExecuteNonQuery();
        }
        void saveDeniedRequestRecord(long datetimeOfProcess, SqlTransaction transaction, SqlConnection con)
        {
            try
            {
                string strQuery = @"INSERT INTO [TBL_DEVICE_REGISTRATION_REQ_DENIED]
                                   ([COMPANY_ID]
                                   ,[USER_ID]
                                   ,[DEVICE_TYPE]
                                   ,[DEVICE_ID]
                                   ,[DENIED_DATETIME]
                                   ,[DEVICE_MODEL]
                                   ,[DEVICE_SIZE]
                                    ,[OS_VERSION])
                             VALUES
                                   (@COMPANY_ID
                                   ,@USER_ID
                                   ,@DEVICE_TYPE
                                   ,@DEVICE_ID
                                   ,@DENIED_DATETIME
                                   ,@DEVICE_MODEL
                                   ,@DEVICE_SIZE
                                    ,@OS_VERSION)";
                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
                cmd.Parameters.AddWithValue("@USER_ID", UserId);
                cmd.Parameters.AddWithValue("@DEVICE_ID", DeviceId);
                cmd.Parameters.AddWithValue("@DEVICE_TYPE", DeviceType);
                cmd.Parameters.AddWithValue("@DEVICE_MODEL", DeviceModel);
                cmd.Parameters.AddWithValue("@DEVICE_SIZE", DeviceSize);
                cmd.Parameters.AddWithValue("@DENIED_DATETIME", datetimeOfProcess);
                cmd.Parameters.AddWithValue("@OS_VERSION", this.OSVersion);
                //execute the query
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //bool isDeviceIdAlreadyTaken()
        //{

        //}

        /// <summary>
        /// This is for logging deleted records for the given user under a subadmin
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="con"></param>
        /// <returns></returns>
        int logDeletedDeviceData(SqlTransaction transaction, SqlConnection con)
        {
            string strSqlQuery = @"INSERT INTO TBL_DELETED_DEVICE(COMPANY_ID,USER_ID,DEVICE_TYPE,DEVICE_ID,DELETED_DATE,DEVICE_MODEL,DEVICE_SIZE)
                                SELECT COMPANY_ID,USER_ID,DEVICE_TYPE,DEVICE_ID,@DateTimeNow,DEVICE_MODEL,DEVICE_SIZE
                                FROM TBL_REGISTERED_DEVICE
                                WHERE USER_ID = @UserId
                                AND SUBADMIN_ID = @SubAdminId";

            SqlCommand cmd = new SqlCommand(strSqlQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@SubAdminId", SubAdminId);
            cmd.Parameters.AddWithValue("@DateTimeNow", DateTime.UtcNow.Ticks);

            return cmd.ExecuteNonQuery();

        }

        /// <summary>
        /// This is for logging deleted records for a given user and specific device
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="con"></param>
        /// <returns></returns>

        int logDeletedDeviceDataByDeviceIdAndType(SqlTransaction transaction, SqlConnection con)
        {
            string strSqlQuery = @"INSERT INTO TBL_DELETED_DEVICE(COMPANY_ID,USER_ID,DEVICE_TYPE,DEVICE_ID,DELETED_DATE,DEVICE_MODEL,DEVICE_SIZE)
                                SELECT COMPANY_ID,USER_ID,DEVICE_TYPE,DEVICE_ID,@DateTimeNow,DEVICE_MODEL,DEVICE_SIZE
                                FROM TBL_REGISTERED_DEVICE
                                WHERE USER_ID = @UserId
                                AND SUBADMIN_ID = @SubAdminId
                                AND DEVICE_ID = @DeviceId
                                AND DEVICE_TYPE = @DeviceType
                                AND DEVICE_MODEL =@DeviceModel
                                AND DEVICE_SIZE = @DEVICE_SIZE";

            SqlCommand cmd = new SqlCommand(strSqlQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@SubAdminId", SubAdminId);
            cmd.Parameters.AddWithValue("@DeviceId", DeviceId);
            cmd.Parameters.AddWithValue("@DeviceType", DeviceType);
            cmd.Parameters.AddWithValue("@DateTimeNow", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@DeviceModel", DeviceModel);
            cmd.Parameters.AddWithValue("@DEVICE_SIZE", DeviceSize);

            return cmd.ExecuteNonQuery();

        }

        int removeFromRequest(SqlTransaction transaction, SqlConnection con)
        {
            string strQuery = @"DELETE FROM TBL_DEVICE_REGISTRATION_REQUEST
                                WHERE USER_ID = @UserId
                                AND COMPANY_ID = @CompanyId
                                AND DEVICE_TYPE = @DeviceType
                                AND DEVICE_ID = @DeviceId
                                AND DEVICE_MODEL=@DeviceModel
                                AND DEVICE_SIZE = @DEVICE_SIZE;";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@CompanyId", CompanyId);
            cmd.Parameters.AddWithValue("@DeviceType", DeviceType);
            cmd.Parameters.AddWithValue("@DeviceId", DeviceId);
            cmd.Parameters.AddWithValue("@DeviceModel", DeviceModel);
            cmd.Parameters.AddWithValue("@DEVICE_SIZE", DeviceSize);
            //execute the query

            return cmd.ExecuteNonQuery();

        }

        void processSaveUpdationReqInfo(
            SqlTransaction transaction,
            SqlConnection con,
            long dateTimeOfProcess)
        {
            try
            {
                saveUpdationRequiredInfo((int)UPDATE_TYPE.COMPANY_LOGO_UPDATE, dateTimeOfProcess, transaction, con);
                saveUpdationRequiredInfo((int)UPDATE_TYPE.USER_DETAILS_UPDATE, dateTimeOfProcess, transaction, con);
                saveUpdationRequiredInfo((int)UPDATE_TYPE.MENU_CATEGORY_UPDATE, dateTimeOfProcess, transaction, con);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        int saveUpdationRequiredInfo(
            int updationType, long dateTimeOfProcess,
            SqlTransaction transaction, SqlConnection con)
        {
            //int StatusCode = -1000,for error.
            try
            {
                SqlCommand cmd = new SqlCommand(@"INSERT INTO [TBL_UPDATION_REQUIRED_DETAIL]
                                               ([COMPANY_ID],[USER_ID],[DEVICE_ID]
                                               ,[DEVICE_TYPE],[UPDATED_ON],[UPDATION_TYPE])
                                         VALUES
                                               (@COMPANY_ID,@USER_ID,@DEVICE_ID
                                               ,@DEVICE_TYPE,@UPDATED_ON,@UPDATION_TYPE)", con, transaction);

                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                cmd.Parameters.AddWithValue("@USER_ID", this.UserId);
                cmd.Parameters.AddWithValue("@DEVICE_ID", this.DeviceId);
                cmd.Parameters.AddWithValue("@DEVICE_TYPE", this.DeviceType);
                cmd.Parameters.AddWithValue("@UPDATED_ON", DateTime.UtcNow.Ticks);
                cmd.Parameters.AddWithValue("@UPDATION_TYPE", updationType);
                return cmd.ExecuteNonQuery();

            }
            catch
            {
                throw new Exception();
            }
        }
        int logDeletedDeviceDataByDeviceIdAndType(SqlTransaction transaction, SqlConnection con, string deviceId, string deviceType, string deviceModel, DEVICE_OR_ICON_SIZE deviceSize)
        {
            string strSqlQuery = @"INSERT INTO TBL_DELETED_DEVICE(COMPANY_ID,USER_ID,DEVICE_TYPE,DEVICE_ID,DELETED_DATE,DEVICE_MODEL,DEVICE_SIZE)
                                SELECT COMPANY_ID,USER_ID,DEVICE_TYPE,DEVICE_ID,@DateTimeNow,DEVICE_MODEL,DEVICE_SIZE
                                FROM TBL_REGISTERED_DEVICE
                                WHERE USER_ID = @UserId
                                AND SUBADMIN_ID = @SubAdminId
                                AND DEVICE_ID = @DeviceId
                                AND DEVICE_TYPE = @DeviceType
                                AND DEVICE_MODEL =@DeviceModel
                                AND DEVICE_SIZE = @DEVICE_SIZE";

            SqlCommand cmd = new SqlCommand(strSqlQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@SubAdminId", SubAdminId);
            cmd.Parameters.AddWithValue("@DeviceId", deviceId);
            cmd.Parameters.AddWithValue("@DeviceType", deviceType);
            cmd.Parameters.AddWithValue("@DateTimeNow", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@DeviceModel", deviceModel);
            cmd.Parameters.AddWithValue("@DEVICE_SIZE", deviceSize);

            return cmd.ExecuteNonQuery();

        }
        int deleteRegisteredDeviceOfUser(SqlTransaction transaction, SqlConnection con, string deviceId, string deviceType, string deviceModel, DEVICE_OR_ICON_SIZE deviceSize)
        {
            string strQuery = @"DELETE FROM TBL_REGISTERED_DEVICE
                                WHERE COMPANY_ID = @CompanyId
                                AND USER_ID = @UserId
                                AND SUBADMIN_ID = @SubAdminId
                                AND DEVICE_ID = @DeviceId
                                AND DEVICE_TYPE = @DeviceType
                                AND DEVICE_MODEL = @DeviceModel
                                AND DEVICE_SIZE = @DEVICE_SIZE";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@CompanyId", CompanyId);
            cmd.Parameters.AddWithValue("@UserId", UserId);
            cmd.Parameters.AddWithValue("@SubAdminId", SubAdminId);
            cmd.Parameters.AddWithValue("@DeviceId", deviceId);
            cmd.Parameters.AddWithValue("@DeviceType", deviceType);
            cmd.Parameters.AddWithValue("@DeviceModel", deviceModel);
            cmd.Parameters.AddWithValue("@DEVICE_SIZE", deviceSize);
            //execute the query

            return cmd.ExecuteNonQuery();
        }


        /// <summary>
        /// Delete existing session when a particular device is deleted
        /// </summary>
        /// <param name="con"></param>
        /// <param name="transaction"></param>
        /// <param name="userId"></param>
        /// <param name="deviceType"></param>
        /// <param name="deviceId"></param>
        /// <param name="logOutType"></param>
        /// <returns></returns>
        int processDeleteExistingSession(SqlConnection con, SqlTransaction transaction, string userId, string deviceType, string deviceId, USER_LOG_OUT_TYPE logOutType, string CompanyID)
        {
            int iStatus = -1000;//For error
            try
            {
                string strLogOutType = "";
                switch (logOutType)
                {
                    case USER_LOG_OUT_TYPE.USER_DELETED:
                        strLogOutType = "User Deleted";
                        break;
                    case USER_LOG_OUT_TYPE.USER_BLOCKED:
                        strLogOutType = "User Blocked";
                        break;
                    case USER_LOG_OUT_TYPE.DEVICE_DELETED:
                        strLogOutType = "Device Deleted";
                        break;
                }
                updateLoginHistory(con, transaction, userId, deviceId, deviceType, strLogOutType, CompanyID);
                //deleteExistingSession(con, transaction, userId, deviceId, deviceType);
                iStatus = 0;
                return iStatus;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// For removing existing session
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlTransaction"></param>
        /// <param name="logOutTime"></param>
        /// <returns></returns>
        int updateLoginHistory(SqlConnection sqlConnection, SqlTransaction sqlTransaction, string userId, string deviceId, string deviceType, string logoutType, string CompanyID)
        {

            string strQuery = @"UPDATE TBL_MOBILE_USER_LOGIN_HISTORY
                                                SET LOGOUT_DATETIME = @LogOutDateTime,
                                                LOGOUT_TYPE = @LogOutType
                                                WHERE USER_ID = @UserId
                                                AND  COMPANY_ID = @COMPANY_ID
                                                AND LOGOUT_DATETIME = 0
                                                AND LOGIN_DEVICE_TOKEN =@LoginDeviceToken
                                                AND LOGIN_DEVICE_TYPE_CODE =@LoginDeviceTypeCode";
            SqlCommand cmd = new SqlCommand(strQuery, sqlConnection, sqlTransaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", userId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyID);
            cmd.Parameters.AddWithValue("@LogOutDateTime", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@LoginDeviceToken", deviceId);
            cmd.Parameters.AddWithValue("@LoginDeviceTypeCode", deviceType);
            cmd.Parameters.AddWithValue("@LogOutType", logoutType);
            return cmd.ExecuteNonQuery();
        }




        /// <summary>
        /// For removing existing session
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="sqlTransaction"></param>
        /// <param name="userLoginDtlsDTable"></param>
        /// <returns></returns>
        //        int deleteExistingSession(SqlConnection sqlConnection, SqlTransaction sqlTransaction, string userId, string deviceId, string deviceType)
        //        {
        //            string strQuery = @"DELETE FROM TBL_MOBILE_USER_LOGIN_SESSION
        //                                WHERE USER_ID = @UserId
        //                                AND LOGIN_DEVICE_TOKEN = @LoginDeviceToken
        //                                AND LOGIN_DEVICE_TYPE_CODE = @LoginDeviceTypeCode;";
        //            SqlCommand cmd = new SqlCommand(strQuery, sqlConnection, sqlTransaction);
        //            cmd.CommandType = CommandType.Text;
        //            cmd.Parameters.AddWithValue("@UserId", userId);
        //            cmd.Parameters.AddWithValue("@LoginDeviceToken", deviceId);
        //            cmd.Parameters.AddWithValue("@LoginDeviceTypeCode", deviceType);
        //            return cmd.ExecuteNonQuery();
        //        }


        /// <summary>
        /// Status code if 0 then process is success else some internal error
        /// </summary>
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string SubAdminId
        {
            set;
            get;
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string UserId
        {
            get;
            set;
        }
        public int RequestType
        {
            get;
            set;
        }
        public string DeviceType
        {
            get;
            set;
        }
        public string DeviceId
        {
            get;
            set;
        }
        public string DeviceModel
        {
            get;
            set;
        }
        public string DevPushMsgId
        {
            get;
            set;
        }
        public string OSVersion
        {
            get;
            set;
        }
        public string AppVersion
        {
            get;
            set;
        }
        public DEVICE_OR_ICON_SIZE DeviceSize
        {
            get;
            set;
        }

        public List<MFEDevice> DevicesToDelete
        {
            get;
            set;
        }
        public string AdminId
        {
            get;
            set;
        }
        public HttpContext AppContext
        {
            get;
            private set;
        }
    }
}
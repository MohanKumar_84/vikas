﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetAdminUsers
    {
        public GetAdminUsers(string _AdminId, string _SubAdminId, string _company_ID, Boolean _IsSelectAll, bool _IsFindBySubAdmin)
        {
            AdminId = _AdminId;
            SubAdminId = _SubAdminId;
            Company_id = _company_ID;
            IsSelectAll = _IsSelectAll;
            IsFindBySubAdmin = _IsFindBySubAdmin;
        }
        public void Process()
        {
            try
            {
                string query;
                DataSet objDataSet;
                SqlCommand objSqlCommand = null;
                if (this.IsSelectAll)
                {
                    query = @"SELECT USERDETAIL.USER_ID,USERDETAIL.USER_NAME, USERDETAIL.FIRST_NAME,USERDETAIL.LAST_NAME,USERDETAIL.EMAIL_ID,USERDETAIL.IS_ACTIVE AS USERISACTIVE,
                        USERDETAIL.IS_BLOCKED AS USERISBLOCKED,isnull(SUBADMIN.FULL_NAME,'Not Exist') as  FULL_NAME,
                        isnull(SUBADMIN.ISACTIVE,0) AS SUBADMINISACTIVE,isnull(SUBADMIN.IS_BLOCKED,0) AS SUBADMINISBLOCKED,
                        isnull(SUBADMIN.SUBADMIN_ID,'') as SUBADMIN_ID
                        FROM TBL_USER_DETAIL AS USERDETAIL Left outer JOIN TBL_SUB_ADMIN AS SUBADMIN ON
                        USERDETAIL.SUBADMIN_ID = SUBADMIN.SUBADMIN_ID WHERE USERDETAIL.COMPANY_ID = @COMPANY_ID ORDER BY USERDETAIL.FIRST_NAME,USERDETAIL.LAST_NAME;";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", AdminId);
                    objSqlCommand.Parameters.AddWithValue("@Company_id", Company_id);
                }
                else if (this.IsFindBySubAdmin)
                {
                    query = @"SELECT USERDETAIL.USER_ID,USERDETAIL.USER_NAME, USERDETAIL.FIRST_NAME,USERDETAIL.LAST_NAME,USERDETAIL.EMAIL_ID,USERDETAIL.IS_ACTIVE AS USERISACTIVE,USERDETAIL.IS_BLOCKED AS USERISBLOCKED,SUBADMIN.FULL_NAME ,SUBADMIN.ISACTIVE AS SUBADMINISACTIVE,SUBADMIN.IS_BLOCKED AS SUBADMINISBLOCKED,SUBADMIN.SUBADMIN_ID
                            FROM TBL_USER_DETAIL AS USERDETAIL INNER JOIN TBL_SUB_ADMIN AS SUBADMIN ON
                            USERDETAIL.SUBADMIN_ID = SUBADMIN.SUBADMIN_ID WHERE SUBADMIN.SUBADMIN_ID=@SUBADMIN_ID AND USERDETAIL.COMPANY_ID = @COMPANY_ID ORDER BY USERDETAIL.FIRST_NAME,USERDETAIL.LAST_NAME;";
                    objSqlCommand = new SqlCommand(query);
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", AdminId);
                    objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                    objSqlCommand.Parameters.AddWithValue("@Company_id", Company_id);
                }
                objDataSet = MSSqlClient.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet != null)
                {
                    this.AdminUsers = objDataSet.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }

            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        public DataTable AdminUsers
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string AdminId
        {
            get;
            set;
        }
        public string SubAdminId
        {
            get;
            set;
        }
        public string Company_id
        {
            get;
            set;
        }
        public Boolean IsSelectAll
        {
            set;
            get;
        }
        public Boolean IsFindBySubAdmin
        {
            set;
            get;
        }
    }
}
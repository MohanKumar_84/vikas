﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class AddUserToGroup
    {
       public AddUserToGroup()
        { 

        }
        public void Process()
        {
            try
            {
                string query = @"INSERT INTO TBL_USER_GROUP_LINK(GROUP_ID,USER_ID,CREATED_ON) VALUES(@GROUP_ID,@USER_ID,@CREATED_ON);";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@GROUP_ID", this.GroupId);
                objSqlCommand.Parameters.AddWithValue("@USER_ID", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@CREATED_ON", DateTime.UtcNow.Ticks);
                
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                  
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                }
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        
        public string GroupId
        {
            set;
            get;
        }
        public string UserId
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
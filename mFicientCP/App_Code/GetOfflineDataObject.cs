﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class GetOfflineDataObject
    {
        #region Private Memebers

        private string id, name, companyId, strQuery;
        private List<OfflineDataObject> lstOfflineDataObjects;
        private OfflineDataObject objOfflineDataObject;
        private DataSet dsDataObject;
        private SqlCommand cmd;

        #endregion

        #region Constructors

        public GetOfflineDataObject(string _id, string _companyId)
        {
            id = _id;
            companyId = _companyId;
            try
            {
                getObjectBasedOnId();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GetOfflineDataObject(string _companyId)
        {
            companyId = _companyId;
            lstOfflineDataObjects = new List<OfflineDataObject>();
            try
            {
                getAllDataObjects();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Public Properties

        public List<OfflineDataObject> DataObjects
        {
            get
            {
                return lstOfflineDataObjects;
            }
        }

        public OfflineDataObject DataObject
        {
            get
            {
                return objOfflineDataObject;
            }
        }

        #endregion

        public void GetObjectBasedOnName(string _name)
        {
            name = _name;
            try
            {
                strQuery = "SELECT * FROM TBL_OFFLINE_OBJECTS WHERE OFFLINE_OBJECT_NAME = @OFFLINE_OBJECT_NAME AND COMPANY_ID = @COMPANY_ID";

                cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@OFFLINE_OBJECT_NAME", name);
                cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
                dsDataObject = MSSqlClient.SelectDataFromSQlCommandAdminData(cmd);
                if (dsDataObject != null && dsDataObject.Tables.Count > 0)
                {
                    DataRow objDataRow = dsDataObject.Tables[0].Rows[0];
                    objOfflineDataObject = new OfflineDataObject(id, objDataRow["COMPANY_ID"].ToString(), objDataRow["SUBADMIN_ID"].ToString(),
                                                                 objDataRow["OFFLINE_OBJECT_NAME"].ToString(), objDataRow["DESCRIPTION"].ToString(),
                                                                 objDataRow["QUERY"].ToString(), objDataRow["INPUT_PARAMETERS"].ToString(),
                                                                 objDataRow["OUTPUTS"].ToString(), objDataRow["OFFLINE_TABLES"].ToString(),
                                                                 (DatabaseCommandType)(Convert.ToInt32(objDataRow["QUERY_TYPE"].ToString())),
                                                                 Convert.ToBoolean(objDataRow["ALLOW_RETRY"].ToString()), Convert.ToBoolean(objDataRow["EXIT_ON_ERROR"].ToString()), objDataRow["ERROR_MESSAGE"].ToString());
                }
                else
                {
                    objOfflineDataObject = null;
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }

        #region Private Methods

        private void getObjectBasedOnId()
        {
            try
            {
                strQuery = "SELECT * FROM TBL_OFFLINE_OBJECTS WHERE OFFLINE_OBJECT_ID = @OFFLINE_OBJECT_ID AND COMPANY_ID = @COMPANY_ID";

                cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@OFFLINE_OBJECT_ID", id);
                cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
                dsDataObject = MSSqlClient.SelectDataFromSQlCommandAdminData(cmd);
                if (dsDataObject != null && dsDataObject.Tables.Count > 0)
                {
                    DataRow objDataRow = dsDataObject.Tables[0].Rows[0];
                    objOfflineDataObject = new OfflineDataObject(id, objDataRow["COMPANY_ID"].ToString(), objDataRow["SUBADMIN_ID"].ToString(),
                                                                 objDataRow["OFFLINE_OBJECT_NAME"].ToString(), objDataRow["DESCRIPTION"].ToString(),
                                                                 objDataRow["QUERY"].ToString(), objDataRow["INPUT_PARAMETERS"].ToString(),
                                                                 objDataRow["OUTPUTS"].ToString(), objDataRow["OFFLINE_TABLES"].ToString(),
                                                                 (DatabaseCommandType)(Convert.ToInt32(objDataRow["QUERY_TYPE"].ToString())),
                                                                 Convert.ToBoolean(objDataRow["ALLOW_RETRY"].ToString()), Convert.ToBoolean(objDataRow["EXIT_ON_ERROR"].ToString()), objDataRow["ERROR_MESSAGE"].ToString());
                }
                else
                {
                    objOfflineDataObject = null;
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }

        private void getAllDataObjects()
        {
            try
            {
                strQuery = "SELECT * FROM TBL_OFFLINE_OBJECTS WHERE COMPANY_ID = @COMPANY_ID";

                cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
                dsDataObject = MSSqlClient.SelectDataFromSQlCommandAdminData(cmd);
                if (dsDataObject != null && dsDataObject.Tables.Count > 0)
                {
                    DataTable dt = dsDataObject.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            objOfflineDataObject = new OfflineDataObject(row["OFFLINE_OBJECT_ID"].ToString(), row["COMPANY_ID"].ToString(), row["SUBADMIN_ID"].ToString(),
                                                                         row["OFFLINE_OBJECT_NAME"].ToString(), row["DESCRIPTION"].ToString(),
                                                                         row["QUERY"].ToString(), row["INPUT_PARAMETERS"].ToString(),
                                                                         row["OUTPUTS"].ToString(), row["OFFLINE_TABLES"].ToString(),
                                                                         (DatabaseCommandType)(Convert.ToInt32(row["QUERY_TYPE"].ToString())),
                                                                         Convert.ToBoolean(row["ALLOW_RETRY"].ToString()), Convert.ToBoolean(row["EXIT_ON_ERROR"].ToString()), row["ERROR_MESSAGE"].ToString());
                            lstOfflineDataObjects.Add(objOfflineDataObject);
                        }
                    }
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw ex;
                }
            }
        }

        #endregion
    }

}
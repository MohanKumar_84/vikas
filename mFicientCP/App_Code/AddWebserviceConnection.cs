﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientCP
{
    public class AddWebserviceConnection
    {
       //public AddWebserviceConnection()
       // { 

       // }

       //public AddWebserviceConnection(string _SubAdminId, string _WebServiceType, string _Url, string _UserName, string _Password, string _CompanyID, string _ResponseType, string _ConnectionName, string _WsdlUrl, string _WsdContent, string _MpluginAgent)
       // {
       //     this.ConnectionName = _ConnectionName;
       //     this.SubAdminId = _SubAdminId;
       //     this.WebserviceType = _WebServiceType;
       //     this.Url = _Url;
       //     this.UserName = _UserName;
       //     this.CompanyId = _CompanyID;
       //     this.Password = _Password;
       //     this.ResponseType = _ResponseType;
       //     this.WsdlUrl = _WsdlUrl;
       //     this.WsdContent = _WsdContent;
       //     this.MpluginAgent = _MpluginAgent;
       // }
     
       public AddWebserviceConnection(string _SubAdminId, string _WebServiceType, string _Url, string _UserName, string _Password, string _Autenticationtype, string _CompanyID, string _ResponseType, string _ConnectionName, string _WsdlUrl, string _WsdContent, string _MpluginAgent,string _CREDENTIAL_PROPERTY)
       {
           this.ConnectionName = _ConnectionName;
           this.SubAdminId = _SubAdminId;
           this.WebserviceType = _WebServiceType;
           this.Url = _Url;
           //this.UserName = _UserName;
           this.CompanyId = _CompanyID;
           //this.Password = _Password;
           this.ResponseType = _ResponseType;
           this.WsdlUrl = _WsdlUrl;
           this.WsdContent = _WsdContent;
           this.MpluginAgent = _MpluginAgent;
           this.HttpAutenticationtype = _Autenticationtype;
           this.CREDENTIAL_PROPERTY = _CREDENTIAL_PROPERTY;
       }
//        public void Process()
//        {
//            try
//            {
//                this.StatusCode = -1000;
//                this.ConnectionId = Utilities.GetMd5Hash(this.SubAdminId + this.ConnectionName + DateTime.UtcNow.Ticks.ToString());
//                string query = @"INSERT INTO TBL_WEBSERVICE_CONNECTION(WS_CONNECTOR_ID,WEBSERVICE_TYPE,WEBSERVICE_URL,USER_NAME,PASSWORD,SUBADMIN_ID,RESPONSE_TYPE,CONNECTION_NAME,WSDL_URL,WSDL_CONTENT,MPLUGIN_AGENT,COMPANY_ID)
//                                        VALUES(@CONNECTION_ID,@WEBSERVICE_TYPE,@WEBSERVICE_URL,@USER_NAME,@PASSWORD,@SUBADMIN_ID,@RESPONSE_TYPE,@CONNECTION_NAME,@WSDL_URL,@WSDL_CONTENT,@MPLUGIN_AGENT,@COMPANY_ID);";

//                SqlCommand objSqlCommand = new SqlCommand(query);
//                objSqlCommand.CommandType = CommandType.Text;
//                objSqlCommand.Parameters.AddWithValue("@CONNECTION_ID", this.ConnectionId);
//                objSqlCommand.Parameters.AddWithValue("@WEBSERVICE_TYPE", this.WebserviceType);
//                objSqlCommand.Parameters.AddWithValue("@WEBSERVICE_URL", this.Url);
//                objSqlCommand.Parameters.AddWithValue("@USER_NAME", this.UserName);
//                objSqlCommand.Parameters.AddWithValue("@PASSWORD", this.Password);
//                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
//                objSqlCommand.Parameters.AddWithValue("@RESPONSE_TYPE", this.ResponseType);
//                objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
//                objSqlCommand.Parameters.AddWithValue("@WSDL_URL", this.WsdlUrl);
//                objSqlCommand.Parameters.AddWithValue("@WSDL_CONTENT", this.WsdContent);
//                objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgent);
//                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
//                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
//            }
//            catch (Exception ex)
//            {
//                this.StatusCode = -1000;
//            }
//        }

        public void Process2()
        {
            try
            {
                this.StatusCode = -1000;
                this.ConnectionId = Utilities.GetMd5Hash(this.SubAdminId + this.ConnectionName + DateTime.UtcNow.Ticks.ToString());
                string query = @"INSERT INTO TBL_WEBSERVICE_CONNECTION(WS_CONNECTOR_ID,WEBSERVICE_TYPE,WEBSERVICE_URL,SUBADMIN_ID,RESPONSE_TYPE,CONNECTION_NAME,WSDL_URL,WSDL_CONTENT,MPLUGIN_AGENT,COMPANY_ID,AUTHENTICATION_TYPE,CREATED_BY,CREATED_ON,UPDATED_ON,CREDENTIAL_PROPERTY)
                                        VALUES(@CONNECTION_ID,@WEBSERVICE_TYPE,@WEBSERVICE_URL,@SUBADMIN_ID,@RESPONSE_TYPE,@CONNECTION_NAME,@WSDL_URL,@WSDL_CONTENT,@MPLUGIN_AGENT,@COMPANY_ID,@AUTHENTICATION_TYPE,@CREATED_BY,@CREATED_ON,@UPDATED_ON,@CREDENTIAL_PROPERTY);";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@CONNECTION_ID", this.ConnectionId);
                objSqlCommand.Parameters.AddWithValue("@WEBSERVICE_TYPE", this.WebserviceType);
                objSqlCommand.Parameters.AddWithValue("@WEBSERVICE_URL", this.Url);
                objSqlCommand.Parameters.AddWithValue("@SUBADMIN_ID", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@RESPONSE_TYPE", this.ResponseType);
                objSqlCommand.Parameters.AddWithValue("@CONNECTION_NAME", this.ConnectionName);
                objSqlCommand.Parameters.AddWithValue("@WSDL_URL", this.WsdlUrl);
                objSqlCommand.Parameters.AddWithValue("@WSDL_CONTENT", this.WsdContent);
                objSqlCommand.Parameters.AddWithValue("@MPLUGIN_AGENT", this.MpluginAgent);
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@AUTHENTICATION_TYPE", this.HttpAutenticationtype);
                objSqlCommand.Parameters.AddWithValue("@CREATED_BY", this.SubAdminId);
                objSqlCommand.Parameters.AddWithValue("@CREATED_ON",DateTime.UtcNow.Ticks);
                objSqlCommand.Parameters.AddWithValue("@UPDATED_ON", DateTime.UtcNow.Ticks);
                objSqlCommand.Parameters.AddWithValue("@CREDENTIAL_PROPERTY", this.CREDENTIAL_PROPERTY);
                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0) this.StatusCode = 0;
            }
            catch 
            {
                this.StatusCode = -1000;
            }
        }


        public string SubAdminId { get; set; }
        public string Database { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string DataBaseType { get; set; }
        public string ConnectionName { get; set; }
        public string ConnectionId { get; set; }
        public int StatusCode { get; set; }
        public string CompanyId { get; set; }
        public string WebserviceType { get; set; }
        public string Url { get; set; }
        public string UserName { get; set; }
        public string ResponseType { get; set; }
        public string WsdlUrl { get; set; }
        public string WsdContent { get; set; }
        public bool UseMplugin { get; set; }
        public string MpluginAgent { get; set; }
        public string HttpAutenticationtype { get; set; }
        public string CREDENTIAL_PROPERTY { get; set; }
    }
}
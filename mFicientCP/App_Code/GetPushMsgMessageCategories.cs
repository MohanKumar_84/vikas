﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class GetPushMsgMessageCategories
    {
        public GetPushMsgMessageCategories(string companyId, string userId)
        {
            this.CompanyId = companyId;
            this.UserId = userId;
        }
        public void Process()
        {
            try
            {
                string query = @"SELECT  * FROM  TBL_PUSH_MSG_MESSAGE_CATEGORY WHERE COMPANY_ID = @COMPANY_ID
                                AND USER_ID = @USER_ID;";
                SqlCommand cmd = new SqlCommand(query);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                cmd.Parameters.AddWithValue("@USER_ID", this.UserId);
                DataSet ds = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (ds != null)
                {
                    this.ResultTable = ds.Tables[0];
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        this.MsgCategories = getMsgCategories(ds.Tables[0]);
                    }
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }

            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        public int ProcessGetMsgCategoriesCount()
        {
            int iMsgCategoryCount = 0;
            try
            {
                string query = @"SELECT COUNT( * ) AS TOTAL FROM  TBL_PUSH_MSG_MESSAGE_CATEGORY WHERE COMPANY_ID = @COMPANY_ID
                                AND USER_ID = @USER_ID;";
                SqlCommand cmd = new SqlCommand(query);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                cmd.Parameters.AddWithValue("@USER_ID", this.UserId);
                DataSet ds = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        iMsgCategoryCount = Convert.ToInt32(ds.Tables[0].Rows[0]["TOTAL"]);
                    }
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }

            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
            return iMsgCategoryCount;
        }
        List<MFEPushMsgMessageCategory> getMsgCategories(DataTable msgCategories)
        {
            if (msgCategories == null) throw new ArgumentNullException();
            List<MFEPushMsgMessageCategory> lstMsgCategories =
                new List<MFEPushMsgMessageCategory>();
            foreach (DataRow row in msgCategories.Rows)
            {
                lstMsgCategories.Add(getMsgCategoryFromRow(row));
            }
            return lstMsgCategories;
        }
        MFEPushMsgMessageCategory getMsgCategoryFromRow(DataRow row)
        {
            if (row == null) throw new ArgumentNullException();
            MFEPushMsgMessageCategory objMsgCategory = new MFEPushMsgMessageCategory();
            objMsgCategory.CompanyId = Convert.ToString(row["COMPANY_ID"]);
            objMsgCategory.SubAdminId = Convert.ToString(row["USER_ID"]);
            objMsgCategory.MsgCategoryId = Convert.ToString(row["MSG_CATEGORY_ID"]);
            objMsgCategory.MessageCategory = Convert.ToString(row["MSG_CATEGORY"]);
            return objMsgCategory;
        }

        public string CompanyId
        {
            get;
            private set;
        }
        public int StatusCode
        {
            get;
            private set;
        }
        public string StatusDescription
        {
            get;
            private set;
        }
        public DataTable ResultTable
        {
            get;
            private set;
        }
        public string UserId
        {
            get;
            private set;
        }
        public List<MFEPushMsgMessageCategory> MsgCategories
        {
            get;
            private set;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientCP
{
    public class DeleteVIPUsers
    {
        public DeleteVIPUsers(string userId,string companyId)
        {
            this.CompanyId = companyId;
            this.UserId = userId;
        }
        public void Process()
        {
            try
            {
                string query = @"DELETE FROM TBL_USER_DEVICE_SETTINGS
                                WHERE USER_ID = @UserId
                                AND COMPANY_ID =@CompanyId;";
                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@UserId", this.UserId);
                objSqlCommand.Parameters.AddWithValue("@CompanyId", this.CompanyId);

                if (MSSqlClient.ExecuteNonQueryRecord(objSqlCommand) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
                }

            }
            catch 
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }

        public string CompanyId
        {
            get;
            set;
        }

        public string UserId
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
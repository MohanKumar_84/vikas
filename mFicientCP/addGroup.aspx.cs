﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientCP
{
    public partial class addGroup : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!Page.IsPostBack)
            {
                BindUsersRepeater();
            }


        }
        protected void BindUsersRepeater()
        {
            GetUserDetail userDetail = new GetUserDetail();
            userDetail.IsSelectAll = true;
            userDetail.SubAdminId = strUserId;
            userDetail.Process();
            DataTable dtUsers = userDetail.ResultTable;
            if (dtUsers != null)
            {
                if (dtUsers.Rows.Count > 0)
                {
                    rptUsers.DataSource = dtUsers;
                    rptUsers.DataBind();
                }
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtGroup.Text != "")
            {
                AddGroup addUserGroup = new AddGroup();
                addUserGroup.GroupName = txtGroup.Text;
                addUserGroup.SubAdminId = strUserId;
                addUserGroup.CompanyId = hfsPart4;
                addUserGroup.Process();
                AddUserToGroup addUser;
                foreach (RepeaterItem item in rptUsers.Items)
                {
                    Label lblUserID = (Label)item.FindControl("lblUserID");
                    CheckBox chkUser = (CheckBox)item.FindControl("chkUser");
                    if (chkUser.Checked)
                    {
                        addUser = new AddUserToGroup();
                        addUser.GroupId = addUserGroup.GroupId;
                        addUser.UserId = lblUserID.Text;
                        addUser.Process();
                        chkUser.Checked = false;
                    }
                }

                Utilities.SendNotificationMenuUpdate(hfsPart4, "5");
                txtGroup.Text = "";
            }
        }


    }
}
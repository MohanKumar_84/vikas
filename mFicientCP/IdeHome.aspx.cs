﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mFicientCP
{
    public partial class IdeHome : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string strCompanyId = string.Empty;
        string strAdminId = string.Empty;
        string SubAdminid = "", CompanyId = "", Sessionid = "", Adminid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            MasterPage mainMaster = Page.Master;
            if (Page.IsPostBack)
            {
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
                ((HiddenField)mainMaster.FindControl("hfs")).Value = hfsValue;
                ((HiddenField)mainMaster.FindControl("hfbid")).Value = hfbidValue;
            }

            Utilities.SaveInContext(context, hfsValue, hfbidValue, out strUserId, out strSessionId, out strCompanyId, out strAdminId);

            //string str = Utilities.DecryptString(hfsValue);
            //SubAdminid = str.Split(',')[0];
            //CompanyId = str.Split(',')[3].ToLower();
            //Sessionid = str.Split(',')[1];
            //Adminid = str.Split(',')[2];

            context.Items["hfs1"] = SubAdminid;
            context.Items["hfs2"] = Sessionid;
            context.Items["hfs3"] = Adminid;
            context.Items["hfs4"] = CompanyId;
        }

    }
}
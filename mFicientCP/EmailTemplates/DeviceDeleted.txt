﻿Dear <%username%>
Your device 
OS / Model :<%OS%>
Device Id : <%DeviceId%>
Device Type : <%DeviceType%>
was deleted by sub admin <%subadminname%>
on <%date%>.

In case of any further queries, please contact support@mficient.com

﻿<%@ Page Title="mFicient | Apps" Language="C#" MasterPageFile="~/master/Canvas.master"
    AutoEventWireup="true" CodeBehind="manageApps.aspx.cs" Inherits="mFicientCP.manageApps" %>

<%@ Register TagPrefix="userDetails" TagName="Details" Src="~/UserControls/UserDetails.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <style type="text/css">
        .modPopUpCheckBoxTable
        {
            border: 0px;
            text-align: left;
            margin-bottom: 6px;
            width: 100%;
            position: relative;
            left: -3px;
            top: 2px;
            background: none;
        }
        table.modPopUpCheckBoxTable td, table.modPopUpCheckBoxTable td
        {
            border: 0px;
            text-align: justify;
            padding: 0px;
        }
        div.radio + label
        {
            position: relative;
            top: 3px;
        }
        div.singleSection .column3 div.selector
        {
            left: -4px;
        }
        .repeaterTable.havingUniformCheckBox td:not(.chkBox) > span
        {
            position: relative;
        }
        span.rptSpanHeaderText
        {
            position: relative;
            top: -2px;
            font-family: 'Trebuchet MS' , 'Lucida Sans Unicode' , 'Lucida Grande' , 'Lucida Sans' , Tahoma, Arial, Helvetica, sans-serif;
            font-size: 12px;
            font-style: normal;
            font-variant: normal;
            font-weight: bold;
        }
        .repeaterTable.havingUniformCheckBox td.chkBox, .repeaterTable.havingUniformCheckBox th.thChkBox
        {
            width: 30px;
        }
        .repeaterTable.havingUniformCheckBox td.chkBox > span, .repeaterTable.havingUniformCheckBox th.thChkBox > span
        {
            float: left;
            position: relative; /*top: 3px;*/
        }
        td.chkBox > span div.checker input, th.thChkBox > span div.checker input, td.chkBox > span div.checker, th.thChkBox > span div.checker
        {
            width: 16px;
            height: 16px;
        }
        form div .modalPopUpDetails div.singleSection fieldset section div.column3
        {
            width: 65%;
        }
        .modalPopUpDetails div.selector span
        {
            width: 270px;
        }
        #divDdlSubAdminCont .selector span
        {
            width: 200px;
        }
    </style>
    <script type="text/javascript">
        function setScroll() {
            var $stickyHeaderTop = $('#stickyheader');
            var $contDiv = $('#<%=pnlRptTableContainer.ClientID %>');
            $($contDiv).scroll(function () {
                var iContDivTop = $($contDiv).offset().top;
                var iStickyHeaderTop = $($stickyHeaderTop).offset().top;
                if (iContDivTop > iStickyHeaderTop) {
                    $($stickyHeaderTop).css({ position: 'fixed', top: iContDivTop });
                    setHeaderColumnWidth();
                } else {
                    $($stickyHeaderTop).css({ position: 'static', top: iContDivTop });
                }
            });

        }
        function setHeaderColumnWidth() {
            //var stickyHeader = $('#stickyheader').offset().top;
            var aryTheadColmn = $($('#stickyheader').children('tr')).children('th');
            var aryFirstRowColmn = $($($('#stickyheader').siblings('tbody').children('tr')[0]).children('td'));
            for (var i = 0; i <= aryFirstRowColmn.length - 1; i++) {
                $(aryTheadColmn[i]).width($(aryFirstRowColmn[i]).width());
            }
        }
        function getSelectedCommaSeparatedAppName() {
            var strSelectedAppNames = "";
            $.each($('.repeaterTable .chkBox'), function (index, value) {
                var $checkBox = $(this).children().find("input[id*='chkSelectApp']");
                var $spanAppName = $(this).children("span[id*='lblHidAppName']");
                if ($($checkBox).is(':checked')) {
                    if (strSelectedAppNames === "") {
                        strSelectedAppNames += $($spanAppName).text();
                    }
                    else {
                        strSelectedAppNames += ", " + $($spanAppName).text();
                    }
                }
            });
            return strSelectedAppNames;
        }
        function processChangeSubAdmin() {
            var strCommaSeparatedAppNames = getSelectedCommaSeparatedAppName();
            if (strCommaSeparatedAppNames) {
                var $popUpAppName = $('#' + '<%=lblAppName.ClientID %>');
                if ($popUpAppName) {
                    $($popUpAppName).text(strCommaSeparatedAppNames);
                }
                showModalPopUpWithOutHeader('divModalContainer', 'Change Subadmin', '480', false);
            }
            else {
                showMessage('Please select an App to change the subadmin.', '$.alert.Error', DialogType.Error);
            }
        }
        function processSelectAllCheckChange(sender) {
            var $chkSelectAllApps = ($(sender).find('input[type="checkbox"]'));
            if ($chkSelectAllApps) {
                if ($($chkSelectAllApps).is(":checked")) {
                    selectDeselectAppCheckboxes(true);
                }
                else {
                    selectDeselectAppCheckboxes(false);
                }
            }
        }
        function selectDeselectAppCheckboxes(select/*bool*/) {
            if (select === true) {
                $.each($('.repeaterTable .chkBox'), function (index, value) {
                    var $checkBox = $(this).children().find("input[id*='chkSelectApp']");
                    $($checkBox).attr('checked', 'checked');
                    var immediateSpanContainer = $($checkBox).parent();
                    //var outerSpanContainer = $($checkBox).parent().parent().parent();
                    immediateSpanContainer.addClass("checked");
                    //outerSpanContainer.addClass("checked");
                });
            }
            else {
                $.each($('.repeaterTable .chkBox'), function (index, value) {
                    var $checkBox = $(this).children().find("input[id*='chkSelectApp']");
                    $($checkBox).removeAttr('checked');
                    var immediateSpanContainer = $($checkBox).parent();
                    //var outerSpanContainer = $($checkBox).parent().parent().parent();
                    immediateSpanContainer.removeClass("checked");
                    //outerSpanContainer.removeClass("checked");
                });
            }
        }
        function selectDeselectSelectAllChkBox(sender) {
            var $chkSelectApps = ($(sender).find('input[type="checkbox"]'));
            if ($chkSelectApps) {
                var $thChkBox = $('.repeaterTable .thChkBox').find('input[type="checkbox"]');
                var immediateSpanContainer = $($thChkBox).parent();
                if ($($chkSelectApps).is(":checked")) {
                    var blnIsAllSelected = false;
                    var iCountOfCheckBoxes = $('.repeaterTable .chkBox') ? $('.repeaterTable .chkBox').length : 0;
                    var iCounterOfSelectedChkBox = 0;
                    $.each($('.repeaterTable .chkBox'), function (index, value) {
                        var $checkBox = $(this).children().find("input[id*='chkSelectApp']");
                        if ($($checkBox).is(':checked')) {
                            iCounterOfSelectedChkBox += 1;
                        }
                    });
                    if (iCounterOfSelectedChkBox === iCountOfCheckBoxes) {
                        immediateSpanContainer.addClass("checked");
                        $($thChkBox).attr('checked', 'checked');
                    }
                }
                else {
                    immediateSpanContainer.removeClass("checked");
                    $($thChkBox).removeAttr('checked');
                }
            }
        }
        function setOnClickEventOfRptChkBoxContSpan() {
            var $divCheckerCont = $(".thChkBox") ? $(".thChkBox").find("div.checker") : [];
            if ($divCheckerCont) {
                $.each($($divCheckerCont), function (index, value) {
                    var $spanChkBoxCont = $(this).parent();
                    if ($spanChkBoxCont) {
                        $($spanChkBoxCont).removeAttr("onchange");
                        $($spanChkBoxCont).click(function () {
                            processSelectAllCheckChange($(this));
                        }
                        );
                    }
                });
            }
            var $divBodyCheckerCont = $(".chkBox") ? $(".chkBox").find("div.checker") : [];
            if ($divBodyCheckerCont) {
                $.each($($divBodyCheckerCont), function (index, value) {
                    var $spanBodyChkBoxCont = $(this).parent();
                    if ($spanBodyChkBoxCont) {
                        $($spanBodyChkBoxCont).removeAttr("onchange");
                        $($spanBodyChkBoxCont).click(function () {
                            selectDeselectSelectAllChkBox($(this));
                        }
                        );
                    }
                });
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="updRepeater" runat="server">
        <ContentTemplate>
            <div id="PageCanvasContent">
                <div style="margin-bottom: 5px;" id="divDdlSubAdminCont">
                    <asp:Label ID="Label1" runat="server" Text="Select Sub Admin :" Style="position: relative;
                        top: -12px;"></asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlSubAdminList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSubAdminList_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div id="divRepeater">
                    <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                        <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                            <div>
                                <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>App List</h1>"></asp:Label>
                            </div>
                            <div style="position: relative; top: 10px; right: 15px;">
                            </div>
                            <div style="height: 0px; clear: both">
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlRptTableContainer" Style="height: 350px; overflow: scroll;" runat="server">
                            <asp:Repeater ID="rptAppDetails" runat="server" OnItemCommand="rptAppDetails_ItemCommand"
                                OnItemDataBound="rptAppDetails_ItemDataBound">
                                <HeaderTemplate>
                                    <table class="repeaterTable havingUniformCheckBox">
                                        <thead id="stickyheader">
                                            <tr>
                                                <th id="thChkBox" class="thChkBox">
                                                    <asp:CheckBox ID="chkSelectAllApp" runat="server" onchange="processSelectAllCheckChange(this);"
                                                        ToolTip="Select All" />
                                                </th>
                                                <th>
                                                    <asp:Label ID="lblWFId" runat="server" CssClass="rptSpanHeaderText">Name ( Version )</asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label4" runat="server" CssClass="rptSpanHeaderText"> Description</asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="Label5" runat="server" CssClass="rptSpanHeaderText">  Sub Admin</asp:Label>
                                                </th>
                                                <%--<th>
                                            </th>--%>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tbody>
                                        <tr class="repeaterItem">
                                            <td class="chkBox">
                                                <asp:CheckBox ID="chkSelectApp" runat="server" onchange="selectDeselectSelectAllChkBox(this);" />
                                                <asp:Label ID="lblHidAppName" runat="server" Text='<%# Eval("WF_NAME") %>' Style="display: none;"></asp:Label>
                                                <asp:Label ID="lblSubAdminId" runat="server" Text='<%# Eval("SUBADMIN_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblWFId" runat="server" Text='<%# Eval("WF_ID") %>' Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAppName" runat="server" Text='<%# Eval("WF_NAME") %>'></asp:Label>
                                                <asp:Label ID="lblSpacerLeftBracket" runat="server" Style="margin-left: 2px;" Text="("></asp:Label>
                                                <asp:Label ID="lblVersion" runat="server" Text='<%# Eval("VERSION") %>'></asp:Label>
                                                <asp:Label ID="lblSpacerRightBracket" runat="server" Style="margin-left: 2px;" Text=")"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("WF_DESCRIPTION") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblSubAdmin" runat="server" Text='<%# Eval("FULL_NAME") %>'></asp:Label>
                                            </td>
                                            <%--<td>
                                            <asp:LinkButton ID="lnkChange" Text="Change SubAdmin" runat="server" CommandName="change"
                                                CssClass="repeaterLink" OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                        </td>--%>
                                        </tr>
                                    </tbody>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tbody>
                                        <tr class="repeaterAlternatingItem">
                                            <td class="chkBox">
                                                <asp:CheckBox ID="chkSelectApp" runat="server" onchange="selectDeselectSelectAllChkBox(this);" />
                                                <asp:Label ID="lblHidAppName" runat="server" Text='<%# Eval("WF_NAME") %>' Style="display: none;"></asp:Label>
                                                <asp:Label ID="lblSubAdminId" runat="server" Text='<%# Eval("SUBADMIN_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblWFId" runat="server" Text='<%# Eval("WF_ID") %>' Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAppName" runat="server" Text='<%# Eval("WF_NAME") %>'></asp:Label>
                                                <asp:Label ID="lblSpacerLeftBracket" runat="server" Style="margin-left: 2px;" Text="("></asp:Label>
                                                <asp:Label ID="lblVersion" runat="server" Text='<%# Eval("VERSION") %>'></asp:Label>
                                                <asp:Label ID="lblSpacerRightBracket" runat="server" Style="margin-left: 2px;" Text=")"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("WF_DESCRIPTION") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblSubAdmin" runat="server" Text='<%# Eval("FULL_NAME") %>'></asp:Label>
                                            </td>
                                            <%--<td>
                                            <asp:LinkButton ID="lnkChange" Text="Change SubAdmin" runat="server" CommandName="change"
                                                CssClass="repeaterLink" OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                        </td>--%>
                                        </tr>
                                    </tbody>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </asp:Panel>
                        <div id="divRptTableContainer">
                        </div>
                    </asp:Panel>
                </div>
                <div>
                    <asp:Button ID="btnChangeSubAdmin" runat="server" Text="Change SubAdmin" OnClientClick="processChangeSubAdmin();return false;" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divModalContainer" style="display: none">
        <div id="divChangeSubAdminPopUp" style="display: block">
            <asp:UpdatePanel ID="updModalContainer" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divChangeSAPopUpError">
                    </div>
                    <div id="modalPopUpSubAdminContent" class="modalPopUpDetails" runat="server">
                        <div id="divDetailsContainer">
                            <div class="singleSection" style="float: left; width: 100%;">
                                <fieldset style="padding-top: 5px;">
                                    <section>
                                        <div class="column1">
                                            <asp:Label ID="lblForAppName" runat="server" Text="Name"></asp:Label>
                                        </div>
                                        <div class="column2">
                                            <asp:Label ID="Label2" runat="server" Text=":"></asp:Label>
                                        </div>
                                        <div class="column3">
                                            <asp:Label ID="lblAppName" runat="server"></asp:Label>
                                        </div>
                                    </section>
                                    <section style="padding-top: 20px;">
                                        <div class="column1" style="margin-top: 10px;">
                                            <asp:Label ID="lblForDDlSubAdmin" runat="server" Text="New SubAdmin"></asp:Label>
                                        </div>
                                        <div class="column2" style="margin-top: 8px;">
                                            <asp:Label ID="Label3" runat="server" Text=":"></asp:Label>
                                        </div>
                                        <div class="column3">
                                            <asp:DropDownList ID="ddlPopupSubAdminList" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </section>
                                    <div style="height: 10px; clear: both;">
                                    </div>
                                    <div class="modalPopUpAction">
                                        <div id="divPopUpActionCont" class="popUpActionCont" style="width: 60%">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="aspButton" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnClose" runat="server" Text="Cancel" CssClass="aspButton" OnClientClick="closeModalPopUp('divModalContainer'); return false;" />
                                        </div>
                                        <asp:HiddenField ID="hfdSubAdminId" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="hfdAppId" runat="server"></asp:HiddenField>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {

            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {

            hideWaitModal();
            isCookieCleanUpRequired('true');
            setOnClickEventOfRptChkBoxContSpan();
        }
    </script>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="UiDetail.aspx.cs" Inherits="mFicientCP.UiDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <style type="text/css">
        .appDesignerIEClass
        {
            padding-right: 30px;
            background-position: 120px;
        }
        
        .appDesignerOtherBrowserClass
        {
            padding-right: 30px;
            background-position: 120px;
            padding-left: 20px;
        }
    </style>
    <script type="text/javascript">

        function changeButtonCSSByBrowser() {
            var styleIE = 'padding-right:30px;padding-left: 20px;background-position: 144px;';
            var styleOtherBrowser = 'padding-right: 30px;padding-left: 20px;background-position: 144px;'; //8/11/2012 earlier it was 120
            var appDesignerButton = document.getElementById('<%= lnkFormDesignerButton.ClientID%>');
            if ($.browser.msie) {
                $(appDesignerButton).attr('style', styleIE);
            } else {
                $(appDesignerButton).attr('style', styleOtherBrowser);
            }
        }
        $('<%=lnkFormDesignerButton.ClientID %>').load(function () {
            // run code
            changeButtonCSSByBrowser();
        });
        $(document).ready(function () { changeButtonCSSByBrowser(); });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div style="float: right;">
    </div>
    <div class="clear">
    </div>
    <div id="PageCanvasContent">
        <section>
        <div style="margin:auto;width:500px;">
            <div class="g10" style="margin: auto;">
                <div class="widget" id="Div2" style="margin: 0px auto;">
                    <h3 class="handle">
                        Overview
                    </h3>
                    <div class="customWidgetForDetails">
                        <div>
                            <div class="leftColumn">
                                <asp:Label ID="lblAppLabel" runat="server" Text="Applications"></asp:Label>
                            </div>
                            <div class="middleColumn">
                                <asp:Label ID="lblSeparator1" runat="server" Text=":"></asp:Label>
                            </div>
                            <div class="rightColumn">
                                <asp:Label ID="lblApplicationsCount" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                            <div class="withoutBorder" style="margin-top:8px;">
                                <div class="leftColumn">
                                    <asp:Label ID="lblDatabaseLabel" runat="server" Text="Database Connectors"></asp:Label>
                                </div>
                                <div class="middleColumn">
                                    <asp:Label ID="lblSeparator2" runat="server" Text=":"></asp:Label>
                                </div>
                                <div class="rightColumn">
                                    <asp:Label ID="lblDatabaseConnCount" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div>
                                <div class="leftColumn">
                                    <asp:Label ID="lblWebServiceLabel" runat="server" Text="Web Service Connectors"></asp:Label>
                                </div>
                                <div class="middleColumn">
                                    <asp:Label ID="lblSeparator3" runat="server" Text=":"></asp:Label>
                                </div>
                                <div class="rightColumn">
                                    <asp:Label ID="lblWebServiceConnCount" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="withoutBorder" style="margin-top:8px;">
                                <div class="leftColumn">
                                    <asp:Label ID="lblCommDatabaseLabel" runat="server" Text="Database Commands"></asp:Label>
                                </div>
                                <div class="middleColumn">
                                    <asp:Label ID="lblSeparator4" runat="server" Text=":"></asp:Label>
                                </div>
                                <div class="rightColumn">
                                    <asp:Label ID="lblDBCommCount" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="withoutBorder">
                                <div class="leftColumn">
                                    <asp:Label ID="lblCommWebServiceLabel" runat="server" Text="Web Service Commands"></asp:Label>
                                </div>
                                <div class="middleColumn">
                                    <asp:Label ID="lblSeparator5" runat="server" Text=":"></asp:Label>
                                </div>
                                <div class="rightColumn">
                                    <asp:Label ID="lblWSCommCount" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
            <div class="clear">
            </div>
            <div class="g10">
                <div style="float: right">
                    <asp:LinkButton ID="lnkFormDesignerButton" runat="server" PostBackUrl="~/ide.aspx"
                        OnClientClick="isCookieCleanUpRequired('false');showWaitModal();" class="btn i_arrow_right icon #555 big formDesignerLinkButton normalCase">Go to App Designer</asp:LinkButton>
                </div>
                <div class="clear">
                </div>
            </div>

            </div>
        </section>
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    
</asp:Content>

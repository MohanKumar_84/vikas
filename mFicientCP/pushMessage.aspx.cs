﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Text;
namespace mFicientCP
{
    public partial class pushMessage : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        enum ERROR_IN_PROCESS
        {
            ConnectionError,
            TableDoesNotExist,
            mPluginServerNotFound,
        }
        enum SHOW_ERROR_AS
        {
            Alert = 1001,
            Message = 1002,
        }
        enum POST_BACK_CONFIRM_PROCESS
        {
            SavePushMsgDetail,
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;


            if (!Page.IsPostBack)
            {
                string strAdminId = strUserId;
                if (hfsValue != string.Empty)
                {
                    bindMpluginDropDown();
                    getPushMessageDetails();
                }
                //Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "$(\"input:checkbox\").uniform();", true);
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    // ScriptManager.RegisterStartupScript(Page, typeof(Page), "MakeInputFieldUniform", "$(\"input:not(div.checker input)\").uniform();", true);
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, "");
                    return;

                }
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "testing", " $(\"select\").uniform();", true);
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback2", true, String.Empty);
                if (!String.IsNullOrEmpty(hidNewPwdEntered.Value))
                {
                    fillPasswordOnPostBack();
                }
            }
            formInformationTableHTML();
        }
        #region Form Information Table
        void formInformationTableHTML()
        {

            divInformationTable.InnerHtml = getTableHTML(getRequiredTableInfo());
        }
        DataTable getRequiredTableInfo()
        {
            DataTable dtblColumnsInTable = new DataTable();
            dtblColumnsInTable.Columns.Add("COLUMN_NAME", typeof(string));
            dtblColumnsInTable.Columns.Add("COLUMN_DATA_TYPE", typeof(string));
            dtblColumnsInTable.Columns.Add("DESCRIPTION", typeof(string));

            DataRow row = dtblColumnsInTable.NewRow();
            row["COLUMN_NAME"] = "CATEGORY";
            row["COLUMN_DATA_TYPE"] = "String";
            row["DESCRIPTION"] = "Max 25 char";
            dtblColumnsInTable.Rows.Add(row);

            row = dtblColumnsInTable.NewRow();
            row["COLUMN_NAME"] = "MESSAGE_ID";
            row["COLUMN_DATA_TYPE"] = "String";
            row["DESCRIPTION"] = "Unique ID ( Max 50 char )";
            dtblColumnsInTable.Rows.Add(row);

            row = dtblColumnsInTable.NewRow();
            row["COLUMN_NAME"] = "USER_NAME";
            row["COLUMN_DATA_TYPE"] = "String";
            row["DESCRIPTION"] = "Max 25 char";
            dtblColumnsInTable.Rows.Add(row);

            row = dtblColumnsInTable.NewRow();
            row["COLUMN_NAME"] = "GROUP";
            row["COLUMN_DATA_TYPE"] = "String";
            row["DESCRIPTION"] = "Max char 512";
            dtblColumnsInTable.Rows.Add(row);

            row = dtblColumnsInTable.NewRow();
            row["COLUMN_NAME"] = "SEND_TIME";
            row["COLUMN_DATA_TYPE"] = "Number";
            row["DESCRIPTION"] = "UTC Unix Datetime<br/> 0 - Immediate sending.";
            dtblColumnsInTable.Rows.Add(row);

            row = dtblColumnsInTable.NewRow();
            row["COLUMN_NAME"] = "EXPIRY";
            row["COLUMN_DATA_TYPE"] = "Number";
            row["DESCRIPTION"] = "Value in days";
            dtblColumnsInTable.Rows.Add(row);

            row = dtblColumnsInTable.NewRow();
            row["COLUMN_NAME"] = "STATUS";
            row["COLUMN_DATA_TYPE"] = "Number";
            row["DESCRIPTION"] = "Default 0";
            dtblColumnsInTable.Rows.Add(row);

            row = dtblColumnsInTable.NewRow();
            row["COLUMN_NAME"] = "MESSAGE";
            row["COLUMN_DATA_TYPE"] = "string";
            row["DESCRIPTION"] = "Max 512 char";
            dtblColumnsInTable.Rows.Add(row);

            return dtblColumnsInTable;
        }
        string getTableHTML(DataTable columnDetailsDtbl)
        {
            StringWriter strWriter = new StringWriter();
            HtmlTextWriter htmlWriter = new HtmlTextWriter(strWriter);

            //Create a table
            Table tbl = new Table();
            //Create column header row
            TableHeaderRow thr = new TableHeaderRow();

            TableHeaderCell th = new TableHeaderCell();
            th.Text = "COLUMN NAME";
            thr.Controls.Add(th);

            th = new TableHeaderCell();
            th.Text = "DATA TYPE";
            thr.Controls.Add(th);

            th = new TableHeaderCell();
            th.Text = "DESCRIPTION";
            thr.Controls.Add(th);

            tbl.Controls.Add(thr);
            thr.TableSection = TableRowSection.TableHeader;

            foreach (DataRow row in columnDetailsDtbl.Rows)
            {
                TableRow tr = new TableRow();
                tr.TableSection = TableRowSection.TableBody;

                TableCell td = new TableCell();
                td.Text = Convert.ToString(row["COLUMN_NAME"]); ;
                tr.Controls.Add(td);
                //th = new TableHeaderCell();
                //th.Text = Convert.ToString(row["COLUMN_NAME"]); ;
                //tr.Controls.Add(th);

                td = new TableCell();
                td.Text = Convert.ToString(row["COLUMN_DATA_TYPE"]);
                tr.Controls.Add(td);

                td = new TableCell();
                td = new TableCell();
                td.Text = Convert.ToString(row["DESCRIPTION"]);
                tr.Controls.Add(td);

                tbl.Controls.Add(tr);
            }
            tbl.RenderControl(htmlWriter);
            return strWriter.ToString();

        }
        #endregion
        void fillPasswordOnPostBack()
        {
            if (!String.IsNullOrEmpty(hidNewPwdEntered.Value))
            {
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "FillPasswordAfterPostBack", "fillPasswordEntered();", true);
                Utilities.runPostBackScript("fillPasswordEntered();", this.Page, "FillPasswordAfterPostBack");
            }
        }
        void getPushMessageDetails()
        {
            GetPushMessageDtls objPushMessageDtls = new GetPushMessageDtls(hfsPart3);
            objPushMessageDtls.Process();
            if (objPushMessageDtls.StatusCode == 0)
            {
                if (objPushMessageDtls.ResultTable != null &&
                    objPushMessageDtls.ResultTable.Rows.Count > 0)
                {
                    fillDataInControls(objPushMessageDtls);
                }
            }
        }
        void fillDataInControls(GetPushMessageDtls pushMsgDtls)
        {
            ddlDBType.SelectedIndex = ddlDBType.Items.IndexOf(ddlDBType.Items.FindByValue(pushMsgDtls.DatabaseType));
            txtConnectionName.Text = pushMsgDtls.ConnectionName;
            txtHostName.Text = pushMsgDtls.HostName;
            txtDatabaseName.Text = pushMsgDtls.DbName;
            txtUserName.Text = pushMsgDtls.UserId;
            hidPwdSvdBefore.Value = pushMsgDtls.Password;
            txtPassword.Text = pushMsgDtls.Password;
            txtTableName.Text = pushMsgDtls.TableName;
            if (!String.IsNullOrEmpty(pushMsgDtls.MpluginAgntName))
            {
                ddlMplugin.SelectedIndex = ddlMplugin.Items.IndexOf(ddlMplugin.Items.FindByText(pushMsgDtls.MpluginAgntName));
            }
            else
            {
                ddlMplugin.SelectedIndex = ddlMplugin.Items.IndexOf(ddlMplugin.Items.FindByText("None"));
            }
            Utilities.runPageStartUpScript(this.Page, "SaveInitialPasswordInHidField", "processFillPasswordDuringLoad();rememberPasswordEntered();");
        }
        protected void btnSaveSettings_Click(object sender, EventArgs e)
        {
            try
            {
                string strErrorInValidation = validateForm();
                if (!String.IsNullOrEmpty(strErrorInValidation))
                {
                    throw new MficientException(((int)SHOW_ERROR_AS.Alert).ToString(), new Exception(strErrorInValidation));
                }
                processSaveSettings();
            }
            catch (MficientException ex)
            {
                if (ex.Message.ToLower() == (((int)SHOW_ERROR_AS.Alert).ToString().ToLower()))
                {
                    Utilities.showAlert(ex.InnerException.Message,
                        "divDatabasePullError", updContainer);
                }
                else
                {
                    if (ex.Message == ((int)ERROR_IN_PROCESS.mPluginServerNotFound).ToString())
                    {
                        Utilities.showMessage(this.getErrorMsgText(ex.Message),
                            updContainer, DIALOG_TYPE.Info);
                    }
                    else
                    {
                        Utilities.showMessage(this.getErrorMsgText(ex.Message),
                            updContainer, DIALOG_TYPE.Error);
                    }
                    //lblConnectionStatus.Text = "";
                }
            }
            catch (Exception)
            {
                Utilities.showMessage("Internal server error.",
                    updContainer, DIALOG_TYPE.Error);

            }
        }
        protected void btnTestConnection_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtblMetaDataOfTbl;
                bool blnUseMPlugin = false;
                if (ddlMplugin.SelectedValue == "-1")
                {
                    blnUseMPlugin = false;
                }
                else
                {
                    blnUseMPlugin = true;
                }

                testConnection(false, blnUseMPlugin, out dtblMetaDataOfTbl);
                //lblConnectionStatus.Text = "Connection was successfull";
                //dtblMetaDataOfTbl == null already handled.
                bool isMetaDataValid = checkIfReqColmnNameExists(dtblMetaDataOfTbl);
                if (isMetaDataValid)
                {
                    lblConfirmationMessage.Text = "Connection and Table name provided is correct.Would you like to save the settings.";
                    btnConfirmYes.CommandArgument = ((int)POST_BACK_CONFIRM_PROCESS.SavePushMsgDetail).ToString();
                    updConfirmation.Update();
                    Utilities.showModalPopup("divPostBackConfirm",
                        updContainer, "Please confirm",
                        "300", false,
                        false, DIALOG_TYPE.Info,
                        "divPostBackConfirm");
                }
                else
                {
                    Utilities.showMessage("Connection was successfull.<br/> But the columns in table name entered does not match that of the table which is required.<br/>Please see table information below.",
                    updContainer, DIALOG_TYPE.Error);
                }
            }
            catch (MficientException ex)
            {
                if (ex.Message.ToLower() == (((int)SHOW_ERROR_AS.Alert).ToString().ToLower()))
                {
                    Utilities.showAlert(ex.InnerException.Message,
                        "divDatabasePullError", updContainer);
                }
                else
                {
                    if (ex.Message == ((int)ERROR_IN_PROCESS.mPluginServerNotFound).ToString())
                    {
                        Utilities.showMessage(this.getErrorMsgText(ex.Message),
                            updContainer, DIALOG_TYPE.Info);
                    }
                    else
                    {
                        Utilities.showMessage(this.getErrorMsgText(ex.Message),
                            updContainer, DIALOG_TYPE.Error);
                    }
                    //lblConnectionStatus.Text = "";
                }
            }
            catch (Exception)
            {
                Utilities.showMessage("Internal server error.",
                    updContainer, DIALOG_TYPE.Error);
            }
        }
        bool checkIfReqColmnNameExists(DataTable metaDataOfTbl)
        {
            if (metaDataOfTbl == null) throw new ArgumentNullException();
            bool isValid = true;
            DataTable dtblRequiredTableInfo = this.getRequiredTableInfo();
            foreach (DataRow row in dtblRequiredTableInfo.Rows)
            {
                string filter = String.Format("COLUMN_NAME LIKE '{0}'", Convert.ToString(row["COLUMN_NAME"]));
                DataRow[] rows = metaDataOfTbl.Select(filter);
                if (rows.Length > 0)
                {
                    continue;
                }
                else
                {
                    isValid = false;
                    break;
                }
            }
            return isValid;
        }
        /// <summary>
        /// Testing the connection when saving.when saving we doesn't need to bindTableNamesDropDown
        /// </summary>
        /// <param name="isSaving"></param>
        /// <returns></returns>
        void testConnection(bool isSaving,
            bool testBymPlugin,
            out DataTable tblMetaData)
        {
            tblMetaData = null;
            string strErrorInValidation = validateForm();
            if (!String.IsNullOrEmpty(strErrorInValidation))
            {
                throw new MficientException(((int)SHOW_ERROR_AS.Alert).ToString(), new Exception(strErrorInValidation));
            }
            string strConnectionString = getConnectionStringByDbType(
                     txtHostName.Text,
                     txtDatabaseName.Text,
                     txtUserName.Text,
                     txtPassword.Text, 0);
            if (String.IsNullOrEmpty(strConnectionString))
                throw new Exception("Internal server error");

            DataTable dtblMetaDataOfTbl = null;
            DatabaseType dbTypeSelected = this.getDbTypeByValueInDdlDbType();
            string strSchemaName = String.Empty;
            switch (dbTypeSelected)
            {
                case DatabaseType.MSSQL:
                    break;
                case DatabaseType.ORACLE:
                    break;
                case DatabaseType.MYSQL:
                    break;
                case DatabaseType.POSTGRESQL:
                    strSchemaName = txtTableName.Text.Substring(0, txtTableName.Text.IndexOf('.'));
                    break;
                case DatabaseType.ODBCDSN:
                    break;

            }
            dtblMetaDataOfTbl = getMetaDataOfTbl(testBymPlugin, hfsPart3,
                    txtDatabaseName.Text, txtTableName.Text,
                    strConnectionString, dbTypeSelected,
                    strSchemaName,
                    ddlMplugin.SelectedValue == "-1" ? String.Empty : ddlMplugin.SelectedItem.Text);

            if (dtblMetaDataOfTbl != null)
            {
                if (dtblMetaDataOfTbl.Rows.Count == 0)
                    throw new MficientException(((int)ERROR_IN_PROCESS.TableDoesNotExist).ToString());
                tblMetaData = dtblMetaDataOfTbl;
            }
            else
                throw new MficientException(((int)ERROR_IN_PROCESS.ConnectionError).ToString());
        }
        string getConnectionStringByDbType(string hostName,
            string databaseName,
            string userId,
            string password,
            uint timeOut
            )
        {
            if (timeOut < 0) throw new Exception("Internal server error.");
            string strConnectionString = String.Empty;
            try
            {
                switch (getDbTypeByValueInDdlDbType())
                {
                    case DatabaseType.MSSQL:
                        strConnectionString = MSSqlClient.getConnectionString(hostName,
                             databaseName, userId,
                             password, Convert.ToInt32(timeOut));
                        break;
                    case DatabaseType.ORACLE:
                        break;
                    case DatabaseType.MYSQL:
                        strConnectionString = MySQLClient.getConnectionString(hostName,
                             databaseName, userId,
                             password, timeOut);
                        break;
                    case DatabaseType.POSTGRESQL:
                        strConnectionString = PostgreSqlClient.getConnectionString(hostName,
                             databaseName, userId,
                             password, timeOut);
                        break;
                    case DatabaseType.ODBCDSN:
                        break;
                }
            }
            catch
            {
                throw new MficientException("Database type selected not supported");
            }
            return strConnectionString;

        }
        DatabaseType getDbTypeByValueInDdlDbType()
        {
            return (DatabaseType)Enum.Parse(typeof(DatabaseType), ddlDBType.SelectedValue);
        }
        DataTable getMetaDataOfTbl(bool useMplugin,
            string companyId,
            string databaseName,
            string tableName,
            string connectionString,
            DatabaseType dbType,
            string schemaName,
            string mPluginAgnt)
        {
            string strErrorMsg = String.Empty;
            DataSet dsMetaData = null;
            if (useMplugin)
            {
                string strQueryToRun = String.Empty;
                List<QueryParameters> lstParameters = new List<QueryParameters>();
                QueryParameters objParam = new QueryParameters();
                switch (dbType)
                {
                    case DatabaseType.MSSQL:
                        strQueryToRun = MSSqlClient.getMetaDataQuery(MetadataType.COLUMNS);
                        objParam.para = "@TABLE_NAME";
                        objParam.val = tableName;
                        lstParameters.Add(objParam);
                        break;
                    case DatabaseType.ORACLE:
                        break;
                    case DatabaseType.MYSQL:
                        strQueryToRun = MySQLClient.getMetaDataQuery(MetadataType.COLUMNS,
                            databaseName);
                        objParam.para = "@TABLE_NAME";
                        objParam.val = tableName;
                        lstParameters.Add(objParam);
                        break;
                    case DatabaseType.POSTGRESQL:
                        strQueryToRun = PostgreSqlClient.getMetaDataQueryForTable();
                        objParam = new QueryParameters();
                        objParam.para = "@TableName";
                        objParam.val = tableName;
                        lstParameters.Add(objParam);
                        objParam = new QueryParameters();
                        objParam.para = "@ShemaName";
                        objParam.val = schemaName;
                        lstParameters.Add(objParam);
                        break;
                    case DatabaseType.ODBCDSN:
                        break;

                }
                GetMetaDataForPushMsgUsingMPlugin objGetMetaDataUsingMplugin =
                    new GetMetaDataForPushMsgUsingMPlugin(companyId,
                        connectionString,
                        strQueryToRun,
                        (int)dbType,
                        mPluginAgnt,
                        lstParameters);
                objGetMetaDataUsingMplugin.Process();
                if (objGetMetaDataUsingMplugin.StatusCode != 0)
                {
                    strErrorMsg = objGetMetaDataUsingMplugin.StatusDescription;
                }
                else
                {
                    dsMetaData = objGetMetaDataUsingMplugin.MetaDataDS;
                }

            }
            else
            {
                switch (dbType)
                {
                    case DatabaseType.MSSQL:
                        dsMetaData = MSSqlClient.getMetaDataByType(MetadataType.COLUMNS,
                    connectionString, tableName, out strErrorMsg);
                        break;
                    case DatabaseType.ORACLE:
                        break;
                    case DatabaseType.MYSQL:
                        dsMetaData = MySQLClient.getMetaDataByType(MetadataType.COLUMNS,
                    connectionString, databaseName, tableName, out strErrorMsg);
                        break;
                    case DatabaseType.POSTGRESQL:
                        dsMetaData = PostgreSqlClient.getMetaDataByType(MetadataType.COLUMNS,
                    connectionString, databaseName, tableName, out strErrorMsg);
                        break;
                    case DatabaseType.ODBCDSN:
                        break;

                }

            }
            if (!String.IsNullOrEmpty(strErrorMsg))
            {
                if (strErrorMsg.Contains("mplugin server not found."))
                {
                    throw new MficientException(((int)ERROR_IN_PROCESS.mPluginServerNotFound).ToString());
                }
                else
                {
                    throw new MficientException(strErrorMsg);
                }
            }
            if (dsMetaData == null) throw new Exception("Internal server error.");
            if (dsMetaData.Tables.Count == 0) throw new MficientException(((int)ERROR_IN_PROCESS.TableDoesNotExist).ToString());
            return dsMetaData.Tables[0];
        }


        public string validateForm()
        {

            string strMessage = "";
            if (!Utilities.IsValidString(txtConnectionName.Text, true, true, true, " ~!@#$%^&*()<>,.{}:|\\,./-=_+`", 1, 50, false, false))
            {
                strMessage += "Please enter valid connection name." + "<br />";
            }
            if (!Utilities.IsValidString(txtHostName.Text, true, true, true, " ~!@#$%^&*()<>,.{}:|\\,./-=_+`", 1, 50, false, false))
            {
                strMessage += "Please enter valid host name." + "<br />";
            }
            if (!Utilities.IsValidString(txtDatabaseName.Text, true, true, true, " ~!@#$%^&*()<>,.{}:|\\,./-=_+`", 1, 50, false, false))
            {
                strMessage += "Please enter valid database name." + "<br />";
            }
            if (!Utilities.IsValidString(txtUserName.Text, true, true, true, " ~!@#$%^&*()<>,.{}:|\\,./-=_+`", 1, 50, false, false))
            {
                strMessage += "Please enter valid user name." + "<br />";
            }
            if (!Utilities.IsValidString(txtPassword.Text, true, true, true, " ~!@#$%^&*()<>,.{}:|\\,./-=_+`", 1, 50, false, false))
            {
                strMessage += "Please enter valid password." + "<br />";
            }
            return strMessage;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="System.Exception">Thrown when there is some internal error</exception>
        void bindMpluginDropDown()
        {
            mPluginAgents objMpluginAgent = new mPluginAgents();
            DataSet dsAgents = objMpluginAgent.GetMpluginAgents(hfsPart3);
            if (dsAgents != null)
            {
                ddlMplugin.DataSource = dsAgents;
                ddlMplugin.DataTextField = "MP_AGENT_NAME";
                ddlMplugin.DataValueField = "MP_AGENT_ID";
                ddlMplugin.DataBind();
                ddlMplugin.Items.Insert(0,
                    new ListItem("None", "-1"));
            }
        }
        string getErrorMsgText(string error)
        {
            string strError = String.Empty;
            int iErrorCode;
            if (int.TryParse(error, out iErrorCode))
            {
                if (Enum.IsDefined(typeof(ERROR_IN_PROCESS), iErrorCode))
                {
                    ERROR_IN_PROCESS eError = (ERROR_IN_PROCESS)Enum.Parse(typeof(ERROR_IN_PROCESS), iErrorCode.ToString());
                    switch (eError)
                    {
                        case ERROR_IN_PROCESS.ConnectionError:
                            strError = "There was some error in connection.";
                            break;
                        case ERROR_IN_PROCESS.TableDoesNotExist:
                            strError = "Table not found.";
                            break;
                        case ERROR_IN_PROCESS.mPluginServerNotFound:
                            strError = "mPlugin server not found.";
                            break;

                    }
                }
            }
            else strError = error;
            return strError;
        }
        protected void btnConfirmYes_Click(object sender, CommandEventArgs e)
        {
            try
            {
                switch (getProcess(((string)e.CommandArgument)))
                {
                    case POST_BACK_CONFIRM_PROCESS.SavePushMsgDetail:
                        processSaveSettings();
                        break;
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error.",
                    updConfirmation, DIALOG_TYPE.Error);
            }
        }
        POST_BACK_CONFIRM_PROCESS getProcess(string processCode)
        {
            return (POST_BACK_CONFIRM_PROCESS)Enum.Parse(typeof(POST_BACK_CONFIRM_PROCESS), processCode);
        }
        void processSaveSettings()
        {
            try
            {
                SavePushMessageDtls objSavePushMsgDtls =
                                    new SavePushMessageDtls(ddlDBType.SelectedValue, hfsPart3,
                                                          txtConnectionName.Text, txtHostName.Text,
                                                           txtDatabaseName.Text, txtUserName.Text,
                                                           txtPassword.Text, txtTableName.Text,
                                                          ddlMplugin.SelectedValue == "-1" ? String.Empty : ddlMplugin.SelectedItem.Text,
                                                           txtAdditionalInfo.Text);
                objSavePushMsgDtls.Process();
                //Utilities.closeModalPopUp("divPostBackConfirm", updConfirmation, "ClosePostBackConfirm", "divPostBackConfirm");
                if (objSavePushMsgDtls.StatusCode == 0)
                {
                    Utilities.showMessage("Push message details saved successfully", updContainer, DIALOG_TYPE.Info);
                }
                else
                {
                    Utilities.showMessage("Internal Error.Please try again", updContainer, DIALOG_TYPE.Error);
                }

            }
            catch
            {
                Utilities.showMessage("Internal Error.Please try again", updContainer, DIALOG_TYPE.Error);
                // Utilities.closeModalPopUp("divPostBackConfirm", updContainer, "ClosePostBackConfirm", "divPostBackConfirm");
            }
        }
    }
}
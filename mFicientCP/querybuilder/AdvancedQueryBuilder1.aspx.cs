﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ActiveDatabaseSoftware;
using ActiveDatabaseSoftware.ActiveQueryBuilder.Web.Control;
using ActiveDatabaseSoftware.ActiveQueryBuilder;
using ActiveDatabaseSoftware.ActiveQueryBuilder.Web.Server;
using System.Security;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Oracle.ManagedDataAccess.Client;
using Npgsql;
using System.Data;
using System.Xml;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace mFicientCP
{
    public partial class AdvancedQueryBuilder1 : System.Web.UI.Page
    {
        public Boolean IsSessionValid = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);

                    hfbid.Value = Convert.ToString((string)HttpContext.Current.Items["hfbid"]);
                    if (HttpContext.Current.Items.Contains("hdfInsertQueryPara"))
                    {
                        hfBackSqlPara.Value = Convert.ToString((string)HttpContext.Current.Items["hdfInsertQueryPara"]);
                    }
                    if (HttpContext.Current.Items.Contains("hdfOutCol"))
                    {
                        hfBackOutCol.Value = Convert.ToString((string)HttpContext.Current.Items["hdfOutCol"]);
                    }
                    if (HttpContext.Current.Items.Contains("hdfOutCol"))
                    {
                        hdfMannualParam.Value = Convert.ToString((string)HttpContext.Current.Items["hdfAutoboxMannulPara"]);
                    }
                }
                else
                {
                    if (Page.PreviousPage != null)
                    {
                        ContentPlaceHolder cphPageContent = (ContentPlaceHolder)Page.PreviousPage.Form.FindControl("MainCanvas");
                        hfs.Value = ((HiddenField)cphPageContent.FindControl("hfs")).Value;
                        hfbid.Value = ((HiddenField)cphPageContent.FindControl("hfbid")).Value;
                        hfBackSqlPara.Value = ((HiddenField)cphPageContent.FindControl("hdfInsertQueryPara")).Value;
                        hfBackOutCol.Value = ((HiddenField)cphPageContent.FindControl("hdfOutCol")).Value;
                        hfBackSql.Value = ((TextBox)cphPageContent.FindControl("txtDbCommand_SqlQuery")).Text;
                        hdfBackInputParam.Value = ((HiddenField)cphPageContent.FindControl("hdfDbCmdPara")).Value;
                        hdfIsMplugIn.Value = ((HiddenField)cphPageContent.FindControl("hdfIsMplugIn")).Value;
                        hdfMannualParam.Value = ((HiddenField)cphPageContent.FindControl("hdfAutoboxMannulPara")).Value;
                        hidWsAuthencationMeta.Value = ((HiddenField)cphPageContent.FindControl("hidWsAuthencationMeta")).Value;
                        string urlName = Request.UrlReferrer.ToString();
                    }
                }
                if (hfs.Value.Length == 0)
                {
                    //Response.Redirect("~/Default.aspx");
                } 
                GetDatabaseConnection objGetDatabaseConnection = new GetDatabaseConnection(false, Utilities.DecryptString(hfs.Value).Split(',')[5], "", Utilities.DecryptString(hfs.Value).Split(',')[3]);
                JObject crdtial = getCradential(Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["CREDENTIAL_PROPERTY"]));
                if (hdfIsMplugIn.Value == "1")
                {
                    if (objGetDatabaseConnection.ResultTable != null)
                    {
                        hdfDbType.Value = Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_TYPE"]);
                        DataSet ds = GetMetaData(Utilities.DecryptString(hfs.Value).Split(',')[3], 
                            Utilities.DecryptString(hfs.Value).Split(',')[5], "0", ((int)Different_Type_Meta.ALL).ToString(),
                            Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["MPLUGIN_AGENT"]),
                            Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_TYPE"]).Trim(), 
                            Convert.ToString(crdtial["uid"]),
                            Convert.ToString(crdtial["pwd"]));
                        MetaDataContainerDynamically(AesEncryption.AESDecrypt(Utilities.DecryptString(hfs.Value).Split(',')[3], Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_NAME"])), ds, Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_TYPE"]));
                    }
                }
                else
                {
                   if (objGetDatabaseConnection.ResultTable != null)
                    {
                        string strConnectionString = "";
                       DataRow dr=objGetDatabaseConnection.ResultTable.Rows[0];
                       hdfDbType.Value = Convert.ToString(dr["DATABASE_TYPE"]); 
                       switch (Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_TYPE"]))
                       {
                           case "1":
                               strConnectionString = "Server=" + objGetDatabaseConnection.ResultTable.Rows[0]["HOST_NAME"] + ";Initial Catalog=" + AesEncryption.AESDecrypt(Utilities.DecryptString(hfs.Value).Split(',')[3], Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_NAME"])) + ";User Id=" + Convert.ToString(crdtial["uid"]) + ";Password=" + Convert.ToString(crdtial["pwd"]) + ";" + (Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["TIME_OUT"]).Trim().Length > 0 ? "Timeout=" + Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["TIME_OUT"]) + ";" : "") + objGetDatabaseConnection.ResultTable.Rows[0]["ADDITIONAL_STRING"];
                               IntQueryBuilderControl(strConnectionString);
                               break;
                           case "2":
                               //strConnectionString = "Server=" + objGetDatabaseConnection.ResultTable.Rows[0]["HOST_NAME"] + ";Data Source=" + AesEncryption.AESDecrypt(Utilities.DecryptString(hfs.Value).Split(',')[3], Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_NAME"])) + ";User Id=" + Convert.ToString(crdtial["uid"]) + ";Password=" + Convert.ToString(crdtial["pwd"]) + ";" + (Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["ADDITIONAL_STRING"]).Trim().Length > 0 ? Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["ADDITIONAL_STRING"]) : "");
                                //Use System.Data.OracleClient as database provider
                                Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder builder = new Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder();
                                builder.DataSource = objGetDatabaseConnection.ResultTable.Rows[0]["HOST_NAME"] + "/" + AesEncryption.AESDecrypt(Utilities.DecryptString(hfs.Value).Split(',')[3], Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_NAME"]));
                                builder.UserID = Convert.ToString(crdtial["uid"]);
                                builder.Password = Convert.ToString(crdtial["pwd"]);
                                strConnectionString = builder.ToString();
                                string AdditionalString = Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["ADDITIONAL_STRING"]).Trim();
                                if (AdditionalString.Length > 0)
                                    strConnectionString = strConnectionString + AdditionalString + ";";
                               IntQueryBuilderControlOracle(strConnectionString);
                               break;
                           case "3":
                               strConnectionString = "SERVER=" + objGetDatabaseConnection.ResultTable.Rows[0]["HOST_NAME"] + ";DATABASE=" + AesEncryption.AESDecrypt(Utilities.DecryptString(hfs.Value).Split(',')[3], Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_NAME"])) + ";UID=" + Convert.ToString(crdtial["uid"]) + ";PASSWORD=" + Convert.ToString(crdtial["pwd"]) + ";" + (Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["ADDITIONAL_STRING"]).Trim().Length > 0 ? Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["ADDITIONAL_STRING"]) : "");
                               IntQueryBuilderControlMysql(strConnectionString);
                               break;
                           case "4":
                               strConnectionString = "Server=" + objGetDatabaseConnection.ResultTable.Rows[0]["HOST_NAME"] + ";"
                                       + "Database=" + AesEncryption.AESEncrypt(Utilities.DecryptString(hfs.Value).Split(',')[3], Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["DATABASE_NAME"])) + ";"
                                           + "User Id=" + Convert.ToString(crdtial["uid"]) + ";"
                                           + "Password=" + Convert.ToString(crdtial["pwd"]) + ";" + (Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["ADDITIONAL_STRING"]).Trim().Length > 0 ? Convert.ToString(objGetDatabaseConnection.ResultTable.Rows[0]["ADDITIONAL_STRING"]) + ";" : "");

                               IntQueryBuilderControlPostgreSql(strConnectionString);
                               break;
                       }
                    }
                }
            }
            else
            {
            }
            if(hdfDbType.Value=="4") ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowSchemaDropDown(false);", true);
            else ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ShowSchemaDropDown(true);", true);
        }
        
        private void IntQueryBuilderControl(string _ConnectionString)
        {
            try
            {
                QueryBuilderControl qbc = (QueryBuilderControl)queryBuilderControl.FindControl("qbc");
                QueryBuilder qb = qbc.QueryBuilder;

                qb.OfflineMode = false;
                MSSQLSyntaxProvider syntaxProvider = new MSSQLSyntaxProvider();
                qb.SyntaxProvider = syntaxProvider;
                SqlConnection connection = new SqlConnection();
                connection.ConnectionString = _ConnectionString;
                MSSQLMetadataProvider metadataProvider = new MSSQLMetadataProvider();
                metadataProvider.Connection = connection;
                qb.MetadataProvider = metadataProvider;
                qb.RefreshMetadata();
                qb.Clear();
            }
            catch
            {
                //ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "CtrlClick();", true);
            }
        }

        private void IntQueryBuilderControlMysql(string _ConnectionString)
        {
            try
            {
                QueryBuilderControl qbc = (QueryBuilderControl)queryBuilderControl.FindControl("qbc");
                QueryBuilder qb = qbc.QueryBuilder;

                MySQLSyntaxProvider syntaxProvider = new MySQLSyntaxProvider();
                qb.SyntaxProvider = syntaxProvider;
                MySqlConnection connection = new MySqlConnection();
                connection.ConnectionString = _ConnectionString;
                MySQLMetadataProvider metadataProvider = new MySQLMetadataProvider();
                metadataProvider.Connection = connection;
                qb.MetadataProvider = metadataProvider;
                qb.RefreshMetadata();
                qb.Clear();
            }
            catch
            {
            }
        }

        private void IntQueryBuilderControlOracle(string _ConnectionString)
        {
            QueryBuilderControl qbc = (QueryBuilderControl)queryBuilderControl.FindControl("qbc");
            QueryBuilder qb = qbc.QueryBuilder;


            qb.MetadataContainer.Clear();

            MetadataContainer Container = qb.MetadataContainer;

            GetTablesDetails objGetTablesDetails = new GetTablesDetails(_ConnectionString, DatabaseType.ORACLE, "");
            DataSet _DataSet = objGetTablesDetails.getOracleDatabseTableAndcolumn();
            if (_DataSet != null)
            {
                DataTable dtTableDetails = _DataSet.Tables[0];
                DataTable dtColumnDetails = _DataSet.Tables[1];

                foreach (DataRow dr in dtTableDetails.Rows)
                {
                    if (Convert.ToString(dr["TABLE_NAME"]).Length > 0) Container.AddTable(Convert.ToString(dr["TABLE_NAME"]), false);
                }
                MetadataField mf;
                foreach (MetadataObject item in Container.Items)
                {
                    foreach (DataRow dr in dtTableDetails.Rows)
                    {
                        if (item.FullName.QualifiedName == Convert.ToString(dr["TABLE_NAME"]))
                        {
                            DataRow[] drArray = dtColumnDetails.Select("table_name='" + Convert.ToString(dr["TABLE_NAME"] + "'"));
                            foreach (DataRow drCols in drArray)
                            {
                                mf = item.Fields.AddField(Convert.ToString(drCols["column_name"]));
                                mf.FieldTypeName = Convert.ToString(drCols["column_type"]);
                                mf.PrimaryKey = false;
                            }
                            break;
                        }
                    }
                }
            }
            qb.OfflineMode = true;
            qb.Clear();
            qb.Refresh();
        }

        private void IntQueryBuilderControlPostgreSql(string _ConnectionString)
        {
            try
            {
                QueryBuilderControl qbc = (QueryBuilderControl)queryBuilderControl.FindControl("qbc");
                QueryBuilder qb = qbc.QueryBuilder;

                PostgreSQLSyntaxProvider syntaxProvider = new PostgreSQLSyntaxProvider();
                qb.SyntaxProvider = syntaxProvider;

                NpgsqlConnection connection = new NpgsqlConnection();
                connection.ConnectionString = _ConnectionString;
                PostgreSQLMetadataProvider metadataProvider = new PostgreSQLMetadataProvider();
                metadataProvider.Connection = connection;
                qb.MetadataProvider = metadataProvider;
                qb.RefreshMetadata();
                qb.Clear();
            }
            catch
            {
            }
        }

        protected void Page_UnLoad(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "CtrlUnLoad();", true);
        }
        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            QueryBuilderControl control = (QueryBuilderControl)queryBuilderControl.FindControl("qbc");
            QueryBuilder queryBuilder = control.QueryBuilder;
            PlainTextSQLBuilder sqlBuilder = control.PlainTextSQLBuilder;

            QueryBuilder queryBuilder2 = SessionStore.Current.QueryBuilder;
            PlainTextSQLBuilder sqlBuilder2 = SessionStore.Current.PlainTextSQLBuilder;

            if (queryBuilder.SQL.Trim().Length == 0)
            {
                hfSql.Value = hfBackSql.Value;
                hfSqlPara.Value = hfBackSqlPara.Value;
                hfOutCol.Value = hfBackOutCol.Value;
                hdfInputParam.Value = hdfBackInputParam.Value;
                btnBackToSimpleQueryBuilder.PostBackUrl = "~/IdeDataObject.aspx";
                ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "btnClicked();", true);
                return;
            }
            // Common part:
            string plainSQL = queryBuilder.SQL; // unformatted text
            string formattedSQL = sqlBuilder.SQL; // formatted text
            if (queryBuilder.Parameters.Count > 0) hfSqlPara.Value = "";
            for (int i = 0; i < queryBuilder.Parameters.Count; i++)
            {
                if (hfSqlPara.Value.Length > 0)
                {
                    hfSqlPara.Value += ",";
                }
                hfSqlPara.Value += queryBuilder.Parameters[i].Name;
            }
            hfSql.Value = queryBuilder.SQL;
            hfOutCol.Value = "";
            btnBackToSimpleQueryBuilder.PostBackUrl = "~/IdeDataObject.aspx";
            ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "btnClicked();", true);
        }

        private void MetaDataContainerDynamically(string _Database,DataSet _DataSet,string _Type)
        {
            _Database = AesEncryption.AESEncrypt(Utilities.DecryptString(hfs.Value).Split(',')[3], _Database);
            if (_DataSet == null) return;
            QueryBuilderControl qbc = (QueryBuilderControl)queryBuilderControl.FindControl("qbc");
            QueryBuilder qb = qbc.QueryBuilder;

            switch (_Type)
            {
                case "1":
                    MSSQLSyntaxProvider spMssql = new MSSQLSyntaxProvider();
                    qb.SyntaxProvider = spMssql;
                    break;
                case "2":
                    OracleSyntaxProvider spOracle = new OracleSyntaxProvider();
                    qb.SyntaxProvider = spOracle;
                    break;
                case "3":
                    MySQLSyntaxProvider spMysql = new MySQLSyntaxProvider();
                    qb.SyntaxProvider = spMysql;
                    break;
                case "4": 
                    PostgreSQLSyntaxProvider spPostGre = new PostgreSQLSyntaxProvider();
                    qb.SyntaxProvider = spPostGre;
                    break;
            }

            qb.MetadataContainer.Clear();
            MetadataContainer Container = qb.MetadataContainer;
            DataTable dtTableDetails = _DataSet.Tables[0];
            DataTable dtColumnDetails = _DataSet.Tables[1];

            switch (_Type)
            {
                case "4":
                    foreach (DataRow dr in dtTableDetails.Rows)
                    {
                        if (Convert.ToString(dr["TABLE_NAME"]).Length > 0)
                        {
                            Container.AddTable("\"" + Convert.ToString(dr["TABLE_SCHEMA"])+"\"", "\"" + Convert.ToString(dr["TABLE_NAME"]) + "\"", false, "\"" + _Database + "\"");
                        }
                    }
                    break;
                default:
                    foreach (DataRow dr in dtTableDetails.Rows)
                    {
                        if (Convert.ToString(dr["TABLE_NAME"]).Length > 0) Container.AddTable(Convert.ToString(dr["TABLE_NAME"]), false);
                    }
                    break;
            }
            MetadataField mf;
            switch (_Type)
            {
                case "4":
                    foreach (MetadataObject item in Container.Items)
                    {
                        foreach (DataRow dr in dtTableDetails.Rows)
                        {
                            if (item.FullName.QualifiedName == "\"" + _Database + "\"" + "." + (Convert.ToString(dr["TABLE_SCHEMA"]).ToUpper() =="PUBLIC" ? Convert.ToString(dr["TABLE_SCHEMA"]):"\"" + Convert.ToString(dr["TABLE_SCHEMA"])+ "\"" ) + "." + "\"" + Convert.ToString(dr["TABLE_NAME"]) + "\"")
                            {
                                DataRow[] drArray = dtColumnDetails.Select("table_name='" + Convert.ToString(dr["TABLE_NAME"]) + "' AND table_schema='" + Convert.ToString(dr["TABLE_SCHEMA"]) + "'");
                                foreach (DataRow drCols in drArray)
                                {
                                    mf = item.Fields.AddField("\"" + Convert.ToString(drCols["column_name"]) + "\"");
                                    mf.FieldTypeName = Convert.ToString(drCols["column_type"]);
                                    mf.PrimaryKey = false;
                                }
                                break;
                            }
                        }
                    }
                    break;
                default:
                    foreach (MetadataObject item in Container.Items)
                    {
                        foreach (DataRow dr in dtTableDetails.Rows)
                        {
                            if (item.FullName.QualifiedName == Convert.ToString(dr["TABLE_NAME"]))
                            {
                                DataRow[] drArray = dtColumnDetails.Select("table_name='" + Convert.ToString(dr["TABLE_NAME"] + "'"));
                                foreach (DataRow drCols in drArray)
                                {
                                    mf = item.Fields.AddField(Convert.ToString(drCols["column_name"]));
                                    mf.FieldTypeName = Convert.ToString(drCols["column_type"]);
                                    mf.PrimaryKey = false;
                                }
                                break;
                            }
                        }
                    }
                    break;
            }
            qb.OfflineMode = true;
            
            qb.Clear();
            qb.Refresh();
        }

        JObject getCradential(string _credentialproperty)
        {
            if (hidWsAuthencationMeta.Value != "")
            {
                JArray crds = JArray.Parse(hidWsAuthencationMeta.Value);
                //JObject crdtial = new JObject();
                foreach (JObject crd in crds)
                    if (Convert.ToString(crd["tag"]) == _credentialproperty)
                        return crd;
            }
            return null;
        }


        private DataSet GetMetaData(string _CompanyId, string _ConnectorId, string _MetaDataType, string _TableName, string _AgentName, string _DbType, string _userName, string _password)
        {
            string strRqst, strUrl = "";
            string strTicks = Convert.ToString(DateTime.Now.Ticks);
            string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);
            mPluginAgents objAgent = new mPluginAgents();
            strRqst = "{\"req\":{\"rid\":\"" + strRqtId + "\",\"eid\":\"" + _CompanyId + "\",\"agtid\":\"" + _AgentName + "\",\"agtpwd\":\"" + objAgent.GetMpluginAgentPassword(_CompanyId, _AgentName) + "\",\"connid\":\"" + _ConnectorId + "\",\"mttyp\":\"" + _MetaDataType + "\",\"tblnm\":\"" + ReplaceDoubleCourseAndBackSlash(_TableName) + "\",\"qrytp\":\"" + (_DbType == "5" ? "0" : "1") + "\",\"unm\":\"" + AesEncryption.AESEncrypt(_CompanyId, _userName) + "\",\"pwd\":\"" + AesEncryption.AESEncrypt(_CompanyId,_password) + "\"}}";

            GetMpluginTestConnectionUrl objServerUrl = new GetMpluginTestConnectionUrl(_CompanyId);
            objServerUrl.Process();
            if (objServerUrl.StatusCode == 0)
            {
                strUrl = objServerUrl.ServerUrl;
                if (strUrl.Length > 0)
                {
                    strUrl = strUrl + "/MPGetDBMetadata.aspx?d=" + Utilities.UrlEncode(strRqst);
                    HTTP oHttp = new HTTP(strUrl);
                    oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                    HttpResponseStatus oResponse = oHttp.Request();
                    if (oResponse.StatusCode == HttpStatusCode.OK)
                    {
                        DataSet ds = new DataSet();
                        MP_GetDatabseMetadataResp obj = new MP_GetDatabseMetadataResp(oResponse.ResponseText);
                        if (obj.Code == "0")
                        {
                            ds = obj.Data;
                        }
                        else
                        {
                            return null;
                        }
                        return ds;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        private string ReplaceDoubleCourseAndBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return ReplaceDoubleCourse(ReplaceBackSlash(_Input));
            }
        }


        private string ReplaceBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return Regex.Replace(_Input, @"\\", @"\\");
            }
        }


        private string ReplaceDoubleCourse(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return Regex.Replace(_Input, "\"", "\\\"");
            }
        }

        private void IntQueryBuilderControlMplugin()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("D:\\aa.xml");
            string strXml = doc.OuterXml;
            QueryBuilderControl qbc = (QueryBuilderControl)queryBuilderControl.FindControl("qbc");
            QueryBuilder qb = qbc.QueryBuilder;

            MSSQLSyntaxProvider syntaxProvider = new MSSQLSyntaxProvider();
            qb.SyntaxProvider = syntaxProvider;

            qb.OfflineMode = true;
            qb.MetadataContainer.LoadFromXML(strXml);
            qb.Clear();
            qb.Refresh();
        }


    }
    
}
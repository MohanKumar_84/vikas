﻿<%@ Page Title="mFicient | Plan History" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="planPurchaseHistory.aspx.cs" Inherits="mFicientCP.planPurchaseHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="upd" runat="server">
        <ContentTemplate>
            <div id="PageCanvasContent">
                <div id="divRepeater">
                    <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                        <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                            <div>
                                <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>Plan Purchase History</h1>"></asp:Label>
                            </div>
                            <div style="position: relative; top: 10px; right: 15px;">
                            </div>
                            <div style="height: 0px; clear: both">
                            </div>
                        </asp:Panel>
                        <asp:Repeater ID="rptPlanPurchaseHistoryDtls" runat="server" OnItemDataBound="rptPlanPurchaseHistoryDtls_ItemDataBound">
                            <HeaderTemplate>
                                <table class="repeaterTable rptWithoutImageAndButton">
                                    <thead>
                                        <tr>
                                            <th>
                                                Plan
                                            </th>
                                            <th>
                                                Type
                                            </th>
                                            <th>
                                                Price
                                            </th>
                                            <th>
                                                Purchase Date
                                            </th>
                                        </tr>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tbody>
                                    <tr class="repeaterItem">
                                        <td>
                                            <asp:Label ID="lblPlanName" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                            &nbsp;(&nbsp;
                                            <asp:Label ID="lblPlanCode" runat="server" Text='<%# Eval("PLAN_CODE") %>'></asp:Label>&nbsp;)&nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTransactionType" runat="server" Text='<%# Eval("TRANSACTION_TYPE") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("ACTUAL_PRICE") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDate" runat="server"></asp:Label>
                                            <asp:Label ID="lblPurchaseDate" runat="server" Text='<%# Eval("PURCHASE_DATE") %>'
                                                Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                </tbody>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tbody>
                                    <tr class="repeaterAlternatingItem">
                                        <td>
                                            <asp:Label ID="lblPlanName" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                            &nbsp;(&nbsp;
                                            <asp:Label ID="lblPlanCode" runat="server" Text='<%# Eval("PLAN_CODE") %>'></asp:Label>&nbsp;)&nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTransactionType" runat="server" Text='<%# Eval("TRANSACTION_TYPE") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("ACTUAL_PRICE") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDate" runat="server"></asp:Label>
                                            <asp:Label ID="lblPurchaseDate" runat="server" Text='<%# Eval("PURCHASE_DATE") %>'
                                                Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                </tbody>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>

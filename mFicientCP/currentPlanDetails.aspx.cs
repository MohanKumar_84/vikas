﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientCP
{
    public partial class currentPlanDetails : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;

        enum RENEW_PLAN_CONTROL_HIDE_SHOW_CONDITIONS
        {
            CurrentDetails,
            Upgrade,
            Downgrade,
            Cancel
        }
        enum PLAN_TYPE
        {
            Purchased,
            Trial,
        }
        enum HIDE_SHOW_CONTAINER_DIVS
        {
            NewPurchase,
            RenewPlan,
            AddUsers,
            UpgradeUsers,
            Details
        }
        enum CONTAINER_PANELS
        {
            CurrentPlanDetails = 0,
            PurchasePlan = 1
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!Page.IsPostBack)
            {
                if (hfsValue != string.Empty)
                {
                    processfillCurrentPlanDetails(null);
                    defaultsOfNewPlanPurchase();
                    processfillCurrentPlanOrder(null);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Show Hide Container Panels On Page Load", @"hideShowContainerPanels(" + ((int)CONTAINER_PANELS.CurrentPlanDetails) + ");", true);
                }
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    processfillCurrentPlanDetails(null);
                    defaultsOfNewPlanPurchase();
                    processfillCurrentPlanOrder(null);
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, @"hideShowContainerPanels(" + ((int)CONTAINER_PANELS.CurrentPlanDetails) + ");");
                    return;
                }
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, String.Empty);
            }

            try
            {
                if (!String.IsNullOrEmpty(hfsValue))
                    CompanyTimezone.getTimezoneInfo(hfsPart3);
            }
            catch
            { }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updPanel">pass null if not called from an update panel</param>
        protected void processfillCurrentPlanDetails(UpdatePanel updPanel)
        {
            CurrentPlanDetail objCurrentPlanDetail = new CurrentPlanDetail(hfsPart3);
            objCurrentPlanDetail.Process();
            DataTable dtCurrentPlanDtl = objCurrentPlanDetail.PlanDetails;
            GetUIDetailCounts objUIDetailsCount = new GetUIDetailCounts(hfsPart3);
            objUIDetailsCount.Process();
            GetUserDetail objUserDetail = new GetUserDetail(false, true, false, hfsPart3, String.Empty);
            objUserDetail.Process();
            CompanyPlan objCompanyPlan = objCurrentPlanDetail.CompanyPlanDetail; ;
            if (objCurrentPlanDetail.StatusCode == 0)
            {
                if (dtCurrentPlanDtl != null && dtCurrentPlanDtl.Rows.Count > 0)
                {
                    if (dtCurrentPlanDtl.TableName == "Plan" && objCompanyPlan != null)
                    {
                        hidIsTrial.Value = "false";
                        lblWorkflow.Text = objCompanyPlan.MaxWorkFlow;
                        lblChargePerMonth.Text = Convert.ToString(objCompanyPlan.UserChargePerMonth);
                        hidChargePerMnthPerUsr.Value = lblChargePerMonth.Text;
                        lblChargeType.Text = objCompanyPlan.ChargeType;
                        lblMinUsers.Text = Convert.ToString(objCurrentPlanDetail.MinNoOfUsersForPlan);
                        lblMaxUsers.Text = Convert.ToString(objCompanyPlan.MaxUsers);
                        btnNewPlan.Visible = false;
                        btnAddUser.Visible = true;

                        lblCurrentPlanName.Text = objCompanyPlan.PlanName;
                        lblBalanceAmt.Text = Convert.ToString(objCompanyPlan.BalanceAmount);
                        lblBalanceAmntCurrType.Text = objCompanyPlan.ChargeType;
                        DateTime purchaseDate = new DateTime(Convert.ToInt64(dtCurrentPlanDtl.Rows[0]["PURCHASE_DATE"].ToString()));
                        DateTime validPeriod = purchaseDate.AddMonths(Convert.ToInt32(Math.Ceiling(Convert.ToDouble(dtCurrentPlanDtl.Rows[0]["VALIDITY"].ToString()))));
                        TimeSpan time = validPeriod.Subtract(DateTime.UtcNow);

                        lblNextMnthPlanName.Text = objCompanyPlan.NextMonthPlanName;
                        hideShowControlsByPlanType(PLAN_TYPE.Purchased);

                        if (!(objCurrentPlanDetail.CompanyPlanDetail.isAccountBalanceValidForNextMonth()))
                        {
                            if (!(Page.IsPostBack))
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "$.wl_Alert('Balance Amount insufficient','info','#divAlert');", true);
                            }
                            else
                            {
                                if (!(updPanel == null))
                                {
                                    Utilities.runPostBackScript("$.wl_Alert('Balance Amount insufficient','info','#divAlert');", updPanel, "Show insufficient amount");
                                }
                            }
                        }
                        if (objUIDetailsCount.StatusCode == 0)
                        {
                            lblWorkflowUsed.Text = Convert.ToString(objUIDetailsCount.ApplicationCount);
                        }
                        if (objUserDetail.StatusCode == 0)
                        {
                            lblCurrentUsersCount.Text = Convert.ToString(objUserDetail.TotalUsersCount);
                        }
                    }
                    else
                    {
                        if (objCompanyPlan != null)
                        {
                            lblCurrentPlanName.Text = objCompanyPlan.PlanName;//the value set here is used in the if condition check so if this is changed then change it there as well.
                            lblWorkflow.Text = objCompanyPlan.MaxWorkFlow;//18/9/2012
                            lblTrialMaxUsersCount.Text = Convert.ToString(objCompanyPlan.MaxUsers);
                            lblTrialCurrentUsers.Text = Convert.ToString(objUserDetail.TotalUsersCount);
                            lblChargePerMonth.Text = Convert.ToString(objCompanyPlan.UserChargePerMonth);
                            lblBalanceAmt.Text = Convert.ToString(objCompanyPlan.BalanceAmount);
                            lblWorkflowUsed.Text = Convert.ToString(objUIDetailsCount.ApplicationCount);
                            hideShowControlsByPlanType(PLAN_TYPE.Trial);
                            hidIsTrial.Value = "true";
                        }
                    }
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="containerDiv">container div to show</param>

        //DataTable getCompanyCurrentPlan()
        //{
        //    CurrentPlanDetail currentPlan = new CurrentPlanDetail(hfsPart3);
        //    currentPlan.Process();
        //    return currentPlan.PlanDetails;
        //}

        //protected DataTable GetDiscountPrice(string validMonths)
        //{
        //    bool flag = false;
        //    GetPlanList objPlanList = new GetPlanList(validMonths, 1);
        //    objPlanList.PlanDiscounts();
        //    return objPlanList.PlanDiscountList;
        //}
        int savePlanRenewOrChangesByReqType(PLAN_RENEW_OR_CHANGE_REQUESTS planRenewOrChngReq, string planCode, string noOfUsers, string months, string currency)
        {
            string requestType = "";
            int iError = 0;
            switch (planRenewOrChngReq)
            {
                case PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_UPGRADE:
                    requestType = Convert.ToString((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_UPGRADE);
                    break;
                case PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_DOWNGRADE:
                    requestType = Convert.ToString((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_DOWNGRADE);
                    break;
                case PLAN_RENEW_OR_CHANGE_REQUESTS.RENEW:
                    requestType = Convert.ToString((int)PLAN_RENEW_OR_CHANGE_REQUESTS.RENEW);
                    break;
                case PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED:
                    requestType = Convert.ToString((int)PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED);
                    break;
                case PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_PURCHASE:
                    requestType = Convert.ToString((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_PURCHASE);
                    break;
            }
            PlanRenewOrChangeReqByReqType objPlanChanges = new PlanRenewOrChangeReqByReqType(hfsPart3, requestType, planCode, noOfUsers, months, currency);
            objPlanChanges.Process();
            if (objPlanChanges.StatusCode == 0)
            {
                clearHiddenPlanCodeField();
            }
            else
            {
                iError = -1000;
                //Utilities.showMessage(objPlanChanges.StatusDescription, updContainer, DIALOG_TYPE.Error);
                throw new MficientException(objPlanChanges.StatusDescription);
            }
            return iError;
        }
        protected void lnkBack_Click(object sender, EventArgs e)
        {

            CurrentPlanDetailsDiv.Visible = true;

        }
        //void clearUpDowngradeHidField()
        //{
        //    hidPlanChangeType.Value = "";
        //    hidUpgrdDowngrdPlanCode.Value = "";
        //}



        protected void btnNewPlan_Click(object sender, EventArgs e)
        {

        }


        void processNewPlanSelection(RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.ToLower() == "select")
                {
                    hfdPlanCode.Value = ((Label)e.Item.FindControl("lblPlanCode")).Text;
                    Label lblPlanName = (Label)e.Item.FindControl("lblPlanName");
                    string strChargeType = "", strPlanValidMonths = "";
                    if (Convert.ToBoolean(hidIsTrial.Value))
                    {
                        strChargeType = ddlPurchaseCurrencyType.SelectedValue;
                        strPlanValidMonths = ddlPurchaseMonths.SelectedValue;
                        int iMinUsers = Convert.ToInt32(((Label)e.Item.FindControl("lblMinUsers")).Text);
                        if (iMinUsers > Convert.ToInt32(txtPurchaseNoOfUsers.Text))
                        {
                            Utilities.showMessage("The Min number of user for this plan is " + Convert.ToString(iMinUsers) + ".</br> Please enter valid no of users or select some other plan.", updModalContainer, DIALOG_TYPE.Error);
                            hideShowContainerDivs(CONTAINER_PANELS.PurchasePlan);
                            return;
                        }
                        lblPurchasePlanName.Text = ((Label)e.Item.FindControl("lblPlanName")).Text;
                        lblPurchasePrice.Text = ((Label)e.Item.FindControl("lblPrice")).Text;
                        lblPurchaseCurrency.Text = ((Label)e.Item.FindControl("lblCurrencyType")).Text;
                        //hideShowCalculateTotalLink(false);
                        hideShowCalculateTotalLinkUsingJavascript(false, updModalContainer);
                        Utilities.closeModalPopUp("divModalContainer", updModalContainer);
                        hideShowContainerPanelsUsingJavascript(updContainer, CONTAINER_PANELS.PurchasePlan);
                        setTextAndCmdArgumentOfLnkPurchase();
                    }
                    else
                    {
                        GetUIDetailCounts objUIDetailsCount = new GetUIDetailCounts(hfsPart3);
                        objUIDetailsCount.Process();
                        if (objUIDetailsCount.StatusCode == 0)
                        {
                            int iWorkFlowInPlan = Convert.ToInt32(((Label)e.Item.FindControl("lbWorkFlow")).Text); //check the work flow count
                            if (iWorkFlowInPlan > objUIDetailsCount.ApplicationCount)
                            {
                                string strError;
                                int iMaxUsers = Convert.ToInt32(lblMaxUsers.Text);
                                int iMinUsers = Convert.ToInt32(((Label)e.Item.FindControl("lblMinUsers")).Text); //check minimum users count
                                if (iMinUsers < iMaxUsers)
                                {
                                    processUpdateNextMonthPlan(e.Item.ItemIndex, out strError);
                                }
                                else
                                {
                                    btnConfirmYes.CommandArgument = Convert.ToString(e.Item.ItemIndex);
                                    lblConfirmationMessage.Text = @"Company\\'s max user count: " + iMaxUsers + " is less than the min users required for the plan." +
                                                                    "</br>Next billing process will consider the min users for the plan if selected(or max users which ever is greater).Would you like to continue.</br>";
                                    Utilities.showModalPopup("divPostBackConfirm", updModalContainer, "Please confirm", "320", false, false, DIALOG_TYPE.Warning, "divPostBackConfirm");
                                    updConfirmation.Update();
                                }

                            }
                            else
                            {
                                Utilities.showMessage("The work flow of the selected plan is more than the work flow already in use by the company.</br>Please select some other plan.</br>Company\\'s current work flow count : " + objUIDetailsCount.ApplicationCount, updContainer, DIALOG_TYPE.Info);
                            }
                        }
                        hideShowContainerPanelsUsingJavascript(updContainer, CONTAINER_PANELS.CurrentPlanDetails);
                    }

                }
            }
            catch
            {
                Utilities.showMessage(
                    "Internal server error.",
                    updContainer,
                    DIALOG_TYPE.Error);
            }
        }

        protected void btnConfirmYes_Click(object sender, EventArgs e)
        {
            string strError;
            processUpdateNextMonthPlan(Convert.ToInt32(((Button)sender).CommandArgument), out strError);
            if (String.IsNullOrEmpty(strError))
            {
                Utilities.closeModalPopUp("divPostBackConfirm", updConfirmation, "confirmBox", "divPostBackConfirm");
                updConfirmation.Update();
            }
            hideShowContainerDivs(CONTAINER_PANELS.CurrentPlanDetails);
        }

        protected void btnConfirmNo_Click(object sender, EventArgs e)
        {
            Utilities.closeModalPopUp("divPostBackConfirm", updConfirmation, "confirmBox", "divPostBackConfirm");
            updConfirmation.Update();
        }
        void processUpdateNextMonthPlan(int rptPlanDetailItemIndex, out string error)
        {
            Label lblPlanName = ((Label)rptPlanDetails.Items[rptPlanDetailItemIndex].FindControl("lblPlanName"));
            UpdateCompanyCurrentPlan objUpdateNextMonthPlan = new UpdateCompanyCurrentPlan(hfdPlanCode.Value, lblPlanName.Text, hfsPart3, PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_CHANGE);
            objUpdateNextMonthPlan.Process();
            if (objUpdateNextMonthPlan.StatusCode == 0)
            {
                Utilities.showMessage("Next month plan saved successfully", updModalContainer, DIALOG_TYPE.Info);
                Utilities.closeModalPopUp("divModalContainer", updModalContainer);
                processfillCurrentPlanDetails(updModalContainer);
                error = "";
            }
            else
            {
                Utilities.showMessage("Internal server error.", updModalContainer, DIALOG_TYPE.Error);
                error = "Internal server error.";
            }

        }
        DataTable getPlanDetails(string planCode, string currency, int months, out string pricePerUserPerMonth)
        {
            GetPlanList objPlanList = new GetPlanList(planCode, currency, months, false);
            objPlanList.Process();
            pricePerUserPerMonth = "";
            DataTable dtblPlanDtls = null;
            if (objPlanList.StatusCode == 0)
            {
                dtblPlanDtls = objPlanList.PlanDetails;
                pricePerUserPerMonth = Convert.ToString(dtblPlanDtls.Rows[0]["PRICE"]);
            }
            return dtblPlanDtls;
        }
        //void fillPurchasePlanDtls(DataTable planDetails, string currencySelected)
        //{
        //    lblPurchasePlanName.Text = Convert.ToString(planDetails.Rows[0]["PLAN_NAME"]);
        //    if (currencySelected.ToLower() == "usd")
        //    {
        //        lblPurchasePrice.Text = Convert.ToString(planDetails.Rows[0]["PRICE"]);//29/10/2012
        //        lblPurchaseCurrency.Text = "USD";
        //    }
        //    else
        //    {
        //        lblPurchasePrice.Text = Convert.ToString(planDetails.Rows[0]["PRICE"]);
        //        lblPurchaseCurrency.Text = "INR";
        //    }
        //}

        protected void lnkPurchasePlnChange_Click(object sender, EventArgs e)
        {
            try
            {
                string strErrorMsg = validatePurchaseForm(false);
                if (String.IsNullOrEmpty(strErrorMsg))
                    showPlanList(ddlPurchaseCurrencyType.SelectedValue, Convert.ToInt32(ddlPurchaseMonths.SelectedValue));
                else
                    Utilities.showAlert(strErrorMsg, "divPurchaseDetails", updContainer);

                hideShowContainerDivs(CONTAINER_PANELS.PurchasePlan);
            }
            catch
            {
                Utilities.showAlert("Internal server error.", "divPurchaseDetails", updContainer);
            }
        }
        protected void lnkCalculateAmnt_Click(object sender, EventArgs e)
        {
            string strValidationError = validatePurchaseForm(true);
            if (strValidationError == "")
            {
                try
                {

                    double iAmountPayable = int.Parse(txtPurchaseNoOfUsers.Text) * int.Parse(ddlPurchaseMonths.SelectedValue) * double.Parse(lblPurchasePrice.Text);
                    Utilities.showMessage("Amount payable :" + " " + iAmountPayable.ToString() + " " + ddlPurchaseCurrencyType.SelectedValue, updContainer, DIALOG_TYPE.Info);
                }
                catch
                {
                    Utilities.showAlert("Internal error.Please try again", "divPurchaseDetails", updContainer);
                }

            }
            else
            {
                Utilities.showAlert(strValidationError, "divPurchaseDetails", updContainer);
            }
            hideShowContainerDivs(CONTAINER_PANELS.PurchasePlan);
        }

        protected void btnPurchasePlanSave_Click(object sender, EventArgs e)
        {
            string strValidationError = validatePurchaseForm(true);
            if (strValidationError == "")
            {
                try
                {
                    int iError =
                        savePlanRenewOrChangesByReqType(
                        PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_PURCHASE,
                        hfdPlanCode.Value,
                        txtPurchaseNoOfUsers.Text,
                        ddlPurchaseMonths.SelectedValue,
                        ddlPurchaseCurrencyType.SelectedValue
                        );
                    hideShowContainerDivs(CONTAINER_PANELS.CurrentPlanDetails);
                    //processfillCurrentPlanDetails(updContainer);
                    processfillCurrentPlanOrder(updContainer);
                    defaultsOfNewPlanPurchase();
                    Utilities.showMessage("Request added successfully", updContainer, DIALOG_TYPE.Info);
                }
                catch (MficientException ex)
                {
                    Utilities.showAlert(ex.Message, "divPurchasePlan", updContainer);
                    hideShowContainerDivs(CONTAINER_PANELS.PurchasePlan);
                }
                catch
                {
                    Utilities.showAlert("Internal server error.", "divPurchasePlan", updContainer);
                    hideShowContainerDivs(CONTAINER_PANELS.PurchasePlan);
                }
                //if (iError == 0)
                //{
                //    hideShowContainerDivs(CONTAINER_PANELS.CurrentPlanDetails);
                //    fillCurrentPlanDetails(updContainer);
                //    processfillCurrentPlanOrder(updContainer);
                //    defaultsOfNewPlanPurchase();
                //}
                //else
                //{
                //    Utilities.showAlert("Internal server error.Please try again.", "divPurchaseDetails", updContainer);
                //    hideShowContainerDivs(CONTAINER_PANELS.PurchasePlan);
                //}

            }
            else
            {
                Utilities.showAlert(strValidationError, "divPurchasePlan", updContainer);
                hideShowContainerDivs(CONTAINER_PANELS.PurchasePlan);
                //hideShowContainerPanelsUsingJavascript(updContainer, CONTAINER_PANELS.PurchasePlan);
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Utilities.closeModalPopUp("divModalContainer", updModalContainer, "Close Modal Plan Details");
        }


        void defaultsOfNewPlanPurchase()
        {
            hfdPlanCode.Value = "";
            setTextAndCmdArgumentOfLnkPurchase();
            lblPurchasePrice.Text = "please select a plan first";
            lblPurchaseCurrency.Text = "";
            lblPurchasePlanName.Text = "";
            ddlPurchaseMonths.SelectedIndex = ddlPurchaseMonths.Items.IndexOf(ddlPurchaseMonths.Items.FindByValue("-1"));
            txtPurchaseNoOfUsers.Text = "";
        }
        void setTextAndCmdArgumentOfLnkPurchase()
        {
            if (hfdPlanCode.Value == "")
            {
                lnkPurchasePlnChange.Text = "[select]";
                lnkPurchasePlnChange.CommandArgument = "Select";
            }
            else
            {
                lnkPurchasePlnChange.Text = "[change]";
                lnkPurchasePlnChange.CommandArgument = "change";
            }
        }

        #region New Process

        string validateAddUserForm()
        {
            string strMessage = "";
            try
            {
                int iNoOfUsers = int.MinValue;
                if (!int.TryParse(txtAddUserCount.Text, out iNoOfUsers))
                {
                    strMessage = "Please enter valid no of users";
                    return strMessage;
                }
                if (iNoOfUsers <= 0)
                {
                    strMessage = "Please enter valid no of users";
                }
                return strMessage;
            }
            catch
            {
                return strMessage = "Internal server error";
            }

        }
        string validateRemoveUserForm()
        {
            string strMessage = "";
            try
            {
                int iNoOfUsers = int.MinValue;
                if (!int.TryParse(txtRemoveUserCount.Text, out iNoOfUsers))
                {
                    strMessage = "Please enter valid no of users";
                    return strMessage;
                }
                if (iNoOfUsers <= 0)
                {
                    strMessage = "Please enter valid no of users";
                }
                return strMessage;
            }
            catch
            {
                return strMessage = "Internal server error";
            }

        }
        string validatePurchaseForm(bool isPlanSelected)
        {
            string strErrorMsg = "";
            if (ddlPurchaseMonths.SelectedValue == "-1")
            {
                strErrorMsg += "Please select a month." + "<br />";
            }
            try
            {
                if (String.IsNullOrEmpty(txtPurchaseNoOfUsers.Text) || Convert.ToInt32(txtPurchaseNoOfUsers.Text) == 0)
                {
                    strErrorMsg += "Please select valid no of users." + "<br />";
                }
            }
            catch
            {
                strErrorMsg += "Please select valid no of users." + "<br />";
            }
            if (isPlanSelected)
            {
                if (String.IsNullOrEmpty(hfdPlanCode.Value))
                {
                    strErrorMsg += "Please select a plan." + "<br/>";
                }
            }
            return strErrorMsg;
        }
        void clearHiddenPlanCodeField()
        {
            hfdPlanCode.Value = "";
        }
        protected void lnkAddUserCheckBalance_Click(object sender, EventArgs e)
        {
            string strError = validateAddUserForm();
            if (String.IsNullOrEmpty(strError))
            {
                CurrentPlanDetail objCurrentPlan = new CurrentPlanDetail(hfsPart3);
                objCurrentPlan.Process();
                CompanyPlan objCompanyPlanDetails = objCurrentPlan.CompanyPlanDetail;
                decimal decBalanceAmount;
                if (objCompanyPlanDetails.isBalanceAmountValidForAddingUsers(Convert.ToInt32(txtAddUserCount.Text), out decBalanceAmount))
                {
                    Utilities.showMessage(
                        "Your account balance after adding " + txtAddUserCount.Text +
                        " users would be " + decBalanceAmount.ToString() + " " +
                        objCompanyPlanDetails.ChargeType,

                        updAddUsers,
                        DIALOG_TYPE.Info);
                }
                else
                {
                    Utilities.showMessage("You don't have sufficient balance to add users", updAddUsers, DIALOG_TYPE.Info);
                }
            }
            else
            {
                Utilities.showAlert(strError, "divAddUsersContainer", updAddUsers);
            }
            hideShowContainerDivs(CONTAINER_PANELS.CurrentPlanDetails);
        }
        protected void btnAddUsersSave_Click(object sender, EventArgs e)
        {
            string strError = validateAddUserForm();
            if (String.IsNullOrEmpty(strError))
            {
                CurrentPlanDetail objCurrentPlan = new CurrentPlanDetail(hfsPart3);
                objCurrentPlan.Process();
                CompanyPlan objCompanyPlanDetails = objCurrentPlan.CompanyPlanDetail;
                if (objCompanyPlanDetails.isBalanceAmountValidForAddingUsers(Convert.ToInt32(txtAddUserCount.Text)))
                {
                    UpdateCompanyCurrentPlan objUpdateCurrentPlan = new UpdateCompanyCurrentPlan(Convert.ToInt32(txtAddUserCount.Text), objCompanyPlanDetails, PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED);
                    objUpdateCurrentPlan.Process();
                    if (objUpdateCurrentPlan.StatusCode == 0)
                    {
                        Utilities.closeModalPopUp("divAddUsersContainer", updAddUsers);
                        Utilities.showMessage("Users added successfully", updAddUsers, DIALOG_TYPE.Info);
                        processfillCurrentPlanDetails(updAddUsers);
                    }
                    else
                    {
                        Utilities.showMessage("Internal server error", updAddUsers, DIALOG_TYPE.Error);
                    }
                }
                else
                {
                    Utilities.showMessage("Your account does not have sufficient balance to add new users.", updAddUsers, DIALOG_TYPE.Error);
                }
            }
            else
            {
                Utilities.showAlert(strError, "divAddUsersContainer", updAddUsers);
            }
            hideShowContainerDivs(CONTAINER_PANELS.CurrentPlanDetails);
        }
        protected void btnRemoveUsersSave_Click(object sender, EventArgs e)
        {
            string strError = validateRemoveUserForm();
            if (String.IsNullOrEmpty(strError))
            {
                CurrentPlanDetail objCurrentPlan = new CurrentPlanDetail(hfsPart3);
                objCurrentPlan.Process();
                CompanyPlan objCompanyPlanDetails = objCurrentPlan.CompanyPlanDetail;
                GetUserDetail objUserDetail = new GetUserDetail(false, true, false, hfsPart3, String.Empty);
                objUserDetail.Process();
                if (objCurrentPlan.StatusCode == 0 && objUserDetail.StatusCode == 0)
                {
                    if (objCurrentPlan.MinNoOfUsersForPlan >= (objUserDetail.TotalUsersCount - Convert.ToInt32(txtRemoveUserCount.Text)))
                    {
                        UpdateCompanyCurrentPlan objUpdateCurrentPlan = new UpdateCompanyCurrentPlan(Convert.ToInt32(txtRemoveUserCount.Text), objCompanyPlanDetails, PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_REMOVED);
                        objUpdateCurrentPlan.Process();
                        if (objUpdateCurrentPlan.StatusCode == 0)
                        {
                            Utilities.closeModalPopUp("divRemoveUsersContainer", updRemoveUsers);
                            Utilities.showMessage("Users removed successfully", updRemoveUsers, DIALOG_TYPE.Info);
                            processfillCurrentPlanDetails(updRemoveUsers);
                        }
                        else
                        {
                            Utilities.showMessage("Internal server error", updRemoveUsers, DIALOG_TYPE.Error);
                        }
                    }
                    else
                    {
                        Utilities.showMessage("Cannot delete these many users.Min number of users for the plan should be" + objCurrentPlan.MinNoOfUsersForPlan, updRemoveUsers, DIALOG_TYPE.Error);
                    }
                }
                else
                {
                    Utilities.showMessage("Internal server error", updRemoveUsers, DIALOG_TYPE.Error);
                }
            }
            else
            {
                Utilities.showAlert(strError, "divRemoveUsersContainer", updRemoveUsers);
            }
            hideShowContainerDivs(CONTAINER_PANELS.CurrentPlanDetails);
        }
        void showPlanList(string currencyType, int months)
        {
            string strErrorMsg;
            bindPlanDetailsRpt(currencyType, months, out strErrorMsg);
            if (String.IsNullOrEmpty(strErrorMsg))
            {
                Utilities.showModalPopup("divModalContainer", updContainer, "Plan Details", "600", false);
            }
            else
            {
                Utilities.showMessage(strErrorMsg, updContainer, DIALOG_TYPE.Info);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currencyType"></param>
        /// <param name="months"></param>
        /// <param name="info">Messages returned by the process</param>
        ///<exception cref="System.ArgumentException">Thrown when currency type is null or empty</exception>
        ///<exception cref="System.Exception"></exception>
        void bindPlanDetailsRpt(string currencyType, int months, out string errorMsg)
        {
            if (String.IsNullOrEmpty(currencyType))
            {
                throw new ArgumentException("Provide Currency Type");
            }
            errorMsg = String.Empty;
            GetPlanList objPlanList = null;
            if (months == 0)
            {
                objPlanList = new GetPlanList(currencyType);
            }
            else
            {
                objPlanList = new GetPlanList(currencyType, months);
            }
            objPlanList.Process();
            DataTable dtblPlanDtls = objPlanList.PlanDetails;
            if (dtblPlanDtls != null)
            {
                if (dtblPlanDtls.Rows.Count > 0)
                {
                    DataView dv = dtblPlanDtls.DefaultView;
                    dv.Sort = "PLAN_NAME";
                    dtblPlanDtls = dv.ToTable();
                    dtblPlanDtls = modifyPlanDetailsDTable(dtblPlanDtls);
                    rptPlanDetails.DataSource = dtblPlanDtls;
                    rptPlanDetails.DataBind();
                }
                else
                {
                    dtblPlanDtls = null;
                    rptPlanDetails.DataSource = dtblPlanDtls;
                    rptPlanDetails.DataBind();
                    errorMsg = "No Plan found";
                }
            }
            else
            {
                throw new Exception();
            }
        }

        DataTable modifyPlanDetailsDTable(DataTable planDtlsDtbl)
        {
            if (planDtlsDtbl == null)
            {
                throw new ArgumentException();
            }
            planDtlsDtbl.Columns.Add("MONTHS", typeof(string));
            foreach (DataRow row in planDtlsDtbl.Rows)
            {
                string strMonth = Convert.ToString(row["FROM_MONTH"]) + " - " + Convert.ToString(row["TO_MONTH"]);
                row["MONTHS"] = strMonth;
            }
            planDtlsDtbl.AcceptChanges();
            return planDtlsDtbl;
        }

        protected void lnkChangeNxtMnthPlan_Click(object sender, EventArgs e)
        {
            try
            {
                string strInformation;
                bindPlanDetailsRpt(lblChargeType.Text.Trim(), 0, out strInformation);
                if (String.IsNullOrEmpty(strInformation))
                {
                    Utilities.showModalPopup("divModalContainer", updContainer, "Select Plan", "800", true);
                }
                else
                {
                    Utilities.showMessage(strInformation, updContainer, DIALOG_TYPE.Info);
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error", updContainer, DIALOG_TYPE.Error);
            }
            hideShowContainerDivs(CONTAINER_PANELS.CurrentPlanDetails);
        }

        protected void rptPlanDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            processNewPlanSelection(e);
        }
        protected void rptPlanDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                RadioButton radSelect = (RadioButton)e.Item.FindControl("radSelect");
                Label lblCurrencyType = (Label)e.Item.FindControl("lblCurrencyType");
                if (hidPlanChangeType.Value.ToLower() != "new")
                {
                    lblCurrencyType.Visible = true;
                    lblCurrencyType.Text = lblChargeType.Text;
                }
                else
                {
                    lblCurrencyType.Visible = true;
                    lblCurrencyType.Text = ddlPurchaseCurrencyType.SelectedValue;
                }
                if (string.IsNullOrEmpty(hidPlanRadButtonFormed.Value))
                {

                }
            }
        }



        #region Add new Plan
        void hideShowControlsByPlanType(PLAN_TYPE planType)
        {
            switch (planType)
            {
                case PLAN_TYPE.Purchased:
                    btnAddUser.Visible = true;
                    btnNewPlan.Visible = false;
                    btnRemoveUser.Visible = true;
                    pnlNextMonthPlan.Visible = true;
                    pnlUserDetailTrial.Visible = false;
                    pnlUserDetailPurchased.Visible = true;
                    btnNewPlan.Visible = false;
                    lblPlanTrialText.Visible = false;
                    break;
                case PLAN_TYPE.Trial:
                    btnAddUser.Visible = false;
                    btnNewPlan.Visible = true;
                    btnRemoveUser.Visible = false;
                    pnlNextMonthPlan.Visible = false;
                    pnlUserDetailTrial.Visible = true;
                    pnlUserDetailPurchased.Visible = false;
                    btnNewPlan.Visible = true;
                    lblPlanTrialText.Visible = true;
                    break;

            }
        }
        void hideShowPlanPurchaseButton(bool hide)
        {
            if (hide)
            {
                btnNewPlan.Visible = false;
            }
            else
            {
                btnNewPlan.Visible = true;
            }
        }
        void hideShowOrderCancelPlanButton(bool hide)
        {
            if (hide)
            {
                btnCurrPlnOdrCancel.Visible = false;
            }
            else
            {
                btnCurrPlnOdrCancel.Visible = true;
            }
        }
        void hideShowPurchaseOrderViewPanel(bool hide)
        {
            if (hide)
            {
                pnlCurrentPlanOrder.Visible = false;
            }
            else
            {
                pnlCurrentPlanOrder.Visible = true;
            }
        }
        void hideShowCalculateTotalLink(bool hide)
        {
            if (hide)
            {
                lnkCalculateAmnt.Visible = false;
            }
            else
            {
                lnkCalculateAmnt.Visible = true;
            }
        }
        void hideShowCalculateTotalLinkUsingJavascript(bool hide, UpdatePanel updPanel)
        {
            if (hide)
            {
                if (updPanel != null)
                {
                    Utilities.runPostBackScript("showHideLnkCalculateTotal('false')", updPanel, "HideCalculateTotalLnkOnPostBack");
                }
                else
                {
                    Utilities.runPageStartUpScript(this.Page, "HideCalculateTotalLnkPageStartUp", "showHideLnkCalculateTotal('false')");
                }
            }
            else
            {
                if (updPanel != null)
                {
                    Utilities.runPostBackScript("showHideLnkCalculateTotal('true')", updPanel, "HideCalculateTotalLnkOnPostBack");
                }
                else
                {
                    Utilities.runPageStartUpScript(this.Page, "HideCalculateTotalLnkPageStartUp", "showHideLnkCalculateTotal('true')");
                }
            }
        }
        void hideShowContainerDivs(CONTAINER_PANELS panelToShow)
        {
            switch (panelToShow)
            {
                case CONTAINER_PANELS.CurrentPlanDetails:
                    hideShowContainerPanelsUsingJavascript(updContainer, CONTAINER_PANELS.CurrentPlanDetails);
                    break;
                case CONTAINER_PANELS.PurchasePlan:
                    hideShowContainerPanelsUsingJavascript(updContainer, CONTAINER_PANELS.PurchasePlan);
                    break;

            }
        }
        void hideShowContainerPanelsUsingJavascript(UpdatePanel updPanel, CONTAINER_PANELS panelToShow)
        {
            Utilities.runPostBackScript("hideShowContainerPanels(" + (int)panelToShow + ")", updPanel, "Hide show container");
        }

        DataTable getCurrentPlanOrderOfCompany(bool isForShowingDetails)
        {
            try
            {
                GetCurrentOrderedPlan objCurrentOrderedPlan = new GetCurrentOrderedPlan(hfsPart3);
                objCurrentOrderedPlan.Process();
                if (objCurrentOrderedPlan.StatusCode == 0)
                {
                    return objCurrentOrderedPlan.OrderRequest;
                }
                else
                {
                    throw new Exception("Internal server error.");
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        void processfillCurrentPlanOrder(UpdatePanel panelUpdatedWhenCalled)
        {
            GetCurrentOrderedPlan objCurrentOrderedPlan = new GetCurrentOrderedPlan(hfsPart3);
            try
            {
                objCurrentOrderedPlan.Process();
            }
            catch
            {
                if (panelUpdatedWhenCalled != null)
                {
                    Utilities.showMessage("Internal server error", panelUpdatedWhenCalled, DIALOG_TYPE.Error);
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(),
                        this.ClientID,
                        @"$.alert(" + "'" + "Internal server error" + "'" + "," + "$.alert.Error" + ");showDialogImage(" + "DialogType.Error" + ");",
                        true);
                }
                return;
            }
            if (objCurrentOrderedPlan.StatusCode == 0)
            {
                if (objCurrentOrderedPlan.OrderRequest.Rows.Count > 0)
                {
                    lblCurrPlnOdrPlanName.Text = objCurrentOrderedPlan.RequestPlanName;
                    lblCurrPlnOdrCurrType.Text = objCurrentOrderedPlan.Currency;
                    lblCurrPlnOdrCharge.Text = objCurrentOrderedPlan.UserChargePerMonth;
                    lblCurrPlnOdrNoOfUsers.Text = Convert.ToString(objCurrentOrderedPlan.NoOfUSers);
                    lblCurrPlnOdrMonths.Text = Convert.ToString(objCurrentOrderedPlan.Months);

                    lblCurrPlnOdrRequestedDate.Text = Utilities.getFormattedDateForUI(new DateTime(objCurrentOrderedPlan.RequestDateTimeTicks, DateTimeKind.Utc));
                    lblCurrPlnOdrRequestType.Text = getRequestTypeFromCode(Convert.ToInt32(objCurrentOrderedPlan.RequestType));
                    //there should be only one record present.if status is zero.then reseller approval is pending
                    //and hence can cancel the plan.
                    //if its 1 0r 2 then reseller,sales has approved this respectively and cannot cancel it.
                    if (Convert.ToInt32(objCurrentOrderedPlan.RequestStatus) != 0)
                    {
                        lblCurrPlnOdrStatus.Text = "Request in process";
                        hideShowOrderCancelPlanButton(true);
                        hideShowPlanPurchaseButton(true);
                        if (!Page.IsPostBack)
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, @"$.wl_Alert(" + "'" + "A request is already in process.No new request can be added" + "'" + ",'info','#" + "divAlert" + "'" + ");", true);
                        }
                        else
                        {
                            Utilities.showAlert("A request is already in process.No new request can be added", "CurrentPlanDetailsDiv", updContainer);
                        }
                    }
                    else
                    {
                        lblCurrPlnOdrStatus.Text = "Request pending with reseller";
                        hideShowOrderCancelPlanButton(false);
                        hideShowPlanPurchaseButton(true);
                        if (!Page.IsPostBack)
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, @"$.wl_Alert(" + "'" + "To buy new plan please cancel the previous order placed." + "'" + ",'info','#" + "divAlert" + "'" + ");", true);
                        }
                        else
                        {
                            Utilities.showAlert("To buy plan please cancel the previous order placed.", "CurrentPlanDetailsDiv", updContainer);
                        }
                    }
                    hideShowPurchaseOrderViewPanel(false);
                }
                else
                {
                    if (panelUpdatedWhenCalled != null)
                    {
                        hideShowPlanPurchaseButton(false);
                    }
                    hideShowPurchaseOrderViewPanel(true);
                }
            }
            else
            {
                if (panelUpdatedWhenCalled != null)
                {
                    Utilities.showMessage("Internal server error", panelUpdatedWhenCalled, DIALOG_TYPE.Error);
                }
                else
                {
                    Utilities.runPageStartUpScript(this.Page, "InternalServerError",
                        @"$.alert(" + "'" + "Internal server error" + "'" + "," + "$.alert.Error" + ");showDialogImage(" + "DialogType.Error" + ");"
                        );
                }
            }

        }
        MFEPlanOrderRequest getLatestPlanOrderRequest()
        {
            GetOrderedPlanRequests objLatestPlanOrderReq = new
            GetOrderedPlanRequests(hfsPart3);
            MFEPlanOrderRequest objMfePlanOrderReq = objLatestPlanOrderReq.getLatestPlanOrderRequest();
            if (objLatestPlanOrderReq.StatusCode != 0)
            {
                throw new MficientException(objLatestPlanOrderReq.StatusDescription);
            }
            else
            {
                return objMfePlanOrderReq;
            }
        }
        string getRequestTypeFromCode(int requestType)
        {
            string strRequestType = "";
            switch (requestType)
            {
                case (int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_DOWNGRADE:
                    strRequestType = "Plan Downgrade";
                    break;
                case (int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_UPGRADE:
                    strRequestType = "Plan Upgrade";
                    break;
                case (int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_PURCHASE:
                    strRequestType = "Plan Purchase";
                    break;
                case (int)PLAN_RENEW_OR_CHANGE_REQUESTS.RENEW:
                    strRequestType = "Renew Plan";
                    break;
                case (int)PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED:
                    strRequestType = "Add Users";
                    break;
            }
            return strRequestType;
        }
        protected void btnCurrPlnOdrCancel_Click(object sender, EventArgs e)
        {
            try
            {
                CancelCurrentPlan objCancelCurrPlan = new CancelCurrentPlan(hfsPart3);
                objCancelCurrPlan.Process();
                if (objCancelCurrPlan.StatusCode == 0)
                {
                    if (objCancelCurrPlan.NoOfRowsAffected == 1)
                    {
                        Utilities.showMessage("The order was cancelled successfully", updContainer, DIALOG_TYPE.Info);
                        hideShowPlanPurchaseButton(false);
                        hideShowPurchaseOrderViewPanel(true);
                    }
                    else
                    {
                        MFEPlanOrderRequest objMfePlanOrderReq = null;
                        string strMessageToShow = String.Empty;
                        bool blnWasRequestDenied = false;
                        try
                        {
                            objMfePlanOrderReq = getLatestPlanOrderRequest();
                            if (objMfePlanOrderReq != null)
                            {
                                PLAN_REQUEST_STATUS eReqStatus =
                                    (PLAN_REQUEST_STATUS)Enum.Parse(typeof(PLAN_REQUEST_STATUS),
                                    (objMfePlanOrderReq.Status).ToString());
                                switch (eReqStatus)
                                {
                                    case PLAN_REQUEST_STATUS.UNAPPROVED:
                                        break;
                                    case PLAN_REQUEST_STATUS.RESELLER_APPROVED:
                                        strMessageToShow = "The request could not be cancelled.The order was approved by reseller.";
                                        blnWasRequestDenied = false;
                                        break;
                                    case PLAN_REQUEST_STATUS.SALES_APPROVED:
                                        strMessageToShow = "The request could not be cancelled.The order was approved.";
                                        blnWasRequestDenied = false;
                                        break;
                                    case PLAN_REQUEST_STATUS.ACCOUNTS_APPROVED:
                                        strMessageToShow = "The request could not be cancelled.The order was approved.";
                                        blnWasRequestDenied = false;
                                        break;
                                    case PLAN_REQUEST_STATUS.CANCELLED:
                                        break;
                                    case PLAN_REQUEST_STATUS.RESELLER_DENIED:
                                        strMessageToShow = "The order was denied by reseller.<br/>" + objMfePlanOrderReq.RemarksDescription;
                                        blnWasRequestDenied = true;
                                        break;
                                    case PLAN_REQUEST_STATUS.SALES_DENIED:
                                        strMessageToShow = "The order was denied by sales manager.<br/>" + objMfePlanOrderReq.RemarksDescription;
                                        blnWasRequestDenied = true;
                                        break;
                                    case PLAN_REQUEST_STATUS.ACCOUNTS_DENIED:
                                        strMessageToShow = "The order was denied by accounts.<br/>" + objMfePlanOrderReq.RemarksDescription;
                                        blnWasRequestDenied = true;
                                        break;
                                }
                            }
                            else
                            {
                                throw new Exception();
                            }
                        }
                        catch
                        {
                        }
                        //this is done before other method call because when current plan order is filled
                        //the purchase button is set to visible 
                        //which we have to change if the plan was not denied.
                        processfillCurrentPlanDetails(updContainer);
                        hideShowContainerDivs(CONTAINER_PANELS.CurrentPlanDetails);
                        if (blnWasRequestDenied)
                        {
                            hideShowPlanPurchaseButton(false);
                            hideShowPurchaseOrderViewPanel(true);
                        }
                        else
                        {
                            hideShowPlanPurchaseButton(true);
                            hideShowPurchaseOrderViewPanel(false);
                            this.processfillCurrentPlanOrder(updContainer);
                        }
                        if (!String.IsNullOrEmpty(strMessageToShow))
                        {
                            Utilities.showMessage(strMessageToShow,
                            updContainer, DIALOG_TYPE.Info);
                        }
                    }
                }
                else
                {
                    Utilities.showMessage("Internal server error.", updContainer, DIALOG_TYPE.Error);
                    hideShowContainerDivs(CONTAINER_PANELS.CurrentPlanDetails);
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error.", updContainer, DIALOG_TYPE.Error);
            }
        }
        void setTextOfMinNoOfUserByPlanType(PLAN_TYPE planType)
        {
            switch (planType)
            {
                case PLAN_TYPE.Purchased:
                    labelMinUsers.Text = "Min Users";
                    break;
                case PLAN_TYPE.Trial:
                    labelMinUsers.Text = "Max Users";
                    break;
            }
        }
        protected void ddlPurchaseCurrencyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (hfdPlanCode.Value == "")
            {
                hideShowContainerDivs(CONTAINER_PANELS.PurchasePlan);
                defualtsOfPurchaseDivPlanDetail();
                hideShowCalculateTotalLinkUsingJavascript(true, updContainer);
                return;
            }
            string strPricePerUserPerMonth;
            DataTable dtblPlanDetails = getPlanDetails(hfdPlanCode.Value, ddlPurchaseCurrencyType.SelectedValue, Convert.ToInt32(ddlPurchaseMonths.SelectedValue), out strPricePerUserPerMonth);
            if (dtblPlanDetails != null)
            {
                lblPurchasePrice.Text = strPricePerUserPerMonth;
                lblPurchaseCurrency.Text = ddlPurchaseCurrencyType.SelectedValue;
                hideShowContainerPanelsUsingJavascript(updContainer, CONTAINER_PANELS.PurchasePlan);
            }
            else
            {
                Utilities.showMessage("Internal server error.Please try again", updContainer, DIALOG_TYPE.Error);
                hideShowContainerDivs(CONTAINER_PANELS.PurchasePlan);
            }
            hideShowContainerDivs(CONTAINER_PANELS.PurchasePlan);
        }

        void defualtsOfPurchaseDivPlanDetail()
        {
            lblPurchasePlanName.Text = "";
            lblPurchasePrice.Text = "please select a plan first";
            lblPurchaseCurrency.Text = "";
            setTextAndCmdArgumentOfLnkPurchase();
        }
        #endregion
        #endregion




    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
namespace mFicientCP
{
    public partial class companyDetails : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;
            if (!Page.IsPostBack)
            {
                fillCompanyDetails(null);
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, "loadCompanyLogo();");
                    return;
                }
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, String.Empty);
            }
        }

        private void fillCompanyDetails(UpdatePanel updUpdated)
        {
            GetCompanyDetails objCompanyDtls = new GetCompanyDetails(hfsPart4, hfsPart3);
            objCompanyDtls.Process();
            if (objCompanyDtls.StatusCode == 0)
            {
                if (objCompanyDtls.ResultTable.Rows.Count > 0)
                {
                    lblCompIdValue.Text = objCompanyDtls.CompanyId;
                    lblCompNameValue.Text = objCompanyDtls.CompanyName;
                    lblAddressValue.Text = objCompanyDtls.Address;
                    if (!string.IsNullOrEmpty(objCompanyDtls.CompanyLogoName))
                    {
                        lblLogoValue.Text = objCompanyDtls.CompanyLogoName;
                      
                        setHidFieldLogoUrl(Utilities.getCompanyLogoUrl(hfsPart3, objCompanyDtls.CompanyLogoName));
                        System.Drawing.Size size = GetSize(Utilities.getCompanyLogoUrl(hfsPart3, objCompanyDtls.CompanyLogoName));
                        setHidFieldLogoDimensions("{\"Width\"" + ":" + size.Width + ",\"Height\"" + ":" + size.Height + "}");
                        setHidFieldIsUploadNewOrEdit(true);
                       
                        showOrHideViewChangeButtonsAndAddButton(true);
                        lblLogoValue.Visible = false;
                       
                    }
                    else
                    {
                        lblLogoValue.Text = "No logo has been uploaded yet";
                        lblLogoValue.Visible = true;
                        showOrHideViewChangeButtonsAndAddButton(false);
                        setHidFieldIsUploadNewOrEdit(false);
                    }
                    
                }
                if (objCompanyDtls.ResultTable1.Rows.Count > 0)
                {
                    string completname = (string)objCompanyDtls.ResultTable1.Rows[0]["FIRST_NAME"] + " " + (string)objCompanyDtls.ResultTable1.Rows[0]["MIDDLE_NAME"] + " " + (string)objCompanyDtls.ResultTable1.Rows[0]["LAST_NAME"];
                    lbladminname.Text = completname;
                    lbladminemail.Text = (string)objCompanyDtls.ResultTable1.Rows[0]["EMAIL_ID"];
                    lblmobile.Text = (string)objCompanyDtls.ResultTable1.Rows[0]["MOBILE"];
                    txtfirstname.Text = (string)objCompanyDtls.ResultTable1.Rows[0]["FIRST_NAME"];
                    txtmiddlename.Text = (string)objCompanyDtls.ResultTable1.Rows[0]["MIDDLE_NAME"];
                    txtlastname.Text = (string)objCompanyDtls.ResultTable1.Rows[0]["LAST_NAME"];
                    txtmobile.Text = (string)objCompanyDtls.ResultTable1.Rows[0]["MOBILE"];
                    txtEmail.Text = (string)objCompanyDtls.ResultTable1.Rows[0]["EMAIL_ID"];
                    hidadmincomplete.Value = txtfirstname.Text + "," + txtmiddlename.Text + "," + txtlastname.Text + "," + txtmobile.Text + "," + txtEmail.Text;

                }
                else
                {
                }
            }
        }


        private System.Drawing.Size GetSize(string imageUrl)
        {
            var request = System.Net.WebRequest.Create(imageUrl);

            request.Method = "GET";
            //request.Accept = "image/jpeg";
            try
            {
                using (var response = (System.Net.HttpWebResponse)request.GetResponse())
                {
                    using (var s = response.GetResponseStream())
                    {
                        var bmp = new System.Drawing.Bitmap(s);
                        var size = new System.Drawing.Size(bmp.Width, bmp.Height);

                        return size;
                    }
                }
            }
            catch
            {
                System.Drawing.Size objNewSize = new System.Drawing.Size();
                objNewSize.Height = 0;
                objNewSize.Width = 0;
                return objNewSize;
            }

        }
        private void showOrHideViewChangeButtonsAndAddButton(bool isViewChangeVisible)
        {
            if (isViewChangeVisible)
            {
                //lnkViewLogo.Visible = true;
                lnkChangeLogo.Visible = true;
                lnkAddLogo.Visible = false;
                phLnkViewAndChange.Visible = true;
                phLnkAddLogo.Visible = false;
            }
            else
            {
                //lnkViewLogo.Visible = false;
                lnkChangeLogo.Visible = false;
                lnkAddLogo.Visible = true;
                phLnkAddLogo.Visible = true;
                phLnkViewAndChange.Visible = false;
            }
        }

        protected void btnImagePopUpClose_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "CloseModal", @"$('#divLogoModalContainer').dialog('close')", true);
        }

        protected void UpdateAdmin_Click(object sender, EventArgs e)
        {
            string strErrorMsg = string.Empty;
            if (!ValidateAdmin(out strErrorMsg))
            {
                string strDialogImageType = "DialogType.Error";
                string strOptionForScript = "$.alert.Error";
                string strScript = @"$.alert(" + "'" + strErrorMsg + "'" + "," + strOptionForScript + ");showDialogImage(" + strDialogImageType + ");openUpdateAdmin();";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Error", strScript, true);
                return;
            }
            else
            {
                UpdateAdmin(txtfirstname.Text, txtmiddlename.Text, txtlastname.Text, txtEmail.Text, txtmobile.Text, hfsPart3, hfsPart4);
            }

        }


        public bool ValidateAdmin(out string strError)
        {
            strError = "";
            bool isValid = true;
            if (txtfirstname.Text.Trim().Length == 0)
            {
                strError += " * Please enter first name.</br>";
            }
            if (txtEmail.Text.Trim().Length == 0)
            {
                strError += " * Please enter email id.</br>";
            }
            bool isEmail = Regex.IsMatch(txtEmail.Text, @"\A(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?)\Z");
            if (!isEmail)
            {
                strError += " * Please enter correct email id.</br>";
            }
            if (txtmobile.Text.Trim().Length == 0)
            {
                strError += " * Please enter mobile no </br>";
            }
            if (!String.IsNullOrEmpty(validateMobileNo(txtmobile.Text)))
            {
                strError += " * Please enter correct mobile no.</br>";
            }
            if (strError.Length > 0)
            {
                isValid = false;
            }
            return isValid;
        }

        string  validateMobileNo(string mobileNo)
        {
            string strErrorMsg = "Invalid mobile number.";
            try
            {
                if (mobileNo.Length < 7 || mobileNo.Length > 20) throw new MficientException(strErrorMsg);
                char chrFirstChar = Convert.ToChar(mobileNo.Substring(0, 1));
                if (chrFirstChar != '+') throw new MficientException(strErrorMsg);
                char chrSecondChar = Convert.ToChar(mobileNo.Substring(1, 1));
                int iSecondChar = 0;
                try
                {
                    //check if it is a number.
                    iSecondChar = int.Parse(chrSecondChar.ToString());
                }
                catch
                {
                    throw new MficientException(strErrorMsg);
                }
                if (iSecondChar == 0) throw new MficientException(strErrorMsg);
                string strNoFromThirdChar = mobileNo.Substring(2, mobileNo.Length - 2);
                try
                {
                    ulong.Parse(strNoFromThirdChar);
                }
                catch
                {
                    throw new MficientException(strErrorMsg);
                }

                strErrorMsg = String.Empty;
                return strErrorMsg;
            }
            catch
            {
                return strErrorMsg;
            }
        }



        private void UpdateAdmin(string firstName, string middlename, string lastname, string Email, string mobile,string companyId,string adminId)
        {
            try
            {
              UpdatedAdminDetails objUpdateWsConnection = new UpdatedAdminDetails(firstName, middlename, lastname, Email, mobile, companyId, adminId);
               string  strDialogImageType,strOptionForScript, msg, strScript;
                if (objUpdateWsConnection.StatusCode == 0)
                {
                    strDialogImageType = "DialogType.Info";
                    strOptionForScript = "$.alert.Information";
                    msg = "Admin Updated successfully.";
                     strScript = @"$.alert(" + "'" + msg + "'" + "," + strOptionForScript + ");showDialogImage(" + strDialogImageType + ");";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "Info", strScript, true);
                 
                    fillCompanyDetails(null);
                }
                else
                {
                    strDialogImageType = "DialogType.Error";
                    strOptionForScript = "$.alert.Error";
                    msg = "Internal Server Error.";
                   strScript = @"$.alert(" + "'" + msg + "'" + "," + strOptionForScript + ");showDialogImage(" + strDialogImageType + ");";
                   ScriptManager.RegisterStartupScript(this, typeof(Page), "Error", strScript, true);

                }
            }
            catch (Exception ex)
            {
               string strDialogImageType = "DialogType.Error";
               string strOptionForScript = "$.alert.Error";
                string strScript = @"$.alert(" + "'" + ex + "'" + "," + strOptionForScript + ");showDialogImage(" + strDialogImageType + ");";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Error", strScript, true);
            }

        }

       


        private void showAlert(string message, string divIdToShowMsg)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");", true);
        }

        private void closeModalPopUp()
        {
            clearControlsUsedInEdit();
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "CloseModal", @"$('#divModalContainer').dialog('close')", true);
        }



        private void clearControlsUsedInEdit()
        {
            hidDesignationIdForEdit.Value = "";
            hidPopUpMode.Value = "";
        }

        protected void btnUpload_Click1(object sender, EventArgs e)
        {
            Button btnremove = (Button)sender;
            string msg;
            string strNewImageName;
            
            if (hidUploadTypeNewOrEdit.Value.ToLower() == "New".ToLower())
            {
                try
                {
                    int iError = processUploadImage(String.Empty, out msg, out strNewImageName);
                    string strDialogImageType = "";
                    string strOptionForScript = "";
                    if (iError == 0)
                    {
                        showOrHideViewChangeButtonsAndAddButton(true);
                        strDialogImageType = "DialogType.Info";
                        strOptionForScript = "$.alert.Information";
                        setHidFieldLogoUrl(Utilities.getCompanyLogoUrl(hfsPart3, strNewImageName));
                    }
                    else
                    {
                        strDialogImageType = "DialogType.Error";
                        strOptionForScript = "$.alert.Error";
                    }
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "uploadImageError2", @"$.alert(" + "'" + msg + "'" + "," + strOptionForScript + ");showDialogImage(" + strDialogImageType + ");loadCompanyLogo();", true);
                }
                catch
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "uploadImageError1", @"$.alert(" + "'" + "Internal server error.Please try again." + "'" + "," + "$.alert.Error" + ");showDialogImage(" + "DialogType.Error" + ");loadCompanyLogo();", true);
                }
            }
            else
            {
                try
                {
                    string strPreviousImage = lblLogoValue.Text;

                    int iError = processUploadImage(strPreviousImage, out msg, out strNewImageName);
                    if (iError == 0)
                    {
                        setHidFieldLogoUrl(Utilities.getCompanyLogoUrl(hfsPart3, strNewImageName));
                        removeUploadedFile(strPreviousImage, Server.MapPath("~\\CompanyImages\\" + hfsPart3 + "\\logo\\"));
                        if (upImage.HasFile)
                        {
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "uploadImage", @"$.alert(" + "'" + "Image uploaded successfully." + "'" + "," + "$.alert.Information" + ");showDialogImage(" + "DialogType.Info" + ");loadCompanyLogo();", true);
                        }
                        else
                        {
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "uploadImage", @"$.alert(" + "'" + "Image removed successfully." + "'" + "," + "$.alert.Information" + ");showDialogImage(" + "DialogType.Info" + ");loadCompanyLogo();", true);
                        }


                    }
                    else
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "uploadImageError", @"$.alert(" + "'" + msg + "'" + "," + "$.alert.Error" + ");showDialogImage(" + "DialogType.Error" + ");loadCompanyLogo();", true);
                    }
                }
                catch
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "uploadImageError", @"$.alert(" + "'" + "Internal server error.Please try again." + "'" + "," + "$.alert.Error" + ");showDialogImage(" + "DialogType.Error" + ");loadCompanyLogo();", true);
                }
            }
          
           
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearControlsUsedInEdit();
            closeModalPopUp();
        }
        void setHidFieldLogoUrl(string value)
        {
            hidCompanyLogoUrl.Value = value;
        }
        void setHidFieldLogoDimensions(string value)
        {
            hidCmpLogoDim.Value = value;
        }
        void setHidFieldIsUploadNewOrEdit(bool isEdit)
        {
            if (isEdit)
            {
                hidUploadTypeNewOrEdit.Value = "Edit";
            }
            else
            {
                hidUploadTypeNewOrEdit.Value = "New";
            }
        }
        protected void lnkChangeLogo_Click(object sender, EventArgs e)
        {
            btnUpload.CommandArgument = "Edit";
            Utilities.showModalPopup("divImageUploadModal", updCompanyDetails, "show image pop up modal", "Change Company Logo", "350", true, false);
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            Button btnUpload = (Button)sender;
            string msg;
            string strNewImageName;
            if (upImage.HasFile)
            {
                if (hidUploadTypeNewOrEdit.Value.ToLower() == "New".ToLower())
                {
                    try
                    {
                        int iError = processUploadImage(String.Empty, out msg, out strNewImageName);
                        string strDialogImageType = "";
                        string strOptionForScript = "";
                        if (iError == 0)
                        {
                            showOrHideViewChangeButtonsAndAddButton(true);
                            strDialogImageType = "DialogType.Info";
                            strOptionForScript = "$.alert.Information";
                            setHidFieldLogoUrl(Utilities.getCompanyLogoUrl(hfsPart3, strNewImageName));
                        }
                        else
                        {
                            strDialogImageType = "DialogType.Error";
                            strOptionForScript = "$.alert.Error";
                        }
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "uploadImageError2", @"$.alert(" + "'" + msg + "'" + "," + strOptionForScript + ");showDialogImage(" + strDialogImageType + ");loadCompanyLogo();", true);
                    }
                    catch
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "uploadImageError1", @"$.alert(" + "'" + "Internal server error.Please try again." + "'" + "," + "$.alert.Error" + ");showDialogImage(" + "DialogType.Error" + ");loadCompanyLogo();", true);
                    }
                }
                else
                {
                    try
                    {
                        string strPreviousImage = lblLogoValue.Text;

                        int iError = processUploadImage(strPreviousImage, out msg, out strNewImageName);
                        if (iError == 0)
                        {
                            setHidFieldLogoUrl(Utilities.getCompanyLogoUrl(hfsPart3, strNewImageName));
                            removeUploadedFile(strPreviousImage, Server.MapPath("~\\CompanyImages\\" + hfsPart3 + "\\logo\\"));
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "uploadImage", @"$.alert(" + "'" + "Image uploaded successfully." + "'" + "," + "$.alert.Information" + ");showDialogImage(" + "DialogType.Info" + ");loadCompanyLogo();", true);
                        }
                        else
                        {
                            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "uploadImageError", @"$.alert(" + "'" + msg + "'" + "," + "$.alert.Error" + ");showDialogImage(" + "DialogType.Error" + ");loadCompanyLogo();", true);
                        }
                    }
                    catch
                    {
                        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "uploadImageError", @"$.alert(" + "'" + "Internal server error.Please try again." + "'" + "," + "$.alert.Error" + ");showDialogImage(" + "DialogType.Error" + ");loadCompanyLogo();", true);
                    }
                }
            }
            else
            {
                if (btnUpload.CommandArgument.ToLower() == "new")
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID
                        , @"showModalPopUpWithOutHeader('divImageUploadModal','Add Company Logo',350,false);
                            $.alert(" + "'" + "Please select an image" + "'" + "," + "$.alert.Error" + ");showDialogImage(" + "DialogType.Error" + ");loadCompanyLogo();", true);
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID
                        , @"showModalPopUpWithOutHeader('divImageUploadModal','Change Company Logo',350,false);
                            $.alert(" + "'" + "Please select an image" + "'" + "," + "$.alert.Error" + ");showDialogImage(" + "DialogType.Error" + ");loadCompanyLogo();", true);
                }
            }
        }

        private int processUploadImage(string oldLogoName, out string msg, out string newLogoName)
        {
            DirectoryInfo dirInfo;
            int iError = 0;
            msg = "";
            string strUploadingFolderPath = Server.MapPath("~\\CompanyImages");
            string strCompanyFolderPath = Server.MapPath("~\\CompanyImages\\" + hfsPart3);
            string strCompanyLogoFolderPath = Server.MapPath("~\\CompanyImages\\" + hfsPart3 + "\\logo");

            if (!imageUploadFolderExists(strUploadingFolderPath))
            {
                dirInfo = new DirectoryInfo(strUploadingFolderPath);
                dirInfo.Create();
            }
            if (!companyImageFolderExists(strCompanyFolderPath))
            {
                dirInfo = new DirectoryInfo(strCompanyFolderPath);
                dirInfo.Create();
            }
            if (!companyImageFolderExists(strCompanyLogoFolderPath))
            {
                dirInfo = new DirectoryInfo(strCompanyLogoFolderPath);
                dirInfo.Create();
            }
            string strNewLogoName;
            if (!String.IsNullOrEmpty(oldLogoName))
            {
                strNewLogoName = getNewImageNameIfNameSimilarToOldImageNameRemove(
                    oldLogoName,
                    upImage.FileName);
            }
            else
            {
                strNewLogoName = upImage.FileName;
            }
            newLogoName = strNewLogoName;
            uploadLogo(strCompanyLogoFolderPath, strNewLogoName, out msg);

            if (String.IsNullOrEmpty(msg))
            {
                iError = saveLogoInfo(strNewLogoName, strCompanyLogoFolderPath, out msg);
            }
            else
            {
                //Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, @"$.wl_Alert(" + "'" + strErrorMessage + "'" + ",'info','#" + "divAlert" + "'" + ");", true);
                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "uploadImageError1",
                //    @"$.alert(" + "'" + msg + "'" + "," + "$.alert.Error" + ");showDialogImage(" + "DialogType.Error" + ");loadCompanyLogo();", true);
                iError = -1000;
            }
            return iError;
        }

        private bool imageUploadFolderExists(string uploadingFolderPath)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(uploadingFolderPath);
            if (dirInfo.Exists)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private bool companyImageFolderExists(string companyFolderPath)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(companyFolderPath);
            if (dirInfo.Exists)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void uploadLogo(string folderPath, string logoName, out string errorMessage)
        {
            //logoName = "";
            errorMessage = "";
            if (upImage.HasFile)
            {
                int imageSize = 1000;
                string strError = "";
                if (upImage.PostedFile.ContentLength > (500 * 1024))
                {
                    strError += "Image size is greater than 500 KB." + "</br>";
                }
                using (Stream memStream = new MemoryStream(upImage.FileBytes))
                {
                    using (System.Drawing.Image img = System.Drawing.Image.FromStream(memStream))
                    {
                        if (img.Width > imageSize && img.Height > imageSize)
                        {
                            strError += "Max dimensions allowed for image " + imageSize + " * " + imageSize + "</br>";
                        }
                        else if (img.Height > imageSize && img.Width < imageSize)
                        {
                            strError += "Max dimensions allowed for image " + imageSize + " * " + imageSize + "</br>";
                        }
                        else if (img.Height < imageSize && img.Width > imageSize)
                        {
                            strError += "Max dimensions allowed for image " + imageSize + " * " + imageSize + "</br>";
                        }
                    }
                }
                if (!CheckFileType(upImage.FileName))
                {
                    strError += "only .jpg/.png files allowed" + "</br>";
                }
                if (strError != "")
                {
                    errorMessage = strError;
                }
                else
                {
                    upImage.SaveAs(folderPath + "\\" + logoName);
                }
            }
        }


        string getNewImageNameIfNameSimilarToOldImageNameRemove(string oldImageName, string newImageName)
        {
            string strOldImageName = Path.GetFileNameWithoutExtension(oldImageName);
            string strNewImageName = "";
            if (strOldImageName == strNewImageName)
                strNewImageName = strNewImageName + "1";
            return strNewImageName + Path.GetExtension(newImageName);
        }


        private bool CheckFileType(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            switch (ext.ToLower())
            {
                case ".png":
                    return true;
                case ".jpg":
                    return true;
                default:
                    return false;
            }
        }
        private int saveLogoInfo(string logoName, string filePath, out string msg)
        {
            int iError = 0;
            msg = "";
            UpdateCompanyLogo objUpdateLogo = new UpdateCompanyLogo(hfsPart3, logoName);
            objUpdateLogo.Process();
            if (objUpdateLogo.StatusCode == 0)
            {
                fillCompanyDetails(updCompanyImgUpload);
                msg = "Image Uploaded successfully";
                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "uploadImage", @"$.alert(" + "'" + "Image uploaded successfully" + "'" + "," + "$.alert.Information" + ");showDialogImage(" + "DialogType.Info" + ");", true);
            }
            else
            {
                removeUploadedFile(logoName, filePath);
                msg = "Internal server error";
                //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "uploadImage", @"$.alert(" + "'" + "Image uploaded successfully" + "'" + "," + "$.alert.Information" + ");showDialogImage(" + "DialogType.Info" + ");", true);
                iError = -1000;
            }
            return iError;
        }

        private void removeUploadedFile(string logoName, string companyLogoFolderPath)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(companyLogoFolderPath);
            if (dirInfo.Exists)
            {
                FileInfo[] flInfo = dirInfo.GetFiles(logoName, SearchOption.TopDirectoryOnly);
                if (flInfo.Length > 0)
                {
                    flInfo[0].Delete();
                }
            }
        }

        string getNewImageNameIfNameSimilarToOldImageName(
            string oldImageName,
            string newImageName)
        {
            if (String.IsNullOrEmpty(oldImageName) || String.IsNullOrEmpty(newImageName))
                throw new ArgumentNullException();
            string strOldImageName = Path.GetFileNameWithoutExtension(oldImageName);
            string strNewImageName = Path.GetFileNameWithoutExtension(newImageName);
            if (strOldImageName == strNewImageName)
            {
                strNewImageName = strNewImageName + "1";
            }
            return strNewImageName + Path.GetExtension(newImageName);
        }

        protected void lnkCancelUpload_Click(object sender, EventArgs e)
        {
            showOrHideViewChangeButtonsAndAddButton(true);
        }


        protected void lnkAddLogo_Click(object sender, EventArgs e)
        {
            Utilities.showModalPopup("divImageUploadModal", updCompanyDetails, "Add Company Logo", "400", true);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;
using System.Data;

namespace mFicientCP
{
    public partial class deviceLocationTrack : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
               
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }
            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!Page.IsPostBack)
            {
                for (int i = 0; i <= 23; i++)
                {
                    string str = i < 10 ? "0" + i.ToString() : i.ToString();
                    ddlToHr.Items.Add(str.ToString());
                    ddlToMin.Items.Add(str.ToString());
                    ddlFoHr.Items.Add(str.ToString());
                    ddlFoMin.Items.Add(str.ToString());
                }
                for (int i = 24; i <= 59; i++)
                {
                    ddlToMin.Items.Add(i.ToString());
                    ddlFoMin.Items.Add(i.ToString());
                }
                SetObjectListInHiddenfieldAndBind();
                getTraceLocationJson();
                getAdditionalProperties();
                bindCompanyGroupsdetails();
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                }
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, @" $(""input"").uniform(); setTraceLocation(true);", true);
        }
        void bindCompanyGroupsdetails()
        {
            GetGroupDetails objgroupUsers = new GetGroupDetails(hfsPart3);
            objgroupUsers.Process();
            if (objgroupUsers.StatusCode == 0)
            {
                if (objgroupUsers.ResultTable != null && objgroupUsers.ResultTable.Rows.Count > 0)
                {
                    List<GroupDetails> lstUserDetailResponse = new List<GroupDetails>();
                    foreach (DataRow user in objgroupUsers.ResultTable.Rows)
                    {
                        GroupDetails objUserDetailMembers = new GroupDetails();
                        objUserDetailMembers.id = Convert.ToString(user["GROUP_ID"]);
                        objUserDetailMembers.name = Convert.ToString(user["GROUP_NAME"]);
                        objUserDetailMembers.fnm = Convert.ToString(user["GROUP_NAME"]);
                        objUserDetailMembers.lnm = Convert.ToString(user["GROUP_NAME"]);
                        lstUserDetailResponse.Add(objUserDetailMembers);
                    }
                    hidgrouplist.Value = Utilities.SerializeJson<List<GroupDetails>>(lstUserDetailResponse);
                }
            }
        }
        string getJsonOfAllDbCommands()
        {
            string strJson = "";
            GetDbCommand objDbCommand = new GetDbCommand(true, false, "", "", "", hfsPart3);
            DataTable dtblDbCmds = objDbCommand.ResultTable;
            strJson = Utilities.SerializeJson<List<IdeDbCmdsForJson>>(objDbCommand.getCommondDetail());
            return strJson;
        }
        string getJsonOfAllWsCommands()
        {
            GetWsCommand objWsCommand = new GetWsCommand(true, false, "", "", "", hfsPart3);
            objWsCommand.Process();
            return Utilities.SerializeJson<List<IdeWsCmdsForJson>>(objWsCommand.GetWsCommandDetail());
        }
        string getJsonOfAllOdataCommands()
        {
            GetOdataCommand objOdataCmds = new GetOdataCommand(true, false, "", "", "", hfsPart3);
            return Utilities.SerializeJson<List<IdeOdataCmdsForJson>>(objOdataCmds.getOdataCommandDetail());
        }
        void SetObjectListInHiddenfieldAndBind()
        {
            hidAllDbCommands.Value = getJsonOfAllDbCommands();
            hidAllWsCommands.Value = getJsonOfAllWsCommands();
            hidAllOdataCommands.Value = getJsonOfAllOdataCommands();
        }

        private void getTraceLocationJson()
        {
            GetEnterpriseAdditionalDefinition objGetWsCommand = new GetEnterpriseAdditionalDefinition(hfsPart3);
            if (objGetWsCommand.StatusCode == 0)
            {
                if (objGetWsCommand.ResultTable != null)
                {
                    if (objGetWsCommand.ResultTable.Rows.Count > 0)
                    {
                        string _traceLocationJson =Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["TRACE_LOCATION"]);
                        if (!string.IsNullOrEmpty(_traceLocationJson))
                        {
                            JObject jo = JObject.Parse(_traceLocationJson);
                            hidTrackLocDetail.Value = _traceLocationJson;
                        }
                        else
                        {
                            hidTrackLocDetail.Value = "{}";
                        }

                    }
                }
                else
                {
                    hidTrackLocDetail.Value = "{}";
                }
            }
            
        }
        protected void getAdditionalProperties()
        {
            GetEnterpriseAdditionalDefinition objAdditionalInfoDefs = new GetEnterpriseAdditionalDefinition(hfsPart3);
            string strAdditionalDtls = objAdditionalInfoDefs.GetUserAdditionalProperties();
            JArray ja = new JArray();                    
            if (!String.IsNullOrEmpty(strAdditionalDtls))
            {
                List<string> lstAdditionalDtls = Utilities.DeserialiseJson<List<string>>(strAdditionalDtls);
                if (lstAdditionalDtls.Count > 0)
                {
                    foreach (string addDtl in lstAdditionalDtls)
                    {
                        if (!String.IsNullOrEmpty(addDtl))
                        {
                            JObject jo=new JObject();
                            jo.Add("prop", addDtl);
                            jo.Add("val", "@" + addDtl);
                            ja.Add(jo);
                        }
                    }
                }
            }
            hidPropperties.Value = ja.ToString();
        }
        protected void btnSaveTrackLocation_Click(object sender, EventArgs e)
        {
            string strJSON = hidTrackLocDetail.Value;
            if (strJSON.Length>0)
            {
                SaveEnterpriseAdditionalDefinition obj = new SaveEnterpriseAdditionalDefinition(hfsPart3, strJSON, EnterpriseAdditionalDefinitionType.TraceDevice);
            }
            hidTrackLocDetail.Value = "";

        }
        protected void btnResetTrackLocation_Click(object sender, EventArgs e)
        {
            SaveEnterpriseAdditionalDefinition obj = new SaveEnterpriseAdditionalDefinition(hfsPart3, "", EnterpriseAdditionalDefinitionType.TraceDevice);
            hidTrackLocDetail.Value = "";
            ScriptManager.RegisterStartupScript(updTrackLoc, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"setTraceLocation(false);", true);
        }
    }
}
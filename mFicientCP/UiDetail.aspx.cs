﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace mFicientCP
{
    public partial class UiDetail : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
           
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                bool isPreviousPageIDE = false;
                if (((System.Web.UI.TemplateControl)(Page.PreviousPage)).AppRelativeVirtualPath == "~/ide.aspx")
                {
                    isPreviousPageIDE = true;
                }
                if (isPreviousPageIDE)
                {
                    hfsValue = ((HiddenField)previousPageForm.FindControl("MainCanvas").FindControl("hfs")).Value;
                    hfbidValue = ((HiddenField)previousPageForm.FindControl("MainCanvas").FindControl("hfbid")).Value;
                }
                else
                {
                    hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                    hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
                }
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!String.IsNullOrEmpty(hfsValue.Trim()))
                getUIDetailCounts();
        }

        void getUIDetailCounts()
        {
            GetUIDetailCounts objUIDtlsCount = new GetUIDetailCounts(strUserId, hfsPart4);
            objUIDtlsCount.Process();
            DataSet dsUIDtlsCount = objUIDtlsCount.ResultTables;
            if (objUIDtlsCount.StatusCode == 0)
            {
                showDataInControls(objUIDtlsCount);
                readDescriptionXMLAndShowDataInControls();
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, @"$.alert(" + "'" + "Internal server error" + "'" + "," + "$.alert.Error" + ");showDialogImage(" + "DialogType.Error" + ");", true);
            }
        }

        void showDataInControls(GetUIDetailCounts uiDetailCount)
        {
            lblApplicationsCount.Text = Convert.ToString(uiDetailCount.ApplicationCount);
            lblDatabaseConnCount.Text = Convert.ToString(uiDetailCount.DatabaseConnectionCount); ;
            lblWebServiceConnCount.Text = Convert.ToString(uiDetailCount.WebServiceConnectionCount);
            lblWSCommCount.Text = Convert.ToString(uiDetailCount.WebServiceCommandCount);
            lblDBCommCount.Text = Convert.ToString(uiDetailCount.DatabaseCommandCount);
        }
        void readDescriptionXMLAndShowDataInControls()
        {
            string xmlHelpDocPath = Server.MapPath("~/xml/UIDetailsText.xml");
            DataSet ds = new DataSet();
            try
            {
                ds.ReadXml(xmlHelpDocPath);
            }
            catch
            {

            }
            if (ds.Tables.Count != 0)
            {
                if (ds.Tables[0].Rows.Count != 0)
                {
                    try
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            Label lblFound = (Label)this.Master.Master.FindControl("MainCanvas").FindControl("PageCanvas").FindControl("lbl" + (string)row["id"]);
                            lblFound.Text = (string)row["hp_Text"];
                        }

                    }
                    catch
                    {

                    }
                }
            }
        }

    }
}
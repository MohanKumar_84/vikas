﻿<%@ Page Title="mFicient | mPlugin Manager" Language="C#" MasterPageFile="~/master/Canvas.master"
    AutoEventWireup="true" CodeBehind="manageMpluginAgents.aspx.cs" Inherits="mFicientCP.manageMpluginAgents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script type="text/javascript">
        function showHidePanelsForProcess(processFor) {
            var pnlAddMPluginAgent = $('#' + '<%= pnlAddNewAgent.ClientID %>');
            var pnlEditMpluginAgent = $('#' + '<%= pnlUpdateAgent.ClientID%>');
            if (processFor.toLowerCase() == "add".toLowerCase()) {
                if (pnlAddMPluginAgent) {
                    $(pnlAddMPluginAgent).show();
                }
                if (pnlEditMpluginAgent) {
                    $(pnlEditMpluginAgent).hide();
                }

            }
            else {
                if (pnlAddMPluginAgent) {
                    $(pnlAddMPluginAgent).hide();
                }
                if (pnlEditMpluginAgent) {
                    $(pnlEditMpluginAgent).show();
                }
            }
        }
        function clearControlsUsedInEdit() {
            var txtAgentName = $('#' + '<%=txtAgentName.ClientID %>');
            var hidMPAgntID = $('#' + '<%=hidmPluginAgentIdForEdit.ClientID %>');
            var hidPopUpMode = $('#' + '<%=hidPopUpMode.ClientID %>');
            var txtAgentPassword = $('#' + '<%=txtAgentPassword.ClientID %>');
            var txtAddAgentPassword = $('#' + '<%=txtAgentPass.ClientID %>');
            var txtAddAgentRePassword = $('#' + '<%=txtRePassword.ClientID %>');
            if (txtAgentName) $(txtAgentName).val('');
            if (hidMPAgntID) $(hidMPAgntID).val('');
            if (hidPopUpMode) $(hidPopUpMode).val('');
            if (txtAgentPassword) $(txtAgentPassword).val('');
            if (txtAddAgentPassword) $(txtAddAgentPassword).val('');
            if (txtAddAgentRePassword) $(txtAddAgentRePassword).val('');
            $('#divModalContainer').find('.alert').remove();
        }
        function showControlsForNewAddition() {
            showHidePanelsForProcess('add');
            //showHideButtonsForProcess('add');
            clearControlsUsedInEdit();
            showModalPopUpWithOutHeader('divModalContainer', 'Add mPlugin Agent', '300', false);
        }
        function doCancelForm() {
            clearControlsUsedInEdit();
            closeModalPopUp('divModalContainer');
            return false;
        }
        function confirmDeleteOfAgent() {
            var confirmResult = confirm('Are you sure you want to delete the mPlugin agent');
            if (confirmResult) {
                isCookieCleanUpRequired('false');
                return true;
            }
            else {
                return false;
            }
        }
        function callValidationByType() {
            var hidAgentId = $('#' + '<%=hidmPluginAgentIdForEdit.ClientID %>');
            if ($(hidAgentId).val() === "") {
                return validateAddNewFields();
            }
            else {
                return validateChangePasswordFields();
            }
        }
        function validateChangePasswordFields() {
            var txtAgentPassword = $('#' + '<%=txtAgentPassword.ClientID %>');
            var txtReAgentPassword = $('#' + '<%=txtReAgentPassword.ClientID %>');
            var strErrorMsg = "";

            if ($(txtAgentPassword).val() === "" || $(txtAgentPassword).val().length < 6) {
                strErrorMsg += "Please enter valid password.<br/>";
            }
            else if ($(txtReAgentPassword).val() === ""
                    || $(txtReAgentPassword).val().length < 6
                     || $(txtReAgentPassword).val() != $(txtAgentPassword).val()
                ) {
                strErrorMsg += "Password entered does not match <br/>";
            }
            if (strErrorMsg !== "") {
                showAlert(strErrorMsg, '#divMPluginContError');
                return false;
            }
            else {
                return true;
            }
        }
        function validateAddNewFields() {
            var txtAgentName = $('#' + '<%=txtAgentName.ClientID %>');
            var txtAgentPass = $('#' + '<%=txtAgentPass.ClientID %>');
            var txtRePassword = $('#' + '<%=txtRePassword.ClientID %>');
            var strErrorMsg = "";
            if ($(txtAgentName).val() === "" || $(txtAgentName).val().length < 3) {
                strErrorMsg += "Please enter a valid agent name.<br/>";
            }
            if ($(txtAgentPass).val() === "" || $(txtAgentPass).val().length < 6) {
                strErrorMsg += "Please enter valid password.<br/>";
            }
            else if ($(txtRePassword).val() === ""
                    || $(txtRePassword).val().length < 6
                     || $(txtRePassword).val() != $(txtAgentPass).val()
                ) {
                strErrorMsg += "Password entered does not match<br/>";
            }
            if (strErrorMsg !== "") {
                showAlert(strErrorMsg, '#divMPluginContError');
                return false;
            }
            else {
                return true;
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="updRepeater" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                            <section>
                                <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                    <div>
                                        <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>mPlugin Agents Details</h1>"></asp:Label>
                                    </div>
                                    <div style="position: relative; top: 10px; right: 15px;" id="divAddNewAgent" runat="server">
                                        <asp:LinkButton ID="lnkAddNewMpluginAgent" runat="server" Text="add" CssClass="repeaterLink fr"
                                            OnClientClick="showControlsForNewAddition(); isCookieCleanUpRequired('false'); return false;"
                                            Visible="true"></asp:LinkButton>
                                    </div>
                                </asp:Panel>
                            </section>
                            <asp:Repeater ID="rptMpluginAgentsDtls" runat="server" OnItemCommand="rptMpluginAgentsDtls_ItemCommand">
                                <HeaderTemplate>
                                    <table class="repeaterTable">
                                        <thead>
                                            <tr>
                                                <th>
                                                    mPlugin Agent Name
                                                </th>
                                                <th>
                                                </th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tbody>
                                        <tr class="repeaterItem">
                                            <td style="display: none">
                                                <asp:Label ID="lblAgentPassword" runat="server" Text='<%# Eval("MP_AGENT_PASSWORD") %>'></asp:Label>
                                                <asp:Label ID="lblAgentId" runat="server" Text='<%# Eval("MP_AGENT_ID") %>'></asp:Label>
                                                <asp:Label ID="lblVersion" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAgentName" runat="server" Text='<%# Eval("MP_AGENT_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CssClass="repeaterLink" CommandName="Edit"
                                                    OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CssClass="repeaterLink"
                                                    CommandName="Delete" OnClientClick="isCookieCleanUpRequired('false'); return confirmDeleteOfAgent();"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </tbody>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tbody>
                                        <tr class="repeaterAlternatingItem">
                                            <td style="display: none">
                                                <asp:Label ID="lblAgentPassword" runat="server" Text='<%# Eval("MP_AGENT_PASSWORD") %>'></asp:Label>
                                                <asp:Label ID="lblAgentId" runat="server" Text='<%# Eval("MP_AGENT_ID") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblAgentName" runat="server" Text='<%# Eval("MP_AGENT_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CssClass="repeaterLink" CommandName="Edit"
                                                    OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CssClass="repeaterLink"
                                                    CommandName="Delete" OnClientClick="isCookieCleanUpRequired('false'); return confirmDeleteOfAgent();"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </tbody>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <div style="clear: both">
                                <asp:HiddenField ID="hidmPluginAgentIdForEdit" runat="server" />
                                <asp:HiddenField ID="hidPopUpMode" runat="server" />
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
    </div>
    <div id="divModalContainer">
        <div id="divMPluginContError">
        </div>
        <div id="divMPluginAgentContainer" class="manageGroupPopUpCont" style="display: block;
            width: 100%">
            <asp:UpdatePanel ID="updModalContainer" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlAddNewAgent" runat="server">
                        <div>
                            <label for="<%=txtAgentName.ClientID %>">
                                mPlugin Agent Name</label>
                            <div>
                                <asp:TextBox ID="txtAgentName" runat="server" Width="100%" MaxLength="20"></asp:TextBox>
                                <div style="font-style: italic; color: Gray;">
                                    ( min 3 characters)</div>
                            </div>
                        </div>
                        <div style="height: 5px;">
                        </div>
                        <div>
                            <label for="<%=txtAgentPass.ClientID %>">
                                mPlugin Agent Password</label>
                            <div>
                                <asp:TextBox ID="txtAgentPass" runat="server" MaxLength="20" Width="100%" TextMode="Password"></asp:TextBox>
                                <div style="font-style: italic; color: Gray;">
                                    ( min 6 and max 20 characters)</div>
                            </div>
                        </div>
                        <div style="height: 5px;">
                        </div>
                        <div>
                            <label for="<%=txtRePassword.ClientID %>">
                                Retype password</label>
                            <div>
                                <asp:TextBox ID="txtRePassword" runat="server" MaxLength="20" Width="100%" TextMode="Password"></asp:TextBox>
                                <div style="font-style: italic; color: Gray;">
                                </div>
                            </div>
                        </div>
                        <div style="height: 10px;">
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlUpdateAgent" runat="server">
                        <div style="margin: auto">
                            <div>
                                <label>
                                    Version
                                    <asp:Label ID="lblversion" runat="server"></asp:Label>
                                    <asp:Label ID="lblEditAgentName" runat="server" Visible="false" Text='<%# Eval("MP_AGENT_NAME") %>'></asp:Label>
                                </label>
                                <div style="height: 5px;">
                                </div>
                                <div style="margin: auto;">
                                    <div>
                                        <div style="border-bottom: 1px solid #AAAAAA; padding-bottom: 4px">
                                            <label style="font-family: Trebuchet MS">
                                                Change Password
                                            </label>
                                        </div>
                                        <div style="height: 5px;">
                                        </div>
                                        <div>
                                            <label for="<%=txtAgentPassword.ClientID %>">
                                                Enter new password
                                            </label>
                                            <div>
                                                <asp:TextBox ID="txtAgentPassword" runat="server" MaxLength="50" Width="97%" TextMode="Password"></asp:TextBox>
                                                <div style="font-style: italic; color: Gray;">
                                                    ( min 6 and max 20 characters)</div>
                                            </div>
                                        </div>
                                        <div style="height: 5px;">
                                        </div>
                                        <div>
                                            <label for="<%=txtReAgentPassword.ClientID %>">
                                                Retype password</label>
                                            <div>
                                                <asp:TextBox ID="txtReAgentPassword" runat="server" MaxLength="20" Width="97%" TextMode="Password"></asp:TextBox>
                                                <div style="font-style: italic; color: Gray;">
                                                </div>
                                            </div>
                                        </div>
                                        <div style="height: 10px;">
                                        </div>
                                        </div>
                                        </div>
                             </div>
                             </div>
                    </asp:Panel>
                    <div class="modalPopUpAction">
                        <div id="divPopUpActionCont" class="popUpActionCont" style="width: 59%;">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="aspButton" OnClick="btnSave_Click"
                                OnClientClick="return callValidationByType();" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="return doCancelForm();"
                                CssClass="aspButton" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <div id="divPostBackConfirm" style="display: none;">
        <asp:UpdatePanel ID="updConfirmation" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="modalPopUpConfirm">
                    <div>
                        <asp:Label ID="lblConfirmationMessage" runat="server" Text="Do you really like to confirm this"></asp:Label></div>
                    <div style="margin-top: 15px; margin-bottom: 5px; float: right;">
                        <div style="float: right;">
                            <span class="ui-button-text">
                                <asp:Button ID="btnConfirmYes" runat="server" Text='Yes' CssClass="aspButton" OnClick="btnConfirmYes_Click" />
                            </span>&nbsp; <span class="ui-button-text">
                                <asp:Button ID="btnConfirmNo" runat="server" Text='No' CssClass="aspButton" OnClick="btnConfirmNo_Click" />
                            </span>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            //$("input").uniform();
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            //$("input").uniform();
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>

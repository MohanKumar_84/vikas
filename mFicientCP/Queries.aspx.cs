﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;


namespace mFicientCP
{

    public partial class Queries : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        string hfsPart5 = string.Empty;
        string hfsPart6 = string.Empty;

        enum POST_BACK_TAB
        {
            MOBILE_USER_QUERIES = 0,
            MY_QUERIES = 1,
            RESOLVED_QUERIES = 2,
            CLOSED_QUERIES = 3
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            HttpContext context = HttpContext.Current;
            Literal ltFullNAME = (Literal)this.Master.Master.FindControl("ltFullNAME");
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];
            int lengthA = hfsParts.Length;
            if (lengthA > 4)
            {
                hfsPart5 = hfsParts[4];
                hfsPart6 = hfsParts[5];
                context.Items["hfs5"] = hfsPart5;
                context.Items["hfs6"] = hfsPart6;
                ltFullNAME.Text = hfsPart5 + " (" + hfsPart6 + ")";
            }

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;


            if (!Page.IsPostBack)
            {
                Utilities.runPageStartUpScript(this.Page, "RunWLDate", "$('#PageCanvasContent').find('input.date, div.date').wl_Date();");
                try
                {
                    bindQueryTypeDropDown();
                }
                catch
                { }
                try
                {
                    BindMBRepeater();
                    hideShowControlsByTabSelected(POST_BACK_TAB.MOBILE_USER_QUERIES);
                }
                catch
                {
                    showMessage(false, "Internal server error", null, DIALOG_TYPE.Error);
                }
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page,
                            @"$('body').find('input.date, div.date').wl_Date();
                            ");
                    try
                    {
                        bindQueryTypeDropDown();
                    }
                    catch
                    { }
                    try
                    {
                        BindMBRepeater();
                        hideShowControlsByTabSelected(POST_BACK_TAB.MOBILE_USER_QUERIES);
                    }
                    catch
                    {
                        showMessage(false, "Internal server error", null, DIALOG_TYPE.Error);
                    }
                }
                else
                {
                    Utilities.makeInputFieldsUniform(this.Page,
                        "SetUniformDesignAfterPostback1", true,
                        "$('#PageCanvasContent').find('input.date, div.date').wl_Date();makeTabAfterPostBack();showSelectedTabOnPostBack();");
                }
            }

            try
            {
                if (!String.IsNullOrEmpty(hfsValue))
                    CompanyTimezone.getTimezoneInfo(hfsPart4);
            }
            catch
            { }




        }
        void processMobileUserQueries()
        {
            try
            {
                BindMBRepeater();
                hideShowControlsByTabSelected(POST_BACK_TAB.MOBILE_USER_QUERIES);
            }
            catch
            {
                showMessage(true, "Internal server error", updRepeater, DIALOG_TYPE.Error);
            }
        }
        void processMyQuery()
        {
            try
            {
                BindMyqueryRepeater();
                hideShowControlsByTabSelected(POST_BACK_TAB.MY_QUERIES);
            }
            catch
            {
                showMessage(true, "Internal server error", updRepeater, DIALOG_TYPE.Error);
            }
        }
        void processClosedQuery()
        {
            try
            {
                BindClosedRepeater();
                hideShowControlsByTabSelected(POST_BACK_TAB.CLOSED_QUERIES);
            }
            catch
            {
                showMessage(true, "Internal server error", updRepeater, DIALOG_TYPE.Error);
            }
        }
        void processResolvedQuery()
        {
            try
            {
                BindResolvedRpt();
                hideShowControlsByTabSelected(POST_BACK_TAB.RESOLVED_QUERIES);
            }
            catch
            {
                showMessage(true, "Internal server error", updRepeater, DIALOG_TYPE.Error);
            }
        }
        protected void BindMBRepeater()
        {
            QueriesCP queries = new QueriesCP();
            DataTable dt = queries.GetMobileQueries(strUserId, hfsPart4);
            if (dt.Rows.Count > 0)
            {
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
                rptUserDetails.Visible = true;
                lblHeaderInfo.Text = "<h1>Mobile User Queries</h1>";
            }
            else if (dt.Rows.Count == 0)
            {
                rptUserDetails.Visible = false;
                lblHeaderInfo.Text = "<h1> No mobile queries added yet. </h1>";
            }
        }

        protected void BindMyqueryRepeater()
        {
            QueriesCP queries = new QueriesCP();
            DataTable dt = queries.GetMyQueries(strUserId);
            if (dt.Rows.Count > 0)
            {
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
                rptUserDetails.Visible = true;
                lblHeaderInfo.Text = "<h1> User Queries</h1>";
            }
            else if (dt.Rows.Count == 0)
            {
                rptUserDetails.Visible = false;
                lblHeaderInfo.Text = "<h1>No user queries added yet.</h1>";
            }
        }

        protected void BindResolvedRpt()
        {
            QueriesCP queries = new QueriesCP();
            DataTable dt = queries.getResolvedQueries(QueriesCP.GET_RESOLVED_QUERY_TYPE.All, false, String.Empty,
                                                        strUserId, String.Empty);
            if (dt.Rows.Count > 0)
            {
                rptUserDetails.Visible = true;
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
                lblHeaderInfo.Text = "<h1> Resolved Queries</h1>";
            }
            else if (dt.Rows.Count == 0)
            {
                rptUserDetails.Visible = false;
                lblHeaderInfo.Text = "<h1>There are no resolved queries.</h1>";
            }
            bindSrchPnlUserDropDown();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tblToBind"></param>
        /// <exception cref="System.ArgumentNullException">Thrown if tbltobind is null</exception>
        void bindResolvedRpt(DataTable tblToBind)
        {
            if (tblToBind == null)
            {
                throw new ArgumentNullException();
            }
            if (tblToBind.Rows.Count > 0)
            {
                rptUserDetails.Visible = true;
                rptUserDetails.DataSource = tblToBind;
                rptUserDetails.DataBind();
                lblHeaderInfo.Text = "<h1> Resolved Queries</h1>";
            }
            else if (tblToBind.Rows.Count == 0)
            {
                rptUserDetails.Visible = false;
                lblHeaderInfo.Text = "<h1>There are no resolved queries.</h1>";
            }
        }

        protected void BindClosedRepeater()
        {
            QueriesCP queries = new QueriesCP();
            DataTable dt = queries.GetClosedQueries(strUserId);
            if (dt.Rows.Count > 0)
            {
                rptUserDetails.Visible = true;
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
                lblHeaderInfo.Text = "<h1>Closed Queries</h1>";
            }
            else if (dt.Rows.Count == 0)
            {
                rptUserDetails.Visible = false;
                lblHeaderInfo.Text = "<h1>No closed queries added yet</h1>";
            }
        }

        //protected void BindCommentsRepeater(string TokenNumber)
        //{
        //    DataTable dt = new DataTable();
        //    dt = getCommentsRptDataForBinding(TokenNumber);
        //    if (dt.Rows.Count > 0)
        //    {
        //        rptComments.DataSource = dt;
        //        rptComments.DataBind();
        //        hideShowRptCommentsAndPanel(false);
        //        hideShowHeaderOfCommentsRpt(true, String.Empty);
        //    }
        //    else
        //    {
        //        rptComments.DataSource = null;
        //        rptComments.DataBind();
        //        hideShowRptCommentsAndPanel(true);
        //        hideShowHeaderOfCommentsRpt(false, "No comments added yet.");
        //    }
        //}
        protected void BindCommentsRepeater(DataTable commentsDtble)
        {
            if (commentsDtble.Rows.Count > 0)
            {
                rptComments.DataSource = commentsDtble;
                rptComments.DataBind();
                hideShowRptCommentsAndPanel(false);
                hideShowHeaderOfCommentsRpt(true, String.Empty);
            }
            else
            {
                rptComments.DataSource = null;
                rptComments.DataBind();
                hideShowRptCommentsAndPanel(true);
                hideShowHeaderOfCommentsRpt(false, "No comments added yet.");
            }
        }
        DataTable getCommentsRptDataForBinding(string tokenNo)
        {
            QueriesCP queries = new QueriesCP();
            DataTable dt = new DataTable();
            try
            {
                POST_BACK_TAB ePostBackTab;

                if (hidTabSelected.Value == String.Empty) ePostBackTab = POST_BACK_TAB.MOBILE_USER_QUERIES;
                else ePostBackTab = (POST_BACK_TAB)Enum.Parse(typeof(POST_BACK_TAB), (Convert.ToInt32(hidTabSelected.Value)).ToString());

                switch (ePostBackTab)
                {
                    case POST_BACK_TAB.MOBILE_USER_QUERIES:
                    case POST_BACK_TAB.RESOLVED_QUERIES:
                    case POST_BACK_TAB.CLOSED_QUERIES:
                        dt = queries.getCommentsListForUsers(tokenNo);
                        break;
                    case POST_BACK_TAB.MY_QUERIES:
                        dt = queries.GetCommentsList(tokenNo);
                        break;
                }
            }
            catch
            {
            }
            return dt;
        }
        bool canSubAdminResolveTheQuery(DataTable commentDtbl)
        {
            if (commentDtbl == null) throw new ArgumentNullException();
            if (commentDtbl.Rows.Count > 0)
            {
                DataView dv = commentDtbl.DefaultView;
                dv.Sort = "COMMENTED_ON desc";
                DataTable dtblSortedComments = dv.ToTable();
                DataRow drLatestCommentRow = dtblSortedComments.Rows[0];
                long lngCommentedOn = Convert.ToInt64(drLatestCommentRow["COMMENTED_ON"]);
                if (isLastCommentBySubAdmin(drLatestCommentRow) &&
                    isLastCommentBefore14Days(lngCommentedOn))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
        bool isLastCommentBySubAdmin(DataRow latestCommentRow)
        {
            bool blnLastCommentBySubAdmin = false;
            try
            {
                SUPPORT_QUERIES_COMMENTER eCommenter = (SUPPORT_QUERIES_COMMENTER)Enum.Parse(typeof(SUPPORT_QUERIES_COMMENTER), Convert.ToString(latestCommentRow["USER_TYPE"]));
                if (Enum.IsDefined(typeof(SUPPORT_QUERIES_COMMENTER), eCommenter))
                {
                    switch (eCommenter)
                    {
                        case SUPPORT_QUERIES_COMMENTER.User:
                        case SUPPORT_QUERIES_COMMENTER.Admin:
                            blnLastCommentBySubAdmin = false;
                            break;
                        case SUPPORT_QUERIES_COMMENTER.SubAdmin:
                            blnLastCommentBySubAdmin = true;
                            break;
                        default:
                            blnLastCommentBySubAdmin = false;
                            break;
                    }
                }
            }
            catch
            {
                blnLastCommentBySubAdmin = false;
            }
            return blnLastCommentBySubAdmin;
        }
        bool isLastCommentBefore14Days(long commentedOn)
        {
            DateTime dtCommentedOn =
                new DateTime(commentedOn, DateTimeKind.Utc);
            DateTime dtUtcNow = DateTime.UtcNow;
            TimeSpan ts = dtUtcNow - dtCommentedOn;
            if (ts.TotalDays > 14) return true;
            else return false;
        }

        void hideShowRptCommentsAndPanel(bool hide)
        {
            if (hide)
            {
                rptComments.Visible = false;
                pnlRptComments.Visible = false;
            }
            else
            {
                rptComments.Visible = true;
                pnlRptComments.Visible = true;
            }
        }
        void hideShowHeaderOfCommentsRpt(bool hide, string headerText)
        {
            if (hide)
            {
                lblCommentsRptHeader.Visible = false;
            }
            else
            {
                lblCommentsRptHeader.Text = "<h6 style=\"font-size:12px;\">" + headerText + "</h6>";
                lblCommentsRptHeader.Visible = true;
            }
        }
        protected void bttncomment_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtcomment.Text))
                {
                    QueriesCP queries = new QueriesCP();
                    queries.AddCommentQuery(
                        hfTokenNumber.Value,
                        Utilities.GetMd5Hash(hfTokenNumber.Value + DateTime.UtcNow.Ticks.ToString()),
                        strUserId,
                        2,
                        txtcomment.Text,
                        strUserId,
                        hfsPart4,
                        chkIsResolved.Checked);
                    if (queries.StatusCode == 0)
                    {
                        txtcomment.Text = string.Empty;
                        DataTable dtblComments =
                            this.getCommentsRptDataForBinding(hfTokenNumber.Value);
                        BindCommentsRepeater(dtblComments);
                        if (chkIsResolved.Checked)
                        {
                            hideShowAddCommentLink(true);
                            POST_BACK_TAB ePostBackTab = this.tabSelected();
                            switch (ePostBackTab)
                            {
                                case POST_BACK_TAB.MOBILE_USER_QUERIES:
                                    BindMBRepeater();
                                    break;
                                case POST_BACK_TAB.MY_QUERIES:
                                    BindMyqueryRepeater();
                                    break;
                                case POST_BACK_TAB.RESOLVED_QUERIES:
                                    BindResolvedRpt();
                                    break;
                                case POST_BACK_TAB.CLOSED_QUERIES:
                                    break;
                            }
                        }
                        else
                        {
                            hideShowAddCommentLink(false);
                        }
                        Utilities.closeModalPopUp("divCommentByUser", updCommentByUser);
                        /*** 
                         * For any case we have to hide chkBoxIsResolved if process is successfull
                         * ***/
                        checkUncheckIsResolvedCheckbox(false);
                        hideShowIsResolvedCheckBox(true);
                        upUserDtlsModal.Update();
                    }
                    else
                    {
                        Utilities.showMessage("Internal server error.",
                            updCommentByUser,
                            DIALOG_TYPE.Error);
                    }
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error.",
                    updAddQueryContainer,
                    DIALOG_TYPE.Error);
            }
        }
        protected void lnkBack_Click(object sender, EventArgs e)
        {
            pnlRepeaterBox.Visible = true;

        }
        protected void enabledisablepanels(string mode)
        {
            if (mode == "Info")
            {
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = true;
            }
            else if (mode == "Add")
            {
                pnlRepeaterBox.Visible = false;
            }
            else
            {
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = false;
            }
        }

        //protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        //{
        //    RepeaterItem item = e.Item;
        //    Label lit_id = (Label)item.FindControl("lblid");
        //    if (e.CommandName == "Info")
        //    {
        //        FillQueryDetails(lit_id.Text);
        //    }
        //}


        protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Info":
                    Info(e);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing2", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        ///<param name="postBackMsg"></param>
        /// <param name="message"></param>
        /// <param name="updPanel">Pass null if for page startup script</param>
        /// <param name="dialogType"></param>
        void showMessage(bool postBackMsg, string message, UpdatePanel updPanel, DIALOG_TYPE dialogType)
        {
            if (postBackMsg)
                Utilities.showMessage(message, updPanel, dialogType);
            else
                Utilities.showMessageUsingPageStartUp(this.Page, "Show Message", message, dialogType);
        }

        protected void FillQueryDetails(string userIdOfUser, string tokenNo)
        {
            QueriesCP querydet = new QueriesCP();
            querydet.QueryDetails(tokenNo);
            DataTable dt = querydet.ResultTable;
            string strFullName = String.Empty;
            string strUserName = String.Empty;
            try
            {
                POST_BACK_TAB ePostBackTab;

                if (hidTabSelected.Value == String.Empty) ePostBackTab = POST_BACK_TAB.MOBILE_USER_QUERIES;
                else ePostBackTab = (POST_BACK_TAB)Enum.Parse(typeof(POST_BACK_TAB), (Convert.ToInt32(hidTabSelected.Value)).ToString());

                switch (ePostBackTab)
                {
                    case POST_BACK_TAB.MOBILE_USER_QUERIES:
                    case POST_BACK_TAB.RESOLVED_QUERIES:
                    case POST_BACK_TAB.CLOSED_QUERIES:
                        strUserName = querydet.getUsernameOfUser(userIdOfUser, strUserId, hfsPart3, out strFullName);
                        break;
                    case POST_BACK_TAB.MY_QUERIES:
                        strUserName = querydet.UserNameCP(strUserId, out strFullName);
                        break;
                }

            }
            catch
            {
                strFullName = String.Empty;
            }
            if (dt.Rows.Count > 0)
            {
                hidDetailsFormMode.Value = tokenNo;
                lname.Text = strUserName;
                lblFullName.Text = strFullName;
                lsubject.Text = Convert.ToString(dt.Rows[0]["QUERY_SUBJECT"]);
                lquery.Text = Convert.ToString(dt.Rows[0]["QUERY"]);
                txtcomment.Text = "";
            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }

        void Info(RepeaterCommandEventArgs e)
        {
            //Label userId = (Label)e.Item.FindControl("lblid");
            try
            {
                hfTokenNumber.Value = ((Label)e.Item.FindControl("lblnumber")).Text;
                POST_BACK_TAB ePostBackTab = tabSelected();
                string strUserId = ((Label)e.Item.FindControl("lblid")).Text;
                FillQueryDetails(strUserId, hfTokenNumber.Value);
                DataTable dtblComments = this.getCommentsRptDataForBinding(hfTokenNumber.Value);

                BindCommentsRepeater(dtblComments);

                setValueOfCommentTextBox(String.Empty);
                if (this.canSubAdminResolveTheQuery(dtblComments))
                {
                    hideShowIsResolvedCheckBox(false);
                    checkUncheckIsResolvedCheckbox(false);
                }
                else
                {
                    hideShowIsResolvedCheckBox(true);
                    checkUncheckIsResolvedCheckbox(false);
                }
                switch (ePostBackTab)
                {
                    case POST_BACK_TAB.MOBILE_USER_QUERIES:
                    case POST_BACK_TAB.MY_QUERIES:
                    case POST_BACK_TAB.CLOSED_QUERIES:
                        hideShowAddCommentLink(false);
                        break;
                    case POST_BACK_TAB.RESOLVED_QUERIES:
                        hideShowAddCommentLink(true);
                        break;

                }
                Utilities.showModalPopup("divUserDtlsModal", updRepeater, "Query Details", "550", true);
                enabledisablepanels("Info");
                upUserDtlsModal.Update();
            }
            catch
            {
                Utilities.showMessage("Internal server error.",
                    updRepeater, DIALOG_TYPE.Error);
            }
        }
        POST_BACK_TAB tabSelected()
        {
            POST_BACK_TAB ePostBackTab;

            if (hidTabSelected.Value == String.Empty) ePostBackTab = POST_BACK_TAB.MOBILE_USER_QUERIES;
            else ePostBackTab = (POST_BACK_TAB)Enum.Parse(typeof(POST_BACK_TAB), (Convert.ToInt32(hidTabSelected.Value)).ToString());
            return ePostBackTab;
        }
        protected void btnTabClickPostBack_Click(object sender, EventArgs e)
        {
            string strPostBackFor = hidValuesUsedAfterPostBack.Value;
            POST_BACK_TAB postBackFor = (POST_BACK_TAB)Enum.Parse(typeof(POST_BACK_TAB), strPostBackFor);
            switch (postBackFor)
            {
                case POST_BACK_TAB.MOBILE_USER_QUERIES:
                    processMobileUserQueries();
                    break;
                case POST_BACK_TAB.CLOSED_QUERIES:
                    processClosedQuery();
                    break;
                case POST_BACK_TAB.MY_QUERIES:
                    processMyQuery();
                    break;
                case POST_BACK_TAB.RESOLVED_QUERIES:
                    processResolvedQuery();
                    setValueOfResolvedSearchTextBox(String.Empty);
                    ddlSrchPnlUser.SelectedIndex = 0;
                    break;
            }
        }

        protected void rptUserDetails_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lbldate = (Label)e.Item.FindControl("lblDate");
                long lngPostedDate = Convert.ToInt64(DataBinder.Eval(e.Item.DataItem, "POSTED_ON"));
                try
                {

                    lbldate.Text = Utilities.getCompanyLocalFormattedDate(CompanyTimezone.CmpTimeZoneInfo, lngPostedDate);
                }
                catch
                {
                    Utilities.showMessage("Internal server error", updRepeater, DIALOG_TYPE.Error);
                    return;
                }
            }
        }

        void hideShowRptHeaderLink(bool hide)
        {
            if (hide)
            {
                lnkAddNewQuery.Visible = false;
            }
            else
            {
                lnkAddNewQuery.Visible = true;
            }
        }

        void hideShowControlsByTabSelected(POST_BACK_TAB tabSelected)
        {
            switch (tabSelected)
            {
                case POST_BACK_TAB.MOBILE_USER_QUERIES:
                case POST_BACK_TAB.CLOSED_QUERIES:
                    hideShowRptHeaderLink(true);
                    hideShowPanelSearch(true);
                    break;
                case POST_BACK_TAB.RESOLVED_QUERIES:
                    hideShowRptHeaderLink(true);
                    hideShowPanelSearch(false);
                    break;
                case POST_BACK_TAB.MY_QUERIES:
                    hideShowRptHeaderLink(false);
                    hideShowPanelSearch(true);
                    break;
            }
        }
        void hideShowPanelSearch(bool hide)
        {
            if (hide)
            {
                pnlResolvedQueriesSearch.Visible = false;
            }
            else
            {
                pnlResolvedQueriesSearch.Visible = true;
            }
        }
        void hideShowIsResolvedCheckBox(bool hide)
        {
            if (hide) chkIsResolved.Visible = false;
            else chkIsResolved.Visible = true;
        }
        void hideShowAddCommentLink(bool hide)
        {
            if (hide) lnkAddComment.Visible = false;
            else lnkAddComment.Visible = true;
        }
        void checkUncheckIsResolvedCheckbox(bool check)
        {
            if (check) chkIsResolved.Checked = true;
            else chkIsResolved.Checked = false;
        }
        void setValueOfCommentTextBox(string value)
        {
            txtcomment.Text = value;
        }
        void setValueOfResolvedSearchTextBox(string value)
        {
            txtSrchPnlTokenNo.Text = value;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="System.Exception">Thrown when no record is found</exception>
        void bindQueryTypeDropDown()
        {
            DataTable dtblQueryType = new QueriesCP().getQueryTypeForSubAdmin();
            if (dtblQueryType.Rows.Count > 0)
            {
                ddlQueryType.Items.Clear();
                ddlQueryType.DataSource = dtblQueryType;
                ddlQueryType.DataTextField = "QUERY_TYPE";
                ddlQueryType.DataValueField = "QUERY_CODE";
                ddlQueryType.DataBind();
            }
            else
            {
                throw new Exception("No record found");
            }
        }
        protected void btnSaveQuery_Click(object sender, EventArgs e)
        {
            string strError = validateAddQueryForm();
            if (strError != String.Empty)
            {
                Utilities.showAlert(strError, "divAddQueryError", updAddQueryContainer);
                Utilities.runPostBackScript(@"changeWidthOfDiv('divPopUpActionCont',POP_UP_ACTION_DIV_2,true);", updAddQueryContainer, "ActionButtonInCenter");
                return;
            }
            else
            {
                QueriesCP objQueryCP = new QueriesCP();
                objQueryCP.AddSubadminQuery(hfsPart4, strUserId, Convert.ToInt32(ddlQueryType.SelectedValue), txtAddQuerySubject.Text, txtAddQueryDescription.Text);
                if (objQueryCP.StatusCode == 0)
                {
                    showMessage(true, "Query saved successfully.", updAddQueryContainer, DIALOG_TYPE.Info);
                    Utilities.closeModalPopUp("divModalContainer", updAddQueryContainer);
                    BindMyqueryRepeater();
                }
                else
                {
                    showMessage(true, "Internal server error.Query not added.", updAddQueryContainer, DIALOG_TYPE.Error);
                    Utilities.runPostBackScript(@"changeWidthOfDiv('divPopUpActionCont',POP_UP_ACTION_DIV_2,true);", updAddQueryContainer, "ActionButtonInCenter");
                }
            }
        }
        string validateAddQueryForm()
        {
            string strError = String.Empty;
            if (txtAddQuerySubject.Text.Trim() == String.Empty)
            {
                strError = "Please enter a subject <br/>";
            }
            if (txtAddQueryDescription.Text.Trim() == String.Empty)
            {
                strError += "Please enter description <br/>";
            }
            return strError;
        }

        void bindSrchPnlUserDropDown()
        {
            GetUserDetail objUserDtl = new GetUserDetail();
            objUserDtl.getUserDetailsForSort(hfsPart4, strUserId, GetUserDetail.ORDER_BY.Name, GetUserDetail.SORT_TYPE.ASC, String.Empty);
            if (objUserDtl.StatusCode == 0)
            {
                DataTable dtblUserDetail = objUserDtl.ResultTable;
                if (dtblUserDetail != null)
                {
                    dtblUserDetail.Columns.Add("FULL_NAME", typeof(string));
                    foreach (DataRow row in dtblUserDetail.Rows)
                    {
                        row["FULL_NAME"] = Convert.ToString(row["FIRST_NAME"]) + " " + Convert.ToString(row["LAST_NAME"]);
                    }
                    dtblUserDetail.AcceptChanges();
                    ddlSrchPnlUser.Items.Clear();
                    ddlSrchPnlUser.Items.Add(new ListItem("MyQuery", "SA_" + strUserId));
                    ddlSrchPnlUser.DataSource = dtblUserDetail;
                    ddlSrchPnlUser.DataTextField = "FULL_NAME";
                    ddlSrchPnlUser.DataValueField = "USER_ID";
                    ddlSrchPnlUser.DataBind();
                }
            }
            else
            {
                Utilities.showMessage("Internal server error.Could not bind user drop down.", updRepeater, DIALOG_TYPE.Error);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                QueriesCP objQueriesCP = new QueriesCP();
                DataTable dtblResolvedQueries = null;
                QueriesCP.GET_RESOLVED_QUERY_TYPE queryType = QueriesCP.GET_RESOLVED_QUERY_TYPE.All;
                if (ddlSrchPnlUser.SelectedValue == "0")
                {
                    queryType = QueriesCP.GET_RESOLVED_QUERY_TYPE.All;
                }
                else if (ddlSrchPnlUser.SelectedValue.Trim().StartsWith("SA"))
                {
                    queryType = QueriesCP.GET_RESOLVED_QUERY_TYPE.SubAdmin;
                }
                else
                {
                    queryType = QueriesCP.GET_RESOLVED_QUERY_TYPE.UserId;
                }

                switch (queryType)
                {
                    case QueriesCP.GET_RESOLVED_QUERY_TYPE.All:
                        dtblResolvedQueries = objQueriesCP.getResolvedQueries(queryType,
                                                   txtSrchPnlTokenNo.Text.Trim() == String.Empty ? false : true,
                                                   String.Empty, strUserId, txtSrchPnlTokenNo.Text);
                        break;
                    case QueriesCP.GET_RESOLVED_QUERY_TYPE.SubAdmin:
                        dtblResolvedQueries = objQueriesCP.getResolvedQueries(queryType,
                                                txtSrchPnlTokenNo.Text.Trim() == String.Empty ? false : true,
                                                strUserId, strUserId, txtSrchPnlTokenNo.Text);//user id and sub admin id is the same
                        break;
                    case QueriesCP.GET_RESOLVED_QUERY_TYPE.UserId:
                        dtblResolvedQueries = objQueriesCP.getResolvedQueries(queryType,
                                                 txtSrchPnlTokenNo.Text.Trim() == String.Empty ? false : true,
                                                 ddlSrchPnlUser.SelectedValue, strUserId, txtSrchPnlTokenNo.Text);
                        break;
                }
                bindResolvedRpt(dtblResolvedQueries);

            }
            catch
            {
                Utilities.showMessage("Internal server error.", updRepeater, DIALOG_TYPE.Error);
            }
        }

    }
}
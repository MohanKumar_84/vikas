﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientCP
{
    public partial class MobileUserList : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {

            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!Page.IsPostBack)
            {
                if (hfsValue != string.Empty)
                {
                    BindSubAdminsDropDown();
                    BindUsersRepeater(null);
                }
                Utilities.runPageStartUpScript(this.Page, "MakeInputUniform", "$(\"input\").uniform();setOnClickEventOfRptChkBoxContSpan();");
            }
            else
            {
                //if (ddlSubAdminList.Items.Count > 2)//considering select and all
                //{
                //    //if items is greater thab 2 then other subadmins are available as well
                //    //Thus chage admin button will be wisible.So we cand remove the css for width
                //    Utilities.runPostBackScript("removeCSSForTdPaddingInRpt();",
                //            this.Page, "RemoveCssForTdPaddingOnEveryPostBack");
                //}
                //else
                //{
                //    Utilities.runPostBackScript("addCSSForTdPaddingInRpt();",
                //            this.Page, "AddCssForTdPaddingOnEveryPostBack");
                //}
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, String.Empty);
                    BindSubAdminsDropDown();
                    BindUsersRepeater(null);
                    return;
                }
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, String.Empty);
            }
        }
        protected void BindSubAdminsDropDown()
        {
            //GetSubAdminDetail getSubAdminDetails = new GetSubAdminDetail(true, false, hfsPart4, "", "", hfsPart3);
            //getSubAdminDetails.Process();
            GetSubAdminDetail getSubAdminDetails =
                new GetSubAdminDetail(hfsPart4, hfsPart3);
            getSubAdminDetails.getSubAdminByRole(GetSubAdminDetail.SUB_ADMIN_ROLE.USER_GROUPS_DEVICES_MANAGER);
            if (getSubAdminDetails.StatusCode == 0)
            {
                if (getSubAdminDetails.ResultTable.Rows.Count > 0)
                {
                    ddlSubAdminList.DataSource = getSubAdminDetails.ResultTable;

                    ddlSubAdminList.DataValueField = "SUBADMIN_ID";
                    ddlSubAdminList.DataTextField = "FULL_NAME";
                    ddlSubAdminList.DataBind();
                    ddlSubAdminList.Items.Insert(0, new ListItem("All", "-1"));

                    BindPopupSubAdminsDropDown(getSubAdminDetails.ResultTable);
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, @"$.alert(" + "'" + "No sub admin added yet" + "'" + "," + "$.alert.Information" + ");showDialogImage(" + "DialogType.Info" + ");", true);
                }
            }
        }
        protected void BindPopupSubAdminsDropDown(DataTable subadminDtls)
        {
            ddlPopUpSubAdmin.Items.Clear();
            ddlPopUpSubAdmin.DataSource = subadminDtls;
            ddlPopUpSubAdmin.DataValueField = "SUBADMIN_ID";
            ddlPopUpSubAdmin.DataTextField = "FULL_NAME";
            ddlPopUpSubAdmin.DataBind();
            ddlPopUpSubAdmin.Items.Insert(0, new ListItem("Select", "-1"));
        }
        //protected void BindOtherSubAdminsDropDown()
        //{
        //    if (hfdSubAdminId.Value != "")
        //    {
        //        GetSubAdminDetail getSubAdminDetails = 
        //            new GetSubAdminDetail(hfsPart4,hfsPart3);
        //        getSubAdminDetails.GetOtherSubAdmins(hfdSubAdminId.Value);
        //        ddlPopUpSubAdmin.DataSource = getSubAdminDetails.ResultTable;
        //        ddlPopUpSubAdmin.Items.Clear();
        //        if (getSubAdminDetails.ResultTable != null)
        //        {
        //            ddlPopUpSubAdmin.DataValueField = "SUBADMIN_ID";
        //            ddlPopUpSubAdmin.DataTextField = "FULL_NAME";
        //            ddlPopUpSubAdmin.DataBind();
        //            ddlPopUpSubAdmin.Items.Insert(0, new ListItem("Select", "-1"));
        //        }
        //    }
        //}
        protected void BindUsersRepeater(UpdatePanel updPanel)
        {
            GetAdminUsers getAdminUsers;
            if (ddlSubAdminList.SelectedValue == "-1")
            {
                getAdminUsers = new GetAdminUsers(hfsPart4, "", hfsPart3, true, false);
            }
            else
            {
                getAdminUsers = new GetAdminUsers(hfsPart4, ddlSubAdminList.SelectedValue, hfsPart3, false, true);
                getAdminUsers.SubAdminId = ddlSubAdminList.SelectedValue;
            }
            getAdminUsers.Process();
            if (getAdminUsers.StatusCode == 0)
            {
                if (getAdminUsers.AdminUsers.Rows.Count > 0)
                {
                    rptAdminUsers.DataSource = getAdminUsers.AdminUsers;
                    rptAdminUsers.DataBind();
                    setTheHeaderOfRepeater("User list");
                    hideShowRepeater(false);
                    if (ddlSubAdminList.Items.Count > 2)//considering all and 1 subAdmin
                    {
                        showHideChangeUsersSubAdminButton(true);
                    }
                    else
                    {
                        showHideChangeUsersSubAdminButton(false);
                    }
                    showHideRepeaterContainerPanel(true);
                }
                else
                {
                    setTheHeaderOfRepeater("No user is added yet");
                    rptAdminUsers.DataSource = null;
                    rptAdminUsers.DataBind();
                    hideShowRepeater(true);
                    showHideChangeUsersSubAdminButton(false);
                    showHideRepeaterContainerPanel(false);
                }
                
                //if (ddlSubAdminList.Items.Count > 2)//considering select and all
                //{
                //    //if items is greater thab 2 then other subadmins are available as well
                //    //Thus chage admin button will be wisible.So we cand remove the css for width
                //    if (updPanel != null)
                //    {
                //        Utilities.runPostBackScript("removeCSSForTdPaddingInRpt();",
                //            updPanel, "RemoveCssForTdPadding");
                //    }
                //    else
                //    {
                //        Utilities.runPageStartUpScript(this.Page,
                //            "RemoveCssForTdPaddingForPageStartup",
                //            "removeCSSForTdPaddingInRpt();"
                //            );
                //    }
                //}
                //else
                //{
                //    if (updPanel != null)
                //    {
                //        Utilities.runPostBackScript("addCSSForTdPaddingInRpt();",
                //            updPanel, "RemoveCssForTdPadding");
                //    }
                //    else
                //    {
                //        Utilities.runPageStartUpScript(this.Page,
                //            "RemoveCssForTdPaddingForPageStartup",
                //            "addCSSForTdPaddingInRpt();"
                //            );
                //    }
                //}
            }
            else
            {
                if (updPanel != null)
                    Utilities.showMessage("Internal server error", updPanel, DIALOG_TYPE.Error);
                else
                    Utilities.showMessageUsingPageStartUp(this.Page, "InternalError", "Internal server error", DIALOG_TYPE.Error);
                hideShowRepeater(true);
                showHideChangeUsersSubAdminButton(false);
                showHideRepeaterContainerPanel(false);
            }
        }
        void hideShowRepeater(bool hide)
        {
            if (hide)
            {
                rptAdminUsers.Visible = false;
            }
            else
            {
                rptAdminUsers.Visible = true;
            }
        }
        protected void ddlSubAdminList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindUsersRepeater(updRepeater);
        }
        void setTheHeaderOfRepeater(string headerInfo)
        {
            lblHeaderInfo.Text = "<h1>" + headerInfo + "</h1>";
        }
        protected void rptAdminUsers_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                DataRowView dr = (DataRowView)(e.Item.DataItem);
                Label lblIsSubAdminBlocked = (Label)e.Item.FindControl("lblIsSubAdminBlocked");
                Label lblIsSubAdminActive = (Label)e.Item.FindControl("lblIsSubAdminActive");
                Label lblSubAdmin = (Label)e.Item.FindControl("lblSubAdmin");
                Label lblSubAdminId = (Label)e.Item.FindControl("lblSubAdminId");
                Label lblIsUserBlockedStatus = (Label)e.Item.FindControl("lblIsUserBlockedStatus");
                Label lblIsUserActiveStatus = (Label)e.Item.FindControl("lblIsUserActiveStatus");
                Label lblStatus = (Label)e.Item.FindControl("lblStatus");
                Label lblUserId = (Label)e.Item.FindControl("lblUserId");
                Label lblUserFullName = (Label)e.Item.FindControl("lblUserFullName");
                //if (lblIsSubAdminActive.Text == "1" && lblIsSubAdminBlocked.Text == "False")
                //{
                //    lblSubAdmin.ForeColor = System.Drawing.Color.Green;
                //}
                //else if (lblIsSubAdminActive.Text == "0" && (lblIsSubAdminBlocked.Text == "True" || lblIsSubAdminBlocked.Text == "False"))
                //{
                //    lblSubAdmin.ForeColor = System.Drawing.Color.Red;
                //}
                //else if (lblIsSubAdminBlocked.Text == "True" && lblIsSubAdminActive.Text == "1")
                //{
                //    lblSubAdmin.ForeColor = System.Drawing.Color.Red;
                //}

                if (Convert.ToBoolean(dr["UserIsBlocked"]))
                {
                    lblStatus.Text = "Blocked";
                }
                else
                {
                    lblStatus.Text = "Active";
                }

                LinkButton lnkUser = (LinkButton)e.Item.FindControl("lnkUser");
                //lnkUser.ID = lblUserId.Text + "_" + "lnkUser";
                lnkUser.Text = Convert.ToString(dr["FIRST_NAME"]) + " " + Convert.ToString(dr["LAST_NAME"]) + " ( " + Convert.ToString(dr["USER_NAME"]) + " )";
                //lnkUser.CommandArgument = lblUserId.Text + "," + Convert.ToString(dr["USER_NAME"]);
                //lnkUser.ToolTip = "User Details";
                //lnkUser.Click += new EventHandler(DetailClicked);

                lblUserFullName.Text = Convert.ToString(dr["FIRST_NAME"]) + " " + Convert.ToString(dr["LAST_NAME"]);
                //LinkButton lnkChange = (LinkButton)e.Item.FindControl("lnkChange");
                //if ((ddlSubAdminList.Items.Count > 2))
                //{

                //    lnkChange.ID = lblSubAdminId.Text + "_" + "btnChange";
                //    lnkChange.Text = "Change SubAdmin";
                //    lnkChange.CommandArgument = lblSubAdminId.Text + "," + lblUserId.Text + "," + lblSubAdmin.Text + "," + dr["FIRST_NAME"] + " " + dr["LAST_NAME"];
                //    lnkChange.Click += new EventHandler(ClickedOne);
                //}
                //else
                //{
                //    lnkChange.Visible = false;

                //}

            }
        }
        protected void rptAdminUsers_ItemCommand(Object Sender, RepeaterCommandEventArgs e)
        {
            Label lblUserId = (Label)e.Item.FindControl("lblUserId");
            Label lblSubAdminId = (Label)e.Item.FindControl("lblSubAdminId");
            Label lblUsername = (Label)e.Item.FindControl("lblUserName");
            ucUserDetails.UserId = lblUserId.Text;
            ucUserDetails.UserName = lblUsername.Text;
            ucUserDetails.rememberUserId();
            ucUserDetails.getUserDetailsAndFillData(lblUserId.Text, hfsPart3);
            ucUserDetails.FindControl("lnkEditUser").Visible = false;
            ucUserDetails.FindControl("lnkBlockUser").Visible = false;
            ucUserDetails.FindControl("lnkUnBlockUser").Visible = false;
            Utilities.showModalPopup("divModalContainer", updRepeater, "User Detail", Convert.ToString(Utilities.getModalPopUpWidth(MODAL_POP_UP_NAME.USER_DETAIL)), true);
            ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "HideSeparator", @"hideHTMLElements('ucSeparatorOne');", true);
        }  
        private void closeModalPopUp()
        {
            Utilities.closeModalPopUp("divModalContainer", updModalContainer);
        }

        protected void ClickedOne(object sender, EventArgs e)
        {
            LinkButton lnkChange = (LinkButton)sender;
            string str = Convert.ToString(lnkChange.CommandArgument);
            modalPopUUpUserDetailContent.Visible = false;
            modalPopUpSubAdminContent.Visible = true;
            hfdSubAdminId.Value = str.Split(',')[0];
            hfdUserId.Value = str.Split(',')[1];
            //lblUserName.Text = str.Split(',')[3];
            //lblSubAdminName.Text = str.Split(',')[2];
            //ddlSubAdmins.Items.Clear();
            //BindOtherSubAdminsDropDown();
            //ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "ShowPopUp", @"showModalPopUp('divModalContainer','" + "Change SubAdmin" + "',400);", true);
            Utilities.showModalPopup("divModalContainer", updRepeater, "Change Subadmin", "400", true);
        }

        protected void DetailClicked(object sender, EventArgs e)
        {
            //LinkButton lnkUser = (LinkButton)sender;
            //modalPopUUpUserDetailContent.Visible = true;
            //modalPopUpSubAdminContent.Visible = false;
            //string strCommangArg = lnkUser.CommandArgument;
            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            closeModalPopUp();
        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<MFEMobileUser> userListSelected = getUserListSelectedFromRpt();
                if (ddlPopUpSubAdmin.SelectedValue == "-1")
                {
                    //form comma separated list as this was filled using javascript at clints end
                    lblUserNameSelected.Text = "";
                    foreach (MFEMobileUser userDetail in userListSelected)
                    {
                        if (String.IsNullOrEmpty(lblUserNameSelected.Text))
                        {
                            lblUserNameSelected.Text += userDetail.FirstName + " " + userDetail.LastName;
                        }
                        else
                        {
                            lblUserNameSelected.Text += ", " + userDetail.FirstName + " " + userDetail.LastName;
                        }
                    }
                    Utilities.showAlert("Please select a subadmin", "divChangeSAPopUpError", updChngSubAdminModal, "ShowAlertSelectSubAdmin");
                    return;
                }
                ChangeUserSubAdmin(userListSelected, ddlPopUpSubAdmin.SelectedValue);
                BindUsersRepeater(updModalContainer);
                Utilities.closeModalPopUp("divChngSubAdminModal", updChngSubAdminModal);
                Utilities.showMessage("Users subadmin changed successfully", updChngSubAdminModal, DIALOG_TYPE.Info);
            }
            catch
            {
                Utilities.showMessage("Internal server error.", updChngSubAdminModal, DIALOG_TYPE.Error);
            }
        }
        List<MFEMobileUser> getUserListSelectedFromRpt()
        {
            List<MFEMobileUser> lstSelectedUserDtl = new List<MFEMobileUser>();
            foreach (RepeaterItem item in rptAdminUsers.Items)
            {
                CheckBox chkAppSelect = (CheckBox)item.FindControl("chkSelectUser");
                Label lblUserId = (Label)item.FindControl("lblUserId");
                Label lblSubAdminId = (Label)item.FindControl("lblSubAdminId");
                Label lblUserFirstName = (Label)item.FindControl("lblUserFirstName");
                Label lblUserLastName = (Label)item.FindControl("lblUserLastName");
                if (chkAppSelect.Checked)
                {
                    MFEMobileUser objUserDetail = new MFEMobileUser();
                    objUserDetail.UserId = lblUserId.Text;
                    objUserDetail.SubAdminId = lblSubAdminId.Text;
                    objUserDetail.FirstName = lblUserFirstName.Text;
                    objUserDetail.LastName = lblUserLastName.Text;
                    lstSelectedUserDtl.Add(objUserDetail);
                }
            }
            return lstSelectedUserDtl;
        }
        private void ChangeUserSubAdmin(List<MFEMobileUser> usersToChangeSubAdmin, string newSUbAdminId)
        {
            ChangeSubAdminOfUsers chngSubAdminOfUsers = new ChangeSubAdminOfUsers(hfsPart3, newSUbAdminId);
            chngSubAdminOfUsers.ProcessForMultipleUsers(usersToChangeSubAdmin);
            if (chngSubAdminOfUsers.StatusCode != 0) throw new Exception();
        }


        void showHideChangeUsersSubAdminButton(bool show)
        {
            if (show)
            {
                btnChangeSubAdmin.Visible = true;
            }
            else
            {
                btnChangeSubAdmin.Visible = false;
            }
        }
        void showHideRepeaterContainerPanel(bool show)
        {
            if (show)
            {
                pnlRptAdminUsersCont.Visible = true;
            }
            else
            {
                pnlRptAdminUsersCont.Visible = false;
            }
        }
    }
}
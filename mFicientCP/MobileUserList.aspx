﻿<%@ Page Title="mFicient | Users" Language="C#" MasterPageFile="~/master/Canvas.master"
    AutoEventWireup="true" CodeBehind="MobileUserList.aspx.cs" Inherits="mFicientCP.MobileUserList" %>

<%@ Register TagPrefix="userDetails" TagName="Details" Src="~/UserControls/UserDetails.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <style type="text/css">
        .modPopUpCheckBoxTable
        {
            border: 0px;
            text-align: left;
            margin-bottom: 6px;
            width: 100%;
            position: relative;
            left: -3px;
            top: 2px;
            background: none;
        }
        table.modPopUpCheckBoxTable td, table.modPopUpCheckBoxTable td
        {
            border: 0px;
            text-align: justify;
            padding: 0px;
        }
        div.radio + label
        {
            position: relative;
            top: 3px;
        }
        div.singleSection .column3 div.selector
        {
            left: -4px;
        }
        #divSelectSubAdminCont div.selector span
        {
            width: 200px;
        }
        .repeaterTable.havingUniformCheckBox td:not(.chkBox) > span
        {
            position: relative;
        }
        span.rptSpanHeaderText
        {
            position: relative;
            top: -2px;
            font-family: 'Trebuchet MS' , 'Lucida Sans Unicode' , 'Lucida Grande' , 'Lucida Sans' , Tahoma, Arial, Helvetica, sans-serif;
            font-size: 12px;
            font-style: normal;
            font-variant: normal;
            font-weight: bold;
        }
        .repeaterTable.havingUniformCheckBox td.chkBox, .repeaterTable.havingUniformCheckBox th.thChkBox
        {
            width: 30px;
        }
        .repeaterTable.havingUniformCheckBox td.chkBox > span, .repeaterTable.havingUniformCheckBox th.thChkBox > span
        {
            float: left;
            position: relative; /*top: 3px;*/
        }
        td.chkBox > span div.checker input, th.thChkBox > span div.checker input, td.chkBox > span div.checker, th.thChkBox > span div.checker
        {
            width: 16px;
            height: 16px;
        }
        form div .modalPopUpDetails div.singleSection fieldset section div.column3
        {
            width: 65%;
        }
        .modalPopUpDetails div.selector span
        {
            width: 270px;
        }
        #divDdlSubAdminCont .selector span
        {
            width: 200px;
        }
    </style>
    <script type="text/javascript">
        function removeCSSForTdPaddingInRpt() {
            $(".repeaterTable").removeClass("rptWithoutImageAndButton");
        }
        function addCSSForTdPaddingInRpt() {
            $(".repeaterTable").addClass("rptWithoutImageAndButton");
        }
        function getSelectedCommaSeparatedAppName() {
            var strSelectedUserNames = "";
            $.each($('.repeaterTable .chkBox'), function (index, value) {
                var $checkBox = $(this).children().find("input[id*='chkSelectUser']");
                var $spanUserName = $(this).children("span[id*='lblUserFullName']");
                if ($($checkBox).is(':checked')) {
                    if (strSelectedUserNames === "") {
                        strSelectedUserNames += $($spanUserName).text();
                    }
                    else {
                        strSelectedUserNames += ", " + $($spanUserName).text();
                    }
                }
            });
            return strSelectedUserNames;
        }
        function processChangeSubAdmin() {
            var strCommaSeparatedUserNames = getSelectedCommaSeparatedAppName();
            if (strCommaSeparatedUserNames) {
                var $popUpUserName = $('#<%=lblUserNameSelected.ClientID %>');
                if ($popUpUserName) {
                    $($popUpUserName).text(strCommaSeparatedUserNames);
                }
                showModalPopUpWithOutHeader('divChngSubAdminModal', 'Change Subadmin', '480', false);
            }
            else {
                showMessage('Please select a user to change the subadmin.', '$.alert.Error', DialogType.Error);
            }
        }
        function processSelectAllCheckChange(sender) {
            var $chkSelectAllUsers = ($(sender).find('input[type="checkbox"]'));
            if ($chkSelectAllUsers) {
                if ($($chkSelectAllUsers).is(":checked")) {
                    selectDeselectUserCheckboxes(true);
                }
                else {
                    selectDeselectUserCheckboxes(false);
                }
            }
        }
        function selectDeselectUserCheckboxes(select/*bool*/) {
            if (select === true) {
                $.each($('.repeaterTable .chkBox'), function (index, value) {
                    var $checkBox = $(this).children().find("input[id*='chkSelectUser']");
                    $($checkBox).attr('checked', 'checked');
                    var immediateSpanContainer = $($checkBox).parent();
                    immediateSpanContainer.addClass("checked");
                });
            }
            else {
                $.each($('.repeaterTable .chkBox'), function (index, value) {
                    var $checkBox = $(this).children().find("input[id*='chkSelectUser']");
                    $($checkBox).removeAttr('checked');
                    var immediateSpanContainer = $($checkBox).parent();
                    immediateSpanContainer.removeClass("checked");
                });
            }
        }
        function selectDeselectSelectAllChkBox(sender) {
            var $chkSelectUsers = ($(sender).find('input[type="checkbox"]'));
            if ($chkSelectUsers) {
                var $thChkBox = $('.repeaterTable .thChkBox').find('input[type="checkbox"]');
                var immediateSpanContainer = $($thChkBox).parent();
                if ($($chkSelectUsers).is(":checked")) {
                    var blnIsAllSelected = false;
                    var iCountOfCheckBoxes = $('.repeaterTable .chkBox') ? $('.repeaterTable .chkBox').length : 0;
                    var iCounterOfSelectedChkBox = 0;
                    $.each($('.repeaterTable .chkBox'), function (index, value) {
                        var $checkBox = $(this).children().find("input[id*='chkSelectUser']");
                        if ($($checkBox).is(':checked')) {
                            iCounterOfSelectedChkBox += 1;
                        }
                    });
                    if (iCounterOfSelectedChkBox === iCountOfCheckBoxes) {
                        immediateSpanContainer.addClass("checked");
                        $($thChkBox).attr('checked', 'checked');
                    }
                }
                else {
                    immediateSpanContainer.removeClass("checked");
                    $($thChkBox).removeAttr('checked');
                }
            }
        }
        function setOnClickEventOfRptChkBoxContSpan() {
            var $divCheckerCont = $(".thChkBox") ? $(".thChkBox").find("div.checker") : [];
            if ($divCheckerCont) {
                $.each($($divCheckerCont), function (index, value) {
                    var $spanChkBoxCont = $(this).parent();
                    if ($spanChkBoxCont) {
                        $($spanChkBoxCont).removeAttr("onchange");
                        $($spanChkBoxCont).click(function () {
                            processSelectAllCheckChange($(this));
                        }
                        );
                    }
                });
            }
            var $divBodyCheckerCont = $(".chkBox") ? $(".chkBox").find("div.checker") : [];
            if ($divBodyCheckerCont) {
                $.each($($divBodyCheckerCont), function (index, value) {
                    var $spanBodyChkBoxCont = $(this).parent();
                    if ($spanBodyChkBoxCont) {
                        $($spanBodyChkBoxCont).removeAttr("onchange");
                        $($spanBodyChkBoxCont).click(function () {
                            selectDeselectSelectAllChkBox($(this));
                        }
                        );
                    }
                });
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="updRepeater" runat="server">
        <ContentTemplate>
            <div id="PageCanvasContent">
                <div id="divSelectSubAdminCont" style="margin-bottom: 5px;">
                    <asp:Label ID="Label1" runat="server" Text="Sub Admin :" Style="position: relative;
                        top: -12px;"></asp:Label>&nbsp;&nbsp;
                    <asp:DropDownList ID="ddlSubAdminList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSubAdminList_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div id="divRepeater">
                    <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                        <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                            <div>
                                <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>User List</h1>"></asp:Label>
                            </div>
                            <div style="position: relative; top: 10px; right: 15px;">
                            </div>
                            <div style="height: 0px; clear: both">
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlRptAdminUsersCont" Style="height: 350px; overflow: scroll;" runat="server">
                            <asp:Repeater ID="rptAdminUsers" runat="server" OnItemDataBound="rptAdminUsers_ItemDataBound"
                                OnItemCommand="rptAdminUsers_ItemCommand">
                                <HeaderTemplate>
                                    <table class="repeaterTable havingUniformCheckBox">
                                        <thead>
                                            <tr>
                                                <th id="thChkBox" class="thChkBox">
                                                    <asp:CheckBox ID="chkSelectAllUsers" runat="server" onchange="processSelectAllCheckChange(this);"
                                                        ToolTip="Select All" />
                                                </th>
                                                <th>
                                                    Name ( username )
                                                </th>
                                                <th>
                                                    Email
                                                </th>
                                                <th>
                                                    Sub Admin
                                                </th>
                                                <th>
                                                    Status
                                                </th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tbody>
                                        <tr class="repeaterItem">
                                            <td class="chkBox">
                                                <asp:CheckBox ID="chkSelectUser" runat="server" onchange="selectDeselectSelectAllChkBox(this);" />
                                                <asp:Label ID="lblSubAdminId" runat="server" Text='<%# Eval("SUBADMIN_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblUserId" runat="server" Text='<%# Eval("USER_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblUserFullName" runat="server" Style="display: none;"></asp:Label>
                                                <asp:Label ID="lblUserFirstName" runat="server" Visible="false" Text='<%# Eval("FIRST_NAME") %>'></asp:Label>
                                                <asp:Label ID="lblUserLastName" runat="server" Visible="false" Text='<%# Eval("LAST_NAME") %>'></asp:Label>
                                                <asp:Label ID="lblUserName" runat="server" Visible="false" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkUser" runat="server" OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("EMAIL_ID") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblSubAdmin" runat="server" Text='<%# Eval("FULL_NAME") %>'></asp:Label>
                                                <asp:Label ID="lblIsSubAdminBlocked" runat="server" Text='<%# Eval("SubAdminIsBlocked") %>'
                                                    Visible="false"></asp:Label>
                                                <asp:Label ID="lblIsSubAdminActive" runat="server" Text='<%# Eval("SubAdminIsActive") %>'
                                                    Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                <asp:Label ID="lblIsUserActiveStatus" runat="server" Text='<%# Eval("UserIsActive") %>'
                                                    Visible="false"></asp:Label>
                                                <asp:Label ID="lblIsUserBlockedStatus" runat="server" Text='<%# Eval("UserIsBlocked") %>'
                                                    Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tbody>
                                        <tr class="repeaterAlternatingItem">
                                            <td class="chkBox">
                                                <asp:CheckBox ID="chkSelectUser" runat="server" onchange="selectDeselectSelectAllChkBox(this);" />
                                                <asp:Label ID="lblSubAdminId" runat="server" Text='<%# Eval("SUBADMIN_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblUserId" runat="server" Text='<%# Eval("USER_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblUserFullName" runat="server" Style="display: none;"></asp:Label>
                                                <asp:Label ID="lblUserFirstName" runat="server" Visible="false" Text='<%# Eval("FIRST_NAME") %>'></asp:Label>
                                                <asp:Label ID="lblUserLastName" runat="server" Visible="false" Text='<%# Eval("LAST_NAME") %>'></asp:Label>
                                                <asp:Label ID="lblUserName" runat="server" Visible="false" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkUser" runat="server" OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("EMAIL_ID") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblSubAdmin" runat="server" Text='<%# Eval("FULL_NAME") %>'></asp:Label>
                                                <asp:Label ID="lblIsSubAdminBlocked" runat="server" Text='<%# Eval("SubAdminIsBlocked") %>'
                                                    Visible="false"></asp:Label>
                                                <asp:Label ID="lblIsSubAdminActive" runat="server" Text='<%# Eval("SubAdminIsActive") %>'
                                                    Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                <asp:Label ID="lblIsUserActiveStatus" runat="server" Text='<%# Eval("UserIsActive") %>'
                                                    Visible="false"></asp:Label>
                                                <asp:Label ID="lblIsUserBlockedStatus" runat="server" Text='<%# Eval("UserIsBlocked") %>'
                                                    Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </asp:Panel>
                    </asp:Panel>
                </div>
                <div>
                    <asp:Button ID="btnChangeSubAdmin" runat="server" Text="Change SubAdmin" OnClientClick="processChangeSubAdmin();return false;" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divModalContainer" style="display: none">
        <div id="divAddDesignationContainer" style="display: block">
            <asp:UpdatePanel ID="updModalContainer" runat="server">
                <ContentTemplate>
                    <div id="modalPopUUpUserDetailContent" runat="server">
                        <div id="divUserDetailUC">
                            <userDetails:Details ID="ucUserDetails" runat="server" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="divChngSubAdminModal" style="display: none">
        <asp:UpdatePanel ID="updChngSubAdminModal" runat="server">
            <ContentTemplate>
                <div id="divChangeSAPopUpError">
                </div>
                <div id="modalPopUpSubAdminContent" class="modalPopUpDetails" runat="server">
                    <div id="divUserSelectedPopUpContainer">
                        <div class="singleSection" style="float: left; width: 100%;">
                            <fieldset style="padding-top: 5px;">
                                <section>
                                    <div class="column1">
                                        <asp:Label ID="lblForUserNames" runat="server" Text="User Name"></asp:Label>
                                    </div>
                                    <div class="column2">
                                        <asp:Label ID="Label2" runat="server" Text=":"></asp:Label>
                                    </div>
                                    <div class="column3">
                                        <asp:Label ID="lblUserNameSelected" runat="server"></asp:Label>
                                    </div>
                                </section>
                                <section style="padding-top: 20px;">
                                    <div class="column1" style="margin-top: 10px;">
                                        <asp:Label ID="lblForDDlSubAdmin" runat="server" Text="New SubAdmin"></asp:Label>
                                    </div>
                                    <div class="column2" style="margin-top: 8px;">
                                        <asp:Label ID="Label3" runat="server" Text=":"></asp:Label>
                                    </div>
                                    <div class="column3">
                                        <asp:DropDownList ID="ddlPopUpSubAdmin" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </section>
                                <div style="height: 10px; clear: both;">
                                </div>
                                <div class="modalPopUpAction">
                                    <div id="divPopUpActionCont" class="popUpActionCont" style="width: 60%">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="aspButton" OnClick="btnSave_Click" />
                                        <asp:Button ID="btnClose" runat="server" Text="Cancel" CssClass="aspButton" OnClientClick="closeModalPopUp('divModalContainer'); return false;" />
                                    </div>
                                    <asp:HiddenField ID="hfdSubAdminId" runat="server"></asp:HiddenField>
                                    <asp:HiddenField ID="hfdUserId" runat="server"></asp:HiddenField>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {

            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {

            hideWaitModal();
            isCookieCleanUpRequired('true');
            setOnClickEventOfRptChkBoxContSpan();
        }
    </script>
</asp:Content>

﻿mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Picture = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, cntrlObj) {
                $(control).val(cntrlObj.type.UIName)
            },
            Name: function (control, cntrlObj) {
                $(control).val(cntrlObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName)
                            evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            LabelText: function (control, appObj) {
                $(control).val(appObj.labelText);
                if (appObj.labelText != undefined && appObj.labelText != null && appObj.labelText.length > 0) {
                    $('#' + appObj.id + '_Label').html(appObj.labelText);
                    $('#' + appObj.id + '_LabelOuterDiv').show();
                }
                else {
                    $('#' + appObj.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                    $('#' + appObj.id + '_LabelOuterDiv').hide();
                }
            },
            FontColor: function (control, appObj, options) {
                var currentCntrl = options.cntrlHtmlObjects;
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelValueProperty = $('#' + appObj.id + '_Label');
                if (appObj.fontColor != undefined || appObj.fontColor != null) {
                    $(currentCntrl.control).val(appObj.fontColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.fontColor);
                    $($LabelValueProperty[0]).css('color', '#' + appObj.fontColor + '!important');
                }

                var fontCol = '';
                var fontColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            fontCol = '#' + hex;
                        }
                        $(currentCntrl.control).val(fontCol);
                        $(currentCntrl.browseBtn).css('background-color', fontCol);
                        appObj.fontColor = $(currentCntrl.control).val();
                        if (fontCol.length > 0) {
                            $($LabelValueProperty[0]).css('color', fontCol + '!important');
                        }
                        else {
                            $($LabelValueProperty[0]).css('color', '#000000' + '!important');
                        }
                    }
                });
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            FontStyle: function (control, appObj) {
                if (appObj.fontStyle == undefined || appObj.fontStyle == null)
                    appObj.fontStyle = "0";
                $(control).val(appObj.fontStyle);
                var lblStyle = getTextFontStyle(appObj);
                $('#' + appObj.id + '_Label').removeClass();
                $('#' + appObj.id + '_Label').addClass(lblStyle);
            },
            Width: function (control, cntrlObj) {
                $(control).val(cntrlObj.fnGetWidth());
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
                cntrlObj.fnSetWidth(cntrlObj.fnGetWidth());
            },
            Cropping: function (control, appObj) {
                if (appObj.cropping == undefined || appObj.cropping == null)
                    appObj.cropping = "0";
                $(control).val(appObj.cropping);
            },
            eventHandlers: {
                LabelTextChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var $LabelProperty = $('#' + propObject.id);
                    if (evnt.target.value.length > 0) {
                        $('#' + propObject.id + '_Label').html(evnt.target.value);
                        $('#' + propObject.id + '_LabelOuterDiv').show();
                    }
                    else {
                        $('#' + propObject.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                        $('#' + propObject.id + '_LabelOuterDiv').hide();
                    }
                    propObject.labelText = evnt.target.value;
                },
                FontStyleChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fontStyle = evnt.target.value;
                    var lblStyle = getTextFontStyle(propObject);
                    $('#' + propObject.id + '_Label').removeClass();
                    $('#' + propObject.id + '_Label').addClass(lblStyle);
                },
                IsCropping: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.cropping = evnt.target.value;
                },
                Width: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var $self = $(this),
                       newVal = $self.val(),
                       prevVal = propObject.fnGetWidth(),
                       propObject = evnt.data.propObject;

                    if (evnt.target.value.trim().length <= 0 || parseInt(evnt.target.value, 10) < 20) {
                        showMessage("Thumbnail width should be greater than 20%.", DialogType.Error);
                        $self.val(propObject.width);
                        return;
                    }

                    if (parseInt(evnt.target.value, 10) > 100) {
                        showMessage("Thumbnail width/height should not be greater than 100%.", DialogType.Error);
                        $self.val(prevVal);
                        return;
                    }

//                    if (parseInt(evnt.target.value, 10) > parseInt(propObject.maxWidth, 10)) {
//                        showMessage("Thumbnail width/height should not be greater than Maximum width.", DialogType.Error);
//                        $self.val(propObject.width);
//                        return;
//                    }

//                    if (parseInt(evnt.target.value, 10) > parseInt(propObject.maxHeight, 10)) {
//                        showMessage("Thumbnail width/height should not be greater than Maximum Height.", DialogType.Error);
//                        $self.val(propObject.width);
//                        return;
//                    }
                    propObject.fnSetWidth(parseInt(newVal, 10));
                }

            }
        },
        Data: {
            MaxHeight: function (control, cntrlObj) {
                $(control).val(cntrlObj.fnGetMaxHeight());
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
            },
            MaxWidth: function (control, cntrlObj) {
                $(control).val(cntrlObj.fnGetMaxWidth());
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
            },
            UploadType: function (control, cntrlObj) {
                if (cntrlObj.uploadType != null && cntrlObj.uploadType != undefined)
                    $(control).val(cntrlObj.uploadType);
                else
                    $(control).val("0");
            },
            FilePath: function (control, cntrlObj, options) {
                var currentCntrl = options.cntrlHtmlObjects;
                if (cntrlObj.uploadType == null || cntrlObj.uploadType == undefined) {
                    cntrlObj.uploadType = "0";
                }
                if (cntrlObj.uploadType === "0") {
                    $(control).val(cntrlObj.filePath);
                    currentCntrl.wrapperDiv.show();
                }
                else {
                    $(control).val("");
                    currentCntrl.wrapperDiv.hide();
                }
            },
            AWSPath: function (control, cntrlObj, options) {
                var currentCntrl = options.cntrlHtmlObjects;
                if (cntrlObj.uploadType != null && cntrlObj.uploadType !== undefined && cntrlObj.uploadType === "1") {
                    $(control).val(cntrlObj.filePath);
                    currentCntrl.wrapperDiv.show();
                }
                else {
                    $(control).val("");
                    currentCntrl.wrapperDiv.hide();
                }
            },
            BucketName: function (control, cntrlObj, options) {
                var currentCntrl = options.cntrlHtmlObjects;
                if (cntrlObj.uploadType != null && cntrlObj.uploadType !== undefined && cntrlObj.uploadType === "1") {
                    $(control).val(cntrlObj.bucketName);
                    currentCntrl.wrapperDiv.show();
                }
                else {
                    $(control).val("");
                    currentCntrl.wrapperDiv.hide();
                }
            },
            Credential: function (control, cntrlObj, options) {
                var strOptionsForDdl =
                    MF_HELPERS.dropDownOptionsHelpers.getawsCredentials(true, "-1");
                var currentCntrl = options.cntrlHtmlObjects;
                if (strOptionsForDdl) {
                    $(control).html(strOptionsForDdl);
                    $(control).val("-1");
                }
                if (cntrlObj.uploadType != null && cntrlObj.uploadType !== undefined && cntrlObj.uploadType === "1") {
                    $(control).val(cntrlObj.credential);
                    currentCntrl.wrapperDiv.show();
                }
                else {
                    currentCntrl.wrapperDiv.hide();
                }
            },
            MPlugin: function (control, cntrlObj) {
                var strOptionsForDdl =
                    MF_HELPERS.dropDownOptionsHelpers.getmPluginAgents(true, "-1"),

                    mPlugin = cntrlObj.fnGetmPlugin();
                $(control).html('');
                if (strOptionsForDdl) {
                    $(control).html(strOptionsForDdl);
                }
                if (mPlugin) {
                    $(control).val(mPlugin);
                }
            },
            eventHandlers: {
                MaxHeight: function (evnt) {

                    var $self = $(this),
                        newVal = $self.val(),
                        propObject = evnt.data.propObject,
                        prevVal = propObject.fnGetMaxHeight();

                    if (parseInt(newVal, 10) < 100) {
                        showMessage("Maximum height should be greater than 100.", DialogType.Error);
                        $self.val(prevVal);
                    }
                    else
                        propObject.fnSetMaxHeight(newVal);

//                    if (propObject.width != undefined && propObject.width.trim().length > 0) {
//                        if (evnt.target.value.trim().length <= 0) {
//                            showMessage("Maximum width should be greater than thumbnail width.", DialogType.Error);
//                            $self.val(propObject.maxWidth);
//                        }
//                    }
                },
                MaxWidth: function (evnt) {
                    var $self = $(this),
                         newVal = $self.val(),
                         propObject = evnt.data.propObject,
                         prevVal = propObject.fnGetMaxWidth();

                    if (parseInt(newVal, 10) < 100) {
                        showMessage("Maximum width should be greater than 100.", DialogType.Error);
                        $self.val(prevVal);
                    }
                    else
                        propObject.fnSetMaxWidth(newVal);

//                    if (propObject.width != undefined && propObject.width.trim().length > 0) {
//                        if (evnt.target.value.trim().length <= 0) {
//                            showMessage("Maximum width should be greater than thumbnail width.", DialogType.Error);
//                            $self.val(propObject.maxWidth);
//                        }
//                    }
                },
                UploadTypeChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.uploadType = $(this).val();
                    var cntrls = evnt.data.cntrls;
                    var $FilePathWrapper = $('#' + cntrls.FilePath.wrapperDiv);
                    var $AWSPathWrapper = $('#' + cntrls.AWSPath.wrapperDiv);
                    var $BucketNameWrapper = $('#' + cntrls.BucketName.wrapperDiv);
                    var $CredentialWrapper = $('#' + cntrls.Credential.wrapperDiv);

                    var $FilePathControl = $('#' + cntrls.FilePath.controlId);
                    var $AWSPathControl = $('#' + cntrls.AWSPath.controlId);
                    var $BucketNameControl = $('#' + cntrls.BucketName.controlId);
                    var $CredentialControl = $('#' + cntrls.Credential.controlId);

                    $FilePathControl.val("");
                    $AWSPathControl.val("");
                    $BucketNameControl.val("");
                    $CredentialControl.val("-1");

                    propObject.filePath = "";
                    propObject.bucketName = "";
                    propObject.credential = "-1";

                    if (propObject.uploadType === "0") {
                        $FilePathWrapper.show();
                        $AWSPathWrapper.hide();
                        $BucketNameWrapper.hide();
                        $CredentialWrapper.hide();
                    }
                    else {
                        $FilePathWrapper.hide();
                        $AWSPathWrapper.show();
                        $BucketNameWrapper.show();
                        $CredentialWrapper.show();
                    }
                },
                FilePath: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.filePath = $(this).val();
                },
                AWSPath: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.filePath = $(this).val();
                },
                BucketName: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.bucketName = $(this).val();
                },
                Credential: function (evnt) {
                    var propObject = evnt.data.propObject,
                        $self = $(this);
                    propObject.credential = $self.val();
                }
            }
        },
        Validation: {
            Required: function (control, appObj) {
                $(control).val(appObj.required)
            },
            eventHandlers: {
                IsRequired: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.required = evnt.target.value;
                }
            }
        },
        Advance: {
            Scripts: function (control, appObj) {
            },
            eventHandlers: {
                Scripts: function (evnt) {
                    $('#drpCntrlEvents').html();
                    $('#drpCntrlEvents').css('width', '');
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    $('#spanOnChngUserDefinedName').text(propObject.userDefinedName);

                    var scriptOnChange = '', scriptOnClear = '', script = '';

                    _jsAceEditorOnChang.setTextInEditor("");
                    var options = '<option value="onChange">onChange</option>'
                                + '<option value="onClear">onClear</option>';

                    $('#drpCntrlEvents').html(options);
                    $('#drpCntrlEvents').val("onChange");
                    $.uniform.update('#drpCntrlEvents');
                    var prevSelectedVal = $('#drpCntrlEvents')[0].value;

                    if (propObject.onChange != null && propObject.onChange != undefined) scriptOnChange = propObject.onChange;
                    if (propObject.onClear != null && propObject.onClear != undefined) scriptOnClear = propObject.onClear;
                    switch ($('#drpCntrlEvents')[0].value) {
                        case "onChange":
                            if (scriptOnChange != null && scriptOnChange != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));
                            }
                            break;
                        case "onClear":
                            if (scriptOnClear != null && scriptOnClear != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnClear));
                            }
                            break;
                    }
                    $('#drpCntrlEvents').unbind('change');
                    $('#drpCntrlEvents').bind('change', function () {
                        $.uniform.update('#drpCntrlEvents');
                        script = _jsAceEditorOnChang.getText();
                        switch (prevSelectedVal) {
                            case "onChange":
                                if (script != null && script != undefined) {
                                    scriptOnChange = Base64.encode(script);
                                }
                                break;
                            case "onClear":
                                if (script != null && script != undefined) {
                                    scriptOnClear = Base64.encode(script);
                                }
                                break;
                        }
                        _jsAceEditorOnChang.setTextInEditor("");
                        prevSelectedVal = this.value;
                        switch (this.value) {
                            case "onChange":
                                if (scriptOnChange != null && scriptOnChange != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));
                                }
                                break;
                            case "onClear":
                                if (scriptOnClear != null && scriptOnClear != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnClear));
                                }
                                break;
                        }
                    });

                    SubProcShowCntrlJsEditor(true, "Scripts");

                    $('[id$=popup_custom_buttom_save]').unbind('click');
                    $('[id$=popup_custom_buttom_save]').bind('click', function () {
                        bindAdvanceScriptsSignatureAndPicture(propObject, scriptOnChange, scriptOnClear);
                        return false;
                    });
//                    $('[id$=btnCntrlJsSaveClose]').unbind('click');
//                    $('[id$=btnCntrlJsSaveClose]').bind('click', function () {
//                        bindAdvanceScriptsSignatureAndPicture(propObject, scriptOnChange, scriptOnClear);
//                        SubProcShowCntrlJsEditor(false);
//                        return false;
//                    });

                    $('[id$=popup_custom_buttom_close]').unbind('click');
                    $('[id$=popup_custom_buttom_close]').bind('click', function () {
                        scriptOnChange = '', scriptOnClear = ''
                        SubProcShowCntrlJsEditor(false);
                        return false;
                    });
                }
            }
        },
        Behaviour: {
            Display: function (control, cntrlObj) {
                if (cntrlObj.conditionalDisplay) {
                    $(control).val(cntrlObj.conditionalDisplay);
                }
            },
            Conditions: function (control, cntrlObj, options) {
                //var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Textbox.propSheetCntrl.Behaviour.getConditionsCntrl();
                var currentCntrl = options.cntrlHtmlObjects;
                if (cntrlObj.conditionalDisplay == "0")
                    currentCntrl.wrapperDiv.show();
                else
                    currentCntrl.wrapperDiv.hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl(
                        {
                            FirstDropDownOptions: json,
                            ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(),
                            IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)),
                            IsEdit: (propObject.condDisplayCntrlProps) ? true : false,
                            SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps)
                        }
                    );
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 500, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    }
};
mFicientIde.PropertySheetJson.Picture = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PICTURE,
    "groups": [
       {
           "name": "Control Properties",
           "prefixText": "ControlProperties",
           "hidden": true,
           "properties": [
             {
                 "text": "Type",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                 "prefixText": "Type",
                 "noOfLines": "0",
                 "validations": [

                ],
                 "customValidations": [

                ],
                 "events": [

                ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "init": {
                     "maxlength": 50,
                     "disabled": true,
                     "defltValue": "Picture",
                     "hidden": false
                 }
             },
             {
                 "text": "Name",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                 "prefixText": "Name",
                 "noOfLines": "0",
                 "validations": [

                ],
                 "customValidations": [

                ],
                 "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Picture.groups.ControlProperties.eventHandlers.NameChanged,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "init": {
                     "maxlength": 50,
                     "disabled": false,
                     "defltValue": "",
                     "hidden": false
                 }
             },
             {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Picture.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
          ]
       },
       {
           "name": "Appearance",
           "prefixText": "Appearance",
           "hidden": false,
           "properties": [
            {
                "text": "Label Text",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "LabelText",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Picture.groups.Appearance.eventHandlers.LabelTextChange,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Picture.groups.Appearance.eventHandlers.LabelTextChange,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
           {
                "text": "Font Style",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FontStyle",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Picture.groups.Appearance.eventHandlers.FontStyleChange,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Normal",
                      "value": "0"
                  },
                  {
                      "text": "Bold",
                      "value": "1"
                  },
                  {
                      "text": "Italics",
                      "value": "2"
                  },
                  {
                      "text": "Bold And Italics",
                      "value": "3"
                  }
               ]
              },
              {
                  "text": "Font Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "FontColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
             {
                 "text": "Thumbnail (%)",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                 "prefixText": "Width",
                 "noOfLines": "0",
                 "validations": [
                ],
                 "customValidations": [
                ],
                 "events": [
                     {
                         "name": "change",
                         "func": PROP_JSON_HTML_MAP.Picture.groups.Appearance.eventHandlers.Width,
                         "context": "",
                         "arguments": {
                         }
                     }
                ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "init": {
                     "maxlength": 3,
                     "disabled": false,
                     "defltValue": "10",
                     "hidden": false
                 }
             },
             {
                 "text": "Cropping",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                 "prefixText": "Cropping",
                 "defltValue": "0",
                 "validations": [

               ],
                 "customValidations": [

               ],
                 "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Picture.groups.Appearance.eventHandlers.IsCropping,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "selectValueBy": "",
                 "init": {
                     "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                     "disabled": false,
                     "hidden": false
                 },
                 "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
             }
          ]
       },
       {
           "name": "Data",
           "prefixText": "Data",
           "hidden": false,
           "properties": [
           {
               "text": "Max Width (px)",
               "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
               "prefixText": "MaxWidth",
               "defltValue": "1",
               "validations": [

                ],
               "customValidations": [

                ],
               "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Picture.groups.Data.eventHandlers.MaxWidth,
                       "context": "",
                       "arguments": {
                       }
                   }
                ],
               "CntrlProp": "",
               "HelpText": "",
               "selectValueBy": "",
               "init": {
                   "bindOptionsType": "",
                   "disabled": false,
                   "hidden": false
               },
               "options": [
                ]
           },
           {
               "text": "Max Height (px)",
               "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
               "prefixText": "MaxHeight",
               "defltValue": "1",
               "validations": [

                ],
               "customValidations": [

                ],
               "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Picture.groups.Data.eventHandlers.MaxHeight,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
               "CntrlProp": "",
               "HelpText": "",
               "selectValueBy": "",
               "init": {
                   "bindOptionsType": "",
                   "disabled": false,
                   "hidden": false
               },
               "options": [
                ]
           },
           {
                "text": "Upload to AWS S3",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "UploadType",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Picture.groups.Data.eventHandlers.UploadTypeChanged,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "FilePath",
                               rtrnPropNm: "FilePath"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "AWSPath",
                               rtrnPropNm: "AWSPath"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "BucketName",
                               rtrnPropNm: "BucketName"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "Credential",
                               rtrnPropNm: "Credential"
                           }
					  ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
            },
            {
               "text": "File Path",
               "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
               "prefixText": "FilePath",
               "defltValue": "1",
               "validations": [

                ],
               "customValidations": [

                ],
               "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Picture.groups.Data.eventHandlers.FilePath,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
               "CntrlProp": "",
               "HelpText": "",
               "selectValueBy": "",
               "init": {
                   "bindOptionsType": "",
                   "disabled": false,
                   "hidden": true
               },
               "options": [
                ]
           },
            {
                "text": "S3 Path",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "AWSPath",
                "defltValue": "1",
                "validations": [

                ],
                "customValidations": [

                ],
                "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Picture.groups.Data.eventHandlers.AWSPath,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": "",
                    "disabled": false,
                    "hidden": true
                },
                "options": [
                ]
            },
            {
                "text": "S3 Bucket",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "BucketName",
                "defltValue": "1",
                "validations": [

                ],
                "customValidations": [

                ],
                "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Picture.groups.Data.eventHandlers.BucketName,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "maxlength": 256,
                    "bindOptionsType": "",
                    "disabled": false,
                    "hidden": true
                },
                "options": [
                ]
            },
            {
                "text": "Credential",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Credential",
                "defltValue": "1",
                "validations": [

                ],
                "customValidations": [

                ],
                "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Picture.groups.Data.eventHandlers.Credential,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": "",
                    "disabled": false,
                    "hidden": true
                },
                "options": [
                ]
            },
            {
               "text": "mPlugin Agent",
               "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
               "prefixText": "MPlugin",
               "defltValue": "1",
               "validations": [

                ],
               "customValidations": [

                ],
               "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Picture.groups.Data.eventHandlers.MPlugin,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
               "CntrlProp": "",
               "HelpText": "",
               "selectValueBy": "",
               "init": {
                   "bindOptionsType": "",
                   "disabled": false,
                   "hidden": true
               },
               "options": [
                ]
           }
         ]
       },
       {
           "name": "Validation",
           "prefixText": "Validation",
           "hidden": false,
           "properties": [
          {
              "text": "Required",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
              "prefixText": "Required",
              "defltValue": "0",
              "validations": [

               ],
              "customValidations": [

               ],
              "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Picture.groups.Validation.eventHandlers.IsRequired,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "selectValueBy": "",
              "init": {
                  "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                  "disabled": false,
                  "hidden": false
              },
              "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
          }
          ]
          },
          {
              "name": "Advance",
              "prefixText": "Advance",
              "hidden": false,
              "properties": [
          {
              "text": "Scripts",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
              "prefixText": "Scripts",
              "disabled": false,
              "hidden": true,
              "noOfLines": "0",
              "validations": [

               ],
              "customValidations": [
               ],
              "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Picture.groups.Advance.eventHandlers.Scripts,
                      "context": "",
                      "arguments": {}
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "init": {
                  "disabled": false,
                  "defltValue": "",
                  "hidden": false
              }
          }
          ]
          },
       {
           "name": "Behaviour",
           "prefixText": "Behaviour",
           "hidden": false,
           "properties": [
             {
                 "text": "Display",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                 "prefixText": "Display",
                 "defltValue": "1",
                 "validations": [
                 ],
                 "customValidations": [
                 ],
                 "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Picture.groups.Behaviour.eventHandlers.Display,
                       "context": "",
                       "arguments": {
                           "cntrls": [
                            {
                                grpPrefText: "Behaviour",
                                cntrlPrefText: "Conditions",
                                rtrnPropNm: "Conditions"
                            }
                         ]
                       }
                   }

                ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "selectValueBy": "",
                 "init": {
                     "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                     "disabled": false,
                     "hidden": false
                 },
                 "options": [
                   {
                       "text": "Always",
                       "value": "1"
                   },
                   {
                       "text": "Conditional",
                       "value": "0"
                   }
                ]
             },
             {
                 "text": "Conditions",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                 "prefixText": "Conditions",
                 "disabled": false,
                 "noOfLines": "0",
                 "validations": [
                 ],
                 "customValidations": [
                ],
                 "events": [
                   {
                       "name": "click",
                       "func": PROP_JSON_HTML_MAP.Picture.groups.Behaviour.eventHandlers.ConditionalDisplay,
                       "context": "",
                       "arguments": {}
                   }
                ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "init": {
                     "disabled": false,
                     "defltValue": "Select Conditions",
                     "hidden": true
                 }
             }
          ]
       }
    ]
};

function Picture(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Picture;
    this.description = opts.description;
    this.type = opts.type;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.labelText = opts.labelText;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.maxHeight = opts.maxHeight;
    this.maxWidth = opts.maxWidth;
    this.filePath = opts.filePath;
    this.mPlugin = opts.mPlugin;
    this.required = opts.required;
    this.width = opts.width;
    this.uploadType = opts.uploadType;
    this.bucketName = opts.bucketName;
    this.credential = opts.credential;
    this.fontStyle = opts.fontStyle;
    this.fontColor = opts.fontColor;
    this.cropping = opts.cropping;
    this.onChange = opts.onChange;
    this.onClear = opts.onClear;
}

Picture.prototype = new ControlNew();
Picture.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Picture;
Picture.prototype.fnSetDefaults = function () {
    this.fnSetWidth('100');
    this.fnSetFilePath("");
    this.fnSetMaxWidth('150');
    this.fnSetMaxHeight('150');

    this.uploadType = "0";
    this.isNew = true;
    this.labelText = "Label Text";
    this.required = "0";
    this.conditionalDisplay = "1";
    this.fontStyle = "0";
    this.cropping = "0";
    this.isDeleted = false;
};
Picture.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
Picture.prototype.fnGetmPlugin = function () {
    return this.mPlugin;
};
Picture.prototype.fnSetmPlugin = function (value) {
    this.mPlugin = value;
};
Picture.prototype.fnGetMaxWidth = function () {
    return this.maxWidth;
};
Picture.prototype.fnSetMaxWidth = function (value) {
    this.maxWidth = value;
};
Picture.prototype.fnGetWidth = function () {
    if (parseFloat(this.width).toFixed(2) > 100) this.width = 100;
    return this.width;
};
Picture.prototype.fnSetWidth = function (value) {
    this.width = value;
    $('#' + this.id + '_OuterDiv').css('width', value + '%');
    var calWidth = $('#' + this.id + '_OuterDiv').width();
    $('#' + this.id + '_OuterDiv').css('height', calWidth + 'px');
};
Picture.prototype.fnGetMaxHeight = function () {
    return this.maxHeight;
};
Picture.prototype.fnSetMaxHeight = function (value) {
    this.maxHeight = value;
};
Picture.prototype.fnGetFilePath = function () {
    return this.filePath;
};
Picture.prototype.fnSetFilePath = function (value) {
    this.filePath = value;
};
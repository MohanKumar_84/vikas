﻿
var dsSelectedData;
var dsCompleteData;
var pageLength = 12;
var DataObjParaCount = 1;
var isAddClicked = false;
var previousDataObjectId = "";
var EditIndex = -1;
var PreviousEditIndex = -1;
var strInputParaJson = '';
var SearchText = "";
var objectUsed = false;

//Odata command global variable
var OdataCmdGlobalVar =
{
    StructuralProp: '',
    SelectedEntityTypeKeys: '',
    count: 1,
    SelectJson: '[]',
    OrderByJson: '[]',
    CommandParam: '',
    ParamCount: 1,
    OutParam: '',
    functionInParam: '',
    Type: '',
    ResponseFormat: '0',
    ReturnType: '0',
    XmlDsPath: 'feed.entry[]',
    XmlSingle: 'entry.content.properties',
    FunctionReturnColl: '',
    ServiceEndpoint: '',
    ServiceVersion: ''
}

function OdataCmdInitVariable(_IsNew) {
    if (_IsNew) {
        OdataCmdGlobalVar.StructuralProp = '';
        OdataCmdGlobalVar.SelectedEntityTypeKeys = '';
        OdataCmdGlobalVar.count = 1;
        OdataCmdGlobalVar.SelectJson = '[]';
        OdataCmdGlobalVar.OrderByJson = '[]';
        OdataCmdGlobalVar.CommandParam = '';
        OdataCmdGlobalVar.ParamCount = 1;
        OdataCmdGlobalVar.OutParam = '';
        OdataCmdGlobalVar.functionInParam = '';
        OdataCmdGlobalVar.ServiceEndpoint = '';
        OdataCmdGlobalVar.ServiceVersion = '';
    }
}

function initializeDataObjects() {
    var isAddClicked = false;
    var previousDataObjectId = "";
    initializeControls();
    deserializeDataJson();
    bindODataObjectsTable();
    addNewODataObject();
    OdataCmdInitVariable(true);
}

function bindODataObjectsTable() {
    $('#divListDataObject').html('<table cellpadding="0" cellspacing="0" border="0" width="100%" class="display" id="tblDataObjects"></table>');
    $('#tblDataObjects').dataTable({
        "data": dsSelectedData,
        "pageLength": pageLength,
        "language": {
            "lengthMenu": "_MENU_"
        },
        "columns": [
            { "title": "ID" },
            { "title": "OBJECT" },
            { "title": "CONNECTOR" },
            { "title": "TYPE" }
        ],
        "columnDefs": [
			{ "visible": false, "targets": [0] }
		]
    });
    DataObjectRowClick();
    searchText("tblDataObjects");
    var table = $('#tblDataObjects').DataTable();
    table.search(SearchText).draw();
    $('#tblDataObjects_length select').prop('id', 'pagesize');
    $('#tblDataObjects_length select').prop('class', 'UseUniformCss');
    $('#tblDataObjects_filter').css("padding-bottom", "10px");


    CallUnformCss('[id$=pagesize]');
}

function deserializeDataJson() {
    dsSelectedData = jQuery.parseJSON($('[id$=hdfSelectedData]').val());
    dsCompleteData = jQuery.parseJSON($('[id$=hdfCompleteData]').val());
}

function DataObjectRowClick() {
    var table = $('#tblDataObjects').DataTable();
    $('#tblDataObjects tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {

        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

        var rowData = table.row(this).data();
        if (EditIndex != -1) PreviousEditIndex = EditIndex;
        EditIndex = $(this).index();
        if (rowData != undefined) {
            isAddClicked = false;
            if (checkForDiscardChanges()) {
                $('[id$=hdfDataObjectId]').val(rowData[0]);
                return;
            }
            else {
                $('[id$=hdfDataObjectId]').val(rowData[0]);
                viewODataObject();
            }
        }
        else {
            alert('No OData Object Selected');
        }
    });
}

function ChangeSelection() {
    var table = $('#tblDataObjects').DataTable();
    table.$('tr.selected').removeClass('selected');

    if (PreviousEditIndex != undefined && PreviousEditIndex != -1) {
        EditIndex = -1;
        if (PreviousEditIndex % 2 === 0) {
            $('#tblDataObjects tbody tr').eq(PreviousEditIndex).addClass('even selected')
        }
        else {
            $('#tblDataObjects tbody tr').eq(PreviousEditIndex).addClass('odd selected')
        }
    }

}

function chkSingleValueOnChange(e) {
    showConnectorInfo();
    ShowAdditionalPathDiv($('[id$=chkSingleValue]')[0].checked);
}

function ShowAdditionalPathDiv(_Show) {
    showConnectorInfo();
    if (_Show) {
        $('#ODataCmd_HttpType_SelectDiv').hide();
    }
    else {
        $('#ODataCmd_HttpType_SelectDiv').show();
    }
}

function UniformControlDesign() {
    showConnectorInfo();
    CallUnformCss('[id$=ddlOdataCmd_Conn]');
    CallUnformCss('[id$=ddlOdataCmd_ResFormat]');
    CallUnformCss('[id$=ddlOdataCmd_EntType]');
    CallUnformCss('[id$=ddlOdataCmd_EntityType]');
    CallUnformCss('[id$=ddlOdataCmd_funcImport]');
    CallUnformCss('[id$=lblOdataCmd_FuncType]');
    CallUnformCss('[id$=ddlOdataCmd_Ent_HttpTyp]');
    CallUnformCss('[id$=ddlOdataCmd_UsingKey]');
    CallUnformCss('[id$=ddlOdataCmd_Ent_ReturnTyp]');
    CallUnformCss('[id$=chkSingleValueOnChange]');

    //Data Cache

    CallUnformCss('[id$=ddl_DataCache_type]');
    CallUnformCss('[id$=ddl_AbsoluteCachePeriod]');
    CallUnformCss('[id$=ddl_AbsoluteMinutes]');
    CallUnformCss('[id$=ddl_AbsoluteDayHour]');
    CallUnformCss('[id$=ddl_AbsoluteDayMin]');
    CallUnformCss('[id$=ddl_AbsoluteWeek]');
    CallUnformCss('[id$=ddl_AbsoluteDate]');
    CallUnformCss('[id$=ddl_AbsoluteYear_Day]');
    CallUnformCss('[id$=ddl_AbsoluteYear_Month]');
    CallUnformCss('[id$=ddl_RelativeCachePeriod]');
    CallUnformCss('[id$=ddl_RelativeMinute]');
    CallUnformCss('[id$=ddl_RelativeHour]');
    CallUnformCss('[id$=ddl_RelativeDay]');
    CallUnformCss('[id$=ddl_RelativeWeek]');
    CallUnformCss('[id$=ddl_RelativeMonth]');
}

// Confirmation message popup
function SubProcConfirmBoxMessageDB(_bol, _title, _width) {
    if (_bol) {
        showModalPopUp('SubProcConfirmBoxMessage', _title, _width);
    }
    else {

        $('#SubProcConfirmBoxMessage').dialog('close');
    }
}

//show delete message popup
function SubProcBoxMessage(_bol) {
    if (_bol) {
        showModalPopUp('SubProcBoxMessage', 'Error', 400);
        $('#btnOkErrorMsg').focus();
        return false;
    }
    else {

        $('#SubProcBoxMessage').dialog('close');
    }
}

function initializeControls() {
    // DetailsDivControls
    $('[id$=lblDataObjectName]').text('');
    $('[id$=lblDesc]').text('');
    $('[id$=lblConnector]').text('');
    $('[id$=lblResourceType]').text('');
    $('[id$=lblReturnType]').text('');
    $('[id$=lblEntityType]').text('');
    $('[id$=lblHttpType]').text('');
    $('[id$=lblFunctionName]').text('');
    $('[id$=lblFunctionType]').text('');
    $('[id$=lblCreatedBy]').text('');
    $('[id$=lblUpdatedBy]').text('');
    $('[id$=txtDataObj_Name]').val('');
    $('[id$=txtDataObj_Desc]').val('');

    // EditDivControls

    $('[id$=ddlOdataCmd_Conn]').val('-1');
    $('select[id$=ddlOdataCmd_Conn]').parent().children("span").text($('select[id$=ddlOdataCmd_Conn]').children('option:selected').text());

    initializeConChangedControls();

    //Data Cache

    $('[id$=ddl_DataCache_type]').val('0');
    $('select[id$=ddl_DataCache_type]').parent().children("span").text($('select[id$=ddl_DataCache_type]').children('option:selected').text());
    //ddl_DataCache_type_Changed($('[id$=ddl_DataCache_type]'));

    //initializeDataCache();
    UniformControlDesign();
}

function initializeConChangedControls() {
    $('[id$=lblEditDataObj_Name]').text('');
    $('#lblOdataInputPara').html('No Parameters defined yet');

    $('[id$=ddlOdataCmd_ResFormat]').val('0');
    $('select[id$=ddlOdataCmd_ResFormat]').parent().children("span").text($('select[id$=ddlOdataCmd_ResFormat]').children('option:selected').text());
    ddlOdataCmd_ResFormat($('[id$=ddlOdataCmd_ResFormat]'));

    $('[id$=ddlOdataCmd_EntType]').val('-1');
    $('select[id$=ddlOdataCmd_EntType]').parent().children("span").text($('select[id$=ddlOdataCmd_EntType]').children('option:selected').text());
    resouceTypeChanged();

    $('[id$=ddlOdataCmd_EntityType]').val('-1');
    $('select[id$=ddlOdataCmd_EntityType]').parent().children("span").text($('select[id$=ddlOdataCmd_EntityType]').children('option:selected').text());
    entityTypeChanged();

    $('[id$=ddlOdataCmd_FuncImport]').val('-1');
    $('select[id$=ddlOdataCmd_FuncImport]').parent().children("span").text($('select[id$=ddlOdataCmd_FuncImport]').children('option:selected').text());
    functionTypeChanged();

    $('[id$=lblOdataCmd_FuncType]').text('');

    $('[id$=ddlOdataCmd_Ent_HttpTyp]').val('-1');
    $('select[id$=ddlOdataCmd_Ent_HttpTyp]').parent().children("span").text($('select[id$=ddlOdataCmd_Ent_HttpTyp]').children('option:selected').text());
    httpTypeChanged();

    $('[id$=ddlOdataCmd_UsingKey]').val('-1');
    $('select[id$=ddlOdataCmd_UsingKey]').parent().children("span").text($('select[id$=ddlOdataCmd_UsingKey]').children('option:selected').text());
    selectUsingKeyDisplayChanged();

    $('[id$=chkSingleValue]')[0].checked = false;
    $('[id$=txt_AdditionalResourcePath]').val('');
    $('[id$=txtOdataCmd_Top]').val('');
    $('[id$=txtOdataCmd_Skip]').val('');
    $('[id$=txtOdataCmd_Filter]').val('');

    $('[id$=ddlOdataCmd_Ent_ReturnTyp]').val('0');
    $('select[id$=ddlOdataCmd_Ent_ReturnTyp]').parent().children("span").text($('select[id$=ddlOdataCmd_Ent_ReturnTyp]').children('option:selected').text());
    ddlOdataCmd_Ent_ReturnTypChanged($('[id$=ddlOdataCmd_Ent_ReturnTyp]'));

    $('[id$=txtOdataCmd_Ent_DsPath]').val('');
    $('#ODataCmd_HttpType_GetKeyInDiv').html('');
    $('#ODataCmd_HttpType_PostInDiv').html('');

    //Hidden Fields
    $('[id$=hdfInsertQueryPara]').val('');
    $('[id$=hdfDbCmdPara]').val('');
    $('[id$=hidIntellegence]').val('');
    $('[id$=hdfOdata_CmdId]').val('');
    $('[id$=hdfOdata_HttpData]').val('');
    $('[id$=hdfOdata_HttpQuery]').val('');
    $('[id$=hdfOdata_InputParam]').val('');
    $('[id$=hdfOdata_OutputParam]').val('');
    $('[id$=hdfOdata_DataJson]').val('');
    $('[id$=hdfOdata_PutKeys]').val('');
    $('[id$=hdfOdata_FuncHType]').val('');
    $('[id$=hdfOdata_FuncType]').val('');

    $('#ODataCmd_EntityDiv').hide();
    $('#ODataCmd_FunctionDiv').hide();
    $('#ODataCmd_EntityDiv').hide();
    $('#ODataCmd_FunctionDiv').hide();

    showConnectorInfo();

    UniformControlDesign();
}

function initializeDataCache() {
    showConnectorInfo();
    $('[id$=ddl_AbsoluteCachePeriod]').val('0');
    $('select[id$=ddl_AbsoluteCachePeriod]').parent().children("span").text($('select[id$=ddl_AbsoluteCachePeriod]').children('option:selected').text());

    $('[id$=ddl_AbsoluteMinutes]').val('0');
    $('select[id$=ddl_AbsoluteMinutes]').parent().children("span").text($('select[id$=ddl_AbsoluteMinutes]').children('option:selected').text());

    $('[id$=ddl_AbsoluteDayHour]').val('0');
    $('select[id$=ddl_AbsoluteDayHour]').parent().children("span").text($('select[id$=ddl_AbsoluteDayHour]').children('option:selected').text());

    $('[id$=ddl_AbsoluteDayMin]').val('0');
    $('select[id$=ddl_AbsoluteDayMin]').parent().children("span").text($('select[id$=ddl_AbsoluteDayMin]').children('option:selected').text());

    $('[id$=ddl_AbsoluteWeek]').val('1');
    $('select[id$=ddl_AbsoluteWeek]').parent().children("span").text($('select[id$=ddl_AbsoluteWeek]').children('option:selected').text());

    $('[id$=ddl_AbsoluteDate]').val('1');
    $('select[id$=ddl_AbsoluteDate]').parent().children("span").text($('select[id$=ddl_AbsoluteDate]').children('option:selected').text());

    $('[id$=ddl_AbsoluteYear_Day]').val('1');
    $('select[id$=ddl_AbsoluteYear_Day]').parent().children("span").text($('select[id$=ddl_AbsoluteYear_Day]').children('option:selected').text());

    $('[id$=ddl_AbsoluteYear_Month]').val('1');
    $('select[id$=ddl_AbsoluteYear_Month]').parent().children("span").text($('select[id$=ddl_AbsoluteYear_Month]').children('option:selected').text());

    $('[id$=ddl_RelativeCachePeriod]').val('0');
    $('select[id$=ddl_RelativeCachePeriod]').parent().children("span").text($('select[id$=ddl_RelativeCachePeriod]').children('option:selected').text());

    $('[id$=ddl_RelativeMinute]').val('0');
    $('select[id$=ddl_RelativeMinute]').parent().children("span").text($('select[id$=ddl_RelativeMinute]').children('option:selected').text());

    $('[id$=ddl_RelativeHour]').val('0');
    $('select[id$=ddl_RelativeHour]').parent().children("span").text($('select[id$=ddl_RelativeHour]').children('option:selected').text());

    $('[id$=ddl_RelativeDay]').val('0');
    $('select[id$=ddl_RelativeDay]').parent().children("span").text($('select[id$=ddl_RelativeDay]').children('option:selected').text());

    $('[id$=ddl_RelativeWeek]').val('0');
    $('select[id$=ddl_RelativeWeek]').parent().children("span").text($('select[id$=ddl_RelativeWeek]').children('option:selected').text());

    $('[id$=ddl_RelativeMonth]').val('0');
    $('select[id$=ddl_RelativeMonth]').parent().children("span").text($('select[id$=ddl_RelativeMonth]').children('option:selected').text());

    $('#divAbsoluteTime').hide();
    $('#divRelativeTime').hide();
    $('#divAbsoluteMinutes').hide();
    $('#divAbsoluteDay').hide();
    $('#divAbsoluteWeek').hide();
    $('#divAbsoluteDate').hide();
    $('#divAbsoluteYear').hide();
    $('#divRelativeTime').hide();
    $('#divRelativeMinute').hide();
    $('#divRelativeHour').hide();
    $('#divRelativeDay').hide();
    $('#divRelativeWeek').hide();
    $('#divRelativeMonth').hide();

    UniformControlDesign();
}

function showConnectorInfo() {
    var value = "-1";
    if ($('[id$=ddlOdataCmd_Conn]')[0] != undefined) {
        value = $('[id$=ddlOdataCmd_Conn]')[0].value;
    }
    else if ($('[id$=ddlOdataCmd_Conn]')[1] != undefined) {
        value = $('[id$=ddlOdataCmd_Conn]')[1].value;
    }
    if (value != "-1")
        $('#imgODataConnectorInfoDiv').show();
    else
        $('#imgODataConnectorInfoDiv').hide();
}

function ddl_DataCache_type_Changed(e) {
    showConnectorInfo();
    initializeDataCache();
    if ($(e).val() == '1') {
        $('#divAbsoluteTime').show();
        $('#divAbsoluteMinutes').show();
        $('#divRelativeTime').hide();
        $('#divRelativeMinute').hide();
        $('#divRelativeHour').hide();
        $('#divRelativeDay').hide();
        $('#divRelativeWeek').hide();
        $('#divRelativeMonth').hide();
    }
    else if ($(e).val() == '2') {
        $('#divAbsoluteTime').hide();
        $('#divAbsoluteMinutes').hide();
        $('#divAbsoluteDay').hide();
        $('#divAbsoluteWeek').hide();
        $('#divAbsoluteDate').hide();
        $('#divAbsoluteYear').hide();
        $('#divRelativeTime').show();
        $('#divRelativeMinute').show();
    }
}

function ddl_AbsoluteCachePeriod_Changed(e) {
    showConnectorInfo();
    $('#divAbsoluteMinutes').hide();
    $('#divAbsoluteDay').hide();
    $('#divAbsoluteWeek').hide();
    $('#divAbsoluteDate').hide();
    $('#divAbsoluteYear').hide();
    switch ($(e).val()) {
        case '0':
            $('#divAbsoluteMinutes').show();
            break;
        case '1':
            $('#divAbsoluteDay').show();
            break;
        case '2':
            $('#divAbsoluteWeek').show();
            break;
        case '3':
            $('#divAbsoluteDate').show();
            break;
        case '4':
            $('#divAbsoluteYear').show();
            break;
    }
}

function ddl_RelativeCachePeriod_Changed(e) {
    showConnectorInfo();
    $('#divRelativeMinute').hide();
    $('#divRelativeHour').hide();
    $('#divRelativeDay').hide();
    $('#divRelativeWeek').hide();
    $('#divRelativeMonth').hide();
    switch ($(e).val()) {
        case '0':
            $('#divRelativeMinute').show();
            break;
        case '1':
            $('#divRelativeHour').show();
            break;
        case '2':
            $('#divRelativeDay').show();
            break;
        case '3':
            $('#divRelativeWeek').show();
            break;
        case '4':
            $('#divRelativeMonth').show();
            break;
    }
}

function CreateODataQueryUrl() {
    showConnectorInfo();
    if (OdataCmdValidation()) {
        if ($('[id$=ddlOdataCmd_EntType]')[1].value == '0') {
            var value = "";

            if ($('[id$=ddlOdataCmd_Ent_HttpTyp]')[1] != undefined) {
                value = $('[id$=ddlOdataCmd_Ent_HttpTyp]')[1].value;
            }
            else if ($('[id$=ddlOdataCmd_Ent_HttpTyp]')[0] != undefined) {
                value = $('[id$=ddlOdataCmd_Ent_HttpTyp]')[0].value;
            }
            switch (value) {
                case "-1":
                    break;
                case "1":
                    CreateODataQueryUrl_EntGet();
                    break;
                case "2":
                    CreateODataQueryUrl_EntPost();
                    break;
                case "3":
                case "4":
                case "5":
                    CreateODataQueryUrl_EntPut();
                    break;
                case "6":
                    CreateODataQueryUrl_EntDel();
                    break;
            }
        }
        else if ($('[id$=ddlOdataCmd_EntType]')[1].value == '1') {
            CreateODataQueryUrl_Func();
        }
        return true;
    }
    else
        return false;
}

//Odata command validation
function OdataCmdValidation() {
    showConnectorInfo();
    var isValid = true;
    var strMessage = "";
    if ($('[id$=hdfDataObjectId]').val().length == 0) {
        if ($('[id$=txtDataObj_Name]').length != 0) {
            var strVal = ValidateNameInIde($('[id$=txtDataObj_Name]').val().trim());
            if (strVal != 0) {
                if (strVal == 1) {
                    strMessage += " * Please enter object name.</br>";
                }
                else if (strVal == 2) {
                    strMessage += " * Object name cannot be more than 20 characters.</br>";
                }
                else if (strVal == 3) {
                    strMessage += " * Object name cannot be less than 3 characters.</br>";
                }
                else if (strVal == 4) {
                    strMessage += " * Please enter valid object name.</br>";
                }
                else if (strVal == 5) {
                    strMessage += " * Please enter valid object name.</br>";
                }
            }
        }
    }

    if ($('[id$=ddlOdataCmd_Conn]').length != 0) {
        if ($('[id$=ddlOdataCmd_Conn]')[1].value == "-1")
            strMessage += " * Please select connector.</br>";
    }
    if ($('[id$=ddlOdataCmd_EntType]').length != 0) {
        if ($('[id$=ddlOdataCmd_EntType]')[1].value == "-1")
            strMessage += " * Please select type.</br>";
        else if ($('[id$=ddlOdataCmd_EntType]')[1].value == "0") {
            if ($("#ODataCmd_EntityDiv").is(':visible')) {
                if ($('[id$=ddlOdataCmd_EntityType]').length != 0) {
                    if ($('[id$=ddlOdataCmd_EntityType]')[1].value == "-1")
                        strMessage += " * Please select entity type.</br>";
                    else {
                        if ($('[id$=ddlOdataCmd_Ent_HttpTyp]').length != 0) {
                            if ($('[id$=ddlOdataCmd_Ent_HttpTyp]')[1].value == "-1")
                                strMessage += " * Please select http type.<br />";
                        }
                    }
                }
            }
        }
        else if ($('[id$=ddlOdataCmd_EntType]')[1].value == "1") {
            if ($('[id$=ddlOdataCmd_funcImport]').length != 0) {
                if ($('[id$=ddlOdataCmd_funcImport]')[1].value == "-1")
                    strMessage += " * Please select function.</br>";
            }
        }
    }

    if (strMessage.length != 0) {
        $('[id$=btnDataObj_Cancel]').hide();
        $('#aMessage').html(strMessage);
        SubProcBoxMessage(true);
        isValid = false;
    }
    else {
    }
    UniformControlDesign();
    SearchText = $('#tablewsobj_filter label input').val();
    return isValid;

}

function cancelButtonClick() {
    if ($('[id$=hdfDataObjectId]').val().trim() != "") {
        showConnectorInfo();
        viewODataObject();
    }
    else {
        addNewODataObject();
    }
}

function viewODataObject() {
    initializeControls();
    $('#divDataObjectDetails').show();
    $('#divEditDataObject').hide();
    for (var i = 0; i < dsCompleteData.length; i++) {
        if ($('[id$=hdfDataObjectId]').val() == dsCompleteData[i].CommandId) {
            $('[id$=lblDataObjectName]').text(dsCompleteData[i].CommandName.trim());
            $('[id$=lblDesc]').text(dsCompleteData[i].Description.trim());
            $('[id$=lblConnector]').text(dsCompleteData[i].ConnectionName.trim());
            $('[id$=lblCreatedBy]').text(dsCompleteData[i].CreatedBy.trim());
            $('[id$=lblUpdatedBy]').text(dsCompleteData[i].UpdatedBy.trim() + " On " + dsCompleteData[i].UpdatedOn.trim());
            $('[id$=lblResourceType]').text(getResourceType(dsCompleteData[i].Resourcetype.toString().trim()));
            $('[id$=lblReturnType]').text(getReturnType(dsCompleteData[i].ReturnType.toString().trim()));
            $('[id$=hidparameter]').val(dsCompleteData[i].InputParajson);
            if (dsCompleteData[i].Resourcetype == 0) {
                $('#OdataCmdDet_EntDiv').show();
                $('#OdataCmdDet_FuncDiv').hide();
                $('[id$=lblEntityType]').text(dsCompleteData[i].ResourcePath.split('(')[0]);

                switch (dsCompleteData[i].Httptype) {
                    case 1:
                        if (dsCompleteData[i].SelectType == "1")
                            $('[id$=lblHttpType]').text("Get ( Primary Key ) ");

                        else if (dsCompleteData[i].SelectType == "0")
                            $('[id$=lblHttpType]').text("Get ( Without Primary Key ) ");
                        else
                            $('[id$=lblHttpType]').text("Get");
                        break;
                    case 2:
                        $('[id$=lblHttpType]').text("Post");
                        break;
                    case 3:
                        $('[id$=lblHttpType]').text("Put");
                        break;
                    case 4:
                        $('[id$=lblHttpType]').text("Patch");
                        break;
                    case 5:
                        $('[id$=lblHttpType]').text("Merge");
                        break;
                    case 6:
                        $('[id$=lblHttpType]').text("Delete");
                        break;
                }
            }
            else if (dsCompleteData[i].Resourcetype == 1) {
                $('#OdataCmdDet_EntDiv').hide();
                $('#OdataCmdDet_FuncDiv').show();
                $('[id$=lblFunctionName]').text(dsCompleteData[i].ResourcePath.split('?')[0]);
                $('[id$=lblFunctionType]').text(getFunctionType(dsCompleteData[i].Functiontype.toString().trim()));
            }
            objectUsed = false;
            var usage = jQuery.parseJSON(dsCompleteData[i].Usage);
            $('#DbCmdAppDiv').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="tblApps"></table>');
            if (usage.length > 0) {
                objectUsed = true;
                $('#tblApps').dataTable({
                    "data": usage,
                    "columns": [{ "title": "App" }, { "title": "View(s)" }, { "title": "Form(s)"}],
                    "sort": false,
                    "searching": false,
                    "paging": false,
                    "info": false
                });
            }
            else $('#DbCmdAppDiv').html("This object is not used any where.");

            break;
        }
    }
    UniformControlDesign();
}

function getResourceType(_type) {
    if (_type == "0")
        return "EntityType";
    else if (_type == "1")
        return "Function";
    else
        return "";
}

function getReturnType(_type) {
    if (_type == "0")
        return "Single Record";
    else if (_type == "1")
        return "Multiple Records";
    else if (_type == "2")
        return "Single Value";
}

function getFunctionType(_type) {
    if (_type == 1)
        return "WEBGET";
    else if (_type == 2)
        return "WEBINVOKE";
    else if (_type == 3)
        return "ACTION";
}


function editODataObject() {
    $('#divDataObjectDetails').hide();
    $('#editDataObjHeader').show();
    $('#addDataObjHeader').hide();
    $('#divDataObjName').hide();
    $('#divEditDataObject').show();
    $('[id$=btnDataObj_Cancel]').show();
    for (var i = 0; i < dsCompleteData.length; i++) {
        if ($('[id$=hdfDataObjectId]').val() == dsCompleteData[i].CommandId) {
            $('[id$=lblEditDataObj_Name]').text(dsCompleteData[i].CommandName);
            $('[id$=txtDataObj_Name]').val(dsCompleteData[i].CommandName);
            $('[id$=txtDataObj_Desc]').val(dsCompleteData[i].Description);

            $('[id$=txtOdataCmd_Ent_DsPath]').val(dsCompleteData[i].DataSetPath.trim());

            if (dsCompleteData[i].Responseformat.toString().trim() == "2" && dsCompleteData[i].AddtionalResourcepath.trim().length > 0) {
                $('[id$=chkSingleValue]')[0].checked = true;
            }
            else if (dsCompleteData[i].Responseformat.toString().trim() == "2" && dsCompleteData[i].AddtionalResourcepath.trim().length <= 0) {
                $('[id$=ddlOdataCmd_ResFormat]').val('0');
                $('select[id$=ddlOdataCmd_ResFormat]').parent().children("span").text($('select[id$=ddlOdataCmd_ResFormat]').children('option:selected').text());
                ddlOdataCmd_ResFormat($('[id$=ddlOdataCmd_ResFormat]'));
            }
            else {
                $('[id$=ddlOdataCmd_ResFormat]').val(dsCompleteData[i].Responseformat.toString().trim());
                $('select[id$=ddlOdataCmd_ResFormat]').parent().children("span").text($('select[id$=ddlOdataCmd_ResFormat]').children('option:selected').text());
                ddlOdataCmd_ResFormat($('[id$=ddlOdataCmd_ResFormat]'));
            }

            $('[id$=ddlOdataCmd_EntType]').val(dsCompleteData[i].Resourcetype);
            $('select[id$=ddlOdataCmd_EntType]').parent().children("span").text($('select[id$=ddlOdataCmd_EntType]').children('option:selected').text());
            resouceTypeChanged();

            $('[id$=ddlOdataCmd_Ent_ReturnTyp]').val(dsCompleteData[i].ReturnType.trim());
            $('select[id$=ddlOdataCmd_Ent_ReturnTyp]').parent().children("span").text($('select[id$=ddlOdataCmd_Ent_ReturnTyp]').children('option:selected').text());
            ddlOdataCmd_Ent_ReturnTypChanged($('select[id$=ddlOdataCmd_Ent_ReturnTyp]'));

            $('[id$=hdfOdata_DataJson]').val(dsCompleteData[i].Datajson.trim());
            $('[id$=hdfOdata_HttpData]').val(dsCompleteData[i].HttpData.trim());
            $('[id$=hdfOdata_InputParam]').val(dsCompleteData[i].InputParajson.trim());
            $('[id$=hdfOdata_OutputParam]').val(dsCompleteData[i].OutputParaJson.trim());

            if (dsCompleteData[i].Resourcetype.toString() == '0') {
                $('#ODataCmd_EntityDiv').show();
                $('#ODataCmd_HttpTypeDiv').show();
                switch (dsCompleteData[i].Httptype) {
                    case 1:
                        $('[id$=ddlOdataCmd_UsingKey]').val(dsCompleteData[i].SelectType);
                        $('select[id$=ddlOdataCmd_UsingKey]').parent().children("span").text($('select[id$=ddlOdataCmd_UsingKey]').children('option:selected').text());
                        selectUsingKeyDisplayChanged();

                        $('[id$=txtOdataCmd_Ent_DsPath]').val(dsCompleteData[i].DataSetPath);
                        $('[id$=ddlOdataCmd_Ent_ReturnTyp]').val(dsCompleteData[i].ReturnType.trim());
                        $('select[id$=ddlOdataCmd_Ent_ReturnTyp]').parent().children("span").text($('select[id$=ddlOdataCmd_Ent_ReturnTyp]').children('option:selected').text());
                        ddlOdataCmd_Ent_ReturnTypChanged($('[id$=ddlOdataCmd_Ent_ReturnTyp]'));
                        oDataCmdQueryTextBoxChange();
                        $('#outputPropDiv').show();

                        $('[id$=ddlOdataCmd_EntityType]').val(dsCompleteData[i].ResourcePath.trim().split('(')[0]);
                        $('select[id$=ddlOdataCmd_EntityType]').parent().children("span").text($('select[id$=ddlOdataCmd_EntityType]').children('option:selected').text());
                        break;
                    case 2:
                        $('[id$=ddlOdataCmd_EntityType]').val(dsCompleteData[i].ResourcePath.trim().split('(')[0]);
                        $('select[id$=ddlOdataCmd_EntityType]').parent().children("span").text($('select[id$=ddlOdataCmd_EntityType]').children('option:selected').text());
                        $('#outputPropDiv').hide();
                        break;
                    case 3:
                    case 4:
                    case 5:
                        $('[id$=ddlOdataCmd_EntityType]').val(dsCompleteData[i].ResourcePath.trim().split('(')[0]);
                        $('select[id$=ddlOdataCmd_EntityType]').parent().children("span").text($('select[id$=ddlOdataCmd_EntityType]').children('option:selected').text());
                        $('#outputPropDiv').hide();
                        break;
                    case 6:
                        $('[id$=ddlOdataCmd_EntityType]').val(dsCompleteData[i].ResourcePath.trim().split('(')[0]);
                        $('select[id$=ddlOdataCmd_EntityType]').parent().children("span").text($('select[id$=ddlOdataCmd_EntityType]').children('option:selected').text());
                        $('#outputPropDiv').hide();
                        break;
                }
                entityTypeChanged();

                $('[id$=ddlOdataCmd_Ent_HttpTyp]').val(dsCompleteData[i].Httptype.toString());
                $('select[id$=ddlOdataCmd_Ent_HttpTyp]').parent().children("span").text($('select[id$=ddlOdataCmd_Ent_HttpTyp]').children('option:selected').text());
                httpTypeChanged();

                setHttpTypeChangedDisplay();
                AddParameterInOdataCmd();
                SubProcODataCmdAddPara(false);
                chkSingleValueOnChange($('[id$=chkSingleValue]')[0].checked);
            }
            else if (dsCompleteData[i].Resourcetype.toString() == '1') {
                $('[id$=hdfOdata_FuncType]').val(getFunctionType(dsCompleteData[i].Functiontype.toString().trim()));
                $('[id$=lblOdataCmd_FuncType]').text(getFunctionType(dsCompleteData[i].Functiontype.toString().trim()));
                switch (dsCompleteData[i].Httptype) {
                    case 1:
                        $('[id$=lblOdataCmd_FuncHType]').text("GET");
                        $('[id$=hdfOdata_FuncHType]').val("GET");
                        break;
                    case 2:
                        $('[id$=lblOdataCmd_FuncHType]').text("POST");
                        $('[id$=hdfOdata_FuncHType]').val("POST");
                        break;
                    case 3:
                        $('[id$=lblOdataCmd_FuncHType]').text("PUT");
                        $('[id$=hdfOdata_FuncHType]').val("PUT");
                        break;
                    case 4:
                        $('[id$=lblOdataCmd_FuncHType]').text("PATCH");
                        $('[id$=hdfOdata_FuncHType]').val("PATCH");
                        break;
                    case 5:
                        $('[id$=lblOdataCmd_FuncHType]').text("MERGE");
                        $('[id$=hdfOdata_FuncHType]').val("MERGE");
                        break;
                    case 6:
                        $('[id$=lblOdataCmd_FuncHType]').text("DELETE");
                        $('[id$=hdfOdata_FuncHType]').val("DELETE");
                        break;
                }

                $('#ODataCmd_FunctionDiv').show();
                $('#ODataCmd_Function_TypeDiv').show();
                $('#divOdataFuncInputPara').show();
                $('#ODataCmd_HttpType_ReturnDiv').show();
                $('#outputPropDiv').show();
                SubProcODataCmdAddPara(false);
                setHttpTypeChangedDisplay();
            }

            editODataCache(dsCompleteData[i].Cache.toString(), dsCompleteData[i].Expiryfrequency.toString(), dsCompleteData[i].ExpiryCondtion);
        }
    }
    showConnectorInfo();
    UniformControlDesign();

    $('[id$=txtDataObj_Name]').attr("disabled", "disabled");
    $('[id$=ddlOdataCmd_Conn]').attr("disabled", "disabled");
}

function editODataCache(cacheType, expFrequency, expCondition) {
    showConnectorInfo();
    initializeDataCache();
    $('[id$=ddl_DataCache_type]').val(cacheType);
    $('select[id$=ddl_DataCache_type]').parent().children("span").text($('select[id$=ddl_DataCache_type]').children('option:selected').text());
    //ddl_DataCache_type_Changed($('[id$=ddl_DataCache_type]'));

    if (cacheType == "1") {
        $('[id$=ddl_AbsoluteCachePeriod]').val(expFrequency);
        $('select[id$=ddl_AbsoluteCachePeriod]').parent().children("span").text($('select[id$=ddl_AbsoluteCachePeriod]').children('option:selected').text());
        $('#divAbsoluteTime').show();
        switch (expFrequency) {
            case '0':
                $('[id$=ddl_AbsoluteMinutes]').val(expCondition);
                $('select[id$=ddl_AbsoluteMinutes]').parent().children("span").text($('select[id$=ddl_AbsoluteMinutes]').children('option:selected').text());
                $('#divAbsoluteMinutes').show();
                break;
            case '1':
                var arry = expCondition.split(':');
                $('[id$=ddl_AbsoluteDayHour]').val(arry[0]);
                $('select[id$=ddl_AbsoluteDayHour]').parent().children("span").text($('select[id$=ddl_AbsoluteDayHour]').children('option:selected').text());
                $('[id$=ddl_AbsoluteDayMin]').val(arry[1]);
                $('select[id$=ddl_AbsoluteDayMin]').parent().children("span").text($('select[id$=ddl_AbsoluteDayMin]').children('option:selected').text());
                $('#divAbsoluteDay').show();
                break;
            case '2':
                $('[id$=ddl_AbsoluteWeek]').val(expCondition);
                $('select[id$=ddl_AbsoluteWeek]').parent().children("span").text($('select[id$=ddl_AbsoluteWeek]').children('option:selected').text());
                $('#divAbsoluteWeek').show();
                break;
            case '3':
                $('[id$=ddl_AbsoluteDate]').val(expCondition);
                $('select[id$=ddl_AbsoluteDate]').parent().children("span").text($('select[id$=ddl_AbsoluteDate]').children('option:selected').text());
                $('#divAbsoluteDate').show();
                break;
            case '4':
                var arry = expCondition.split(':');
                $('[id$=ddl_AbsoluteYear_Day]').val(arry[0]);
                $('select[id$=ddl_AbsoluteYear_Day]').parent().children("span").text($('select[id$=ddl_AbsoluteYear_Day]').children('option:selected').text());
                $('[id$=ddl_AbsoluteYear_Month]').val(arry[1]);
                $('select[id$=ddl_AbsoluteYear_Month]').parent().children("span").text($('select[id$=ddl_AbsoluteYear_Month]').children('option:selected').text());
                $('#divAbsoluteYear').show();
                break;
        }
    }
    else if (cacheType == "2") {
        $('[id$=ddl_RelativeCachePeriod]').val(expFrequency);
        $('select[id$=ddl_RelativeCachePeriod]').parent().children("span").text($('select[id$=ddl_RelativeCachePeriod]').children('option:selected').text());
        $('#divRelativeTime').show();
        switch (expFrequency) {
            case '0':
                $('[id$=ddl_RelativeMinute]').val(expCondition);
                $('select[id$=ddl_RelativeMinute]').parent().children("span").text($('select[id$=ddl_RelativeMinute]').children('option:selected').text());
                $('#divRelativeMinute').show();
                break;
            case '1':
                $('[id$=ddl_RelativeHour]').val(expCondition);
                $('select[id$=ddl_RelativeHour]').parent().children("span").text($('select[id$=ddl_RelativeHour]').children('option:selected').text());
                $('#divRelativeHour').show();
                break;
            case '2':
                $('[id$=ddl_RelativeDay]').val(expCondition);
                $('select[id$=ddl_RelativeDay]').parent().children("span").text($('select[id$=ddl_RelativeDay]').children('option:selected').text());
                $('#divRelativeDay').show();
                break;
            case '3':
                $('[id$=ddl_RelativeWeek]').val(expCondition);
                $('select[id$=ddl_RelativeWeek]').parent().children("span").text($('select[id$=ddl_RelativeWeek]').children('option:selected').text());
                $('#divRelativeWeek').show();
                break;
            case '4':
                $('[id$=ddl_RelativeMonth]').val(expCondition);
                $('select[id$=ddl_RelativeMonth]').parent().children("span").text($('select[id$=ddl_RelativeMonth]').children('option:selected').text());
                $('#divRelativeMonth').show();
                break;
        }
    }
    UniformControlDesign();
}

function checkForDiscardChanges() {
    if ($('#divEditDataObject').is(':visible')) {
        if ($('[id$=hdfDataObjectId]').val().trim().length > 0) {
            SubModifiction(true);
            return true;
        }
        else
            return false;
    }
    else
        return false;
}

function discardChanges() {
    SubModifiction(false);
    if (isAddClicked) {
        initializeDataObjects();
    }
    else {
        viewODataObject();
    }
}

function cancelDiscardChanges() {
    SubModifiction(false);
    ChangeSelection();
}

function addInitializeNewObject() {
    $('[id$=hdfPreviousDataObjectId]').val($('[id$=hdfDataObjectId]').val());
    isAddClicked = true;
    if (!checkForDiscardChanges()) {
        initializeDataObjects();
    }
    else
        return;
}

function addNewODataObject() {
    initializeControls();
    $('[id$=txtDataObj_Name]').val('');
    $('[id$=txtDataObj_Desc]').val('');
    $('#divDataObjectDetails').hide();
    $('#divEditDataObject').show();
    $('#divEditDataObjLbl').hide();
    $('#addDataObjHeader').show();
    $('#editDataObjHeader').hide();
    $('#outputPropDiv').hide();
    $('[id$=hdfDataObjectId]').val('');
    $('[id$=txtDataObj_Name]').removeAttr("disabled");
    $('[id$=ddlOdataCmd_Conn]').removeAttr("disabled");
    $('[id$=btnDataObj_Cancel]').show();
    OdataCmdInitVariable(true);
    setDefaultDisplay();
    showConnectorInfo();
    UniformControlDesign();
}

function setDefaultDisplay() {
    $('#divDataObjName').show();
    $('#divDataObjDesc').show();
    $('#divConnector').show();
    $('#divResponseFormat').show();
    $('#divResourceType').show();
    $('#divDataCache').show();

    $('#divInputParams').hide();
    $('#ODataCmd_EntityDiv').hide();
    $('#ODataCmd_FunctionDiv').hide();
    $('#ODataCmd_Function_TypeDiv').hide();
    $('#ODataCmd_HttpTypeDiv').hide();
    $('#SelectUsingKeyDiv').hide();
    $('#ODataCmd_HttpType_GetDiv').hide();
    $('#ODataCmd_HttpType_KeyLabelDiv').hide();
    $('#ODataCmd_HttpType_KeyDiv').hide();
    $('#ODataCmd_HttpType_PropDiv').hide();
    $('#outputPropDiv').hide();
    $('#divOdataFuncInputPara').hide();
    $('#ODataCmd_HttpType_SelectDiv').hide();
    $('#ODataCmd_Function_TypeDiv').hide();
    $('#ODataCmd_HttpType_ReturnDiv').hide();
    $('#ODataCmd_HttpType_DsPath').hide();

    if ($('[id$=hdfDataObjectId]').val().length > 0) {
        $('[id$=txtDataObj_Name]').attr("disabled", "disabled");
        $('[id$=ddlOdataCmd_Conn]').attr("disabled", "disabled");
    }
}

function resouceTypeChanged(e) {
    setDefaultDisplay();
    showConnectorInfo();

    if ($(e).val() == "0") {
        $('#ODataCmd_EntityDiv').show();
    }
    else if ($(e).val() == "1") {
        $('#ODataCmd_FunctionDiv').show();
    }
    else if ($(e).val() == "-1") {
        $('#ODataCmd_EntityDiv').hide();
        $('#ODataCmd_FunctionDiv').hide();
    }
    $('[id$=ddlOdataCmd_EntityType]').val('-1');
    $('select[id$=ddlOdataCmd_EntityType]').parent().children("span").text($('select[id$=ddlOdataCmd_EntityType]').children('option:selected').text());

    $('[id$=ddlOdataCmd_Ent_HttpTyp]').val('-1');
    $('select[id$=ddlOdataCmd_Ent_HttpTyp]').parent().children("span").text($('select[id$=ddlOdataCmd_Ent_HttpTyp]').children('option:selected').text());

    $('[id$=ddlOdataCmd_funcImport]').val('-1');
    $('select[id$=ddlOdataCmd_funcImport]').parent().children("span").text($('select[id$=ddlOdataCmd_funcImport]').children('option:selected').text());


    if ($('[id$=hdfDataObjectId]').val().length > 0) {
        $('[id$=txtDataObj_Name]').attr("disabled", "disabled");
        $('[id$=ddlOdataCmd_Conn]').attr("disabled", "disabled");
    }
    UniformControlDesign();
}

function functionTypeChanged() {
    setDefaultDisplay();
    OdataCmd_CreateFunctionHtml();
    showConnectorInfo();
    $('#ODataCmd_FunctionDiv').show();
    if ($('[id$=ddlOdataCmd_funcImport]')[1].value != '-1') {
        $('#divOdataFuncInputPara').show();
        $('#ODataCmd_Function_TypeDiv').show();
        $('#ODataCmd_HttpType_ReturnDiv').show();
        $('#outputPropDiv').show();
    }

    if ($('[id$=hdfDataObjectId]').val().length > 0) {
        $('[id$=txtDataObj_Name]').attr("disabled", "disabled");
        $('[id$=ddlOdataCmd_Conn]').attr("disabled", "disabled");
    }
    UniformControlDesign();
}

function initializeGetHttpTypeControls() {
    showConnectorInfo();
    $('[id$=chkSingleValue]')[0].checked = false;
    $('[id$=txt_AdditionalResourcePath]').val('');
    $('[id$=txtOdataCmd_Top]').val('');
    $('[id$=txtOdataCmd_Skip]').val('');
    $('[id$=txtOdataCmd_Filter]').val('');

    $('[id$=ddlOdataCmd_Ent_ReturnTyp]').val('0');
    $('select[id$=ddlOdataCmd_Ent_ReturnTyp]').parent().children("span").text($('select[id$=ddlOdataCmd_Ent_ReturnTyp]').children('option:selected').text());
    ddlOdataCmd_Ent_ReturnTypChanged($('[id$=ddlOdataCmd_Ent_ReturnTyp]'));

    $('[id$=txtOdataCmd_Ent_DsPath]').val('');
    $('#ODataCmd_HttpType_GetKeyInDiv').html('');
    $('#ODataCmd_HttpType_PostInDiv').html('');

    if ($('[id$=hdfDataObjectId]').val().length > 0) {
        $('[id$=txtDataObj_Name]').attr("disabled", "disabled");
        $('[id$=ddlOdataCmd_Conn]').attr("disabled", "disabled");
    }
}

function initializeEntityTypeResourceControls() {
    showConnectorInfo();
    $('#lblOdataInputPara').html('No Parameters defined yet');

    $('[id$=ddlOdataCmd_Ent_HttpTyp]').val('-1');
    $('select[id$=ddlOdataCmd_Ent_HttpTyp]').parent().children("span").text($('select[id$=ddlOdataCmd_Ent_HttpTyp]').children('option:selected').text());
    httpTypeChanged();

    $('[id$=ddlOdataCmd_UsingKey]').val('-1');
    $('select[id$=ddlOdataCmd_UsingKey]').parent().children("span").text($('select[id$=ddlOdataCmd_UsingKey]').children('option:selected').text());
    selectUsingKeyDisplayChanged();

    initializeGetHttpTypeControls();

    if ($('[id$=hdfDataObjectId]').val().length > 0) {
        $('[id$=txtDataObj_Name]').attr("disabled", "disabled");
        $('[id$=ddlOdataCmd_Conn]').attr("disabled", "disabled");
    }
    UniformControlDesign();
}

function deleteODataObject() {
    showConnectorInfo();
    if (objectUsed) {
        $('[id$=btnDataObj_Cancel]').hide();
        $('#aMessage').html('OData object already used in app(s).');
        SubProcBoxMessage(true);
    }
    else {
        $('#aMsgCmdDelCmf').html('Do you want to delete this object ?');
        SubProcDeleteCmdConfrmation(true, 'Delete Object');
    }
    UniformControlDesign();
}

function initializeHttpTypeControls() {
    showConnectorInfo();
    $('#lblOdataInputPara').html('No Parameters defined yet');

    $('[id$=ddlOdataCmd_UsingKey]').val('-1');
    $('select[id$=ddlOdataCmd_UsingKey]').parent().children("span").text($('select[id$=ddlOdataCmd_UsingKey]').children('option:selected').text());
    selectUsingKeyDisplayChanged();

    initializeGetHttpTypeControls();

    if ($('[id$=hdfDataObjectId]').val().length > 0) {
        $('[id$=txtDataObj_Name]').attr("disabled", "disabled");
        $('[id$=ddlOdataCmd_Conn]').attr("disabled", "disabled");
    }

    UniformControlDesign();
}

function entityTypeChanged() {
    setDefaultDisplay();
    initializeEntityTypeResourceControls();
    $('#ODataCmd_EntityDiv').show();
    $('#ODataCmd_HttpTypeDiv').show();
    UniformControlDesign();
}

function setHttpTypeChangedDisplay() {
    showConnectorInfo();
    var value = "";
    if ($('[id$=ddlOdataCmd_Ent_HttpTyp]')[1] != undefined) {
        value = $('[id$=ddlOdataCmd_Ent_HttpTyp]')[1].value;
    }
    else if ($('[id$=ddlOdataCmd_Ent_HttpTyp]')[0] != undefined) {
        value = $('[id$=ddlOdataCmd_Ent_HttpTyp]')[0].value;
    }
    switch (value) {
        case "-1":
            $('#SelectUsingKeyDiv').hide();
            $('#ODataCmd_HttpType_PropDiv').hide();
            $('#ODataCmd_HttpType_KeyLabelDiv').hide();
            $('#ODataCmd_HttpType_KeyDiv').hide();
            break;
        case "1":
            $('#SelectUsingKeyDiv').show();
            break;
        case "2":
            OdataCmd_CreatePostHtml();
            $('#ODataCmd_HttpType_PropDiv').show();
            $('#outputPropDiv').hide();
            break;
        case "3":
        case "4":
        case "5":
            OdataCmd_CreatePutHtml();
            $('#ODataCmd_HttpType_KeyLabelDiv').show();
            $('#ODataCmd_HttpType_KeyDiv').show();
            $('#ODataCmd_HttpType_PropDiv').show();
            $('#outputPropDiv').hide();
            break;
        case "6":
            OdataCmd_CreateDeleteHtml();
            $('#ODataCmd_HttpType_KeyLabelDiv').show();
            $('#ODataCmd_HttpType_KeyDiv').show();
            $('#outputPropDiv').hide();
            break;
    }

    if ($('[id$=hdfDataObjectId]').val().length > 0) {
        $('[id$=txtDataObj_Name]').attr("disabled", "disabled");
        $('[id$=ddlOdataCmd_Conn]').attr("disabled", "disabled");
    }
    UniformControlDesign();
}

function httpTypeChanged() {
    setDefaultDisplay();
    initializeHttpTypeControls();
    $('#ODataCmd_EntityDiv').show();
    $('#ODataCmd_HttpTypeDiv').show();
    setHttpTypeChangedDisplay();

    if ($('[id$=hdfDataObjectId]').val().length > 0) {
        $('[id$=txtDataObj_Name]').attr("disabled", "disabled");
        $('[id$=ddlOdataCmd_Conn]').attr("disabled", "disabled");
    }
}

function selectUsingKeyDisplayChanged() {
    showConnectorInfo();
    oDataCmdQueryTextBoxChange();
    if ($('[id$=ddlOdataCmd_Ent_HttpTyp]')[1] != undefined) {
        if ($('[id$=ddlOdataCmd_Ent_HttpTyp]')[1].value == '1')
            $('#SelectUsingKeyDiv').show();
    }
    else if ($('[id$=ddlOdataCmd_Ent_HttpTyp]')[0] != undefined) {
        if ($('[id$=ddlOdataCmd_Ent_HttpTyp]')[0].value == '1')
            $('#SelectUsingKeyDiv').show();
    }
    var value = "";
    if ($('[id$=ddlOdataCmd_UsingKey]')[1] != undefined) {
        value = $('[id$=ddlOdataCmd_UsingKey]')[1].value;
    }
    else if ($('[id$=ddlOdataCmd_UsingKey]')[0] != undefined) {
        value = $('[id$=ddlOdataCmd_UsingKey]')[0].value;
    }
    if (value == '0') {
        $('#ODataCmd_HttpType_GetDiv').show();
        $('#ODataGetWithoutKeyDiv').show();
        $('#outputPropDiv').show();
        $('#ODataCmd_HttpType_SelectDiv').show();
        $('#ODataCmd_HttpType_ReturnDiv').show();
        //$('#ODataCmd_HttpType_DsPath').show();
        $('#divInputParams').show();
        AddParameterInOdataCmd();
    }
    else if (value == '1') {
        OdataCmd_CreateGetHtml();
        $('#ODataCmd_HttpType_KeyLabelDiv').show();
        $('#ODataCmd_HttpType_KeyDiv').show();
        $('#ODataCmd_HttpType_GetDiv').show();
        $('#ODataCmd_HttpType_SelectDiv').show();
        $('#outputPropDiv').show();
        $('#ODataCmd_SingleValueReturnCheckDiv').show();
        $('#ODataGetWithoutKeyDiv').hide();
    }

    if ($('[id$=hdfDataObjectId]').val().length > 0) {
        $('[id$=txtDataObj_Name]').attr("disabled", "disabled");
        $('[id$=ddlOdataCmd_Conn]').attr("disabled", "disabled");
    }
    UniformControlDesign();
}

function selectUsingKeyChanged() {
    setDefaultDisplay();
    showConnectorInfo();
    $('#ODataCmd_EntityDiv').show();
    $('#ODataCmd_HttpTypeDiv').show();
    initializeGetHttpTypeControls();
    selectUsingKeyDisplayChanged();

    if ($('[id$=hdfDataObjectId]').val().length > 0) {
        $('[id$=txtDataObj_Name]').attr("disabled", "disabled");
        $('[id$=ddlOdataCmd_Conn]').attr("disabled", "disabled");
    }
}

function setInputPara(_InputParaJson) {
    UniformControlDesign();
    OdataCmdGlobalVar.CommandParam = JSON.stringify(_InputParaJson);
}

//Edit Command -Initialize command global variable when http type is Get
function OdataComdEdit_Get(_Json, _InputJson, _OutPutJson) {
    initializeEditVariables();
    var inCount = 1;
    var strHtml = '';
    if (_Json.length > 0) {
        OdataCmdGlobalVar.count = 1;
        OdataCmdGlobalVar.SelectJson = '[]';
        OdataCmdGlobalVar.OrderByJson = '[]';
        OdataCmdGlobalVar.CommandParam = '';
        OdataCmdGlobalVar.ParamCount = 1;
        var obj = jQuery.parseJSON(_Json);
        OdataCmdGlobalVar.CommandParam = _InputJson;
        OdataCmdGlobalVar.OutParam = _OutPutJson;
        OdataCmdGlobalVar.functionInParam = '';
        OdataCmdGlobalVar.SelectJson = JSON.stringify(obj.select);
        OdataCmdGlobalVar.OrderByJson = JSON.stringify(obj.orderBy);
        var strRet = $($('[id$=ddlOdataCmd_Ent_ReturnTyp]')[1]).val();
        var strInputParaJson = '';
        if (OdataCmdGlobalVar.CommandParam != undefined && OdataCmdGlobalVar.CommandParam.length > 0) {
            $.each(jQuery.parseJSON(OdataCmdGlobalVar.CommandParam), function () {
                if (strInputParaJson.length > 0)
                    strInputParaJson += ', ' + this['para'];
                else
                    strInputParaJson += this['para'];
                $('#lblOdataInputPara').html(strInputParaJson);
            });
        }

        $('#txtOdataCmd_Top').val(obj.top)
        $('#txtOdataCmd_Skip').val(obj.skip)
        $('[id$=txtOdataCmd_Filter]').val(obj.filter);
        if (obj.selectusing != null || obj.selectusing != undefined) {
            selectUsingKeyDisplayChanged();
        }

        if (obj.keys != null) {
            $('#ODataCmd_HttpType_GetKeyInDiv').find('.ODataPostValueLeftDiv').each(function () {
                if (this.id.indexOf('ODataCmdHttpGetProp') > -1) {
                    if (obj.keys[$(this).html().trim()] != null) {
                        $('#txtOdataCmd_HttpGet_' + this.id.split('_')[1]).val(obj.keys[$(this).html().trim()])
                    }
                }
            });
        }
    }

    //OdataCmdEditVariable();
}

function initializeEditVariables() {
    OdataCmdGlobalVar.SelectJson = '[]';
    OdataCmdGlobalVar.OrderByJson = '[]';
    OdataCmdGlobalVar.CommandParam = '';
    OdataCmdGlobalVar.ParamCount = 1;
    OdataCmdGlobalVar.OutParam = '';
    OdataCmdGlobalVar.CommandParam = '';
    OdataCmdGlobalVar.functionInParam = '';
}

//Edit Command -Initialize command global variable when http type is Post
function OdataComdEdit_Post(_Json, _InputJson, _OutPutJson) {
    initializeEditVariables();
    if (_Json.length > 0) {
        OdataCmdGlobalVar.count = 1;
        OdataCmdGlobalVar.SelectJson = '[]';
        OdataCmdGlobalVar.OrderByJson = '[]';
        OdataCmdGlobalVar.CommandParam = '';
        OdataCmdGlobalVar.ParamCount = 1;
        OdataCmdGlobalVar.OutParam = '';
        OdataCmdGlobalVar.CommandParam = _InputJson;
        OdataCmdGlobalVar.functionInParam = '';
        var strInputParaJson = '';
        if (OdataCmdGlobalVar.CommandParam != undefined && OdataCmdGlobalVar.CommandParam.length > 0) {
            $.each(jQuery.parseJSON(OdataCmdGlobalVar.CommandParam), function () {
                if (strInputParaJson.length > 0)
                    strInputParaJson += ', ' + this['para'];
                else
                    strInputParaJson += this['para'];


                $('#lblOdataInputPara').html(strInputParaJson);
            });
        }
        var obj = jQuery.parseJSON(_Json);
        var strVal = '';
        $('#ODataCmd_HttpType_PostInDiv').find('.ODataPostValueLeftDiv').each(function () {
            if (this.id.indexOf('ODataCmdPostProp') > -1) {
                if (obj[$(this).html().trim()] != null) {
                    $('#txtOdataCmd_HttpPost_' + this.id.split('_')[1]).val(obj[$(this).html().trim()])
                }
            }
        });
    }
    //OdataCmdEditVariable();
}

//Edit Command -Initialize command global variable when http type is Put
function OdataComdEdit_Put(_Json, _InputJson, _OutPutJson) {
    initializeEditVariables();
    if (_Json.length > 0) {
        OdataCmdGlobalVar.count = 1;
        OdataCmdGlobalVar.SelectJson = '[]';
        OdataCmdGlobalVar.OrderByJson = '[]';
        OdataCmdGlobalVar.CommandParam = '';
        OdataCmdGlobalVar.ParamCount = 1;
        OdataCmdGlobalVar.CommandParam = _InputJson;
        OdataCmdGlobalVar.OutParam = '';
        OdataCmdGlobalVar.functionInParam = '';
        var obj = jQuery.parseJSON(_Json);
        var objKeys = obj.keys;
        var objProp = obj.Prop;
        var strInputParaJson = '';
        if (OdataCmdGlobalVar.CommandParam != undefined && OdataCmdGlobalVar.CommandParam.length > 0) {
            $.each(jQuery.parseJSON(OdataCmdGlobalVar.CommandParam), function () {
                if (strInputParaJson.length > 0)
                    strInputParaJson += ', ' + this['para'];
                else
                    strInputParaJson += this['para'];


                $('#lblOdataInputPara').html(strInputParaJson);
            });
        }
        var strVal = '';

        $('#ODataCmd_HttpType_GetKeyInDiv').find('.ODataPostValueLeftDiv').each(function () {
            if (this.id.indexOf('ODataCmdHttpPut') > -1) {
                if (objKeys[$(this).html().trim()] != null) {
                    $('#txtOdataCmd_HttpPut_' + this.id.split('_')[1]).val(objKeys[$(this).html().trim()])
                }
            }
        });
        $('#ODataCmd_HttpType_PostInDiv').find('.ODataPostValueLeftDiv').each(function () {
            if (this.id.indexOf('ODataCmdHttpPutProp') > -1) {
                if (objProp[$(this).html().trim()] != null) {
                    $('#txtOdataCmd_HttpPutProp_' + this.id.split('_')[1]).val(objProp[$(this).html().trim()])
                }
            }
        });
    }
    //OdataCmdEditVariable();
}

//Edit Command -Initialize command global variable when http type is Delete
function OdataComdEdit_Del(_Json, _InputJson, _OutPutJson) {
    initializeEditVariables();
    if (_Json.length > 0) {
        OdataCmdGlobalVar.count = 1;
        OdataCmdGlobalVar.SelectJson = '[]';
        OdataCmdGlobalVar.OrderByJson = '[]';
        OdataCmdGlobalVar.CommandParam = '';
        OdataCmdGlobalVar.ParamCount = 1;
        OdataCmdGlobalVar.CommandParam = _InputJson;
        OdataCmdGlobalVar.OutParam = '';
        var obj = jQuery.parseJSON(_Json);
        var objKeys = obj.keys;
        var strInputParaJson = '';
        if (OdataCmdGlobalVar.CommandParam != undefined && OdataCmdGlobalVar.CommandParam.length > 0) {
            $.each(jQuery.parseJSON(OdataCmdGlobalVar.CommandParam), function () {
                if (strInputParaJson.length > 0)
                    strInputParaJson += ', ' + this['para'];
                else
                    strInputParaJson += this['para'];


                $('#lblOdataInputPara').html(strInputParaJson);
            });
        }
        var strVal = '';
        $('#ODataCmd_HttpType_GetKeyInDiv').find('.ODataPostValueLeftDiv').each(function () {
            if (this.id.indexOf('ODataCmdHttpDelProp') > -1) {
                if (objKeys[$(this).html().trim()] != null) {
                    $('#txtOdataCmd_HttpDel_' + this.id.split('_')[1]).val(objKeys[$(this).html().trim()])
                }
            }
        });
    }
    //OdataCmdEditVariable();
}

//Edit Command -Initialize command global variable when http type is Function
function OdataComdEdit_Func(_Json, _InputJson, _OutPutJson) {
    initializeEditVariables();
    if (_Json.length > 0) {
        OdataCmdGlobalVar.count = 1;
        OdataCmdGlobalVar.SelectJson = '[]';
        OdataCmdGlobalVar.OrderByJson = '[]';
        OdataCmdGlobalVar.CommandParam = '';
        OdataCmdGlobalVar.ParamCount = 1;
        OdataCmdGlobalVar.CommandParam = _InputJson;
        OdataCmdGlobalVar.OutParam = _OutPutJson;
        var strInputParaJson = '';
        var obj = jQuery.parseJSON(_Json);
        $('#ODataCmd_FuncType_InDiv').find('.ODataPostValueLeftDiv').each(function () {
            if (this.id.indexOf('ODataCmdFuncProp') > -1) {
                if (obj[$(this).html().trim()] != null) {
                    $('#txtOdataCmd_Func_' + this.id.split('_')[1]).val(obj[$(this).html().trim()])
                }
            }
        });

    }
}

//btnOrderOdataSelect click event--(Show odata orderby prop)
function btnOrderOdataSelectClick() {
    OdataCmdGlobalVar.count = 1;
    $('#ODataCmdContentDiv').html('');
    OdataOrderByHtml();
    OdataCmdParseSelectJson(1);
    SubProcAddODataSelect(true, 450);
}

//Show select prop modal popup
function SubProcAddODataSelect(_bol, _Width) {
    if (_bol) {
        showModalPopUp('SubProcAddODataSelect', 'Select Value', _Width);
    }
    else {
        $('#SubProcAddODataSelect').dialog('close');
    }
}

//Html to add select properties.
function OdataSelectHtml() {
    var strHead = '<div class="QPTextHeader" align="center">'
                + '<div class="QPHeaderText">'
                    + 'Value'
                + '</div>'
            + '</div>'
            + '<div class="QPDelete">'
            + '</div>';
    $('#ODataCmdHeadDiv').html(strHead);

    var strAddHtml = '<div class="QPContentRow">'
                            + '<div class="QPContentText">'
                                + '<select id="ddlSelect_ODataEntity" class="ddlQPOption" style="width:90%;" >'
                                + '</select>'
                           + '</div>'
                           + '<div class="QPContentDelete"  align="center">'
                                + '<img id="img15" alt="" src="//enterprise.mficient.com/images/add.png" onclick="OdataSelectAddClick();" class="CursorPointer" />'
                            + '</div>'
                       + ' </div>';
    $('#ODataCmdAddDiv').html(strAddHtml);
    var strProp = '<option value="-1">Select Property</option>';
    if (OdataCmdGlobalVar.StructuralProp.length > 0) {
        $.each(OdataCmdGlobalVar.StructuralProp.split(','), function () {
            if (this.length > 0) strProp += '<option value="' + this + '">' + this + '</option>';
        });
    }
    $('#ddlSelect_ODataEntity').html(strProp);
}

//Create json for select 
function OdataCmdSaveSelectJson(_Type) {
    var strJson = '';
    if (_Type == 0) {
        $('#ODataCmdContentDiv').find('.QPContentRow').each(function () {
            strJson += strJson.length > 0 ? ',' : '';
            strJson += '{"prop":"' + $('#ODataCmdSelectPropDiv_' + this.id.split('_')[1]).html() + '"}';
        });
        OdataCmdGlobalVar.SelectJson = '[' + strJson + ']';
    }
    else {
        $('#ODataCmdContentDiv').find('.QPContentRow').each(function () {
            strJson += strJson.length > 0 ? ',' : '';
            strJson += '{"prop":"' + $('#ODataCmdOrderByPropDiv_' + this.id.split('_')[1]).html() + '","orderby":"' + $('#ODataCmdOrderByDiv_' + this.id.split('_')[1]).html() + '"}';
        });
        OdataCmdGlobalVar.OrderByJson = '[' + strJson + ']';
    }
}

//Html to add new property in select propeties.
function OdataSelectAddClick() {
    if ($('#ddlSelect_ODataEntity').val().trim() == '-1') return;
    var strHtml = '<div id="ODataCmdSelectDiv_' + OdataCmdGlobalVar.count + '" class="QPContentRow">' +
                                            '<div id="ODataCmdSelectPropDiv_' + OdataCmdGlobalVar.count + '"  class="QPContentText">' +
                                                $('#ddlSelect_ODataEntity').val() +
                                            '</div>' +
                                            '<div class="QPContentDelete" align="center">' +
                                                '<img id="imgODataSelectDel_' + OdataCmdGlobalVar.count + '" alt="" src="//enterprise.mficient.com/images/cross.png" class="CursorPointer" />' +
                                            '</div>' +
                      '</div><div style="clear: both;"></div>';
    $('#ODataCmdContentDiv').append(strHtml);
    $('#imgODataSelectDel_' + OdataCmdGlobalVar.count).bind('click', function (e) {
        $('#ODataCmdSelectDiv_' + this.id.split('_')[1]).remove();
        OdataCmdSaveSelectJson(0);
    });
    OdataCmdSaveSelectJson(0);
    OdataCmdGlobalVar.count = OdataCmdGlobalVar.count + 1;
}

//Html to add order by properties.
function OdataOrderByHtml() {
    var strHead = '<div class="QPTextHeader" align="center">'
                        + '<div class="QPHeaderText">'
                            + 'Value'
                        + '</div>'
                  + '</div>'
                  + '<div class="QPTextHeader" align="center" style="width:80px;">'
                        + '<div class="QPHeaderText">'
                            + 'Order By'
                        + '</div>'
                  + '</div>'
                  + '<div class="QPDelete"></div>';
    $('#ODataCmdHeadDiv').html(strHead);

    var strAddHtml = '<div class="QPContentRow">'
                          + '<div class="QPContentText">'
                                + '<select id="ddlOrderBy_ODataEntity" class="ddlQPOption" style="width:90%;" >'
                                + '</select>'
                           + '</div>'
                           + '<div class="QPContentText" style="width:80px;">'
                                + '<select id="ddlOrderBy_Order" class="ddlQPOption" style="width:90%;" >'
                                    + '<option value="ASE">Ascending</option>'
                                    + '<option value="DES">Descending</option>'
                                + '</select>'
                           + '</div>'
                           + '<div class="QPContentDelete"  align="center">'
                                + '<img id="img15" alt="" src="//enterprise.mficient.com/images/add.png" class="CursorPointer" onclick="OdataOrderByAddClick();" />'
                            + '</div>'
                       + ' </div>';
    $('#ODataCmdAddDiv').html(strAddHtml);
    var strProp = '<option value="-1">Select Property</option>';
    if (OdataCmdGlobalVar.StructuralProp.length > 0) {
        $.each(OdataCmdGlobalVar.StructuralProp.split(','), function () {
            if (this.length > 0) strProp += '<option value="' + this + '">' + this + '</option>';
        });
    }
    $('#ddlOrderBy_ODataEntity').html(strProp);
}

//Html to add new property in order by propeties.
function OdataOrderByAddClick() {
    if ($('#ddlOrderBy_ODataEntity').val().trim() == '-1') return;
    var strHtml = '<div id="ODataCmdSelectDiv_' + OdataCmdGlobalVar.count + '" class="QPContentRow">' +
                                            '<div id="ODataCmdOrderByPropDiv_' + OdataCmdGlobalVar.count + '" class="QPContentText">' +
                                                $('#ddlOrderBy_ODataEntity').val() +
                                            '</div>' +
                                            '<div id="ODataCmdOrderByDiv_' + OdataCmdGlobalVar.count + '" class="QPContentText" style="width:80px;">' +
                                                getOrderBy($('#ddlOrderBy_Order').val()) +
                                            '</div>' +
                                            '<div class="QPContentDelete" align="center">' +
                                                '<img id="imgODataSelectDel_' + OdataCmdGlobalVar.count + '" alt="" src="//enterprise.mficient.com/images/cross.png" class="CursorPointer" />' +
                                            '</div>' +
                      '</div><div style="clear: both;"></div>';
    $('#ODataCmdContentDiv').append(strHtml);
    $('#imgODataSelectDel_' + OdataCmdGlobalVar.count).bind('click', function (e) {
        $('#ODataCmdSelectDiv_' + this.id.split('_')[1]).remove();
        OdataCmdSaveSelectJson(1);
    });
    OdataCmdSaveSelectJson(1);
    OdataCmdGlobalVar.count = OdataCmdGlobalVar.count + 1;
}

//Create json for order by
function OdataCmdParseSelectJson(_Type) {
    var strHtml = '';
    if (_Type == 0) {
        if (OdataCmdGlobalVar.SelectJson != undefined && OdataCmdGlobalVar.SelectJson.length > 0) {
            $.each(jQuery.parseJSON(OdataCmdGlobalVar.SelectJson), function () {
                if (this.length == 0) return;
                strHtml = '<div id="ODataCmdSelectDiv_' + OdataCmdGlobalVar.count + '" class="QPContentRow">' +
                                            '<div id="ODataCmdSelectPropDiv_' + OdataCmdGlobalVar.count + '"  class="QPContentText">' +
                                                this['prop'] +
                                            '</div>' +
                                            '<div class="QPContentDelete" align="center">' +
                                                '<img id="imgODataSelectDel_' + OdataCmdGlobalVar.count + '" alt="" src="//enterprise.mficient.com/images/cross.png" class="CursorPointer" />' +
                                            '</div>' +
                      '</div><div style="clear: both;"></div>';
                $('#ODataCmdContentDiv').append(strHtml);
                $('#imgODataSelectDel_' + OdataCmdGlobalVar.count).bind('click', function (e) {
                    $('#ODataCmdSelectDiv_' + this.id.split('_')[1]).remove();
                    OdataCmdSaveSelectJson(0);
                });
                OdataCmdGlobalVar.count = OdataCmdGlobalVar.count + 1;
            });
        }
    }
    else {
        if (OdataCmdGlobalVar.OrderByJson != undefined && OdataCmdGlobalVar.OrderByJson.length > 0) {
            $.each(jQuery.parseJSON(OdataCmdGlobalVar.OrderByJson), function () {
                if (this.length == 0) return;
                strHtml = '<div id="ODataCmdSelectDiv_' + OdataCmdGlobalVar.count + '" class="QPContentRow">' +
                                            '<div id="ODataCmdOrderByPropDiv_' + OdataCmdGlobalVar.count + '"  class="QPContentText">' +
                                                this['prop'] +
                                            '</div>' +
                                            '<div id="ODataCmdOrderByDiv_' + OdataCmdGlobalVar.count + '" class="QPContentText" style="width:80px;">' +
                                                this['orderby'] +
                                            '</div>' +
                                            '<div class="QPContentDelete" align="center">' +
                                                '<img id="imgODataSelectDel_' + OdataCmdGlobalVar.count + '" alt="" src="//enterprise.mficient.com/images/cross.png" class="CursorPointer" />' +
                                            '</div>' +
                      '</div><div style="clear: both;"></div>';
                $('#ODataCmdContentDiv').append(strHtml);
                $('#imgODataSelectDel_' + OdataCmdGlobalVar.count).bind('click', function (e) {
                    $('#ODataCmdSelectDiv_' + this.id.split('_')[1]).remove();
                    OdataCmdSaveSelectJson(0);
                });
                OdataCmdGlobalVar.count = OdataCmdGlobalVar.count + 1;
            });
        }
    }
}

function getOrderBy(orderBy) {
    if (orderBy == "ASE")
        return "Ascending";
    else if (orderBy == "DES")
        return "Descending";
}

//Http type get- Values for command saving
function CreateODataQueryUrl_EntGet() {
    var strJson = '';
    var strSelect = '';
    var strOrderBy = '';

    var strKeyJson = '';
    var strKey = '';
    $('#ODataCmd_HttpType_GetKeyInDiv').find('.ODataPostValueLeftDiv').each(function () {
        if (this.id.indexOf('ODataCmdHttpGetProp') > -1) {
            strKeyJson += (strKeyJson.length > 0 ? ',' : '');
            strKeyJson += '"' + $(this).html().trim() + '":' + JSON.stringify($('#txtOdataCmd_HttpGet_' + this.id.split('_')[1]).val()) + '';

            strKey += (strKey.length > 0 ? ',' : '');
            strKey += $('#txtOdataCmd_HttpGet_' + this.id.split('_')[1]).val();
        }
    });

    if ($('#ODataCmd_HttpType_GetKeyDiv').is(':visible')) {
        strJson = '{'
                   + '"top":"",'
                   + '"skip":"",'
                   + '"select":' + (OdataCmdGlobalVar.SelectJson.length > 0 ? OdataCmdGlobalVar.SelectJson : '[]') + ','
                   + '"orderBy":"",'
                   + '"filter":"",'
                   + '"selectusing":"1",'
                   + '"keys":' + (strKeyJson.length > 0 ? '{' + strKeyJson + '}' : '')
                   + '}';
    }
    else {
        strJson = '{'
                   + '"top":"' + $('#txtOdataCmd_Top').val() + '",'
                   + '"skip":"' + $('#txtOdataCmd_Skip').val() + '",'
                   + '"select":' + (OdataCmdGlobalVar.SelectJson.length > 0 ? OdataCmdGlobalVar.SelectJson : '[]') + ','
                   + '"orderBy":' + (OdataCmdGlobalVar.OrderByJson.length > 0 ? OdataCmdGlobalVar.OrderByJson : '[]') + ','
                   + '"filter":' + JSON.stringify($('[id$=txtOdataCmd_Filter]').val())
                   + ',"selectusing":"0",'
                   + '"keys":""'
                   + '}';
    }
    $('[id$=hdfOdata_DataJson]').val(strJson);
    $('[id$=hdfOdata_HttpData]').val('');

    if (OdataCmdGlobalVar.SelectJson != undefined && OdataCmdGlobalVar.SelectJson.length > 0) {
        $.each(jQuery.parseJSON(OdataCmdGlobalVar.SelectJson), function () {
            if (this.prop.length > 0) {
                strSelect += (strSelect.length > 0 ? ',' + this.prop : this.prop);
            }
        });
    }

    if (OdataCmdGlobalVar.OrderByJson != undefined && OdataCmdGlobalVar.OrderByJson.length > 0) {
        $.each(jQuery.parseJSON(OdataCmdGlobalVar.OrderByJson), function () {
            if (this.prop.length > 0) {
                strOrderBy += (strOrderBy.length > 0 ? ',' + this.prop + (this.orderby == 'ASE' ? ' asc' : ' desc') : this.prop + (this.orderby == 'ASE' ? ' asc' : ' desc'));
            }
        });
    }

    var strQueryOption = ($('#txtOdataCmd_Top').val().trim().length > 0 ? '$top=' + $('#txtOdataCmd_Top').val().trim() : '');
    if ($('#txtOdataCmd_Skip').val().trim().length > 0) {
        strQueryOption += (strQueryOption.length > 0 ? '&' : '') + 'skip=' + $('#txtOdataCmd_Skip').val().trim();
    }

    if (strSelect.length > 0) {
        strQueryOption += (strQueryOption.trim().length > 0 ? '&' : '') + '$select=' + strSelect;
    }

    if (strOrderBy.length > 0) {
        strQueryOption += (strQueryOption.length > 0 ? '&' : '') + '$orderby=' + strOrderBy;
    }

    if ($('[id$=txtOdataCmd_Filter]').val().trim().length > 0) {
        strQueryOption += (strQueryOption.length > 0 ? '&' : '') + '$filter=' + $('[id$=txtOdataCmd_Filter]').val().trim();
    }



    $('[id$=hdfOdata_HttpQuery]').val(strQueryOption);
    $('[id$=hdfOdata_InputParam]').val(OdataCmdGlobalVar.CommandParam);
    $('[id$=hdfOdata_OutputParam]').val(OdataCmdGlobalVar.OutParam);

    $('[id$=hdfOdata_PutKeys]').val(strKey);
    ////http://services.odata.org/OData/OData.svc/Products?$top=3&skip=1&$select=Price&$orderby=Rating,Category/Name desc&filter=Rate gt 2
}

//Http type post- Values for command saving
function CreateODataQueryUrl_EntPost() {
    var strJson = '';
    $('#ODataCmd_HttpType_PostInDiv').find('.ODataPostValueLeftDiv').each(function () {
        if (this.id.indexOf('ODataCmdPostProp') > -1) {
            strJson += (strJson.length > 0 ? ',' : '');
            strJson += '"' + $(this).html().trim() + '":"' + $('#txtOdataCmd_HttpPost_' + this.id.split('_')[1]).val() + '"';
        }
    });
    strJson = (strJson.length > 0 ? '{' + strJson + '}' : '');
    $('[id$=hdfOdata_DataJson]').val(strJson);
    $('[id$=hdfOdata_HttpData]').val(strJson);
    $('[id$=hdfOdata_InputParam]').val(OdataCmdGlobalVar.CommandParam);
    $('[id$=hdfOdata_OutputParam]').val('');
    var test = '';
}

//Http type put- Values for command saving
function CreateODataQueryUrl_EntPut() {
    var strKeyJson = '';
    var strKey = '';
    $('#ODataCmd_HttpType_GetKeyInDiv').find('.ODataPostValueLeftDiv').each(function () {
        if (this.id.indexOf('ODataCmdHttpPut') > -1) {
            strKeyJson += (strKeyJson.length > 0 ? ',' : '');
            strKeyJson += '"' + $(this).html().trim() + '":"' + $('#txtOdataCmd_HttpPut_' + this.id.split('_')[1]).val() + '"';

            strKey += (strKey.length > 0 ? ',' : '');
            strKey += $('#txtOdataCmd_HttpPut_' + this.id.split('_')[1]).val();
        }
    });
    var strJson = '';
    $('#ODataCmd_HttpType_PostInDiv').find('.ODataPostValueLeftDiv').each(function () {
        if (this.id.indexOf('ODataCmdHttpPutProp') > -1) {
            strJson += (strJson.length > 0 ? ',' : '');
            strJson += '"' + $(this).html().trim() + '":"' + $('#txtOdataCmd_HttpPutProp_' + this.id.split('_')[1]).val() + '"';
        }
    });


    $('[id$=hdfOdata_HttpData]').val((strJson.length > 0 ? '{' + strJson + '}' : ''));
    strJson = '{"entity":"' + $($('[id$=ddlOdataCmd_EntityType]')[1]).val() + '", "keys":' + (strKeyJson.length > 0 ? '{' + strKeyJson + '}' : '""') + ',"Prop":' + (strJson.length > 0 ? '{' + strJson + '}' : '""') + '}';


    $('[id$=hdfOdata_InputParam]').val(OdataCmdGlobalVar.CommandParam);
    $('[id$=hdfOdata_OutputParam]').val('');
    $('[id$=hdfOdata_DataJson]').val(strJson);
    $('[id$=hdfOdata_PutKeys]').val(strKey);
}

//Http type delete- Values for command saving
function CreateODataQueryUrl_EntDel() {
    var strKeyJson = '';
    var strKey = '';
    $('#ODataCmd_HttpType_GetKeyInDiv').find('.ODataPostValueLeftDiv').each(function () {
        if (this.id.indexOf('ODataCmdHttpDelProp') > -1) {
            strKeyJson += (strKeyJson.length > 0 ? ',' : '');
            strKeyJson += '"' + $(this).html().trim() + '":"' + $('#txtOdataCmd_HttpDel_' + this.id.split('_')[1]).val() + '"';

            strKey += (strKey.length > 0 ? ',' : '');
            strKey += $('#txtOdataCmd_HttpDel_' + this.id.split('_')[1]).val();
        }
    });

    strJson = '{"entity":"' + $($('[id$=ddlOdataCmd_EntityType]')[1]).val() + '", "keys":' + (strKeyJson.length > 0 ? '{' + strKeyJson + '}' : '""') + '}';

    $('[id$=hdfOdata_HttpData]').val('');
    $('[id$=hdfOdata_InputParam]').val(OdataCmdGlobalVar.CommandParam);
    $('[id$=hdfOdata_OutputParam]').val('');
    $('[id$=hdfOdata_DataJson]').val(strJson);
    $('[id$=hdfOdata_PutKeys]').val(strKey);
}

//Query type function- Values for command saving
function CreateODataQueryUrl_Func() {
    var strJson = '';
    var strParam = '';
    $('#ODataCmd_FuncType_InDiv').find('.ODataPostValueLeftDiv').each(function () {
        if (this.id.indexOf('ODataCmdFuncProp') > -1) {
            strJson += (strJson.length > 0 ? ',' : '');
            strJson += '"' + $(this).html().trim() + '":"' + $('#txtOdataCmd_Func_' + this.id.split('_')[1]).val() + '"';

            strParam += (strParam.length > 0 ? '&' : '');
            strParam += $(this).html().trim() + '=' + $('#txtOdataCmd_Func_' + this.id.split('_')[1]).val();
        }
    });
    strJson = (strJson.length > 0 ? '{' + strJson + '}' : '""');
    $('[id$=hdfOdata_DataJson]').val(strJson);
    if ($('[id$=lblOdataCmd_FuncType]').html().trim() == "WEBGET" || $('[id$=lblOdataCmd_FuncType]').html().trim() == "WEBINVOKE") {
        $('[id$=hdfOdata_HttpData]').val(strParam);
    }
    else if ($('[id$=lblOdataCmd_FuncType]').html().trim() == "ACTION") {
        $('[id$=hdfOdata_HttpData]').val(strJson);
    }
    else {
        $('[id$=hdfOdata_HttpData]').val('');
    }
    $('[id$=hdfOdata_InputParam]').val(OdataCmdGlobalVar.CommandParam);
    $('[id$=hdfOdata_OutputParam]').val(OdataCmdGlobalVar.OutParam);
    $('[id$=hdfOdata_FuncHType]').val($('[id$=lblOdataCmd_FuncHType]').html().trim());
    $('[id$=hdfOdata_FuncType]').val($('[id$=lblOdataCmd_FuncType]').html().trim());
}

//Http type Get - Create html to show properties
function OdataCmd_CreateGetHtml() {
    var strHtml = '';
    var inCount = 1;
    $('#ODataCmd_HttpType_GetKeyInDiv').html('');
    if (OdataCmdGlobalVar.SelectedEntityTypeKeys.trim().length > 0) {
        $.each(OdataCmdGlobalVar.SelectedEntityTypeKeys.split(','), function () {
            if (this.length > 0) {
                strHtml = '<div id="ODataCmdHttpGetProp_' + inCount + '" class="ODataPostValueLeftDiv">' + this + '</div>'
                          + '<div class="ODataPostValueRigthDiv"><input id="txtOdataCmd_HttpGet_' + inCount + '" disabled="disabled" type="text" class="ODataPostValuetxt" value="@@' + this + '@@"/></div>'
                          + '<div class="clear"></div>';
                $('#ODataCmd_HttpType_GetKeyInDiv').append(strHtml);
                ODataCtrlChangeEvent('txtOdataCmd_HttpGet_' + inCount, 3);
                inCount = inCount + 1;
            }
        });
    }
}
//Http type post - Create html to show properties
function OdataCmd_CreatePostHtml() {
    var strHtml = '';
    var inCount = 1;
    $('#ODataCmd_HttpType_PostInDiv').html('');
    if (OdataCmdGlobalVar.StructuralProp.trim().length > 0) {
        $.each(OdataCmdGlobalVar.StructuralProp.split(','), function () {
            if (this.length > 0) {
                strHtml = '<div id="ODataCmdPostProp_' + inCount + '" class="ODataPostValueLeftDiv">' + this + '</div>'
                          + '<div class="ODataPostValueRigthDiv"><input id="txtOdataCmd_HttpPost_' + inCount + '" disabled="disabled" type="text" class="ODataPostValuetxt" value="@@' + this + '@@"/></div>'
                          + '<div class="clear"></div>';

                $('#ODataCmd_HttpType_PostInDiv').append(strHtml);
                ODataCtrlChangeEvent('txtOdataCmd_HttpPost_' + inCount, 3);
                inCount = inCount + 1;
            }
        });
    }
}

//Http type Put - Create html to show properties
function OdataCmd_CreatePutHtml() {
    var strHtml = '';
    var inCount = 1;
    $('#ODataCmd_HttpType_GetKeyInDiv').html('');
    if (OdataCmdGlobalVar.SelectedEntityTypeKeys.trim().length > 0) {
        $.each(OdataCmdGlobalVar.SelectedEntityTypeKeys.split(','), function () {
            if (this.length > 0) {
                strHtml = '<div id="ODataCmdHttpPut_' + inCount + '" class="ODataPostValueLeftDiv">' + this + '</div>'
                          + '<div class="ODataPostValueRigthDiv"><input id="txtOdataCmd_HttpPut_' + inCount + '" disabled="disabled" type="text" class="ODataPostValuetxt" value="@@' + this + '@@"/></div>'
                          + '<div class="clear"></div>';

                $('#ODataCmd_HttpType_GetKeyInDiv').append(strHtml);
                ODataCtrlChangeEvent('txtOdataCmd_HttpPut_' + inCount, 3);
                inCount = inCount + 1;
            }
        });
    }
    inCount = 1;
    strHtml = '';
    $('#ODataCmd_HttpType_PostInDiv').html('');
    var EntityTypeKeys = OdataCmdGlobalVar.SelectedEntityTypeKeys.split(',');
    if (OdataCmdGlobalVar.StructuralProp.trim().length > 0) {
        $.each(OdataCmdGlobalVar.StructuralProp.split(','), function () {
            if (this.length > 0 && (EntityTypeKeys.indexOf(String(this)) == -1)) {
                strHtml = '<div id="ODataCmdHttpPutProp_' + inCount + '" class="ODataPostValueLeftDiv">' + this + '</div>'
                          + '<div class="ODataPostValueRigthDiv"><input id="txtOdataCmd_HttpPutProp_' + inCount + '" disabled="disabled" type="text" class="ODataPostValuetxt" value="@@' + this + '@@"/></div>'
                          + '<div class="clear"></div>';

                $('#ODataCmd_HttpType_PostInDiv').append(strHtml);
                ODataCtrlChangeEvent('txtOdataCmd_HttpPutProp_' + inCount, 3);
                inCount = inCount + 1;
            }
        });
    }

}

//Http type Delete - Create html to show properties
function OdataCmd_CreateDeleteHtml() {
    var strHtml = '';
    var inCount = 1;
    $('#ODataCmd_HttpType_GetKeyInDiv').html('');
    if (OdataCmdGlobalVar.SelectedEntityTypeKeys.trim().length > 0) {
        $.each(OdataCmdGlobalVar.SelectedEntityTypeKeys.split(','), function () {
            if (this.length > 0) {
                strHtml = '<div id="ODataCmdHttpDelProp_' + inCount + '" class="ODataPostValueLeftDiv">' + this + '</div>'
                          + '<div class="ODataPostValueRigthDiv"><input id="txtOdataCmd_HttpDel_' + inCount + '" disabled="disabled" type="text" class="ODataPostValuetxt" value="@@' + this + '@@"/></div>'
                          + '<div class="clear"></div>';

                $('#ODataCmd_HttpType_GetKeyInDiv').append(strHtml);
                ODataCtrlChangeEvent('txtOdataCmd_HttpDel_' + inCount, 3);
                inCount = inCount + 1;
            }
        });
    }
}

//Entity Type Function - Create html to show properties
function OdataCmd_CreateFunctionHtml() {
    var strHtml = '';
    var inCount = 1;
    $('#ODataCmd_FuncType_InDiv').html('');
    if (OdataCmdGlobalVar.functionInParam.trim().length > 0) {
        $.each(OdataCmdGlobalVar.functionInParam.split(','), function () {
            if (this.length > 0) {
                strHtml = '<div id="ODataCmdFuncProp_' + inCount + '" class="ODataPostValueLeftDiv">' + this + '</div>'
                          + '<div class="ODataPostValueRigthDiv"><input id="txtOdataCmd_Func_' + inCount + '" disabled="disabled" type="text" class="ODataPostValuetxt" value="@@' + this + '@@"/></div>'
                          + '<div class="clear"></div>';

                $('#ODataCmd_FuncType_InDiv').append(strHtml);
                ODataCtrlChangeEvent('txtOdataCmd_Func_' + inCount, 3);
                inCount = inCount + 1;
            }
        });
    }
}

//ddlOdataCmd_Ent_ReturnTyp change event
function ddlOdataCmd_ResFormat(e) {
    if ($(e).val() == '0') {
        $('[id$=txtOdataCmd_Ent_DsPath]').val(OdataCmdGlobalVar.XmlDsPath);
    }
    else if ($(e).val() == '1') {
        $('[id$=txtOdataCmd_Ent_DsPath]').val('');
    }
    else {
        $('[id$=txtOdataCmd_Ent_DsPath]').val('');
    }
}

function ddlOdataCmd_Ent_ReturnTypChanged(e) {
    var _Val = $(e).val()
    var strResFor = $($('[id$=ddlOdataCmd_ResFormat]')[1]).val();
    OdataCmdGlobalVar.ResponseFormat = strResFor;
    OdataCmdGlobalVar.ReturnType = _Val;
    if (_Val == '0') {
        $('#ODataCmd_HttpType_DsPath').hide();
    }
    else if (_Val == '1') {
        $('#ODataCmd_HttpType_DsPath').show();
        if (strResFor == '0') {
            $('[id$=txtOdataCmd_Ent_DsPath]').val(OdataCmdGlobalVar.XmlDsPath);
        }
        else {
            $('[id$=txtOdataCmd_Ent_DsPath]').val('');
        }

    }
    else if (_Val == '2') {
        $('#ODataCmd_HttpType_DsPath').hide();
    }
    else {
    }
}

//Modal popup to show command parameter
function SubProcODataCmdAddPara(_bol) {
    if (_bol) {
        AddParameterInOdataCmd();
        showModalPopUp('SubProcODataCmdAddPara', 'Command Parameters', 500);
    }
    else {
        if ($('#SubProcODataCmdAddPara').is(':visible'))
            $('#SubProcODataCmdAddPara').dialog('close');
    }
}

//Add input parameter in odata command Tanika
function AddParameterInOdataCmd() {
    OdataCmdGlobalVar.ParamCount = 1;
    $('#AddOdataParamDiv').html('');
    var strInputParaJson = '';
    if (OdataCmdGlobalVar.CommandParam != undefined && OdataCmdGlobalVar.CommandParam.length > 0) {
        $.each(jQuery.parseJSON(OdataCmdGlobalVar.CommandParam), function () {
            if (strInputParaJson.length > 0)
                strInputParaJson += ', ' + this['para'] + ' [' + GetOdataTypeText(this['typ']) + '] ';
            else
                strInputParaJson += this['para'] + ' [' + GetOdataTypeText(this['typ']) + '] ';
            var strHtml = '<div id="OdataAddParaDiv_' + OdataCmdGlobalVar.ParamCount + '" class="QPContentRow">' +
                                            '<div class="QPContentText">' +
                                                this['para'] +
                                            '</div>' +
                                            '<div class="QPContentType">' +
                                                 GetOdataTypeText(this['typ']) +
                                            '</div>' +
                                            '<div class="QPContentDelete" align="center">' +
                                                '<img id="imgOdataDelQueryPara_' + OdataCmdGlobalVar.ParamCount + '" alt="" src="//enterprise.mficient.com/images/cross.png" />' +
                                            '</div>' +
                      '</div><div style="clear: both;"></div>';
            $('#AddOdataParamDiv').append(strHtml);
            $('#imgOdataDelQueryPara_' + OdataCmdGlobalVar.ParamCount).bind('click', function (e) {
                $('#OdataAddParaDiv_' + this.id.split('_')[1]).remove();
                createOdataCmdParaJson();
            });
            OdataCmdGlobalVar.ParamCount = OdataCmdGlobalVar.ParamCount + 1;
        });
        $('#lblOdataInputPara').html(strInputParaJson);
    }
    else
        $('#lblOdataInputPara').html("No Parameters defined yet");
    $('#btnOdata_AddParam').unbind('click');
    $('#btnOdata_AddParam').bind('click', function (e) {
        if ($('#txtOdata_AddParam').val().trim().length == 0 || $('#ddlOdata_AddParam').val() == "-1") {
            return;
        }
        if (IsStringContainsSpecialChar($('#txtOdata_AddParam').val())) {
            CommonErrorMessages(77);
            return;
        }

        var strHtml = '<div id="OdataAddParaDiv_' + OdataCmdGlobalVar.ParamCount + '"  class="QPContentRow">' +
                                            '<div class="QPContentText">' +
                                                $('#txtOdata_AddParam').val() +
                                            '</div>' +
                                            '<div class="QPContentType">' +
                                                GetOdataTypeText($('#ddlOdata_AddParam').val().trim()) +
                                            '</div>' +
                                            '<div class="QPContentDelete" align="center">' +
                                                '<img id="imgOdataDelQueryPara_' + OdataCmdGlobalVar.ParamCount + '" alt="" src="//enterprise.mficient.com/images/cross.png" />' +
                                            '</div>' +
                      '</div><div style="clear: both;"></div>';
        $('#AddOdataParamDiv').append(strHtml);
        $('#imgOdataDelQueryPara_' + OdataCmdGlobalVar.ParamCount).bind('click', function (e) {
            $('#OdataAddParaDiv_' + this.id.split('_')[1]).remove();
            createOdataCmdParaJson();
        });
        OdataCmdGlobalVar.ParamCount = OdataCmdGlobalVar.ParamCount + 1;
        $('#txtOdata_AddParam').val('');
        //  $('#ddlOdata_AddParam').val('-1');
        createOdataCmdParaJson();
    });
}

//Get odata parameter type text
function GetOdataTypeText(_Val) {
    if (_Val == '0') {
        return 'String';
    }
    else if (_Val == '1') {
        return 'Decimal';
    }
    else if (_Val == '2') {
        return 'Integer';
    }
    else if (_Val == '3') {
        return 'Boolean';
    }
    else {
        return '';
    }
}

//Get odata parameter type val
function GetOdataTypeVal(_Val) {
    if (_Val == 'String') {
        return '0';
    }
    else if (_Val == 'Decimal') {
        return '1';
    }
    else if (_Val == 'Integer') {
        return '2';
    }
    else if (_Val == 'Boolean') {
        return '3';
    }
    else {
        return '';
    }
}

//Create odata command input parameter
function createOdataCmdParaJson() {
    var outerJson = "";
    var strpara = "";
    var strInputParaJson = '';

    jQuery.each($('#AddOdataParamDiv')[0].children, function () {
        var innerJson = "";
        jQuery.each(this.children, function () {
            if (this.className == "QPContentText") {
                if (strInputParaJson.length > 0)
                    strInputParaJson += ', ';
                innerJson += '"para":' + JSON.stringify($(this).html()) + '';
                strInputParaJson += $(this).html();
                if (strpara.length > 0) strpara += ',';
                strpara += $(this).html();
            }
            if (this.className == "QPContentType") {
                innerJson += ',"typ":"' + GetOdataTypeVal($(this).html().trim()) + '"';
                strInputParaJson += ' [' + $(this).html().trim() + '] ';
            }
        });
        if (innerJson.length != 0) {
            if (outerJson.length > 0) outerJson += ",";
            outerJson += '{' + innerJson + '}';
        }
    });
    $('#lblOdataInputPara').html(strInputParaJson);
    OdataCmdGlobalVar.CommandParam = '[' + outerJson + ']';
}

//Edit odata command input parameter
function EditOdataCmdOutParam() {
    OdataCmdGlobalVar.ParamCount = 1;
    $('#OdataOutParamContent').html('');
    OdataCmdGlobalVar.ParamCount = 1;
    OdataCmdGlobalVar.Type = SetQueryOptionType();
    $('[id$=txtWsdlCommand_DatasetPath]').blur();
    var strHtml = "";
    AppActiveCtrl['Id'] = '';
    AppActiveCtrl['Value'] = '';
    AppActiveCtrl['LastKeyDown'] = 0;
    if (OdataCmdGlobalVar.OutParam != undefined && OdataCmdGlobalVar.OutParam.length > 0) {
        $.each(jQuery.parseJSON(OdataCmdGlobalVar.OutParam), function () {
            OdataCmdGlobalVar.ParamCount = OdataCmdGlobalVar.ParamCount + 1;
            $('#OdataOutParamContent').append('<div id="OdataParaContentText_' + OdataCmdGlobalVar.ParamCount + '" class="DpContentRow">' +
                    '<div class="DpContentText"> <input id="OdataParatext_' + OdataCmdGlobalVar.ParamCount + '" type="text" class="txtDpOption" /></div>' +
                    '<div class="DpContentValue"> <input id="OdataParaPath_' + OdataCmdGlobalVar.ParamCount + '" type="text"  class="txtDpOption"/></div>' +
                    '<div id="OdataParaDel_' + OdataCmdGlobalVar.ParamCount + '" class="DpOptionContentDelete">' +
                        '<img id="imgOdataParaDelete_' + OdataCmdGlobalVar.ParamCount + '" alt="delete" src="//enterprise.mficient.com/images/cross.png" class="CursorPointer" title="Delete Property"/>' +
                    '</div>' +
                  '</div><div style="clear: both;"></div>');
            $('#OdataParatext_' + OdataCmdGlobalVar.ParamCount).val(this['name']);
            $('#OdataParaPath_' + OdataCmdGlobalVar.ParamCount).val(OdataOutPathReplaceValue(this['path']));
            $('#imgOdataParaDelete_' + OdataCmdGlobalVar.ParamCount).bind('click', function () {
                $('#' + this.parentNode.parentNode.id).remove();
                OdataOutParamAddBtn();
            });
            ODataCtrlChangeEvent('OdataParaPath_' + OdataCmdGlobalVar.ParamCount, OdataCmdGlobalVar.Type);
        });
    }
    if (OdataCmdGlobalVar.ParamCount == 1) addNewOdataOutPara();
    OdataOutParamAddBtn();
    SubProcODataOutParam(true);
}

function SetQueryOptionType() {
    if ($($('[id$=ddlOdataCmd_EntType]')[1]).length > 0) {
        if ($($('[id$=ddlOdataCmd_EntType]')[1]).val() == '0') {
            return 1;
        }
        else if ($($('[id$=ddlOdataCmd_EntType]')[1]).val() == '1') {
            return 2;
        }
    }
    return 1;
}

function OdataOutPathReplaceValue(_Val) {
    if (OdataCmdGlobalVar.ResponseFormat == '0') {
        if (OdataCmdGlobalVar.ReturnType == '1') {
            _Val = _Val.replace(OdataCmdGlobalVar.XmlSingle + '.', '');
        }
    }
    else {
    }
    return _Val;
}

//Add out parameter
function OdataOutParamAddBtn() {
    var strLastDiv = "";
    var strHtml = '<img id="imgOdataOutParamAdd" alt="add" src="//enterprise.mficient.com/images/add.png" style="margin-left:5px;" class="CursorPointer" title="Add Property"/>';
    $('#imgOdataOutParamAdd').remove();
    $('#OdataOutParamContent').find('.DpOptionContentDelete').each(function () {
        strLastDiv = this;
    });
    if (strLastDiv.length != 0) {
        $(strLastDiv).append(strHtml);
        $('#imgOdataOutParamAdd').bind('click', function () {
            addNewOdataOutPara();
        });
    }
}

//imgOdataOutParamAdd click event to add parameter
function addNewOdataOutPara() {
    var IsValid = true;
    $('#OdataOutParamContent').find("*").each(function () {
        if (this.id.split('_')[0] == 'WsParatext') {
            if (this.value.trim().length == 0) {
                $('#aMessage').html('Please enter parameter name.');
                SubProcBoxMessage(true);
                IsValid = false;
            }
        }
    });
    if (!IsValid) return;
    OdataCmdGlobalVar.ParamCount = OdataCmdGlobalVar.ParamCount + 1;
    $('#OdataOutParamContent').append('<div id="OdataParaContentText_' + OdataCmdGlobalVar.ParamCount + '" class="DpContentRow"><div class="DpContentText"> <input id="OdataParatext_' + OdataCmdGlobalVar.ParamCount + '" type="text" class="txtDpOption" /></div><div class="DpContentValue"> <input id="OdataParaPath_' + OdataCmdGlobalVar.ParamCount + '" type="text"  class="txtDpOption"/></div><div id="OdataParaDel_' + OdataCmdGlobalVar.ParamCount + '" class="DpOptionContentDelete"><img id="imgOdataParaDelete_' + OdataCmdGlobalVar.ParamCount + '" alt="delete" src="//enterprise.mficient.com/images/cross.png" class="CursorPointer" title="Delete Property"/></div> </div><div style="clear: both;"></div>');
    $('#imgOdataParaDelete_' + OdataCmdGlobalVar.ParamCount).bind('click', function () {
        $('#' + this.parentNode.parentNode.id).remove();
        OdataOutParamAddBtn();
    });
    OdataCmdGlobalVar.Type = SetQueryOptionType();
    ODataCtrlChangeEvent('OdataParaPath_' + OdataCmdGlobalVar.ParamCount, OdataCmdGlobalVar.Type);
    OdataOutParamAddBtn();
}


//Popup for command output modal popup
function SubProcODataOutParam(_bol) {
    if (_bol) {
        showModalPopUp('SubProcODataOutParam', 'Output Properties', 520);
    }
    else {

        $('#SubProcODataOutParam').dialog('close');

    }
    if (_bol) {
        if ($($("#SubProcODataOutParam")[0].parentElement).length > 0) {
            $($("#SubProcODataOutParam")[0].parentElement).bind('click', function () {
                SubWsIntls(false);
            });
        }
        try {
            $('#SubProcODataOutParam').dialog('option', 'close', function () {
                SubWsIntls(false);
            });
            $('#SubProcODataOutParam').bind('dialogdragstart', function () {
                //SubWsIntls(false);
            });
        }
        catch (err) {
        }
    }
}

//Save odata command out parameter
function SaveOdataOutParam() {
    var strJson = "";
    var IsValid = true;
    var PreFix = '';
    if (OdataCmdGlobalVar.ResponseFormat == '0') {
        if (OdataCmdGlobalVar.ReturnType == '1') {
            PreFix = (OdataCmdGlobalVar.XmlSingle.length > 0 ? OdataCmdGlobalVar.XmlSingle + '.' : '');
        }
    }
    else {
        PreFix = '';
    }
    $('#OdataOutParamContent').find("*").each(function () {
        if (this.id.split('_')[0] == 'OdataParatext') {
            if (this.value.trim().length == 0) {
                $('#aMessage').html('Please enter parameter name.');
                SubProcBoxMessage(true);
                IsValid = false;
            }
            else {
            }

            if (strJson.length > 0) strJson += ",";
            strJson += '{"name":' + JSON.stringify(this.value.trim()) + ',"path":' + JSON.stringify(PreFix + $('#OdataParaPath_' + this.id.split('_')[1]).val().trim()) + '}';
        }
    });
    if (!IsValid) return;
    strJson = '[' + strJson + ']';
    OdataCmdGlobalVar.OutParam = strJson;
    SubProcODataOutParam(false);
}

//Change event for output properties values
function ODataCtrlChangeEvent(_Id, _Type) {
    //_Type 1 Output
    OdataCmdGlobalVar.Type = _Type;
    $('#' + _Id).unbind('click');
    $('#' + _Id).unbind('keyup');
    $('#' + _Id).unbind('keydown');
    $('#' + _Id).bind('click', function (event) {
        AppActiveCtrl['Id'] = this.id;
    });
    $('#' + _Id).bind('keyup', function (event) {
        var _Type = OdataCmdGlobalVar.Type;
        if (this.id.indexOf('txtOdataCmd_HttpGet_') > -1
           || this.id.indexOf('txtOdataCmd_HttpPost_') > -1
           || this.id.indexOf('txtOdataCmd_HttpPut_') > -1
           || this.id.indexOf('txtOdataCmd_HttpPutProp_') > -1
           || this.id.indexOf('txtOdataCmd_HttpDel_') > -1
           || this.id.indexOf('txtOdataCmd_Func_') > -1) {
            _Type = 3;
        }
        OdataPathChangeEvent(this, event, false, _Type);
    });
    $('#' + _Id).bind('keydown', function (event) {
        var _Type = OdataCmdGlobalVar.Type;
        if (this.id.indexOf('txtOdataCmd_HttpGet_') > -1
           || this.id.indexOf('txtOdataCmd_HttpPost_') > -1
           || this.id.indexOf('txtOdataCmd_HttpPut_') > -1
           || this.id.indexOf('txtOdataCmd_HttpPutProp_') > -1
           || this.id.indexOf('txtOdataCmd_HttpDel_') > -1
           || this.id.indexOf('txtOdataCmd_Func_') > -1) {
            _Type = 3;
        }
        OdataPathChangeEvent(this, event, true, _Type);
    });
}
//Change event to show intelligence popup
function OdataPathChangeEvent(e, event, IsKeyDown, Type) {
    $('#SubWsInnrIntls').html('');
    if (IsKeyDown) {
        if (event.which == '13') {
            if ($("#SubWsIntls").is(':visible')) {
                event.preventDefault();
                $("#SubWsIntls").find('.INTS-SELECTED').each(function () {
                    AddSelectedInslsVal(this.id);
                    var test = "";
                });
            }
        }
        if (event.which == '40' || event.which == '38') {
        }
        else {
            SubWsIntls(false, e);
        }
        $('#' + AppActiveCtrl['Id']).focus();
    }
    else {
        if (AppActiveCtrl['Id'] != e.id) AppActiveCtrl['LastKeyDown'] = 0;
        AppActiveCtrl['Id'] = e.id;
        AppActiveCtrl['Value'] = $(e).val();
        OdataParamIntlsn(e.id, $(e).val(), Type);
        SubWsIntls(true, e);
        $(e).focus();
        if (AppActiveCtrl['LastDivCount'] == undefined || AppActiveCtrl['LastDivCount'].length == 0) AppActiveCtrl['LastDivCount'] == 1;
        if (event.which == '40') {
            if ($("#SubWsIntls").is(':visible')) {
                var test = "";
                if (AppActiveCtrl['LastKeyDown'] != '40') {
                    if ($(AppActiveCtrl['SelectedIntls']).length > 0) {
                        changeIntlsColor('#WfViewCtrlDiv_' + (parseInt(AppActiveCtrl['SelectedIntls'].split('_')[1]) + 1));
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = 2;
                    }
                    else {
                        AppActiveCtrl['SelectedIntls'] = '#WfViewCtrlDiv_1';
                    }
                }
                else {
                    changeIntlsColor('#WfViewCtrlDiv_' + (AppActiveCtrl['LastDivCount'] + 1));
                    AppActiveCtrl['LastKeyDown'] = event.which;
                    if (AppActiveCtrl['LastDivCount'] >= (AppActiveCtrl['MaxDivCount'] - 1)) {
                        changeIntlsColor('#WfViewCtrlDiv_1');
                        AppActiveCtrl['LastDivCount'] = 1;
                    }
                    else AppActiveCtrl['LastDivCount'] = (AppActiveCtrl['LastDivCount'] + 1);
                }
            }
        }
        else if (event.which == '38') {
            if ($("#SubWsIntls").is(':visible')) {
                var test = "";
                if (AppActiveCtrl['LastKeyDown'] != '38') {
                    if ((AppActiveCtrl['LastDivCount'] - 1) < 1) {
                        changeIntlsColor('#WfViewCtrlDiv_1');
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = 1;
                    }
                    else {
                        changeIntlsColor('#WfViewCtrlDiv_' + (AppActiveCtrl['LastDivCount'] - 1));
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = (AppActiveCtrl['LastDivCount'] - 1);
                    }
                }
                else {
                    if ((AppActiveCtrl['LastDivCount'] - 1) < 1) {
                        changeIntlsColor('#WfViewCtrlDiv_' + (AppActiveCtrl['MaxDivCount'] - 1));
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = (AppActiveCtrl['MaxDivCount'] - 1);
                    }
                    else {
                        changeIntlsColor('#WfViewCtrlDiv_' + (AppActiveCtrl['LastDivCount'] - 1));
                        AppActiveCtrl['LastKeyDown'] = event.which;
                        AppActiveCtrl['LastDivCount'] = (AppActiveCtrl['LastDivCount'] - 1);
                    }
                }
            }
        }
        else {
            AppActiveCtrl['LastKeyDown'] = event.which;
            AppActiveCtrl['LastDivCount'] = 1;
        }
    }
}

//Add value in intelligence popup
function OdataParamIntlsn(_Id, _Val, _Type) {
    $('#SubWsInnrIntls').html('');
    $("#dialogInnerDiv").html('');
    var strVCount = 1;
    AppActiveCtrl['Length'] = 0;
    //_Type:1 in case of output properties
    if (_Type == 1) {
        if (OdataCmdGlobalVar.StructuralProp.length > 0) {
            $.each(OdataCmdGlobalVar.StructuralProp.split(','), function () {
                if (this.length > 0) {
                    $('#SubWsInnrIntls').append('<div id="WfViewCtrlDiv_' + strVCount + '" class="DbCsmdAddPara CursorPointer">' + this + '</div>');
                    WsdlParamMouseEvent('WfViewCtrlDiv_' + strVCount);
                    strVCount = strVCount + 1;
                }
            });
        }
    }

    //_Type:2 in case of funtion return
    else if (_Type == 2) {
        if (OdataCmdGlobalVar.FunctionReturnColl.length > 0) {
            $.each(OdataCmdGlobalVar.FunctionReturnColl.split(','), function () {
                if (this.length > 0) {
                    $('#SubWsInnrIntls').append('<div id="WfViewCtrlDiv_' + strVCount + '" class="DbCmdAddPara CursorPointer">' + this + '</div>');
                    WsdlParamMouseEvent('WfViewCtrlDiv_' + strVCount);
                    strVCount = strVCount + 1;
                }
            });
        }
    }

    //_Type:2 in case of input parameters
    else if (_Type == 3) {
        if (OdataCmdGlobalVar.CommandParam != undefined && OdataCmdGlobalVar.CommandParam.length > 0) {
            $.each(jQuery.parseJSON(OdataCmdGlobalVar.CommandParam), function () {
                if (this['para'].length > 0) {
                    $('#SubWsInnrIntls').append('<div id="WfViewCtrlDiv_' + strVCount + '" class="DbCmdAddPara CursorPointer">@@' + this['para'] + '@@</div>');
                    WsdlParamMouseEvent('WfViewCtrlDiv_' + strVCount);
                    strVCount = strVCount + 1;
                }
            });
        }
    }
    AppActiveCtrl['MaxDivCount'] = strVCount;
}

//Show webservice/odata command intelligence box
function SubWsIntls(_bol, _Id) {
    if (_bol) {
        $("#dialogInnerDiv").html('');
        $("#SubWsIntls").dialog({
            closeOnEscape: false,
            resizable: false,
            autoOpen: true,
            minWidth: 150,
            width: $(_Id).width(),
            draggable: false,
            zIndex: 100000
        });
        $($('#SubWsIntls')[0].previousElementSibling).hide();
        $('#' + $('#SubWsIntls').parent().find('div.ui-dialog-titlebar')[0].children[0].id).remove();
        $('#SubWsIntls').parent().find('div.ui-dialog-titlebar').find('a.ui-state-focus').removeClass('ui-state-focus');
        $('#SubWsIntls').parent().find('div.ui-dialog-titlebar').removeClass('ui-widget-header');
        $('#SubWsIntls').parent().find('div.ui-dialog-titlebar').removeClass('ui-corner-all');
        $('#SubWsIntls').parent().find('div.ui-dialog-titlebar').removeClass('ui-helper-clearfix');
        $('#SubWsIntls').parent().find('div.ui-dialog-content').removeClass('ui-widget-content');
        $('#SubWsIntls').parent().find('div.ui-dialog-content').removeClass('ui-dialog-content');
        $($("#SubWsIntls")[0].parentElement).css('z-index', '103');
        var TopPos = $($(_Id)[0].parentElement).offset().top + 25;
        var LeftPos = $($(_Id)[0].parentElement).offset().left;
        try { TopPos = TopPos - ($(window).scrollTop() != undefined ? $(window).scrollTop() : 0); }
        catch (err) { TopPos = $($(_Id)[0].parentElement).offset().top + 25; }
        try { LeftPos = LeftPos - ($(window).scrollLeft() != undefined ? $(window).scrollLeft() : 0); }
        catch (err) { LeftPos = $($(_Id)[0].parentElement).offset().left; }
        $("#SubWsIntls").dialog({ position: [LeftPos, TopPos] });
        $($("#SubWsIntls")[0].parentElement).css('top', TopPos);
        $($("#SubWsIntls")[0].parentElement).css('left', LeftPos);
        $("#SubWsIntls").focus();
        $($('#SubWsIntls')[0].previousElementSibling).hide();
    }
    else {
        if ($("#SubWsIntls").is(':visible')) {
            $("#SubWsIntls").dialog("close");
        }
    }
}


/*Show intellisense on filter(HttpType - Get)*/

var strCursorPos = "";
var xPos = "";
var yPos = "";
var ODataCmdParaShowCount = 1;

//this sub is used to set event in txtOdataCmd_Filter
function oDataCmdQueryTextBoxChange() {
    $('[id$=txtOdataCmd_Filter]').unbind('keydown');
    $('[id$=txtOdataCmd_Filter]').unbind('keypress');
    $('[id$=txtOdataCmd_Filter]').unbind('mousemove');
    $('[id$=txtOdataCmd_Filter]').bind("keydown", function (e) {
        if (e.which == 8) {
            if ($('#ODataDialogPara').is(':visible')) $("#ODataDialogPara").dialog("close");
        }
    });
    $('[id$=txtOdataCmd_Filter]').keypress(function (e) {
        QuerTextBoxKeyPress(e, '[id$=txtOdataCmd_Filter]');
    });
    $('[id$=txtOdataCmd_Filter]').bind('mousemove', function (e) {
        xPos = e.pageX;
        yPos = e.pageY;
    });
}

function getcursopos(_Id) {
    var pos = 0;
    var el = $(_Id).get(0);
    // IE Support 
    if (document.selection) {
        el.focus();
        var Sel = document.selection.createRange();
        var SelLength = document.selection.createRange().text.length;
        Sel.moveStart('character', -el.value.length);
        pos = Sel.text.length - SelLength;
    }
    // Firefox support 
    else if (el.selectionStart || el.selectionStart == '0')
        pos = el.selectionStart;

    return pos;
}

//this sub is used when keyPress in txtOdataCmd_Filter
function QuerTextBoxKeyPress(e, _Id) {
    strCursorPos = getcursopos(_Id);
    var KeyString = "@";
    //if ($('[id$=hdfDbCmdType]').val() == '4') KeyString = ":";
    if (String.fromCharCode(e.keyCode) == KeyString) {
        var str = $(_Id).val();
        if (str.substring(0, strCursorPos).replace(/^\s+|\s+$/g, "").lastIndexOf('=') == "-1") {
            return;
        }
        if (str.substring(0, strCursorPos).replace(/^\s+|\s+$/g, "").lastIndexOf('=') == str.substring(0, strCursorPos).replace(/^\s+|\s+$/g, "").length - 1) {
            ShowODataCmdParaInPopup(_Id);
            if ($("#ODataDialogPara").is(':visible')) {
                $("#ODataDialogPara").dialog("close");
            }
            else {
                ODataCommandParaDialogOpen(_Id, strCursorPos);
                //$("#ODataDialogPara").dialog("close");
                //ODataCommandParaDialogOpen(_Id, strCursorPos);
            }
        }
    }
}

//this sub is used to open popup window which contains parameters
function ODataCommandParaDialogOpen(_Id, strCursorPos) {
    $("#ODataDialogPara").dialog({
        closeOnEscape: false,
        resizable: false,
        autoOpen: true,
        minWidth: 50,
        width: 120,
        draggable: false,
        zIndex: 100000
    });

    $($('#ODataDialogPara')[0].previousElementSibling).hide();
    $('#' + $('#ODataDialogPara').parent().find('div.ui-dialog-titlebar')[0].children[0].id).remove();
    $('#ODataDialogPara').parent().find('div.ui-dialog-titlebar').find('a.ui-state-focus').removeClass('ui-state-focus');
    $('#ODataDialogPara').parent().find('div.ui-dialog-titlebar').removeClass('ui-widget-header');
    $('#ODataDialogPara').parent().find('div.ui-dialog-titlebar').removeClass('ui-corner-all');
    $('#ODataDialogPara').parent().find('div.ui-dialog-titlebar').removeClass('ui-helper-clearfix');
    $('#ODataDialogPara').parent().find('div.ui-dialog-content').removeClass('ui-widget-content');
    $('#ODataDialogPara').parent().find('div.ui-dialog-content').removeClass('ui-dialog-content');
    $("#ODataDialogPara").dialog({ position: [$(_Id).offset().left, $(_Id).offset().top + 20] });
    $("#ODataDialogPara").focus();
    setCursorInQueryBox($(_Id)[0].id, strCursorPos);
    $($('#ODataDialogPara')[0].previousElementSibling).hide();
}


//this sub is used to show  parameter popup
function ShowODataCmdParaInPopup(_Id) {
    ODataCmdParaShowCount = 1;
    $('#ShowODataCmdParaDiv').html('');
    if (OdataCmdGlobalVar.CommandParam !== null && OdataCmdGlobalVar.CommandParam.length !== 0) {
        $.each(jQuery.parseJSON(OdataCmdGlobalVar.CommandParam), function () {
            var strHtml = '<div id="OdataCmdAddParaDiv_' + ODataCmdParaShowCount + '" class="DbCmdAddPara">' + this['para'] + '</div>';
            $('#ShowODataCmdParaDiv').append(strHtml);
            $('#OdataCmdAddParaDiv_' + ODataCmdParaShowCount).bind('click', function (e) {
                $(_Id).val($(_Id).val().substring(0, strCursorPos + 1) + "" + $(this).html() + $(_Id).val().substring(strCursorPos + 1));
                if ($('#ODataDialogPara').is(':visible')) $("#ODataDialogPara").dialog("close");
                $(_Id).SelectionStart = strCursorPos;
                setCursorInQueryBox($(_Id)[0].id, ($(_Id).val().substring(0, strCursorPos + 1) + "" + $(this).html()).length);
            });
            $('#OdataCmdAddParaDiv_' + ODataCmdParaShowCount).bind('mouseover', function (e) {
                $('#OdataCmdAddParaDiv_' + this.id.split('_')[1]).css('background-color', '#C8C8C8');
            });
            $('#OdataCmdAddParaDiv_' + ODataCmdParaShowCount).bind('mouseout', function (e) {
                $('#OdataCmdAddParaDiv_' + this.id.split('_')[1]).css('background-color', '');
            });
            ODataCmdParaShowCount = ODataCmdParaShowCount + 1;
        });
    }


    $('[id$=txtOdataCmd_Filter]').click(function (e) {
        if ($("#ODataDialogPara").is(':visible')) {
            $("#ODataDialogPara").dialog("close");
        }
    });
}

function btnAddOdataSelectClick() {
    OdataCmdGlobalVar.count = 1;
    $('#ODataCmdContentDiv').html('');
    OdataSelectHtml();
    OdataCmdParseSelectJson(0);
    SubProcAddODataSelect(true, 350);
}


function showODataCopyObjectPopUp(_bol) {
    if (_bol) {
        showModalPopUp('divODataCopyObject', 'Copy Objects', 400);
    }
    else {
        $('#divODataCopyObject').dialog('close');

    }
    UniformControlDesign();
}

function validateObjectName() {
    var isValid = true;
    var strMessage = "";
    var strVal = ValidateNameInIde($('[id$=txtOdataCopyObjectName]').val().trim());
    if (strVal != 0) {
        if (strVal == 1) {
            strMessage += " * Please enter object name.</br>";
        }
        else if (strVal == 2) {
            strMessage += " * Object name cannot be more than 20 characters.</br>";
        }
        else if (strVal == 3) {
            strMessage += " * Object name cannot be less than 3 characters.</br>";
        }
        else if (strVal == 4) {
            strMessage += " * Please enter valid object name.</br>";
        }
        else if (strVal == 5) {
            strMessage += " * Please enter valid object name.</br>";
        }
    }

    if (strMessage.length != 0) {
        $('[id$=btnDataObj_Cancel]').hide();
        $('#aMessage').html(strMessage);
        SubProcBoxMessage(true);
        isValid = false;
    }
    return isValid;
}


function dialogtest() {
    $('#divdetails').children().remove();
    strInputParaJson = '';
    if ($('[id$=hidparameter]').val().length > 0) {
        strInputParaJson = '';
        var parajson = jQuery.parseJSON($('[id$=hidparameter]').val());
        $('#divdetails').append('<div id="divparameter" class="AddParameter">');
        $.each(jQuery.parseJSON($('[id$=hidparameter]').val()), function () {

            if (strInputParaJson == "") strInputParaJson = this['para'];
            else strInputParaJson += ', ' + $.trim(this['para']);
            var strHtml = '<div id="TestAddParaDiv_' + this['para'] + '" >' +
                             '<div class="QPContentText1">' + this['para'] + '</div>' +
                                    '<div class="QPContentType1" style="float:right;padding-right:105px !important;">' +
                                        '<input  id="para_' + this['para'] + '"type="text" style="width:100%" ><br>' +
                                        '</div></div><div style="clear: both;"></div>';
            $('#divparameter').append(strHtml);
        });
    }
    else {
        $('#divdetails').append('<div id="divparameter" class="AddParameter"> No Input Parameter in this object.</div>');
    }
    $('#divdetails').append('<div class="SubProcborderDiv"><div class="SubProcBtnMrgn" style="text-align:center" ><button type="button"  class="InputStyle"  onclick="fncsave();">Execute</button></div>');

    opendialogtest(true);
}


//Odata connector edit
function opendialogtest(_bol) {
    if (_bol) {
        showModalPopUp('dialogtestPara', 'Execute  Object', 500);

    }
    else {
        $('#dialogtestPara').dialog('close');
    }
}



function fncsave() {
    var lp = [];
    if (strInputParaJson.length > 0) {
        var resparameter = strInputParaJson.split(',');
        var i;
        for (i = 0; i < resparameter.length; i++) {
            lp.push({ "para": $.trim(resparameter[i]), "val": $('#para_' + resparameter[i]).val() });
        }
    }
    else {
    }
    $('[id$=hdflp]').val(JSON.stringify(lp));
    $('[id$=savebtn]').click();
}


//function DataCredentialBy(_bol) {
//    if (_bol) {
//        showModalPopUp('divCredentialBymobile', 'Enter Credential', 300);
//        $('[id$=txtUsername]').val('');
//        $('[id$=txtPassword]').val('');
//    }
//    else {
//        $('#divCredentialBymobile').dialog('close');
//    }
//}


function Credentialvalidation() {
    var user = $('[id$=txtUsername]').val();
    var password = $('[id$=txtPassword]').val();
    if (user == '') {
        alert('Please enter username');
    }
    else if (password == '') {
        alert('Please enter password');
    }
    if (user != '' && password != '') {
        DataCredentialBy(false);

        return true;
    }
    else {
        return false;
    }
}


function SubProcBoxMessageDataConection(_bol, Header) {
    if (_bol) {
        showModalPopUp('SubProcBoxMessage', Header, 400);
        $('#btnOkErrorMsg').focus();
    }
    else {
        $('#SubProcBoxMessage').dialog('close');
    }
}

﻿var count = 1;
var noOfRadioButton = 3;
var noOfCheckbox = 3;
var p;
var ifp = '';
var hashTable = {};
var DpOptionCount = 0;
var IsReport = false;
var IsMasterPage = false;
var IsForm = true;
var alignInHeader = '';
var IsDragStart = false;
var pvsControlId = '';
var IsDelete = false;
var AllFrmCmdIds = '';
var strRptTableColJson = "";
var IsFormChange = false;
var IsLocCtrlExist = false;
var IsRptCtrlExist = false;
var IsEltCtrlExist = false;
var IsTblCtrlExist = false;
//Tanika 
var ImageFolderPath = "";
/*----------------------------------------------------------------------*/
/* ASP Modal PopUp
/* 
/*----------------------------------------------------------------------*/
// (function ($) {
// /**
// * This is a simple jQuery plugin that works with the jQuery UI
// * dialog. This plugin makes the jQuery UI dialog append to the
// * first form on the page (i.e. the asp.net form) so that
// * forms in the dialog will post back to the server.
// *
// * This plugin is merely used to open dialogs. Use the normal
// * $.fn.dialog() function to close dialogs programatically.
// */
// $.fn.aspdialog = function () {
// if (typeof $.fn.dialog !== "function") return;

// var dlg = {};

// if ((arguments.length == 0)
// || (arguments[0] instanceof String)) {
// // If we just want to open it without any options
// // we do it this way.
// dlg = this.dialog({ "autoOpen": false, modal: true, resizable: false });
// dlg.parent().appendTo('form:first');
// dlg.dialog('open');
// } else {
// var options = arguments[0];
// options.autoOpen = false;
// options.bgiframe = true;

// dlg = this.dialog(options);
// dlg.parent().appendTo('form:first');
// dlg.dialog('open');
// }
// };
// })(jQuery);

/*Tanika*/
var arryCntrlJson = [];
function CntrlJson() {
    var id = '';
    var json = '';
}

function S4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
}

function GetGuid() {
    // then to call it, plus stitch in '4' in the third group
    guid = (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
    return guid;
}
/*Tanika*/


var FrmCurrentDiv = '';
var FrmPreviousDiv = '';
var wsType = '';
var HighLightSelectedDiv = false;
var PrvSelectedDiv = '';
var dpWsDispalyText = "";
var dpWsDispalyValue = "";
var appVersion = "1.0";
var FormVerison = "1.0";
var IsInitializeApp = false;

var IsTablet = false;
var IsAppForTablet = false;
var ApplicaionIconPath = '//enterprise.mficient.com/images/icon';

var ApplicaionIconImage = 'GRAY0207.png';
var ApplicaionIconName = 'GRAY0207';
var SESSION_TIMEOUT_MINUTES = 30;
var AppActiveCtrl = {};
$(function () {
    IsInitializeApp = true;
});
//****************************//
//Form change discard button click.
//****************************//
function FormChangesDiscardClick() {
    var test = "";
    IsFormChange = false;
    if (FrmCurrentDiv.split(',').length > 2) {
        SubProcFormChange(false);
        if (FrmCurrentDiv.split(',')[2] != '-1') {
            if (FrmCurrentDiv.split(',')[1] == 'view') {
                WfLoadFormDetails(FrmCurrentDiv.split(',')[0], FrmCurrentDiv.split(',')[2]);
            }
            else if (FrmCurrentDiv.split(',')[1] == 'viewMst') {
                ApplicationLoadMasterPage();
            }
            else if (FrmCurrentDiv.split(',')[1] == 'app') {
                ApplicationLoadApp(FrmCurrentDiv.split(',')[0], FrmCurrentDiv.split(',')[2]);
            }
            else if (FrmCurrentDiv.split(',')[1] == 'Mainapp') {
                ApplicationLoadMainApp('MainAppDiv', $('#MainAppId')[0].innerHTML.trim());
            }
            else if (FrmCurrentDiv.split(',')[1] == 'viewNew') {
                ApplicationLoadNewView();
            }
            else if (FrmCurrentDiv.split(',')[1] == 'appNew') {
                ApplicationLoadNewApp();
            }
            else if (FrmCurrentDiv.split(',')[1] == 'viewClose') {
                ApplicationLoadCloseView();
            }
            else if (FrmCurrentDiv.split(',')[1] == 'appClose') {
                ApplicationLoadCloseApp();
            }
        }
        else {
            $('#' + FrmCurrentDiv.split(',')[0]).addClass('active');
            SubProcFormChange(false);
            switch (FrmCurrentDiv.split(',')[1]) {
                case "Process":
                    LeftNavProcessClick(FrmCurrentDiv.split(',')[0]);
                    IsAppForTablet = false;
                    $('[id$=hdfWfIsTablet]').val('0');
                    break;
                case "Media":
                    LeftNavMediaFilesClick(FrmCurrentDiv.split(',')[0]);
                    break;
                case "MenuDef":
                    LeftNavMenuDefinitionFilesClick(FrmCurrentDiv.split(',')[0]);
                    break;
                case "WsCmd":
                    LeftNavWsCommandClick(FrmCurrentDiv.split(',')[0]);
                    break;
                case "WsCon":
                    LeftNavWsConnectorClick(FrmCurrentDiv.split(',')[0]);
                    break;
                case "DbCmd":
                    LeftNavDbCommandClick(FrmCurrentDiv.split(',')[0]);
                    break;
                case "DbCon":
                    LeftNavDbConnectorClick(FrmCurrentDiv.split(',')[0]);
                    break;
                case "ProcSet":
                    LeftNavProcSettingClick(FrmCurrentDiv.split(',')[0]);
                    break;
                case "PublishApp":
                    LeftNavPublishAppClick(FrmCurrentDiv.split(',')[0]);
                    break;
                case "EditView":
                    EditViewClicked();
                    SetUpdatedDivType('34', 'EditView');
                    break;
                case "SwitchApp":
                    SwitchPhoneToTablet();
                    break;
            }
        }
    }

}


// DataTable Serch BackGround Place in the Place Holder
function searchText(tableName) {
    var tablename = "#" + tableName + "_filter label input";
    $(tablename).attr("placeholder", "Search");
    $(tablename).addClass("InputStyle w_95")
    $(tablename).each(function () {
        $(this).click(
                    function () {
                        $(this).val('');
                    });
        $(this).blur(
                    function () {
                        if ($(this).val == '') {
                            $(this).val($(this).attr('placeholder'));
                        }
                    });
    });

}
//Left nav(Uploaded Images) click
function LeftNavMediaFilesClick(_Id) {
    LeftNavClickCommonMethod(_Id, 'Media', 'MediaFilesDiv', 'mPower - Uploaded Images');
}

//Left nav(Menu Settings) click
function LeftNavMenuDefinitionFilesClick(_Id) {
    LeftNavClickCommonMethod(_Id, 'MenuDef', 'MenuDefinitionDiv', 'mPower - Menu Settings');
}

//Left nav(Database Commands) click
function LeftNavDbCommandClick(_Id) {
    LeftNavClickCommonMethod(_Id, 'DbCmd', 'DatabaseCommandDiv', 'mPower - Database Commands');
}

//Left nav(Database Connectors) click
function LeftNavDbConnectorClick(_Id) {
    LeftNavClickCommonMethod(_Id, 'DbCon', 'DatabaseConnectorDiv', 'mPower - Database Connectors');
}

//Left nav(Webservice Connectors) click
function LeftNavWsConnectorClick(_Id) {
    LeftNavClickCommonMethod(_Id, 'WsCon', 'WebserviceConnectorDiv', 'mPower - Web service Connectors');
}

//Left nav(Webservice Commands) click
function LeftNavWsCommandClick(_Id) {
    LeftNavClickCommonMethod(_Id, 'WsCmd', 'WebserviceCommandDiv', 'mPower - Web service Commands');
}

//Left nav(OData Connectors) click
function LeftNavOdataConnClick(_Id) {
    LeftNavClickCommonMethod(_Id, 'OdataConn', 'OdataConnectorDiv', 'mPower - OData Connectors');
}

//Left nav(OData Commands) click
function LeftNavOdataCmdClick(_Id) {
    LeftNavClickCommonMethod(_Id, 'OdataCmd', 'OdataCommandDiv', 'mPower - OData Commands');
}

//Left nav(Application) click
function LeftNavAppliction(_Id) {
    CurrentDiv = _Id;
    LeftNavProcessClick(_Id)
}

//Left nav(App Permissions) click
function LeftNavProcSettingClick(_Id) {
    LeftNavClickCommonMethod(_Id, 'ProcSet', 'ProcessSettingsDiv', 'mPower - App Permissions');
}

//Left nav(Publish App) click
function LeftNavPublishAppClick(_Id) {
    LeftNavClickCommonMethod(_Id, 'PublishApp', 'PublishAppDiv', 'mPower - Publish App');
}

//Common method for left nav click
function LeftNavClickCommonMethod(_Id, _Value, _ContDiv, _Title) {
    try {
        if (setChangeInForm(_Id, _Value)) {
            return;
        }
        $('#' + _Id).addClass('active');
        WfSetIsFrmRptMst();
        ShowContainerDiv(_ContDiv);
        $(document).attr('title', _Title);
        WfIsWorkFlow = false;
    }
    catch (err) {
    }
}

//Left nav process click
function LeftNavProcessClick(_Id) {
    try {
        $('#' + _Id).addClass('active');
        if (setChangeInForm(_Id, "Process")) {
            return;
        }
        WfSetIsFrmRptMst();
        ShowContainerDiv('ProcessWorkflowDiv');
        $(document).attr('title', 'mPower - Application');
        WfIsWorkFlow = true;
        ApplicationClose();
        $('#WfToolboxHeadDiv').show();
    }
    catch (err) {
    }
}

//********************/
//This sub is used close appliction
//********************/

// function ApplicationClose() {
// $('#WfApplNameHeaderDiv').hide();
// $('#WfMainContainerDiv').hide();
// $('#FormToolboxHeadDiv').hide();
// $('#WfToolboxAppPropDiv').hide();
// $('#WfSaveDiv').hide();
// $('#WfSaveAsDiv').hide();
// $('#WfAddViewDiv').hide();
// $('#WfCloseAppDiv').hide();
// $('#WfSwitchAppDiv').hide();
// $('#WfRemoveDiv').hide();
// $('#WfNewDiv').show();
// $('#WfOpenDiv').show();
// $('#WfSep3').hide();
// $('#WfSep4').hide();
// $('#WfSep5').hide();
// $('#WfSep1').show();
// $('#WfSep2').show();
// $('#WfSep6').hide();
// $('#WfSep7').hide();
// $('#WfSep8').hide();
// $('#WfCommitDiv').hide();
// $('#WfSep7').hide();
// $('#WfSep9').hide();
// }

//****************************//
//this sub is used to check if there is any unsaved changes in form,report or masterpage.
//****************************//
function setChangeInForm(_Id, _Type) {
    WfDialogBox(false);
    FromContextMenu(false);
    return ApplictionSetChangeInForm(_Id, _Type, '-1');
}

//Set page type
function WfSetIsFrmRptMst() {
    IsReport = false;
    IsMasterPage = false;
    IsForm = false;
}

//Show container div
function ShowContainerDiv(_Id) {
    hideControl();
    $('#' + _Id).show();
}

//hide containers
function hideControl() {
    $('#DatabaseConnectorDiv').hide();
    $('#DatabaseCommandDiv').hide();
    $('#WebserviceConnectorDiv').hide();
    $('#WebserviceCommandDiv').hide();
    $('#MenuDefinitionDiv').hide();
    $('#MediaFilesDiv').hide();
    $('#MastersDiv').hide();
    $('#FormsDiv').hide();
    $('#ReportsDiv').hide();
    $('#FormDesignerDiv').hide();
    $('#ProcessWorkflowDiv').hide();
    $('#ProcessSettingsDiv').hide();
    $('#PublishAppDiv').hide();
    $('#OdataConnectorDiv').hide();
    $('#OdataCommandDiv').hide();
}

//When change is in form ,app or in master page
function ApplictionSetChangeInForm(_Id, _Type, _FrmId) {
    FrmPreviousDiv = FrmCurrentDiv;
    FrmCurrentDiv = _Id + ',' + _Type + ',' + _FrmId;
    if (IsForm)
        return ApplictionSetChangeCommon('The form has been modified and there are unsaved changes.<br />Do you want to save this form ?', 1, IsFormChange);
    if (IsMasterPage)
        return ApplictionSetChangeCommon('The master page has been modified and there are unsaved changes.<br />Do you want to save this master page ?', 2, IsFormChange);
    if (WfIsWorkFlow)
        return ApplictionSetChangeCommon('The app has been modified and there are unsaved changes.<br />Do you want to save this app ?', 3, IsFormChange);
    return false;
}

//Method used in ApplictionSetChangeInForm
function ApplictionSetChangeCommon(_Text, _Show, _IsFormChange) {
    if (!_IsFormChange) {
        return false;
    }
    else {
        $('#aFormChangeMsg').html(_Text);
        if (_Show == 1) {
            $('#btnSaveView').show();
            $('#btnSaveAppOnCfm').hide();
        }
        else if (_Show == 2) {
            $('#btnSaveView').show();
            $('#btnSaveAppOnCfm').hide();
        }
        else if (_Show == 3) {
            $('#btnSaveView').hide();
            $('#btnSaveAppOnCfm').show();
        }
        SubProcFormChange(true);
        return true;
    }
}

//Common error message method.
function CommonErrorMessages(_MsgNo) {
    switch (_MsgNo) {
        case 0:
            $('#aMessage').html('Image size is greater than 250 KB.');
            break;
        case 1:
            $('#aMessage').html('Width of image is greater than.');
            break;
        case 2:
            $('#aMessage').html('Width of image is less than 960px.');
            break;
        case 3:
            $('#aMessage').html('Height of image is greater than.');
            break;
        case 4:
            $('#aMessage').html('Height of image is less than 960px.');
            break;
        case 5:
            $('#aMessage').html('Can not upload more than 20 images.');
            break;
        case 6:
            $('#aMessage').html('Only jpg and png file format accepted.');
            break;
        case 7:
            $('#aMessage').html('File can not be uploaded.Only xml or txt file format is allowed.');
            break;
        case 8:
            $('#aMessage').html('File not uploaded.Internal server Error.');
            break;
        case 9:
            $('#aMessage').html('File can not be uploaded.File size greater than 500KB.');
            break;
        case 10:
            $('#aMessage').html('File can not be uploaded.Only csv format is allowed.');
            break;
        case 11:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid name.');
            break;
        case 12:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid name. Name already exist from another value.');
            break;
        case 13:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Enter form name of all forms.');
            break;
        case 14:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Name already exists.');
            break;
        case 15:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please add any in-transition.');
            break;
        case 16:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please add any out transition.');
            break;
        case 17:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid name.');
            break;
        case 18:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid name. Name can not be start with numeric value.');
            break;
        case 19:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid control name. Name can not be start with numeric value.');
            break;
        case 20:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid name. Name can not be start with numeric value.');
            break;
        case 21:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid control name.');
            break;
        case 22:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid from title.');
            break;
        case 23:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Name already exist. Please enter unique row id name.');
            break;
        case 24:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Name already exist. Please enter unique title name.');
            break;
        case 25:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Control name already exist. Please change name of control.');
            break;
        case 26:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid list row id name.');
            break;
        case 27:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid list title name.');
            break;
        case 28:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>There can be only one out transition from view.');
            break;
        case 29:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter only numeric values.');
            break;
        case 32:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid form name.');
            break;
        case 33:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>You cannot delete this section.');
            break;
        case 34:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Parameter name already exists.');
            break;
        case 35:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Control minwidth cannot be less than 200px.');
            break;
        case 36:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Database command does not contains any parameter.');
            break;
        case 37:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>App cannot be deleted.Internal server error.');
            break;
        case 38:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter version history.');
            break;
        case 39:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>App connot be committed.Internal server error.');
            break;
        case 40:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Internal server error.Please try again.');
            break;
        case 41:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please select a category.');
            break;
        case 42:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please select atleast one group.');
            break;
        case 43:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Select atleast on option.');
            break;
        case 44:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter App name.');
            break;
        case 45:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>App name already exist.');
            break;
        case 46:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Data cannot be saved.Internal server error.');
            break;
        case 47:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Internal server error.');
            break;
        case 48:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid app name.');
            break;
        case 49:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid view name.');
            break;
        case 50:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Control name cannot be more than 20 charecters.');
            break;
        case 51:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Control name cannot be less than 3 charecters.');
            break;
        case 52:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Description cannot be more than 30 charecters.');
            break;
        case 53:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Title cannot be more than 20 charecters.');
            break;
        case 54:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Title name cannot be less than 3 charecters.');
            break;
        case 55:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid title.');
            break;
        case 56:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid description.');
            break;
        case 57:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Syntax Error.Please enter valid control name.');
            break;
        case 58:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Syntax Error.Please enter valid view name.');
            break;
        case 59:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Syntax Error.Please enter valid command name.');
            break;
        case 60:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Syntax Error.Please enter valid value.');
            break;
        case 61:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid source view name.');
            break;
        case 62:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid condionbox name.');
            break;
        case 63:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter App description.');
            break;
        case 64:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter plot width.');
            break;
        case 65:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter max plot width.');
            break;
        case 66:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Wsdl does not contains Soap port type.');
            break;
        case 67:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please select application.');
            break;
        case 68:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Condition Box does not contain any condition.');
            break;
        case 69:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Condition Box does not contain any source view.');
            break;
        case 70:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Label text can not be empty.');
            break;
        case 71:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Button text can not be empty.');
            break;
        case 72:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please select detail view.');
            break;
        case 73:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please defind Save/Next button first.');
            break;
        case 74:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Only databse connectors can be execute in batch processing mode.');
            break;
        case 75:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Only HiddenVariables and labels are permitted with EditableList.');
            break;
        case 76:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>App description must contain at least 10 characters.');
            break;
        case 77:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid parameter name.<br />Special characters are not allowed in parameter name');
            break;
        case 78:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please add editable list property first.');
            break;
        case 79:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please select column for table.');
            break;
        case 80:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter header text.');
            break;
        case 81:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please select unique column for table.');
            break;
        case 82:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter unique header text for table.');
            break;
        case 83:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter placeholder text or label text.');
            break;
        case 84:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Multiple editable lists are not allowed in a command.<br /> Please use only one editable list.');
            break;
        case 85:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Prompt text can not be empty.');
            break;
        case 86:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid Multiplication Factor.');
            break;
        case 87:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid Auto Refresh Time.<br />It should not be less than 0.5(min).');
            break;
        case 88:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>All Editable List properties are not linked to a control.');
            break;
        case 89:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid range.<br />[Range Example : [Numeric]-[Numeric] ]');
            break;
        case 90:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>');
            break;
        case 91:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter valid property name.Property name cannot be "count".');
            break;
        case 92:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Only hiddenVariables and labels are permitted with table control.');
            break;
        case 93:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Workflow count exceeds from allotted maximum number of workflows.');
            break;
        case 94:
            $('#aMessage').html('<a class="hide">' + _MsgNo + '</a>Please enter only integers.');
            break;
    }
    SubProcBoxMessage(true);
    //showMessage($('#aMessage').html(), $.alert.Error, DialogType.Error);
}

//To show spinner popup
function showWait(_bol) {
    if (_bol) {
        spinDiv(true);
        if ($('#spinDiv').length > 0)
            if ($($('#spinDiv')[0].parentElement).length > 0) {
                $($('#spinDiv')[0].parentElement).width(60);
                $($('#spinDiv')[0].parentElement).height(60);
                setModalPosition($('#spinDiv').parent());
                $($('#spinDiv')[0].previousElementSibling).hide();
                $($('#spinDiv')[0].parentElement).addClass("spinDivCorner");
            }
    }
    else {
        spinDiv(false);
    }
}

//To show/hide spinner container div
function spinDiv(_bol) {
    if (_bol) {
        $("#spinDiv").dialog({
            modal: true,
            closeOnEscape: false,
            resizable: false,
            autoOpen: true
        });
    }
    else {
        $('#spinDiv').dialog('close');
    }
}

//To set popup position
//Copied from Scripts.js
function setModalPosition(divToSet) {

    PgDim = getPageSize();
    var vleft = ((PgDim[2] - divToSet.width()) / 2);
    var vtop = ((PgDim[3] - divToSet.height()) / 2);
    divToSet.offset({ top: vtop, left: vleft });
}

//Copied from Scripts.js
function getPageSize() {
    var xScroll, yScroll;
    if (window.innerHeight && window.scrollMaxY) {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    }
    // all but Explorer Mac 
    else if (document.body.scrollHeight > document.body.offsetHeight) {
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    }
    // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari 
    else {
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }

    var windowWidth, windowHeight;
    // all except Explorer
    if (self.innerHeight) {
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    }
    // Explorer 6 Strict Mode 
    else if (document.documentElement && document.documentElement.clientHeight) {
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    }
    // other Explorers 
    else if (document.body) {
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }
    if (yScroll < windowHeight) pageHeight = windowHeight;
    else pageHeight = yScroll;
    // for small pages with total width less then width of the viewport
    if (xScroll < windowWidth) {
        pageWidth = windowWidth;
    }
    else {
        pageWidth = xScroll;
    }
    arrayPageSize = new Array(pageWidth, pageHeight, windowWidth, windowHeight)
    return arrayPageSize;
}


//Context menu(On Right click) in app flow.
// function WfDialogBox(_Open, _PositionX, _PositionY) {
// if (_Open) {
// FromContextMenu(false);
// ShowContextMenu('dialog', _PositionX, _PositionY);
// }
// else {
// $("#dialog").dialog("close");
// }
// }

//Context menu(On Right click) in app flow.
// function FromContextMenu(_Open, _PositionX, _PositionY) {//
// if (_Open) {
// WfDialogBox(false);
// ShowContextMenu('SubprocFormContext', _PositionX, _PositionY);
// }
// else {
// $("#SubprocFormContext").dialog("close");
// }
// }

//Change context menu css
// function ShowContextMenu(_Id, _PositionX, _PositionY) {
// $("#" + _Id).siblings(".ui-dialog-titlebar").hide();
// $("#" + _Id).dialog("option", "minWidth", 100);
// $("#" + _Id).dialog("option", "resizable", false);
// $("#" + _Id).dialog("option", "width", 160);
// $("#" + _Id).dialog("option", "draggable", false);
// $("#" + _Id).dialog({ position: [_PositionX, _PositionY] });
// $("#" + _Id).dialog("open");
// $("#" + _Id).focus();
// $("#" + _Id).css('min-height', '40px');
// $("#" + _Id).css('padding-left', '2px');
// $("#" + _Id).css('padding-top', '1px');
// $("#" + _Id).css('padding-right', '2px');
// }


//Upload image popup
function ImageShow() {
    SubProcImageUploadIFrame(false);
    UpdateUpdUploadedImage();
}

function ChangeWsdlFileId(_Id) {
    $('[id$=hdfWsdlUploadId]').val(_Id);
}

//Clear all hidden fields
function ClearHiddenFields() {
    $('[id$=hdfWsContent]').val('');
    $('[id$=hdfAllCtrlInFrm]').val('');

    $('[id$=hdfHtml]').val('');
    $('[id$=hdfJson]').val('');
    $('[id$=hdfSaveToDatabse]').val('');
    $('[id$=hdfAllControlName]').val('');
    $('[id$=hdfFrmName]').val('');

    $('[id$=hdfChkDetails]').val('');
    $('[id$=hdfWsChkDetails]').val('');
    $('[id$=hdfGuid]').val('');
    $('[id$=hdfSubmitButton]').val('');
    $('[id$=hdfFtype]').val('');
    $('[id$=hdfIsValid]').val('');
    $('[id$=hdfid]').val('');
    $('[id$=hdfvalidWs]').val('');
    $('[id$=hdfDbJson]').val('');
    $('[id$=hdfHHtml]').val('');
    $('[id$=hdfFHtmL]').val('');
    $('[id$=hdfc]').val('');
    $('[id$=hdfCmdIds]').val('');
    $('[id$=hdfWfFrmId]').val('');
    $('[id$=hdfConFrmId]').val('');
    $('[id$=hdfCndCtrlFId]').val('');
    $('[id$=hdfWfJson]').val('');
    $('[id$=hdfWfForms]').val('');
    $('[id$=hdfWFId]').val('');
    $('[id$=hdfWfValid]').val('');
    $('[id$=hdfWFparentId]').val('');
    $('[id$=hdfSql]').val('');
    $('[id$=hdfSqlParam]').val('');
    $('[id$=hdfOutCol]').val('');
    $('[id$=hdfDataTran]').val('');
    $('[id$=hdfidDel]').val('');
    $('[id$=hdfRpcParam]').val('');
    $('[id$=hdfOutRpcParam]').val('');
    $('[id$=hdfIsRptClk]').val('');
    $('[id$=hdfOnClsCmdId]').val('');


    $('#hdfCndSourceFrm').val('');
    $('#hdfLast').val('');
    $('#hdfChartType').val('');
    $('#hdfIsValidWfCnd').val('');
    $('#hdfOutJson').val('');
    $('#hdfCndCtrl').val('');
    $('#hdfCndCtrlTyp').val('');
    $('#hdfElementName').val('');
    $('#hdfAppStartUpIntCmd').val('');
    $('#hdfIsAppStart').val('');


    $('[id$=hdfRptRowIndex]').val('');
    $('[id$=hdfViewIntCmd]').val('');
    $('[id$=hdfViewIntCmdType]').val('');
    $('[id$=hdfIsIntView]').val('');
    $('[id$=hdfWfIntJson]').val('');
    $('[id$=hdfIsEdit]').val('');
    $('[id$=hdfIsTablet]').val('');
    $('[id$=hdfWfIsTablet]').val('');
    $('[id$=hdfWfSingleRecord]').val('');
    $('[id$=hdfAllCtrlInFrm]').val('');
}

//Row click event of repeaters
function RptRowClick(e, _type) {
    $.each(e.children, function () {
        if (this.title == "id") {
            $('[id$=hdfRptRowIndex]').val(_type + ',' + this.innerHTML.trim());
            if (_type == '3') {
                SubProcWfList(false);
                WfNewWorkFlow();
                ApplicationSelectAppDiv('#MainAppDiv');
            }
        }
    });
    UpdateUpdBtnRowIndex();
}

//rptMenuCategoryDtls row click event of repeaters
function RptMenuCatRowClick(e, _type) {
    $.each($(e.parentElement)[0].children, function () {
        if (this.title == "id") {
            $('[id$=hdfRptRowIndex]').val(_type + ',' + this.innerHTML.trim());
        }
    });
    UpdateUpdBtnRowIndex();
}

//Change dropdown css
function ChangeDropDownStyle() {
    CallUnformCss("[id$=ddlDbConnectorDbType]");
    CallUnformCss('[id$=ddlDbDsns]');
    CallUnformCss('[id$=ddlUseMplugin]');
    CallUnformCss("[id$=ddlWebSrcConnectorType]");
    CallUnformCss("[id$=ddlWsUseMplugin]");
    CallUnformCss("[id$=ddlWebSrcConnectorRsp]");
    CallUnformCss("[id$=ddl_DbConnactor]");
    CallUnformCss('[id$=ddl_WsConnactor]');
    CallUnformCss('[id$=ddlWsCommad_DataReturned]');
    CallUnformCss('[id$=ddlWsdlCommad_DataReturned]');
    CallUnformCss("[id$=ddlWsCmd_WsdlUpdType]");
    CallUnformCss("[id$=ddl_WsConnectorMethod]");
    CallUnformCss("[id$=ddlWsConnServices]");
    CallUnformCss("#ddlRpc_SelectStructPara");
    CallUnformCss("#ddlRpc_ParaType");
    CallUnformCss("#ddlRpc_SelectArrayPara");
    CallUnformCss("#ddlRpc_ArrayDataType");
}

//Set image path
function setIfp(_Ifp) {
    ifp = _Ifp + '/';
}

//txtDbCommand_SqlQuery change event
function txtDbCommandSqlQueryChange() {
    $('[id$=hdfOutCol]').val('');
}

//ddlDbCommand_ReturnType change event
function ddlDbCommand_ReturnTypeChange() {
    CallUnformCss('[id$=ddlDbCommand_ReturnType]');
}

//this sub is used to add advanced data translation.
function SubProcFormChange(_bol) {
    SubProcCommonMethod('SubProcFormChange', _bol, 'Save Changes', 350);
}

//SubProcImageUploadIFrame popup
function SubProcImageUploadIFrame(_bol) {
    SubProcCommonMethod('SubProcImageUploadIFrame', _bol, 'Upload Image', 420);
}

//SubProcMenuCatIcon popup
function SubProcMenuCatIcon(_bol) {
    SubProcCommonMethod('SubProcMenuCatIcon', _bol, 'Icons', 930);
}

//SubProcPublishApp popup
function SubProcPublishApp(_bol) {
    SubProcCommonMethod('SubProcPublishApp', _bol, 'Publish App', 400);
}

//SubProcAppInfo popup
function SubProcAppInfo(_bol) {
    SubProcCommonMethod('SubProcAppInfo', _bol, 'App Info', 400);
}

//SubProcImageDelConfirnmation popup
function SubProcImageDelConfirnmation(_bol, _title) {
    SubProcCommonMethod('SubProcImageDelConfirnmation', _bol, _title, 300);
}

//SubProcDeleteCmdConfrmation popup
function SubProcDeleteCmdConfrmation(_bol, _title) {
    SubProcCommonMethod('SubProcDeleteCmdConfrmation', _bol, _title, 300);
}

//Common method to show popup
function SubProcCommonMethod(_Id, _Bol, _Title, _Width) {
    if (_Bol) {
        showModalPopUp(_Id, _Title, _Width);
    }
    else {
        $('#' + _Id).dialog('close');
    }
}

//show delete message popup
function SubProcBoxMessage(_bol) {
    if (_bol) {
        showModalPopUp('SubProcBoxMessage', 'Error', 400);
        //showModalPopUp('SubProcBoxMessage', '<div><div style="float:left;margin-right:5px;max-height:20px;"><img src="//enterprise.mficient.com/images/dialog_error.png"/></div><div style="float:left;font-weight:700;">Error Message</div></div><div style="clear: both;"></div>', 400);
        $('#btnOkErrorMsg').focus();
        //     $($('#SubProcBoxDeleteMessage')[0].parentElement).attr('min-height', '128px');
    }
    else {
        $('#SubProcBoxMessage').dialog('close');
    }
}
//Show Sccess Meessage 


function SubProcBoxSucessessMessage(_bol) {
    if (_bol) {
        showModalPopUp('SubProcBoxMessage', 'Info', 400);
        //showModalPopUp('SubProcBoxMessage', '<div><div style="float:left;margin-right:5px;max-height:20px;"><img src="//enterprise.mficient.com/images/dialog_error.png"/></div><div style="float:left;font-weight:700;">Error Message</div></div><div style="clear: both;"></div>', 400);
        $('#btnOkErrorMsg').focus();
        $($('#SubProcBoxDeleteMessage')[0].parentElement).attr('min-height', '128px');
    }
    else {
        $('#SubProcBoxMessage').dialog('close');
    }
}











//show delete control message popup
function SubProcBoxDeleteMessage(_bol, isSepOrSpacerCntrl) {
    if (_bol) {
        SubProcDeletSpacerOrSepControl
        if (isSepOrSpacerCntrl) {
            showDialog('SubProcDeletSpacerOrSepControl', 'Warning', 250, 'dialog_warning.png', false);
        }
        else {
            showDialog('SubProcBoxDeleteMessage', 'Warning', 400, 'dialog_warning.png', false);
        }
    }
    else {
        if ($('#SubProcBoxDeleteMessage').is(':visible')) $('#SubProcBoxDeleteMessage').dialog('close');
        else if ($('#SubProcDeletSpacerOrSepControl').is(':visible')) $('#SubProcDeletSpacerOrSepControl').dialog('close');
    }
}
//show delete control message popup
function SubProcBoxDeleteSectionMessage(_bol) {
    if (_bol) {
        showDialog('SubProcBoxDeleteSectionMessage', 'Delete Control', 400, 'dialog_warning.png');
    }
    else {
        $('#SubProcBoxDeleteSectionMessage').dialog('close');
    }
}

//To show renew session message popup
function SubProcRenewSession(_bol) {
    if (_bol) {
        if (!$("#SubProcRenewSession").is(":visible")) {
            $('[id$=txtReEnterPassword]').val('');
            showModalPopUp('SubProcRenewSession', 'Session Expired', 400);
        }
    }
    else {
        $('#SubProcRenewSession').dialog('close');
    }
}

//Confirmation message popup
function SubProcConfirmBoxMessage(_bol, _title, _width) {
    if (_bol) {
        showModalPopUp('SubProcBoxMessage', _title, 400);
        // showModalPopUp('SubProcConfirmBoxMessage', '<div><div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/dialog_info.png"/></div><div style="float:left;font-weight:700;">  ' + _title + '</div></div><div style="clear: both;"></div>', _width);
        $('#btnCnfFormSave').focus();
    }
    else {
        $('#SubProcConfirmBoxMessage').dialog('close');
    }
}

//Message box in wsdlfile upload
function SubProcBoxUldWsdlCfm(_bol, _width, _Type) {
    if (_bol) {
        var strTitle = 'Error';
        if (_Type == 1)
            strTitle = "Upload Wsdl File";
        $('#UploadWsdlCfnDiv').show(); $('#btnUploadWsdlCfn').show(); $('#GetWsdlMannual').hide();
        showModalPopUp('SubProcBoxUldWsdlCfm', strTitle, _width);
    }
    else {
        $('#SubProcBoxUldWsdlCfm').dialog('close');
    }
}

//Set cursor in querybox
function setCursorInQueryBox(node, pos) {
    var node = (typeof node == "string" ||
    node instanceof String) ? document.getElementById(node) : node;
    if (!node) {
        return false;
    }
    else if (node.setSelectionRange) {
        node.setSelectionRange(pos, pos);
        return true;
    }
    return false;
}

//Check string contains special character
function IsStringContainsSpecialChar(_Name) {
    var IsExist = false;
    if (/[^A-Za-z\d]/.test(_Name)) {
        IsExist = true;
    }
    return IsExist;
}

//Initialize app method
// function InitializeApp() {
// if (IsInitializeApp) {
// InitializeAppDesigner();
// IsInitializeApp = false;
// }
// }

//Validate name string
function ValidateNameInIde(_Name) {
    if (_Name.trim().length == 0)
        return 1;
    if (_Name.trim().length > 20)
        return 2;
    if (_Name.trim().length < 3)
        return 3;
    if (IsStringContainsSpecialChar(_Name))
        return 4;
    if (Math.floor(_Name.substring(0, 1)) == _Name.substring(0, 1))
        return 5;
    return 0;
}

//Validate name app name
function ValidateNameIdeAppName(_Name) {
    if (_Name.trim().length == 0)
        return 1;
    if (_Name.trim().length > 20)
        return 2;
    if (_Name.trim().length < 3)
        return 3;
    if (CheckIdeAppName(_Name))
        return 4;
    if (Math.floor(_Name.substring(0, 1)) == _Name.substring(0, 1))
        return 5;
    return 0;
}

//Validate app name
function CheckIdeAppName(_Name) {
    var IsExist = false;

    if (!/^[a-z0-9-\s]+$/i.test(_Name)) {
        IsExist = true;
    }

    return IsExist;
}


//Validate description in app
function ValidateDescriptionInIde(_Des) {
    if (_Des.length > 30) {
        CommonErrorMessages(52);
        return 2;
    }
    if (!IsWfFrmTitleValid(_Title.trim())) {
        CommonErrorMessages(56);
        return 4;
    }
    return 0;
}

//Validate title in app
function ValidateTitleInIde(_Title) {
    if (_Title.trim().length == 0) {
        CommonErrorMessages(55);
        return 1;
    }
    if (_Title.trim().length > 20) {
        CommonErrorMessages(53);
        return 2;
    }
    if (_Title.trim().length < 3) {
        CommonErrorMessages(54);
        return 3;
    }
    return 0;
}



//Validate app publish (btnPulishAppSave click event) 
function ValidateAppPublish() {
    if ($('[id$=ddlChangeToVersion]')[0] == undefined) return;
    if ($('[id$=ddlChangeToVersion]')[0].children[1] == undefined) return;
    if ($('[id$=ddlChangeToVersion]')[0].children[1].value == "0") {
        CommonErrorMessages(43);
    }
}

//COOKIE_MSID = "MFMSID";
//COOKIE_MUEID = "MFMUEID";

//function createCookie(cookieName, cookieValue, cookieExpiryMins) {
//    if (cookieExpiryMins) {
//        var date = new Date();
//        date.setTime(date.getTime() + (cookieExpiryMins * 60 * 1000));
//        var expires = "; expires=" + date.toUTCString();
//    }
//    else var expires = "";
//    document.cookie = cookieName + "=" + encodeURIComponent(cookieValue) + expires + "; path=/";
//}

//function readCookie(name) {
//    var nameEQ = name + "=";
//    var ca = document.cookie.split(';');
//    for (var i = 0; i < ca.length; i++) {
//        var c = ca[i];
//        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
//        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
//    }
//    return null;
//}

//function eraseCookie(name) {
//    createCookie(name, "", -1);
//}
COOKIE_MSID = "MFMSID";
COOKIE_MUEID = "MFMUEID";

function createCookie(cookieName, cookieValue, cookieExpiryMins) {
    if (cookieExpiryMins) {
        var date = new Date();
        date.setTime(date.getTime() + (cookieExpiryMins * 60 * 1000));
        var expires = "; expires=" + date.toUTCString();
    }
    else var expires = "";
    document.cookie = cookieName + "=" + encodeURIComponent(cookieValue) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}
function setExpiryDateOfCookie() {
    var cookieValue = decodeURIComponent(readCookie(COOKIE_MSID));
    eraseCookie(COOKIE_MSID);
    createCookie(COOKIE_MSID, cookieValue, SESSION_TIMEOUT_MINUTES); //30 minutes
    //alert("I was called from backend again");
}

//Click event lnkBackToUiDetails
// function BackToUiDetails() {
// $('[id$=lnkBackToUiDetails]').click();
// }

//$(window).bind('beforeunload', function () {
//  try {
//        alert('window unloading');
//      //clearBrowserWindowIdFromCookie();
//      
//  }  catch (err) {
// }
//});


//function clearBrowserWindowIdFromCookie() {
//    if ($('[id$=hidIsCleanUpRequired]').val() !== 'false') {
//        var cookieValue = decodeURIComponent(readCookie(COOKIE_MSID));
//        var hidBid = $('[id$=hfbid]').val();
//        var aryCookieValue = cookieValue.length > 0 ? cookieValue.split('|') : "";
//        if (aryCookieValue.length == 3) {//no more window is open
//            eraseCookie(COOKIE_MSID);
//            return;
//        }
//        for (var i = 0; i <= aryCookieValue.length - 1; i++) {
//            if (hidBid == aryCookieValue[i]) {
//                aryCookieValue[i] = "";
//            }
//        }
//        cookieValue = "";
//        for (var i = 0; i <= aryCookieValue.length - 1; i++) {
//            if (aryCookieValue[i] !== "") {
//                cookieValue += cookieValue.length == 0 ? aryCookieValue[i] : "|" + aryCookieValue[i]
//            }
//        }
//        eraseCookie(COOKIE_MSID);
//        createCookie(COOKIE_MSID, cookieValue, SESSION_TIMEOUT_MINUTES);
//    }
//}

//PluginTo Increase equalHeight
$.fn.equalHeights = function (px) {
    $(this).each(function () {
        var currentTallest = 0;
        $(this).children().each(function (i) {
            if ($(this).height() > currentTallest) { currentTallest = $(this).height(); }
        });
        if (!px && Number.prototype.pxToEm) currentTallest = currentTallest.pxToEm(); //use ems unless px is specified
        // for ie6, set height since min-height isn't supported
        if ($.browser.msie && $.browser.version == 6.0) { $(this).children().css({ 'height': currentTallest }); }
        $(this).children().css({ 'min-height': currentTallest });
    });
    return this;
};

function ValidateViewExitValues(_Value) {
    var isExists = false; var subStr = ''; var subInnStr = ''; var len = ('Format("').length;
    if (_Value.split('.').length == 3) {
        if ((/^[0-9A-Za-z]+\[\d\]/).test(_Value.split('.')[1])) {
            //validation for command ex commandName[index]
            return false;
        }
        else if (!IsStringContainsSpecialChar(_Value.split('.')[1])) {
            subStr = _Value.split('.')[2].trim();
            if (subStr.length > len) {
                if (subStr.substring(0, len) == 'Format("') {
                    subInnStr = subStr.substring(len, subStr.length);
                    if (subInnStr.length > 2 || (subInnStr.substring(subInnStr.length - 2, subInnStr.length).toLowerCase() == '")')) {
                        return true;
                    }
                }
                else if (!IsStringContainsSpecialChar(_Value.split('.')[2])) {
                    return true;
                }
            }
            else {

                if (!IsStringContainsSpecialChar(_Value.split('.')[2])) {
                    return true;
                }
            }
        }
    }
    return isExists;
}

//lnkBackButton click event
function IdMasterBackBtnClick() {
    try {
        WfCloseAppDivClick();
        SubProcFormChange(false);
    }
    catch (err) {
    }
    ClearHiddenFields(); isCookieCleanUpRequired('false');
}

//Search value inside repeater table
// function doSearch(_type) {
// var searchText = null;
// var targetTable = null;
// if (_type == 1) {
// searchText = $('#txtDbCmdSearch').val();
// targetTable = $('#RptTableDbCmd')[0];
// }
// else if (_type == 2) {
// searchText = $('#txtWsCmdSearch').val();
// targetTable = $('#RptTableWsCmd')[0];
// }
// else if (_type == 3) {
// searchText = $('#txtOdataCmdSearch').val();
// targetTable = $('#RptTableODataCmd')[0];
// }
// if (searchText == null) return;
// try {
// for (var rowIndex = 1; rowIndex < targetTable.rows.length; rowIndex++) {
// var rowData = '';
// rowData = targetTable.rows.item(rowIndex).cells.item(1).textContent.trim();
// if (StringStartWith(rowData.trim(), searchText.trim())) {
// $(targetTable.rows.item(rowIndex)).show();
// }
// else {
// $(targetTable.rows.item(rowIndex)).hide();
// }
// }
// }
// catch (err)
// { }
// }

// Control UniformCSS
function CallUnformCss(_id) {
    if ($(_id).length == 0) {
        return;
    }
    if ($(_id)[0].className == "UseUniformCss") {
        $(_id).uniform();
        $(_id).removeClass("UseUniformCss");
        $(_id).addClass("UsedUniformCss");
    }
    if ($(_id).width() != 0) {
        if ($(_id).width() < 300) {
            $(_id).width($(_id).width() + 10);
        }
        $($(_id)[0].parentElement).width('auto');
    }
    else {
    }
}
//Check string value start with
function StringStartWith(rowData, searchText) {
    var bol = false;
    if (rowData.length > searchText.length) {
        if (rowData.substring(0, searchText.length).toLowerCase() == searchText.toLowerCase()) {
            bol = true;
        }
    }
    else if (rowData.length == searchText.length) {
        if (rowData.toLowerCase() == searchText.toLowerCase()) {
            bol = true;
        }
    }
    else {
    }
    return bol;
}


//Change color of value in intelligence
function changeIntlsColor(_Id) {
    if ($(AppActiveCtrl['SelectedIntls']).length > 0) {
        $(AppActiveCtrl['SelectedIntls']).css('background-color', '');
        $(AppActiveCtrl['SelectedIntls']).removeClass('INTS-SELECTED');
    }
    $(_Id).css('background-color', '#C8C8C8');
    $(_Id).addClass('INTS-SELECTED');
    AppActiveCtrl['SelectedIntls'] = _Id;
}

//Show webservice/odata command intelligence box
function SubWsIntls(_bol, _Id) {
    if (_bol) {
        $("#dialogInnerDiv").html('');
        $("#SubWsIntls").dialog({
            closeOnEscape: false,
            resizable: false,
            autoOpen: true,
            minWidth: 150,
            width: $(_Id).width(),
            draggable: false,
            zIndex: 100000
        });
        $($('#SubWsIntls')[0].previousElementSibling).hide();
        $('#' + $('#SubWsIntls').parent().find('div.ui-dialog-titlebar')[0].children[0].id).remove();
        $('#SubWsIntls').parent().find('div.ui-dialog-titlebar').find('a.ui-state-focus').removeClass('ui-state-focus');
        $('#SubWsIntls').parent().find('div.ui-dialog-titlebar').removeClass('ui-widget-header');
        $('#SubWsIntls').parent().find('div.ui-dialog-titlebar').removeClass('ui-corner-all');
        $('#SubWsIntls').parent().find('div.ui-dialog-titlebar').removeClass('ui-helper-clearfix');
        $('#SubWsIntls').parent().find('div.ui-dialog-content').removeClass('ui-widget-content');
        $('#SubWsIntls').parent().find('div.ui-dialog-content').removeClass('ui-dialog-content');
        var TopPos = $($(_Id)[0].parentElement).offset().top + 20;
        var LeftPos = $($(_Id)[0].parentElement).offset().left;
        try { TopPos = TopPos - ($(window).scrollTop() != undefined ? $(window).scrollTop() : 0); }
        catch (err) { TopPos = $($(_Id)[0].parentElement).offset().top + 20; }
        try { LeftPos = LeftPos - ($(window).scrollLeft() != undefined ? $(window).scrollLeft() : 0); }
        catch (err) { LeftPos = $($(_Id)[0].parentElement).offset().left; }
        $("#SubWsIntls").dialog({ position: [LeftPos, TopPos] });
        $("#SubWsIntls").focus();
        $($('#SubWsIntls')[0].previousElementSibling).hide();
    }
    else {
        if ($("#SubWsIntls").is(':visible')) {
            $("#SubWsIntls").dialog("close");
        }
    }
}

//Bind mouse over event in intelligence
function WsdlParamMouseEvent(_Id) {
    $('#' + _Id).unbind('click');
    $('#' + _Id).unbind('mouseover');
    $('#' + _Id).unbind('mouseout');
    $('#' + _Id).bind('click', function (e) {
        AddSelectedInslsVal(this.id);
    });
    $('#' + _Id).bind('mouseover', function (e) {
        changeIntlsColor('#' + this.id);
    });
    $('#' + _Id).bind('mouseout', function (e) {
        $(this).css('background-color', '');
        $(this).removeClass('INTS-SELECTED');
    });
}

//Select value from intelligence
function AddSelectedInslsVal(_Id) {
    var val = ''; var arr;
    SubWsIntls(false);
    if (AppActiveCtrl['Length'] > 0) {
        arr = AppActiveCtrl['Value'].split('.');
        for (i = 0; i < AppActiveCtrl['Length']; i++) {
            val += (val.length > 0 ? '.' + arr[i] : arr[i]);
        }
        val += (val.length > 0 ? '.' + $('#' + _Id).html() : $('#' + _Id).html());
    }
    else {
        val = $('#' + _Id).html();
    }
    $('#' + AppActiveCtrl['Id']).focus();
    $('#' + AppActiveCtrl['Id']).val(val);
}

// $(document).ready(function () {
// /*----------------------------------------------------------------------*/
// /* Parse the data from an data-attribute of DOM Elements
// /*----------------------------------------------------------------------*/
// $.parseData = function (data, returnArray) {
// if (/^\[(.*)\]$/.test(data)) { //array
// data = data.substr(1, data.length - 2).split(',');
// }
// if (returnArray && !$.isArray(data) && data != null) {
// data = Array(data);
// }
// return data;
// };

// /*----------------------------------------------------------------------*/
// /* Image Preloader
// /* http://engineeredweb.com/blog/09/12/preloading-images-jquery-and-javascript
// /*----------------------------------------------------------------------*/

// // Arguments are image paths relative to the current page.
// $.preload = function () {
// var cache = [],
// args_len = arguments.length;
// for (var i = args_len; i--; ) {
// var cacheImage = document.createElement('img');
// cacheImage.src = arguments[i];
// cache.push(cacheImage);
// }
// };

// /*----------------------------------------------------------------------*/
// /* fadeInSlide by revaxarts.com
// /* Fades out a box and slide it up before it will get removed
// /*----------------------------------------------------------------------*/

// $.fn.fadeInSlide = function (speed, callback) {
// if ($.isFunction(speed)) callback = speed;
// if (!speed) speed = 200;
// if (!callback) callback = function () { };
// this.each(function () {

// var $this = $(this);
// $this.fadeTo(speed / 2, 1).slideDown(speed / 2, function () {
// callback();
// });
// });
// return this;
// };
// /*----------------------------------------------------------------------*/
// /* fadeOutSlide by revaxarts.com
// /* Fades out a box and slide it up before it will get removed
// /*----------------------------------------------------------------------*/

// $.fn.fadeOutSlide = function (speed, callback) {
// if ($.isFunction(speed)) callback = speed;
// if (!speed) speed = 200;
// if (!callback) callback = function () { };
// this.each(function () {

// var $this = $(this);
// $this.fadeTo(speed / 2, 0).slideUp(speed / 2, function () {
// $this.remove();
// callback();
// });
// });
// return this;
// };
// /*----------------------------------------------------------------------*/
// /* textFadeOut by revaxarts.com
// /* Fades out a box and slide it up before it will get removed
// /*----------------------------------------------------------------------*/

// $.fn.textFadeOut = function (text, delay, callback) {
// if (!text) return false;
// if ($.isFunction(delay)) callback = delay;
// if (!delay) delay = 2000;
// if (!callback) callback = function () { };
// this.each(function () {

// var $this = $(this);
// $this.stop().text(text).show().delay(delay).fadeOut(1000, function () {
// $this.text('').show();
// callback();
// })
// });
// return this;
// };
// /*----------------------------------------------------------------------*/
// /* leadingZero by revaxarts.com
// /* adds a leding zero if necessary
// /*----------------------------------------------------------------------*/

// $.leadingZero = function (value) {
// value = parseInt(value, 10);
// if (!isNaN(value)) {
// (value < 10) ? value = '0' + value : value;
// }
// return value;
// };


// if (!String.prototype.startsWith) {
// Object.defineProperty(String.prototype, 'startsWith', {
// enumerable: false,
// configurable: false,
// writable: false,
// value: function (searchString, position) {
// position = position || 0;
// return this.indexOf(searchString, position) === position;
// }
// });
// }

// });
/*----------------------------------------------------------------------*/
/* wl_Alert v 1.1 by revaxarts.com
/* description: Handles alert boxes
/* dependency: jquery UI Slider, fadeOutSlide plugin
/*----------------------------------------------------------------------*/


// $.fn.wl_Alert = function (method) {
// var args = arguments;
// return this.each(function () {

// var $this = $(this);


// if ($.fn.wl_Alert.methods[method]) {
// return $.fn.wl_Alert.methods[method].apply(this, Array.prototype.slice.call(args, 1));
// } else if (typeof method === 'object' || !method) {
// if ($this.data('wl_Alert')) {
// var opts = $.extend({}, $this.data('wl_Alert'), method);
// } else {

// var opts = $.extend({}, $.fn.wl_Alert.defaults, method, $this.data());
// }
// } else {
// $.error('Method "' + method + '" does not exist');
// }


// if (!$this.data('wl_Alert')) {

// $this.data('wl_Alert', {});

// //bind click events to hide alert box
// $this.bind('click.wl_Alert', function (event) {
// event.preventDefault();

// //Don't hide if it is sticky
// if (!$this.data('wl_Alert').sticky) {
// $.fn.wl_Alert.methods.close.call($this[0]);
// }

// //prevent hiding the box if an inline link is clicked
// }).find('a').bind('click.wl_Alert', function (event) {
// event.stopPropagation();
// });
// } else {

// }
// //show it if it is hidden
// if ($this.is(':hidden')) {
// $this.slideDown(opts.speed / 2);
// }

// if (opts) $.extend($this.data('wl_Alert'), opts);
// });

// };

// $.fn.wl_Alert.defaults = {
// speed: 500,
// sticky: false,
// onBeforeClose: function (element) { },
// onClose: function (element) { }
// };
//$.fn.wl_Alert.version = '1.1';


// $.fn.wl_Alert.methods = {
// close: function () {
// var $this = $(this),
// opts = $this.data('wl_Alert');
// //call callback and stop if it returns false
// if (opts.onBeforeClose.call(this, $this) === false) {
// return false;
// };
// //fadeout and call an callback
// $this.fadeOutSlide(opts.speed, function () {
// opts.onClose.call($this[0], $this);
// });
// },
// set: function () {
// var $this = $(this),
// options = {};
// if (typeof arguments[0] === 'object') {
// options = arguments[0];
// } else if (arguments[0] && arguments[1] !== undefined) {
// options[arguments[0]] = arguments[1];
// }
// $.each(options, function (key, value) {
// if ($.fn.wl_Alert.defaults[key] !== undefined || $.fn.wl_Alert.defaults[key] == null) {
// $this.data('wl_Alert')[key] = value;
// } else {
// $.error('Key "' + key + '" is not defined');
// }
// });

// }
// };

//to create an alert box on the fly
// $.wl_Alert = function (text, cssclass, insert, after, options) {
// //go thru all
// $('div.alert').each(function () {
// var _this = $(this);
// //...and hide if one with the same text is allready set
// if (_this.text() == text) {
// _this.slideUp($.fn.wl_Alert.defaults.speed);
// }
// });

// //create a new DOM element and inject it
// var al = $('<div class="alert ' + cssclass + '">' + text + '</div>').hide();
// (after) ? al.appendTo(insert).wl_Alert(options) : al.prependTo(insert).wl_Alert(options);

// //return the element
// return al;
// };



/*----------------------------------------------------------------------*/
/* wl_Date v 1.0 by revaxarts.com
/* description: extends the Datepicker
/* dependency: jQuery Datepicker, mousewheel plugin
/*----------------------------------------------------------------------*/


// $.fn.wl_Date = function (method) {

// var args = arguments;
// return this.each(function () {

// var $this = $(this);


// if ($.fn.wl_Date.methods[method]) {
// return $.fn.wl_Date.methods[method].apply(this, Array.prototype.slice.call(args, 1));
// } else if (typeof method === 'object' || !method) {
// if ($this.data('wl_Date')) {
// var opts = $.extend({}, $this.data('wl_Date'), method);
// } else {
// var opts = $.extend({}, $.fn.wl_Date.defaults, method, $this.data());
// }
// } else {
// try {
// return $this.datepicker(method, args[1], args[2]);
// } catch (e) {
// $.error('Method "' + method + '" does not exist');
// }
// }


// if (!$this.data('wl_Date')) {

// $this.data('wl_Date', {});






// //call the jQuery UI datepicker plugin
// $this.datepicker(opts);

// //bind a mousewheel event to the input field
// $this.bind('mousewheel.wl_Date', function (event, delta) {
// if (opts.mousewheel) {
// event.preventDefault();
// //delta must be 1 or -1 (different on macs and with shiftkey pressed)
// delta = (delta < 0) ? -1 : 1;

// //shift key is pressed
// if (event.shiftKey) {
// var _date = $this.datepicker('getDate');
// //new delta is the current month day count (month in days)
// delta *= new Date(_date.getFullYear(), _date.getMonth() + 1, 0).getDate();
// }
// //call the method
// $.fn.wl_Date.methods.changeDay.call($this[0], delta);
// }
// });


// //value is set and has to get translated (self-explanatory) 
// if (opts.value) {
// var today = new Date().getTime();
// switch (opts.value) {
// case 'now':
// case 'today':
// $this.datepicker('setDate', new Date());
// break;
// case 'next':
// case 'tomorrow':
// $this.datepicker('setDate', new Date(today + 864e5 * 1));
// break;
// case 'prev':
// case 'yesterday':
// $this.datepicker('setDate', new Date(today + 864e5 * -1));
// break;
// default:
// //if a valid number add them as days to the date field
// if (!isNaN(opts.value)) $this.datepicker('setDate', new Date(today + 864e5 * opts.value));
// }

// }
// //disable if set
// if (opts.disabled) {
// $.fn.wl_Date.methods.disable.call(this);
// }
// } else {

// }

// if (opts) $.extend($this.data('wl_Date'), opts);
// });

// };

// $.fn.wl_Date.defaults = {
// value: null,
// mousewheel: true
// };
//$.fn.wl_Date.version = '1.0';


// $.fn.wl_Date.methods = {
// disable: function () {
// var $this = $(this);
// //disable the datepicker
// $this.datepicker('disable');
// $this.data('wl_Date').disabled = true;
// },
// enable: function () {
// var $this = $(this);
// //enable the datepicker
// $this.datepicker('enable');
// $this.data('wl_Date').disabled = false;
// },
// next: function () {
// //select next day
// $.fn.wl_Date.methods.changeDay.call(this, 1);
// },
// prev: function () {
// //select previous day
// $.fn.wl_Date.methods.changeDay.call(this, -1);
// },
// changeDay: function (delta) {
// var $this = $(this),
// _current = $this.datepicker('getDate') || new Date();
// //set day to currentday + delta
// _current.setDate(_current.getDate() + delta);
// $this.datepicker('setDate', _current);
// },
// set: function () {
// var $this = $(this),
// options = {};
// if (typeof arguments[0] === 'object') {
// options = arguments[0];
// } else if (arguments[0] && arguments[1] !== undefined) {
// options[arguments[0]] = arguments[1];
// }
// $.each(options, function (key, value) {
// if ($.fn.wl_Date.defaults[key] !== undefined || $.fn.wl_Date.defaults[key] == null) {
// $this.data('wl_Date')[key] = value;
// } else {
// $.error('Key "' + key + '" is not defined');
// }
// });

// }
// };



/*----------------------------------------------------------------------*/
/* wl_Dialog v 1.1 by revaxarts.com
/* description: handles alert boxes, prompt boxes and confirm boxes and
/*                message boxes
/*                contains 4 plugins
/* dependency: jquery UI Dialog
/*----------------------------------------------------------------------*/


/*----------------------------------------------------------------------*/
/* Confirm Dialog
/* like the native confirm method
/*----------------------------------------------------------------------*/
// $.confirm = function (text, callback, cancelcallback) {

// var options = $.extend(true, {}, $.alert.defaults, $.confirm.defaults);

// //nativ behaviour
// if (options.nativ) {
// if (result = confirm(unescape(text))) {
// if ($.isFunction(callback)) callback.call(this);
// } else {
// if ($.isFunction(cancelcallback)) cancelcallback.call(this);
// }
// return;
// }

// //the callbackfunction
// var cb = function () {
// if ($.isFunction(callback)) callback.call(this);
// $(this).dialog('close');
// $('#wl_dialog').remove();
// },

// //the callbackfunction on cancel
// ccb = function () {
// if ($.isFunction(cancelcallback)) cancelcallback.call(this);
// $(this).dialog('close');
// $('#wl_dialog').remove();
// };

// //set some options
// options = $.extend({}, {
// buttons: [{
// text: options.text.ok,
// click: cb

// }, {
// text: options.text.cancel,
// click: ccb

// }]
// }, options);

// //use the dialog
// return $.alert(unescape(text), options);
// };

// $.confirm.defaults = {
// text: {
// header: 'Please confirm',
// ok: 'Yes',
// cancel: 'No'
// }
// };

/*----------------------------------------------------------------------*/
/* Prompt Dialog
/* like the native prompt method
/*----------------------------------------------------------------------*/

// $.prompt = function (text, value, callback, cancelcallback) {

// var options = $.extend(true, {}, $.alert.defaults, $.prompt.defaults);

// //nativ behaviour
// if (options.nativ) {
// var val = prompt(unescape($.trim(text)), unescape(value));
// if ($.isFunction(callback) && val !== null) {
// callback.call(this, val);
// } else {
// if ($.isFunction(cancelcallback)) cancelcallback.call(this);
// }
// return;
// }

// //the callbackfunction
// var cb = function (value) {
// if ($.isFunction(callback)) callback.call(this, value);
// $(this).dialog('close');
// $('#wl_dialog').remove();
// },

// //the callbackfunction on cancel
// ccb = function () {
// if ($.isFunction(cancelcallback)) cancelcallback.call(this);
// $(this).dialog('close');
// $('#wl_dialog').remove();
// };

// //set some options
// options = $.extend({}, {
// buttons: [{
// text: options.text.ok,
// click: function () {
// cb.call(this, $('#wl_promptinputfield').val());
// }
// }, {
// text: options.text.cancel,
// click: ccb
// }],
// open: function () {
// $('#wl_promptinputfield').focus().select();
// $('#wl_promptinputfield').uniform();
// $('#wl_promptinputform').bind('submit', function (event) {
// event.preventDefault();
// cb.call(this, $('#wl_promptinputfield').val());
// $(this).parent().dialog('close');
// $('#wl_dialog').remove();
// });

// }
// }, options);

// //use the dialog
// return $.alert('<p>' + unescape(text) + '</p><form id="wl_promptinputform"><input id="wl_promptinputfield" name="wl_promptinputfield" value="' + unescape(value) + '"></form>', options);
// };

// $.prompt.defaults = {
// text: {
// header: 'Please prompt',
// ok: 'OK',
// cancel: 'Cancel'
// }
// };


// /*----------------------------------------------------------------------*/
// /* Alert Dialog
// /* like the native alert method
// /*----------------------------------------------------------------------*/

// $.alert = function (content, options) {


// //if no options it is a normal dialog
// if (!options) {
// var options = $.extend(true, {}, {
// buttons: [{
// text: $.alert.defaults.text.ok,
// click: function () {
// $(this).dialog('close');
// //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove();
// $('#wl_dialog_alert').prev().find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
// $('#wl_dialog_alert').remove();
// }
// }],
// open: function () { // open event handler
// $(this) // the element being dialogged
// .parent() // get the dialog widget element
// .find(".ui-dialog-titlebar-close") // find the close button for this dialog
// .hide(); // hide it
// },
// beforeClose: function (event, ui) {
// removeDialogImage();
// //console.log('Event Fire');
// }
// }, $.alert.defaults);
// }

// //nativ behaviour
// if (options.nativ) {
// alert(content);
// return;
// }

// //create a container
// var container = $('<div/>', {
// id: 'wl_dialog_alert'
// }).appendTo('body'); //wl_dialog 11/20/2012.Alert will have an image aginst the header.so to make this unique,making this change.

// //set a header
// if (options.text.header) {
// container.attr('title', options.text.header);
// }

// //fill the container
// container.html(content.replace(/\n/g, '<br>'));
// //display the dialog
// container.dialog(options);
// return {
// close: function (callback) {
// container.dialog('close');
// container.remove();
// if ($.isFunction(callback)) callback.call(this);
// },
// setHeader: function (text) {
// this.set('title', text);
// },
// setBody: function (html) {
// container.html(html);
// },
// set: function (option, value) {
// container.dialog("option", option, value);
// }
// }


// };


// $.alert.defaults = {
// nativ: false,
// resizable: false,
// modal: true,
// text: {
// header: 'Notification',
// ok: 'OK'
// }
// };


// $.alert.Error = {
// nativ: false,
// resizable: false,
// modal: true,
// text: {
// header: 'Error',
// ok: 'OK'
// },
// buttons: [{
// text: $.alert.defaults.text.ok,
// click: function () {
// $(this).dialog('close');
// //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove();
// $('#wl_dialog_alert').prev().find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
// $('#wl_dialog_alert').remove();
// }
// }],
// open: function () { // open event handler
// $(this) // the element being dialogged
// .parent() // get the dialog widget element
// .find(".ui-dialog-titlebar-close") // find the close button for this dialog
// .hide(); // hide it
// },
// beforeClose: function (event, ui) {
// removeDialogImage();
// //console.log('Event Fire');
// }
// };

// $.alert.Information = {
// nativ: false,
// resizable: false,
// modal: true,
// text: {
// header: 'Notification',
// ok: 'OK'
// },
// buttons: [{
// text: $.alert.defaults.text.ok,
// click: function () {
// $(this).dialog('close');
// //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove();
// $('#wl_dialog_alert').prev().find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
// $('#wl_dialog_alert').remove();
// }
// }],
// open: function () { // open event handler
// $(this) // the element being dialogged
// .parent() // get the dialog widget element
// .find(".ui-dialog-titlebar-close") // find the close button for this dialog
// .hide(); // hide it
// },
// beforeClose: function (event, ui) {
// removeDialogImage();
// //console.log('Event Fire');
// }
// };

// /*----------------------------------------------------------------------*/
// /* Message Function
// /*----------------------------------------------------------------------*/


// $.msg = function (content, options) {


// //get the options
// var options = $.extend({}, $.msg.defaults, options);

// var container = $('#wl_msg'),
// msgbox;

// //the container doen't exists => create it
// if (!container.length) {
// container = $('<div/>', {
// id: 'wl_msg'
// }).appendTo('body').data('msgcount', 0);
// var topoffset = parseInt(container.css('top'), 10);

// //bind some events to it
// container.bind('mouseenter', function () {
// container.data('pause', true);
// }).bind('mouseleave', function () {
// container.data('pause', false);
// });
// container.delegate('.msg-close', 'click', function () {
// container.data('pause', false);
// close($(this).parent());
// });
// container.delegate('.msg-box-close', 'click', function () {
// container.fadeOutSlide(options.fadeTime);
// });

// //bind the scroll event
// $(window).unbind('scroll.wl_msg').bind('scroll.wl_msg', function () {
// var pos = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
// if (pos > topoffset) {
// (window.navigator.standalone === undefined) ? container.css({
// position: 'fixed',
// top: 10
// }) : container.css({
// top: pos + 10
// });
// } else {
// (window.navigator.standalone === undefined) ? container.removeAttr('style') : container.css({
// top: topoffset
// });
// }
// }).trigger('scroll.wl_msg');
// }

// //stop if no content is set
// if (!content) return false;

// //count of displayed messages
// var count = container.data('msgcount');

// function getHTML(content, headline) {
// return '<div class="msg-box"><h3>' + (headline || '') + '</h3><a class="msg-close">close</a><div class="msg-content">' + content.replace('\n', '<br>') + '</div></div>';
// }

// function create() {
// msgbox = $(getHTML(content, options.header)),
// closeall = $('.msg-box-close');

// //we have some messages allready
// if (count) {

// //No close all button
// if (!closeall.length) {
// msgbox.appendTo(container);
// $('<div class="msg-box-close">close all</div>').appendTo(container).fadeInSlide(options.fadeTime);

// //Close all button
// } else {
// msgbox.insertBefore(closeall);
// }

// //first message
// } else {
// msgbox.appendTo(container);
// }

// //fade it in nicely
// msgbox.fadeInSlide(options.fadeTime);

// //add the count of the messages to the container
// container.data('msgcount', ++count);

// //outclose it only if it's not sticky
// if (!options.sticky) {
// close(msgbox, options.live);
// }
// }

// function close(item, delay, callback) {
// if ($.isFunction(delay)) {
// callback = delay;
// delay = 0;
// } else if (!delay) {
// delay = 0;
// }
// setTimeout(function () {

// //if the mouse isn't over the container
// if (!container.data('pause')) {
// item.fadeOutSlide(options.fadeTime, function () {
// var count = $('.msg-box').length;
// if (count < 2 && $('.msg-box-close').length) {
// $('.msg-box-close').fadeOutSlide(options.fadeTime);
// }
// container.data('msgcount', count);
// if ($.isFunction(callback)) callback.call(item);
// })
// //try again...
// } else {
// close(item, delay);
// }

// }, delay);
// }

// //create the messsage
// create();

// return {
// close: function (callback) {
// close(msgbox, callback);
// },
// setHeader: function (text) {
// msgbox.find('h3').eq(0).text(text);
// },
// setBody: function (html) {
// msgbox.find('.msg-content').eq(0).html(html);
// },
// closeAll: function (callback) {
// container.fadeOutSlide(options.fadeTime, function () {
// if ($.isFunction(callback)) callback.call(this);
// });
// }
// }

// };

// $.msg.defaults = {
// header: null,
// live: 5000,
// topoffset: 90,
// fadeTime: 500,
// sticky: false
// };



// $.modalPopUp = function (text, value, html) {

// var options = $.extend(true, {}, $.alert.defaults);

// //nativ behaviour
// if (options.nativ) {
// var val = prompt(unescape($.trim(text)), unescape(value));
// if ($.isFunction(callback) && val !== null) {
// callback.call(this, val);
// } else {
// if ($.isFunction(cancelcallback)) cancelcallback.call(this);
// }
// return;
// }

// //use the dialog
// return $.alert('<p>' + unescape(text) + '</p>' + html, options);
// };

// $.modalPopUp.defaults = {
// sticky: false,
// autoOpen: false,
// height: 300,
// width: 350
// };

/*-----------------------------------------------------------------------*/
/*My custom script*/
/*-----------------------------------------------------------------------*/

function setDropDownListWidthOnChange(dropdown) {
    var elem = dropdown;
    var divTag = elem.parent("div");
    var spanTag = elem.siblings("span");
    var selected = elem.find(":selected:first");
    if (selected.length == 0) {
        selected = elem.find("option:first");
    }
    spanTag.html(selected.html());



    var elemWidth = 0;
    if (divTag.outerWidth(false) == 10) //width of the div tag if elem initial width is zero
    {
        elemWidth = divTag.outerWidth(false) + spanTag.outerWidth(false) + 25 + 2 + 10;

    } else {
        elemWidth = spanTag.outerWidth(false) + 25 + 2 + 10;
    }
    elem.width(elemWidth);
}
//Modal pop up action buttons are center aligned
//we have to change the width of div according to the 
//no of buttons in the action div.
//var POP_UP_ACTION_DIV_2 = 50; //for 2 buttons
//var POP_UP_ACTION_DIV_3 = 77; //for 3 buttons
//var POP_UP_ACTION_DIV_MOZ_2 = 60; //for 2 buttons
//var POP_UP_ACTION_DIV_MOZ_3 = 90; //for 3 buttons

//function showModalPopUp(divToOpen, popUpTitle, popUpWidth, resizable) {
//    if (resizable || resizable == undefined) {
//        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, modal: true });
//    }
//    else {
//        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, resizable: false, modal: true });
//    }
//    if ($('#' + divToOpen)) {
//        ($('#' + divToOpen).parent()).find('.ui-dialog-titlebar > a.ui-dialog-titlebar-close').show();
//    }
//}

function xferBack() {
    document.forms[0].submit();
    return true;
}
// function AspDialogCloseCBOption(callBack, context) {
// this.cb = callBack;
// this.ctxt = context;
// }

// function showModalPopUp(divToOpen, popUpTitle, popUpWidth, resizable, closeCBOptions) {
// if (resizable == undefined) resizable = false;
// $("#" + divToOpen).aspdialog({
// modal: true,
// resizable: resizable,
// title: popUpTitle,
// width: popUpWidth
// });
// }
// function showModalPopUpWithOutHeader(divToOpen, popUpTitle, popUpWidth, resizable) {
// if (resizable || resizable == undefined) {
// $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, modal: true });
// }
// else {
// $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, resizable: false, modal: true });
// }
// if ($('#' + divToOpen)) {
// ($('#' + divToOpen).parent()).find('.ui-dialog-titlebar > .ui-dialog-titlebar-close').hide();
// }
// }
// function closeModalPopUp(divIdToClose) {
// $('#' + divIdToClose).dialog('close');
// }
// function showAlert(message, divIdToShowMsgWithHash) {
// $.wl_Alert(message, 'info', divIdToShowMsgWithHash);
// }
// function showWait() {
// $("#divWaitBox").dialog({
// modal: true,
// width: 0,
// height: 0,
// closeOnEscape: false,
// resizable: false,
// autoOpen: true,
// open: function () {
// //$(".ui-dialog-titlebar-close", $(this).parent()).hide(); //hides the little 'x' button
// $("#divWaitBox").siblings(".ui-dialog-titlebar").hide();
// }
// });
// }
// function hideWait() {
// $("#divWaitBox").dialog("close");
// $(".ui-dialog-titlebar").show();
// }

// function hideHTMLElements(commaSeparatedIdsOfElements) {
// var aryOfElements = commaSeparatedIdsOfElements.toString().split(',');
// for (var i = 0; i < aryOfElements.length; i++) {
// $('#' + aryOfElements[0]).hide();
// }
// }

// Repeater =
// {
// ADD_USER_USER_DETAIL: 0
// }

// function showHTMLElements(commaSeparatedIdsOfElements) {
// var aryOfElements = commaSeparatedIdsOfElements.toString().split(',');
// for (var i = 0; i < aryOfElements.length; i++) {
// $('#' + aryOfElements[0]).show();
// }
//}

// function spinDiv(_bol) {
// if (_bol) {
// showModalPopUp('spinDiv', '', 0);
// }
// else {
// $('#spinDiv').dialog('close');
// }
// }

function showWaitModal() {
//    $("#divWaitBox").dialog({
//        modal: true,
//        closeOnEscape: false,
//        resizable: false,
//        autoOpen: true
//    });
    showModalPopUp('divWaitBox',"",56,false,null,null,null);
    if ($('#divWaitBox').length > 0) {
        if ($($('#divWaitBox')[0].parentElement).length > 0) {
            $($('#divWaitBox')[0].parentElement).width(56);
            $($('#divWaitBox')[0].parentElement).height(56);
            setModalPosition($('#divWaitBox').parent());
            $($('#divWaitBox')[0].previousElementSibling).hide();
            $($('#divWaitBox')[0].parentElement).addClass("waitBoxCorner");
        }
    }
    //$("#divWaitBox").dialog({
    //        modal: true,
    //        closeOnEscape: false,
    //        resizable: false,
    //        autoOpen: true,
    //        width:56,
    //        height:56,
    //    });
}

function hideWaitModal() {
    $("#divWaitBox").dialog("close");
}

// DialogType = {
// Error: 0,
// Warning: 1,
// Info: 3
// }

// function showDialogImage(type, divToPrependImg) {

// var divToPrependImage = getDivToPrependImage(divToPrependImg);
// if (divToPrependImage) {
// if ($.browser.msie) {

// switch (type) {

// case DialogType.Error:
// divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogError\" />');
// //divToPrependImage.prepend('<div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/dialog_error.png"/></div>');
// getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogError");
// break;
// case DialogType.Warning:
// divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogWarning\" />');
// //divToPrependImage.prepend('<div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/dialog_info.png"/></div>');
// getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogWarning");
// break;
// case DialogType.Info:
// divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogInfo\" />');
// //divToPrependImage.prepend('<div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/dialog_info.png"/></div>');
// getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogInfo");
// break;
// }
// }
// else if ($.browser.webkit) {

// switch (type) {

// case DialogType.Error:
// divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogError\" />');
// getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogError dialogErrorWebKit");
// break;
// case DialogType.Warning:
// divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogWarning\" />');
// getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogWarning dialogWarningWebKit");
// break;
// case DialogType.Info:
// divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogInfo\" />');
// getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogInfo dialogInfoWebKit");
// break;
// }
// }
// else {

// switch (type) {

// case DialogType.Error:
// divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogError\" />');
// break;
// case DialogType.Warning:
// divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogWarning\" />');
// break;
// case DialogType.Info:
// divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogInfo\" />');
// break;
// }
// }


// }
// }
// function getDivToPrependImage(divToPrependImage) {
// if (divToPrependImage == undefined) {
// return $('#wl_dialog_alert').prev();
// }
// else {
// return $('#' + divToPrependImage).prev();
// }
// }
// function removeDialogImage(divToRemoveImage) {
// //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
// if (divToRemoveImage == undefined) {
// $('#wl_dialog_alert').prev().find($('#dialogImageId')).remove();
// }
// else {
// $('#' + divToRemoveImage).prev().find($('#dialogImageId')).remove();
// }

// }
// function getDialogImageSpan(divToPrependImage) {
// //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
// if (divToPrependImage == undefined) {
// return $('#wl_dialog_alert').prev().find('#dialogImageId');
// }
// else {
// return $('#' + divToPrependImage).prev().find('#dialogImageId');
// }

// }
function setModalPosition(divToSet) {

    PgDim = getPageSize();
    var vleft = ((PgDim[2] - divToSet.width()) / 2);
    var vtop = (((PgDim[3]) - divToSet.height()) / 2) + $(document).scrollTop();
    divToSet.offset({ top: vtop, left: vleft });
}

function st(obj) {
    return o(obj).style;
}


function getPageSize() {
    var xScroll, yScroll;
    if (window.innerHeight && window.scrollMaxY) {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    }
    // all but Explorer Mac 
    else if (document.body.scrollHeight > document.body.offsetHeight) {
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    }
    // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari 
    else {
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }

    var windowWidth, windowHeight;
    // all except Explorer
    if (self.innerHeight) {
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    }
    // Explorer 6 Strict Mode 
    else if (document.documentElement && document.documentElement.clientHeight) {
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    }
    // other Explorers 
    else if (document.body) {
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }
    if (yScroll < windowHeight) pageHeight = windowHeight;
    else pageHeight = yScroll;
    // for small pages with total width less then width of the viewport
    if (xScroll < windowWidth) {
        pageWidth = windowWidth;
    }
    else {
        pageWidth = xScroll;
    }
    arrayPageSize = new Array(pageWidth, pageHeight, windowWidth, windowHeight)
    return arrayPageSize;


}



// function o(obj) {
// if (typeof obj == 'object') {
// return obj;
// }
// else {
// return document.getElementById(obj);
// }
// }

// function stopDefualtOfDateTxtBox(txtBox) {
// if (txtBox) {
// txtBox.addEventListener(
// 'keydown', stopDefAction, false
// );
// }
// }

// function rptTableCellClick(cell, repeater) {
// var row = $(cell).parent();
// rptRowClick(row[0], repeater);
// }
// function rptRowClick(row, repeater) {
// processRowClick(row);
// }
// function processRowClick(row) {
// var hidUsedAfterPostBack = $('[id$=hidRowValuesUsedAfterPostBack]');
// var strValuesReqAfterPostBack = '';
// $.each(row.children, function () {
// if (this.title == "id") {
// var td = this;
// $.each(td.children, function () {
// if (strValuesReqAfterPostBack.length == 0)
// strValuesReqAfterPostBack += ($.trim(this.innerHTML));
// else
// strValuesReqAfterPostBack += ($.trim(';' + this.innerHTML));
// })
// hidUsedAfterPostBack.val(strValuesReqAfterPostBack);
// }
// });
// UpdateRowClick();
// }
// function UpdateRowClick() {
// $('[id$=btnRowClickPostBack]').click();
// }
// function rptHeaderClickForSorting(header, hidFieldUsedForInfo) {
// UpdateRowClick();
// }
// //If the button to be clicked is in the pop up then pass the id of that button
// function clickButtonForPostBack(button) {
// if (button) {
// $('[id$=' + button + ']').click();
// }
// else {
// $('[id$=btnScriptClickPostBack]').click();
// }
// }
// function setWidthOfDropDown() {
// $("select").each(function (i) {
// var parentDiv = this.parent("div");
// if (parentDiv && parentDiv != undefined) {
// this.width(parentDiv.outerWidth(false));
// }
// })
// }
// MessageOption = {
// Error: 0,
// Warning: 1,
// Info: 3
// }
// function showMessage(message, optionForScript, dialogImageType) {
// var strOption = '';
// switch (optionForScript) {
// case DialogType.Error:
// strOption = '$.alert.Error';
// break;
// case DialogType.Info:
// strOption = '$.alert.Information';
// break;
// }
// $.alert(message, strOption); showDialogImage(dialogImageType);
// }

// function showConfirmation(message) {

// var blnConfirm = confirm(message);
// if (blnConfirm) {
// isCookieCleanUpRequired('false');
// }
// return blnConfirm;
// }


//right now it is used only changing the divs of modal pop up
//action divs.So do the firefox related changes here only.
//But change the name of function after testing.
function changeWidthOfDiv(divId, iWidth, inPercent) {
    var divToChange = $('#' + divId);
    if ($.browser.mozilla) {
        if (iWidth === POP_UP_ACTION_DIV_2) iWidth = POP_UP_ACTION_DIV_MOZ_2;
        else if (iWidth === POP_UP_ACTION_DIV_3) iWidth = POP_UP_ACTION_DIV_MOZ_3;
    }
    if (inPercent) {
        $(divToChange).width(iWidth.toString() + "%");
    }
    else {
        $(divToChange).width(iWidth);
    }
}
function changeWidthOfActionDiv(divId, iWidth, inPercent) {
    var divToChange = $('#' + divId);
    if (inPercent) {
        $(divToChange).width(iWidth.toString() + "%");
    }
    else {
        $(divToChange).width(iWidth);
    }
}

function cancelEventBubbling(e) {
    if (!e)
        e = window.event;

    //IE9 & Other Browsers
    if (e.stopPropagation) {
        e.stopPropagation();
    }
    //IE8 and Lower
    else {
        e.cancelBubble = true;
    }
}
function childHandler(e) {
    if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();

    alert('child clicked');

};
function uncheckUniformedCheckBox(checkboxObject) {
    $(checkboxObject).removeAttr('checked');
    var containerOfChkBox = $(checkboxObject).parent();
    if ($(containerOfChkBox).is("span")) {
        $(containerOfChkBox).removeAttr("class");
    }
}
function checkUniformedCheckBox(checkboxObject) {
    $(checkboxObject).attr('checked', 'checked');
    var containerOfChkBox = $(checkboxObject).parent();
    if ($(containerOfChkBox).is("span")) {
        $(containerOfChkBox).attr("class", "checked");
    }
}
function makeSelectUniformOnPostBack() {
    $("select").each(function (index) {
        var parentTag = $(this).parent();
        //only make only those select uniform
        // which are not already uniformed
        if (!($(parentTag).hasClass("selector"))) {
            $(this).uniform();
        }
    });
}
(function ($) {
    $.fn.inlineStyle = function (prop) {
         var styles = this.attr("style"),
             value;
         styles && styles.split(";").forEach(function (e) {
             var style = e.split(":");
             if ($.trim(style[0]) === prop) {
                 value = style[1];           
             }                    
         });   
         return value;
    };
}(jQuery));
(function ($) {
    $.uniform = {
        options: {
            selectClass: 'selector',
            radioClass: 'radio',
            checkboxClass: 'checker',
            fileClass: 'uploader',
            filenameClass: 'filename',
            fileBtnClass: 'action',
            fileDefaultText: 'No file selected',
            fileBtnText: 'Choose File',
            checkedClass: 'checked',
            focusClass: 'focus',
            disabledClass: 'disabled',
            buttonClass: 'button',
            activeClass: 'active',
            hoverClass: 'hover',
            useID: true,
            idPrefix: 'uniform',
            resetSelector: false,
            autoHide: true
        },
        elements: []
    };

    if ($.browser.msie && $.browser.version < 7) {
        $.support.selectOpacity = false;
    } else {
        $.support.selectOpacity = true;
    }

    $.fn.uniform = function (options) {

        options = $.extend($.uniform.options, options);

        var el = this;
        //code for specifying a reset button
        if (options.resetSelector != false) {
            $(options.resetSelector).mouseup(function () {
                function resetThis() {
                    $.uniform.update(el);
                }
                setTimeout(resetThis, 10);
            });
        }

        function doInput(elem) {
            $el = $(elem);
            $el.addClass($el.attr("type"));
            storeElement(elem);
        }

        function doTextarea(elem) {
            $(elem).addClass("uniform");
            storeElement(elem);
        }

        function doButton(elem) {
            var $el = $(elem);
            if (!$el.is(":submit")) {

                var divTag = $("<div>"),
          spanTag = $("<span>");

                divTag.addClass(options.buttonClass);

                if (options.useID && $el.attr("id") != "") divTag.attr("id", options.idPrefix + "-" + $el.attr("id"));

                var btnText;

                if ($el.is("a") || $el.is("button")) {
                    btnText = $el.text();
                } else if ($el.is(":submit") || $el.is(":reset") || $el.is("input[type=button]")) {
                    btnText = $el.attr("value");
                }

                btnText = btnText == "" ? $el.is(":reset") ? "Reset" : "Submit" : btnText;

                spanTag.html(btnText);

                $el.css("opacity", 0);
                $el.wrap(divTag);
                $el.wrap(spanTag);

                //redefine variables
                divTag = $el.closest("div");
                spanTag = $el.closest("span");

                if ($el.is(":disabled")) divTag.addClass(options.disabledClass);

                divTag.bind({
                    "mouseenter.uniform": function () {
                        divTag.addClass(options.hoverClass);
                    },
                    "mouseleave.uniform": function () {
                        divTag.removeClass(options.hoverClass);
                        divTag.removeClass(options.activeClass);
                    },
                    "mousedown.uniform touchbegin.uniform": function () {
                        divTag.addClass(options.activeClass);
                    },
                    "mouseup.uniform touchend.uniform": function () {
                        divTag.removeClass(options.activeClass);
                    },
                    "click.uniform touchend.uniform": function (e) {
                        if ($(e.target).is("span") || $(e.target).is("div")) {
                            if (elem[0].dispatchEvent) {
                                var ev = document.createEvent('MouseEvents');
                                ev.initEvent('click', true, true);
                                elem[0].dispatchEvent(ev);
                            } else {
                                elem[0].click();
                            }
                        }
                    }
                });

                elem.bind({
                    "focus.uniform": function () {
                        divTag.addClass(options.focusClass);
                    },
                    "blur.uniform": function () {
                        divTag.removeClass(options.focusClass);
                    }
                });

                $.uniform.noSelect(divTag);
                storeElement(elem);
            }
        }

        function doSelect(elem) {
            var $el = $(elem);

            var divTag = $('<div />'),
          spanTag = $('<span />');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.selectClass);

            if (options.useID && elem.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
            }

            var selected = elem.find(":selected:first");
            if (selected.length == 0) {
                selected = elem.find("option:first");
            }
            spanTag.html(selected.html());

            elem.css('opacity', 0);
            elem.wrap(divTag);
            elem.before(spanTag);

            //redefine variables
            divTag = elem.parent("div");
            spanTag = elem.siblings("span");

            elem.bind({
                "change.uniform": function () {
                    spanTag.text(elem.find(":selected").html());
                    divTag.removeClass(options.activeClass);
                },
                "focus.uniform": function () {
                    divTag.addClass(options.focusClass);
                },
                "blur.uniform": function () {
                    divTag.removeClass(options.focusClass);
                    divTag.removeClass(options.activeClass);
                },
                "mousedown.uniform touchbegin.uniform": function () {
                    divTag.addClass(options.activeClass);
                },
                "mouseup.uniform touchend.uniform": function () {
                    divTag.removeClass(options.activeClass);
                },
                "click.uniform touchend.uniform": function () {
                    divTag.removeClass(options.activeClass);
                },
                "mouseenter.uniform": function () {
                    divTag.addClass(options.hoverClass);
                },
                "mouseleave.uniform": function () {
                    divTag.removeClass(options.hoverClass);
                    divTag.removeClass(options.activeClass);
                },
                "keyup.uniform": function () {
                    spanTag.text(elem.find(":selected").html());
                }
            });

            //handle disabled state
            if ($(elem).attr("disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }
            $.uniform.noSelect(spanTag);

            storeElement(elem);
            
            var inlineWidth = $(elem).inlineStyle("width");
            if(inlineWidth != undefined){
                inlineWidth = $(elem).outerWidth(true);
                $(spanTag).css({"width":inlineWidth-40+"px"});
            }
            // $(divTag).on('mouseenter',function(){
            //     var elemWidth = $(elem).css("width");
            //     var $elemSiblingSpan = $(elem).siblings("span");
            //     $elemSiblingSpan.css({"width":elemWidth-40});
            // });
        }

        function doCheckbox(elem) {
            var $el = $(elem);
            var divTag = $('<div />'),
          spanTag = $('<span />');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.checkboxClass);

            //assign the id of the element
            if (options.useID && elem.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
            }

            //wrap with the proper elements
            $(elem).wrap(divTag);
            $(elem).wrap(spanTag);

            //redefine variables
            spanTag = elem.parent();
            divTag = spanTag.parent();

            //hide normal input and add focus classes
            $(elem)
      .css("opacity", 0)
      .bind({
          "focus.uniform": function () {
              divTag.addClass(options.focusClass);
          },
          "blur.uniform": function () {
              divTag.removeClass(options.focusClass);
          },
          "click.uniform touchend.uniform": function () {
              if (!$(elem).attr("checked")) {
                  //box was just unchecked, uncheck span
                  spanTag.removeClass(options.checkedClass);
              } else {
                  //box was just checked, check span.
                  spanTag.addClass(options.checkedClass);
              }
          },
          "mousedown.uniform touchbegin.uniform": function () {
              divTag.addClass(options.activeClass);
          },
          "mouseup.uniform touchend.uniform": function () {
              divTag.removeClass(options.activeClass);
          },
          "mouseenter.uniform": function () {
              divTag.addClass(options.hoverClass);
          },
          "mouseleave.uniform": function () {
              divTag.removeClass(options.hoverClass);
              divTag.removeClass(options.activeClass);
          }
      });

            //handle defaults
            if ($(elem).attr("checked")) {
                //box is checked by default, check our box
                spanTag.addClass(options.checkedClass);
            }

            //handle disabled state
            if ($(elem).attr("disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }

            storeElement(elem);
        }

        function doRadio(elem) {
            var $el = $(elem);

            var divTag = $('<div />'),
          spanTag = $('<span />');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.radioClass);

            if (options.useID && elem.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
            }

            //wrap with the proper elements
            $(elem).wrap(divTag);
            $(elem).wrap(spanTag);

            //redefine variables
            spanTag = elem.parent();
            divTag = spanTag.parent();

            //hide normal input and add focus classes
            $(elem)
      .css("opacity", 0)
      .bind({
          "focus.uniform": function () {
              divTag.addClass(options.focusClass);
          },
          "blur.uniform": function () {
              divTag.removeClass(options.focusClass);
          },
          "click.uniform touchend.uniform": function () {
              if (!$(elem).attr("checked")) {
                  //box was just unchecked, uncheck span
                  spanTag.removeClass(options.checkedClass);
              } else {
                  //box was just checked, check span
                  var classes = options.radioClass.split(" ")[0];
                  $("." + classes + " span." + options.checkedClass + ":has([name='" + $(elem).attr('name') + "'])").removeClass(options.checkedClass);
                  spanTag.addClass(options.checkedClass);
              }
          },
          "mousedown.uniform touchend.uniform": function () {
              if (!$(elem).is(":disabled")) {
                  divTag.addClass(options.activeClass);
              }
          },
          "mouseup.uniform touchbegin.uniform": function () {
              divTag.removeClass(options.activeClass);
          },
          "mouseenter.uniform touchend.uniform": function () {
              divTag.addClass(options.hoverClass);
          },
          "mouseleave.uniform": function () {
              divTag.removeClass(options.hoverClass);
              divTag.removeClass(options.activeClass);
          }
      });

            //handle defaults
            if ($(elem).attr("checked")) {
                //box is checked by default, check span
                spanTag.addClass(options.checkedClass);
            }
            //handle disabled state
            if ($(elem).attr("disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }

            storeElement(elem);

        }

        function doFile(elem) {
            //sanitize input
            var $el = $(elem);

            var divTag = $('<div />'),
          filenameTag = $('<span>' + options.fileDefaultText + '</span>'),
          btnTag = $('<span>' + options.fileBtnText + '</span>');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.fileClass);
            filenameTag.addClass(options.filenameClass);
            btnTag.addClass(options.fileBtnClass);

            if (options.useID && $el.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + $el.attr("id"));
            }

            //wrap with the proper elements
            $el.wrap(divTag);
            $el.after(btnTag);
            $el.after(filenameTag);

            //redefine variables
            divTag = $el.closest("div");
            filenameTag = $el.siblings("." + options.filenameClass);
            btnTag = $el.siblings("." + options.fileBtnClass);

            //set the size
            if (!$el.attr("size")) {
                var divWidth = divTag.width();
                //$el.css("width", divWidth);
                $el.attr("size", divWidth / 10);
            }

            //actions
            var setFilename = function () {
                var filename = $el.val();
                if (filename === '') {
                    filename = options.fileDefaultText;
                }
                else {
                    filename = filename.split(/[\/\\]+/);
                    filename = filename[(filename.length - 1)];
                }
                filenameTag.text(filename);
            };

            // Account for input saved across refreshes
            setFilename();

            $el
      .css("opacity", 0)
      .bind({
          "focus.uniform": function () {
              divTag.addClass(options.focusClass);
          },
          "blur.uniform": function () {
              divTag.removeClass(options.focusClass);
          },
          "mousedown.uniform": function () {
              if (!$(elem).is(":disabled")) {
                  divTag.addClass(options.activeClass);
              }
          },
          "mouseup.uniform": function () {
              divTag.removeClass(options.activeClass);
          },
          "mouseenter.uniform": function () {
              divTag.addClass(options.hoverClass);
          },
          "mouseleave.uniform": function () {
              divTag.removeClass(options.hoverClass);
              divTag.removeClass(options.activeClass);
          }
      });

            // IE7 doesn't fire onChange until blur or second fire.
            if ($.browser.msie) {
                // IE considers browser chrome blocking I/O, so it
                // suspends tiemouts until after the file has been selected.
                $el.bind('click.uniform.ie7', function () {
                    setTimeout(setFilename, 0);
                });
            } else {
                // All other browsers behave properly
                $el.bind('change.uniform', setFilename);
            }

            //handle defaults
            if ($el.attr("disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }

            $.uniform.noSelect(filenameTag);
            $.uniform.noSelect(btnTag);

            storeElement(elem);

        }

        $.uniform.restore = function (elem) {
            if (elem == undefined) {
                elem = $($.uniform.elements);
            }

            $(elem).each(function () {
                if ($(this).is(":checkbox")) {
                    //unwrap from span and div
                    $(this).unwrap().unwrap();
                } else if ($(this).is("select")) {
                    //remove sibling span
                    $(this).siblings("span").remove();
                    //unwrap parent div
                    $(this).unwrap();
                } else if ($(this).is(":radio")) {
                    //unwrap from span and div
                    $(this).unwrap().unwrap();
                } else if ($(this).is(":file")) {
                    //remove sibling spans
                    $(this).siblings("span").remove();
                    //unwrap parent div
                    $(this).unwrap();
                } else if ($(this).is("button, :submit, :reset, a, input[type='button']")) {
                    //unwrap from span and div
                    $(this).unwrap().unwrap();
                }

                //unbind events
                $(this).unbind(".uniform");

                //reset inline style
                $(this).css("opacity", "1");

                //remove item from list of uniformed elements
                var index = $.inArray($(elem), $.uniform.elements);
                $.uniform.elements.splice(index, 1);
            });
        };

        function storeElement(elem) {
            //store this element in our global array
            elem = $(elem).get();
            if (elem.length > 1) {
                $.each(elem, function (i, val) {
                    $.uniform.elements.push(val);
                });
            } else {
                $.uniform.elements.push(elem);
            }
        }

        //noSelect v1.0
        $.uniform.noSelect = function (elem) {
            function f() {
                return false;
            };
            $(elem).each(function () {
                this.onselectstart = this.ondragstart = f; // Webkit & IE
                $(this)
        .mousedown(f) // Webkit & Opera
        .css({ MozUserSelect: 'none' }); // Firefox
            });
        };

        $.uniform.update = function (elem) {
            if (elem == undefined) {
                elem = $($.uniform.elements);
            }
            //sanitize input
            elem = $(elem);

            elem.each(function () {
                //do to each item in the selector
                //function to reset all classes
                var $e = $(this);

                if ($e.is("select")) {
                    //element is a select
                    var spanTag = $e.siblings("span");
                    var divTag = $e.parent("div");

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);

                    //reset current selected text
                    spanTag.html($e.find(":selected").html());

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }

                } else if ($e.is(":checkbox")) {
                    //element is a checkbox
                    var spanTag = $e.closest("span");
                    var divTag = $e.closest("div");

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);
                    spanTag.removeClass(options.checkedClass);

                    if ($e.is(":checked")) {
                        spanTag.addClass(options.checkedClass);
                    }
                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }

                } else if ($e.is(":radio")) {
                    //element is a radio
                    var spanTag = $e.closest("span");
                    var divTag = $e.closest("div");

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);
                    spanTag.removeClass(options.checkedClass);

                    if ($e.is(":checked")) {
                        spanTag.addClass(options.checkedClass);
                    }

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }
                } else if ($e.is(":file")) {
                    var divTag = $e.parent("div");
                    var filenameTag = $e.siblings(options.filenameClass);
                    btnTag = $e.siblings(options.fileBtnClass);

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);

                    filenameTag.text($e.val());

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }
                } else if ($e.is(":submit") || $e.is(":reset") || $e.is("button") || $e.is("a") || elem.is("input[type=button]")) {
                    var divTag = $e.closest("div");
                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }

                }

            });
        };

        return this.each(function () {
            if ($.support.selectOpacity) {
                var elem = $(this);

                if (elem.is("select")) {
                    //element is a select
                    if (elem.attr("multiple") != true) {
                        //element is not a multi-select
                        if (elem.attr("size") == undefined || elem.attr("size") <= 1) {
                            doSelect(elem);
                        }
                    }
                } else if (elem.is(":checkbox")) {
                    //element is a checkbox
                    doCheckbox(elem);
                } else if (elem.is(":radio")) {
                    //element is a radio
                    doRadio(elem);
                } else if (elem.is(":file")) {
                    //element is a file upload
                    doFile(elem);
                } else if (elem.is(":text, :password, input[type='email']")) {
                    doInput(elem);
                } else if (elem.is("textarea")) {
                    doTextarea(elem);
                } else if (elem.is("a") || elem.is(":submit") || elem.is(":reset") || elem.is("button") || elem.is("input[type=button]")) {
                    doButton(elem);
                }

            }
        });
    };
})(jQuery);

/*
------------------------------------------------------------------------------------------------------------------------------------------------
Plugins // By Mohan

*/


//MFicient Plugins
/*----------------------------------------------------------------------*/
/* ASP Modal PopUp
/* 
/*----------------------------------------------------------------------*/
(function ($) {
    /**
    * This is a simple jQuery plugin that works with the jQuery UI
    * dialog. This plugin makes the jQuery UI dialog append to the
    * first form on the page (i.e. the asp.net form) so that
    * forms in the dialog will post back to the server.
    *
    * This plugin is merely used to open dialogs. Use the normal
    * $.fn.dialog() function to close dialogs programatically.
    */
    $.fn.aspdialog = function () {
        if (typeof $.fn.dialog !== "function") return;

        var dlg = {};

        if ((arguments.length == 0)
                || (arguments[0] instanceof String)) {
            // If we just want to open it without any options
            // we do it this way.
            dlg = this.dialog({ "autoOpen": false, modal: true, resizable: false, appendTo: 'form:first' });
            //dlg.parent().appendTo('form:first');
            dlg.dialog('open');
        } else {
            var options = arguments[0];
            options.autoOpen = false;
            options.bgiframe = true;
            options.appendTo = 'form:first';
            dlg = this.dialog(options);
            //dlg.parent().appendTo('form:first');
            dlg.dialog('open');
        }
    };
})(jQuery);
(function ($) {
    $.fn.viewActionBtnDialogSettings = function (opts) {
        var CONSTANTS = {
            classes: {
                "ConfirmSetting": "ConfirmSetting",
                "ConfirmMessage": "ConfirmMessage",
                "ConfirmDismissTxt": "ConfirmDismissTxt",
                "ConfirmProceedText": "ConfirmProceedText",
                "AlertSetting": "AlertSetting",
                "AlertMessage": "AlertMessage",
                "AlertDismissTxt": "AlertDismissTxt"
            }

        }
        var settingType = {
            alert: "1",
            confirm: "2"
        }
        var defaults = {
            editObject: null
        };
        var aryOrderOfTableRows = [settingType.alert, settingType.confirm];
        var options = $.extend(defaults, opts);
        //Control type which is formed (Controls which is dragged.)
        var $self = $(this);
        $self.children().remove(); //remove any previous html.
        if (options.editObject && $.isArray(options.editObject) && options.editObject.length > 0) {
            aryOrderOfTableRows = [];
            $.each(options.editObject, function (index, value) {
                aryOrderOfTableRows.push(_getTypeByValue(value.type));
            });
        }
        $self.html(_formHtml());
        _showUpDownArrowInTable();
        _setEventsOfViewTable();
        $self.data(MF_IDE_CONSTANTS.jqueryDataKeys.actionBtnDialogSettings, { finalSetting: _returnFinalSettingsObject });
        if (options.editObject) {
            _fillDataInHtmlForEdit(options.editObject);
        }
        return $self;

        function _formHtml() {
            var strHtml = '<table class="detailsTable withTextArea">';
            strHtml += '<thead><th>Type</th><th>Message</th><th>Dismiss Button Text</th><th>Proceed Button Text</th><th></th></thead>';
            strHtml += '<tbody>';
            for (var i = 0; i <= aryOrderOfTableRows.length - 1; i++) {
                strHtml += _getRowByType(aryOrderOfTableRows[i]);
            }
            // strHtml +='<tr class="ConfirmSetting MoveableRow"><td><span>Confirm<span></td><td><textarea class="ConfirmMessage" ></textarea></td>';
            // strHtml += '<td><input type="text" class="ConfirmDismissTxt"/></td><td><input type="text" class="ConfirmProceedText"/></td>'
            // strHtml += "<td><span class=\"moveUp16x16\"></span><span class=\"moveDown16x16\"></span></td>";
            // strHtml +='</tr>';
            // strHtml +='<tr class="AlertSetting MoveableRow"><td><span>Alert<span></td><td><textarea class="AlertMessage"></textarea></td>';
            // strHtml += '<td><input type="text" class="AlertDismissTxt"/></td><td><span>Not Applicable<span></td>';
            // strHtml += "<td><span class=\"moveUp16x16\"></span><span class=\"moveDown16x16\"></span></td>";
            // strHtml += '</tr>';
            strHtml += '</tbody>';
            strHtml += '</table>';
            return strHtml;
        }
        function _getRowByType(type) {
            var strRowHtml = "";
            switch (type) {
                case settingType.alert:
                    strRowHtml = _getAlertTableRow();
                    break;
                case settingType.confirm:
                    strRowHtml = _getConfirmTableRow();
                    break;
            }
            return strRowHtml;
        }
        function _getConfirmTableRow() {
            var strHtml = '<tr class="ConfirmSetting MoveableRow"><td><span>Confirm<span></td><td><textarea id="textarea_ConfirmMsg" class="ConfirmMessage" onkeydown="return enterKeyLinefeed(this.id, event);"></textarea></td>';
            strHtml += '<td><input type="text" class="ConfirmDismissTxt" onkeydown="return enterKeyFilter(event);"/></td><td><input type="text" class="ConfirmProceedText" onkeydown="return enterKeyFilter(event);"/></td>';
            strHtml += '<td><span class="moveUp16x16"></span><span class="moveDown16x16"></span></td>';
            strHtml += '</tr>';
            return strHtml;
        }
        function _getAlertTableRow() {
            var strHtml = '<tr class="AlertSetting MoveableRow"><td><span>Alert<span></td><td><textarea id="textarea_AlertMsg" class="AlertMessage" onkeydown="return enterKeyLinefeed(this.id, event);"></textarea></td>';
            strHtml += '<td><input type="text" class="AlertDismissTxt" onkeydown="return enterKeyFilter(event);"/></td><td><span>Not Applicable<span></td>';
            strHtml += '<td><span class="moveUp16x16"></span><span class="moveDown16x16"></span></td>';
            strHtml += '</tr>';
            return strHtml;
        }
        function _getTable() {
            return $self.find('table');
        }
        function _showUpDownArrowInTable() {
            var tbl = _getTable();
            var noOfRows = tbl.find("tbody tr").length;
            var count = 0;
            if (noOfRows === 1) {
                $(tbl).find("tbody tr").each(function () {
                    $(this).find(".moveUp16x16,.moveDown16x16").hide();
                });
            }
            else if (noOfRows === 2) {
                $(tbl).find("tbody tr").each(function () {
                    if (count === 0) {
                        $(this).find(".moveDown16x16").show();
                        $(this).find(".moveUp16x16").hide();
                    }
                    else {
                        $(this).find(".moveDown16x16").hide();
                        $(this).find(".moveUp16x16").show();
                    }
                    count++;
                });
            }
            else {
                $(tbl).find("tbody tr").each(function () {
                    if (count === 0) {
                        $(this).find(".moveDown16x16").show();
                        $(this).find(".moveUp16x16").hide();
                    }
                    else if (count === noOfRows - 1) {
                        $(this).find(".moveDown16x16").hide();
                        $(this).find(".moveUp16x16").show();
                    }
                    else {
                        $(this).find(".moveDown16x16").show();
                        $(this).find(".moveUp16x16").show();
                    }
                    count++;
                });
            }
        }
        function _setEventsOfViewTable() {
            var $tbl = _getTable();
            $tbl.find('.moveUp16x16').on('click', function (evnt) {
                _moveUpDownCmdObject(this, "up");
                evnt.stopPropagation();
            });
            $tbl.find('.moveDown16x16').on('click', function (evnt) {
                _moveUpDownCmdObject(this, "down");
                evnt.stopPropagation();
            });
        }
        function _moveUpDownCmdObject(sender, command/*up down*/) {
            switch (command.toString().toLowerCase()) {
                case "up":
                    var rowToMove = $(sender).parents('tr.MoveableRow:first');
                    var prev = rowToMove.prev('tr.MoveableRow');
                    if (prev.length == 1) {
                        prev.before(rowToMove);
                    }
                    break;
                case "down":
                    var rowToMove = $(sender).parents('tr.MoveableRow:first');
                    var next = rowToMove.next('tr.MoveableRow');
                    if (next.length == 1) {
                        next.after(rowToMove);
                    }
                    break;
            }
            _showUpDownArrowInTable();
        }
        function _returnFinalSettingsObject() {
            var $tr = $self.find('table tbody tr');
            var aryBtnMsgs = [];
            $tr.each(function (index) {
                //console.log( index + ": " + $( this ).text() );
                var objSettings = new ObjectBindingDialogSettings();
                if ($(this).hasClass(CONSTANTS.classes.ConfirmSetting)) {
                    objSettings.type = settingType.confirm;
                    objSettings.msg = $(this).find('.ConfirmMessage').val();
                    objSettings.dismissBtnText = $(this).find('.ConfirmDismissTxt').val();
                    objSettings.proceedBtnText = $(this).find('.ConfirmProceedText').val();
                    aryBtnMsgs.push(objSettings);
                }
                else {
                    objSettings.type = settingType.alert;
                    objSettings.msg = $(this).find('.AlertMessage').val();
                    objSettings.dismissBtnText = $(this).find('.AlertDismissTxt').val();
                    objSettings.proceedBtnText = $(this).find('.AlertProceedText').val();
                    aryBtnMsgs.push(objSettings);
                }
            });
            return aryBtnMsgs;
        }
        function _fillDataInHtmlForEdit(settings/*settings object*/) {
            if (settings) {
                $.each(settings, function (index, value) {
                    if (value.type === settingType.alert) {
                        $tr = $self.find('table tbody tr.AlertSetting');
                        $tr.find('.' + CONSTANTS.classes.AlertMessage).val(value.msg);
                        $tr.find('.' + CONSTANTS.classes.AlertDismissTxt).val(value.dismissBtnText);
                    }
                    else {
                        $tr = $self.find('table tbody tr.ConfirmSetting');
                        $tr.find('.' + CONSTANTS.classes.ConfirmMessage).val(value.msg);
                        $tr.find('.' + CONSTANTS.classes.ConfirmDismissTxt).val(value.dismissBtnText);
                        $tr.find('.' + CONSTANTS.classes.ConfirmProceedText).val(value.proceedBtnText);
                    }
                });
            }
        }
        function _getTypeByValue(value) {
            var setting = "";
            switch (value) {
                case "1":
                    setting = settingType.alert;
                    break;
                case "2":
                    setting = settingType.confirm;
                    break;
            }
            return setting;
        }
    };
})(jQuery);
(function ($) {
    $.fn.textIntellisense = function (options) {
        //var intellisence = JSON.stringify(intlsense);
        var settings = $.extend({
            IntlsJson: "[]"
        }, options || {});
        $.widget("custom.catcomplete", $.ui.autocomplete, {
            _create: function() {
              this._super();
              this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
            },
            _renderMenu: function (ul, items) {
                var that = this,
                currentCategory = "";
                $.each(items, function (index, item) {
                    if (item.category != currentCategory) {
                        ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                        currentCategory = item.category;
                    }
                    li = that._renderItemData(ul, item);
                    if (item.category) {
                        li.attr("aria-label", item.category + " : " + item.label);
                    }
                });
            }
        });
        this.catcomplete({
            source: function (request, response) {
                var txtVal = request.term;
                tags = getOptionsByNoOfDots(txtVal);
                var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(txtVal), "i");
                response($.grep(tags, function (item) {
                    return matcher.test(item.value.toLowerCase());
                }));
            },
            focus: function (event, ui) {
                // prevent autocomplete from updating the textbox
                event.preventDefault();
                // manually update the textbox
                $(this).val(ui.item.value);
            },
            select: function (event, ui) {
                // prevent autocomplete from updating the textbox
                event.preventDefault();
                // manually update the textbox and hidden field
                $(this).val(ui.item.value);
            },
            minLength: 0,
            delay: 0,
            appendTo: "body"
        });


        function formDataSourceFromAryOptions(options /*ary of objects prop and val*/) {
            var tags = [];
            if (options && $.isArray(options) && options.length > 0) {
                for (var i = 0; i <= options.length - 1; i++) {
                    var objFinalOptionObjct = new Object();
                    objFinalOptionObjct.value = options[i].val;
                    objFinalOptionObjct.label = options[i].prop;
                    objFinalOptionObjct.category = options[i].category;
                    tags.push(objFinalOptionObjct);
                }
            }
            return tags;
        }

        function getFirstLevelOptions() {
            return formDataSourceFromAryOptions($.parseJSON(settings.IntlsJson));
        }

        function getOptionsByNoOfDots(textBal) {
            var tags = [];
            if (textBal) {
                var aryTextValSelected = textBal.split('.');
                if (aryTextValSelected.length <= 1) {
                    tags = getFirstLevelOptions();
                } else {
                    var lstCharacter = (textBal).substring((textBal.length) - 1);
                    var iNoOfDotsToConsider = aryTextValSelected.length;
                    var finalValue = "";
                    var objByLevel = $.parseJSON(settings.IntlsJson);
                    var textValToConsider = 0;
                    var strObjectNameOfInnerParam = "";
                    for (var i = 0; i <= iNoOfDotsToConsider - 1; i++) {
                        var objTempPropertySelected = [];
                        if ((i % 2) === 0) {
                            //textValToConsider++;
                            for (var j = 0; j <= objByLevel.length - 1; j++) {
                                if (aryTextValSelected[i] === "") {
                                    //this has to be an array.
                                    //objTempPropertySelected = objByLevel;
                                    var aryInnerParam = [];
                                    if (objByLevel[j] && $.isPlainObject(objByLevel[j]) && objByLevel[j].hasOwnProperty('ip')) {
                                        strObjectNameOfInnerParam = objByLevel[j].val;
                                        aryInnerParam = objByLevel[j].ip;
                                    }
                                    for (var n = 0; n <= aryInnerParam.length - 1; n++) {
                                        aryInnerParam[n].val = strObjectNameOfInnerParam + "." + aryInnerParam[n].val;
                                        objTempPropertySelected.push(aryInnerParam[n]);
                                    }
                                    strObjectNameOfInnerParam = "";

                                    //break;
                                } else {
                                    if (i === 0) {
                                        if (objByLevel[j].val.toLowerCase().startsWith(aryTextValSelected[i].toLowerCase())) {
                                            objTempPropertySelected.push(objByLevel[j]);
                                        }
                                    }
                                    else {
                                        var aryInnerParam = [];
                                        if (objByLevel[j] && $.isPlainObject(objByLevel[j]) && objByLevel[j].hasOwnProperty('ip')) {
                                            strObjectNameOfInnerParam = objByLevel[j].val;
                                            aryInnerParam = objByLevel[j].ip;
                                        }
                                        for (var q = 0; q <= aryInnerParam.length - 1; q++) {
                                            if ((/[0-9A-Za-z]\[[0-9]*]/).test(aryTextValSelected[i].trim())) {
                                                var strSelectedValWithoutBracket = aryTextValSelected[i].split("[")[0];
                                                if (aryInnerParam[q].val.toLowerCase().startsWith(aryTextValSelected[i].toLowerCase())) {
                                                    aryInnerParam[q].val = strObjectNameOfInnerParam + "." + aryTextValSelected[i];
                                                    objTempPropertySelected.push(aryInnerParam[q]);
                                                }

                                            }
                                            else {
                                                if (aryInnerParam[q].val.toLowerCase().startsWith(aryTextValSelected[i].toLowerCase())) {
                                                    aryInnerParam[q].val = strObjectNameOfInnerParam + "." + aryInnerParam[q].val;
                                                    objTempPropertySelected.push(aryInnerParam[q]);
                                                }
                                            }

                                        }
                                        strObjectNameOfInnerParam = "";
                                    }

                                }
                            }

                        } else {
                            for (var m = 0; m <= objByLevel.length - 1; m++) {
                                if (aryTextValSelected[i] === "") {
                                    var aryInnerParam = [];
                                    if (objByLevel[m] && $.isPlainObject(objByLevel[m]) && objByLevel[m].hasOwnProperty('ip')) {
                                        strObjectNameOfInnerParam = objByLevel[m].val;
                                        aryInnerParam = objByLevel[m].ip;
                                    }
                                    for (var n = 0; n <= aryInnerParam.length - 1; n++) {
                                        aryInnerParam[n].val = strObjectNameOfInnerParam + "." + aryInnerParam[n].val;
                                        objTempPropertySelected.push(aryInnerParam[n]);
                                    }
                                    strObjectNameOfInnerParam = "";


                                } else {
                                    var aryInnerParam = [];
                                    if (objByLevel[m] && $.isPlainObject(objByLevel[m]) && objByLevel[m].hasOwnProperty('ip')) {
                                        strObjectNameOfInnerParam = objByLevel[m].val;
                                        aryInnerParam = objByLevel[m].ip;
                                    }
                                    for (var p = 0; p <= aryInnerParam.length - 1; p++) {
                                        if ((/[0-9A-Za-z]\[[0-9]*]/).test(aryTextValSelected[i].trim())) {
                                            var strSelectedValWithoutBracket = aryTextValSelected[i].split("[")[0];
                                            if (aryInnerParam[p].val.toLowerCase().startsWith(aryTextValSelected[i].toLowerCase())) {
                                                aryInnerParam[p].val = strObjectNameOfInnerParam + "." + aryTextValSelected[i].trim();
                                                objTempPropertySelected.push(aryInnerParam[p]);

                                            }
                                        }
                                        else {
                                            if (aryInnerParam[p].val.toLowerCase().startsWith(aryTextValSelected[i].toLowerCase())) {
                                                aryInnerParam[p].val = strObjectNameOfInnerParam + "." + aryInnerParam[p].val;
                                                objTempPropertySelected.push(aryInnerParam[p]);

                                            }
                                        }
                                    }
                                    strObjectNameOfInnerParam = "";
                                }

                            }

                        }
                        objByLevel = objTempPropertySelected;
                    }
                    if ($.isArray(objByLevel)) {
                        tags = formDataSourceFromAryOptions(objByLevel);
                    }
                }
            } else {
                tags = getFirstLevelOptions();
            }
            return tags;
        }

        function IntellisenseObject(prop, val, ip) {
            this.prop = prop;
            this.val = val;
            this.ip = ip;
        }

    };
})(jQuery);
(function ($) {
    $.fn.MFEtextIntellisense = function (options) {
        //var intellisence = JSON.stringify(intlsense);
        var settings = $.extend({
            IntlsJson: "[]"
        }, options || {});

        this.autocomplete({
            source: function (request, response) {
                var txtVal = request.term;
                tags = getOptionsByNoOfDots(txtVal);
                var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(txtVal), "i");
                response($.grep(tags, function (item) {
                    return matcher.test(item.value.toLowerCase());
                }));
            },
            focus: function (event, ui) {
                // prevent autocomplete from updating the textbox
                event.preventDefault();
                // manually update the textbox
                $(this).val(ui.item.value);
            },
            select: function (event, ui) {
                // prevent autocomplete from updating the textbox
                event.preventDefault();
                // manually update the textbox and hidden field
                $(this).val(ui.item.value);
            },
            minLength: 0,
            delay: 1
        });

        function formDataSourceFromAryOptions(options /*ary of objects prop and val*/) {
            var tags = [];
            if (options && $.isArray(options) && options.length > 0) {
                for (var i = 0; i <= options.length - 1; i++) {
                    var objFinalOptionObjct = new Object();
                    objFinalOptionObjct.value = options[i].val;
                    objFinalOptionObjct.label = options[i].prop;
                    tags.push(objFinalOptionObjct);
                }
            }
            $.merger(tags, MF_IDE_CONSTANTS.IntlsenseFixedParams);
            return tags;
        }

        function getFirstLevelOptions() {
            return formDataSourceFromAryOptions($.parseJSON(settings.IntlsJson));
        }

        function getOptionsByNoOfDots(textBal) {
            var tags = [];
            if (textBal) {
                var aryTextValSelected = textBal.split('.');
                if (aryTextValSelected.length <= 1) {
                    tags = getFirstLevelOptions();
                } else {
                    var lstCharacter = (textBal).substring((textBal.length) - 1);
                    var iNoOfDotsToConsider = aryTextValSelected.length;
                    var finalValue = "";
                    var objByLevel = $.parseJSON(settings.IntlsJson);
                    var textValToConsider = 0;
                    var strObjectNameOfInnerParam = "";
                    for (var i = 0; i <= iNoOfDotsToConsider - 1; i++) {
                        var objTempPropertySelected = [];
                        if ((i % 2) === 0) {
                            //textValToConsider++;
                            for (var j = 0; j <= objByLevel.length - 1; j++) {
                                if (aryTextValSelected[i] === "") {
                                    //this has to be an array.
                                    //objTempPropertySelected = objByLevel;
                                    var aryInnerParam = [];
                                    if (objByLevel[j] && $.isPlainObject(objByLevel[j]) && objByLevel[j].hasOwnProperty('ip')) {
                                        strObjectNameOfInnerParam = objByLevel[j].val;
                                        aryInnerParam = objByLevel[j].ip;
                                    }
                                    for (var n = 0; n <= aryInnerParam.length - 1; n++) {
                                        aryInnerParam[n].val = strObjectNameOfInnerParam + "." + aryInnerParam[n].val;
                                        objTempPropertySelected.push(aryInnerParam[n]);
                                    }
                                    strObjectNameOfInnerParam = "";

                                    break;
                                } else {
                                    if (i === 0) {
                                        if (objByLevel[j].val.startsWith(aryTextValSelected[i])) {
                                            objTempPropertySelected.push(objByLevel[j]);
                                        }
                                    }
                                    else {
                                        var aryInnerParam = [];
                                        if (objByLevel[j] && $.isPlainObject(objByLevel[j]) && objByLevel[j].hasOwnProperty('ip')) {
                                            strObjectNameOfInnerParam = objByLevel[j].val;
                                            aryInnerParam = objByLevel[j].ip;
                                        }
                                        for (var q = 0; q <= aryInnerParam.length - 1; q++) {
                                            if (aryInnerParam[q].val.startsWith(aryTextValSelected[i])) {
                                                aryInnerParam[q].val = strObjectNameOfInnerParam + "." + aryInnerParam[q].val;
                                                objTempPropertySelected.push(aryInnerParam[q]);
                                            }
                                        }
                                        strObjectNameOfInnerParam = "";
                                    }

                                }
                            }

                        } else {
                            for (var m = 0; m <= objByLevel.length - 1; m++) {
                                if (aryTextValSelected[i] === "") {
                                    var aryInnerParam = [];
                                    if (objByLevel[m] && $.isPlainObject(objByLevel[m]) && objByLevel[m].hasOwnProperty('ip')) {
                                        strObjectNameOfInnerParam = objByLevel[m].val;
                                        aryInnerParam = objByLevel[m].ip;
                                    }
                                    for (var n = 0; n <= aryInnerParam.length - 1; n++) {
                                        aryInnerParam[n].val = strObjectNameOfInnerParam + "." + aryInnerParam[n].val;
                                        objTempPropertySelected.push(aryInnerParam[n]);
                                    }
                                    strObjectNameOfInnerParam = "";


                                } else {
                                    var aryInnerParam = [];
                                    if (objByLevel[m] && $.isPlainObject(objByLevel[m]) && objByLevel[m].hasOwnProperty('ip')) {
                                        strObjectNameOfInnerParam = objByLevel[m].val;
                                        aryInnerParam = objByLevel[m].ip;
                                    }
                                    for (var p = 0; p <= aryInnerParam.length - 1; p++) {
                                        if (aryInnerParam[p].val.startsWith(aryTextValSelected[i])) {
                                            aryInnerParam[p].val = strObjectNameOfInnerParam + "." + aryInnerParam[p].val;
                                            objTempPropertySelected.push(aryInnerParam[p]);

                                        }
                                    }
                                    strObjectNameOfInnerParam = "";
                                }

                            }

                        }
                        objByLevel = objTempPropertySelected;
                    }
                    if ($.isArray(objByLevel)) {
                        tags = formDataSourceFromAryOptions(objByLevel);
                    }
                }
            } else {
                tags = getFirstLevelOptions();
            }
            return tags;
        }

        function IntellisenseObject(prop, val, ip) {
            this.prop = prop;
            this.val = val;
            this.ip = ip;
        }

    };
})(jQuery);
(function ($) {
    $.fn.mfConditionalLogicControl = function (options) {
        var CONSTANTS = {
            FirstSelectPrefix: "ddlFirstColDDL",
            SecondSelectPrefix: "ddlMapOperator",
            ThirdTextPrefix: "txtMapWithIntls",
            AddButtonPrefix: "AddRow",
            DeleteButtonPrefix: "DeleteRow"
        };
        var CSS_CLASSES = {
            detailsTable: "detailsTable",
            MatchTypeDdl: "MatchTypeDdl",
            notUniform: "notUniform",
            intellisense: "intlsense"
        };
        var MatchTypeEnum = {
            Any: "0",
            All: "1"
        };
        var settings = $.extend({
            FirstDropDownOptions: "[{\"text\":\"Select\",\"val\":\"-1\"}]",
            ConditionalLogicDdlVals: "[{\"text\":\"Select\",\"val\":\"-1\"}]",
            ThirdDropDownOptions: "[{\"text\":\"Select\",\"val\":\"-1\"}]",
            UseIntellisense: true,
            IntlsJson: "{}",
            CallbackFunctionOnSave: function (savedJson) {
                alert(savedJson);
            },
            CallbackFunctionOnCancel: function () {
                alert("Called the call back function on CNANCEL");
            },
            FirstColumnTagForJson: "FirstCol",
            SecondColumnTagForJson: "SecondCol",
            ThirdColumnTagForJson: "ThirdCol",
            IsEdit: false,
            SavedJsonIfEdit: "[]",
            SelectFirstDdlValBy: "Value",
            SelectCondLogicDdlValBy: "Value",
            ShowMatchType: true,
            intlsValidationFunction: MF_HELPERS.intlsenseHelper.isIntlCntrlValueValidInCondLogic,
            showRedBorderOnError: true,
            firstColWidth: 130,
            secondColWidth: 150,
            thirdColWidth: 150

        }, options || {});

        //initialisation
        var $self = this;
        //remove any already existing html.
        $self.children().remove();
        var _id = $self.attr('id') + "CondLogicTable1";
        $self.html(_getInitialisationHTML());
        var $tblAdded = $($self).find("[id=\"" + _id + "\"]");
        //add the first row
        if (settings.IsEdit) {
            var objSavedJson = _parseSavedJson(),
                matchingType = MatchTypeEnum.All,
                aryConditions = [];
            if (objSavedJson) {
                matchingType = _getMatchTypeFromSavedData(objSavedJson);
                aryConditions = _getConditionsFromSavedData(objSavedJson);
                _fillMatchingTypeDropdown(matchingType);
                if (aryConditions &&
                    $.isArray(aryConditions) &&
                    aryConditions.length > 0) {
                    for (var i = 0; i <= aryConditions.length - 1; i++) {
                        _processAddRowToTable();
                        //_fillSavedMapeedData();
                        _fillSavedMappedConditionalData(aryConditions[i], i);
                    }
                }
            }
        }
        else {
            _processAddRowToTable();
        }
        this.data("FinalJson", formMappedCompleteJSON);
        this.data("ValidateIntlsText", checkIfTextValExistsInJson);
        return this;
        //_bindEventToDivActionButtons();
        //end initialisation


        function _getInitialisationHTML() {
            var html = "";
            if (settings.ShowMatchType ||
                settings.ShowMatchType === true) {
                html += _getMatchTypeDivHtml();
            }
            html += _getTableSkeletonToAdd();
            //html +=_getActionButtonDivToAdd();
            return html;
        }
        function _getMatchTypeDivHtml() {
            var strHtml = "";
            strHtml += '<div class="g12">';
            strHtml += '<span class="mrgr15">Match</span>';
            strHtml += '<select class="' + CSS_CLASSES.MatchTypeDdl + ' w_80p" onkeydown=\"return enterKeyFilter(event);\">';
            strHtml += '<option value=' + MatchTypeEnum.All + '>All</option>';
            strHtml += '<option value=' + MatchTypeEnum.Any + '>Any</option>';
            strHtml += '</select>';
            strHtml += '<span class="mrgl15">Condition</span>';
            strHtml += '</div>';
            return strHtml;
        }
        function _getTableSkeletonToAdd() {
            var html = '<div class="g12">';
            html += _getTableOpeningTag();
            html += _getSkeletonTbodyTag();
            html += _getTableEndTag();
            html += '</div>';
            return html;
        }
        function _getActionButtonDivToAdd() {
            var html = "";
            html += "<div style=\"border-top:1px solid grey;margin-top:3px;text-align:center;\">";
            html += "<input id=\"btnConLogicOk" + _id + "\" type=\"submit\" value=\"OK\" />";
            html += "<input id=\"btnConLogicCancel" + _id + "\" type=\"submit\" value=\"CANCEL\" />";
            html += "</div>";
            return html;
        }
        function _getTableOpeningTag() {
            return '<table id ="' + _id + '" class="' + CSS_CLASSES.detailsTable + '">';
        }
        function _getTableEndTag() {
            return "</table>";
        }
        function _getSkeletonTbodyTag() {
            return "<tbody></tbody>";
        }

        /**
        Adds the required HTML And Then binds the dropdown and makes 
        intellisense textbox binds event to add and delete button.
        **/
        function _processAddRowToTable() {
            var tblBinding = $tblAdded;
            var index = tblBinding.find("tbody tr").length;
            (index > 0) ?
            tblBinding.children('tbody:last').children('tr:last').after(_getHtmlToAddToCntlColMapTable(index + 1)) :
            tblBinding.children('tbody:last').append(_getHtmlToAddToCntlColMapTable(index + 1));

            //$(tblBinding).find("tbody tr:last").after();
            _bindAndAddEventsToLastRowControls();
        }
        function _bindAndAddEventsToLastRowControls() {
            var firstDropDown = $tblAdded.find("tbody tr:last").find("[id^=\"" + CONSTANTS.FirstSelectPrefix + "\"]");
            var aryFirstDDOptions = $.parseJSON(settings.FirstDropDownOptions);
            var condlLogicDropDown = $tblAdded.find("tbody tr:last").find("[id^=\"" + CONSTANTS.SecondSelectPrefix + "\"]");
            var arySecondDDOptions = $.parseJSON(settings.ConditionalLogicDdlVals);
            var addButton = $tblAdded.find("tbody tr:last").find("[id^=\"" + CONSTANTS.AddButtonPrefix + "\"]");
            var deleteButton = $tblAdded.find("tbody tr:last").find("[id^=\"" + CONSTANTS.DeleteButtonPrefix + "\"]");
            var intlsTextBox = $tblAdded.find("tbody tr:last").find(":text[id^=\"" + CONSTANTS.ThirdTextPrefix + "\"]");
            //TODO 
            /**
            COMPLETE THE ELSE STATEMENT IF NOT USINF INTELLISENSE TEXT BOX
            THEN ITHE HEML WILL CONTAIN A TEXT BOX SO SET OPTIONS FOR TAHT
            **/
            if (settings.UseIntellisense) {
                intlsTextBox.textIntellisense({
                    IntlsJson: settings.IntlsJson
                });
            }
            else {

            }
            _bindDropdown(aryFirstDDOptions, firstDropDown);
            _bindDropdown(arySecondDDOptions, condlLogicDropDown);
            $(addButton).on('click', function () {
                _addCntrlColMapRowToTable(this)
            });
            $(deleteButton).on('click', function () {
                _deleteTableRowOfSender(this)
            });
        }
        function _bindEventToDivActionButtons() {
            var $OKButton = $self.find("[id^=\"btnConLogicOk\"]");
            var $CANCELButton = $self.find("[id^=\"btnConLogicCancel\"]");
            $OKButton.on('click', function () {
                var lastJson = JSON.stringify(_formMappedCompleteJSON());
                settings.CallbackFunctionOnSave(lastJson);
            });
            $CANCELButton.on('click', function () {
                settings.CallbackFunctionOnCancel();
            });
        }
        //private methods
        function _addCntrlColMapRowToTable(sender) {
            _processAddRowToTable();
        }
        function _addCntrlColMapRowToTableFromJson(cntrlColMapWhereJson) {
            if (cntrlColMapWhereJson) {
                var tblCntrlDataBindingOflnColMap = _getCntlOFDBDatabindingTable();
                $(tblCntrlDataBindingOflnColMap).find('tbody tr').remove();
                var index = 0;
                var htmlToAdd = "";
                var objCntrlColMapWhereObject = $.parseJSON(cntrlColMapWhereJson);
                if (objCntrlColMapWhereObject) {
                    var aryCntrlColMapObj = objCntrlColMapWhereObject.Logic;
                    if (aryCntrlColMapObj.length > 0) {
                        for (var i = 0; i <= aryCntrlColMapObj.length - 1; i++) {
                            htmlToAdd += _getHtmlToAddToCntlColMapTable(index);
                        }
                    }

                }
                var aryCntrls = getAllValidCntrlsObjectForIntlsense();
                var htmlToAdd = _getHtmlToAddToCntlColMapTable(index);
                $(tblCntrlDataBindingOflnColMap).find("tbody tr:last").after(htmlToAdd);
                makeCntrlOFDBMapTextboxIntellisense(aryCntrls, "CntrlOFDBMaptxt" + index);
            }
        }
        function _getHtmlToAddToCntlColMapTable(index) {
            var htmlToAdd = "<tr>";
            htmlToAdd += "<td><select id=\"" + CONSTANTS.FirstSelectPrefix + index + _id + "\" class=\"notUniform cntrl150\" style=\"width:" + settings.firstColWidth + "px\" onkeydown=\"return enterKeyFilter(event);\"></select></td>";
            htmlToAdd += "<td><select id=\"" + CONSTANTS.SecondSelectPrefix + index + _id + "\" class=\"notUniform cntrl100\" style=\"width:" + settings.secondColWidth + "px\" onkeydown=\"return enterKeyFilter(event);\"></select></td>";
            if (settings.UseIntellisense === true) {
                htmlToAdd += "<td><input class=\"" + CSS_CLASSES.intellisense + "\" type=\"text\" id=\"" + CONSTANTS.ThirdTextPrefix + _id + index + "\" style=\"width:" + settings.firstColWidth + "px\" onkeydown=\"return enterKeyFilter(event);\"></input></td>";
            }
            else {
                htmlToAdd += "<td><input  type=\"text\" id=\"" + CONSTANTS.ThirdTextPrefix + _id + index + "\" style=\"width:" + settings.firstColWidth + "px\" onkeydown=\"return enterKeyFilter(event);\"></input></td>";
            }
            htmlToAdd += "<td><span class=\"addImg16x16\" id=\"" + CONSTANTS.AddButtonPrefix + index + _id + "\"></span></td>";
            htmlToAdd += "<td><span class=\"crossImg16x16\" id=\"" + CONSTANTS.DeleteButtonPrefix + index + _id + "\"></span></td>";
            htmlToAdd += "</tr>";
            return htmlToAdd;
        }
        function _getOflnTableColDetailObjectFromHid() {
            var hidOflnTableColDetJson = $('hidden[id$="hidOflnTableColDetJson"]');
            var objTblColDtlJson = {};
            if (hidOflnTableColDetJson) {
                strTblColJson = $(hidOflnTableColDetJson).val();
            }
            if (strTblColJson) {
                objTblColDtlJson = $.parseJSON(strTblColJson);
            }
            return objTblColDtlJson;
        }
        function _getCntlOFDBDatabindingTable() {
            return $("#tblCntrlDataBindingOflnColMap");
        }
        function _bindDropdown(dropDownOptionsObject, dropdownToBind) {
            if (dropDownOptionsObject && $.isArray(dropDownOptionsObject) && dropDownOptionsObject.length > 0) {
                var strOptionForDdls = "";
                for (var i = 0; i <= dropDownOptionsObject.length - 1; i++) {
                    strOptionForDdls += "<option value=\"" + dropDownOptionsObject[i].val + "\">" + dropDownOptionsObject[i].text + "</option>";
                } //.end
                $(dropdownToBind)
                .html(strOptionForDdls);
                //.val('-1');
            }
        }
        function _bindCntrlOFDBBindingCondLogicOperator(value /*string value selected in the dropdown*/, ddlToBind) {
            if (typeof value === "string" && value) {
                var strOptionForDdls = "<option value=\"-1\">Select</option>";
                if (MF_IDE_CONSTANTS.OfflineDbColType.String.toLowerCase() === value.toLowerCase) {
                    for (var i = 0; i <= MF_IDE_CONSTANTS.CntlOFDBBindingStringOperator.length - 1; i++) {
                        strOptionForDdls += "<option value=\"" + MF_IDE_CONSTANTS.CntlOFDBBindingStringOperator[i] + ">" + MF_IDE_CONSTANTS.CntlOFDBBindingStringOperator[i] + "</option>";
                    }
                }
                else {
                    for (var i = 0; i <= MF_IDE_CONSTANTS.CntlOFDBBindingNumberOperator.length - 1; i++) {
                        strOptionForDdls += "<option value=\"" + MF_IDE_CONSTANTS.CntlOFDBBindingNumberOperator[i] + ">" + MF_IDE_CONSTANTS.CntlOFDBBindingNumberOperator[i] + "</option>";
                    }
                }
                $(ddlToBind).find('option')
                .remove()
                .end()
                .append(strOptionForDdls);
                //.val('-1');
            }
        }
        function _deleteTableRowOfSender(sender) {
            if (sender) {
                var confirmDelete = confirm('Are you sure you want to remove the column');
                if (confirmDelete) {
                    var tr = $(sender).closest('tr');
                    tr.remove();
                    if (_noOfRowsInTable() === 0) {
                        _addCntrlColMapRowToTable();
                    }
                    //tr.fadeOut(400, function () {
                    //
                    //});
                }
            }
        }
        function _noOfRowsInTable() {
            return ($self.find('tbody tr')).length;
        }
        function formMappedCompleteJSON() {
            var matchingType = MatchTypeEnum.All;
            if (settings.ShowMatchType || settings.ShowMatchType === true) {
                matchingType = $self.find('.' + CSS_CLASSES.MatchTypeDdl).val();
            }
            var aryConditions = [];
            $self.find('tbody tr').each(function () {
                var $firstDropDown = $(this).find("[id^=\"" + CONSTANTS.FirstSelectPrefix + "\"]");
                var $condlLogicDropDown = $(this).find("[id^=\"" + CONSTANTS.SecondSelectPrefix + "\"]");
                var $intlsTextBox = $(this).find(":text[id^=\"" + CONSTANTS.ThirdTextPrefix + "\"]");
                aryConditions.push(getMappedDataDtlObject($firstDropDown, $condlLogicDropDown, $intlsTextBox));
            });
            return {
                matchingType: matchingType,
                conditions: aryConditions
            };
        }
        function _getMatchTypeFromSavedData(savedData) {
            var matchingType = MatchTypeEnum.Any;
            if (savedData) {
                matchingType = savedData.matchingType;
            }
            return matchingType;
        }
        function _getConditionsFromSavedData(savedData) {
            var aryConditions = [];
            if (savedData) {
                aryConditions = savedData.conditions;
            }
            return aryConditions;
        }
        function getMappedDataDtlObject(firstDdl, secondDdl, thirdControl) {
            var objCompleteDataObject = new Object(),
                strIntlTextVal = $(thirdControl).val();
            if (settings.SelectFirstDdlValBy === "Value") {
                objCompleteDataObject[settings.FirstColumnTagForJson] = $(firstDdl).val();
            }
            else {
                objCompleteDataObject[settings.FirstColumnTagForJson] = $(firstDdl).children('option:selected').text();
            }
            if (settings.SelectCondLogicDdlValBy === "Value") {
                objCompleteDataObject[settings.SecondColumnTagForJson] = $(secondDdl).val();
            }
            else {
                objCompleteDataObject[settings.SecondColumnTagForJson] = $(secondDdl).children('option:selected').text();
            }
            if (strIntlTextVal) {
                objCompleteDataObject[settings.ThirdColumnTagForJson] =
                    MF_HELPERS.intlsenseHelper.getTextForObjectProperty(strIntlTextVal);
            }
            else {
                objCompleteDataObject[settings.ThirdColumnTagForJson] = "";
            }
            return objCompleteDataObject;
        }
        function _parseSavedJson() {
            return $.parseJSON(settings.SavedJsonIfEdit);
        }
        function _fillSavedMappedConditionalData(savedObjectByIndex, index) {
            var tablerow = $self.find("tbody tr")[index];
            var $firstDropDown = $(tablerow).find("[id^=\"" + CONSTANTS.FirstSelectPrefix + "\"]");
            var $condlLogicDropDown = $(tablerow).find("[id^=\"" + CONSTANTS.SecondSelectPrefix + "\"]");
            var $intlsTextBox = $(tablerow).find(":text[id^=\"" + CONSTANTS.ThirdTextPrefix + "\"]"),
                strThirdColVal = savedObjectByIndex[settings.ThirdColumnTagForJson];
            if (settings.SelectFirstDdlValBy === "Value") {
                $firstDropDown.val(savedObjectByIndex[settings.FirstColumnTagForJson]);
            }
            else {
                $firstDropDown.children("option:contains(" + savedObjectByIndex[settings.FirstColumnTagForJson] + ")").attr('selected', true);
            }
            if (settings.SelectCondLogicDdlValBy === "Value") {
                $condlLogicDropDown.val(savedObjectByIndex[settings.SecondColumnTagForJson]);
            }
            else {
                $condlLogicDropDown.children("option:contains(" + savedObjectByIndex[settings.SecondColumnTagForJson] + ")").attr('selected', true);
            }
            if (strThirdColVal) {
                $intlsTextBox.val(MF_HELPERS.intlsenseHelper.getTextForUI(strThirdColVal));
            }
            else {
                $intlsTextBox.val("");
            }
        }
        function _fillMatchingTypeDropdown(matchingType) {
            $self.find('.' + CSS_CLASSES.MatchTypeDdl).val(matchingType);
        }
        function checkIfTextValExistsInJson(textVal) {
            var blnIsAllValuesCorrect = true;
            var aryOfRowsAdded = $tblAdded.find('tbody').children('tr');
            //var aryAllPossibleIntlsVlue = getAllPossibleFinalIntlsProperty();
            $.each(aryOfRowsAdded, function (index, value) {
                var $IntlsTextBox = $(value).find("[id^=" + CONSTANTS.ThirdTextPrefix + "]");
                if ($IntlsTextBox) {
                    if ($IntlsTextBox.val() === "") {
                        return true;
                    }
                    else {
                        blnIsAllValuesCorrect = settings.intlsValidationFunction(
                            $IntlsTextBox.val(),
                            settings.IntlsJson
                        );
                    }
                    //else if ($IntlsTextBox.val() && $.inArray($IntlsTextBox.val(), aryAllPossibleIntlsVlue) !== -1) {
                    //return true;
                    //}
                    //else {
                    //  blnIsAllValuesCorrect = false;
                    //return false;
                    //}
                }

            });
            return blnIsAllValuesCorrect;
        }





        function getAllPossibleFinalIntlsProperty() {
            var intlsense = JSON.parse(settings.IntlsJson);
            var aryIntlsFronJson = $.parseJSON(JSON.stringify(intlsense));
            var strVlue = "",
                aryOfPossibleCombintionFromObject = [],
                i = 0,
                aryFinalCombintionsOfText = [],
                aryArtOfFinalPropertirs = [];

            if (aryIntlsFronJson.length > 0) {
                for (i = 0; i <= aryIntlsFronJson.length - 1; i++) {
                    var strPropFinalText = "";
                    aryOfPossibleCombintionFromObject = [];
                    aryOfPossibleCombintionFromObject = _getPossibleTextValsFromObject(aryIntlsFronJson[i], strPropFinalText);
                    for (var k = 0; k <= aryOfPossibleCombintionFromObject.length - 1; k++) {
                        aryArtOfFinalPropertirs.push(aryOfPossibleCombintionFromObject[k]);
                    }
                }
            }
            i = 0;
            for (i = 0; i <= aryArtOfFinalPropertirs.length - 1; i++) {
                var aryOfFinlCOmbination = aryArtOfFinalPropertirs[i];
                if (typeof aryOfFinlCOmbination === "string") {
                    aryFinalCombintionsOfText.push(aryOfFinlCOmbination);
                }
                else if ($.isArray(aryOfFinlCOmbination)) {
                    for (var k = 0; k <= aryOfFinlCOmbination.length - 1; k++) {
                        aryFinalCombintionsOfText.push(aryOfFinlCOmbination[k]);
                    }
                }
            }
            return aryFinalCombintionsOfText;
        }
        function _getPossibleTextValsFromObject(propObject, previousStringEvluted /*string*/) {
            var aryFinalCombintionsOfText = [];
            var aryOfCombinationsInnerProp = [];
            if ($.isPlainObject(propObject) && propObject.hasOwnProperty("prop")) {
                previousStringEvluted = addTextFromPropObject(propObject, previousStringEvluted);
                if (propObject.hasOwnProperty("ip")) {
                    aryOfCombinationsInnerProp = _loopThroughInnerProperty(propObject.ip, previousStringEvluted);
                }
            }
            if (aryOfCombinationsInnerProp.length > 0) {
                for (var i = 0; i <= aryOfCombinationsInnerProp.length - 1; i++) {
                    aryFinalCombintionsOfText.push(aryOfCombinationsInnerProp[i]);
                }
                return aryFinalCombintionsOfText;
            }
            else {
                //aryFinalCombintionsOfText.push(previousStringEvluted);
                return previousStringEvluted;
            }

        }
        function addTextFromPropObject(propObject, prevuousPropText) {
            if (prevuousPropText === "") {
                return propObject.prop;
            }
            else {
                return prevuousPropText + "." + propObject.prop;
            }

        }
        function _loopThroughInnerProperty(innerPropArryObject, previousPropTextEvaluated) {
            var aryOfCombinationsInnerProp = [];

            if ($.isArray(innerPropArryObject) && innerPropArryObject.length > 0) {
                for (var i = 0; i <= innerPropArryObject.length - 1; i++) {
                    var strStrtString = previousPropTextEvaluated;
                    if ($.isPlainObject(innerPropArryObject[i])) {
                        aryOfCombinationsInnerProp.push(_getPossibleTextValsFromObject(innerPropArryObject[i], strStrtString));
                    }

                }
            }
            return aryOfCombinationsInnerProp;
        }
        //private methods
    };

})(jQuery);

(function ($) {
    $.fn.viewOnEnter = function (opts) {
        //eg : inputJson {source:[//array of view id,name{id="",name=""}],controls:[//array of control object control name and type}name:"",type:""}}
        var DataKey_SourceCont_SourceDtl = "sourceDtl";
        var ID_OF_START_UP_VIEW_CONT = "0";
        var defaults = {
            srcsCntrlsJson: null,
            intellisenseJson: "", //stringified Json
            editData: [], //array of onEnter data of view
            highlightInvalidMapping: true
        };
        var constants = {
            containerClass: "sourceContainer",
            containerIdPrefix: "sourceId",
            allSourceContainer: "onEnterSourcesContainer",
            headingDivClass: "ViewIntHeadingText",
            headingDivIdPrefix: "headingDiv",
            checkBoxClass: "chkSelect",
            spanControlName: "cntrlName",
            spanControlType: "cntrlType",
            intellisense: "intlsense",
            sourceSelectorDdl :"SrcSelectorDdl"
        };
        var options = $.extend(defaults, opts);
        
        var $self = $(this);
        
        var _srcSelectorDdl = (function(){
            
            function _setEvent(){
                var $ddlSrcSelector = _getSrcSelectorDdl();
                $ddlSrcSelector.on('change',function(evnt){
                    var $self =$(this);
                    _showHideContDivsById(constants.containerIdPrefix +'_'+ $self.val());
                });
            }
            function _getSrcSelectorDdl(){
                var $mainContainerDiv = $self;
                return $mainContainerDiv.find('.'+constants.sourceSelectorDdl);
            }
            
            function _showHideContDivBySelectedVal(){
                var $ddlSrcSelector = _getSrcSelectorDdl();
                _showHideContDivsById(constants.containerIdPrefix+'_'+ $ddlSrcSelector.val());
            }
            return {
                setEvent : function(){
                    _setEvent();
                },
                showHideContDivBySelectedVal: function(){
                    _showHideContDivBySelectedVal();
                },
                setOptions : function(options){
                    var $ddlSrcSelector = _getSrcSelectorDdl();
                    $ddlSrcSelector.html(options);
                },
                setValue : function(value){
                    var $ddl = _getSrcSelectorDdl();
                    $ddl.val(value);
                    $.uniform.update($ddl);
                },
                makeUniform : function(){
                    _getSrcSelectorDdl().uniform({selectAutoWidth : true});
                },
                setId:function(){
                    _getSrcSelectorDdl().attr('id',"ONENTER_SRC_SELECTOR_DDL");
                }
            };
        })();
        
        var _contDivData = (function(){
            
            return {
                setDataFromSrcInfo : function(contDiv,sourceInfo,contIndex,nameInUI,contDivIdSuffix){
                    contDiv.data(DataKey_SourceCont_SourceDtl,{
                                    id:sourceInfo.id,
                                    name:sourceInfo.name,
                                    contIndex:contIndex,
                                    immediateSrcId:sourceInfo.immediateSrcId,
                                    nameInUI:nameInUI,
                                    divIdSuffix : contDivIdSuffix
                                });
                },
                getDataOfContDiv : function(contDiv){
                    return contDiv.data(DataKey_SourceCont_SourceDtl);
                },
                getId : function(divData){
                    return divData.id;
                },
                getName : function(divData){
                    return divData.name;
                },
                getContIndex : function(divData){
                    return divData.contIndex;
                },
                getImmediateSrcId : function(divData){
                    return divData.immediateSrcId;
                },
                getNameInUI : function(divData){
                    return divData.nameInUI;
                },
                getDivIdSuffix : function(divData){
                    return divData.divIdSuffix;
                }
            };
        })();
        
        var _validateErrorDtl = (function(){
            return {
                getErrorDtlObj : function(isAnyValInvalid,isLabelInvalid,srcsWithError){
                    return {
                        isAnyValInvalid : isAnyValInvalid,
                        isLabelInvalid : isLabelInvalid,
                        srcsWithError : srcsWithError
                    };
                },
                getInAnyValInvalid : function(error){
                    return error.isAnyValInvalid;
                },
                getIsLabelInvalid : function(error){
                    return error.isLabelInvalid;
                },
                getSrcsWithError : function(error){
                    return error.srcsWithError;
                }
            };
        })();
        
        if (options.srcsCntrlsJson) {
            //remove previous html
            $self.children().remove();
            var controls = options.srcsCntrlsJson.controls;
            var sources = options.srcsCntrlsJson.sources;
            var strViewTitleHtml = "",
                strOptionsForSrcDdl= "",
                i=0,
                strContDivIdSuffix = "";
            if (sources && $.isArray(sources) && sources.length > 0) {
                
                var $containerDiv = "",
                    $headingDiv = "",
                    $viewTitleDiv = "",
                    $divSourceSelectorDiv = "",
                    $allSourcesContainerDiv = $([]);
                
                $divSourceSelectorDiv  = $('<div class="mrgb10 fr SourceSelectorDiv"></div>').addClass('SelectorDiv');
                $divSourceSelectorDiv.html('<div class="fl"><span style="position:relative;top:10px">Source View :</span></div><div class="fl"><select style="width:220px" class=" '+constants.sourceSelectorDdl+'"></select></div>');
                $self.append($divSourceSelectorDiv);
                $self.append($('<div class="clear"></div>'));
                $allSourcesContainerDiv = $('<div></div>').addClass(constants.allSourceContainer);
                
                i=0;
                for (i = 0; i <= sources.length - 1; i++) {
                    
                    strContDivIdSuffix = sources[i].id+'_'+(sources[i].id === "0"?"0":sources[i].immediateSrcId);
                    $containerDiv = $('<div></div>')
                                    .addClass(constants.containerClass)
                                    .addClass('hide')
                                    .attr('id', constants.containerIdPrefix +'_'+strContDivIdSuffix);
                    
                    //$containerDiv.data(DataKey_SourceCont_SourceDtl, {id:sources[i].id,name:sources[i].name,contIndex:i,immediateSrcId:sources[i].immediateSrcId});
                    _contDivData.setDataFromSrcInfo($containerDiv,sources[i],i,sources[i].id === "0"?"Startup View":sources[i].name,strContDivIdSuffix);
                    /*$headingDiv = $('<div></div>').addClass(constants.headingDivClass)
                                .attr('id', constants.headingDivIdPrefix + strContDivIdSuffix);*/

                    if (sources[i].id === ID_OF_START_UP_VIEW_CONT) {
                        //$headingDiv.html("Startup View");
                        strOptionsForSrcDdl += '<option value="'+strContDivIdSuffix+'">'+"Startup View"+'</option>';
                    }
                    else {
                        //$headingDiv.html("If source view is : " + sources[i].name);
                        strOptionsForSrcDdl += '<option value="'+strContDivIdSuffix+'">'+sources[i].name+'</option>';
                    }

                    $viewTitleDiv = $('<div></div>')
                                    .attr('id', constants.headingDivIdPrefix + sources[i].name + "viewTitle");
                    strViewTitleHtml = "";
                    strViewTitleHtml += '<table class="detailsTable ViewTitleCont">';
                    strViewTitleHtml += '<thead><tr ><th class="w_50p"></th><th>Initialize View Title</th><th></th><th></th></tr></thead>';
                    strViewTitleHtml += '<tbody><tr><td class="w_50p"></td><td><span>Template*:</span></td>';
                    strViewTitleHtml += '<td><input id="' + 'txt_' + sources[i].name + '_ViewTitle_Template" type="text" style="width:95%;" onkeydown=\"return enterKeyFilter(event);\"></td><td></td></tr>';
                    strViewTitleHtml += '<tr><td class="w_50p"></td><td><span>Parameter:</span></td>';
                    strViewTitleHtml += '<td><input id="' + 'txt_' + sources[i].name + '_ViewTitle_Parameter" type="text" class="intlsense ViewTitle" onkeydown=\"return enterKeyFilter(event);\"></td><td></td></tr>';
                    strViewTitleHtml += '<tr><td class="w_50p"></td><td><span style="font-size: smaller; font-style: italic;">(*maximum one parameter allowed)</span></td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td></tr></tbody></table>';
                    $viewTitleDiv.html(strViewTitleHtml);
                    if (!controls || !$.isArray(controls)) {
                        controls = [];
                    }
                    var $innerHtml = _getSourceContainerInnerHtml(sources[i].name, controls);
                    //$containerDiv.append($headingDiv);
                    $containerDiv.append($viewTitleDiv);
                    $containerDiv.append($innerHtml);
                    $allSourcesContainerDiv.append($containerDiv);
                }
                $self.append($allSourcesContainerDiv);
                
                $self.find('input.intlsense').textIntellisense({
                    IntlsJson: options.intellisenseJson
                });
                
                _srcSelectorDdl.setOptions(strOptionsForSrcDdl);
                _srcSelectorDdl.setEvent();
                _srcSelectorDdl.showHideContDivBySelectedVal();
                _srcSelectorDdl.setId();
                _srcSelectorDdl.makeUniform();
                _setValidateOnBlurOfViewTitleTxt();
            }
            //TODO if No sourceContainer//Not required

            if ($.isArray(options.editData) && options.editData.length > 0) {
                _putDataInHtmlForEdit();
            }
            $self.data(MF_IDE_CONSTANTS.jqueryDataKeys.viewOnEnterTblCont, {
                finalSaveData: _getFinalSaveData,
                validate: ""
            });
        }
        
        
        function _showHideContDivsById(divId){
            var $mainContDiv = $self;
            $mainContDiv.find('.'+constants.containerClass).hide();
            $mainContDiv.find('#'+divId).show();
        }
        
        function _getSourceContainerInnerHtml(sourceName, controls) {
            // create table
            var $table = $('<table class="CntrlInits">');
            $table.addClass('detailsTable');
            // caption
            //$table.append('<caption>MyTable</caption>')
            // thead
            $table.append('<thead>').children('thead')
            .append('<tr />').children('tr').append('<th></th><th>Initialize Control</th><th>Type</th><th>Value</th>');

            //tbody
            var $tbody = $table.append('<tbody />').children('tbody');

            // add row
            if (controls.length > 0) {
                for (var i = 0; i <= controls.length - 1; i++) {
                    var $tr = $('<tr></tr>');
                    var $td = $('<td class="w_50p"></td>');
                    var strControlName = controls[i].userDefinedName;
                    var strControlType = controls[i].type.UIName;
                    var $checkbox = $('<input></input>', {
                        id: "chk_" + sourceName + "_" + strControlName,
                        click: function () {
                            //alert('Foo has been clicked!');
                            var $self = $(this);
                            var type = $self.closest('tr').find('.cntrlType').html(),
                            $tr = $([]);
                            if (type === MF_IDE_CONSTANTS.CONTROLS.LABEL.type) {
                                for (var n = 1; n <= 3; n++) {
                                    $tr = $table[0].rows['tr_' + $self[0].id.split('_')[1] + '_' + $self[0].id.split('_')[2] + '_' + n];
                                    $.each($('.intlsense', $tr), function () {
                                        if ($self.is(':checked')) {
                                            $($tr).removeClass('hide');
                                            $(this).removeAttr('disabled');
                                        }
                                        else {
                                            $($tr).addClass('hide');
                                            $(this).val('');
                                            $(this).removeClass('errorTxtBox');
                                            $(this).attr('disabled', 'disabled');
                                        }
                                    });
                                }
                            }
                            else if (type === MF_IDE_CONSTANTS.CONTROLS.LOCATION.type) {
                                for (var k = 1; k <= 2; k++) {
                                    var tr = $table[0].rows['tr_' + $self[0].id.split('_')[1] + '_' + $self[0].id.split('_')[2] + '_' + k];
                                    $tr = $(tr);
                                    if ($self.is(':checked')) {
                                        $tr.removeClass('hide');
                                        $.each($('.intlsense', $tr), function () {
                                            $(this).removeAttr('disabled');
                                        });
                                    }
                                    else {
                                        $tr.addClass('hide');
                                        $.each($('.intlsense', $tr), function () {
                                            $(this).val('');
                                            $(this).removeClass('errorTxtBox');
                                            $(this).attr('disabled', 'disabled');
                                        });
                                    }
                                }
                            }
                            else {
                                var $intlsText = $self.closest('tr').find('.intlsense');
                                if ($self.is(':checked')) {
                                    $intlsText.removeAttr('disabled');
                                }
                                else {
                                    $intlsText.val('');
                                    $intlsText.removeClass('errorTxtBox');
                                    $intlsText.attr('disabled', 'disabled');
                                }
                            }
                        },
                        type: "checkbox",
                        "class": constants.checkBoxClass + " "
                    });
                    $td.append($checkbox);
                    $tr.append($td);

                    $td = $('<td></td>');
                    var $spanCntrlName = $('<span class="cntrlName font-bold"></span>').html(strControlName);
                    $td.append($spanCntrlName);
                    $tr.append($td);

                    $td = $('<td></td>');
                    var $spanCntrlType = $('<span class="cntrlType font-bold"></span>').html(strControlType);
                    $td.append($spanCntrlType);
                    $tr.append($td);

                    if (strControlType === MF_IDE_CONSTANTS.CONTROLS.LABEL.type) {
                        $td = $('<td></td>');
                        $tr.append($td);
                        $tbody.append($tr);

                        //First Row [Parameter1% && Parameter2%]
                        $tr = $('<tr id="tr_' + sourceName + "_" + strControlName + "_" + 1 + '" class="hide"></tr>');
                        $td = $('<td></td>');
                        $tr.append($td);

                        $td = _addValueTextbox("txt_" + sourceName + "_" + strControlName + "_" + 1, "1");
                        $tr.append($td);

                        $td = $('<td></td>');
                        $tr.append($td);

                        $td = _addValueTextbox("txt_" + sourceName + "_" + strControlName + "_" + 2, "2");
                        $tr.append($td);
                        $tbody.append($tr);

                        //Second Row [Parameter3% && Parameter4%]
                        $tr = $('<tr id="tr_' + sourceName + "_" + strControlName + "_" + 2 + '"  class="hide"></tr>');
                        $td = $('<td></td>');
                        $tr.append($td);

                        $td = _addValueTextbox("txt_" + sourceName + "_" + strControlName + "_" + 3, "3");
                        $tr.append($td);

                        $td = $('<td></td>');
                        $tr.append($td);

                        $td = _addValueTextbox("txt_" + sourceName + "_" + strControlName + "_" + 4, "4");
                        $tr.append($td);
                        $tbody.append($tr);

                        //Third Row [Parameter5%]
                        $tr = $('<tr id="tr_' + sourceName + "_" + strControlName + "_" + 3 + '"  class="hide"></tr>');
                        $td = $('<td></td>');
                        $tr.append($td);

                        $td = _addValueTextbox("txt_" + sourceName + "_" + strControlName + "_" + 5, "5");
                        $tr.append($td);
                        
                        $td = $('<td></td>');
                        $tr.append($td);
                        
                        $td = $('<td></td>');
                        $tr.append($td);
                        $tbody.append($tr);
                    }
                    else if (strControlType === MF_IDE_CONSTANTS.CONTROLS.LOCATION.type) {
                        $td = $('<td></td>');
                        $tr.append($td);
                        $tbody.append($tr);

                        $tr = $('<tr id="tr_' + sourceName + "_" + strControlName + "_" + 1 + '" class="hide"></tr>');
                        $td = $('<td></td>');
                        $tr.append($td);

                        $td = _getTdWithValueTextboxForSpecialCntrls(
                                    "txt_" + sourceName + "_" + strControlName + "_" + 1, "Latitude");
                        $tr.append($td);

                        $td = $('<td></td>');
                        $tr.append($td);

                        $td = _getTdWithValueTextboxForSpecialCntrls(
                                    "txt_" + sourceName + "_" + strControlName + "_" + 2, "Longitude");
                        $tr.append($td);

                        $tbody.append($tr);
                    }
                    else {
                        $td = $('<td></td>');
                        var $intlsTextBox = $('<input></input>', {
                            id: "txt_" + sourceName + "_" + strControlName,
                            type: "text",
                            "class": "intlsense"
                        });
                        if (options.highlightInvalidMapping === true) {
                            $intlsTextBox.on('blur', _validateIntellisenseValOnBlur);
                        }
                        $intlsTextBox.attr('disabled', 'disabled');
                        $intlsTextBox.attr('onkeydown', 'return enterKeyFilter(event);');
                        $td.append($intlsTextBox);
                        $tr.append($td);

                        $tbody.append($tr);
                    }
                }
            }
            $table.append($tbody);
            return $table;
        }
        function _getTdWithValueTextboxForSpecialCntrls(txtBoxId, labelName) {
            var $td = $('<td></td>');
            var $span = $('<span>' + labelName + '</span>');
            $td.append($span);
            var $intlsTextBox = $('<input></input>', {
                id: txtBoxId,
                type: "text",
                "class": "intlsense"
            });
            if (options.highlightInvalidMapping === true) {
                $intlsTextBox.on('blur', _validateIntellisenseValOnBlur);
            }
            $intlsTextBox.attr('disabled', 'disabled');
            $intlsTextBox.attr('onkeydown', 'return enterKeyFilter(event);');
            $td.append($intlsTextBox);

            return $td;
        }


        function _addValueTextbox(id, count) {
            var $td = $('<td></td>');
            var $span = $('<span>Parameter-' + count + ' (%'+count+')</span>');
            $td.append($span);

            var $intlsTextBox = $('<input></input>', {
                id: id,
                type: "text",
                "class": "intlsense"
            });
            if (options.highlightInvalidMapping === true) {
                $intlsTextBox.on('blur', _validateIntellisenseValOnBlur);
            }
            $intlsTextBox.attr('disabled', 'disabled');
            $intlsTextBox.attr('onkeydown', 'return enterKeyFilter(event);');
            $td.append($intlsTextBox);

            return $td;
        }
        function _setValidateOnBlurOfViewTitleTxt() {
            $self.find(".ViewTitle").on('blur', _validateIntellisenseValOnBlur);
        }
        function _validateValue(value, intlsJsonObjs, intlsAllCombination) {
            var isValid = false;
            if (value) {
                if (!MF_HELPERS.intlsenseHelper.isValueValid({
                    val: value,
                    ignoreUserConst: false,
                    isEmptyValid: false,
                    intlsArray: intlsJsonObjs,
                    intlsAllCombination: intlsAllCombination
                })) {
                    isValid = false;
                }
                else {
                    isValid = true;
                }
            }
            return isValid;
        }
        function _validateIntellisenseValOnBlur(evnt) {
            if (options.intellisenseJson) {
                var intlsJson = $.parseJSON(options.intellisenseJson);
                var aryAllPossibleIntlsValue = MF_HELPERS.intlsenseHelper.getAllPossibleFinalIntlsProperty(intlsJson);
                var $textbox = $(evnt.target);
                var txtVal = $textbox.val();
                if (txtVal !== "") {
                    //if(!MF_HELPERS.intlsenseHelper.isValueValid({
                    // val:txtVal,
                    //ignoreUserConst:false,
                    //isEmptyValid:true,
                    //intlsArray:intlsJson,
                    //intlsAllCombination:aryAllPossibleIntlsValue
                    //})){

                    //}
                    //else{
                    //$textbox.removeClass('errorTxtBox');

                    //}
                    if (!_validateValue(txtVal, intlsJson, aryAllPossibleIntlsValue)) {
                        MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($textbox, false);
                    }
                    else {
                        MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($textbox, true);
                    }
                }
                else {
                    //$textbox.removeClass('errorTxtBox');
                    MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($textbox, true);
                }
            }
        }
        function _getFinalSaveData() {
            //TODO VALIDATE
            // var aryErrors = _validateSaving();
            // if (aryErrors && $.isArray(aryErrors) && aryErrors.length > 0) {
            //     throw new MfError(aryErrors);
            // }
            var objErrDtls = _validateSaving(),
                arySrcsWithError =[],
                strErrorMsg = '<p> Invalid value mapped to controls in source view : </p><ul>',
                z=0,
                aryErrors = [];
            if (objErrDtls) {
                if(_validateErrorDtl.getInAnyValInvalid(objErrDtls)){
                    arySrcsWithError =_validateErrorDtl.getSrcsWithError(objErrDtls);
                    for(z=0;z<=arySrcsWithError.length-1;z++){
                        strErrorMsg += '<li> <b>'+_contDivData.getNameInUI(arySrcsWithError[z])+'</b></li>';
                    }
                    strErrorMsg += '</ul>';
                    
                    aryErrors.push(strErrorMsg);
                    if(_validateErrorDtl.getIsLabelInvalid(objErrDtls)){
                        aryErrors.push("Atleast one parameter of label should be mapped with a correct value");
                    }
                    _srcSelectorDdl.setValue(_contDivData.getDivIdSuffix(arySrcsWithError[0]));
                    _srcSelectorDdl.showHideContDivBySelectedVal();
                    throw new MfError(aryErrors);
                }
            }
            var aryFinalData = [];
            $self.find('.' + constants.containerClass).each(function (index) {
                var $container = $(this);
                var divData = $container.data(DataKey_SourceCont_SourceDtl);
                var $table = $self.find('.CntrlInits');
                var $trViewTitle = $container.find('.ViewTitleCont tbody tr');
                var $tr = $container.find('.CntrlInits tbody tr');
                var objOnEnter = new ViewOnEnter();
                //objOnEnter.sourceId = divData.id;
                objOnEnter.fnSetSourceId(divData.id);
                objOnEnter.fnSetImmediateSourceId(divData.immediateSrcId);
                //objOnEnter.cntrlInits = [];
                objOnEnter.fnSetCntrlInits([]);
                //                objOnEnter.title = MF_HELPERS.intlsenseHelper.getTextForObjectProperty(
                //                                    $trViewTitle.find('#txt_' + divData.name + '_ViewTitle_Template').val());
                //objOnEnter.title = $trViewTitle.find('#txt_' + divData.name + '_ViewTitle_Template').val();
                objOnEnter.fnSetTitle($trViewTitle.find('#txt_' + divData.name + '_ViewTitle_Template').val());
                //objOnEnter.parameter = MF_HELPERS.intlsenseHelper.getTextForObjectProperty(
                                  // $trViewTitle.find('#txt_' + divData.name + '_ViewTitle_Parameter').val());
                objOnEnter.fnSetParameter(MF_HELPERS.intlsenseHelper.getTextForObjectProperty(
                                          $trViewTitle.find('#txt_' + divData.name + '_ViewTitle_Parameter').val()));
                $tr.each(function (index) {
                    if ($(this)[0].id === "") {
                        var $chkBox = $(this).find('.' + constants.checkBoxClass);
                        if ($chkBox.is(":checked")) {
                            var objCntrlInits = new ControlInitialisation();
                            objCntrlInits.controlName = $(this).find('.' + constants.spanControlName).html();
                            objCntrlInits.controlType = $(this).find('.' + constants.spanControlType).html();
                            var arrCntrlVal = [],
                                cntrlType = $(this).find('.cntrlType').html(),
                                k = 0;

                            if (cntrlType === MF_IDE_CONSTANTS.CONTROLS.LABEL.type) {
                                for (var n = 1; n <= 3; n++) {
                                    //var $row = $table[0].rows['tr_' + $chkBox[0].id.split('_')[1] + '_' + $chkBox[0].id.split('_')[2] + '_' + n];
                                    var $row = $table.find('#'+'tr_' + $chkBox[0].id.split('_')[1] + '_' + $chkBox[0].id.split('_')[2] + '_' + n);
                                    $.each($('.intlsense', $row), function () {
                                        arrCntrlVal.push(MF_HELPERS.intlsenseHelper.getTextForObjectProperty(
                                                            $(this).val()));
                                    });
                                }
                            }
                            else if (cntrlType === MF_IDE_CONSTANTS.CONTROLS.LOCATION.type) {
                                //there is only one row.
                                //var $row = $table[0].rows['tr_' + $chkBox[0].id.split('_')[1] + '_' + $chkBox[0].id.split('_')[2] + '_' + 1];
                                var $row = $(this).next();
                                $.each($('.intlsense', $row), function () {
                                    arrCntrlVal.push(MF_HELPERS.intlsenseHelper.getTextForObjectProperty(
                                                        $(this).val()));
                                });
                            }
                            else {
                                arrCntrlVal.push(MF_HELPERS.intlsenseHelper.getTextForObjectProperty(
                                    $(this).find('.' + constants.intellisense).val()
                                ));
                            }

                            objCntrlInits.controlValue = arrCntrlVal;
                            objOnEnter.fnAddControlInits(objCntrlInits);
                        }
                    }
                });
                aryFinalData.push(objOnEnter);
            });
            return aryFinalData;
        }
        function _putDataInHtmlForEdit() {
            var aryIntellisenseValues = [], objAllPossibleCombinationOfIntlsVals = null;
            if (options.intellisenseJson) {
                aryIntellisenseValues = $.parseJSON(options.intellisenseJson);
                objAllPossibleCombinationOfIntlsVals =
                    MF_HELPERS.intlsenseHelper.getAllPossibleFinalIntlsProperty(aryIntellisenseValues);
            }
            $self.find('.' + constants.containerClass).each(function (index) {
                var $container = $(this);
                var $table = $self.find('.CntrlInits');
                var divData = $container.data(DataKey_SourceCont_SourceDtl);
                var objViewOnEnter = null,
                    strImmediateSourceId = "";
                var aryViewOnEnterBySource = $.grep(options.editData, function (value, index) {
                     strImmediateSourceId = value.fnGetImmediateSourceId();
                     if(mfUtil.isNullOrUndefined(strImmediateSourceId)){
                         return value.fnGetSourceId() === divData.id;
                     }
                     else{
                       return (strImmediateSourceId === divData.immediateSrcId);
                     }
                });
                
                // objViewOnEnterByContIndex = options.editData[divData.contIndex];
                // if(!mfUtil.isNullOrUndefined(objViewOnEnterByContIndex)){
                //     objViewOnEnter = objViewOnEnterByContIndex;
                //     var $trViewTitle = $container.find('.ViewTitleCont tbody tr');
                //     var $txtViewTitle = $trViewTitle.find('#txt_' + divData.name + '_ViewTitle_Template');
                //     var $txtViewParameter = $trViewTitle.find('#txt_' + divData.name + '_ViewTitle_Parameter');
                //     var $tr = $container.find('.CntrlInits tbody tr');
                //     //$txtViewTitle.val(MF_HELPERS.intlsenseHelper.getTextForUI(objViewOnEnter.title));
                //     $txtViewTitle.val(objViewOnEnter.title);
                //     $txtViewParameter.val(MF_HELPERS.intlsenseHelper.getTextForUI(objViewOnEnter.parameter));
                //     if (options.intellisenseJson) {
                //         if (!MF_HELPERS.intlsenseHelper.isValueValid({
                //             val: $txtViewParameter.val(),
                //             ignoreUserConst: false,
                //             isEmptyValid: true,
                //             intlsArray: aryIntellisenseValues,
                //             intlsAllCombination: objAllPossibleCombinationOfIntlsVals
                //         })) {
                //             MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($txtViewParameter, false);
                //         }
                //         else {
                //             MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($txtViewParameter, true);
                //         }
                //     }
                //     $tr.each(function (index) {
                //         if ($(this)[0].id == "") {
                //             var $chkBox = $(this).find('.' + constants.checkBoxClass);
                //             var cntrlName = $(this).find('.' + constants.spanControlName).html();
                //             var cntrlType = $(this).find('.' + constants.spanControlType).html();
                //             var objCntrlInit = $.grep(objViewOnEnter.cntrlInits, function (value, index) {
                //                 return value.controlName === cntrlName;
                //                 //return value.controlName === cntrlName && 
                //                 //value.controlType === cntrlType;
                //             });

                //             if (cntrlType === MF_IDE_CONSTANTS.CONTROLS.LABEL.type) {
                //                 for (var n = 1; n <= 3; n++) {
                //                     //this was not working properly.
                //                     //var $row = $table[0].rows['tr_' + $chkBox[0].id.split('_')[1] + '_' + $chkBox[0].id.split('_')[2] + '_' + n];
                //                     var $row = $table.find('#'+'tr_' + $chkBox[0].id.split('_')[1] + '_' + $chkBox[0].id.split('_')[2] + '_' + n);
                //                     $.each($('.intlsense', $row), function () {
                //                         var index = parseInt($(this)[0].id.split('_')[3], 10) - 1;
                //                         if (objCntrlInit && $.isArray(objCntrlInit) && objCntrlInit.length > 0) {
                //                             $($row).removeClass('hide');
                //                             $(this).removeAttr('disabled')
                //                                 .val(MF_HELPERS.intlsenseHelper.getTextForUI(objCntrlInit[0].controlValue[index]));
                //                             $chkBox.prop('checked', true);
                //                             if (options.intellisenseJson) {
                //                                 if (!MF_HELPERS.intlsenseHelper.isValueValid({
                //                                     val: $(this).val(),
                //                                     ignoreUserConst: false,
                //                                     isEmptyValid: true,
                //                                     intlsArray: aryIntellisenseValues,
                //                                     intlsAllCombination: objAllPossibleCombinationOfIntlsVals
                //                                 })) {
                //                                     MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), false);
                //                                 }
                //                                 else {
                //                                     MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), true);
                //                                 }
                //                             }
                //                         }
                //                         else {
                //                             $($row).addClass('hide');
                //                             $(this).attr('disabled', 'disabled').val('');
                //                             $chkBox.prop('checked', false);
                //                         }
                //                     });
                //                 }
                //             }
                //             else if (cntrlType === MF_IDE_CONSTANTS.CONTROLS.LOCATION.type) {
                //                 var $row = $(this).next();
                //                 if (objCntrlInit && $.isArray(objCntrlInit) && objCntrlInit.length > 0) {
                //                     $row.removeClass('hide');
                //                     $.each($('.intlsense', $row), function (txtIndex) {
                //                         $(this).removeAttr('disabled')
                //                               .val(MF_HELPERS.intlsenseHelper.getTextForUI(objCntrlInit[0].controlValue[txtIndex]));
                //                         $chkBox.prop('checked', true);
                //                         if (options.intellisenseJson) {
                //                             if (!MF_HELPERS.intlsenseHelper.isValueValid({
                //                                 val: $(this).val(),
                //                                 ignoreUserConst: false,
                //                                 isEmptyValid: true,
                //                                 intlsArray: aryIntellisenseValues,
                //                                 intlsAllCombination: objAllPossibleCombinationOfIntlsVals
                //                             })) {
                //                                 MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), false);
                //                             }
                //                             else {
                //                                 MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), true);
                //                             }
                //                         }
                //                     });

                //                 }
                //                 else {
                //                     $row.addClass('hide');
                //                     $.each($('.intlsense', $row), function (index) {
                //                         $(this).attr('disabled', 'disabled').val('');
                //                     });
                //                     $chkBox.prop('checked', false);
                //                 }
                //             }
                //             else {
                //                 var $txtIntls = $(this).find('.' + constants.intellisense);
                //                 if (objCntrlInit && $.isArray(objCntrlInit) && objCntrlInit.length > 0) {
                //                     $txtIntls.removeAttr('disabled')
                //                         .val(MF_HELPERS.intlsenseHelper.getTextForUI(objCntrlInit[0].controlValue[0]));
                //                     $chkBox.prop('checked', true);
                //                     if (options.intellisenseJson) {
                //                         if (!MF_HELPERS.intlsenseHelper.isValueValid({
                //                             val: $txtIntls.val(),
                //                             ignoreUserConst: false,
                //                             isEmptyValid: true,
                //                             intlsArray: aryIntellisenseValues,
                //                             intlsAllCombination: objAllPossibleCombinationOfIntlsVals
                //                         })) {
                //                             MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($txtIntls, false);
                //                         }
                //                         else {
                //                             MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($txtIntls, true);
                //                         }
                //                     }
                //                 }
                //                 else {
                //                     $txtIntls.attr('disabled', 'disabled').val('');
                //                     $chkBox.prop('checked', false);
                //                 }
                //             }
                //         }
                //     });
                // }
                if (aryViewOnEnterBySource &&
                    $.isArray(aryViewOnEnterBySource) &&
                    aryViewOnEnterBySource.length > 0) {

                    objViewOnEnter = aryViewOnEnterBySource[0];
                    var $trViewTitle = $container.find('.ViewTitleCont tbody tr');
                    var $txtViewTitle = $trViewTitle.find('#txt_' + divData.name + '_ViewTitle_Template');
                    var $txtViewParameter = $trViewTitle.find('#txt_' + divData.name + '_ViewTitle_Parameter');
                    var $tr = $container.find('.CntrlInits tbody tr');
                    //$txtViewTitle.val(MF_HELPERS.intlsenseHelper.getTextForUI(objViewOnEnter.title));
                    $txtViewTitle.val(objViewOnEnter.title);
                    $txtViewParameter.val(MF_HELPERS.intlsenseHelper.getTextForUI(objViewOnEnter.parameter));
                    if (options.intellisenseJson) {
                        if (!MF_HELPERS.intlsenseHelper.isValueValid({
                            val: $txtViewParameter.val(),
                            ignoreUserConst: false,
                            isEmptyValid: true,
                            intlsArray: aryIntellisenseValues,
                            intlsAllCombination: objAllPossibleCombinationOfIntlsVals
                        })) {
                            MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($txtViewParameter, false);
                        }
                        else {
                            MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($txtViewParameter, true);
                        }
                    }
                    $tr.each(function (index) {
                        if ($(this)[0].id == "") {
                            var $chkBox = $(this).find('.' + constants.checkBoxClass);
                            var cntrlName = $(this).find('.' + constants.spanControlName).html();
                            var cntrlType = $(this).find('.' + constants.spanControlType).html();
                            var objCntrlInit = $.grep(objViewOnEnter.cntrlInits, function (value, index) {
                                return value.controlName === cntrlName;
                                //return value.controlName === cntrlName && 
                                //value.controlType === cntrlType;
                            });

                            if (cntrlType === MF_IDE_CONSTANTS.CONTROLS.LABEL.type) {
                                for (var n = 1; n <= 3; n++) {
                                    //this was not working properly.
                                    //var $row = $table[0].rows['tr_' + $chkBox[0].id.split('_')[1] + '_' + $chkBox[0].id.split('_')[2] + '_' + n];
                                    var $row = $table.find('#'+'tr_' + $chkBox[0].id.split('_')[1] + '_' + $chkBox[0].id.split('_')[2] + '_' + n);
                                    $.each($('.intlsense', $row), function () {
                                        var index = parseInt($(this)[0].id.split('_')[3], 10) - 1;
                                        if (objCntrlInit && $.isArray(objCntrlInit) && objCntrlInit.length > 0) {
                                            $($row).removeClass('hide');
                                            $(this).removeAttr('disabled')
                                                .val(MF_HELPERS.intlsenseHelper.getTextForUI(objCntrlInit[0].controlValue[index]));
                                            $chkBox.prop('checked', true);
                                            if (options.intellisenseJson) {
                                                if (!MF_HELPERS.intlsenseHelper.isValueValid({
                                                    val: $(this).val(),
                                                    ignoreUserConst: false,
                                                    isEmptyValid: true,
                                                    intlsArray: aryIntellisenseValues,
                                                    intlsAllCombination: objAllPossibleCombinationOfIntlsVals
                                                })) {
                                                    MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), false);
                                                }
                                                else {
                                                    MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), true);
                                                }
                                            }
                                        }
                                        else {
                                            $($row).addClass('hide');
                                            $(this).attr('disabled', 'disabled').val('');
                                            $chkBox.prop('checked', false);
                                        }
                                    });
                                }
                            }
                            else if (cntrlType === MF_IDE_CONSTANTS.CONTROLS.LOCATION.type) {
                                var $row = $(this).next();
                                if (objCntrlInit && $.isArray(objCntrlInit) && objCntrlInit.length > 0) {
                                    $row.removeClass('hide');
                                    $.each($('.intlsense', $row), function (txtIndex) {
                                        $(this).removeAttr('disabled')
                                              .val(MF_HELPERS.intlsenseHelper.getTextForUI(objCntrlInit[0].controlValue[txtIndex]));
                                        $chkBox.prop('checked', true);
                                        if (options.intellisenseJson) {
                                            if (!MF_HELPERS.intlsenseHelper.isValueValid({
                                                val: $(this).val(),
                                                ignoreUserConst: false,
                                                isEmptyValid: true,
                                                intlsArray: aryIntellisenseValues,
                                                intlsAllCombination: objAllPossibleCombinationOfIntlsVals
                                            })) {
                                                MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), false);
                                            }
                                            else {
                                                MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), true);
                                            }
                                        }
                                    });

                                }
                                else {
                                    $row.addClass('hide');
                                    $.each($('.intlsense', $row), function (index) {
                                        $(this).attr('disabled', 'disabled').val('');
                                    });
                                    $chkBox.prop('checked', false);
                                }
                            }
                            else {
                                var $txtIntls = $(this).find('.' + constants.intellisense);
                                if (objCntrlInit && $.isArray(objCntrlInit) && objCntrlInit.length > 0) {
                                    $txtIntls.removeAttr('disabled')
                                        .val(MF_HELPERS.intlsenseHelper.getTextForUI(objCntrlInit[0].controlValue[0]));
                                    $chkBox.prop('checked', true);
                                    if (options.intellisenseJson) {
                                        if (!MF_HELPERS.intlsenseHelper.isValueValid({
                                            val: $txtIntls.val(),
                                            ignoreUserConst: false,
                                            isEmptyValid: true,
                                            intlsArray: aryIntellisenseValues,
                                            intlsAllCombination: objAllPossibleCombinationOfIntlsVals
                                        })) {
                                            MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($txtIntls, false);
                                        }
                                        else {
                                            MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($txtIntls, true);
                                        }
                                    }
                                }
                                else {
                                    $txtIntls.attr('disabled', 'disabled').val('');
                                    $chkBox.prop('checked', false);
                                }
                            }
                        }
                    });
                }
            });
        }
        function _validateSaving() {
            var intlsJson = [], objAllPossibleCombinationOfIntlsVals = null;
            //var aryErrorMessages = [];
            var isAnyOneValueInvalid = false,
                isAnyLabelInitInvalid= false,
                isAnyLabelMappingInvalid = true,
                isEveryLabelMappingEmpty= true,
                arySrcsWithError=[],
                isErrorInSrc= false; 
            //TODO change for getting error by index.
            //this will not be rquiued
            if (options.intellisenseJson) {
                intlsJson = $.parseJSON(options.intellisenseJson);
                objAllPossibleCombinationOfIntlsVals =
                    MF_HELPERS.intlsenseHelper.getAllPossibleFinalIntlsProperty(intlsJson);
            }
            $self.find('.' + constants.containerClass).each(function (index) {
                isErrorInSrc = false;
                var $container = $(this);
                var $table = $self.find('.CntrlInits');
                var $trViewTitle = $container.find('.ViewTitleCont tbody tr');
                var $tr = $container.find('.CntrlInits tbody tr');
                var divData = _contDivData.getDataOfContDiv($container);
                var $txtViewTitle = $trViewTitle.find('#txt_' + divData.name + '_ViewTitle_Template');
                var $txtViewParameter = $trViewTitle.find('#txt_' + divData.name + '_ViewTitle_Parameter');
                var strViewTitleMapping = $txtViewParameter.val();

                $tr.each(function (index) {
                    if ($(this)[0].id === "") {
                        var $chkBox = $(this).find('.' + constants.checkBoxClass),
                            cntrlType = $(this).find('.cntrlType').html();
                        
                        if ($chkBox.is(":checked")) {

                            if (cntrlType === MF_IDE_CONSTANTS.CONTROLS.LABEL.type) {
                                //remove all textboxes highlighted
                                if(options.highlightInvalidMapping){
                                    for (var n = 1; n <= 3; n++) {
                                        $row = $table.find('#'+'tr_' + $chkBox[0].id.split('_')[1] + '_' + $chkBox[0].id.split('_')[2] + '_' + n);
                                        $.each($('.intlsense', $row), function () {
                                            MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), true);
                                        });
                                    }
                                }
                                isAnyLabelMappingInvalid = false;
                                isEveryLabelMappingEmpty= true;
                                for (var n = 1; n <= 3; n++) {
                                    //var $row = $table[0].rows['tr_' + $chkBox[0].id.split('_')[1] + '_' + $chkBox[0].id.split('_')[2] + '_' + n];
                                    // if (parseInt(strValue.trim().length, 10) > 0) {
                                    //     if (!_validateValue(strValue, intlsJson, objAllPossibleCombinationOfIntlsVals)) {
                                    //         isAnyOneValueInvalid = true;
                                    //         if (options.highlightInvalidMapping) {
                                    //             MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), false);
                                    //         }
                                    //     }
                                    //     else {
                                    //         MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), true);
                                    //     }
                                    // }
                                    
                                    var $row = $table.find('#'+'tr_' + $chkBox[0].id.split('_')[1] + '_' + $chkBox[0].id.split('_')[2] + '_' + n);
                                    //isAnyLabelMappingValid = false;
                                    $.each($('.intlsense', $row), function () {
                                        var strValue = $(this).val();
                                        if(!mfUtil.isNullOrUndefined(strValue) && !mfUtil.isEmptyString(strValue)){
                                            isEveryLabelMappingEmpty = false;
                                            if (!_validateValue(strValue, intlsJson, objAllPossibleCombinationOfIntlsVals)) {
                                                
                                                isAnyLabelMappingInvalid = true;
                                                isErrorInSrc = true;
                                                if (options.highlightInvalidMapping) {
                                                    MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), false);
                                                }
                                            }
                                        }
                                        
                                    });
                                }
                                if(isEveryLabelMappingEmpty){
                                    //highlight only first text box of the first row..see final value as 1.
                                    $row = $table.find('#'+'tr_' + $chkBox[0].id.split('_')[1] + '_' + $chkBox[0].id.split('_')[2] + '_' + 1);
                                    $.each($('.intlsense', $row), function () {
                                        MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), false);
                                        return false;
                                    });
                                    isAnyLabelInitInvalid = true;
                                    isAnyOneValueInvalid = true;
                                    isErrorInSrc = true;
                                }
                                else if(isAnyLabelMappingInvalid){
                                    isAnyLabelInitInvalid = true;
                                    isAnyOneValueInvalid = true;
                                    
                                    //atleast one textbox is having a value and it is invalid.The textbox is highlighted in above loop
                                    isErrorInSrc = true;
                                }
                            }
                            else if (cntrlType === MF_IDE_CONSTANTS.CONTROLS.LOCATION.type) {
                                //there is only one row.
                                var $row = $(this).next();
                                $.each($('.intlsense', $row), function () {
                                    var strValue = $(this).val();
                                    if (!_validateValue(strValue, intlsJson, objAllPossibleCombinationOfIntlsVals)) {
                                        isAnyOneValueInvalid = true;
                                        if (options.highlightInvalidMapping) {
                                            MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), false);
                                        }
                                        isErrorInSrc = true;
                                    }
                                    else {
                                        MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($(this), true);
                                    }
                                });
                            }
                            else {
                                var $txtIntls = $(this).find('.' + constants.intellisense);
                                var strValue = $txtIntls.val();
                                //TODO GIVE ERROR MESSAGE WITH INDEX
                                if (!_validateValue(strValue, intlsJson, objAllPossibleCombinationOfIntlsVals)) {
                                    isAnyOneValueInvalid = true;
                                    if (options.highlightInvalidMapping) {
                                        MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($txtIntls, false);
                                    }
                                    isErrorInSrc = true;
                                }
                                else {
                                    MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($txtIntls, true);
                                }
                            }
                        }
                    }
                });
                if (strViewTitleMapping) {
                    if (!_validateValue(strViewTitleMapping, intlsJson, objAllPossibleCombinationOfIntlsVals)) {
                        isAnyOneValueInvalid = true;
                        if (options.highlightInvalidMapping) {
                            MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($txtViewParameter, false);
                        }
                        isErrorInSrc = true;
                    }
                    else {
                        MF_HELPERS.intlsenseHelper.changeHtmlOfTxtBoxForValidity($txtViewParameter, true);
                    }
                }
                
                if(isErrorInSrc){
                    arySrcsWithError.push(divData);
                }
            });
            // if (isAnyOneValueInvalid === true) {
            //     aryErrorMessages.push("Invalid value mapped to control");
            //     if(isAnyLabelInitInvalid){
            //         aryErrorMessages.push("Atleast one parameter of label should be mapped with a valid value.");
            //     }
            // }
            return _validateErrorDtl.getErrorDtlObj(isAnyOneValueInvalid,isAnyLabelInitInvalid,arySrcsWithError);
                
            //return aryErrorMessages;
        }
    };
})(jQuery);
//slider settings menu
(function ($) {
    $.fn.sliderMenu = function (method) {
        var methods = {
            init: function (options) {
                this.sliderMenu.settings = $.extend({}, this.sliderMenu.defaults, options);
                return this.each(function () {
                    var $el = $(this),
                    el = this;
                    (function () {
                        settings = $.fn.sliderMenu.settings;
                        var $menu = $el.find('.menu');
                        $el.find('.menu ul.menuList').hide();
                        //$menuUl.hide();
                        $menu.find('.clicker').click(function (e) {
                            var $menuUl = $el.find('.menu ul.menuList');
                            if ($menuUl.is(':visible')) {
                                //var menuInstance = $menuUl.menu( "instance" );
                                var menuInstance = $menuUl.menu("widget");
                                //$menuUl.slideUp();
                                settings.beforeHide.call(this);
                                $menuUl.hide();
                                $('.clicker').removeClass('active');
                                //if(menuInstance){
                                //  $menuUl.menu("destroy");
                                //}
                            }
                            else {
                                settings.beforeShow.call(this);
                                var clickNavId = $menuUl.parents(".click-nav").attr("id");
                                if (clickNavId) {
                                    //var viewId = clickNavId.split('_');
                                    $menuUl.menu({
                                        select: function (event, ui) {
                                            settings.onMenuClick.call(this, event);
                                            //$menuUl.slideToggle(200);
                                            $menuUl.hide();
                                            $('.clicker').toggleClass('active');
                                            try {
                                                //$menuUl.menu( "collapse" );
                                                $menuUl.menu("destroy");
                                            }
                                            catch (e) {
                                            }
                                            $menuUl.hide();
                                            //event.stopPropagation();
                                        },
                                        _closeOnDocumentClick: function (event) {
                                            return true;
                                        }
                                    });
                                    //$menuUl.slideToggle(200);
                                    $menuUl.show();
                                    $('.clicker').toggleClass('active');
                                    //e.stopPropagation();
                                }
                            }

                        });
                        //$menu.find('.menuList a').click(function(e) {
                        //settings.onMenuClick.call(this,e);
                        //$menuUl.slideToggle(200);
                        //$('.clicker').toggleClass('active');
                        //e.stopPropagation();
                        //});
                    })();

                });
            }
        };
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method "' + method + '" does not exist in sliderMenu plugin!');
        }
    };
    $.fn.sliderMenu.defaults = {
        action: 'click',
        submenuEl: 'ul',
        triggerEl: 'a',
        triggerParentEl: 'li',
        afterLoad: function () {
            alert("pop up after load")
        },
        beforeShow: function () {
            alert("pop up before show")
        },
        afterShow: function () {
            alert("pop up after show")
        },
        beforeHide: function () {
            //alert("pop up before hide")
        },
        afterHide: function () {
            alert("pop up after hide")
        },
        onMenuClick: function () { }
    };
    $.fn.sliderMenu.settings = {};
})(jQuery);
// (function ($) {
// $.fn.textIntellisense = function (options) {
// //var intellisence = JSON.stringify(intlsense);
// var settings = $.extend({
// IntlsJson: "[]"
// }, options || {});

// this.autocomplete({
// source: function (request, response) {
// var txtVal = request.term;
// tags = getOptionsByNoOfDots(txtVal);
// var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(txtVal), "i");
// response($.grep(tags, function (item) {
// return matcher.test(item.value.toLowerCase());
// }));
// },
// focus: function (event, ui) {
// // prevent autocomplete from updating the textbox
// event.preventDefault();
// // manually update the textbox
// $(this).val(ui.item.value);
// },
// select: function (event, ui) {
// // prevent autocomplete from updating the textbox
// event.preventDefault();
// // manually update the textbox and hidden field
// $(this).val(ui.item.value);
// },
// minLength: 0,
// delay: 1
// });

// function formDataSourceFromAryOptions(options /*ary of objects prop and val*/) {
// var tags = [];
// if (options && $.isArray(options) && options.length > 0) {
// for (var i = 0; i <= options.length - 1; i++) {
// var objFinalOptionObjct = new Object();
// objFinalOptionObjct.value = options[i].val;
// objFinalOptionObjct.label = options[i].prop;
// tags.push(objFinalOptionObjct);
// }
// }
// return tags;
// }

// function getFirstLevelOptions() {
// return formDataSourceFromAryOptions($.parseJSON(settings.IntlsJson));
// }

// function getOptionsByNoOfDots(textBal) {
// var tags = [];
// if (textBal) {
// var aryTextValSelected = textBal.split('.');
// if (aryTextValSelected.length <= 1) {
// tags = getFirstLevelOptions();
// } else {
// var lstCharacter = (textBal).substring((textBal.length) - 1);
// var iNoOfDotsToConsider = aryTextValSelected.length;
// var finalValue = "";
// var objByLevel = $.parseJSON(settings.IntlsJson);
// var textValToConsider = 0;
// var strObjectNameOfInnerParam = "";
// for (var i = 0; i <= iNoOfDotsToConsider - 1; i++) {
// var objTempPropertySelected = [];
// if ((i % 2) === 0) {
// //textValToConsider++;
// for (var j = 0; j <= objByLevel.length - 1; j++) {
// if (aryTextValSelected[i] === "") {
// //this has to be an array.
// //objTempPropertySelected = objByLevel;
// var aryInnerParam = [];
// if (objByLevel[j] && $.isPlainObject(objByLevel[j]) && objByLevel[j].hasOwnProperty('ip')) {
// strObjectNameOfInnerParam = objByLevel[j].val;
// aryInnerParam = objByLevel[j].ip;
// }
// for (var n = 0; n <= aryInnerParam.length - 1; n++) {
// aryInnerParam[n].val = strObjectNameOfInnerParam + "." + aryInnerParam[n].val;
// objTempPropertySelected.push(aryInnerParam[n]);
// }
// strObjectNameOfInnerParam = "";

// break;
// } else {
// if (i === 0) {
// if (objByLevel[j].prop.startsWith(aryTextValSelected[i])) {
// objTempPropertySelected.push(objByLevel[j]);
// }
// }
// else {
// var aryInnerParam = [];
// if (objByLevel[j] && $.isPlainObject(objByLevel[j]) && objByLevel[j].hasOwnProperty('ip')) {
// strObjectNameOfInnerParam = objByLevel[j].val;
// aryInnerParam = objByLevel[j].ip;
// }
// for (var q = 0; q <= aryInnerParam.length - 1; q++) {
// if (aryInnerParam[q].prop.startsWith(aryTextValSelected[i])) {
// aryInnerParam[q].val = strObjectNameOfInnerParam + "." + aryInnerParam[q].val;
// objTempPropertySelected.push(aryInnerParam[q]);
// }
// }
// strObjectNameOfInnerParam = "";
// }

// }
// }

// } else {
// for (var m = 0; m <= objByLevel.length - 1; m++) {
// if (aryTextValSelected[i] === "") {
// var aryInnerParam = [];
// if (objByLevel[m] && $.isPlainObject(objByLevel[m]) && objByLevel[m].hasOwnProperty('ip')) {
// strObjectNameOfInnerParam = objByLevel[m].val;
// aryInnerParam = objByLevel[m].ip;
// }
// for (var n = 0; n <= aryInnerParam.length - 1; n++) {
// aryInnerParam[n].val = strObjectNameOfInnerParam + "." + aryInnerParam[n].val;
// objTempPropertySelected.push(aryInnerParam[n]);
// }
// strObjectNameOfInnerParam = "";


// } else {
// var aryInnerParam = [];
// if (objByLevel[m] && $.isPlainObject(objByLevel[m]) && objByLevel[m].hasOwnProperty('ip')) {
// strObjectNameOfInnerParam = objByLevel[m].val;
// aryInnerParam = objByLevel[m].ip;
// }
// for (var p = 0; p <= aryInnerParam.length - 1; p++) {
// if (aryInnerParam[p].prop.startsWith(aryTextValSelected[i])) {
// aryInnerParam[p].val = strObjectNameOfInnerParam + "." + aryInnerParam[p].val;
// objTempPropertySelected.push(aryInnerParam[p]);

// }
// }
// strObjectNameOfInnerParam = "";
// }

// }

// }
// objByLevel = objTempPropertySelected;
// }
// if ($.isArray(objByLevel)) {
// tags = formDataSourceFromAryOptions(objByLevel);
// }
// }
// } else {
// tags = getFirstLevelOptions();
// }
// return tags;
// }

// function IntellisenseObject(prop, val, ip) {
// this.prop = prop;
// this.val = val;
// this.ip = ip;
// }

// };
// })(jQuery);
// (function ($) {
// $.fn.mfConditionalLogicControl = function (options) {
// var CONSTANTS = {
// FirstSelectPrefix: "ddlFirstColDDL",
// SecondSelectPrefix: "ddlMapOperator",
// ThirdTextPrefix: "txtMapWithIntls",
// AddButtonPrefix: "AddRow",
// DeleteButtonPrefix: "DeleteRow"
// };


// var settings = $.extend({
// FirstDropDownOptions: "[{\"text\":\"Select\",\"val\":\"-1\"}]",
// ConditionalLogicDdlVals: "[{\"text\":\"Select\",\"val\":\"-1\"}]",
// ThirdDropDownOptions: "[{\"text\":\"Select\",\"val\":\"-1\"}]",
// UseIntellisense: true,
// IntlsJson: "{}",
// CallbackFunctionOnSave: function (savedJson) {
// alert(savedJson);
// },
// CallbackFunctionOnCancel: function () {
// alert("Called the call back function on CNANCEL");
// },
// FirstColumnTagForJson: "FirstCol",
// SecondColumnTagForJson: "SecondCol",
// ThirdColumnTagForJson: "ThirdCol",
// IsEdit: false,
// SavedJsonIfEdit: "[]",
// SelectFirstDdlValBy: "Value",
// SelectCondLogicDdlValBy: "Value"
// }, options || {});

// //initialisation
// var $self = this;
// //remove any already existing html.
// $self.children().remove();
// var _id = $self.attr('id') + "CondLogicTable1";
// $self.html(_getInitialisationHTML());
// var $tblAdded = $($self).children("[id=\"" + _id + "\"]");
// //add the first row
// if (settings.IsEdit) {
// var objSavedJson = _parseSavedJson();
// if (objSavedJson && $.isArray(objSavedJson) && objSavedJson.length > 0) {
// for (var i = 0; i <= objSavedJson.length - 1; i++) {
// _processAddRowToTable();
// _fillSavedMapeedData(objSavedJson[i], i);
// }
// }
// }
// else {
// _processAddRowToTable();
// }
// this.data("FinalJson", FormMappedCompleteJSON);
// this.data("ValidateIntlsText", checkIfTextValExistsInJson);
// return this;
// //_bindEventToDivActionButtons();
// //end initialisation


// function _getInitialisationHTML() {
// var html = "";
// html += _getTableSkeletonToAdd();
// //html +=_getActionButtonDivToAdd();
// return html;
// }
// function _getTableSkeletonToAdd() {
// var html = "";
// html += _getTableOpeningTag();
// html += _getSkeletonTbodyTag();
// html += _getTableEndTag();
// return html;
// }
// function _getActionButtonDivToAdd() {
// var html = "";
// html += "<div style=\"border-top:1px solid grey;margin-top:3px;text-align:center;\">";
// html += "<input id=\"btnConLogicOk" + _id + "\" type=\"submit\" value=\"OK\" />";
// html += "<input id=\"btnConLogicCancel" + _id + "\" type=\"submit\" value=\"CANCEL\" />";
// html += "</div>";
// return html;
// }
// function _getTableOpeningTag() {
// return "<table id = \"" + _id + "\">";
// }
// function _getTableEndTag() {
// return "</table>";
// }
// function _getSkeletonTbodyTag() {
// return "<tbody></tbody>";
// }

// /**
// Adds the required HTML And Then binds the dropdown and makes 
// intellisense textbox binds event to add and delete button.
// **/
// function _processAddRowToTable() {
// var tblBinding = $tblAdded;
// var index = tblBinding.find("tbody tr").length;
// (index > 0) ?
// tblBinding.children('tbody:last').children('tr:last').after(_getHtmlToAddToCntlColMapTable(index + 1)) :
// tblBinding.children('tbody:last').append(_getHtmlToAddToCntlColMapTable(index + 1));

// //$(tblBinding).find("tbody tr:last").after();
// _bindAndAddEventsToLastRowControls();
// }
// function _bindAndAddEventsToLastRowControls() {
// var firstDropDown = $tblAdded.find("tbody tr:last").find("[id^=\"" + CONSTANTS.FirstSelectPrefix + "\"]\"");
// var aryFirstDDOptions = $.parseJSON(settings.FirstDropDownOptions);
// var condlLogicDropDown = $tblAdded.find("tbody tr:last").find("[id^=\"" + CONSTANTS.SecondSelectPrefix + "\"]\"");
// var arySecondDDOptions = $.parseJSON(settings.ConditionalLogicDdlVals);
// var addButton = $tblAdded.find("tbody tr:last").find("[id^=\"" + CONSTANTS.AddButtonPrefix + "\"]\"");
// var deleteButton = $tblAdded.find("tbody tr:last").find("[id^=\"" + CONSTANTS.DeleteButtonPrefix + "\"]\"");
// var intlsTextBox = $tblAdded.find("tbody tr:last").find(":text[id^=\"" + CONSTANTS.ThirdTextPrefix + "\"]\"");
// //TODO 
// /**
// COMPLETE THE ELSE STATEMENT IF NOT USINF INTELLISENSE TEXT BOX
// THEN ITHE HEML WILL CONTAIN A TEXT BOX SO SET OPTIONS FOR TAHT
// **/
// if (settings.UseIntellisense) {
// intlsTextBox.textIntellisense({
// IntlsJson: settings.IntlsJson
// });
// }
// else {

// }
// _bindDropdown(aryFirstDDOptions, firstDropDown);
// _bindDropdown(arySecondDDOptions, condlLogicDropDown);
// $(addButton).on('click', function () {
// _addCntrlColMapRowToTable(this)
// });
// $(deleteButton).on('click', function () {
// _deleteTableRowOfSender(this)
// });
// }
// function _bindEventToDivActionButtons() {
// var $OKButton = $self.find("[id^=\"btnConLogicOk\"]");
// var $CANCELButton = $self.find("[id^=\"btnConLogicCancel\"]");
// $OKButton.on('click', function () {
// var lastJson = JSON.stringify(_formMappedCompleteJSON());
// settings.CallbackFunctionOnSave(lastJson);
// });
// $CANCELButton.on('click', function () {
// settings.CallbackFunctionOnCancel();
// });
// }
// //private methods
// function _addCntrlColMapRowToTable(sender) {
// _processAddRowToTable();
// }
// function _addCntrlColMapRowToTableFromJson(cntrlColMapWhereJson) {
// if (cntrlColMapWhereJson) {
// var tblCntrlDataBindingOflnColMap = _getCntlOFDBDatabindingTable();
// $(tblCntrlDataBindingOflnColMap).find('tbody tr').remove();
// var index = 0;
// var htmlToAdd = "";
// var objCntrlColMapWhereObject = $.parseJSON(cntrlColMapWhereJson);
// if (objCntrlColMapWhereObject) {
// var aryCntrlColMapObj = objCntrlColMapWhereObject.Logic;
// if (aryCntrlColMapObj.length > 0) {
// for (var i = 0; i <= aryCntrlColMapObj.length - 1; i++) {
// htmlToAdd += _getHtmlToAddToCntlColMapTable(index);
// }
// }

// }
// var aryCntrls = getAllValidCntrlsObjectForIntlsense();
// var htmlToAdd = _getHtmlToAddToCntlColMapTable(index);
// $(tblCntrlDataBindingOflnColMap).find("tbody tr:last").after(htmlToAdd);
// makeCntrlOFDBMapTextboxIntellisense(aryCntrls, "CntrlOFDBMaptxt" + index);
// }
// }
// function _getHtmlToAddToCntlColMapTable(index) {
// var htmlToAdd = "<tr>";
// htmlToAdd += "<td><select id=\"" + CONSTANTS.FirstSelectPrefix + index + _id + "\" class=\"notUniform cntrl150\"></select></td>";
// htmlToAdd += "<td><select id=\"" + CONSTANTS.SecondSelectPrefix + index + _id + "\" class=\"notUniform cntrl100\"></select></td>";
// htmlToAdd += "<td><input type=\"text\" id=\"" + CONSTANTS.ThirdTextPrefix + _id + index + "\"></input></td>";
// htmlToAdd += "<td><span class=\"addImg16x16\" id=\"" + CONSTANTS.AddButtonPrefix + index + _id + "\"></span></td>";
// htmlToAdd += "<td><span class=\"crossImg16x16\" id=\"" + CONSTANTS.DeleteButtonPrefix + index + _id + "\"></span></td>";
// htmlToAdd += "</tr>";
// return htmlToAdd;
// }
// function _getOflnTableColDetailObjectFromHid() {
// var hidOflnTableColDetJson = $('hidden[id$="hidOflnTableColDetJson"]');
// var objTblColDtlJson = {};
// if (hidOflnTableColDetJson) {
// strTblColJson = $(hidOflnTableColDetJson).val();
// }
// if (strTblColJson) {
// objTblColDtlJson = $.parseJSON(strTblColJson);
// }
// return objTblColDtlJson;
// }
// function _getCntlOFDBDatabindingTable() {
// return $("#tblCntrlDataBindingOflnColMap");
// }
// function _bindDropdown(dropDownOptionsObject, dropdownToBind) {
// if (dropDownOptionsObject && $.isArray(dropDownOptionsObject) && dropDownOptionsObject.length > 0) {
// var strOptionForDdls = "";
// for (var i = 0; i <= dropDownOptionsObject.length - 1; i++) {
// strOptionForDdls += "<option value=\"" + dropDownOptionsObject[i].val + "\">" + dropDownOptionsObject[i].text + "</option>";
// } //.end
// $(dropdownToBind)
// .html(strOptionForDdls)
// .val('-1');
// }
// }
// function _bindCntrlOFDBBindingCondLogicOperator(value/*string value selected in the dropdown*/, ddlToBind) {
// if (typeof value === "string" && value) {
// var strOptionForDdls = "<option value=\"-1\">Select</option>";
// if (MF_IDE_CONSTANTS.OfflineDbColType.String.toLowerCase() === value.toLowerCase) {
// for (var i = 0; i <= MF_IDE_CONSTANTS.CntlOFDBBindingStringOperator.length - 1; i++) {
// strOptionForDdls += "<option value=\"" + MF_IDE_CONSTANTS.CntlOFDBBindingStringOperator[i] + ">" + MF_IDE_CONSTANTS.CntlOFDBBindingStringOperator[i] + "</option>";
// }
// }
// else {
// for (var i = 0; i <= MF_IDE_CONSTANTS.CntlOFDBBindingNumberOperator.length - 1; i++) {
// strOptionForDdls += "<option value=\"" + MF_IDE_CONSTANTS.CntlOFDBBindingNumberOperator[i] + ">" + MF_IDE_CONSTANTS.CntlOFDBBindingNumberOperator[i] + "</option>";
// }
// }
// $(ddlToBind).find('option')
// .remove()
// .end()
// .append(strOptionForDdls)
// .val('-1');
// }
// }
// function _deleteTableRowOfSender(sender) {
// if (sender) {
// var confirmDelete = confirm('Are you sure you want to remove the column');
// if (confirmDelete) {
// var tr = $(sender).closest('tr');
// tr.remove();
// if (_noOfRowsInTable() === 0) {
// _addCntrlColMapRowToTable();
// }
// //tr.fadeOut(400, function () {
// //
// //});
// }
// }
// }
// function _noOfRowsInTable() {
// return ($self.find('tbody tr')).length;
// }
// function FormMappedCompleteJSON() {
// var aryJson = [];
// $self.find('tbody tr').each(function () {
// var $firstDropDown = $(this).find("[id^=\"" + CONSTANTS.FirstSelectPrefix + "\"]\"");
// var $condlLogicDropDown = $(this).find("[id^=\"" + CONSTANTS.SecondSelectPrefix + "\"]\"");
// var $intlsTextBox = $(this).find(":text[id^=\"" + CONSTANTS.ThirdTextPrefix + "\"]\"");
// aryJson.push(getMappedDataDtlObject($firstDropDown, $condlLogicDropDown, $intlsTextBox));
// });
// return aryJson;
// }
// function getMappedDataDtlObject(firstDdl, secondDdl, thirdControl) {
// var objCompleteDataObject = new Object();
// if (settings.SelectFirstDdlValBy === "Value") {
// objCompleteDataObject[settings.FirstColumnTagForJson] = $(firstDdl).val();
// }
// else {
// objCompleteDataObject[settings.FirstColumnTagForJson] = $(firstDdl).children('option:selected').text();
// }
// if (settings.SelectCondLogicDdlValBy === "Value") {
// objCompleteDataObject[settings.SecondColumnTagForJson] = $(secondDdl).val();
// }
// else {
// objCompleteDataObject[settings.SecondColumnTagForJson] = $(secondDdl).children('option:selected').text();
// }
// objCompleteDataObject[settings.ThirdColumnTagForJson] = $(thirdControl).val();
// return objCompleteDataObject;
// }
// function _parseSavedJson() {
// return $.parseJSON(settings.SavedJsonIfEdit);
// }
// function _fillSavedMapeedData(savedObjectByIndex, index) {
// var tablerow = $self.find("tbody tr")[index];
// var $firstDropDown = $(tablerow).find("[id^=\"" + CONSTANTS.FirstSelectPrefix + "\"]\"");
// var $condlLogicDropDown = $(tablerow).find("[id^=\"" + CONSTANTS.SecondSelectPrefix + "\"]\"");
// var $intlsTextBox = $(tablerow).find(":text[id^=\"" + CONSTANTS.ThirdTextPrefix + "\"]\"");
// if (settings.SelectFirstDdlValBy === "Value") {
// $firstDropDown.val(savedObjectByIndex[settings.FirstColumnTagForJson]);
// }
// else {
// $firstDropDown.children("option:contains(" + savedObjectByIndex[settings.FirstColumnTagForJson] + ")").attr('selected', true);
// }
// if (settings.SelectCondLogicDdlValBy === "Value") {
// $condlLogicDropDown.val(savedObjectByIndex[settings.SecondColumnTagForJson]);
// }
// else {
// $condlLogicDropDown.children("option:contains(" + savedObjectByIndex[settings.SecondColumnTagForJson] + ")").attr('selected', true);
// }
// $intlsTextBox.val(savedObjectByIndex[settings.ThirdColumnTagForJson]);
// }
// function checkIfTextValExistsInJson(textVal) {
// var blnIsAllValuesCorrect = true;
// var aryOfRowsAdded = $tblAdded.find('tbody').children('tr'); ;
// var aryAllPossibleIntlsVlue = getAllPossibleFinalIntlsProperty();
// $.each(aryOfRowsAdded, function (index, value) {
// var $IntlsTextBox = $(value).find("[id^=" + CONSTANTS.ThirdTextPrefix + "]");
// if ($IntlsTextBox) {
// if ($IntlsTextBox.val() === "") {
// return true;
// }
// else if ($IntlsTextBox.val() && $.inArray($IntlsTextBox.val(), aryAllPossibleIntlsVlue) !== -1) {
// return true;
// }
// else {
// blnIsAllValuesCorrect = false;
// return false;
// }
// }

// });
// return blnIsAllValuesCorrect;
// }
// function getAllPossibleFinalIntlsProperty() {
// var aryFinalCombintionsOfText = [];
// var aryArtOfFinalPropertirs = [];
// var intlsense = JSON.parse(settings.IntlsJson);
// var aryIntlsFronJson = $.parseJSON(JSON.stringify(intlsense));
// var strVlue = "";
// var aryOfPossibleCombintionFromObject = [];
// if (aryIntlsFronJson.length > 0) {
// for (var i = 0; i <= aryIntlsFronJson.length - 1; i++) {
// var strPropFinalText = "";
// aryOfPossibleCombintionFromObject = [];
// aryOfPossibleCombintionFromObject = _getPossibleTextValsFromObject(aryIntlsFronJson[i], strPropFinalText);
// for (var k = 0; k <= aryOfPossibleCombintionFromObject.length - 1; k++) {
// aryArtOfFinalPropertirs.push(aryOfPossibleCombintionFromObject[k]);
// }
// }
// }
// i = 0;
// for (i = 0; i <= aryArtOfFinalPropertirs.length - 1; i++) {
// var aryOfFinlCOmbination = aryArtOfFinalPropertirs[i];
// if (typeof aryOfFinlCOmbination === "string") {
// aryFinalCombintionsOfText.push(aryOfFinlCOmbination);
// }
// else if ($.isArray(aryOfFinlCOmbination)) {
// for (var k = 0; k <= aryOfFinlCOmbination.length - 1; k++) {
// aryFinalCombintionsOfText.push(aryOfFinlCOmbination[k]);
// }
// }
// }
// return aryFinalCombintionsOfText;
// }
// function _getPossibleTextValsFromObject(propObject, previousStringEvluted/*string*/) {
// var aryFinalCombintionsOfText = [];
// var aryOfCombinationsInnerProp = [];
// if ($.isPlainObject(propObject) && propObject.hasOwnProperty("prop")) {
// previousStringEvluted = addTextFromPropObject(propObject, previousStringEvluted);
// if (propObject.hasOwnProperty("ip")) {
// aryOfCombinationsInnerProp = _loopThroughInnerProperty(propObject.ip, previousStringEvluted);
// }
// }
// if (aryOfCombinationsInnerProp.length > 0) {
// for (var i = 0; i <= aryOfCombinationsInnerProp.length - 1; i++) {
// aryFinalCombintionsOfText.push(aryOfCombinationsInnerProp[i]);
// }
// return aryFinalCombintionsOfText;
// }
// else {
// //aryFinalCombintionsOfText.push(previousStringEvluted);
// return previousStringEvluted;
// }

// }
// function addTextFromPropObject(propObject, prevuousPropText) {
// if (prevuousPropText === "") {
// return propObject.prop;
// }
// else {
// return prevuousPropText + "." + propObject.prop;
// }

// }
// function _loopThroughInnerProperty(innerPropArryObject, previousPropTextEvaluated) {
// var aryOfCombinationsInnerProp = [];

// if ($.isArray(innerPropArryObject) && innerPropArryObject.length > 0) {
// for (var i = 0; i <= innerPropArryObject.length - 1; i++) {
// var strStrtString = previousPropTextEvaluated;
// if ($.isPlainObject(innerPropArryObject[i])) {
// aryOfCombinationsInnerProp.push(_getPossibleTextValsFromObject(innerPropArryObject[i], strStrtString));
// }

// }
// }
// return aryOfCombinationsInnerProp;
// }
// //private methods
// };

// })(jQuery);(function ($) {
$(document).ready(function () {
    /*----------------------------------------------------------------------*/
    /* Parse the data from an data-attribute of DOM Elements
    /*----------------------------------------------------------------------*/
    $.parseData = function (data, returnArray) {
        if (/^\[(.*)\]$/.test(data)) { //array
            data = data.substr(1, data.length - 2).split(',');
        }
        if (returnArray && !$.isArray(data) && data != null) {
            data = Array(data);
        }
        return data;
    };

    /*----------------------------------------------------------------------*/
    /* Image Preloader
    /* http://engineeredweb.com/blog/09/12/preloading-images-jquery-and-javascript
    /*----------------------------------------------------------------------*/

    // Arguments are image paths relative to the current page.
    $.preload = function () {
        var cache = [],
      args_len = arguments.length;
        for (var i = args_len; i--; ) {
            var cacheImage = document.createElement('img');
            cacheImage.src = arguments[i];
            cache.push(cacheImage);
        }
    };

    /*----------------------------------------------------------------------*/
    /* fadeInSlide by revaxarts.com
    /* Fades out a box and slide it up before it will get removed
    /*----------------------------------------------------------------------*/

    $.fn.fadeInSlide = function (speed, callback) {
        if ($.isFunction(speed)) callback = speed;
        if (!speed) speed = 200;
        if (!callback) callback = function () { };
        this.each(function () {

            var $this = $(this);
            $this.fadeTo(speed / 2, 1).slideDown(speed / 2, function () {
                callback();
            });
        });
        return this;
    };
    /*----------------------------------------------------------------------*/
    /* fadeOutSlide by revaxarts.com
    /* Fades out a box and slide it up before it will get removed
    /*----------------------------------------------------------------------*/

    $.fn.fadeOutSlide = function (speed, callback) {
        if ($.isFunction(speed)) callback = speed;
        if (!speed) speed = 200;
        if (!callback) callback = function () { };
        this.each(function () {

            var $this = $(this);
            $this.fadeTo(speed / 2, 0).slideUp(speed / 2, function () {
                $this.remove();
                callback();
            });
        });
        return this;
    };
    /*----------------------------------------------------------------------*/
    /* textFadeOut by revaxarts.com
    /* Fades out a box and slide it up before it will get removed
    /*----------------------------------------------------------------------*/

    $.fn.textFadeOut = function (text, delay, callback) {
        if (!text) return false;
        if ($.isFunction(delay)) callback = delay;
        if (!delay) delay = 2000;
        if (!callback) callback = function () { };
        this.each(function () {

            var $this = $(this);
            $this.stop().text(text).show().delay(delay).fadeOut(1000, function () {
                $this.text('').show();
                callback();
            })
        });
        return this;
    };
    /*----------------------------------------------------------------------*/
    /* leadingZero by revaxarts.com
    /* adds a leding zero if necessary
    /*----------------------------------------------------------------------*/

    $.leadingZero = function (value) {
        value = parseInt(value, 10);
        if (!isNaN(value)) {
            (value < 10) ? value = '0' + value : value;
        }
        return value;
    };


    if (!String.prototype.startsWith) {
        Object.defineProperty(String.prototype, 'startsWith', {
            enumerable: false,
            configurable: false,
            writable: false,
            value: function (searchString, position) {
                position = position || 0;
                return this.indexOf(searchString, position) === position;
            }
        });
    }
    if (!String.prototype.endsWith) {
        Object.defineProperty(String.prototype, 'endsWith', {
            value: function (searchString, position) {
                var subjectString = this.toString();
                if (position === undefined || position > subjectString.length) {
                    position = subjectString.length;
                }
                position -= searchString.length;
                var lastIndex = subjectString.indexOf(searchString, position);
                return lastIndex !== -1 && lastIndex === position;
            }
        });
    }

});
/*----------------------------------------------------------------------*/
/* wl_Alert v 1.1 by revaxarts.com
/* description: Handles alert boxes
/* dependency: jquery UI Slider, fadeOutSlide plugin
/*----------------------------------------------------------------------*/
$.fn.wl_Alert = function (method) {
    var args = arguments;
    return this.each(function () {

        var $this = $(this);


        if ($.fn.wl_Alert.methods[method]) {
            return $.fn.wl_Alert.methods[method].apply(this, Array.prototype.slice.call(args, 1));
        } else if (typeof method === 'object' || !method) {
            if ($this.data('wl_Alert')) {
                var opts = $.extend({}, $this.data('wl_Alert'), method);
            } else {

                var opts = $.extend({}, $.fn.wl_Alert.defaults, method, $this.data());
            }
        } else {
            $.error('Method "' + method + '" does not exist');
        }


        if (!$this.data('wl_Alert')) {

            $this.data('wl_Alert', {});

            //bind click events to hide alert box
            $this.bind('click.wl_Alert', function (event) {
                event.preventDefault();

                //Don't hide if it is sticky
                if (!$this.data('wl_Alert').sticky) {
                    $.fn.wl_Alert.methods.close.call($this[0]);
                }

                //prevent hiding the box if an inline link is clicked
            }).find('a').bind('click.wl_Alert', function (event) {
                event.stopPropagation();
            });
        } else {

        }
        //show it if it is hidden
        if ($this.is(':hidden')) {
            $this.slideDown(opts.speed / 2);
        }

        if (opts) $.extend($this.data('wl_Alert'), opts);
    });

};
$.fn.wl_Alert.defaults = {
    speed: 500,
    sticky: false,
    onBeforeClose: function (element) { },
    onClose: function (element) { }
};
$.fn.wl_Alert.version = '1.1';
$.fn.wl_Alert.methods = {
    close: function () {
        var $this = $(this),
      opts = $this.data('wl_Alert');
        //call callback and stop if it returns false
        if (opts.onBeforeClose.call(this, $this) === false) {
            return false;
        };
        //fadeout and call an callback
        $this.fadeOutSlide(opts.speed, function () {
            opts.onClose.call($this[0], $this);
        });
    },
    set: function () {
        var $this = $(this),
      options = {};
        if (typeof arguments[0] === 'object') {
            options = arguments[0];
        } else if (arguments[0] && arguments[1] !== undefined) {
            options[arguments[0]] = arguments[1];
        }
        $.each(options, function (key, value) {
            if ($.fn.wl_Alert.defaults[key] !== undefined || $.fn.wl_Alert.defaults[key] == null) {
                $this.data('wl_Alert')[key] = value;
            } else {
                $.error('Key "' + key + '" is not defined');
            }
        });

    }
};

//to create an alert box on the fly
$.wl_Alert = function (text, cssclass, insert, after, options) {
    //go thru all
    $('div.alert').each(function () {
        var _this = $(this);
        //...and hide if one with the same text is allready set
        if (_this.text() == text) {
            _this.slideUp($.fn.wl_Alert.defaults.speed);
        }
    });

    //create a new DOM element and inject it
    var al = $('<div class="alert ' + cssclass + '">' + text + '</div>').hide();
    (after) ? al.appendTo(insert).wl_Alert(options) : al.prependTo(insert).wl_Alert(options);

    //return the element
    return al;
};

/*----------------------------------------------------------------------*/
/* wl_Date v 1.0 by revaxarts.com
/* description: extends the Datepicker
/* dependency: jQuery Datepicker, mousewheel plugin
/*----------------------------------------------------------------------*/
$.fn.wl_Date = function (method) {

    var args = arguments;
    return this.each(function () {

        var $this = $(this);


        if ($.fn.wl_Date.methods[method]) {
            return $.fn.wl_Date.methods[method].apply(this, Array.prototype.slice.call(args, 1));
        } else if (typeof method === 'object' || !method) {
            if ($this.data('wl_Date')) {
                var opts = $.extend({}, $this.data('wl_Date'), method);
            } else {
                var opts = $.extend({}, $.fn.wl_Date.defaults, method, $this.data());
            }
        } else {
            try {
                return $this.datepicker(method, args[1], args[2]);
            } catch (e) {
                $.error('Method "' + method + '" does not exist');
            }
        }


        if (!$this.data('wl_Date')) {

            $this.data('wl_Date', {});






            //call the jQuery UI datepicker plugin
            $this.datepicker(opts);

            //bind a mousewheel event to the input field
            $this.bind('mousewheel.wl_Date', function (event, delta) {
                if (opts.mousewheel) {
                    event.preventDefault();
                    //delta must be 1 or -1 (different on macs and with shiftkey pressed)
                    delta = (delta < 0) ? -1 : 1;

                    //shift key is pressed
                    if (event.shiftKey) {
                        var _date = $this.datepicker('getDate');
                        //new delta is the current month day count (month in days)
                        delta *= new Date(_date.getFullYear(), _date.getMonth() + 1, 0).getDate();
                    }
                    //call the method
                    $.fn.wl_Date.methods.changeDay.call($this[0], delta);
                }
            });


            //value is set and has to get translated (self-explanatory) 
            if (opts.value) {
                var today = new Date().getTime();
                switch (opts.value) {
                    case 'now':
                    case 'today':
                        $this.datepicker('setDate', new Date());
                        break;
                    case 'next':
                    case 'tomorrow':
                        $this.datepicker('setDate', new Date(today + 864e5 * 1));
                        break;
                    case 'prev':
                    case 'yesterday':
                        $this.datepicker('setDate', new Date(today + 864e5 * -1));
                        break;
                    default:
                        //if a valid number add them as days to the date field
                        if (!isNaN(opts.value)) $this.datepicker('setDate', new Date(today + 864e5 * opts.value));
                }

            }
            //disable if set
            if (opts.disabled) {
                $.fn.wl_Date.methods.disable.call(this);
            }
        } else {

        }

        if (opts) $.extend($this.data('wl_Date'), opts);
    });

};
$.fn.wl_Date.defaults = {
    value: null,
    mousewheel: true
};
$.fn.wl_Date.version = '1.0';
$.fn.wl_Date.methods = {
    disable: function () {
        var $this = $(this);
        //disable the datepicker
        $this.datepicker('disable');
        $this.data('wl_Date').disabled = true;
    },
    enable: function () {
        var $this = $(this);
        //enable the datepicker
        $this.datepicker('enable');
        $this.data('wl_Date').disabled = false;
    },
    next: function () {
        //select next day
        $.fn.wl_Date.methods.changeDay.call(this, 1);
    },
    prev: function () {
        //select previous day
        $.fn.wl_Date.methods.changeDay.call(this, -1);
    },
    changeDay: function (delta) {
        var $this = $(this),
      _current = $this.datepicker('getDate') || new Date();
        //set day to currentday + delta
        _current.setDate(_current.getDate() + delta);
        $this.datepicker('setDate', _current);
    },
    set: function () {
        var $this = $(this),
      options = {};
        if (typeof arguments[0] === 'object') {
            options = arguments[0];
        } else if (arguments[0] && arguments[1] !== undefined) {
            options[arguments[0]] = arguments[1];
        }
        $.each(options, function (key, value) {
            if ($.fn.wl_Date.defaults[key] !== undefined || $.fn.wl_Date.defaults[key] == null) {
                $this.data('wl_Date')[key] = value;
            } else {
                $.error('Key "' + key + '" is not defined');
            }
        });

    }
};

/*----------------------------------------------------------------------*/
/* wl_Dialog v 1.1 by revaxarts.com
/* description: handles alert boxes, prompt boxes and confirm boxes and
/*                message boxes
/*                contains 4 plugins
/* dependency: jquery UI Dialog
/*----------------------------------------------------------------------*/


/*----------------------------------------------------------------------*/
/* Confirm Dialog
/* like the native confirm method
/*----------------------------------------------------------------------*/
$.confirm = function (text, callback, cancelcallback) {

    var options = $.extend(true, {}, $.alert.defaults, $.confirm.defaults);

    //nativ behaviour
    if (options.nativ) {
        if (result = confirm(unescape(text))) {
            if ($.isFunction(callback)) callback.call(this);
        } else {
            if ($.isFunction(cancelcallback)) cancelcallback.call(this);
        }
        return;
    }

    //the callbackfunction
    var cb = function () {
        $(this).dialog('close');
        removeDialogImage();
        $('#wl_dialog_alert').remove();
        if ($.isFunction(callback)) callback.call(this);
    },

    //the callbackfunction on cancel
      ccb = function () {
          $(this).dialog('close');
          removeDialogImage();
          $('#wl_dialog_alert').remove();
          if ($.isFunction(cancelcallback)) cancelcallback.call(this);
      };
    if (cancelcallback && $.isFunction(cancelcallback)) {
        options.beforeClose = function (event, ui) {
            removeDialogImage();
            $('#wl_dialog_alert').remove();
            if ($(event.toElement).parent().hasClass('ui-dialog-titlebar-close')) {
                cancelcallback.call(this);
            }
        };
    }
    else {
        options.beforeClose = function (event, ui) {
            removeDialogImage();
            $('#wl_dialog_alert').remove();
        };
    }
    //set some options
    options = $.extend({}, {
        buttons: [{
            text: options.text.ok,
            click: cb

        }, {
            text: options.text.cancel,
            click: ccb

        }]
    }, options);

    //use the dialog
    return $.alert(unescape(text), options);
};
$.confirm.defaults = {
    text: {
        header: 'Please confirm',
        ok: 'Yes',
        cancel: 'No'
    }
};

/*----------------------------------------------------------------------*/
/* Prompt Dialog
/* like the native prompt method
/*----------------------------------------------------------------------*/
$.prompt = function (text, value, callback, cancelcallback) {

    var options = $.extend(true, {}, $.alert.defaults, $.prompt.defaults);

    //nativ behaviour
    if (options.nativ) {
        var val = prompt(unescape($.trim(text)), unescape(value));
        if ($.isFunction(callback) && val !== null) {
            callback.call(this, val);
        } else {
            if ($.isFunction(cancelcallback)) cancelcallback.call(this);
        }
        return;
    }

    //the callbackfunction
    var cb = function (value) {
        if ($.isFunction(callback)) callback.call(this, value);
        $(this).dialog('close');
        $('#wl_dialog').remove();
    },

    //the callbackfunction on cancel
  ccb = function () {
      if ($.isFunction(cancelcallback)) cancelcallback.call(this);
      $(this).dialog('close');
      $('#wl_dialog').remove();
  };

    //set some options
    options = $.extend({}, {
        buttons: [{
            text: options.text.ok,
            click: function () {
                cb.call(this, $('#wl_promptinputfield').val());
            }
        }, {
            text: options.text.cancel,
            click: ccb
        }],
        open: function () {
            $('#wl_promptinputfield').focus().select();
            $('#wl_promptinputfield').uniform();
            $('#wl_promptinputform').bind('submit', function (event) {
                event.preventDefault();
                cb.call(this, $('#wl_promptinputfield').val());
                $(this).parent().dialog('close');
                $('#wl_dialog').remove();
            });

        }
    }, options);

    //use the dialog
    return $.alert('<p>' + unescape(text) + '</p><form id="wl_promptinputform"><input id="wl_promptinputfield" name="wl_promptinputfield" value="' + unescape(value) + '" onkeydown=\"return enterKeyFilter(event);\"></form>', options);
};
$.prompt.defaults = {
    text: {
        header: 'Please prompt',
        ok: 'OK',
        cancel: 'Cancel'
    }
};

/*----------------------------------------------------------------------*/
/* Alert Dialog
/* like the native alert method
/*----------------------------------------------------------------------*/

$.alert = function (content, options) {


    //if no options it is a normal dialog
    if (!options) {
        var options = $.extend(true, {}, {
            buttons: [{
                text: $.alert.defaults.text.ok,
                click: function () {
                    $(this).dialog('close');
                    //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove();
                    $('#wl_dialog_alert').prev().find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
                    $('#wl_dialog_alert').remove();
                }
            }],
            open: function () { // open event handler
                $(this) // the element being dialogged
                .parent() // get the dialog widget element
                .find(".ui-dialog-titlebar-close") // find the close button for this dialog
                .hide(); // hide it
            },
            beforeClose: function (event, ui) {
                removeDialogImage();
                //console.log('Event Fire');
            }
        }, $.alert.defaults);
    }

    //nativ behaviour
    if (options.nativ) {
        alert(content);
        return;
    }

    //create a container
    var container = $('<div/>', {
        id: 'wl_dialog_alert'
    }).appendTo('body'); //wl_dialog 11/20/2012.Alert will have an image aginst the header.so to make this unique,making this change.

    //set a header
    if (options.text.header) {
        container.attr('title', options.text.header);
    }

    //fill the container
    container.html(content.replace(/\n/g, '<br>'));
    //display the dialog
    //container.dialog(options);
    container.aspdialog(options);
    return {
        close: function (callback) {
            container.dialog('close');
            container.remove();
            if ($.isFunction(callback)) callback.call(this);
        },
        setHeader: function (text) {
            this.set('title', text);
        },
        setBody: function (html) {
            container.html(html);
        },
        set: function (option, value) {
            //container.dialog("option", option, value);
            container.aspdialog("option", option, value);
        }
    }


};
$.alert.defaults = {
    nativ: false,
    resizable: false,
    modal: true,
    text: {
        header: 'Notification',
        ok: 'OK'
    }
};
$.alert.Error = {
    nativ: false,
    resizable: false,
    modal: true,
    text: {
        header: 'Error',
        ok: 'OK'
    },
    buttons: [{
        text: $.alert.defaults.text.ok,
        click: function () {
            $(this).dialog('close');
            //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove();
            $('#wl_dialog_alert').prev().find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
            $('#wl_dialog_alert').remove();
        }
    }],
    open: function () { // open event handler
        $(this) // the element being dialogged
    .parent() // get the dialog widget element
    .find(".ui-dialog-titlebar-close") // find the close button for this dialog
    .hide(); // hide it
    },
    beforeClose: function (event, ui) {
        removeDialogImage();
        //console.log('Event Fire');
    }
};
$.alert.Information = {
    nativ: false,
    resizable: false,
    modal: true,
    text: {
        header: 'Notification',
        ok: 'OK'
    },
    buttons: [{
        text: $.alert.defaults.text.ok,
        click: function () {
            $(this).dialog('close');
            //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove();
            $('#wl_dialog_alert').prev().find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
            $('#wl_dialog_alert').remove();
        }
    }],
    open: function () { // open event handler
        $(this) // the element being dialogged
    .parent() // get the dialog widget element
    .find(".ui-dialog-titlebar-close") // find the close button for this dialog
    .hide(); // hide it
    },
    beforeClose: function (event, ui) {
        removeDialogImage();
        //console.log('Event Fire');
    }
};

/*----------------------------------------------------------------------*/
/* Message Function
/*----------------------------------------------------------------------*/

$.msg = function (content, options) {


    //get the options
    var options = $.extend({}, $.msg.defaults, options);

    var container = $('#wl_msg'),
    msgbox;

    //the container doen't exists => create it
    if (!container.length) {
        container = $('<div/>', {
            id: 'wl_msg'
        }).appendTo('body').data('msgcount', 0);
        var topoffset = parseInt(container.css('top'), 10);

        //bind some events to it
        container.bind('mouseenter', function () {
            container.data('pause', true);
        }).bind('mouseleave', function () {
            container.data('pause', false);
        });
        container.delegate('.msg-close', 'click', function () {
            container.data('pause', false);
            close($(this).parent());
        });
        container.delegate('.msg-box-close', 'click', function () {
            container.fadeOutSlide(options.fadeTime);
        });

        //bind the scroll event
        $(window).unbind('scroll.wl_msg').bind('scroll.wl_msg', function () {
            var pos = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
            if (pos > topoffset) {
                (window.navigator.standalone === undefined) ? container.css({
                    position: 'fixed',
                    top: 10
                }) : container.css({
                    top: pos + 10
                });
            } else {
                (window.navigator.standalone === undefined) ? container.removeAttr('style') : container.css({
                    top: topoffset
                });
            }
        }).trigger('scroll.wl_msg');
    }

    //stop if no content is set
    if (!content) return false;

    //count of displayed messages
    var count = container.data('msgcount');

    function getHTML(content, headline) {
        return '<div class="msg-box"><h3>' + (headline || '') + '</h3><a class="msg-close">close</a><div class="msg-content">' + content.replace('\n', '<br>') + '</div></div>';
    }

    function create() {
        msgbox = $(getHTML(content, options.header)),
    closeall = $('.msg-box-close');

        //we have some messages allready
        if (count) {

            //No close all button
            if (!closeall.length) {
                msgbox.appendTo(container);
                $('<div class="msg-box-close">close all</div>').appendTo(container).fadeInSlide(options.fadeTime);

                //Close all button
            } else {
                msgbox.insertBefore(closeall);
            }

            //first message
        } else {
            msgbox.appendTo(container);
        }

        //fade it in nicely
        msgbox.fadeInSlide(options.fadeTime);

        //add the count of the messages to the container
        container.data('msgcount', ++count);

        //outclose it only if it's not sticky
        if (!options.sticky) {
            close(msgbox, options.live);
        }
    }

    function close(item, delay, callback) {
        if ($.isFunction(delay)) {
            callback = delay;
            delay = 0;
        } else if (!delay) {
            delay = 0;
        }
        setTimeout(function () {

            //if the mouse isn't over the container
            if (!container.data('pause')) {
                item.fadeOutSlide(options.fadeTime, function () {
                    var count = $('.msg-box').length;
                    if (count < 2 && $('.msg-box-close').length) {
                        $('.msg-box-close').fadeOutSlide(options.fadeTime);
                    }
                    container.data('msgcount', count);
                    if ($.isFunction(callback)) callback.call(item);
                })
                //try again...
            } else {
                close(item, delay);
            }

        }, delay);
    }

    //create the messsage
    create();

    return {
        close: function (callback) {
            close(msgbox, callback);
        },
        setHeader: function (text) {
            msgbox.find('h3').eq(0).text(text);
        },
        setBody: function (html) {
            msgbox.find('.msg-content').eq(0).html(html);
        },
        closeAll: function (callback) {
            container.fadeOutSlide(options.fadeTime, function () {
                if ($.isFunction(callback)) callback.call(this);
            });
        }
    }

};
$.msg.defaults = {
    header: null,
    live: 5000,
    topoffset: 90,
    fadeTime: 500,
    sticky: false
};
$.modalPopUp = function (text, value, html) {

    var options = $.extend(true, {}, $.alert.defaults);

    //nativ behaviour
    if (options.nativ) {
        var val = prompt(unescape($.trim(text)), unescape(value));
        if ($.isFunction(callback) && val !== null) {
            callback.call(this, val);
        } else {
            if ($.isFunction(cancelcallback)) cancelcallback.call(this);
        }
        return;
    }

    //use the dialog
    return $.alert('<p>' + unescape(text) + '</p>' + html, options);
};
$.modalPopUp.defaults = {
    sticky: false,
    autoOpen: false,
    height: 300,
    width: 350
};
//End Mficient Plubins
//Common functions
function AspDialogCloseCBOption(callBack, context) {
    this.cb = callBack;
    this.ctxt = context;
}
function showModalPopUp(divToOpen, popUpTitle,
    popUpWidth, resizable,
    crossClickCBOptions,
    dialogType/*pass dialog type DialogType Enum to show image on header*/,
    params/*Object of params*/) {

    //{minHeight}
    if (resizable == null) resizable = false;
    var objDialogOptions = {};
    objDialogOptions.title = popUpTitle;
    objDialogOptions.width = popUpWidth ? popUpWidth : 350;
    objDialogOptions.modal = true;
    if (!mfUtil.isNullOrUndefined(params)) {
        if (params.minHeight) {
            objDialogOptions.minHeight = params.minHeight;
        }
    }

    if (!resizable) {
        objDialogOptions.resizable = false;
    }
    if (crossClickCBOptions) {
        objDialogOptions.beforeClose = function (event, ui) {
            if (event.originalEvent != null) {
                if ($.isFunction(crossClickCBOptions.cb)) {
                    crossClickCBOptions.cb.call(crossClickCBOptions.ctxt);
                }
            }
            removeDialogImage(divToOpen);
        };
    }
    else {
        if (dialogType) {
            objDialogOptions.beforeClose = function (event, ui) {
                removeDialogImage(divToOpen);
            }
        }
    }

    $('#' + divToOpen).aspdialog(objDialogOptions);
    $('#' + divToOpen).parent().find('.ui-dialog-titlebar > a.ui-dialog-titlebar-close').show();
    if (dialogType) {
        showDialogImage(dialogType, divToOpen);
    }
    //$("#" + divToOpen).aspdialog({
    //   modal: true,
    //   resizable: resizable,
    //  title: popUpTitle,
    //  width: popUpWidth
    //});
}
function showModalPopUpWithOutHeader(divToOpen, popUpTitle, popUpWidth, resizable) {
    if (resizable || resizable == null) {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, modal: true });
    }
    else {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, resizable: false, modal: true });
    }
    if ($('#' + divToOpen)) {
        ($('#' + divToOpen).parent()).find('.ui-dialog-titlebar > .ui-dialog-titlebar-close').hide();
    }
}
function showDialog(divToOpen, popUpTitle, popUpWidth, imageName, resizable) {
    if (resizable || resizable == null) {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, modal: true });
    }
    else {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, resizable: false, modal: true });
    }
    if ($('#' + divToOpen)) {
        ($('#' + divToOpen).parent()).find('.ui-dialog-titlebar > .ui-dialog-titlebar-close').hide();
        if ($($($('#' + divToOpen).parent())[0]).find("span")[0].id == "dialog_img") {
            $($($('#' + divToOpen).parent())[0]).find("span")[0].innerHTML = "";
            $($($('#' + divToOpen).parent())[0]).find("span")[0].innerHTML = '<img src="//enterprise.mficient.com/images/' + imageName + '">';

        }
        else {
            $('<span id="dialog_img" style="margin-right: 8px; float: left;"><img src="//enterprise.mficient.com/images/' + imageName + '"> </span>').insertBefore(($('#' + divToOpen).parent()).find('.ui-dialog-title'));
        }
    }
}
function showDialogWithHeader(divToOpen, popUpTitle, popUpWidth, imageName, resizable) {
    if (resizable || resizable == null) {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, modal: true });
    }
    else {
        $('#' + divToOpen + '').aspdialog({ title: popUpTitle, width: popUpWidth ? popUpWidth : 350, resizable: false, modal: true });
    }
    if ($('#' + divToOpen)) {
        $('#' + divToOpen).parent().find('.ui-dialog-titlebar > a.ui-dialog-titlebar-close').show();
        if ($($($('#' + divToOpen).parent())[0]).find("span")[0].id == "dialog_img") {
            $($($('#' + divToOpen).parent())[0]).find("span")[0].innerHTML = "";
            $($($('#' + divToOpen).parent())[0]).find("span")[0].innerHTML = '<img src="//enterprise.mficient.com/images/' + imageName + '">';

        }
        else {
            $('<span id="dialog_img" style="margin-right: 8px; float: left;"><img src="//enterprise.mficient.com/images/' + imageName + '"> </span>').insertBefore(($('#' + divToOpen).parent()).find('.ui-dialog-title'));
        }
    }
}
function closeModalPopUp(divIdToClose) {
    $('#' + divIdToClose).dialog('close');
}
function showAlert(message, divIdToShowMsgWithHash) {
    $.wl_Alert(message, 'info', divIdToShowMsgWithHash);
}

DialogType = {
    Error: 0,
    Warning: 1,
    Info: 3
}
function showDialogImage(type, divToPrependImg) {

    var divToPrependImage = getDivToPrependImage(divToPrependImg);
    if (divToPrependImage) {
        if ($.browser.msie) {

            switch (type) {

                case DialogType.Error:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogError\" />');
                    //divToPrependImage.prepend('<div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/dialog_error.png"/></div>');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogError");
                    break;
                case DialogType.Warning:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogWarning\" />');
                    //divToPrependImage.prepend('<div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/dialog_info.png"/></div>');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogWarning");
                    break;
                case DialogType.Info:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogInfo\" />');
                    //divToPrependImage.prepend('<div style="float:left;margin-right:5px;"><img src="//enterprise.mficient.com/images/dialog_info.png"/></div>');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogInfo");
                    break;
            }
        }
        else if ($.browser.webkit) {

            switch (type) {

                case DialogType.Error:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogError\" />');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogError dialogErrorWebKit");
                    break;
                case DialogType.Warning:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogWarning\" />');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogWarning dialogWarningWebKit");
                    break;
                case DialogType.Info:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogInfo\" />');
                    getDialogImageSpan(divToPrependImg).removeClass().addClass("dialogInfo dialogInfoWebKit");
                    break;
            }
        }
        else {

            switch (type) {

                case DialogType.Error:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogError\" />');
                    break;
                case DialogType.Warning:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogWarning\" />');
                    break;
                case DialogType.Info:
                    divToPrependImage.prepend('<span id=\"dialogImageId\" class=\"dialogInfo\" />');
                    break;
            }
        }


    }
}
function getDivToPrependImage(divToPrependImage) {
    if (divToPrependImage == undefined) {
        return $('#wl_dialog_alert').prev();
    }
    else {
        return $('#' + divToPrependImage).prev();
    }
}
function removeDialogImage(divToRemoveImage) {
    //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
    if (divToRemoveImage == undefined) {
        $('#wl_dialog_alert').prev().find($('#dialogImageId')).remove();
    }
    else {
        $('#' + divToRemoveImage).prev().find($('#dialogImageId')).remove();
    }

}
function getDialogImageSpan(divToPrependImage) {
    //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
    if (divToPrependImage == undefined) {
        return $('#wl_dialog_alert').prev().find('#dialogImageId');
    }
    else {
        return $('#' + divToPrependImage).prev().find('#dialogImageId');
    }

}
MessageOption = {
    Error: 0,
    Warning: 1,
    Info: 3
}
function showMessage(message, optionForScript/*DialogType*/) {
    var strOption = '', dialogImageType = '';
    switch (optionForScript) {
        case DialogType.Error:
            strOption = $.alert.Error;
            dialogImageType = DialogType.Error
            break;
        case DialogType.Info:
            strOption = $.alert.Information;
            dialogImageType = DialogType.Info
            break;
    }
    $.alert(message, strOption); showDialogImage(dialogImageType);
}
//
function showHideDiv(divId/*string*/, show/*bool*/) {
    if (divId) {
        if (show) {
            $('#' + divId).show();
        }
        else {
            $('#' + divId).hide();
        }
    }
}
function enableDisableCheckbox(chkId, isAspCntrl, enable/*bool*/) {
    if (enable) {
        if (!isAspCntrl) {
            $('#' + chkId).prop("disabled", false);
        }
        else {
            $('[id$=' + chkId + ']').prop("disabled", false);
        }
    }
    else {
        if (!isAspCntrl) {
            $('#' + chkId).prop("disabled", true);
        }
        else {
            $('[id$=' + chkId + ']').prop("disabled", true);
        }

    }
}
function checkUncheckCheckbox(chkId, isAspCntrl, check/*bool*/) {
    if (check) {
        if (!isAspCntrl) {
            $('#' + chkId).prop("checked", true);
        }
        else {
            $('[id$=' + chkId + ']').prop("checked", true);
        }
    }
    else {
        if (!isAspCntrl) {
            $('#' + chkId).prop("checked", false);
        }
        else {
            $('[id$=' + chkId + ']').prop("checked", false);
        }
    }
}
function ideCloseModalPopup(divId, isAspPanel/*bool*/) {
    if (isAspPanel) {
        $('[id$="' + divId + '"]').dialog('close');
    }
    else {
        $('#' + divId).dialog('close');
    }
}
function _makeIdeTabs() {
    $(".tabs").tabs();
}
function MfError(msgs /*array of messages*/) {
    this.msgs = msgs;
}
var processPostbackByHtmlCntrl = (function () {
    function clickButtonForPostBack(buttonId) {
        if (buttonId) {
            $('[id$=' + button + ']').click();
        }
        else {
            $('[id$=btnPostbackByHtmlCntrl]').click();
        }
    }
    function setDataInHidField(procFor, data) {
        var dataObject = {
            processFor: procFor,
            data: data
        };
        $('[id$="hidDtlsForPostBackByHtmlCntrl"]').val(JSON.stringify(dataObject)); //rowData[0]
    }
    return {
        postBack: function (procFor/*postBackByHtmlProcess*/,
                            data/*array of strings*/,
                            buttonId/*optional otherwise default button willbe clicked*/) {
            setDataInHidField(procFor, data);
            clickButtonForPostBack(buttonId);
        }
    };
})();

//MOHAN
//Popup to show webservice command details
function SubprocWsCmdDetails(_bol) {
    SubProcCommonMethod('SubprocWsCmdDetails', _bol, 'Object Details', 580);
}
//Popup to show databse command details
function SubprocDbCmdDetails(_bol) {
    SubProcCommonMethod('SubprocDbCmdDetails', _bol, 'Object Details', 580);
}

var mfUtil = (function () {

    return {
        isNullOrUndefined: function (value) {
            var blnReturn = false;
            if (value === null || value === undefined) {
                blnReturn = true;
            }
            return blnReturn;
        },
        isNull: function (value) {
            var blnReturn = false;
            if (value === null) {
                blnReturn = true;
            }
            return blnReturn;
        },
        isUndefined: function (value) {
            var blnReturn = false;
            if (value === undefined) {
                blnReturn = true;
            }
            return blnReturn;
        },
        isEmptyString: function (value /*string*/) {
            if (typeof value === "string") {
                if (value.trim().length > 0) {
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    }

})();

function enterKeyFilter(e) {
    if (e.srcElement && e.srcElement.className === "ace_text-input") {
        return true;
    }
    else {
        return e.keyCode != 13;
    }
}
function enterKeyLinefeed(cntrlId, e) {
    if (e.keyCode === 13) {
        var control = document.getElementById(cntrlId);
        var allText = control.value;

        currentPos = getCaret(control);

        control.value = allText.substr(0, currentPos) + '\n' + allText.substr(currentPos);
        setCaretPosition(control, currentPos + 1);
        return false;
    }
    //return true;
}
function getCaret(el) {
    if (el.selectionStart) {
        return el.selectionStart;
    } else if (document.selection) {
        el.focus();

        var r = document.selection.createRange();
        if (r == null) {
            return 0;
        }

        var re = el.createTextRange(),
        rc = re.duplicate();
        re.moveToBookmark(r.getBookmark());
        rc.setEndPoint('EndToStart', re);

        return rc.text.length;
    }
    return 0;
}

function setCaretPosition(elem, caretPos) {
    if (elem.createTextRange) {
        var range = elem.createTextRange();
        range.move('character', caretPos);
        range.select();
    }
    else {
        if (elem.selectionStart) {
            elem.focus();
            elem.setSelectionRange(caretPos, caretPos);
        }
        else
            elem.focus();
    } 
}

//Database connector popup
function SubModifiction(_bol) {
    if (_bol) {
        showModalPopUpWithOutHeader('SubProobjectChange', 'Discard Changes', 250,false);
    }
    else {
        $('#SubProobjectChange').dialog('close');
    }
}




function DataCredentialBy(_bol) {
    if (_bol) {
        showModalPopUp('divCredentialBymobile', 'Enter Credential', 300);
        $('[id$=txtUsername]').val('');
        $('[id$=txtPassword]').val('');
    }
    else {
        $('#divCredentialBymobile').dialog('close');
    }
}



function CreateDynamicTable(result) {
    $('#Div7').children().remove();
    if (result.length > 0) {
        $('#Div7').html('<table id="resulttbl"></table>');
        columnCount = 0;
        rowCount = 0;
        var columndt = [];
        $.each(result, function (headindex, headvalue) {
            columnCount++;
            rowCount = rowCount < headvalue.val.length ? headvalue.val.length : rowCount;
            columndt.push({ 'title': headvalue.cnm });
        });

        var dtRows = [];
        for (var rindex = 0; rindex < rowCount; rindex++) {
            var dtCol = [];
            for (var cindex = 0; cindex < columnCount; cindex++) {
                dtCol.push(result[cindex].val[rindex]);
            }
            dtRows.push(dtCol);
        }
        $('#resulttbl').dataTable({
            "data": dtRows,
            "columns": columndt,
            "searching": false,
            "paging": false,
            "info": false
        });
    }
    else {
        $('#Div7').html('<div style="width:100%;text-align:center;">No records found</div>');
    }
    openDynamictable(true, 600); opendialogtest(false);
}
function openDynamictable(_bol, width) {
    if (_bol) {
        var objectheader = $('[id$=hdfDbType]').val();
        objectheader = ' ( ' + objectheader + ' )';

        showModalPopUp('dialogdynamictable', 'Execute Details' + objectheader, width);
    }
    else {
        $('#dialogdynamictable').dialog('close');
    }
}


//Odata connector edit
function opendialogtest(_bol) {
    if (_bol) {
        showModalPopUp('dialogtestPara', 'Execute  Object', 500);

    }
    else {
        $('#dialogtestPara').dialog('close');
    }
}

/*-------------------------------------------Base 64 Encode Decode--------------------------------------------------------*/

var Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function (e) {
        var t = "";
        var n, r, i, s, o, u, a;
        var f = 0;
        e = Base64._utf8_encode(e);
        while (f < e.length) {
            n = e.charCodeAt(f++);
            r = e.charCodeAt(f++);
            i = e.charCodeAt(f++);
            s = n >> 2;
            o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6;
            a = i & 63;
            if (isNaN(r)) {
                u = a = 64
            } else if (isNaN(i)) {
                a = 64
            }
            t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
        }
        return t
    },
    decode: function (e) {
        var t = "";
        var n, r, i;
        var s, o, u, a;
        var f = 0;
        e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (f < e.length) {
            s = this._keyStr.indexOf(e.charAt(f++));
            o = this._keyStr.indexOf(e.charAt(f++));
            u = this._keyStr.indexOf(e.charAt(f++));
            a = this._keyStr.indexOf(e.charAt(f++));
            n = s << 2 | o >> 4;
            r = (o & 15) << 4 | u >> 2;
            i = (u & 3) << 6 | a;
            t = t + String.fromCharCode(n);
            if (u != 64) {
                t = t + String.fromCharCode(r)
            }
            if (a != 64) {
                t = t + String.fromCharCode(i)
            }
        }
        t = Base64._utf8_decode(t);
        return t
    },
    _utf8_encode: function (e) {
        e = e.replace(/\r\n/g, "\n");
        var t = "";
        for (var n = 0; n < e.length; n++) {
            var r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r)
            } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192);
                t += String.fromCharCode(r & 63 | 128)
            } else {
                t += String.fromCharCode(r >> 12 | 224);
                t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128)
            }
        }
        return t
    },
    _utf8_decode: function (e) {
        var t = "";
        var n = 0;
        var r = c1 = c2 = 0;
        while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r);
                n++
            } else if (r > 191 && r < 224) {
                c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                n += 2
            } else {
                c2 = e.charCodeAt(n + 1);
                c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                n += 3
            }
        }
        return t;
    }
}

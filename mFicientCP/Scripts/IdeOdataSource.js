﻿var selectOdataset;
var Odataset;
var discardobject;
var index;
var setclass;
var EditIndex;
//Initialize control values on add new odata connector
function AddNewODataConn() {
    $('[id$=hdfODataConnId]').val('');
    $('[id$=txtOdataConnName]').val('');
    $('[id$=txtOdataService]').val('');
    $('[id$=ddlOdataMpluginConn]').val('-1');
    $('[id$=txtOdataUserName]').val('');
    $('[id$=txtOdataPass]').val('');
    $('#OdataHttpAuthDiv').hide();
    $('#btnBackODataConn').hide();
    $('[id$=ddlWsUseMplugin]').change();
    $('[id$=chkODataHttp]')[0].checked = false;
    chkODataHttpOnChange(this);
    $('#divOdataCrPre').hide();
    $('#OdataHttpAuthDiv').hide();
    $('#Chkcredense').hide();
    $('#ChkcredenseChk').hide();
    
   
}


//Modal popup to show odata connector
function SubProcAddOdataConn(_bol) {
    if (_bol) {

        showModalPopUp('SubProcAddOdataConn', 'OData Connector', 505);
    }
    else {
        $('#SubProcAddOdataConn').dialog('close');
    }
}

//chkODataHttp change event
function chkODataHttpOnChange(e) {
    ShowOdataHttpDiv($('[id$=chkODataHttp]')[0].checked);
}

//Show hide http autentication div
function ShowOdataHttpDiv(_Show) {
    if (_Show) {
        $('#OdataHttpAuthDiv').show();
        $('#divOdataCrPre').show();
    }
    else {
        $('#OdataHttpAuthDiv').hide();
        $('#divOdataCrPre').hide();
        $('[id$=drpOCredentiail]').val('-1');

    }
}

//Change dropdown css
function CallUniformCssForOdataConn() {
    //    CallUnformCss('[id$=ddlOdataMpluginConn]');
    //    CallUnformCss('[id$=ddlOdataRType]');
    //    CallUnformCss('[id$=drpOdataAutenticationtype]');
    //    CallUnformCss('[id$=drpOCredentiail]');

}

//Odata connector edit 
function ShowODataConnEdit() {
    $('[id$=addodataconnector]').show();
    $('#editodata').hide();
    $('[id$=diveditodataheader]').show();
    $('[id$=divaddodataheader]').hide();
    $('[id$=btnBackODataCancel]').show();
    $('[id$=ODataAddDiv]').show();
    $('[id$=Divlabelsource]').show();
    $('[id$=ODatatxtConnDiv]').hide();
    $('[id$=btnodatareset]').hide();
    $('[id$=hideditdone]').val('1');

}

function RemoveEdit() {
    $('#editodata').hide();
    $('[id$=diveditodataheader]').hide();
    $('[id$=divaddodataheader]').show();
    $('.showDetails').show();
    $('[id$=btnBackODataCancel]').hide();
    $('[id$=ODataAddDiv]').show();
    $('[id$=Divlabelsource]').hide();
    $('[id$=txtOdataConnName]').show();
    $('#btnodatareset').show();
    
}

//Odata connector edit
function ODataConnectorEdit(_Bol) {
    if (_Bol) {
        $('#ODatalblConnDiv').hide();
        $('#ODatatxtConnDiv').show();
        $('#btnBackODataConn').hide();
        $('[id$=txtOdataConnName]').removeAttr('disabled');
    }
    else {
        $('#ODatalblConnDiv').show();
        $('#ODatatxtConnDiv').hide();
        $('#btnBackODataConn').show();
        $('[id$=txtOdataConnName]').attr('disabled', 'disabled');
    }
}

//Odata connector delete click event
function ODataConnectorDeleteClick() {
    $('[id$=hdfDelType]').val('5');
    var v = $('[id$=lbloconnectorid]').text();
    $('[id$=hdfDelId]').val(v);

    $('#aDelCfmMsg').html('Are you sure you want to delete this odata source?'); SubProcImageDelConfirnmation(true, 'Delete OData Source');
}


//btnBackODataConn click event (Back button)
function ODataConnectorBackClick() {
    $('#addodataconnector').show();
}

//Odata connector show/hide on update
function ODataConnOnUpdate() {
    if ($('[id$=hdfODataConnId]').val().length > 0) {
        $('#ODatalblConnDiv').show();
        $('#ODatatxtConnDiv').hide();
        $('#btnBackODataConn').show();
    }
    else {
        ShowODataConnEdit();
    }
}

//Odata connector validation on saving
function ODataConnectorError() {
    var strMessage = "";
    if ($('#ODatatxtConnDiv').is(':visible')) {
        var strVal = ValidateNameInIde($('[id$=txtOdataConnName]').val());
        if (strVal != 0) {
            if (strVal == 1) {
                strMessage += " * Please enter connector name.</br>";
            }
            else if (strVal == 2) {
                strMessage += " * Connector name cannot be more than 20 charecters.</br>";
            }
            else if (strVal == 3) {
                strMessage += " * Connector name cannot be less than 3 charecters.</br>";
            }
            else if (strVal == 4) {
                strMessage += " * Please enter valid connector name.</br>";
            }
            else if (strVal == 5) {
                strMessage += " * Please enter valid connector name.</br>";
            }
        }
    }

    if ($('[id$=txtOdataService]').val().trim().length == 0)
        strMessage += " * Please enter service endpoint.</br>";

    if (strMessage.length != 0) {
        $('#aMessage').html(strMessage);
        SubProcBoxMessage(true);
    }
}
function DataDisplay(id) {
    $('#addodataconnector').hide();
    $('#editodata').show();
    //  $('[id$=txtOdataConnName]').hide();
    $('[id$=ODatatxtConnDiv]').hide();

    for (var i = 0; i < Odataset.length; i++) {
        intializeOdataSource();
        if (id == Odataset[i].CONNECTION_NAME) {
            discardobject = Odataset[i].CONNECTION_NAME;
            $('[id$=lblODataConn_UseMplugin]').text(Odataset[i].MPLUGIN_AGENT);
            $('[id$=lblODataConn_Version]').text(Odataset[i].VERSION);
            $('[id$=lbloconnectorid]').text(Odataset[i].ODATA_CONNECTOR_ID);
            $('[id$=lblviewConnector]').text(Odataset[i].CONNECTION_NAME);
            $('[id$=hidviewconnector]').val(Odataset[i].CONNECTION_NAME);
            $('[id$=hidcommadagin]').val(Odataset[i].CONNECTION_NAME);
            $('[id$=lblOdatacreatedby]').text(Odataset[i].CreatedBY);
            $('[id$=lblOdataupdatedby]').text(Odataset[i].FULL_NAME + ' On  ' + Odataset[i].DateTime);
            $('[id$=lblOdataActivityTime]').text(Odataset[i].DateTime);
            $('[id$=hidodatacredentialusername]').val(Odataset[i].USER_NAME);
            $('[id$=hidodatacredentialpassword]').val(Odataset[i].PASSWORD);
            var v = Odataset[i].ODATA_CONNECTOR_ID;
            $('[id$=hdfODataConnId]').val(Odataset[i].ODATA_CONNECTOR_ID);
            $('[id$=hdfDelId]').val(Odataset[i].ODATA_CONNECTOR_ID);
            $('[id$=lblheaderConnector]').text(Odataset[i].CONNECTION_NAME);
                $('[id$=chkODataHttp]')[0].checked = true;
                $('[id$=OdataHttpAuthDiv]').show();
                $('[id$=divOdataCrPre]').show();
                $('[id$=hidodatatypleofcredential]').val('0');
            $('[id$=hidOdataobjdts]').val(Odataset[i].ODATA_OBJ);
            var str = Odataset[i].ODATA_OBJ;
            if (str === "" || str === "undefined" || str === null) {
                str = "No Odata objects yet not added in this source";
                var vtest = "<div class=\"ClCmdDMHeader\" style=\"width:100%;background-color:inherit;\">"
                                    + "<div class=\"FLeft\"  style=\"font-size:12px;\">" + "&nbsp;&nbsp;" + str + "&nbsp;&nbsp;" + "</div>"
                              + "</div>"
                              + "<div class=\"clear\"></div>";
            }
            else {
                str = Odataset[i].ODATA_OBJ;
                var vtest = "<div class=\"ClCmdDMHeader\" style=\"width:100%;background-color:inherit;\">"
                                    + "<div class=\"FLeft\"  style=\"font-size:12px;\">" + "&nbsp;&nbsp;" + str + "&nbsp;&nbsp;" + "</div>"
                              + "</div>"
                              + "<div class=\"clear\"></div>";
            }
            $('#OdataCmdAppDiv').html(vtest);
            $('[id$=lblODataConn_Url]').text(Odataset[i].ODATA_ENDPOINT);
            $('[id$=txtOdataConnName]').val(Odataset[i].CONNECTION_NAME);

            $('[id$=txtOdataService]').val(Odataset[i].ODATA_ENDPOINT);

            $('[id$= lblOdataConnName]').text(Odataset[i].CONNECTION_NAME);
            $('[id$=ddlOdataMpluginConn]').parent().children("span").text(Odataset[i].MPLUGIN_AGENT);
            var agent = '';
            agent = Odataset[i].MPLUGIN_AGENT;
            if (agent == 'None') {
                $('[id$=ddlOdataMpluginConn]').val('-1');
            }
            else {
                $('[id$=ddlOdataMpluginConn]').val(agent);
            }
            $('[id$=drpOdataAutenticationtype]').val(Odataset[i].AUTHENTICATION_TYPE);
            $('select[id$=drpOdataAutenticationtype]').parent().children("span").text($('select[id$=drpOdataAutenticationtype]').children('option:selected').text());

            $('[id$=hidautenticationtype]').val(Odataset[i].AUTHENTICATION_TYPE);

            if (Odataset[i].CREDENTIAL_PROPERTY.length > 0 && Odataset[i].CREDENTIAL_PROPERTY != "-1") {
                $('[id$=chkODataHttp]')[0].checked = true;
                chkODataHttpOnChange(this);
                var vcrendention = $.trim(Odataset[i].CREDENTIAL_PROPERTY);
                $('[id$=drpOCredentiail]').val(vcrendention);
                $('select[id$=drpOCredentiail]').parent().children("span").text($('select[id$=drpOCredentiail]').children('option:selected').text());
                SelectedOdataCredential(vcrendention);
            }
            else {
                $('[id$=chkODataHttp]')[0].checked = false;
                chkODataHttpOnChange(this);
            }
            break;
        }
    }

}

function BindOdata() {
    $('#divodatasource').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="odatatable"></table>');
    $('#odatatable').dataTable({
        "language": {
            "lengthMenu": " _MENU_"
        },
        "data": selectOdataset,
        "lengthMenu": [[20, 25, 30, 45, 55, -1], [20, 25, 30, 45, 55, "All"]],
        "columns": [
            { "title": "CONNECTOR" },
            { "title": "MPLUGIN" },
            { "title": "URL" }
        ]
    });
    var table = $('#odatatable').DataTable();

    $('#odatatable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        var rowData = table.row(this).data();
        if (rowData != undefined) {
            var val = $('[id$=hideditdone]').val();
            if (val == '1') {
                SubModifiction(true);
                viewConnectiontest(rowData[0], $(this).index());
            }
            else {
                DataDisplay(rowData[0]);
                EditIndex = $(this).index();
                setclass = '';
            }
        }
        else {
            alert('No Row Data Selected');
        }
    });
    makeSelectUniformOnPostBack();
    searchText("odatatable");
    $('#odatatable_filter').css("padding-bottom", "10px");
}


function viewConnectiontest(id, index) {
    $('[id$=hiddiscardcommadtype]').val('');
    $('[id$=hiddiscardcommadtype]').val('5');
    setclass = index;
    for (var i = 0; i < Odataset.length; i++) {
        if (id == Odataset[i].CONNECTION_NAME) {
            discardobject = Odataset[i].CONNECTION_NAME;
            $('[id$=hidcommadagin]').val(Odataset[i].CONNECTION_NAME);
            break;
        }
    }
}

function discardedit() {
    if (setclass.toString() != '') {
        DataDisplay(discardobject);
        ChangeRowSelectedCSS(setclass);
    }
    else {
        intializeOdataSource();
        AfterAddClick();
        $('[id$=btnBackODataCancel]').hide();
        $('[id$=btnodatareset]').show();
        setclass = '';
        index = '';
        EditIndex = '';
    }
   }


function RefreshOdata() {
    selectOdataset = jQuery.parseJSON($('[id$=hidodataset]').val());
    Odataset = jQuery.parseJSON($('[id$=hidodatacompletedataset]').val());
    $('[id$=hidodatacompletedataset]').val('');
    $('[id$=hidodataset]').val('');

}
function Updatetable() {
    var editrow = $('[id$=hideditdatatablerow]').val();
    selectOdataset.search('editrow').draw();
    Odataset.search('editrow').draw();
}
//Add New Connection Data
function AddNewOdataConnector() {
    $('[id$=txtOdataConnName]').val('');
    $('[id$=ddlOdataMpluginConn]').val('-1');
    $('[id$=txtOdataService]').val('');
    $('[id$=lblOdataConnName]').val('');
    $('[id$=hdfODataConnId]').val('');
    $('[id$=chkODataHttp]')[0].checked = false;
    $('#OdataHttpAuthDiv').hide();
}


function AddOdataConnector() {
    $('#divaddodataheader').show();
    $('#diveditodataheader').hide();
    $('[id$=txtOdataConnName]').val('');
    $('[id$=ddlOdataMpluginConn]').val('-1');
    $('[id$=txtOdataService]').val('');
    $('[id$=hdfODataConnId]').val('');
}
function Odatadropdownstyle() {
    CallUnformCss('[id$=ddlOdataMpluginConn]');
}
//Confirmation message popup
function SubProcConfirmBoxMessage(_bol, _title, _width) {
    if (_bol) {
        showModalPopUp('SubProcConfirmBoxMessage', _title, _width);
        $('#btnCnfFormSave').focus();
    }
    else {
        $('#SubProcConfirmBoxMessage').dialog('close');
    }
}

function AfterAddOdata() {
    var vtest = $('[id$=hidodataset]').val();
    if (vtest != "" || vtest != undefined || vtest != '') {
        RefreshOdata();
    }
    BindOdata();
    
}

function hideheader() {
    $('#diveditodataheader').hide();
    $('#divaddodataheader').show();
}
function clearconId() {
    $('[id$=hdfODataConnId]').val('');
}


function Cancelupdate() {
    $('#editodata').show();
    $('#addodataconnector').hide();
    $('[id$=chkODataHttp]')[0].checked = false;
    chkODataHttpOnChange(this);
   var val=  $('[id$=hidviewconnector]').val();
   DataDisplay(val);
   $('[id$=hideditdone]').val('');
}

function CancelButtonhide() {
    $('#btnBackODataCancel').hide();
    $('#btnodatareset').show();
    

}
function CancelButtonshow() {
    $('#btnBackODataCancel').show();
    $('#btnodatareset').hide();
}

function imgOdataCmdAppClickStart(e) {
    if (e.alt.trim() == 'Expand') {
        $(e).attr('src', '//enterprise.mficient.com/images/collapse.png');
        $(e).attr('alt', 'Collapse');
        $(e).attr('title', 'Collapse');

        $('#divodataobj').show();

    }
    else {
        $(e).attr('src', '//enterprise.mficient.com/images/expand.png');
        $(e).attr('alt', 'Expand');
        $(e).attr('title', 'Expand');

        $('#divodataobj').hide();
    }

}
function imageOdataexpand() {
    var Ctr = $('#img2');
    Ctr.attr('src', '//enterprise.mficient.com/images/expand.png');
    Ctr.attr('alt', 'Expand');
    Ctr.attr('title', 'Expand');
    $('#divodataobj').hide();
}

//show delete message popup
function SubProcImageNotDelConfirmation(_bol) {
    if (_bol) {
        showModalPopUp('SubProcBoxMessage', 'Delete Not Allowed', 400);

    }
    else {
        $('#SubProcBoxMessage').dialog('close');
    }
}




//Odata connector edit
function ODataCredentialBy(_bol) {
    if (_bol) {
        showModalPopUp('divCredentialBymobile', 'Enter Credetials', 300);
        $('[id$=txtodatapredefinedpass]').val('');
    }
    else {
        $('#divCredentialBymobile').dialog('close');
    }

}

function intializeOdataSource() {
    $('[id$=hideditdone]').val('');
    $('[id$=txtOdataConnName]').val('');
    $('[id$=ddlOdataMpluginConn]').val('-1');
    $('[id$=txtOdataService]').val('');
    $('[id$=txtodatapredefinedpass]').val('');
    $('[id$=txtodatapredefinedusername]').val('');
    $('[id$=hdfODataConnId]').val('');
    $('[id$=ddlOdataMpluginConn]').val('-1');
    $('[id$=txtodatapredefinedusername]').removeAttr('disabled');
    $('[id$=txtodatapredefinedpass]').val('');
    $('select[id$=ddlOdataMpluginConn]').parent().children("span").text($('select[id$=ddlOdataMpluginConn]').children('option:selected').text());
    $('[id$=chkODataHttp]')[0].checked = false;
    chkODataHttpOnChange(this);
    $('[id$=drpOdataAutenticationtype]').val('0');
    $('select[id$=drpOdataAutenticationtype]').parent().children("span").text($('select[id$=drpOdataAutenticationtype]').children('option:selected').text());
    $('[id$=drpOCredentiail]').val('-1');
    $('select[id$=drpOCredentiail]').parent().children("span").text($('select[id$=drpOCredentiail]').children('option:selected').text());
    $('[id$=lbloconnectorid]').text('');
    $('[id$=lblheaderConnector]').text('');
    $('[id$=lblheaderConnector]').text('');
    $('[id$=lblODataConn_UseMplugin]').text('');
    $('[id$=lblODataConn_Url]').text('');
    $('[id$=lblODataConn_Version]').text('');

    $('[id$=lblOdatacreatedby]').text('');
    $('[id$=lblOdataupdatedby]').text('');
    $('[id$=lblOdataConnName]').text('');
    $('[id$=hidautenticationtype]').val('');
    $('[id$=hdfDelId]').val('');
    $('[id$=hidviewconnector]').val('');
    clearCredential();
    imageOdataexpand();
}

//Function  Selected StoreProcedure 
function SelectedOdataCredential(_selectvalue) {
    var $hid = $('[id$=hidodatacredential]');
    var permanent = '';

    if (_selectvalue != "-1") {

        if ($hid.val()) {
            permanent = jQuery.parseJSON($hid.val());
            for (var i = 0; i < permanent.length; i++) {
                if (_selectvalue == permanent[i].CreadentialId) {
                    $('[id$=hidodatacredentialusername]').val('');
                    $('[id$=hidodatacredentialpassword]').val('');
                    $('[id$=hidodatatypleofcredential]').val('');
                    $('[id$=txtodatapredefinedusername]').val('');
                    var username = permanent[i].UserId;
                    var password = permanent[i].Password;
                    var Credential = permanent[i].Typeval;
                    $('[id$=txtodatapredefinedusername]').removeAttr("disabled");
                    if (Credential == 3) {
                        $('[id$=txtodatapredefinedusername]').val(username);
                        $('[id$=txtodatapredefinedusername]').attr("disabled", "disabled");
                    }
                    else if (Credential == 0) {
                        $('[id$=hidodatacredentialusername]').val(username);
                        $('[id$=hidodatacredentialpassword]').val(password);
                        $('[id$=hidodatatypleofcredential]').val(Credential);
                    }
                   
                    break;
                }
            }
        }
    }
}


function SetOdataname() {
    var dbcname = $('[id$=hidviewconnector]').val();
    $('[id$=lblOdataConnName]').text(dbcname);

}

function ODeleteClick() {
    $('[id$=editodata]').show();
    $('[id$=addodataconnector]').hide();
}

function AfterAddClick() {
    $('[id$=editodata]').hide();
    $('[id$=addodataconnector]').show();
    $('[id$=diveditodataheader]').hide();
    $('[id$=divaddodataheader]').show();
    $('[id$=ODatatxtConnDiv]').show();
    $('[id$=Divlabelsource]').hide();
    $('[id$=txtOdataConnName]').show();

    BindOdata();
  
}


function OdataValidation() {
    var strVal, webdervicetype, endpoint, wscredential, chk;
    var strMessage = '';
    strVal = $('[id$=txtOdataConnName]').val();
    endpoint = $('[id$=txtOdataService]').val();
    chk = $('[id$=chkODataHttp]')[0].checked;
    odatacredential = $('select[id$=drpOCredentiail]').val();
    if (strVal == '') {
        strMessage += " * Please enter Odata source name.</br>";
    }
    else if (strVal < 3) {
        strMessage += " * Source name cannot be less than 3 charecters.</br>";
    }
    else if (strVal > 20) {
        strMessage += " * Source name cannot be more than 20 charecters.</br>";
    }
    if (endpoint == '') {
        strMessage += " * Please enter service endpoint.</br>";
    }
    if (chk == true) {
        if (odatacredential == '' || odatacredential == '-1') {
            strMessage += "* Please select credential.</br>";
        }
    }
    if (strMessage.length > 0) {
        showMessage(strMessage, DialogType.Error);
        return false;
    }
}


function clearCredential() {
    $('[id$=hidodatacredentialusername]').val('');
    $('[id$=hidodatacredentialpassword]').val('');
    $('[id$=hidodatatypleofcredential]').val('');
}

function Odatacredential() {
    var vcre = $('select[id$=drpOCredentiail]').val();
    var conid = $('[id$=hdfDelId]').val();
    if (conid != '' && vcre != '-1') {
        SetOdataname();
        ShowOdataHttpDiv(true);
        ShowODataConnEdit();
        SelectedOdataCredential(vcre);

    }
    else if (conid != '' && vcre == '-1') {
        SetOdataname();
        ShowOdataHttpDiv(true);
          ShowODataConnEdit();
          $('[id$=hidodatacredentialusername]').val('');
          $('[id$=hidodatacredentialpassword]').val('');

    }
    else if (vcre != '-1') {
        SelectedOdataCredential(vcre);
    }
}

function OdataCredentialvalidation() {
    var user = $('[id$=txtodatapredefinedusername]').val();
    var password = $('[id$=txtodatapredefinedpass]').val();
    if (user == '') {
        alert('Please enter username');
    }
    else if (password == '') {
        alert('Please enter password');
    }
    if (user != '' && password != '') {
        
        return true;
    }
    else {
        return false;
    }

}
function Odataaddbtndiscard() {
    $('[id$=hiddiscardcommadtype]').val('');
    $('[id$=hiddiscardcommadtype]').val('6');
}

function Discarddataobjectcancel() {
    var postback = $('[id$=hdfPostBackPageMode]').val();
    var v = $('[id$=hidcommadagin]').val();
  if (postback == 'odatasource') {

      ShowODataConnEdit();
      ChangeRowSelectedCSS(EditIndex);
    }

}

function ChangeRowSelectedCSS(val) {
    selIndex = val;
    if (selIndex != undefined) {
        $('#odatatable tbody tr.selected').removeClass('selected');
        if (selIndex % 2 === 0) {
            $('#odatatable tbody tr').eq(selIndex).addClass('even selected')
        }
        else {
            $('#odatatable tbody tr').eq(selIndex).addClass('odd selected')
        }
    }
}




if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
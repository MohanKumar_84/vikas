﻿mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.button = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, cntrlObj) {
                $(control).val(cntrlObj.type.UIName);
            },
            Name: function (control, cntrlObj) {
                $(control).val(cntrlObj.fnGetUserDefinedName());
            },
            Description: function (control, cntrlObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = cntrlObj.fnGetDescription();
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
            },
            eventHandlers: {
                Type: function () { },
                Name: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        showMessage("Name already exists for another control.Please provide some other name.", DialogType.Error);
                        $self.val(propObject.fnGetUserDefinedName());
                        $self.focus();
                        return;
                    }
                    propObject.fnRename($self.val());
                },
                Description: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.fnSetDescription($self.val());
                }
            }
        },
        Appearance: {
            Width: function (control, cntrlObj) {
                $(control).val(cntrlObj.fnGetWidth());
            },
            WidthInPercent: function (control, cntrlObj, args) {
                if (cntrlObj.fnGetWidth() === MF_IDE_CONSTANTS.buttonControlWidthType.custom) {
                    $(control).val(cntrlObj.fnGetWidthInPercent());
                    args.cntrlHtmlObjects.wrapperDiv.show();
                }
                else {
                    $(control).val("0");
                    args.cntrlHtmlObjects.wrapperDiv.hide();
                }
            },
            BackgroundColor: function (control, cntrlObj) {
                $(control).val(cntrlObj.fnGetBackgroundColor());
            },
            Icon: function (control, cntrlObj) {
                var aryOptionsForDdl = [{ text: "None", val: "0"}];
                aryOptionsForDdl = $.merge(aryOptionsForDdl,
                                    MF_HELPERS.jqueryMobileIconsHelper.getAsTextValPairList());
                MF_HELPERS.bindDropdowns([$(control)], aryOptionsForDdl);
                var strIcon = cntrlObj.fnGetIcon();
                if (!mfUtil.isNullOrUndefined(strIcon)) {
                    if (mfUtil.isEmptyString(strIcon)) {
                        $(control).val("0");
                    }
                    else {
                        $(control).val(strIcon);
                    }
                }
                else {
                    $(control).val("0");
                }
            },
            IconPosition: function (control, cntrlObj, args) {
                var strIcon = cntrlObj.fnGetIcon();
                if (!mfUtil.isNullOrUndefined(strIcon)) {
                    if (mfUtil.isEmptyString(strIcon)) {
                        args.cntrlHtmlObjects.wrapperDiv.hide();
                        $(control).val("1");
                    }
                    else {
                        args.cntrlHtmlObjects.wrapperDiv.show();
                        $(control).val(cntrlObj.fnGetIconPosition());
                    }
                }
                else {
                    args.cntrlHtmlObjects.wrapperDiv.hide();
                    $(control).val("1");
                }
            },
            Size: function (control, cntrlObj) {
                $(control).val(cntrlObj.fnGetDataMini());
            },
            Text: function (control, cntrlObj) {
                $(control).val(cntrlObj.fnGetText());
            },
            eventHandlers: {
                Width: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var cntrls = evntData.cntrls;
                    propObject.fnSetWidth($self.val());
                    if ($self.val() === MF_IDE_CONSTANTS.buttonControlWidthType.custom) {
                        cntrls.WidthInPercent.wrapperDivCntrl.show();
                        propObject.fnSetWidthInPercent("100");
                        cntrls.WidthInPercent.controlCntrl.val("100");
                    }
                    else {
                        cntrls.WidthInPercent.wrapperDivCntrl.hide();
                        propObject.fnSetWidthInPercent("0");
                    }
                    mFicientIde.ButtonControlHelper.setHtmlOfBtnCntrlInUI(propObject);
                },
                WidthInPercent: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject,
                        iVal = 0;
                    if ($.isNumeric($self.val())) {
                        iVal = parseInt($self.val());
                        if ((0 < iVal) && (iVal <= 100)) {
                            propObject.fnSetWidthInPercent($self.val());
                        }
                        else {
                            showMessage("The value of width should be a number between 0 and 100.", DialogType.Error);
                            $self.val(propObject.fnGetWidthInPercent());
                        }
                    }
                    else {
                        showMessage("The value of width should be a number between 0 and 100.", DialogType.Error);
                        $self.val(propObject.fnGetWidthInPercent());
                    }
                    mFicientIde.ButtonControlHelper.setHtmlOfBtnCntrlInUI(propObject);
                },
                BackgroundColor: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.fnSetBackgroundColor($self.val());
                    mFicientIde.ButtonControlHelper.setHtmlOfBtnCntrlInUI(propObject);
                },
                Icon: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var cntrls = evntData.cntrls;
                    propObject.fnSetIcon($self.val());
                    if ($self.val() === "0") {
                        cntrls.IconPosition.wrapperDivCntrl.hide();
                        propObject.fnSetIconPosition(MF_IDE_CONSTANTS.buttonControlIconPosition.left);
                    }
                    else {
                        cntrls.IconPosition.wrapperDivCntrl.show();
                        //propObject.fnSetIconPosition(MF_IDE_CONSTANTS.buttonControlIconPosition.left);
                    }
                    mFicientIde.ButtonControlHelper.setHtmlOfBtnCntrlInUI(propObject);
                },
                IconPosition: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.fnSetIconPosition($self.val());
                    mFicientIde.ButtonControlHelper.setHtmlOfBtnCntrlInUI(propObject);
                },
                Size: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.fnSetDataMini($self.val());
                    mFicientIde.ButtonControlHelper.setHtmlOfBtnCntrlInUI(propObject);
                },
                Text: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.fnSetText($self.val());
                    mFicientIde.ButtonControlHelper.setHtmlOfBtnCntrlInUI(propObject);
                }
            }
        },
        Behaviour: {
            Display: function (control, cntrlObj) {
                $(control).val(cntrlObj.fnGetConditionalDisplay());
            },
            SelectControl: function (control, cntrlObj, args) {
                if (cntrlObj.fnGetConditionalDisplay() === "0") {
                    args.cntrlHtmlObjects.wrapperDiv.show();
                }
                else {
                    args.cntrlHtmlObjects.wrapperDiv.hide();
                }
            },
            Enable: function (control, cntrlObj) {
                $(control).val(cntrlObj.fnGetEnable());
            },
            EnableConditions: function (control, cntrlObj, args) {
                if (cntrlObj.fnGetEnable() === "0") {
                    args.cntrlHtmlObjects.wrapperDiv.show();
                }
                else {
                    args.cntrlHtmlObjects.wrapperDiv.hide();
                }
            },
            OnTouchProcType: function (control, cntrlObj, args) {
                $(control).val(cntrlObj.fnGetOnTouchProcType());
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    //propObject.conditionalDisplay = evnt.target.value;
                    propObject.fnSetConditionalDisplay(evnt.target.value);
                    if (evnt.target.value == "0") {
                        cntrls.SelectControl.wrapperDivCntrl.show();
                    }
                    else {
                        cntrls.SelectControl.wrapperDivCntrl.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        //propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        propObject.fnSetCondDisplayControlProps($('#divConditionalControl').data("FinalJson")());
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                },
                Enable: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.fnSetEnable(evnt.target.value);
                    if (evnt.target.value == "0") {
                        cntrls.EnableConditions.wrapperDivCntrl.show();
                    }
                    else {
                        cntrls.EnableConditions.wrapperDivCntrl.hide();
                    }
                },
                EnableConditions: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {},
                        enableConditions = propObject.fnGetEnableConditions();
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";

                    $('#divConditionalControl').mfConditionalLogicControl({
                        FirstDropDownOptions: json,
                        ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(),
                        IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)),
                        IsEdit: (enableConditions) ? true : false,
                        SavedJsonIfEdit: enableConditions ? JSON.stringify(enableConditions) : "",
                        firstColWidth: 120
                    });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.fnSetEnableConditions($('#divConditionalControl').data("FinalJson")());
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                },
                OnTouchProcType: function (evnt) {
                    var evntData = evnt.data;
                    var $self = $(this);
                    var propObject = evntData.propObject;
                    propObject.fnSetOnTouchProcType($self.val());
                    propObject.fnSetIsTouchTypeProcChanged(true);
                }
            }

        },
        Advance: {
            Scripts: function () {
            },
            eventHandlers: {
                Scripts: function (evnt) {
                    $('#drpCntrlEvents').html();
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    $('#spanOnChngUserDefinedName').text(propObject.fnGetUserDefinedName());
                    _jsAceEditorOnChang.setTextInEditor("");
                    var scriptOnChange = '';
                    var options = '<option value="onTouch">onTouch</option>';
                    $('#drpCntrlEvents').html(options);
                    $('#drpCntrlEvents').val("onTouch");
                    $.uniform.update('#drpCntrlEvents');

                    scriptOnChange = propObject.fnGetOnTouchScript();
                    if (!mfUtil.isNullOrUndefined(scriptOnChange) && !mfUtil.isEmptyString(scriptOnChange)) {
                        _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));
                    }
                    else {
                        _jsAceEditorOnChang.setTextInEditor("");
                    }
                    SubProcShowCntrlJsEditor(true, "Scripts");
                    $('[id$=popup_custom_buttom_save]').unbind('click');
                    $('[id$=popup_custom_buttom_save]').bind('click', function () {
                        bindAdvanceScriptButton(propObject);
                        return false;
                    });
//                    $('[id$=btnCntrlJsSaveClose]').unbind('click');
//                    $('[id$=btnCntrlJsSaveClose]').bind('click', function () {
//                        bindAdvanceScriptButton(propObject);
//                        SubProcShowCntrlJsEditor(false);
//                        return false;
//                    });
                    $('[id$=popup_custom_buttom_close]').unbind('click');
                    $('[id$=popup_custom_buttom_close]').bind('click', function () {
                        SubProcShowCntrlJsEditor(false);
                        return false;
                    });

                    //mFicientIde.HiddenFieldControlHelper.formDetailsHtml(propObject);
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }

        // _appearance = {
        //     getWidthInPercent
        // };
        // function _getBehaviourActionBtn1Cntrl() {
        //     var divData = _getDivControlHelperData();
        //     var objControl = null;
        //     if (divData) {
        //         objControl = divData.getControlDtls(
        //         MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
        //             "Behaviour", "ActionButton1");
        //     }
        //     return objControl;
        // }
        return {

        };
    })()
};
mFicientIde.PropertySheetJson.Button = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BUTTON,
    "groups": [{
        "name": "Control Properties",
        "prefixText": "ControlProperties",
        "hidden": true,
        "properties": [{
            "text": "Type",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
            "prefixText": "Type",
            "noOfLines": "0",
            "validations": [],
            "customValidations": [],
            "events": [],
            "CntrlProp": "",
            "HelpText": "",
            "init": {
                "maxlength": 50,
                "disabled": true,
                "defltValue": "Button",
                "hidden": false
            }
        }, {
            "text": "Name",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
            "prefixText": "Name",
            "noOfLines": "0",
            "validations": [

            ],
            "customValidations": [

            ],
            "events": [{
                "name": "change",
                "func": PROP_JSON_HTML_MAP.button.groups.ControlProperties.eventHandlers.Name,
                "context": "",
                "arguments": {}
            }],
            "CntrlProp": "",
            "HelpText": "",
            "init": {
                "maxlength": 50,
                "disabled": false,
                "defltValue": "",
                "hidden": false
            }
        }, {
            "text": "Description",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
            "prefixText": "Description",
            "noOfLines": "0",
            "validations": [

            ],
            "customValidations": [

            ],
            "events": [{
                "name": "change",
                "func": PROP_JSON_HTML_MAP.button.groups.ControlProperties.eventHandlers.Description,
                "context": "",
                "arguments": {}
            }

            ],
            "CntrlProp": "",
            "HelpText": "",
            "init": {
                "maxlength": 250,
                "disabled": false,
                "defltValue": "",
                "hidden": false
            }
        }]
    }, {
        "name": "Appearance",
        "prefixText": "Appearance",
        "hidden": false,
        "properties": [{
            "text": "Width",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
            "prefixText": "Width",
            "defltValue": "1",
            "validations": [],
            "customValidations": [
            ],
            "events": [{
                "name": "change",
                "func": PROP_JSON_HTML_MAP.button.groups.Appearance.eventHandlers.Width,
                "context": "",
                "arguments": {
                    "cntrls": [{
                        grpPrefText: "Appearance",
                        cntrlPrefText: "WidthInPercent",
                        rtrnPropNm: "WidthInPercent"
                    }]
                }
            }],
            "CntrlProp": "",
            "HelpText": "",
            "selectValueBy": "",
            "init": {
                "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                "disabled": false,
                "hidden": false,
                "defltValue": "1"
            },
            "options": [{
                "text": "Full",
                "value": MF_IDE_CONSTANTS.buttonControlWidthType.full
            }, {
                "text": "Auto",
                "value": MF_IDE_CONSTANTS.buttonControlWidthType.auto
            }, {
                "text": "Custom",
                "value": MF_IDE_CONSTANTS.buttonControlWidthType.custom
            }]
        }, {
            "text": "Width ( % )",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
            "prefixText": "WidthInPercent",
            "noOfLines": "0",
            "validations": [],
            "customValidations": [],
            "events": [{
                "name": "change",
                "func": PROP_JSON_HTML_MAP.button.groups.Appearance.eventHandlers.WidthInPercent,
                "context": "",
                "arguments": {}
            }],
            "CntrlProp": "",
            "helpText": {},
            "init": {
                "maxlength": 3,
                "disabled": false,
                "defltValue": "100",
                "hidden": true
            }
        }, {
            "text": "Background Color",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
            "prefixText": "BackgroundColor",
            "defltValue": "",
            "validations": [],
            "customValidations": [],
            "events": [{
                "name": "change",
                "func": PROP_JSON_HTML_MAP.button.groups.Appearance.eventHandlers.BackgroundColor,
                "context": "",
                "arguments": {
                }
            }],
            "CntrlProp": "",
            "helpText": {
            },
            "selectValueBy": "",
            "init": {
                "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                "disabled": false,
                "hidden": false
            },
            "options": [{
                "text": "Default",
                "value": "#AFB6BB"
            }, {
                "text": "Red",
                "value": "#E0392E"
            }, {
                "text": "Yellow",
                "value": "#EDB90F"
            }, {
                "text": "Green",
                "value": "#1FB18A"
            }, {
                "text": "Blue",
                "value": "#2B84D2"
            }]
        }, {
            "text": "Icon",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
            "prefixText": "Icon",
            "defltValue": "",
            "validations": [],
            "customValidations": [],
            "events": [{
                "name": "change",
                "func": PROP_JSON_HTML_MAP.button.groups.Appearance.eventHandlers.Icon,
                "context": "",
                "arguments": {
                    "cntrls": [{
                        grpPrefText: "Appearance",
                        cntrlPrefText: "IconPosition",
                        rtrnPropNm: "IconPosition"
                    }]

                }
            }],
            "CntrlProp": "",
            "helpText": {},
            "selectValueBy": "",
            "init": {
                "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                "disabled": false,
                "hidden": false
            },
            "options": []
        }, {
            "text": "Icon Position",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
            "prefixText": "IconPosition",
            "defltValue": "",
            "validations": [],
            "customValidations": [],
            "events": [{
                "name": "change",
                "func": PROP_JSON_HTML_MAP.button.groups.Appearance.eventHandlers.IconPosition,
                "context": "",
                "arguments": {}
            }],
            "CntrlProp": "",
            "helpText": {

            },
            "selectValueBy": "",
            "init": {
                "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                "disabled": false,
                "hidden": true
            },
            "options": [{
                "text": "Left",
                "value": MF_IDE_CONSTANTS.buttonControlIconPosition.left
            }, {
                "text": "Right",
                "value": MF_IDE_CONSTANTS.buttonControlIconPosition.right
            }, {
                "text": "Top",
                "value": MF_IDE_CONSTANTS.buttonControlIconPosition.top
            }, {
                "text": "Bottom",
                "value": MF_IDE_CONSTANTS.buttonControlIconPosition.bottom
            }]
        }, {
            "text": "Size",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
            "prefixText": "Size",
            "defltValue": "0",
            "validations": [],
            "customValidations": [],
            "events": [{
                "name": "change",
                "func": PROP_JSON_HTML_MAP.button.groups.Appearance.eventHandlers.Size,
                "context": "",
                "arguments": {
                    "cntrls": []
                }
            }],
            "CntrlProp": "",
            "HelpText": "",
            "selectValueBy": "",
            "init": {
                "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                "disabled": false,
                "hidden": false
            },
            "options": [{
                "text": "Normal",
                "value": MF_IDE_CONSTANTS.CNTRL_SIZES.normal
            }, {
                "text": "Mini",
                "value": MF_IDE_CONSTANTS.CNTRL_SIZES.mini
            }]


        }, {
            "text": "Text",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
            "prefixText": "Text",
            "noOfLines": "0",
            "validations": [

              ],
            "customValidations": [

              ],
            "events": [{
                "name": "change",
                "func": PROP_JSON_HTML_MAP.button.groups.Appearance.eventHandlers.Text,
                "context": "",
                "arguments": {}
            }],
            "CntrlProp": "",
            "HelpText": "",
            "init": {
                "maxlength": 50,
                "disabled": false,
                "defltValue": "",
                "hidden": false
            }
        }]
    }, {
        "name": "Advance",
        "prefixText": "Advance",
        "hidden": false,
        "properties": [{
            "text": "Scripts",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
            "prefixText": "Scripts",
            "disabled": false,
            "hidden": true,
            "noOfLines": "0",
            "validations": [

            ],
            "customValidations": [],
            "events": [{
                "name": "click",
                "func": PROP_JSON_HTML_MAP.button.groups.Advance.eventHandlers.Scripts,
                "context": "",
                "arguments": {}
            }],
            "CntrlProp": "",
            "HelpText": "",
            "init": {
                "disabled": false,
                "defltValue": "",
                "hidden": false
            }
        }]
    }, {
        "name": "Behaviour",
        "prefixText": "Behaviour",
        "hidden": false,
        "properties": [{
            "text": "Display",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
            "prefixText": "Display",
            "defltValue": "1",
            "validations": [],
            "customValidations": [],
            "events": [{
                "name": "change",
                "func": PROP_JSON_HTML_MAP.button.groups.Behaviour.eventHandlers.Display,
                "context": "",
                "arguments": {
                    "cntrls": [{
                        grpPrefText: "Behaviour",
                        cntrlPrefText: "SelectControl",
                        rtrnPropNm: "SelectControl"
                    }]
                }
            }

            ],
            "CntrlProp": "",
            "HelpText": "",
            "selectValueBy": "",
            "init": {
                "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                "disabled": false,
                "hidden": false,
                "defltValue": "1"
            },
            "options": [{
                "text": "Always",
                "value": "1"
            }, {
                "text": "Conditional",
                "value": "0"
            }]
        }, {
            "text": "Select Control",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
            "prefixText": "SelectControl",
            "disabled": false,
            "noOfLines": "0",
            "validations": [],
            "customValidations": [],
            "events": [{
                "name": "click",
                "func": PROP_JSON_HTML_MAP.button.groups.Behaviour.eventHandlers.ConditionalDisplay,
                "context": "",
                "arguments": {}
            }],
            "CntrlProp": "",
            "HelpText": "",
            "init": {
                "disabled": false,
                "defltValue": "Select Control Name",
                "hidden": true
            }
        }, {
            "text": "Enable",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
            "prefixText": "Enable",
            "defltValue": "1",
            "validations": [],
            "customValidations": [],
            "events": [{
                "name": "change",
                "func": PROP_JSON_HTML_MAP.button.groups.Behaviour.eventHandlers.Enable,
                "context": "",
                "arguments": {
                    "cntrls": [{
                        grpPrefText: "Behaviour",
                        cntrlPrefText: "EnableConditions",
                        rtrnPropNm: "EnableConditions"
                    }]
                }
            }

              ],
            "CntrlProp": "",
            "HelpText": "",
            "selectValueBy": "",
            "init": {
                "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                "disabled": false,
                "hidden": false,
                "defltValue": "1"
            },
            "options": [{
                "text": "Always",
                "value": "1"
            }, {
                "text": "Conditional",
                "value": "0"
            }]
        }, {
            "text": "Enable Conditions",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
            "prefixText": "EnableConditions",
            "disabled": false,
            "noOfLines": "0",
            "validations": [],
            "customValidations": [],
            "events": [{
                "name": "click",
                "func": PROP_JSON_HTML_MAP.button.groups.Behaviour.eventHandlers.EnableConditions,
                "context": "",
                "arguments": {}
            }],
            "CntrlProp": "",
            "HelpText": "",
            "init": {
                "disabled": false,
                "defltValue": "Select Control Name",
                "hidden": true
            }
        }, {
            "text": "On Touch",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
            "prefixText": "OnTouchProcType",
            "defltValue": "1",
            "validations": [],
            "customValidations": [],
            "events": [{
                "name": "change",
                "func": PROP_JSON_HTML_MAP.button.groups.Behaviour.eventHandlers.OnTouchProcType,
                "context": "",
                "arguments": {
                }
            }

                  ],
            "CntrlProp": "",
            "HelpText": "",
            "selectValueBy": "",
            "init": {
                "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                "disabled": false,
                "hidden": false,
                "defltValue": "1"
            },
            "options": [{
                "text": "Transition",
                "value": MF_IDE_CONSTANTS.procTypeForMfObjsEvents.transition
            }, {
                "text": "Command",
                "value": MF_IDE_CONSTANTS.procTypeForMfObjsEvents.command
            }]
        }]
    }]
};

function Button(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Button;
    this.description = opts.description;
    this.type = opts.type;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;

    this.width = opts.width;
    this.widthInPercent = opts.widthInPercent;
    this.backgroundColor = opts.backgroundColor;
    this.icon = opts.icon;
    this.iconPosition = opts.iconPosition;
    this.dataMini = opts.dataMini;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.onTouchScript = opts.onTouchScript;
    this.refreshControls = opts.refreshControls;
    this.enable = opts.enable;
    this.enableConditions = opts.enableConditions;
    this.onTouchProcType = opts.onTouchProcType;
    this.isTouchProcTypeChanged = opts.isTouchProcTypeChanged;
    if (!opts.text) {
        this.text = opts.userDefinedName;
    }
    else {
        this.text = opts.text;
    }
}

Button.prototype = new ControlNew();
Button.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.button;
Button.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.conditionalDisplay = "1";
    this.dataMini = MF_IDE_CONSTANTS.CNTRL_SIZES.normal;
    this.icon = "";
    this.backgroundColor = "#AFB6BB";
    this.width = MF_IDE_CONSTANTS.buttonControlWidthType.full;
    this.widthInPercent = "0";
    this.iconPosition = MF_IDE_CONSTANTS.buttonControlIconPosition.left;
    this.enable = "1";
    this.onTouchProcType = "1";
    this.isTouchProcTypeChanged = false;
    this.text = "Button";
};
Button.prototype.fnGetId = function () {
    return this.id;
};
Button.prototype.fnSetId = function (value) {
    this.id = value;
};
Button.prototype.fnGetUserDefinedName = function () {
    return this.userDefinedName;
};
Button.prototype.fnSetUserDefinedName = function (value) {
    this.userDefinedName = value;
};
Button.prototype.fnGetDescription = function () {
    return this.description;
};
Button.prototype.fnSetDescription = function (value) {
    this.description = value;
};
Button.prototype.fnGetWidth = function () {
    return this.width;
};
Button.prototype.fnIsWidthFull = function () {
    var strWidthType = this.fnGetWidth();
    if (strWidthType === MF_IDE_CONSTANTS.buttonControlWidthType.full) {
        return true;
    }
    else {
        return false;
    }
};
Button.prototype.fnIsWidthCustom = function () {
    var strWidthType = this.fnGetWidth();
    if (strWidthType === MF_IDE_CONSTANTS.buttonControlWidthType.custom) {
        return true;
    }
    else {
        return false;
    }
};
Button.prototype.fnIsWidthAuto = function () {
    var strWidthType = this.fnGetWidth();
    if (strWidthType === MF_IDE_CONSTANTS.buttonControlWidthType.auto) {
        return true;
    }
    else {
        return false;
    }
};
Button.prototype.fnSetWidth = function (value) {
    this.width = value;
};
Button.prototype.fnGetWidthInPercent = function () {
    return this.widthInPercent;
};
Button.prototype.fnSetWidthInPercent = function (value) {
    this.widthInPercent = value;
};
Button.prototype.fnGetBackgroundColor = function () {
    return this.backgroundColor;
};
Button.prototype.fnSetBackgroundColor = function (value) {
    this.backgroundColor = value;
};
Button.prototype.fnIsBackgroundColorDefault = function () {
    if (this.fnGetBackgroundColor() === "#AFB6BB") {
        return true;
    }
    else {
        return false;
    }
};
Button.prototype.fnGetIcon = function () {
    return this.icon;
};
Button.prototype.fnSetIcon = function (value) {
    this.icon = value;
};
Button.prototype.fnHasIcon = function () {
    var strIcon = this.fnGetIcon();
    if (mfUtil.isNullOrUndefined(strIcon) || mfUtil.isEmptyString(strIcon)) {
        return false;
    }
    else {
        return true;
    }
};
Button.prototype.fnGetIconPosition = function () {
    return this.iconPosition;
};
Button.prototype.fnSetIconPosition = function (value) {
    this.iconPosition = value;
};
Button.prototype.fnGetDataMini = function () {
    return this.dataMini;
};
Button.prototype.fnSetDataMini = function (value) {
    this.dataMini = value;
};
Button.prototype.fnIsSizeDataMini = function () {
    var strSize = this.fnGetDataMini();
    if (strSize === MF_IDE_CONSTANTS.CNTRL_SIZES.normal) {
        return false;
    }
    else {
        return true;
    }
};
Button.prototype.fnGetConditionalDisplay = function () {
    return this.conditionalDisplay;
};
Button.prototype.fnSetConditionalDisplay = function (value) {
    this.conditionalDisplay = value;
};
Button.prototype.fnGetCondDisplayControlProps = function () {
    return this.condDisplayCntrlProps;
};
Button.prototype.fnSetCondDisplayControlProps = function (value) {
    this.condDisplayCntrlProps = value;
};
Button.prototype.fnGetOnTouchScript = function () {
    return this.onTouchScript;
};
Button.prototype.fnSetOnTouchScript = function (value) {
    this.onTouchScript = value;
};
Button.prototype.fnGetRefreshControls = function () {
    return this.refreshControls;
};
Button.prototype.fnSetRefreshControls = function (value) {
    this.refreshControls = value;
};
Button.prototype.fnGetEnable = function () {
    return this.enable;
};
Button.prototype.fnSetEnable = function (value) {
    this.enable = value;
};
Button.prototype.fnGetEnableConditions = function () {
    return this.enableConditions;
};
Button.prototype.fnSetEnableConditions = function (value) {
    this.enableConditions = value;
};
Button.prototype.fnGetOnTouchProcType = function () {
    return this.onTouchProcType;
};
Button.prototype.fnIsOnTouchProcTypeTransition = function () {
    var onTouchProcType = this.fnGetOnTouchProcType();
    if (onTouchProcType === MF_IDE_CONSTANTS.procTypeForMfObjsEvents.transition) {
        return true;
    }
    else {
        return false;
    }
};
Button.prototype.fnSetOnTouchProcType = function (value) {
    this.onTouchProcType = value;
};
Button.prototype.fnRename = function (name) {
    if (!this.fnIsNewlyAdded() &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.fnSetUserDefinedName(name);
    }
    else if (this.fnGetUserDefinedName() != name) {
        this.fnSetUserDefinedName(name);
    }
};
Button.prototype.fnIsNewlyAdded = function () {
    return this.isNew;
};
Button.prototype.fnGetIsTouchTypeProcChanged = function () {
    return this.isTouchProcTypeChanged;
};
Button.prototype.fnSetIsTouchTypeProcChanged = function (val) {
    this.isTouchProcTypeChanged = val;
};
Button.prototype.fnGetText = function () {
    return this.text;
};
Button.prototype.fnSetText = function (val) {
    this.text = val;
};
// Button.prototype.fnGetOnExitTasks = function () {
//   return this.onExitTasks;
// };
// Button.prototype.fnSetOnExitTasks = function (value/*ViewTasksCmdObjectBinding*/) {
//   if(value && value instanceof ViewTasksCmdObjectBinding){
//       this.onExitTasks = value;
//   }
// };
// Button.prototype.fnDeleteOnExitTasks = function () {
//   this.onExitTasks = null;
// };
// Button.prototype.fnGetOnExitTaskProcMode = function () {
//   return this.onExitTasksProcMode;
// };
// Button.prototype.fnSetOnExitTasksProcMode = function (value) {
//   this.onExitTasksProcMode = value;
// };
// Button.prototype.fnSetDefaultOfOnExitTasksProcMode = function () {
//   this.onExitTasksProcMode = MF_IDE_CONSTANTS.cmdObjectSettingsProcMode.sequential;
// };
// Button.prototype.fnGetAllOnExitDatabindingObjs = function(){
//     var objOnExitTasks = this.fnGetOnExitTasks(),
//         allDatabindingObjs = [];
//     if(objOnExitTasks){
//         allDatabindingObjs = objOnExitTasks.fnGetDatabindingObjs();
//     }
//     return allDatabindingObjs;
// };
// Button.prototype.fnGetAllDsNamesInOnExitTasks = function(){
//     var aryOnExitDatabindingObjs = this.fnGetAllOnExitDatabindingObjs(),
//         aryDsNames = [],
//         j=0,
//         objDatabindingObj= null;
//     if($.isArray(aryOnExitDatabindingObjs) && aryOnExitDatabindingObjs.length>0){
//         for (j = 0; j <= aryOnExitDatabindingObjs.length - 1; j++) {
//             objDatabindingObj = aryOnExitDatabindingObjs[j];
//             if(objDatabindingObj && objDatabindingObj.fnGetDatasetName()){
//                 aryDsNames.push(objDatabindingObj.fnGetDatasetName());
//             }
//         }
//     }
//     return aryDsNames;
// };
// Button.prototype.fnResetObjectDetails=function(){
//     var onExitTasks = this.fnGetOnExitTasks(),
//     objNewOnExitCmd= null;
//     this.fnDeleteOnExitTasks();
//     if(onExitTasks){
//         objNewOnExitCmd = new ViewTasksCmdObjectBinding(
//             onExitTasks.dialogSettings,
//             onExitTasks.databindingObjs,
//             onExitTasks.pushMsgSettings);
//         objNewOnExitCmd.fnResetObjectDetails();
//         this.fnSetOnExitTasks(objNewOnExitCmd);
//     }
// };
mFicientIde.ButtonControlHelper = (function () {
    var FONT_SIZE_FOR_NORMAL_SIZE = 16;
    var FONT_SIZE_FOR_MINI_SIZE = 12.5;
    var BORDER_WIDTH = 1;
    var PADDING_WITH_IMAGE = 3.5/*2.5em + 1 em*/;
    var PADDING_WITHOUT_IMAGE = 2/*1em+1em*/;
    var BUTTON_ICON_THEME = {
        black: "1",
        white: "2"
    };
    function _getIconSuffixesByTheme(iconTheme) {
        var suffix = "";
        switch (iconTheme) {
            case BUTTON_ICON_THEME.black:
                suffix = "-black.png";
                break;
            case BUTTON_ICON_THEME.white:
                suffix = "-white.png";
                break;
        }
        return suffix;
    }
    function _getThemeOfButtonIcon() {
        return BUTTON_ICON_THEME.white;
        // if(isBackgroudColorDflt){
        //     return BUTTON_ICON_THEME.black;
        // }
        // else{
        //     return BUTTON_ICON_THEME.white;
        // }
    }
    var _buttonStyle = {
        /**
        * Gets the width of the button style as object
        * @method getWidth
        * @param {CONSTANTS.buttonControlWidthType value} widthTypeVal
        * @param {string optional} widthInPercent required only when widthTypeVla is custon otherwise not used
        * @return {object} Returns style of css as object or empty object
        */
        getWidth: function (widthTypeVal, widthInPercent) {
            var objStyle = {};
            switch (widthTypeVal) {
                case MF_IDE_CONSTANTS.buttonControlWidthType.full:
                    objStyle = { "width": "auto", "display": "block" };
                    break;
                case MF_IDE_CONSTANTS.buttonControlWidthType.auto:
                    objStyle = { "width": "auto", "display": "inline" };
                    break;
                case MF_IDE_CONSTANTS.buttonControlWidthType.custom:
                    objStyle = { "width": widthInPercent + "%", "display": "block" };
                    break;
            }
            return objStyle;
        },
        /**
        * Gets the background color style as string
        * @method getBackgroundColor
        * @param {string} colorHash
        * @return {string} Returns style of css as string 
        */
        getBackgroundColor: function (colorHash) {
            return { "background-color": colorHash };
        },
        /**
        * Gets the icon style for button as string
        * @method getIconStyle
        * @param {string} iconName
        * @param {CONSTANTS.buttonControlIconPosition val} position
        * @return {string} Returns style of css as string 
        */
        getIconStyle: function (iconName, position) {
            var objStyle = {},
                strPosition = "";
            if (mfUtil.isNullOrUndefined(iconName) || mfUtil.isEmptyString(iconName)) {
                objStyle = {};
            }
            else {

                objStyle["background-image"] = "url(" + MF_HELPERS.jqueryMobileIconsHelper.getCompleteIconPath(iconName) + ")";
                objStyle["background-repeat"] = "no-repeat";
                switch (position) {
                    case MF_IDE_CONSTANTS.buttonControlIconPosition.left:
                        strPosition = "center left";
                        break;
                    case MF_IDE_CONSTANTS.buttonControlIconPosition.right:
                        strPosition = "center right";
                        break;
                    case MF_IDE_CONSTANTS.buttonControlIconPosition.top:
                        strPosition = "center top";
                        break;
                    case MF_IDE_CONSTANTS.buttonControlIconPosition.bottom:
                        strPosition = "center bottom";
                        break;
                }
                objStyle["background-position"] = strPosition;
            }
            return objStyle;
        },
        /**
        * Gets the  style for button as string
        * @method getSize
        * @param {CONSTANTS.CNTRL_SIZES val} sizeType
        * @return {string} Returns style of css as string 
        */
        getSize: function (sizeType) {
            var objStyle = {};
            switch (sizeType) {
                case MF_IDE_CONSTANTS.CNTRL_SIZES.normal:
                    objStyle = { "font-size": "16px", "height": "30px" };
                    break;
                case MF_IDE_CONSTANTS.CNTRL_SIZES.mini:
                    objStyle = { "font-size": "12.5px", "height": "25px" };
                    break;
            }
            return objStyle;
        },
        getIconUrl: function (iconName, iconTheme) {
            var strFullName = iconName + _getIconSuffixesByTheme(iconTheme);
            return "url(" + MF_HELPERS.jqueryMobileIconsHelper.getCompleteIconPath(strFullName) + ")";
        },
        getWidthInPixelForUI: function (widthInPercent, isBtnWithIcon, iconPos, sizeType, contWidth) {
            var iWidthInPercent = parseInt(widthInPercent),
                fontSize = FONT_SIZE_FOR_NORMAL_SIZE,
                iSize = 0,
                iPadding = PADDING_WITH_IMAGE;

            switch (sizeType) {
                case MF_IDE_CONSTANTS.CNTRL_SIZES.normal:
                    fontSize = FONT_SIZE_FOR_NORMAL_SIZE;
                    break;
                case MF_IDE_CONSTANTS.CNTRL_SIZES.mini:
                    fontSize = FONT_SIZE_FOR_MINI_SIZE;
                    break;
            }
            if (isBtnWithIcon &&
                        (iconPos === MF_IDE_CONSTANTS.buttonControlIconPosition.left ||
                         iconPos === MF_IDE_CONSTANTS.buttonControlIconPosition.right
                        )
                ) {
                iPadding = PADDING_WITH_IMAGE;
            }
            else {
                iPadding = PADDING_WITHOUT_IMAGE;
            }
            iSize = ((iWidthInPercent / 100) * contWidth) - ((iPadding * fontSize) + (2 * BORDER_WIDTH));
            return Math.ceil(iSize).toString();
            return
        },
        getFontColor: function () {
            return { "color": "white" };
            // if(isBackgroundColorDflt){
            //     return {"color": "#333"};
            // }
            // else{
            //     return {"color":"white"};
            // }
        }
    };
    var _buttonStyleCss = {
        getWidth: function (widthTypeVal) {
            var strStyle = "";
            switch (widthTypeVal) {
                case MF_IDE_CONSTANTS.buttonControlWidthType.full:
                    strStyle = "full";
                    break;
                case MF_IDE_CONSTANTS.buttonControlWidthType.auto:
                    strStyle = "inline";
                    break;
                case MF_IDE_CONSTANTS.buttonControlWidthType.custom:
                    strStyle = "custom";
                    break;
            }
            return strStyle;
        },
        getIconPositionCss: function (position) {
            var strPosition = "";
            switch (position) {
                case MF_IDE_CONSTANTS.buttonControlIconPosition.left:
                    strPosition = "iconLeft";
                    break;
                case MF_IDE_CONSTANTS.buttonControlIconPosition.right:
                    strPosition = "iconRight";
                    break;
                case MF_IDE_CONSTANTS.buttonControlIconPosition.top:
                    strPosition = "iconTop";
                    break;
                case MF_IDE_CONSTANTS.buttonControlIconPosition.bottom:
                    strPosition = "iconBottom";
                    break;
            }
            return strPosition;
        },
        getSize: function (sizeType) {
            var strCss = {};
            switch (sizeType) {
                case MF_IDE_CONSTANTS.CNTRL_SIZES.normal:
                    strCss = "normal";
                    break;
                case MF_IDE_CONSTANTS.CNTRL_SIZES.mini:
                    strCss = "mini";
                    break;
            }
            return strCss;
        },
        getIconCss: function () {
            return "btnIcon";
        },
        getBtnNameSpanCss: function () {
            return "btnName";
        }
    };
    /**
    * Gets the html of button to be shown in the form designer
    * @param {object} args
    {
    {CONSTANTS.buttonControlWidthType} widthType,
    {boolean} hasIcon => default is false,
    {CONSTANTS.buttonControlIconPosition} iconPos => optional provide if hasIcon is true
    {string } icon=> name of the icon,
    {string} widthInPercent
    }
    * @return {string}
    */
    // function _getStyleForButtonInUI(button){
    //     var objStyle = {};
    //     $.extend(objStyle,_buttonStyle.getWidth(button.fnGetWidth(),button.fnGetWidthInPercent()));
    //     $.extend(objStyle,_buttonStyle.getBackgroundColor(button.fnGetBackgroundColor()));
    //     $.extend(objStyle,_buttonStyle.getIconStyle(button.fnGetIcon(),button.fnGetIconPosition()));
    //     $.extend(objStyle,_buttonStyle.getSize(button.fnGetDataMini()));

    //     return objStyle;
    // }
    // function _setStyleOfBtnCntrlInUI(button){
    //     var $btnDiv = $('#DIV_'+button.fnGetId());
    //     $btnDiv.css(_getStyleForButtonInUI(button));
    // }

    function _setHtmlOfBtnCntrlInUI(button) {
        var objStyle = {},
            strCss = "",
            strHtml = "",
            isButtonWithIcon = button.fnHasIcon(),
            $btnDiv = $('#DIV_' + button.fnGetId()),
            $cont = $btnDiv.closest('div.mfIdeButton'),
            strWidthInPixelForUI = "",
            strWidthInPercent = "100",
            strCssPropertyToSet = "width",
            tempObj = null;

        //isBackgroundColorDeflt = button.fnIsBackgroundColorDefault();    
        $btnDiv.removeAttr('style');
        $btnDiv.removeClass();
        //get css to add + html to be inserted
        strCss += _buttonStyleCss.getWidth(button.fnGetWidth()) + " ";
        if (isButtonWithIcon) {
            strCss += _buttonStyleCss.getIconPositionCss(button.fnGetIconPosition()) + " ";
            strHtml += '<span style="background-image:';
            strHtml += _buttonStyle.getIconUrl(button.fnGetIcon(), _getThemeOfButtonIcon());
            strHtml += '" class="' + _buttonStyleCss.getIconCss() + '"></span>';
        }
        strCss += _buttonStyleCss.getSize(button.fnGetDataMini()) + " ";
        strHtml += '<span class="' + _buttonStyleCss.getBtnNameSpanCss() + '">' + button.fnGetText() + '</span>';

        $btnDiv.addClass("btnStyleCont node");
        $btnDiv.addClass(strCss);
        //$btnDiv.removeAttr('style');

        $btnDiv.html(strHtml);

        //get inline styles    
        $.extend(objStyle, _buttonStyle.getBackgroundColor(button.fnGetBackgroundColor()));
        /*When the button width was set to custom and the percentage width was changed. 
        the width is set for the div
        After that when the value was set to full
        The width is set to auto in css. 
        but the value of width which was set previously is not reset in google chrome
        It width is auto it is adjusting itself to that width
        even if i had removed the style and css of the div and resetting it again. 
        For this we are setting the value for all the conditions. 
        */
        if (button.fnIsWidthFull()) {
            strWidthInPercent = "100";
            strCssPropertyToSet = "width";
        }
        else if (button.fnIsWidthAuto()) {
            strWidthInPercent = "100";
            strCssPropertyToSet = "max-width";
        }
        else if (button.fnIsWidthCustom()) {
            strWidthInPercent = button.fnGetWidthInPercent();
            strCssPropertyToSet = "width";
        }
        strWidthInPixelForUI = _buttonStyle.getWidthInPixelForUI(
                                   strWidthInPercent,
                                   isButtonWithIcon,
                                   isButtonWithIcon ? button.fnGetIconPosition() : null,
                                   button.fnGetDataMini(),
                                   $cont.width()
                               );
        tempObj = {};
        tempObj[strCssPropertyToSet] = strWidthInPixelForUI + "px";
        $.extend(objStyle, tempObj);
        $.extend(objStyle, _buttonStyle.getFontColor());
        $btnDiv.css(objStyle);
    }

    return {
        setHtmlOfBtnCntrlInUI: function (button) {
            _setHtmlOfBtnCntrlInUI(button);
        }
    };
})();


function bindAdvanceScriptButton(propObject) {
    scriptOnChange = _jsAceEditorOnChang.getText();
    if (!mfUtil.isNullOrUndefined(scriptOnChange) && !mfUtil.isEmptyString(scriptOnChange)) {
        propObject.fnSetOnTouchScript(Base64.encode(scriptOnChange));
    }
    else {
        propObject.fnSetOnTouchScript("");
    }
}
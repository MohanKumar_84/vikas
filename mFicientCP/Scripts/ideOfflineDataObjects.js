﻿
var dsSelectedData;
var dsCompleteData;
var offlineDatatables;
var pageLength = 12;
var DataObjParaCount = 1;

function initializeDataObjects() {
    initializeControls();
    resetInitialControls();
    deserializeDataJson();
    bindOfflineDataObjectsTable();
    bindOfflineDatatables();
    addNewOfflineDataObject();

    //OnChange event of QueryType Dropdown
    bindOnChangeEventOfObjectType();
}

function bindOfflineDataObjectsTable() {
    $('#divListDataObject').html('<table cellpadding="0" cellspacing="0" border="0" width="100%" class="display" id="tblDataObjects"></table>');
    $('#tblDataObjects').dataTable({
        "data": dsSelectedData,
        "pageLength": pageLength,
        "language": {

            "lengthMenu": "_MENU_"

        },
        "columns": [
            { "title": "ID" },
            { "title": "OBJECT" },
            { "title": "DESCRIPTION" },
            { "title": "TYPE" }
        ],
        "columnDefs": [
			{ "visible": false, "targets": [0] }
		]
    });

    DataObjectRowClick();
    searchText("tblDataObjects");
    $('#tblDataObjects_length select').prop('id', 'pagesize');
    $('#tblDataObjects_length select').prop('class', 'UseUniformCss');
    $('#tblDataObjects_filter').css("padding-bottom", "10px");

    CallUnformCss('[id$=pagesize]');
}

function displayBasedOnQueryType(queryType, isEditMode) {
    switch (queryType) {
        case "0":
            initializeControls();
            $('#divQueryOutput').hide();
            $('#divColumns').hide();
            $('#divInputParams').hide();
            $('#divDataObjQueryCondition').hide();
            $('#divOfflineTableUsed').hide();
            break;
        case "1":
            if ($('select[id$=ddlOfflineTable]').val() != "-1") {
                $('#divQueryOutput').show();
                $('#divInputParams').show();
                $('#divDataObjQueryCondition').show();
            }
            else {
                $('#divQueryOutput').hide();
                $('#divInputParams').hide();
                $('#divDataObjQueryCondition').hide();
            }
            $('#divColumns').hide();
            $('#divOfflineTableUsed').show();
            break;
        case "2":
            bindOfflineDataColumns('ddlOfflineTable');
            $('#divQueryOutput').hide();
            if ($('select[id$=ddlOfflineTable]').val() != "-1") {
                $('#divColumns').show();
                $('#divInputParams').show();
            }
            else {
                $('#divColumns').hide();
                $('#divInputParams').hide();
            }
            $('#divDataObjQueryCondition').hide();
            $('#divOfflineTableUsed').show();
            break;
        case "3":
            bindOfflineDataColumns('ddlOfflineTable');
            $('#divQueryOutput').hide();
            if ($('select[id$=ddlOfflineTable]').val() != "-1") {
                $('#divColumns').show();
                $('#divDataObjQueryCondition').show();
                $('#divInputParams').show();
            }
            else {
                $('#divColumns').hide();
                $('#divDataObjQueryCondition').hide();
                $('#divInputParams').hide();
            }
            $('#divOfflineTableUsed').show();
            break;
        case "4":
            $('#divQueryOutput').hide();
            $('#divColumns').hide();
            if ($('select[id$=ddlOfflineTable]').val() != "-1") {
                $('#divDataObjQueryCondition').show();
                $('#divInputParams').show();
            }
            else {
                $('#divDataObjQueryCondition').hide();
                $('#divInputParams').hide();
            }
            $('#divOfflineTableUsed').show();
            break;
    }
}

function bindOnChangeEventOfObjectType() {
    $('select[id$=ddl_DataObjType]').change(function (event) {
        displayBasedOnQueryType($(this).val(), false);
    });
}

function deserializeDataJson() {
    dsSelectedData = jQuery.parseJSON($('[id$=hdfSelectedData]').val());
    dsCompleteData = jQuery.parseJSON($('[id$=hdfCompleteData]').val());
    offlineDatatables = jQuery.parseJSON($('[id$=hdfOfflineTables]').val());
}

function bindOfflineDatatables() {
    $('select[id$=ddlOfflineTable]').html('');
    var options = '<option value="-1">Select Table</option>';
    if (offlineDatatables) {
        $.each(offlineDatatables, function () {
            options += '<option value="' + this["TableId"].trim() + '">' + this["TableName"].trim() + '</option>';
        });
    }

    $('select[id$=ddlOfflineTable]').html(options);
    $('[id$=ddlOfflineTable]').val("-1");
    $('select[id$=ddlOfflineTable]').parent().children("span").text($('select[id$=ddlOfflineTable]').children('option:selected').text());
    $('[id$=hdfOfflineTablesUsed]').val($('select[id$=ddlOfflineTable]').val());

    $('select[id$=ddlOfflineTable]').change(function (event) {
        $('[id$=hdfOfflineTablesUsed]').val($('select[id$=ddlOfflineTable]').val());
        if ($(this).val() != "-1") {
            switch ($('select[id$=ddl_DataObjType]').val()) {
                case "0":
                    $('#divQueryOutput').hide();
                    $('#divColumns').hide();
                    $('#divDataObjQueryCondition').hide();
                    $('#divInputParams').hide();
                    break;
                case "1":
                    $('#divQueryOutput').show();
                    $('#divColumns').hide();
                    $('#divDataObjQueryCondition').show();
                    $('#divInputParams').show();
                    break;
                case "2":
                    bindOfflineDataColumns(event.target.id);
                    $('#divColumns').show();
                    $('#divDataObjQueryCondition').hide();
                    $('#divQueryOutput').hide();
                    $('#divInputParams').show();
                    break;
                case "3":
                    bindOfflineDataColumns(event.target.id);
                    $('#divColumns').show();
                    $('#divDataObjQueryCondition').show();
                    $('#divQueryOutput').hide();
                    $('#divInputParams').show();
                    break;
                case "4":
                    $('#divDataObjQueryCondition').show();
                    $('#divQueryOutput').hide();
                    $('#divColumns').hide();
                    $('#divInputParams').show();
                    break;
            }
        }
        else
            $('#divColumns').hide();
        makeOutputParaTextBoxAutoComplete();
    });
}

function initializeControls() {
    // DetailsDivControls
    $('[id$=lblDataObjectName]').text('');
    $('[id$=lblDesc]').text('');
    $('[id$=lblDataObjType]').text('');
    $('[id$=lblQuery]').text('');
    $('[id$=lblTablesUsed]').text('');
    $('[id$=lblErrorMessage]').text('');
    $('[id$=lblAllowRetry]').text('');
    $('[id$=lblExitOnError]').text('');

    // EditDivControls
    $('[id$=lblEditDataObj_Name]').val('');
    $('[id$=ddl_DataObjType]').val('0');
    $('select[id$=ddl_DataObjType]').parent().children("span").text($('select[id$=ddl_DataObjType]').children('option:selected').text());
    $('[id$=ddlOfflineTable]').val('-1');
    $('select[id$=ddlOfflineTable]').parent().children("span").text($('select[id$=ddlOfflineTable]').children('option:selected').text());
    $('#lblInputParameters').html('No Parameters defined yet');
    $('[id$=txtDataObj_Query]').val('');
    $('[id$=txtDataObj_QueryPara]').val('');
    $('[id$=lblColJson]').text('');

    $('[id$=ddl_DataObjType]').removeAttr("disabled");
    $('[id$=txt_CopyObject]').val('');

    //Hidden Fields
    $('[id$=hdfDbCmdPara]').val('');
    $('[id$=hdfAutoboxMannulPara]').val('');
    $('[id$=hdfInsertQueryPara]').val('');
    $('[id$=hdfOfflineTablesUsed]').val('');
    UniformControlDesign();
}

function resetInitialControls() {
    $('[id$=txtDataObj_Name]').val('');
    $('[id$=txtDataObj_Desc]').val('');
    $('[id$=txtErrorMessages]').val('');
    $('[id$=ddlAllowRetry]').val('0');
    $('select[id$=ddlAllowRetry]').parent().children("span").text($('select[id$=ddlAllowRetry]').children('option:selected').text());
    $('[id$=ddlExitOnError]').val('0');
    $('select[id$=ddlExitOnError]').parent().children("span").text($('select[id$=ddlExitOnError]').children('option:selected').text());
}

function viewOfflineDataObject() {
    initializeControls();
    resetInitialControls();
    $('#divDataObjectDetails').show();
    $('#divEditDataObject').hide();
    for (var i = 0; i < dsCompleteData.length; i++) {
        if ($('[id$=hdfDataObjectId]').val() == dsCompleteData[i].ID) {
            $('[id$=lblDataObjectName]').text(dsCompleteData[i].Name);
            $('[id$=lblDesc]').text(dsCompleteData[i].Description);
            $('[id$=lblErrorMessage]').text(dsCompleteData[i].ErrorMessage);
            $('[id$=lblAllowRetry]').text(dsCompleteData[i].AllowRetry == false ? 'No' : 'Yes');
            $('[id$=lblExitOnError]').text(dsCompleteData[i].ExitOnError == false ? 'No' : 'Yes');
            $('[id$=hdfDbCmdPara]').val(dsCompleteData[i].InputParameters);
            $('[id$=hdfOfflineTablesUsed]').val(dsCompleteData[i].OfflineTable);

            if (offlineDatatables) {
                $.each(offlineDatatables, function () {
                    if (this["TableId"].trim() === dsCompleteData[i].OfflineTable) {
                        $('[id$=lblTablesUsed]').text(this["TableName"].trim());
                        return false;
                    }
                });
            }

            var strInputParaJson = '';
            if ($('[id$=hdfDbCmdPara]').val().length > 0) {
                $.each(jQuery.parseJSON($('[id$=hdfDbCmdPara]').val()), function () {
                    if (strInputParaJson.length > 0)
                        strInputParaJson += ', ' + this['para'] + ' [' + this['typ'] + '] ';
                    else
                        strInputParaJson += this['para'] + ' [' + this['typ'] + '] ';
                });
            }
            $('[id$=lblInputs]').text("");
            if (strInputParaJson.length > 0)
                $('[id$=lblInputs]').text(strInputParaJson);

            switch(dsCompleteData[i].QueryType)
            {
                case 1:
                    $('[id$=lblDataObjType]').text("SELECT");
                    $('#dtlDivColumns').hide();
                    $('#dtlDivWhere').show();
                    $('#dtlDivOutputColumns').show();
                    $('#dtlDivInputParams').show();
                    $('[id$=lblQuery]').text(dsCompleteData[i].Query);
                    var outputCols = '';
                    if (dsCompleteData[i].OutputParameters.length > 0) {
                        $.each(JSON.parse(dsCompleteData[i].OutputParameters), function () {
                            if (outputCols.trim().length > 0)
                                outputCols += ',';
                            outputCols += this["col"];
                        });
                    }
                    $('[id$=lblOutputColumns]').text(outputCols);
                    break;
                case 2:
                    $('[id$=lblDataObjType]').text("INSERT");
                    $('#dtlDivColumns').show();
                    $('#dtlDivWhere').hide();
                    $('#dtlDivOutputColumns').hide();
                    $('#dtlDivInputParams').show();
                    $('[id$=lblColJson]').text(dsCompleteData[i].OutputParameters);
                    $('[id$=lblColumns]').text(viewOfflineDataColumns());
                    break;
                case 3:
                    $('[id$=lblDataObjType]').text("UPDATE");
                    $('#dtlDivColumns').show();
                    $('#dtlDivWhere').show();
                    $('#dtlDivOutputColumns').hide();
                    $('#dtlDivInputParams').show();
                    $('[id$=lblColJson]').text(dsCompleteData[i].OutputParameters);
                    $('[id$=lblQuery]').text(dsCompleteData[i].Query);
                    $('[id$=lblColumns]').text(viewOfflineDataColumns());
                    break;
                case 4:
                    $('[id$=lblDataObjType]').text("DELETE");
                    $('#dtlDivColumns').hide();
                    $('#dtlDivWhere').show();
                    $('#dtlDivOutputColumns').hide();
                    $('#dtlDivInputParams').show();
                    $('[id$=lblQuery]').text(dsCompleteData[i].Query);
                    break;
            }
        }
    }
    UniformControlDesign();
}

function editOfflineDataObject() {
    initializeControls();
    $('#divDataObjectDetails').hide();
    $('#editDataObjHeader').show();
    $('#addDataObjHeader').hide();
    $('#divDataObjName').hide();
    $('#divEditDataObject').show();
    $('[id$=btnDataObj_Cancel]').show();
    var strInputParaJson = '', strpara = '';
    for (var i = 0; i < dsCompleteData.length; i++) {
        if ($('[id$=hdfDataObjectId]').val() == dsCompleteData[i].ID) {
            $('[id$=lblEditDataObj_Name]').text(dsCompleteData[i].Name);
            $('[id$=txtDataObj_Name]').val(dsCompleteData[i].Name);
            $('[id$=txtDataObj_Desc]').val(dsCompleteData[i].Description);
            $('[id$=txtDataObj_Query]').val(dsCompleteData[i].Query);
            $('[id$=hdfOfflineTablesUsed]').val(dsCompleteData[i].OfflineTable);
            $('[id$=hdfDbCmdPara]').val(dsCompleteData[i].InputParameters);
            $('[id$=txtErrorMessages]').val(dsCompleteData[i].ErrorMessage);
            $('[id$=ddl_DataObjType]').val(dsCompleteData[i].QueryType.toString());
            $('select[id$=ddl_DataObjType]').parent().children("span").text($('select[id$=ddl_DataObjType]').children('option:selected').text());
            
            $('[id$=ddlOfflineTable]').val(dsCompleteData[i].OfflineTable.toString());
            $('select[id$=ddlOfflineTable]').parent().children("span").text($('select[id$=ddlOfflineTable]').children('option:selected').text());


            $('[id$=ddlAllowRetry]').val(dsCompleteData[i].AllowRetry == false ? '0' : '1');
            $('select[id$=ddlAllowRetry]').parent().children("span").text($('select[id$=ddlAllowRetry]').children('option:selected').text());
            $('[id$=ddlExitOnError]').val(dsCompleteData[i].ExitOnError == false ? '0' : '1');
            $('select[id$=ddlExitOnError]').parent().children("span").text($('select[id$=ddlExitOnError]').children('option:selected').text());

            if ($('[id$=hdfDbCmdPara]').val().length > 0) {
                $.each(jQuery.parseJSON($('[id$=hdfDbCmdPara]').val()), function () {
                    if (strInputParaJson.length > 0)
                        strInputParaJson += ', ' + this['para'] + ' [' + this['typ'] + '] ';
                    else
                        strInputParaJson += this['para'] + ' [' + this['typ'] + '] ';

                    if (strpara.length > 0)
                        strpara += ', ';
                    strpara += this['para'];
                });
            }
            if (strInputParaJson.length > 0) $('#lblInputParameters').html(strInputParaJson);
            else
                $('#lblInputParameters').html("No Parameters defined yet");

            $('[id$=hdfInsertQueryPara]').val(strpara);

            if (dsCompleteData[i].QueryType === 2 || dsCompleteData[i].QueryType === 3) {
                $('[id$=lblColJson]').text(dsCompleteData[i].OutputParameters);
            }

            if(dsCompleteData[i].QueryType === 1){
                var strOutParaJson = ''
                if (parseInt(dsCompleteData[i].OutputParameters.length, 10) > 0) {
                    var index = 0;
                    $.each(JSON.parse(dsCompleteData[i].OutputParameters), function () {
                        if (strOutParaJson.length > 0)
                            strOutParaJson += ', '
                        strOutParaJson += '{"id":"' + this["col"] + '", "name":"' + this["col"] + '", "fnm":"' + this["col"] + '", "lnm": "' + this["col"] + '"}';                   
                        index = index +1;
                    });
                    makeOutputParaTextBoxAutoComplete(JSON.parse('[' + strOutParaJson + ']'));
                }
            }

            displayBasedOnQueryType(dsCompleteData[i].QueryType.toString(), true);
            $('#divOfflineTableUsed').show();
        }
    }

    $('[id$=txtDataObj_Name]').attr("disabled", "disabled");
    AddParameterInDbCommand();
    //dbcmdQueryTextBoxChange();

    $('[id$=ddl_DataObjType]').attr("disabled", "disabled");
    UniformControlDesign();
}

function addNewOfflineDataObject() {
    initializeControls();
    resetInitialControls();
    $('#divDataObjectDetails').hide();
    $('#divEditDataObject').show();
    $('#divEditDataObjLbl').hide();
    $('#editDataObjHeader').hide();
    $('#addDataObjHeader').show();
    $('#divDataObjName').show();
    $('[id$=btnDataObj_Cancel]').show();
    $('[id$=txtDataObj_Name]').removeAttr("disabled");
    $('[id$=hdfDataObjectId]').val('');
    $('#divQueryOutput').hide();
    $('#divColumns').hide();
    $('#divInputParams').hide();
    $('#divDataObjQueryCondition').hide();
    $('#divOfflineTableUsed').hide();
    AddParameterInDbCommand();
    makeOutputParaTextBoxAutoComplete();
    UniformControlDesign();
}

function cancelButtonClick() {
    if ($('[id$=hdfDataObjectId]').val().trim() != "")
        viewOfflineDataObject();
    else
        addNewOfflineDataObject();
}

function DataObjectRowClick() {
    var table = $('#tblDataObjects').DataTable();
    $('#tblDataObjects tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
//            if ($('#intial1').length) {
//                $('#intial1').removeClass('test');
//                $('#intial1').find('td').removeClass('test');
//                $('#intial1').removeAttr('id');
//                $('#tblDataObjects tbody tr.selected').attr('id', 'intial1');
//                $('#tblDataObjects tbody tr.selected').find('td:eq(0)').addClass('test');
//                $('#tblDataObjects tbody tr.selected').addClass('test');

//            }
//            else {
//                $('#tblDataObjects tbody tr.selected').attr('id', 'intial1');
//                $('#tblDataObjects tbody tr.selected').find('td:eq(0)').addClass('test');
//                $('#tblDataObjects tbody tr.selected').addClass('test');
//            }
        }

        var rowData = table.row(this).data();
        if (rowData != undefined) {
            $('[id$=hdfDataObjectId]').val(rowData[0]);
            viewOfflineDataObject();
        }
        else {
            alert('No Offline Data Object Selected');
        }
    });
}

function UniformControlDesign() {
    CallUnformCss('[id$=ddl_DataObjType]');
    CallUnformCss('[id$=ddlAllowRetry]');
    CallUnformCss('[id$=ddlExitOnError]');
    CallUnformCss('[id$=ddlOfflineTable]');
}

function validateDataObject() {
    var isValid = true;
    var strMessage = "";

    if (checkIfObjectAlreadyExist()) {
        strMessage += " * Offline Data Object Name already exist.";
        $('#aMessage').html(strMessage);
        SubProcBoxMessage(true);
        isValid = false;
    }
    else {
        strMessage += validateObjectName('txtDataObj_Name');
        if ($('[id$=ddl_DataObjType]')[1].value == "0")
            strMessage += " * Please select object type.</br>";

        if ($('[id$=ddlOfflineTable]')[1].value == "-1")
            strMessage += " * Please select offline table used.</br>";

        if ($('[id$=txtDataObj_Query]').val().trim().length > 0) {
            $('[id$=txtDataObj_Query]').val($('[id$=txtDataObj_Query]').val().trim().replace(/(\r\n|\n|\r)/gm, ""));
        }


        if (strMessage.length != 0) {
            $('#aMessage').html(strMessage);
            SubProcBoxMessage(true);
            isValid = false;
        }
        else {
            if ($('[id$=ddl_DataObjType]')[1].value == "2" || $('[id$=ddl_DataObjType]')[1].value == "3")
                createOfflineColumnsJson();
            else if ($('[id$=ddl_DataObjType]')[1].value == "1")
                createOutputColumnsJson();
        }
    }
    return isValid;
}

function validateObjectName(cntrlId) {
    var strMessage = "";
    if ($('[id$=' + cntrlId + ']').length != 0) {
        var strVal = ValidateNameInIde($('[id$=' + cntrlId + ']').val());
        if (strVal != 0) {
            if (strVal == 1) {
                strMessage += " * Please enter offline data object name.</br>";
            }
            else if (strVal == 2) {
                strMessage += " * Object name cannot be more than 20 charecters.</br>";
            }
            else if (strVal == 3) {
                strMessage += " * Object name cannot be less than 3 charecters.</br>";
            }
            else if (strVal == 4) {
                strMessage += " * Please enter valid object name.</br>";
            }
            else if (strVal == 5) {
                strMessage += " * Please enter valid object name.</br>";
            }
        }
    }
    return strMessage;
}

function validateCopiedObject(){
    var isValid = true;
    var strMessage = "";

    if (checkIfCopiedObjectAlreadyExist()) {
        strMessage += " * Offline Data Object Name already exist.";
        $('#aMessage').html(strMessage);
        SubProcBoxMessage(true);
        isValid = false;
    }
    else {
       strMessage += validateObjectName('txt_CopyObject');
        if (strMessage.length != 0) {
            $('#aMessage').html(strMessage);
            SubProcBoxMessage(true);
            isValid = false;
        }
    }
    if (isValid)
        ShowCopyObjectModalPopup(false);
    return isValid;
}

function checkIfObjectAlreadyExist() {
    if ($('[id$=hdfDataObjectId]').val().length <= 0) {
        for (var i = 0; i < dsCompleteData.length; i++) {
            if ($('[id$=txtDataObj_Name]').val().trim() == dsCompleteData[i].Name.trim())
                return true;
        }
    }
}

function checkIfCopiedObjectAlreadyExist() {
    for (var i = 0; i < dsCompleteData.length; i++) {
        if ($('[id$=txt_CopyObject]').val().trim() == dsCompleteData[i].Name.trim())
            return true;
    }
}

function ShowCopyObjectModalPopup(_bol) {
    if (_bol) {
        showModalPopUp('divCopyObject', 'Copy Objct', 400);
    }
    else {
        $('#divCopyObject').dialog('close');

    }
    UniformControlDesign();
}

// Confirmation message popup
function SubProcConfirmBoxMessageDB(_bol, _title, _width) {
    if (_bol) {
        showModalPopUp('SubProcConfirmBoxMessage', _title, _width);
    }
    else {

        $('#SubProcConfirmBoxMessage').dialog('close');
    }
}

//show delete message popup
function SubProcBoxMessage(_bol) {
    if (_bol) {
        showModalPopUp('SubProcBoxMessage', 'Error', 400);
        $('#btnOkErrorMsg').focus();
        return false;
    }
    else {

        $('#SubProcBoxMessage').dialog('close');
    }
}

/*-------------------------------------------------------Offline Data Tables Used-----------------------------------------------------*/

function SubProcAddOfflineTables(_bol) {
    if (_bol) {
        var selectedOfflineDataTables = [];
        $.each($('[id$=lblOfflineTables]').html().split(','), function () {
            selectedOfflineDataTables.push(this.trim());
        });
        $('#offlineTablesDiv').offlineDataObjectTables({ OfflineTables: offlineDatatables, SelectedOfflineTables: selectedOfflineDataTables });
        showModalPopUp('SubProcAddOfflineTables', 'Select Tables', 300);
    }
    else {
        $('#SubProcAddOfflineTables').dialog('close');
    }
    UniformControlDesign();
}

function saveSelectedOfflineDataTables() {
    var selectedTables = "";
    $.each($('#offlineTablesDiv').data("SelectedOfflineTables"), function () {
        if (parseInt(selectedTables.trim().length, 10) > 0)
            selectedTables += ",";
        if (parseInt(this.trim().length, 10) > 0) selectedTables += this;
    }); 

    $('[id$=lblOfflineTables]').text(selectedTables);
    $('[id$=hdfOfflineTablesUsed]').val(selectedTables);
    SubProcAddOfflineTables(false);
}

function updateOfflineTablesUsed(value) {
    var oldArryTbls = [];
    oldArryTbls = $('[id$=hdfOfflineTablesUsed]').val().split(',');

    var valueArry = [];
    valueArry = value.split(',');

    var arryAllOfflineDataTables = [];
    $.each(offlineDatatables, function () {
        if (offlineDatatables.length > 0 && $.inArray(this["TableName"].trim(), arryAllOfflineDataTables) <= -1) {
            arryAllOfflineDataTables.push(this["TableName"]);
        }
    });

    var strSelectedTbls = '';
    $.each(oldArryTbls, function () {
        if ($.inArray(this.trim(), arryAllOfflineDataTables) > -1) {
            if (parseInt(strSelectedTbls.length, 10) > 0) strSelectedTbls += ",";
            strSelectedTbls += this;
        }
    });

    if (valueArry.length > 0) {
        $.each(valueArry, function () {
            if (parseInt(this.trim().length, 10) > 0 && $.inArray(this.trim(), oldArryTbls) <= -1) {
                if (parseInt(strSelectedTbls.length, 10) > 0) strSelectedTbls += ",";
                strSelectedTbls += this;
            }
        });
    }

    $('[id$=lblOfflineTables]').text(strSelectedTbls);
    $('[id$=hdfOfflineTablesUsed]').val(strSelectedTbls);
}

function deleteOfflineDataObject() {
    $('#aMsgCmdDelCmf').html('Do you want to delete this object ?');
    SubProcDeleteCmdConfrmation(true, 'Delete Object');
    UniformControlDesign();
}

(function ($) {
    $.fn.offlineDataObjectTables = function (optns) {
        var CONSTANTS = {
            OfflineTables: [],
            SelectedOfflineTables: [],
            arrCheckbox: []
        };

        var options = $.extend(optns);
        var $self = this;
        $self.html(_getOfflineTablesHtml());
        $.each(CONSTANTS.arrCheckbox, function () {
            $('#' + this).unbind('click');
            $('#' + this).bind('click', function () {
                if ($(this).is(":checked")) {
                    options.SelectedOfflineTables.push(this.id.split('_')[1]);
                }
                else {
                    options.SelectedOfflineTables.pop(this.id.split('_')[1]);
                }
            });
        });

        this.data("SelectedOfflineTables", options.SelectedOfflineTables);

        function _getOfflineTablesHtml() {
            var strHtml = "";
            $('#offlineTablesDiv').html('');
            $.each(options.OfflineTables, function () {
                strHtml += "<tr>"
                            + "<td id=\"" + this["TableName"] + "\"> "
                            + this["TableName"]
                            + "</td>"
                            + "<td style=\"width: 30px; padding-left: 25px;\">"
                            + "<input id=\"chk_" + this["TableName"] + "\" type=\"checkbox\" " + ($.inArray(this["TableName"].trim(), options.SelectedOfflineTables) > -1 ? " checked=\"checked\" " : "") + " />"
                            + "</td>"
                            + "</tr>";

                CONSTANTS.arrCheckbox.push("chk_" + this["TableName"]);
            });
            return strHtml;
        }
    };
})(jQuery);

/*------------------------------------------------Columns Used-------------------------------------------------------*/
function getOfflineTableJson() {
    var offlineDataTableJson = '';
    if (offlineDatatables) {
        $.each(offlineDatatables, function () {
            if (this["TableId"].trim() === $('[id$=hdfOfflineTablesUsed]').val()) {
                offlineDataTableJson = this["TableJson"];
            }
        });
    }
    return offlineDataTableJson;
}

function getOfflineTableColumnsArray() {
    var arryOfflineDataTableCols = [];
    var offlineDataTableJson = getOfflineTableJson();
    if (offlineDataTableJson) {
        $.each($.parseJSON(offlineDataTableJson).tbl.col, function (index) {
            arryOfflineDataTableCols.push(JSON.parse('{"id":"' + this["cnm"] + '", "name":"' + this["cnm"] + '", "fnm":"' + this["cnm"] + '", "lnm": "' + this["cnm"] + '"}'));
        });
    }
    return arryOfflineDataTableCols;
}


function bindOfflineDataColumns(drpOfflineTableId) {
    var selectedOfflineDataCols = [];
    var selectedOfflineDataColParams = [];
    if ($('[id$=hdfDataObjectId]').val().length > 0) {
        $.each($.parseJSON($('[id$=lblColJson]').html()), function () {
            selectedOfflineDataColParams.push(this["col"] + "_" + this["para"]);
            selectedOfflineDataCols.push(this["col"]);
        });
    }

    $('#tblOfllineDataCols').offlineDataObjectColumns({ OfflineColumnsJson: getOfflineTableJson(), OfflineColumnsPara: selectedOfflineDataColParams, OfflineSelectedCol: selectedOfflineDataCols });
    setIntellisenseForOfflineColumns();
}


(function ($) {
    $.fn.offlineDataObjectColumns = function (optns) {
        var CONSTANTS = {
            OfflineColumnsJson: '',
            OfflineColumnsPara: [],
            OfflineSelectedCol :[]
        };

        var options = $.extend(optns);
        var $self = this;
        $self.html(_getOfflineColumnsHtml());
        setIntellisenseForOfflineColumns();

        function _getOfflineColumnsHtml() {
            var strHtml = "";
            $('#tblOfllineDataCols').html('');
            if(options.OfflineColumnsJson.length > 0){
                $.each($.parseJSON(options.OfflineColumnsJson).tbl.col, function () {
                    strHtml += "<tr id=\"" + this["cnm"] + "\">"
                                + "<td>"
                                + this["cnm"]
                                + "</td>"
                                + "<td style=\"width: 30px; padding-left: 25px;\">"
                                + "<input id=\"txt_" + this["cnm"] + "\" type=\"text\" value=\"" + _getColumnParameter(this["cnm"]) + "\"/>"
                                + "</td>"
                                + "</tr>";                
                });
            }
            return strHtml;
       }

        function _getColumnParameter(colName){
            var paraName = '';
            if($.inArray(colName, options.OfflineSelectedCol) > -1) {
                $.each(options.OfflineColumnsPara, function(){
                    if(this.split('_')[0] === colName)
                    paraName = this.split('_')[1];
                });
            }
            return paraName;
        }
    };
})(jQuery);

function createOfflineColumnsJson(){
    var colJson = '';
    var $offlineColumnsTable = $('#tblOfllineDataCols');
        $offlineColumnsTable.find('tr').each(function( index ) {
            if($('#txt_' + this.id).val().length > 0){
                if(colJson.length > 0)
                    colJson += ",";
                colJson += '{"col" : "' + this.id + '", "para":"' + $('#txt_' + this.id).val() + '"}';
            }
        });

        $('[id$=hdfOfflineColumns]').val('[' + colJson + ']');
        $('[id$=hdfOfflineTablesUsed]').val($('select[id$=ddlOfflineTable]').val());
}

function setIntellisenseForOfflineColumns() {
    var $offlineColumnsTable = $('#tblOfllineDataCols');
    var dataArray = [];
    var strpara=$('[id$=hdfInsertQueryPara]').val().split(',');
    jQuery.each(strpara, function () {
        dataArray.push($.trim(this));
    });
    $offlineColumnsTable.find('tr').each(function (index) {
        if ($('[id$=hdfInsertQueryPara]').val().length > 0)
            SetIntellisenseOfOfflineInputParameter(dataArray, '#txt_' + this.id);
    });
    SetIntellisenseOfOfflineInputParameter(dataArray, '[id$=txtDataObj_Query]');
}

function viewOfflineDataColumns() {
    var strCols = '';
    if ($('[id$=lblColJson]').html().length > 0) {
        $.each($.parseJSON($('[id$=lblColJson]').html()), function () {
            if (strCols.length > 0)
                strCols += ',';
            strCols += this["col"] + ' = ' + this["para"];
        });
    }
    return strCols;
}

/*------------------------------------------------InputParameters----------------------------------------------------*/

function SubProcDbCmdAddPara(_bol) {
    if (_bol) {
        showModalPopUp('SubProcDbCmdAddPara', 'Object Parameters', 350);
    }
    else {
        if ($('#SubProcDbCmdAddPara').is(':visible')) $('#SubProcDbCmdAddPara').dialog('close');
    }
    UniformControlDesign();
}

//this sub is used to add parameter in database command.
function AddParameterInDbCommand() {
    DbCmdParaCount = 1;
    $('#AddParaDiv').html('');
    var strInputParaJson = '';
    if ($('[id$=hdfDbCmdPara]').val().length > 0) {
        $.each(jQuery.parseJSON($('[id$=hdfDbCmdPara]').val()), function () {
            if (strInputParaJson.length > 0)
                strInputParaJson += ', ' + this['para'];
            else
                strInputParaJson += this['para'];
            var strHtml = '<div id="QpAddParaDiv_' + DbCmdParaCount + '" class="QPContentRow">' +
                                            '<div class="QPContentText">' +
                                                this['para'] +
                                            '</div>' +
                                            '<div class="QPContentDelete" align="center">' +
                                                '<img id="imgDelQueryPara_' + DbCmdParaCount + '" alt="" src="//enterprise.mficient.com/images/cross.png" />' +
                                            '</div>' +
                      '</div><div style="clear: both;"></div>';
            $('#AddParaDiv').append(strHtml);
            $('#imgDelQueryPara_' + DbCmdParaCount).bind('click', function (e) {
                $('#QpAddParaDiv_' + this.id.split('_')[1]).remove();
                createDbCmdParaJson();
            });
            DbCmdParaCount = DbCmdParaCount + 1;
        });
        if (strInputParaJson.length > 0) $('#lblInputParameters').html(strInputParaJson);
        else
            $('#lblInputParameters').html("No Parameters defined yet");

    }
    else
        $('#lblInputParameters').html("No Parameters defined yet");

        $('#imgAddQueryPara').bind('click', function (e) {
            DbCmdParaCount = AddNewPara(DbCmdParaCount);
        });

        $('#txtAddParaName').keydown(function (e) {
            if (e.which == 13) {
                DbCmdParaCount = AddNewPara(DbCmdParaCount);
            }
        });
}

function AddNewPara(DbCmdParaCount){
        if ($('#txtAddParaName').val().trim().length == 0 || $('#ddlAddParaType').val() == "-1") {
            return;
        }
        if (IsStringContainsSpecialChar($('#txtAddParaName').val())) {
            CommonErrorMessages(77);
            return;
        }
        if (IsDbCmdParaNameAlreadyExists()) {
            CommonErrorMessages(34);
            return;
        }
        var strHtml = '<div id="QpAddParaDiv_' + DbCmdParaCount + '"  class="QPContentRow">' +
                                            '<div class="QPContentText">' +
                                                $('#txtAddParaName').val() +
                                            '</div>' +
                                            '<div class="QPContentDelete" align="center">' +
                                                '<img id="imgDelQueryPara_' + DbCmdParaCount + '" alt="" src="//enterprise.mficient.com/images/cross.png" />' +
                                            '</div>' +
                      '</div><div style="clear: both;"></div>';
        $('#AddParaDiv').append(strHtml);
        $('#imgDelQueryPara_' + DbCmdParaCount).bind('click', function (e) {
            $('#QpAddParaDiv_' + this.id.split('_')[1]).remove();
            createDbCmdParaJson();
        });
        DbCmdParaCount = DbCmdParaCount + 1;
        $('#txtAddParaName').val('');
        $('#ddlAddParaType').val('String');
        createDbCmdParaJson();
        setIntellisenseForOfflineColumns();
        return DbCmdParaCount;
}

//this sub is used to check parameter name already exists or not
function IsDbCmdParaNameAlreadyExists() {
    var Exists = false;
    jQuery.each($('#AddParaDiv')[0].children, function () {
        var innerJson = "";
        jQuery.each(this.children, function () {
            if (this.className == "QPContentText") {
                if ($(this).html().trim() == $('#txtAddParaName').val().trim()) {
                    Exists = true;
                }
            }
        });
    });
    return Exists;
}

//this sub is used to create json of dropdown command
function createDbCmdParaJson() {
    var outerJson = "";
    var strpara = "";
    var strInputParaJson = '';
    jQuery.each($('#AddParaDiv')[0].children, function () {
        var innerJson = "";
        jQuery.each(this.children, function () {
            if (this.className == "QPContentText") {
                if (strInputParaJson.length > 0)
                    strInputParaJson += ', ';
                innerJson += '"para":' + JSON.stringify($(this).html()) + ',"typ":"String"';
                strInputParaJson += $(this).html();
                if (strpara.length > 0) strpara += ',';
                strpara += $(this).html();
            }
        });
        if (innerJson.length != 0) {
            if (outerJson.length > 0) outerJson += ",";
            outerJson += '{' + innerJson + '}';
        }
    });
    if (strInputParaJson.length > 0) $('#lblInputParameters').html(strInputParaJson);
    else $('#lblInputParameters').html('No Parameters defined yet');
    $('[id$=hdfInsertQueryPara]').val(strpara);
    $('[id$=hdfDbCmdPara]').val('[' + outerJson + ']');
    var dataArray = [];
    jQuery.each(strpara.split(','), function () {
        dataArray.push($.trim(this));
    });
    SetIntellisenseOfOfflineInputParameter(dataArray, '[id$=txtDataObj_Query]');
}

function SetIntellisenseOfOfflineInputParameter(strpara, textboxid) {
    var KeyString = ":";   
    $(textboxid).atwho({
        at: KeyString,
        data: strpara,
        limit: 200,
        startWithSpace: false,
        callbacks: {
            afterMatchFailed: function (at, el) {
                if (at == '#') {
                    tags.push(el.text().trim().slice(1));
                    this.model.save(tags);
                    this.insert(el.text().trim());
                    return false;
                }
            }
        }
    });
}

/*------------------------------------------------------------Output Parameters--------------------------------------------------*/

//Auto complete textbox to add parameters (facebook style textbox)

function makeOutputParaTextBoxAutoComplete(prePopulateJsonObject) {
    $($("[id$=txtDataObj_QueryPara]").parent()).find('.token-input-list-facebook').each(function () {
        $(this).remove();
    });
    var lstColName = getOfflineTableColumnsArray();

    if (prePopulateJsonObject) {
        if (lstColName) {
            $('[id$=txtDataObj_QueryPara]').tokenInput(lstColName, {
                theme: "facebook",
                prePopulate: prePopulateJsonObject,
                preventDuplicates: true
            });
        }
    }
    else {
        if (lstColName) {
            $('[id$=txtDataObj_QueryPara]').tokenInput(lstColName, {
                theme: "facebook",
                preventDuplicates: true
            });
        }
    }

    $($("[id$=txtDataObj_QueryPara]").parent()).find('.token-input-list-facebook').css('width', '81%');
}

function createOutputColumnsJson(){
    var colJson = '';
    var outputCols = [];
    if ($('[id$=txtDataObj_QueryPara]')[1].value.length > 0) {
        outputCols = $('[id$=txtDataObj_QueryPara]')[1].value.split(',');

        $.each(outputCols, function (index) {
            if (colJson.length > 0)
                colJson += ",";
            colJson += '{"col" : "' + this + '", "para":""}';
        });
    }

    $('[id$=hdfOfflineColumns]').val('[' + colJson + ']');
}



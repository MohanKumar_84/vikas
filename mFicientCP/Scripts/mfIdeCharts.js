﻿
/*---------------------------------------------------PieChart---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.PieChart = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            Title: function (control, appObj) {
                $(control).val(appObj.chartTitle);
            },
            DataLabelThreshold: function (control, appObj) {
                $(control).val(appObj.dataLblThreshold);
            },
            ShowValueAs: function (control, appObj) {
                $(control).val(appObj.valueAs);
            },
            Animation: function (control, appObj) {
                $(control).val(appObj.animation)
            },
            DonutStyle: function (control, cntrlObj) {
                var strDonutStyle = cntrlObj.fnGetDonutStyle();
                //var objInnerRadiusCntrls = PROP_JSON_HTML_MAP
                //  .PieChart
                //.propSheetCntrl
                //.Appearance.getInnerRadius();

                if (!mfUtil.isNullOrUndefined(strDonutStyle)
                && !mfUtil.isEmptyString(strDonutStyle)) {
                    $(control).val(strDonutStyle);
                    var $uiImg = $('img[id="' + cntrlObj.fnGetId() + '"]');
                    if (strDonutStyle === "1") {
                        $uiImg.attr('src', MF_IDE_CONSTANTS.enterpriseImagesPath + "donut.png");
                    }
                    else {
                        $uiImg.attr('src', MF_IDE_CONSTANTS.enterpriseImagesPath + "piechart.png");
                    }
                }
            },
            InnerRadius: function (control, cntrlObj, args) {
                var strInnerRadius = cntrlObj.fnGetInnerRadius();
                var strDonutStyle = cntrlObj.fnGetDonutStyle();
                $(control).val(strInnerRadius);
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
                if (!mfUtil.isNullOrUndefined(strDonutStyle)
                && !mfUtil.isEmptyString(strDonutStyle)) {
                    if (strDonutStyle === "1") {
                        args.cntrlHtmlObjects.wrapperDiv.show();
                    }
                    else {
                        args.cntrlHtmlObjects.wrapperDiv.hide();
                    }
                }
            },
            Border: function (control, cntrlObj) {
                $(control).val(cntrlObj.border);
                if (!cntrlObj.border)
                    cntrlObj.border = "0";
                if (cntrlObj.border === "0")
                    $('#' + cntrlObj.id + '_OuterDiv').removeClass('data-inset');
                else
                    $('#' + cntrlObj.id + '_OuterDiv').addClass('data-inset');
            },
            eventHandlers: {
                Title: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.chartTitle = evnt.target.value;
                },
                DataLabelThreshold: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.dataLblThreshold = evnt.target.value;
                },
                ShowValueAs: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.valueAs = evnt.target.value;
                },
                Animation: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.animation = evnt.target.value;
                },
                SetBorder: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.border = evnt.target.value;
                    if (propObject.border === "0")
                        $('#' + propObject.id + '_OuterDiv').removeClass('data-inset');
                    else
                        $('#' + propObject.id + '_OuterDiv').addClass('data-inset');
                },
                DonutStyle: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.fnSetDonutStyle($self.val());
                    var objInnerRadiusCntrls = PROP_JSON_HTML_MAP
                        .PieChart
                        .propSheetCntrl
                        .Appearance.getInnerRadius();

                    var $uiImg = $('img[id="' + propObject.fnGetId() + '"]');

                    if ($self.val() === "1") {
                        objInnerRadiusCntrls.wrapperDiv.show();
                        objInnerRadiusCntrls.control.val("50");
                        propObject.fnSetInnerRadius("50");
                        $uiImg.attr('src', MF_IDE_CONSTANTS.enterpriseImagesPath + "donut.png");
                    }
                    else {
                        objInnerRadiusCntrls.wrapperDiv.hide();
                        objInnerRadiusCntrls.control.val("");
                        propObject.fnSetInnerRadius("0");
                        $uiImg.attr('src', MF_IDE_CONSTANTS.enterpriseImagesPath + "piechart.png");
                    }
                },
                InnerRadius: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var strValidationErrorMsg = "Inner radius should be between 10 and 90 ,both included.";
                    //value is numeric chekcec on key press
                    var iVal = 0;
                    if (!mfUtil.isNullOrUndefined($self.val())
                    && !mfUtil.isEmptyString($self.val())
                    ) {
                        iVal = parseInt($self.val());
                        if (!isNaN(iVal)) {
                            if (iVal < 10 || iVal > 90) {
                                showMessage(strValidationErrorMsg, DialogType.Error);
                                $self.val("50");
                                propObject.fnSetInnerRadius("50");
                                return;
                            }
                            else {
                                propObject.fnSetInnerRadius($self.val());
                            }
                        }
                        else {
                            showMessage(strValidationErrorMsg, DialogType.Error);
                            $self.val("50");
                            propObject.fnSetInnerRadius("50");
                            return;
                        }
                    }
                    else {
                        showMessage(strValidationErrorMsg, DialogType.Error);
                        $self.val("50");
                        propObject.fnSetInnerRadius("50");
                        return;
                    }

                }
            }
        },
        Data: {
            Databinding: function (control, appObj) {
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id + '">None</option>';
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id + '">Offline Database</option>';
                    control.html(strOptions);
                    appObj.fnSetBindType(MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj);
                    appObj.fnClearAllDatabindObjs();
                    appObj.fnAddDatabindObj(
                        new DatabindingObj(
                            "", [], "0", MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id,
                            "", "", "", "", "",
                            { ignoreCache: false }
                        )
                   );
                }
                else {
                }
                $(control).val(appObj.bindType.id);
            },
            DataObject: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.PieChart.propSheetCntrl.Data.getDataObjectCntrl();
                if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
                if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) {
                    $.each(appObj.databindObjs, function () {
                        if (this['name'].length > 0)
                            $(currentCntrl.control).val(this['name']);
                        else
                            $(currentCntrl.control).val('Select Object');
                        switch (this['bindObjType']) {
                            case "1":
                                mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "2":
                                mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "5":
                                mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control);
                                break;
                        }
                    });
                }
                else {
                    $(currentCntrl.control).val('Select Object');
                }
            },
            eventHandlers: {
                DataObject: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;

                    var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv);
                    var $DataObjectControl = $('#' + cntrls.DataObject.controlId);
                    var dataObject = new DatabindingObj();
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    if (evnt.target.value == "0") {
                        $ViewProperty.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        dataObject.bindObjType = bindType.id;
                    }
                    if (evnt.target.value == "1") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "2") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "5") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "6") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                        dataObject.bindObjType = bindType;
                    }
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            if (this.bindObjType != bindType.id) {
                                this.id = "";
                                this.name = "";
                                this.cmdParams = [];
                                propObject.displayText = "";
                                this.bindObjType = bindType.id;
                                $($DataObjectControl).val("Select Object");
                            }
                        });
                    }
                    else {
                        var dataObject = new DatabindingObj("", [], "0", bindType.id, "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        $($DataObjectControl).val("Select Object");
                    }

                    propObject.bindType = bindType;
                },
                DataBinding: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.PieChart.propSheetCntrl.Data.getDataObjectCntrl();
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                        intlJson = [],
                        isEdit = false;
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            var objDatabindObj = jQuery.extend(true, {}, this);
                            if (this['name'].length > 0)
                                $(currentCntrl.control).val(this['name']);
                            else
                                $(currentCntrl.control).val('Select Object');
                            switch (this['bindObjType']) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:
                                    //                                    intlJson = MF_HELPERS
                                    //                                            .intlsenseHelper
                                    //                                            .getControlDatabindingIntellisenseJson(
                                    //                                                objCurrentForm, propObject.id, true
                                    //                                            );
                                    if (objDatabindObj != null
                                    && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                    ) {
                                        isEdit = true;
                                    }

                                    mFicientIde
                                .MF_DATA_BINDING
                                .processDataBindingByObjectType({
                                    controlType: MF_IDE_CONSTANTS.CONTROLS.PIECHART,
                                    databindObject: objDatabindObj,
                                    objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                    isEdit: isEdit,
                                    intlsJson: intlJson,
                                    control: propObject
                                });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 420, false);
                                    break;
                            }
                        });
                    }
                    else {
                        $(currentCntrl.control).val('Select Object');
                    }
                }
            }
        },
        Behaviour: {
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay)
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.PieChart.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.PIECHART.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.PIECHART.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }
        function _getAppearanceInnerRadiusCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.PIECHART.propPluginPrefix,
                    "Appearance", "InnerRadius");
            }
            return objControl;
        }
        return {
            Data: {
                getDataObjectCntrl: _getDataObjectCntrl
            },
            Behaviour: {
                getConditionsCntrl: _getConditionsCntrl
            },
            Appearance: {
                getInnerRadius: _getAppearanceInnerRadiusCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.PieChart = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "PieChart",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.PieChart.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.PieChart.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Title",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Title",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.PieChart.groups.Appearance.eventHandlers.Title,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "DataLabel Threshold",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "DataLabelThreshold",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.PieChart.groups.Appearance.eventHandlers.DataLabelThreshold,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": true
                }
            },
            {
                "text": "Show Value As",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "ShowValueAs",
                "defltValue": "3",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.PieChart.groups.Appearance.eventHandlers.ShowValueAs,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": true
                },
                "options": [
                  {
                      "text": "Percentage",
                      "value": "Percentage"
                  },
                  {
                      "text": "Absolute",
                      "value": "Absolute"
                  }
               ]
            },
            {
                "text": "Animation",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Animation",
                "defltValue": "0",
                "validations": [

            ],
                "customValidations": [

            ],
                "events": [
                {
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.PieChart.groups.Appearance.eventHandlers.Animation,
                    "context": "",
                    "arguments": {

                    }
                }
            ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                ]
            },
            {
                "text": "Border",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Border",
                "defltValue": "0",
                "validations": [

            ],
                "customValidations": [

            ],
                "events": [
                {
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.PieChart.groups.Appearance.eventHandlers.SetBorder,
                    "context": "",
                    "arguments": {

                    }
                }
            ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                ]
            },
            {
                "text": "Donut Style",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "DonutStyle",
                "defltValue": "0",
                "validations": [
                ],
                "customValidations": [
                ],
                "events": [
                {
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.PieChart.groups.Appearance.eventHandlers.DonutStyle,
                    "context": "",
                    "arguments": {
                    }
                }
                ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                ]
            },
            {
                "text": "Inner Radius (%)",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "InnerRadius",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.PieChart.groups.Appearance.eventHandlers.InnerRadius,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 2,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": true
                }
            }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
            {
                "text": "Databinding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Databinding",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.PieChart.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                //                  {
                //                      "text": "None",
                //                      "value": "0"
                //                  },
                  {
                  "text": "Database Object",
                  "value": "1"
              },
                  {
                      "text": "Web Service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  },
                   {
                       "text": "Offline Database",
                       "value": "6"
                   }
               ]
            },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.PieChart.groups.Data.eventHandlers.DataBinding,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            }
         ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.PieChart.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.PieChart.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};


function PieChart(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Pie;
    this.description = opts.description;
    this.type = opts.type;
    this.bindType = opts.bindType;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.chartTitle = opts.chartTitle;
    this.title = opts.title;
    this.value = opts.value;
    this.animation = opts.animation;
    this.dataLblThreshold = opts.dataLblThreshold;
    this.valueAs = opts.valueAs;
    this.databindObjs = opts.databindObjs;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.donutStyle = opts.donutStyle;
    this.innerRadius = opts.innerRadius;
    this.border = opts.border;
}

PieChart.prototype = new ControlNew();
PieChart.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.PieChart;
PieChart.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
PieChart.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.dataLblThreshold = "3";
    this.animation = "0";
    this.valueAs = "Absolute";
    this.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
    this.conditionalDisplay = "1";
    this.isDeleted = false;
    this.donutStyle = "0";
    this.innerRadius = "0";
    this.border = "0";
    var dataObject = new DatabindingObj("", [], "0", this.bindType.id, "", "", "", "", "", { ignoreCache: false });
    this.fnAddDatabindObj(dataObject);
};
PieChart.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
}
PieChart.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
};
//shalini Reset Function
PieChart.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(
                     databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }
};
PieChart.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
};
PieChart.prototype.fnClearAllDatabindObjs = function () {
    this.databindObjs = [];
};

PieChart.prototype.fnAddBindOption = function (bindOption/*ManualBindOptions Object*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }

    if (bindOption && bindOption instanceof ManualBindOptions) {
        this.bindOptions.push(bindOption);
    }
}
PieChart.prototype.fnAddBindOptions = function (bindOptions/*ManualBindOptions Object[]*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }
    if ($.isArray(bindOptions)) {
        var self = this;
        $.each(bindOptions, function (index, value) {
            if (value && value instanceof ManualBindOptions) {
                self.databindObjs.push(value);
            }
        });
    }
}
PieChart.prototype.fnGetTitle = function () {
    return this.title;
};
PieChart.prototype.fnGetValue = function () {
    return this.value;
};
PieChart.prototype.fnGetId = function () {
    return this.id;
};
PieChart.prototype.fnSetId = function (value) {
    this.id = value;
};
PieChart.prototype.fnSetTitle = function (value) {
    this.title = value;
};
PieChart.prototype.fnSetValue = function (value) {
    this.value = value;
};
PieChart.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
PieChart.prototype.fnSetDonutStyle = function (value) {
    this.donutStyle = value;
};
PieChart.prototype.fnGetDonutStyle = function () {
    return this.donutStyle;
};
PieChart.prototype.fnSetInnerRadius = function (value) {
    this.innerRadius = value;
};
PieChart.prototype.fnGetInnerRadius = function () {
    return this.innerRadius;
};
PieChart.prototype.fnSetBindType = function (value) {
    this.bindType = value;
};
/*---------------------------------------------------BarGraph---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.BarGraph = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            Title: function (control, appObj) {
                $(control).val(appObj.chartTitle);
            },
            X_AxisTitle: function (control, appObj) {
                $(control).val(appObj.xAxisLabel);
            },
            Y_AxisTitle: function (control, appObj) {
                $(control).val(appObj.yAxisLabel);
            },
            BarType: function (control, appObj) {
                $(control).val(appObj.barGraphType);
            },
            BarDirection: function (control, appObj) {
                $(control).val(appObj.barGraphDir);
            },
            Animation: function (control, appObj) {
                $(control).val(appObj.animation)
            },
            Border: function (control, cntrlObj) {
                $(control).val(cntrlObj.border);
                if (!cntrlObj.border)
                    cntrlObj.border = "0";
                if (cntrlObj.border === "0")
                    $('#' + cntrlObj.id + '_OuterDiv').removeClass('data-inset');
                else
                    $('#' + cntrlObj.id + '_OuterDiv').addClass('data-inset');
            },
            eventHandlers: {
                Title: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.chartTitle = evnt.target.value;
                },
                X_AxisTitle: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.xAxisLabel = evnt.target.value;
                },
                Y_AxisTitle: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.yAxisLabel = evnt.target.value;
                },
                BarType: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.barGraphType = evnt.target.value;
                },
                BarDirection: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.barGraphDir = evnt.target.value;
                },
                Animation: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.animation = evnt.target.value;
                },
                SetBorder: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.border = evnt.target.value;
                    if (propObject.border === "0")
                        $('#' + propObject.id + '_OuterDiv').removeClass('data-inset');
                    else
                        $('#' + propObject.id + '_OuterDiv').addClass('data-inset');
                }
            }
        },
        Data: {
            Databinding: function (control, appObj) {
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id + '">None</option>';
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id + '">Offline Database</option>';
                    control.html(strOptions);
                    appObj.fnSetBindType(MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj);
                    appObj.fnClearAllDatabindObjs();
                    appObj.fnAddDatabindObj(
                        new DatabindingObj(
                            "", [], "0", MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id,
                            "", "", "", "", "",
                            { ignoreCache: false }
                        )
                   );
                }
                else {
                }
                $(control).val(appObj.bindType.id);
            },
            DataObject: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.BarGraph.propSheetCntrl.Data.getDataObjectCntrl();
                if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
                if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) {
                    $.each(appObj.databindObjs, function () {
                        if (this['name'].length > 0)
                            $(currentCntrl.control).val(this['name']);
                        else
                            $(currentCntrl.control).val('Select Object');
                        switch (this['bindObjType']) {
                            case "1":
                                mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "2":
                                mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "5":
                                mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control);
                                break;
                        }
                    });
                }
                else {
                    $(currentCntrl.control).val('Select Object');
                }
            },
            NoOfSeries: function (control, appObj) {
                $(control).val(appObj.noOfSeries)
            },
            NoOfDecimal: function (control, appObj) {
                $(control).val(appObj.noOfDecimalPoint)
            },
            eventHandlers: {
                DataObject: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;

                    var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv);
                    var $DataObjectControl = $('#' + cntrls.DataObject.controlId);
                    var dataObject = new DatabindingObj();
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    if (evnt.target.value == "0") {
                        $ViewProperty.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        dataObject.bindObjType = bindType.id;
                    }
                    if (evnt.target.value == "1") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "2") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "5") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "6") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                        dataObject.bindObjType = bindType.id;
                    }
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            if (this.bindObjType != bindType.id) {
                                this.id = "";
                                this.name = "";
                                this.cmdParams = [];
                                propObject.displayText = "";
                                this.bindObjType = bindType.id;
                                propObject.yAxisDataObjs = [];
                                $($DataObjectControl).val("Select Object");
                            }
                        });
                    }
                    else {
                        var dataObject = new DatabindingObj("", [], "0", bindType.id, "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        propObject.yAxisDataObjs = [];
                        $($DataObjectControl).val("Select Object");
                    }

                    propObject.bindType = bindType;
                },
                DataBinding: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.BarGraph.propSheetCntrl.Data.getDataObjectCntrl(),
                        objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                        intlJson = [],
                        isEdit = false; ;
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            var objDatabindObj = jQuery.extend(true, {}, this);
                            if (this['name'].length > 0)
                                $(currentCntrl.control).val(this['name']);
                            else
                                $(currentCntrl.control).val('Select Object');
                            switch (this['bindObjType']) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 520, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 520, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 520, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:
                                    //                                    intlJson = MF_HELPERS
                                    //                                                .intlsenseHelper
                                    //                                                .getControlDatabindingIntellisenseJson(
                                    //                                                    objCurrentForm, propObject.id, true
                                    //                                                );
                                    if (objDatabindObj != null
                                        && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                        ) {
                                        isEdit = true;
                                    }

                                    mFicientIde
                                    .MF_DATA_BINDING
                                    .processDataBindingByObjectType({
                                        controlType: MF_IDE_CONSTANTS.CONTROLS.BARGRAPH,
                                        databindObject: objDatabindObj,
                                        objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                        isEdit: isEdit,
                                        intlsJson: intlJson,
                                        control: propObject
                                    });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 550, false);
                                    break;
                            }
                        });
                    }
                    else {
                        $(currentCntrl.control).val('Select Object');
                    }
                },
                NoOfSeries: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.noOfSeries = evnt.target.value;
                },
                NoOfDecimal: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.noOfDecimalPoint = evnt.target.value;
                }
            }
        },
        Behaviour: {
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay);
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.BarGraph.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.BARGRAPH.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.BARGRAPH.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }
        return {
            Data: {
                getDataObjectCntrl: _getDataObjectCntrl
            },
            Behaviour: {
                getConditionsCntrl: _getConditionsCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.BarGraph = {
    "type": MF_IDE_CONSTANTS.CONTROLS.BARGRAPH,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "BarGraph",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.BarGraph.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.BarGraph.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Title",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Title",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.BarGraph.groups.Appearance.eventHandlers.Title,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "X-Axis Title",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "X_AxisTitle",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.BarGraph.groups.Appearance.eventHandlers.X_AxisTitle,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "Y-Axis Title",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Y_AxisTitle",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.BarGraph.groups.Appearance.eventHandlers.Y_AxisTitle,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "Bar Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "BarType",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.BarGraph.groups.Appearance.eventHandlers.BarType,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Clustered",
                      "value": "0"
                  },
                  {
                      "text": "Stacked",
                      "value": "1"
                  }
               ]
            },
            {
                "text": "Bar Direction",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "BarDirection",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.BarGraph.groups.Appearance.eventHandlers.BarDirection,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Vertical",
                      "value": "0"
                  },
                  {
                      "text": "Horizontal",
                      "value": "1"
                  }
               ]
            },
            {
                "text": "Animation",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Animation",
                "defltValue": "0",
                "validations": [

            ],
                "customValidations": [

            ],
                "events": [
                {
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.BarGraph.groups.Appearance.eventHandlers.Animation,
                    "context": "",
                    "arguments": {

                    }
                }
            ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                ]
            },
            {
                "text": "Border",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Border",
                "defltValue": "0",
                "validations": [

            ],
                "customValidations": [

            ],
                "events": [
                {
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.BarGraph.groups.Appearance.eventHandlers.SetBorder,
                    "context": "",
                    "arguments": {

                    }
                }
            ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                ]
            }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
            {
                "text": "No Of Series",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "NoOfSeries",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.BarGraph.groups.Data.eventHandlers.NoOfSeries,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "1",
                      "value": "1"
                  },
                  {
                      "text": "2",
                      "value": "2"
                  },
                  {
                      "text": "3",
                      "value": "3"
                  },
                  {
                      "text": "4",
                      "value": "4"
                  },
                  {
                      "text": "5",
                      "value": "5"
                  }
               ]
            },
            {
                "text": "Databinding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Databinding",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.BarGraph.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                //                  {
                //                      "text": "None",
                //                      "value": "0"
                //                  },
                  {
                  "text": "Database Object",
                  "value": "1"
              },
                  {
                      "text": "Web Service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  }
                  ,
                  {
                      "text": "Offline Database",
                      "value": "6"
                  }
               ]
            },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.BarGraph.groups.Data.eventHandlers.DataBinding,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            },
            {
                "text": "No Of Decimal",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "NoOfDecimal",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.BarGraph.groups.Data.eventHandlers.NoOfDecimal,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "0",
                      "value": "0"
                  },
                  {
                      "text": "1",
                      "value": "1"
                  },
                  {
                      "text": "2",
                      "value": "2"
                  },
                  {
                      "text": "3",
                      "value": "3"
                  },
                  {
                      "text": "4",
                      "value": "4"
                  },
                  {
                      "text": "5",
                      "value": "5"
                  }
               ]
            }
         ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.BarGraph.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.BarGraph.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};


function BarGraph(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Bargraph;
    this.description = opts.description;
    this.type = opts.type;
    this.bindType = opts.bindType;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.chartTitle = opts.chartTitle;
    this.xAxisLabel = opts.xAxisLabel;
    this.yAxisLabel = opts.yAxisLabel;
    this.xAxisData = opts.xAxisData;
    this.yAxisDataObjs = opts.yAxisDataObjs;
    this.barGraphType = opts.barGraphType;
    this.barGraphDir = opts.barGraphDir;
    this.animation = opts.animation;
    this.noOfSeries = opts.noOfSeries;
    this.noOfDecimalPoint = opts.noOfDecimalPoint;
    this.autoRefreshTime = opts.autoRefreshTime;
    this.databindObjs = opts.databindObjs;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.border = opts.border;
}

BarGraph.prototype = new ControlNew();
BarGraph.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.BarGraph;
BarGraph.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
BarGraph.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.barGraphType = "0";
    this.barGraphDir = "0";
    this.animation = "0";
    this.noOfSeries = "1";
    this.noOfDecimalPoint = "0";
    this.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
    this.conditionalDisplay = "1";
    this.border = "0";
    this.isDeleted = false;

    var dataObject = new DatabindingObj("", [], "0", this.bindType.id, "", "", "", "", "", { ignoreCache: false });
    this.fnAddDatabindObj(dataObject);

};
BarGraph.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
}
BarGraph.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
} //shalini Reset Function
BarGraph.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(
                     databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }

    var yAxisDataObjs = this.yAxisDataObjs;
    this.yAxisDataObjs = [];
    var i = 0;
    if (yAxisDataObjs && $.isArray(yAxisDataObjs) && yAxisDataObjs.length > 0) {
        for (i = 0; i <= yAxisDataObjs.length - 1; i++) {
            var objSeriesObj = new SeriesObj(yAxisDataObjs[i]);
            this.fnAddSeriesObj(objSeriesObj);
        }
    }
}
BarGraph.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
}
BarGraph.prototype.fnClearAllDatabindObjs = function () {
    this.databindObjs = [];
}

BarGraph.prototype.fnAddBindOption = function (bindOption/*ManualBindOptions Object*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }

    if (bindOption && bindOption instanceof ManualBindOptions) {
        this.bindOptions.push(bindOption);
    }
}
BarGraph.prototype.fnAddBindOptions = function (bindOptions/*ManualBindOptions Object[]*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }
    if ($.isArray(bindOptions)) {
        var self = this;
        $.each(bindOptions, function (index, value) {
            if (value && value instanceof ManualBindOptions) {
                self.databindObjs.push(value);
            }
        });
    }
}
BarGraph.prototype.fnAddSeriesObjs = function (seriesObjs/*SeriesObj object array*/) {
    if (!this.yAxisDataObjs || !$.isArray(this.yAxisDataObjs)) {
        this.yAxisDataObjs = [];
    }
    if ($.isArray(yAxisDataObjs)) {
        var self = this;
        $.each(yAxisDataObjs, function (index, value) {
            if (value && value instanceof SeriesObj) {
                self.yAxisDataObjs.push(value);
            }
        });
    }
}
BarGraph.prototype.fnAddSeriesObj = function (seriesObj/*SeriesObj object*/) {
    if (!this.yAxisDataObjs || !$.isArray(this.yAxisDataObjs)) {
        this.yAxisDataObjs = [];
    }
    if (seriesObj && seriesObj instanceof SeriesObj) {
        this.yAxisDataObjs.push(seriesObj);
    }
}
BarGraph.prototype.fnClearAllSeriesObjs = function () {
    this.yAxisDataObjs = [];
};
BarGraph.prototype.fnGetNoOfSeries = function () {
    return this.noOfSeries;
};
BarGraph.prototype.fnSetYAxisDataObjs = function (seriesObjs/*ary of SeriesObj*/) {
    this.yAxisDataObjs = seriesObjs;
};
BarGraph.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
BarGraph.prototype.fnGetXAxisData = function () {
    return this.xAxisData;
};
BarGraph.prototype.fnGetYAxisDataObjs = function () {
    return this.yAxisDataObjs;
};
BarGraph.prototype.fnSetXAxisData = function (value) {
    this.xAxisData = value;
};
BarGraph.prototype.fnSetBindType = function (value) {
    this.bindType = value;
};
function SeriesObj(opts) {
    if (!opts) opts = {};
    this.index = opts.index;
    this.label = opts.label;
    this.value = opts.value;
    this.scaleFactor = opts.scaleFactor;
}

SeriesObj.prototype = new SeriesObj();
/*---------------------------------------------------LineGraph---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.LineGraph = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            Title: function (control, appObj) {
                $(control).val(appObj.chartTitle);
            },
            AutoRefreshTime: function (control, appObj) {
                $(control).val(appObj.autoRefreshTime);
            },
            X_AxisTitle: function (control, appObj) {
                $(control).val(appObj.xAxisLabel);
            },
            Y_AxisTitle: function (control, appObj) {
                $(control).val(appObj.yAxisLabel);
            },
            Animation: function (control, appObj) {
                $(control).val(appObj.animation)
            },
            Border: function (control, cntrlObj) {
                $(control).val(cntrlObj.border);
                if (!cntrlObj.border)
                    cntrlObj.border = "0";
                if (cntrlObj.border === "0")
                    $('#' + cntrlObj.id + '_OuterDiv').removeClass('data-inset');
                else
                    $('#' + cntrlObj.id + '_OuterDiv').addClass('data-inset');
            },
            eventHandlers: {
                Title: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.chartTitle = evnt.target.value;
                },
                AutoRefreshTime: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.autoRefreshTime = evnt.target.value;
                },
                X_AxisTitle: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.xAxisLabel = evnt.target.value;
                },
                Y_AxisTitle: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.yAxisLabel = evnt.target.value;
                },
                Animation: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.animation = evnt.target.value;
                },		
                SetBorder: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.border = evnt.target.value;
                    if (propObject.border === "0")
                        $('#' + propObject.id + '_OuterDiv').removeClass('data-inset');
                    else
                        $('#' + propObject.id + '_OuterDiv').addClass('data-inset');
                }
            }
        },
        Data: {
            Databinding: function (control, appObj) {
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id + '">None</option>';
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id + '">Offline Database</option>';
                    control.html(strOptions);
                    appObj.fnSetBindType(MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj);
                    appObj.fnClearAllDatabindObjs();
                    appObj.fnAddDatabindObj(
                        new DatabindingObj(
                            "", [], "0", MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id,
                            "", "", "", "", "",
                            { ignoreCache: false }
                        )
                   );
                }
                else {
                }
                $(control).val(appObj.bindType.id);
            },
            DataObject: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.LineGraph.propSheetCntrl.Data.getDataObjectCntrl();
                if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
                if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) {
                    $.each(appObj.databindObjs, function () {
                        if (this['name'].length > 0)
                            $(currentCntrl.control).val(this['name']);
                        else
                            $(currentCntrl.control).val('Select Object');
                        switch (this['bindObjType']) {
                            case "1":
                                mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control, currentCntrl.control);
                                break;
                            case "2":
                                mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control, currentCntrl.control);
                                break;
                            case "5":
                                mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control, currentCntrl.control);
                                break;
                        }
                    });
                }
                else {
                    $(currentCntrl.control).val('Select Object');
                }
            },
            NoOfSeries: function (control, appObj) {
                $(control).val(appObj.noOfSeries)
            },
            NoOfDecimal: function (control, appObj) {
                $(control).val(appObj.noOfDecimalPoint)
            },
            eventHandlers: {
                DataObject: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;

                    var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv);
                    var $DataObjectControl = $('#' + cntrls.DataObject.controlId);
                    var dataObject = new DatabindingObj();
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    if (evnt.target.value == "0") {
                        $ViewProperty.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        dataObject.bindObjType = bindType.id;
                    }
                    if (evnt.target.value == "1") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "2") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "5") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "6") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                        dataObject.bindObjType = bindType.id;
                    }
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            if (this.bindObjType != bindType.id) {
                                this.id = "";
                                this.name = "";
                                this.cmdParams = [];
                                propObject.displayText = "";
                                this.bindObjType = bindType.id;
                                propObject.yAxisDataObjs = [];
                                $($DataObjectControl).val("Select Object");
                            }
                        });
                    }
                    else {
                        var dataObject = new DatabindingObj("", [], "0", bindType.id, "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        propObject.yAxisDataObjs = [];
                        $($DataObjectControl).val("Select Object");
                    }

                    propObject.bindType = bindType;
                },
                DataBinding: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.LineGraph.propSheetCntrl.Data.getDataObjectCntrl(),
                        objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                        intlJson = [],
                        isEdit = false; ;
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            var objDatabindObj = jQuery.extend(true, {}, this);
                            if (this['name'].length > 0)
                                $(currentCntrl.control).val(this['name']);
                            else
                                $(currentCntrl.control).val('Select Object');
                            switch (this['bindObjType']) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 520, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 520, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 520, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:
                                    //                                    intlJson = MF_HELPERS
                                    //                                                .intlsenseHelper
                                    //                                                .getControlDatabindingIntellisenseJson(
                                    //                                                    objCurrentForm, propObject.id, true
                                    //                                                );
                                    if (objDatabindObj != null
                                        && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                        ) {
                                        isEdit = true;
                                    }

                                    mFicientIde
                                    .MF_DATA_BINDING
                                    .processDataBindingByObjectType({
                                        controlType: MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH,
                                        databindObject: objDatabindObj,
                                        objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                        isEdit: isEdit,
                                        intlsJson: intlJson,
                                        control: propObject
                                    });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 550, false);
                                    break;
                            }
                        });
                    }
                    else {
                        $(currentCntrl.control).val('Select Object');
                    }
                },
                NoOfSeries: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.noOfSeries = evnt.target.value;
                },
                NoOfDecimal: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.noOfDecimalPoint = evnt.target.value;
                }
            }
        },
        Behaviour: {
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay)
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.LineGraph.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }
        return {
            Data: {
                getDataObjectCntrl: _getDataObjectCntrl
            },
            Behaviour: {
                getConditionsCntrl: _getConditionsCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.LineGraph = {
    "type": MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "BarGraph",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.LineGraph.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.LineGraph.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Title",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Title",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.LineGraph.groups.Appearance.eventHandlers.Title,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "Auto Refresh Time(min)",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "AutoRefreshTime",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.LineGraph.groups.Appearance.eventHandlers.AutoRefreshTime,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "X-Axis Title",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "X_AxisTitle",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.LineGraph.groups.Appearance.eventHandlers.X_AxisTitle,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "Y-Axis Title",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Y_AxisTitle",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.LineGraph.groups.Appearance.eventHandlers.Y_AxisTitle,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "Animation",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Animation",
                "defltValue": "0",
                "validations": [

            ],
                "customValidations": [

            ],
                "events": [
                {
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.LineGraph.groups.Appearance.eventHandlers.Animation,
                    "context": "",
                    "arguments": {

                    }
                }
            ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                ]
            },
            {
                "text": "Border",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Border",
                "defltValue": "0",
                "validations": [

            ],
                "customValidations": [

            ],
                "events": [
                {
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.LineGraph.groups.Appearance.eventHandlers.SetBorder,
                    "context": "",
                    "arguments": {

                    }
                }
            ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                ]
            }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
            {
                "text": "No Of Series",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "NoOfSeries",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.LineGraph.groups.Data.eventHandlers.NoOfSeries,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "1",
                      "value": "1"
                  },
                  {
                      "text": "2",
                      "value": "2"
                  },
                  {
                      "text": "3",
                      "value": "3"
                  },
                  {
                      "text": "4",
                      "value": "4"
                  },
                  {
                      "text": "5",
                      "value": "5"
                  }
               ]
            },
            {
                "text": "Databinding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Databinding",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.LineGraph.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                //                  {
                //                      "text": "None",
                //                      "value": "0"
                //                  },
                  {
                  "text": "Database Object",
                  "value": "1"
              },
                  {
                      "text": "Web Service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  },
                   {
                       "text": "Offline Database",
                       "value": "6"
                   }
               ]
            },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.LineGraph.groups.Data.eventHandlers.DataBinding,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            },
            {
                "text": "No Of Decimal",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "NoOfDecimal",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.LineGraph.groups.Data.eventHandlers.NoOfDecimal,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "0",
                      "value": "0"
                  },
                  {
                      "text": "1",
                      "value": "1"
                  },
                  {
                      "text": "2",
                      "value": "2"
                  },
                  {
                      "text": "3",
                      "value": "3"
                  },
                  {
                      "text": "4",
                      "value": "4"
                  },
                  {
                      "text": "5",
                      "value": "5"
                  }
               ]
            }
         ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.LineGraph.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.LineGraph.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};


function LineGraph(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Line;
    this.description = opts.description;
    this.type = opts.type;
    this.bindType = opts.bindType;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.chartTitle = opts.chartTitle;
    this.xAxisLabel = opts.xAxisLabel;
    this.yAxisLabel = opts.yAxisLabel;
    this.xAxisData = opts.xAxisData;
    this.animation = opts.animation;
    this.yAxisDataObjs = opts.yAxisDataObjs;
    this.noOfSeries = opts.noOfSeries;
    this.noOfDecimalPoint = opts.noOfDecimalPoint;
    this.autoRefreshTime = opts.autoRefreshTime;
    this.databindObjs = opts.databindObjs;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.border = opts.border;
}

LineGraph.prototype = new ControlNew();
LineGraph.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.LineGraph;
LineGraph.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
LineGraph.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
}
LineGraph.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.animation = "0";
    this.noOfSeries = "1";
    this.noOfDecimalPoint = "0";
    this.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
    this.conditionalDisplay = "1";
    this.border = "0";
    this.isDeleted = false;

    var dataObject = new DatabindingObj("", [], "0", this.bindType.id, "", "", "", "", "", { ignoreCache: false });
    this.fnAddDatabindObj(dataObject);
};
LineGraph.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
}
//shalini Reset Function
LineGraph.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(
                     databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }

    var yAxisDataObjs = this.yAxisDataObjs;
    this.yAxisDataObjs = [];
    var i = 0;
    if (yAxisDataObjs && $.isArray(yAxisDataObjs) && yAxisDataObjs.length > 0) {
        for (i = 0; i <= yAxisDataObjs.length - 1; i++) {
            var objSeriesObj = new SeriesObj(yAxisDataObjs[i]);
            this.fnAddSeriesObj(objSeriesObj);
        }
    }
}
LineGraph.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
}
LineGraph.prototype.fnClearAllDatabindObjs = function () {
    this.databindObjs = [];
}

LineGraph.prototype.fnAddBindOption = function (bindOption/*ManualBindOptions Object*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }

    if (bindOption && bindOption instanceof ManualBindOptions) {
        this.bindOptions.push(bindOption);
    }
}
LineGraph.prototype.fnAddBindOptions = function (bindOptions/*ManualBindOptions Object[]*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }
    if ($.isArray(bindOptions)) {
        var self = this;
        $.each(bindOptions, function (index, value) {
            if (value && value instanceof ManualBindOptions) {
                self.databindObjs.push(value);
            }
        });
    }
}
LineGraph.prototype.fnAddSeriesObjs = function (seriesObjs/*SeriesObj object array*/) {
    if (!this.yAxisDataObjs || !$.isArray(this.yAxisDataObjs)) {
        this.yAxisDataObjs = [];
    }
    if ($.isArray(yAxisDataObjs)) {
        var self = this;
        $.each(yAxisDataObjs, function (index, value) {
            if (value && value instanceof SeriesObj) {
                self.yAxisDataObjs.push(value);
            }
        });
    }
}
LineGraph.prototype.fnAddSeriesObj = function (seriesObj/*SeriesObj object*/) {
    if (!this.yAxisDataObjs || !$.isArray(this.yAxisDataObjs)) {
        this.yAxisDataObjs = [];
    }
    if (seriesObj && seriesObj instanceof SeriesObj) {
        this.yAxisDataObjs.push(seriesObj);
    }
}
LineGraph.prototype.fnClearAllSeriesObjs = function () {
    this.yAxisDataObjs = [];
}
LineGraph.prototype.fnGetNoOfSeries = function () {
    return this.noOfSeries;
};
LineGraph.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
LineGraph.prototype.fnGetXAxisData = function () {
    return this.xAxisData;
};
LineGraph.prototype.fnGetYAxisDataObjs = function () {
    return this.yAxisDataObjs;
};
LineGraph.prototype.fnSetXAxisData = function (value) {
    this.xAxisData = value;
};
LineGraph.prototype.fnSetBindType = function (value) {
    this.bindType = value;
};


/*---------------------------------------------------AngularGuage---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.AngularGuage = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            Title: function (control, appObj) {
                $(control).val(appObj.chartTitle);
            },
            NoOfAxis: function (control, appObj) {
                $(control).val(appObj.noOfAxis);
            },
            AxisData: function (control, appObj) {
                _editAngularGuageAxis(appObj);
            },
            NeedleColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.AngularGuage.propSheetCntrl.Appearance.getNeedleColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                if (appObj.needleColor != undefined || appObj.needleColor != null) {
                    $(currentCntrl.control).val('#' + appObj.needleColor);
                    $(currentCntrl.browseBtn).css('background-color', '#' + appObj.needleColor);
                }

                var needleColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        $(currentCntrl.browseBtn).css('background-color', '#' + hex);
                        appObj.needleColor = '#' + hex;
                        $(currentCntrl.control).val('#' + hex);
                        if (!bySetColor) $(currentCntrl.control).val('#' + hex);
                    }
                });
                $(currentCntrl.control).keyup(function () {
                    $(needleColorPicker).colpickSetColor(this.value);
                });
                $(currentCntrl.control).bind("change", function () {
                    $(needleColorPicker).colpickSetColor(this.value);
                });
            },
            NeedleRadius: function (control, appObj) {
                $(control).val(appObj.needleRadius);
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
            },
            Border: function (control, cntrlObj) {
                $(control).val(cntrlObj.border);
                if (!cntrlObj.border)
                    cntrlObj.border = "0";
                if (cntrlObj.border === "0")
                    $('#' + cntrlObj.id + '_OuterDiv').removeClass('data-inset');
                else
                    $('#' + cntrlObj.id + '_OuterDiv').addClass('data-inset');
            },
            eventHandlers: {
                Title: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.chartTitle = evnt.target.value;
                },
                NoOfAxis: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.noOfAxis = evnt.target.value;
                    if (propObject.axisObjs.length > propObject.noOfAxis) {
                        propObject.fnDeleteAxisObj("2");
                    }
                },
                AxisData: function (evnt) {
                    var propObject = evnt.data.propObject;
                    _editAngularGuageAxis(propObject);
                    SubProcAngularGuageData(true);
                    $('[id$=btnSaveAngularGuageData]').unbind('click');
                    $('[id$=btnSaveAngularGuageData]').bind('click', function () {
                        _saveAngularGuageAxisData(propObject);
                        return false;
                    });
                    $('[id$=btnCancelAngularGuageData]').unbind('click');
                    $('[id$=btnCancelAngularGuageData]').bind('click', function () {
                        SubProcAngularGuageData(false);
                        return false;
                    });

                },
                NeedleColor: function (evnt) {
                    var propObject = evnt.data.propObject;
                    //propObject.needleColor = evnt.target.value;
                },
                NeedleRadius: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.needleRadius = evnt.target.value;
                },
                SetBorder: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.border = evnt.target.value;
                    if (propObject.border === "0")
                        $('#' + propObject.id + '_OuterDiv').removeClass('data-inset');
                    else
                        $('#' + propObject.id + '_OuterDiv').addClass('data-inset');
                }
            }
        },
        Data: {
            Databinding: function (control, appObj) {
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id + '">None</option>';
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id + '">Offline Database</option>';
                    control.html(strOptions);
                    appObj.fnSetBindType(MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj);
                    appObj.fnClearAllDatabindObjs();
                    appObj.fnAddDatabindObj(
                        new DatabindingObj(
                            "", [], "0", MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id,
                            "", "", "", "", "",
                            { ignoreCache: false }
                        )
                   );
                }
                else {
                }
                $(control).val(appObj.bindType.id);
            },
            DataObject: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.AngularGuage.propSheetCntrl.Data.getDataObjectCntrl();
                if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
                if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) {
                    $.each(appObj.databindObjs, function () {
                        if (this['name'].length > 0)
                            $(currentCntrl.control).val(this['name']);
                        else
                            $(currentCntrl.control).val('Select Object');
                        switch (this['bindObjType']) {
                            case "1":
                                mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "2":
                                mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "5":
                                mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control);
                                break;
                        }
                    });
                }
                else {
                    $(currentCntrl.control).val('Select Object');
                }
            },
            eventHandlers: {
                DataObject: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;

                    var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv);
                    var $DataObjectControl = $('#' + cntrls.DataObject.controlId);
                    var dataObject = new DatabindingObj();
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    if (evnt.target.value == "0") {
                        $ViewProperty.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        dataObject.bindObjType = bindType.id;
                    }
                    if (evnt.target.value == "1") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "2") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "5") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "6") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                        dataObject.bindObjType = bindType.id;
                    }
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            if (this.bindObjType != bindType.id) {
                                this.id = "";
                                this.name = "";
                                this.cmdParams = [];
                                propObject.displayText = "";
                                this.bindObjType = bindType.id;
                                $($DataObjectControl).val("Select Object");
                            }
                        });
                    }
                    else {
                        var dataObject = new DatabindingObj("", [], "0", bindType.id, "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        $($DataObjectControl).val("Select Object");
                    }

                    propObject.bindType = bindType;
                },
                DataBinding: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.AngularGuage.propSheetCntrl.Data.getDataObjectCntrl();
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                        intlJson = [],
                        isEdit = false;
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            var objDatabindObj = jQuery.extend(true, {}, this);
                            if (this['name'].length > 0)
                                $(currentCntrl.control).val(this['name']);
                            else
                                $(currentCntrl.control).val('Select Object');
                            switch (this['bindObjType']) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:
                                    //                                    intlJson = MF_HELPERS
                                    //                                                    .intlsenseHelper
                                    //                                                    .getControlDatabindingIntellisenseJson(
                                    //                                                        objCurrentForm, propObject.id, true
                                    //                                                    );
                                    if (objDatabindObj != null
                                            && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                            ) {
                                        isEdit = true;
                                    }

                                    mFicientIde
                                        .MF_DATA_BINDING
                                        .processDataBindingByObjectType({
                                            controlType: MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE,
                                            databindObject: objDatabindObj,
                                            objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                            isEdit: isEdit,
                                            intlsJson: intlJson,
                                            control: propObject
                                        });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 420, false);
                                    break;
                            }
                        });
                    }
                    else {
                        $(currentCntrl.control).val('Select Object');
                    }
                }
            }
        },
        Behaviour: {
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay);
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.AngularGuage.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getNeedleColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE.propPluginPrefix,
                    "Appearance", "NeedleColor");
            }
            return objControl;
        }
        function _getNeedleRadiusCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE.propPluginPrefix,
                    "Appearance", "NeedleRadius");
            }
            return objControl;
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }
        return {
            Appearance: {
                getNeedleColorCntrl: _getNeedleColorCntrl,
                getNeedleRadiusCntrl: _getNeedleRadiusCntrl
            },
            Data: {
                getDataObjectCntrl: _getDataObjectCntrl
            },
            Behaviour: {
                getConditionsCntrl: _getConditionsCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.AngularGuage = {
    "type": MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Angular Guage",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.AngularGuage.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.AngularGuage.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Title",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Title",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.AngularGuage.groups.Appearance.eventHandlers.Title,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "No Of Scale",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "NoOfAxis",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.AngularGuage.groups.Appearance.eventHandlers.NoOfAxis,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": true
                },
                "options": [
                  {
                      "text": "1",
                      "value": "1"
                  },
                  {
                      "text": "2",
                      "value": "2"
                  }
               ]
            },
            {
                "text": "Scale Configuration",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "AxisData",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.AngularGuage.groups.Appearance.eventHandlers.AxisData,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Data",
                    "hidden": false
                }
            },
              {
                  "text": "Needle Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "NeedleColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.AngularGuage.groups.Appearance.eventHandlers.NeedleColor,
                      "context": "",
                      "arguments": {}
                  }
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
            {
                "text": "Needle Radius (%)",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "NeedleRadius",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.AngularGuage.groups.Appearance.eventHandlers.NeedleRadius,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Border",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Border",
                "defltValue": "0",
                "validations": [

            ],
                "customValidations": [

            ],
                "events": [
                {
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.AngularGuage.groups.Appearance.eventHandlers.SetBorder,
                    "context": "",
                    "arguments": {

                    }
                }
            ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                ]
            }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
            {
                "text": "Databinding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Databinding",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.AngularGuage.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                //                  {
                //                      "text": "None",
                //                      "value": "0"
                //                  },
                  {
                  "text": "Database Object",
                  "value": "1"
              },
                  {
                      "text": "Web Service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  },
                  {
                      "text": "Offline Database",
                      "value": "6"
                  }
               ]
            },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.AngularGuage.groups.Data.eventHandlers.DataBinding,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            }
         ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.AngularGuage.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.AngularGuage.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};

function AngularGuage(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.AngularGuage;
    this.description = opts.description;
    this.type = opts.type;
    this.bindType = opts.bindType;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.chartTitle = opts.chartTitle;
    this.noOfAxis = opts.noOfAxis;
    this.axisObjs = opts.axisObjs;
    this.value = opts.value;
    this.maxVal = opts.maxVal;
    this.needleColor = opts.needleColor;
    this.needleRadius = opts.needleRadius;
    this.databindObjs = opts.databindObjs;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.border = opts.border;
};

AngularGuage.prototype = new ControlNew();
AngularGuage.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.AngularGuage;
AngularGuage.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
AngularGuage.prototype.fnGetValue = function () {
    return this.value;
}
AngularGuage.prototype.fnGetMaxValue = function () {
    return this.maxVal;
}
AngularGuage.prototype.fnSetValue = function (val) {
    this.value = val;
}
AngularGuage.prototype.fnSetMaxValue = function (val) {
    this.maxVal = val;
}
AngularGuage.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
}
AngularGuage.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.noOfAxis = "1";
    this.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
    this.conditionalDisplay = "1";
    this.isDeleted = false;
    this.needleColor = "#000000";
    this.needleRadius = "100";
    this.border = "0";

    var angularAxis1 = new AxisObject();
    angularAxis1.index = "1";
    angularAxis1.tickInterval = "10";
    angularAxis1.startVal = "0";
    angularAxis1.endVal = "200";
    angularAxis1.bands = [];

    var angularAxis2 = new AxisObject();
    angularAxis2.index = "2";
    angularAxis2.tickInterval = "10";
    angularAxis2.startVal = "0";
    angularAxis2.endVal = "200";
    angularAxis2.bands = [];

    var objAxisBand = new AxisBandObj();
    objAxisBand.index = 1;
    objAxisBand.startVal = "10";
    objAxisBand.color = "#3c32c7";
    objAxisBand.innerRadius = "95";

    angularAxis1.fnAddAxisBandObj(objAxisBand);
    angularAxis2.fnAddAxisBandObj(objAxisBand);
    this.fnAddAxisObj(angularAxis1);
    this.fnAddAxisObj(angularAxis2);
    _addNewBand(angularAxis1, objAxisBand, this);
    _addNewBand(angularAxis2, objAxisBand, this);

    var dataObject = new DatabindingObj("", [], "0", this.bindType.id, "", "", "", "", "", { ignoreCache: false });
    this.fnAddDatabindObj(dataObject);

}
AngularGuage.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
}
AngularGuage.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(
                     databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }

    var axisObjs = this.axisObjs;
    this.axisObjs = [];
    var i = 0;
    if (axisObjs && $.isArray(axisObjs) && axisObjs.length > 0) {
        for (i = 0; i <= axisObjs.length - 1; i++) {
            var axisObj = new AxisObject(axisObjs[i]);
            axisObj.fnResetObjectDetails();
            this.fnAddAxisObj(axisObj);
        }
    }
}
AngularGuage.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
}
AngularGuage.prototype.fnClearAllDatabindObjs = function () {
    this.databindObjs = [];
}
AngularGuage.prototype.fnGetValue = function () {
    return this.value;
}
AngularGuage.prototype.fnGetMaxValue = function () {
    return this.maxVal;
}
AngularGuage.prototype.fnAddAxisObjs = function (axisObjs/*AxisObject object array*/) {
    if (!this.axisObjs || !$.isArray(this.axisObjs)) {
        this.axisObjs = [];
    }
    if ($.isArray(axisObjs)) {
        var self = this;
        $.each(axisObjs, function (index, value) {
            if (value && value instanceof AxisObject) {
                self.axisObjs.push(value);
            }
        });
    }
}
AngularGuage.prototype.fnAddAxisObj = function (axisObj/*AxisObject object*/) {
    if (!this.axisObjs || !$.isArray(this.axisObjs)) {
        this.axisObjs = [];
    }
    if (axisObj && axisObj instanceof AxisObject) {
        this.axisObjs.push(axisObj);
    }
}
AngularGuage.prototype.fnDeleteAxisObj = function (index) {
    var aryNewAxisObjs = $.grep(this.axisObjs, function (axisObj) {
        if (axisObj instanceof AxisObject) {
            return axisObj.index !== index;
        }
    });
    this.axisObjs = aryNewAxisObjs;
}
AngularGuage.prototype.fnClearAllAxisObjs = function () {
    this.axisObjs = [];
}
AngularGuage.prototype.fnGetValue = function () {
    return this.value;
};
AngularGuage.prototype.fnSetValue = function (value) {
    this.value = value;
};
AngularGuage.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
AngularGuage.prototype.fnSetBindType = function (value) {
    this.bindType = value;
};
function AxisObject(opts) {
    if (!opts) opts = {};
    this.index = opts.index;
    this.tickInterval = opts.tickInterval;
    this.startVal = opts.startVal;
    //this.endVal = opts.endVal;
    this.bands = opts.bands;
};

AxisObject.prototype = new AxisObject();
AxisObject.prototype.fnAddAxisBandObjs = function (bands/*AxisBandObj object array*/) {
    if (!this.bands || !$.isArray(this.bands)) {
        this.bands = [];
    }
    if ($.isArray(bands)) {
        var self = this;
        $.each(bands, function (index, value) {
            if (value && value instanceof AxisBandObj) {
                self.bands.push(value);
            }
        });
    }
}
AxisObject.prototype.fnAddAxisBandObj = function (axisBandObj/*AxisBandObj object*/) {
    if (!this.bands || !$.isArray(this.bands)) {
        this.bands = [];
    }
    if (axisBandObj && axisBandObj instanceof AxisBandObj) {
        this.bands.push(axisBandObj);
    }
}
AxisObject.prototype.fnDeleteAxisBandObj = function (index) {
    var aryNewBands = $.grep(this.bands, function (axisBandObj) {
        if (axisBandObj instanceof AxisBandObj) {
            return axisBandObj.index !== index;
        }
    });
    this.bands = aryNewBands;
}
AxisObject.prototype.fnClearAllAxisBandObjs = function () {
    this.bands = [];
}

AxisObject.prototype.fnResetObjectDetails = function () {
    var bands = this.bands;
    this.bands = [];
    var i = 0;
    if (bands && $.isArray(bands) && bands.length > 0) {
        for (i = 0; i <= bands.length - 1; i++) {
            var axisBandObj = new AxisBandObj(bands[i]);
            this.fnAddAxisBandObj(axisBandObj);
        }
    }
}

function AxisBandObj(opts) {
    if (!opts) opts = {};
    this.index = opts.index;
    this.startVal = opts.startVal;
    this.color = opts.color;
    this.innerRadius = opts.innerRadius;
}

AxisBandObj.prototype = new AxisBandObj();

var maxNoOfBands = 3;

function SubProcAngularGuageData(_bol) {
    if (_bol)
        showModalPopUpWithOutHeader('SubProcAngularGuageData', 'Scale Configuration', 440, false);
    else
        $('#SubProcAngularGuageData').dialog('close');
}

function _editAngularGuageAxis(cntrl) {
    $('#Angular_Axis1').hide();
    $('#Angular_Axis1_Band').hide();
    $('#Angular_Axis1_BandsHeader').hide();
    $('#Angular_Axis1_Band_div').hide();
    $('#Angular_Axis2').hide();
    $('#Angular_Axis2_Band').hide();
    $('#Angular_Axis2_BandsHeader').hide();
    $('#Angular_Axis2_Band_div').hide();


    $('#txt_Angular_Axis1_TickInterval').keydown(function (event) {
        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
    });

    $('#txt_Angular_Axis2_TickInterval').keypress(function (event) {
        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
    });

    $('#txt_Angular_Axis1_StartVal').keypress(function (event) {
        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
    });

    $('#txt_Angular_Axis2_StartVal').keypress(function (event) {
        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
    });

    $('#txt_Angular_Axis1_EndVal').keypress(function (event) {
        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
    });

    $('#txt_Angular_Axis2_EndVal').keypress(function (event) {
        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
    });

    $('#txt_Angular_Axis1_TickInterval').blur(function (event) {
        if (event.target.value.length <= 0) {
            event.target.value = 10;
        }
    });

    $('#txt_Angular_Axis2_TickInterval').blur(function (event) {
        if (event.target.value.length <= 0) {
            event.target.value = 10;
        }
    });

    $('#txt_Angular_Axis1_StartVal').blur(function (event) {
        if (event.target.value.length <= 0) {
            event.target.value = 0;
        }
    });

    $('#txt_Angular_Axis2_StartVal').blur(function (event) {
        if (event.target.value.length <= 0) {
            event.target.value = 0;
        }
    });

    //    $('#txt_Angular_Axis1_EndVal').blur(function (event) {
    //        if (event.target.value.length <= 0) {
    //            event.target.value = 200;
    //        }
    //    });

    //    $('#txt_Angular_Axis2_EndVal').blur(function (event) {
    //        if (event.target.value.length <= 0) {
    //            event.target.value = 200;
    //        }
    //    });

    if (cntrl.axisObjs != undefined && cntrl.axisObjs.length > 0) {
        for (var i = 1; i <= parseInt(cntrl.noOfAxis, 10); i++) {
            $('#Angular_Axis' + i + '_Band_div').html('');
            if (cntrl.axisObjs[i - 1] != undefined || cntrl.axisObjs[i - 1] != null) {
                $('#txt_Angular_Axis' + i + '_TickInterval').val(cntrl.axisObjs[i - 1].tickInterval);
                $('#txt_Angular_Axis' + i + '_StartVal').val(cntrl.axisObjs[i - 1].startVal);
                //                $('#txt_Angular_Axis' + i + '_EndVal').val(cntrl.axisObjs[i - 1].endVal);

                $.each(cntrl.axisObjs[i - 1].bands, function () {
                    _addNewBand(cntrl.axisObjs[i - 1], this, cntrl);
                });
            }
            else {
                var angularAxis = new AxisObject();
                angularAxis.index = i;
                angularAxis.tickInterval = 10;
                angularAxis.startVal = 0;
                //angularAxis.endVal = 200;

                var objAxisBand = new AxisBandObj();
                objAxisBand.index = 1;
                objAxisBand.startVal = 10;
                objAxisBand.innerRadius = 95;
                objAxisBand.color = '#3c32c7';

                _addNewBand(angularAxis, objAxisBand, cntrl);
            }
            $('#Angular_Axis' + i).show();
            $('#Angular_Axis' + i + '_Band').show();
            $('#Angular_Axis' + i + '_BandsHeader').show();
            $('#Angular_Axis' + i + '_Band_div').show();
        }
    }
}

function _addNewBand(axis, band, cntrl) {
    $('#Angular_Axis' + axis.index + '_Band_div').append('<tr id="Angular_Axis' + axis.index + '_Band_' + band.index + '">'
                                 + '<td id="Angular_Axis' + axis.index + '_Band_' + band.index + '_StartVal">'
                                 + '<input id="txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_StartVal" type="text" style="width:70px;" value="' + band.startVal + '" onkeydown=\"return enterKeyFilter(event);\"/>'
                                 + '</td>'
                                 + '<td id="Angular_Axis' + axis.index + '_Band_' + band.index + '_InnerRad">'
                                 + '<input id="txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_InnerRad" type="text" value="' + band.innerRadius + '" style="width:80px;" onkeydown=\"return enterKeyFilter(event);\"/>'
                                 + '</td>'
                                 + '<td id="Angular_Axis' + axis.index + '_Band_' + band.index + '_Color" style="width:200px;" >'
                                 + '<input id="txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_Color" type="text" value="' + band.color + '" onkeydown=\"return enterKeyFilter(event);\"/>'
                                 + '<input id="btn_Angular_Axis' + axis.index + '_Band_' + band.index + '_Color" type="button"/>'
                                 + '</td>'
                                 + '<td id="Angular_Axis' + axis.index + '_Band_' + band.index + '_Add">'
                                 + '<img title="Delete Option" id="img_Del_Angular_Axis' + axis.index + '_Band_' + band.index + '" style="padding-left: 10px;'
                                 + 'cursor: pointer;" alt="delete" src="//enterprise.mficient.com/images/cross.png"/></td>'
                                 + '<td id="Angular_Axis' + axis.index + '_Band_' + band.index + 'Del">'
                                 + '<img title="Delete Option" id="img_Add_Angular_Axis' + axis.index + '_Band_' + band.index + '" style="padding-left: 10px;'
                                 + 'cursor: pointer;" alt="delete" src="//enterprise.mficient.com/images/add.png"/></td>'
                                 + '</tr>');

    $('#img_Del_Angular_Axis' + axis.index + '_Band_' + band.index).bind('click', function () {
        if (parseInt($('tbody#Angular_Axis' + axis.index + '_Band_div').find("tr").length, 10) > 1)
            $('#Angular_Axis' + axis.index + '_Band_' + band.index).remove();
        else
            showMessage('This band cannot be deleted. Angular Guage Axis should have at least 1 band.', DialogType.Error);
    });

    $('#txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_StartVal').keypress(function (event) {
        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
        return;
    });

    $('#txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_StartVal').blur(function (event) {
        var errors = [];
        errors = validateBandStartVal(cntrl, 'Angular_Axis' + axis.index + '_Band_div', 'txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_StartVal', axis.index, event.target.value);

        //        if (!validateBandStartVal(cntrl, 'Angular_Axis' + axis.index + '_Band_div', 'txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_StartVal', axis.index, event.target.value)) {
        //            errors.push('Its StartValue should be greater than previous Bands StartValue.');
        //        }
        //        if (event.target.value > 100) {
        //            errors.push('StartValue cannot be greater than 100.');
        //        }

        if (errors.length > 0) {
            showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
            return;
        }
    });

    $('#txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_InnerRad').keypress(function (event) {
        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
    });

    $('#txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_InnerRad').blur(function (event) {
        if (event.target.value.length <= 0) {
            event.target.value = 95;
        }
    });

    $('#btn_Angular_Axis' + axis.index + '_Band_' + band.index + '_Color').css('background-color', band.color);
    $('#txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_Color').val(band.color);

    var bandColorPicker = $('#btn_Angular_Axis' + axis.index + '_Band_' + band.index + '_Color').colpick({
        layout: 'hex',
        submit: 0,
        colorScheme: 'dark',
        onChange: function (hsb, hex, rgb, el, bySetColor) {
            if (!bySetColor)
                $('#txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_Color').val('#' + hex);

            if ($('#txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_Color').val() == "" || $('#txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_Color').val() == undefined)
                $('#txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_Color').val('#3c32c7');

            $('#btn_Angular_Axis' + axis.index + '_Band_' + band.index + '_Color').css('background-color', $('#txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_Color').val());
        }
    })
    $('#txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_Color').keyup(function () {
        $(bandColorPicker).colpickSetColor(this.value);
    });
    $('#txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_Color').bind("change", function () {
        $(bandColorPicker).colpickSetColor(this.value);
    });
    $('#txt_Angular_Axis' + axis.index + '_Band_' + band.index + '_Color').bind("blur", function () {
        $(bandColorPicker).colpickSetColor(this.value);
    });

    $('#img_Add_Angular_Axis' + axis.index + '_Band_' + band.index).bind('click', function () {
        if ($('#Angular_Axis' + axis.index + '_Band_div').children().length < maxNoOfBands) {
            var newBand = new AxisBandObj();
            newBand.index = parseInt($('tbody#Angular_Axis' + axis.index + '_Band_div').find("tr").length, 10) + 1;
            var prvBandIndex = parseInt(newBand.index, 10) - 1;
            newBand.startVal = parseFloat($('#txt_Angular_Axis' + axis.index + '_Band_' + prvBandIndex + '_StartVal').val()) + 10;
            newBand.color = "#3c32c7";
            newBand.innerRadius = "95";
            _addNewBand(axis, newBand, cntrl);
        }
        else
            showMessage('More than 3 bands cannot be added in Angular Guage Axis.', DialogType.Error);
    });
}

function validateBandStartVal(cntrl, axisBandId, currentBandId, axisIndex, value) {
    var errors = [];
    if (parseFloat(value) > 100)
        if ($.inArray('StartValue cannot be greater than 100.', errors) < 0) errors.push('StartValue cannot be greater than 100.');

    $('#' + axisBandId).find("*").each(function () {
        if (this.nodeName.toLowerCase() === 'input' && this.type === "text") {
            if (this.id.split('_')[5] == "StartVal") {
                if (currentBandId.split('_')[4] == "1" && value.length <= 0) {
                    $('#' + currentBandId).val('0');
                }
                else {
                    if (this.id != currentBandId && parseInt(this.id.split('_')[4], 10) < parseInt(currentBandId.split('_')[4], 10)) {
                        if ((parseFloat(value) <= parseFloat(this.value))) {
                            if ($.inArray('StartValue should be greater than previous Bands StartValue.', errors) < 0) errors.push('StartValue should be greater than previous Bands StartValue.');
                        }
                    }
                    if (this.id != currentBandId && parseInt(this.id.split('_')[4], 10) > parseInt(currentBandId.split('_')[4], 10)) {
                        if ((parseFloat(value) >= parseFloat(this.value))) {
                            if ($.inArray('StartValue cannot be greater than Higher Bands StartValue.', errors) < 0) errors.push('StartValue cannot be greater than Higher Bands StartValue.');
                        }
                    }
                }
            }
        }
    });

    if (errors.length > 0) {
        var index = parseInt(currentBandId.split('_')[4], 10) - 1;
        if (cntrl.axisObjs[0].bands != undefined && cntrl.axisObjs[0].bands != null && cntrl.axisObjs[0].bands.length > 0) {
            if (cntrl.axisObjs[0].bands[index] != undefined || cntrl.axisObjs[0].bands[index] != null)
                $('#' + currentBandId).val(cntrl.axisObjs[0].bands[index].startVal);
        }
        else {
            var index = parseInt(currentBandId.split('_')[4], 10) - 1;
            if ($('#txt_Angular_Axis' + axisIndex + '_Band_' + index + '_StartVal').val() != undefined)
                $('#' + currentBandId).val(parseInt($('#txt_Angular_Axis' + axisIndex + '_Band_' + index + '_StartVal').val(), 10) + 10);
            else
                $('#' + currentBandId).val("10");
        }
    }
    return errors;
}

function _saveAngularGuageAxisData(cntrl) {
    cntrl.axisObjs = [];
    for (var i = 1; i <= parseInt(cntrl.noOfAxis, 10); i++) {
        var angularAxis = _getAngularGuageAxisData("Angular_Axis" + i, $('#Angular_Axis' + i + '_Index').html().trim());
        if (angularAxis != null)
            cntrl.fnAddAxisObj(angularAxis);
    }

    if (cntrl.noOfAxis != cntrl.axisObjs.length)
        showMessage('Please define selected no. of Angular Gauge Axis.', DialogType.Error);

    SubProcAngularGuageData(false);
}

function _getAngularGuageAxisData(_Id, _Index) {
    var angularAxis = new AxisObject();
    angularAxis.index = _Index;
    angularAxis.bands = [];
    $('#' + _Id).find("*").each(function () {
        if (this.nodeName.toLowerCase() === 'input' && this.type === "text") {
            switch (this.id.split('_')[3]) {
                case "TickInterval":
                    angularAxis.tickInterval = this.value;
                    break;
                case "StartVal":
                    angularAxis.startVal = this.value;
                    break;
                //                case "EndVal":   
                //                    angularAxis.endVal = this.value;   
                //                    break;   
            }
        }
    });
    if (angularAxis.tickInterval.length > 0 && angularAxis.startVal.length > 0) {
        angularAxis.bands = [];
        var objAxisBand = new AxisBandObj();
        var bandIndex = 1;
        $('#' + _Id + '_Band_div').find("tr").each(function () {
            objAxisBand = new AxisBandObj();
            objAxisBand.index = bandIndex;
            $(this).find("*").each(function () {
                if (this.nodeName.toLowerCase() === 'input' && this.type === "text") {
                    switch (this.id.split('_')[5]) {
                        case "StartVal":
                            objAxisBand.startVal = this.value;
                            break;
                        case "InnerRad":
                            objAxisBand.innerRadius = this.value;
                            break;
                        case "Color":
                            objAxisBand.color = this.value;
                            break;
                    }
                }
            });
            bandIndex = bandIndex + 1;
            if (objAxisBand.startVal != undefined && objAxisBand.innerRadius != undefined && objAxisBand.color != undefined) {
                if (objAxisBand.startVal.length > 0 && objAxisBand.innerRadius.length > 0 && objAxisBand.color.length > 0) {
                    angularAxis.fnAddAxisBandObj(objAxisBand);
                }
            }
        });
        if (angularAxis.bands.length <= 0)
            showMessage('Please define at least 1 Band in Angular Gauge Axis' + angularAxis.index, DialogType.Error);
        return angularAxis;
    }
    else {
        return null;
    }
}

/*---------------------------------------------------CylinderGuage---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.CylinderGuage = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            Title: function (control, appObj) {
                $(control).val(appObj.chartTitle);
            },
            MinValue: function (control, appObj) {
                $(control).val(appObj.minVal);

                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
            },
            ShowValueAs: function (control, appObj) {
                $(control).val(appObj.valueAs);
            },
            FillColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.CylinderGuage.propSheetCntrl.Appearance.getFillColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                if (appObj.fillColor != undefined || appObj.fillColor != null) {
                    $(currentCntrl.control).val('#' + appObj.fillColor);
                    $(currentCntrl.browseBtn).css('background-color', '#' + appObj.fillColor);
                }
                else {
                    $(currentCntrl.control).val('#823554');
                    $(currentCntrl.browseBtn).css('background-color', '#823554');
                }


                var fillCol = '';
                var fillColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            fillCol = '#' + hex;
                        }

                        if ($(currentCntrl.control).val() == "" || $(currentCntrl.control).val() == undefined)
                            fillCol = '#823554';

                        $(currentCntrl.control).val(fillCol);
                        $(currentCntrl.browseBtn).css('background-color', fillCol);
                        appObj.fillColor = $(currentCntrl.control).val();
                    }
                });
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.fillColor = evnt.target.value;
                    fillCol = evnt.target.value;
                    $(fillColorPicker).colpickSetColor(this.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.fillColor = evnt.target.value;
                    fillCol = evnt.target.value;
                    $(fillColorPicker).colpickSetColor(this.value);
                });
            },
            Border: function (control, cntrlObj) {
                $(control).val(cntrlObj.border);
                if (!cntrlObj.border)
                    cntrlObj.border = "0";
                if (cntrlObj.border === "0")
                    $('#' + cntrlObj.id + '_OuterDiv').removeClass('data-inset');
                else
                    $('#' + cntrlObj.id + '_OuterDiv').addClass('data-inset');
            },
            eventHandlers: {
                Title: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.chartTitle = evnt.target.value;
                },
                MinValue: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.minVal = evnt.target.value;
                },
                ShowValueAs: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.valueAs = evnt.target.value;
                },
                FillColor: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fillColor = evnt.target.value;
                },
                SetBorder: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.border = evnt.target.value;
                    if (propObject.border === "0")
                        $('#' + propObject.id + '_OuterDiv').removeClass('data-inset');
                    else
                        $('#' + propObject.id + '_OuterDiv').addClass('data-inset');
                }
            }
        },
        Data: {
            Databinding: function (control, appObj) {
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id + '">None</option>';
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id + '">Offline Database</option>';
                    control.html(strOptions);
                    appObj.fnSetBindType(MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj);
                    appObj.fnClearAllDatabindObjs();
                    appObj.fnAddDatabindObj(
                        new DatabindingObj(
                            "", [], "0", MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id,
                            "", "", "", "", "",
                            { ignoreCache: false }
                        )
                   );
                }
                else {
                }
                $(control).val(appObj.bindType.id);
            },
            DataObject: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.CylinderGuage.propSheetCntrl.Data.getDataObjectCntrl();
                if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
                if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) {
                    $.each(appObj.databindObjs, function () {
                        if (this['name'].length > 0)
                            $(currentCntrl.control).val(this['name']);
                        else
                            $(currentCntrl.control).val('Select Object');
                        switch (this['bindObjType']) {
                            case "1":
                                mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "2":
                                mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "5":
                                mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control);
                                break;
                        }
                    });
                }
                else {
                    $(currentCntrl.control).val('Select Object');
                }
            },
            eventHandlers: {
                DataObject: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;

                    var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv);
                    var $DataObjectControl = $('#' + cntrls.DataObject.controlId);
                    var dataObject = new DatabindingObj();
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    if (evnt.target.value == "0") {
                        $ViewProperty.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        dataObject.bindObjType = bindType.id;
                    }
                    if (evnt.target.value == "1") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "2") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "5") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "6") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                        dataObject.bindObjType = bindType.id;
                    }
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            if (this.bindObjType != bindType.id) {
                                this.id = "";
                                this.name = "";
                                this.cmdParams = [];
                                propObject.displayText = "";
                                this.bindObjType = bindType.id;
                                $($DataObjectControl).val("Select Object");
                            }
                        });
                    }
                    else {
                        var dataObject = new DatabindingObj("", [], "0", bindType.id, "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        $($DataObjectControl).val("Select Object");
                    }

                    propObject.bindType = bindType;
                },
                DataBinding: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.CylinderGuage.propSheetCntrl.Data.getDataObjectCntrl();
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                        intlJson = [],
                        isEdit = false;
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            var objDatabindObj = jQuery.extend(true, {}, this);
                            if (this['name'].length > 0)
                                $(currentCntrl.control).val(this['name']);
                            else
                                $(currentCntrl.control).val('Select Object');

                            switch (this['bindObjType']) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:
                                    //                                    intlJson = MF_HELPERS
                                    //                                                    .intlsenseHelper
                                    //                                                    .getControlDatabindingIntellisenseJson(
                                    //                                                        objCurrentForm, propObject.id, true
                                    //                                                    );
                                    if (objDatabindObj != null
                                            && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                            ) {
                                        isEdit = true;
                                    }

                                    mFicientIde
                                        .MF_DATA_BINDING
                                        .processDataBindingByObjectType({
                                            controlType: MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE,
                                            databindObject: objDatabindObj,
                                            objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                            isEdit: isEdit,
                                            intlsJson: intlJson,
                                            control: propObject
                                        });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 420, false);
                                    break;
                            }
                        });
                    }
                    else {
                        $(currentCntrl.control).val('Select Object');
                    }
                }
            }
        },
        Behaviour: {
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay);
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.CylinderGuage.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getFillColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE.propPluginPrefix,
                    "Appearance", "FillColor");
            }
            return objControl;
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }
        return {
            Appearance: {
                getFillColorCntrl: _getFillColorCntrl
            },
            Data: {
                getDataObjectCntrl: _getDataObjectCntrl
            },
            Behaviour: {
                getConditionsCntrl: _getConditionsCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.CylinderGuage = {
    "type": MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Cylinder Guage",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.CylinderGuage.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.CylinderGuage.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Title",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Title",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.CylinderGuage.groups.Appearance.eventHandlers.Title,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "Min Value",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MinValue",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.CylinderGuage.groups.Appearance.eventHandlers.MinValue,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "Show Value As",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "ShowValueAs",
                "defltValue": "3",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.CylinderGuage.groups.Appearance.eventHandlers.ShowValueAs,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Percentage",
                      "value": "Percentage"
                  },
                  {
                      "text": "Absolute",
                      "value": "Absolute"
                  }
               ]
            },
            {
                "text": "Fill Color",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "FillColor",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.CylinderGuage.groups.Appearance.eventHandlers.FillColor,
                      "context": "",
                      "arguments": {}
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Border",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Border",
                "defltValue": "0",
                "validations": [

            ],
                "customValidations": [

            ],
                "events": [
                {
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.CylinderGuage.groups.Appearance.eventHandlers.SetBorder,
                    "context": "",
                    "arguments": {

                    }
                }
            ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                ]
            }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
            {
                "text": "Databinding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Databinding",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.CylinderGuage.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                //                  {
                //                      "text": "None",
                //                      "value": "0"
                //                  },
                  {
                  "text": "Database Object",
                  "value": "1"
              },
                  {
                      "text": "Web Service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  },
                   {
                       "text": "Offline Database",
                       "value": "6"
                   }
               ]
            },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.CylinderGuage.groups.Data.eventHandlers.DataBinding,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            }
         ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.CylinderGuage.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.CylinderGuage.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};


function CylinderGuage(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.CylinderGuage;
    this.description = opts.description;
    this.type = opts.type;
    this.bindType = opts.bindType;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.chartTitle = opts.chartTitle;
    this.minVal = opts.minVal;
    this.maxVal = opts.maxVal;
    this.value = opts.value;
    this.fillColor = opts.fillColor;
    this.valueAs = opts.valueAs;
    this.databindObjs = opts.databindObjs;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.border = opts.border;
};

CylinderGuage.prototype = new ControlNew();
CylinderGuage.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.CylinderGuage;
CylinderGuage.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
CylinderGuage.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.valueAs = "Percentage";
    this.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
    this.conditionalDisplay = "1";
    this.border = "0";
    this.isDeleted = false;

    var dataObject = new DatabindingObj("", [], "0", this.bindType.id, "", "", "", "", "", { ignoreCache: false });
    this.fnAddDatabindObj(dataObject);
}
CylinderGuage.prototype.fnGetValue = function () {
    return this.value;
}
CylinderGuage.prototype.fnGetMaxValue = function () {
    return this.maxVal;
}
CylinderGuage.prototype.fnSetValue = function (val) {
    this.value = val;
}
CylinderGuage.prototype.fnSetMaxValue = function (val) {
    this.maxVal = val;
}
CylinderGuage.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
}
CylinderGuage.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
}
CylinderGuage.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(
                     databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }
}
CylinderGuage.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
}
CylinderGuage.prototype.fnClearAllDatabindObjs = function () {
    this.databindObjs = [];
}
CylinderGuage.prototype.fnGetValue = function () {
    return this.value;
};
CylinderGuage.prototype.fnSetValue = function (value) {
    this.value = value;
};
CylinderGuage.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
CylinderGuage.prototype.fnSetBindType = function (value) {
    this.bindType = value;
};

/*---------------------------------------------------DrilldownPieChart---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DrilldownPieChart = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, cntrlObj) {
                $(control).val(cntrlObj.type.UIName);
            },
            Name: function (control, cntrlObj) {
                $(control).val(cntrlObj.userDefinedName);
            },
            Description: function (control, cntrlObj, options/*object of control related html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = cntrlObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            Title: function (control, cntrlObj) {
                $(control).val(cntrlObj.chartTitle);
            },
            DataLabelThreshold: function (control, cntrlObj) {
                $(control).val(cntrlObj.dataLblThreshold);
            },
            ShowValueAs: function (control, cntrlObj) {
                $(control).val(cntrlObj.valueAs);
            },
            Animation: function (control, cntrlObj) {
                $(control).val(cntrlObj.animation);
            },
            DonutStyle: function (control, cntrlObj) {
                var strDonutStyle = cntrlObj.fnGetDonutStyle();

                if (!mfUtil.isNullOrUndefined(strDonutStyle)
                && !mfUtil.isEmptyString(strDonutStyle)) {
                    $(control).val(strDonutStyle);

                    var $uiImg = $('img[id="' + cntrlObj.fnGetId() + '"]');
                    if (strDonutStyle === "1") {
                        $uiImg.attr('src', MF_IDE_CONSTANTS.enterpriseImagesPath + "drilldonut.png");
                    }
                    else {
                        $uiImg.attr('src', MF_IDE_CONSTANTS.enterpriseImagesPath + "drilldownpiechart.png");
                    }
                }


            },
            InnerRadius: function (control, cntrlObj, args) {
                var strInnerRadius = cntrlObj.fnGetInnerRadius();
                var strDonutStyle = cntrlObj.fnGetDonutStyle();
                $(control).val(strInnerRadius);
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
                if (!mfUtil.isNullOrUndefined(strDonutStyle)
                && !mfUtil.isEmptyString(strDonutStyle)) {
                    if (strDonutStyle === "1") {
                        args.cntrlHtmlObjects.wrapperDiv.show();
                    }
                    else {
                        args.cntrlHtmlObjects.wrapperDiv.hide();
                    }
                }
            },
            Border: function (control, cntrlObj) {
                $(control).val(cntrlObj.border);
                if (!cntrlObj.border)
                    cntrlObj.border = "0";
                if (cntrlObj.border === "0")
                    $('#' + cntrlObj.id + '_OuterDiv').removeClass('data-inset');
                else
                    $('#' + cntrlObj.id + '_OuterDiv').addClass('data-inset');
            },
            eventHandlers: {
                Title: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.chartTitle = evnt.target.value;
                },
                DataLabelThreshold: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.dataLblThreshold = evnt.target.value;
                },
                ShowValueAs: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.valueAs = evnt.target.value;
                },
                Animation: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.animation = evnt.target.value;
                },
                DonutStyle: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.fnSetDonutStyle($self.val());
                    var objInnerRadiusCntrls = PROP_JSON_HTML_MAP
                        .DrilldownPieChart
                        .propSheetCntrl
                        .Appearance.getInnerRadius();

                    var $uiImg = $('img[id="' + propObject.fnGetId() + '"]');

                    if ($self.val() === "1") {
                        objInnerRadiusCntrls.wrapperDiv.show();
                        objInnerRadiusCntrls.control.val("50");
                        propObject.fnSetInnerRadius("50");
                        $uiImg.attr('src', MF_IDE_CONSTANTS.enterpriseImagesPath + "drilldonut.png");
                    }
                    else {
                        objInnerRadiusCntrls.wrapperDiv.hide();
                        objInnerRadiusCntrls.control.val("");
                        propObject.fnSetInnerRadius("0");
                        $uiImg.attr('src', MF_IDE_CONSTANTS.enterpriseImagesPath + "drilldownpiechart.png");
                    }
                },					
                SetBorder: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.border = evnt.target.value;
                    if (propObject.border === "0")
                        $('#' + propObject.id + '_OuterDiv').removeClass('data-inset');
                    else
                        $('#' + propObject.id + '_OuterDiv').addClass('data-inset');
                },
                InnerRadius: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var strValidationErrorMsg = "Inner radius should be between 10 and 90 ,both included.";
                    //value is numeric chekcec on key press
                    var iVal = 0;
                    if (!mfUtil.isNullOrUndefined($self.val())
                    && !mfUtil.isEmptyString($self.val())
                    ) {
                        iVal = parseInt($self.val());
                        if (!isNaN(iVal)) {
                            if (iVal < 10 || iVal > 90) {
                                showMessage(strValidationErrorMsg, DialogType.Error);
                                $self.val("50");
                                propObject.fnSetInnerRadius("50");
                                return;
                            }
                            else {
                                propObject.fnSetInnerRadius($self.val());
                            }
                        }
                        else {
                            showMessage(strValidationErrorMsg, DialogType.Error);
                            $self.val("50");
                            propObject.fnSetInnerRadius("50");
                            return;
                        }
                    }
                    else {
                        showMessage(strValidationErrorMsg, DialogType.Error);
                        $self.val("50");
                        propObject.fnSetInnerRadius("50");
                        return;
                    }

                }
            }
        },
        Data: {
            Databinding: function (control, cntrlObj) {
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id + '">None</option>';
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id + '">Offline Database</option>';
                    control.html(strOptions);
                    cntrlObj.fnSetBindType(MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj);
                    cntrlObj.fnClearAllDatabindObjs();
                    cntrlObj.fnAddDatabindObj(
                        new DatabindingObj(
                            "", [], "0", MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id,
                            "", "", "", "", "",
                            { ignoreCache: false }
                        )
                   );
                }
                else {
                }
                $(control).val(cntrlObj.bindType.id);
            },
            DataObject: function (control, cntrlObj) {
                var currentCntrl = PROP_JSON_HTML_MAP.DrilldownPieChart.propSheetCntrl.Data.getDataObjectCntrl();
                if (cntrlObj.bindType.id != "0" && cntrlObj.bindType.id != undefined)
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
                if ((cntrlObj.databindObjs != null || cntrlObj.databindObjs != undefined) && cntrlObj.databindObjs.length > 0) {
                    $.each(cntrlObj.databindObjs, function () {
                        if (this['name'].length > 0)
                            $(currentCntrl.control).val(this['name']);
                        else
                            $(currentCntrl.control).val('Select Object');
                        // switch (this['bindObjType']) {
                        //     case "1":
                        //         mFicientIde.MF_DATA_BINDING.databaseObjectBinding(cntrlObj, this, currentCntrl.control);
                        //         break;
                        //     case "2":
                        //         mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(cntrlObj, this, currentCntrl.control);
                        //         break;
                        //     case "5":
                        //         mFicientIde.MF_DATA_BINDING.oDataObjectBinding(cntrlObj, this, currentCntrl.control);
                        //         break;
                        // }
                    });
                }
                else {
                    $(currentCntrl.control).val('Select Object');
                }
            },
            eventHandlers: {
                DataObject: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;

                    var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv);
                    var $DataObjectControl = $('#' + cntrls.DataObject.controlId);
                    var dataObject = new DatabindingObj();
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    if (evnt.target.value == "0") {
                        $ViewProperty.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        dataObject.bindObjType = bindType.id;
                    }
                    if (evnt.target.value == "1") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "2") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "5") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "6") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                        dataObject.bindObjType = bindType;
                    }
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            if (this.bindObjType != bindType.id) {
                                this.id = "";
                                this.name = "";
                                this.cmdParams = [];
                                propObject.displayText = "";
                                this.bindObjType = bindType.id;
                                $($DataObjectControl).val("Select Object");
                            }
                        });
                    }
                    else {
                        var dataObject = new DatabindingObj("", [], "0", bindType.id, "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        $($DataObjectControl).val("Select Object");
                    }

                    propObject.bindType = bindType;
                },
                DataBinding: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = PROP_JSON_HTML_MAP.DrilldownPieChart.propSheetCntrl.Data.getDataObjectCntrl();
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                        intlJson = [],
                        isEdit = false;
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            var objDatabindObj = jQuery.extend(true, {}, this);
                            if (this['name'].length > 0)
                                $(currentCntrl.control).val(this['name']);
                            else
                                $(currentCntrl.control).val('Select Object');
                            switch (this['bindObjType']) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:
                                    if (objDatabindObj != null
                                    && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                    ) {
                                        isEdit = true;
                                    }

                                    mFicientIde
                                   .MF_DATA_BINDING
                                   .processDataBindingByObjectType({
                                       controlType: MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART,
                                       databindObject: objDatabindObj,
                                       objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                       isEdit: isEdit,
                                       intlsJson: intlJson,
                                       control: propObject
                                   });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 420, false);
                                    break;
                            }
                        });
                    }
                    else {
                        $(currentCntrl.control).val('Select Object');
                    }
                }
            }
        },
        Behaviour: {
            Display: function (control, cntrlObj) {
                $(control).val(cntrlObj.conditionalDisplay);
            },
            Conditions: function (control, cntrlObj) {
                var currentCntrl = PROP_JSON_HTML_MAP.DrilldownPieChart.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (cntrlObj.conditionalDisplay == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData)
                   .data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }
        function _getAppearanceInnerRadiusCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART.propPluginPrefix,
                    "Appearance", "InnerRadius");
            }
            return objControl;
        }
        return {
            Data: {
                getDataObjectCntrl: _getDataObjectCntrl
            },
            Behaviour: {
                getConditionsCntrl: _getConditionsCntrl
            },
            Appearance: {
                getInnerRadius: _getAppearanceInnerRadiusCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.DrilldownPieChart = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "PieChart (Drilldown)",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DrilldownPieChart.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DrilldownPieChart.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Title",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Title",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DrilldownPieChart.groups.Appearance.eventHandlers.Title,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "DataLabel Threshold",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "DataLabelThreshold",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DrilldownPieChart.groups.Appearance.eventHandlers.DataLabelThreshold,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": true
                }
            },
            {
                "text": "Show Value As",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "ShowValueAs",
                "defltValue": "3",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DrilldownPieChart.groups.Appearance.eventHandlers.ShowValueAs,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": true
                },
                "options": [
                  {
                      "text": "Percentage",
                      "value": "Percentage"
                  },
                  {
                      "text": "Absolute",
                      "value": "Absolute"
                  }
               ]
            },
            {
                "text": "Animation",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Animation",
                "defltValue": "0",
                "validations": [

            ],
                "customValidations": [

            ],
                "events": [
                {
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.DrilldownPieChart.groups.Appearance.eventHandlers.Animation,
                    "context": "",
                    "arguments": {

                    }
                }
            ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                ]
            },
            {
                "text": "Border",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Border",
                "defltValue": "0",
                "validations": [

            ],
                "customValidations": [

            ],
                "events": [
                {
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.DrilldownPieChart.groups.Appearance.eventHandlers.SetBorder,
                    "context": "",
                    "arguments": {

                    }
                }
            ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                ]
            },
            {
                "text": "Donut Style",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "DonutStyle",
                "defltValue": "0",
                "validations": [
                ],
                "customValidations": [
                ],
                "events": [
                {
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.DrilldownPieChart.groups.Appearance.eventHandlers.DonutStyle,
                    "context": "",
                    "arguments": {
                    }
                }
                ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                ]
            },
            {
                "text": "Inner Radius (%)",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "InnerRadius",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DrilldownPieChart.groups.Appearance.eventHandlers.InnerRadius,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 2,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": true
                }
            }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
            {
                "text": "Databinding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Databinding",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DrilldownPieChart.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                //                  {
                //                      "text": "None",
                //                      "value": "0"
                //                  },
                  {
                  "text": "Database Object",
                  "value": "1"
              },
                  {
                      "text": "Web Service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  },
                   {
                       "text": "Offline Database",
                       "value": "6"
                   }
               ]
            },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.DrilldownPieChart.groups.Data.eventHandlers.DataBinding,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            }
         ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DrilldownPieChart.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.DrilldownPieChart.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};


function DrilldownPieChart(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Pie;
    this.description = opts.description;
    this.type = opts.type;
    this.bindType = opts.bindType;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.chartTitle = opts.chartTitle;
    this.title = opts.title;
    this.value = opts.value;
    this.animation = opts.animation;
    this.dataLblThreshold = opts.dataLblThreshold;
    this.valueAs = opts.valueAs;
    this.databindObjs = opts.databindObjs;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.donutStyle = opts.donutStyle;
    this.innerRadius = opts.innerRadius;
    this.subSliceTitle = opts.subSliceTitle;
    this.border = opts.border;
}

DrilldownPieChart.prototype = new ControlNew();
DrilldownPieChart.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DrilldownPieChart;
DrilldownPieChart.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
DrilldownPieChart.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.dataLblThreshold = "3";
    this.animation = "0";
    this.valueAs = "Absolute";
    this.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
    this.conditionalDisplay = "1";
    this.isDeleted = false;
    this.donutStyle = "0";
    this.innerRadius = "0";
    this.border = "0";
    var dataObject = new DatabindingObj("", [], "0", this.bindType.id, "", "", "", "", "", { ignoreCache: false });
    this.fnAddDatabindObj(dataObject);
};
DrilldownPieChart.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
};
DrilldownPieChart.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
};
//shalini Reset Function
DrilldownPieChart.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(
                     databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }
};
DrilldownPieChart.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
};
DrilldownPieChart.prototype.fnClearAllDatabindObjs = function () {
    this.databindObjs = [];
};

DrilldownPieChart.prototype.fnAddBindOption = function (bindOption/*ManualBindOptions Object*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }

    if (bindOption && bindOption instanceof ManualBindOptions) {
        this.bindOptions.push(bindOption);
    }
};
DrilldownPieChart.prototype.fnAddBindOptions = function (bindOptions/*ManualBindOptions Object[]*/) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) {
        this.bindOptions = [];
    }
    if ($.isArray(bindOptions)) {
        var self = this;
        $.each(bindOptions, function (index, value) {
            if (value && value instanceof ManualBindOptions) {
                self.databindObjs.push(value);
            }
        });
    }
};
DrilldownPieChart.prototype.fnGetTitle = function () {
    return this.title;
};
DrilldownPieChart.prototype.fnGetValue = function () {
    return this.value;
};
DrilldownPieChart.prototype.fnGetId = function () {
    return this.id;
};
DrilldownPieChart.prototype.fnSetId = function (value) {
    this.id = value;
};
DrilldownPieChart.prototype.fnSetTitle = function (value) {
    this.title = value;
};
DrilldownPieChart.prototype.fnSetValue = function (value) {
    this.value = value;
};
DrilldownPieChart.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
DrilldownPieChart.prototype.fnSetDonutStyle = function (value) {
    this.donutStyle = value;
};
DrilldownPieChart.prototype.fnGetDonutStyle = function () {
    return this.donutStyle;
};
DrilldownPieChart.prototype.fnSetInnerRadius = function (value) {
    this.innerRadius = value;
};
DrilldownPieChart.prototype.fnGetInnerRadius = function () {
    return this.innerRadius;
};
DrilldownPieChart.prototype.fnSetSubSliceTitle = function (value) {
    this.subSliceTitle = value;
};
DrilldownPieChart.prototype.fnGetSubSliceTitle = function () {
    return this.subSliceTitle;
};
DrilldownPieChart.prototype.fnSetBindType = function (value) {
    this.bindType = value;
};


/*---------------------------------------------------DataPoint Chart---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DataPointChart = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            ChartDescription: function (control, appObj, options) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.chartDescription;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                $('#' + appObj.id + '_Desc').html(appObj.chartDescription);
            },
            Color: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DataPointChart.propSheetCntrl.Appearance.getColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                if (appObj.color != undefined || appObj.color != null) {
                    $(currentCntrl.control).val('#' + appObj.color);
                    $(currentCntrl.browseBtn).css('background-color', '#' + appObj.color);
                    $('#' + appObj.id + '_Value').css('color', '#' + appObj.color);
                }
                else {
                    $(currentCntrl.control).val('#1f7cbf');
                    $(currentCntrl.browseBtn).css('background-color', '#1f7cbf');
                    $('#' + appObj.id + '_Value').css('color', '#1f7cbf');
                }


                var color = '';
                var colorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            color = '#' + hex;
                        }

                        if ($(currentCntrl.control).val() == "" || $(currentCntrl.control).val() == undefined)
                            fillCol = '#823554';

                        $(currentCntrl.control).val(color);
                        $(currentCntrl.browseBtn).css('background-color', color);
                        $('#' + appObj.id + '_Value').css('color', color);
                        appObj.color = $(currentCntrl.control).val();
                    }
                });
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.color = evnt.target.value;
                    color = evnt.target.value;
                    $(colorPicker).colpickSetColor(this.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.color = evnt.target.value;
                    color = evnt.target.value;
                    $(colorPicker).colpickSetColor(this.value);
                });
            },
            eventHandlers: {
                ChartDescription: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.chartDescription = evnt.target.value;
                    $('#' + propObject.id + '_Desc').html(propObject.chartDescription);
                }
            }
        },
        Data: {
            Databinding: function (control, appObj) {
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id + '">None</option>';
                    strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id + '">Offline Database</option>';
                    control.html(strOptions);
                    appObj.fnSetBindType(MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj);
                    appObj.fnClearAllDatabindObjs();
                    appObj.fnAddDatabindObj(
                        new DatabindingObj(
                            "", [], "0", MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id,
                            "", "", "", "", "",
                            { ignoreCache: false }
                        )
                   );
                }
                else {
                }
                $(control).val(appObj.bindType.id);
            },
            DataObject: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DataPointChart.propSheetCntrl.Data.getDataObjectCntrl();
                if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
                if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) {
                    $.each(appObj.databindObjs, function () {
                        if (this['name'].length > 0)
                            $(currentCntrl.control).val(this['name']);
                        else
                            $(currentCntrl.control).val('Select Object');
                        switch (this['bindObjType']) {
                            case "1":
                                mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "2":
                                mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "5":
                                mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control);
                                break;
                        }
                    });
                }
                else {
                    $(currentCntrl.control).val('Select Object');
                }
            },
            Unit: function (control, appObj) {
                $(control).val(appObj.unit);
                $('#' + appObj.id + '_Unit').html(appObj.unit);
            },
            Position: function (control, appObj) {
                $(control).val(appObj.position);
            },
            eventHandlers: {
                Unit: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.unit = evnt.target.value;
                    $('#' + propObject.id + '_Unit').html(propObject.unit);
                },
                Position: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.position = evnt.target.value;
                },
                DataObject: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;

                    var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv);
                    var $DataObjectControl = $('#' + cntrls.DataObject.controlId);
                    var dataObject = new DatabindingObj();
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    if (evnt.target.value == "0") {
                        $ViewProperty.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        dataObject.bindObjType = bindType.id;
                    }
                    if (evnt.target.value == "1") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "2") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "5") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "6") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                        dataObject.bindObjType = bindType.id;
                    }
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            if (this.bindObjType != bindType.id) {
                                this.id = "";
                                this.name = "";
                                this.cmdParams = [];
                                propObject.displayText = "";
                                this.bindObjType = bindType.id;
                                $($DataObjectControl).val("Select Object");
                            }
                        });
                    }
                    else {
                        var dataObject = new DatabindingObj("", [], "0", bindType.id, "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        $($DataObjectControl).val("Select Object");
                    }

                    propObject.bindType = bindType;
                },
                DataBinding: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DataPointChart.propSheetCntrl.Data.getDataObjectCntrl();
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                        intlJson = [],
                        isEdit = false;
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            var objDatabindObj = jQuery.extend(true, {}, this);
                            if (this['name'].length > 0)
                                $(currentCntrl.control).val(this['name']);
                            else
                                $(currentCntrl.control).val('Select Object');

                            switch (this['bindObjType']) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:
                                    //                                    intlJson = MF_HELPERS
                                    //                                                    .intlsenseHelper
                                    //                                                    .getControlDatabindingIntellisenseJson(
                                    //                                                        objCurrentForm, propObject.id, true
                                    //                                                    );
                                    if (objDatabindObj != null
                                            && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                            ) {
                                        isEdit = true;
                                    }

                                    mFicientIde
                                        .MF_DATA_BINDING
                                        .processDataBindingByObjectType({
                                            controlType: MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE,
                                            databindObject: objDatabindObj,
                                            objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                            isEdit: isEdit,
                                            intlsJson: intlJson,
                                            control: propObject
                                        });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 420, false);
                                    break;
                            }
                        });
                    }
                    else {
                        $(currentCntrl.control).val('Select Object');
                    }
                }
            }
        },
        Behaviour: {
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay);
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DataPointChart.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART.propPluginPrefix,
                    "Appearance", "Color");
            }
            return objControl;
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }
        return {
            Appearance: {
                getColorCntrl: _getColorCntrl
            },
            Data: {
                getDataObjectCntrl: _getDataObjectCntrl
            },
            Behaviour: {
                getConditionsCntrl: _getConditionsCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.DataPointChart = {
    "type": MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Cylinder Guage",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DataPointChart.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DataPointChart.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Chart Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "ChartDescription",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DataPointChart.groups.Appearance.eventHandlers.ChartDescription,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 500,
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "Color",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Color",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.DataPointChart.groups.Appearance.eventHandlers.Color,
                      "context": "",
                      "arguments": {}
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
            {
                "text": "Databinding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Databinding",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DataPointChart.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Database Object",
                      "value": "1"
                  },
                  {
                      "text": "Web Service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  },
                   {
                       "text": "Offline Database",
                       "value": "6"
                   }
               ]
            },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.DataPointChart.groups.Data.eventHandlers.DataBinding,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            },
            {
                  "text": "Unit",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "Unit",
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [

               ],
                  "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DataPointChart.groups.Data.eventHandlers.Unit,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 8,
                      "disabled": false,
                      "defltValue": "Label Text",
                      "hidden": false
                  }
              },
              {
                "text": "Position",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Position",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.DataPointChart.groups.Data.eventHandlers.Position,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Prefix",
                      "value": "1"
                  },
                  {
                      "text": "Suffix",
                      "value": "2"
                  }
               ]
            }
         ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.CylinderGuage.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.CylinderGuage.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};


function DataPointChart(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.DataPointChart;
    this.description = opts.description;
    this.type = opts.type;
    this.bindType = opts.bindType;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.chartDescription = opts.chartDescription;
    this.unit = opts.unit;
    this.position = opts.position;
    this.value = opts.value;
    this.color = opts.color;
    this.databindObjs = opts.databindObjs;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
};

DataPointChart.prototype = new ControlNew();
DataPointChart.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.DataPointChart;
DataPointChart.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
DataPointChart.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.position = "1";
    this.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
    this.conditionalDisplay = "1";
    this.isDeleted = false;
    this.chartDescription = "Data Point Chart Description";
    this.unit = "million";

    var dataObject = new DatabindingObj("", [], "0", this.bindType.id, "", "", "", "", "", { ignoreCache: false });
    this.fnAddDatabindObj(dataObject);
}
DataPointChart.prototype.fnGetValue = function () {
    return this.value;
}
DataPointChart.prototype.fnSetValue = function (val) {
    this.value = val;
}
DataPointChart.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
}
DataPointChart.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
}
DataPointChart.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(
                     databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }
}
DataPointChart.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
}
DataPointChart.prototype.fnClearAllDatabindObjs = function () {
    this.databindObjs = [];
};
DataPointChart.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
DataPointChart.prototype.fnSetBindType = function (value) {
    this.bindType = value;
};

var mIdeChartControls = "mIdeChartControls";
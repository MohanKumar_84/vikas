﻿
mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Form = {
    groups: {
        Properties: {
            Name: function (control, appObj) {
                $(control).val(appObj.name)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.formDescription;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
            },
            HiddenVariables: function (control, objForm) {
                var currentControl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Form.propSheetCntrl.Properties.getHiddenVariables();
                var aryHidFields = objForm.fnGetAllControlsofSection(MF_IDE_CONSTANTS.hiddenFieldsSection);
                control.val(aryHidFields.length + " " + "Hidden Variables");
            },
            PropertyBinding: function (control, objForm, htmlDtls) {
                control.val("");
                if (objForm.isChildForm) {
                    htmlDtls.cntrlHtmlObjects.wrapperDiv.show();
                }
                else {
                    htmlDtls.cntrlHtmlObjects.wrapperDiv.hide();
                }
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    app = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                    if (!propObject.isChildForm) {
                        if (app.fnDoesFormWithNameExists(evnt.target.value, propObject.id)) {
                            if (propObject.name != undefined || propObject.name != null) evnt.target.value = propObject.name;
                            else evnt.target.value = "";
                            showMessage("Form name already exist.", DialogType.Error);
                            return;
                        }
                        else {
                            propObject.fnRename(evnt.target.value);
                        }
                    }
                    else {
                        if (app.fnDoesChildFormWithNameExists(evnt.target.value, propObject.id)) {
                            if (propObject.name != undefined || propObject.name != null) evnt.target.value = propObject.name;
                            else evnt.target.value = "";
                            showMessage("Form name already exist.", DialogType.Error);
                            return;
                        }
                        else {
                            propObject.fnRename(evnt.target.value);
                        }
                    }
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.formDescription = evnt.target.value;
                },
                HiddenVariables: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    mFicientIde.HiddenFieldControlHelper.formDetailsHtml(propObject);
                },
                Scripts: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    _jsAceEditor.setTextInEditor("");

                    var scriptViewAppear = '', scriptViewReAppear = '', scriptViewDisAppear = '', scriptCommonFunc = '',
                    scriptSubmitBtn = '', scriptBackBtn = '', scriptCancelBtn = '', scriptActionSheetBtn = '', script = '';

                    $('#drpFormEvents').val("1");
                    $.uniform.update('#drpFormEvents');
                    var prevSelectedVal = $('#drpFormEvents')[0].value;

                    if (propObject.viewAppear != null && propObject.viewAppear != undefined) scriptViewAppear = propObject.viewAppear;
                    if (propObject.viewReAppear != null && propObject.viewReAppear != undefined) scriptViewReAppear = propObject.viewReAppear;
                    if (propObject.viewDisAppear != null && propObject.viewDisAppear != undefined) scriptViewDisAppear = propObject.viewDisAppear;
                    if (propObject.commonFunc != null && propObject.commonFunc != undefined) scriptCommonFunc = propObject.commonFunc;
                    if (propObject.submitBtn != null && propObject.submitBtn != undefined) scriptSubmitBtn = propObject.submitBtn;
                    if (propObject.backBtn != null && propObject.backBtn != undefined) scriptBackBtn = propObject.backBtn;
                    if (propObject.cancelBtn != null && propObject.cancelBtn != undefined) scriptCancelBtn = propObject.cancelBtn;
                    if (propObject.actionSheetBtn != null && propObject.actionSheetBtn != undefined) scriptActionSheetBtn = propObject.actionSheetBtn;

                    switch ($('#drpFormEvents')[0].value) {
                        case "0":
                            if (scriptCommonFunc != null && scriptCommonFunc != undefined) {
                                _jsAceEditor.setTextInEditor(Base64.decode(scriptCommonFunc));
                            }
                            break;
                        case "1":
                            if (scriptViewAppear != null && scriptViewAppear != undefined) {
                                _jsAceEditor.setTextInEditor(Base64.decode(scriptViewAppear));
                            }
                            break;
                        case "2":
                            if (scriptViewReAppear != null && scriptViewReAppear != undefined) {
                                _jsAceEditor.setTextInEditor(Base64.decode(scriptViewReAppear));
                            }
                            break;
                        case "3":
                            if (scriptViewDisAppear != null && scriptViewDisAppear != undefined) {
                                _jsAceEditor.setTextInEditor(Base64.decode(scriptViewDisAppear));
                            }
                            break;
                        case "4":
                            if (scriptSubmitBtn != null && scriptSubmitBtn != undefined) {
                                _jsAceEditor.setTextInEditor(Base64.decode(scriptSubmitBtn));
                            }
                            break;
                        case "5":
                            if (scriptBackBtn != null && scriptBackBtn != undefined) {
                                _jsAceEditor.setTextInEditor(Base64.decode(scriptBackBtn));
                            }
                            break;
                        case "6":
                            if (scriptCancelBtn != null && scriptCancelBtn != undefined) {
                                _jsAceEditor.setTextInEditor(Base64.decode(scriptCancelBtn));
                            }
                            break;
                        case "7":
                            if (scriptActionSheetBtn != null && scriptActionSheetBtn != undefined) {
                                _jsAceEditor.setTextInEditor(Base64.decode(scriptActionSheetBtn));
                            }
                    }
                    $('#drpFormEvents').unbind('change');
                    $('#drpFormEvents').bind('change', function () {
                        $.uniform.update('#drpFormEvents');
                        script = _jsAceEditor.getText();
                        switch (prevSelectedVal) {
                            case "0":
                                if (script != null && script != undefined) {
                                    scriptCommonFunc = Base64.encode(script);
                                }
                                break;
                            case "1":
                                if (script != null && script != undefined) {
                                    scriptViewAppear = Base64.encode(script);
                                }
                                break;
                            case "2":
                                if (script != null && script != undefined) {
                                    scriptViewReAppear = Base64.encode(script);
                                }
                                break;
                            case "3":
                                if (script != null && script != undefined) {
                                    scriptViewDisAppear = Base64.encode(script);
                                }
                                break;
                            case "4":
                                if (script != null && script != undefined) {
                                    scriptSubmitBtn = Base64.encode(script);
                                }
                                break;
                            case "5":
                                if (script != null && script != undefined) {
                                    scriptBackBtn = Base64.encode(script);
                                }
                                break;
                            case "6":
                                if (script != null && script != undefined) {
                                    scriptCancelBtn = Base64.encode(script);
                                }

                                break;
                            case "7":
                                if (script != null && script != undefined) {
                                    scriptActionSheetBtn = Base64.encode(script);
                                }
                                break;
                        }
                        _jsAceEditor.setTextInEditor("");
                        prevSelectedVal = this.value;
                        switch (this.value) {
                            case "0":
                                if (scriptCommonFunc != null && scriptCommonFunc != undefined) {
                                    _jsAceEditor.setTextInEditor(Base64.decode(scriptCommonFunc));
                                }
                                break;
                            case "1":
                                if (scriptViewAppear != null && scriptViewAppear != undefined) {
                                    _jsAceEditor.setTextInEditor(Base64.decode(scriptViewAppear));
                                }
                                break;
                            case "2":
                                if (scriptViewReAppear != null && scriptViewReAppear != undefined) {
                                    _jsAceEditor.setTextInEditor(Base64.decode(scriptViewReAppear));
                                }
                                break;
                            case "3":
                                if (scriptViewDisAppear != null && scriptViewDisAppear != undefined) {
                                    _jsAceEditor.setTextInEditor(Base64.decode(scriptViewDisAppear));
                                }
                                break;
                            case "4":
                                if (scriptSubmitBtn != null && scriptSubmitBtn != undefined) {
                                    _jsAceEditor.setTextInEditor(Base64.decode(scriptSubmitBtn));
                                }
                                break;
                            case "5":
                                if (scriptBackBtn != null && scriptBackBtn != undefined) {
                                    _jsAceEditor.setTextInEditor(Base64.decode(scriptBackBtn));
                                }
                                break;
                            case "6":
                                if (scriptCancelBtn != null && scriptCancelBtn != undefined) {
                                    _jsAceEditor.setTextInEditor(Base64.decode(scriptCancelBtn));
                                }
                                break;
                            case "7":
                                if (scriptActionSheetBtn != null && scriptActionSheetBtn != undefined) {
                                    _jsAceEditor.setTextInEditor(Base64.decode(scriptActionSheetBtn));
                                }
                        }
                    });

                    SubProcShowJsEditor(true, "Scripts");
                    $('[id$=popup_custom_buttom_save]').unbind('click');
                    $('[id$=popup_custom_buttom_save]').bind('click', function () {
                        SaveAceScriptForForm(propObject, scriptCommonFunc, scriptViewAppear, scriptViewReAppear,scriptViewDisAppear, scriptSubmitBtn, scriptBackBtn, scriptCancelBtn, scriptActionSheetBtn, _jsAceEditor);
                        return false;
                    });
                    $('[id$=popup_custom_buttom_close]').unbind('click');
                    $('[id$=popup_custom_buttom_close]').bind('click', function () {
                        $('[id$=btnSaveScript]').unbind('click');
                        $('[id$=btnSaveScript]').bind('click', function () {
                            SaveAceScriptForForm(propObject, scriptCommonFunc, scriptViewAppear, scriptViewReAppear,scriptViewDisAppear, scriptSubmitBtn, scriptBackBtn, scriptCancelBtn, scriptActionSheetBtn, _jsAceEditor);
                            $('#subProcScriptConfirmation').dialog('close');
                            SubProcShowJsEditor(false);
                            return false;
                        });
                        $('[id$=btnDiscartScript]').unbind('click');
                        $('[id$=btnDiscartScript]').bind('click', function () {
                            scriptViewAppear = '', scriptViewReAppear = '', scriptViewDisAppear = '', scriptCommonFunc = '',
                        scriptSubmitBtn = '', scriptBackBtn = '', scriptCancelBtn = '', scriptActionSheetBtn = '', script = '';
                            $('#subProcScriptConfirmation').dialog('close');
                            SubProcShowJsEditor(false);
                            return false;
                        });
                        showDialog('subProcScriptConfirmation', 'Save Changes', 300, 'dialog_warning.png', false);
                        
                    });

                    //mFicientIde.HiddenFieldControlHelper.formDetailsHtml(propObject);
                },
                PropertyBinding: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    //var objForm = form;
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    var objMainForm = form;
                    var aryHidFields = objCurrentForm.fnGetAllControlsofSection(MF_IDE_CONSTANTS.hiddenFieldsSection);
                    var editListCntrl = objMainForm.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                    var editListProps = editListCntrl.fnGetProperties();
                    if ($.isArray(aryHidFields) && aryHidFields.length === 0) {
                        showMessage('There are no hidden fields added to the form.', DialogType.Error);
                        return;
                    }

                    //make html
                    mFicientIde.HiddenFieldControlHelper
                    .editListPropsBindingInChildForm
                    .createHtmlForPropsBinding(aryHidFields, editListProps);

                    //show modal pop up
                    showModalPopUp("divChildFormHidFieldPropBinding", "Property Binding", 400, false, "");
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.FormData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getHiddenVariablesControl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(MF_IDE_CONSTANTS.OBJECTS_WITH_PROPERTY.FORM.propPluginPrefix,
                        "Properties", "HiddenVariables");
            }
            return objControl;
        }
        return {
            Properties: {
                getHiddenVariables: _getHiddenVariablesControl
            }
        };
    })()

};

mFicientIde.PropertySheetJson.Form = {
    "type": MF_IDE_CONSTANTS.OBJECTS_WITH_PROPERTY.FORM,
    "groups": [
      {
          "name": "Properties",
          "prefixText": "Properties",
          "hidden": true,
          "properties": [
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Form.groups.Properties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Form.groups.Properties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Hidden Variables",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "HiddenVariables",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Form.groups.Properties.eventHandlers.HiddenVariables,
                      "context": "",
                      "arguments": {}
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Scripts",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Scripts",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Form.groups.Properties.eventHandlers.Scripts,
                      "context": "",
                      "arguments": {}
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Property Binding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "PropertyBinding",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [
                ],
                "customValidations": [
                ],
                "events": [{
                    "name": "click",
                    "func": PROP_JSON_HTML_MAP.Form.groups.Properties.eventHandlers.PropertyBinding,
                    "context": "",
                    "arguments": {}
                }],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      }
   ]
};

function Form(options) {
    if (!options) options = {};
    this.id = options.id;
    this.oldName = options.oldName;
    this.isNew = options.isNew;
    this.rowPanels = options.rowPanels;
    this.formDescription = options.formDescription;
    this.name = options.name;
    this.rptRowClick = options.rptRowClick;
    this.appId = options.appId;
    this.formModelType = options.formModelType;
    this.actionBtn1Setting = options.actionBtn1Setting; /*object of label and operationType*/
    this.actionBtn2Setting = options.actionBtn2Setting;
    this.actionBtn3Setting = options.actionBtn3Setting;
    this.isDeleted = options.isDeleted;
    this.isTablet = options.isTablet;
    this.isChildForm = options.isChildForm;
    this.bindedProps = options.bindedProps;
    this.viewAppear = options.viewAppear;
    this.viewReAppear = options.viewReAppear;
    this.viewDisAppear = options.viewDisAppear;
    this.commonFunc = options.commonFunc;
    this.submitBtn = options.submitBtn;
    this.backBtn = options.backBtn;
    this.cancelBtn = options.cancelBtn;
    this.actionSheetBtn = options.actionSheetBtn;
    this.listRowTouch = options.listRowTouch;
    this.btnCntrl = options.btnCntrl;
    this.childFormType = options.childFormType; // mFicientIde.MF_IDE_CONSTANTS.CHILD_FORM_TYPE
    //this.saveUpdateProp = {}; // {isNew : "true", rename ; "", isDeleted : "false"}
    this.xRefForSaving = {
        isNew: false,
        oldName: "",
        isDeleted: false,
        rptRowClickChanged: false,
        actionBtn1Changed: false,
        actionBtn2Changed: false,
        actionBtn3Changed: false
    }
}

Form.prototype = new Form();
Form.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Form;
Form.prototype.fnAddRowPanels = function (rowPanel/*RowPanel*/) {
    if (!this.rowPanels || !$.isArray(this.rowPanels)) {
        this.rowPanels = [];
    }
    if (rowPanel && rowPanel instanceof RowPanel) {
        this.rowPanels.push(rowPanel);
    }
}
Form.prototype.fnRename = function (formName) {
    if (!this.isNew &&
        !this.oldName &&
        this.name !== formName) {

        this.oldName = this.name;
        this.name = formName;
    }
    else if (this.name != formName) {
        this.name = formName;
    }
};
Form.prototype.fnGetId = function () {
    return this.id;
};
Form.prototype.fnSetId = function (value) {
    this.id = value;
};
Form.prototype.fnGetName = function () {
    return this.name;
};
Form.prototype.fnSetName = function (value) {
    this.name = value;
};
Form.prototype.fnIsChildForm = function () {
    return this.isChildForm;
};
Form.prototype.fnAddBindProperty = function (propertyName, previousPropName, control) {
    var isValid = true;
    if (!this.bindedProps || !$.isArray(this.bindedProps)) {
        this.bindedProps = [];
    }

    if (propertyName) {
        if (!this.fnIsBindedPropExist(propertyName, control)) {
            if (previousPropName && previousPropName != "-1") {
                var toDelete = true;
                var controls = this.fnGetControlofBindedProperty(previousPropName);
                if (controls && controls.length > 0) {
                    for (var i = 0; i < controls.length; i++) {
                        if (controls[i].id != control.id) {
                            toDelete = false;
                            break;
                        }
                    }
                }
                if (toDelete)
                    this.fnDeleteBindProperty(previousPropName);
            }
            if (propertyName != "-1" && $.inArray(propertyName, this.bindedProps) < 0) this.bindedProps.push(propertyName);
        }
        else {
            showMessage('One EditableList Property cannot be linked to multiple Controls.', DialogType.Error);
            isValid = false;
        }
    }
    return isValid;
}

Form.prototype.fnDeleteBindProperty = function (propertyName) {
    if (this.bindedProps != undefined || this.bindedProps != null) {
        var aryNewbindedProps = $.grep(this.bindedProps, function (value, index) {
            if (propertyName) {
                return value !== propertyName;
            }
        });
        this.bindedProps = aryNewbindedProps;
    }
}

Form.prototype.fnIsBindedPropExist = function (propertyName, control) {
    var exist = false;
    if (this.childFormType.idPrefix == mFicientIde.MF_IDE_CONSTANTS.CHILD_FORM_TYPE.VIEW.idPrefix)
        return exist;
    if (this.childFormType.idPrefix == mFicientIde.MF_IDE_CONSTANTS.CHILD_FORM_TYPE.EDIT.idPrefix && control.type.idPrefix == mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL.idPrefix)
        return exist;
    if ((this.bindedProps != undefined || this.bindedProps != null) && this.bindedProps.length > 0) {
        var _this = this;
        $.each(this.bindedProps, function () {
            if (this == propertyName) {
                var controls = _this.fnGetControlofBindedProperty(propertyName);
                if (controls && controls.length > 0) {
                    for (var i = 0; i < controls.length; i++) {
                        if (controls[i].type.idPrefix != mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL.idPrefix) {
                            exist = true;
                            return false;
                        }
                    }
                }
            }
        });
    }
    return exist;
}

Form.prototype.fnGetControlofBindedProperty = function (propertyName) {
    var controls = this.fnGetAllControls();
    var objControls = [];
    if (controls) {
        $.each(controls, function (cntrlIndex, control) {
            if (control) {
                if (control.bindToProperty && control.bindToProperty === propertyName) {
                    objControls.push(control);
                }
                if (control.bindTextToProperty && control.bindTextToProperty === propertyName) {
                    objControls.push(control);
                }
                if (control.bindValueToProperty && control.bindValueToProperty === propertyName) {
                    objControls.push(control);
                }
            }
        });
    }
    return objControls;
}

Form.prototype.fnResetObjectDetails = function () {
    var rowPanels = this.rowPanels;
    this.rowPanels = [];
    var i = 0;
    if (rowPanels && $.isArray(rowPanels) && rowPanels.length > 0) {
        for (i = 0; i <= rowPanels.length - 1; i++) {
            var objRowPanel = new RowPanel(rowPanels[i]);
            objRowPanel.fnResetObjectDetails();
            this.fnAddRowPanels(objRowPanel);
        }
    }
}

Form.prototype.fnDeleteRowPanel = function (rowPanelId/*RowPanelId*/) {
    var objRowPanel = this.fnGetRowPanel(rowPanelId);
    if (objRowPanel != null && objRowPanel != undefined) {
        objRowPanel.isDeleted = true;
        if ((objRowPanel.colmnPanels != null || objRowPanel.colmnPanels != undefined) && objRowPanel.colmnPanels.length > 0) {
            $.each(objRowPanel.colmnPanels, function (colIndex, columnPanel) {
                if (columnPanel != null && columnPanel != undefined) {
                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                            if (control)
                                control.isDeleted = true;
                        });
                    }
                }
            });
        }
    }
}

Form.prototype.fnDeleteRowPanelObject = function (rowPanelId/*RowPanelId*/) {
    var aryNewRowPanels = $.grep(this.rowPanels, function (objRowPanel) {
        if (objRowPanel instanceof RowPanel) {
            return objRowPanel.id !== rowPanelId;
        }
    });
    this.rowPanels = aryNewRowPanels;
}

Form.prototype.fnGetRowPanel = function (rowPanelId/*ColumnPanel*/) {
    var objRowPanel = null;
    for (var row = 0; row <= this.rowPanels.length - 1; row++) {
        if (this.rowPanels[row] && this.rowPanels[row].id == rowPanelId && this.rowPanels[row].isDeleted == false)
            objRowPanel = this.rowPanels[row];
    }
    return objRowPanel;
}

Form.prototype.fnGetRowPanelCount = function () {
    var arrRowPanel = [];
    for (var row = 0; row <= this.rowPanels.length - 1; row++) {
        if (this.rowPanels[row].id !== "Sec_hd" && this.rowPanels[row].isDeleted == false)
            arrRowPanel.push(this.rowPanels[row]);
    }
    return arrRowPanel.length;
}

Form.prototype.fnGetControl = function (rowPanelId/*RowPanelId*/, columnPanelId/*ColumnPanelId*/, controlId/*ControlId*/) {
    var objControl = null;
    if (rowPanelId != '' || rowPanelId != undefined) var rowPanel = this.fnGetRowPanel(rowPanelId);
    if (rowPanel != null && rowPanel != undefined && rowPanel.isDeleted == false) {
        var columnPanel = rowPanel.fnGetColumnPanel(columnPanelId);
        if (columnPanel != null) objControl = columnPanel.fnGetControl(controlId);
    }
    return objControl;
}

Form.prototype.fnGetControlFromId = function (controlId/*ControlId*/) {
    var objControl = null;
    if ((this.rowPanels != null || this.rowPanels != undefined) && this.rowPanels.length > 0) {
        $.each(this.rowPanels, function (rowIndex, rowPanel) {
            if (rowPanel != undefined && rowPanel != null) {
                if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                    $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                        if (columnPanel != undefined && columnPanel != null) {
                            if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                $.each(columnPanel.controls, function (cntrlIndex, control) {
                                    if (control != undefined && control != null) {
                                        if (control.id == controlId) {
                                            objControl = control;
                                            return false;
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }
    return objControl;
}
Form.prototype.fnGetAllControlsByType = function (cntrlType/*MF_IDE_CONSTANTS.CONTROLS*/) {
    var controls = [];
    if (this.rowPanels && this.rowPanels.length > 0) {
        $.each(this.rowPanels, function (rowIndex, rowPanel) {
            if (rowPanel != undefined && rowPanel != null) {
                if (rowPanel.colmnPanels && rowPanel.colmnPanels.length > 0) {
                    $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                        if (columnPanel != undefined && columnPanel != null) {
                            if (columnPanel.controls && columnPanel.controls.length > 0) {
                                $.each(columnPanel.controls, function (cntrlIndex, control) {
                                    if (control.type == cntrlType) {
                                        controls.push(control);
                                        //return false;
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }
    return controls;
};
Form.prototype.fnGetAllControls = function () {
    var controls = [];
    if ((this.rowPanels != null || this.rowPanels != undefined) && this.rowPanels.length > 0) {
        $.each(this.rowPanels, function (rowIndex, rowPanel) {
            if (rowPanel != undefined && rowPanel != null) {
                if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                    $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                        if (columnPanel != undefined && columnPanel != null) {
                            if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                $.each(columnPanel.controls, function (cntrlIndex, control) {
                                    controls.push(control);
                                });
                            }
                        }
                    });
                }
            }
        });
    }
    return controls;
};
Form.prototype.fnGetAllColumnPanels = function () {
    var columnPanels = [];
    if ((this.rowPanels != null || this.rowPanels != undefined) && this.rowPanels.length > 0) {
        $.each(this.rowPanels, function (rowIndex, rowPanel) {
            if (rowPanel != undefined && rowPanel != null) {
                if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                    $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                        if (columnPanel != undefined && columnPanel != null) {
                            columnPanels.push(columnPanel);
                        }
                    });
                }
            }
        });
    }
    return columnPanels;
}
Form.prototype.fnGetAllRowPanels = function () {
    return this.rowPanels;
}
Form.prototype.fnGetAllDeletedRowPanels = function () {
    var arryRowPanels = [];
    if ((this.rowPanels != null || this.rowPanels != undefined) && this.rowPanels.length > 0) {
        $.each(this.rowPanels, function (rowIndex, rowPanel) {
            if (rowPanel != undefined && rowPanel != null && rowPanel.isDeleted) {
                arryRowPanels.push(rowPanel);
            }
        });
    }
    return arryRowPanels;
}
Form.prototype.fnGetAllDeletedColumnPanels = function (rowPanelId) {
    var arryColPanels = [];
    var objRowPanel = this.fnGetRowPanel(rowPanelId);
    if (objRowPanel != undefined && objRowPanel != null) {
        if ((objRowPanel.colmnPanels != null || objRowPanel.colmnPanels != undefined) && objRowPanel.colmnPanels.length > 0) {
            $.each(objRowPanel.colmnPanels, function (colIndex, columnPanel) {
                if (columnPanel != undefined && columnPanel != null && columnPanel.isDeleted) {
                    arryColPanels.push(columnPanel);
                }
            });
        }
    }
    return arryColPanels;
}
Form.prototype.fnGetAllDeletedControls = function (rowPanelId, colPanelId) {
    var arryCntrls = [];
    var objRowPanel = this.fnGetRowPanel(rowPanelId);
    var objColPanel = null;
    if (objRowPanel != undefined && objRowPanel != null) {
        if ((objRowPanel.colmnPanels != null || objRowPanel.colmnPanels != undefined) && objRowPanel.colmnPanels.length > 0) {
            $.each(objRowPanel.colmnPanels, function (colIndex, columnPanel) {
                if (columnPanel != undefined && columnPanel != null && columnPanel.id === colPanelId) {
                    objColPanel = columnPanel;
                    return;
                }
            });
        }
    }
    if (objColPanel != undefined && objColPanel != null) {
        if ((objColPanel.controls != null || objColPanel.controls != undefined) && objColPanel.controls.length > 0) {
            $.each(objColPanel.controls, function (cntrlIndex, control) {
                if (control != undefined && control != null && control.isDeleted)
                    arryCntrls.push(control);
            });
        }
    }
    return arryCntrls;
}

Form.prototype.fnGetAllControlsofSection = function (rowPanelId/*RowPanelId*/) {
    var controls = [];
    var objRowPanel = this.fnGetRowPanel(rowPanelId);
    if (objRowPanel != null || objRowPanel != undefined) {
        if ((objRowPanel.colmnPanels != null || objRowPanel.colmnPanels != undefined) && objRowPanel.colmnPanels.length > 0) {
            $.each(objRowPanel.colmnPanels, function (colIndex, columnPanel) {
                if (columnPanel != undefined && columnPanel != null) {
                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                            controls.push(control);
                        });
                    }
                }
            });
        }
    }
    return controls;
}
Form.prototype.fnIsControlUserDefinedNameExist = function (controlUserDefinedName/*ControlUserDefinedName*/, controlId/*ControlId*/) {
    var controls = this.fnGetAllControls();
    var isAlreadyExist = false;
    if ((controls != null || controls != undefined) && controls.length > 0) {
        $.each(controls, function () {
            if (this != undefined && this != null) {
                if (this.userDefinedName === controlUserDefinedName && this.id !== controlId && this.isDeleted === false) {
                    isAlreadyExist = true;
                    return false;
                }
            }
        });
    }
    return isAlreadyExist;
}

Form.prototype.fnIsSectionUserDefinedNameExist = function (sectionUserDefinedName/*SectionUserDefinedName*/, sectionId/*SectionId*/) {
    var sections = this.rowPanels;
    var isAlreadyExist = false;
    if ((sections != null || sections != undefined) && sections.length > 0) {
        $.each(sections, function () {
            if (this != undefined && this != null) {
                if (this.userDefinedName === sectionUserDefinedName && this.id !== sectionId && this.isDeleted === false) {
                    isAlreadyExist = true;
                    return false;
                }
            }
        });
    }
    return isAlreadyExist;
}

Form.prototype.fnAddControl = function (rowPanelId/*RowPanelId*/, columnPanelId/*ColumnPanelId*/, control/*Control*/) {
    if (rowPanelId != '' || rowPanelId != undefined) var rowPanel = this.fnGetRowPanel(rowPanelId);
    if (rowPanel != null) {
        var columnPanel = rowPanel.fnGetColumnPanel(columnPanelId);
        if (columnPanel != null) columnPanel.fnAddControl(control);
    }
}
//Form.prototype.fnDeleteControl = function (controlId/*ControlId*/) {
//    var control = this.fnGetControlFromId(controlId);
//    if (control.type === MF_IDE_CONSTANTS.CONTROLS.LIST) {
//        this.fnSetRowClickableOfForm("0");
//        this.fnDeleteActionBtnSettingByType(
//            MF_IDE_CONSTANTS.listActionBtnType.actionBtn1);
//        this.fnDeleteActionBtnSettingByType(
//            MF_IDE_CONSTANTS.listActionBtnType.actionBtn2);
//        this.fnDeleteActionBtnSettingByType(
//            MF_IDE_CONSTANTS.listActionBtnType.actionBtn3);
//    }
//    if (control != null || control != undefined) {
//        control.isDeleted = true;
//        this.deleteControlBindedProp(control);
//    }
//}
Form.prototype.fnDeleteControl = function (controlId/*ControlId*/) {
    var control = this.fnGetControlFromId(controlId);
    if (control != null || control != undefined) {
        control.isDeleted = true;
        this.deleteControlBindedProp(control);
        if (control.type === MF_IDE_CONSTANTS.CONTROLS.LIST) {
            this.fnSetRowClickableOfForm("0");
            this.fnDeleteActionBtnSettingByType(
                MF_IDE_CONSTANTS.listActionBtnType.actionBtn1);
            this.fnDeleteActionBtnSettingByType(
                MF_IDE_CONSTANTS.listActionBtnType.actionBtn2);
            this.fnDeleteActionBtnSettingByType(
                MF_IDE_CONSTANTS.listActionBtnType.actionBtn3);
        }
        if (control.isNew === true) {
            this.fnDeleteControlObject(controlId);
        }
    }
};
Form.prototype.fnDeleteControlObject = function (controlId/*ControlId*/) {
    var colPanel;
    if ((this.rowPanels != null || this.rowPanels != undefined) && this.rowPanels.length > 0) {
        $.each(this.rowPanels, function (rowIndex, rowPanel) {
            if (rowPanel != undefined && rowPanel != null) {
                if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                    $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                        if (columnPanel != undefined && columnPanel != null) {
                            if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                $.each(columnPanel.controls, function (cntrlIndex, control) {
                                    if (control.id == controlId) {
                                        colPanel = columnPanel;
                                        return false;
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

    var arryNewControlObjs = $.grep(colPanel.controls, function (controlObj) {
        if (controlObj instanceof ControlNew) {
            return controlObj.id !== controlId;
        }
    });

    colPanel.controls = arryNewControlObjs;
}
Form.prototype.fnGetRowPanelOfControl = function (controlId/*ControlId*/) {
    var rowPnl;
    if ((this.rowPanels != null || this.rowPanels != undefined) && this.rowPanels.length > 0) {
        $.each(this.rowPanels, function (rowIndex, rowPanel) {
            if (rowPanel != undefined && rowPanel != null) {
                if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                    $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                        if (columnPanel != undefined && columnPanel != null) {
                            if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                $.each(columnPanel.controls, function (cntrlIndex, control) {
                                    if (control.id == controlId) {
                                        rowPnl = rowPanel;
                                        return false;
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

    return rowPnl;
}

Form.prototype.fnGetColPanelOfControl = function (controlId/*ControlId*/) {
    var colPnl;
    if ((this.rowPanels != null || this.rowPanels != undefined) && this.rowPanels.length > 0) {
        $.each(this.rowPanels, function (rowIndex, rowPanel) {
            if (rowPanel != undefined && rowPanel != null) {
                if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                    $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                        if (columnPanel != undefined && columnPanel != null) {
                            if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                $.each(columnPanel.controls, function (cntrlIndex, control) {
                                    if (control.id == controlId) {
                                        colPnl = columnPanel;
                                        return false;
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

    return colPnl;
}

Form.prototype.fnDeleteColPanelObject = function (colPanelId/*ControlId*/) {
    var objRowPanel;
    if ((this.rowPanels != null || this.rowPanels != undefined) && this.rowPanels.length > 0) {
        $.each(this.rowPanels, function (rowIndex, rowPanel) {
            if (rowPanel != undefined && rowPanel != null) {
                if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                    $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                        if (columnPanel != undefined && columnPanel != null) {
                            if (columnPanel.id == colPanelId) {
                                objRowPanel = rowPanel;
                                return false;
                            }
                        }
                    });
                }
            }
        });
    }

    var arryNewColPanelObjs = $.grep(objRowPanel.colmnPanels, function (colPanel) {
        if (colPanel instanceof ColumnPanel) {
            return colPanel.id !== colPanelId;
        }
    });

    objRowPanel.colmnPanels = arryNewColPanelObjs;
}

Form.prototype.fnDeleteRowPanelObject = function (rowPanelId/*ControlId*/) {
    var arryNewRowPanelObjs = $.grep(this.rowPanels, function (objRowPanel) {
        if (objRowPanel instanceof RowPanel) {
            return objRowPanel.id !== rowPanelId;
        }
    });

    this.rowPanels = arryNewRowPanelObjs;
}

Form.prototype.fillProperties = function (uiJson) {
    if (uiJson) {
        var objUIJson = MF_HELPERS.getJsonObject(uiJson);
        var objFormJson = MF_HELPERS.getJsonObject(objUIJson.formJson) && MF_HELPERS.getJsonObject(objUIJson.formJson).page;
        if (objUIJson) {
            this.id = objUIJson.formId;
            this.name = objUIJson.formName;
            this.formDescription = objUIJson.formDesc;
            this.type = objFormJson.ptyp;
            this.rptRowClick = objUIJson.rptRowClick;
            this.masterId = objUIJson.masterid;
            this.appId = objUIJson.parentId;
            this.formModelType = objUIJson.modelType;
        }
    }
}
Form.prototype.fnGetAllControlsIdAndName = function () {
    var objFormFromJson = $.parseJSON(objForm.formJson);
    var aryCntrlsIdAndName = [];
    if (objFormFromJson) {
        var rows = objFormFromJson.page.rows;
        for (var i = 0; i <= rows.length - 1; i++) {
            var cols = rows[i].cols;
            if (cols) {
                for (var j = 0; j <= cols.length - 1; j++) {
                    var cntrls = cols[i] && cols[i].gctrl;
                    if (cntrls) {
                        for (var k = 0; k <= cntrls.length - 1; k++) {
                            aryCntrlsIdAndName.push({
                                name: cntrls[k].udn,
                                id: cntrls[k].id
                            });
                        }
                    }
                }
            }
        }
    }
    return aryCntrlsIdAndName;
};

Form.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Form;
/*MOHAN*/
Form.prototype.fnSetControlsOfColumnPanel = function (rowPanelId, colPanelId, controls) {
    var objRowPanel = this.fnGetRowPanel(rowPanelId);
    var objColumnPanel = objRowPanel.fnGetColumnPanel(colPanelId);
    objColumnPanel.controls = [];
    if (controls && $.isArray(controls) && controls.length > 0) {
        for (var i = 0; i <= controls.length - 1; i++) {
            objColumnPanel.fnAddControl(controls[i]);
        }
    }
}

Form.prototype.fnControlNameExist = function (name,
        idsToIgnore/*optional aryof ids.ids of controls.If providedthen this ids name will be ignored.*/) {
    var controls = this.fnGetAllControls();
    var isAlreadyExist = false;
    if (controls && controls.length > 0) {
        if (idsToIgnore && $.isArray(idsToIgnore) && idsToIgnore.length > 0) {
            $.each(controls, function (index, value) {
                if ($.inArray(value.id, idsToIgnore) === -1) {
                    if (value.userDefinedName === name) {
                        isAlreadyExist = true;
                        return false;
                    }
                }
            });
        }
        else {
            $.each(controls, function (index, value) {
                if (value.userDefinedName === name) {
                    isAlreadyExist = true;
                    return false;
                }
            });
        }

    }
    return isAlreadyExist;
}
Form.prototype.fnGetOldControlByOldName = function (name) {
    var objControl = null;
    if (this.rowPanels && $.isArray(this.rowPanels) && this.rowPanels.length > 0) {
        $.each(this.rowPanels, function (rowIndex, rowPanel) {
            if (rowPanel != undefined && rowPanel != null) {
                if (rowPanel.colmnPanels && $.isArray(rowPanel.colmnPanels) && rowPanel.colmnPanels.length > 0) {
                    $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                        if (columnPanel != undefined && columnPanel != null) {
                            if (columnPanel.controls && $.isArray(columnPanel.controls) && columnPanel.controls.length > 0) {
                                $.each(columnPanel.controls, function (cntrlIndex, control) {
                                    if (control != undefined && control != null) {
                                        if ((control.oldName && control.oldName === name) && control.isNew === false) {
                                            objControl = control;
                                            return false;
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }
    return objControl;
};
Form.prototype.fnGetOldControlByNameIfNotRenamed = function (name) {
    var objControl = null;
    if (this.rowPanels && $.isArray(this.rowPanels) && this.rowPanels.length > 0) {
        $.each(this.rowPanels, function (rowIndex, rowPanel) {
            if (rowPanel != undefined && rowPanel != null) {
                if (rowPanel.colmnPanels && $.isArray(rowPanel.colmnPanels) && rowPanel.colmnPanels.length > 0) {
                    $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                        if (columnPanel != undefined && columnPanel != null) {
                            if (columnPanel.controls && $.isArray(columnPanel.controls) && columnPanel.controls.length > 0) {
                                $.each(columnPanel.controls, function (cntrlIndex, control) {
                                    if (control != undefined && control != null) {
                                        if ((control.userDefinedName === name) && !control.oldName && control.isNew === false) {

                                            objControl = control;
                                            return false;
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }
    return objControl;
};
//Form.prototype.fnDeleteControlObject = function (controlId) {
//    var control = this.fnGetControlFromId(controlId);
//    if (control != null || control != undefined)
//        control.isDeleted = true;
//};

Form.prototype.deleteControlBindedProp = function (cntrl) {
    switch (cntrl.type) {
        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER:
        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARCODE:
        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
            this.fnDeleteBindProperty(cntrl.bindToProperty);
            break;
        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON:
        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
            this.fnDeleteBindProperty(cntrl.bindValueToProperty);
            this.fnDeleteBindProperty(cntrl.bindTextToProperty);
            break;
    }
}

Form.prototype.ValidateControls = function () {
    var errors = [];
    var invalidControls = [];
    var controls = this.fnGetAllControls();
    var i = 0;
    if (controls && $.isArray(controls) && controls.length > 0) {
        for (i = 0; i <= controls.length - 1; i++) {
            var objControl = controls[i];
            if (objControl != undefined && objControl != null) {
                if (!objControl.isDeleted) {
                    if ((objControl.bindType != undefined || objControl.bindType != null) && objControl.bindType.id != "0" && objControl.type != mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE) {
                        if (objControl.databindObjs == undefined || objControl.databindObjs == null) {
                            errors.push('In ' + objControl.userDefinedName + ' Databinding object is not defined');
                        }
                        else if (objControl.databindObjs != undefined && objControl.databindObjs != null && objControl.databindObjs.length <= 0) {
                            errors.push('In ' + objControl.userDefinedName + ' Databinding object is not defined');
                        }
                        else if (objControl.databindObjs != undefined && objControl.databindObjs.length > 0) {
                            if (objControl.databindObjs[0].bindObjType != "0" && parseInt(objControl.databindObjs[0].id.length, 10) <= 0) {
                                errors.push('In ' + objControl.userDefinedName + ' Databinding object is not defined');
                            }
                        }
                    }
                    switch (objControl.type) {
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER:
                            if (objControl.labelText.length <= 0 && objControl.placeholder.length <= 0) {
                                errors.push('In ' + objControl.userDefinedName + ' either Label Text or Placeholder property should be defined.');
                            }
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
                            if ((objControl.labelText == undefined || objControl.labelText.length <= 0) && (objControl.placeholder == undefined || objControl.placeholder.length <= 0)) {
                                errors.push('In ' + objControl.userDefinedName + ' either Label Text or Placeholder property should be defined.');
                                invalidControls.push(objControl.userDefinedName);
                            }
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
                            if ((objControl.labelText == undefined || objControl.labelText.length <= 0) && (objControl.promptText == undefined || objControl.promptText.length <= 0)) {
                                errors.push('In ' + objControl.userDefinedName + ' either Label Text or PromptText property should be defined.');
                                invalidControls.push(objControl.userDefinedName);
                            }
                            if (objControl.promptText == undefined || (objControl.promptText != undefined && objControl.promptText.trim().length <= 0)) {
                                objControl.required = "0";
                            }
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON:
                            //                            if (objControl.labelText == undefined || objControl.labelText.length <= 0) {
                            //                                errors.push('In ' + objControl.userDefinedName + ' Label Text cannot be Empty.');
                            //                                invalidControls.push(objControl.userDefinedName);
                            //                            }
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                            if (objControl.labelText == undefined || objControl.labelText.length <= 0) {
                                errors.push('In ' + objControl.userDefinedName + ' Label Text cannot be Empty.');
                                invalidControls.push(objControl.userDefinedName);
                            }
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                            if (objControl.imageBindType != undefined && objControl.imageBindType == "1") {
                                if (objControl.databindObjs == undefined || objControl.databindObjs == null) {
                                    errors.push('In ' + objControl.userDefinedName + ' Databinding object is not defined');
                                }
                                else if (objControl.databindObjs != undefined && objControl.databindObjs != null && objControl.databindObjs.length <= 0) {
                                    errors.push('In ' + objControl.userDefinedName + ' Databinding object is not defined');
                                }
                                else if (objControl.databindObjs != undefined && objControl.databindObjs.length > 0) {
                                    if (objControl.databindObjs[0].bindObjType != "0" && parseInt(objControl.databindObjs[0].id.length, 10) <= 0) {
                                        errors.push('In ' + objControl.userDefinedName + ' Databinding object is not defined');
                                    }
                                }
                            }
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                            if (objControl.labelText == undefined || objControl.labelText.length <= 0) {
                                errors.push('In ' + objControl.userDefinedName + ' Label Text cannot be Empty.');
                                invalidControls.push(objControl.userDefinedName);
                            }
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
                            if (objControl.labelText == undefined || objControl.labelText.length <= 0) {
                                errors.push('In ' + objControl.userDefinedName + ' Label Text cannot be Empty.');
                                invalidControls.push(objControl.userDefinedName);
                            }
                            if (objControl.dfltValue != undefined && parseInt(objControl.dfltValue.trim().length, 10) > 0) {
                                if (objControl.minVal == undefined || objControl.minVal == null) {
                                    errors.push('In ' + objControl.userDefinedName + ' Please define minimum value.');
                                    invalidControls.push(objControl.dfltValue);
                                }
                                else if (objControl.minVal != undefined && parseInt(objControl.dfltValue.trim(), 10) < parseInt(objControl.minVal.trim(), 10)) {
                                    errors.push('In ' + objControl.userDefinedName + ' Default value should not be less than minimum value.');
                                    invalidControls.push(objControl.dfltValue);

                                }
                                if (objControl.maxVal == undefined || objControl.maxVal == null) {
                                    errors.push('In ' + objControl.userDefinedName + ' Please define maximum value.');
                                    invalidControls.push(objControl.dfltValue);
                                }
                                if (objControl.maxVal != undefined && parseInt(objControl.dfltValue.trim(), 10) > parseInt(objControl.maxVal.trim(), 10)) {
                                    errors.push('In ' + objControl.userDefinedName + ' Default value should not be greater than maximum value.');
                                    invalidControls.push(objControl.dfltValue);

                                }
                            }
                            else {
                                if (objControl.minVal != undefined && parseInt(objControl.minVal.trim().length, 10) > 0) {
                                    objControl.dfltValue = objControl.minVal;
                                }
                            }
                            if ((objControl.minVal != undefined || objControl.minVal != null) && (objControl.maxVal != undefined || objControl.maxVal != null)) {
                                if (parseInt(objControl.minVal.trim(), 10) > parseInt(objControl.maxVal.trim(), 10)) {
                                    errors.push('In ' + objControl.userDefinedName + ' minimum value should be less than maximum value.');
                                    invalidControls.push(objControl.dfltValue);
                                }
                            }
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SEPARATOR:
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARCODE:
                            if ((objControl.labelText == undefined || objControl.labelText.length <= 0) && (objControl.placeholder == undefined || objControl.placeholder.length <= 0)) {
                                errors.push('In ' + objControl.userDefinedName + ' either Label Text or Placeholder property should be defined.');
                                invalidControls.push(objControl.userDefinedName);
                            }
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
                            if (objControl.labelText == undefined || objControl.labelText.length <= 0) {
                                errors.push('In ' + objControl.userDefinedName + ' Label Text cannot be Empty.');
                                invalidControls.push(objControl.userDefinedName);
                            }
                            if (!mfUtil.isNullOrUndefined(objControl.fnGetValidationType())
                               && objControl.fnGetValidationType() !== "-1") {
                                if (mfUtil.isNullOrUndefined(objControl.fnGetValidationVal()) ||
                                   mfUtil.isEmptyString(objControl.fnGetValidationVal())) {
                                    errors.push('In ' + objControl.userDefinedName + ' validation value is not defined.');
                                    invalidControls.push(objControl.userDefinedName);
                                }
                            }
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SIGNATURE:
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PICTURE:
                            //                            if (objControl.filePath == undefined || objControl.filePath == null || parseInt(objControl.filePath.length, 10) <= 0) {
                            //                                errors.push('In ' + objControl.userDefinedName + ' filepath is not defined');
                            //                                invalidControls.push(objControl.userDefinedName);
                            //                            }
                            if (objControl.uploadType === "1") {
                                if (objControl.bucketName == undefined || objControl.bucketName == null || parseInt(objControl.bucketName.length, 10) <= 0) {
                                    errors.push('In ' + objControl.userDefinedName + ' Bucket Name is not defined');
                                    invalidControls.push(objControl.userDefinedName);
                                }
                                if (objControl.credential == undefined || objControl.credential == null || objControl.credential === "-1") {
                                    errors.push('In ' + objControl.userDefinedName + ' AWS Credential is not defined');
                                    invalidControls.push(objControl.userDefinedName);
                                }
                            }
                            break;
                    }
                }
            }
        }
    }
    return errors;
};

Form.prototype.fnSetActionBtnSettingsByType = function (setting/*actionBtnSettings*/, actionBtnType/*Contants.listActionBtnType*/) {
    if (!actionBtnType || !setting) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
    var settings = null;
    settings = {
        label: setting.text,
        operationType: setting.actionType
    }
    switch (actionBtnType) {
        case MF_IDE_CONSTANTS.listActionBtnType.actionBtn1:
            this.actionBtn1Setting = settings;
            this.xRefForSaving.actionBtn1Changed = true;
            break;
        case MF_IDE_CONSTANTS.listActionBtnType.actionBtn2:
            this.actionBtn2Setting = settings;
            this.xRefForSaving.actionBtn2Changed = true;
            break;
        case MF_IDE_CONSTANTS.listActionBtnType.actionBtn3:
            this.actionBtn3Setting = settings;
            this.xRefForSaving.actionBtn3Changed = true;
            break;
    }
};
Form.prototype.fnDeleteActionBtnSettingByType = function (actionBtnType/*Contants.listActionBtnType*/) {
    if (!actionBtnType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
    switch (actionBtnType) {
        case MF_IDE_CONSTANTS.listActionBtnType.actionBtn1:
            this.actionBtn1Setting = null;
            this.xRefForSaving.actionBtn1Changed = true;
            break;
        case MF_IDE_CONSTANTS.listActionBtnType.actionBtn2:
            this.actionBtn2Setting = null;
            this.xRefForSaving.actionBtn2Changed = true;
            break;
        case MF_IDE_CONSTANTS.listActionBtnType.actionBtn3:
            this.actionBtn3Setting = null;
            this.xRefForSaving.actionBtn3Changed = true;
            break;
    }
};
Form.prototype.fnGetModelType = function () {
    return this.formModelType;
};
Form.prototype.fnSetRowClickableOfForm = function (rowClickable/*string value 0 or1 */) {
    this.rptRowClick = rowClickable;
    this.xRefForSaving.rptRowClickChanged = true;
};
Form.prototype.fnGetXRefsForSaving = function () {
    return this.xRefForSaving;
};
Form.prototype.fnGetRptRowClickValue = function () {
    return this.rptRowClick;
};
Form.prototype.fnGetActionBtn1Settings = function () {
    return this.actionBtn1Setting;
};
Form.prototype.fnIsRptRowClickableForForm = function () {
    var iRptRowClick = this.fnGetRptRowClickValue();
    if (iRptRowClick && iRptRowClick === "1") {
        return true;
    }
    else if (iRptRowClick && iRptRowClick === "0") {
        return false;
    }
    else {
        return false;
    }
};
Form.prototype.fnGetActionBtn2Settings = function () {
    return this.actionBtn2Setting;
};
Form.prototype.fnGetActionBtn3Settings = function () {
    return this.actionBtn3Setting;
};
//Do this after deleting the controls.changing isDeleted to false here.
Form.prototype.fnResetXRefValuesOnSave = function () {
    var aryControls = this.fnGetAllControls();
    var objControl = null;
    if (aryControls && $.isArray(aryControls) && aryControls.length > 0) {
        for (var i = 0; i <= aryControls.length - 1; i++) {
            objControl = aryControls[i];
            if (objControl) {
                objControl.isNew = false;
                objControl.oldName = "";
                objControl.isDeleted = false;
            }
        }
    }
    this.xRefForSaving = {
        isNew: false,
        oldName: "",
        isDeleted: false,
        rptRowClickChanged: false,
        actionBtn1Changed: false,
        actionBtn2Changed: false,
        actionBtn3Changed: false
    }
};
Form.prototype.fnOnlyHiddenFieldsAndLabelExists = function () {
    var controls = this.fnGetAllControls();
    var blnOnlyHiddenFieldsAndLabelExists = true,
    i = 0, cntrlType = null;
    if (controls && $.isArray(controls) && controls.length > 0) {
        for (i = 0; i <= controls.length - 1; i++) {
            cntrlType = controls[i].type;
            if (cntrlType) {
                if (cntrlType.idPrefix !== MF_IDE_CONSTANTS.CONTROLS.LABEL.idPrefix &&
                    cntrlType.idPrefix !== MF_IDE_CONSTANTS.CONTROLS.HIDDEN.idPrefix && cntrlType.idPrefix !== MF_IDE_CONSTANTS.CONTROLS.SEPARATOR.idPrefix && controls[i].isDeleted == false) {
                    blnOnlyHiddenFieldsAndLabelExists = false;
                    break;
                }
            }
        }
    }
    return blnOnlyHiddenFieldsAndLabelExists;
};
Form.prototype.fnDoesEditableListExists = function () {
    var controls = this.fnGetAllControls();
    var blnEditableListExists = false,
    i = 0, cntrlType = null;
    if (controls && $.isArray(controls) && controls.length > 0) {
        for (i = 0; i <= controls.length - 1; i++) {
            if (controls[i]) {
                cntrlType = controls[i].type;
                if (cntrlType.idPrefix) {
                    if (cntrlType.idPrefix === MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.idPrefix && controls[i].isDeleted == false) {
                        blnEditableListExists = true;
                        break;
                    }
                }
            }
        }
    }
    return blnEditableListExists;
};
Form.prototype.fnDoesListExists = function () {
    var controls = this.fnGetAllControls();
    var blnListExists = false,
    i = 0, cntrlType = null;
    if (controls && $.isArray(controls) && controls.length > 0) {
        for (i = 0; i <= controls.length - 1; i++) {
            if (controls[i]) {
                cntrlType = controls[i].type;
                if (cntrlType.idPrefix) {
                    if (cntrlType.idPrefix === MF_IDE_CONSTANTS.CONTROLS.LIST.idPrefix && controls[i].isDeleted == false) {
                        blnListExists = true;
                        break;
                    }
                }
            }
        }
    }
    return blnListExists;
};
Form.prototype.fnDoesTableExists = function () {
    var controls = this.fnGetAllControls();
    var blnTableExists = false,
    i = 0, cntrlType = null;
    if (controls && $.isArray(controls) && controls.length > 0) {
        for (i = 0; i <= controls.length - 1; i++) {
            if (controls[i]) {
                cntrlType = controls[i].type;
                if (cntrlType) {
                    if (cntrlType.idPrefix === MF_IDE_CONSTANTS.CONTROLS.TABLE.idPrefix && controls[i].isDeleted == false) {
                        blnTableExists = true;
                        break;
                    }
                }
            }
        }
    }
    return blnTableExists;
};
Form.prototype.fnDoesControlExist = function (id) {
    var controls = this.fnGetAllControls();
    var blnExists = false,
    i = 0;
    if (controls && $.isArray(controls) && controls.length > 0) {
        for (i = 0; i <= controls.length - 1; i++) {
            if (controls[i]) {
                if (controls[i].id === id && controls[i].isDeleted == false) {
                    blnExists = true;
                    break;
                }
            }
        }
    }
    return blnExists;
};
Form.prototype.fnGetControlsForIntellisense = function (controlId) {
    var aryControls = this.fnGetAllControls();
    if (aryControls && $.isArray(aryControls) && aryControls.length > 0) {
        aryControls = $.grep(aryControls, function (value, index) {
            return (value.isDeleted === false &&
                    mFicientIde.MF_DATA_BINDING.isValidConditionalControl(value.type) &&
                    value.id !== controlId
                    );
        });
    }
    return aryControls;
};
//MOHAN

//MOHAN BUTTON CONTROL
Form.prototype.fnDoesButtonExists = function () {
    var controls = this.fnGetAllControls();
    var blnExists = false,
    i = 0, cntrlType = null;
    if ($.isArray(controls) && controls.length > 0) {
        for (i = 0; i <= controls.length - 1; i++) {
            if (controls[i]) {
                cntrlType = controls[i].type;
                if (cntrlType) {
                    if (cntrlType.idPrefix === MF_IDE_CONSTANTS.CONTROLS.BUTTON.idPrefix && controls[i].isDeleted == false) {
                        blnExists = true;
                        break;
                    }
                }
            }
        }
    }
    return blnExists;
};
Form.prototype.fnGetButtonsWithOnTouchProcTypeTransition = function () {
    var buttons = this.fnGetAllControlsByType(MF_IDE_CONSTANTS.CONTROLS.BUTTON);
    var i = 0, cntrlType = null,
        objButton = null,
        aryBtnsWithTransition = [];
    if ($.isArray(buttons) && buttons.length > 0) {
        for (i = 0; i <= buttons.length - 1; i++) {
            objButton = buttons[i];
            if (objButton && objButton.fnIsOnTouchProcTypeTransition()) {
                aryBtnsWithTransition.push(objButton);
            }
        }
    }
    return aryBtnsWithTransition;
};
Form.prototype.fnGetColPanelOfControl = function (controlId/*ControlId*/) {
    var colPanel = null;
    if ($.isArray(this.rowPanels) && this.rowPanels.length > 0) {
        $.each(this.rowPanels, function (rowIndex, rowPanel) {
            if (rowPanel) {
                if ($.isArray(rowPanel.colmnPanels) && rowPanel.colmnPanels.length > 0) {
                    $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                        if (columnPanel) {
                            if ($.isArray(columnPanel.controls) && columnPanel.controls.length > 0) {
                                $.each(columnPanel.controls, function (cntrlIndex, control) {
                                    if (control.id == controlId) {
                                        colPanel = columnPanel;
                                        return false;
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

    return colPanel;
};
Form.prototype.fnIsTablet = function () {
    return this.isTablet;
};

Form.prototype.fnValidateNewControlAddition = function (controlType) {
    var aryErrors = [];
    if (controlType === MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST ||
       controlType === MF_IDE_CONSTANTS.CONTROLS.TABLE) {
        if (!this.fnOnlyHiddenFieldsAndLabelExists()) {
            aryErrors.push("Only Separator and Labels are permitted with " + controlType.UIName + " control.");
        }
    }
    else {
        if (controlType !== MF_IDE_CONSTANTS.CONTROLS.LABEL &&
            controlType !== MF_IDE_CONSTANTS.CONTROLS.SEPARATOR) {

            if (controlType === MF_IDE_CONSTANTS.CONTROLS.LIST) {
                if (this.fnDoesListExists()) {
                    aryErrors.push("List control already exists in form.");
                }
            }
            if (this.fnDoesEditableListExists()) {
                aryErrors.push("Only Separator and Labels are permitted with Editable List control.");
            }
            else if (this.fnDoesTableExists()) {
                aryErrors.push("Only Separator and Labels are permitted with Table control.");
            }

        }
    }


    return aryErrors;
};
/*-------------------------------------------------Section--------------------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Section = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Template: function (control, appObj) {
                var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Section.propSheetCntrl.ControlProperties.getTemplateControl();
                if (objForm.isTablet) {
                    $(currentCntrl.wrapperDiv).css('display', 'block');
                    $(currentCntrl.control).val(appObj.type.UiName);
                }
                else {
                    $(currentCntrl.wrapperDiv).css('display', 'none');
                }
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (objForm.isTablet) {
                        if (IsUserAllotedNameExist(objForm, 'TBGB_' + count, evnt.target.value) != false) {
                            if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                            else evnt.target.value = "";
                            return;
                        }
                        else {
                            propObject.userDefinedName = evnt.target.value;
                        }
                    }
                    else {
                        if (IsUserAllotedNameExist(objForm, 'GB_' + count, evnt.target.value) != false) {
                            if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                            else evnt.target.value = "";
                            return;
                        }
                        else {
                            propObject.userDefinedName = evnt.target.value;
                        }
                    }
                },
                TemplateChange: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Section.propSheetCntrl.ControlProperties.getTemplateControl();
                    SectionTemplateChange(propObject);

                    $('[id$=btnChangeTemp]').unbind('click');
                    $('[id$=btnChangeTemp]').bind('click', function () {
                        ChangeSecTempClick(propObject);
                        $(currentCntrl.control).val(propObject.type.UiName);
                        var children = $('#' + propObject.id)[0].childNodes;
                        if (children != undefined && children != null && children.length > 0) {
                            $.each(children, function (index) {
                                $(this).addClass('containerDivChange');
                            });
                        }
                        reloadSectionControls(propObject);
                        $('.TbOuterCont').equalHeights();
                        return false;
                    });

                    $('[id$=btnCancelChangeTemp]').unbind('click');
                    $('[id$=btnCancelChangeTemp]').bind('click', function () {
                        SubProcTempPromt(false);
                        $(currentCntrl.control).val(propObject.type.UiName);
                        var children = $('#' + propObject.id)[0].childNodes;
                        if (children != undefined && children != null && children.length > 0) {
                            $.each(children, function (index) {
                                $(this).addClass('containerDivChange');
                            });
                        }
                        $('.TbOuterCont').equalHeights();
                        return false;
                    });
                    $(currentCntrl.control).val(propObject.type.UiName);
                }
            }
        },
        Behaviour: {
            Display: function (control, cntrlObj) {
                if (cntrlObj.conditionalDisplay) {
                    $(control).val(cntrlObj.conditionalDisplay);
                }
            },
            Conditions: function (control, cntrlObj, options) {
                var currentCntrl = options.cntrlHtmlObjects;
                if (cntrlObj.conditionalDisplay == "0")
                    currentCntrl.wrapperDiv.show();
                else
                    currentCntrl.wrapperDiv.hide();
            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl(
                         {
                             FirstDropDownOptions: json,
                             ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(),
                             IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)),
                             IsEdit: (propObject.condDisplayCntrlProps) ? true : false,
                             SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps)
                         }
                     );
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 450, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getTemplateCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.SECTION.propPluginPrefix,
                    "ControlProperties", "Template");
            }
            return objControl;
        }

        return {
            ControlProperties: {
                getTemplateControl: _getTemplateCntrl
            }
        };
    })()
};

//Select template change
function SectionTemplateChange(cntrl) {
    $('#ddlNewSectionTemp').html('<option value="1">100%</option>' +
                            '<option value="4">34%-66%</option>' +
                            '<option value="5">50%-50%</option>' +
                            '<option value="3">66%-34%</option>' +
                            '<option value="2">33%-34%-33%</option>');
    $("#ddlNewSectionTemp option[value='" + cntrl.type.id + "']").remove();
    SubProcChangeTemp(true);
}

//Change section template
function ChangeSecTempClick(cntrl) {
    var PreFix = cntrl.id + '_' + $('#ddlNewSectionTemp').val();
    var ColArray = GetInnerHtmlCol($('#ddlNewSectionTemp').val());
    var index = 0;
    var colNo = 1;

    var prentCol = $('#' + cntrl.id)[0].children.length;
    if (prentCol > ColArray[1]) {
        cntrl.fnDeleteColumnPanel($('#' + cntrl.id)[0].children[prentCol - 1].id);
        $($('#' + cntrl.id)[0].children[prentCol - 1]).remove();
        prentCol = $('#' + cntrl.id)[0].children.length;
        if (prentCol > ColArray[1]) {
            cntrl.fnDeleteColumnPanel($('#' + cntrl.id)[0].children[prentCol - 1].id);
            $($('#' + cntrl.id)[0].children[prentCol - 1]).remove();
        }
    }
    colNo = 1;
    $.each($('#' + cntrl.id)[0].children, function (e) {
        if (colNo > ColArray[1]) {
        }
        else {
            if (this.id.indexOf(cntrl.id) >= 0) {
                var columnPanel = cntrl.fnGetColumnPanel(this.id);
                columnPanel.id = PreFix + '_' + colNo;
                $(this).css('width', ColArray[0][index]);
                $(this).attr('id', PreFix + '_' + colNo);
                index = index + 1;
            }
        }
        colNo = colNo + 1;
    });

    prentCol = $('#' + cntrl.id)[0].children.length;
    AddNewColOnChangeTemp(PreFix, cntrl.id, prentCol, ColArray[1], ColArray, index, cntrl);
    prentCol = $('#' + cntrl.id)[0].children.length;
    if (prentCol == 1) {
        $('#' + PreFix + '_1').attr('class', 'TbInnerRightCont');
    }
    else if (prentCol == 2) {
        $('#' + PreFix + '_1').attr('class', 'TbInnerLeftCont');
        $('#' + PreFix + '_2').attr('class', 'TbInnerRightCont');
    }
    else if (prentCol == 3) {
        $('#' + PreFix + '_1').attr('class', 'TbInnerLeftCont');
        $('#' + PreFix + '_2').attr('class', 'TbInnerLeftCont');
        $('#' + PreFix + '_3').attr('class', 'TbInnerRightCont');
    }
    setTempType(cntrl);
    SubProcChangeTemp(false);
    SubProcTempPromt(false);
}

function setTempType(cntrl) {
    switch ($('#ddlNewSectionTemp').val()) {
        case "1":
            cntrl.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_100;
            break;
        case "2":
            cntrl.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_33_34_33;
            break;
        case "3":
            cntrl.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_66_34;
            break;
        case "4":
            cntrl.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_34_66;
            break;
        case "5":
            cntrl.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_50_50;
            break;
    }
}

//Add new column to section 
function AddNewColOnChangeTemp(PreFix, Id, PrentCol, Col, ColArray, index, cntrl) {
    if (PrentCol < Col) {
        $('#' + Id).append('<div id="' + PreFix + '_' + (parseInt(PrentCol, 10) + 1) + '" class="TbInnerRightCont" style="width: ' + ColArray[0][index] + ';"></div>'); //height : ' + $($('#' + Id)[0].childNodes['0']).height() + 'px;
        var columnPanel = new ColumnPanel((parseInt(PrentCol, 10)) + 1, PreFix + "_" + (parseInt(PrentCol, 10) + 1), cntrl.id + "_Col_" + (parseInt(PrentCol, 10) + 1), "1", [], [], false);
        cntrl.fnAddColumnPanel(columnPanel);
        if ($('[id$=hdfIsChildForm]').val() == 'false') DragItemToGroupbox(PreFix + '_' + (parseInt(PrentCol, 10) + 1));
        if ($('[id$=hdfIsChildForm]').val() == 'true') DragItemToContainer(PreFix + '_' + (parseInt(PrentCol, 10) + 1));
        DragItemToGroupbox(PreFix + '_' + (parseInt(PrentCol, 10) + 1));
        PrentCol = PrentCol + 1;
        index = index + 1;
        PrentCol = $('#' + Id)[0].children.length;
        AddNewColOnChangeTemp(PreFix, Id, PrentCol, Col, ColArray, index, cntrl);
        $('.TbOuterCont').equalHeights();
    }
}

//Get section column width
function GetInnerHtmlCol(_Temp) {
    var width = []; var col = [];
    switch (_Temp) {
        case '1':
            width.push('98%');
            col.push(1);
            break;
        case '2':
            width.push('31.5%');
            width.push('34%');
            width.push('31.5%');
            col.push(3);
            break;
        case '3':
            width.push('65%');
            width.push('32.5%');
            col.push(2);
            break;
        case '4':
            width.push('32%');
            width.push('65.5%');
            col.push(2);
            break;
        case '5':
            width.push('48.5%');
            width.push('48.5%');
            col.push(2);
            break;
    }
    return [width, col];
}

function reloadSectionControls(cntrl) {
    if (cntrl) {
        var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
        if (objForm) {
            var arrCntrls = objForm.fnGetAllControlsofSection(cntrl.id);
            if (arrCntrls && arrCntrls.length > 0) {
                $.each(arrCntrls, function (index, control) {
                    $('#outerDiv' + control.id.split('_')[1]).click();
                });
            }
        }
    }
}

mFicientIde.PropertySheetJson.Section = {
    "type": MF_IDE_CONSTANTS.CONTROLS.SECTION,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Section.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "Section",
                    "hidden": false
                }
            },
            {
                "text": "Template",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Template",
                "defltValue": "value",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Section.groups.ControlProperties.eventHandlers.TemplateChange,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "",
                    "hidden": true
                }
            }
         ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
             {
                 "text": "Display",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                 "prefixText": "Display",
                 "defltValue": "1",
                 "validations": [
                 ],
                 "customValidations": [
                 ],
                 "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Section.groups.Behaviour.eventHandlers.Display,
                       "context": "",
                       "arguments": {
                           "cntrls": [
                            {
                                grpPrefText: "Behaviour",
                                cntrlPrefText: "Conditions",
                                rtrnPropNm: "Conditions"
                            }
                         ]
                       }
                   }

                ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "selectValueBy": "",
                 "init": {
                     "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                     "disabled": false,
                     "hidden": false
                 },
                 "options": [
                   {
                       "text": "Always",
                       "value": "1"
                   },
                   {
                       "text": "Conditional",
                       "value": "0"
                   }
                ]
             },
             {
                 "text": "Conditions",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                 "prefixText": "Conditions",
                 "disabled": false,
                 "noOfLines": "0",
                 "validations": [
                 ],
                 "customValidations": [
                ],
                 "events": [
                   {
                       "name": "click",
                       "func": PROP_JSON_HTML_MAP.Section.groups.Behaviour.eventHandlers.ConditionalDisplay,
                       "context": "",
                       "arguments": {}
                   }
                ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "init": {
                     "disabled": false,
                     "defltValue": "Select Conditions",
                     "hidden": true
                 }
             }
          ]
      }
   ]
};

function RowPanel(options)/*id/*string*,userDefinedName/*string*,type/*string*,colPanels/*Array of ColumnPanel*, isDeleted)*/{
    if (!options) options = {};
    this.index = options.index;
    this.id = options.id;
    this.userDefinedName = options.userDefinedName;
    this.type = options.type;
    this.colmnPanels = options.colmnPanels;
    this.isDeleted = options.isDeleted;
    this.conditionalDisplay = options.conditionalDisplay;
    this.condDisplayCntrlProps = options.condDisplayCntrlProps;
    RowPanel.prototype.fnAddColumnPanel = function (colPanel/*ColumnPanel*/) {
        if (!this.colmnPanels || !$.isArray(this.colmnPanels)) {
            this.colmnPanels = [];
        }
        if (colPanel && colPanel instanceof ColumnPanel) {
            this.colmnPanels.push(colPanel);
        }
    }
    RowPanel.prototype.fnGetColumnPanel = function (colPanelId/*ColumnPanelId*/) {
        var objColPanel = null;
        if (this.colmnPanels != null || this.colmnPanels != undefined) {
            for (var col = 0; col <= this.colmnPanels.length - 1; col++) {
                if (this.colmnPanels[col].id == colPanelId && this.colmnPanels[col].isDeleted == false)
                    objColPanel = this.colmnPanels[col];
            }
        }
        return objColPanel;
    }

    RowPanel.prototype.fnDeleteColumnPanel = function (colPanelId/*ColumnPanelId*/) {
        var objColPanel = this.fnGetColumnPanel(colPanelId);
        objColPanel.isDeleted = true;
        if ((objColPanel.controls != null || objColPanel.controls != undefined) && objColPanel.controls.length > 0) {
            $.each(objColPanel.controls, function (cntrlIndex, control) {
                control.isDeleted = true;
            });
        }
    }

    RowPanel.prototype.fnResetObjectDetails = function () {
        var colmnPanels = this.colmnPanels;
        this.colmnPanels = [];
        var i = 0;
        if (colmnPanels && $.isArray(colmnPanels) && colmnPanels.length > 0) {
            for (i = 0; i <= colmnPanels.length - 1; i++) {
                var objColPanel = new ColumnPanel(colmnPanels[i].index, colmnPanels[i].id, colmnPanels[i].name, colmnPanels[i].display, colmnPanels[i].conditions, colmnPanels[i].controls, false);
                objColPanel.fnResetObjectDetails();
                this.fnAddColumnPanel(objColPanel);
            }
        }
    }
}
//MOHAN
RowPanel.prototype.fnGetId = function () {
    return this.id;
};

//Tanika
function ColumnPanel(index/*string*/,
                    id/*string*/,
                    name/*string*/,
                    display/*bit*/,
                    conditions/*Array of ConditionalLogic*/,
                    controls/*Array of Control*/, isDeleted) {
    this.index = index;
    this.id = id;
    this.name = name;
    this.display = display;
    this.conditions = conditions;
    this.controls = controls;
    this.isDeleted = isDeleted;

    ColumnPanel.prototype.fnAddControl = function (control/*Control*/) {
        if (!this.controls || !$.isArray(this.controls)) {
            this.controls = [];
        }
        if (control && control instanceof ControlNew) {
            this.controls.push(control);
        }
    }

    ColumnPanel.prototype.fnGetControl = function (controlId/*ControlId*/) {
        var objControl = null;
        if (this.controls != null || this.controls != undefined) {
            for (var cntrl = 0; cntrl <= this.controls.length - 1; cntrl++) {
                if (this.controls[cntrl].id == controlId)
                    objControl = this.controls[cntrl];
            }
        }
        return objControl;
    }

    ColumnPanel.prototype.fnGetAllControls = function () {
        var arrControls = [];
        if (this.controls != null || this.controls != undefined) {
            for (var cntrl = 0; cntrl <= this.controls.length - 1; cntrl++) {
                arrControls.push(this.controls[cntrl]);
            }
        }
        return arrControls;
    }

    ColumnPanel.prototype.fnResetObjectDetails = function () {
        var controls = this.controls;
        this.controls = [];
        var i = 0;
        if (controls && $.isArray(controls) && controls.length > 0) {
            for (i = 0; i <= controls.length - 1; i++) {
                var objControl = controls[i];
                switch (objControl.type.idPrefix) {
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER.idPrefix:
                        var objDateTimePicker = new DateTimePicker(objControl);
                        objDateTimePicker.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER;
                        this.fnAddControl(objDateTimePicker);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL.idPrefix:
                        var objLabel = new Label(objControl);
                        objLabel.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL;
                        objLabel.fnResetObjectDetails();
                        this.fnAddControl(objLabel);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.idPrefix:
                        var objTextbox = new Textbox(objControl);
                        objTextbox.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX;
                        objTextbox.fnResetObjectDetails();
                        this.fnAddControl(objTextbox);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.idPrefix:
                        var objDropdown = new Dropdown(objControl);
                        objDropdown.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN;
                        objDropdown.fnResetObjectDetails();
                        this.fnAddControl(objDropdown);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.idPrefix: //RadioButton
                        var objRadioButton = new RadioButton(objControl);
                        objRadioButton.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON;
                        objRadioButton.fnResetObjectDetails();
                        this.fnAddControl(objRadioButton);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.idPrefix:
                        var objCheckbox = new Checkbox(objControl);
                        objCheckbox.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX;
                        this.fnAddControl(objCheckbox);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE.idPrefix:
                        var objImage = new Image(objControl);
                        objImage.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE;
                        objImage.fnResetObjectDetails();
                        this.fnAddControl(objImage);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST.idPrefix:
                        //MOHAN
                        var objList = new List(objControl);
                        objList.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST;
                        objList.fnResetObjectDetails();
                        this.fnAddControl(objList);
                        //MOHAN
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.idPrefix:
                        var objEditableList = new EditableList(objControl);
                        objEditableList.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST;
                        objEditableList.fnResetObjectDetails();
                        this.fnAddControl(objEditableList);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN.idPrefix:
                        var objHiddenField = new HiddenField(objControl);
                        objHiddenField.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN;
                        objHiddenField.fnResetObjectDetails();
                        this.fnAddControl(objHiddenField);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE.idPrefix:
                        var objToggle = new Toggle(objControl);
                        objToggle.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE;
                        objToggle.fnResetObjectDetails();
                        this.fnAddControl(objToggle);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER.idPrefix:
                        var objSlider = new Slider(objControl);
                        objSlider.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER;
                        this.fnAddControl(objSlider);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SEPARATOR.idPrefix:
                        var objSeparator = new Separator(objControl);
                        objSeparator.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SEPARATOR;
                        this.fnAddControl(objSeparator);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARCODE.idPrefix:
                        var objBarcode = new Barcode(objControl);
                        objBarcode.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARCODE;
                        this.fnAddControl(objBarcode);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION.idPrefix:
                        var objLocation = new Location(objControl);
                        objLocation.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION;
                        objLocation.fnResetObjectDetails();
                        this.fnAddControl(objLocation);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART.idPrefix:
                        var objPieChart = new PieChart(objControl);
                        objPieChart.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART;
                        objPieChart.fnResetObjectDetails();
                        this.fnAddControl(objPieChart);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH.idPrefix:
                        var objBarGraph = new BarGraph(objControl);
                        objBarGraph.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH;
                        objBarGraph.fnResetObjectDetails();
                        this.fnAddControl(objBarGraph);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE.idPrefix:
                        var objAngularGuage = new AngularGuage(objControl);
                        objAngularGuage.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE;
                        objAngularGuage.fnResetObjectDetails();
                        this.fnAddControl(objAngularGuage);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE.idPrefix:
                        var objCylinderGuage = new CylinderGuage(objControl);
                        objCylinderGuage.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE;
                        objCylinderGuage.fnResetObjectDetails();
                        this.fnAddControl(objCylinderGuage);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH.idPrefix:
                        var objLineGraph = new LineGraph(objControl);
                        objLineGraph.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH;
                        objLineGraph.fnResetObjectDetails();
                        this.fnAddControl(objLineGraph);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE.idPrefix:
                        //MOHAN
                        var objTable = new Table(objControl);
                        objTable.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE;
                        objTable.fnResetObjectDetails();
                        this.fnAddControl(objTable);
                        //MOHAN
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SPACER.idPrefix:
                        var objSpacer = new Spacer(objControl);
                        objSpacer.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SPACER;
                        this.fnAddControl(objSpacer);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PICTURE.idPrefix:
                        var objPicture = new Picture(objControl);
                        objPicture.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PICTURE;
                        this.fnAddControl(objPicture);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SIGNATURE.idPrefix:
                        var objSignature = new Signature(objControl);
                        objSignature.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SIGNATURE;
                        this.fnAddControl(objSignature);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART.idPrefix:
                        var objDrilldownPie = new DrilldownPieChart(objControl);
                        objDrilldownPie.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART;
                        objDrilldownPie.fnResetObjectDetails();
                        this.fnAddControl(objDrilldownPie);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BUTTON.idPrefix:
                        var objButton = new Button(objControl);
                        objButton.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BUTTON;
                        this.fnAddControl(objButton);
                        break;
                    case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CUSTOM_CONTROL.idPrefix:
                        var objCustomControl = new CustomControl(objControl);
                        objCustomControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CUSTOM_CONTROL;
                        this.fnAddControl(objCustomControl);
                        break;
                }
            }
        }
    };
}
//MOHAN
ColumnPanel.prototype.fnGetId = function () {
    return this.id;
};
RowPanel.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Section;

/*-------------------------------------------------Javascript Editor------------------------------------------------*/
//var _jsAceEditor = (function () {
//    function _setTextInEditor(text, editorLineNo, editorColNo, editorContLines, editorContLength, editor) {
//        if (mfUtil.isNullOrUndefined(text)) {
//            throw mfExceptionMsgs.argumentsNull;
//        }
//        _removeChangeEvent(editor);
//        _removeCursorChangeEvent(editor);
//        editor.setValue(text);
//        _setEditorContentDetails(editor, editorContLines, editorContLength);
//        _setEditorCursorDetails(editor, editorLineNo, editorColNo);
//        _setOnCursorChangeEvent(editor, editorContLines, editorContLength);
//        _setOnChangeEvent(editor, editorContLines, editorContLength);
//        editor.getSession().setValue(text);
//    }
//    function _setEditorContentDetails(editor, editorContLines, editorContLength) {
//        var strValue = editor.getValue();
//        var noOfLines = editor.session.getLength();
//        $('#' + editorContLines).text(noOfLines);
//        $('#' + editorContLength).text(strValue.length);
//    }
//    function _removeChangeEvent(editor) {
//        editor.getSession().removeListener('change');
//    }
//    function _setOnChangeEvent(editor, editorContLines, editorContLength) {
//        editor.getSession().on('change', _setEditorContentDetails(editor, editorContLines, editorContLength));
//    }
//    function _removeCursorChangeEvent(editor) {
//        editor.getSession().removeListener('changeCursor');
//    }
//    function _setOnCursorChangeEvent(editor, editorContLines, editorContLength) {
//        editor.getSession().on('changeCursor', _setEditorContentDetails(editor, editorContLines, editorContLength));
//    }
//    function _setEditorCursorDetails(editor, editorLineNo, editorColNo) {
//        var cursorPosition = editor.selection.getCursor();
//        var row = 0, column = 0;
//        if (cursorPosition) {
//            row = (cursorPosition.row) + 1;
//            column = cursorPosition.column;
//        }
//        $('#' + editorLineNo).text(row);
//        $('#' + editorColNo).text(column);
//    }
//    function _getText(editor) {
//        return editor.getSession().getValue();
//    }
//    return {
//        showHideContainer: function (blnShow, contDiv) {
//            if (blnShow === true) {
//                $('#' + contDiv).show();
//            }
//            else {
//                $('#' + contDiv).show();
//            }
//        },
//        setTextInEditor: function (text, editorLineNo, editorColNo, editorContLines, editorContLength, editor) {
//            _setTextInEditor(text, editorLineNo, editorColNo, editorContLines, editorContLength, editor);
//        },
//        changeEventHandler: function (editor, editorContLines, editorContLength) {
//            _setEditorContentDetails(editor, editorContLines, editorContLength);
//        },
//        setOnChangeEvent: function (editor, editorContLines, editorContLength) {
//            _setOnChangeEvent(editor, editorContLines, editorContLength);
//        },
//        removeOnChangeEvent: function (editor) {
//            _removeChangeEvent(editor);
//        },
//        changeCursorEventHandler: function (editor, editorLineNo, editorColNo) {
//            _setEditorCursorDetails(editor, editorLineNo, editorColNo);
//        },
//        executeFindAndReplace: function (editor) {
//            editor.execCommand('replace');
//        },
//        getText: function (editor) {
//            return _getText(editor);
//        },
//        getTextForEditorFromHidField: function () {
//            var strFileContent =
//                        _hidFieldsHelper.hidSelectedFileContent.getValue();
//            var objFileContent = null;
//            if (!mfUtil.isNullOrUndefined(strFileContent) &&
//                       !mfUtil.isEmptyString(strFileContent)) {

//                objFileContent = $.parseJSON(strFileContent);
//                if (objFileContent) {
//                    strFileContent = objFileContent.content;
//                }
//            }
//            else {
//                strFileContent = "";
//            }
//            return strFileContent;
//        },
//        setTextFromEditorInHidField: function (editor) {
//            var fileContent = _getText(editor);
//            var stringifiedText = "";
//            if (!mfUtil.isNullOrUndefined(fileContent)) {
//                //fileContent = _.escape(fileContent)
//                stringifiedText = JSON.stringify({
//                    content: fileContent
//                });
//            }
//            stringifiedText = $('<div/>').text(stringifiedText).html();
//            _hidFieldsHelper.hidSelectedFileContent.setValue(
//                         stringifiedText);
//        }
//    };
//})();

var _jsAceEditor = (function () {
    function _getAceEditorInstance() {
        return jsaceEditor;
    }
    function _setTextInEditor(text) {
        if (mfUtil.isNullOrUndefined(text)) {
            throw mfExceptionMsgs.argumentsNull;
        }
        var editor = _getAceEditorInstance();
        _removeChangeEvent();
        _removeCursorChangeEvent();
        editor.setValue(text,-1);
        _setEditorContentDetails();
        _setEditorCursorDetails();
        _setOnCursorChangeEvent();
        _setOnChangeEvent();
        //editor.getSession().setValue(text);
    }
    function _setEditorContentDetails() {
        var editor = _getAceEditorInstance();
        var strValue = editor.getValue();
        var noOfLines = editor.session.getLength();
        $('#spanAceEditorContentLines').text(noOfLines);
        $('#spanAceEditorContentLength').text(strValue.length);
    }
    function _removeChangeEvent() {
        var editor = _getAceEditorInstance();
        editor.getSession().removeListener('change');
    }
    function _setOnChangeEvent() {
        var editor = _getAceEditorInstance();
        editor.getSession().on('change', _setEditorContentDetails);
    }
    function _removeCursorChangeEvent() {
        var editor = _getAceEditorInstance();
        editor.getSession().removeListener('changeCursor');
    }
    function _setOnCursorChangeEvent() {
        var editor = _getAceEditorInstance();
        editor.getSession().on('changeCursor', _setEditorContentDetails);
    }
    function _setEditorCursorDetails() {
        var editor = _getAceEditorInstance();
        var cursorPosition = editor.selection.getCursor();
        var row = 0, column = 0;
        if (cursorPosition) {
            row = (cursorPosition.row) + 1;
            column = cursorPosition.column;
        }
        $('#spanAceEditorLineNo').text(row);
        $('#spanAceEditorColumnNo').text(column);
    }
    function _getText() {
        var editor = _getAceEditorInstance();
        return editor.getSession().getValue();
    }
    return {
        setTextInEditor: function (text) {
            _setTextInEditor(text);
        },
        changeEventHandler: function () {
            _setEditorContentDetails();
        },
        setOnChangeEvent: function () {
            _setOnChangeEvent();
        },
        removeOnChangeEvent: function () {
            _removeChangeEvent();
        },
        changeCursorEventHandler: function () {
            _setEditorCursorDetails();
        },
        getText: function () {
            return _getText();
        }
    };
})();

function SubProcShowJsEditor(_bol, _Title) {
    if (_bol) {
        $('#divFormEditorsEvents').css("display", "block");
        $('#divCntrlEditorsEvents').css("display", "none");
        showModalPopUpWithOutHeader('SubProcAceEditor', _Title, '99%', false);
        $('#popup_custom_buttom_save').remove();
        $('#popup_custom_buttom_close').remove();
        var toolbar = ($('#SubProcAceEditor').parent()).find(".ui-dialog-titlebar");
        toolbar.css("padding", "0px");
        toolbar.find(".ui-dialog-title").css("margin", "7px 0px 0px 5px");
        toolbar.append('<button style="float:right" title="Save" role="button" type="button" id="popup_custom_buttom_close" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >Close</button>');
        toolbar.append('<button style="float:right" title="Save" role="button" type="button" id="popup_custom_buttom_save" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >Save</button>');
    }
    else
        $('#SubProcAceEditor').dialog('close');
}


function SubProcShowCntrlJsEditor(_bol, _Title) {
    if (_bol) {
        $('#divFormEditorsEvents').css("display", "none");
        $('#divCntrlEditorsEvents').css("display", "block");
        showModalPopUpWithOutHeader('SubProcAceEditor', _Title, '99%', false);
        $('#popup_custom_buttom_save').remove();
        $('#popup_custom_buttom_close').remove();
        var toolbar = ($('#SubProcAceEditor').parent()).find(".ui-dialog-titlebar");
        toolbar.css("padding", "0px");
        toolbar.find(".ui-dialog-title").css("margin", "7px 0px 0px 5px");
        toolbar.append('<button style="float:right" title="Save" role="button" type="button" id="popup_custom_buttom_close" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >Close</button>');
        toolbar.append('<button style="float:right" title="Save" role="button" type="button" id="popup_custom_buttom_save" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >Save</button>');
//        $('#divFormEditorsEvents').css("display", "none");
//        $('#divCntrlEditorsEvents').css("display", "block");
//        showModalPopUpWithOutHeader('SubProcAceEditor', _Title, '99%', false);
//        $('#popup_custom_buttom_save').remove();
//        $('#popup_custom_buttom_close').remove();
////        $('#popup_ctrl_custom_buttom_save').remove();
////        $('#popup_ctrl_custom_buttom_close').remove();
//        var toolbar = ($('#SubProcCntrlAceEditor').parent()).find(".ui-dialog-titlebar");
//        toolbar.css("padding", "0px");
//        toolbar.find(".ui-dialog-title").css("margin", "7px 0px 0px 5px");
//        toolbar.append('<button style="float:right" title="Save" role="button" type="button" id="popup_custom_buttom_close" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >Close</button>');
//        toolbar.append('<button style="float:right" title="Save" role="button" type="button" id="popup_custom_buttom_save" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" >Save</button>');
    }
    else
        $('#SubProcAceEditor').dialog('close');
}

function SaveAceScriptForForm(propObject, scriptCommonFunc, scriptViewAppear, scriptViewReAppear,
            scriptViewDisAppear, scriptSubmitBtn, scriptBackBtn, scriptCancelBtn, scriptActionSheetBtn, _jsAceEditor) {
    var script = _jsAceEditor.getText();
    switch ($('#drpFormEvents')[0].value) {
        case "0":
            if (script != null && script != undefined) {
                scriptCommonFunc = Base64.encode(script);
            }
            break;
        case "1":
            if (script != null && script != undefined) {
                scriptViewAppear = Base64.encode(script);
            }
            break;
        case "2":
            if (script != null && script != undefined) {
                scriptViewReAppear = Base64.encode(script);
            }
            break;
        case "3":
            if (script != null && script != undefined) {
                scriptViewDisAppear = Base64.encode(script);
            }
            break;
        case "4":
            if (script != null && script != undefined) {
                scriptSubmitBtn = Base64.encode(script);
            }
            break;
        case "5":
            if (script != null && script != undefined) {
                scriptBackBtn = Base64.encode(script);
            }
            break;
        case "6":
            if (script != null && script != undefined) {
                scriptCancelBtn = Base64.encode(script);
            }
            break;
        case "7":
            if (script != null && script != undefined) {
                scriptActionSheetBtn = Base64.encode(script);
            }
    }

    if (scriptViewAppear != null && scriptViewAppear != undefined) propObject.viewAppear = scriptViewAppear;
    if (scriptViewReAppear != null && scriptViewReAppear != undefined) propObject.viewReAppear = scriptViewReAppear;
    if (scriptViewDisAppear != null && scriptViewDisAppear != undefined) propObject.viewDisAppear = scriptViewDisAppear;
    if (scriptCommonFunc != null && scriptCommonFunc != undefined) propObject.commonFunc = scriptCommonFunc;
    if (scriptSubmitBtn != null && scriptSubmitBtn != undefined) propObject.submitBtn = scriptSubmitBtn;
    if (scriptBackBtn != null && scriptBackBtn != undefined) propObject.backBtn = scriptBackBtn;
    if (scriptCancelBtn != null && scriptCancelBtn != undefined) propObject.cancelBtn = scriptCancelBtn;
    if (scriptActionSheetBtn != null && scriptActionSheetBtn != undefined) propObject.actionSheetBtn = scriptActionSheetBtn;
}

//function resetColumns(cntrl, prentCol) {
//    var col = cntrl.fnGetColumnPanel($('#' + cntrl.id)[0].children[prentCol - 1].id);
//    var colControls = col.fnGetAllControls();

//    var len = prentCol - 1;
//    var count = 0;
//    var newCol = cntrl.fnGetColumnPanel($('#' + cntrl.id)[0].children[len - 1].id);
//    if (colControls != null && colControls != undefined && colControls.length > 0) {
//        $.each(colControls, function () {
//            //var count = this.id.split('_')[1];
//            //strControlHtml = '<div id="outerDiv' + count + '"  class="outerDiv">' + $('#outerDiv' + count)[0].innerHTML + '</div>';
//            //$('#' + newCol.id).append(strControlHtml);
//            //eventOfOuterDiv('#outerDiv' + count, objForm);
//            //ControlDivMouseOverAndOut('outerDiv' + count);
//            //this.isDeleted = false;
//            //newCol.fnAddControl(this);
//        });
//    }

//    $('#outerDiv' + count).click();
//}

var mfIdeForm = "mfIdeForm";
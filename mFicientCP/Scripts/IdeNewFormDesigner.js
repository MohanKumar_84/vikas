﻿
var defaultImage = new Image();
defaultImage.src = "//enterprise.mficient.com/images/DefaultImage .png";
var form = new Form();
var app = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();

function initialize() {
    form = new Form();
    app = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
    $('[id$=hdfEditableListId]').val('');
    count = 1;
}

function InitializeFormDesigner() {
    OnToolboxControlMouseOver();
    $("#ControlsContainer .parentDivDraggable").draggable({
        appendTo: "body",
        helper: "clone",
        refreshPositions: true,
        start: createDrops,
        containment: '.mainContainerDiv',
        cursorAt: { left: 0, top: 0 }

    });
    $("#ControlsContainer .parentDivDraggable").draggable("option", "scroll", true);
    IsLocCtrlExist = false;
    IsRptCtrlExist = false;
    IsEltCtrlExist = false;
    IsTblCtrlExist = false;

    ApplicationInitNewView();
}

function OnToolboxControlMouseOver() {
    ToolboxMouseEvent('draggableLabelDiv');
    ToolboxMouseEvent('draggableTextboxDiv');
    ToolboxMouseEvent('draggableSubmitButtonDiv');
    ToolboxMouseEvent('draggableTextareaDiv');
    ToolboxMouseEvent('draggableDropdownDiv');
    ToolboxMouseEvent('draggableSelectorListDiv');
    ToolboxMouseEvent('draggableRadioButtonDiv');
    ToolboxMouseEvent('draggableCheckboxDiv');
    ToolboxMouseEvent('draggableImageDiv');
    ToolboxMouseEvent('draggableRepeaterDiv');
    ToolboxMouseEvent('draggableEditableListDiv');
    ToolboxMouseEvent('draggableHiddenFieldDiv');
    ToolboxMouseEvent('draggableDatetimePickerDiv');
    ToolboxMouseEvent('TableDiv');
    ToolboxMouseEvent('BarGraphDiv');
    ToolboxMouseEvent('PieChartDiv');
    ToolboxMouseEvent('DrilldownPieChartDiv');
    ToolboxMouseEvent('DataPointChartDiv');
    ToolboxMouseEvent('LineChartDiv');
    ToolboxMouseEvent('AngularGuageDiv');
    ToolboxMouseEvent('CylinderGuageDiv');
    ToolboxMouseEvent('draggableToggleDiv');
    ToolboxMouseEvent('draggableHyperlinkDiv');
    ToolboxMouseEvent('draggableSliderDiv');
    ToolboxMouseEvent('draggableSeparatorDiv');
    ToolboxMouseEvent('draggableBarcodeDiv');
    ToolboxMouseEvent('draggableLocationDiv');
    ToolboxMouseEvent('SpacerDiv');
    ToolboxMouseEvent('PictureDiv');
    ToolboxMouseEvent('SignatureDiv');
    FormMouseEvent('FromSaveDiv');
    FormMouseEvent('FromSaveAsDiv');
    FormMouseEvent('FormDeleteDiv');
    FormMouseEvent('FormCloseDiv');
    ToolboxGroupMouseOver('draggableGroupboxDiv');
}

function ToolboxMouseEvent(_Id) {
    $('#' + _Id).bind('mouseover', function () {
        ToolboxMouseOver('#' + this.id);
    });
    $('#' + _Id).bind('mouseout', function () {
        ToolboxMouseOut('#' + this.id);
    });
}

//Form mouse over event
function FormMouseEvent(_Id) {
    $('#' + _Id).bind('mouseover', function () {
    });
    $('#' + _Id).bind('mouseout', function () {
    });
}

//Toolbox mouse event
function ToolboxMouseOver(Id) {
    $(Id).addClass('draggableElementMouseOver');
    $(Id).removeClass('draggableElement');
}
//Toolbox mouse event
function ToolboxMouseOut(Id) {
    $(Id).removeClass('draggableElementMouseOver');
    $(Id).addClass('draggableElement');
}

//Toolbox mouse event
function ToolboxGroupMouseOver(_Id) {
    $('#' + _Id).bind('mouseover', function () {
        $(this).addClass('draggableGroupboxMouseOver');
        $(this).removeClass('draggableGroupboxDiv');
    });
    $('#' + _Id).bind('mouseout', function () {
        $(this).removeClass('draggableGroupboxMouseOver');
        $(this).addClass('draggableGroupboxDiv');
    });
}


function createDrops(ev, ui) {
    var strInnerHtml = '';
    var strOutDiv = '<div id="IMG_DRAGDiv" style="background-color: #F4F3F3;">';
    var strId = '';
    try {
        strId = $($(ui)[0].helper[0].childNodes).find('img')[0].id;
    }
    catch (err)
    { strId = ''; }
    switch (strId) {
        case "Groupbox":
            strInnerHtml = '<div id="IMG_DRAG" style="background-color: #F4F3F3;padding:8px 80px 8px 10px;border: dashed 1px #CCC;">' +
                '<div>' +
                    '<a style="color: black; font-size: 10px; font-weight: bold;"> Section </a>' +
                '</div>' +
            '</div>';
            break;
        case "Label":
            strInnerHtml = '<div id="IMG_DRAG" style="background-color: #F4F3F3;padding:8px 80px 8px 10px;border: solid 1px #CCC;">' +
                '<div>' +
                    '<a style="color: black; font-size: 10px; font-weight: bold;"> LabelText </a>' +
                '</div>' +
            '</div>';
            break;
        case "changeText":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="Textbox" src="//enterprise.mficient.com/images/ImgTextbox.png" class="IMG_DRAG"/>' + '</div>';
            break;
        case "Dropdown":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="DropdownList" src="//enterprise.mficient.com/images/ImgDropdown.png" class="IMG_DRAG"/>' + '</div>';
            break;
        case "RadioButton":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="RadioButton" src="//enterprise.mficient.com/images/ImgRadioButton.png" class="IMG_DRAG"/>' + '</div>';
            break;
        case "Checkbox":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="Checkbox" src="//enterprise.mficient.com/images/ImgCheckbox.png" class="IMG_DRAG"/>' + '</div>';
            break;
        case "Image":
            strInnerHtml = '<img id="IMG_DRAG" alt="Image" src="//enterprise.mficient.com/images/DefaultImage .png" class="IMG_DRAG_IMG"/>';
            break;
        case "Repeater":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="List" src="//enterprise.mficient.com/images/ImgList.png" class="IMG_DRAG"/>' + '</div>';
            break;
        case "EditableList":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="EditableList" src="//enterprise.mficient.com/images/ImgList.png" class="IMG_DRAG"/>' + '</div>';
            break;
        case "HiddenField":
            strInnerHtml = '<div id="IMG_DRAG" style="background-color: #F4F3F3;padding:5px;border: solid 1px #CCC;">' +
                '<div>' +
                    '<a style="color: black; font-size: 10px; font-weight: bold;">HiddenVariable</a><br /><a style="font-style: italic; font-size: 10px;">(This field will not be visible on screen<br /> at runtime.)</a>' +
                '</div>' +
            '</div>';
            break;
        case "DatetimePicker":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="DatetimePicker" src="//enterprise.mficient.com/images/ImgDatetime.png" class="IMG_DRAG"/>' + '</div>';
            break;
        case "Toggle":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="Toggle" src="//enterprise.mficient.com/images/ImgToggle.png" class="IMG_DRAG"/>' + '</div>';
            break;
        case "Hyperlink":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="bargarph" src="//enterprise.mficient.com/images/ImgHyperLink.png" class="IMG_DRAG">' + '</div>';
            break;
        case "Slider":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="Slider" src="//enterprise.mficient.com/images/ImgScroll.png" class="IMG_DRAG"/>' + '</div>';
            break;
        case "Separator":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="Separator" src="//enterprise.mficient.com/images/ImgDivider.png" class="IMG_DRAG"/>' + '</div>';
            break;
        case "BarcodeReader":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="BarcodeReader" src="./images/img_barcode.png" class="IMG_DRAG"/>' + '</div>';
            break;
        case "Location":
            strInnerHtml = '<img id="IMG_DRAG" alt="Location" src="//enterprise.mficient.com/images/Location.png" class="IMG_DRAG"/>';
            break;
        case "PieChart":
            strInnerHtml = '<img id="IMG_DRAG" alt="PieChart" src="//enterprise.mficient.com/images/piechart.png" class="IMG_DRAG_PIE"/>';
            break;
        case "DrilldownPieChart":
            strInnerHtml = '<img id="IMG_DRAG" alt="PieChart (Drilldown)" src="//enterprise.mficient.com/images/drilldownpiechart.png" class="IMG_DRAG_PIE"/>';
            break;
        case "BarGraph":
            strInnerHtml = '<img id="IMG_DRAG" alt="BarGraph" src="//enterprise.mficient.com/images/bargarph.jpg" class="IMG_DRAG_GRAPH"/>';
            break;
        case "LineChart":
            strInnerHtml = '<img id="IMG_DRAG" alt="LineChart" src="//enterprise.mficient.com/images/lineChart.jpg" class="IMG_DRAG_GRAPH"/>';
            break;
        case "AngularGuage":
            strInnerHtml = '<img id="IMG_DRAG" alt="AngularGuage" src="../images/AngularGaugeImage.png" class="IMG_DRAG_GRAPH"/>';
            break;
        case "CylinderGuage":
            strInnerHtml = '<img id="IMG_DRAG" alt="CylinderGuage" src="../images/imageCylinderGauge.png" class="IMG_DRAG_GRAPH"/>';
            break;
        case "DataPointChart":
            strInnerHtml = '<img id="IMG_DRAG" alt="DataPointChart" src="../images/imageCylinderGauge.png" class="IMG_DRAG_GRAPH"/>';
            break;
        case "Table":
            strInnerHtml = '<img id="IMG_DRAG" alt="Table" src="//enterprise.mficient.com/images/Table.jpg" class="IMG_DRAG_TBL"/>';
            break;
        case "Spacer":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="Spacer" src="//enterprise.mficient.com/images/Spacer.png" style="border:1px solid;" class="IMG_DRAG_SPACER"/>' + '</div>';
            break;
        case "Picture":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="Picture" src="//enterprise.mficient.com/images/Picture.png" class="IMG_DRAG_PICTURE"/>' + '</div>';
            break;
        case "Signature":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="Signature" src="//enterprise.mficient.com/images/Scribble.png" class="IMG_DRAG_PICTURE"/>' + '</div>';
            break;
        case "Button":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="Button" src="//enterprise.mficient.com/images/ImgButton.png" />' + '</div>';
            break;
        case "CustomControl":
            strInnerHtml = strOutDiv + '<img id="IMG_DRAG" alt="CustomControl" src="//enterprise.mficient.com/images/imgCustom.png" />' + '</div>';
            break;
    }
    if (strInnerHtml.length > 0) {
        $(ui.helper[0]).html(strInnerHtml);
        var Img = $('#IMG_DRAG');
        var ImgDiv = $('#IMG_DRAGDiv');
        var _Parent = '';
        if (ImgDiv.length > 0) {
            _Parent = ImgDiv.parent();
        }
        else {
            _Parent = Img.parent();
        }

        $(_Parent).css('backgroundColor', 'transparent');
        $(_Parent).css('border', 'none');
        $(_Parent).removeClass('draggableElementMouseOver');
        $(_Parent).removeAttr('title');
    }

}
//Get html for table section
function GetInnerHtmlOfGroupBox(_Temp, _PreFix) {
    var html = '';
    switch (_Temp) {
        case '1':
            html = '<div id="' + _PreFix + '_1' + '" class="TbInnerRightCont" style="width: 98%;margin-left: 7px;"></div>';
            break;
        case '2':
            html = '<div id="' + _PreFix + '_1' + '" class="TbInnerLeftCont" style="width: 31.5%;margin-left: 7px;"></div>' +
                   '<div id="' + _PreFix + '_2' + '" class="TbInnerLeftCont" style="width: 34%;"></div>' +
                   '<div id="' + _PreFix + '_3' + '" class="TbInnerRightCont" style="width: 31.5%;"></div>';
            break;
        case '3':
            html = '<div id="' + _PreFix + '_1' + '" class="TbInnerLeftCont" style="width: 65%;margin-left: 7px;"></div>' +
                   '<div id="' + _PreFix + '_2' + '" class="TbInnerRightCont" style="width: 32.5%;"></div>';
            break;
        case '4':
            html = '<div id="' + _PreFix + '_1' + '" class="TbInnerLeftCont" style="width: 32%;margin-left: 7px;"></div>' +
                   '<div id="' + _PreFix + '_2' + '" class="TbInnerRightCont" style="width: 65.5%;"></div>';
            break;
        case '5':
            html = '<div id="' + _PreFix + '_1' + '" class="TbInnerLeftCont" style="width: 48.5%;margin-left: 7px;"></div>' +
                   '<div id="' + _PreFix + '_2' + '" class="TbInnerRightCont" style="width: 48.5%;"></div>';
            break;
    }
    return html;
}

function formEdit(editableForm) {
    form = editableForm;
    form.isNew = false;
    $('[id$=hdfCurrentFormId]').val(form.id);
    $('[id$=hdfIsChildForm]').val('false');
    InitializeFormDesigner();
    $('#FrmInnerPropertieDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Form, editObject: form });
    if (form.isTablet) {
        $('#TBmainFormbox').show();
        $('#mainFormbox').hide();
        createTabletLayout();
    }
    else {
        $('#TBmainFormbox').hide();
        $('#mainFormbox').show();
        createPhoneLayout();
    }
}

function createPhoneLayout() {
    var strHtml = '';
    var arryIndex = [];
    var hgt = 0;
    var newCount = 0;
    $.each(form.rowPanels, function (rowIndex, objRowPanel) {
        strHtml = '';
        if (objRowPanel.isDeleted == false && objRowPanel.id != "Sec_hd") {
            if ((objRowPanel.colmnPanels != null || objRowPanel.colmnPanels != undefined) && objRowPanel.colmnPanels.length > 0) {
                strHtml += '<div id="' + objRowPanel.id + '" class="containerOuterDiv">'
                + '<div>'
                + '<img id="imgSettings_' + objRowPanel.index + '" src="//enterprise.mficient.com/images/settings.png" alt="move" style="cursor:pointer;" />'
                + '<img id="imgGpbDelete_' + objRowPanel.index + '"  alt="delete" src="//enterprise.mficient.com/images/cross.png" title="Delete Control" style="cursor:pointer;" />'
                + '</div>';
                count = parseInt(objRowPanel.index);
                $.each(objRowPanel.colmnPanels, function (colIndex, objColPanel) {
                    if (objColPanel.isDeleted == false) {
                        strHtml += '<div id="' + objColPanel.id + '"  class="containerDiv"  style="width: 98%;"></div></div>';
                        $('#FormDesignContainerDiv').append(strHtml);
                        if ((objColPanel.controls != null || objColPanel.controls != undefined) && objColPanel.controls.length > 0) {
                            $.each(objColPanel.controls, function (cntrlIndex, objControl) {
                                if (objControl.isDeleted == false) {
                                    var strControlHtml = getHtml(objControl.type.type, objControl.id.split('_')[1]);
                                    if (objControl.type == mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST)
                                        $('[id$=hdfEditableListId]').val(objControl.id);
                                    $('#' + objColPanel.id).append(strControlHtml);
                                    newCount = parseInt(objControl.id.split('_')[1]);
                                    arryIndex.push(newCount);
                                    if (count < newCount)
                                        count = newCount;
                                    eventOfOuterDiv('#outerDiv' + newCount);
                                    ControlDivMouseOverAndOut('outerDiv' + newCount);
                                    mFicientIde.MfApp.form.CntrlCopyPaste.contextMenu.setEvent($('#outerDiv' + newCount));
                                    $('#outerDiv' + newCount).click();
                                }
                            });
                            //                            if (document.getElementById(objColPanel.id) != undefined) {
                            //                                hgt = window.getComputedStyle(document.getElementById(objColPanel.id)).height.replace("px", "");
                            //                                $('#' + objColPanel.id).css('min-height', parseInt(hgt, 10) + 50 + 'px');
                            //                            }
                        }
                        DragItemToGroupbox(objColPanel.id);
                    }
                });

                SettingButtonClick(objRowPanel.index);
                DeleteGroupbox(objRowPanel.index);
            }
        }
    });
    $('#outerDiv' + newCount).click();
    DragGbToContainer('FormDesignContainerDiv');
    $('#FrmInnerPropertieDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Form, editObject: form });
    if (arryIndex.length > 0) count = Math.max.apply(Math, arryIndex) + 1;
    else count = 1;
}

function createTabletLayout() {
    var strHtml = '';
    var hgt = 0;
    var arryIndex = [];
    var newCount = 0;
    $.each(form.rowPanels, function (rowIndex, objRowPanel) {
        strHtml = '';
        if (objRowPanel.isDeleted == false && (objRowPanel.colmnPanels != null || objRowPanel.colmnPanels != undefined) && objRowPanel.colmnPanels.length > 0 && objRowPanel.id != "Sec_hd") {
            strHtml += '<div id="TBGB_' + objRowPanel.index + '" class="TBcontainerOuterDiv"><div>'
                        + '<img id="imgSettings_' + objRowPanel.index + '" src="//enterprise.mficient.com/images/settings.png" alt="move" style="cursor:pointer;" />'
                        + '<img id="imgGpbDelete_' + objRowPanel.index + '"  alt="delete" src="//enterprise.mficient.com/images/cross.png" title="Delete Control" style="cursor:pointer;" />'
                        + '</div><div id="' + objRowPanel.id + '"  class="TbOuterCont"></div>'
                        + '<div class="clear"></div></div>';
            $('#TbFormDesignContainerDiv').append(strHtml);
            count = parseInt(objRowPanel.index);
            $.each(objRowPanel.colmnPanels, function (colIndex, objColPanel) {
                if (objColPanel.isDeleted == false) {
                    $('#' + objRowPanel.id).append(GetInnerHtmlOfGroupColumns(objColPanel.id, objColPanel.index, objRowPanel.type.id));
                    if ((objColPanel.controls != null || objColPanel.controls != undefined) && objColPanel.controls.length > 0) {
                        $.each(objColPanel.controls, function (cntrlIndex, objControl) {
                            if (objControl.isDeleted == false) {
                                var strControlHtml = getHtml(objControl.type.type, objControl.id.split('_')[1]);
                                if (objControl.type == mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST)
                                    $('[id$=hdfEditableListId]').val(objControl.id);
                                $('#' + objColPanel.id).append(strControlHtml);
                                newCount = parseInt(objControl.id.split('_')[1]);
                                arryIndex.push(newCount);
                                if (count < newCount)
                                    count = newCount;

                                eventOfOuterDiv('#outerDiv' + newCount);
                                ControlDivMouseOverAndOut('outerDiv' + newCount);
                                mFicientIde.MfApp.form.CntrlCopyPaste.contextMenu.setEvent($('#outerDiv' + newCount));
                                $('#outerDiv' + newCount).click();

                            }
                        });
                        //                        if (document.getElementById(objColPanel.id) != undefined) {
                        //                            hgt = window.getComputedStyle(document.getElementById(objColPanel.id)).height.replace("px", "");
                        //                            $('#' + objColPanel.id).css('min-height', parseInt(hgt, 10) + 50 + 'px');
                        //                        }
                    }
                    DragItemToGroupbox(objColPanel.id);
                }
            });

            SettingButtonClick(objRowPanel.index);
            DeleteGroupbox(objRowPanel.index);
        }
    });
    $('#outerDiv' + newCount).click();
    DragGbToContainer('TbFormDesignContainerDiv');
    $('#FrmInnerPropertieDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Form, editObject: form });
    if (arryIndex.length > 0) count = Math.max.apply(Math, arryIndex) + 1;
    else count = 1;
}

//Get html for table section
function GetInnerHtmlOfGroupColumns(_id, _index, _tempType) {
    var html = '';
    switch (_tempType) {
        case '1':
            html = '<div id="' + _id + '" class="TbInnerRightCont" style="width: 98%;margin-left: 7px;"></div>';
            break;
        case '2':
            if (_index == '1') {
                html = '<div id="' + _id + '" class="TbInnerLeftCont" style="width: 31.5%;margin-left: 7px;"></div>';
            }
            else if (_index == '2') {
                html = '<div id="' + _id + '" class="TbInnerLeftCont" style="width: 34%;"></div>';
            }
            else if (_index == '3') {
                html = '<div id="' + _id + '" class="TbInnerRightCont" style="width: 31.5%;"></div>';
            }
            break;
        case '3':
            if (_index == '1') {
                html = '<div id="' + _id + '" class="TbInnerLeftCont" style="width: 65%;margin-left: 7px;"></div>';
            }
            else if (_index == '2') {
                html = '<div id="' + _id + '" class="TbInnerRightCont" style="width: 32.5%;"></div>';

            }
            break;
        case '4':
            if (_index == '1') {
                html = '<div id="' + _id + '" class="TbInnerLeftCont" style="width: 32%;margin-left: 7px;"></div>';
            }
            else if (_index == '2') {
                html = '<div id="' + _id + '" class="TbInnerRightCont" style="width: 65.5%;"></div>';

            }
            break;
        case '5':
            if (_index == '1') {
                html = '<div id="' + _id + '" class="TbInnerLeftCont" style="width: 48.5%;margin-left: 7px;"></div>';
            }
            else if (_index == '2') {
                html = '<div id="' + _id + '" class="TbInnerRightCont" style="width: 48.5%;"></div>';

            }
            break;
    }
    return html;
}

//Add new view form or tablet
function btnAddNewFormClick(isTabletInterface) {
    $('[id$=hdfCurrentFormId]').val(GetGuid());
    form.id = $('[id$=hdfCurrentFormId]').val();
    $('[id$=hdfIsChildForm]').val('false');
    form.isDeleted = false;
    form.isChildForm = false;
    form.appId = app.id;
    app.fnAddNewForm(form);

    if (isTabletInterface) {
        if (IsUserAllotedNameExist(form, 'TBGB_' + count, $('#txtAddViewSection').val()) != false) {
            return;
        }
    }
    else {
        if (IsUserAllotedNameExist(form, 'GB_' + count, $('#txtAddViewSection').val()) != false) {
            return;
        }
    }
    $('#txtAddViewSection').val("Section" + count);
    var rowPanel = new RowPanel();
    rowPanel.index = count;
    rowPanel.isDeleted = false;
    rowPanel.userDefinedName = $('#txtAddViewSection').val();
    if (isTabletInterface) {
        form.formModelType = MF_IDE_CONSTANTS.IDE_APP_MODEL_TYPE.tablet;
        $('#TBmainFormbox').show();
        $('#mainFormbox').hide();
        var strHtml = '<div id="TBGB_' + count + '" class="TBcontainerOuterDiv"><div>'
                        + '<img id="imgSettings_' + count + '" src="//enterprise.mficient.com/images/settings.png" alt="move" style="cursor:pointer;" />'
                        + '<img id="imgGpbDelete_' + count + '"  alt="delete" src="//enterprise.mficient.com/images/cross.png" title="Delete Control" style="cursor:pointer;" />'
                        + '</div><div id="Sec_' + count + '"  class="TbOuterCont">' + GetInnerHtmlOfGroupBox($('#ddlAddVeiwSecTemp').val(), 'Sec_' + count + '_' + $('#ddlAddVeiwSecTemp').val()) + '</div>'
                        + '<div class="clear"></div></div>';
        $('#TbFormDesignContainerDiv').html(strHtml);
        var secName = "";
        if ($('#Sec_' + count + '_' + $('#ddlAddVeiwSecTemp').val() + '_1').length > 0) secName = 'Sec_' + count + '_' + $('#ddlAddVeiwSecTemp').val() + '_1';
        if ($('#Sec_' + count + '_' + $('#ddlAddVeiwSecTemp').val() + '_2').length > 0) secName = 'Sec_' + count + '_' + $('#ddlAddVeiwSecTemp').val() + '_2';
        if ($('#Sec_' + count + '_' + $('#ddlAddVeiwSecTemp').val() + '_3').length > 0) secName = 'Sec_' + count + '_' + $('#ddlAddVeiwSecTemp').val() + '_3';
        if ($('#Sec_' + count + '_' + $('#ddlAddVeiwSecTemp').val() + '_4').length > 0) secName = 'Sec_' + count + '_' + $('#ddlAddVeiwSecTemp').val() + '_4';

        DragItemToGroupbox(secName);
        form.isTablet = true;
        rowPanel.id = 'Sec_' + count;

        switch ($('#ddlAddVeiwSecTemp').val()) {
            case "1":
                rowPanel.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_100;
                break;
            case "2":
                rowPanel.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_33_34_33;
                break;
            case "3":
                rowPanel.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_66_34;
                break;
            case "4":
                rowPanel.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_34_66;
                break;
            case "5":
                rowPanel.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_50_50;
                break;
        }

        DragGbToContainer('TbFormDesignContainerDiv');

        if (secName.split('_')[2] == "1") {
            var columnPanel = new ColumnPanel("1", secName, "Sec_" + count + "_Col_1", "1", [], [], false);
            rowPanel.fnAddColumnPanel(columnPanel);
            DragItemToGroupbox('Sec_' + count + '_' + $('#ddlAddVeiwSecTemp').val() + "_" + i);
        }
        else if (secName.split('_')[2] == "2") {
            for (var i = 1; i <= 3; i++) {
                var columnPanel = new ColumnPanel(i, 'Sec_' + count + '_' + $('#ddlAddVeiwSecTemp').val() + "_" + i, "Sec_" + count + "_Col_" + i, "1", [], [], false);
                rowPanel.fnAddColumnPanel(columnPanel);
                DragItemToGroupbox('Sec_' + count + '_' + $('#ddlAddVeiwSecTemp').val() + "_" + i);
            }
        }
        else if (secName.split('_')[2] == "3" || secName.split('_')[2] == "4" || secName.split('_')[2] == "5") {
            for (var i = 1; i <= 2; i++) {
                var columnPanel = new ColumnPanel(i, 'Sec_' + count + '_' + $('#ddlAddVeiwSecTemp').val() + "_" + i, "Sec_" + count + "_Col_" + i, "1", [], [], false);
                rowPanel.fnAddColumnPanel(columnPanel);
                DragItemToGroupbox('Sec_' + count + '_' + $('#ddlAddVeiwSecTemp').val() + "_" + i);
            }
        }
    }
    else {
        $('#FormDesignContainerDiv').html('<div id="GB_' + count + '" class="containerOuterDiv">'
                + '<div>'
                + '<img id="imgSettings_' + count + '" src="//enterprise.mficient.com/images/settings.png" alt="move" style="cursor:pointer;" />'
                + '<img id="imgGpbDelete_' + count + '"  alt="delete" src="//enterprise.mficient.com/images/cross.png" title="Delete Control" style="cursor:pointer;" />'
                + '</div><div id="containerDiv_' + count + '"  class="containerDiv"  style="width: 98%;"></div></div>');
        $('#TBmainFormbox').hide();
        $('#mainFormbox').show();
        form.isTablet = false;
        form.formModelType = MF_IDE_CONSTANTS.IDE_APP_MODEL_TYPE.phone;
        rowPanel.id = "GB_" + count;
        rowPanel.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_100;

        var columnPanel = new ColumnPanel("1", "containerDiv_" + count, "containerDiv_" + count + "_Col", "1", [], [], false);
        rowPanel.fnAddColumnPanel(columnPanel);

        DragGbToContainer('FormDesignContainerDiv');
        DragItemToGroupbox('containerDiv_' + count);
    }

    var clonedForm = $.parseJSON(JSON.stringify(form));
    var objClonedForm = new Form(clonedForm);
    objClonedForm.fnResetObjectDetails();
    //MOHAN

    $('#FrmInnerPropertieDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Form, editObject: objClonedForm });
    form = objClonedForm;
    form.isNew = true;
    form.fnAddRowPanels(rowPanel);

    if (isTabletInterface)
        form.isTablet = true;
    else
        form.isTablet = false;

    SettingButtonClick(count.toString());
    DeleteGroupbox(count.toString());
    $('#imgSettings_' + count.toString()).click();
    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Section, editObject: rowPanel });

    //MOHAN
    //form is added to app.
    //now sending a colned form for editing.
    //this means every time the form is in edit mode only.
}



//Add new view form or tablet
function TabletTempRadioClick(e, _IsTemp) {
    if (e.checked) {
        if (_IsTemp) $('#AddFormTempDiv').show();
        else $('#AddFormTempDiv').hide();
    }
    else {
        if (_IsTemp) $('#AddFormTempDiv').hide();
        else $('#AddFormTempDiv').show();
    }
}

//inisialize new added viewApplicationLoadNewView
function ApplicationInitNewView() {
    ShowNewForm();
    ShowFormDesigner();
}

//popup to add new form
function SubProcAddNewForm() {
    //IMPORTANT setting the position of each view in app.
    //For this the app designer window should be visible
    //Hide app designer only after this step.

    var iScrollTop = 0, iScrollLeft = 0, objCanvasDivScroollInfo = null;
    objCanvasDivScrollInfo = mFicientIde.MfApp.AppDesigner.getScrollInfoOfAppDesignerCanvas();
    if (objCanvasDivScrollInfo) {
        iScrollTop = objCanvasDivScrollInfo.scrollTop;
        iScrollLeft = objCanvasDivScrollInfo.scrollLeft;
    }
    app = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
    app.fnSetPositionOfViews(iScrollLeft, iScrollTop);
    //END 

    InitializeFormDesigner();
    initialize();
    $('#txtAddViewSection').val("Section" + count);

    if (app.modelType === MF_IDE_CONSTANTS.IDE_APP_MODEL_TYPE.phone) {
        btnAddNewFormClick(false);
    }
    else if (app.modelType === MF_IDE_CONSTANTS.IDE_APP_MODEL_TYPE.tablet) {
        btnAddNewFormClick(true);
    }
}


//New form button click
function NewButtonClick() {
    ShowNewForm();
}

//used to show new form
function ShowNewForm() {
    count = 1;
    noOfRadioButton = 3;
    noOfCheckbox = 3;
    DpOptionCount = 0;
    IsDragStart = false;
    pvsControlId = '';
    ShowForm();
    $('#FormDesignContainerDiv').html('');
    $('#TbFormDesignContainerDiv').html('');
    $('#MainControlPropetiesDiv').css('height', '425px');
    $('#FromSaveAsDiv').show();
    $('#FormDeleteDiv').show();
    $('#MstFormHeader').html('');
    $('#MstFormFooter').html('');
    $('#TbMstFormHeader').html('');
    $('#TbMstFormFooter').html('');
    $('#PropertiesHelpDiv').html('');

    $('[id$=hdfEditfrmId]').val('');
    $('[id$=hdfIsEditList]').val('0');
}


//Show form related div and control
function ShowForm() {
    count = 1;
    $('#ControlPropetiesDiv').html('');

    CtrlAttRemoveAndDisabled('draggableGroupboxDiv');
    CtrlAttRemoveAndDisabled('draggableSeparatorDiv');
    CtrlAttRemoveAndDisabled('draggableTextboxDiv');
    CtrlAttRemoveAndDisabled('draggableSubmitButtonDiv');
    CtrlAttRemoveAndDisabled('draggableTextareaDiv');
    CtrlAttRemoveAndDisabled('draggableDropdownDiv');
    CtrlAttRemoveAndDisabled('draggableSelectorListDiv');
    CtrlAttRemoveAndDisabled('draggableRadioButtonDiv');
    CtrlAttRemoveAndDisabled('draggableCheckboxDiv');
    CtrlAttRemoveAndDisabled('draggableRepeaterDiv');
    CtrlAttRemoveAndDisabled('draggableEditableListDiv');
    CtrlAttRemoveAndDisabled('draggableHiddenFieldDiv');
    CtrlAttRemoveAndDisabled('draggableDatetimePickerDiv');
    CtrlAttRemoveAndDisabled('draggableToggleDiv');
    CtrlAttRemoveAndDisabled('draggableHyperlinkDiv');
    CtrlAttRemoveAndDisabled('draggableSliderDiv');
    CtrlAttRemoveAndDisabled('draggableSeparatorDiv');
    CtrlAttRemoveAndDisabled('draggableBarcodeDiv');
    CtrlAttRemoveAndDisabled('draggableLocationDiv');
    CtrlAttRemoveAndDisabled('draggableGroupboxDiv');
    CtrlAttRemoveAndDisabled('PieChartDiv');
    //MOHAN
    CtrlAttRemoveAndDisabled('DrilldownPieChartDiv');
    CtrlAttRemoveAndDisabled('DataPointChartDiv');
    CtrlAttRemoveAndDisabled('BarGraphDiv');
    CtrlAttRemoveAndDisabled('LineChartDiv');
    CtrlAttRemoveAndDisabled('SignatureDiv');
    CtrlAttRemoveAndDisabled('SpacerDiv');
    CtrlAttRemoveAndDisabled('PictureDiv');
    CtrlAttRemoveAndDisabled('AngularGuageDiv');
    CtrlAttRemoveAndDisabled('CylinderGuageDiv');
    CtrlAttRemoveAndDisabled('TableDiv');
    CtrlAttRemoveAndDisabled('draggableImageDiv');
}

//Disable toolbox control
function CtrlAttRemoveAndDisabled(_Id) {
    $('#' + _Id).removeAttr('disabled');
    $('#' + _Id).draggable({ disabled: false });
    $('#' + _Id).css('opacity', '');
}

//Enable toolbox control
function CtrlAttAddAndEnabled(_Id) {
    $('#' + _Id).attr('disabled', 'disabled');
    $('#' + _Id).draggable({ disabled: true });
    $('#' + _Id).css('opacity', '.6');
}

//Database command link parameter popup
function SubProcBoxLinkParameterToCtrl(_bol) {
    SubProcCommonMethod('SubProcBoxLinkParameterToCtrl', _bol, 'Object Parameters', 550);
}

//Drag section control
function DragGbToContainer(containerID) {
    $('#' + containerID).droppable({
        activeClass: "ui-state-default",
        accept: ":not(.ui-sortable-helper)",
        accept: ":not(.draggableElement)",
        //hoverClass: "formDroppableHover",
        drop: function (event, ui) {
            count = count + 1;
            if (ui.draggable[0].children[0] != null && ui.draggable[0].children[0].attributes[0] != null) {
                if ($(ui.draggable[0].childNodes).find('img')[0] != undefined && $(ui.draggable[0].childNodes).find('img')[0] != null) {
                    var strXml = getHtmlForMainContainer($(ui.draggable[0].childNodes).find('img')[0].id, count, form);
                    if (strXml[0] != undefined) {
                        if (strXml[0].trim().length > 0) {
                            $(this).append(strXml[0]);
                            //var outerDivid = "#containerDiv_" + count;
                            $('#txtGbUserName').val('Section' + count);
                            btnPanelTempSelect();
                            //SubProcGroupBoxTemp(true);
                        }
                    }
                }
            }
        }
    })
    .sortable({
        handle: '.handle'
    });
    $('#' + containerID).sortable("option", "scroll", false);
    DeleteAndSortControls('#' + containerID);
}


//Get html for section control
function getHtmlForMainContainer(_CName, _Count, objForm) {
    var strXml = '';
    var strCtrlName = '';
    var strDragCont = '';
    var rowPanel = new RowPanel();
    rowPanel.index = _Count;
    rowPanel.isDeleted = false;
    if (objForm.isTablet) {
        switch (_CName) {
            case 'Groupbox':
                strCtrlName = ('Section' + _Count);
                strDragCont = strCtrlName + '_1' + ',' + strCtrlName + '_2';
                strXml = '<div id="TBGB_' + _Count + '" class="TBcontainerOuterDiv"><div>'
                + '<img id="imgSettings_' + _Count + '" src="//enterprise.mficient.com/images/settings.png" alt="move" style="cursor:pointer;" />'
                + '<img id="imgGpbDelete_' + _Count + '"  alt="delete" src="//enterprise.mficient.com/images/cross.png" title="Delete Control" style="cursor:pointer;" />'
                + '</div><div id="Sec_' + _Count + '"  class="TbOuterCont">' + GetInnerHtmlOfGroupBox('1', 'Sec_' + _Count + '_1') + '</div>'
                + '<div class="clear"></div></div>';

                break;
        }

        $('#hdfElementName').val('');
        $('#txtElementUserName').val('');
        if (strXml.length > 0) $('#hdfElementName').val('TBGB_' + _Count + ',' + 'TBGB_' + _Count);
        if (strCtrlName.length > 0) $('#txtElementUserName').val(strCtrlName);
    }
    else {
        switch (_CName) {
            case 'Groupbox':
                strCtrlName = ('Section' + _Count);
                strXml = '<div id="GB_' + _Count + '" class="containerOuterDiv">'
                + '<div>'
                + '<img id="imgSettings_' + _Count + '" src="//enterprise.mficient.com/images/settings.png" alt="move" style="cursor:pointer;" />'
                + '<img id="imgGpbDelete_' + _Count + '"  alt="delete" src="//enterprise.mficient.com/images/cross.png" title="Delete Control" style="cursor:pointer;" />'
                + '</div><div id="containerDiv_' + _Count + '"  class="containerDiv"  style="width: 98%;"></div></div>';
                break;
        }

        $('#hdfElementName').val('');
        $('#txtElementUserName').val('');
        if (strXml.length > 0) $('#hdfElementName').val('GB_' + _Count + ',' + 'GB_' + _Count);
        if (strCtrlName.length > 0) $('#txtElementUserName').val(strCtrlName);
    }
    SettingButtonClick(_Count.toString());
    DeleteGroupbox(_Count.toString());
    $('#imgSettings_' + _Count.toString()).click();
    return [strXml, strDragCont];
}

//select section control template
function btnPanelTempSelect() {
    var strCount;
    var strHtml;
    var objForm = new Form();
    if ($('[id$=hdfIsChildForm]').val() == 'false') objForm = form;
    if ($('[id$=hdfIsChildForm]').val() == 'true') objForm = childForm;

    if ($('#hdfElementName').val().trim().length == 0) return;
    if (IsUserAllotedNameExist(objForm, $('#hdfElementName').val().split(',')[1], $('#txtGbUserName').val()) != false) {
        return;
    }

    var strCount = ($('#hdfElementName').val().split(',')[1]).split('_')[1];
    var rowPanel = new RowPanel();
    rowPanel.isDeleted = false;
    rowPanel.index = strCount;

    if (objForm.isTablet) {
        var secName = "";
        $('#Sec_' + strCount).html(GetInnerHtmlOfGroupBox($('#ddlPanelTemp').val(), 'Sec_' + strCount + '_' + $('#ddlPanelTemp').val()));
        if ($('#Sec_' + strCount + '_' + $('#ddlPanelTemp').val() + '_1').length > 0) secName = 'Sec_' + strCount + '_' + $('#ddlPanelTemp').val() + '_1';
        if ($('#Sec_' + strCount + '_' + $('#ddlPanelTemp').val() + '_2').length > 0) secName = 'Sec_' + strCount + '_' + $('#ddlPanelTemp').val() + '_2';
        if ($('#Sec_' + strCount + '_' + $('#ddlPanelTemp').val() + '_3').length > 0) secName = 'Sec_' + strCount + '_' + $('#ddlPanelTemp').val() + '_3';
        if ($('#Sec_' + strCount + '_' + $('#ddlPanelTemp').val() + '_4').length > 0) secName = 'Sec_' + strCount + '_' + $('#ddlPanelTemp').val() + '_4';


        if ($('[id$=hdfIsChildForm]').val() == 'false') DragGbToContainer('TbFormDesignContainerDiv');
        if ($('[id$=hdfIsChildForm]').val() == 'true') DragGroupBoxToContainer('TbFormDesignContainerDiv');

        if ($('[id$=hdfIsChildForm]').val() == 'false') DragItemToGroupbox(secName);
        if ($('[id$=hdfIsChildForm]').val() == 'true') DragItemToContainer(secName);

        rowPanel.id = 'Sec_' + strCount;
        rowPanel.userDefinedName = $('#txtGbUserName').val();
        rowPanel.isDeleted = false;
        switch ($('#ddlPanelTemp').val()) {
            case "1":
                rowPanel.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_100;
                break;
            case "2":
                rowPanel.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_33_34_33;
                break;
            case "3":
                rowPanel.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_66_34;
                break;
            case "4":
                rowPanel.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_34_66;
                break;
            case "5":
                rowPanel.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_50_50;
                break;
        }
        if (secName.split('_')[2] == "1") {
            var columnPanel = new ColumnPanel("1", secName, "Sec_" + strCount + "_Col_1", "1", [], [], false);
            rowPanel.fnAddColumnPanel(columnPanel);
            if ($('[id$=hdfIsChildForm]').val() == 'false') DragItemToGroupbox("Sec_" + strCount + "_Col");
            if ($('[id$=hdfIsChildForm]').val() == 'true') DragItemToContainer("Sec_" + strCount + "_Col");

        }
        else if (secName.split('_')[2] == "2") {
            for (var i = 1; i <= 3; i++) {
                var columnPanel = new ColumnPanel(i, 'Sec_' + strCount + '_' + $('#ddlPanelTemp').val() + "_" + i, "Sec_" + count + "_Col_" + i, "1", [], [], false);
                rowPanel.fnAddColumnPanel(columnPanel);
                if ($('[id$=hdfIsChildForm]').val() == 'false') DragItemToGroupbox('Sec_' + strCount + '_' + $('#ddlPanelTemp').val() + "_" + i);
                if ($('[id$=hdfIsChildForm]').val() == 'true') DragItemToContainer('Sec_' + strCount + '_' + $('#ddlPanelTemp').val() + "_" + i);
            }
        }
        else if (secName.split('_')[2] == "3" || secName.split('_')[2] == "4" || secName.split('_')[2] == "5") {
            for (var i = 1; i <= 2; i++) {
                var columnPanel = new ColumnPanel(i, 'Sec_' + strCount + '_' + $('#ddlPanelTemp').val() + "_" + i, "Sec_" + count + "_Col_" + i, "1", [], [], false);
                rowPanel.fnAddColumnPanel(columnPanel);
                if ($('[id$=hdfIsChildForm]').val() == 'false') DragItemToGroupbox('Sec_' + strCount + '_' + $('#ddlPanelTemp').val() + "_" + i);
                if ($('[id$=hdfIsChildForm]').val() == 'true') DragItemToContainer('Sec_' + strCount + '_' + $('#ddlPanelTemp').val() + "_" + i);
            }
        }
    }
    else {
        if ($('[id$=hdfIsChildForm]').val() == 'false') DragGbToContainer('FormDesignContainerDiv');
        if ($('[id$=hdfIsChildForm]').val() == 'true') DragGroupBoxToContainer('FormDesignContainerDiv');

        rowPanel.id = $('#hdfElementName').val().split(',')[1];
        rowPanel.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_100;
        rowPanel.userDefinedName = $('#txtGbUserName').val();
        var columnPanel = new ColumnPanel("1", "containerDiv_" + strCount, "containerDiv_" + strCount + "_Col", "1", [], [], false);
        rowPanel.fnAddColumnPanel(columnPanel);

        if ($('[id$=hdfIsChildForm]').val() == 'false') DragItemToGroupbox('containerDiv_' + strCount);
        if ($('[id$=hdfIsChildForm]').val() == 'true') DragItemToContainer('containerDiv_' + strCount);
    }

    objForm.fnAddRowPanels(rowPanel);
    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Section, editObject: rowPanel });

    SettingButtonClick(strCount);
    DeleteGroupbox(strCount);
    $('#' + $('#hdfElementName').val().split(',')[0]).click();
    $('#imgSettings_' + strCount).click();
    //SubProcGroupBoxTemp(false);
}


//section control template popup
function SubProcGroupBoxTemp(_bol) {
    if ($('[id$=hdfIsChildForm]').val() == 'false') objForm = form;
    if ($('[id$=hdfIsChildForm]').val() == 'true') objForm = childForm;
    if (objForm.isTablet) $('#SectionNameDiv').show();
    else $('#SectionNameDiv').hide();

    if (_bol)
        showDialog('SubProcGroupBoxTemp', 'Element Name', 300, 'dialog_info.png', false);
    else
        $('#SubProcGroupBoxTemp').dialog('close');
}

function btnSectionCancelClick() {
    if ($('#hdfElementName').val().trim().length > 0) {
        $('#' + $('#hdfElementName').val().split(',')[0]).remove();
    }
    //SubProcGroupBoxTemp(false);
}

//popup to change template
function SubProcChangeTemp(_bol) {
    if (_bol) {
        showDialog('SubProcChangeTemp', 'Change Template', 350, 'dialog_info.png', false);
    }
    else
        $('#SubProcChangeTemp').dialog('close');
}

//popup to change template
function SubProcTempPromt(_bol) {
    if (_bol) {
        var cntrl = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
        var showWarning = false;
        if (cntrl.type.id == "2")
            showWarning = true;
        else if ($('#ddlNewSectionTemp').val() == "1")
            showWarning = true;
        else
            showWarning = false;

        if (showWarning) $('#warningMsg').show();
        else $('#warningMsg').hide();
        showDialog('SubProcTempPromt', 'Change Template', 400, 'dialog_warning.png', false);
    }
    else
        $('#SubProcTempPromt').dialog('close');
}

//****************************//
//this sub is used when panel setting button clicked .
//****************************//

function SettingButtonClick(count) {
    var section = null;
    $('#imgSettings_' + count).click(function (eImg) {
        var objForm = null;
        if ($('[id$=hdfIsChildForm]').val() == 'false') objForm = form;
        if ($('[id$=hdfIsChildForm]').val() == 'true') objForm = childForm;

        $('#InnerPropertiesDiv').remove();
        $('#PropertyBehaviourHeader').hide(); $('#PropOnChangeDiv').hide();

        $('.containerOuterDiv .outerDivChange').removeClass('outerDivChange').addClass('outerDiv');
        $('.TbOuterCont .outerDivChange').removeClass('outerDivChange').addClass('outerDiv');

        if (objForm.isTablet) {
            section = objForm.fnGetRowPanel(eImg.target.parentNode.parentNode.childNodes['1'].id);
            $('.TbOuterCont .containerDivChange').removeClass('containerDivChange');

            if (section) {
                var children = $('#' + section.id)[0].childNodes;
                if (children != undefined && children != null && children.length > 0) {
                    $.each(children, function (index) {
                        $(this).addClass('containerDivChange');
                    });
                }
            }
        }
        else {
            section = objForm.fnGetRowPanel(eImg.target.parentNode.parentNode.id);
            $('.containerOuterDiv .containerDivChange').removeClass('containerDivChange');
            $('#' + section.colmnPanels[0].id).addClass('containerDivChange');
        }
        $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Section, editObject: section });
    });
}

//****************************//
//this sub is used delete panel.
//****************************//

function DeleteGroupbox(count) {
    $('#imgGpbDelete_' + count).click(function (eImg) {
        var objForm = null;
        if ($('[id$=hdfIsChildForm]').val() == 'false') objForm = form;
        if ($('[id$=hdfIsChildForm]').val() == 'true') objForm = childForm;
        if (objForm.fnGetRowPanelCount() > 1) {
            if (objForm.isTablet) {
                section = objForm.fnGetRowPanel(eImg.target.parentNode.parentNode.childNodes['1'].id);
                $('#hdfDelSection').val(eImg.target.parentNode.parentNode.childNodes['1'].id);
            }
            else {
                section = objForm.fnGetRowPanel(eImg.target.parentNode.parentNode.id);
                $('#hdfDelSection').val(eImg.target.parentNode.parentNode.id);
            }
            SubProcBoxDeleteSectionMessage(true);
        }
        else {
            showMessage('You cannot delete this section.', DialogType.Error);
        }
    });
}

//Delete section
function DeleteSectionControl() {
    var objForm = new Form();
    if ($('[id$=hdfIsChildForm]').val() == 'false') objForm = form;
    if ($('[id$=hdfIsChildForm]').val() == 'true') objForm = childForm;
    if ($('#hdfDelSection').val().length > 0) {
        if (objForm.isTablet) $('#' + $('#' + $('#hdfDelSection').val())[0].parentElement.id).remove();
        else $('#' + $('#hdfDelSection').val()).remove();
        objForm.fnDeleteRowPanel($('#hdfDelSection').val());
        $('#ControlPropetiesDiv').html('');
    }
}

//this sub is used find control inside panel.
function getControlfromDiv(div, objForm) {
    var control = null;
    var labelText = '';
    var labelID = '';
    var parentDivId = '';
    var divFloat = '';
    var elms = $('#' + div.id).find("*");
    for (var i = 0, maxI = elms.length; i < maxI; ++i) {
        var elm = elms[i];
        if (elm == undefined) {
            return;
        }
        switch (elm.type) {
            case "Label":
                if (labelText.length == 0) {
                    labelText = elm.innerHTML;
                    labelID = elm.id;
                    parentDivId = $('#' + elm.id).parent()[0].id;
                }
                break;
            case "text":
                if (elm.id == undefined || elm.id.length == 0) {
                }
                else {
                    control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.id, elm.id);
                }
                if (control != null)
                    return control;
                break;
            case "select-one":
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.id);
                if (control != null)
                    return control;
                break;
            case "radio":
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id);
                if (control != null)
                    return control;
                break;
            case "checkbox":
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id);
                if (control != null)
                    return control;
                break;
            case "Image":
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.firstChild.id);
                if (control != null)
                    return control;
                break;
            case 'Repeater':
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;
            case 'EditableList':
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;
            case 'HeaderLabel':
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;
            case 'HiddenField':
                break;
            case 'dateTimePicker':
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;
            case "PieChart":
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.childNodes[0].id);
                if (control != null)
                    return control;
                break;
            case "DrilldownPieChart":
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.childNodes[0].id);
                if (control != null)
                    return control;
                break;
            case "DataPointChart":
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.parentElement.id);
                if (control != null)
                    return control;
                break;
            case "BarGraph":
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.childNodes[0].id);
                if (control != null)
                    return control;
                break;
            case "AngularGuage":
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.childNodes[0].id);
                if (control != null)
                    return control;
                break;
            case "CylinderGuage":
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.childNodes[0].id);
                if (control != null)
                    return control;
                break;
            case "LineChart":
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.childNodes[0].id);
                break;
            case 'Toggle':
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;
            case 'Hyperlink':
                break;
            case 'Slider':
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;
            case 'BarcodeReader':
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.id, elm.id);
                //control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;
            case 'Location':
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;
            case 'Separator':
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;
            case 'Table':
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;
            case 'Spacer':
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;
            case 'Picture':
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;
            case 'Signature':
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;
            case 'Button':
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;
            case 'CustomControl':
                control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null)
                    return control;
                break;

        }
    }
    return control;
}

function ShowDeleteControlDialog(cntrl) {
    if (cntrl == null || cntrl == undefined)
        return;
    var objForm = new Form();
    if ($('[id$=hdfIsChildForm]').val() == 'false') objForm = form;
    if ($('[id$=hdfIsChildForm]').val() == 'true') objForm = childForm;
    var control = getControlfromDiv(cntrl, objForm);
    if (control != null || control != undefined) {
        $('#SubProcBoxDeleteMessage').data('ControlId', control.id);
        if (control.type !== mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SEPARATOR && control.type !== mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SPACER &&
                        control.type !== mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART && control.type !== mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH &&
                        control.type !== mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE && control.type !== mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE &&
                        control.type !== mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH)
            SubProcBoxDeleteMessage(true, false);
        else
            SubProcBoxDeleteMessage(true, true);
    }
}

//Delete control
function DeleteControl() {
    var objForm = new Form();
    if ($('[id$=hdfIsChildForm]').val() == 'false') objForm = form;
    if ($('[id$=hdfIsChildForm]').val() == 'true') objForm = childForm;
    var control = objForm.fnGetControlFromId($('#SubProcBoxDeleteMessage').data('ControlId')); //mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
    var rowPnl = objForm.fnGetRowPanelOfControl(control.id);
    if (control != null && control != undefined) {
        if (objForm.isTablet) {
            if (rowPnl != null && rowPnl != undefined) {
                var arryHeights = [];
                var maxHeight;

                var children = $('#' + rowPnl.id)[0].childNodes;
                if (children != undefined && children != null && children.length > 0) {
                    $.each(children, function (index) {
                        $(this).css('min-height', '');
                    });
                    $.each(children, function (index) {
                        arryHeights.push($(this).height());
                    });
                }

                maxHeight = Math.max.apply(Math, arryHeights);
                $.each(children, function (index) {
                    $(this).css('min-height', maxHeight + 'px');
                });
            }
            $('.TbOuterCont').equalHeights();
        }
        if (!objForm.isTablet && rowPnl != null && rowPnl != undefined) {
            $(rowPnl.colmnPanels[0].id).css('min-height', '');
        }
        objForm.fnDeleteControl(control.id);
        if ($('#outerDiv' + control.id.split('_')[1]).hasClass('outerDivChange'))
            $('#ControlPropetiesDiv').html('');
        $('#outerDiv' + control.id.split('_')[1]).remove();
    }
}

//Drag controls in section
function DragItemToGroupbox(containerID) {
    $('#' + containerID).bind("dropover", function (event, ui) {
        IsLocCtrlExist = false;
        IsRptCtrlExist = false;
        IsEltCtrlExist = false;
        IsTblCtrlExist = false;
        if (ui.draggable[0].id == 'draggableGroupboxDiv' || ui.draggable[0].id.length == 0) {
            return;
        }
        IsLocCtrlExist = IsCtrlExistsInForm('Location');
        IsRptCtrlExist = IsCtrlExistsInForm('Repeater');
        IsEltCtrlExist = IsCtrlExistsInForm('EditableList');
        IsTblCtrlExist = IsCtrlExistsInForm('Table');

        if (ui.draggable[0].id.indexOf("draggable") >= 0) {
            IsDragStart = true;
        }
        if (ui.draggable[0].id == 'PieChartDiv' || ui.draggable[0].id == 'BarGraphDiv' || ui.draggable[0].id == 'TableDiv' || ui.draggable[0].id == 'LineChartDiv'
        || ui.draggable[0].id == 'AngularGuageDiv' || ui.draggable[0].id == 'CylinderGuageDiv'
        || ui.draggable[0].id == 'SpacerDiv' || ui.draggable[0].id == 'PictureDiv'
        || ui.draggable[0].id == 'SignatureDiv' || ui.draggable[0].id == 'DrilldownPieChartDiv' || ui.draggable[0].id == 'DataPointChartDiv'
        ) {

            IsDragStart = true;
        }

    });
    $('#' + containerID).bind("dropout", function (event, ui) {
        //IsDragStart = false;
    });
    $('#' + containerID).droppable({
        activeClass: "ui-state-default",
        accept: ":not(.ui-sortable-helper)",
        hoverClass: "formDroppableHover",
        tolerance: "pointer",
        drop: function (event, ui) {
            IsDragStart = false;
            var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
            var isTab = objForm.isTablet;
            if (isTab && ui.draggable[0].children[0].id == "Table") {
                if (String(this.id).split('_')[2] != '1') {   //to check for section template(if 100% or not)
                    showMessage('Tables are only allowed with 100% sections.', DialogType.Error);
                    return;
                }
            }
            if (ui.draggable[0].children[0].id == "EditableList" || ui.draggable[0].children[0].id == "Table") {
                if (!objForm.fnOnlyHiddenFieldsAndLabelExists()) {
                    if (ui.draggable[0].children[0].id == "EditableList") {
                        showMessage('Only Separator and Labels are permitted with EditableList.', DialogType.Error);
                        return;
                    }
                    else if (ui.draggable[0].children[0].id == "Table") {
                        showMessage('Only Separator and Labels are permitted with Table.', DialogType.Error);
                        return;
                    }
                }
            }
            if ($(ui.draggable[0].children[0]).find('img')[0] != undefined && $(ui.draggable[0].children[0]).find('img')[0] != null) {
                if ($(ui.draggable[0].children[0]).find('img')[0].id == "EditableList" || $(ui.draggable[0].children[0]).find('img')[0].id == "Table") {
                    if (!objForm.fnOnlyHiddenFieldsAndLabelExists()) {
                        if ($(ui.draggable[0].children[0]).find('img')[0].id == "EditableList") {
                            showMessage('Only Separator and Labels are permitted with EditableList.', DialogType.Error);
                            return;
                        }
                        else if ($(ui.draggable[0].children[0]).find('img')[0].id == "Table") {
                            showMessage('Only Separator and Labels are permitted with Table.', DialogType.Error);
                            return;
                        }
                    }
                }
            }
            if (ui.draggable[0].children[0].id != "deleteOuterContDiv") {
                if ($(ui.draggable[0].children[0]).find('img')[0] != undefined && $(ui.draggable[0].children[0]).find('img')[0] != null) {
                    if ($(ui.draggable[0].children[0]).find('img')[0].id != "Label" && $(ui.draggable[0].children[0]).find('img')[0].id != "Separator") {
                        if (objForm.fnDoesEditableListExists()) {
                            showMessage('Only Separator and Labels are permitted with EditableList.', DialogType.Error);
                            return;
                        }
                        if (objForm.fnDoesTableExists()) {
                            showMessage('Only Separator and Labels are permitted with table control.', DialogType.Error);
                            return;
                        }
                    }
                }
                else {
                    if (ui.draggable[0].children[0].id != "Label" && ui.draggable[0].children[0].id != "Separator" && ui.draggable[0].children[0].id.indexOf("Separator") < 0 && ui.draggable[0].children[0].id.indexOf("Label") < 0) {
                        if (!objForm.fnDoesControlExist(ui.draggable[0].children[0].id) && objForm.fnDoesEditableListExists()) {
                            showMessage('Only Separator and Labels are permitted with EditableList.', DialogType.Error);
                            return;
                        }
                        if (!objForm.fnDoesControlExist(ui.draggable[0].children[0].id) && objForm.fnDoesTableExists()) {
                            showMessage('Only Separator and Labels are permitted with table control.', DialogType.Error);
                            return;
                        }
                    }
                }
            }
            count = count + 1;
            var strXml = "";
            if ($(ui.draggable[0].childNodes).find('img')[0] != undefined && $(ui.draggable[0].childNodes).find('img')[0] != null) {
                var strXml = getHtml($(ui.draggable[0].childNodes).find('img')[0].id, count);
            }

            if (strXml == undefined || strXml.length == 0) {
                count = count - 1;
                return;
            }

            if (pvsControlId.length > 0) {
                $(pvsControlId).before(strXml);
                pvsControlId = '';
                btnElementNameOkClick();
            }
            else {
                $(this).append(strXml);
                btnElementNameOkClick();
            }
            if (objForm.isTablet) $('.TbOuterCont').equalHeights();
            eventOfOuterDiv('#outerDiv' + count);
            ControlDivMouseOverAndOut('outerDiv' + count);
            mFicientIde.MfApp.form.CntrlCopyPaste.contextMenu.setEvent($('#outerDiv' + count));
            //            var hgt = window.getComputedStyle(document.getElementById(event.target.id)).height.replace("px", "");
            //            $('#' + event.target.id).css('min-height', parseInt(hgt, 10) + 50 + 'px');
            $('#outerDiv' + count).click();

        }
    })
   .sortable({
       items: "div:not(.node)",
       sort: function () {
           $(this).removeClass("ui-state-default");
       }
   });
    $('#' + containerID).sortable("option", "scroll", false);
    DeleteAndSortControls('#' + containerID);
}

//Check is control exist in form
function IsCtrlExistsInForm(_Type) {
    var IsExist = false;
    var elms = $('#StyleFormContentDiv').find("*");
    if (form.isTablet) elms = $('#TbStyleFormContentDiv').find("*");
    elms.each(function () {
        if (this.type == _Type) IsExist = true;
    });
    return IsExist;
}

//Get properties html of controls
function getHtml(controlName, cntrlCount) {
    var strHtml = '';
    var strCtrlName = '';
    var strCtrlId = '';
    switch (controlName) {
        case 'Label':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                            + '<div id="LBL_' + cntrlCount + '_LabelDiv" class="node ui-corner-all container-none" style="margin-top:6px;margin-right:8px;">'
                                + '<a id="LBL_' + cntrlCount + '" type="HeaderLabel" class="hide">Label Text</a>'
                                + '<div id="LBL_' + cntrlCount + '_Title" class="node title-ttl-top-txt-left title-font-small">'
                                + 'Title'
                                + '</div>'
                                + '<div id="LBL_' + cntrlCount + '_Label" class="node text-ttl-top-txt-left text-font-small text-wrap">'
                                + 'Label Text'
                                + '</div>'
                            + '</div>'
                        + '</div>';
            strCtrlName = 'Label' + cntrlCount;
            strCtrlId = 'LBL_' + cntrlCount;
            //$('#LBL_' + cntrlCount + '_LabelDiv').draggable("destroy");
            break;
        case 'changeText':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                        + '<div id="textboxLabelOuterDiv_' + cntrlCount + '" class="node">'
                            + '<div id="TXT_' + cntrlCount + '_ImgDiv" style="float:left; padding-top:8px; padding-right:8px;" class="node hide">'
                                + '<img id="TXT_' + cntrlCount + '_Img" src=""/>'
                            + '</div>'
                            + '<div id="TXT_' + cntrlCount + '_Label" style="white-space: nowrap; overflow:hidden; -ms-text-overflow: ellipsis;" class="node">Label Text'
                            + '</div>'
                            + '<div class="clear"></div>'
                            + '<div id="textboxDiv_' + cntrlCount + '" class="node Ctrl_Width96 IdeUi-input-text IdeUi-shadow-inset IdeUi-corner-all IdeUi-btn-shadow IdeUi-body-c IdeUi-input-has-clear">'
                                + '<input id="TXT_' + cntrlCount + '" type="text" style="overflow:hidden; white-space: nowrap; -ms-text-overflow: ellipsis;" class="IdeUi-input-text IdeUi-body-c IdeUi-input" onkeydown=\"return enterKeyFilter(event);\" readonly/>'
                            + '</div>'
                        + '</div>'
                     + '</div>';


            strCtrlName = 'Textbox' + cntrlCount;
            strCtrlId = 'TXT_' + cntrlCount;
            break;
        case 'Dropdown':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                        + '<div id="DDL_' + cntrlCount + '"  class="node">'
                            + '<div id="DropdownDiv_' + cntrlCount + '" class="node hide" >'
                                + '<select id="DDL_' + cntrlCount + 'optionsDiv"  class="FrmDropdown" onkeydown=\"return enterKeyFilter(event);\">'
                                    + '<option value="value1">option1</option>'
                                    + '<option value="value2">option2</option>'
                                + '</select>'
                            + '</div>'
                            + '<div id="DDL_' + cntrlCount + '_Div" class="node" style="width:96%;">'
                                + '<div id="DrpOuterDiv_' + cntrlCount + '" class="node" >'
                                + '<div id="DDL_' + cntrlCount + '_Label" style="word-wrap: break-word;overflow: hidden; white-space: nowrap; -ms-text-overflow: ellipsis;" class="IdeUi-controlgroup-label node" role="heading">'
                                    + 'Label Text'
                                + '</div>'
                                + '<div class="IdeUi-controlgroup-controls node">'
                                + '<div class="IdeUi-select node">'
                                        + '<div class="IdeUi-btn IdeUi-btn-up-c IdeUi-shadow IdeUi-btn-corner-all IdeUi-btn-icon-right IdeUi-first-child IdeUi-last-child node">'
                                            + ' <span class="IdeUi-btn-inner" id="DDL_' + cntrlCount + '_Control"><span class="IdeUi-btn-text" id="DDL_' + cntrlCount + '_TextCntrl"><span id="DDL_' + cntrlCount + 'PromptText" style="white-space: nowrap; -ms-text-overflow: ellipsis; overflow:hidden;">Select Item </span></span>'
                                            + '<span id="DDL_' + cntrlCount + '_Icon" class="normal-drop-icon IdeUi-icon IdeUi-icon-arrow-d IdeUi-icon-shadow">&nbsp;</span></span>'
                                        + '</div>'
                                    + '</div>'
                                + '</div>'
                                + '</div>'
                              + '</div>'
                            + '</div>'
                        + '</div>'
                    + '</div>';

            strCtrlName = 'Dropdown' + cntrlCount;
            strCtrlId = 'DDL_' + cntrlCount;
            break;
        case 'SelectorList':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                        + '<div id="SEL_' + cntrlCount + '"  class="node">'
                            + '<div id="SelectorListLabelDiv_' + cntrlCount + '" class="node" >'
                                + '<a id="SelectorList_' + cntrlCount + '" type="Label" class="hide">Edit Text</a>'
                            + '</div>'
                           + '<div class="node clear"></div>'
                            + '<div id="SelectorListDiv_' + cntrlCount + '" class="node hide" >'
                                + '<select id="SEL_' + cntrlCount + 'optionsDiv"  class="FrmDropdown" onkeydown=\"return enterKeyFilter(event);\">'
                                    + '<option value="value1">option1</option>'
                                    + '<option value="value2">option2</option>'
                                + '</select>'
                            + '</div>'
                           + '<div class="node clear"></div>'
                            + '<div class="node" style="width:96%;">'
                                + '<fieldset class="IdeUi-corner-all IdeUi-controlgroup IdeUi-controlgroup-vertical node" id="IDESEL_' + cntrlCount + '">'
                                    + '<div class="IdeUi-controlgroup-label node" role="heading">'
                                        + '<legend id="SEL_' + cntrlCount + '_Label" style="display: table; word-wrap: break-word;">Label Text </legend>'
                                    + '</div>'
                                    + '<div class="IdeUi-controlgroup-controls node">'
                                        + '<div class="IdeUi-select node">'
                                            + '<div class="IdeUi-btn IdeUi-btn-up-c IdeUi-shadow IdeUi-btn-corner-all IdeUi-btn-icon-right IdeUi-first-child IdeUi-last-child node">'
                                                + ' <span class="IdeUi-btn-inner"><span class="IdeUi-btn-text"><span>Option1 </span></span><span'
                                                    + ' class="IdeUi-icon IdeUi-icon-arrow-d IdeUi-icon-shadow">&nbsp;</span></span>'
                                            + '</div>'
                                        + '</div>'
                                    + '</div>'
                                + '</fieldset>'
                              + '</div>'
                            + '</div>'
                        + '</div>'
                    + '</div>';

            strCtrlName = 'SelectorList' + cntrlCount;
            strCtrlId = 'SEL_' + cntrlCount;
            break;
        case 'RadioButton':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                        + '<div id="RDB_' + cntrlCount + '" class="node">'
                            + '<div id="RadioButtonHeaderLabelDiv_' + cntrlCount + '" class="node">'
                                + '<a id="RadioButtonHeaderText_' + cntrlCount + '" type="Label" class="hide">Select Option</a>'
                            + '</div>'
                            + '<div class="node hide" id="innerRadioDiv' + cntrlCount + '">'
                                + '<div class="node" style="clear: both;height:20px;" id="radioDiv_' + (cntrlCount + 1) + 'Set' + cntrlCount + '">'
                                    + '<input id="Radio_' + (cntrlCount + 1) + 'Set' + cntrlCount + '" type="radio"  name="radio' + cntrlCount + '" value="Value1" />'
                                    + '<a id="RadioButtonText_' + (cntrlCount + 1) + 'Set' + cntrlCount + '" type="Label">Option1</a>'
                                + '</div>'
                            + '</div>'
                           + '<div class="node clear"></div>'
                            + '<div class="node RadioButtonFieldsetCont" style="width:96%;">'
                                + '<fieldset class="node IdeUi-corner-all IdeUi-controlgroup IdeUi-controlgroup-vertical">'
                                    + '<div class="node IdeUi-controlgroup-label" role="heading">'
                                        + '<legend id="RDB_' + cntrlCount + '_Label" style="display: table;word-wrap: break-word;">Label Text </legend>'
                                    + '</div>'
                                    + '<div class="node IdeUi-controlgroup-controls IdeUi-btn-corner-all" id="RDB_' + cntrlCount + 'optionsDiv">'
                                        + '<div class="IdeUi-radio node ">'
                                            + '<input type="radio" value="Value1" id="RDB_' + cntrlCount + 'txt_1"/><label class="IdeUi-first-child IdeUi-radio-on IdeUi-btn IdeUi-fullsize IdeUi-btn-icon-left IdeUi-btn-up-c node">'
                                            + '<span class="node IdeUi-btn-inner" id="RDB_' + cntrlCount + 'Outer_span_1">'
                                            + '<span class="node IdeUi-btn-text" id="RDB_' + cntrlCount + 'span_1">Option1</span>'
                                            + '<span id="RDB_' + cntrlCount + 'Icon_span_1"  class="Checkbox-Icon node IdeUi-icon normal-radio-icon IdeUi-icon-radio-on IdeUi-icon-shadow">&nbsp;</span></span></label>'
                                        + '</div>'
                                        + '<div class="IdeUi-radio node ">'
                                            + '<input type="radio" value="Value2" id="RDB_' + cntrlCount + 'txt_2"/><label'
                                                + ' class="IdeUi-radio-off IdeUi-btn IdeUi-fullsize IdeUi-btn-icon-left IdeUi-btn-up-c node">'
                                                + '<span class="node IdeUi-btn-inner" id="RDB_' + cntrlCount + 'Outer_span_2">'
                                                + '<span class="IdeUi-btn-text node" id="RDB_' + cntrlCount + 'span_2">Option2</span>'
                                                + '<span id="RDB_' + cntrlCount + 'Icon_span_2"  class="node Checkbox-Icon IdeUi-icon normal-radio-icon IdeUi-icon-radio-off IdeUi-icon-shadow">&nbsp;</span></span></label>'
                                        + '</div>'
                                        + '<div class="IdeUi-radio node ">'
                                            + '<input type="radio" value="Value3" id="RDB_' + cntrlCount + 'txt_3"/><label'
                                                + ' class="IdeUi-radio-off IdeUi-last-child IdeUi-btn IdeUi-btn-up-c IdeUi-fullsize IdeUi-btn-icon-left node">'
                                                + '<span class="node IdeUi-btn-inner" id="RDB_' + cntrlCount + 'Outer_span_3">'
                                                + '<span class="IdeUi-btn-text node" id="RDB_' + cntrlCount + 'span_3">Option3</span>'
                                                + '<span id="RDB_' + cntrlCount + 'Icon_span_3" class="node Checkbox-Icon IdeUi-icon normal-radio-icon IdeUi-icon-radio-off IdeUi-icon-shadow">&nbsp;</span></span></label>'
                                        + '</div>'
                                    + '</div>'
                                + '</fieldset>'
                            + '</div>'
                        + '</div>'
                   + '</div>';
            strCtrlName = 'RadioButton' + cntrlCount;
            strCtrlId = 'RDB_' + cntrlCount;
            break;
        case 'Checkbox':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                          + '<div id="CHK_' + cntrlCount + '"  class="node">'
                                + '<div id="CheckboxHeaderLabelDiv_' + cntrlCount + '" class="node">'
                                               + '<a id="CheckboxHeaderText_' + cntrlCount + '" type="Label" class="hide">Option1</a>'
                                + '</div>'
                                + '<div class="node hide" id="innerCheckDiv_' + cntrlCount + '">'
                                    + '<div class="node" style="clear: both;height:20px;" id="checkDiv_' + (cntrlCount + 1) + 'Set' + cntrlCount + '">'
                                        + '<input id="check_' + (cntrlCount + 1) + 'Set' + cntrlCount + '" type="checkbox"  name="check' + cntrlCount + '" value="Value1" />'
                                        + '<a id="checkboxText_' + (cntrlCount + 1) + 'Set' + cntrlCount + '" type="Label" >Option1</a> '
                                    + '</div>'
                                + '</div>'
                               + '<div class="node clear"></div>'
                               + '<div class="node" style="width:96%;">'
                                   + ' <fieldset class="IdeUi-corner-all IdeUi-controlgroup IdeUi-controlgroup-vertical node Ctrl_Head_padBottom Ctrl_CHK_pad">'
                                        + '<div class="IdeUi-controlgroup-label node" role="heading">'
                                        + '<legend style="display: table; word-wrap: break-word;">&nbsp;&nbsp;&nbsp;&nbsp;</legend>'
                                    + '</div>'
                                        + '<div class="IdeUi-controlgroup-controls node" style="margin-top: -28px; margin-bottom:-5px;">'
                                            + '<div class="IdeUi-checkbox node">'
                                                + '<label id="CHK_' + cntrlCount + '_align" class=" node IdeUi-first-child IdeUi-last-child IdeUi-checkbox-on IdeUi-btn IdeUi-btn-corner-all IdeUi-fullsize IdeUi-btn-icon-left IdeUi-btn-up-c">'
                                                + '<div class="node IdeUi-btn-inner" id="CHK_' + cntrlCount + '_Group">'
                                                + '<div id="CHK_' + cntrlCount + '_Label" class="node IdeUi-btn-text" style="float:left;">Option1</div>'
                                                + '<div id="CHK_' + cntrlCount + '_Checkbox"  style="float:left;" class="node Checkbox-Icon IdeUi-icon IdeUi-icon-checkbox-on IdeUi-icon-shadow">&nbsp;</div>'
                                                + '</div>'
                                                        + '</label>'
                                            + '</div>'
                                        + '</div>'
                                    + '</fieldset>'
                               + '</div>'
                           + '</div>'
                       + '</div>';


            strCtrlName = 'Checkbox' + cntrlCount;
            strCtrlId = 'CHK_' + cntrlCount;
            break;
        case 'Image':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                            + '<div id="IMG_' + cntrlCount + '_OuterDiv"  class="node" style="margin-top: -6px; margin-right: 8px; margin-bottom:3px;">'
                                    + '<a id="Imagetag_' + cntrlCount + '" type="Image">'
                                        + '<img id="IMG_' + cntrlCount + '" src="//enterprise.mficient.com/images/DefaultImage .png"/>'
                                    + '</a>'
                                    + '<div class="img-cntrl-caption-div node" id="IMG_' + cntrlCount + '_DescDiv">'
                                    + '<p id="IMG_' + cntrlCount + '_Label" style="margin-top:6px;"></p>'
                                    + '</div>'
                            + '</div>'
                            + '</div>'
                      + '</div>';
            strCtrlName = 'Image' + cntrlCount;
            strCtrlId = 'IMG_' + cntrlCount;
            break;
        case 'Repeater':
            if (IsRptCtrlExist) {
                showMessage('List control already exist', DialogType.Error);
                return;
            }
            strHtml = '<div id="outerDiv' + cntrlCount + '" class="outerDiv">'
                    + '<a id="RPT_' + cntrlCount + '" type="Repeater"></a>'
                        + '<div style="margin-top:-3px;vertical-align:middle;text-align:center;"  class="node">'
                            + '<div id="RPT_' + cntrlCount + '_Cntrl" class="node" style="margin-left: -8px; display: inline-block;">'
                            + '</div>'
                        + '</div>'
                    + '</div>';
            strCtrlName = 'List' + cntrlCount;
            strCtrlId = 'RPT_' + cntrlCount;
            break;
        case 'EditableList':
            if (IsEltCtrlExist) {
                showMessage('Editable List control already exist', DialogType.Error);
                return;
            }
            strHtml = '<div id="outerDiv' + cntrlCount + '" class="outerDiv">'
                    + '<a id="ELT_' + cntrlCount + '" type="EditableList"></a>'
                        + '<div style="height: 170px;margin-top:-12px;margin-bottom:-12px;vertical-align:middle;text-align:center;"  class="node">'
                            + '<div style="height: 10px;"  class="node"></div>'
                            + '<div id="repeateritemDiv' + cntrlCount + '" class="node FrmRptInner">'
                            + '<img alt="item" src="//enterprise.mficient.com/images/ImgList.png" class="FrmRptInnerImg"/>'
                            + '</div>'
                            + '<div style="height: 10px;"  class="node"></div>'
                        + '</div>'
                    + '</div>';
            strCtrlName = 'EditableList' + cntrlCount;
            strCtrlId = 'ELT_' + cntrlCount;
            break;
        case 'HiddenField':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                            + '<div id="HiddenFieldDiv_' + cntrlCount + '" class="node Ctrl_Head_pad" >'
                                + '<a id="HDF_' + cntrlCount + '" type="HiddenField"  style="font-size:12px;color:Black;font-weight:bold;">HiddenVariable</a><br />'
                                + '<a style="font-style:italic;">(This field will not be visible on screen<br /> at runtime.)</a>'
                            + '</div>'
                      + '</div>';
            strCtrlName = 'HiddenVariable' + cntrlCount;
            strCtrlId = 'HDF_' + cntrlCount;
            break;
        case 'DatetimePicker':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                        + '<div id="dateTimePickerDiv_' + cntrlCount + '" class="node hide">'
                            + '<a id="DTP_' + cntrlCount + '" type="dateTimePicker">'
                                + '<img id="imgDTPicker_' + cntrlCount + '" alt="item"/>'
                            + '</a>'
                        + '</div>'
                        + '<div id="dateTimePickerLabelOuterDiv_' + cntrlCount + '" class="node">'
                            + '<div id="DTP_' + cntrlCount + '_ImgDiv" style="float:left; padding-top:8px; padding-right:8px;" class="node hide">'
                                + '<img id="DTP_' + cntrlCount + '_Img" src=""/>'
                            + '</div>'
                            + '<div id="DTP_' + cntrlCount + '_Label" style="font-size:12.5px; white-space: nowrap; overflow:hidden; -ms-text-overflow: ellipsis;" class="node">Label Text'
                            + '</div>'
                            + '<div class="clear"></div>'
                            + '<div class="node Ctrl_Width96 IdeUi-input-text IdeUi-shadow-inset IdeUi-corner-all IdeUi-btn-shadow IdeUi-body-c IdeUi-input-has-clear">'
                                + '<div style="color:#9a9a9a;" class="node IdeUi-input-text IdeUi-body-c Ctrl_Height " id="DTP_' + cntrlCount + '_PlaceHolder"> DateTime Picker </div> '
                            + '</div>'
                        + '</div>'
                     + '</div>';
            strCtrlName = 'DateTimePicker' + cntrlCount;
            strCtrlId = 'DTP_' + cntrlCount;
            break;
        case 'PieChart':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv" align="center">'
                            + '<div id="PIE_' + cntrlCount + '_OuterDiv"  class="node"  style="width:96%; padding:4px; margin-left: -8px;">'
                                    + '<a id="PieCharttag_' + cntrlCount + '" type="PieChart">'
                                            + '<img id="PIE_' + cntrlCount + '" src="//enterprise.mficient.com/images/piechart.png" class="FrmChartImage"/>'
                                    + '</a>'
                            + '</div>'
                     + '</div>';
            strCtrlName = 'PieChart' + cntrlCount;
            strCtrlId = 'PIE_' + cntrlCount;
            break;
        case 'DrilldownPieChart':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv" align="center">'
                            + '<div id="DPIE_' + cntrlCount + '_OuterDiv"  class="node"  style="width:96%; padding:4px; margin-left: -8px;">'
                                    + '<a id="DPieCharttag_' + cntrlCount + '" type="DrilldownPieChart">'
                                            + '<img id="DPIE_' + cntrlCount + '" src="//enterprise.mficient.com/images/drilldownpiechart.png" class="FrmChartImage"/>'
                                    + '</a>'
                            + '</div>'
                     + '</div>';
            strCtrlName = 'DrilldownPieChart' + cntrlCount;
            strCtrlId = 'DPIE_' + cntrlCount;
            break;
        case 'BarGraph':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv" align="center">'
                            + '<div id="BAR_' + cntrlCount + '_OuterDiv"  class="node"  style="width:96%; padding:4px; margin-left: -8px;">'
                                    + '<a id="BarGraphtag_' + cntrlCount + '" type="BarGraph">'
                                        + '<img id="BAR_' + cntrlCount + '" src="//enterprise.mficient.com/images/bargarph.jpg" class="FrmChartImage"/>'
                                    + '</a>'
                            + '</div>'
                     + '</div>';
            strCtrlName = 'BarGraph' + cntrlCount;
            strCtrlId = 'BAR_' + cntrlCount;
            break;
        case 'LineChart':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv" align="center">'
                            + '<div id="LINE_' + cntrlCount + '_OuterDiv"  class="node"  style="width:96%; padding:4px; margin-left: -8px;">'
                                + '<a id="LineCharttag_' + cntrlCount + '" type="LineChart">'
                                    + '<img id="LINE_' + cntrlCount + '" src="//enterprise.mficient.com/images/lineChart.jpg" class="FrmChartImage"/>'
                                + '</a>'
                            + '</div>'
                     + '</div>';
            strCtrlName = 'LineChart' + cntrlCount;
            strCtrlId = 'LINE_' + cntrlCount;
            break;
        case 'AngularGuage':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv" align="center">'
                            + '<div id="ANGU_' + cntrlCount + '_OuterDiv"  class="node"  style="width:96%; padding:4px; margin-left: -8px;">'
                                + '<a id="AngularGuagetag_' + cntrlCount + '" type="AngularGuage">'
                                    + '<img id="ANGU_' + cntrlCount + '" src="../images/AngularGaugeImage.png" class="FrmChartImage"  style="margin-top: -4px;"/>'
                                + '</a>'
                            + '</div>'
                     + '</div>';
            strCtrlName = 'AngularGuage' + cntrlCount;
            strCtrlId = 'ANGU_' + cntrlCount;
            break;
        case 'CylinderGuage':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv" align="center">'
                            + '<div id="CYGU_' + cntrlCount + '_OuterDiv"  class="node"  style="width:96%; padding:4px; margin-left: -8px;">'
                                + '<a id="CylinderGuagetag_' + cntrlCount + '" type="CylinderGuage">'
                                    + '<img id="CYGU_' + cntrlCount + '" src="../images/imageCylinderGauge.png" class="FrmChartImage"/>'
                                + '</a>'
                            + '</div>'
                     + '</div>';
            strCtrlName = 'CylinderGuage' + cntrlCount;
            strCtrlId = 'CYGU_' + cntrlCount;
            break;
        case 'DataPointChart':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv" align="center">'
                            + '<div id="DTPNT_' + cntrlCount + '" class="node clearfix">'
                                + '<a id="DataPointCharttag_' + cntrlCount + '" type="DataPointChart">'
                                + '</a>'
                                + '<div style="width: 36%; float: left;text-align:center;" class="node">'
                                    + '<div id="DTPNT_' + cntrlCount + '_Value" style="font-size: 48px; font-family: FontAwesome; float: left;" class="node">'
                                    + '78</div>'
                                    + '<div id="DTPNT_' + cntrlCount + '_Unit" style="float: left;margin-top:24px;margin-left:4px;font-size:12.5px;" class="node">'
                                    + 'million</div>'
                                + '</div>'
                                + '<div id="DTPNT_' + cntrlCount + '_Desc" style="width: 60%; float: left;margin-top:7px;text-align:left;font-size:12.5px;white-space: pre-wrap; margin-left: 8px;" class="node">'
                                    + 'Chart Description'
                                + '</div>'
                            + '</div>'
                     + '</div>';
            strCtrlName = 'DataPointChart' + cntrlCount;
            strCtrlId = 'DTPNT_' + cntrlCount;
            break;
        case 'Toggle':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                                + '<div id="ToggleLabelOuterDiv_' + cntrlCount + '" class="node">'
                                        + '<div id="ToggleLabelDiv_' + cntrlCount + '" class="node FLeft Ctrl_Head_pad">'
                                            + '<a id="Togglelabel_' + cntrlCount + '" type="Label" class="hide">Label Text</a>'
                                        + '</div>'
                                        + '<div id="ToggleDiv_' + cntrlCount + '" class="node FLeft hide" style=\"max-width:95%;\">'
                                            + '<a id="TGL_' + cntrlCount + '" type="Toggle" >'
                                                + '<img id="imgTGL_' + cntrlCount + '" src="//enterprise.mficient.com/images/ImgToggle.png" style="max-width:95%;"/>'
                                            + '</a>'
                                        + '</div>'
                                        + '<div class="clear node "></div>'
                                        + '<div class="node">'
                                          + '<fieldset id="IDETGL_' + cntrlCount + '">'
                                            + '<div id="TGL_' + cntrlCount + '_Toggle" class="IdeUi-grid-a node" style="margin-top: -12px; margin-bottom: -6px;">'
                                                + '<div class="IdeUi-block-a left-67 node">'
                                                    + '<label class="IdeUi-slider" id="TGL_' + cntrlCount + '_Label" for="Toggle7" style="display: block; word-wrap: break-word;margin-top:1px;">'
                                                        + 'Label Text</label>'
                                                + '</div>'
                                                + '<div class="IdeUi-block-b right-33 node" style="padding:0px !important;">'
                                                    + '<div class="IdeUi-slider-switch Ctrl_Toggle node" style="height:25px !important;width:76px;">'
                                                        + '<span id="TGL_' + cntrlCount + '_Val" class="IdeUi-slider-label IdeUi-slider-label-a IdeUi-btn-active IdeUi-btn-corner-all">On </span>'
                                                        + '<div class="IdeUi-slider-inneroffset IdeUi-mini node" style="margin-right:8px !important;">'
                                                            + '<a id="TGL_' + cntrlCount + '_Btn" class="IdeUi-slider-handle IdeUi-slider-handle-snapping IdeUi-btn IdeUi-shadow IdeUi-btn-up-c"'
                                                                + ' style="left: 100%;border-radius: 1em !important;height:19px !important;width:19px !important;"><span class="IdeUi-btn-inner">'
                                                                    + '<span class="IdeUi-btn-text"></span></span></a>'
                                                        + '</div>'
                                                    + '</div>'
                                                + '</div>'
                                            + '</div>'
                                        + '</fieldset>'
                                        + '</div>'
                                        + '<div class="node" style="clear:both"></div>'
                                + '</div>'
                            + '</div>';
            strCtrlName = 'Toggle' + cntrlCount;
            strCtrlId = 'TGL_' + cntrlCount;
            break;
        case 'Hyperlink':
            break;
        case 'Slider':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                            + '<div id="SliderLabelOuterDiv_' + cntrlCount + '" class="node">'
                                + '<div id="SliderLabelDiv_' + cntrlCount + '" class="node" style="margin-top:2px;margin-right:5px;word-wrap:break-word;max-width:70%;" >'
                                    + '<a id="Sliderlabel_' + cntrlCount + '" type="Label" class="hide">Label Text</a>'
                                + '</div>'
                            + '</div>'
                            + '<div class="node clear"></div>'
                            + '<div id="SliderDiv_' + cntrlCount + '" class="node" >'
                                + '<a id="SLD_' + cntrlCount + '" type="Slider"></a>'
                                + '<div id="slider_' + cntrlCount + '" class="node">'
                                   + '<fieldset id="IDESLD_' + cntrlCount + '" style="margin-bottom: -6px;">'
                                        + '<legend id="SLD_' + cntrlCount + '_Label" style="display: table; word-wrap: break-word;">Label Text </legend>'
                                        + '<div class="IdeUi-slider node" style="margin-bottom: -50px;">'
                                            + '<div id="SLD_' + cntrlCount + '_Val" class="node IdeUi-input-text IdeUi-body-c IdeUi-corner-all IdeUi-shadow-inset IdeUi-slider-input FLeft" style="pointer-events: none;width:40px;line-height: 19px; margin-bottom: -5px; font-size:12.5px !important;height: 21.3px !important;padding:5px 0px 0px 5px !important;top:-4px;">500</div>'
                                            + '<div id="SLD_' + cntrlCount + '_Slider" class="IdeUi-slider-track IdeUi-btn-down-c IdeUi-btn-corner-all node" role="application" style="top: 7px;">'
                                                + '<div class="IdeUi-slider-bg IdeUi-btn-active IdeUi-btn-corner-all node" style="width: 30%;">'
                                                + '</div>'
                                                + '<a id="SLD_' + cntrlCount + '_Btn" title="300" class="IdeUi-slider-handle IdeUi-btn IdeUi-btn-up-c IdeUi-shadow IdeUi-btn-corner-all"'
                                                    + ' style="left: 30%;border-radius: 1em !important;height:20px !important;width:19px !important;margin-top:-12px !important;"><span class="IdeUi-btn-inner">'
                                                        + '<span class="IdeUi-btn-text"></span></span></a>'
                                            + '</div>'
                                        + '</div>'
                                    + '</fieldset>'
                                + '</div>'
                            + '</div>'
                     + '</div>';

            strCtrlName = 'Slider' + cntrlCount;
            strCtrlId = 'SLD_' + cntrlCount;
            $('#SLD_' + cntrlCount + '_Val').attr('draggable', 'false');
            break;
        case 'BarcodeReader':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                        + '<div id="BarcodeReaderLabelOuterDiv_' + cntrlCount + '" class="node">'
                            + '<div id="BarcodeReaderDiv_' + cntrlCount + '" class="node Ctrl_Head_pad" >'
                                + '<a id="BarcodeReaderlabel_' + cntrlCount + '" type="Label" class="Ctrl_Label hide">Label Text</a>'
                                + '<a id="BCODE_' + cntrlCount + '_Label" class="Ctrl_Label" style="font-size: 12.5px; display: block; word-wrap: break-word;">Label Text</a>'
                            + '</div>'
                            + '<div id="BarcodeReaderOuterDiv_' + cntrlCount + '"  class="node">'
                                    + '<a id="BCODE_' + cntrlCount + '" type="BarcodeReader">'
                                    + '</a>'
                            + '</div>'
                           + '<div class="clear"></div>'
                             + '<div class="IdeUi-grid-a node ">'
                            + '<div id="BCODE_' + cntrlCount + '_TextDiv" class="IdeUi-block-a left-80 node Ctrl_BCD_pad node Ctrl_Width96 IdeUi-input-text IdeUi-shadow-inset IdeUi-corner-all IdeUi-btn-shadow IdeUi-body-c IdeUi-input-has-clear">'
                                + '<input id="BCODE_' + cntrlCount + '_Text" type="text" class="IdeUi-input-text IdeUi-body-c IdeUi-input" maxlength="50" onkeydown=\"return enterKeyFilter(event);\" readonly/>'
                            + '</div>'
                            + '<div id="BCODE_' + cntrlCount + '_ButtonDiv" class="IdeUi-block-b right-20 node Ctrl_BCD_pad">'
                                + '<button type="button" style="background-image:url(images/barcode_btn.png);background-repeat: no-repeat;background-position: center center; border-color: rgb(224, 224, 209);padding: 0px; width: 40px !important; height: 33px; margin-top: 4px;" disabled="true">'
                                + '</button>'
                            + '</div>'
                             + '</div>'
                        + '</div>'
                     + '</div>';

            strCtrlName = 'BarcodeReader' + cntrlCount;
            strCtrlId = 'BCODE_' + cntrlCount;
            break;
        case 'Location':
            if (IsLocCtrlExist) {
                showMessage('Location control already exist', DialogType.Error);
                return;
            }
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                            + '<div id="LocationLabelOuterDiv_' + cntrlCount + '" class="node Ctrl_Head_pad">'
                                   + '<div id="LocationDiv_' + cntrlCount + '" class="node" style="margin-top:2px;margin-right:5px;word-wrap:break-word;max-width:70%;padding-bottom:7px; " >'
                                        + '<a id="Locationlabel_' + cntrlCount + '" type="Label" class="hide">Label Text</a>'
                                         + '<a id="LOC_' + cntrlCount + '_Label" class="Ctrl_Label" style="font-size: 12.5px; display: block; word-wrap: break-word;">Label Text</a>'
                                   + '</div>'
                            + '</div>'
                            + '<div id="LocationOuterDiv_' + cntrlCount + '"  class="node" align="center" style="background-image:url(images/google-map.jpg);background-repeat: no-repeat;background-position: center center;margin-bottom: 4px; margin-left: -8px;">'
                                + '<a id="LOC_' + cntrlCount + '" type="Location">'
            //                                    + '<img style="width:96%;" id="LocationImg_' + cntrlCount + '" src="//enterprise.mficient.com/images/Location.png" style=""/>'
                                + '</a>'
                            + '</div>'
                       + '</div>';
            strCtrlName = 'Location' + cntrlCount;
            strCtrlId = 'LOC_' + cntrlCount;
            break;
        case 'Separator':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                            + '<div id="SeparatorDiv_' + cntrlCount + '" class="node" style="width:98%;border-bottom:1px solid #C0C0C0;">'
                                + '<a id="SEP_' + cntrlCount + '" type="Separator" ></a>'
                            + '</div>'
                      + '</div>';
            strCtrlName = 'Separator' + cntrlCount;
            strCtrlId = 'SEP_' + cntrlCount;
            break;
        case 'Table':
            if (IsTblCtrlExist) {
                showMessage('Table control already exist', DialogType.Error);
                return;
            }
            strHtml = '<div id="outerDiv' + cntrlCount + '" class="outerDiv" align="center">'
                    + '<a id="TBL_' + cntrlCount + '" type="Table"></a>'
                        + '<div style="height: 180px;margin-top:-2px; margin-bottom: -10px;"  class="node">'
                            + '<div style="height: 10px;"  class="node"></div>'
                            + '<div id="TblitemDiv' + cntrlCount + '" class="node FrmRptInner">'
                                + '<img alt="item" src="images/Table.jpg" class="FrmRptInnerImg"/>'
                            + '</div>'
                        + '</div>'
                    + '</div>';
            strCtrlName = 'Table' + cntrlCount;
            strCtrlId = 'TBL_' + cntrlCount;
            break;
        case 'Spacer':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv" align="center">'
                            + '<div id="SpacerOuterDiv_' + cntrlCount + '"  class="node"  style="width:98%;">'
                                + '<a id="SPC_' + cntrlCount + '" type="Spacer">'
            //+ '<img id="Spacer_' + cntrlCount + '" src="../images/SpacerImage.png" class="FrmChartImage"/>'
                                + '</a>'
                            + '</div>'
                     + '</div>';
            strCtrlName = 'Spacer' + cntrlCount;
            strCtrlId = 'SPC_' + cntrlCount;
            break;
        case 'Picture':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv" align="center">'
                            + '<div id="PIC_' + cntrlCount + '_LabelOuterDiv" class="node Ctrl_Head_pad" >'
                                + '<a id="PIC_' + cntrlCount + '_Label" class="Ctrl_Label" style="font-size: 12.5px; display: block; word-wrap: break-word;">Label Text</a>'
                            + '</div>'
                            + '<div class="clear node"></div></br>'
                            + '<div id="PIC_' + cntrlCount + '_OuterDiv"  class="node pictureCntrl">'
                            + '<div id="PIC_' + cntrlCount + '_Div"  class="node"  style="width:96%;">'
                                + '<a id="PIC_' + cntrlCount + '" type="Picture">'
                                + '</a>'
                            + '</div>'
                            + '</div>'
            //                            + '<div class="node"><a id="PIC_' + cntrlCount + '_ImgLabel" style="display: block;margin-top:4px;">eg.  100px x 100px</a></div>'
                     + '</div>';
            strCtrlName = 'Picture' + cntrlCount;
            strCtrlId = 'PIC_' + cntrlCount;
            break;
        case 'Signature':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv" align="center">'
                            + '<div id="SCRB_' + cntrlCount + '_OuterDiv"  class="node"  style="border: 2px dotted gray; border-image: none; width: 90px; height:auto; margin-left: -7px;background:#FFF url(//enterprise.mficient.com/images/Scribble.png) no-repeat center;">'
                            + '<div id="SCRB_' + cntrlCount + '_LabelOuterDiv" class="node Ctrl_Head_pad" >'
                                + '<a id="SCRB_' + cntrlCount + '_Label" class="Ctrl_Label" style="font-size: 12.5px; display: block; word-wrap: break-word;">Label Text</a>'
                            + '<div class="clear node"></div>'
                            + '</div>'
                             + '<div class="node">'
                                + '<a id="SCRB_' + cntrlCount + '" type="Signature">'
                                + '</a>'
                            + '</div>'
                            + '</div>'
            //                            + '<div class="node"><a id="SCRB_' + cntrlCount + '_ImgLabel" style="margin-top: 2px; margin-left: -6px; display: block;">eg.  100px x 100px</a></div>'
                     + '</div>';
            strCtrlName = 'Signature' + cntrlCount;
            strCtrlId = 'SCRB_' + cntrlCount;
            break;
        case 'Button':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv">'
                            + '<div id="BTN_' + cntrlCount + '_OuterDiv"  class="node mfIdeButton" style="padding-right:6px;">'
                            + '<a id="BTN_' + cntrlCount + '" type="Button" class="hide">'
                            + '</a>'
                            + '<div id="DIV_BTN_' + cntrlCount + '" class="btnStyleCont"><span class="buttonText">Button</span></div>'
                            + '</div>'
                      + '</div>';
            strCtrlName = 'Button' + cntrlCount;
            strCtrlId = 'BTN_' + cntrlCount;
            break;
        case 'CustomControl':
            strHtml = '<div id="outerDiv' + cntrlCount + '"  class="outerDiv" style="height:43px;">'
                            + '<div id="CUST_' + cntrlCount + '_OuterDiv"  class="node mfIdeCustomControl" style="padding-right:6px;">'
                            + '<a id="CUST_' + cntrlCount + '" type="CustomControl" class="hide">'
                            + '</a>'
                            + '<div id="DIV_CUST_' + cntrlCount + '" class="border1 p3p node" style="position:relative;height:30px;">'
                            + '<div style="width:30px;height:30px;border-radius:15px;background:grey;color:white;" class="centerAligned node">'
                            + '<div class="centerAligned node">JS</div></div>'
                            + '</div>'
                            + '</div>'
                      + '</div>';
            strCtrlName = 'CustomControl' + cntrlCount;
            strCtrlId = 'CUST_' + cntrlCount;
            break;
    }
    if (controlName != 'Groupbox') {
        $('#hdfElementName').val('');
        $('#txtElementUserName').val('');
        if (strHtml.length > 0) $('#hdfElementName').val('outerDiv' + cntrlCount + ',' + strCtrlId);
        if (strCtrlName.length > 0) $('#txtElementUserName').val(strCtrlName);
    }
    return strHtml;
}


//event of control outer div
function eventOfOuterDiv(ctrlid) {
    $(ctrlid).click(function (es) {
        var loopCount = 0;
        var objForm = null;
        if ($('[id$=hdfIsChildForm]').val() == 'false') objForm = form;
        if ($('[id$=hdfIsChildForm]').val() == 'true') objForm = childForm;
        if (objForm.isChildForm) {
            loopCount = cntrlCount;
        }
        else {
            loopCount = count;
        }

        if ($('#text').length == 0) {
            for (index = 1; index <= loopCount; index++) {
                $("#outerDiv" + index).attr("class", "outerDiv");
            }

            $('.containerOuterDiv .containerDivChange').removeClass('containerDivChange');
            $('.TbOuterCont .containerDivChange').removeClass('containerDivChange');

            if (es.target.className == "outerDiv") {
                $("#" + es.target.id.toString()).attr("class", "outerDivChange");
                controlsInsideDiv(es.currentTarget.id.toString(), objForm);
            }
            else {
                $("#" + es.currentTarget.id.toString()).attr("class", "outerDivChange");
                controlsInsideDiv(es.currentTarget.id.toString(), objForm);
            }
        }
        $(this).focus();
        if (objForm.isTablet) $('.TbOuterCont').equalHeights();
    });
}

//this sub is used find control inside panel.
function controlsInsideDiv(div, objForm) {
    var elms = $('#' + div).find("*");
    var labelText = '';
    var labelID = '';
    var IsShowProperty = true;
    var options = '';
    var parentDivId = '';
    var divFloat = '';
    var IsRadioButtonPropertyAdded = false;
    var IsCheckboxPropertyAdded = false;
    for (var i = 0, maxI = elms.length; i < maxI; ++i) {
        var elm = elms[i];
        if (elm == undefined) {
            return;
        }
        switch (elm.type) {
            case "Label":
                if (labelText.length == 0) {
                    labelText = elm.innerHTML;
                    labelID = elm.id;
                    parentDivId = $('#' + elm.id).parent()[0].id;
                }
                break;
            case "text":
                if (elm.id == undefined || elm.id.length == 0) {
                }
                else {
                    var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.id, elm.id);
                }
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Textbox, editObject: control });
                    return false;
                }
                break;
            case "select-one":
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Dropdown, editObject: control });
                    return false;
                }
                break;
            case "radio":
                IsShowProperty = false;
                if (!IsRadioButtonPropertyAdded) {
                    IsRadioButtonPropertyAdded = true;
                    var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id);
                    if (control != null) {
                        $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.RadioButton, editObject: control });
                        return false;
                    }
                }
                break;
            case "checkbox":
                IsShowProperty = false;
                if (!IsCheckboxPropertyAdded) {
                    IsCheckboxPropertyAdded = true;
                    var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id);
                    if (control != null) {
                        $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Checkbox, editObject: control });
                        return false;
                    }
                }
                break;
            case "Image":
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.firstChild.id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Image, editObject: control });
                    return false;
                }
                break;
            case 'Repeater':
                $('#InnerPropertiesDiv').remove();
                if (elm.id == undefined || elm.id.length == 0) {
                }
                else {
                    var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.id, elm.id);
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.List, editObject: control });
                }
                break;
            case 'EditableList':
                $('#InnerPropertiesDiv').remove();
                if (elm.id == undefined || elm.id.length == 0) {
                }
                else {
                    var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.id, elm.id);
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.EditableList, editObject: control });
                }
                break;
            case 'HeaderLabel':
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Label, editObject: control });
                    return false;
                }
                break;
            case 'HiddenField':
                $('#InnerPropertiesDiv').remove();
                getHiddenFieldProperties(elm.id, elm.UserAllotedName, elm.DisplayValue, elm.Description, elm.DisplayText, elm.OptionBindType, elm.DbLpJson, elm.Suffix, elm.Prefix, elm.MFact);
                break;
            case 'dateTimePicker':
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.DateTimePicker, editObject: control });
                    return false;
                }
                break;
            case "PieChart":
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.childNodes[0].id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.PieChart, editObject: control });
                    return false;
                }
                break;
            case "DrilldownPieChart":
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.childNodes[0].id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.DrilldownPieChart, editObject: control });
                    return false;
                }
                break;
            case "DataPointChart":
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.parentElement.id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.DataPointChart, editObject: control });
                    return false;
                }
                break;
            case "BarGraph":
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.childNodes[0].id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.BarGraph, editObject: control });
                    return false;
                }
                break;
            case "LineChart":
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.childNodes[0].id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.LineGraph, editObject: control });
                    return false;
                }
                break;
            case "AngularGuage":
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.childNodes[0].id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.AngularGuage, editObject: control });
                    return false;
                }
                break;
            case "CylinderGuage":
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.childNodes[0].id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.CylinderGuage, editObject: control });
                    return false;
                }
                break;
            case 'Toggle':
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Toggle, editObject: control });
                    return false;
                }
                break;
            case 'Hyperlink':
                $('#InnerPropertiesDiv').remove();
                getHyperlinkProperties(elm.id, elm.UserAllotedName, elm.innerHTML, elm.HlpTxt, elm.HlpId, elm.UrlType, elm.OptionBindType, elm.TextType, elm.DisplayValue, elm.DisplayText, elm.WsCommand, elm.WsLpJson, elm.DbCommand, elm.DbLpJson, elm.Description);
                break;
            case 'Slider':
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Slider, editObject: control });
                    return false;
                }
                break;
            case 'BarcodeReader':
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.id, elm.id);
                //var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Barcode, editObject: control });
                    return false;
                }
                break;
            case 'Location':
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Location, editObject: control });
                    return false;
                }
                break;
            case 'Separator':
                var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                if (control != null) {
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Separator, editObject: control });
                    return false;
                }
                break;
            case 'Table':
                $('#InnerPropertiesDiv').remove();
                if (elm.id == undefined || elm.id.length == 0) {
                }
                else {
                    var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.id, elm.id);
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.BITable, editObject: control });
                }
                break;
            case 'Spacer':
                $('#InnerPropertiesDiv').remove();
                if (elm.id == undefined || elm.id.length == 0) {
                }
                else {
                    var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Spacer, editObject: control });
                }
                break;
            case 'Picture':
                $('#InnerPropertiesDiv').remove();
                if (elm.id == undefined || elm.id.length == 0) {
                }
                else {
                    var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.id, elm.id);
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Picture, editObject: control });
                }
                break;
            case 'Signature':
                $('#InnerPropertiesDiv').remove();
                if (elm.id == undefined || elm.id.length == 0) {
                }
                else {
                    var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.parentElement.id, elm.id);
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Signature, editObject: control });
                }
                break;
            case 'Button':
                $('#InnerPropertiesDiv').remove();
                if (elm.id == undefined || elm.id.length == 0) {
                }
                else {
                    var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Button, editObject: control });
                    mFicientIde.ButtonControlHelper.setHtmlOfBtnCntrlInUI(control);
                }
                break;
            case 'CustomControl':
                $('#InnerPropertiesDiv').remove();
                if (elm.id == undefined || elm.id.length == 0) {
                }
                else {
                    var control = objForm.fnGetControl(elm.parentElement.parentElement.parentElement.parentElement.id, elm.parentElement.parentElement.parentElement.id, elm.id);
                    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.CustomControl, editObject: control });

                }
                break;
        }
    }
}

function ControlDivMouseOverAndOut(Id) {
    $('#' + Id).droppable({
        hoverClass: "MouseOverOuterDiv",
        refreshPositions: true,
        over: function (event, ui) {
            pvsControlId = '#' + this.id;
        },
        out: function (event, ui) {
            if ($('#' + this.id)[0].className == "outerDivChange") {
                $('#' + this.id).attr("class", "outerDivChange");
                return;
            }
            $('#' + this.id).attr("class", "outerDiv");
            pvsControlId = '';
        },
        drop: function (event, ui) {
            pvsControlId = '#' + this.id;
        }
    }).sortable({
        handle: '.handle'
    });
    $('#' + Id).sortable("option", "scroll", false);


    $('#' + Id).bind('mouseover', function () {
        if (!IsDragStart) {
            if ($('#' + this.id)[0].className == "outerDivChange") {
                return;
            }
            $('#' + this.id).attr("class", "MouseOverOuterDivChange");
        }
        else
            pvsControlId = '#' + this.id;
    });
    $('#' + Id).bind('mouseout', function () {
        if (!IsDragStart) {
            if ($('#' + this.id)[0].className == "outerDivChange") {
                $('#' + this.id).attr("class", "outerDivChange");
                return;
            }
            $('#' + this.id).attr("class", "outerDiv");
        }
    });
}

function DeleteAndSortControls(Id) {
    $(Id).sortable({
        start: function (event, ui) {
            var cntrl = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
            if (cntrl) {
                $('#outerDiv' + cntrl.id.split('_')[1]).attr("class", "outerDivChange");
            }
        },
        stop: function (event, ui) {
            var container = "";
            var objForm = new Form();
            if ($('[id$=hdfIsChildForm]').val() == 'false') objForm = form;
            if ($('[id$=hdfIsChildForm]').val() == 'true') objForm = childForm;
            if (objForm.isTablet) container = "#tablet_Div";
            else container = "#form_Div";

            var coords = $(container).position();
            if (coords) {
                coords.bottom = coords.top + $(container).height();
                coords.bottomRight = coords.left + $(container).width();
                var uiRightPosition = ui.position.left + $(event.target).width();
                if (ui.originalPosition.left < ui.position.left) {
                    if (ui.position.top > coords.top && ui.position.top < coords.bottom
                        && ui.position.left > coords.left && ui.position.left < coords.bottomRight) {
                    } else {
                        ShowDeleteControlDialog(ui.item[0]);
                    }
                }
                else {
                    if (uiRightPosition <= coords.left) {
                        ShowDeleteControlDialog(ui.item[0]);
                    }
                }
            }

            var cntrl = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
            if (cntrl) {
                $('#outerDiv' + cntrl.id.split('_')[1]).attr("class", "outerDivChange");
            }
        }
    });
}

//Popup for name
function SubProcElementName(_bol) {
    if (_bol &&
        ($('#hdfElementName').val().split(',')[1].split('_')[0] == "SEP" ||
         $('#hdfElementName').val().split(',')[1].split('_')[0] == "SPC")) {
        $('[id$=btnElementNameOk]').click();
    }
    else {
        if (_bol)
            showDialog('SubProcElementName', 'Element Name', 300, 'dialog_info.png', false);
        else
            if ($('#SubProcElementName').is(':visible')) $('#SubProcElementName').dialog('close');
    }
}


function btnElementNameCancelClick() {
    if ($('#hdfElementName').val().trim().length > 0) {
        $('#' + $('#hdfElementName').val().split(',')[0]).remove();
    }
}

//Wsdl parameter name link click
function btnElementNameOkClick() {
    if ($('#hdfElementName').val().trim().length == 0) {
        return;
    }
    var objForm = new Form();
    if ($('[id$=hdfIsChildForm]').val() == 'false') objForm = form;
    if ($('[id$=hdfIsChildForm]').val() == 'true') objForm = childForm;

    //    if (IsUserAllotedNameExist(objForm, $('#hdfElementName').val().split(',')[1], $('#txtElementUserName').val()) != false) {
    //        return;
    //    }
    var control = new Control();
    var cntrl = $('#' + $('#hdfElementName').val().split(',')[1]);
    if (cntrl[0]) {
        switch ($('#hdfElementName').val().split(',')[1].split('_')[0]) {
            case "LBL":
                var label = new Label();
                label.userDefinedName = $('#txtElementUserName').val();
                label.id = $('#hdfElementName').val().split(',')[1];
                label.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL;
                label.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.id, label);
                break;
            case "SEP":
                var separator = new Separator();
                separator.userDefinedName = $('#txtElementUserName').val();
                separator.id = $('#hdfElementName').val().split(',')[1];
                separator.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SEPARATOR;
                separator.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.id, separator);
                break;
            case "DTP":
                var dateTimePicker = new DateTimePicker();
                dateTimePicker.userDefinedName = $('#txtElementUserName').val();
                dateTimePicker.id = $('#hdfElementName').val().split(',')[1];
                dateTimePicker.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER;
                dateTimePicker.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.id, dateTimePicker);
                break;
            case "SLD":
                var slider = new Slider();
                slider.userDefinedName = $('#txtElementUserName').val();
                slider.id = $('#hdfElementName').val().split(',')[1];
                slider.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER;
                slider.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.id, slider);
                break;
            case "TXT":
                var textbox = new Textbox();
                textbox.userDefinedName = $('#txtElementUserName').val();
                textbox.id = $('#hdfElementName').val().split(',')[1];
                textbox.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX;
                textbox.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.parentElement.id, textbox);
                break;
            case "TGL":
                var toggle = new Toggle();
                toggle.userDefinedName = $('#txtElementUserName').val();
                toggle.id = $('#hdfElementName').val().split(',')[1];
                toggle.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE;
                toggle.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.parentElement.id, toggle);
                break;
            case "CHK":
                var checkbox = new Checkbox();
                checkbox.userDefinedName = $('#txtElementUserName').val();
                checkbox.id = $('#hdfElementName').val().split(',')[1];
                checkbox.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX;
                checkbox.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.id, checkbox);
                break;
            case "BCODE":
                var barcode = new Barcode();
                barcode.userDefinedName = $('#txtElementUserName').val();
                barcode.id = $('#hdfElementName').val().split(',')[1];
                barcode.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARCODE;
                barcode.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.parentElement.id, barcode);
                //objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.id, barcode);
                break;
            case "LOC":
                var location = new Location();
                location.userDefinedName = $('#txtElementUserName').val();
                location.id = $('#hdfElementName').val().split(',')[1];
                location.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION;
                location.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.id, location);
                break;
            case "IMG":
                var image = new Image();
                image.userDefinedName = $('#txtElementUserName').val();
                image.id = $('#hdfElementName').val().split(',')[1];
                image.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE;
                image.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.parentElement.id, image);
                break;
            case "PIE":
                var pieChart = new PieChart();
                pieChart.userDefinedName = $('#txtElementUserName').val();
                pieChart.id = $('#hdfElementName').val().split(',')[1];
                pieChart.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART;
                pieChart.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.parentElement.id, pieChart);
                break;
            case "DTPNT":
                var dtPointChart = new DataPointChart();
                dtPointChart.userDefinedName = $('#txtElementUserName').val();
                dtPointChart.id = $('#hdfElementName').val().split(',')[1];
                dtPointChart.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART;
                dtPointChart.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.id, dtPointChart);
                break;
            case "DPIE":
                var ddPieChart = new DrilldownPieChart();
                ddPieChart.userDefinedName = $('#txtElementUserName').val();
                ddPieChart.id = $('#hdfElementName').val().split(',')[1];
                ddPieChart.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART;
                ddPieChart.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.parentElement.id, ddPieChart);
                break;
            case "BAR":
                var barGraph = new BarGraph();
                barGraph.userDefinedName = $('#txtElementUserName').val();
                barGraph.id = $('#hdfElementName').val().split(',')[1];
                barGraph.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH;
                barGraph.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.parentElement.id, barGraph);
                break;
            case "LINE":
                var lineGraph = new LineGraph();
                lineGraph.userDefinedName = $('#txtElementUserName').val();
                lineGraph.id = $('#hdfElementName').val().split(',')[1];
                lineGraph.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH;
                lineGraph.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.parentElement.id, lineGraph);
                break;
            case "ANGU":
                var angularGuage = new AngularGuage();
                angularGuage.userDefinedName = $('#txtElementUserName').val();
                angularGuage.id = $('#hdfElementName').val().split(',')[1];
                angularGuage.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE;
                angularGuage.fnSetDefaults();

                cntrl = $('#' + $('#hdfElementName').val().split(',')[1]);
                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.parentElement.id, angularGuage);
                break;
            case "CYGU":
                var cylinderGuage = new CylinderGuage();
                cylinderGuage.userDefinedName = $('#txtElementUserName').val();
                cylinderGuage.id = $('#hdfElementName').val().split(',')[1];
                cylinderGuage.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE;
                cylinderGuage.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.parentElement.id, cylinderGuage);
                break;
            case "RDB":
                $('#RbContent').html('');
                var radioButton = new RadioButton();
                radioButton.userDefinedName = $('#txtElementUserName').val();
                radioButton.id = $('#hdfElementName').val().split(',')[1];
                radioButton.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON;
                radioButton.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.id, radioButton);
                break;
            case "DDL":
                $('#DpContent').html('');
                var dropdown = new Dropdown();
                dropdown.userDefinedName = $('#txtElementUserName').val();
                dropdown.id = $('#hdfElementName').val().split(',')[1];
                dropdown.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN;
                dropdown.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.id, dropdown);
                break;
            case "SEL":
                $('#DpContent').html('');
                var selectorList = new Dropdown();
                selectorList.userDefinedName = $('#txtElementUserName').val();
                selectorList.id = $('#hdfElementName').val().split(',')[1];
                selectorList.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST;
                selectorList.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.id, selectorList);
                break;
            case "ELT": //MOHAN
                var objEditableList = new EditableList({
                    addFormTitle: "Add Item",
                    editFormTitle: "Edit Item",
                    viewFormTitle: "View Item",
                    allowItemAddition: "0",
                    allowItemEditing: "0",
                    allowItemDelete: "0",
                    minItems: "1",
                    maxItems: "0",
                    addConfirmation: "0"
                });

                $('[id$=hdfEditableListId]').val($('#hdfElementName').val().split(',')[1]);
                objEditableList.userDefinedName = $('#txtElementUserName').val();
                objEditableList.id = $('#hdfElementName').val().split(',')[1];
                objEditableList.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST;
                objEditableList.isDeleted = false;

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.id, objEditableList);
                break;
            case "TBL":
                var objTable = new Table({
                    columns: []
                });
                objTable.fnSetDefaults();
                objTable.userDefinedName = $('#txtElementUserName').val();
                objTable.id = $('#hdfElementName').val().split(',')[1];
                objTable.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE;
                objTable.isDeleted = false;

                form.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.id, objTable);
                break;
            case "RPT":
                // var objList = new List({
                //     //headerText: "Header Text",
                //     //footerText: "Footer Text",
                //     listType: "1",
                //     dataInset: "0",
                //     countBubble: "0",
                //     actionButton: "1",
                //     rowClickable: "0",
                //     enablePaging: "0",
                //     dataGrouping: "0",
                //     selectedRowStyle: "0",
                //     titleColor: "",
                //     selectedRow: "",
                //     backNavRefresh: ""
                // });
                var objList = new List();
                objList.userDefinedName = $('#txtElementUserName').val();
                objList.id = $('#hdfElementName').val().split(',')[1];
                objList.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST;
                objList.isDeleted = false;
                objList.fnSetDefaults();
                var listDataOption = new ManualDataOption();
                listDataOption.rowId = "1";
                listDataOption.title = "title";
                listDataOption.description = "description";
                objList.fnAddManualDataOption(listDataOption);

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.id, objList);
                break;        //mohan
            case "SPC":
                var objSpacer = new Spacer();
                objSpacer.userDefinedName = $('#txtElementUserName').val();
                objSpacer.id = $('#hdfElementName').val().split(',')[1];
                objSpacer.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SPACER;
                objSpacer.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.id, objSpacer);
                break;
            case "PIC":
                var objPicture = new Picture();
                objPicture.fnSetDefaults();
                objPicture.userDefinedName = $('#txtElementUserName').val();
                objPicture.id = $('#hdfElementName').val().split(',')[1];
                objPicture.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PICTURE;

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.parentElement.id, objPicture);
                break;
            case "SCRB":
                var objSignature = new Signature();
                objSignature.userDefinedName = $('#txtElementUserName').val();
                objSignature.id = $('#hdfElementName').val().split(',')[1];
                objSignature.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SIGNATURE;
                objSignature.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.parentElement.id, objSignature);
                break;
            case "BTN":
                var objButton = new Button();
                objButton.userDefinedName = $('#txtElementUserName').val();
                objButton.id = $('#hdfElementName').val().split(',')[1];
                objButton.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BUTTON;
                objButton.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.id, objButton);
                break;
            case "CUST":
                var objCustomControl = new CustomControl();
                objCustomControl.userDefinedName = $('#txtElementUserName').val();
                objCustomControl.id = $('#hdfElementName').val().split(',')[1];
                objCustomControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CUSTOM_CONTROL;
                objCustomControl.fnSetDefaults();

                objForm.fnAddControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.id, objCustomControl);
                break;
        }
    }
    $('#' + $('#hdfElementName').val().split(',')[0]).click();
    //    if ($('#hdfElementName').val().split(',')[1].split('_')[0] != "SEP" ||
    //        $('#hdfElementName').val().split(',')[1].split('_')[0] != "SPC") SubProcElementName(false);
}

function SaveConditionalBehaviour() {
    var json = $('#divConditionalControl').data("FinalJson")();
    var cntrl = $('#' + $('#divConditionalControl').data("ControlId"));
    var control = form.fnGetControl(cntrl[0].parentElement.parentElement.parentElement.parentElement.parentElement.id, cntrl[0].parentElement.parentElement.parentElement.parentElement.id, $('#divConditionalControl').data("ControlId"));
    control.condDisplayCntrlProps = json;
    $('#SubProcConditionalDisplay').dialog('close');

}

//encode html
function htmlEncode(value) {
    if (value) {
        return jQuery('<div />').text(value).html();
    } else {
        return '';
    }
}

//Decode html
function htmlDecode(value) {
    if (value) {
        return $('<div />').html(value).text();
    } else {
        return '';
    }
}

//Add wsdl manual/ click event
function ShowGetWsdlMannualDiv() {
    $('#UploadWsdlCfnDiv').hide(); $('#btnUploadWsdlCfn').hide(); $('#GetWsdlMannual').show();
}

//add new option in radio button options

//Control command type change event
function ctrlCTypeDdlChange() {
    $('#ddlOpsBindType').bind('change', function () {
        ctrlCDivVisible(this.value, false);
        $('#txtTDbCommand').val('Select Object');
        $('#txtTWsCommand').val('Select Object');
    });
}


//****************************//
//this sub is used check weather control name exists.
//****************************//

function IsUserAllotedNameExist(objForm, _Id, _Name, _Type) {
    if (_Id == undefined || _Name == undefined) {
        return true;
    }
    if (IsStringContainsSpecialChar(_Name) || _Name == "") {
        showMessage('Please enter valid control name.', DialogType.Error);
        return true;
    }
    if (Math.floor(_Name.substring(0, 1)) == _Name.substring(0, 1)) {
        showMessage('Please enter valid control name. Name can not start with numeric value.', DialogType.Error);
        return true;
    }
    if (_Name.trim().length > 50) {
        showMessage('Control name cannot be more than 20 characters.', DialogType.Error);
        return true;
    }
    if (_Name.trim().length < 3) {
        showMessage('Control name cannot be less than 3 characters.', DialogType.Error);
        return true;
    }
    var IsExist = false;

    if ((_Id.split('_')[0] == 'GB' || _Id.split('_')[0] == 'TBGB') && objForm.fnIsSectionUserDefinedNameExist(_Name, _Id)) {
        IsExist = true;
    }
    else if (objForm.fnIsControlUserDefinedNameExist(_Name, _Id)) {
        IsExist = true;
    }
    if (IsExist == true) {
        if (_Type == 'Id') showMessage('Name already exist. Please enter unique row id name.', DialogType.Error);
        else if (_Type == 'Title') showMessage('Name already exist. Please enter unique title name.', DialogType.Error);
        else showMessage('Control name already exist. Please change name of control.', DialogType.Error);
    }
    return IsExist;
}

//Save form designer
function ShowFormDesigner() {
    $('#ProcessWorkflowDiv').show();
    $('#Wf_FormDesigner').show();
    $('#FormDesignerDiv').show();
    $('#WF_mainPropertiesbox').hide();
    $('#WF_mainFormbox').hide();
    $('#FormToolboxHeadDiv').show();
    $('#WfToolboxHeadDiv').hide();
    $('#WfToolboxAppPropDiv').hide();
}

/*-----------------------------------------------------------------Form Edit---------------------------------------------------------------*/

function SubProcViewSaveAs(_bol) {
    if (_bol) {
        //showDialog('SubProcViewSaveAs', 'Save Form', 400, 'dialog_info.png', false);
        showModalPopUp('SubProcViewSaveAs', "Save Form", 400, false, null, DialogType.Info, null);
    }
    else {
        $('#SubProcViewSaveAs').dialog('close');
    }
}

function btnVeiwSaveClick() {
    // var error = ValidateForm($('[id$=txtViewName]').val(), true);
    // if (error === false) {
    //     form.name = $('[id$=txtViewName]').val();
    //     saveForm(form);
    //     SubProcViewSaveAs(false);
    // }
    try {
        showWaitModal();
        var strFormName = $('[id$=txtViewName]').val();
        mFicientIde.MfApp.FormMainMenuEventProcess.processSaveAsFormSave(strFormName);
        $('[id$=txtViewName]').val('');
        SubProcViewSaveAs(false);
    }
    catch (error) {
        if (error instanceof MfError) {
            showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
        } else {
            console.log(error);
        }
    }
    hideWaitModal();
}

function ValidateForm(formName, checkFormExistence) {
    var errors = [];
    if (!formName) {
        errors.push('Please enter valid form name.');
    }
    else if (IsStringContainsSpecialChar(formName)) {
        errors.push('Please enter valid form name.');

    }
    else if (formName.trim().length > 20) {
        errors.push('Form name cannot be more than 20 characters.');

    }
    else if (formName.trim().length < 3) {
        errors.push('Form name cannot be less than 3 characters.');

    }
    if (checkFormExistence) {
        app = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
        var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
        if (app.fnDoesFormWithNameExists(formName, objForm.id)) {
            errors.push('Form name already exist.');
        }
    }

    if (errors.length > 0) {
        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
        return true;
    }
    else {
        return false;
    }
}

function saveAllControlsToCurrentForm() {
    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
    var allCntrls = [];
    var aryControls = [];
    var aryRowPanels = [];
    var arryDeletedRowPanels = [];
    var arryDeletedColPanels = [];
    var arryDeletedCntrls = [];
    var elms = null;
    var rowPanel = null;
    var control = null;
    if (objForm) {
        if (objForm.isTablet != undefined && objForm.isTablet) elms = document.getElementById('TbStyleFormContentDiv').getElementsByTagName("*");
        else elms = document.getElementById('StyleFormContentDiv').getElementsByTagName("*");

        var elmId = "";
        var objHiddenFieldRowPanel = objForm.fnGetRowPanel("Sec_hd");
        aryRowPanels.push(objHiddenFieldRowPanel);

        for (var i = 0, maxI = elms.length; i < maxI; ++i) {
            var elm = elms[i];
            if ((elm.className == "TBcontainerOuterDiv" || elm.className == "containerOuterDiv") && elm.id != "Sec_hd") {
                rowPanel = null;
                if (objForm.isTablet) rowPanel = objForm.fnGetRowPanel("Sec_" + elm.id.split('_')[1]);
                else rowPanel = objForm.fnGetRowPanel(elm.id);
                if ((rowPanel != null || rowPanel != undefined) && rowPanel.colmnPanels.length > 0) {
                    $.each(rowPanel.colmnPanels, function (colIndex, objColPanel) {
                        if ((objColPanel.controls != undefined || objColPanel.controls != undefined) && objColPanel.controls.length > 0) {
                            aryControls = [];
                            $('#' + objColPanel.id).find("*").each(function () {
                                switch (this.type) {
                                    case "text":
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case "select-one":
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.parentElement.parentElement.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case "radio":
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.parentElement.parentElement.parentElement.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case "checkbox":
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.parentElement.parentElement.parentElement.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case "Image":
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.firstChild.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case 'Repeater':
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case 'EditableList':
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case 'HeaderLabel':
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case 'dateTimePicker':
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case "PieChart":
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.childNodes[0].id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case "BarGraph":
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.childNodes[0].id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case "AngularGuage":
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.childNodes[0].id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case "CylinderGuage":
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.childNodes[0].id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case "LineChart":
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.childNodes[0].id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case 'Toggle':
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case 'Slider':
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case 'BarcodeReader':
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case 'Location':
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case 'Separator':
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case 'Table':
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case 'Spacer':
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case 'Picture':
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case 'Signature':
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case "DrilldownPieChart":
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.childNodes[0].id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case "DataPointChart":
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.parentElement.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case "Button":
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                    case "CustomControl":
                                        control = null;
                                        if (this.id != undefined || this.id.length != 0) {
                                            control = objForm.fnGetControlFromId(this.id);
                                            if (control != null) aryControls.push(control);
                                        }
                                        break;
                                }
                            });
                            arryDeletedCntrls = [];
                            arryDeletedCntrls = objForm.fnGetAllDeletedControls(rowPanel.id, objColPanel.id);
                            if (arryDeletedCntrls != null && arryDeletedCntrls != undefined && arryDeletedCntrls.length > 0) {
                                $.each(arryDeletedCntrls, function (cntrlIndex, objCntrl) {
                                    aryControls.push(objCntrl);
                                });
                            }

                            objColPanel.controls = [];
                            objColPanel.controls = aryControls;
                        }
                    });
                    aryRowPanels.push(rowPanel);
                }
            }
        }
        arryDeletedRowPanels = objForm.fnGetAllDeletedRowPanels();
        if (arryDeletedRowPanels != null && arryDeletedRowPanels != undefined && arryDeletedRowPanels.length > 0) {
            $.each(arryDeletedRowPanels, function (rowIndex, objRowPanel) {
                aryRowPanels.push(objRowPanel);
            });
        }
        objForm.rowPanels = [];
        objForm.rowPanels = aryRowPanels;
    }
}


function SaveCurrentForm(isDiscard) {
    if ($('#SubProcFormChange').is(':visible')) $('#SubProcFormChange').dialog('close');
    showWaitModal();
    setTimeout(function () {
        try {
            saveAllControlsToCurrentForm();
            var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
            var error = ValidateForm(objform.name, true);
            if (error === false) {
                try {
                    var errors = objform.ValidateControls();
                    if (errors.length > 0) {
                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
                    }
                    else {
                        if (objform.isChildForm && $('[id$=hdfIsChildForm]').val() == 'true') {
                            var editableListControl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                            if (!editableListControl.fnAddChildForm(objform))
                                isDiscard = false;
                        }
                        else {
                            mFicientIde.MfApp.FormMainMenuEventProcess.processSaveForm(objform);
                            $("#tblEditListProps > tbody").html("");
                        }
                        if (isDiscard)
                            subProcCloseAndDiscardCurrentForm();
                    }
                }
                catch (error) {
                    if (error instanceof MfError) {
                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                    }
                    else {
                        showMessage("Could not save form.Please try again", DialogType.Error);
                    }
                }
            }
        }
        catch (error) {
            if (error instanceof MfError) {
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
            } else {
                console.log(error);
            }
        }
        finally {
            hideWaitModal();
        }
    }, 0);
}
function deleteForm() {
    var blnConfirm = confirm('Are you sure you want to delete this form');
    if (blnConfirm) {
        showWaitModal();
        setTimeout(function () {
            try {
                var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                if (objform.isChildForm && $('[id$=hdfIsChildForm]').val() == 'true') {
                    var editableListControl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                    editableListControl.fnDeleteChildFormObj(objform.id);
                    if ($('[id$=hdfIsChildForm]').val() == 'true') {
                        formEdit(form);
                    }
                }
                else {
                    mFicientIde.MfApp.FormMainMenuEventProcess.processDeleteForm(objform);
                }
            }
            catch (error) {
                if (error instanceof MfError) {
                    showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                } else {
                    console.log(error);
                }
            }
            finally {
                hideWaitModal();
            }
        }, 0);
    }
    else {
    }
}
function SaveAsCurrentForm() {

    showWaitModal();
    setTimeout(function () {
        try {
            var objMainForm = null,
                objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                aryErrors = [],
                objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();

            if (objForm.fnIsChildForm() === true) {
                objMainForm = MF_HELPERS.currentObjectHelper.getMainFormIfCurrFormIsChildForm();
                aryErrors = MfIdeFormSaveHelper.validateForm(objForm, objApp);
                if ($.isArray(aryErrors) && aryErrors.length > 0) {
                    showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(aryErrors), DialogType.Error);
                    return;
                }
            }
            else {
                aryErrors = MfIdeFormSaveHelper.validateFormControls(objForm);
                if ($.isArray(aryErrors) && aryErrors.length > 0) {
                    showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(aryErrors), DialogType.Error);
                    return;
                }
            }
            $('[id$=txtViewName]').val('');
            SubProcViewSaveAs(true);
        }
        catch (error) {
            if (error instanceof MfError) {
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
            } else {
                console.log(error);
            }
        }
        finally {
            hideWaitModal();
        }
    }, 0);

}

function closeChildForm(objform) {
    var newForm = true;
    if ((objform != undefined || objform != null) && objform.fnGetAllControls().length > 0 && objform.isNew) {
        var editableListControl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
        if (editableListControl.childForms != undefined && editableListControl.childForms.length > 0) {
            $.each(editableListControl.childForms, function () {
                if (this.id == objform.id) {
                    newForm = false;
                }
            });
        }
    }
    else {
        newForm = false;
    }
    if (newForm) {
        showDialog('SubProcFormChange', 'Save Changes', 300, 'dialog_warning.png', false);
    }
    else
        closeAndDiscardCurrentForm(objform);
}

function closeMainForm(objform) {
    if ((objform != undefined || objform != null) && objform.fnGetAllControls().length > 0 && objform.isNew) {
        if (MF_HELPERS.currentObjectHelper.getFormFromApp(objform.id) == null)
            showDialog('SubProcFormChange', 'Save Changes', 300, 'dialog_warning.png', false);
        else
            closeAndDiscardCurrentForm(objform);
    }
    else
        closeAndDiscardCurrentForm(objform);
}

function closeAndDiscardCurrentForm() {
    showWaitModal();
    setTimeout(function () {
        try {
            subProcCloseAndDiscardCurrentForm();
        }
        catch (error) {
            if (error instanceof MfError) {
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
            } else {
                console.log(error);
            }
        }
        finally {
            hideWaitModal();
        }
    }, 0);
}
function subProcCloseAndDiscardCurrentForm() {
    var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
    if ($('[id$=hdfIsChildForm]').val() == 'false') {
        $('#TBmainFormbox').hide();
        $('#mainFormbox').hide();
        $('#FrmInnerPropertieDiv').html('');
        $('#ControlPropetiesDiv').html('');
        //var modelType = objform.fnGetModelType();
        if (objform != null) mFicientIde.MfApp.FormMainMenuEventProcess.closeForm(objform);
        else mFicientIde.MfApp.FormMainMenuEventProcess.closeForm(mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm());
    }
    if ($('[id$=hdfIsChildForm]').val() == 'true') {
        formEdit(form);
    }
}

function closeCurrentForm() {
    showWaitModal();
    showDialog('SubProcFormChange', 'Save Changes', 300, 'dialog_warning.png', false);
    hideWaitModal();
}

function getId() {
    var now = new Date();
    var datetime = now.getHours() + now.getMinutes() + now.getSeconds();
    var random = (((1 + Math.random()) * 0x10990) | 0).toString(16).substring(1);
    return (random + datetime + random + "4" + random.substr(0, 3) + random + random.substr(2, 5) + random + random).toUpperCase();
}

function GetGuid() {
    app = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
    var exist = false;
    var guid = getId();
    if (app.forms != undefined && app.forms.length > 0) {
        $.each(app.forms, function (index, val) {
            if (this.id == guid) {
                exist = true;
            }
        });
    }
    if (!exist)
        return guid;
    else
        GetGuid();
}





//-------------------------------------------------------------------MOHAN _________---------------------------------------------------------------//
//Check only hdf and lbl exist with control
//function OnlyLblAndHdfExist() {
//    var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
//    var IsValid = true;
//    var elms = $('#StyleFormContentDiv');
//    if (objform.isTablet) elms = $('#TbStyleFormContentDiv');

//    elms.find("*").each(function () {
//        switch (this.type) {
//            case "text":
//            case "select-one":
//            case "radio":
//            case "checkbox":
//            case "Image":
//            case 'Repeater':
//            case 'dateTimePicker':
//            case "PieChart":
//            case "BarGraph":
//            case "LineChart":
//            case 'Toggle':
//            case 'Hyperlink':
//            case 'Slider':
//            case 'BarcodeReader':
//            case 'Table':
//            case 'Location':
//                IsValid = false;
//                break;
//            case 'EditableList':
//                IsValid = false;
//                break;

//        }
//    });
//    return IsValid;
//}

//IMPORTANT 
//THE HTML GENERATED IS SIMILAR TO WAY DRAG DROP FUCTIONALITY IS PROVIDED
//COPIED FROM THAT PROCESS
//AFTER ADDING HTML ALL THE PROCESS IN DRAG DROP FUNCTIONALITY NEEDS TO BE DONE
mFicientIde.MfApp.form.CntrlCopyPaste = (function () {

    var _globalData = {
        setData: function (div, data) {
            //{controlId,currFocusCntrlDiv}
            div.data(MF_IDE_CONSTANTS.jqueryCommonDataKey, data);
        },
        removeData: function (div) {
            div.removeData(MF_IDE_CONSTANTS.jqueryCommonDataKey);
        },
        getData: function (div) {
            return div.data(MF_IDE_CONSTANTS.jqueryCommonDataKey);
        },
        getCntrlId: function (div) {
            return this.getData(div).controlId;
        },
        getCurrFocusCntrlDiv: function (div) {
            return this.getData(div).currFocusCntrlDiv;
        }
    };
    var NEW_CNTRL_PASTE_POSITION = {
        above: "1",
        below: "2"
    }
    function _getContextMenuDiv() {
        return $('#divFormCntrlCopyPasteContextMenu');
    }

    var _contextMenu = (function () {
        function _show(div, evnt) {
            div.dialog({
                dialogClass: "noTitlebar",
                resizable: false,
                width: 160,
                minHeight: 20,
                draggable: false,
                position: {
                    my: "left top",
                    of: evnt
                }
            });
            div.css('min-height', '20px')
            .css('padding-left', '2px')
            .css('padding-top', '1px')
            .css('padding-right', '2px');
        }
        function _setEvent(div) {
            div.on('mousedown', function (evnt) {
                if (evnt.which === 3) {
                    evnt.currentTarget.click();
                    var objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                        objControl = MF_HELPERS.currentObjectHelper.getCurrentWorkingControl(),
                        $menuContainer = _getContextMenuDiv();
                    _processShowHideMenuElements(objControl, objForm);
                    _globalData.setData($menuContainer,
                            {
                                controlId: objControl.id,
                                currFocusCntrlDiv: $(evnt.currentTarget).attr('id')
                            });
                    _show($menuContainer, evnt);
                    evnt.stopPropagation();
                }
            });
        }
        function _showHideMenuElements(
                     isClipbrdEmpty,
                     isCntrlClickedTypeSameAsClipbrd,
                     isCntrlClickedIdSameAsClipbrd) {

            $('#divFrmCntrlCopyContextMenu').show().next().hide();
            $('#divFrmCntrlPasteAboveContextMenu').hide().next().hide();
            $('#divFrmCntrlPasteBelowContextMenu').hide().next().hide();
            $('#divFrmCntrlPasteStyleContextMenu').hide().next().hide();
            if (!isClipbrdEmpty) {
                $('#divFrmCntrlCopyContextMenu').next().show();
                $('#divFrmCntrlPasteAboveContextMenu').show().next().show();
                $('#divFrmCntrlPasteBelowContextMenu').show();
                if (isCntrlClickedTypeSameAsClipbrd && !isCntrlClickedIdSameAsClipbrd) {
                    $('#divFrmCntrlPasteBelowContextMenu').next().show();
                    $('#divFrmCntrlPasteStyleContextMenu').show();
                }
            }
        }
        function _processShowHideMenuElements(control, form) {
            var isClipbrdEmpty = mFicientIde.Cache.isControlToCopyEmpty(),
                objClipbrdCntrl = null,
                isCntrlClickedTypeSameAsClipbrd = false,
                isCntrlClickedIdSameAsClipbrd = false;

            if (!isClipbrdEmpty) {
                objClipbrdCntrl = mFicientIde.Cache.getControlToCopy();
                if (objClipbrdCntrl.type === control.type) {
                    isCntrlClickedTypeSameAsClipbrd = true;
                }
                else {
                    isCntrlClickedTypeSameAsClipbrd = false;
                }
                if (objClipbrdCntrl.id === control.id) {
                    isCntrlClickedIdSameAsClipbrd = true;
                }
                else {
                    isCntrlClickedIdSameAsClipbrd = false;
                }
            }
            _showHideMenuElements(isClipbrdEmpty,
                     isCntrlClickedTypeSameAsClipbrd,
                     isCntrlClickedIdSameAsClipbrd);
        }
        return {
            show: function (event) {
                var $divContextMenu = _getContextMenuDiv();
                _show($divContextMenu, event);
            },
            setEvent: function (div) {
                _setEvent(div);
            },
            showHideMenuElements: function (isClipbrdEmpty, isCntrlClickedTypeSameAsClipbrd) {
                _showHideMenuElements(isClipbrdEmpty, isCntrlClickedTypeSameAsClipbrd);
            },
            processShowHideMenuElements: function (control, form) {
                _processShowHideMenuElements(control, form);
            }
        };
    })();

    var _generateCntrlForForm = (function () {
        function _cloneControl(control) {
            var objControl = $.parseJSON(JSON.stringify(control));
            objControl = MF_HELPERS.getControlAfterResetByType(objControl);
            //objControl.userDefinedName = $('#txtElementUserName').val();
            //objControl.id = $('#hdfElementName').val().split(',')[1];
            return objControl;
        }
        return {

            getNewControl: function () {
                var objClipbrdControl = mFicientIde.Cache.getControlToCopy();
                return _cloneControl(objClipbrdControl);
            }

        };
    })();
    var _generateCntrlForUI = (function () {

        return {
            getHtml: function (cntrlType) {
                //count IS A GLOBAL VARIABLE USED TO MAINTAIN THE COUNT OF CONTROLS IN FORM
                return getHtml(cntrlType.type, count);
            }
        };
    })();
    var _addNewCntrlToForm = (function () {

        return {
            /**
            * Adds the control to the form
            * @method add
            * @param {Control Object} newCntrl. The control that needs to be added.
            * @param {Control Object} currClickedCntrl. The control which is in focus(clicked)right now
            * @param {Form} form. The form to which the control is to be added
            * @return {void} undefined
            */
            add: function (newCntrl, currClickedCntrl, form) {
                var currCntrlRowPanel = null,
                    currCntrlClmPanel = null;
                currCntrlClmPanel = form.fnGetColPanelOfControl(currClickedCntrl.id);
                currCntrlRowPanel = form.fnGetRowPanelOfControl(currClickedCntrl.id);
                form.fnAddControl(
                    currCntrlRowPanel.fnGetId(),
                    currCntrlClmPanel.fnGetId(),
                    newCntrl);
            },
            addAfterDeletingCurrent: function (newCntrl, currClickedCntrl, form) {
                var currCntrlRowPanel = null,
                    currCntrlClmPanel = null;
                currCntrlClmPanel = form.fnGetColPanelOfControl(currClickedCntrl.id);
                currCntrlRowPanel = form.fnGetRowPanelOfControl(currClickedCntrl.id);
                form.fnDeleteControlObject(currClickedCntrl.id);
                form.fnAddControl(
                    currCntrlRowPanel.fnGetId(),
                    currCntrlClmPanel.fnGetId(),
                    newCntrl);
            }
        }
    })();
    var _addNewCntrlToUI = (function () {
        function _add(newCntrlType, currFocusDiv, pastePosition) {
            var strHtml = _generateCntrlForUI.getHtml(newCntrlType),
                $divToReturn = null;
            if (strHtml) {
                switch (pastePosition) {
                    case NEW_CNTRL_PASTE_POSITION.above:
                        currFocusDiv.before(strHtml);
                        $divToReturn = currFocusDiv.prev();
                        break;
                    case NEW_CNTRL_PASTE_POSITION.below:
                        currFocusDiv.after(strHtml);
                        $divToReturn = currFocusDiv.next();
                        break;
                }
            }
            return $divToReturn;
        }
        return {
            /**
            * Adds the control to the UI above the control which is in focus right now
            * @method add
            * @param {CONSTANTS.CONTROL} newCntrlType. The type of control that needs to be added.
            * @param {jquery Object} currFocusDiv. The div above which the new html is to be added.
            * @return {jquery Object} return the jquery object of the HTML added.
            *   if nothing is added then NULL is returned
            */
            addAbove: function (newCntrlType, currFocusDiv) {
                return _add(newCntrlType, currFocusDiv, NEW_CNTRL_PASTE_POSITION.above);
            },
            /**
            * Adds the control to the UI below the control which is in focus right now
            * @method add
            * @param {CONSTANTS.CONTROL} newCntrlType. The type of control that needs to be added.
            * @param {jquery Object} currFocusDiv. The div above which the new html is to be added.
            * @return {jquery Object} return the jquery object of the HTML added.
            * if nothing is added then NULL is returned
            */
            addBelow: function (newCntrlType, currFocusDiv) {
                return _add(newCntrlType, currFocusDiv, NEW_CNTRL_PASTE_POSITION.below);
            }
        }
    })();
    function _processCopyClick(evnt) {
        var $menuDiv = _getContextMenuDiv(),
             strCntrlId = _globalData.getCntrlId($menuDiv),
             objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
             objControl = null;
        objControl = objForm.fnGetControlFromId(strCntrlId);
        mFicientIde.Cache.setControlToCopy(objControl);
        _globalData.removeData($menuDiv);
        closeModalPopUp($menuDiv.attr('id'));
    }
    function _processPaste(evnt, pastePos) {
        var $menuDiv = _getContextMenuDiv(),
            $currFocusDiv = $('#' + _globalData.getCurrFocusCntrlDiv($menuDiv)),
            objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
            objCurrFocusCntrl = MF_HELPERS.currentObjectHelper.getCurrentWorkingControl(),
            objNewCntrl = null,
            $newCntrlDiv = $([]);
        //THIS IS A GLOBAL VARIABLE MAINTAAINED FOR COUNT OF CONTROL
        //THE PROCESS STEP WAS REPLICATED FROM DRAG AND DROP FUNCTIONS
        count = count + 1;
        objNewCntrl = _generateCntrlForForm.getNewControl();
        var aryErrors = objForm.fnValidateNewControlAddition(objNewCntrl.type);
        if (aryErrors.length > 0) {
            throw new MfError(aryErrors);
        }
        //WE CAN GET THE ID AND NAME ONLY AFTER HTML IS GENERATED HENCE THIS IS DONE BEFORE
        switch (pastePos) {
            case NEW_CNTRL_PASTE_POSITION.above:
                $newCntrlDiv = _addNewCntrlToUI.addAbove(objNewCntrl.type, $currFocusDiv);
                break;
            case NEW_CNTRL_PASTE_POSITION.below:
                $newCntrlDiv = _addNewCntrlToUI.addBelow(objNewCntrl.type, $currFocusDiv);
                break;
        }
        objNewCntrl.userDefinedName = $('#txtElementUserName').val();
        objNewCntrl.id = $('#hdfElementName').val().split(',')[1];
        objNewCntrl.isNew = true;
        objNewCntrl.oldName = "";
        _addNewCntrlToForm.add(objNewCntrl, objCurrFocusCntrl, objForm);
        if (objForm.fnIsTablet()) $('.TbOuterCont').equalHeights();
        eventOfOuterDiv('#outerDiv' + count);
        ControlDivMouseOverAndOut('outerDiv' + count);
        _contextMenu.setEvent($('#outerDiv' + count));
        $('#outerDiv' + count).click();
        _globalData.removeData($menuDiv);
        closeModalPopUp($menuDiv.attr('id'));
    }
    function _processPasteStyleClick(evnt) {
        var $menuDiv = _getContextMenuDiv(),
            $currFocusDiv = $('#' + _globalData.getCurrFocusCntrlDiv($menuDiv)),
            objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
            objCurrFocusCntrl = MF_HELPERS.currentObjectHelper.getCurrentWorkingControl(),
            objNewCntrl = null;

        objNewCntrl = _generateCntrlForForm.getNewControl();
        objNewCntrl.id = objCurrFocusCntrl.id;
        objNewCntrl.userDefinedName = objCurrFocusCntrl.userDefinedName;
        objNewCntrl.isNew = objCurrFocusCntrl.isNew;
        objNewCntrl.oldName = objCurrFocusCntrl.oldName;
        _addNewCntrlToForm.addAfterDeletingCurrent(objNewCntrl, objCurrFocusCntrl, objForm);
        $currFocusDiv.click();
        _globalData.removeData($menuDiv);
        closeModalPopUp($menuDiv.attr('id'));
    }
    function _processPasteAboveClick(evnt) {
        _processPaste(evnt, NEW_CNTRL_PASTE_POSITION.above);
    }
    function _processPasteBelowClick(evnt) {
        _processPaste(evnt, NEW_CNTRL_PASTE_POSITION.below);
    }
    return {
        contextMenu: {
            show: function (event) {
                _contextMenu.show(event);
            },
            setEvent: function (div) {
                _contextMenu.setEvent(div);
            },
            processShowHideMenuElements: function (control, form) {
                _contextMenu.processShowHideMenuElements(control, form);
            }
        },
        processCopyClick: function (evnt) {
            _processCopyClick(evnt);
        },
        processPasteAboveClick: function (evnt) {
            _processPasteAboveClick(evnt);
        },
        processPasteBelowClick: function (evnt) {
            _processPasteBelowClick(evnt);
        },
        processPasteStyleClick: function (evnt) {
            _processPasteStyleClick(evnt);
        }
    };
})();
$(document).ready(function () {
    $('#divFrmCntrlCopyContextMenu').on('click', function (evnt) {
        try {
            mFicientIde.MfApp.form.CntrlCopyPaste.processCopyClick(evnt);
            evnt.stopPropagation();
        }
        catch (error) {
            if (error instanceof MfError) {
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
            } else {
                console.log(error);
            }
        }
    });
    $('#divFrmCntrlPasteAboveContextMenu').on('click', function (evnt) {
        try {
            mFicientIde.MfApp.form.CntrlCopyPaste.processPasteAboveClick(evnt);
            evnt.stopPropagation();
        }
        catch (error) {
            if (error instanceof MfError) {
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
            } else {
                console.log(error);
            }
        }
    });
    $('#divFrmCntrlPasteBelowContextMenu').on('click', function (evnt) {
        try {
            mFicientIde.MfApp.form.CntrlCopyPaste.processPasteBelowClick(evnt);
            evnt.stopPropagation();
        }
        catch (error) {
            if (error instanceof MfError) {
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
            } else {
                console.log(error);
            }
        }
    });
    $('#divFrmCntrlPasteStyleContextMenu').on('click', function (evnt) {
        try {
            mFicientIde.MfApp.form.CntrlCopyPaste.processPasteStyleClick(evnt);
            evnt.stopPropagation();
        }
        catch (error) {
            if (error instanceof MfError) {
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
            } else {
                console.log(error);
            }
        }
    });
});
var mfIdeDesigner = "mfIdeDesigner";
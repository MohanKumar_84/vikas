﻿
var cntrlCount = 1;
var dragStart = false;
var pvsCntrlId = '';
var pvsSelDiv = '';

var childForm;
var app = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();

function initializeChildForm() {
    $('[id$=hdfIsChildForm]').val('true');
    OnToolboxControlMouseOver();
    $("#ControlsContainer .parentDivDraggable").draggable({
        appendTo: "body",
        helper: "clone",
        refreshPositions: true,
        start: createDrops, 
        cursorAt: { left: -10, top: -10 }

    });
    $("#ControlsContainer .parentDivDraggable").draggable("option", "scroll", false);

    cntrlCount = 1;
    dragStart = false;
    pvsCntrlId = '';
    $('#ControlPropetiesDiv').html('');

    ShowFormDesigner();
    if (pvsSelDiv.length > 0) {
        $(pvsSelDiv).removeClass('draggableElementSelcted');
    }
    pvsSelDiv = '';
}

function addNewChildForm() {
    $('#txtAddViewSection').val("Section" + cntrlCount);

    var hdRowPanel = new RowPanel();
    hdRowPanel.isDeleted = false;
    hdRowPanel.id = "Sec_hd";
    hdRowPanel.userDefinedName = "Sec_hd_variables";
    var hdColumnPanel = new ColumnPanel("0", "Sec_hd_Col", "Sec_hd_variables_Col", "1", [], [], false);
    hdRowPanel.fnAddColumnPanel(hdColumnPanel);
    childForm.fnAddRowPanels(hdRowPanel);

    var rowPanel = new RowPanel();
    rowPanel.index = cntrlCount;
    rowPanel.isDeleted = false;
    rowPanel.userDefinedName = $('#txtAddViewSection').val();
    if (childForm.isTablet) {
        $('#TBmainFormbox').show();
        $('#mainFormbox').hide();
        childForm.formModelType = MF_IDE_CONSTANTS.IDE_APP_MODEL_TYPE.tablet;
        var strHtml = '<div id="TBGB_' + cntrlCount + '" class="TBcontainerOuterDiv"><div>'
                        + '<img id="imgSettings_' + cntrlCount + '" src="//enterprise.mficient.com/images/settings.png" alt="move" style="cursor:pointer;" />'
                        + '<img id="imgGpbDelete_' + cntrlCount + '"  alt="delete" src="//enterprise.mficient.com/images/cross.png" title="Delete Control" style="cursor:pointer;" />'
                        + '</div><div id="Sec_' + cntrlCount + '"  class="TbOuterCont">' + GetInnerHtmlOfGroupBox($('#ddlAddVeiwSecTemp').val(), 'Sec_' + cntrlCount + '_' + $('#ddlAddVeiwSecTemp').val()) + '</div>'
                        + '<div class="clear"></div></div>';
        $('#TbFormDesignContainerDiv').html(strHtml);
        var secName = 'Sec_' + cntrlCount + '_1_1';
        childForm.isTablet = true;
        rowPanel.id = 'Sec_' + cntrlCount;
        rowPanel.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_100;
        var columnPanel = new ColumnPanel("1", secName, "Sec_" + cntrlCount + "_Col_1", "1", [], [], false);
        rowPanel.fnAddColumnPanel(columnPanel);

        DragGroupBoxToContainer('TbFormDesignContainerDiv');
        DragItemToContainer(secName);
    }
    else {
        $('#FormDesignContainerDiv').html('<div id="GB_' + cntrlCount + '" class="containerOuterDiv">'
                + '<div>'
                + '<img id="imgSettings_' + cntrlCount + '" src="//enterprise.mficient.com/images/settings.png" alt="move" style="cursor:pointer;" />'
                + '<img id="imgGpbDelete_' + cntrlCount + '"  alt="delete" src="//enterprise.mficient.com/images/cross.png" title="Delete Control" style="cursor:pointer;" />'
                + '</div><div id="containerDiv_' + cntrlCount + '"  class="containerDiv"  style="width: 98%;"></div></div>');
        $('#TBmainFormbox').hide();
        $('#mainFormbox').show();
        childForm.isTablet = false;
        childForm.formModelType = MF_IDE_CONSTANTS.IDE_APP_MODEL_TYPE.phone;
        rowPanel.id = "GB_" + cntrlCount;
        rowPanel.type = mFicientIde.MF_IDE_CONSTANTS.IDE_ROWPANEL_TYPE.TEMP_100;

        var columnPanel = new ColumnPanel("1", "containerDiv_" + cntrlCount, "containerDiv" + cntrlCount + "_Col", "1", [], [], false);
        rowPanel.fnAddColumnPanel(columnPanel);

        DragGroupBoxToContainer('FormDesignContainerDiv');
        DragItemToContainer('containerDiv_' + cntrlCount);
    }

    SettingButtonClick(cntrlCount);
    DeleteGroupbox(cntrlCount);
    childForm.fnAddRowPanels(rowPanel);
    $('#FrmInnerPropertieDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Form, editObject: childForm });
    $('#ControlPropetiesDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Section, editObject: rowPanel });
}

function DragGroupBoxToContainer(containerID) {
    $('#' + containerID).droppable({
        activeClass: "ui-state-default",
        accept: ":not(.ui-sortable-helper)",
        accept: ":not(.draggableElement)",
        hoverClass: "formDroppableHover",
        drop: function (event, ui) {
            IsFormChange = true;
            cntrlCount = cntrlCount + 1;
            if (ui.draggable[0].children[0] != null && ui.draggable[0].children[0].attributes[0] != null) {
                if ($(ui.draggable[0].childNodes).find('img')[0] != undefined && $(ui.draggable[0].childNodes).find('img')[0] != null) {
                    var strXml = getHtmlForMainContainer($(ui.draggable[0].childNodes).find('img')[0].id, cntrlCount, childForm);
                    if (strXml[0] != undefined) {
                        if (strXml[0].trim().length > 0) {
                            $(this).append(strXml[0]);
                            //var outerDivid = "#containerDiv_" + cntrlCount;
                            cntrlCount = parseInt(cntrlCount, 10) + 1;
                            $('#txtGbUserName').val('Section' + cntrlCount);
                            btnPanelTempSelect();
                            //SubProcGroupBoxTemp(true);
                        }
                    }
                }
            }
        }
    })
    .sortable({
        handle: '.handle'
    });
    $('#' + containerID).sortable("option", "scroll", false);
}

function DragItemToContainer(containerID) {
    $('#' + containerID).bind("dropover", function (event, ui) {
        if (ui.draggable[0].id == 'draggableGroupboxDiv' || ui.draggable[0].id.length == 0) {
            return;
        }

        if (ui.draggable[0].id.indexOf("draggable") >= 0) {
            dragStart = true;
        }
        if (ui.draggable[0].id == 'PieChartDiv' || ui.draggable[0].id == 'BarGraphDiv' || ui.draggable[0].id == 'TableDiv' || ui.draggable[0].id == 'LineChartDiv'
        || ui.draggable[0].id == 'AngularGuageDiv' || ui.draggable[0].id == 'CylinderGuageDiv' || ui.draggable[0].id == 'SpacerDiv' || ui.draggable[0].id == 'PictureDiv' || ui.draggable[0].id == 'SignatureDiv') {
            dragStart = true;
        }

    });
    $('#' + containerID).bind("dropout", function (event, ui) {
        //dragStart = false;
    });
    $('#' + containerID).droppable({
        activeClass: "ui-state-default",
        accept: ":not(.ui-sortable-helper)",
        hoverClass: "formDroppableHover",
        drop: function (event, ui) {
            dragStart = false;
            cntrlCount = cntrlCount + 1;
            var strXml = "";
            if ($(ui.draggable[0].childNodes).find('img')[0] != undefined && $(ui.draggable[0].childNodes).find('img')[0] != null) {
                var strXml = getHtml($(ui.draggable[0].childNodes).find('img')[0].id, cntrlCount);
            }

            if (strXml == undefined || strXml.length == 0) {
                cntrlCount = cntrlCount - 1;
                return;
            }

            if (pvsCntrlId.length > 0) {
                $(pvsCntrlId).before(strXml);
                pvsCntrlId = '';
                btnElementNameOkClick();
            }
            else {
                $(this).append(strXml);
                btnElementNameOkClick();
            }
            if (childForm.isTablet) $('.TbOuterCont').equalHeights();
            var outerDivid = "#outerDiv" + cntrlCount;
            eventOfOuterDiv('#outerDiv' + cntrlCount);
            ControlOuterDivMouseOverAndOut('outerDiv' + cntrlCount);
            
//            var hgt = window.getComputedStyle(document.getElementById(event.target.id)).height.replace("px", "");
//            $('#' + event.target.id).css('min-height', parseInt(hgt, 10) + 50 + 'px');
            $('#outerDiv' + cntrlCount).click();
        }
    })
   .sortable({
       items: "div:not(.node)",
       sort: function () {
           $(this).removeClass("ui-state-default");
       }
   });
   $('#' + containerID).sortable("option", "scroll", false);
   DeleteAndSortControls('#' + containerID);
}

function ControlOuterDivMouseOverAndOut(Id) {
    $('#' + Id).droppable({
        hoverClass: "MouseOverOuterDiv",
        refreshPositions: true,
        over: function (event, ui) {
            pvsCntrlId = '#' + this.id;
        },
        out: function (event, ui) {
            if ($('#' + this.id)[0].className == "outerDivChange") {
                $('#' + this.id).attr("class", "outerDivChange");
                return;
            }
            $('#' + this.id).attr("class", "outerDiv");
            pvsCntrlId = '';
        },
        drop: function (event, ui) {
            pvsCntrlId = '#' + this.id;
        }
    }).sortable({
        handle: '.handle'
    });
    $('#' + Id).sortable("option", "scroll", false);


    $('#' + Id).bind('mouseover', function () {
        if (!dragStart) {
            if ($('#' + this.id)[0].className == "outerDivChange") {
                return;
            }
            $('#' + this.id).attr("class", "MouseOverOuterDivChange");
        }
        else
            pvsCntrlId = '#' + this.id;
    });
    $('#' + Id).bind('mouseout', function () {
        if (!dragStart) {
            if ($('#' + this.id)[0].className == "outerDivChange") {
                $('#' + this.id).attr("class", "outerDivChange");
                return;
            }
            $('#' + this.id).attr("class", "outerDiv");
        }
    });
}

function childFormEdit() {
    $('[id$=hdfIsChildForm]').val('true');
    initializeChildForm();
    $('#TbFormDesignContainerDiv').html('');
    $('#FormDesignContainerDiv').html('');
    $('#FrmInnerPropertieDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Form, editObject: childForm });
    if (childForm.isTablet) {
        $('#TBmainFormbox').show();
        $('#mainFormbox').hide();
        createChildFormTabletLayout();
    }
    else {
        $('#TBmainFormbox').hide();
        $('#mainFormbox').show();
        createChildFormPhoneLayout();
    }
}

function createChildFormPhoneLayout() {
    var strHtml = '';
    var hgt = 0;
    var arryIndex = [];
    $.each(childForm.rowPanels, function (rowIndex, objRowPanel) {
        strHtml = '';
        if (objRowPanel != undefined && objRowPanel != null) {
            if (objRowPanel.isDeleted == false && (objRowPanel.colmnPanels != null || objRowPanel.colmnPanels != undefined) && objRowPanel.colmnPanels.length > 0 && objRowPanel.id != "Sec_hd") {
                strHtml += '<div id="' + objRowPanel.id + '" class="containerOuterDiv">'
                + '<div>'
                + '<img id="imgSettings_' + objRowPanel.index + '" src="//enterprise.mficient.com/images/settings.png" alt="move" style="cursor:pointer;" />'
                + '<img id="imgGpbDelete_' + objRowPanel.index + '"  alt="delete" src="//enterprise.mficient.com/images/cross.png" title="Delete Control" style="cursor:pointer;" />'
                + '</div>';
                cntrlCount = parseInt(objRowPanel.index);
                $.each(objRowPanel.colmnPanels, function (colIndex, objColPanel) {
                    if (objColPanel != undefined && objColPanel != null) {
                        if (objColPanel.isDeleted == false) {
                            strHtml += '<div id="' + objColPanel.id + '"  class="containerDiv"  style="width: 98%;"></div></div>';
                            $('#FormDesignContainerDiv').append(strHtml);
                            if ((objColPanel.controls != null || objColPanel.controls != undefined) && objColPanel.controls.length > 0) {
                                $.each(objColPanel.controls, function (cntrlIndex, objControl) {
                                    if (objControl != undefined && objControl != null) {
                                        if (objControl.isDeleted == false) {
                                            var strControlHtml = getHtml(objControl.type.type, objControl.id.split('_')[1]);
                                            $('#' + objColPanel.id).append(strControlHtml);
                                            cntrlCount = parseInt(objControl.id.split('_')[1]);
                                            arryIndex.push(cntrlCount);
                                            eventOfOuterDiv('#outerDiv' + cntrlCount);
                                            ControlOuterDivMouseOverAndOut('outerDiv' + cntrlCount);
                                            $('#outerDiv' + cntrlCount).click();
                                        }
                                    }
                                });

//                                if (document.getElementById(objColPanel.id) != undefined) {
//                                    hgt = window.getComputedStyle(document.getElementById(objColPanel.id)).height.replace("px", "");
//                                    $('#' + objColPanel.id).css('min-height', parseInt(hgt, 10) + 50 + 'px');
//                                }
                            }

                            DragItemToContainer(objColPanel.id);
                        }
                    }
                });

                SettingButtonClick(objRowPanel.index);
                DeleteGroupbox(objRowPanel.index);
            }
        }

    });
    $('#outerDiv' + cntrlCount).click();
    DragGroupBoxToContainer('FormDesignContainerDiv');
    $('#FrmInnerPropertieDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Form, editObject: childForm });
    if (arryIndex.length > 0) cntrlCount = Math.max.apply(Math, arryIndex) + 1;
    else cntrlCount = 1;
}

function createChildFormTabletLayout() {
    var strHtml = '';
    var hgt = 0;
    var arryIndex = [];
    $.each(childForm.rowPanels, function (rowIndex, objRowPanel) {
        strHtml = '';
        if (objRowPanel.isDeleted == false && (objRowPanel.colmnPanels != null || objRowPanel.colmnPanels != undefined) && objRowPanel.colmnPanels.length > 0 && objRowPanel.id != "Sec_hd") {
            strHtml += '<div id="TBGB_' + objRowPanel.index + '" class="TBcontainerOuterDiv"><div>'
                        + '<img id="imgSettings_' + objRowPanel.index + '" src="//enterprise.mficient.com/images/settings.png" alt="move" style="cursor:pointer;" />'
                        + '<img id="imgGpbDelete_' + objRowPanel.index + '"  alt="delete" src="//enterprise.mficient.com/images/cross.png" title="Delete Control" style="cursor:pointer;" />'
                        + '</div><div id="' + objRowPanel.id + '"  class="TbOuterCont"></div>'
                        + '<div class="clear"></div></div>';
            $('#TbFormDesignContainerDiv').append(strHtml);
            cntrlCount = parseInt(objRowPanel.index);
            $.each(objRowPanel.colmnPanels, function (colIndex, objColPanel) {
                if (objColPanel.isDeleted == false) {
                    $('#' + objRowPanel.id).append(GetInnerHtmlOfGroupColumns(objColPanel.id, objColPanel.index, objRowPanel.type.id));
                    if ((objColPanel.controls != null || objColPanel.controls != undefined) && objColPanel.controls.length > 0) {
                        $.each(objColPanel.controls, function (cntrlIndex, objControl) {
                            if (objControl.isDeleted == false) {
                                var strControlHtml = getHtml(objControl.type.type, objControl.id.split('_')[1]);
                                $('#' + objColPanel.id).append(strControlHtml);
                                cntrlCount = parseInt(objControl.id.split('_')[1]);
                                arryIndex.push(cntrlCount);
                                eventOfOuterDiv('#outerDiv' + cntrlCount);
                                ControlOuterDivMouseOverAndOut('outerDiv' + cntrlCount);
                                $('#outerDiv' + cntrlCount).click();
                            }
                        });

//                        if (document.getElementById(objColPanel.id) != undefined) {
//                            hgt = window.getComputedStyle(document.getElementById(objColPanel.id)).height.replace("px", "");
//                            $('#' + objColPanel.id).css('min-height', parseInt(hgt, 10) + 50 + 'px');
//                        }
                    }

                    DragItemToContainer(objColPanel.id);
                }
            });

            SettingButtonClick(objRowPanel.index);
            DeleteGroupbox(objRowPanel.index);
        }

    });
    $('#outerDiv' + cntrlCount).click();
    DragGroupBoxToContainer('TbFormDesignContainerDiv');
    $('#FrmInnerPropertieDiv').mficientCntrlProps({ propertySheetJson: mFicientIde.PropertySheetJson.Form, editObject: childForm });
    if (arryIndex.length > 0) cntrlCount = Math.max.apply(Math, arryIndex) + 1;
    else cntrlCount = 1;
}

var mfIdeChildForm = "mfIdeChildForm";
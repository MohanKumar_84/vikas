﻿
/*---------------------------------------------------ToggleSwitch---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName);
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName);
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25)
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    else
                        $textAreaLabel.val(strDescription);
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) !== false) {
                        if (propObject.userDefinedName !== undefined || propObject.userDefinedName !== null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            BindValueToProperty: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Appearance.getBindValueToPropertyCntrl();
                if ($('[id$=hdfIsChildForm]').val() == 'true') {
                    var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                    var strOptionsHtml = '<option value="-1">Select Item</option>';
                    if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) {
                        $.each(eltCntrl.properties, function () {
                            strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>';
                        });
                    }
                    $(control).html(strOptionsHtml);
                    if (appObj.bindValueToProperty != undefined) $(control).val(appObj.bindValueToProperty);
                    else $(control).val('-1');
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            BindTextToProperty: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Appearance.getBindTextToPropertyCntrl();
                if ($('[id$=hdfIsChildForm]').val() == 'true') {
                    var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                    var strOptionsHtml = '<option value="-1">Select Item</option>';
                    if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) {
                        $.each(eltCntrl.properties, function () {
                            strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>';
                        });
                    }
                    $(control).html(strOptionsHtml);
                    if (appObj.bindTextToProperty != undefined) $(control).val(appObj.bindTextToProperty);
                    else $(control).val('-1');
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            LabelText: function (control, appObj) {
                $(control).val(appObj.labelText);
                if (appObj.labelText != undefined && appObj.labelText != null && appObj.labelText.length > 0) {
                    $('#' + appObj.id + '_Label').html(appObj.labelText);
                }
                else {
                    $('#' + appObj.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");
                }
            },
            FontStyle: function (control, appObj) {
                if (appObj.fontStyle == undefined || appObj.fontStyle == null)
                    appObj.fontStyle = "0";
                $(control).val(appObj.fontStyle);
                var lblStyle = getTextFontStyle(appObj);
                $('#' + appObj.id + '_Label').removeClass();
                $('#' + appObj.id + '_Label').addClass(lblStyle);
            },
            FontColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Appearance.getFontColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelValueProperty = $('#' + appObj.id + '_Label');
                if (appObj.fontColor != undefined || appObj.fontColor != null) {
                    $(currentCntrl.control).val(appObj.fontColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.fontColor);
                    $($LabelValueProperty[0]).css('color', '#' + appObj.fontColor + '!important');
                }

                var fontCol = '';
                var fontColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            fontCol = '#' + hex;
                        }
                        $(currentCntrl.control).val(fontCol);
                        $(currentCntrl.browseBtn).css('background-color', fontCol);
                        appObj.fontColor = $(currentCntrl.control).val();
                        if (fontCol.length > 0) {
                            $($LabelValueProperty[0]).css('color', fontCol + ' !important');
                        }
                        else {
                            $($LabelValueProperty[0]).css('color', '#000000' + ' !important');
                        }
                    }
                });
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            SwitchText: function (control, appObj) {
                $(control).val(appObj.switchText);
            },
            DefaultState: function (control, appObj) {
                var options = '';
                switch (appObj.switchText) {
                    case "0":
                        options += '<option value="On">ON</option>'
                        + '<option value="Off">OFF</option>';
                        break;
                    case "1":
                        options += '<option value="Yes">YES</option>'
                        + '<option value="No">NO</option>';
                        break;
                    case "2":
                        options += '<option value="True">TRUE</option>'
                        + '<option value="False">FALSE</option>';
                        break;
                    case "3":
                        options += '<option value="OnText" selected="selected">ON Text</option>'
                            + '<option value="OffText">OFF Text</option>';
                        break;
                }
                $(control).html(options);
                $(control).val(appObj.defaultState);
                if (appObj.switchText && appObj.switchText.trim() != "3")
                    $('#' + appObj.id + '_Val').html(appObj.defaultState + "&nbsp; ");
                else if (appObj.defaultState) {
                    if (appObj.defaultState.trim() == "OnText" && appObj.customOnText)
                        $('#' + appObj.id + '_Val').html(appObj.customOnText.substring(0, 6) + "&nbsp; ");
                    else if (appObj.defaultState.trim() == "OffText" && appObj.customOffText)
                        $('#' + appObj.id + '_Val').html(appObj.customOffText.substring(0, 6) + "&nbsp; ");
                }
            },
            Disabled: function (control, appObj) {
                if (appObj.disabled) {
                    $(control).val(appObj.disabled);
                }
            },
            OnText: function (control, appObj) {
                $(control).val(appObj.customOnText);
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Appearance.getOnTextCntrl();
                if (appObj.switchText && appObj.switchText.trim() == "3") {
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            OffText: function (control, appObj) {
                $(control).val(appObj.customOffText);
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Appearance.getOffTextCntrl();
                if (appObj.switchText && appObj.switchText.trim() == "3") {
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            Size: function (control, cntrlObj) {
                if (!cntrlObj.dataMini)
                    cntrlObj.dataMini = "0";
                var size = cntrlObj.fnGetDataMini();
                $(control).val(size);
                setDataMiniToggleUI(cntrlObj);
            },
            eventHandlers: {
                BindValueToProperty: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Appearance.getBindValueToPropertyCntrl();
                    var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (objform.fnAddBindProperty(evnt.target.value, propObject.bindValueToProperty, propObject))
                        propObject.bindValueToProperty = evnt.target.value;
                    else {
                        if (propObject.bindValueToProperty != undefined) evnt.target.value = propObject.bindValueToProperty;
                        else evnt.target.value = '-1';
                    }
                },
                BindTextToProperty: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Appearance.getBindTextToPropertyCntrl();
                    var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (objform.fnAddBindProperty(evnt.target.value, propObject.bindTextToProperty, propObject))
                        propObject.bindTextToProperty = evnt.target.value;
                    else {
                        if (propObject.bindTextToProperty != undefined) evnt.target.value = propObject.bindTextToProperty;
                        else evnt.target.value = '-1';
                    }
                },
                LabelTextChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.labelText = evnt.target.value;
                    if (evnt.target.value.length > 0)
                        $('#' + propObject.id + '_Label').html(evnt.target.value);
                    else
                        $('#' + propObject.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");
                },
                FontStyleChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fontStyle = evnt.target.value;
                    var lblStyle = getTextFontStyle(propObject);
                    $('#' + propObject.id + '_Label').removeClass();
                    $('#' + propObject.id + '_Label').addClass(lblStyle);
                },
                ToggleText: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.switchText = evnt.target.value;
                    var cntrls = evnt.data.cntrls;
                    var $DefaultState = $('#' + cntrls.DefaultState.controlId);
                    var $OnTextId = $('#' + cntrls.OnText.controlId);
                    var $OffTextId = $('#' + cntrls.OffText.controlId);
                    var $OnTextWrapper = $('#' + cntrls.OnText.wrapperDiv);
                    var $OffTextWrapper = $('#' + cntrls.OffText.wrapperDiv);

                    $OnTextWrapper.hide();
                    $OffTextWrapper.hide();

                    var options = '';
                    if (propObject.switchText) {
                        switch (propObject.switchText) {
                            case "0":
                                options += '<option value="On" selected="selected">ON</option>'
                            + '<option value="Off">OFF</option>';
                                propObject.defaultState = "On";
                                break;
                            case "1":
                                options += '<option value="Yes" selected="selected">YES</option>'
                            + '<option value="No">NO</option>';
                                propObject.defaultState = "Yes";
                                break;
                            case "2":
                                options += '<option value="True" selected="selected">TRUE</option>'
                            + '<option value="False">FALSE</option>';
                                propObject.defaultState = "True";
                                break;
                            case "3":
                                propObject.customOnText = "On Text";
                                propObject.customOffText = "Off Text";
                                $OnTextId.val(propObject.customOnText);
                                $OffTextId.val(propObject.customOffText);
                                $OnTextWrapper.show();
                                $OffTextWrapper.show();
                                options += '<option value="OnText" selected="selected">ON Text</option>'
                            + '<option value="OffText">OFF Text</option>';
                                propObject.defaultState = "OnText";
                                break;
                        }
                    }
                    $DefaultState.html(options);
                    if (propObject.switchText && propObject.switchText.trim() != "3")
                        $('#' + propObject.id + '_Val').html(propObject.defaultState + "&nbsp; ");
                    else if (propObject.customOnText) {
                        $('#' + propObject.id + '_Val').html(propObject.customOnText.substring(0, 6) + "&nbsp; ");
                    }
                },
                ToggleDefaultState: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.defaultState = evnt.target.value;
                    if (propObject.switchText && propObject.switchText.trim() != "3")
                        $('#' + propObject.id + '_Val').html(propObject.defaultState + "&nbsp; ");
                    else if (propObject.defaultState) {
                        if (propObject.customOnText && propObject.defaultState.trim() == "OnText")
                            $('#' + propObject.id + '_Val').html(propObject.customOnText.substring(0, 6) + "&nbsp; ");
                        else if (propObject.customOffText && propObject.defaultState.trim() == "OffText")
                            $('#' + propObject.id + '_Val').html(propObject.customOffText.substring(0, 6) + "&nbsp; ");
                    }
                },
                Disabled: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.disabled = evnt.target.value;
                },
                OnTextChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.customOnText = evnt.target.value;
                    if (propObject.switchText.trim() == "3" && propObject.defaultState.trim() == "OnText" && propObject.customOnText != undefined)
                        $('#' + propObject.id + '_Val').html(propObject.customOnText.substring(0, 6) + "&nbsp; ");
                },
                OffTextChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.customOffText = evnt.target.value;
                    if (propObject.switchText.trim() == "3" && propObject.defaultState.trim() == "OffText" && propObject.customOffText != undefined)
                        $('#' + propObject.id + '_Val').html(propObject.customOffText.substring(0, 6) + "&nbsp; ");
                },
                Size: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fnSetDataMini(evnt.target.value);
                    setDataMiniToggleUI(propObject);
                }
            }
        },
        Data: {
            Databinding: function (control, appObj) {
                //$(control).val(appObj.bindType.id);
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += ' <option value="0">None</option>';
                    strOptions += '<option value="6">Offline Database</option>';
                    control.html(strOptions);
                }
                else {
                }
                $(control).val(appObj.bindType.id);
            },
            DataObject: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Data.getDataObjectCntrl();
                if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
                if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) {
                    $.each(appObj.databindObjs, function () {
                        if (this['name'].length > 0)
                            $(currentCntrl.control).val(this['name']);
                        else
                            $(currentCntrl.control).val('Select Object');
                        switch (this['bindObjType']) {
                            case "1":
                                mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "2":
                                mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "5":
                                mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control);
                                break;
                        }
                    });
                }
                else {
                    $(currentCntrl.control).val('Select Object');
                }
            },
            eventHandlers: {
                DataObject: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv);
                    var $DataObjectControl = $('#' + cntrls.DataObject.controlId);
                    var dataObject = new DatabindingObj();
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    if (evnt.target.value == "0") {
                        $ViewProperty.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        dataObject.bindObjType = bindType.id;
                    }
                    if (evnt.target.value == "1") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "2") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "5") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "6") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                        dataObject.bindObjType = bindType.id;
                    }
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            if (this.bindObjType != bindType.id) {
                                this.id = "";
                                this.name = "";
                                this.cmdParams = [];
                                propObject.displayText = "";
                                this.bindObjType = bindType.id;
                                $($DataObjectControl).val("Select Object");
                            }
                        });
                    }
                    else {
                        var dataObject = new DatabindingObj("", [], "0", bindType.id, "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        $($DataObjectControl).val("Select Object");
                    }

                    propObject.bindType = bindType;
                },
                DataBinding: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Data.getDataObjectCntrl();
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                    intlJson = [],
                    isEdit = false;
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            var objDatabindObj = jQuery.extend(true, {}, this);
                            if (this['name'].length > 0)
                                $(currentCntrl.control).val(this['name']);
                            else
                                $(currentCntrl.control).val('Select Object');
                            switch (this['bindObjType']) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:
                                    //                                    intlJson = MF_HELPERS
                                    //                                                .intlsenseHelper
                                    //                                                .getControlDatabindingIntellisenseJson(
                                    //                                                    objCurrentForm, propObject.id, true
                                    //                                                );
                                    if (objDatabindObj != null
                                        && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                        ) {
                                        isEdit = true;
                                    }

                                    mFicientIde
                                    .MF_DATA_BINDING
                                    .processDataBindingByObjectType({
                                        controlType: MF_IDE_CONSTANTS.CONTROLS.TOGGLE,
                                        databindObject: objDatabindObj,
                                        objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                        isEdit: isEdit,
                                        intlsJson: intlJson,
                                        control: propObject
                                    });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 420, false);
                                    break;
                            }
                        });
                    }
                    else {
                        $(currentCntrl.control).val('Select Object');
                    }
                }
            }
        },
        Advance: {
            Scripts: function (control, appObj) {
            },
            eventHandlers: {
                Scripts: function (evnt) {
                    bindAdvanceScriptsSelectors(evnt);
//                    $('#drpCntrlEvents').html();
//                    $('#drpCntrlEvents').css('width', '');
//                    var evntData = evnt.data;
//                    var propObject = evnt.data.propObject;
//                    $('#spanOnChngUserDefinedName').text(propObject.userDefinedName);
//                    _jsAceEditorOnChang.setTextInEditor("");
//                    var scriptOnChange = '';
//                    var options = '<option value="onChange">onChange</option>';
//                    $('#drpCntrlEvents').html(options);
//                    $('#drpCntrlEvents').val("onChange");
//                    $.uniform.update('#drpCntrlEvents');

//                    if (propObject.onChange != null && propObject.onChange != undefined) scriptOnChange = propObject.onChange;
//                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));

//                    SubProcShowCntrlJsEditor(true, "Scripts");
//                    $('[id$=btnCntrlJsSave]').unbind('click');
//                    $('[id$=btnCntrlJsSave]').bind('click', function () {
//                        scriptOnChange = _jsAceEditorOnChang.getText();
//                        if (scriptOnChange != null && scriptOnChange != undefined) propObject.onChange = Base64.encode(scriptOnChange);
//                        SubProcShowCntrlJsEditor(false);
//                        return false;
//                    });
//                    $('[id$=btnCntrlJsCancel]').unbind('click');
//                    $('[id$=btnCntrlJsCancel]').bind('click', function () {
//                        SubProcShowCntrlJsEditor(false);
//                        return false;
//                    });

                    //mFicientIde.HiddenFieldControlHelper.formDetailsHtml(propObject);
                }
            }
        },
        Behaviour: {
            Update: function (control, appObj) {
                if ((appObj.refreshControls != undefined || appObj.refreshControls != null) && appObj.refreshControls.length > 0) $(control).val(appObj.refreshControls.length + " Control(s)");
                else $(control).val("0 Control(s)");
            },
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay)
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0") $(currentCntrl.wrapperDiv).show();
                else $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                RefreshControls: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Behaviour.getRefreshControls();
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            if (control.id != propObject.id && !control.isDeleted && isValidUpdateControl(control)) {
                                                controls.push(control.userDefinedName);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if (controls.length == 0) {
                        showMessage('Please add controls first.', DialogType.Error);
                        return;
                    }
                    else {
                        $('#OnChangeElmDiv').mficientRefreshCntrlsOnChng({ Controls: controls, SelectedControls: (propObject.refreshControls != undefined ? propObject.refreshControls : []) });
                        showModalPopUpWithOutHeader('SubProcOnChange', 'Controls', 280, false);
                        $('#OnChangeElmDiv').data("ControlId", propObject.id);
                    }

                    $('[id$=btnSaveRefreshControls]').unbind('click');
                    $('[id$=btnSaveRefreshControls]').bind('click', function () {
                        propObject.refreshControls = $('#OnChangeElmDiv').data("SelectedControls");
                        $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)");
                        $('#SubProcOnChange').dialog('close');
                        return false;
                    });

                    $('[id$=btnCancelRefreshControl]').unbind('click');
                    $('[id$=btnCancelRefreshControl]').bind('click', function () {
                        $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)");
                        $('#SubProcOnChange').dialog('close');
                        return false;
                    });
                },
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getFontColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TOGGLE.propPluginPrefix,
                    "Appearance", "FontColor");
            }
            return objControl;
        }
        function _getRefreshControlsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TOGGLE.propPluginPrefix,
                    "Behaviour", "Update");
            }
            return objControl;
        }
        function _getBindValueToPropertyCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TOGGLE.propPluginPrefix,
                    "Appearance", "BindValueToProperty");
            }
            return objControl;
        }
        function _getBindTextToPropertyCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TOGGLE.propPluginPrefix,
                    "Appearance", "BindTextToProperty");
            }
            return objControl;
        }
        function _getOnTextCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TOGGLE.propPluginPrefix,
                    "Appearance", "OnText");
            }
            return objControl;
        }
        function _getOffTextCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TOGGLE.propPluginPrefix,
                    "Appearance", "OffText");
            }
            return objControl;
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TOGGLE.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.TOGGLE.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }
        return {
            Appearance: {
                getBindValueToPropertyCntrl: _getBindValueToPropertyCntrl,
                getBindTextToPropertyCntrl: _getBindTextToPropertyCntrl,
                getOnTextCntrl: _getOnTextCntrl,
                getOffTextCntrl: _getOffTextCntrl,
                getFontColorCntrl: _getFontColorCntrl
            },
            Data: {
                getDataObjectCntrl: _getDataObjectCntrl
            },
            Behaviour: {
                getRefreshControls: _getRefreshControlsCntrl,
                getConditionsCntrl: _getConditionsCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.Toggle = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "ToggleSwitch",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Toggle.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Toggle.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
              {
                  "text": "Bind Value To Property",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "BindValueToProperty",
                  "defltValue": "0",
                  "validations": [

                       ],
                  "customValidations": [

                       ],
                  "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.BindValueToProperty,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": true
                  },
                  "options": [
                       ]
              },
              {
                  "text": "Bind Text To Property",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "BindTextToProperty",
                  "defltValue": "0",
                  "validations": [

                       ],
                  "customValidations": [

                       ],
                  "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.BindTextToProperty,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": true
                  },
                  "options": [
                       ]
              },
            {
                "text": "Label Text",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "LabelText",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.LabelTextChange,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "Font Style",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FontStyle",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.FontStyleChange,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Normal",
                      "value": "0"
                  },
                  {
                      "text": "Bold",
                      "value": "1"
                  },
                  {
                      "text": "Italics",
                      "value": "2"
                  },
                  {
                      "text": "Bold And Italics",
                      "value": "3"
                  }
               ]
              },
              {
                  "text": "Font Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "FontColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
            {
                "text": "Switch Text",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "SwitchText",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.ToggleText,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "DefaultState",
                               rtrnPropNm: "DefaultState"
                           },
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "OnText",
                               rtrnPropNm: "OnText"
                           },
                           {
                               grpPrefText: "Appearance",
                               cntrlPrefText: "OffText",
                               rtrnPropNm: "OffText"
                           }
                         ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "ON/OFF",
                      "value": "0"
                  },
                  {
                      "text": "YES/NO",
                      "value": "1"
                  },
                  {
                      "text": "TRUE/FALSE",
                      "value": "2"
                  },
                  {
                      "text": "Custom Text",
                      "value": "3"
                  }
               ]
            },
              {
                  "text": "On Text",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "OnText",
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [

               ],
                  "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.OnTextChange,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 250,
                      "disabled": false,
                      "defltValue": "On Text",
                      "hidden": true
                  }
              },
            {
                "text": "Off Text",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "OffText",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.OffTextChange,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "Off Text",
                    "hidden": true
                }
            },
            {
                "text": "Default State",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "DefaultState",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.ToggleDefaultState,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
               ]
            },
            {
                "text": "Disabled",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Disabled",
                "defltValue": "0",
                "validations": [
                    ],
                "customValidations": [

                       ],
                "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.Disabled,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                  ]
            },
            {
                "text": "Size",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Size",
                "defltValue": "0",
                "validations": [
                    ],
                "customValidations": [

                       ],
                "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.Size,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "Normal",
                        "value": MF_IDE_CONSTANTS.CNTRL_SIZES.normal
                    },
                    {
                        "text": "Mini",
                        "value": MF_IDE_CONSTANTS.CNTRL_SIZES.mini
                    }
                  ]
            }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
            {
                "text": "Databinding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Databinding",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Toggle.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "None",
                      "value": "0"
                  },
                  {
                      "text": "Database Object",
                      "value": "1"
                  },
                  {
                      "text": "Web Service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  },
                   {
                       "text": "Offline Database",
                       "value": "6"
                   }
               ]
            },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Toggle.groups.Data.eventHandlers.DataBinding,
                      "context": "",
                      "arguments": {}
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            }
         ]
      },
        {
            "name": "Advance",
            "prefixText": "Advance",
            "hidden": false,
            "properties": [
          {
              "text": "Scripts",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
              "prefixText": "Scripts",
              "disabled": false,
              "hidden": true,
              "noOfLines": "0",
              "validations": [

               ],
              "customValidations": [
               ],
              "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Toggle.groups.Advance.eventHandlers.Scripts,
                      "context": "",
                      "arguments": {}
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "init": {
                  "disabled": false,
                  "defltValue": "",
                  "hidden": false
              }
          }
          ]
        },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Refresh On Change",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Update",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Toggle.groups.Behaviour.eventHandlers.RefreshControls,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "None",
                    "hidden": false
                }
            },
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Toggle.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Toggle.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};


function Toggle(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Toggle;
    this.description = opts.description;
    this.type = opts.type;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.labelText = opts.labelText;
    this.switchText = opts.switchText;
    this.displayText = opts.displayText;
    this.defaultState = opts.defaultState;
    this.refreshControls = opts.refreshControls;
    this.bindValueToProperty = opts.bindValueToProperty;
    this.bindTextToProperty = opts.bindTextToProperty;
    this.bindType = opts.bindType;
    this.databindObjs = opts.databindObjs;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.disabled = opts.disabled;
    this.dataMini = opts.dataMini;
    this.customOnText = opts.customOnText;
    this.customOffText = opts.customOffText;
    this.onChange = opts.onChange;
    this.fontStyle = opts.fontStyle;
    this.fontColor = opts.fontColor;
}

Toggle.prototype = new ControlNew();
Toggle.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle;
Toggle.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.labelText = "Label Text";
    this.switchText = "0";
    this.defaultState = "On";
    this.customOnText = "On Text";
    this.customOffText = "Off Text";
    this.conditionalDisplay = "1";
    this.disabled = "0";
    this.fontStyle = "0";
    this.isDeleted = false;
    this.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
    this.dataMini = MF_IDE_CONSTANTS.CNTRL_SIZES.normal;
};
Toggle.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
Toggle.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
};
Toggle.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
}
Toggle.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
}

Toggle.prototype.fnResetObjectDetails = function () {
    if (mfUtil.isNullOrUndefined(this.fnGetDataMini())) {
        this.fnSetDataMini(MF_IDE_CONSTANTS.CNTRL_SIZES.normal);
    }
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(
                     databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }
};
Toggle.prototype.fnClearAllDatabindObjs = function () {
    this.databindObjs = [];
}
Toggle.prototype.fnGetDisplayText = function () {
    return this.displayText;
};
Toggle.prototype.fnSetDisplayText = function (value) {
    this.displayText = value;
};
Toggle.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
Toggle.prototype.fnGetDataMini = function () {
    return this.dataMini;
};
Toggle.prototype.fnSetDataMini = function (value) {
    this.dataMini = value;
};

function setDataMiniToggleUI(control) {
    if (control.dataMini === "0") {
        $('#' + control.id + '_Label').css('font-size', '12.5px');
        $('#' + control.id + '_Label').css('margin-bottom', '');
        $('#' + control.id + '_Val').removeAttr('style').removeClass('mini-toggle-val').addClass('normal-toggle-val');
        $('#' + control.id + '_Btn').removeAttr('style').removeClass('mini-toggle-btn').addClass('normal-toggle-btn');
        $('#' + control.id + '_Toggle').removeAttr('style').removeClass('mini-toggle').addClass('normal-toggle');
    }
    else if (control.dataMini === "1") {
        $('#' + control.id + '_Label').css('font-size', '11px');
        $('#' + control.id + '_Label').css('margin-bottom', '-11px');
        $('#' + control.id + '_Val').removeAttr('style').removeClass('normal-toggle-val').addClass('mini-toggle-val');
        $('#' + control.id + '_Btn').removeAttr('style').removeClass('normal-toggle-btn').addClass('mini-toggle-btn');
        $('#' + control.id + '_Toggle').removeAttr('style').removeClass('normal-toggle').addClass('mini-toggle');
    }
}
/*---------------------------------------------------Checkbox---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            LabelText: function (control, appObj) {
                if (appObj.labelText != undefined && appObj.labelText != null && appObj.labelText.length > 0)
                    $('#' + appObj.id + '_Label').html(appObj.labelText);
                else {
                    appObj.labelText = "Option1";
                    $('#' + appObj.id + '_Label').html("Option1");
                }
                $(control).val(appObj.labelText);
            },
            Alignment: function (control, appObj) {
                $(control).val(appObj.alignment);
                if (appObj.alignment == "0") {
                    if ($('#' + appObj.id + '_align').hasClass("IdeUi-btn-icon-right")) $('#' + appObj.id + '_align').removeClass("IdeUi-btn-icon-right");
                    $('#' + appObj.id + '_align').addClass('IdeUi-btn-icon-left');
                }
                else {
                    if ($('#' + appObj.id + '_align').hasClass("IdeUi-btn-icon-left")) $('#' + appObj.id + '_align').removeClass("IdeUi-btn-icon-left");
                    $('#' + appObj.id + '_align').addClass('IdeUi-btn-icon-right');
                }
            },
            Disabled: function (control, appObj) {
                if (appObj.disabled) {
                    $(control).val(appObj.disabled);
                    if (appObj.disabled == "1")
                        $('#' + appObj.id + '_Group').css('opacity', '.3');
                    else
                        $('#' + appObj.id + '_Group').css('opacity', '1');
                }
            },
            Size: function (control, cntrlObj) {
                if (!cntrlObj.dataMini)
                    cntrlObj.dataMini = "0";
                var size = cntrlObj.fnGetDataMini();
                $(control).val(size);
                setCheckboxSizeUI(cntrlObj);
            },
            eventHandlers: {
                LabelTextChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    if (evnt.target.value.length > 0)
                        propObject.labelText = evnt.target.value;
                    else {
                        propObject.labelText = "Option1";
                        evnt.target.value = propObject.labelText;
                    }
                    $('#' + propObject.id + '_Label').html(propObject.labelText);
                },
                Alignment: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.alignment = evnt.target.value;
                    if (propObject.alignment == "0") {
                        if ($('#' + propObject.id + '_align').hasClass("IdeUi-btn-icon-right")) $('#' + propObject.id + '_align').removeClass("IdeUi-btn-icon-right");
                        $('#' + propObject.id + '_align').addClass('IdeUi-btn-icon-left');
                    }
                    else {
                        if ($('#' + propObject.id + '_align').hasClass("IdeUi-btn-icon-left")) $('#' + propObject.id + '_align').removeClass("IdeUi-btn-icon-left");
                        $('#' + propObject.id + '_align').addClass('IdeUi-btn-icon-right');
                    }
                },
                Disabled: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.disabled = evnt.target.value;
                    if (propObject.disabled == "1")
                        $('#' + propObject.id + '_Group').css('opacity', '.3');
                    else
                        $('#' + propObject.id + '_Group').css('opacity', '1');
                },
                Size: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fnSetDataMini(evnt.target.value);
                    setCheckboxSizeUI(propObject);
                }
            }
        },
        Data: {
            BindToProperty: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox.propSheetCntrl.Data.getBindToPropertyCntrl();
                if ($('[id$=hdfIsChildForm]').val() == 'true') {
                    var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                    var strOptionsHtml = '<option value="-1">Select Item</option>';
                    if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) {
                        $.each(eltCntrl.properties, function () {
                            strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>';
                        });
                    }
                    $(control).html(strOptionsHtml);
                    if (appObj.bindToProperty != undefined) $(control).val(appObj.bindToProperty);
                    else $(control).val('-1');
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            Databinding: function (control, appObj) {
                //$(control).val(appObj.bindType.id)
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += ' <option value="0">None</option>';
                    strOptions += '<option value="6">Offline Database</option>';
                    control.html(strOptions);
                }
                else {
                }
                $(control).val(appObj.bindType.id);
            },
            DataObject: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox.propSheetCntrl.Data.getDataObjectCntrl();
                if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
                if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) {
                    $.each(appObj.databindObjs, function () {
                        if (this['name'].length > 0)
                            $(currentCntrl.control).val(this['name']);
                        else
                            $(currentCntrl.control).val('Select Object');
                        switch (this['bindObjType']) {
                            case "1":
                                mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "2":
                                mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "5":
                                mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control);
                                break;
                        }
                    });
                }
                else {
                    $(currentCntrl.control).val('Select Object');
                }
            },
            eventHandlers: {
                BindToProperty: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox.propSheetCntrl.Data.getBindToPropertyCntrl();
                    var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (objform.fnAddBindProperty(evnt.target.value, propObject.bindToProperty, propObject))
                        propObject.bindToProperty = evnt.target.value;
                    else {
                        if (propObject.bindToProperty != undefined) evnt.target.value = propObject.bindToProperty;
                        else evnt.target.value = '-1';
                    }
                },
                DataObject: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;

                    var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv);
                    var $DataObjectControl = $('#' + cntrls.DataObject.controlId);
                    var dataObject = new DatabindingObj();
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    if (evnt.target.value == "0") {
                        $ViewProperty.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        dataObject.bindObjType = bindType.id;
                    }
                    if (evnt.target.value == "1") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "2") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "5") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "6") {
                        $ViewProperty.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                        dataObject.bindObjType = bindType.id;
                    }
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            if (this.bindObjType != bindType.id) {
                                this.id = "";
                                this.name = "";
                                this.cmdParams = [];
                                propObject.checkedVal = "";
                                this.bindObjType = bindType.id;
                                $($DataObjectControl).val("Select Object");
                            }
                        });
                    }
                    else {
                        var dataObject = new DatabindingObj("", [], "0", bindType.id, "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        $($DataObjectControl).val("Select Object");
                    }

                    propObject.bindType = bindType;
                },
                DataBinding: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox.propSheetCntrl.Data.getDataObjectCntrl(),
                        objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                        intlJson = [],
                        isEdit = false;
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            var objDatabindObj = jQuery.extend(true, {}, this);
                            if (this['name'].length > 0)
                                $(currentCntrl.control).val(this['name']);
                            else
                                $(currentCntrl.control).val('Select Object');
                            switch (this['bindObjType']) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:
                                    //                                    intlJson = MF_HELPERS
                                    //                                                .intlsenseHelper
                                    //                                                .getControlDatabindingIntellisenseJson(
                                    //                                                    objCurrentForm, propObject.id, true
                                    //                                                );
                                    if (objDatabindObj != null
                                        && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                        ) {
                                        isEdit = true;
                                    }

                                    mFicientIde
                                    .MF_DATA_BINDING
                                    .processDataBindingByObjectType({
                                        controlType: MF_IDE_CONSTANTS.CONTROLS.CHECKBOX,
                                        databindObject: objDatabindObj,
                                        objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                        isEdit: isEdit,
                                        intlsJson: intlJson,
                                        control: propObject
                                    });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 420, false);
                                    break;
                            }
                        });
                    }
                    else {
                        $(currentCntrl.control).val('Select Object');
                    }
                }
            }
        },
        Advance: {
            Scripts: function (control, appObj) {
            },
            eventHandlers: {
                Scripts: function (evnt) {
                    bindAdvanceScriptsSelectors(evnt);
//                    $('#drpCntrlEvents').html();
//                    $('#drpCntrlEvents').css('width', '');
//                    var evntData = evnt.data;
//                    var propObject = evnt.data.propObject;
//                    $('#spanOnChngUserDefinedName').text(propObject.userDefinedName);
//                    _jsAceEditorOnChang.setTextInEditor("");
//                    var scriptOnChange = '';
//                    var options = '<option value="onChange">onChange</option>';
//                    $('#drpCntrlEvents').html(options);
//                    $('#drpCntrlEvents').val("onChange");
//                    $.uniform.update('#drpCntrlEvents');

//                    if (propObject.onChange != null && propObject.onChange != undefined) scriptOnChange = propObject.onChange;
//                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));

//                    SubProcShowCntrlJsEditor(true, "Scripts");
//                    $('[id$=btnCntrlJsSave]').unbind('click');
//                    $('[id$=btnCntrlJsSave]').bind('click', function () {
//                        scriptOnChange = _jsAceEditorOnChang.getText();
//                        if (scriptOnChange != null && scriptOnChange != undefined) propObject.onChange = Base64.encode(scriptOnChange);
//                        SubProcShowCntrlJsEditor(false);
//                        return false;
//                    });
//                    $('[id$=btnCntrlJsCancel]').unbind('click');
//                    $('[id$=btnCntrlJsCancel]').bind('click', function () {
//                        SubProcShowCntrlJsEditor(false);
//                        return false;
//                    });

                    //mFicientIde.HiddenFieldControlHelper.formDetailsHtml(propObject);
                }
            }
        },
        Behaviour: {
            Update: function (control, appObj) {
                if ((appObj.refreshControls != undefined || appObj.refreshControls != null) && appObj.refreshControls.length > 0) $(control).val(appObj.refreshControls.length + " Control(s)");
                else $(control).val("0 Control(s)");
            },
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay)
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                RefreshControls: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox.propSheetCntrl.Behaviour.getRefreshControls();
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            if (control.id != propObject.id && !control.isDeleted && isValidUpdateControl(control)) {
                                                controls.push(control.userDefinedName);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if (controls.length == 0) {
                        showMessage('Please add controls first.', DialogType.Error);
                        return;
                    }
                    else {
                        $('#OnChangeElmDiv').mficientRefreshCntrlsOnChng({ Controls: controls, SelectedControls: (propObject.refreshControls != undefined ? propObject.refreshControls : []) });
                        showModalPopUpWithOutHeader('SubProcOnChange', 'Controls', 280, false);
                        $('#OnChangeElmDiv').data("ControlId", propObject.id);
                    }

                    $('[id$=btnSaveRefreshControls]').unbind('click');
                    $('[id$=btnSaveRefreshControls]').bind('click', function () {
                        propObject.refreshControls = $('#OnChangeElmDiv').data("SelectedControls");
                        $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)");
                        $('#SubProcOnChange').dialog('close');
                        return false;
                    });

                    $('[id$=btnCancelRefreshControl]').unbind('click');
                    $('[id$=btnCancelRefreshControl]').bind('click', function () {
                        $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)");
                        $('#SubProcOnChange').dialog('close');
                        return false;
                    });
                },
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getRefreshControlsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.propPluginPrefix,
                    "Behaviour", "Update");
            }
            return objControl;
        }
        function _getBindToPropertyCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.propPluginPrefix,
                    "Data", "BindToProperty");
            }
            return objControl;
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getDataBindingCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.propPluginPrefix,
                    "Data", "DataBinding");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }

        return {
            Behaviour: {
                getRefreshControls: _getRefreshControlsCntrl,
                getConditionsCntrl: _getConditionsCntrl
            },
            Data: {
                getBindToPropertyCntrl: _getBindToPropertyCntrl,
                getDataObjectCntrl: _getDataObjectCntrl,
                getDataBindingCntrl: _getDataBindingCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.Checkbox = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Checkbox",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Checkbox.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Checkbox.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Label Text",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "LabelText",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Checkbox.groups.Appearance.eventHandlers.LabelTextChange,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "Alignment",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Alignment",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Checkbox.groups.Appearance.eventHandlers.Alignment,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Left",
                      "value": "0"
                  },
                  {
                      "text": "Right",
                      "value": "1"
                  }
               ]
            },
            {
                "text": "Disabled",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Disabled",
                "defltValue": "0",
                "validations": [
                    ],
                "customValidations": [

                       ],
                "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.Checkbox.groups.Appearance.eventHandlers.Disabled,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                  ]
            },
             {
                 "text": "Size",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                 "prefixText": "Size",
                 "defltValue": "0",
                 "validations": [
                     ],
                 "customValidations": [

                        ],
                 "events": [
                           {
                               "name": "change",
                               "func": PROP_JSON_HTML_MAP.Checkbox.groups.Appearance.eventHandlers.Size,
                               "context": "",
                               "arguments": {
                                   "cntrls": [
                                 ]
                               }
                           }
                        ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "selectValueBy": "",
                 "init": {
                     "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                     "disabled": false,
                     "hidden": false
                 },
                 "options": [
                     {
                         "text": "Normal",
                         "value": MF_IDE_CONSTANTS.CNTRL_SIZES.normal
                     },
                     {
                         "text": "Mini",
                         "value": MF_IDE_CONSTANTS.CNTRL_SIZES.mini
                     }
                   ]
             }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
            {
                "text": "Bind To Property",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "BindToProperty",
                "defltValue": "0",
                "validations": [

                       ],
                "customValidations": [

                       ],
                "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.Checkbox.groups.Data.eventHandlers.BindToProperty,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": true
                },
                "options": [
                       ]
            },
            {
                "text": "Databinding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Databinding",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Checkbox.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "None",
                      "value": "0"
                  },
                  {
                      "text": "Database Object",
                      "value": "1"
                  },
                  {
                      "text": "Web Service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  },
                  {
                      "text": "Offline Database",
                      "value": "6"
                  }
               ]
            },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Checkbox.groups.Data.eventHandlers.DataBinding,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            }
         ]
      },
        {
            "name": "Advance",
            "prefixText": "Advance",
            "hidden": false,
            "properties": [
          {
              "text": "Scripts",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
              "prefixText": "Scripts",
              "disabled": false,
              "hidden": true,
              "noOfLines": "0",
              "validations": [

               ],
              "customValidations": [
               ],
              "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Checkbox.groups.Advance.eventHandlers.Scripts,
                      "context": "",
                      "arguments": {}
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "init": {
                  "disabled": false,
                  "defltValue": "",
                  "hidden": false
              }
          }
          ]
        },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
          {
              "text": "Refresh On Change",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
              "prefixText": "Update",
              "disabled": false,
              "hidden": true,
              "noOfLines": "0",
              "validations": [

               ],
              "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
              "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Checkbox.groups.Behaviour.eventHandlers.RefreshControls,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "init": {
                  "disabled": false,
                  "defltValue": "None",
                  "hidden": false
              }
          },
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Checkbox.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Checkbox.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};


function Checkbox(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Checkbox;
    this.description = opts.description;
    this.type = opts.type;
    this.bindType = opts.bindType;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.labelText = opts.labelText;
    this.alignment = opts.alignment;
    this.checkedVal = opts.checkedVal;
    this.bindToProperty = opts.bindToProperty;
    this.databindObjs = opts.databindObjs;
    this.refreshControls = opts.refreshControls;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.disabled = opts.disabled;
    this.dataMini = opts.dataMini;
    this.onChange = opts.onChange;
}

Checkbox.prototype = new ControlNew();
Checkbox.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox;
Checkbox.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.labelText = "Option1";
    this.alignment = "0";
    this.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
    this.conditionalDisplay = "1";
    this.disabled = "0";
    this.isDeleted = false;
    this.dataMini = MF_IDE_CONSTANTS.CNTRL_SIZES.normal;
};
Checkbox.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
Checkbox.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
}
Checkbox.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
}
//shalini Reset Function
Checkbox.prototype.fnResetObjectDetails = function () {
    if (mfUtil.isNullOrUndefined(this.fnGetDataMini())) {
        this.fnSetDataMini(MF_IDE_CONSTANTS.CNTRL_SIZES.normal);
    }
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(
                     databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }
};
Checkbox.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
}
Checkbox.prototype.fnClearAllDatabindObjs = function () {
    this.databindObjs = [];
}
Checkbox.prototype.fnGetCheckedVal = function () {
    return this.checkedVal;
};
Checkbox.prototype.fnSetCheckedVal = function (value) {
    this.checkedVal = value;
};
Checkbox.prototype.fnSetDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
};
Checkbox.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
Checkbox.prototype.fnGetDataMini = function () {
    return this.dataMini;
};
Checkbox.prototype.fnSetDataMini = function (value) {
    this.dataMini = value;
};

function setCheckboxSizeUI(cntrlObj) {
    if (cntrlObj) {
        var size = cntrlObj.fnGetDataMini();
        var $ControlDiv = $('#' + cntrlObj.id + '_align');
        var $Control = $('#' + cntrlObj.id + '_Checkbox');
        var $ControlText = $('#' + cntrlObj.id + '_Label');
        if (size === "0") {
            $Control.css('height', '');
            $Control.css('width', '');
            $Control.css('margin-top', '');
            $ControlText.css('margin-top', '');
            $ControlText.css('font-size', '');
            $Control.removeClass('IdeUi-icon-checkbox-on-mini').addClass('IdeUi-icon-checkbox-on');
        }
        else if (size === "1") {
            $Control.css('height', '15px');
            $Control.css('width', '15px');
            $Control.css('margin-top', '-7.8px');
            $ControlText.css('margin-top', '-2px');
           // $('#' + cntrlObj.id + '_Label').html("Option1");
            $ControlText.css('font-size', '11px');
            $Control.removeClass('IdeUi-icon-checkbox-on').addClass('IdeUi-icon-checkbox-on-mini');
        }
    }
}
/*---------------------------------------------------RadioButton---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            LabelText: function (control, appObj) {
                $(control).val(appObj.labelText);
                if (appObj.labelText != undefined && appObj.labelText != null && appObj.labelText.length > 0) {
                    $('#' + appObj.id + '_Label').html(appObj.labelText);
                    $('#' + appObj.id + '_Label').css('margin-bottom', '');
                }
                else {
                    $('#' + appObj.id + '_Label').html("");
                    $('#' + appObj.id + '_Label').css('margin-bottom', '-23px');
                }
            },
            FontStyle: function (control, appObj) {
                if (appObj.fontStyle == undefined || appObj.fontStyle == null)
                    appObj.fontStyle = "0";
                $(control).val(appObj.fontStyle);
                var lblStyle = getTextFontStyle(appObj);
                $('#' + appObj.id + '_Label').removeClass();
                $('#' + appObj.id + '_Label').addClass(lblStyle);
            },
            FontColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Appearance.getFontColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelValueProperty = $('#' + appObj.id + '_Label');
                if (appObj.fontColor != undefined || appObj.fontColor != null) {
                    $(currentCntrl.control).val(appObj.fontColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.fontColor);
                    $($LabelValueProperty).css('color', appObj.fontColor + ' !important');
                }

                var fontCol = '';
                var fontColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            fontCol = '#' + hex;
                        }

                        var $LabelProp = $('#' + appObj.id + '_Label');
                        $(currentCntrl.control).val(fontCol);
                        $(currentCntrl.browseBtn).css('background-color', fontCol);
                        appObj.fontColor = $(currentCntrl.control).val();
                        if (fontCol.length > 0) {
                            ($LabelProp).css('color', fontCol + ' !important');
                        }
                        else {
                            ($LabelProp).css('color', '#000000 !important');
                        }
                    }
                });
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            Size: function (control, cntrlObj) {
                if (!cntrlObj.dataMini)
                    cntrlObj.dataMini = "0";
                var size = cntrlObj.fnGetDataMini();
                $(control).val(size);
                setRadioButtonOptionsGUI(cntrlObj);
            },
            Style: function (control, cntrlObj) {
                $(control).val(cntrlObj.fnGetStyle());
                setRadioButtonOptionsInUI(cntrlObj);
            },
            eventHandlers: {
                LabelTextChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.labelText = evnt.target.value;

                    if (evnt.target.value.length > 0) {
                        $('#' + propObject.id + '_Label').html(evnt.target.value);
                        $('#' + propObject.id + '_Label').css('margin-bottom', '');
                    }
                    else {
                        $('#' + propObject.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");
                        $('#' + propObject.id + '_Label').css('margin-bottom', '-23px');
                    }
                },
                FontStyleChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fontStyle = evnt.target.value;
                    var lblStyle = getTextFontStyle(propObject);
                    $('#' + propObject.id + '_Label').removeClass();
                    $('#' + propObject.id + '_Label').addClass(lblStyle);
                },
                Size: function (evnt) {
                    var propObject = evnt.data.propObject
                    propObject.fnSetDataMini(evnt.target.value);
                    setRadioButtonOptionsGUI(propObject);
                },
                Style: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fnSetStyle(evnt.target.value);
                    setRadioButtonOptionsInUI(propObject);
                }
            }
        },
        Data: {
            BindValueToProperty: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Data.getBindValueToPropertyCntrl();
                if ($('[id$=hdfIsChildForm]').val() == 'true') {
                    var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                    var strOptionsHtml = '<option value="-1">Select Item</option>';
                    if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) {
                        $.each(eltCntrl.properties, function () {
                            strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>';
                        });
                    }
                    $(control).html(strOptionsHtml);
                    if (appObj.bindValueToProperty != undefined) $(control).val(appObj.bindValueToProperty);
                    else $(control).val('-1');
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            BindTextToProperty: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Data.getBindTextToPropertyCntrl();
                if ($('[id$=hdfIsChildForm]').val() == 'true') {
                    var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                    var strOptionsHtml = '<option value="-1">Select Item</option>';
                    if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) {
                        $.each(eltCntrl.properties, function () {
                            strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>';
                        });
                    }
                    $(control).html(strOptionsHtml);
                    if (appObj.bindTextToProperty != undefined) $(control).val(appObj.bindTextToProperty);
                    else $(control).val('-1');
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            RadioButtonOptions: function (control, appObj) {
                $(control).val(appObj.options.length + " Option(s)");
                //rbOptionEdit(appObj);
                // $('#' + appObj.id + 'optionsDiv').html('');
                // var html = '';
                // var valIndex = 1;
                // $.each(appObj.options, function () {
                //     html += '<div class="IdeUi-radio node "><input type="radio" value="' + this.value + '" id="' + appObj.id + 'txt_' + valIndex + '"/><label class="' + (valIndex == '1' ? 'IdeUi-first-child' : '') + ' ' + (valIndex == appObj.options.length ? 'IdeUi-last-child' : '') + ' ' + (this.isDefault == '1' ? 'IdeUi-radio-on ' : 'IdeUi-radio-off ') + 'IdeUi-btn IdeUi-fullsize IdeUi-btn-icon-left IdeUi-btn-up-c node "><span class="IdeUi-btn-inner">'
                //   + '<span class="node IdeUi-btn-text" id="' + appObj.id + 'span_' + valIndex + '">' + this.text + '</span><span class="Checkbox-Icon node IdeUi-icon' + (this.isDefault == '1' ? ' IdeUi-icon-radio-on' : ' IdeUi-icon-radio-off') + ' IdeUi-icon-shadow">&nbsp;</span></span></label>'
                //                         + '</div>';
                //     valIndex = valIndex + 1;
                // });

                // $('#' + appObj.id + 'optionsDiv').append(html);

                setRadioButtonOptionsInUI(appObj);
            },
            eventHandlers: {
                BindValueToProperty: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Data.getBindValueToPropertyCntrl();
                    var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (objform.fnAddBindProperty(evnt.target.value, propObject.bindValueToProperty, propObject))
                        propObject.bindValueToProperty = evnt.target.value;
                    else {
                        if (propObject.bindValueToProperty != undefined) evnt.target.value = propObject.bindValueToProperty;
                        else evnt.target.value = '-1';
                    }
                },
                BindTextToProperty: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Data.getBindTextToPropertyCntrl();
                    var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (objform.fnAddBindProperty(evnt.target.value, propObject.bindTextToProperty, propObject))
                        propObject.bindTextToProperty = evnt.target.value;
                    else {
                        if (propObject.bindTextToProperty != undefined) evnt.target.value = propObject.bindTextToProperty;
                        else evnt.target.value = '-1';
                    }
                },
                RadioButtonOptions: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Data.getRadButtonOptions();

                    rbOptionEdit(propObject);
                    $('#btnSaveRadioBtnOptions').unbind('click');
                    $('#btnSaveRadioBtnOptions').bind('click', function () {
                        _saveRbOption(propObject);
                        if (currentCntrl && propObject.options != undefined) $(currentCntrl.control).val(propObject.options.length + " Option(s)");
                        return false;
                    });
                    $('#btnCancelRadioBtnOptions').unbind('click');
                    $('#btnCancelRadioBtnOptions').bind('click', function () {
                        SubProcAddOptionInRbAndChk(false);
                        if (currentCntrl && propObject.options != undefined) $(currentCntrl.control).val(propObject.options.length + " Option(s)");
                        return false;
                    });

                    SubProcAddOptionInRbAndChk(true, "RadioButton Options");
                }
            }
        },
        Validation: {
            Required: function (control, appObj) {
                $(control).val(appObj.required)
            },
            eventHandlers: {
                IsRequired: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.required = evnt.target.value;
                }
            }
        },
        Advance: {
            Scripts: function (control, appObj) {
            },
            eventHandlers: {
                Scripts: function (evnt) {
                    bindAdvanceScriptsSelectors(evnt);
//                    $('#drpCntrlEvents').html();
//                    $('#drpCntrlEvents').css('width', '');
//                    var evntData = evnt.data;
//                    var propObject = evnt.data.propObject;
//                    $('#spanOnChngUserDefinedName').text(propObject.userDefinedName);
//                    _jsAceEditorOnChang.setTextInEditor("");
//                    var scriptOnChange = '';
//                    var options = '<option value="onChange">onChange</option>';
//                    $('#drpCntrlEvents').html(options);
//                    $('#drpCntrlEvents').val("onChange");
//                    $.uniform.update('#drpCntrlEvents');

//                    if (propObject.onChange != null && propObject.onChange != undefined) scriptOnChange = propObject.onChange;
//                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));

//                    SubProcShowCntrlJsEditor(true, "Scripts");
//                    $('[id$=btnCntrlJsSave]').unbind('click');
//                    $('[id$=btnCntrlJsSave]').bind('click', function () {
//                        scriptOnChange = _jsAceEditorOnChang.getText();
//                        if (scriptOnChange != null && scriptOnChange != undefined) propObject.onChange = Base64.encode(scriptOnChange);
//                        return false;
//                    });
//                    $('[id$=btnCntrlJsSaveClose]').unbind('click');
//                    $('[id$=btnCntrlJsSaveClose]').bind('click', function () {
//                        scriptOnChange = _jsAceEditorOnChang.getText();
//                        if (scriptOnChange != null && scriptOnChange != undefined) propObject.onChange = Base64.encode(scriptOnChange);
//                        SubProcShowCntrlJsEditor(false);
//                        return false;
//                    });

//                    $('[id$=btnCntrlJsCancel]').unbind('click');
//                    $('[id$=btnCntrlJsCancel]').bind('click', function () {
//                        SubProcShowCntrlJsEditor(false);
//                        return false;
//                    });

                    //mFicientIde.HiddenFieldControlHelper.formDetailsHtml(propObject);
                }
            }
        },
        Behaviour: {
            Update: function (control, appObj) {
                if ((appObj.refreshControls != undefined || appObj.refreshControls != null) && appObj.refreshControls.length > 0) $(control).val(appObj.refreshControls.length + " Control(s)");
                else $(control).val("0 Control(s)");
            },
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay)
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                RefreshControls: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Behaviour.getRefreshControls();
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            if (control.id != propObject.id && !control.isDeleted && isValidUpdateControl(control)) {
                                                controls.push(control.userDefinedName);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if (controls.length == 0) {
                        showMessage('Please add controls first.', DialogType.Error);
                        return;
                    }
                    else {
                        $('#OnChangeElmDiv').mficientRefreshCntrlsOnChng({ Controls: controls, SelectedControls: (propObject.refreshControls != undefined ? propObject.refreshControls : []) });
                        showModalPopUpWithOutHeader('SubProcOnChange', 'Controls', 280, false);
                        $('#OnChangeElmDiv').data("ControlId", propObject.id);
                    }

                    $('[id$=btnSaveRefreshControls]').unbind('click');
                    $('[id$=btnSaveRefreshControls]').bind('click', function () {
                        propObject.refreshControls = $('#OnChangeElmDiv').data("SelectedControls");
                        $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)");
                        $('#SubProcOnChange').dialog('close');
                        return false;
                    });

                    $('[id$=btnCancelRefreshControl]').unbind('click');
                    $('[id$=btnCancelRefreshControl]').bind('click', function () {
                        $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)");
                        $('#SubProcOnChange').dialog('close');
                        return false;
                    });
                },
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getFontColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.propPluginPrefix,
                    "Appearance", "FontColor");
            }
            return objControl;
        }
        function _getDataRadButtonOptionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.propPluginPrefix,
                    "Data", "RadioButtonOptions");
            }
            return objControl;
        }
        function _getBindValueToPropertyCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.propPluginPrefix,
                    "Data", "BindValueToProperty");
            }
            return objControl;
        }
        function _getBindTextToPropertyCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.propPluginPrefix,
                    "Data", "BindTextToProperty");
            }
            return objControl;
        }
        function _getRefreshControlsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.propPluginPrefix,
                    "Behaviour", "Update");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }

        return {
            Appearance: {
                getFontColorCntrl: _getFontColorCntrl
            },
            Data: {
                getRadButtonOptions: _getDataRadButtonOptionsCntrl,
                getBindValueToPropertyCntrl: _getBindValueToPropertyCntrl,
                getBindTextToPropertyCntrl: _getBindTextToPropertyCntrl
            },
            Behaviour: {
                getRefreshControls: _getRefreshControlsCntrl,
                getConditionsCntrl: _getConditionsCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.RadioButton = {
    "type": MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Radio Button",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.RadioButton.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.RadioButton.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Label Text",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "LabelText",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.RadioButton.groups.Appearance.eventHandlers.LabelTextChange,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "Font Style",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FontStyle",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.RadioButton.groups.Appearance.eventHandlers.FontStyleChange,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Normal",
                      "value": "0"
                  },
                  {
                      "text": "Bold",
                      "value": "1"
                  },
                  {
                      "text": "Italics",
                      "value": "2"
                  },
                  {
                      "text": "Bold And Italics",
                      "value": "3"
                  }
               ]
            },
            {
                "text": "Font Color",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "FontColor",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
             {
                 "text": "Size",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                 "prefixText": "Size",
                 "defltValue": "0",
                 "validations": [
                     ],
                 "customValidations": [

                        ],
                 "events": [
                           {
                               "name": "change",
                               "func": PROP_JSON_HTML_MAP.RadioButton.groups.Appearance.eventHandlers.Size,
                               "context": "",
                               "arguments": {
                                   "cntrls": [
                                 ]
                               }
                           }
                        ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "selectValueBy": "",
                 "init": {
                     "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                     "disabled": false,
                     "hidden": false
                 },
                 "options": [
                     {
                         "text": "Normal",
                         "value": MF_IDE_CONSTANTS.CNTRL_SIZES.normal
                     },
                     {
                         "text": "Mini",
                         "value": MF_IDE_CONSTANTS.CNTRL_SIZES.mini
                     }
                   ]
             }, {
                 "text": "Style",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                 "prefixText": "Style",
                 "defltValue": "0",
                 "validations": [
                     ],
                 "customValidations": [
                    ],
                 "events": [
                     {
                         "name": "change",
                         "func": PROP_JSON_HTML_MAP.RadioButton.groups.Appearance.eventHandlers.Style,
                         "context": "",
                         "arguments": {
                             "cntrls": [
                                    ]
                         }
                     }
                  ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "selectValueBy": "",
                 "init": {
                     "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                     "disabled": false,
                     "hidden": false
                 },
                 "options": [
                     {
                         "text": "Vertical",
                         "value": MF_IDE_CONSTANTS.radioButtonStyle.vertical
                     },
                     {
                         "text": "Horizontal",
                         "value": MF_IDE_CONSTANTS.radioButtonStyle.horizontal
                     }
                   ]
             }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
              {
                  "text": "Bind Value To Property",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "BindValueToProperty",
                  "defltValue": "0",
                  "validations": [

                       ],
                  "customValidations": [

                       ],
                  "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.RadioButton.groups.Data.eventHandlers.BindValueToProperty,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": true
                  },
                  "options": [
                       ]
              },
              {
                  "text": "Bind Text To Property",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "BindTextToProperty",
                  "defltValue": "0",
                  "validations": [

                       ],
                  "customValidations": [

                       ],
                  "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.RadioButton.groups.Data.eventHandlers.BindTextToProperty,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": true
                  },
                  "options": [
                       ]
              },
            {
                "text": "Radio Button Options",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "RadioButtonOptions",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.RadioButton.groups.Data.eventHandlers.RadioButtonOptions,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "3 Option(s)",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Validation",
          "prefixText": "Validation",
          "hidden": false,
          "properties": [
          {
              "text": "Required",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
              "prefixText": "Required",
              "defltValue": "0",
              "validations": [

               ],
              "customValidations": [

               ],
              "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.RadioButton.groups.Validation.eventHandlers.IsRequired,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "selectValueBy": "",
              "init": {
                  "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                  "disabled": false,
                  "hidden": false
              },
              "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
          }
          ]
      },
          {
              "name": "Advance",
              "prefixText": "Advance",
              "hidden": false,
              "properties": [
          {
              "text": "Scripts",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
              "prefixText": "Scripts",
              "disabled": false,
              "hidden": true,
              "noOfLines": "0",
              "validations": [

               ],
              "customValidations": [
               ],
              "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.RadioButton.groups.Advance.eventHandlers.Scripts,
                      "context": "",
                      "arguments": {}
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "init": {
                  "disabled": false,
                  "defltValue": "",
                  "hidden": false
              }
          }
          ]
          },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Refresh On Change",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Update",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.RadioButton.groups.Behaviour.eventHandlers.RefreshControls,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "None",
                    "hidden": false
                }
            },
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.RadioButton.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.RadioButton.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};

function RadioButton(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.RadioButton;
    this.description = opts.description;
    this.type = opts.type;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.labelText = opts.labelText;
    this.required = opts.required;
    this.options = opts.options;
    this.refreshControls = opts.refreshControls;
    this.bindValueToProperty = opts.bindValueToProperty;
    this.bindTextToProperty = opts.bindTextToProperty;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.dataMini = opts.dataMini;
    this.onChange = opts.onChange;
    this.style = opts.style;
    this.fontStyle = opts.fontStyle;
    this.fontColor = opts.fontColor;
}

RadioButton.prototype = new ControlNew();
RadioButton.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton;
RadioButton.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.required = "0";
    this.labelText = "Option1";
    this.conditionalDisplay = "1";
    this.fontStyle = "0";
    this.options = [];
    var radioButtonOption = new RadioButtonOption();
    for (var i = 1; i <= 3; i++) {
        radioButtonOption = new RadioButtonOption();
        radioButtonOption.id = 'RbContentText_' + i;
        radioButtonOption.count = 3;
        radioButtonOption.text = "Option" + i;
        radioButtonOption.value = "Value" + i;
        radioButtonOption.isDefault = (i == 1 ? '1' : '0');
        this.fnAddRadioButtonOption(radioButtonOption);
    }
    rdbcount = this.options.length;
    this.isDeleted = false;
    this.dataMini = MF_IDE_CONSTANTS.CNTRL_SIZES.normal;
    this.style = MF_IDE_CONSTANTS.radioButtonStyle.vertical;
};
RadioButton.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};

RadioButton.prototype.fnAddRadioButtonOption = function (radioButtonOption/*RadioButtonOptions Object*/) {
    if (!this.options || !$.isArray(this.options)) {
        this.options = [];
    }

    if (radioButtonOption && radioButtonOption instanceof RadioButtonOption) {
        this.options.push(radioButtonOption);
    }
}
RadioButton.prototype.fnAddRadioButtonOptions = function (radioButtonOptions/*RadioButtonOptions Object[]*/) {
    if (!this.options || !$.isArray(this.options)) {
        this.options = [];
    }
    if ($.isArray(radioButtonOptions)) {
        var self = this;
        $.each(radioButtonOptions, function (index, value) {
            if (value && value instanceof RadioButtonOption) {
                self.options.push(value);
            }
        });
    }
}
// shaini reset
RadioButton.prototype.fnResetObjectDetails = function () {
    if (mfUtil.isNullOrUndefined(this.fnGetDataMini())) {
        this.fnSetDataMini(MF_IDE_CONSTANTS.CNTRL_SIZES.normal);
    }

    if (mfUtil.isNullOrUndefined(this.fnGetStyle())) {
        this.fnSetStyle(MF_IDE_CONSTANTS.radioButtonStyle.vertical);
    }
    var options = this.options;
    this.options = [];
    var i = 0;
    if (options && $.isArray(options) && options.length > 0) {
        for (i = 0; i <= options.length - 1; i++) {
            var objOption = new RadioButtonOption(options[i]);
            this.fnAddRadioButtonOption(objOption);
        }
    }
};
RadioButton.prototype.fnDeleteRadioButtonOption = function (id) {
    var aryNewRadioButtonOptions = $.grep(this.options, function (radioButtonOption) {
        if (radioButtonOption instanceof RadioButtonOption) {
            return radioButtonOption.id !== id;
        }
    });
    this.options = aryNewRadioButtonOptions;
}
RadioButton.prototype.fnGetDataMini = function () {
    return this.dataMini;
};
RadioButton.prototype.fnSetDataMini = function (value) {
    this.dataMini = value;
};
RadioButton.prototype.fnGetId = function () {
    return this.id;
};
RadioButton.prototype.fnGetStyle = function () {
    return this.style;
};
RadioButton.prototype.fnSetStyle = function (value) {
    this.style = value;
};
function RadioButtonOption(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.description = opts.description;
    this.type = opts.type;
    this.value = opts.value;
    this.text = opts.text;
    this.isDefault = opts.isDefault;
    this.count = opts.count;
}

RadioButtonOption.prototype = new RadioButtonOption();

var PrvChecked = "";
//Radio button option edit
function rbOptionEdit(cntrl) {
    PrvChecked = "";
    $('#RbContent').html('');
    var DpOptionCount = 0;
    if (cntrl.options != undefined && cntrl.options.length > 0) {
        $.each(cntrl.options, function () {
            DpOptionCount = DpOptionCount + 1;
            _addNewRbOptionRow(DpOptionCount, cntrl);
            $('#Rbtext_' + DpOptionCount).val(this.text);
            $('#RbValue_' + DpOptionCount).val(this.value);
            if (this.isDefault == '1') {
                $("#chkRbDefault_" + DpOptionCount).attr('checked', 'checked');
                PrvChecked = "chkRbDefault_" + DpOptionCount;
            }
        });
    }
    else {
        _addNewRbOptionRow(1, cntrl);
    }
}

function _addNewRbOptionRow(DpOptionCount, cntrl, ctr) {
    if (checkIfRowIdExist("RbContentText_" + DpOptionCount, "RbContent"))
        DpOptionCount = getHighestRowIdCount("RbContent");
    if (ctr != undefined) {
        var row = $('#' + ctr).closest("tr");
        row.after(generateRadioOptionHtml(DpOptionCount));
    }
    else {
        $('#RbContent').append(generateRadioOptionHtml(DpOptionCount));
    }


    $('#imgRbDelete_' + DpOptionCount).bind('click', function () {
        if (parseInt($("tbody#RbContent").find("tr").length, 10) > 1) {
            $('#' + this.parentElement.parentElement.id).remove();
        }
        else {
            showMessage('This option cannot be deleted. RadioButton should have at least 1 option.', DialogType.Error);
        }
    });
    $('#imgRbAdd_' + DpOptionCount).bind('click', function () {
        var ctr = $(this).attr('id');
        _addNewRbOptionRow(parseInt($("tbody#RbContent").find("tr").length, 10) + 1, cntrl, ctr);
    });

    $('#chkRbDefault_' + DpOptionCount).bind('change', function (e) {
        if ($('#' + this.id).is(":checked")) {
            $('#' + PrvChecked).removeAttr('checked');
            $('#' + this.id).attr('checked', 'checked');
            PrvChecked = this.id;
        }
        else {
            $('#' + this.id).removeAttr('checked');
            $('#' + PrvChecked).attr('checked', 'checked');
        }
    });
}

function generateRadioOptionHtml(DpOptionCount) {
    var radioHtml = '<tr id="RbContentText_' + DpOptionCount + '"><td> <input id="Rbtext_' + DpOptionCount + '" type="text" value="" onkeydown=\"return enterKeyFilter(event);\"/></td>'
                               + '<td> <input id="RbValue_' + DpOptionCount + '" type="text" value="" onkeydown=\"return enterKeyFilter(event);\"/></td><td style="padding-left: 25px;"><input id="chkRbDefault_' + DpOptionCount + '" type="checkbox" "checked"="")/></td>'
                               + '<td><img id="imgRbDelete_' + DpOptionCount + '" alt="delete" src="//enterprise.mficient.com/images/cross.png" style="padding-left:10px;cursor:pointer;"  title="Delete Option"/></td><td><img id="imgRbAdd_' + DpOptionCount + '" alt="add" src="//enterprise.mficient.com/images/add.png" style="padding-left:10px;cursor:pointer;"  title="Add Option"/></td></tr>';
    return radioHtml;
}

//save radio button options
function _saveRbOption(cntrl) {
    cntrl.options = [];
    var index = 1;
    $('#' + cntrl.id + 'optionsDiv').html('');
    $('#RbContent').find("*").each(function () {
        switch (this.id.split('_')[0]) {
            case "RbContentText":
                var radioButtonOption = _getRbOption(this.id);
                if (radioButtonOption != null || radioButtonOption != undefined) {
                    cntrl.fnAddRadioButtonOption(radioButtonOption);
                }
                break;
        }
    });

    //var strHtml = '';
    // $.each(cntrl.options, function () {
    //     strHtml += '<div class="IdeUi-radio node "><input type="radio" value="' + this.value + '" id="' + cntrl.id + 'txt_' + index + '"/><label class="' + (index == '1' ? 'IdeUi-first-child' : '') + ' ' + (index == cntrl.options.length ? 'IdeUi-last-child' : '') + ' ' + (this.isDefault == '1' ? 'IdeUi-radio-on ' : 'IdeUi-radio-off ') + 'IdeUi-btn IdeUi-fullsize IdeUi-btn-icon-left IdeUi-btn-up-c node "><span class="IdeUi-btn-inner">'
    //     + '<span class="node IdeUi-btn-text" id="' + cntrl.id + 'span_' + index + '">' + this.text + '</span><span class="Checkbox-Icon node IdeUi-icon' + (this.isDefault == '1' ? ' IdeUi-icon-radio-on' : ' IdeUi-icon-radio-off') + ' IdeUi-icon-shadow">&nbsp;</span></span></label>'
    //     + '</div>';

    //     index = index + 1;
    // });
    // strHtml = getRadioButtonHtmlForUI({
    //     style : MF_IDE_CONSTANTS.radioButtonStyle.horizontal,
    //     options :cntrl.options,
    //     cntrlCount : cntrl.id.split('_')[1],
    //     cntrl : cntrl
    // });
    // $('#' + cntrl.id).find('.RadioButtonFieldsetCont').html(strHtml)
    // if (cntrl.labelText != undefined && cntrl.labelText != null && cntrl.labelText.length > 0)
    //     $('#' + cntrl.id + '_Label').html(cntrl.labelText);
    // else
    //     $('#' + cntrl.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");

    setRadioButtonOptionsInUI(cntrl);

    SubProcAddOptionInRbAndChk(false);
}

//Get radio button options
function _getRbOption(_Id) {
    var radioButtonOption = new RadioButtonOption();
    radioButtonOption.id = _Id;
    radioButtonOption.count = $("tbody#RbContent").find("tr").length;
    $('#' + _Id).find("*").each(function () {
        switch (this.id.split('_')[0]) {
            case "Rbtext":
                radioButtonOption.text = this.value;
                break;
            case "RbValue":
                radioButtonOption.value = this.value;
                break;
            case "chkRbDefault":
                radioButtonOption.isDefault = (this.checked ? '1' : '0');
                break;

        }
    });
    if (radioButtonOption.text.length > 0 && radioButtonOption.value.length > 0) {
        return radioButtonOption;
    }
    else {
        return null;
    }
}

function SubProcAddOptionInRbAndChk(_bol, _Title) {
    if (_bol)
        showModalPopUpWithOutHeader('SubProcAddOptionInRbAndChk', _Title, 515, false);
    else
        $('#SubProcAddOptionInRbAndChk').dialog('close');
}

/**
* We have two types of radio button style vertical and horizontal.
* To show it in UI we have to get different Html for both
* this provides the required HTML
* @args Object 
*      style Constantts.radioButtonStyle
*      options Radion button options list stored in Control Object
*      cntrlCount 
*      cntrl
*/
function getRadioButtonHtmlForUI(args) {

    var strHtml = ""
    index = 1,
        optionsCount = 0,
        i = 0,
        objOption = null,
        cntrl = args.cntrl;

    if (cntrl && (cntrl.fontStyle == undefined || cntrl.fontStyle == null))
        cntrl.fontStyle = "0";
    var lblStyle = getTextFontStyle(cntrl);

    if (cntrl.fontColor == undefined || cntrl.fontColor == null)
        cntrl.fontColor = '#000000';

    switch (args.style) {

        case MF_IDE_CONSTANTS.radioButtonStyle.vertical:
            strHtml = '<fieldset class="node IdeUi-corner-all IdeUi-controlgroup IdeUi-controlgroup-vertical">';
            strHtml += '<div class="node IdeUi-controlgroup-label" role="heading">';
            strHtml += '<legend id="RDB_' + args.cntrlCount + '_Label" style="color:' + cntrl.fontColor + ';display: table; word-wrap: break-word;" class="' + lblStyle + '"></legend>';
            strHtml += '</div>';
            strHtml += '<div class="IdeUi-controlgroup-controls IdeUi-btn-corner-all node" id="RDB_' + args.cntrlCount + 'optionsDiv">';
            if (args.options) {
                $.each(args.options, function () {
                    strHtml += '<div class="IdeUi-radio node"><input type="radio" value="' + this.value + '" id="' + cntrl.id + 'txt_' + index + '"/><label class="' + (index == '1' ? 'IdeUi-first-child' : '') + ' ' + (index == cntrl.options.length ? 'IdeUi-last-child' : '') + ' ' + (this.isDefault == '1' ? 'IdeUi-radio-on ' : 'IdeUi-radio-off ') + 'IdeUi-btn IdeUi-fullsize IdeUi-btn-icon-left IdeUi-btn-up-c node "><span class="IdeUi-btn-inner" id="' + cntrl.id + 'Outer_span_' + index + '">'
                    + '<span class="node IdeUi-btn-text" id="' + cntrl.id + 'span_' + index + '">' + this.text + '</span><span id="' + cntrl.id + 'Icon_span_' + index + '" class="Checkbox-Icon node IdeUi-icon normal-radio-icon' + (this.isDefault == '1' ? ' IdeUi-icon-radio-on' : ' IdeUi-icon-radio-off') + ' IdeUi-icon-shadow">&nbsp;</span></span></label>'
                    + '</div>';

                    index = index + 1;
                });
            }
            strHtml += '</div>';
            strHtml += '</fieldset>';
            break;
        case MF_IDE_CONSTANTS.radioButtonStyle.horizontal:
            strHtml = '<fieldset class="node IdeUi-corner-all IdeUi-controlgroup IdeUi-controlgroup-horizontal">';
            strHtml += '<div class="node IdeUi-controlgroup-label" role="heading">';
            strHtml += '<legend id="RDB_' + args.cntrlCount + '_Label" style="color:' + cntrl.fontColor + ';display: table; word-wrap: break-word;" class="' + lblStyle + '"></legend>';
            strHtml += '</div>';
            strHtml += '<div class="IdeUi-controlgroup-controls node" id="RDB_' + args.cntrlCount + 'optionsDiv">';
            if (args.options) {
                optionsCount = args.options.length;
                for (i = 0; i <= optionsCount - 1; i++) {
                    strClasses = ""
                    objOption = args.options[i];
                    strHtml += '<div class="IdeUi-radio node">';
                    strHtml += '<input type="radio" value="' + objOption.value + '" id="' + cntrl.fnGetId() + 'txt_' + i + '"/>';

                    strHtml += '<label class="IdeUi-btn IdeUi-btn-up-c '; //this has opened a  double quoute end it
                    strHtml += (i === 0 ? 'IdeUi-corner-left ' : " ");
                    strHtml += (objOption.isDefault == '1' ? 'IdeUi-radio-on IdeUi-btn-active ' : "IdeUi-radio-off ");
                    strHtml += (i === optionsCount - 1 ? 'IdeUi-corner-right IdeUi-controlgroup-last ' : " ");
                    strHtml += '">'; //ending the opened double qoute in class

                    strHtml += '<span id="' + cntrl.id + 'Outer_span_' + index + '" class="IdeUi-btn-inner '; //start quoute
                    strHtml += (i === 0 ? 'IdeUi-corner-left ' : " ");
                    strHtml += (i === optionsCount - 1 ? 'IdeUi-corner-right IdeUi-controlgroup-last ' : " ");
                    strHtml += '">'; //end quoute

                    strHtml += '<span id="' + cntrl.id + 'span_' + index + '" class="IdeUi-btn-text">' + objOption.text + '</span>';

                    strHtml += '</span>';

                    strHtml += '</label>';

                    strHtml += '</div>';

                    index = index + 1;
                }

            }
            strHtml += '</div>';
            strHtml += '</fieldset>';
            break;
    }
    return strHtml;
}
/** 
* Method used to show the html of radio button in UI
* Use it whenever the options of the radio button is changed
* @control radio button control object

*/
function setRadioButtonOptionsInUI(control) {
    var strHtml = '';
    strHtml = getRadioButtonHtmlForUI({
        style: control.fnGetStyle(),
        options: control.options,
        cntrlCount: control.fnGetId().split('_')[1],
        cntrl: control
    });
    $('#' + control.fnGetId()).find('.RadioButtonFieldsetCont').html(strHtml)
    if (control.labelText != undefined && control.labelText != null && control.labelText.length > 0)
        $('#' + control.id + '_Label').html(control.labelText);
    else
        $('#' + control.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");

    setRadioButtonOptionsGUI(control);
}

function setRadioButtonOptionsGUI(control) {
    if (control) {
        var size = control.fnGetDataMini();
        var cntrlStyle = control.fnGetStyle();
        var optionsCount = control.options.length;
        if (cntrlStyle === "0") {
            if (size === "0") {
                $('#' + control.id + '_Label').css('font-size', '12.5px');
                for (var i = 0; i <= optionsCount; i++) {
                    $('#' + control.id + 'span_' + i).css('font-size', '12.5px');
                    $('#' + control.id + 'Outer_span_' + i).removeClass('mini-horz-radio-outer-span').removeClass('mini-radio-outer-span');
                    $('#' + control.id + 'Icon_span_' + i).removeClass('mini-radio-icon').addClass('normal-radio-icon');
                    if (i == 0)
                        $('#' + control.id + 'Icon_span_' + i).removeClass('IdeUi-icon-radio-on-mini').addClass('IdeUi-icon-radio-on');
                    else
                        $('#' + control.id + 'Icon_span_' + i).removeClass('IdeUi-icon-radio-off-mini').addClass('IdeUi-icon-radio-off');
                }
            }
            else if (size === "1") {
                $('#' + control.id + '_Label').css('font-size', '11px');
                for (var j = 0; j <= optionsCount; j++) {
                    $('#' + control.id + 'span_' + j).css('font-size', '11px');
                    $('#' + control.id + 'Outer_span_' + j).removeClass('mini-horz-radio-outer-span').addClass('mini-radio-outer-span');
                    $('#' + control.id + 'Icon_span_' + j).removeClass('normal-radio-icon').addClass('mini-radio-icon');
                    if (j == 0)
                        $('#' + control.id + 'Icon_span_' + j).removeClass('IdeUi-icon-radio-on').addClass('IdeUi-icon-radio-on-mini');
                    else
                        $('#' + control.id + 'Icon_span_' + j).removeClass('IdeUi-icon-radio-off').addClass('IdeUi-icon-radio-off-mini');
                }
            }
        }
        else if (cntrlStyle === "1") {
            if (size === "0") {
                $('#' + control.id + '_Label').css('font-size', '12.5px');
                for (var i = 0; i <= optionsCount; i++) {
                    $('#' + control.id + 'span_' + i).css('font-size', '12.5px');
                    $('#' + control.id + 'Outer_span_' + i).removeClass('mini-radio-outer-span').removeClass('mini-horz-radio-outer-span');
                }
            }
            else if (size === "1") {
                $('#' + control.id + '_Label').css('font-size', '11px');
                for (var j = 0; j <= optionsCount; j++) {
                    $('#' + control.id + 'span_' + j).css('font-size', '11px');
                    $('#' + control.id + 'Outer_span_' + j).removeClass('mini-radio-outer-span').addClass('mini-horz-radio-outer-span');
                }
            }
        }
    }
}
/*---------------------------------------------------Dropdown---------------------------------------------------*/
var dropdownOptionsCntrlId = "";

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription && strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            LabelText: function (control, appObj) {
                $(control).val(appObj.labelText);
                setDropdownLabelText(appObj);
            },
            FontStyle: function (control, appObj) {
                if (appObj.fontStyle == undefined || appObj.fontStyle == null)
                    appObj.fontStyle = "0";
                $(control).val(appObj.fontStyle);
                setDropdownStyle(appObj);
            },
            FontColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Appearance.getFontColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelValueProperty = $('#' + appObj.id + '_Label');
                if (appObj.fontColor != undefined || appObj.fontColor != null) {
                    $(currentCntrl.control).val(appObj.fontColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.fontColor);
                    $($LabelValueProperty[0]).css('color', '#' + appObj.fontColor + '!important');
                }

                var fontCol = '';
                var fontColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            fontCol = '#' + hex;
                        }

                        var $LabelVal = $('#' + appObj.id + '_Label');
                        $(currentCntrl.control).val(fontCol);
                        $(currentCntrl.browseBtn).css('background-color', fontCol);
                        appObj.fontColor = $(currentCntrl.control).val();
                        if (fontCol.length > 0) {
                            $LabelVal.css('color', fontCol + ' !important');
                        }
                        else {
                            $LabelVal.css('color', '#000000' + ' !important');
                        }
                    }
                });
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            MinWidth: function (control, appObj) {
                $(control).val(appObj.minWidth)
            },
            Width: function (control, appObj) {
                $(control).val(appObj.widthInPercent)
            },
            Size: function (control, cntrlObj) {
                if (!cntrlObj.dataMini)
                    cntrlObj.dataMini = "0";
                var size = cntrlObj.fnGetDataMini();
                $(control).val(size);
                setDataMiniDropDownUI(cntrlObj);
            },
            Inline: function (control, appObj) {
                $(control).val(appObj.inline);
                getDropdownHtml(appObj);
            },
            Icon: function (control, cntrlObj, options) {
                var aryOptionsForDdl = [{ text: "None", val: "0"}];
                aryOptionsForDdl = $.merge(aryOptionsForDdl,
                                    MF_HELPERS.jqueryMobileIconsHelper.getAsTextValPairList());
                MF_HELPERS.bindDropdowns([$(control)], aryOptionsForDdl);
                var strIcon = cntrlObj.fnGetIcon();
                if (!mfUtil.isNullOrUndefined(strIcon)) {
                    if (mfUtil.isEmptyString(strIcon)) {
                        $(control).val("0");
                    }
                    else {
                        $(control).val(strIcon);
                    }
                }
                else {
                    $(control).val("0");
                }

                var currentCntrl = options.cntrlHtmlObjects;
                if (cntrlObj.inline && cntrlObj.inline === "1") {
                    currentCntrl.wrapperDiv.show();
                    setDropdownIcon(cntrlObj);
                }
                else {
                    currentCntrl.wrapperDiv.hide();
                }
            },
            eventHandlers: {
                LabelTextChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.labelText = evnt.target.value;
                    setDropdownLabelText(propObject);
                },
                FontStyleChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fontStyle = evnt.target.value;
                    setDropdownStyle(propObject);
                },
                MinWidthChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.minWidth = evnt.target.value;
                },
                WidthChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.widthInPercent = evnt.target.value;
                },
                Size: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fnSetDataMini(evnt.target.value);
                    setDataMiniDropDownUI(propObject);
                },
                InlineChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.inline = evnt.target.value;
                    var cntrls = evnt.data.cntrls;
                    getDropdownHtml(propObject);

                    var $IconWrapper = $('#' + cntrls.Icon.wrapperDiv);
                    var $IconControl = $('#' + cntrls.Icon.controlId);
                    if (propObject.inline === "1") {
                        propObject.fnSetIcon("0");
                        $IconWrapper.show();
                    }
                    else {
                        propObject.icon = "0";
                        $IconControl.val("0");
                        $IconWrapper.hide();
                    }
                },
                Icon: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var cntrls = evntData.cntrls;
                    propObject.fnSetIcon($self.val());

                    setDropdownIcon(propObject);
                }
            }
        },
        Data: {
            BindValueToProperty: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getBindValueToPropertyCntrl();
                if ($('[id$=hdfIsChildForm]').val() == 'true') {
                    var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                    var strOptionsHtml = '<option value="-1">Select Item</option>';
                    if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) {
                        $.each(eltCntrl.properties, function () {
                            strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>';
                        });
                    }
                    $(control).html(strOptionsHtml);
                    if (appObj.bindValueToProperty != undefined) $(control).val(appObj.bindValueToProperty);
                    else $(control).val('-1');
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            BindTextToProperty: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getBindTextToPropertyCntrl();
                if ($('[id$=hdfIsChildForm]').val() == 'true') {
                    var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                    var strOptionsHtml = '<option value="-1">Select Item</option>';
                    if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) {
                        $.each(eltCntrl.properties, function () {
                            strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>';
                        });
                    }
                    $(control).html(strOptionsHtml);
                    if (appObj.bindTextToProperty != undefined) $(control).val(appObj.bindTextToProperty);
                    else $(control).val('-1');
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            Databinding: function (control, appObj) {
                //$(control).val(appObj.bindType.id);
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += ' <option value="0">None</option>';
                    strOptions += '<option value="6">Offline Database</option>';
                    control.html(strOptions);
                }
                else {
                }
                $(control).val(appObj.bindType.id);
                setPromptText(appObj);
            },
            DataObject: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getDataObjectCntrl();
                if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
                if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) {
                    $.each(appObj.databindObjs, function () {
                        if (this['name'].length > 0)
                            $(currentCntrl.control).val(this['name']);
                        else
                            $(currentCntrl.control).val('Select Object');
                        switch (this['bindObjType']) {
                            case "1":
                                mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "2":
                                mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control);
                                break;
                            case "5":
                                mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control);
                                break;
                        }
                    });
                }
                else {
                    $(currentCntrl.control).val('Select Object');
                }
            },
            PromptText: function (control, appObj) {
                if (appObj.promptText == undefined || appObj.promptText == null || appObj.promptText.length <= 0)
                    appObj.promptText = "Select Item";
                $(control).val(appObj.promptText);
                setPromptText(appObj);
            },
            PromptValue: function (control, appObj) {
                $(control).val(appObj.promptValue)
            },
            DropdownOptions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getDropdownOptions();
                if (appObj.bindType.id == mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id) {
                    $(control).val(appObj.options.length + " Option(s)");
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                BindValueToProperty: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getBindValueToPropertyCntrl();
                    var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (objform.fnAddBindProperty(evnt.target.value, propObject.bindValueToProperty, propObject))
                        propObject.bindValueToProperty = evnt.target.value;
                    else {
                        if (propObject.bindValueToProperty != undefined) evnt.target.value = propObject.bindValueToProperty;
                        else evnt.target.value = "-1";
                    }
                },
                BindTextToProperty: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getBindTextToPropertyCntrl();
                    var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (objform.fnAddBindProperty(evnt.target.value, propObject.bindTextToProperty, propObject))
                        propObject.bindTextToProperty = evnt.target.value;
                    else {
                        if (propObject.bindTextToProperty != undefined) evnt.target.value = propObject.bindTextToProperty;
                        else evnt.target.value = '-1';
                    }
                },
                PromptText: function (evnt) {
                    var propObject = evnt.data.propObject;
                    if (evnt.target.value == undefined || evnt.target.value == null || evnt.target.value.length <= 0)
                        evnt.target.value = "Select Item";
                    propObject.promptText = evnt.target.value;
                    setPromptText(propObject);
                },
                PromptValue: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.promptValue = evnt.target.value;
                },
                DropdownOptions: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getDropdownOptions();
                    dropdownOptionsCntrlId = currentCntrl.control;
                    dpOptionEdit(propObject);
                    $('[id$=btnSaveDp]').unbind('click');
                    $('[id$=btnSaveDp]').bind('click', function () {
                        saveOption(propObject);
                        $(dropdownOptionsCntrlId).val(propObject.options.length + " Option(s)");
                        return false;
                    });
                    $('[id$=btnAddDpClose]').unbind('click');
                    $('[id$=btnAddDpClose]').bind('click', function () {
                        if (dropdownOptionsCntrlId && propObject.options != undefined) $(dropdownOptionsCntrlId).val(propObject.options.length + " Option(s)");
                        SubProcAddOptionInDropdown(false, "Dropdown Options");
                        return false;
                    });
                    $('[id$=btnUploadCsvFile]').unbind('click');
                    $('[id$=btnUploadCsvFile]').bind('click', function () {
                        SubProcIframe(true);
                        if (dropdownOptionsCntrlId && propObject.options != undefined) $(dropdownOptionsCntrlId).val(propObject.options.length + " Option(s)");
                        return false;
                    });
                    $('[id$=btnImport]').unbind('click');
                    $('[id$=btnImport]').bind('click', function () {
                        SubProcAddOptionInDropdown(false);
                        if (dropdownOptionsCntrlId && propObject.options != undefined) $(dropdownOptionsCntrlId).val(propObject.options.length + " Option(s)");
                        return false;
                    });
                    SubProcAddOptionInDropdown(true, "Dropdown Options");
                },
                DataObject: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv);
                    var $DropdownOptions = $('#' + cntrls.DropdownOptions.wrapperDiv);
                    var $DataObjectControl = $('#' + cntrls.DataObject.controlId);
                    var dataObject = new DatabindingObj();
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    if (evnt.target.value != "0") {
                        if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                            $.each(propObject.databindObjs, function () {
                                if (this.bindObjType != bindType.id) {
                                }
                                else {
                                    var blnConfirm = confirm("All Options will be deleted. Do you want to Proceed ?");
                                    if (blnConfirm) {
                                        if (propObject.options.length > 0)
                                            propObject.fnDeleteAllDropdownOptions();
                                    }
                                    else {
                                        var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getDataBindingCntrl();
                                        $(currentCntrl.control).val("0");
                                        return;
                                    }
                                }
                            });
                        }
                        else {
                            var blnConfirm = confirm("All Options will be deleted. Do you want to Proceed ?");
                            if (blnConfirm) {
                                if (propObject.options.length > 0)
                                    propObject.fnDeleteAllDropdownOptions();
                            }
                            else {
                                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getDataBindingCntrl();
                                $(currentCntrl.control).val("0");
                                return;
                            }
                        }
                    }
                    else {
                        if (propObject.options.length <= 0) {
                            var dropdownOption = new DropdownOption();
                            for (var i = 1; i <= 2; i++) {
                                dropdownOption = new DropdownOption();
                                dropdownOption.id = 'DpContentText_' + i;
                                dropdownOption.text = "option" + i;
                                dropdownOption.value = "value" + i;
                                propObject.fnAddDropdownOption(dropdownOption);
                            }
                        }
                    }
                    var dropdownOptionsCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getDropdownOptions();
                    if (dropdownOptionsCntrl && propObject.options != undefined) $(dropdownOptionsCntrl.control).val(propObject.options.length + " Option(s)");

                    if (evnt.target.value == "0") {
                        $ViewProperty.hide();
                        $DropdownOptions.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        dataObject.bindObjType = bindType.id;
                    }

                    if (evnt.target.value == "1") {
                        $ViewProperty.show();
                        $DropdownOptions.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "2") {
                        $ViewProperty.show();
                        $DropdownOptions.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "5") {
                        $ViewProperty.show();
                        $DropdownOptions.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                        dataObject.bindObjType = bindType.id;
                    }
                    else if (evnt.target.value == "6") {
                        $ViewProperty.show();
                        $DropdownOptions.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                        dataObject.bindObjType = bindType.id;
                    }
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            if (this.bindObjType != bindType.id) {
                                this.id = "";
                                this.name = "";
                                this.cmdParams = [];
                                propObject.displayText = "";
                                this.bindObjType = bindType.id;
                                $($DataObjectControl).val("Select Object");
                            }
                        });
                    }
                    else {
                        var dataObject = new DatabindingObj("", [], "0", bindType.id, "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        $($DataObjectControl).val("Select Object");
                    }

                    propObject.bindType = bindType;
                    setPromptText(propObject);
                },
                DataBinding: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getDataObjectCntrl();
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                    intlJson = [],
                    isEdit = false;
                    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
                        $.each(propObject.databindObjs, function () {
                            var objDatabindObj = jQuery.extend(true, {}, this);
                            if (this['name'].length > 0)
                                $(currentCntrl.control).val(this['name']);
                            else
                                $(currentCntrl.control).val('Select Object');
                            switch (this['bindObjType']) {
                                case "1":
                                    mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false);
                                    break;
                                case "2":
                                    mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false);
                                    break;
                                case "5":
                                    mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control);
                                    showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false);
                                    break;
                                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:
                                    //                                    intlJson = MF_HELPERS
                                    //                                                    .intlsenseHelper
                                    //                                                    .getControlDatabindingIntellisenseJson(
                                    //                                                        objCurrentForm, propObject.id, true
                                    //                                                    );
                                    if (objDatabindObj != null
                                            && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                            ) {
                                        isEdit = true;
                                    }

                                    mFicientIde
                                        .MF_DATA_BINDING
                                        .processDataBindingByObjectType({
                                            controlType: MF_IDE_CONSTANTS.CONTROLS.DROPDOWN,
                                            databindObject: objDatabindObj,
                                            objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                            isEdit: isEdit,
                                            intlsJson: intlJson,
                                            control: propObject
                                        });
                                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 420, false);
                                    break;
                            }
                        });
                    }
                    else {
                        $(currentCntrl.control).val('Select Object');
                    }
                }
            }
        },
        Validation: {
            Required: function (control, appObj) {
                $(control).val(appObj.required)
            },
            eventHandlers: {
                IsRequired: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.required = evnt.target.value;
                }
            }
        },
        Advance: {
            Scripts: function (control, appObj) {
            },
            eventHandlers: {
                Scripts: function (evnt) {
                    bindAdvanceScriptsSelectors(evnt);
//                    $('#drpCntrlEvents').html();
//                    $('#drpCntrlEvents').css('width', '');
//                    var evntData = evnt.data;
//                    var propObject = evnt.data.propObject;
//                    $('#spanOnChngUserDefinedName').text(propObject.userDefinedName);
//                    _jsAceEditorOnChang.setTextInEditor("");
//                    var scriptOnChange = '';
//                    var options = '<option value="onChange">onChange</option>';
//                    $('#drpCntrlEvents').html(options);
//                    $('#drpCntrlEvents').val("onChange");
//                    $.uniform.update('#drpCntrlEvents');

//                    if (propObject.onChange != null && propObject.onChange != undefined) scriptOnChange = propObject.onChange;
//                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));

//                    SubProcShowCntrlJsEditor(true, "Scripts");
//                    $('[id$=btnCntrlJsSave]').unbind('click');
//                    $('[id$=btnCntrlJsSave]').bind('click', function () {
//                        scriptOnChange = _jsAceEditorOnChang.getText();
//                        if (scriptOnChange != null && scriptOnChange != undefined) propObject.onChange = Base64.encode(scriptOnChange);
//                        SubProcShowCntrlJsEditor(false);
//                        return false;
//                    });
//                    $('[id$=btnCntrlJsCancel]').unbind('click');
//                    $('[id$=btnCntrlJsCancel]').bind('click', function () {
//                        SubProcShowCntrlJsEditor(false);
//                        return false;
//                    });
                }
            }
        },
        Behaviour: {
            Update: function (control, appObj) {
                if ((appObj.refreshControls != undefined || appObj.refreshControls != null) && appObj.refreshControls.length > 0) $(control).val(appObj.refreshControls.length + " Control(s)");
                else $(control).val("0 Control(s)");
            },
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay)
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                RefreshControls: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Behaviour.getRefreshControls();
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            if (control.id != propObject.id && !control.isDeleted && isValidUpdateControl(control)) {
                                                controls.push(control.userDefinedName);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if (controls.length == 0) {
                        showMessage('Please add controls first.', DialogType.Error);
                        return;
                    }
                    else {
                        $('#OnChangeElmDiv').mficientRefreshCntrlsOnChng({ Controls: controls, SelectedControls: (propObject.refreshControls != undefined ? propObject.refreshControls : []) });
                        showModalPopUpWithOutHeader('SubProcOnChange', 'Controls', 280, false);
                        $('#OnChangeElmDiv').data("ControlId", propObject.id);
                    }

                    $('[id$=btnSaveRefreshControls]').unbind('click');
                    $('[id$=btnSaveRefreshControls]').bind('click', function () {
                        propObject.refreshControls = $('#OnChangeElmDiv').data("SelectedControls");
                        $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)");
                        $('#SubProcOnChange').dialog('close');
                        return false;
                    });

                    $('[id$=btnCancelRefreshControl]').unbind('click');
                    $('[id$=btnCancelRefreshControl]').bind('click', function () {
                        $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)");
                        $('#SubProcOnChange').dialog('close');
                        return false;
                    });
                },
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getFontColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix,
                    "Appearance", "FontColor");
            }
            return objControl;
        }
        function _getDataDropdownOptionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix,
                    "Data", "DropdownOptions");
            }
            return objControl;
        }
        function _getDataBindingCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix,
                    "Data", "Databinding");
            }
            return objControl;
        }
        function _getRefreshControlsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix,
                    "Behaviour", "Update");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }
        function _getBindValueToPropertyCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix,
                    "Data", "BindValueToProperty");
            }
            return objControl;
        }
        function _getBindTextToPropertyCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix,
                    "Data", "BindTextToProperty");
            }
            return objControl;
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }

        return {
            Appearance: {
                getFontColorCntrl: _getFontColorCntrl
            },
            Data: {
                getDropdownOptions: _getDataDropdownOptionsCntrl,
                getDataBindingCntrl: _getDataBindingCntrl,
                getBindValueToPropertyCntrl: _getBindValueToPropertyCntrl,
                getBindTextToPropertyCntrl: _getBindTextToPropertyCntrl,
                getDataObjectCntrl: _getDataObjectCntrl
            },
            Behaviour: {
                getRefreshControls: _getRefreshControlsCntrl,
                getConditionsCntrl: _getConditionsCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.Dropdown = {
    "type": MF_IDE_CONSTANTS.CONTROLS.DROPDOWN,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Dropdown",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Label Text",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "LabelText",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Appearance.eventHandlers.LabelTextChange,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
            {
                "text": "Font Style",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FontStyle",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Appearance.eventHandlers.FontStyleChange,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Normal",
                      "value": "0"
                  },
                  {
                      "text": "Bold",
                      "value": "1"
                  },
                  {
                      "text": "Italics",
                      "value": "2"
                  },
                  {
                      "text": "Bold And Italics",
                      "value": "3"
                  }
               ]
              },
              {
                  "text": "Font Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "FontColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
            {
                "text": "Min Width (px)",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MinWidth",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Appearance.eventHandlers.MinWidthChanged,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "280",
                    "hidden": false
                }
            },
             {
                 "text": "Size",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                 "prefixText": "Size",
                 "defltValue": "0",
                 "validations": [
                     ],
                 "customValidations": [

                        ],
                 "events": [
                           {
                               "name": "change",
                               "func": PROP_JSON_HTML_MAP.Dropdown.groups.Appearance.eventHandlers.Size,
                               "context": "",
                               "arguments": {
                                   "cntrls": [
                                 ]
                               }
                           }
                        ],
                 "CntrlProp": "",
                 "HelpText": "",
                 "selectValueBy": "",
                 "init": {
                     "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                     "disabled": false,
                     "hidden": false
                 },
                 "options": [
                     {
                         "text": "Normal",
                         "value": MF_IDE_CONSTANTS.CNTRL_SIZES.normal
                     },
                     {
                         "text": "Mini",
                         "value": MF_IDE_CONSTANTS.CNTRL_SIZES.mini
                     }
                   ]
             },
            {
                "text": "Width",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Width",
                "defltValue": "3",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Appearance.eventHandlers.WidthChanged,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "25%",
                      "value": "0"
                  },
                  {
                      "text": "50%",
                      "value": "1"
                  },
                  {
                      "text": "75%",
                      "value": "2"
                  },
                  {
                      "text": "100%",
                      "value": "3"
                  }
               ]
             },
             {
                "text": "Inline",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Inline",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Appearance.eventHandlers.InlineChanged,
                      "context": "",
                      "arguments": {
                          "cntrls": [{
                              grpPrefText: "Appearance",
                              cntrlPrefText: "Icon",
                              rtrnPropNm: "Icon"
                          }]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
            },
            {
                 "text": "Icon",
                 "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                 "prefixText": "Icon",
                 "defltValue": "",
                 "validations": [],
                 "customValidations": [],
                 "events": [{
                     "name": "change",
                     "func": PROP_JSON_HTML_MAP.Dropdown.groups.Appearance.eventHandlers.Icon,
                     "context": "",
                     "arguments": {
                     }
                 }],
                 "CntrlProp": "",
                 "helpText": {},
                 "selectValueBy": "",
                 "init": {
                     "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                     "disabled": false,
                     "hidden": true
                 },
                 "options": []
             }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
              {
                  "text": "Bind Value To Property",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "BindValueToProperty",
                  "defltValue": "0",
                  "validations": [

                       ],
                  "customValidations": [

                       ],
                  "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.Dropdown.groups.Data.eventHandlers.BindValueToProperty,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": true
                  },
                  "options": [
                       ]
              },
              {
                  "text": "Bind Text To Property",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "BindTextToProperty",
                  "defltValue": "0",
                  "validations": [

                       ],
                  "customValidations": [

                       ],
                  "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.Dropdown.groups.Data.eventHandlers.BindTextToProperty,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": true
                  },
                  "options": [
                       ]
              },
            {
                "text": "Databinding",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Databinding",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DropdownOptions",
                               rtrnPropNm: "DropdownOptions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "None",
                      "value": "0"
                  },
                  {
                      "text": "Database Object",
                      "value": "1"
                  },
                  {
                      "text": "Web Service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  },
                   {
                       "text": "Offline Database",
                       "value": "6"
                   }

               ]
            },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Data.eventHandlers.DataBinding,
                      "context": "",
                      "arguments": {}
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            },
            {
                "text": "Dropdown Options",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DropdownOptions",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Data.eventHandlers.DropdownOptions,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "2 Option(s)",
                    "hidden": false
                }
            },
            {
                "text": "Prompt Text",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "PromptText",
                "noOfLines": "false",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Data.eventHandlers.PromptText,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Prompt Value",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "PromptValue",
                "noOfLines": "false",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Data.eventHandlers.PromptValue,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Validation",
          "prefixText": "Validation",
          "hidden": false,
          "properties": [
          {
              "text": "Required",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
              "prefixText": "Required",
              "defltValue": "0",
              "validations": [

               ],
              "customValidations": [

               ],
              "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Validation.eventHandlers.IsRequired,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "selectValueBy": "",
              "init": {
                  "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                  "disabled": false,
                  "hidden": false
              },
              "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
          }
          ]
      },
          {
              "name": "Advance",
              "prefixText": "Advance",
              "hidden": false,
              "properties": [
          {
              "text": "Scripts",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
              "prefixText": "Scripts",
              "disabled": false,
              "hidden": true,
              "noOfLines": "0",
              "validations": [

               ],
              "customValidations": [
               ],
              "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Advance.eventHandlers.Scripts,
                      "context": "",
                      "arguments": {}
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "init": {
                  "disabled": false,
                  "defltValue": "",
                  "hidden": false
              }
          }
          ]
          },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Refresh On Change",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Update",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Behaviour.eventHandlers.RefreshControls,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "None",
                    "hidden": false
                }
            },
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Dropdown.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};

function Dropdown(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Dropdown;
    this.description = opts.description;
    this.type = opts.type;
    this.bindType = opts.bindType;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.labelText = opts.labelText;
    this.minWidth = opts.minWidth;
    this.widthInPercent = opts.widthInPercent;
    this.required = opts.required;
    this.options = opts.options;
    this.databindObjs = opts.databindObjs;
    this.promptText = opts.promptText;
    this.promptValue = opts.promptValue;
    this.optionText = opts.optionText;
    this.optionValue = opts.optionValue;
    this.refreshControls = opts.refreshControls;
    this.bindValueToProperty = opts.bindValueToProperty;
    this.bindTextToProperty = opts.bindTextToProperty;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.dataMini = opts.dataMini;
    this.onChange = opts.onChange;
    this.fontStyle = opts.fontStyle;
    this.fontColor = opts.fontColor;
    this.inline = opts.inline;
    this.icon = opts.icon;
}

Dropdown.prototype = new ControlNew();
Dropdown.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown;
Dropdown.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.required = "1";
    this.labelText = "Label Text";
    this.conditionalDisplay = "1";
    this.promptText = "Select Item";
    this.minWidth = "280";
    this.widthInPercent = "3";
    this.fontStyle = "0";
    this.inline = "0";
    this.dataMini = MF_IDE_CONSTANTS.CNTRL_SIZES.normal;
    this.options = [];
    this.bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
    var dropdownOption = new DropdownOption();
    for (var i = 1; i <= 2; i++) {
        dropdownOption = new DropdownOption();
        dropdownOption.id = 'DpContentText_' + i;
        dropdownOption.text = "option" + i;
        dropdownOption.value = "value" + i;
        this.fnAddDropdownOption(dropdownOption);
    }
    this.isDeleted = false;
}
Dropdown.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
Dropdown.prototype.fnGetDataMini = function () {
    return this.dataMini;
};
Dropdown.prototype.fnSetDataMini = function (value) {
    this.dataMini = value;
};
Dropdown.prototype.fnGetIcon = function () {
    return this.icon;
};
Dropdown.prototype.fnSetIcon = function (value) {
    this.icon = value;
};
Dropdown.prototype.fnAddDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
}
Dropdown.prototype.fnAddDatabindObj = function (databindObj/*DatabindingObj object*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
} //shalini Reset Function
Dropdown.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(
                     databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }

    var options = this.options;
    this.options = [];
    i = 0;
    if (options && $.isArray(options) && options.length > 0) {
        for (i = 0; i <= options.length - 1; i++) {
            var objOption = new DropdownOption(options[i]);
            this.fnAddDropdownOption(objOption);
        }
    }
}
Dropdown.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        if (databindObj instanceof DatabindingObj) {
            return databindObj.name !== name;
        }
    });
    this.databindObjs = aryNewDatabindObjs;
}
Dropdown.prototype.fnClearAllDatabindObjs = function () {
    this.databindObjs = [];
}

Dropdown.prototype.fnAddDropdownOption = function (dropdownOption/*DropdownOption Object*/) {
    if (!this.options || !$.isArray(this.options)) {
        this.options = [];
    }

    if (dropdownOption && dropdownOption instanceof DropdownOption) {
        this.options.push(dropdownOption);
    }
}
Dropdown.prototype.fnAddDropdownOptions = function (dropdownOptions/*DropdownOptions Object[]*/) {
    if (!this.options || !$.isArray(this.options)) {
        this.options = [];
    }
    if ($.isArray(dropdownOptions)) {
        var self = this;
        $.each(dropdownOptions, function (index, value) {
            if (value && value instanceof DropdownOption) {
                self.options.push(value);
            }
        });
    }
}
Dropdown.prototype.fnDeleteDropdownOption = function (id) {
    var aryNewDropdownOptions = $.grep(this.options, function (dropdownOption) {
        if (dropdownOption instanceof DropdownOption) {
            return dropdownOption.id !== id;
        }
    });
    this.options = aryNewDropdownOptions;
}
Dropdown.prototype.fnDeleteAllDropdownOptions = function () {
    this.options = [];
};
Dropdown.prototype.fnGetDisplayText = function () {
    return this.promptText;
};
Dropdown.prototype.fnSetDisplayText = function (value) {
    this.promptText = value;
};
Dropdown.prototype.fnGetDisplayVal = function () {
    return this.promptValue;
};
Dropdown.prototype.fnSetDisplayVal = function (value) {
    this.promptValue = value;
};
Dropdown.prototype.fnGetOptionText = function () {
    return this.optionText;
};
Dropdown.prototype.fnSetOptionText = function (value) {
    this.optionText = value;
};
Dropdown.prototype.fnGetOptionVal = function () {
    return this.optionValue;
};
Dropdown.prototype.fnSetOptionVal = function (value) {
    this.optionValue = value;
};
Dropdown.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};

function DropdownOption(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.value = opts.value;
    this.text = opts.text;
}

DropdownOption.prototype = new DropdownOption();

function dpOptionEdit(cntrl) {
    $('#DpContent').html('');
    var DpOptionCount = 0;
    if (cntrl.options != undefined && cntrl.options.length > 0) {
        $.each(cntrl.options, function () {
            DpOptionCount = DpOptionCount + 1;
            _addDropDownNewRow(DpOptionCount, cntrl);
            $('#Dptext_' + DpOptionCount).val(this.text);
            $('#DpValue_' + DpOptionCount).val(htmlDecode(this.value));
        });
    }
    else {
        _addDropDownNewRow(1, cntrl);
    }
}

function _addDropDownNewRow(DpOptionCount, cntrl, ctr) {
    if (checkIfRowIdExist("DpContentText_" + DpOptionCount, "DpContent"))
        DpOptionCount = getHighestRowIdCount("DpContent");
    if (ctr != undefined) {
        var row = $('#' + ctr).closest("tr");
        row.after(generateDropdownOptionHtml(DpOptionCount));
    }
    else {
        $('#DpContent').append(generateDropdownOptionHtml(DpOptionCount));
    }
    $('#imgDpDelete_' + DpOptionCount).bind('click', function () {
        if (parseInt($("tbody#DpContent").find("tr").length, 10) > 1) {
            $('#' + this.parentElement.parentElement.id).remove();
        }
        else {
            showMessage('This option cannot be deleted. Dropdown should have at least 1 option.', DialogType.Error);
        }
    });
    $('#imgDpAdd_' + DpOptionCount).bind('click', function () {
        var ctr = $(this).attr('id');
        _addDropDownNewRow(parseInt($("tbody#DpContent").find("tr").length, 10) + 1, cntrl, ctr);
    });
}

function generateDropdownOptionHtml(DpOptionCount) {
    var strDrpHtml = '<tr id="DpContentText_' + DpOptionCount + '">'
                            + '<td> <input id="Dptext_' + DpOptionCount + '" type="text" value="" onkeydown=\"return enterKeyFilter(event);\"/></td>'
                            + '<td> <input id="DpValue_' + DpOptionCount + '" type="text" value="" onkeydown=\"return enterKeyFilter(event);\" /></td>'
                            + '<td><img id="imgDpDelete_' + DpOptionCount + '" alt="delete" src="//enterprise.mficient.com/images/cross.png" style="padding-left:10px;"  title="Delete Option"/></td>'
                            + '<td><img id="imgDpAdd_' + DpOptionCount + '" alt="add" src="//enterprise.mficient.com/images/add.png" style="padding-left:10px;"  title="Add Option"/></td></tr>';
    return strDrpHtml;
}

function saveOption(cntrl) {
    cntrl.options = [];
    var index = 1;
    $('#' + cntrl.id + 'optionsDiv').html('');
    var option = ""
    $('#DpContent').find("*").each(function () {
        switch (this.id.split('_')[0]) {
            case "DpContentText":
                var dropdownOption = _getDpOption(this.id)
                if (dropdownOption != null || dropdownOption != undefined) {
                    cntrl.fnAddDropdownOption(dropdownOption);
                }
                break;
        }
    });
    var strHtml = '';
    $.each(cntrl.options, function () {
        strHtml += "<option value=\"" + (htmlEncode(this.value).replace(/\"/g, '&quot;')) + "\">" + this.text + "</option>";
    });
    $('#' + cntrl.id + 'optionsDiv').append(strHtml);

    SubProcAddOptionInDropdown(false);
}

function _getDpOption(_Id) {
    var dropdownOption = new DropdownOption();
    dropdownOption.id = _Id;
    $('#' + _Id).find("*").each(function () {
        switch (this.id.split('_')[0]) {
            case "Dptext":
                dropdownOption.text = this.value;
                break;
            case "DpValue":
                dropdownOption.value = this.value;
                break;
        }
    });
    if (dropdownOption.text.length > 0 && dropdownOption.value.length > 0) {
        return dropdownOption;
    }
    else {
        return null;
    }
}

//****************************//
//this sub is used to append option in dropdown control.
//****************************//

function appendOptionInDropDown(json) {
    var strHtml = '';
    if (json.length != 0) {
        var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
        var cntrl = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();  //form.fnGetControlFromId($('#hdfElementName').val().split(',')[1]);
        if (cntrl != null || cntrl != undefined) {
            cntrl.options = [];
            var dropdownOption = new DropdownOption();
            var reqJson = jQuery.parseJSON(json);
            $.each(reqJson, function () {
                if (this['text'].length > 0 && this['val'].length > 0) {
                    dropdownOption = new DropdownOption();
                    dropdownOption.text = this['text'];
                    dropdownOption.value = this['val'];
                    cntrl.fnAddDropdownOption(dropdownOption);
                }
            });
            $('#' + cntrl.id + 'optionsDiv').html('');
            $('#DpContent').html('');
            $.each(cntrl.options, function () {
                strHtml += "<option value=\"" + (htmlEncode(this.value).replace(/\"/g, '&quot;')) + "\">" + this.text + "</option>";
                DpOptionCount = DpOptionCount + 1;
                $('#DpContent').append("<div id=\"DpContentText_" + DpOptionCount + "\" class=\"DpContentRow\"><div class=\"DpContentText\"> <input id=\"Dptext_" + DpOptionCount + "\" type=\"text\" class=\"txtDpOption\" value=\"\" onkeydown=\"return enterKeyFilter(event);\"/></div><div class=\"DpContentValue\"> <input id=\"DpValue_" + DpOptionCount + "\" type=\"text\"  class=\"txtDpOption\" value=\"\" onkeydown=\"return enterKeyFilter(event);\"/></div><div class=\"DpOptionContentDelete\"><img id=\"imgDpDelete_" + DpOptionCount + "\" alt=\"delete\" src=\"//enterprise.mficient.com/images/cross.png\" style=\"padding-left:10px;\"  title=\"Delete Option\"/></div> </div><div style=\"clear: both;\"></div>");
                $('#Dptext_' + DpOptionCount).val(this.text);
                $('#DpValue_' + DpOptionCount).val(htmlDecode(this.value));
                $('#imgDpDelete_' + DpOptionCount).bind('click', function () {
                    if (parseInt($("tbody#DpContent").find("tr").length, 10) > 1) {
                        $('#' + this.parentElement.parentElement.id).remove();
                    }
                    else {
                        showMessage('This option cannot be deleted. Dropdown should have at least 1 option.', DialogType.Error);
                    }
                });
            });
            $('#' + cntrl.id + 'optionsDiv').append(strHtml);

            $(dropdownOptionsCntrlId).val(cntrl.options.length + " Option(s)");
        }
    }
}

function SubProcAddOptionInDropdown(_bol, _Title) {
    if (_bol)
        showModalPopUpWithOutHeader('SubProcBoxAddDropDownOption', _Title, 500, false);
    else
        $('#SubProcBoxAddDropDownOption').dialog('close');
}

function SubProcIframe(_bol) {
    SubProcCommonMethod('SubProcIframe', _bol, 'Upload CSV File', 450);
}

function setPromptText(cntrl) {
    if (cntrl != undefined) {
        if (cntrl.promptText && cntrl.promptText.trim().length > 0)
            $('#' + cntrl.id + 'PromptText').html(cntrl.promptText);
        else if (cntrl.bindType.id == mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id) {
            if (cntrl.options != undefined && cntrl.options.length > 0) {
                $('#' + cntrl.id + 'PromptText').html(cntrl.options[0].text);
            }
        }
        else if (cntrl.promptText && cntrl.bindType.id != mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id) {
            if (cntrl.promptText.trim().length <= 0) $('#' + cntrl.id + 'PromptText').html("Option1");
        }
        else {
            $('#' + cntrl.id + 'PromptText').html("Option1");
        }
    }
}

function setDataMiniDropDownUI(cntrlObj) {
    var $Control = $('#' + cntrlObj.id + '_Control');
    var $ControlText = $('#' + cntrlObj.id + '_TextCntrl');
    if (cntrlObj.dataMini === "0") {
        $Control.css('height', '');
        $ControlText.css('margin-top', '');
        $($('#' + cntrlObj.id + '_Label')[0]).css('font-size', '12.5px');
        $($('#' + cntrlObj.id + 'PromptText')[0]).css('font-size', '12.5px');
        $('#' + cntrlObj.id + '_Icon').removeClass("mini-drop-icon").addClass("normal-drop-icon");
        $('#' + cntrlObj.id + '_Icon').removeClass("IdeUi-icon-arrow-d-mini").addClass("IdeUi-icon-arrow-d");
    }
    else if (cntrlObj.dataMini === "1") {
        $Control.css('height', '10px');
        $ControlText.css('margin-top', '-2px');
        $($('#' + cntrlObj.id + '_Label')[0]).css('font-size', '11px');
        $($('#' + cntrlObj.id + 'PromptText')[0]).css('font-size', '11px');
        $('#' + cntrlObj.id + '_Icon').removeClass("normal-drop-icon").addClass("mini-drop-icon");
        $('#' + cntrlObj.id + '_Icon').removeClass("IdeUi-icon-arrow-d").addClass("IdeUi-icon-arrow-d-mini");
    }
}

function getDropdownHtml(propObject){
    var strHtml = '';
    if (propObject) {
        var idCount = propObject.id.split('_')[1];
        if (propObject.inline === "1") {
				strHtml = '<div id="DrpOuterDiv_' + idCount + '" class="node Ctrl_Width96 clearfix" style="border-bottom:1px solid black;min-height:25px;" >'
                    + '<div id="DDL_' + idCount + '_ImgDiv" style="float:left; padding-top:8px; padding-right:8px;" class="node">'
                        + '<img id="DDL_' + idCount + '_Img" src=""/>'
                    + '</div>'
                    + '<div class="clear hide"></div>'
                    + '<div id="DDL_' + idCount + '_Label" class="node" style="float:left;padding-top:12px;padding-right:6px;min-width:10px;max-width:80px; overflow:hidden; white-space: nowrap; -ms-text-overflow: ellipsis;">Label Text'
                    + '</div>'
                    + '<div id="DDLDiv_' + idCount + '" style="float:left;padding-top:12px;"class="node">'
                        + '<div id="DDL_' + idCount + 'PromptText" style="min-width:30px;max-width:100px; white-space: nowrap; -ms-text-overflow: ellipsis; overflow:hidden;">Select Item </div>'
                    + '</div>'
					+ '<div style="float:right;padding-top:12px;"><img src="//enterprise.mficient.com/jqueryMobileIcons/carat-d-black.png"/></div>'
                    + '</div>'
        }
        else{
                strHtml = '<div id="DrpOuterDiv_' + idCount + '" class="node" >'
                        + '<div id="DDL_' + idCount + '_Label" style="word-wrap: break-word;overflow: hidden; white-space: nowrap; -ms-text-overflow: ellipsis;" class="IdeUi-controlgroup-label node" role="heading">'
                            + 'Label Text'
                        + '</div>'
                        + '<div class="IdeUi-controlgroup-controls node">'
                        + '<div class="IdeUi-select node">'
                                + '<div class="IdeUi-btn IdeUi-btn-up-c IdeUi-shadow IdeUi-btn-corner-all IdeUi-btn-icon-right IdeUi-first-child IdeUi-last-child node">'
                                    + ' <span class="IdeUi-btn-inner" id="DDL_' + idCount + '_Control"><span class="IdeUi-btn-text" id="DDL_' + idCount + '_TextCntrl"><span id="DDL_' + idCount + 'PromptText" style="white-space: nowrap; -ms-text-overflow: ellipsis; overflow:hidden;">Select Item </span></span>'
                                    + '<span id="DDL_' + idCount + '_Icon" class="normal-drop-icon IdeUi-icon IdeUi-icon-arrow-d IdeUi-icon-shadow">&nbsp;</span></span>'
                                + '</div>'
                            + '</div>'
                        + '</div>'
                        + '</div>'
        }

        $('#DDL_' + idCount + '_Div').html('');
        $('#DDL_' + idCount + '_Div').append(strHtml);

        setDataMiniDropDownUI(propObject);
        setDropdownIcon(propObject);
        setPromptText(propObject);
        setDropdownLabelText(propObject);
        setDropdownStyle(propObject);
    }
}

function setDropdownIcon(propObject) {
    if (propObject.inline && propObject.inline === "1") {
    var iconName = propObject.fnGetIcon();
    var iconPath = MF_HELPERS.jqueryMobileIconsHelper.getCompleteIconPath(iconName);
    var iconNameSuffix = "-black.png";
        if(iconName != undefined && iconName != "0" && parseInt(iconPath.length) > 0 && iconPath != undefined) {
            $('#' + propObject.id + '_ImgDiv').removeClass('hide');
            $('#' + propObject.id + '_Img').attr("src", iconPath + iconNameSuffix);
        }
        else
            $('#' + propObject.id + '_ImgDiv').addClass('hide');
    }
    else
        $('#' + propObject.id + '_ImgDiv').addClass('hide');
}

function setDropdownLabelText(appObj) {
    if (appObj.labelText != undefined && appObj.labelText != null && appObj.labelText.length > 0) {
        $('#' + appObj.id + '_Label').html(appObj.labelText);
        $('#' + appObj.id + '_Label').css('margin-bottom', '');
    }
    else {
        $('#' + appObj.id + '_Label').html("");
        $('#' + appObj.id + '_Label').css('margin-bottom', '-5.5px');
    }
}

function setDropdownStyle(appObj) {
    var lblStyle = getTextFontStyle(appObj);
    $('#' + appObj.id + '_Label').removeClass();
    $('#' + appObj.id + '_Label').addClass(lblStyle + ' node');
}

function bindAdvanceScriptsSelectors(evnt) {
    $('#drpCntrlEvents').html();
    $('#drpCntrlEvents').css('width', '');
    var evntData = evnt.data;
    var propObject = evnt.data.propObject;
    $('#spanOnChngUserDefinedName').text(propObject.userDefinedName);
    _jsAceEditorOnChang.setTextInEditor("");
    var scriptOnChange = '';
    var options = '<option value="onChange">onChange</option>';
    $('#drpCntrlEvents').html(options);
    $('#drpCntrlEvents').val("onChange");
    $.uniform.update('#drpCntrlEvents');

    if (propObject.onChange != null && propObject.onChange != undefined) scriptOnChange = propObject.onChange;
    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));

    SubProcShowCntrlJsEditor(true, "Scripts");
    $('[id$=popup_custom_buttom_save]').unbind('click');
    $('[id$=popup_custom_buttom_save]').bind('click', function () {
        scriptOnChange = _jsAceEditorOnChang.getText();
        if (scriptOnChange != null && scriptOnChange != undefined) propObject.onChange = Base64.encode(scriptOnChange);
        return false;
    });
//    $('[id$=btnCntrlJsSaveClose]').unbind('click');
//    $('[id$=btnCntrlJsSaveClose]').bind('click', function () {
//        scriptOnChange = _jsAceEditorOnChang.getText();
//        if (scriptOnChange != null && scriptOnChange != undefined) propObject.onChange = Base64.encode(scriptOnChange);
//        SubProcShowCntrlJsEditor(false);
//        return false;
//    });
    $('[id$=popup_custom_buttom_close]').unbind('click');
    $('[id$=popup_custom_buttom_close]').bind('click', function () {
        SubProcShowCntrlJsEditor(false);
        return false;
    });
}
var mfIdeSelectorControls = "mfIdeSelectorControls";
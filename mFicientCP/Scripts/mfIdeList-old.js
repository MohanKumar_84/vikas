﻿
var manualOptionsHtml = '<option value="0">No Action</option>'
                        + '<option value="1">Transition</option>';
var dataBindingOptionsHtml = '<option value="0">No Action</option>'
                             + '<option value="1">Transition</option>'
                             + '<option value="2">Single Row Selection</option>'
                             + '<option value="3">Multiple Row Selection</option>';

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.list = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, tableObj) {
                $(control).val(tableObj.type.UIName)
            },
            Name: function (control, tableObj) {
                $(control).val(tableObj.userDefinedName);
            },
            Description: function (control, tableObj) {
                $(control).val(tableObj.description);
            },
            eventHandlers: {
                Type: function (evnt) {
                },
                Name: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        showMessage("Name already exists for another control.Please provide some other name.", DialogType.Error);
                        $self.val(propObject.userDefinedName);
                        $self.focus();
                        return;
                    }
                    //propObject.userDefinedName = $self.val();
                    propObject.fnRename($self.val());
                },
                Description: function (evnt) {
                    //TODO VALIDATIONS
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.description = $self.val();
                }
            }
        },
        Appearance: {
            //            HeaderText: function (control, listObj) {
            //                $(control).val(listObj.headerText);
            //            },
            //            FooterText: function (control, listObj) {
            //                $(control).val(listObj.footerText);
            //            },
            ListType: function (control, listObj) {
                $(control).val(listObj.listType);
                drawListHtml(listObj);
            },
            DataInset: function (control, listObj) {
                $(control).val(listObj.dataInset);
                if (listObj.dataInset) {
                    if (listObj.dataInset === "0") {
                        $('#' + listObj.id + '_Cntrl').css('display', '');
                        $('#' + listObj.id + '_Cntrl > ul').removeClass('data-inset').css('width', '');
                    }
                    else {
                        $('#' + listObj.id + '_Cntrl').css('display', 'inline-block');
                        $('#' + listObj.id + '_Cntrl > ul').addClass('data-inset').css('width', getListWidth(listObj));
                    }
                }
            },
            DataGrouping: function (control, listObj, options) {
                $(control).val(listObj.dataGrouping);

                var objPropSheetCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Appearance.getDataGrouping();
                setDisplay(listObj, objPropSheetCntrl);

                var currentCntrl = options.cntrlHtmlObjects;
                var objDatabindingObj = listObj.fnGetDatabindingObj();
                var objbindType = null;
                var listType = listObj.fnGetListTypeByValue();
                if (objDatabindingObj) objbindType = objDatabindingObj.fnGetBindTypeObject();

                if (listType === MF_IDE_CONSTANTS.listCntrlType.Simple
                    || listType === MF_IDE_CONSTANTS.listCntrlType.EventList
                    || listType === MF_IDE_CONSTANTS.listCntrlType.ImageList
                    || listType === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    currentCntrl.wrapperDiv.hide();
                }
                else {
                    if (objbindType == null || (objbindType && objbindType === MF_IDE_CONSTANTS.ideCommandObjectTypes.None))
                        currentCntrl.wrapperDiv.hide();
                    else
                        currentCntrl.wrapperDiv.show();
                }
            },
            AppearanceCountBubble: function (control, listObj, options) {
                $(control).val(listObj.countBubble);

                var objPropSheetCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Appearance.getCountBubble();
                setDisplay(listObj, objPropSheetCntrl);

                var currentCntrl = options.cntrlHtmlObjects;
                var objDatabindingObj = listObj.fnGetDatabindingObj();
                var objbindType = null;
                var listType = listObj.fnGetListTypeByValue();
                if (objDatabindingObj) objbindType = objDatabindingObj.fnGetBindTypeObject();

                if (listType === MF_IDE_CONSTANTS.listCntrlType.Simple
                    || listType === MF_IDE_CONSTANTS.listCntrlType.EventList
                    || listType === MF_IDE_CONSTANTS.listCntrlType.ImageList
                    || listType === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    currentCntrl.wrapperDiv.hide();
                }
                else {
                    if (objbindType == null || (objbindType && objbindType === MF_IDE_CONSTANTS.ideCommandObjectTypes.None))
                        currentCntrl.wrapperDiv.hide();
                    else
                        currentCntrl.wrapperDiv.show();
                }
                drawListHtml(listObj);
            },
            EmptyListMessage: function (control, listObj, options) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel,
                    strEmptyListMsg = listObj.emptyListMsg;
                $(control).val(strEmptyListMsg);
                MF_HELPERS.setTextOfTextAreaLabelOfPropSheet(
                    strEmptyListMsg,
                    $textAreaLabel);

                var objPropSheetCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Appearance.getEmptyListMessage();
                setDisplay(listObj, objPropSheetCntrl);
                //$(control).val(listObj.emptyListMsg);
            },
            AllowFilter: function (control, listObj, params) {
                $(control).val(listObj.fnGetAllowFilter());
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    params.cntrlHtmlObjects.wrapperDiv.hide();
                }
            },
            Placeholder: function (control, listObj, params) {
                $(control).val(listObj.fnGetPlaceholder());
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    params.cntrlHtmlObjects.wrapperDiv.hide();
                }
                else {
                    if (listObj.fnGetAllowFilter() === MF_IDE_CONSTANTS.defltVal.yes) {
                        params.cntrlHtmlObjects.wrapperDiv.show();
                    }
                    else {
                        params.cntrlHtmlObjects.wrapperDiv.hide();
                    }
                }
            },
            Caption: function (control, listObj, options) {
                $(control).val(listObj.caption);
                var currentCntrl = options.cntrlHtmlObjects,
                    listType = listObj.fnGetListTypeByValue();
                if (listType === MF_IDE_CONSTANTS.listCntrlType.ImageList) {
                    currentCntrl.wrapperDiv.show();
                }
                else if (listType === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    currentCntrl.wrapperDiv.hide();
                }
                else {
                    currentCntrl.wrapperDiv.hide();
                }

            },
            eventHandlers: {
                //                HeaderText: function (evnt) {
                //                    var $self = $(this);
                //                    var evntData = evnt.data;
                //                    var propObject = evntData.propObject;
                //                    propObject.headerText = $self.val();
                //                    $('#repeaterHeader_' + propObject.id + '_HeaderText')
                //                    .text($self.val());
                //                },
                //                FooterText: function (evnt) {
                //                    var $self = $(this);
                //                    var evntData = evnt.data;
                //                    var propObject = evntData.propObject;
                //                    propObject.footerText = $self.val();
                //                    $('#repeaterFooter_' + propObject.id + '_FooterText')
                //                    .text($self.val());
                //                },
                ListType: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var cntrls = evntData.cntrls,
                        bindType = null,
                        bindTypeValue = null,
                        rowClickable = null,
                        objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();

                    propObject.listType = $self.val();

                    var objDataSubtitleCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                            .Data.getSubtitle();
                    var objDataImageNameCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                            .Data.getImageName();
                    var objDataInfoCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                            .Data.getInformation();
                    var objDataAdditionalInfoCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                            .Data.getAdditionalInformation();
                    var objDataRowIdCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getRowId();
                    var objDataTitleCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getTitle();
                    var objDataCountBubbleCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                            .Data.getCountBubble();
                    var objDataDateCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                            .Data.getDataDateDatabindCntrl();

                    var $CaptionWrapper = $('#' + cntrls.Caption.wrapperDiv);
                    var $CaptionControl = $('#' + cntrls.Caption.controlId);

                    var $CountBubbleWrapper = $('#' + cntrls.AppearanceCountBubble.wrapperDiv);
                    var $CountBubbleControl = $('#' + cntrls.AppearanceCountBubble.controlId);

                    var $DataGroupingWrapper = $('#' + cntrls.DataGrouping.wrapperDiv);
                    var $DataGroupingControl = $('#' + cntrls.DataGrouping.controlId);

                    var $ActionButtonWrapper = $('#' + evntData.cntrls.ActionButton.wrapperDiv);
                    var $ActionButton1Wrapper = $('#' + evntData.cntrls.ActionButton1.wrapperDiv);
                    var $ActionButton2Wrapper = $('#' + evntData.cntrls.ActionButton2.wrapperDiv);
                    var $ActionButton3Wrapper = $('#' + evntData.cntrls.ActionButton3.wrapperDiv);

                    var objDatabindingObj = propObject.fnGetDatabindingObj();
                    var objbindType = null;
                    if (objDatabindingObj) objbindType = objDatabindingObj.fnGetBindTypeObject();




                    objDataSubtitleCntrl.wrapperDiv.hide();
                    objDataInfoCntrl.wrapperDiv.hide();
                    objDataAdditionalInfoCntrl.wrapperDiv.hide();
                    objDataImageNameCntrl.wrapperDiv.hide();
                    objDataRowIdCntrl.wrapperDiv.hide();
                    objDataTitleCntrl.wrapperDiv.hide();
                    objDataDateCntrl.wrapperDiv.hide();

                    switch (propObject.fnGetListTypeByValue($self.val())) {
                        case MF_IDE_CONSTANTS.listCntrlType.Basic:
                        case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                            propObject.subtitle = "",
                            propObject.information = "";
                            propObject.additionalInfo = "";
                            propObject.imageName = "";
                            objDataSubtitleCntrl.control.val('None');
                            objDataInfoCntrl.control.val('None');
                            objDataAdditionalInfoCntrl.control.val('None');
                            objDataImageNameCntrl.control.val('None');

                            setDisplay(propObject, objDataRowIdCntrl);
                            setDisplay(propObject, objDataTitleCntrl);

                            //                            setDisplay(propObject, objDataSubtitleCntrl);
                            //                            setDisplay(propObject, objDataInfoCntrl);
                            //                            setDisplay(propObject, objDataAdditionalInfoCntrl);
                            //                            setDisplay(propObject, objDataImageNameCntrl);
                            break;
                        case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                            setDisplay(propObject, objDataRowIdCntrl);
                            setDisplay(propObject, objDataTitleCntrl);
                            setDisplay(propObject, objDataSubtitleCntrl);
                            setDisplay(propObject, objDataInfoCntrl);
                            setDisplay(propObject, objDataAdditionalInfoCntrl);

                            propObject.imageName = "";
                            objDataImageNameCntrl.control.val('None');
                            // objDataSubtitleCntrl.control.val('None');
                            // objDataInfoCntrl.control.val('None');
                            // objDataAdditionalInfoCntrl.control.val('None');
                            break;
                        case MF_IDE_CONSTANTS.listCntrlType.Icon:
                        case MF_IDE_CONSTANTS.listCntrlType.Simple:
                            setDisplay(propObject, objDataRowIdCntrl);
                            setDisplay(propObject, objDataTitleCntrl);
                            setDisplay(propObject, objDataImageNameCntrl);
                            //objDataImageNameCntrl.control.val('None');
                            propObject.subtitle = "",
                            propObject.information = "";
                            propObject.additionalInfo = "";

                            objDataSubtitleCntrl.control.val('None');
                            objDataInfoCntrl.control.val('None');
                            objDataAdditionalInfoCntrl.control.val('None');

                            break;
                        case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                        case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                            setDisplay(propObject, objDataRowIdCntrl);
                            setDisplay(propObject, objDataTitleCntrl);
                            setDisplay(propObject, objDataSubtitleCntrl);
                            setDisplay(propObject, objDataInfoCntrl);
                            setDisplay(propObject, objDataAdditionalInfoCntrl);
                            setDisplay(propObject, objDataImageNameCntrl);

                            // objDataSubtitleCntrl.control.val('None');
                            // objDataInfoCntrl.control.val('None');
                            // objDataAdditionalInfoCntrl.control.val('None');
                            // objDataImageNameCntrl.control.val('None');
                            break;
                        case MF_IDE_CONSTANTS.listCntrlType.EventList:
                            setDisplay(propObject, objDataRowIdCntrl);
                            setDisplay(propObject, objDataTitleCntrl);
                            setDisplay(propObject, objDataSubtitleCntrl);
                            setDisplay(propObject, objDataInfoCntrl);
                            setDisplay(propObject, objDataAdditionalInfoCntrl);
                            setDisplay(propObject, objDataDateCntrl);
                            break;
                    }

                    setDataControlDisplay(propObject, objDataCountBubbleCntrl);

                    $($CaptionControl).val('0');
                    propObject.caption = "0";

                    var type = propObject.fnGetListTypeByValue($self.val());

                    mFicientIde.ListControlHelper.setOptionsForDatabindingInPropSheet(type, objApp.fnGetType());


                    if (type === MF_IDE_CONSTANTS.listCntrlType.ImageList) {
                        $CaptionWrapper.show();
                        $DataGroupingWrapper.hide();
                        cntrls.AppearanceAllowFilter.wrapperDivCntrl.hide();
                        cntrls.AppearanceFilterPlaceholder.wrapperDivCntrl.hide();
                        if (objbindType == null || (objbindType && objbindType === MF_IDE_CONSTANTS.ideCommandObjectTypes.None)) {
                            $CountBubbleWrapper.hide();
                            $ActionButtonWrapper.hide();
                            $ActionButton1Wrapper.hide();
                            $ActionButton2Wrapper.hide();
                            $ActionButton3Wrapper.hide();
                        }
                        else {
                            $CountBubbleWrapper.show();
                            $ActionButtonWrapper.show();
                            $ActionButton1Wrapper.show();
                            $ActionButton2Wrapper.show();
                            $ActionButton3Wrapper.show();
                        }
                    }
                    else if (type === MF_IDE_CONSTANTS.listCntrlType.Simple) {
                        $($CountBubbleControl).val('0');
                        $($DataGroupingControl).val('0');
                        propObject.dataGrouping = "0";
                        propObject.countBubble = "0";
                        $CountBubbleWrapper.hide();
                        $DataGroupingWrapper.hide();
                        $CaptionWrapper.hide();

                        $ActionButtonWrapper.hide();
                        $ActionButton1Wrapper.hide();
                        $ActionButton2Wrapper.hide();
                        $ActionButton3Wrapper.hide();

                        cntrls.AppearanceAllowFilter.wrapperDivCntrl.hide();
                        cntrls.AppearanceFilterPlaceholder.wrapperDivCntrl.hide();
                    }
                    else if (type === MF_IDE_CONSTANTS.listCntrlType.EventList) {
                        $($CountBubbleControl).val('0');
                        $($DataGroupingControl).val('0');
                        propObject.dataGrouping = "0";
                        propObject.countBubble = "0";
                        $CountBubbleWrapper.hide();
                        $DataGroupingWrapper.hide();
                        $CaptionWrapper.hide();
                    }
                    else if (type === MF_IDE_CONSTANTS.listCntrlType.CustomList) {

                        //propObject.dataGrouping = "0";
                        //propObject.countBubble = "0";
                        //propObject.fnSetAllowFilter(MF_IDE_CONSTANTS.defltVal.no);
                        rowClickable = propObject.fnGetRowClickable();
                        propObject.fnSetDefaultsByListType();

                        mFicientIde
                        .ListControlHelper
                        .procShowHideHtmlInPropSheetByListProperties(
                            propObject
                        );
                        mFicientIde.ListControlHelper.setDefaultsInHtmlOfDataboundPropsInPropSheet(type);
                        if (!objbindType ||

                            (objbindType
                             && objbindType === MF_IDE_CONSTANTS.ideCommandObjectTypes.None)) {

                            propObject.fnDeleteAllManualDataOptions();
                            propObject.fnAddDatabindObj(new DatabindingObj(
                                                            "", [], "0",
                                                            MF_IDE_CONSTANTS.ideCommandObjectTypes.db.id,
                                                            "", "", "",
                                                            "", "",
                                                            { ignoreCache: false }
                                                        ));
                            propObject.fnRemoveAllDataBindingProperties();
                        }
                        else {
                        }
                        if (rowClickable === "0" || rowClickable === "1") {
                        }
                        else {
                            propObject.fnSetRowClickable("0");
                        }
                        propObject.fnDeleteScriptBtnCntrl();
                    }
                    else {
                        $CaptionWrapper.hide();
                        cntrls.AppearanceAllowFilter.wrapperDivCntrl.show();
                        if (propObject.fnGetAllowFilter() === MF_IDE_CONSTANTS.defltVal.yes) {
                            cntrls.AppearanceFilterPlaceholder.wrapperDivCntrl.show();
                        }
                        else {
                            cntrls.AppearanceFilterPlaceholder.wrapperDivCntrl.hide();
                        }
                        cntrls.DataEnablePaging.wrapperDivCntrl.show();
                        if (propObject.fnGetEnablePaging() === MF_IDE_CONSTANTS.defltVal.yes) {
                            cntrls.DataNoOfRecords.wrapperDivCntrl.show();
                        }
                        else {
                            cntrls.DataNoOfRecords.wrapperDivCntrl.hide();
                        }
                        if (objbindType == null || (objbindType && objbindType === MF_IDE_CONSTANTS.ideCommandObjectTypes.None)) {
                            $CountBubbleWrapper.hide();
                            $DataGroupingWrapper.hide();
                            propObject.dataGrouping = "0";
                            propObject.countBubble = "0";

                            $ActionButtonWrapper.hide();
                            $ActionButton1Wrapper.hide();
                            $ActionButton2Wrapper.hide();
                            $ActionButton3Wrapper.hide();
                        }
                        else {
                            $CountBubbleWrapper.show();
                            $DataGroupingWrapper.show();

                            $ActionButtonWrapper.show();
                            $ActionButton1Wrapper.show();
                            $ActionButton2Wrapper.show();
                            $ActionButton3Wrapper.show();
                        }
                    }


                    bindType = propObject.fnGetBindTypeOfDatabindObj();
                    bindTypeValue = propObject.fnGetBindTypeOfDatabindObjValue();
                    cntrls.Databinding.controlCntrl.val(bindTypeValue);
                    if (bindType === MF_IDE_CONSTANTS.ideCommandObjectTypes.None) {
                        cntrls.DataObject.wrapperDivCntrl.hide();
                    }
                    else {
                        cntrls.DataObject.wrapperDivCntrl.show();
                    }
                    mFicientIde.ListControlHelper.setOptionsForDetectRowTouchInPropSheet(type, bindType);
                    rowClickable = propObject.fnGetRowClickable();
                    cntrls.BehaviourDetectRowTouch.controlCntrl.val(rowClickable);
                    if (rowClickable === "0" || rowClickable === "1") {
                        cntrls.BehaviourSelectedRow.wrapperDivCntrl.hide();
                        propObject.fnSetSelectedRowStyle("0");
                        cntrls.BehaviourTitleColor.wrapperDivCntrl.hide();
                        propObject.fnSetTitleColor('#000000');
                    }
                    else {
                        cntrls.BehaviourSelectedRow.wrapperDivCntrl.show();
                        if (propObject.fnGetSelectedRowStyle() === "1") {
                            cntrls.BehaviourTitleColor.wrapperDivCntrl.show();
                        }
                        else {
                            cntrls.BehaviourTitleColor.wrapperDivCntrl.hide();
                        }
                    }
                    setAdvanceGroupDisplay(propObject);
                    drawListHtml(propObject);
                },
                DataInset: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.dataInset = $self.val();
                    if (propObject.dataInset) {
                        if (propObject.dataInset === "0") {
                            $('#' + propObject.id + '_Cntrl').css('display', '');
                            $('#' + propObject.id + '_Cntrl > ul').removeClass('data-inset').css('width', '');
                        }
                        else {
                            $('#' + propObject.id + '_Cntrl').css('display', 'inline-block');
                            $('#' + propObject.id + '_Cntrl > ul').addClass('data-inset').css('width', getListWidth(propObject));
                        }
                    }
                },
                DataGrouping: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.dataGrouping = $self.val();
                },
                SetCountBubble: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.countBubble = $self.val();
                    drawListHtml(propObject);
                    var objDataCountBubbleCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                            .Data.getCountBubble();
                    setDataControlDisplay(propObject, objDataCountBubbleCntrl);
                },
                EmptyListMessage: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.emptyListMsg = $self.val();
                },
                AllowFilter: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject,
                        objPlaceholderHtml = PROP_JSON_HTML_MAP
                                                .list
                                                .propSheetCntrl.Appearance.getPlaceholder();
                    propObject.fnSetAllowFilter($self.val());

                    if ($self.val() === MF_IDE_CONSTANTS.defltVal.yes) {
                        objPlaceholderHtml.wrapperDiv.show();
                    }
                    else {
                        objPlaceholderHtml.wrapperDiv.hide();
                        propObject.fnSetPlaceholder(MF_IDE_CONSTANTS.defltVal.searchPlaceHolder);
                        objPlaceholderHtml.control.val(MF_IDE_CONSTANTS.defltVal.searchPlaceHolder);
                    }
                },
                Placeholder: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.fnSetPlaceholder($self.val());
                },
                SetCaption: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.caption = $self.val();
                    drawListHtml(propObject);

                    var $ActionButtonWrapper = $('#' + evntData.cntrls.ActionButton.wrapperDiv);
                    var $ActionButton1Wrapper = $('#' + evntData.cntrls.ActionButton1.wrapperDiv);
                    var $ActionButton2Wrapper = $('#' + evntData.cntrls.ActionButton2.wrapperDiv);
                    var $ActionButton3Wrapper = $('#' + evntData.cntrls.ActionButton3.wrapperDiv);
                    var $dataCountBubbleWrapper = $('#' + evntData.cntrls.AppearanceCountBubble.wrapperDiv);

                    var objDatabindingObj = propObject.fnGetDatabindingObj();
                    var objbindType = null;
                    if (objDatabindingObj) objbindType = objDatabindingObj.fnGetBindTypeObject();

                    if (propObject.caption === "1") {
                        //propObject.actionButton = "0";
                        $dataCountBubbleWrapper.hide();

                        $ActionButtonWrapper.show();
                        if (propObject.actionButton === "1") {
                            $ActionButton1Wrapper.show();
                            $ActionButton2Wrapper.show();
                            $ActionButton3Wrapper.show();
                        }
                        else {
                            $ActionButton1Wrapper.hide();
                            $ActionButton2Wrapper.hide();
                            $ActionButton3Wrapper.hide();
                        }
                    }
                    else {
                        $dataCountBubbleWrapper.show();
                        if (objbindType == null || (objbindType && objbindType === MF_IDE_CONSTANTS.ideCommandObjectTypes.None)) {
                            $ActionButtonWrapper.hide();
                            $ActionButton1Wrapper.hide();
                            $ActionButton2Wrapper.hide();
                            $ActionButton3Wrapper.hide();
                        }
                        else if (objbindType && objbindType != MF_IDE_CONSTANTS.ideCommandObjectTypes.None) {
                            $ActionButtonWrapper.show();
                            //                            $ActionButton1Wrapper.show();
                            //                            $ActionButton2Wrapper.show();
                            //                            $ActionButton3Wrapper.show();
                        }
                    }
                }
            }
        },
        Data: {
            EnablePaging: function (control, listObj, params) {
                $(control).val(listObj.enablePaging);
                var objPropSheetCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getEnablePaging();
                setDisplay(listObj, objPropSheetCntrl);
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    params.cntrlHtmlObjects.wrapperDiv.hide();
                }
            },
            NoOfRecords: function (control, listObj, params) {
                var objPropSheetNoOfRec = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getNoOfRecords();
                if (listObj.enablePaging === "1") {
                    $(control).val(listObj.noOfRecords);
                    objPropSheetNoOfRec.wrapperDiv.show();
                }
                else {
                    $(control).val("10");
                    objPropSheetNoOfRec.wrapperDiv.hide();
                }
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    params.cntrlHtmlObjects.wrapperDiv.hide();
                }
            },
            Databinding: function (control, tableObj) {
                //$(control).val(appObj.type.UIName)
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                // if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                //     strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id + '">None</option>';
                //     strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id + '">Offline Database</option>';
                //     control.html(strOptions);
                // }
                // else {


                // }
                mFicientIde.ListControlHelper.setOptionsForDatabindingInPropSheet(tableObj.fnGetListTypeByValue(), objApp.fnGetType());
                var objDatabindingObj = tableObj.fnGetDatabindingObj();
                if (objDatabindingObj) {
                    $(control).val(objDatabindingObj.fnGetBindTypeValue());
                }

                setAdvanceGroupDisplay(tableObj);
            },
            DataObject: function (control, tableObj) {
                // $(control).val(appObj.userDefinedName);
                var objDatabindingObj = tableObj.fnGetDatabindingObj();
                var objPropSheetCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getDataObject();
                if (objDatabindingObj) {
                    var objbindType = objDatabindingObj.fnGetBindTypeObject();
                    if (objbindType && objbindType !== MF_IDE_CONSTANTS.ideCommandObjectTypes.None) {
                        if (objPropSheetCntrl) {
                            var cmdName = objDatabindingObj.fnGetCommandName();
                            if (cmdName && cmdName.length > 0)
                                objPropSheetCntrl.control.val(cmdName);
                            else
                                objPropSheetCntrl.control.val("Select object");
                            objPropSheetCntrl.wrapperDiv.show();
                        }

                    }
                    else {
                        $(control).val("Select object");
                        objPropSheetCntrl.wrapperDiv.hide();
                    }
                }
                else {
                    $(control).val("Select object");
                    objPropSheetCntrl.wrapperDiv.hide();
                }
            },
            RowID: function (control, listObj, params) {
                if (listObj.rowId) {
                    $(control).val(listObj.rowId);
                }
                else {
                    $(control).val('None');
                }
                var objPropSheetCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getRowId();
                setDataControlDisplay(listObj, objPropSheetCntrl);
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    params.cntrlHtmlObjects.wrapperDiv.hide();
                }
                //                var objDatabindingObj = listObj.fnGetDatabindingObj();
                //                if (objDatabindingObj) {
                //                    var objbindType = objDatabindingObj.fnGetBindTypeObject();
                //                    if (objPropSheetCntrl) {
                //                        if (objbindType && objbindType !== MF_IDE_CONSTANTS.ideCommandObjectTypes.None)
                //                            objPropSheetCntrl.wrapperDiv.show();
                //                        else
                //                            objPropSheetCntrl.wrapperDiv.hide();
                //                    }
                //                }
                //                else {
                //                    if (objPropSheetCntrl)
                //                        objPropSheetCntrl.wrapperDiv.hide();
                //                }
            },
            Title: function (control, listObj, params) {
                if (listObj.title) {
                    $(control).val(listObj.title);
                }
                else {
                    $(control).val('None');
                }

                var objPropSheetCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getTitle();
                setDataControlDisplay(listObj, objPropSheetCntrl);
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    params.cntrlHtmlObjects.wrapperDiv.hide();
                }
                //                setDisplay(listObj, objPropSheetCntrl);
            },
            Subtitle: function (control, listObj, params) {
                var objDataSubtitleCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                            .Data.getSubtitle();
                if (listObj.subtitle) {
                    $(control).val(listObj.subtitle);
                }
                else {
                    $(control).val('None');
                }
                setDataControlDisplay(listObj, objDataSubtitleCntrl);
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    params.cntrlHtmlObjects.wrapperDiv.hide();
                }
                //                switch (listObj.fnGetListTypeByValue()) {
                //                    case MF_IDE_CONSTANTS.listCntrlType.Basic:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Icon:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Simple:
                //                        objDataSubtitleCntrl.wrapperDiv.hide();
                //                        break;
                //                    case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                //                    case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                //                        objDataSubtitleCntrl.wrapperDiv.show();
                //                        break;
                //                }

                //                var objPropSheetCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getSubtitle();
                //                setDisplay(listObj, objPropSheetCntrl);
            },
            Information: function (control, listObj, params) {
                if (listObj.information) {
                    $(control).val(listObj.information);
                }
                else {
                    $(control).val('None');
                }
                var objDataInfoCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                            .Data.getInformation();
                setDataControlDisplay(listObj, objDataInfoCntrl);
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    params.cntrlHtmlObjects.wrapperDiv.hide();
                }
                //                switch (listObj.fnGetListTypeByValue()) {
                //                    case MF_IDE_CONSTANTS.listCntrlType.Basic:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Icon:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Simple:
                //                        objDataInfoCntrl.wrapperDiv.hide();
                //                        break;
                //                    case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                //                    case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                //                        objDataInfoCntrl.wrapperDiv.show();
                //                        break;
                //                }

                //                var objPropSheetCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getInformation();
                //                setDisplay(listObj, objPropSheetCntrl);
            },
            AdditionalInformation: function (control, listObj, params) {
                if (listObj.additionalInfo) {
                    $(control).val(listObj.additionalInfo);
                }
                else {
                    $(control).val('None');
                }
                var objDataAdditionalInfoCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                            .Data.getAdditionalInformation();
                setDataControlDisplay(listObj, objDataAdditionalInfoCntrl);
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    params.cntrlHtmlObjects.wrapperDiv.hide();
                }
                //                switch (listObj.fnGetListTypeByValue()) {
                //                    case MF_IDE_CONSTANTS.listCntrlType.Basic:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Icon:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Simple:
                //                        objDataAdditionalInfoCntrl.wrapperDiv.hide();
                //                        break;
                //                    case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                //                    case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                //                        objDataAdditionalInfoCntrl.wrapperDiv.show();
                //                        break;
                //                }

                //                var objPropSheetCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getAdditionalInformation();
                //                setDisplay(listObj, objPropSheetCntrl);
            },
            ImageName: function (control, listObj, params) {
                if (listObj.imageName) {
                    $(control).val(listObj.imageName);
                }
                else {
                    $(control).val('None');
                }
                var objDataImageNameCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                            .Data.getImageName();
                setDataControlDisplay(listObj, objDataImageNameCntrl);
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    params.cntrlHtmlObjects.wrapperDiv.hide();
                }
                //                switch (listObj.fnGetListTypeByValue()) {
                //                    case MF_IDE_CONSTANTS.listCntrlType.Basic:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                //                    case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                //                        objDataImageNameCntrl.wrapperDiv.hide();
                //                        break;
                //                    case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                //                    case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Icon:
                //                    case MF_IDE_CONSTANTS.listCntrlType.Simple:
                //                        objDataImageNameCntrl.wrapperDiv.show();
                //                        break;
                //                }

                //                var objPropSheetCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getImageName();
                //                setDisplay(listObj, objPropSheetCntrl);
            },
            CountBubble: function (control, listObj, params) {
                if (listObj.countField) {
                    $(control).val(listObj.countField);
                }
                else {
                    $(control).val('None');
                }
                var objDataCountBubbleCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                            .Data.getCountBubble();
                setDataControlDisplay(listObj, objDataCountBubbleCntrl);
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    params.cntrlHtmlObjects.wrapperDiv.hide();
                }
                //                if (listObj.fnIsCountBubbleEnabled()) {
                //                    objDataCountBubbleCntrl.wrapperDiv.show();
                //                }
                //                else {
                //                    objDataCountBubbleCntrl.wrapperDiv.hide();
                //                }
            },
            DateDatabind: function (control, listObj, params) {
                if (listObj.selectedRow) {
                    $(control).val(listObj.selectedRow);
                }
                else {
                    $(control).val('None');
                }
                var objDataDateCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                            .Data.getDataDateDatabindCntrl();
                setDataControlDisplay(listObj, objDataDateCntrl);
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    params.cntrlHtmlObjects.wrapperDiv.hide();
                }
            },
            SelectedRowDatabind: function (control, listObj, options) {
                if (listObj.selectedRow) {
                    $(control).val(listObj.selectedRow);
                }
                else {
                    $(control).val('None');
                }
                var objPropSheetCntrls = options.cntrlHtmlObjects;
                var rowClickable = listObj.fnGetRowClickable();
                if (rowClickable && (rowClickable === "2" || rowClickable === "3")) {
                    objPropSheetCntrls.wrapperDiv.show();
                }
                else {
                    objPropSheetCntrls.wrapperDiv.hide();
                }
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    objPropSheetCntrls.wrapperDiv.hide();
                }
            },
            ManualData: function (control, listObj, params) {
                var objDatabindingObj = listObj.fnGetDatabindingObj();
                var objPropSheetCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.list.propSheetCntrl.Data.getManualDataOptions();
                if (objDatabindingObj) {
                    var objbindType = objDatabindingObj.fnGetBindTypeObject();
                    if (objPropSheetCntrl) {
                        if (objbindType && objbindType === MF_IDE_CONSTANTS.ideCommandObjectTypes.None) {
                            objPropSheetCntrl.control.val(listObj.manualData.length + " Option(s)");
                            objPropSheetCntrl.wrapperDiv.show();
                        }
                        else {
                            objPropSheetCntrl.wrapperDiv.hide();
                        }
                    }
                }
                else {
                    if (objPropSheetCntrl) {
                        objPropSheetCntrl.control.val(listObj.manualData.length + " Option(s)");
                        objPropSheetCntrl.wrapperDiv.show();
                    }
                }

                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    params.cntrlHtmlObjects.wrapperDiv.hide();
                }
            },
            eventHandlers: {
                EnablePaging: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var objNoOfRecCntrl = evnt.data.cntrls.NoOfRecords;
                    if ($self.val() === "0") {
                        propObject.fnSetNoOfRecords("0");
                        propObject.fnSetEnablePaging($self.val());
                        $('#' + objNoOfRecCntrl.controlId).val('0');
                        $('#' + objNoOfRecCntrl.wrapperDiv).hide();
                    }
                    else {
                        propObject.fnSetNoOfRecords("10");
                        propObject.fnSetEnablePaging($self.val());
                        $('#' + objNoOfRecCntrl.controlId).val('10');
                        $('#' + objNoOfRecCntrl.wrapperDiv).show();
                    }
                },
                NoOfRecords: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    var iVal = 0;
                    var aryError = [];
                    if (!$self.val() || !$.isNumeric($self.val())) {
                        aryError.push("Please enter a valid value in number of records.It must be a number.");
                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(aryError), DialogType.Error);
                        $self.focus();
                        return;
                    }
                    else if (parseInt($self.val()) < 10) {
                        aryError.push(" Minimum number of records is 10. ");
                        $self.val(propObject.fnGetNoOfRecords());
                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(aryError), DialogType.Error);
                        $self.focus();
                        return;
                    }
                    propObject.fnSetNoOfRecords($self.val());
                },
                ManualData: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.list.propSheetCntrl.Data.getManualDataOptions();
                    manualDataCntrlId = currentCntrl.control;
                    listOptionEdit(propObject);
                    $('[id$=btnSaveListData]').unbind('click');
                    $('[id$=btnSaveListData]').bind('click', function () {
                        saveManualData(propObject);
                        if (propObject.manualData != undefined) $(manualDataCntrlId).val(propObject.manualData.length + " Option(s)");
                        return false;
                    });
                    $('[id$=btnAddListDataClose]').unbind('click');
                    $('[id$=btnAddListDataClose]').bind('click', function () {
                        if (propObject.manualData != undefined) $(manualDataCntrlId).val(propObject.manualData.length + " Option(s)");
                        SubProcBoxAddListData(false, "Manual Data");
                        return false;
                    });
                    SubProcBoxAddListData(true, "Manual Data");
                },
                Databinding: function (evnt) {
                    var evntData = evnt.data,
                        cntrls = evntData.cntrls;
                    //var confirm = confirm('Are you sure you want to change Databindin')
                    var $dataObjectWrapper = $('#' + evntData.cntrls.DataObject.wrapperDiv);
                    var $dataObjectCntrl = $('#' + evntData.cntrls.DataObject.controlId);
                    var $dataManualDataCntrlWrapper = $('#' + evntData.cntrls.ManualData.wrapperDiv);
                    var $dataRowIdCntrlWrapper = $('#' + evntData.cntrls.RowId.wrapperDiv);
                    var $dataTitleCntrlWrapper = $('#' + evntData.cntrls.Title.wrapperDiv);
                    var $dataSubtitleWrapper = $('#' + evntData.cntrls.Subtitle.wrapperDiv);
                    var $dataEnablePagingCntrlWrapper = $('#' + evntData.cntrls.EnablePaging.wrapperDiv);
                    var $dataInfoWrapper = $('#' + evntData.cntrls.Information.wrapperDiv);
                    var $dataAdditionalInfoWrapper = $('#' + evntData.cntrls.AdditionalInformation.wrapperDiv);
                    var $dataCountBubbleWrapper = $('#' + evntData.cntrls.CountBubble.wrapperDiv);
                    var $dataDateWrapper = $('#' + evntData.cntrls.DateDatabind.wrapperDiv);
                    var $appearCountBubbleWrapper = $('#' + evntData.cntrls.AppearanceCountBubble.wrapperDiv);
                    var $dataEmptyListMessageWrapper = $('#' + evntData.cntrls.EmptyListMessage.wrapperDiv);
                    var $dataBackNavRefreshWrapper = $('#' + evntData.cntrls.BackNavRefresh.wrapperDiv);
                    var $dataDataGroupingWrapper = $('#' + evntData.cntrls.DataGrouping.wrapperDiv);
                    var $dataImageNameWrapper = $('#' + evntData.cntrls.ImageName.wrapperDiv);
                    var $behaviourRowTouchWrapper = $('#' + evntData.cntrls.DetectRowTouch.wrapperDiv);

                    var $ActionButtonWrapper = $('#' + evntData.cntrls.ActionButton.wrapperDiv);
                    var $ActionButton1Wrapper = $('#' + evntData.cntrls.ActionButton1.wrapperDiv);
                    var $ActionButton2Wrapper = $('#' + evntData.cntrls.ActionButton2.wrapperDiv);
                    var $ActionButton3Wrapper = $('#' + evntData.cntrls.ActionButton3.wrapperDiv);





                    var $dataRowIdCntrl = $('#' + evntData.cntrls.RowId.controlId);
                    var $dataTitleCntrl = $('#' + evntData.cntrls.Title.controlId);
                    var $dataSubtitleCntrl = $('#' + evntData.cntrls.Subtitle.controlId);
                    var $dataImageNameCntrl = $('#' + evntData.cntrls.ImageName.controlId);
                    var $dataInfoCntrl = $('#' + evntData.cntrls.Information.controlId);
                    var $dataAdditionalInfoCntrl = $('#' + evntData.cntrls.AdditionalInformation.controlId);
                    var $dataCountBubbleCntrl = $('#' + evntData.cntrls.CountBubble.controlId);
                    var $appearCountBubbleCntrl = $('#' + evntData.cntrls.AppearanceCountBubble.controlId);
                    var $dataSelectedRowCntrl = $('#' + evntData.cntrls.SelectedRowDatabind.controlId);
                    var $dataDateCntrl = $('#' + evntData.cntrls.DateDatabind.controlId);
                    var $behaviourRowTouchCntrl = $('#' + evntData.cntrls.DetectRowTouch.controlId);
                    var $dataManualDataCntrl = $('#' + evntData.cntrls.ManualData.controlId);

                    var $ActionButtonCntrl = $('#' + evntData.cntrls.ActionButton.controlId);

                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    var propObject = evntData.propObject,
                        listType = propObject.fnGetListTypeByValue();
                    $dataManualDataCntrl.val('');

                    var $self = $(this);
                    if ($self.val() === "0") {
                        $dataObjectWrapper.hide();
                        $dataRowIdCntrlWrapper.hide();
                        $dataTitleCntrlWrapper.hide();
                        $dataEnablePagingCntrlWrapper.hide();
                        $dataManualDataCntrlWrapper.show();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        propObject.fnRemoveDatabindObjs();

                        $dataSubtitleWrapper.hide();
                        $dataInfoWrapper.hide();
                        $dataAdditionalInfoWrapper.hide();
                        $dataCountBubbleWrapper.hide();
                        $dataEmptyListMessageWrapper.hide();
                        //$dataBackNavRefreshWrapper.hide();
                        $dataDataGroupingWrapper.hide();
                        $dataImageNameWrapper.hide();
                        //$behaviourRowTouchWrapper.hide();
                        $appearCountBubbleWrapper.hide();
                        $dataDateWrapper.hide();


                        //$dataRowIdCntrl.val('None');
                        //$dataTitleCntrl.val('None');
                        $behaviourRowTouchCntrl.html(manualOptionsHtml);
                        $dataSubtitleCntrl.val('None');
                        $dataImageNameCntrl.val('None');
                        $dataInfoCntrl.val('None');
                        $dataAdditionalInfoCntrl.val('None');
                        $dataSelectedRowCntrl.val('None');
                        $dataDateCntrl.val('None');

                        $ActionButton1Wrapper.hide();
                        $ActionButton2Wrapper.hide();
                        $ActionButton3Wrapper.hide();
                        $ActionButtonCntrl.val("0");
                        $dataCountBubbleCntrl.val("None");
                        $appearCountBubbleCntrl.val("0");
                        $behaviourRowTouchCntrl.val("0");
                        $ActionButtonCntrl.prop('disabled', 'disabled');

                        propObject.countBubble = "0";
                        propObject.rowClickable = "0";
                        propObject.actionButton = "0";
                        propObject.dataGrouping = "0";
                        //propObject.rowId = "";
                        //propObject.title = "";
                        //propObject.subtitle = "";
                        //propObject.information = "";
                        //propObject.additionalInfo = "";
                        //propObject.imageName = "";
                        propObject.fnRemoveAllDataBindingProperties();
                        propObject.fnDeleteSelectedRowDatabind();
                        propObject.manualData = [];
                        var listDataOption = new ManualDataOption();
                        listDataOption.rowId = "1";
                        listDataOption.title = "title";
                        listDataOption.description = "description";
                        propObject.fnAddManualDataOption(listDataOption);
                        $dataManualDataCntrl.val(propObject.manualData.length + " Option(s)");

                        //Here there will be no case of CustomList
                        if (propObject.fnGetListTypeByValue(propObject.listType) === MF_IDE_CONSTANTS.listCntrlType.Simple) {
                            cntrls.AppearanceAllowFilter.wrapperDivCntrl.hide();
                            cntrls.AppearanceFilterPlaceholder.wrapperDivCntrl.hide();
                            cntrls.EnablePaging.wrapperDivCntrl.hide();
                            cntrls.DataNoOfRecords.wrapperDivCntrl.hide();
                        }
                        else if (propObject.fnGetListTypeByValue(propObject.listType) === MF_IDE_CONSTANTS.listCntrlType.ImageList) {
                            cntrls.AppearanceAllowFilter.wrapperDivCntrl.hide();
                            cntrls.AppearanceFilterPlaceholder.wrapperDivCntrl.hide();
                            cntrls.EnablePaging.wrapperDivCntrl.hide();
                            cntrls.DataNoOfRecords.wrapperDivCntrl.hide();
                        }
                        else {
                            cntrls.AppearanceAllowFilter.wrapperDivCntrl.show();
                            if (propObject.fnGetAllowFilter() === MF_IDE_CONSTANTS.defltVal.yes) {
                                cntrls.AppearanceFilterPlaceholder.wrapperDivCntrl.show();
                            }
                            else {
                                cntrls.AppearanceFilterPlaceholder.wrapperDivCntrl.hide();
                            }

                            cntrls.EnablePaging.wrapperDivCntrl.show();
                            if (propObject.fnGetEnablePaging() === MF_IDE_CONSTANTS.defltVal.yes) {
                                cntrls.DataNoOfRecords.wrapperDivCntrl.show();
                            }
                            else {
                                cntrls.DataNoOfRecords.wrapperDivCntrl.hide();
                            }
                        }

                    }
                    else {
                        $dataObjectWrapper.show();
                        $dataEnablePagingCntrlWrapper.show();
                        $dataRowIdCntrlWrapper.show();
                        $dataTitleCntrlWrapper.show();

                        $dataSubtitleWrapper.hide();
                        $dataInfoWrapper.hide();
                        $dataAdditionalInfoWrapper.hide();
                        $dataImageNameWrapper.hide();
                        $ActionButtonCntrl.removeAttr('disabled');
                        $behaviourRowTouchWrapper.show();

                        switch (propObject.fnGetListTypeByValue(propObject.listType)) {
                            case MF_IDE_CONSTANTS.listCntrlType.Basic:
                            case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                                break;
                            case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                                $dataSubtitleWrapper.show();
                                $dataInfoWrapper.show();
                                $dataAdditionalInfoWrapper.show();
                                break;
                            case MF_IDE_CONSTANTS.listCntrlType.Icon:
                            case MF_IDE_CONSTANTS.listCntrlType.Simple:
                                $dataImageNameWrapper.show();
                                break;
                            case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                            case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                                $dataSubtitleWrapper.show();
                                $dataInfoWrapper.show();
                                $dataAdditionalInfoWrapper.show();
                                $dataImageNameWrapper.show();
                                break;
                            case MF_IDE_CONSTANTS.listCntrlType.EventList:
                                $dataSubtitleWrapper.show();
                                $dataInfoWrapper.show();
                                $dataAdditionalInfoWrapper.show();
                                $dataDateWrapper.show();
                                break;
                        }

                        if (propObject.countBubble && propObject.countBubble === "1") $dataCountBubbleWrapper.show();
                        else $dataCountBubbleWrapper.hide();
                        $dataEmptyListMessageWrapper.show();
                        $dataBackNavRefreshWrapper.show();

                        $dataManualDataCntrlWrapper.hide();
                        propObject.fnRemoveDatabindObjs();
                        $behaviourRowTouchCntrl.html(dataBindingOptionsHtml);
                        $behaviourRowTouchCntrl.val(propObject.rowClickable);
                        var dataObject = new DatabindingObj("", [], "0", $self.val(), "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                        propObject.fnRemoveAllDataBindingProperties();
                        mFicientIde.ListControlHelper.fillDatabindingSettingsInHtml(propObject);

                        if (listType === MF_IDE_CONSTANTS.listCntrlType.Simple) {
                            $appearCountBubbleWrapper.hide();
                            $dataDataGroupingWrapper.hide();

                            $ActionButtonWrapper.hide();
                            $ActionButton1Wrapper.hide();
                            $ActionButton2Wrapper.hide();
                            $ActionButton3Wrapper.hide();

                            cntrls.AppearanceAllowFilter.wrapperDivCntrl.hide();
                            cntrls.AppearanceFilterPlaceholder.wrapperDivCntrl.hide();
                            cntrls.EnablePaging.wrapperDivCntrl.hide();
                            cntrls.DataNoOfRecords.wrapperDivCntrl.hide();
                        }
                        else if (listType === MF_IDE_CONSTANTS.listCntrlType.EventList) {
                            $appearCountBubbleWrapper.hide();
                            $dataDataGroupingWrapper.hide();
                            cntrls.AppearanceAllowFilter.wrapperDivCntrl.show();
                            if (propObject.fnGetAllowFilter() === MF_IDE_CONSTANTS.defltVal.yes) {
                                cntrls.AppearanceFilterPlaceholder.wrapperDivCntrl.show();
                            }
                            else {
                                cntrls.AppearanceFilterPlaceholder.wrapperDivCntrl.hide();
                            }

                            cntrls.EnablePaging.wrapperDivCntrl.show();
                            if (propObject.fnGetEnablePaging() === MF_IDE_CONSTANTS.defltVal.yes) {
                                cntrls.DataNoOfRecords.wrapperDivCntrl.show();
                            }
                            else {
                                cntrls.DataNoOfRecords.wrapperDivCntrl.hide();
                            }
                        }
                        else if (listType === MF_IDE_CONSTANTS.listCntrlType.ImageList) {
                            $dataDataGroupingWrapper.hide();

                            $ActionButtonWrapper.show();
                            if (propObject.actionButton === "1") {
                                $ActionButton1Wrapper.show();
                                $ActionButton2Wrapper.show();
                                $ActionButton3Wrapper.show();
                            }
                            else {
                                $ActionButton1Wrapper.hide();
                                $ActionButton2Wrapper.hide();
                                $ActionButton3Wrapper.hide();
                            }

                            if (propObject.caption === "1") {
                                $appearCountBubbleWrapper.hide();
                            }
                            else {
                                $appearCountBubbleWrapper.show();
                            }

                            cntrls.AppearanceAllowFilter.wrapperDivCntrl.hide();
                            cntrls.AppearanceFilterPlaceholder.wrapperDivCntrl.hide();
                            cntrls.EnablePaging.wrapperDivCntrl.hide();
                            cntrls.DataNoOfRecords.wrapperDivCntrl.hide();
                        }
                        else if (listType === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                            mFicientIde.ListControlHelper.procShowHideHtmlInPropSheetByListProperties(
                                propObject
                            );
                        }
                        else {
                            $appearCountBubbleWrapper.show();
                            $dataDataGroupingWrapper.show();

                            $ActionButtonWrapper.show();
                            $ActionButton1Wrapper.show();
                            $ActionButton2Wrapper.show();
                            $ActionButton3Wrapper.show();

                            cntrls.AppearanceAllowFilter.wrapperDivCntrl.show();
                            if (propObject.fnGetAllowFilter() === MF_IDE_CONSTANTS.defltVal.yes) {
                                cntrls.AppearanceFilterPlaceholder.wrapperDivCntrl.show();
                            }
                            else {
                                cntrls.AppearanceFilterPlaceholder.wrapperDivCntrl.hide();
                            }

                            cntrls.EnablePaging.wrapperDivCntrl.show();
                            if (propObject.fnGetEnablePaging() === MF_IDE_CONSTANTS.defltVal.yes) {
                                cntrls.DataNoOfRecords.wrapperDivCntrl.show();
                            }
                            else {
                                cntrls.DataNoOfRecords.wrapperDivCntrl.hide();
                            }
                        }
                    }
                    $dataObjectCntrl.val("Select Object");

                    drawListHtml(propObject);
                    setAdvanceGroupDisplay(propObject);
                },
                DataObject: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    var intlJson = [],
                        isEdit = false;
                    try {
                        if (propObject && propObject.databindObjs
                        && $.isArray(propObject.databindObjs)) {
                            $.each(propObject.databindObjs, function (index, value) {
                                var objDatabindObj = jQuery.extend(true, {}, value);
                                switch (value.bindObjType) {
                                    case "1":
                                        //bindType = MF_HELPERS.getCommandTypeByValue(value.bindObjType);
                                        mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj);
                                        showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 500);
                                        if (propObject.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                                            $('#Db_List_Options_Div').hide();
                                        }

                                        break;
                                    case "2":
                                        mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj);
                                        showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 500);
                                        if (propObject.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                                            $('#WS_List_Options').hide();
                                        }
                                        break;
                                    case "5":
                                        mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj);
                                        showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 500);
                                        if (propObject.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                                            $('#OData_List_Options_Div').hide();
                                        }
                                        break;
                                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:

                                        intlJson = MF_HELPERS
                                                    .intlsenseHelper
                                                    .getControlDatabindingIntellisenseJson(
                                                        objCurrentForm, propObject.id, true
                                                    );
                                        if (objDatabindObj != null
                                            && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                            ) {
                                            isEdit = true;
                                        }

                                        mFicientIde
                                        .MF_DATA_BINDING
                                        .processDataBindingByObjectType({
                                            controlType: MF_IDE_CONSTANTS.CONTROLS.LIST,
                                            databindObject: objDatabindObj,
                                            objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                            isEdit: isEdit,
                                            intlsJson: intlJson,
                                            control: propObject
                                        });
                                        showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 500, false);
                                        if (propObject.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                                            $('#DataBind_List_Options_Div').hide();
                                        }
                                        break;
                                }
                            });
                        }
                    }
                    catch (error) {
                        if (error instanceof MfError) {
                            showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                        } else {
                            console.log(error);
                        }
                    }
                }
            }
        },
        Advance: {
            Scripts: function (control, appObj) {
                setAdvanceGroupDisplay(appObj);
            },
            eventHandlers: {
                Scripts: function (evnt) {
                    $('#drpCntrlEvents').html();
                    $('#drpCntrlEvents').css('width', '90px');
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    $('#spanOnChngUserDefinedName').text(propObject.userDefinedName);

                    var scriptListRowTouch = '', scriptBtnCntrl = '', script = '';

                    _jsAceEditorOnChang.setTextInEditor("");
                    var options = "";
                    if (propObject.rowClickable === "1") {
                        options += '<option value="ListRowTouch">List Row Touch</option>';
                        $('#drpCntrlEvents').val("ListRowTouch");
                    }
                    if (propObject.actionButton === "1") {
                        options += '<option value="ActionButtonTouch">Action Button Touch</option>';
                        $('#drpCntrlEvents').val("ActionButtonTouch");
                    }

                    $('#drpCntrlEvents').html(options);
                    $.uniform.update('#drpCntrlEvents');
                    var prevSelectedVal = $('#drpCntrlEvents')[0].value;

                    if (propObject.scriptListRowTouch != null && propObject.scriptListRowTouch != undefined) scriptListRowTouch = propObject.fnGetScriptRowTouch();
                    if (propObject.scriptBtnCntrl != null && propObject.scriptBtnCntrl != undefined) scriptBtnCntrl = propObject.fnGetScriptBtnCntrl();

                    switch ($('#drpCntrlEvents')[0].value) {
                        case "ListRowTouch":
                            if (scriptListRowTouch != null && scriptListRowTouch != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptListRowTouch));
                            }
                            break;
                        case "ActionButtonTouch":
                            if (scriptBtnCntrl != null && scriptBtnCntrl != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptBtnCntrl));
                            }
                            break;
                    }
                    $('#drpCntrlEvents').unbind('change');
                    $('#drpCntrlEvents').bind('change', function () {
                        $.uniform.update('#drpCntrlEvents');
                        script = _jsAceEditorOnChang.getText();
                        switch (prevSelectedVal) {
                            case "ListRowTouch":
                                if (script != null && script != undefined) {
                                    scriptListRowTouch = Base64.encode(script);
                                }
                                break;
                            case "ActionButtonTouch":
                                if (script != null && script != undefined) {
                                    scriptBtnCntrl = Base64.encode(script);
                                }
                                break;
                        }
                        _jsAceEditorOnChang.setTextInEditor("");
                        prevSelectedVal = this.value;
                        switch (this.value) {
                            case "ListRowTouch":
                                if (scriptListRowTouch != null && scriptListRowTouch != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptListRowTouch));
                                }
                                break;
                            case "ActionButtonTouch":
                                if (scriptBtnCntrl != null && scriptBtnCntrl != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptBtnCntrl));
                                }
                                break;
                        }
                    });

                    SubProcShowCntrlJsEditor(true, "Scripts");
                    $('[id$=btnCntrlJsSave]').unbind('click');
                    $('[id$=btnCntrlJsSave]').bind('click', function () {
                        script = _jsAceEditorOnChang.getText();
                        switch ($('#drpCntrlEvents')[0].value) {
                            case "ListRowTouch":
                                if (script != null && script != undefined) {
                                    scriptListRowTouch = Base64.encode(script);
                                }
                                break;
                            case "ActionButtonTouch":
                                if (script != null && script != undefined) {
                                    scriptBtnCntrl = Base64.encode(script);
                                }
                        }

                        if (scriptListRowTouch != null && scriptListRowTouch != undefined) propObject.fnSetScriptRowTouch(scriptListRowTouch);
                        if (scriptBtnCntrl != null && scriptBtnCntrl != undefined) propObject.fnSetScriptBtnCntrl(scriptBtnCntrl);
                        SubProcShowCntrlJsEditor(false);
                        return false;
                    });
                    $('[id$=popup_custom_buttom_close]').unbind('click');
                    $('[id$=popup_custom_buttom_close]').bind('click', function () {
                        scriptListRowTouch = '', scriptBtnCntrl = '', scriptOnFocus = '';
                        SubProcShowCntrlJsEditor(false);
                        return false;
                    });
                }
            }
        },
        Behaviour: {
            Display: function (control, editableListObj) {
                //$(control).val(appObj.type.UIName)
            },
            ActionButton: function (control, listObj, options) {
                $(control).val(listObj.actionButton);
                var objDatabindingObj = listObj.fnGetDatabindingObj();
                var objPropSheetCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.list.propSheetCntrl.Data.getManualDataOptions();
                if (objDatabindingObj) {
                    var objbindType = objDatabindingObj.fnGetBindTypeObject();
                    if (objPropSheetCntrl) {
                        if (objbindType && objbindType === MF_IDE_CONSTANTS.ideCommandObjectTypes.None) {
                            $(control).prop("disabled", true);
                        }
                        else {
                            $(control).prop("disabled", false);
                        }
                    }
                }
                else {
                    if (objPropSheetCntrl) {
                        $(control).prop("disabled", true);
                    }
                }
                setAdvanceGroupDisplay(listObj);

                //                var currentCntrl = options.cntrlHtmlObjects;
                //                if (listObj.fnGetListTypeByValue(listObj.listType) === MF_IDE_CONSTANTS.listCntrlType.ImageList) {
                //                    if (listObj.caption === "1")
                //                        currentCntrl.wrapperDiv.hide();
                //                    else
                //                        currentCntrl.wrapperDiv.show();
                //                }
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    options.cntrlHtmlObjects.wrapperDiv.hide();
                }
                drawListHtml(listObj);
            },
            ActionButton1: function (control, listObj, options) {
                var $self = $(control);
                var objCntrlHtmlDtls = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                       .Behaviour.getActionButton1();
                var $wrapperDiv = objCntrlHtmlDtls.wrapperDiv;

                var objDatabindingObj = listObj.fnGetDatabindingObj();
                var objPropSheetCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.list.propSheetCntrl.Data.getManualDataOptions();
                if (objDatabindingObj) {
                    var objbindType = objDatabindingObj.fnGetBindTypeObject();
                    if (objPropSheetCntrl) {
                        if (objbindType && objbindType === MF_IDE_CONSTANTS.ideCommandObjectTypes.None) {
                            $wrapperDiv.hide();
                            return;
                        }
                        else {
                            $wrapperDiv.show();
                        }
                    }
                }
                else {
                    if (objPropSheetCntrl) {
                        $wrapperDiv.hide();
                        return;
                    }
                }

                if (listObj.actionButton === "1") {
                    $wrapperDiv.show();
                }
                else {
                    $wrapperDiv.hide();
                }

                if (listObj.actionBtn1Settings) {
                    $(control).val(listObj.actionBtn1Settings.text);
                }
                else {
                    $(control).val("Select Details");
                }

                var currentCntrl = options.cntrlHtmlObjects;
                if (listObj.fnGetListTypeByValue(listObj.listType) === MF_IDE_CONSTANTS.listCntrlType.ImageList) {
                    if (listObj.caption === "1")
                        currentCntrl.wrapperDiv.hide();
                    else
                        currentCntrl.wrapperDiv.show();
                }
                else if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    options.cntrlHtmlObjects.wrapperDiv.hide();
                }
            },
            ActionButton2: function (control, listObj, options) {
                var $self = $(control);
                var objCntrlHtmlDtls = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                       .Behaviour.getActionButton2();
                var $wrapperDiv = objCntrlHtmlDtls.wrapperDiv;

                var objDatabindingObj = listObj.fnGetDatabindingObj();
                var objPropSheetCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.list.propSheetCntrl.Data.getManualDataOptions();
                if (objDatabindingObj) {
                    var objbindType = objDatabindingObj.fnGetBindTypeObject();
                    if (objPropSheetCntrl) {
                        if (objbindType && objbindType === MF_IDE_CONSTANTS.ideCommandObjectTypes.None) {
                            $wrapperDiv.hide();
                            return;
                        }
                        else {
                            $wrapperDiv.show();
                        }
                    }
                }
                else {
                    if (objPropSheetCntrl) {
                        $wrapperDiv.hide();
                        return;
                    }
                }

                if (listObj.actionButton === "1") {
                    $wrapperDiv.show();
                }
                else {
                    $wrapperDiv.hide();
                }

                if (listObj.actionBtn2Settings) {
                    $(control).val(listObj.actionBtn2Settings.text);
                }
                else {
                    $(control).val("Select Details");
                }

                var currentCntrl = options.cntrlHtmlObjects;
                if (listObj.fnGetListTypeByValue(listObj.listType) === MF_IDE_CONSTANTS.listCntrlType.ImageList) {
                    if (listObj.caption === "1")
                        currentCntrl.wrapperDiv.hide();
                    else
                        currentCntrl.wrapperDiv.show();
                }
                else if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    options.cntrlHtmlObjects.wrapperDiv.hide();
                }
            },
            ActionButton3: function (control, listObj, options) {
                var $self = $(control);
                var objCntrlHtmlDtls = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                       .Behaviour.getActionButton3();
                var $wrapperDiv = objCntrlHtmlDtls.wrapperDiv;

                var objDatabindingObj = listObj.fnGetDatabindingObj();
                var objPropSheetCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.list.propSheetCntrl.Data.getManualDataOptions();
                if (objDatabindingObj) {
                    var objbindType = objDatabindingObj.fnGetBindTypeObject();
                    if (objPropSheetCntrl) {
                        if (objbindType && objbindType === MF_IDE_CONSTANTS.ideCommandObjectTypes.None) {
                            $wrapperDiv.hide();
                            return;
                        }
                        else {
                            $wrapperDiv.show();
                        }
                    }
                }
                else {
                    if (objPropSheetCntrl) {
                        $wrapperDiv.hide();
                        return;
                    }
                }

                if (listObj.actionButton === "1") {
                    $wrapperDiv.show();
                }
                else {
                    $wrapperDiv.hide();
                }

                if (listObj.actionBtn3Settings) {
                    $(control).val(listObj.actionBtn3Settings.text);
                }
                else {
                    $(control).val("Select Details");
                }

                var currentCntrl = options.cntrlHtmlObjects;
                if (listObj.fnGetListTypeByValue(listObj.listType) === MF_IDE_CONSTANTS.listCntrlType.ImageList) {
                    if (listObj.caption === "1")
                        currentCntrl.wrapperDiv.hide();
                    else
                        currentCntrl.wrapperDiv.show();
                }
                else if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    options.cntrlHtmlObjects.wrapperDiv.hide();
                }
            },
            DetectRowTouch: function (control, listObj) {
                var objDatabindingObj = listObj.fnGetDatabindingObj();
                var objPropSheetCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Behaviour.getRowTouchCntrl();

                if (objDatabindingObj) {
                    var objbindType = objDatabindingObj.fnGetBindTypeObject();
                    if (objPropSheetCntrl) {
                        if (objbindType && objbindType !== MF_IDE_CONSTANTS.ideCommandObjectTypes.None) {
                            objPropSheetCntrl.control.html(dataBindingOptionsHtml);
                        }
                        else {
                            objPropSheetCntrl.control.html(manualOptionsHtml);
                        }
                    }
                }
                else {
                    if (objPropSheetCntrl) {
                        objPropSheetCntrl.control.html(manualOptionsHtml);
                    }
                }
                if (listObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                    objPropSheetCntrl.control.html(manualOptionsHtml);
                }
                $(control).val(listObj.rowClickable);

                setAdvanceGroupDisplay(listObj);

                drawListHtml(listObj);
            },
            SelectedRow: function (control, listObj, options) {
                var objPropSheetCntrls = options.cntrlHtmlObjects;
                if (listObj.rowClickable === "2" || listObj.rowClickable === "3") {
                    if (listObj.selectedRowStyle) {
                        $(control).val(listObj.selectedRowStyle);
                    }
                    objPropSheetCntrls.wrapperDiv.show();
                }
                else {
                    objPropSheetCntrls.wrapperDiv.hide();
                }
            },
            SelectControl: function (control, editableListObj) {
                //$(control).val(editableListObj.userDefinedName);
            },
            TitleColor: function (control, listObj, options) {
                //$(control).val(listObj.titleColor);
                var objPropSheetCntrls = options.cntrlHtmlObjects;
                objPropSheetCntrls.control
                .removeAttr('disabled readonly')
                .removeClass('propDisable');

                objPropSheetCntrls.browseBtn.val('');
                if (listObj.titleColor) {
                    objPropSheetCntrls.control.val(listObj.titleColor);
                    objPropSheetCntrls.browseBtn.css('background-color', listObj.titleColor);
                }
                else {
                    objPropSheetCntrls.control.val('#000000');
                    objPropSheetCntrls.browseBtn.css('background-color', '#000000');
                }
                // var titleColorPicker = $(objPropSheetCntrls.browseBtn).colpick({
                //     layout: 'hex',
                //     submit: 0,
                //     colorScheme: 'dark',
                //     onChange: function (hsb, hex, rgb, el, bySetColor) {
                //         $(objPropSheetCntrls.browseBtn).css('background-color', '#' + hex);
                //         listObj.fnSetTitleColor(hex);
                //         $(objPropSheetCntrls.control).val('#' + hex);
                //         if (!bySetColor) $(objPropSheetCntrls.control).val('#' + hex);
                //     }
                // });
                // $(objPropSheetCntrls.control).keyup(function () {
                //     $(titleColorPicker).colpickSetColor(this.value);
                // });
                var fontCol = '#000000';
                var titleColorPicker = $(objPropSheetCntrls.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(objPropSheetCntrls.control).val('#' + hex);
                            fontCol = '#' + hex;
                        }

                        if ($(objPropSheetCntrls.control).val() == "" || $(objPropSheetCntrls.control).val() == undefined)
                            fontCol = '#000000';

                        $(objPropSheetCntrls.control).val(fontCol);
                        $(objPropSheetCntrls.browseBtn).css('background-color', fontCol);
                        //appObj.fontColor = $(objPropSheetCntrls.control).val();
                        listObj.fnSetTitleColor($(objPropSheetCntrls.control).val());
                    }
                });
                $(objPropSheetCntrls.control).keyup(function (evnt) {
                    listObj.fnSetTitleColor(evnt.target.value);
                    fontCol = evnt.target.value;
                    $(titleColorPicker).colpickSetColor(evnt.target.value);
                });
                $(objPropSheetCntrls.control).bind("change", function (evnt) {
                    listObj.fnSetTitleColor(evnt.target.value);
                    fontCol = evnt.target.value;
                    $(titleColorPicker).colpickSetColor(evnt.target.value);
                });
                $(objPropSheetCntrls.control).bind("blur", function (evnt) {
                    listObj.fnSetTitleColor(evnt.target.value);
                    fontCol = evnt.target.value;
                    $(titleColorPicker).colpickSetColor(evnt.target.value);
                });


                var selectedRowStyle = listObj.fnGetSelectedRowStyle();
                if (selectedRowStyle === "1") {
                    objPropSheetCntrls.wrapperDiv.show();
                }
                else {
                    objPropSheetCntrls.wrapperDiv.hide();
                }
            },
            BackNavRefresh: function (control, listObj, options) {
                var backNavRefresh = listObj.fnGetBackNavRefresh();
                if (backNavRefresh) {
                    $(control).val(backNavRefresh);
                }

                var objPropSheetCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Behaviour.getBackNavRefresh();
                setDisplay(listObj, objPropSheetCntrl);
            },
            eventHandlers: {
                Display: function (evnt) {
                },
                SelectControl: function (evnt) {
                },
                ActionButton: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    if ($self.val() === "0") {
                        propObject.fnDeleteActionBtn1Settings();
                        propObject.fnDeleteActionBtn2Settings();
                        propObject.fnDeleteActionBtn3Settings();
                        //var objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                        //if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                        //objForm.fnDeleteActionBtnSettingByType(MF_IDE_CONSTANTS.listActionBtnType.actionBtn1);
                        //objForm.fnDeleteActionBtnSettingByType(MF_IDE_CONSTANTS.listActionBtnType.actionBtn2);
                        //objForm.fnDeleteActionBtnSettingByType(MF_IDE_CONSTANTS.listActionBtnType.actionBtn3);

                        var objActBt1Cntrl = evnt.data.cntrls.ActionButton1;
                        var objActBt2Cntrl = evnt.data.cntrls.ActionButton2;
                        var objActBt3Cntrl = evnt.data.cntrls.ActionButton3;
                        $('#' + objActBt1Cntrl.controlId).val('Select Details');
                        $('#' + objActBt2Cntrl.controlId).val('Select Details');
                        $('#' + objActBt3Cntrl.controlId).val('Select Details');
                        $('#' + objActBt1Cntrl.wrapperDiv).hide();
                        $('#' + objActBt2Cntrl.wrapperDiv).hide();
                        $('#' + objActBt3Cntrl.wrapperDiv).hide();

                        propObject.actionButton = "0";
                    }
                    else {
                        propObject.actionButton = "1";
                        var objActBt1Cntrl = evnt.data.cntrls.ActionButton1;
                        var objActBt2Cntrl = evnt.data.cntrls.ActionButton2;
                        var objActBt3Cntrl = evnt.data.cntrls.ActionButton3;
                        $('#' + objActBt1Cntrl.controlId).val('Select Details');
                        $('#' + objActBt2Cntrl.controlId).val('Select Details');
                        $('#' + objActBt3Cntrl.controlId).val('Select Details');
                        $('#' + objActBt1Cntrl.wrapperDiv).show();
                        $('#' + objActBt2Cntrl.wrapperDiv).show();
                        $('#' + objActBt3Cntrl.wrapperDiv).show();
                    }

                    setAdvanceGroupDisplay(propObject);

                    drawListHtml(propObject);
                },
                ActionButton1: function (evnt) {
                    var evntData = evnt.data;
                    try {
                        mFicientIde.ListControlHelper
                        .setDataOfActionBtnSettingsContainerDiv(
                            MF_IDE_CONSTANTS.listActionBtnType.actionBtn1
                        );
                        mFicientIde.ListControlHelper
                        .processShowActionBtnSettingsDivBytype(
                            evntData.propObject,
                            MF_IDE_CONSTANTS.listActionBtnType.actionBtn1
                        );
                    }
                    catch (error) {
                        if (error instanceof MfError) {
                            showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                        } else {
                            console.log(error);
                        }
                    }
                },
                ActionButton2: function (evnt) {
                    var evntData = evnt.data;
                    try {
                        mFicientIde.ListControlHelper
                        .setDataOfActionBtnSettingsContainerDiv(
                            MF_IDE_CONSTANTS.listActionBtnType.actionBtn2
                        );
                        mFicientIde.ListControlHelper
                        .processShowActionBtnSettingsDivBytype(
                            evntData.propObject,
                            MF_IDE_CONSTANTS.listActionBtnType.actionBtn2
                        );
                    }
                    catch (error) {
                        if (error instanceof MfError) {
                            showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                        } else {
                            console.log(error);
                        }
                    }
                },
                ActionButton3: function (evnt) {
                    var evntData = evnt.data;
                    try {
                        mFicientIde.ListControlHelper
                        .setDataOfActionBtnSettingsContainerDiv(
                            MF_IDE_CONSTANTS.listActionBtnType.actionBtn3
                        );
                        mFicientIde.ListControlHelper
                        .processShowActionBtnSettingsDivBytype(
                            evntData.propObject,
                            MF_IDE_CONSTANTS.listActionBtnType.actionBtn3
                        );
                    }
                    catch (error) {
                        if (error instanceof MfError) {
                            showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                        } else {
                            console.log(error);
                        }
                    }
                },
                DetectRowTouch: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;

                    var iRowClickable = 0;

                    var actionBtnConfigured = propObject.fnGetActionButton();
                    if (actionBtnConfigured === "1") {
                        if ($self.val() === "2" || $self.val() === "3") {
                            $self.val(propObject.rowClickable);
                            showMessage("Row selection not allowed if action button are configured.", DialogType.Error);
                            return;
                        }
                    }

                    if (propObject.fnGetListTypeValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
                        if ($self.val() === "2" || $self.val() === "3") {
                            $self.val(propObject.rowClickable);
                            showMessage("Row selection not allowed if action button are configured.", DialogType.Error);
                            return;
                        }
                    }
                    propObject.rowClickable = $self.val();
                    if ($self.val() === "1" || $self.val() === "2" || $self.val() === "3") {
                        iRowClickable = "1";
                    }
                    else {
                        iRowClickable = "0";
                    }
                    var objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                    objForm.fnSetRowClickableOfForm(iRowClickable);

                    var $dataTitleColorWrapper = $('#' + evntData.cntrls.TitleColor.wrapperDiv);
                    var $dataTitleColorCntrl = $('#' + evntData.cntrls.TitleColor.controlId);
                    var $dataTitleColorBrowse = $('#' + evntData.cntrls.TitleColor.browseBtn);

                    var $dataSelectRowWrapper = $('#' + evntData.cntrls.SelectedRow.wrapperDiv);
                    var $dataSelectRowCntrl = $('#' + evntData.cntrls.SelectedRow.controlId);

                    var $dataSelectRowDatabindWrapper = $('#' + evntData.cntrls.SelectedRowDatabind.wrapperDiv);
                    var $dataSelectRowDatabindCntrl = $('#' + evntData.cntrls.SelectedRowDatabind.controlId);

                    if ($self.val() === "0" || $self.val() === "1") {
                        $dataTitleColorCntrl.val('');
                        $dataTitleColorWrapper.hide();
                        $dataTitleColorBrowse.removeAttr('style');
                        $dataSelectRowCntrl.val('0');
                        $dataSelectRowWrapper.hide();
                        $dataSelectRowDatabindWrapper.hide();
                        $dataSelectRowDatabindCntrl.val('None');
                        propObject.fnDeleteTitleColor();
                        propObject.fnDeleteSelectedRowStyle();
                        propObject.fnDeleteSelectedRowDatabind();
                    }
                    else {
                        $dataSelectRowWrapper.show();
                        $dataSelectRowDatabindWrapper.show();
                        var selectedRowStyle = propObject.fnGetSelectedRowStyle();
                        if (selectedRowStyle && selectedRowStyle === "1") {
                            $dataTitleColorWrapper.show();
                        }
                        else {
                            $dataTitleColorWrapper.hide();
                            $dataTitleColorBrowse.removeAttr('style');
                        }
                    }

                    setAdvanceGroupDisplay(propObject);

                    drawListHtml(propObject);
                },
                SelectedRow: function (evnt) {
                    var evntData = evnt.data;

                    var $dataTitleColorWrapper = $('#' + evntData.cntrls.TitleColor.wrapperDiv);
                    var $dataTitleColorCntrl = $('#' + evntData.cntrls.TitleColor.controlId);

                    var propObject = evntData.propObject;
                    var $self = $(this);
                    propObject.fnSetSelectedRowStyle($self.val());
                    if ($self.val() === "0") {
                        $dataTitleColorWrapper.hide();
                        propObject.fnDeleteTitleColor();
                    }
                    else {
                        $dataTitleColorWrapper.show();
                        propObject.fnDeleteTitleColor();
                    }
                    $dataTitleColorCntrl.val("");
                },
                BackNavRefresh: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    var propObject = evntData.propObject;
                    propObject.fnSetBackNavRefresh($self.val());
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getBehaviourActionBtn1Cntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Behaviour", "ActionButton1");
            }
            return objControl;
        }
        function _getBehaviourActionBtn2Cntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Behaviour", "ActionButton2");
            }
            return objControl;
        }
        function _getBackNavRefreshCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Behaviour", "BackNavRefresh");
            }
            return objControl;
        }
        function _getBehaviourActionBtn3Cntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Behaviour", "ActionButton3");
            }
            return objControl;
        }
        function _getBehaviourRowTouchCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Behaviour", "DetectRowTouch");
            }
            return objControl;
        }
        function _getDataRowIDCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Data", "RowID");
            }
            return objControl;
        }
        function _getDataImageNameCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Data", "ImageName");
            }
            return objControl;
        }
        function _getDataTitleCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Data", "Title");
            }
            return objControl;
        }
        function _getDataSubtitleCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Data", "Subtitle");
            }
            return objControl;
        }
        function _getDataInformationCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Data", "Information");
            }
            return objControl;
        }
        function _getDataAdditionalInformationCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Data", "AdditionalInformation");
            }
            return objControl;
        }
        function _getDataCountBubbleCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Data", "CountBubble");
            }
            return objControl;
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getNoOfRecordsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Data", "NoOfRecords");
            }
            return objControl;
        }
        function _getDataSelectedRowDatabindCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Data", "SelectedRowDatabind");
            }
            return objControl;
        }
        function _getManualDataOptionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Data", "ManualData");
            }
            return objControl;
        }
        function _getEnablePaging() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Data", "EnablePaging");
            }
            return objControl;
        }
        function _getDataDateDatabindCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Data", "DateDatabind");
            }
            return objControl;
        }
        function _getDataGroupingCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Appearance", "DataGrouping");
            }
            return objControl;
        }
        function _getCountBubbleCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Appearance", "AppearanceCountBubble");
            }
            return objControl;
        }
        function _getEmptyListMessageCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Appearance", "EmptyListMessage");
            }
            return objControl;
        }
        function _getAdvanceGroup() {
            var divData = _getDivControlHelperData();
            var objWrapper = null;
            if (divData) {
                objWrapper = divData.getGroupWrapperDivPrefix(
                    MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                    "Advance");
            }
            return objWrapper;
        }

        var _appearance = (function () {
            function _getAllowFilter() {
                var divData = _getDivControlHelperData();
                var objControl = null;
                if (divData) {
                    objControl = divData.getControlDtls(
                        MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                        "Appearance", "AllowFilter");
                }
                return objControl;
            }
            function _getPlaceholder() {
                var divData = _getDivControlHelperData();
                var objControl = null;
                if (divData) {
                    objControl = divData.getControlDtls(
                        MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                        "Appearance", "Placeholder");
                }
                return objControl;
            }
            function _getListType() {
                var divData = _getDivControlHelperData();
                var objControl = null;
                if (divData) {
                    objControl = divData.getControlDtls(
                        MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                        "Appearance", "ListType");
                }
                return objControl;
            }
            function _getDataInset() {
                var divData = _getDivControlHelperData();
                var objControl = null;
                if (divData) {
                    objControl = divData.getControlDtls(
                       MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                       "Appearance", "DataInset");
                }
                return objControl;
            }
            function _getCaption() {
                var divData = _getDivControlHelperData();
                var objControl = null;
                if (divData) {
                    objControl = divData.getControlDtls(
                        MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                        "Appearance", "Caption");
                }
                return objControl;
            }
            return {
                getAllowFilter: function () {
                    return _getAllowFilter();
                },
                getPlaceholder: function () {
                    return _getPlaceholder();
                },
                getListType: function () {
                    return _getListType();
                },
                getDataInset: function () {
                    return _getDataInset();
                },
                getCaption: function () {
                    return _getCaption();
                },
                getAllControls: function () {
                    return {
                        ListType: _getListType(),
                        DataInset: _getDataInset(),
                        Caption: _getCaption(),
                        DataGrouping: _getDataGroupingCntrl(),
                        AppearanceCountBubble: _getCountBubbleCntrl(),
                        EmptyListMessage: _getEmptyListMessageCntrl(),
                        AllowFilter: _getAllowFilter(),
                        Placeholder: _getPlaceholder()
                    };
                }
            };
        })();
        var _data = (function () {
            function _getDatabinding() {
                var divData = _getDivControlHelperData();
                var objControl = null;
                if (divData) {
                    objControl = divData.getControlDtls(
                        MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                        "Data", "Databinding");
                }
                return objControl;
            }
            function _getManualData() {
                var divData = _getDivControlHelperData();
                var objControl = null;
                if (divData) {
                    objControl = divData.getControlDtls(
                        MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                        "Data", "ManualData");
                }
                return objControl;
            }
            return {
                getDatabinding: function () {
                    return _getDatabinding();
                },
                getManualData: function () {
                    return _getManualData();
                },
                getAllControls: function () {
                    return {
                        Databinding: _getDatabinding(),
                        ManualData: _getManualData(),
                        DataObject: _getDataObjectCntrl(),
                        RowID: _getDataRowIDCntrl(),
                        Title: _getDataTitleCntrl(),
                        Subtitle: _getDataSubtitleCntrl(),
                        Information: _getDataInformationCntrl(),
                        AdditionalInformation: _getDataAdditionalInformationCntrl(),
                        ImageName: _getDataImageNameCntrl(),
                        SelectedRowDatabind: _getDataSelectedRowDatabindCntrl(),
                        CountBubble: _getDataCountBubbleCntrl(),
                        DateDatabind: _getDataDateDatabindCntrl(),
                        EnablePaging: _getEnablePaging(),
                        NoOfRecords: _getNoOfRecordsCntrl()
                    };
                }
            };
        })();
        var _behaviour = (function () {
            function _getDisplay() {
                var divData = _getDivControlHelperData();
                var objControl = null;
                if (divData) {
                    objControl = divData.getControlDtls(
                        MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                        "Behaviour", "Display");
                }
                return objControl;
            }
            function _getActionButton() {
                var divData = _getDivControlHelperData();
                var objControl = null;
                if (divData) {
                    objControl = divData.getControlDtls(
                        MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                        "Behaviour", "ActionButton"
                    );
                }
                return objControl;
            }
            function _getSelectedRow() {
                var divData = _getDivControlHelperData();
                var objControl = null;
                if (divData) {
                    objControl = divData.getControlDtls(
                        MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                        "Behaviour", "SelectedRow"
                    );
                }
                return objControl;
            }
            function _getTitleColor() {
                var divData = _getDivControlHelperData();
                var objControl = null;
                if (divData) {
                    objControl = divData.getControlDtls(
                        MF_IDE_CONSTANTS.CONTROLS.LIST.propPluginPrefix,
                        "Behaviour", "TitleColor"
                    );
                }
                return objControl;
            }
            return {
                getDisplay: function () {
                    return _getDisplay();
                },
                getActionButton: function () {
                    return _getActionButton();
                },
                getSelectedRow: function () {
                    return _getSelectedRow();
                },
                getTitleColor: function () {
                    return _getTitleColor();
                },
                getDetectRowTouch: function () {
                    return _getBehaviourRowTouchCntrl();
                },
                getAllControls: function () {
                    return {
                        Display: _getDisplay(),
                        ActionButton: _getActionButton(),
                        SelectControl: _getSelectedRow(),
                        ActionButton1: _getBehaviourActionBtn1Cntrl(),
                        ActionButton2: _getBehaviourActionBtn2Cntrl(),
                        ActionButton3: _getBehaviourActionBtn3Cntrl(),
                        DetectRowTouch: _getBehaviourRowTouchCntrl(),
                        TitleColor: _getTitleColor(),
                        BackNavRefresh: _getBackNavRefreshCntrl()
                    };
                }
            };
        })();
        var _advance = (function () {

            return {
                getCompleteGroup: function () {
                    return _getAdvanceGroup();
                }
            };
        })();
        return {
            getAdvanceGroup: _getAdvanceGroup,
            Data: {
                getRowId: _getDataRowIDCntrl,
                getImageName: _getDataImageNameCntrl,
                getTitle: _getDataTitleCntrl,
                getSubtitle: _getDataSubtitleCntrl,
                getInformation: _getDataInformationCntrl,
                getAdditionalInformation: _getDataAdditionalInformationCntrl,
                getCountBubble: _getDataCountBubbleCntrl,
                getDataDateDatabindCntrl: _getDataDateDatabindCntrl,
                getDataObject: _getDataObjectCntrl,
                getNoOfRecords: _getNoOfRecordsCntrl,
                getSelectedRowDatabind: _getDataSelectedRowDatabindCntrl,
                getManualDataOptions: _getManualDataOptionsCntrl,
                getEnablePaging: _getEnablePaging,
                getDatabinding: _data.getDatabinding,
                getAllControls: _data.getAllControls
            },
            Behaviour: {
                getActionButton1: _getBehaviourActionBtn1Cntrl,
                getActionButton2: _getBehaviourActionBtn2Cntrl,
                getActionButton3: _getBehaviourActionBtn3Cntrl,
                getRowTouchCntrl: _getBehaviourRowTouchCntrl,
                getBackNavRefresh: _getBackNavRefreshCntrl,
                getAllControls: _behaviour.getAllControls,
                getDetectRowTouch: _behaviour.getDetectRowTouch
            },
            Appearance: {
                getDataGrouping: _getDataGroupingCntrl,
                getCountBubble: _getCountBubbleCntrl,
                getEmptyListMessage: _getEmptyListMessageCntrl,
                getAllowFilter: _appearance.getAllowFilter,
                getPlaceholder: _appearance.getPlaceholder,
                getAllControls: _appearance.getAllControls
            }
        };
    })()
};
mFicientIde.PropertySheetJson.List = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST,
    "groups": [
        {
            "name": "Control Properties",
            "prefixText": "ControlProperties",
            "hidden": true,
            "properties": [{
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

                ],
                "customValidations": [

                ],
                "events": [

                ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "List",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

                ],
                "customValidations": [

                ],
                "events": [{
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.list.groups.ControlProperties.eventHandlers.Name,
                    "context": "",
                    "arguments": {}
                }],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

                ],
                "customValidations": [

                ],
                "events": [{
                    "name": "change",
                    "func": PROP_JSON_HTML_MAP.list.groups.ControlProperties.eventHandlers.Description,
                    "context": "",
                    "arguments": {}
                }

                ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
            ]
        },
        {
            "name": "Appearance",
            "prefixText": "Appearance",
            "hidden": false,
            "properties": [
              {
                  "text": "List Type",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "ListType",
                  "defltValue": "1",
                  "validations": [
                    ],
                  "customValidations": [

                 ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.list.groups.Appearance.eventHandlers.ListType,
                        "context": "",
                        "arguments": {
                            "cntrls": [
                             {
                                 grpPrefText: "Appearance",
                                 cntrlPrefText: "Caption",
                                 rtrnPropNm: "Caption"
                             },
                             {
                                 grpPrefText: "Appearance",
                                 cntrlPrefText: "DataGrouping",
                                 rtrnPropNm: "DataGrouping"
                             },
                             {
                                 grpPrefText: "Appearance",
                                 cntrlPrefText: "AllowFilter",
                                 rtrnPropNm: "AppearanceAllowFilter"
                             },
                             {
                                 grpPrefText: "Appearance",
                                 cntrlPrefText: "AppearanceCountBubble",
                                 rtrnPropNm: "AppearanceCountBubble"
                             },
                             {
                                 grpPrefText: "Appearance",
                                 cntrlPrefText: "Placeholder",
                                 rtrnPropNm: "AppearanceFilterPlaceholder"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "RowID",
                                 rtrnPropNm: "DataRowId"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "ImageName",
                                 rtrnPropNm: "DataImageName"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "Title",
                                 rtrnPropNm: "DataTitle"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "Subtitle",
                                 rtrnPropNm: "DataSubtitle"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "Information",
                                 rtrnPropNm: "DataInfo"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "AdditionalInformation",
                                 rtrnPropNm: "DataAdditionalInfo"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "Databinding",
                                 rtrnPropNm: "Databinding"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "DateDatabind",
                                 rtrnPropNm: "DataDate"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "DataObject",
                                 rtrnPropNm: "DataObject"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "ManualData",
                                 rtrnPropNm: "DataManualData"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "SelectedRowDatabind",
                                 rtrnPropNm: "DataSelectedRow"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "CountBubble",
                                 rtrnPropNm: "DataCountBubble"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "DataObject",
                                 rtrnPropNm: "DataObject"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "EnablePaging",
                                 rtrnPropNm: "DataEnablePaging"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "NoOfRecords",
                                 rtrnPropNm: "DataNoOfRecords"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton",
                                 rtrnPropNm: "ActionButton"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton1",
                                 rtrnPropNm: "ActionButton1"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton2",
                                 rtrnPropNm: "ActionButton2"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton3",
                                 rtrnPropNm: "ActionButton3"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "DetectRowTouch",
                                 rtrnPropNm: "BehaviourDetectRowTouch"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "SelectedRow",
                                 rtrnPropNm: "BehaviourSelectedRow"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "TitleColor",
                                 rtrnPropNm: "BehaviourTitleColor"
                             }

                          ]
                        }
                    }
                ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false,
                      "defltValue": "1"
                  },
                  "options": [
                    {
                        "text": "Simple",
                        "value": "0"
                    },
                    {
                        "text": "Basic",
                        "value": "1"
                    },
                    {
                        "text": "Basic with description",
                        "value": "2"
                    }
                    ,
                    {
                        "text": "Numbered",
                        "value": "3"
                    },
                     {
                         "text": "Icon",
                         "value": "4"
                     }, {
                         "text": "Thumbnail",
                         "value": "5"
                     }, {
                         "text": "Image List",
                         "value": "6"
                     }, {
                         "text": "Event List",
                         "value": "7"
                     }, {
                         "text": "Custom List",
                         "value": "8"
                     }
                 ]
              },
              {
                  "text": "Data Inset",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "DataInset",
                  "defltValue": "1",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.list.groups.Appearance.eventHandlers.DataInset,
                        "context": "",
                        "arguments": {
                        }
                    }
                ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false,
                      "defltValue": "0"
                  },
                  "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                 ]
              },
              {
                  "text": "Overlay",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "Caption",
                  "defltValue": "1",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.list.groups.Appearance.eventHandlers.SetCaption,
                        "context": "",
                        "arguments": {
                            "cntrls": [
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton",
                                 rtrnPropNm: "ActionButton"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton1",
                                 rtrnPropNm: "ActionButton1"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton2",
                                 rtrnPropNm: "ActionButton2"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton3",
                                 rtrnPropNm: "ActionButton3"
                             },
                             {
                                 grpPrefText: "Appearance",
                                 cntrlPrefText: "AppearanceCountBubble",
                                 rtrnPropNm: "AppearanceCountBubble"
                             }
                           ]
                        }
                    }
                ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": true,
                      "defltValue": "0"
                  },
                  "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                 ]
              },
              {
                  "text": "Data Grouping",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "DataGrouping",
                  "defltValue": "1",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.list.groups.Appearance.eventHandlers.DataGrouping,
                        "context": "",
                        "arguments": {
                        }
                    }
                ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false,
                      "defltValue": "0"
                  },
                  "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                 ]
              },
              {
                  "text": "Count Bubble",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "AppearanceCountBubble",
                  "defltValue": "1",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.list.groups.Appearance.eventHandlers.SetCountBubble,
                        "context": "",
                        "arguments": {
                        }
                    }
                ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false,
                      "defltValue": "0"
                  },
                  "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                 ]
              },
              {
                  "text": "Empty List Message",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                  "prefixText": "EmptyListMessage",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [{
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.list.groups.Appearance.eventHandlers.EmptyListMessage,
                      "context": "",
                      "arguments": {}
                  }],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
              {
                  "text": "Allow Filter",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "AllowFilter",
                  "defltValue": "1",
                  "validations": [
                  ],
                  "customValidations": [
                  ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.list.groups.Appearance.eventHandlers.AllowFilter,
                        "context": "",
                        "arguments": {
                        }
                    }
                  ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false,
                      "defltValue": MF_IDE_CONSTANTS.defltVal.no
                  },
                  "options": [
                    {
                        "text": "No",
                        "value": MF_IDE_CONSTANTS.defltVal.no
                    },
                    {
                        "text": "Yes",
                        "value": MF_IDE_CONSTANTS.defltVal.yes
                    }
                 ]
              },
              {
                  "text": "Filter Placeholder",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "Placeholder",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [
                     {
                         "name": "change",
                         "func": PROP_JSON_HTML_MAP.list.groups.Appearance.eventHandlers.Placeholder,
                         "context": "",
                         "arguments": {
                         }
                     }
                  ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": false,
                      "defltValue": MF_IDE_CONSTANTS.defltVal.searchPlaceHolder,
                      "hidden": true
                  }
              }
           ]
        },
        {
            "name": "Data",
            "prefixText": "Data",
            "hidden": false,
            "properties": [
                {
                    "text": "Databinding",
                    "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                    "prefixText": "Databinding",
                    "defltValue": "0",
                    "validations": [

                 ],
                    "customValidations": [

                 ],
                    "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.list.groups.Data.eventHandlers.Databinding,
                        "context": "",
                        "arguments": {
                            "cntrls": [
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "DataObject",
                                 rtrnPropNm: "DataObject"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "RowID",
                                 rtrnPropNm: "RowId"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "ImageName",
                                 rtrnPropNm: "ImageName"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "Title",
                                 rtrnPropNm: "Title"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "Subtitle",
                                 rtrnPropNm: "Subtitle"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "Information",
                                 rtrnPropNm: "Information"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "AdditionalInformation",
                                 rtrnPropNm: "AdditionalInformation"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "CountBubble",
                                 rtrnPropNm: "CountBubble"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "SelectedRowDatabind",
                                 rtrnPropNm: "SelectedRowDatabind"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "DateDatabind",
                                 rtrnPropNm: "DateDatabind"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "EnablePaging",
                                 rtrnPropNm: "EnablePaging"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "NoOfRecords",
                                 rtrnPropNm: "DataNoOfRecords"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "ManualData",
                                 rtrnPropNm: "ManualData"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "ImageName",
                                 rtrnPropNm: "ImageName"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "DetectRowTouch",
                                 rtrnPropNm: "DetectRowTouch"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton",
                                 rtrnPropNm: "ActionButton"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton1",
                                 rtrnPropNm: "ActionButton1"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton2",
                                 rtrnPropNm: "ActionButton2"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton3",
                                 rtrnPropNm: "ActionButton3"
                             },
                             {
                                 grpPrefText: "Appearance",
                                 cntrlPrefText: "DataGrouping",
                                 rtrnPropNm: "DataGrouping"
                             },
                             {
                                 grpPrefText: "Appearance",
                                 cntrlPrefText: "EmptyListMessage",
                                 rtrnPropNm: "EmptyListMessage"
                             },
                             {
                                 grpPrefText: "Appearance",
                                 cntrlPrefText: "AppearanceCountBubble",
                                 rtrnPropNm: "AppearanceCountBubble"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "BackNavRefresh",
                                 rtrnPropNm: "BackNavRefresh"
                             },
                             {
                                 grpPrefText: "Appearance",
                                 cntrlPrefText: "AllowFilter",
                                 rtrnPropNm: "AppearanceAllowFilter"
                             },
                             {
                                 grpPrefText: "Appearance",
                                 cntrlPrefText: "Placeholder",
                                 rtrnPropNm: "AppearanceFilterPlaceholder"
                             }
                          ]
                        }
                    }
                 ],
                    "CntrlProp": "",
                    "HelpText": "",
                    "selectValueBy": "",
                    "init": {
                        "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                        "disabled": false,
                        "hidden": false,
                        "defltValue": "0"
                    },
                    "options": [
                    {
                        "text": "Manual",
                        "value": "0"
                    },
                    {
                        "text": "Database Object",
                        "value": "1"
                    },
                    {
                        "text": "Web Service Object",
                        "value": "2"
                    },
                    {
                        "text": "OData Object",
                        "value": "5"
                    },
                    {
                        "text": "Offline Database",
                        "value": "6"
                    }
                 ]
                },
              {
                  "text": "Manual Data",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "ManualData",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
               ],
                  "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.list.groups.Data.eventHandlers.ManualData,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "2 Option(s)",
                      "hidden": true
                  }
              },
              {
                  "text": "Data Object",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "DataObject",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

                 ],
                  "customValidations": [
                    {
                        "func": "functionName",
                        "context": "context"
                    }
                 ],
                  "events": [
                    {
                        "name": "click",
                        "func": PROP_JSON_HTML_MAP.list.groups.Data.eventHandlers.DataObject,
                        "context": "",
                        "arguments": {}
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "Select Object",
                      "hidden": true
                  }
              },
              {
                  "text": "Row ID",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "RowID",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": true,
                      "defltValue": "None",
                      "hidden": false
                  }
              },
              {
                  "text": "Title",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "Title",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": true,
                      "defltValue": "None",
                      "hidden": false
                  }
              },
              {
                  "text": "Subtitle",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "Subtitle",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": true,
                      "defltValue": "None",
                      "hidden": true
                  }
              },
              {
                  "text": "Information",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "Information",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": true,
                      "defltValue": "None",
                      "hidden": true
                  }
              },
              {
                  "text": "Additional Information",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "AdditionalInformation",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": true,
                      "defltValue": "None",
                      "hidden": true
                  }
              },
              {
                  "text": "Image Name",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "ImageName",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": true,
                      "defltValue": "None",
                      "hidden": true
                  }
              },
              {
                  "text": "Selected Row",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "SelectedRowDatabind",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": true,
                      "defltValue": "None",
                      "hidden": true
                  }
              },
              {
                  "text": "Count Bubble",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "CountBubble",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": true,
                      "defltValue": "None",
                      "hidden": true
                  }
              },
              {
                  "text": "Date",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "DateDatabind",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": true,
                      "defltValue": "None",
                      "hidden": true
                  }
              },
              {
                  "text": "Enable Paging",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "EnablePaging",
                  "defltValue": "0",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.list.groups.Data.eventHandlers.EnablePaging,
                        "context": "",
                        "arguments": {
                            "cntrls": [
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "NoOfRecords",
                                 rtrnPropNm: "NoOfRecords"
                             }
                          ]
                        }
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false
                  },
                  "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                 ]
              },
              {
                  "text": "No. of Records",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                  "prefixText": "NoOfRecords",
                  "noOfLines": "0",
                  "validations": [],
                  "customValidations": [],
                  "events": [
                        {
                            "name": "change",
                            "func": PROP_JSON_HTML_MAP.list.groups.Data.eventHandlers.NoOfRecords,
                            "context": "",
                            "arguments": {
                            }
                        }
                ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "maxlength": 50,
                      "disabled": false,
                      "defltValue": "10",
                      "hidden": true
                  }
              }
           ]
        },
        {
            "name": "Advance",
            "prefixText": "Advance",
            "hidden": false,
            "properties": [
          {
              "text": "Scripts",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
              "prefixText": "Scripts",
              "disabled": false,
              "hidden": true,
              "noOfLines": "0",
              "validations": [

               ],
              "customValidations": [
               ],
              "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.list.groups.Advance.eventHandlers.Scripts,
                      "context": "",
                      "arguments": {}
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "init": {
                  "disabled": false,
                  "defltValue": "",
                  "hidden": false
              }
          }
          ]
        },
        {
            "name": "Behaviour",
            "prefixText": "Behaviour",
            "hidden": false,
            "properties": [
              {
                  "text": "Display",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "Display",
                  "defltValue": "1",
                  "validations": [
                ],
                  "customValidations": [

                 ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.list.groups.Behaviour.eventHandlers.Display,
                        "context": "",
                        "arguments": {
                            "cntrls": [
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "SelectControl",
                                 rtrnPropNm: "SelectControl"
                             }
                          ]
                        }
                    }

                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": true,
                      "hidden": false,
                      "defltValue": "1"
                  },
                  "options": [
                    {
                        "text": "Always",
                        "value": "1"
                    },
                    {
                        "text": "Conditional",
                        "value": "0"
                    }
                 ]
              },
              {
                  "text": "Action Button",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "ActionButton",
                  "defltValue": "0",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.list.groups.Behaviour.eventHandlers.ActionButton,
                        "context": "",
                        "arguments": {
                            "cntrls": [
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton1",
                                 rtrnPropNm: "ActionButton1"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton2",
                                 rtrnPropNm: "ActionButton2"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "ActionButton3",
                                 rtrnPropNm: "ActionButton3"
                             }
                        ]
                        }
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false,
                      "defltValue": 0
                  },
                  "options": [
                    {
                        "text": "No",
                        "value": "0"
                    },
                    {
                        "text": "Yes",
                        "value": "1"
                    }
                 ]
              },
              {
                  "text": "Select Control",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "SelectControl",
                  "disabled": false,
                  "noOfLines": "0",
                  "validations": [

                 ],
                  "customValidations": [
                    {
                        "func": "functionName",
                        "context": "context"
                    }
                 ],
                  "events": [
                    {
                        "name": "click",
                        "func": PROP_JSON_HTML_MAP.list.groups.Behaviour.eventHandlers.ConditionalDisplay,
                        "context": "",
                        "arguments": {}
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "Select Control Name",
                      "hidden": true
                  }
              },
              {
                  "text": "Action Button-1 ",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "ActionButton1",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

                 ],
                  "customValidations": [
                 ],
                  "events": [
                    {
                        "name": "click",
                        "func": PROP_JSON_HTML_MAP.list.groups.Behaviour.eventHandlers.ActionButton1,
                        "context": "",
                        "arguments": {}
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "Select Details",
                      "hidden": true
                  }
              },
              {
                  "text": "Action Button-2 ",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "ActionButton2",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

                 ],
                  "customValidations": [
                 ],
                  "events": [
                    {
                        "name": "click",
                        "func": PROP_JSON_HTML_MAP.list.groups.Behaviour.eventHandlers.ActionButton2,
                        "context": "",
                        "arguments": {}
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "Select Details",
                      "hidden": true
                  }
              },
              {
                  "text": "Action Button-3 ",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "ActionButton3",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

                 ],
                  "customValidations": [
                 ],
                  "events": [
                    {
                        "name": "click",
                        "func": PROP_JSON_HTML_MAP.list.groups.Behaviour.eventHandlers.ActionButton3,
                        "context": "",
                        "arguments": {}
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "Select Details",
                      "hidden": true
                  }
              },
              {
                  "text": "On Row Touch",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "DetectRowTouch",
                  "defltValue": "0",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.list.groups.Behaviour.eventHandlers.DetectRowTouch,
                        "context": "",
                        "arguments": {
                            "cntrls": [
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "SelectedRow",
                                 rtrnPropNm: "SelectedRow"
                             },
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "TitleColor",
                                 rtrnPropNm: "TitleColor"
                             },
                             {
                                 grpPrefText: "Data",
                                 cntrlPrefText: "SelectedRowDatabind",
                                 rtrnPropNm: "SelectedRowDatabind"
                             }
                          ]
                        }
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false
                  },
                  "options": [
                    {
                        "text": "No Action",
                        "value": "0"
                    },
                    {
                        "text": "Transition",
                        "value": "1"
                    },
                    {
                        "text": "Single Row Selection",
                        "value": "2"
                    },
                    {
                        "text": "Multiple Row Selection",
                        "value": "3"
                    }
                 ]
              },
              {
                  "text": "Selected Row",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "SelectedRow",
                  "defltValue": "0",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.list.groups.Behaviour.eventHandlers.SelectedRow,
                        "context": "",
                        "arguments": {
                            "cntrls": [
                             {
                                 grpPrefText: "Behaviour",
                                 cntrlPrefText: "TitleColor",
                                 rtrnPropNm: "TitleColor"
                             }
                          ]
                        }
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": true
                  },
                  "options": [
                    {
                        "text": "Right Align Tick",
                        "value": "0"
                    },
                    {
                        "text": "Change Title Color",
                        "value": "1"
                    }
                 ]
              },
              {
                  "text": "Title Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "TitleColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [
                ],
                  "customValidations": [
               ],
                  "events": [
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
              {
                  "text": "On Back Navigation",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "BackNavRefresh",
                  "defltValue": "0",
                  "validations": [
                    ],
                  "customValidations": [
                    ],
                  "events": [
                    {
                        "name": "change",
                        "func": PROP_JSON_HTML_MAP.list.groups.Behaviour.eventHandlers.BackNavRefresh,
                        "context": "",
                        "arguments": {
                            "cntrls": [
                          ]
                        }
                    }
                 ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false
                  },
                  "options": [
                    {
                        "text": "No Action",
                        "value": "0"
                    },
                    {
                        "text": "Refresh List",
                        "value": "1"
                    }
                 ]
              }
           ]
        }
    ]
};

function List(options) {
    if (!options) options = {};
    //    this.headerText = options.headerText;
    //    this.footerText = options.footerText;
    this.id = options.id;
    this.userDefinedName = options.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.List;
    this.description = options.description;
    this.type = options.type;
    this.listType = options.listType;
    this.dataInset = options.dataInset;
    this.dataGrouping = options.dataGrouping;
    this.countBubble = options.countBubble;
    this.emptyListMsg = options.emptyListMsg;
    this.actionButton = options.actionButton;
    this.actionBtn1Settings = options.actionBtn1Settings/*ActionButtonSetting Object*/;
    this.actionBtn2Settings = options.actionBtn2Settings/*ActionButtonSetting Object*/;
    this.actionBtn3Settings = options.actionBtn3Settings/*ActionButtonSetting Object*/;
    this.rowClickable = options.rowClickable;
    this.enablePaging = options.enablePaging;
    this.rowId = options.rowId;
    this.title = options.title;
    this.subtitle = options.subtitle;
    this.information = options.information;
    this.additionalInfo = options.additionalInfo;
    this.imageName = options.imageName;
    this.countField = options.countField;
    this.databindObjs = options.databindObjs; //array of Databinding Objs;
    this.noOfRecords = options.noOfRecords;
    this.isDeleted = options.isDeleted;
    this.oldName = options.oldName;
    this.isNew = options.isNew;
    this.selectedRowStyle = options.selectedRowStyle;
    this.titleColor = options.titleColor;
    this.selectedRow = options.selectedRow;
    this.backNavRefresh = options.backNavRefresh;
    this.manualData = options.manualData;

    this.allowFilter = options.allowFilter;
    this.placeholder = options.placeholder;
    this.scriptBtnCntrl = options.scriptBtnCntrl;
    this.scriptListRowTouch = options.scriptListRowTouch;
    this.caption = options.caption;
    this.date = options.date;
    this.dateFormat = options.dateFormat;
}

List.prototype = new ControlNew();
List.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.list;
List.prototype.fnSetDefaults = function () {
    this.listType = "1",
    this.dataInset = "0",
    this.countBubble = "0",
    this.actionButton = "0",
    this.rowClickable = "0",
    this.enablePaging = "0",
    this.dataGrouping = "0",
    this.selectedRowStyle = "0",
    this.titleColor = "",
    this.selectedRow = "",
    this.backNavRefresh = "",
    this.caption = "0",
    this.fnSetAllowFilter(MF_IDE_CONSTANTS.defltVal.no);
    this.fnSetPlaceholder(MF_IDE_CONSTANTS.defltVal.searchPlaceHolder);
};
List.prototype.fnGetDataGrouping = function () {
    return this.dataGrouping;
};
List.prototype.fnSetDataGrouping = function (value) {
    this.dataGrouping = value;
};
List.prototype.fnRemoveDatabindObjs = function () {
    this.databindObjs = [];
};
List.prototype.fnAddDatabindObj = function (databindObj) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
};
List.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        return databindObj.name !== name;
    });
    this.databindObjs = aryNewDatabindObjs;
};
List.prototype.fnGetSelectedRowStyle = function () {
    return this.selectedRowStyle;
};
List.prototype.fnGetTitleColor = function () {
    return this.titleColor;
};
List.prototype.fnGetListTypeValue = function () {
    return this.listType;
};
List.prototype.fnGetSelectedRowDatabind = function () {
    return this.selectedRow;
};
List.prototype.fnGetBackNavRefresh = function () {
    return this.backNavRefresh;
};
List.prototype.fnSetSelectedRowStyle = function (val) {
    this.selectedRowStyle = val;
};
List.prototype.fnSetTitleColor = function (val) {
    this.titleColor = val;
};
List.prototype.fnSetSelectedRowDatabind = function (val) {
    this.selectedRow = val;
};
List.prototype.fnSetBackNavRefresh = function (val) {
    this.backNavRefresh = val;
};
List.prototype.fnDeleteSelectedRowStyle = function () {
    this.fnSetSelectedRowStyle("");
};
List.prototype.fnDeleteTitleColor = function () {
    this.fnSetTitleColor("");
};
List.prototype.fnDeleteSelectedRowDatabind = function () {
    this.fnSetSelectedRowDatabind("");
};
List.prototype.fnDeleteBackNavRefresh = function () {
    this.fnSetBackNavRefresh("");
};
//shalini Reset Function
List.prototype.fnResetObjectDetails = function () {

    if (mfUtil.isNullOrUndefined(this.fnGetAllowFilter())) {
        this.fnSetAllowFilter(MF_IDE_CONSTANTS.defltVal.no);
    }
    if (mfUtil.isNullOrUndefined(this.fnGetPlaceholder())) {
        this.fnSetPlaceholder(MF_IDE_CONSTANTS.defltVal.searchPlaceHolder);
    }
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }
    var objActionBtnSettings = this.actionBtn1Settings;
    this.actionBtn1Settings = null;
    if (objActionBtnSettings) {
        this.actionBtn1Settings = new ActionButtonSetting(objActionBtnSettings);
    }
    objActionBtnSettings = this.actionBtn2Settings;
    this.actionBtn2Settings = null;
    if (objActionBtnSettings) {
        this.actionBtn2Settings = new ActionButtonSetting(objActionBtnSettings);
    }

    objActionBtnSettings = this.actionBtn3Settings;
    this.actionBtn3Settings = null;
    if (objActionBtnSettings) {
        this.actionBtn3Settings = new ActionButtonSetting(objActionBtnSettings);
    }
};
List.prototype.fnSetDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
};
List.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
List.prototype.fnGetBindTypeOfDatabindObj = function () {
    var objDatabindingObject = this.fnGetDatabindingObj();
    var objBindType = MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
    if (objDatabindingObject) {
        objBindType = objDatabindingObject.fnGetBindTypeObject();
    }
    return objBindType;
};
List.prototype.fnGetBindTypeOfDatabindObjValue = function () {
    var objDatabindingObject = this.fnGetDatabindingObj();
    var objBindType = null;
    if (this.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.CustomList) {
        objBindType = MF_IDE_CONSTANTS.ideCommandObjectTypes.db.id;
    }
    else {
        objBindType = MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id;
    }

    if (objDatabindingObject) {
        objBindType = objDatabindingObject.fnGetBindTypeObject().id;
    }
    return objBindType;
};
List.prototype.fnDeleteActionBtn1Settings = function () {
    this.actionBtn1Settings = null;
    var objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
    if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
    objForm.fnDeleteActionBtnSettingByType(MF_IDE_CONSTANTS.listActionBtnType.actionBtn1);
};
List.prototype.fnSetActionBtn1Settings = function (settings /*ActionButtonSetting*/) {
    if (settings && settings instanceof ActionButtonSetting) {
        this.actionBtn1Settings = settings;
        var objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
        if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
        objForm.fnSetActionBtnSettingsByType(settings, MF_IDE_CONSTANTS.listActionBtnType.actionBtn1);
    }
};

List.prototype.fnDeleteActionBtn2Settings = function () {
    this.actionBtn2Settings = null;
    var objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
    if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
    objForm.fnDeleteActionBtnSettingByType(MF_IDE_CONSTANTS.listActionBtnType.actionBtn2);
};
List.prototype.fnSetActionBtn2Settings = function (settings /*ActionButtonSetting*/) {
    if (settings && settings instanceof ActionButtonSetting) {
        this.actionBtn2Settings = settings;
        var objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
        if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
        objForm.fnSetActionBtnSettingsByType(settings, MF_IDE_CONSTANTS.listActionBtnType.actionBtn2);
    }
};
List.prototype.fnDeleteActionBtn3Settings = function () {
    this.actionBtn3Settings = null;
    var objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
    if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
    objForm.fnDeleteActionBtnSettingByType(MF_IDE_CONSTANTS.listActionBtnType.actionBtn3);
};
List.prototype.fnSetActionBtn3Settings = function (settings /*ActionButtonsettings*/) {
    if (settings && settings instanceof ActionButtonSetting) {
        this.actionBtn3Settings = settings;
        var objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
        if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
        objForm.fnSetActionBtnSettingsByType(settings, MF_IDE_CONSTANTS.listActionBtnType.actionBtn3);
    }
};
List.prototype.fnGetActionBtnSettingsByType = function (buttonType/*Contants.listActionBtnType*/) {
    if (!buttonType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
    var settings = null;
    switch (buttonType) {
        case MF_IDE_CONSTANTS.listActionBtnType.actionBtn1:
            settings = this.actionBtn1Settings;
            break;
        case MF_IDE_CONSTANTS.listActionBtnType.actionBtn2:
            settings = this.actionBtn2Settings;
            break;
        case MF_IDE_CONSTANTS.listActionBtnType.actionBtn3:
            settings = this.actionBtn3Settings;
            break;
    }
    return settings;
};
List.prototype.fnGetListTypeByValue = function () {
    var listType = "";
    if (this.listType) {
        switch (this.listType) {
            case "0":
                listType = MF_IDE_CONSTANTS.listCntrlType.Simple;
                break;
            case "1":
                listType = MF_IDE_CONSTANTS.listCntrlType.Basic;
                break;
            case "2":
                listType = MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription;
                break;
            case "3":
                listType = MF_IDE_CONSTANTS.listCntrlType.Numbered;
                break;
            case "4":
                listType = MF_IDE_CONSTANTS.listCntrlType.Icon;
                break;
            case "5":
                listType = MF_IDE_CONSTANTS.listCntrlType.Thumbnail;
                break;
            case "6":
                listType = MF_IDE_CONSTANTS.listCntrlType.ImageList;
                break;
            case "7":
                listType = MF_IDE_CONSTANTS.listCntrlType.EventList;
                break;
            case "8":
                listType = MF_IDE_CONSTANTS.listCntrlType.CustomList;
                break;
        }
    }
    return listType;
};
List.prototype.fnRemoveAllDataBindingProperties = function () {
    this.rowId = "";
    this.title = "";
    this.subtitle = "";
    this.information = "";
    this.additionalInfo = "";
    this.imageName = "";
    this.countField = "";
    this.date = "";
    this.fnDeleteSelectedRowDatabind();
};
List.prototype.fnSetNoOfRecords = function (noOfRecords) {
    this.noOfRecords = noOfRecords;
};
List.prototype.fnGetNoOfRecords = function () {
    return this.noOfRecords;
};
List.prototype.fnIsCountBubbleEnabled = function () {
    if (this.countBubble === "1") {
        return true;
    }
    else {
        return false;
    }
};
List.prototype.fnGetRowClickable = function () {
    return this.rowClickable;
};
List.prototype.fnSetRowClickable = function (value) {
    this.rowClickable = value;
};
List.prototype.fnIsRowClickableARowSelectionType = function () {
    var rowClickable = this.fnGetRowClickable();
    if (rowClickable && (rowClickable === "2" || rowClickable === "3")) {
        return true;
    }
    else {
        return false;
    }
};
// List.prototype.fnSetNoOfRecordsDefault = function (noOfRecords) {
// this.fnSetNoOfRecords("10") ;
// };
List.prototype.fnSetEnablePaging = function (enablePaging/*int value 1 or 0*/) {
    this.enablePaging = enablePaging;
};
List.prototype.fnGetEnablePaging = function () {
    return this.enablePaging;
};
List.prototype.fnRename = function (name) {
    if (!this.fnIsNewlyAdded() &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
List.prototype.fnIsNewlyAdded = function () {
    return this.isNew;
};
List.prototype.fnGetActionButton = function () {
    return this.actionButton;
};
List.prototype.fnSetActionButton = function (value) {
    this.actionButton = value;
};
List.prototype.fnAddManualDataOption = function (manualDataOption/*ManualDataOption Object*/) {
    if (!this.manualData || !$.isArray(this.manualData)) {
        this.manualData = [];
    }

    if (manualDataOption && manualDataOption instanceof ManualDataOption) {
        this.manualData.push(manualDataOption);
    }
}
List.prototype.fnAddManualDataOptions = function (manualDataOptions/*ManualDataOptions Object[]*/) {
    if (!this.manualData || !$.isArray(this.manualData)) {
        this.manualData = [];
    }
    if ($.isArray(manualDataOptions)) {
        var self = this;
        $.each(manualDataOptions, function (index, value) {
            if (value && value instanceof ManualDataOption) {
                self.manualData.push(value);
            }
        });
    }
}
List.prototype.fnDeleteManualDataOption = function (rowId) {
    var aryNewManualDataOptions = $.grep(this.manualData, function (manualDataOption) {
        if (manualDataOption instanceof ManualDataOption) {
            return manualDataOption.rowId !== rowId;
        }
    });
    this.manualData = aryNewManualDataOptions;
}
List.prototype.fnDeleteAllManualDataOptions = function () {
    this.manualData = [];
}
List.prototype.fnGetAllowFilter = function () {
    return this.allowFilter;
};
List.prototype.fnSetAllowFilter = function (value) {
    this.allowFilter = value;
};
List.prototype.fnGetPlaceholder = function () {
    return this.placeholder;
};
List.prototype.fnSetPlaceholder = function (value) {
    this.placeholder = value;
};
List.prototype.fnGetCaption = function () {
    return this.caption;
};
List.prototype.fnSetCaption = function (value) {
    this.caption = value;
};
List.prototype.fnGetCountBubble = function () {
    return this.countBubble;
};
List.prototype.fnSetCountBubble = function (value) {
    this.countBubble = value;
};
List.prototype.fnSetDefaultsByListType = function () {
    var listType = this.fnGetListTypeByValue();
    switch (listType) {
        case MF_IDE_CONSTANTS.listCntrlType.Basic:
        case MF_IDE_CONSTANTS.listCntrlType.Numbered:
        case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
        case MF_IDE_CONSTANTS.listCntrlType.Icon:
        case MF_IDE_CONSTANTS.listCntrlType.Simple:
        case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
        case MF_IDE_CONSTANTS.listCntrlType.ImageList:
        case MF_IDE_CONSTANTS.listCntrlType.EventList:
            break;
        case MF_IDE_CONSTANTS.listCntrlType.CustomList:
            this.fnSetCaption("0");
            this.fnSetDataGrouping("0");
            this.fnSetCountBubble("0");
            this.fnSetAllowFilter("0");
            this.fnSetPlaceholder(MF_IDE_CONSTANTS.defltVal.searchPlaceHolder);
            this.fnRemoveAllDataBindingProperties();
            this.fnSetEnablePaging('0');
            this.fnSetNoOfRecords('10');
            this.fnSetActionButton('0');
            this.fnDeleteActionBtn1Settings();
            this.fnDeleteActionBtn2Settings();
            this.fnDeleteActionBtn3Settings();
            break;
    }
};
List.prototype.fnGetScriptRowTouch = function () {
    return this.scriptListRowTouch;
};
List.prototype.fnSetScriptRowTouch = function (value) {
    this.scriptListRowTouch = value;
};
List.prototype.fnGetScriptBtnCntrl = function () {
    return this.scriptBtnCntrl;
};
List.prototype.fnSetScriptBtnCntrl = function (value) {
    this.scriptBtnCntrl = value;
};
List.prototype.fnDeleteScriptBtnCntrl = function () {
    this.fnSetScriptBtnCntrl("");
};
function ManualDataOption(opts) {
    if (!opts) opts = {};
    this.rowId = opts.rowId;
    this.title = opts.title;
    this.description = opts.description;
}

function ActionButtonSetting(options/*object*/) {
    if (!options) options = {};
    this.text = options.text;
    this.refreshList = options.refreshList;
    this.color = options.color;
    this.actionType = options.actionType; /*CONSTANTS.ListActionBtnActionType*/
}
ActionButtonSetting.prototype.fnGetText = function () {
    return this.text;
};
ActionButtonSetting.prototype.fnSetText = function (value) {
    this.text = value;
};
mFicientIde.ListControlHelper = (function () {
    var jqueryKeyForActionBtnSettings = "actionBtnSettings";
    function _getActionBtnSettingsContDiv() {
        return $('#divListCntrlActionBtnSettings');
    }
    function _validateActionBtnSave() {
        var aryErrors = [];
        var $settingsContDiv = _getActionBtnSettingsContDiv();
        var $txtRptButtonText = $('#txtRptButtonText', $settingsContDiv);
        if ($txtRptButtonText.val() === "") {
            aryErrors.push("Please enter text for button.");
        }
        return aryErrors;
    }
    function _saveActionBtnSettingsByType() {
        var $settingsContDiv = _getActionBtnSettingsContDiv();
        var divData = $settingsContDiv.data(jqueryKeyForActionBtnSettings);
        if (!divData) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
        var errors = _validateActionBtnSave();
        if (errors.length > 0) {
            throw new MfError(errors);
        }
        var cntrl = MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
        if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
        var objSettings = new ActionButtonSetting();
        var $settingsText = $('#txtRptButtonText');
        var $ddlRefreshList = $('#ddlRptBtnRefresh');
        var $ddlColorText = $('#ddlRptBtnColor');
        var $ddlActionType = $('#ddlListCntrlActionType');
        objSettings.text = $settingsText.val();
        objSettings.refreshList = $ddlRefreshList.val();
        objSettings.color = $ddlColorText.val();
        objSettings.actionType = $ddlActionType.val();
        switch (divData.buttonType) {
            case MF_IDE_CONSTANTS.listActionBtnType.actionBtn1:
                cntrl.fnSetActionBtn1Settings(objSettings);
                break;
            case MF_IDE_CONSTANTS.listActionBtnType.actionBtn2:
                cntrl.fnSetActionBtn2Settings(objSettings);
                break;
            case MF_IDE_CONSTANTS.listActionBtnType.actionBtn3:
                cntrl.fnSetActionBtn3Settings(objSettings);
                break;
        }
    }
    function _setDivDataOfActionBtnSettings(buttonType/*CONSTANTS.listActionBtnType*/) {
        if (!buttonType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
        var $containerDiv = _getActionBtnSettingsContDiv();
        $containerDiv.data(jqueryKeyForActionBtnSettings,
            { buttonType: buttonType });
    }
    function _removeDivDataOfActionBtnSettings() {
        var $containerDiv = _getActionBtnSettingsContDiv();
        $containerDiv.removeData(jqueryKeyForActionBtnSettings);
    }
    function _fillSettingsDataByType(cntrl, buttonType) {
        if (!cntrl || !buttonType)
            throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;

        var objSettingsDtls = cntrl.fnGetActionBtnSettingsByType(buttonType);
        if (objSettingsDtls) {
            var $settingsText = $('#txtRptButtonText');
            var $ddlRefreshList = $('#ddlRptBtnRefresh');
            var $ddlColorText = $('#ddlRptBtnColor');
            var $ddlActionType = $('#ddlListCntrlActionType');
            $settingsText.val(objSettingsDtls.text);
            $ddlRefreshList.val(objSettingsDtls.refreshList);
            $ddlColorText.val(objSettingsDtls.color);
            $ddlActionType.val(objSettingsDtls.actionType);
        }
    }
    function _clearSettingsControls() {
        $('#txtRptButtonText').val('');
        $('#ddlRptBtnRefresh').val('0');
        $('#ddlRptBtnColor').val('0');
        $('#ddlListCntrlActionType').val('1');
    }
    function _fillActionBtnSettingsInHtml() {
        var objCntrlHtmlDtls = null;
        var $settingsContDiv = _getActionBtnSettingsContDiv();
        var divData = $settingsContDiv.data(jqueryKeyForActionBtnSettings);
        if (!divData) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
        switch (divData.buttonType) {
            case MF_IDE_CONSTANTS.listActionBtnType.actionBtn1:
                objCntrlHtmlDtls = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                    .Behaviour.getActionButton1();
                break;
            case MF_IDE_CONSTANTS.listActionBtnType.actionBtn2:
                objCntrlHtmlDtls = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                    .Behaviour.getActionButton2();
                break;
            case MF_IDE_CONSTANTS.listActionBtnType.actionBtn3:
                objCntrlHtmlDtls = PROP_JSON_HTML_MAP.list.propSheetCntrl
                                    .Behaviour.getActionButton3();
                break;
        }
        objCntrlHtmlDtls.control.val($('#txtRptButtonText').val());
    }
    function _fillDatabindingSettingsInHtml(cntrl) {
        if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
        var listType = cntrl.fnGetListTypeByValue();
        var objDatabinding = cntrl.fnGetDatabindingObj();
        var objRowIDCntrlDet = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getRowId();
        var objImageNameCntrlDet = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getImageName();
        var objTitleCntrlDet = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getTitle();
        var objSubtitleCntrlDet = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getSubtitle();
        var objInfoCntrlDet = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getInformation();
        var objAddInfoCntrlDet = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getAdditionalInformation();
        var objCountBubbleCntrlDet = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getCountBubble();
        var objDataObjectCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getDataObject();
        var objSelectedRowDatabindCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getSelectedRowDatabind();
        var objDateDatabindCntrl = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getDataDateDatabindCntrl();
        objDataObjectCntrl.control.val(
                objDatabinding.name ? objDatabinding.name : "Select Object");
        objRowIDCntrlDet.control.val(cntrl.rowId ?
                            cntrl.rowId : "None");
        objTitleCntrlDet.control.val(cntrl.title ?
                cntrl.title : "None");
        if (listType) {
            switch (listType) {
                case MF_IDE_CONSTANTS.listCntrlType.Basic:
                case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                    objSubtitleCntrlDet.control.val(cntrl.subtitle ?
                         cntrl.subtitle : "None");
                    objInfoCntrlDet.control.val(cntrl.information ?
                     cntrl.information : "None");
                    objAddInfoCntrlDet.control.val(cntrl.additionalInfo ?
                    cntrl.additionalInfo : "None");
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.Icon:
                case MF_IDE_CONSTANTS.listCntrlType.Simple:
                    objImageNameCntrlDet.control.val(cntrl.imageName ?
                     cntrl.imageName : "None");
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                    objSubtitleCntrlDet.control.val(cntrl.subtitle ?
                         cntrl.subtitle : "None");
                    objInfoCntrlDet.control.val(cntrl.information ?
                     cntrl.information : "None");
                    objAddInfoCntrlDet.control.val(cntrl.additionalInfo ?
                    cntrl.additionalInfo : "None");
                    objImageNameCntrlDet.control.val(cntrl.imageName ?
                     cntrl.imageName : "None");
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.EventList:
                    objSubtitleCntrlDet.control.val(cntrl.subtitle ?
                         cntrl.subtitle : "None");
                    objInfoCntrlDet.control.val(cntrl.information ?
                     cntrl.information : "None");
                    objAddInfoCntrlDet.control.val(cntrl.additionalInfo ?
                    cntrl.additionalInfo : "None");
                    objDateDatabindCntrl.control.val(cntrl.date ?
                     cntrl.date : "None");
                    break;
            }
        }
        if (cntrl.fnIsCountBubbleEnabled()) {
            objCountBubbleCntrlDet.control.val(cntrl.countField ?
                    cntrl.countField : "None");
        }
        if (cntrl.fnIsRowClickableARowSelectionType()) {
            var selectedRowDatabind = cntrl.fnGetSelectedRowDatabind();
            objSelectedRowDatabindCntrl.control.val(
                    selectedRowDatabind ?
                    selectedRowDatabind : "None");
        }
    }

    /**
    * Show hide html controls ,the controls (properties of list) that are databound
    * For example when databinding type is Database and list type is Basic
    * then we have two four databound properties subtitle information additionalInfo imageName
    * These properties are shown in the property sheet also
    * These changes By list type.This function shows hide that html based on list type
    * @method _showHideHtmlOfDataboundPropsInPropSheet
    * @param {Object} Object of controls 
    * @param {List} list
    * @return {Void} undefined
    */
    function _procShowHideHtmlInPropSheetByListProperties(list) {

        var listType = list.fnGetListTypeByValue(),
            objDatabindingObj = list.fnGetDatabindingObj(),
            objbindType = null;

        if (objDatabindingObj) {
            objbindType = objDatabindingObj.fnGetBindTypeObject();
        }

        var _allDataCntrls = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getAllControls(),
            _allBehaviourCntrls = PROP_JSON_HTML_MAP.list.propSheetCntrl.Behaviour.getAllControls(),
            _allAppearanceCntrls = PROP_JSON_HTML_MAP.list.propSheetCntrl.Appearance.getAllControls();

        _allAppearanceCntrls.Caption.wrapperDiv.hide();
        _allAppearanceCntrls.AppearanceCountBubble.wrapperDiv.hide();
        _allAppearanceCntrls.DataGrouping.wrapperDiv.hide();


        _allDataCntrls.Subtitle.wrapperDiv.hide();
        _allDataCntrls.Information.wrapperDiv.hide();
        _allDataCntrls.AdditionalInformation.wrapperDiv.hide();
        _allDataCntrls.ImageName.wrapperDiv.hide();
        _allDataCntrls.RowID.wrapperDiv.hide();
        _allDataCntrls.Title.wrapperDiv.hide();
        _allDataCntrls.DateDatabind.wrapperDiv.hide();

        _allBehaviourCntrls.ActionButton.wrapperDiv.hide();
        _allBehaviourCntrls.ActionButton1.wrapperDiv.hide();
        _allBehaviourCntrls.ActionButton2.wrapperDiv.hide();
        _allBehaviourCntrls.ActionButton3.wrapperDiv.hide();


        switch (listType) {
            case MF_IDE_CONSTANTS.listCntrlType.Basic:
            case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                setDisplay(list, _allDataCntrls.RowID);
                setDisplay(list, _allDataCntrls.Title);
                _allAppearanceCntrls.AllowFilter.wrapperDiv.show();
                break;
            case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                setDisplay(list, _allDataCntrls.RowID);
                setDisplay(list, _allDataCntrls.Title);
                setDisplay(list, _allDataCntrls.Subtitle);
                setDisplay(list, _allDataCntrls.Information);
                setDisplay(list, _allDataCntrls.AdditionalInformation);
                _allAppearanceCntrls.AllowFilter.wrapperDiv.show();
                break;
            case MF_IDE_CONSTANTS.listCntrlType.Icon:
                setDisplay(list, _allDataCntrls.RowID);
                setDisplay(list, _allDataCntrls.Title);
                setDisplay(list, _allDataCntrls.ImageName);
                _allAppearanceCntrls.AllowFilter.wrapperDiv.show();
                break;
            case MF_IDE_CONSTANTS.listCntrlType.Simple:
                setDisplay(list, _allDataCntrls.RowID);
                setDisplay(list, _allDataCntrls.Title);
                setDisplay(list, _allDataCntrls.ImageName);
                _allAppearanceCntrls.AllowFilter.wrapperDiv.hide();
                break;
            case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                setDisplay(list, _allDataCntrls.RowID);
                setDisplay(list, _allDataCntrls.Title);
                setDisplay(list, _allDataCntrls.Subtitle);
                setDisplay(list, _allDataCntrls.Information);
                setDisplay(list, _allDataCntrls.AdditionalInformation);
                setDisplay(list, _allDataCntrls.ImageName);
                _allAppearanceCntrls.AllowFilter.wrapperDiv.show();
                break;
            case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                setDisplay(list, _allDataCntrls.RowID);
                setDisplay(list, _allDataCntrls.Title);
                setDisplay(list, _allDataCntrls.Subtitle);
                setDisplay(list, _allDataCntrls.Information);
                setDisplay(list, _allDataCntrls.AdditionalInformation);
                setDisplay(list, _allDataCntrls.ImageName);

                _allAppearanceCntrls.Caption.wrapperDivCntrl.show();
                _allAppearanceCntrls.AllowFilter.wrapperDiv.hide();
                setDisplay(list, _allAppearanceCntrls.AppearanceCountBubble);
                setDisplay(list, _allBehaviourCntrls.ActionButton);
                setDisplay(list, _allBehaviourCntrls.ActionButton1);
                setDisplay(list, _allBehaviourCntrls.ActionButton2);
                setDisplay(list, _allBehaviourCntrls.ActionButton3);
                break;
            case MF_IDE_CONSTANTS.listCntrlType.EventList:
                setDisplay(list, _allDataCntrls.RowID);
                setDisplay(list, _allDataCntrls.Title);
                setDisplay(list, _allDataCntrls.Subtitle);
                setDisplay(list, _allDataCntrls.Information);
                setDisplay(list, _allDataCntrls.AdditionalInformation);
                setDisplay(list, _allDataCntrls.DateDatabind);
                _allBehaviourCntrls.ActionButton.wrapperDiv.show();
                _allBehaviourCntrls.ActionButton1.wrapperDiv.show();
                _allBehaviourCntrls.ActionButton2.wrapperDiv.show();
                _allBehaviourCntrls.ActionButton3.wrapperDiv.show();
                _allAppearanceCntrls.AllowFilter.wrapperDiv.show();
                break;
            case MF_IDE_CONSTANTS.listCntrlType.CustomList:
                _allAppearanceCntrls.AllowFilter.wrapperDiv.hide();
                _allAppearanceCntrls.Placeholder.wrapperDiv.hide();
                _allAppearanceCntrls.EmptyListMessage.wrapperDiv.show();
                _allDataCntrls.ManualData.wrapperDiv.hide();
                _allDataCntrls.DataObject.wrapperDiv.show();
                _allDataCntrls.CountBubble.wrapperDiv.hide();
                _allDataCntrls.EnablePaging.wrapperDiv.hide();
                _allDataCntrls.NoOfRecords.wrapperDiv.hide();
                break;
        }
    }
    function _setDefaultsInHtmlOfDataboundPropsInPropSheet(listType) {
        var _allDataCntrls = PROP_JSON_HTML_MAP.list.propSheetCntrl.Data.getAllControls(),
            _allBehaviourCntrls = PROP_JSON_HTML_MAP.list.propSheetCntrl.Behaviour.getAllControls(),
            _allAppearanceCntrls = PROP_JSON_HTML_MAP.list.propSheetCntrl.Appearance.getAllControls();
        switch (listType) {
            case MF_IDE_CONSTANTS.listCntrlType.Basic:
            case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                break;
            case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                break;
            case MF_IDE_CONSTANTS.listCntrlType.Icon:
            case MF_IDE_CONSTANTS.listCntrlType.Simple:
                break;
            case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
            case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                break;
            case MF_IDE_CONSTANTS.listCntrlType.EventList:
                break;
            case MF_IDE_CONSTANTS.listCntrlType.CustomList:

                _allAppearanceCntrls.Caption.control.val('0');
                _allAppearanceCntrls.DataGrouping.control.val('0');
                _allAppearanceCntrls.AppearanceCountBubble.control.val('0');
                _allAppearanceCntrls.AllowFilter.control.val('0');
                _allAppearanceCntrls.Placeholder.control.val(MF_IDE_CONSTANTS.defltVal.searchPlaceHolder);

                _allDataCntrls.RowID.control.val('None');
                _allDataCntrls.Title.control.val('None');
                _allDataCntrls.Subtitle.control.val('None');
                _allDataCntrls.Information.control.val('None');
                _allDataCntrls.AdditionalInformation.control.val('None');
                _allDataCntrls.ImageName.control.val('None');
                _allDataCntrls.CountBubble.control.val('None');
                _allDataCntrls.DateDatabind.control.val('None');
                _allDataCntrls.EnablePaging.control.val('0');
                _allDataCntrls.NoOfRecords.control.val('10');


                _allBehaviourCntrls.ActionButton.control.val('0');
                _allBehaviourCntrls.ActionButton1.control.val('Select Details');
                _allBehaviourCntrls.ActionButton2.control.val('Select Details');
                _allBehaviourCntrls.ActionButton3.control.val('Select Details');
                break;
        }
    }
    function _setOptionsForDetectRowTouchInPropSheet(listType, bindObjType) {
        var $ddlDetectRowTouch = PROP_JSON_HTML_MAP.list
                                .propSheetCntrl
                                .Behaviour
                                .getDetectRowTouch();

        switch (listType) {
            case MF_IDE_CONSTANTS.listCntrlType.Basic:
            case MF_IDE_CONSTANTS.listCntrlType.Numbered:
            case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
            case MF_IDE_CONSTANTS.listCntrlType.Icon:
            case MF_IDE_CONSTANTS.listCntrlType.Simple:
            case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
            case MF_IDE_CONSTANTS.listCntrlType.ImageList:
            case MF_IDE_CONSTANTS.listCntrlType.EventList:
                if (bindObjType === MF_IDE_CONSTANTS.ideCommandObjectTypes.None) {
                    $ddlDetectRowTouch.control.html(manualOptionsHtml);
                }
                else {
                    $ddlDetectRowTouch.control.html(dataBindingOptionsHtml);
                }
                break;
            case MF_IDE_CONSTANTS.listCntrlType.CustomList:
                $ddlDetectRowTouch.control.html(manualOptionsHtml);
                break;
        }
    }
    function _getOptionsForDatabindingDdl(listType, appType) {
        var strOptions = "";
        if (listType !== MF_IDE_CONSTANTS.listCntrlType.CustomList) {
            strOptions += '<option value="0">Manual</option>';
        }
        else {
        }
        if (appType === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
            strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id + '">Offline Database</option>';
        }
        else {
            strOptions += '<option value="1">Database Object</option>';
            strOptions += '<option value="2">Web Service Object</option>';
            strOptions += '<option value="5">OData Object</option>';
            strOptions += '<option value="6">Offline Database</option>';
        }
        return strOptions;
    }
    function _setOptionsForDatabindingInPropSheet(listType, appType) {
        var $ddlDatabinding = PROP_JSON_HTML_MAP.list
                                .propSheetCntrl
                                .Data
                                .getDatabinding();

        $ddlDatabinding.control.html(_getOptionsForDatabindingDdl(listType, appType));
    }
    return {
        processSaveActionBtnSettingsByType: function (evnt) {
            _saveActionBtnSettingsByType();
            _fillActionBtnSettingsInHtml();
            _removeDivDataOfActionBtnSettings();
            _clearSettingsControls();
            closeModalPopUp('divListCntrlActionBtnSettings');
        },
        setDataOfActionBtnSettingsContainerDiv: function (buttonType/*CONSTANTS.listActionBtnType*/) {
            _setDivDataOfActionBtnSettings(buttonType);
        },
        processShowActionBtnSettingsDivBytype: function (cntrl, buttonType) {
            if (!buttonType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            _clearSettingsControls();
            _fillSettingsDataByType(cntrl, buttonType);
            switch (buttonType) {
                case MF_IDE_CONSTANTS.listActionBtnType.actionBtn1:
                    showModalPopUp('divListCntrlActionBtnSettings',
                    'Action Button 1-Details',
                    450);
                    break;
                case MF_IDE_CONSTANTS.listActionBtnType.actionBtn2:
                    showModalPopUp('divListCntrlActionBtnSettings',
                    'Action Button 2-Details',
                    450);
                    break;
                case MF_IDE_CONSTANTS.listActionBtnType.actionBtn3:
                    showModalPopUp('divListCntrlActionBtnSettings',
                    'Action Button 3-Details',
                    450);
                    break;
            }

        },
        fillDatabindingSettingsInHtml: function (cntrl) {
            _fillDatabindingSettingsInHtml(cntrl);
        },
        getOptionsForDatabindingDdl: function (listType) {
            return _getOptionsForDatabindingDdl();
        },
        procShowHideHtmlInPropSheetByListProperties: function (list, jqoCntrls) {
            _procShowHideHtmlInPropSheetByListProperties(list, jqoCntrls);
        },
        setDefaultsInHtmlOfDataboundPropsInPropSheet: function (listType) {
            _setDefaultsInHtmlOfDataboundPropsInPropSheet(listType);
        },
        setOptionsForDetectRowTouchInPropSheet: function (listType, bindObjType) {
            _setOptionsForDetectRowTouchInPropSheet(listType, bindObjType);
        },
        setOptionsForDatabindingInPropSheet: function (listType, appType) {
            _setOptionsForDatabindingInPropSheet(listType);
        }
    };
})();
$(document).ready(function () {
    $('#btnListActionBtnSettingsSave').on('click', function (evnt) {
        try {
            mFicientIde.ListControlHelper.processSaveActionBtnSettingsByType(evnt);
        }
        catch (error) {
            if (error instanceof MfError) {
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
            } else {
                console.log(error);
            }
        }
    });

});

//-------------------------------------------------------Manual Data---------------------------------------------//

function moveDown(sender) {
    var row = $('#' + $(sender).attr('id')).closest("tr");
    var existRowId = row.find('td:first').text();
    var moveRowId = row.next().find('td:first').text();
    if (moveRowId != '') {
        row.find('td:first').text(moveRowId);
        row.next().find('td:first').text(existRowId);
        row.insertAfter(row.next());
        _showUpDownArrowInTable();
    }
}

function moveUp(sender) {
    var row = $('#' + $(sender).attr('id')).closest("tr");
    var existRowId = row.find('td:first').text();
    var moveRowId = row.prev().find('td:first').text();
    if (moveRowId != '') {
        row.find('td:first').text(moveRowId);
        row.prev().find('td:first').text(existRowId);
        row.insertBefore(row.prev());
        _showUpDownArrowInTable();
    }
}

function _addNewListRow(ListOptionCount, cntrl, ctr) {
    if (checkIfRowIdExist("ListContentText_" + ListOptionCount, "ListContent"))
        ListOptionCount = getHighestRowIdCount("ListContent");
    if (ctr != undefined) {
        var row = $('#' + ctr).closest("tr");
        row.after(generateHtml(ListOptionCount));
    }
    else {
        $('#ListContent').append(generateHtml(ListOptionCount));
    }

    $('#imgListDelete_' + ListOptionCount).bind('click', function () {
        if (parseInt($("tbody#ListContent").find("tr").length, 10) > 1) {
            $('#' + this.parentElement.parentElement.id).remove();
            ReconcileRowid();
        }
        else {
            showMessage('This option cannot be deleted. List Data should have at least 1 option.', DialogType.Error);
        }
    });
    $('#imgListAdd_' + ListOptionCount).bind('click', function () {
        var ctr = $(this).attr('id');
        _addNewListRow(parseInt($("tbody#ListContent").find("tr").length, 10) + 1, cntrl, ctr);
        ReconcileRowid();
    });
}

function generateHtml(ListOptionCount) {
    var strHtml = '<tr id="ListContentText_' + ListOptionCount + '">'
                        + '<td>' + ListOptionCount + '</td>'
                        + '<td><input id="ListTitle_' + ListOptionCount + '" type="text" value="" onkeydown=\"return enterKeyFilter(event);\"/></td>'
                        + '<td><input id="ListDesc_' + ListOptionCount + '" type="text" value="" onkeydown=\"return enterKeyFilter(event);\"/></td>'
                        + '<td><img id="imgmoveup_' + ListOptionCount + '" alt="moveup" src="//enterprise.mficient.com/images/arrowup.png" style="float:right" onclick="moveUp(this)" class="moveUp16x16"  title="Move Up"/><img id="imgmovedown_' + ListOptionCount + '" alt="imgmovedown" src="//enterprise.mficient.com/images/arrowdown.png" onclick="moveDown(this)" class="moveDown16x16" title="Move down"/></td>'
                        + '<td><img id="imgListAdd_' + ListOptionCount + '" alt="add" src="//enterprise.mficient.com/images/add.png" style="padding-top:4px !important"  title="Add Option"/> <img id="imgListDelete_' + ListOptionCount + '" alt="delete" src="//enterprise.mficient.com/images/cross.png" style="float:right; padding-top:4px !important"  title="Delete Option"/> </td></tr>';
    return strHtml;
}

function listOptionEdit(cntrl) {
    $('#ListContent').html('');
    var listOptionCount = 0;
    if (cntrl.manualData != undefined && cntrl.manualData.length > 0) {
        $.each(cntrl.manualData, function () {
            listOptionCount = listOptionCount + 1;
            _addNewListRow(listOptionCount, cntrl);
            $('#ListTitle_' + listOptionCount).val(this.title);
            $('#ListDesc_' + listOptionCount).val(htmlDecode(this.description));
        });
    }
    else {
        _addNewListRow(1, cntrl);
    }
}

function saveManualData(cntrl) {
    cntrl.manualData = [];
    var index = 1;
    var option = "";
    $('#ListContent').find("*").each(function () {
        switch (this.id.split('_')[0]) {
            case "ListContentText":
                var listDataOption = _getListOption(this.id)
                if (listDataOption != null || listDataOption != undefined) {
                    cntrl.fnAddManualDataOption(listDataOption);
                }
                break;
        }
    });

    SubProcBoxAddListData(false);
}

function _getListOption(_Id) {
    var listDataOption = new ManualDataOption();
    listDataOption.rowId = $('#' + _Id).find('td:first').text();
    $('#' + _Id).find("*").each(function () {
        switch (this.id.split('_')[0]) {
            case "ListTitle":
                listDataOption.title = this.value;
                break;
            case "ListDesc":
                listDataOption.description = this.value;
                break;
        }
    });
    if (listDataOption.title.length > 0 && listDataOption.description.length > 0) {
        return listDataOption;
    }
    else {
        return null;
    }
}

function ReconcileRowid() {
    var rowIndex = 1;
    $('#ListContent tr').each(function (index) {
        $(this).find('td:first').html(rowIndex);
        rowIndex++;
    });
    _showUpDownArrowInTable();
}

function _showUpDownArrowInTable() {
    var noOfRows = $("#ListContent").find("tr").length;
    var count = 0;
    if (noOfRows === 1) {
        $("#ListContent").find("tr").each(function () {
            $(this).find(".moveUp16x16,.moveDown16x16").hide();
        });
    }
    else if (noOfRows === 2) {
        $("#ListContent").find("tr").each(function () {
            if (count === 0) {
                $(this).find(".moveDown16x16").show();
                $(this).find(".moveUp16x16").hide();
            }
            else {
                $(this).find(".moveDown16x16").hide();
                $(this).find(".moveUp16x16").show();
            }
            count++;
        });
    }
    else {
        $("#ListContent").find("tr").each(function () {
            if (count === 0) {
                $(this).find(".moveDown16x16").show();
                $(this).find(".moveUp16x16").hide();
            }
            else if (count === noOfRows - 1) {
                $(this).find(".moveDown16x16").hide();
                $(this).find(".moveUp16x16").show();
            }
            else {
                $(this).find(".moveDown16x16").show();
                $(this).find(".moveUp16x16").show();
            }

            count++;
        });
    }
}

function SubProcBoxAddListData(_bol, _Title) {
    if (_bol) {
        showModalPopUpWithOutHeader('SubProcBoxAddListData', _Title, 600, false);
    }
    else {
        $('#SubProcBoxAddListData').dialog('close');
    }
    _showUpDownArrowInTable();
}

//-------------------------------------------------------Manual Data---------------------------------------------//
















//-----------------------------------------------------MOHAN--------------------------------------------------//
//-----------------------------------------------------Hidden Field ------------------------------------------//
function HiddenField(options) {
    if (!options) options = {};
    this.id = options.id;
    this.userDefinedName = options.userDefinedName;
    this.description = options.description;
    this.type = options.type;
    this.databindObjs = options.databindObjs; //array of Databinding Objs;
    this.displayText = options.displayText;
    this.oldName = options.oldName;
    this.isDeleted = options.isDeleted;
    this.isNew = options.isNew;
    this.bindToProperty = options.bindToProperty;
}
HiddenField.prototype = new ControlNew();
HiddenField.prototype.fnRemoveDatabindObjs = function () {
    this.databindObjs = [];
};
HiddenField.prototype.fnGetId = function () {
    return this.id;
};
HiddenField.prototype.fnGetName = function () {
    return this.userDefinedName;
};
HiddenField.prototype.fnAddDatabindObj = function (databindObj) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
};
HiddenField.prototype.fnDeleteDatabindObj = function (name) {
    var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) {
        return databindObj.name !== name;
    });
    this.databindObjs = aryNewDatabindObjs;
};
HiddenField.prototype.fnSetDatabindObjs = function (databindObjs/*DatabindingObj object array*/) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if ($.isArray(databindObjs)) {
        var self = this;
        $.each(databindObjs, function (index, value) {
            if (value && value instanceof DatabindingObj) {
                self.databindObjs.push(value);
            }
        });
    }
};
HiddenField.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
HiddenField.prototype.fnGetBindTypeOfDatabindObj = function () {
    var objDatabindingObject = this.fnGetDatabindingObj();
    var objBindType = MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
    if (objDatabindingObject) {
        objBindType = objDatabindingObject.fnGetBindTypeObject();
    }
    return objBindType;
};
HiddenField.prototype.fnSetUserDefinedName = function (name) {
    this.userDefinedName = name;
};
HiddenField.prototype.fnIsNewlyAdded = function () {
    return this.isNew;
}
HiddenField.prototype.fnMarkDeleted = function () {
    this.isDeleted = true;
}
HiddenField.prototype.fnRename = function (name) {
    if (!this.fnIsNewlyAdded() &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
HiddenField.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }
};
HiddenField.prototype.fnGetDisplayText = function () {
    return this.displayText;
};
HiddenField.prototype.fnSetDisplayText = function (value) {
    this.displayText = value;
};
HiddenField.prototype.fnGetBindToProperty = function () {
    return this.bindToProperty;
};
HiddenField.prototype.fnSetBindToProperty = function (value) {
    this.bindToProperty = value; ;
};
mFicientIde.HiddenFieldControlHelper = (function () {
    var _currentLargestIdOfHidField = 0;
    var _hiddenFieldsSection = "Sec_hd";
    var _hiddenFieldsColumnPanel = "Sec_hd_Col";
    var _jqueryKeyForContDiv = "contDivDataKey";
    var _jqueryKeyForTableRow = "tableRowDataKey";
    var _jqueryKeyForTable = "tableDataKey";
    //var _jqueryKeyForAdditionRow = "additionRowDataKey";
    //MANAGE GLOBAL VARIABLE
    function _setCurrentLargestIdOfHidField(cntrls) {
        var largestValue = 0;
        if (cntrls && $.isArray(cntrls) && cntrls.length > 0) {
            for (var i = 0; i <= cntrls.length - 1; i++) {
                var cntrl = cntrls[i];
                var idNumber = cntrl.id.split('_')[1];
                if (idNumber > largestValue) {
                    largestValue = idNumber;
                }
            }
        }
        else {
            _currentLargestIdOfHidField = 0;
        }
    }
    function _incrementCurrentLargestId() {
        _currentLargestIdOfHidField = _currentLargestIdOfHidField + 1;
    }
    function _resetCurrentLargestIdOfHidField() {
        _currentLargestIdOfHidField = 0;
    }
    //MANAGE GLOBAL VARIABLE

    function _getDivContainerOfHidFieldDtl() {
        return $('#divDetailsTableContainer');
    }
    function _getHidFieldsDetailTable() {
        var $containerDiv = _getDivContainerOfHidFieldDtl();
        return $containerDiv.find('table');
    }


    //Bind Details Table
    function _getTableStartTag() {
        return '<table class="detailsTable">';
    }
    function _getTableEndTag() {
        return '</table>';
    }
    function _getMappingTableHeadRow() {
        var strHtml = '<thead><tr>';
        strHtml += '<th >Name</th>';
        strHtml += '<th>Binding Type</th>';
        strHtml += '<th >Databinding</th><th class="w_30p"></th></tr></thead>';
        return strHtml;
    }
    function _getTBodyStartTag() {
        return '<tbody>';
    }
    function _getTBodyEndTag() {
        return '</tbody>';
    }
    function _getColumnsOfRow(isAdditionRow) {
        var strHtml = "";
        strHtml += '<td ><input type="text" class="HidFieldName"></td>';
        strHtml += '<td ><select  class="BindingType">';
        strHtml += _getOptionsForBindingType() + '</select></td>';
        strHtml += '<td ><input type="text" value="None" maxlength="50" class="txtPropSmall propDisable ObjectName" disabled="disabled" readonly="readonly">';
        strHtml += '<input type="button" class="BindButton w_100p" value="..."/></td>';
        if (!isAdditionRow) {
            strHtml += "<td class=\"w_30p\"><span class=\"crossImg16x16\"></span></td>";
        }
        else {
            strHtml += "<td class=\"w_30p\"><span class=\"addImg16x16\"></span></td>";
        }
        return strHtml;
    }
    function _getHidDtlsTableRowWithCross() {
        var strHtml = '<tr>';
        strHtml += _getColumnsOfRow(false);
        strHtml += '</tr>';
        return strHtml;
    }
    function _getHidDtlsTableRowOnlyAdd() {
        var strHtml = '<tr class="AdditionRow">';
        strHtml += _getColumnsOfRow(true);
        strHtml += '</tr>';
        return strHtml;
    }
    function _getOptionsForBindingType() {
        var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
        var strOptions = "";
        if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
            strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id + '">None</option>';
            strOptions += '<option value="' + MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id + '">Offline Database</option>';
        }
        else {
            strOptions = '<option value="0">None</option>';
            strOptions += '<option value="1">Database Object</option>';
            strOptions += '<option value="2">Web Service Object</option>';
            strOptions += '<option value="5">OData Object</option>';
            strOptions += '<option value="6">Offline Database</option>';
        }
        return strOptions;
    }
    function _addAdditionRowToTable() {
        var $mappingTable = _getHidFieldsDetailTable();
        var $tbody = $mappingTable.find('tbody');
        var $tr = $tbody.find('tr');

        if ($tr.length > 0) {
            $tbody.find('tr:last').after(_getHidDtlsTableRowOnlyAdd());
        }
        else {
            $tbody.append(_getHidDtlsTableRowOnlyAdd());
        }
    }
    function _addDetailsRowToTable(detailsTable/*jquery object*/) {
        var $detailsTable = null;
        if (detailsTable) {
            $detailsTable = detailsTable;
        }
        else {
            $detailsTable = _getHidFieldsDetailTable();
        }
        var $tbody = $detailsTable.find('tbody');
        var $tr = $tbody.find('tr').not('.AdditionRow');

        if ($tr.length > 0) {
            $tr.last().after(_getHidDtlsTableRowWithCross());
        }
        else {
            $tbody.find('tr.AdditionRow').before(_getHidDtlsTableRowWithCross());
        }
    }

    //BIND DETAILS TABLE

    //JQUERY DATA GET SET
    function _setDataOfContainerDiv(formId, cntrls/*array of hidden controls*/) {
        var $containerDiv = _getDivContainerOfHidFieldDtl();
        var aryCopyOfHidFields = [];
        if (cntrls && $.isArray(cntrls)) {
            $.extend(true, aryCopyOfHidFields, cntrls);
        }
        $containerDiv.data(_jqueryKeyForContDiv, {
            previousSavedDataCopy: aryCopyOfHidFields,
            formId: formId
        });
    }
    function _getDataOfContainerDiv() {
        var $containerDiv = _getDivContainerOfHidFieldDtl();
        return $containerDiv.data(_jqueryKeyForContDiv);
    }
    function _setDataOfAdditionRow() {
        var $detailsTable = _getHidFieldsDetailTable();
        var objHiddenField = new HiddenField({
            id: "HDF_" + _currentLargestIdOfHidField,
            userDefinedName: "",
            type: MF_IDE_CONSTANTS.CONTROLS.HIDDEN,
            databindObjs: [],
            isNew: true,
            oldName: "",
            isDeleted: false
        });

        var objDataObject = new DatabindingObj("", [], "0",
                            MF_IDE_CONSTANTS.ideCommandObjectTypes.None.id,
                            "", "");

        objHiddenField.fnAddDatabindObj(objDataObject);

        $detailsTable.find('tbody tr.AdditionRow')
        .data(_jqueryKeyForTableRow, objHiddenField);
    }
    function _setDataOfTableRow(tr/*jquery object*/, data) {
        if (!tr) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
        tr.data(_jqueryKeyForTableRow, data);
    }
    function _getDataOfTableRow(tr/*jquery key*/) {
        return tr.data(_jqueryKeyForTableRow);
    }
    function _setDataOfContTable(data) {
        var $detailsTbl = _getHidFieldsDetailTable();
        $detailsTbl.data(_jqueryKeyForTable, data);
    }
    function _getDataOfContTable() {
        var $detailsTbl = _getHidFieldsDetailTable();
        return $detailsTbl.data(_jqueryKeyForTable);
    }
    function _removeDataOfContTable() {
        var $detailsTbl = _getHidFieldsDetailTable();
        $detailsTbl.removeData(_jqueryKeyForTable);
    }
    function _removeDataOfContDiv() {
        var $contDiv = _getDivContainerOfHidFieldDtl();
        $contDiv.removeData(_jqueryKeyForContDiv);
    }
    //JQUERY DATA GET SET

    //Events 
    function _setEventsOfRow(tblRow/*jquery object*/) {
        tblRow.find('.BindButton').on('click', function (evnt) {
            var $self = $(this);
            var $tr = $self.closest('tr');
            var objHiddenField = _getDataOfTableRow($tr);
            var objDatabindObject = objHiddenField.fnGetDatabindingObj();
            var objDatabindObjectCopy = $.extend(true, {}, objDatabindObject);
            var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
            var intlJson = [],
                isEdit = false;
            switch (objHiddenField.fnGetBindTypeOfDatabindObj()) {
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.None:
                    showMessage("Object bind type is set as none.For databinding bind type should be other than none.", DialogType.Info);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                    mFicientIde.MF_DATA_BINDING
                        .databaseObjectBinding(objHiddenField, objDatabindObjectCopy);
                    _setDataOfContTable({
                        rowIndex: $tr.index()
                    });
                    showModalPopUpWithOutHeader(
                            'SubProcCtrlDbCmd', 'Database Object', 500);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:

                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                    mFicientIde.MF_DATA_BINDING
                    .webserviceObjectBinding(objHiddenField, objDatabindObjectCopy);
                    _setDataOfContTable({
                        rowIndex: $tr.index()
                    });
                    showModalPopUpWithOutHeader(
                        'SubProcCtrlWsCmd', 'Webservice Object', 500);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                    mFicientIde.MF_DATA_BINDING
                    .oDataObjectBinding(objHiddenField, objDatabindObjectCopy);
                    _setDataOfContTable({
                        rowIndex: $tr.index()
                    });
                    showModalPopUpWithOutHeader(
                        'SubProcCtrlODataCmd', 'OData Object', 500);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                    intlJson = MF_HELPERS
                                   .intlsenseHelper
                                   .getControlDatabindingIntellisenseJson(
                                       objCurrentForm, objHiddenField.id, true
                                   );
                    if (objDatabindObjectCopy != null &&
                            (objDatabindObjectCopy.id && objDatabindObjectCopy.id !== "-1")) {

                        isEdit = true;
                    }

                    mFicientIde
                       .MF_DATA_BINDING
                       .processDataBindingByObjectType({
                           controlType: MF_IDE_CONSTANTS.CONTROLS.HIDDEN,
                           databindObject: objDatabindObjectCopy,
                           objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                           isEdit: isEdit,
                           intlsJson: intlJson,
                           control: objHiddenField
                       });

                    _setDataOfContTable({
                        rowIndex: $tr.index()
                    });
                    showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 550, false);
                    break;
            }
        });
        tblRow.find('.crossImg16x16').on('click', function (evnt) {
            var blnConfirm = confirm('Are you sure you want to remove this property.');
            if (blnConfirm) {
                var $tr = $(this).closest('tr');
                var objDataOfContDiv = _getDataOfContainerDiv();
                var objHiddenField = _getDataOfTableRow($tr);
                if (objHiddenField.fnIsNewlyAdded()) {
                    _removeHidFieldFromContDiv(objHiddenField);
                }
                else {
                    objHiddenField.fnMarkDeleted();
                }
                $tr.remove();
            }
        });
        tblRow.find('.addImg16x16').on('click', _processAddRowToTable);
        tblRow.find('.BindingType').on('change', function (evnt) {
            var $self = $(this);
            var $tr = $self.closest('tr');
            var objHiddenField = _getDataOfTableRow($tr);
            objHiddenField.fnRemoveDatabindObjs();
            var dataObject = new DatabindingObj("", [], "0", $self.val(), "", "");
            objHiddenField.fnAddDatabindObj(dataObject);
            //objHiddenField.fnRemoveAllDataBindingProperties();
        });
    }
    function _setEventsOfCompleteTable() {
        var $tbl = _getHidFieldsDetailTable();
        $tbl.find('tbody tr').each(function (index) {
            _setEventsOfRow($(this));
        });
    }
    function _setEventOfLastAddedRow() {
        var $tbl = _getHidFieldsDetailTable();
        _setEventsOfRow($tbl.find('tbody tr').not('.AdditionRow').last());
    }
    //Events

    //PROCESS
    function _formHidDtlsHtml(formId, cntrls/*list of hid cntrl*/) {

        if (!cntrls) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
        _setDataOfContainerDiv(formId, cntrls);
        var i = 0;
        var $hidFieldsContDiv = _getDivContainerOfHidFieldDtl();

        var strHtml = _getTableStartTag();
        strHtml += _getMappingTableHeadRow();
        strHtml += _getTBodyStartTag();
        strHtml += _getTBodyEndTag();

        strHtml += _getTableEndTag();

        $hidFieldsContDiv.html('');

        $hidFieldsContDiv.html(strHtml);

        var $detailsTable = _getHidFieldsDetailTable();
        _addAdditionRowToTable();
        if (cntrls && $.isArray(cntrls) &&
            cntrls.length > 0) {
            for (i = 0; i <= cntrls.length - 1; i++) {
                _addDetailsRowToTable($detailsTable);
            }
        }
        _setCurrentLargestIdOfHidField(cntrls);
        _fillDataToHtml(cntrls);
        _setDataOfAdditionRow();
        _setEventsOfCompleteTable();
    }
    function _fillDataToHtml(cntrls) {
        if (cntrls && $.isArray(cntrls) && cntrls.length > 0) {
            var $detailsTable = _getHidFieldsDetailTable();
            var objContDivData = _getDataOfContainerDiv();
            var aryHidFields = [];
            if (objContDivData) {
                aryHidFields = objContDivData.previousSavedDataCopy; //should get this.
            }
            $detailsTable.find('tbody tr')
           .not('.AdditionRow')
           .each(function (index) {
               _fillDataToHtmlTblRow($(this), aryHidFields[index]);
           });
        }
    }
    function _fillDataToHtmlTblRow(tblRow/*jquery object*/,
                dataObject/*HiddenField Object*/) {

        if (!tblRow)
            throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
        if (dataObject) {
            var $row = tblRow;
            var $txtHidFieldName = $row.find('.HidFieldName');
            var $ddlBindType = $row.find('.BindingType');
            var $txtObjectName = $row.find('.ObjectName');
            var objDatabindingObj = dataObject.fnGetDatabindingObj();
            $txtHidFieldName.val(dataObject.userDefinedName);
            $ddlBindType.val(dataObject.fnGetBindTypeOfDatabindObj().id);
            if (!mfUtil.isNullOrUndefined(objDatabindingObj.name)
                && !mfUtil.isEmptyString(objDatabindingObj.name)) {
                $txtObjectName.val(objDatabindingObj.name);
            }
            else {
                $txtObjectName.val('None');
            }
            _setDataOfTableRow(tblRow, dataObject);
        }
    }
    function _getAllHidFieldIdsFromContDiv() {
        var objContDivData = _getDataOfContainerDiv();
        var aryHidFields = objContDivData.previousSavedDataCopy;
        var aryIds = [];
        if (aryHidFields && $.isArray(aryHidFields) && aryHidFields.length > 0) {
            for (var i = 0; i <= aryHidFields.length - 1; i++) {
                aryIds.push(aryHidFields[i].id);
            }
        }
        return aryIds;
    }
    function _getAllOldHidFieldsDeleted() {
        var objContDivData = _getDataOfContainerDiv();
        var aryHidFields = objContDivData.previousSavedDataCopy;
        var aryFilteredHidfields = [];
        if (aryHidFields && $.isArray(aryHidFields) && aryHidFields.length > 0) {
            aryFilteredHidfields = $.grep(aryHidFields, function (value, index) {
                return value.isDeleted !== false;
            });
        }
        return aryFilteredHidfields;
    }
    function _removeHidFieldFromContDiv(hiddenField) {
        if (hiddenField) {
            var objContDivData = _getDataOfContainerDiv();
            var arySavedHiddenFields = objContDivData.previousSavedDataCopy;
            var aryFilteredHidField = [];
            if (arySavedHiddenFields && $.isArray(arySavedHiddenFields) &&
                arySavedHiddenFields.length > 0) {
                arySavedHiddenFields = $.grep(arySavedHiddenFields,
                        function (value, index) {
                            return value.id !== hiddenField.id;
                        });
            }
            objContDivData.previousSavedDataCopy = arySavedHiddenFields;
        }
    }
    //PROCESS

    // function _getAllHidFieldNamesFromTable(){
    //     var aryNames = [];
    //     var $detailsTable = _getHidFieldsDetailTable();
    //     $detailsTable.find('tbody tr')
    //     .not('.AdditionRow')
    //     .each(function (index) {
    //         var $txtName = $(this).find('.HidFieldName');
    //         aryNames.push($txtName.val());
    //     });
    //     return aryNames;
    // }
    function _validateSaving() {
        var aryNamesAlreadyChecked = [];
        var aryErrors = [];
        var $detailsTable = _getHidFieldsDetailTable();
        var blnDoesNameExists = false;
        var objContDivData = _getDataOfContainerDiv();
        var objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
        if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
        var aryOldHidFieldsDeleted = _getAllOldHidFieldsDeleted();
        $detailsTable.find('tbody tr')
        //.not('.AdditionRow')
        .each(function (index) {
            var strIndex = "At Row Index :" + (index + 1) + "</br>";
            var strError = "";
            var $txtName = $(this).find('.HidFieldName');
            var strName = $txtName.val();
            var blnValidateData = false;
            if ($(this).hasClass('AdditionRow')) {
                if (!mfUtil.isEmptyString(strName)) {
                    blnValidateData = true;
                }
                else {
                    blnValidateData = false;
                }
            }
            else {
                blnValidateData = true;
            }
            if (blnValidateData === true) {
                if (!strName) {
                    strError += "Please enter name for hidden field.<br/>";
                }
                else {
                    //first check in html table.then check for other controls in form
                    blnDoesNameExists =
                    $.inArray(strName, aryNamesAlreadyChecked) === -1 ? false : true;
                    if (!blnDoesNameExists) {
                        blnDoesNameExists = objForm.fnControlNameExist($txtName.val()
                           , _getAllHidFieldIdsFromContDiv());
                        if (blnDoesNameExists) {
                            strError += 'Control name already exist. Please change name of control.</br>';
                        }
                        else {
                            var aryHidFieldWithSameName = $.grep(aryOldHidFieldsDeleted, function (value, index) {
                                return value.userDefinedName === strName;
                            });
                            if (aryHidFieldWithSameName.length > 0) {
                                aryErrors.push('Control name already exist. Please change name of control.');
                            }
                        }
                    }
                    else {
                        strError += 'Control name already exist. Please change name of control.</br>';
                    }
                }
            }
            if (strError.length > 0) {
                aryErrors.push(strIndex + strError);
            }
            if (!mfUtil.isEmptyString(strName)) {
                aryNamesAlreadyChecked.push(strName);
            }
        });
        return aryErrors;
    }

    function _validateColumnAddition(tblRow/*jquery object table row*/) {
        if (!tblRow) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
        var aryErrors = [];
        var objContDivData = _getDataOfContainerDiv();
        var $detailsTable = _getHidFieldsDetailTable();
        var $row = tblRow;
        var blnDoesNameExists = false;
        var strName = $row.find('.HidFieldName').val();
        var aryOldHidFieldsDeleted = [];
        //var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();

        var objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
        if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;

        if (!strName) {
            aryErrors.push("Please enter name for hidden field.");
        }
        else {
            //first check in html table.then check for other controls in form
            blnDoesNameExists = _nameExistsInDtlsTblExceptAdditionRow(strName);
            if (!blnDoesNameExists) {
                blnDoesNameExists = objForm.fnControlNameExist(strName
                    , _getAllHidFieldIdsFromContDiv());

                if (blnDoesNameExists) {
                    aryErrors.push('Control name already exist. Please change name of control.');
                }
                else {
                    aryOldHidFieldsDeleted = _getAllOldHidFieldsDeleted();
                    var aryHidFieldWithSameName = $.grep(aryOldHidFieldsDeleted, function (value, index) {
                        return value.userDefinedName === strName;
                    });
                    if (aryHidFieldWithSameName.length > 0) {
                        aryErrors.push('Control name already exist. Please change name of control.');
                    }
                }
            }
            else {
                aryErrors.push('Control name already exist. Please change name of control.');
            }
        }
        return aryErrors;
    }
    function _processAddRowToTable(evnt) {
        try {
            var $row = $(this).closest('tr');
            var errors = _validateColumnAddition($row);
            if (errors.length > 0)
                throw new MfError(errors);

            var objHiddenField = _getDataOfTableRow($row);
            objHiddenField.fnSetUserDefinedName($row.find('.HidFieldName').val());
            var $containerDiv = _getDivContainerOfHidFieldDtl();
            var objContainerDivData = _getDataOfContainerDiv();
            var $detailsTable = _getHidFieldsDetailTable();

            _addDetailsRowToTable($detailsTable);


            var $tbody = $detailsTable.find('tbody');
            var $tr = $tbody.find('tr').not('.AdditionRow');

            _fillDataToHtmlTblRow($tr.last(), objHiddenField);
            objContainerDivData.previousSavedDataCopy.push(objHiddenField);
            _setEventOfLastAddedRow();
            _incrementCurrentLargestId();
            _resetAdditionRow();
        }
        catch (error) {
            if (error instanceof MfError) {
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
            } else {
                console.log(error);
            }
        }

    }
    //check name accept in addition row
    //checked when the row is addesd to details taable
    function _nameExistsInDtlsTblExceptAdditionRow(name) {
        var blnNameExists = false;
        var $detailsTable = _getHidFieldsDetailTable();
        $detailsTable.find('tbody tr')
        .not('.AdditionRow')
        .each(function (index) {
            var $txtName = $(this).find('.HidFieldName');
            if ($txtName.val() === name) {
                blnNameExists = true;
                return false;
            }
            //   if($(this).hasClass('AdditionRow')){
            //       if(!mfUtil.isEmptyString($txtName.val())){
            //           if ($txtName.val() === name) {
            //               blnNameExists = true;
            //               return false;
            //           }
            //       }
            //   }
            //   else{

            //   }
        });
        return blnNameExists;
    }
    function _resetAdditionRow() {
        var $detailsTable = _getHidFieldsDetailTable();
        var $additionRow = $detailsTable.find('tr.AdditionRow');
        $additionRow.find('.HidFieldName').val("");
        $additionRow.find('.BindingType').val("0");
        $additionRow.find('.ObjectName').val("None");
        _setDataOfAdditionRow();
    }
    function _processSaveHiddenFields() {
        var aryErrors = _validateSaving();
        if (aryErrors.length > 0) {
            throw new MfError(aryErrors);
        }
        var aryControls = [];
        var $detailsTable = _getHidFieldsDetailTable();
        $detailsTable.find('tbody tr')
        //.not('.AdditionRow')
        .each(function (index) {
            var $txtName = $(this).find('.HidFieldName');
            var blnAddHidFldToColl = false;
            var objHiddenField = null;
            if ($(this).hasClass('AdditionRow')) {
                if (!mfUtil.isEmptyString($txtName.val())) {
                    blnAddHidFldToColl = true;
                }
            }
            else {
                blnAddHidFldToColl = true;
            }
            if (blnAddHidFldToColl === true) {
                objHiddenField = _getDataOfTableRow($(this));
                objHiddenField.fnRename($txtName.val());
                objHiddenField.fnSetUserDefinedName($txtName.val());
                aryControls.push(objHiddenField);
            }
        });
        var objContDivData = _getDataOfContainerDiv();
        var objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
        if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
        objForm.fnSetControlsOfColumnPanel(_hiddenFieldsSection,
                _hiddenFieldsColumnPanel, aryControls);
        var objPropSheetCntrl = PROP_JSON_HTML_MAP.Form.propSheetCntrl.Properties.getHiddenVariables();
        var aryHidFields = objForm.fnGetAllControlsofSection(_hiddenFieldsSection);
        if (objPropSheetCntrl) {
            objPropSheetCntrl.control.val(aryHidFields.length + " " + "Hidden Variables");
        }
    }
    function _processDatabindingObjectSaved() {
        var objContTblData = _getDataOfContTable();
        var rowIndex = objContTblData.rowIndex;
        var $table = _getHidFieldsDetailTable();
        var $tr = $table.find('tbody tr').eq(rowIndex);
        var objHidField = _getDataOfTableRow($tr);
        var objDatabinding = objHidField.fnGetDatabindingObj();
        if (objDatabinding) {
            $tr.find('.ObjectName').val(objDatabinding.name);
        }

        _removeDataOfContTable();
    }

    var _editableListPropsBindInChildForm = (function () {
        function _getPropsSettingTable() {
            return $('#tblChildFormHdfPropBind');
        }
        function _getListPropertiesAsDdlOptions(editListProps) {
            var strOptions = '<option value="-1">Select</option>',
                listProp = null,
                propName = "";
            if ($.isArray(editListProps) && editListProps.length > 0) {
                for (var i = 0; i <= editListProps.length - 1; i++) {
                    listProp = editListProps[i];
                    propName = listProp.fnGetName();
                    strOptions += '<option value=' + propName + '>' + propName + '</option>';
                }
            }
            return strOptions;
        }
        function _getRowsOfHtmlTbl(hiddenFields, editListProps) {
            var strOptionsForDdl = _getListPropertiesAsDdlOptions(editListProps);
            var strHtml = "", i = 0,
                objHidField = null;
            if ($.isArray(hiddenFields) && hiddenFields.length > 0) {
                for (i = 0; i <= hiddenFields.length - 1; i++) {
                    objHidField = hiddenFields[i];
                    strHtml += '<tr class="pad10">';
                    strHtml += '<td><span class="HidFieldId hide">' + objHidField.fnGetId() + '</span><span class="HidFieldName">' + objHidField.fnGetName() + '</span></td>';
                    strHtml += '<td><select class="EditListProp" onkeydown="return enterKeyFilter(event);">' + strOptionsForDdl + '</select></td>';
                    strHtml += '</tr>';
                }
            }
            return strHtml;
        }

        function _formHtml(hiddenFields, editListProps) {
            var $tblEditListProps = _getPropsSettingTable();
            $tblEditListProps.find('tbody tr').remove();
            var strRowHtmlToAdd = _getRowsOfHtmlTbl(hiddenFields, editListProps);
            if (strRowHtmlToAdd) {
                $tblEditListProps.find('tbody').html(strRowHtmlToAdd);
            }
        }

        function _fillDataInHtml(hiddenFields) {

            if ($.isArray(hiddenFields) &&
              hiddenFields.length > 0) {

                var $propSettingTbl = _getPropsSettingTable();
                var $tableRows = $propSettingTbl.find('tbody tr');
                $.each(hiddenFields, function (index, hidField) {
                    var objHidField = hidField;
                    var $row = $($tableRows[index]);
                    _fillDataInHtmlByRow($row, objHidField);
                });
            }
        }
        function _fillDataInHtmlByRow(tr/*jqueryObject*/, hidField) {
            var objHidField = hidField,
                  $row = tr,
                  mappedProp = objHidField.fnGetBindToProperty();
            if (mappedProp === "") {

            }
            else {
                $row.find('.EditListProp').val(mappedProp);
            }

        }
        function _getHidFieldAndPropMappingFromHtml() {
            var aryMappings = [];
            var $mappingTbl = _getPropsSettingTable();
            var $tr = $mappingTbl.find('tbody tr');
            $tr.each(function (index) {
                var $self = $(this);
                var $spanHidFieldId = $self.find('.HidFieldId');
                var $ddlEditListProp = $self.find('.EditListProp');
                var strEditPropVal = $ddlEditListProp.val();
                if (strEditPropVal === "-1") {
                    aryMappings.push({
                        hidId: $spanHidFieldId.text(),
                        prop: ""
                    });
                }
                else {
                    aryMappings.push({
                        hidId: $spanHidFieldId.text(),
                        prop: strEditPropVal
                    });
                }
            });
            return aryMappings;
        }

        return {
            createHtmlForPropsBinding: function (hidFields/*ary of hidden fields*/,
                        editListProps/*ary of editable list props*/) {

                _formHtml(hidFields, editListProps);
                _fillDataInHtml(hidFields, editListProps);
            },
            saveMappingOfHidAndEditListPropInForm: function () {
                var objForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(),
                    objHidField = null,
                    objMapping = null; //{hidId:"",prop:"" }
                if (!objForm) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;

                var aryMappings = _getHidFieldAndPropMappingFromHtml();
                if ($.isArray(aryMappings)) {
                    for (var i = 0; i <= aryMappings.length - 1; i++) {
                        objMapping = aryMappings[i];
                        objHidField = objForm.fnGetControlFromId(objMapping.hidId);
                        objHidField.fnSetBindToProperty(objMapping.prop);
                    }
                }
            }
        };
    })();
    return {
        formDetailsHtml: function (form) {
            if (!form) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var aryHidFields = form.fnGetAllControlsofSection(_hiddenFieldsSection);
            _formHidDtlsHtml(form.id, aryHidFields);
            showModalPopUp("divFormHiddenFieldContainer", "Hidden Variables", 600, false, "");
        },
        processSaveHiddenFieldsInForm: function (evnt) {
            _processSaveHiddenFields();
            _removeDataOfContDiv();
            closeModalPopUp("divFormHiddenFieldContainer");
        },
        processDatabindingObjectSaved: function () {
            _processDatabindingObjectSaved();
        },
        processCloseHiddenFieldDetails: function (evnt) {
            _removeDataOfContDiv();
            closeModalPopUp("divFormHiddenFieldContainer");
        },
        editListPropsBindingInChildForm: {
            createHtmlForPropsBinding: function (hidFields/*ary of hidden fields*/,
                     editListProps/*ary of editable list props*/) {

                _editableListPropsBindInChildForm
               .createHtmlForPropsBinding(hidFields, editListProps);
            },
            saveMappingOfHidAndEditListPropInForm: function () {
                _editableListPropsBindInChildForm.saveMappingOfHidAndEditListPropInForm();
            },
            closeMappingPropsDiv: function () {
                closeModalPopUp('divChildFormHidFieldPropBinding');
            }
        }
    };
})();
$(document).ready(function () {
    $('#btnAddHiddenFieldsToForm').on('click', function (evnt) {
        try {
            mFicientIde.HiddenFieldControlHelper.processSaveHiddenFieldsInForm(evnt);
        }
        catch (error) {
            if (error instanceof MfError) {
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
            } else {
                console.log(error);
            }
        }
    });
    $('#btnHidFieldsAdditionCancel').on('click', function (evnt) {
        try {
            mFicientIde.HiddenFieldControlHelper.processCloseHiddenFieldDetails(evnt);
        }
        catch (error) {
            if (error instanceof MfError) {
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
            } else {
                console.log(error);
            }
        }
    });
    $('#btnChildFormHdfPropBindSave').on('click', function (evnt) {
        try {
            mFicientIde.HiddenFieldControlHelper
             .editListPropsBindingInChildForm
             .saveMappingOfHidAndEditListPropInForm(evnt);

            mFicientIde.HiddenFieldControlHelper
             .editListPropsBindingInChildForm
             .closeMappingPropsDiv();
        }
        catch (error) {
            if (error instanceof MfError) {
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
            } else {
                console.log(error);
            }
        }
    });
    $('#btnChildFormHdfPropBindCancel').on('click', function (evnt) {
        try {
            mFicientIde.HiddenFieldControlHelper
            .editListPropsBindingInChildForm
            .closeMappingPropsDiv(evnt);
        }
        catch (error) {
            if (error instanceof MfError) {
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
            } else {
                console.log(error);
            }
        }
    });
});

function setDisplay(listObj, objPropSheetCntrl) {
    var objDatabindingObj = listObj.fnGetDatabindingObj();
    if (objDatabindingObj) {
        var objbindType = objDatabindingObj.fnGetBindTypeObject();
        if (objPropSheetCntrl) {
            if (objbindType && objbindType !== MF_IDE_CONSTANTS.ideCommandObjectTypes.None)
                objPropSheetCntrl.wrapperDiv.show();
            else
                objPropSheetCntrl.wrapperDiv.hide();
        }
    }
    else {
        if (objPropSheetCntrl)
            objPropSheetCntrl.wrapperDiv.hide();
    }
}

function setDataControlDisplay(listObj, objPropSheetCntrl) {
    var listType = listObj.fnGetListTypeByValue();
    if (objPropSheetCntrl) {
        switch (objPropSheetCntrl.control[0].id.split('_')[2]) {
            case "RowID":
            case "Title":
                setDisplay(listObj, objPropSheetCntrl);
                break;
            case "Subtitle":
            case "Information":
            case "AdditionalInformation":
                if (listType === MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription || listType === MF_IDE_CONSTANTS.listCntrlType.Thumbnail
                || listType === MF_IDE_CONSTANTS.listCntrlType.ImageList || listType === MF_IDE_CONSTANTS.listCntrlType.EventList)
                    setDisplay(listObj, objPropSheetCntrl);
                else
                    objPropSheetCntrl.wrapperDiv.hide();
                break;
            case "ImageName":
                if (listType === MF_IDE_CONSTANTS.listCntrlType.Icon || listType === MF_IDE_CONSTANTS.listCntrlType.Thumbnail
                || listType === MF_IDE_CONSTANTS.listCntrlType.ImageList)
                    setDisplay(listObj, objPropSheetCntrl);
                else
                    objPropSheetCntrl.wrapperDiv.hide();
                break;
            case "CountBubble":
                if (listObj.countBubble && listObj.countBubble === "1" && listType !== MF_IDE_CONSTANTS.listCntrlType.Simple)
                    setDisplay(listObj, objPropSheetCntrl);
                else
                    objPropSheetCntrl.wrapperDiv.hide();
                break;
            case "DateDatabind":
                if (listType !== MF_IDE_CONSTANTS.listCntrlType.EventList)
                    setDisplay(listObj, objPropSheetCntrl);
                else
                    objPropSheetCntrl.wrapperDiv.hide();
                break;
        }
    }
}

function setAdvanceGroupDisplay(appObj) {
    if (appObj) {
        var advanceGroup = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.list.propSheetCntrl.getAdvanceGroup();
        if (appObj.rowClickable === "1" || appObj.actionButton === "1")
            $('#' + advanceGroup).show();
        else
            $('#' + advanceGroup).hide();
    }
}

function drawListHtml(appObj) {
    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
    $('#' + appObj.id + '_Cntrl').html('');
    $('#' + appObj.id + '_Cntrl').append(getListHtml(appObj, objForm));
    if (appObj.dataInset) {
        if (appObj.dataInset === "0") {
            $('#' + appObj.id + '_Cntrl').css('display', '');
            $('#' + appObj.id + '_Cntrl > ul').removeClass('data-inset').css('width', '');
        }
        else {
            $('#' + appObj.id + '_Cntrl').css('display', 'inline-block');
            $('#' + appObj.id + '_Cntrl > ul').addClass('data-inset').css('width', getListWidth(appObj));
        }
    }
    if (appObj.fnGetListTypeByValue() === MF_IDE_CONSTANTS.listCntrlType.EventList) {
        for (var i = 1; i <= 4; i++) {
            $('#div_Day' + i + '').html(getDateAndTime('1'));
            $('#div_MonYr' + i + '').html(getDateAndTime('2'));
            $('#div_Time' + i + '').html(getDateAndTime('3'));
        }
    }
    if (objForm && objForm.isTablet)
        $('.TbOuterCont').equalHeights();
}

function getListHtml(appObj, objForm) {
    var type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Simple_list;
    if (appObj) {
        switch (appObj.fnGetListTypeByValue()) {
            case MF_IDE_CONSTANTS.listCntrlType.Simple:
                if (appObj.rowClickable === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Simple_list_with_transition;
                else
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Simple_list;
                break;
            case MF_IDE_CONSTANTS.listCntrlType.Basic:
                if (appObj.countBubble != "1" && appObj.rowClickable != "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list;
                else if (appObj.countBubble === "1" && appObj.rowClickable != "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable === "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_trans;
                else if (appObj.countBubble === "1" && appObj.rowClickable === "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_trans_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable != "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_actnbtn;
                else if (appObj.countBubble === "1" && appObj.rowClickable != "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_actnbtn_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable === "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_actnbtn;
                else if (appObj.countBubble === "1" && appObj.rowClickable === "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_actnbtn_with_count_bub;
                break;
            case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                if (appObj.countBubble != "1" && appObj.rowClickable != "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list;
                else if (appObj.countBubble === "1" && appObj.rowClickable != "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable === "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_trans;
                else if (appObj.countBubble === "1" && appObj.rowClickable === "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_trans_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable != "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_actnbtn;
                else if (appObj.countBubble === "1" && appObj.rowClickable != "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_actnbtn_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable === "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_actnbtn;
                else if (appObj.countBubble === "1" && appObj.rowClickable === "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_actnbtn_with_count_bub;
                break;
            case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                if (appObj.countBubble != "1" && appObj.rowClickable != "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list;
                else if (appObj.countBubble === "1" && appObj.rowClickable != "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable === "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_trans;
                else if (appObj.countBubble === "1" && appObj.rowClickable === "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_trans_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable != "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_actnbtn;
                else if (appObj.countBubble === "1" && appObj.rowClickable != "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_actnbtn_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable === "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_actnbtn;
                else if (appObj.countBubble === "1" && appObj.rowClickable === "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_actnbtn_with_count_bub;
                break;
            case MF_IDE_CONSTANTS.listCntrlType.Icon:
                if (appObj.countBubble != "1" && appObj.rowClickable != "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list;
                else if (appObj.countBubble === "1" && appObj.rowClickable != "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable === "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_trans;
                else if (appObj.countBubble === "1" && appObj.rowClickable === "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_trans_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable != "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_actnbtn;
                else if (appObj.countBubble === "1" && appObj.rowClickable != "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_actnbtn_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable === "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_actnbtn;
                else if (appObj.countBubble === "1" && appObj.rowClickable === "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_actnbtn_with_count_bub;
                break;
            case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                if (appObj.countBubble != "1" && appObj.rowClickable != "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list;
                else if (appObj.countBubble === "1" && appObj.rowClickable != "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable === "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_trans;
                else if (appObj.countBubble === "1" && appObj.rowClickable === "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_trans_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable != "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_actnbtn;
                else if (appObj.countBubble === "1" && appObj.rowClickable != "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_actnbtn_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable === "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_actnbtn;
                else if (appObj.countBubble === "1" && appObj.rowClickable === "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_actnbtn_with_count_bub;
                break;
            case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                if (appObj.countBubble != "1" && appObj.rowClickable != "1" && appObj.actionButton != "1" && appObj.caption === "0")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list;
                else if (appObj.countBubble === "1" && appObj.rowClickable != "1" && appObj.actionButton != "1" && appObj.caption === "0")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable === "1" && appObj.actionButton != "1" && appObj.caption === "0")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_trans;
                else if (appObj.countBubble === "1" && appObj.rowClickable === "1" && appObj.actionButton != "1" && appObj.caption === "0")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_trans_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable != "1" && appObj.actionButton === "1" && appObj.caption === "0")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_actnbtn;
                else if (appObj.countBubble === "1" && appObj.rowClickable != "1" && appObj.actionButton === "1" && appObj.caption === "0")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_actnbtn_with_count_bub;
                else if (appObj.countBubble != "1" && appObj.rowClickable === "1" && appObj.actionButton === "1" && appObj.caption === "0")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_actnbtn;
                else if (appObj.countBubble === "1" && appObj.rowClickable === "1" && appObj.actionButton === "1" && appObj.caption === "0")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_actnbtn_with_count_bub;
                else if (appObj.caption === "1" && appObj.rowClickable === "0" && appObj.actionButton === "0")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_with_caption;
                else if (appObj.caption === "1" && appObj.rowClickable === "1" && appObj.actionButton === "0")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_with_caption_with_transition;
                else if (appObj.caption === "1" && appObj.rowClickable === "0" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_with_caption_with_actnbtn;
                else if (appObj.caption === "1" && appObj.rowClickable === "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_with_caption_with_actnbtn;
                break;
            case MF_IDE_CONSTANTS.listCntrlType.EventList:
                if (appObj.rowClickable != "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Event_list;
                else if (appObj.rowClickable === "1" && appObj.actionButton != "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Event_list_trans;
                else if (appObj.rowClickable != "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Event_list_actnbtn;
                else if (appObj.rowClickable === "1" && appObj.actionButton === "1")
                    type = mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Event_list_actnbtn;
                break;
            case MF_IDE_CONSTANTS.listCntrlType.CustomList:
                type = MF_IDE_CONSTANTS.MF_LIST_CONTROL.Custom_list;
                break;
        }
    }
    var strListHtml = '';
    if (objForm) {
        if (!objForm.isTablet)
            strListHtml = generateListHtml(type);
        else {
            var colPanel = objForm.fnGetColPanelOfControl(appObj.id);
            if (colPanel) {
                switch (colPanel.id.split('_')[2]) {
                    case '1':
                        strListHtml = generateListHtml(type);
                        break;
                    case '2':
                        strListHtml = generateSmallTabListHtml(type);
                        break;
                    case '3':
                        if (colPanel.id.split('_')[3] == '1')
                            strListHtml = generateListHtml(type);
                        else
                            strListHtml = generateSmallTabListHtml(type);
                        break;
                    case '4':
                        if (colPanel.id.split('_')[3] == '1')
                            strListHtml = generateSmallTabListHtml(type);
                        else
                            strListHtml = generateListHtml(type);
                        break;
                    case '5':
                        strListHtml = generateListHtml(type);
                        break;
                }
            }
        }
    }
    return strListHtml;
}

function generateListHtml(type) {
    var strListHtml = '';
    if (type) {
        switch (type) {
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Simple_list:
                strListHtml = '<ul class="simplelist node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                              + '<div class="icon-list-text node" style="width: 83%; float: left; text-align:left;">'
                              + 'Title</div>'
                              + '<div style="width: 10%; float: left;" class="list-icon node hide">'
                              + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Simple_list_with_transition:
                strListHtml = '<ul class="simplelist" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                              + '<div class="icon-list-text node" style="width: 83%; float: left; text-align:left;">'
                              + 'Title</div>'
                              + '<div style="width: 10%; float: left;" class="list-icon node">'
                              + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list:
                strListHtml = '<ul class="unorderdlist" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                            + '<div class="icon-list-text node" style="width: 68%; float: left; text-align:left;">'
                            + 'Title</div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_with_count_bub:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                            + '<div class="icon-list-text node" style="width: 68%; float: left; text-align:left;">'
                            + 'Title</div>'
                            + '<div style="width: 12%; float: left; margin-right: 2px;">'
                            + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                            + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                            + '&nbsp;</div>'
                            + '<div style="width: 10%; float: left;" class="list-icon node hide">'
                            + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_trans:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                            + '<div class="icon-list-text node" style="width: 68%; float: left; text-align:left;">'
                            + 'Title</div>'
                            + '<div class="hide" style="width: 12%; float: left; margin-right: 2px;">'
                            + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                            + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                            + '&nbsp;</div>'
                            + '<div style="width: 10%; float: left;" class="list-icon node">'
                            + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_trans_with_count_bub:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                            + '<div class="icon-list-text node" style="width: 68%; float: left; text-align:left;">'
                            + 'Title</div>'
                            + '<div style="width: 12%; float: left; margin-right: 2px;">'
                            + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                            + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                            + '&nbsp;</div>'
                            + '<div style="width: 10%; float: left;" class="list-icon node">'
                            + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_actnbtn:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                            + '<div class="icon-list-text node" style="width: 68%; float: left; text-align:left;">'
                            + 'Title</div>'
                            + '<div style="width: 12%; float: left; margin-right: 2px;" class="hide">'
                            + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                            + '<div class="action-button-without-desc node" style="width: 2%; float: left;">'
                            + '&nbsp;</div>'
                            + '<div style="width: 10%; float: left;" class="list-icon node">'
                            + '<img src="images/action-black.png" class="transition-img" /></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_actnbtn_with_count_bub:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                            + '<div class="icon-list-text node" style="width: 68%; float: left; text-align:left;">'
                            + 'Title</div>'
                            + '<div style="width: 12%; float: left; margin-right: 2px;">'
                            + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                            + '<div class="action-button-without-desc node" style="width: 2%; float: left;">'
                            + '&nbsp;</div>'
                            + '<div style="width: 10%; float: left;" class="list-icon node">'
                            + '<img src="images/action-black.png" class="transition-img" /></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list:
                strListHtml = '<ul class="unorderdlist data-inset node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                    + '<div class="icon-list-text node" style="width: 68%; float: left; text-align:left;">'
                    + i + '&nbsp;&nbsp;.Title</div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_with_count_bub:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                    + '<div class="icon-list-text node" style="width: 68%; float: left; text-align:left;">'
                    + i + '&nbsp;&nbsp;.Title</div>'
                    + '<div class="node" style="width: 12%; float: left; margin-right: 2px;">'
                    + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                    + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                    + '&nbsp;</div>'
                    + '<div style="width: 10%; float: left;" class="list-icon node hide">'
                    + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_trans:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                    + '<div class="icon-list-text node" style="width: 68%; float: left; text-align:left;">'
                    + i + '&nbsp;&nbsp;.Title</div>'
                    + '<div class="node hide" style="width: 12%; float: left; margin-right: 2px;">'
                    + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                    + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                    + '&nbsp;</div>'
                    + '<div style="width: 10%; float: left;" class="list-icon node">'
                    + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_trans_with_count_bub:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                    + '<div class="icon-list-text node" style="width: 68%; float: left; text-align:left;">'
                    + i + '&nbsp;&nbsp;.Title</div>'
                    + '<div class="node" style="width: 12%; float: left; margin-right: 2px;">'
                    + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                    + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                    + '&nbsp;</div>'
                    + '<div style="width: 10%; float: left;" class="list-icon node">'
                    + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_actnbtn:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                    + '<div class="icon-list-text node" style="width: 68%; float: left; text-align:left;">'
                    + i + '&nbsp;&nbsp;.Title</div>'
                    + '<div class="node hide" style="width: 12%; float: left; margin-right: 2px;">'
                    + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                    + '<div class="action-button-without-desc node" style="width: 2%; float: left;">'
                    + '&nbsp;</div>'
                    + '<div style="width: 10%; float: left;" class="list-icon node">'
                    + '<img src="images/action-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_actnbtn_with_count_bub:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                    + '<div class="icon-list-text node" style="width: 68%; float: left; text-align:left;">'
                    + i + '&nbsp;&nbsp;.Title</div>'
                    + '<div class="node" style="width: 12%; float: left; margin-right: 2px;">'
                    + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                    + '<div class="action-button-without-desc node" style="width: 2%; float: left;">'
                    + '&nbsp;</div>'
                    + '<div style="width: 10%; float: left;" class="list-icon node">'
                    + '<img src="images/action-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list:
                strListHtml += '<ul class="unorderdlist node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                  + '<div class="list-icon node" style="width: 10%; float: left;">'
                  + '<img src="images/bars-black.png" /></div>'
                  + '<div class="icon-list-text node" style="width: 60%; float: left; text-align:left;">'
                  + 'Title</div>'
                + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_with_count_bub:
                strListHtml += '<ul class="unorderdlist node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                      + '<div class="list-icon node" style="width: 10%; float: left;">'
                      + '<img src="images/bars-black.png" /></div>'
                      + '<div class="icon-list-text node" style="width: 60%; float: left; text-align:left;">'
                      + 'Title</div>'
                      + '<div class="node" style="width: 12%; float: left; margin-right: 2px;">'
                      + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                      + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                      + '&nbsp;</div>'
                      + '<div style="width: 10%; float: left;" class="list-icon node hide">'
                      + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_trans:
                strListHtml += '<ul class="unorderdlist node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                      + '<div class="list-icon node" style="width: 10%; float: left;">'
                      + '<img src="images/bars-black.png" /></div>'
                      + '<div class="icon-list-text node" style="width: 60%; float: left; text-align:left;">'
                      + 'Title</div>'
                      + '<div class="node hide" style="width: 12%; float: left; margin-right: 2px;">'
                      + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                      + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                      + '&nbsp;</div>'
                      + '<div style="width: 10%; float: left;" class="list-icon node">'
                      + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_trans_with_count_bub:
                strListHtml += '<ul class="unorderdlist node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                      + '<div class="list-icon node" style="width: 10%; float: left;">'
                      + '<img src="images/bars-black.png" /></div>'
                      + '<div class="icon-list-text node" style="width: 60%; float: left; text-align:left;">'
                      + 'Title</div>'
                      + '<div class="node" style="width: 12%; float: left; margin-right: 2px;">'
                      + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                      + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                      + '&nbsp;</div>'
                      + '<div style="width: 10%; float: left;" class="list-icon node">'
                      + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_actnbtn:
                strListHtml += '<ul class="unorderdlist node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                      + '<div class="list-icon node" style="width: 10%; float: left;">'
                      + '<img src="images/bars-black.png" /></div>'
                      + '<div class="icon-list-text node" style="width: 60%; float: left; text-align:left;">'
                      + 'Title</div>'
                      + '<div class="node hide" style="width: 12%; float: left; margin-right: 2px;">'
                      + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                      + '<div class="action-button-without-desc node" style="width: 2%; float: left;">'
                      + '&nbsp;</div>'
                      + '<div style="width: 10%; float: left;" class="list-icon node">'
                      + '<img src="images/action-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_actnbtn_with_count_bub:
                strListHtml += '<ul class="unorderdlist node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                      + '<div class="list-icon node" style="width: 10%; float: left;">'
                      + '<img src="images/bars-black.png" /></div>'
                      + '<div class="icon-list-text node" style="width: 60%; float: left; text-align:left;">'
                      + 'Title</div>'
                      + '<div class="node" style="width: 12%; float: left; margin-right: 2px;">'
                      + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                      + '<div class="action-button-without-desc node" style="width: 2%; float: left;">'
                      + '&nbsp;</div>'
                      + '<div style="width: 10%; float: left;" class="list-icon node">'
                      + '<img src="images/action-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                   + '<div class="basic-desc-list-item node" style="text-align: left;">'
                   + 'Title'
                   + '</div>'
                   + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                   + '</div>'
                + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node hide">'
                       + '<img class="basic-desc-list-img" src="images/carat-r-black.png" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_trans:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node hide" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node">'
                       + '<img class="basic-desc-list-img" src="images/carat-r-black.png" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_trans_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node">'
                       + '<img class="basic-desc-list-img" src="images/carat-r-black.png" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_actnbtn:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node hide" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node" style="border-left: 1px solid #E7E7E7;">'
                       + '<img class="basic-desc-list-img" src="images/action-black.png" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_actnbtn_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node" style="border-left: 1px solid #E7E7E7;">'
                       + '<img class="basic-desc-list-img" src="images/action-black.png" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 48px;">'
                    + '<div class="node" style="height: 48px; margin-top: -11px; float: left;">'
                    + '<img src="images/DefaultImage.png" width="60" height="60" /></div>'
                    + '<div class="node" style="width: 57%; height: 48px; margin-left: -4px; float: left;">'
                    + '<div class="basic-desc-list-item node" style="text-align: left;">'
                    + 'Title'
                    + '</div>'
                    + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                    + '</div>'
            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 48px;">'
                    + '<div class="node" style="height: 48px; margin-top: -11px; float: left;">'
                    + '<img src="images/DefaultImage.png" width="60" height="60" /></div>'
                    + '<div  class="node"style="width: 57%; height: 48px; margin-left: -4px; float: left;">'
                    + '<div class="basic-desc-list-item node" style="text-align: left;">'
                    + 'Title'
                    + '<div class="count-bubble basic-desc-list-count-bub node">'
                    + i + 3 + '</div>'
                    + '</div>'
                    + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                    + '</div>'
            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_trans:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 48px;">'
                    + '<div class="node" style="height: 48px; margin-top: -11px; float: left;">'
                    + '<img src="images/DefaultImage.png" width="60" height="60" /></div>'
                    + '<div class="node" style="width: 57%; height: 48px; margin-left: -4px; float: left;">'
                    + '<div class="basic-desc-list-item node" style="text-align: left;">'
                    + 'Title'
                    + '</div>'
                    + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                    + '</div>'
                    + '<div class="node" style="width: 10%; height: 48px; float: left;">'
                    + '<img class="basic-desc-list-img" src="images/carat-r-black.png" /></div>'
            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_trans_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 48px;">'
                    + '<div class="node" style="height: 48px; margin-top: -11px; float: left;">'
                    + '<img src="images/DefaultImage.png" width="60" height="60" /></div>'
                    + '<div class="node" style="width: 57%; height: 48px; margin-left: -4px; float: left;">'
                    + '<div class="basic-desc-list-item node" style="text-align: left;">'
                    + 'Title'
                    + '<div class="count-bubble basic-desc-list-count-bub node">'
                    + i + 3 + '</div>'
                    + '</div>'
                    + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                    + '</div>'
                    + '<div class="node" style="width: 10%; height: 48px; float: left;">'
                    + '<img class="basic-desc-list-img" src="images/carat-r-black.png" /></div>'
            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_actnbtn:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 48px;">'
                    + '<div class="node" style="height: 48px; margin-top: -11px; float: left;">'
                    + '<img src="images/DefaultImage.png" width="60" height="60" /></div>'
                    + '<div class="node" style="width: 57%; height: 48px; margin-left: -4px; float: left;">'
                    + '<div class="basic-desc-list-item node" style="text-align: left;">'
                    + 'Title'
                    + '</div>'
                    + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                    + '</div>'
                    + '<div class="node" style="width: 10%; height: 48px; border-left: 1px solid #E7E7E7; float: left;">'
                    + '<img class="basic-desc-list-img" src="images/action-black.png" /></div>'
            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_actnbtn_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 48px;">'
                    + '<div class="node" style="height: 48px; margin-top: -11px; float: left;">'
                    + '<img src="images/DefaultImage.png" width="60" height="60" /></div>'
                    + '<div class="node" style="width: 57%; height: 48px; margin-left: -4px; float: left;">'
                    + '<div class="basic-desc-list-item node" style="text-align: left;">'
                    + 'Title'
                    + '<div class="count-bubble basic-desc-list-count-bub node">'
                    + i + 3 + '</div>'
                    + '</div>'
                    + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                    + '</div>'
                    + '<div class="node" style="width: 10%; height: 48px; border-left: 1px solid #E7E7E7;float: left;">'
                    + '<img class="basic-desc-list-img" src="images/action-black.png" /></div>'
            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 2; i++) {
                    strListHtml += '<li>'
                    + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                    + 'background-repeat: no-repeat;background-position: center center;'
                    + '-o-background-size: contain;-moz-background-size: contain;-webkit-background-size: contain;'
                    + 'background-size: contain;">&nbsp;</div>'
                    + '</li>'
                    + '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                   + '<div class="basic-desc-list-item node" style="text-align: left;">'
                   + 'Title'
                   + '</div>'
                   + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                   + '</div>'
                + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 2; i++) {
                    strListHtml += '<li>'
                    + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                    + 'background-repeat: no-repeat;background-position: center center;'
                    + '-o-background-size: contain;-moz-background-size: contain;-webkit-background-size: contain;'
                    + 'background-size: contain;">&nbsp;</div>'
                    + '</li>'
                        + '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node hide">'
                       + '<img class="basic-desc-list-img" src="images/carat-r-black.png" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_trans:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 2; i++) {
                    strListHtml += '<li>'
                    + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                    + 'background-repeat: no-repeat;background-position: center center;'
                    + '-o-background-size: contain;-moz-background-size: contain;-webkit-background-size: contain;'
                    + 'background-size: contain;">&nbsp;</div>'
                    + '</li>'
                        + '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node hide" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node">'
                       + '<img class="basic-desc-list-img" src="images/carat-r-black.png" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_trans_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 2; i++) {
                    strListHtml += '<li>'
                    + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                    + 'background-repeat: no-repeat;background-position: center center;'
                    + '-o-background-size: contain;-moz-background-size: contain;-webkit-background-size: contain;'
                    + 'background-size: contain;">&nbsp;</div>'
                    + '</li>'
                       + '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node">'
                       + '<img class="basic-desc-list-img" src="images/carat-r-black.png" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_actnbtn:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 2; i++) {
                    strListHtml += '<li>'
                    + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                    + 'background-repeat: no-repeat;background-position: center center;'
                    + '-o-background-size: contain;-moz-background-size: contain;-webkit-background-size: contain;'
                    + 'background-size: contain;">&nbsp;</div>'
                    + '</li>'
                       + '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node hide" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node" style="border-left: 1px solid #E7E7E7;">'
                       + '<img class="basic-desc-list-img" src="images/action-black.png" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_actnbtn_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 2; i++) {
                    strListHtml += '<li>'
                    + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                    + 'background-repeat: no-repeat;background-position: center center;'
                    + '-o-background-size: contain;-moz-background-size: contain;-webkit-background-size: contain;'
                    + 'background-size: contain;">&nbsp;</div>'
                    + '</li>'
                       + '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node" style="border-left: 1px solid #E7E7E7;">'
                       + '<img class="basic-desc-list-img" src="images/action-black.png" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_with_caption:
                strListHtml = '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li>'
                            + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                            + 'background-repeat: no-repeat;background-position: center center;'
                            + '-o-background-size: contain;-moz-background-size: contain;'
                            + '-webkit-background-size: contain;background-size: contain;position:relative;">'
                            + '<div class="transbox node"><p class="node" style="text-align: left; font-size:12.5px;">description</p></div></div>'
                            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_with_caption_with_transition:
                strListHtml = '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li>'
                            + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                            + 'background-repeat: no-repeat;background-position: center center;'
                            + '-o-background-size: contain;-moz-background-size: contain;'
                            + '-webkit-background-size: contain;background-size: contain;position:relative;">'
                            + '<div class="transbox node">'
                            + '<p class="node" style="text-align: left; font-size:12.5px;float:left;">description</p>'
                            + '<p class="node" style="text-align: left; font-size:12.5px;float:right;margin-right:8px;"><img src="images/carat-r-white.png" /></p>'
                            + '</div></div>'
                            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_with_caption_with_actnbtn:
                strListHtml = '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li>'
                            + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                            + 'background-repeat: no-repeat;background-position: center center;'
                            + '-o-background-size: contain;-moz-background-size: contain;'
                            + '-webkit-background-size: contain;background-size: contain;position:relative;">'
                            + '<div class="transbox node">'
                            + '<p class="node" style="text-align: left; font-size:12.5px;float:left;">description</p>'
                            + '<p class="node" style="text-align: left; font-size:12.5px;float:right;margin-right:8px;"><img src="images/action-white.png" /></p>'
                            + '</div></div>'
                            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Event_list:
                strListHtml = '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 60px;">'
                                + '<div class="node" style="width: 32%; height: 60px; float: left;border-right:1px solid #E7E7E7;">'
                                + '<div class="node" id="div_Day' + i + '" style="text-align: center; font-size: 30px; font-family: FontAwesome; color:#2f3e46;"></div>'
                                + '<div class="node" id="div_MonYr' + i + '" style="text-align: center; font-size: 11.5px; font-family: FontAwesome; color:#2f3e46;"></div>'
                                + '<div class="node" id="div_Time' + i + '" style="text-align: center; margin-top:2px; font-size: 11.5px; font-family: FontAwesome; color:#2f3e46;"></div>'
                                + '</div>'
                                + '<div class="node" style="width: 54%; height: 60px; margin-left: -26px; float: left;">'
                                + '<div class="basic-desc-list-item node" style="margin-top:2px;">Title</div>'
                                + '<p class="node">description</p>'
                                + '</div>'
                                + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Event_list_trans:
                strListHtml = '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 60px;">'
                                + '<div class="node" style="width: 32%; height: 60px; float: left;border-right:1px solid #E7E7E7;">'
                                + '<div class="node" id="div_Day' + i + '" style="text-align: center; font-size: 30px; font-family: FontAwesome; color:#2f3e46;"></div>'
                                + '<div class="node" id="div_MonYr' + i + '" style="text-align: center; font-size: 11.5px; font-family: FontAwesome; color:#2f3e46;"></div>'
                                + '<div class="node" id="div_Time' + i + '" style="text-align: center; margin-top:2px; font-size: 11.5px; font-family: FontAwesome; color:#2f3e46;"></div>'
                                + '</div>'
                                + '<div class="node" style="width: 54%; height: 60px; margin-left: -26px; float: left;">'
                                + '<div class="basic-desc-list-item node" style="margin-top:2px;">Title</div>'
                                + '<p class="node">description</p>'
                                + '</div>'
                                + '<div class="node" style="width: 10%; height: 60px; float: left;">'
                                + '<img class="basic-desc-list-img" src="images/carat-r-black.png" /></div>'
                                + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Event_list_actnbtn:
                strListHtml = '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 60px;">'
                                + '<div class="node" style="width: 32%; height: 60px; float: left;border-right:1px solid #E7E7E7;">'
                                + '<div class="node" id="div_Day' + i + '" style="text-align: center; font-size: 30px; font-family: FontAwesome; color:#2f3e46;"></div>'
                                + '<div class="node" id="div_MonYr' + i + '" style="text-align: center; font-size: 11.5px; font-family: FontAwesome; color:#2f3e46;"></div>'
                                + '<div class="node" id="div_Time' + i + '" style="text-align: center; margin-top:2px; font-size: 11.5px; font-family: FontAwesome; color:#2f3e46;"></div>'
                                + '</div>'
                                + '<div class="node" style="width: 54%; height: 60px; margin-left: -26px; float: left;">'
                                + '<div class="basic-desc-list-item node" style="margin-top:2px;">Title</div>'
                                + '<p class="node">description</p>'
                                + '</div>'
                                + '<div class="node" style="width: 10%; height: 60px; float: left;border-left:1px solid #E7E7E7;">'
                                + '<img class="basic-desc-list-img" src="images/action-black.png" /></div>'
                                + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case MF_IDE_CONSTANTS.MF_LIST_CONTROL.Custom_list:
                strListHtml = '<ul class="unorderdlist" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node" style="position:relative;height:30px;">'
                            + '<div style="width:30px;height:30px;border-radius:15px;background:grey;color:white;" class="centerAligned">'
                            + '<div class="centerAligned">JS</div></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
        }
    }
    return strListHtml;
}

function generateSmallTabListHtml(type) {
    var strListHtml = '';
    if (type) {
        switch (type) {
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Simple_list:
                strListHtml = '<ul class="simplelist node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                              + '<div class="icon-list-text node" style="width: 75%; float: left; text-align:left;">'
                              + 'Title</div>'
                              + '<div style="width: 10%; float: left;" class="list-icon node hide">'
                              + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Simple_list_with_transition:
                strListHtml = '<ul class="simplelist" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                              + '<div class="icon-list-text node" style="width: 75%; float: left; text-align:left;">'
                              + 'Title</div>'
                              + '<div style="width: 10%; float: left;" class="list-icon node">'
                              + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list:
                strListHtml = '<ul class="unorderdlist" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                            + '<div class="icon-list-text node" style="width: 68%; float: left; text-align:left;">'
                            + 'Title</div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_with_count_bub:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                            + '<div class="icon-list-text node" style="width: 54%; float: left; text-align:left;">'
                            + 'Title</div>'
                            + '<div style="width: 12%; float: left; margin-right: 10px;">'
                            + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                            + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                            + '&nbsp;</div>'
                            + '<div style="width: 10%; float: left;" class="list-icon node hide">'
                            + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_trans:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                            + '<div class="icon-list-text node" style="width: 54%; float: left; text-align:left;">'
                            + 'Title</div>'
                            + '<div class="hide" style="width: 12%; float: left; margin-right: 10px;">'
                            + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                            + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                            + '&nbsp;</div>'
                            + '<div style="width: 10%; float: left;" class="list-icon node">'
                            + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_trans_with_count_bub:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                            + '<div class="icon-list-text node" style="width: 54%; float: left; text-align:left;">'
                            + 'Title</div>'
                            + '<div style="width: 12%; float: left; margin-right: 10px;">'
                            + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                            + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                            + '&nbsp;</div>'
                            + '<div style="width: 10%; float: left;" class="list-icon node">'
                            + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_actnbtn:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                            + '<div class="icon-list-text node" style="width: 54%; float: left; text-align:left;">'
                            + 'Title</div>'
                            + '<div style="width: 12%; float: left; margin-right: 10px;" class="hide">'
                            + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                            + '<div class="action-button-without-desc node" style="width: 2%; float: left;">'
                            + '&nbsp;</div>'
                            + '<div style="width: 10%; float: left;" class="list-icon node">'
                            + '<img src="images/action-black.png" class="transition-img" /></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_list_actnbtn_with_count_bub:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                            + '<div class="icon-list-text node" style="width: 54%; float: left; text-align:left;">'
                            + 'Title</div>'
                            + '<div style="width: 12%; float: left; margin-right: 10px;">'
                            + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                            + '<div class="action-button-without-desc node" style="width: 2%; float: left;">'
                            + '&nbsp;</div>'
                            + '<div style="width: 10%; float: left;" class="list-icon node">'
                            + '<img src="images/action-black.png" class="transition-img" /></div>'
                            + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list:
                strListHtml = '<ul class="unorderdlist data-inset node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                    + '<div class="icon-list-text node" style="width: 68%; float: left; text-align:left;">'
                    + i + '&nbsp;&nbsp;.Title</div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_with_count_bub:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                    + '<div class="icon-list-text node" style="width: 54%; float: left; text-align:left;">'
                    + i + '&nbsp;&nbsp;.Title</div>'
                    + '<div class="node" style="width: 12%; float: left; margin-right: 10px;">'
                    + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                    + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                    + '&nbsp;</div>'
                    + '<div style="width: 10%; float: left;" class="list-icon node hide">'
                    + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_trans:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                    + '<div class="icon-list-text node" style="width: 54%; float: left; text-align:left;">'
                    + i + '&nbsp;&nbsp;.Title</div>'
                    + '<div class="node hide" style="width: 12%; float: left; margin-right: 10px;">'
                    + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                    + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                    + '&nbsp;</div>'
                    + '<div style="width: 10%; float: left;" class="list-icon node">'
                    + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_trans_with_count_bub:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                    + '<div class="icon-list-text node" style="width: 54%; float: left; text-align:left;">'
                    + i + '&nbsp;&nbsp;.Title</div>'
                    + '<div class="node" style="width: 12%; float: left; margin-right: 10px;">'
                    + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                    + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                    + '&nbsp;</div>'
                    + '<div style="width: 10%; float: left;" class="list-icon node">'
                    + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_actnbtn:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                    + '<div class="icon-list-text node" style="width: 54%; float: left; text-align:left;">'
                    + i + '&nbsp;&nbsp;.Title</div>'
                    + '<div class="node hide" style="width: 12%; float: left; margin-right: 10px;">'
                    + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                    + '<div class="action-button-without-desc node" style="width: 2%; float: left;">'
                    + '&nbsp;</div>'
                    + '<div style="width: 10%; float: left;" class="list-icon node">'
                    + '<img src="images/action-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Numbered_list_actnbtn_with_count_bub:
                strListHtml = '<ul class="unorderdlist node" style="list-style: none;">'
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                    + '<div class="icon-list-text node" style="width: 54%; float: left; text-align:left;">'
                    + i + '&nbsp;&nbsp;.Title</div>'
                    + '<div class="node" style="width: 12%; float: left; margin-right: 10px;">'
                    + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                    + '<div class="action-button-without-desc node" style="width: 2%; float: left;">'
                    + '&nbsp;</div>'
                    + '<div style="width: 10%; float: left;" class="list-icon node">'
                    + '<img src="images/action-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list:
                strListHtml += '<ul class="unorderdlist node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                  + '<div class="list-icon node" style="width: 10%; float: left;">'
                  + '<img src="images/bars-black.png" /></div>'
                  + '<div class="icon-list-text node" style="width: 60%; float: left; text-align:left;">'
                  + 'Title</div>'
                + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_with_count_bub:
                strListHtml += '<ul class="unorderdlist node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                      + '<div class="list-icon node" style="width: 10%; float: left;">'
                      + '<img src="images/bars-black.png" /></div>'
                      + '<div class="icon-list-text node" style="width: 54%; float: left; text-align:left;">'
                      + 'Title</div>'
                      + '<div class="node" style="width: 12%; float: left; margin-right: 10px;">'
                      + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                      + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                      + '&nbsp;</div>'
                      + '<div style="width: 10%; float: left;" class="list-icon node hide">'
                      + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_trans:
                strListHtml += '<ul class="unorderdlist node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                      + '<div class="list-icon node" style="width: 10%; float: left;">'
                      + '<img src="images/bars-black.png" /></div>'
                      + '<div class="icon-list-text node" style="width: 54%; float: left; text-align:left;">'
                      + 'Title</div>'
                      + '<div class="node hide" style="width: 12%; float: left; margin-right: 10px;">'
                      + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                      + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                      + '&nbsp;</div>'
                      + '<div style="width: 10%; float: left;" class="list-icon node">'
                      + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_trans_with_count_bub:
                strListHtml += '<ul class="unorderdlist node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                      + '<div class="list-icon node" style="width: 10%; float: left;">'
                      + '<img src="images/bars-black.png" /></div>'
                      + '<div class="icon-list-text node" style="width: 48%; float: left; text-align:left;">'
                      + 'Title</div>'
                      + '<div class="node" style="width: 12%; float: left; margin-right: 10px;">'
                      + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                      + '<div class="action-button-without-desc node hide" style="width: 2%; float: left;">'
                      + '&nbsp;</div>'
                      + '<div style="width: 10%; float: left;" class="list-icon node">'
                      + '<img src="images/carat-r-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_actnbtn:
                strListHtml += '<ul class="unorderdlist node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                      + '<div class="list-icon node" style="width: 10%; float: left;">'
                      + '<img src="images/bars-black.png" /></div>'
                      + '<div class="icon-list-text node" style="width: 54%; float: left; text-align:left;">'
                      + 'Title</div>'
                      + '<div class="node hide" style="width: 12%; float: left; margin-right: 10px;">'
                      + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                      + '<div class="action-button-without-desc node" style="width: 2%; float: left;">'
                      + '&nbsp;</div>'
                      + '<div style="width: 10%; float: left;" class="list-icon node">'
                      + '<img src="images/action-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Icon_list_actnbtn_with_count_bub:
                strListHtml += '<ul class="unorderdlist node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li><div class="item node">'
                      + '<div class="list-icon node" style="width: 10%; float: left;">'
                      + '<img src="images/bars-black.png" /></div>'
                      + '<div class="icon-list-text node" style="width: 48%; float: left; text-align:left;">'
                      + 'Title</div>'
                      + '<div class="node" style="width: 12%; float: left; margin-right: 10px;">'
                      + '<span class="count-bubble node">' + i + 3 + '</span></div>'
                      + '<div class="action-button-without-desc node" style="width: 2%; float: left;">'
                      + '&nbsp;</div>'
                      + '<div style="width: 10%; float: left;" class="list-icon node">'
                      + '<img src="images/action-black.png" class="transition-img" /></div>'
                    + '</div></li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                   + '<div class="basic-desc-list-item node" style="text-align: left;">'
                   + 'Title'
                   + '</div>'
                   + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                   + '</div>'
                + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node hide">'
                       + '<img class="basic-desc-list-img" src="images/carat-r-black.png"/></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_trans:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node hide" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node">'
                       + '<img class="basic-desc-list-img" src="images/carat-r-black.png" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_trans_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node">'
                       + '<img class="basic-desc-list-img" src="images/carat-r-black.png" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_actnbtn:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node hide" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node" style="border-left: 1px solid #E7E7E7;">'
                       + '<img class="basic-desc-list-img" style="margin-left:6px;" src="images/action-black.png" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Basic_desc_list_actnbtn_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node" style="border-left: 1px solid #E7E7E7;">'
                       + '<img class="basic-desc-list-img" style="margin-left:6px;" src="images/action-black.png" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 48px;">'
                    + '<div class="node" style="width: 30%; height: 48px; margin-top: -11px; float: left;">'
                    + '<img src="images/DefaultImage.png" width="60" height="60" /></div>'
                    + '<div class="node" style="width: 54%; height: 48px; margin-left: -4px; float: left;">'
                    + '<div class="basic-desc-list-item node" style="text-align: left;">'
                    + 'Title'
                    + '</div>'
                    + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                    + '</div>'
            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 48px;">'
                    + '<div class="node" style="width: 30%; height: 48px; margin-top: -11px; float: left;">'
                    + '<img src="images/DefaultImage.png" width="40" height="60" /></div>'
                    + '<div  class="node"style="width: 54%; height: 48px; margin-left: -4px; float: left;">'
                    + '<div class="basic-desc-list-item node" style="text-align: left;">'
                    + 'Title'
                    + '<div class="count-bubble basic-desc-list-count-bub node">'
                    + i + 3 + '</div>'
                    + '</div>'
                    + '<p class="node" style="text-align: left;margin-left:10px;">desc..</p>'
                    + '</div>'
            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_trans:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 48px;">'
                    + '<div class="node" style="width: 30%; height: 48px; margin-top: -11px; float: left;">'
                    + '<img src="images/DefaultImage.png" width="40" height="60" /></div>'
                    + '<div class="node" style="width: 54%; height: 48px; margin-left: -4px; float: left;">'
                    + '<div class="basic-desc-list-item node" style="text-align: left;">'
                    + 'Title'
                    + '</div>'
                    + '<p class="node" style="text-align: left;margin-left:10px;">desc..</p>'
                    + '</div>'
                    + '<div class="node" style="width: 10%; height: 48px; float: left;">'
                    + '<img class="basic-desc-list-img" src="images/carat-r-black.png" style="margin-left:6px;" /></div>'
            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_trans_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 48px;">'
                    + '<div class="node" style="width: 30%; height: 48px; margin-top: -11px; float: left;">'
                    + '<img src="images/DefaultImage.png" width="40" height="60" /></div>'
                    + '<div class="node" style="width: 54%; height: 48px; margin-left: -4px; float: left;">'
                    + '<div class="basic-desc-list-item node" style="text-align: left;">'
                    + 'Title'
                    + '<div class="count-bubble basic-desc-list-count-bub node">'
                    + i + 3 + '</div>'
                    + '</div>'
                    + '<p class="node" style="text-align: left;margin-left:10px;">desc..</p>'
                    + '</div>'
                    + '<div class="node" style="width: 10%; height: 48px; float: left;">'
                    + '<img class="basic-desc-list-img" src="images/carat-r-black.png" style="margin-left:6px;" /></div>'
            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_actnbtn:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 48px;">'
                    + '<div class="node" style="width: 30%; height: 48px; margin-top: -11px; float: left;">'
                    + '<img src="images/DefaultImage.png" width="40" height="60" /></div>'
                    + '<div class="node" style="width: 54%; height: 48px; margin-left: -4px; float: left;">'
                    + '<div class="basic-desc-list-item node" style="text-align: left;">'
                    + 'Title'
                    + '</div>'
                    + '<p class="node" style="text-align: left;margin-left:10px;">desc..</p>'
                    + '</div>'
                    + '<div class="node" style="width: 10%; height: 48px; border-left: 1px solid #E7E7E7; float: left;">'
                    + '<img class="basic-desc-list-img" style="margin-left:6px;" src="images/action-black.png" /></div>'
            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Thumbnail_list_actnbtn_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li style="height: 48px;">'
                    + '<div class="node" style="width: 30%; height: 48px; margin-top: -11px; float: left;">'
                    + '<img src="images/DefaultImage.png" width="40" height="60" /></div>'
                    + '<div class="node" style="width: 54%; height: 48px; margin-left: -4px; float: left;">'
                    + '<div class="basic-desc-list-item node" style="text-align: left;">'
                    + 'Title'
                    + '<div class="count-bubble basic-desc-list-count-bub node">'
                    + i + 3 + '</div>'
                    + '</div>'
                    + '<p class="node" style="text-align: left;margin-left:10px;">desc..</p>'
                    + '</div>'
                    + '<div class="node" style="width: 10%; height: 48px; border-left: 1px solid #E7E7E7;float: left;">'
                    + '<img class="basic-desc-list-img" src="images/action-black.png" style="margin-left:6px;" /></div>'
            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 2; i++) {
                    strListHtml += '<li>'
                    + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                    + 'background-repeat: no-repeat;background-position: center center;'
                    + '-o-background-size: contain;-moz-background-size: contain;-webkit-background-size: contain;'
                    + 'background-size: contain;">&nbsp;</div>'
                    + '</li>'
                    + '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                   + '<div class="basic-desc-list-item node" style="text-align: left;">'
                   + 'Title'
                   + '</div>'
                   + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                   + '</div>'
                + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 2; i++) {
                    strListHtml += '<li>'
                    + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                    + 'background-repeat: no-repeat;background-position: center center;'
                    + '-o-background-size: contain;-moz-background-size: contain;-webkit-background-size: contain;'
                    + 'background-size: contain;">&nbsp;</div>'
                    + '</li>'
                        + '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node hide">'
                       + '<img class="basic-desc-list-img" src="images/carat-r-black.png" style="margin-left:6px;" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_trans:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 2; i++) {
                    strListHtml += '<li>'
                    + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                    + 'background-repeat: no-repeat;background-position: center center;'
                    + '-o-background-size: contain;-moz-background-size: contain;-webkit-background-size: contain;'
                    + 'background-size: contain;">&nbsp;</div>'
                    + '</li>'
                        + '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node hide" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node">'
                       + '<img class="basic-desc-list-img" src="images/carat-r-black.png" style="margin-left:6px;" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_trans_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 2; i++) {
                    strListHtml += '<li>'
                    + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                    + 'background-repeat: no-repeat;background-position: center center;'
                    + '-o-background-size: contain;-moz-background-size: contain;-webkit-background-size: contain;'
                    + 'background-size: contain;">&nbsp;</div>'
                    + '</li>'
                       + '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node">'
                       + '<img class="basic-desc-list-img" src="images/carat-r-black.png" style="margin-left:6px;" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_actnbtn:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 2; i++) {
                    strListHtml += '<li>'
                    + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                    + 'background-repeat: no-repeat;background-position: center center;'
                    + '-o-background-size: contain;-moz-background-size: contain;-webkit-background-size: contain;'
                    + 'background-size: contain;">&nbsp;</div>'
                    + '</li>'
                       + '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node hide" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node" style="border-left: 1px solid #E7E7E7;">'
                       + '<img class="basic-desc-list-img" src="images/action-black.png" style="margin-left:6px;" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_actnbtn_with_count_bub:
                strListHtml += '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 2; i++) {
                    strListHtml += '<li>'
                    + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                    + 'background-repeat: no-repeat;background-position: center center;'
                    + '-o-background-size: contain;-moz-background-size: contain;-webkit-background-size: contain;'
                    + 'background-size: contain;">&nbsp;</div>'
                    + '</li>'
                       + '<li style="height: 56px;"><div class="basic-desc-list-text node">'
                       + '<div class="basic-desc-list-item node" style="text-align: left;">'
                       + 'Title'
                       + '<div class="basic-desc-list-count-bub basic-desc-count-bubble node" style="margin-top:9px;">'
                       + i + 3 + '</div>'
                       + '</div>'
                       + '<p class="node" style="text-align: left;margin-left:10px;">description</p>'
                       + '</div>'
                       + '<div class="basic-desc-list-btn node" style="border-left: 1px solid #E7E7E7;">'
                       + '<img class="basic-desc-list-img" src="images/action-black.png" style="margin-left:6px;" /></div>'
                    + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_with_caption:
                strListHtml = '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li>'
                            + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                            + 'background-repeat: no-repeat;background-position: center center;'
                            + '-o-background-size: contain;-moz-background-size: contain;'
                            + '-webkit-background-size: contain;background-size: contain;position:relative;">'
                            + '<div class="transbox node"><p class="node" style="text-align: left; font-size:12.5px;">description</p></div></div>'
                            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_with_caption_with_transition:
                strListHtml = '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li>'
                            + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                            + 'background-repeat: no-repeat;background-position: center center;'
                            + '-o-background-size: contain;-moz-background-size: contain;'
                            + '-webkit-background-size: contain;background-size: contain;position:relative;">'
                            + '<div class="transbox node">'
                            + '<p class="node" style="text-align: left; font-size:12.5px;float:left;">description</p>'
                            + '<p class="node" style="text-align: left; font-size:12.5px;float:right;margin-right:8px;"><img src="images/carat-r-white.png" /></p>'
                            + '</div></div>'
                            + '</li>';
                }
                strListHtml += '</ul>';
                break;
            case mFicientIde.MF_IDE_CONSTANTS.MF_LIST_CONTROL.Image_list_with_caption_with_actnbtn:
                strListHtml = '<ul class="basic-desc-list node" style="list-style: none;">';
                for (var i = 1; i <= 4; i++) {
                    strListHtml += '<li>'
                            + '<div class="node" style="width:100%;height: 56px; background-image: url(images/wp-architecture.png);'
                            + 'background-repeat: no-repeat;background-position: center center;'
                            + '-o-background-size: contain;-moz-background-size: contain;'
                            + '-webkit-background-size: contain;background-size: contain;position:relative;">'
                            + '<div class="transbox node">'
                            + '<p class="node" style="text-align: left; font-size:12.5px;float:left;">description</p>'
                            + '<p class="node" style="text-align: left; font-size:12.5px;float:right;margin-right:8px;"><img src="images/action-white.png" /></p>'
                            + '</div></div>'
                            + '</li>';
                }
                strListHtml += '</ul>';
                break;
        }
    }
    return strListHtml;
}
function getListWidth(appObj) {
    var width = '';
    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
    if (objForm) {
        if (!objForm.isTablet) {
            width = '210px';
        }
        else {
            var colPanel = objForm.fnGetColPanelOfControl(appObj.id);
            if (colPanel) {
                switch (colPanel.id.split('_')[2]) {
                    case '1':
                        width = '310px';
                        break;
                    case '2':
                        width = '120px';
                        break;
                    case '3':
                        if (colPanel.id.split('_')[3] == '1')
                            width = '210px';
                        else
                            width = '120px';
                        break;
                    case '4':
                        if (colPanel.id.split('_')[3] == '1')
                            width = '120px';
                        else
                            width = '210px';
                        break;
                    case '5':
                        width = '190px';
                        break;
                }
            }
        }
    }
    return width;
}

function getDateAndTime(type) {
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                              "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var d = new Date();
    var strDate = '';
    switch (type) {
        case "1":
            strDate = d.getDate();
            break;
        case "2":
            strDate = monthNames[d.getMonth()] + " " + d.getFullYear();
            break;
        case "3":
            var h = d.getHours(), m = d.getMinutes();
            var suffix = (h >= 12) ? 'PM' : 'AM';
            h = (h > 12) ? h - 12 : h;

            //if 00 then it is 12 am
            h = (h == '00') ? 12 : h;
            strDate = h + ':' + m + ' ' + suffix;
            break;
    }
    return strDate;
}

var mfIdeListControl = "mfIdeListControl";
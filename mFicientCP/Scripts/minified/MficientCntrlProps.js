(function ($) {
    $.fn.mficientCntrlProps = function (opts) {
        var CONSTANTS = { classes: { "innerPropertiesDiv": "InnerPropertieDiv", "propertyName": "propertyName", "propertyValue": "propertyValue", "txtProp": "txtProp", "propertyHeader": "propertyHeader", "propertyHeaderImage": "propertyHeaderImage", "propertyHeaderText": "propertyHeaderText", "propertyCollapse": "PropertyCollapse", "ddlProp": "ddlProp", "propDisable": "propDisable", "btnProp": "btnProp", "txtBarPropSmall": "txtBarPropSmall", "txtPropSmall": "txtPropSmall", "imgIconClass": "MenuIconHead", "groupWrapper": "propSheetCntrlGrpWrapper", "propSheetFocus": "propSheetFocus", "cntrlTextArea": "TextArea", "cntrlTextAreaLabel": "TextAreaLabel", "helpHeaderText": "HelpHeadDiv", "helpDescription": "HelpContentDiv"} }
        var defaults = { propertySheetJson: {}, editUIJson: "", editObject: null }; var options = $.extend(defaults, opts); var $self = $(this); if (options.propertySheetJson) {
            var objPropSheet = options.propertySheetJson; if (objPropSheet) {
                var _mfControlPrefix = objPropSheet.type.propPluginPrefix; $self.children().remove(); $self.html(_formHtml(objPropSheet, _mfControlPrefix)); if (options.editObject) { $self.data(MF_IDE_CONSTANTS.JqueryDataKeyForPropSheet, options.editObject); }
                else { $self.data(MF_IDE_CONSTANTS.JqueryDataKeyForPropSheet, _formObjectForData(objPropSheet.type, options.editUIJson)); }
                $self.data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper, { getControlPropertyId: _getPropControlPrefix, getControlWrapperId: _getWrapperDivPrefix, getBrowseBtnControlId: _getBrowseBtnPrefix, getGroupWrapperDivPrefix: _getGroupWrapperDivPrefix, getControlDtls: _getControlDtls, getAllWrapperDivs: _getAllWrapperControls }); if (options.editObject || options.editUIJson) { _initControls(objPropSheet, _mfControlPrefix, $self.data(MF_IDE_CONSTANTS.JqueryDataKeyForPropSheet), $self.data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper)); }
                else { _initControls(objPropSheet, _mfControlPrefix, options.editObject, $self.data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper)); }
                _bindEvents(objPropSheet, _mfControlPrefix); _bindEventsForVisualChangesOnCntrlFocus(objPropSheet);
            } 
        }
        return $self; function _formObjectForData(type, json) {
            var objForData = null; switch (type) {
                case MF_IDE_CONSTANTS.CONTROLS.SECTION: break; case MF_IDE_CONSTANTS.CONTROLS.LABEL: break; case MF_IDE_CONSTANTS.CONTROLS.TEXTBOX: break; case MF_IDE_CONSTANTS.CONTROLS.DROPDOWN: break; case MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON: break; case MF_IDE_CONSTANTS.CONTROLS.CHECKBOX: break; case MF_IDE_CONSTANTS.CONTROLS.IMAGE: break; case MF_IDE_CONSTANTS.CONTROLS.LIST: break; case MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST: break; case MF_IDE_CONSTANTS.CONTROLS.HIDDEN: break; case MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER: break; case MF_IDE_CONSTANTS.CONTROLS.TOGGLE: break; case MF_IDE_CONSTANTS.CONTROLS.SLIDER: break; case MF_IDE_CONSTANTS.CONTROLS.SEPARATOR: break; case MF_IDE_CONSTANTS.CONTROLS.BARCODE: break; case MF_IDE_CONSTANTS.CONTROLS.LOCATION: break; case MF_IDE_CONSTANTS.CONTROLS.PIECHART: break; case MF_IDE_CONSTANTS.CONTROLS.BARGRAPH: break; case MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH: break; case MF_IDE_CONSTANTS.CONTROLS.TABLE: break; case MF_IDE_CONSTANTS.OBJECTS_WITH_PROPERTY.APPS: objForData = new Apps(); if (json) { objForData.fillProperties(json); }
                    break; case MF_IDE_CONSTANTS.OBJECTS_WITH_PROPERTY.FORM: if (json) { form.fillProperties(json); }
                    break;
            }
            return objForData;
        }
        function _formHtml(objPropSheet, mfControlPrefix) {
            var strHtml = ""; if (objPropSheet) { if (objPropSheet.groups && $.isArray(objPropSheet.groups) && objPropSheet.groups.length > 0) { var objPropGroups = objPropSheet.groups; var strGroupName = ""; var objGroup = {}; for (var i = 0; i <= objPropGroups.length - 1; i++) { strGroupName = objPropGroups[i].name; objGroup = objPropGroups[i]; strHtml += _getDivStartTag(_getGroupWrapperDivPrefix(mfControlPrefix, objGroup.prefixText), CONSTANTS.classes.groupWrapper); strHtml += _getHtmlOfPropTextAndControlFromPropObj(objGroup, mfControlPrefix, strGroupName); strHtml += _getDivEndTag(); } } }
            return strHtml;
        }
        function _initControls(objPropSheet, mfControlPrefix, editObject, pluginPublicHelper) { if (objPropSheet) { if (objPropSheet.groups && $.isArray(objPropSheet.groups) && objPropSheet.groups.length > 0) { var objPropGroups = objPropSheet.groups; var strGroupName = ""; var objGroup = {}; for (var i = 0; i <= objPropGroups.length - 1; i++) { strGroupPrefixText = objPropGroups[i].prefixText; objGroup = objPropGroups[i]; _initControlsByGroup(objGroup, mfControlPrefix, strGroupPrefixText, editObject, pluginPublicHelper); } } } }
        function _bindEvents(objPropSheet, mfControlPrefix) { if (objPropSheet) { if (objPropSheet.groups && $.isArray(objPropSheet.groups) && objPropSheet.groups.length > 0) { var objPropGroups = objPropSheet.groups; var strGroupName = ""; var objGroup = {}; for (var i = 0; i <= objPropGroups.length - 1; i++) { strGroupPrefixText = objPropGroups[i].prefixText; objGroup = objPropGroups[i]; _bindEventsForControlByGroup(objGroup, mfControlPrefix, strGroupPrefixText); } } } }
        function _bindEventsForVisualChangesOnCntrlFocus(objPropSheet) {
            if (objPropSheet) {
                var $wrapperDivs = $self.find("." + CONSTANTS.classes.innerPropertiesDiv); $wrapperDivs.on('click', function (evnt) {
                    var $otherPropSheetsWrapperDivs = $([]), aryOtherPropSheetVisible = objPropSheet.otherPropSheetVisible, i = 0, objPropSheetCntrl = null, divData = null, strHelpTextDivId = "", $helpTextDiv = $([]); if (aryOtherPropSheetVisible && $.isArray(aryOtherPropSheetVisible) && aryOtherPropSheetVisible.length > 0) { for (i = 0; i <= aryOtherPropSheetVisible.length - 1; i++) { objPropSheetCntrl = aryOtherPropSheetVisible[i].propSheetCntrl; if (objPropSheetCntrl && $.isFunction(objPropSheetCntrl.AllWrapperDivs)) { $otherPropSheetsWrapperDivs = objPropSheetCntrl.AllWrapperDivs(); $otherPropSheetsWrapperDivs.removeClass(CONSTANTS.classes.propSheetFocus); } } }
                    $wrapperDivs.removeClass(CONSTANTS.classes.propSheetFocus); $(this).addClass(CONSTANTS.classes.propSheetFocus); divData = $(this).data(MF_IDE_CONSTANTS.jqueryCommonDataKey); if (divData) {
                        strHelpTextDivId = divData.helpTextDivId; $helpTextDiv = $('#' + strHelpTextDivId); if (divData.helpText && divData.helpText.header && divData.helpText.description) { $helpTextDiv.find('.' + CONSTANTS.classes.helpHeaderText).html(divData.helpText.header); $helpTextDiv.find('.' + CONSTANTS.classes.helpDescription).html(divData.helpText.description); }
                        else { $helpTextDiv.find('.' + CONSTANTS.classes.helpHeaderText).html(''); $helpTextDiv.find('.' + CONSTANTS.classes.helpDescription).html(''); } 
                    }
                    evnt.stopPropagation();
                });
            } 
        }
        function _initControlsByGroup(group, mfControlPrefix, groupPrefixText, editObject, pluginPublicHelper) {
            var blnIsEdit = false, mapFunctionToCall = null; var objControlsToPassForEditInit = null; if (editObject) { blnIsEdit = true; objControlsToPassForEditInit = {}; }
            for (var i = 0; i <= group.properties.length - 1; i++) {
                var property = group.properties[i]; var propPrefixText = property.prefixText; var $control = $self.find("#" + _getPropControlPrefix(mfControlPrefix, groupPrefixText, propPrefixText)); var $divWrapperParent = $self.find("#" + _getWrapperDivPrefix(mfControlPrefix, groupPrefixText, propPrefixText)); var objCntrlJqueryObjects = _getControlDtls(mfControlPrefix, groupPrefixText, propPrefixText); $divWrapperParent.data(MF_IDE_CONSTANTS.jqueryCommonDataKey, { helpTextDivId: group.helpTextDivId, helpText: property.helpText }); var $browseBtn = $(); if (property.init && $.isPlainObject(property.init)) {
                    var objInit = property.init; switch (property.control) {
                        case MF_PROPS_CONSTANTS.CONTROL_TYPE.label: break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox: $control.attr('maxlength', objInit.maxlength); $control.prop("disabled", objInit.disabled); break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea: break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown: $control.prop("disabled", objInit.disabled); switch (objInit.bindOptionsType) {
                                case MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual: $control.children().remove(); var strOptions = ""; if (property.options && property.options.length > 0) { for (var j = 0; j <= property.options.length - 1; j++) { strOptions += "<option value= \"" + property.options[j].value + "\">"; strOptions += property.options[j].text + "</option>"; } }
                                    $control.html(strOptions); break; case MF_PROPS_CONSTANTS.DDL_BIND_TYPE: break; case MF_PROPS_CONSTANTS.DDL_BIND_TYPE.webservice: break; case MF_PROPS_CONSTANTS.DDL_BIND_TYPE.odata: break;
                            }
                            break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.browse: $browseBtn = $self.find("#" + _getBrowseBtnPrefix(mfControlPrefix, groupPrefixText, propPrefixText)); $control.val(objInit.defltValue); $browseBtn.prop("disabled", objInit.disabled); break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.browseOfIcon: $browseBtn = $self.find("#" + _getBrowseBtnPrefix(mfControlPrefix, groupPrefixText, propPrefixText)); var $icon = $browseBtn.parent().find('img[class="' + CONSTANTS.classes.imgIconClass + '\"]'); $browseBtn.prop("disabled", objInit.disabled); $icon.attr("src", objInit.iconSrc); $control = $icon; break;
                    }
                    if (objInit.hidden) { $divWrapperParent.hide(); }
                    else { $divWrapperParent.show(); }
                    if (blnIsEdit) { mapFunctionToCall = editObject.propSheetHtmlMapping.groups[groupPrefixText] && editObject.propSheetHtmlMapping.groups[groupPrefixText][propPrefixText]; if (mapFunctionToCall && $.isFunction(mapFunctionToCall)) { mapFunctionToCall($control, editObject, { cntrlHtmlObjects: objCntrlJqueryObjects }); } }
                    else { if (objInit.defltValue) { $control.val(objInit.defltValue); } }
                    if (objInit.finalEditInit && objInit.finalEditInit.passForFinalEdit === true) { objControlsToPassForEditInit[objInit.finalEditInit.propName] = { wrapDiv: $divWrapperParent, control: $control, browseBtn: $browseBtn }; } 
                } 
            }
            if (blnIsEdit) {
                var otherInitOnEdit = editObject.propSheetHtmlMapping.groups[groupPrefixText] && editObject.propSheetHtmlMapping.groups[groupPrefixText]["OtherInitOnEdit"]
                if (otherInitOnEdit && $.isFunction(otherInitOnEdit)) { otherInitOnEdit(editObject, objControlsToPassForEditInit); } 
            } 
        }
        function _bindEventsForControlByGroup(group, mfControlPrefix, groupPrefixText) {
            for (var i = 0; i <= group.properties.length - 1; i++) {
                var property = group.properties[i]; var $control = $self.find("#" + _getPropControlPrefix(mfControlPrefix, groupPrefixText, property.prefixText)); var objCntrlJqueryObjects = _getControlDtls(mfControlPrefix, groupPrefixText, property.prefixText); switch (property.control) { case MF_PROPS_CONSTANTS.CONTROL_TYPE.label: break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox: break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea: break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown: break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.browse: $control = $self.find("#" + _getBrowseBtnPrefix(mfControlPrefix, groupPrefixText, property.prefixText)); break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.browseOfIcon: $control = $self.find("#" + _getBrowseBtnPrefix(mfControlPrefix, groupPrefixText, property.prefixText)); break; }
                if (property.events && $.isArray(property.events) && property.events.length > 0) {
                    for (var j = 0; j <= property.events.length - 1; j++) {
                        var evnt = property.events[j]; var objEvntData = {}; if (evnt.arguments) { objEvntData = processGetArgumentObjectForFunc(evnt.arguments, mfControlPrefix); }
                        objEvntData.propObject = $self.data(MF_IDE_CONSTANTS.JqueryDataKeyForPropSheet); if ($.isFunction(evnt.func)) { $control.on(evnt.name, objEvntData, evnt.func); } 
                    } 
                }
                switch (property.control) {
                    case MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea: var objJqueryObjects = objCntrlJqueryObjects; objJqueryObjects.textAreaLabel.on('click', function (evnt) { objJqueryObjects.control.show(); objJqueryObjects.textAreaLabel.hide(); objJqueryObjects.control.parent().show("slow"); objJqueryObjects.control.parents('.' + CONSTANTS.classes.propertyValue).addClass('w_100'); objJqueryObjects.wrapperDiv.addClass('propSheetFocus'); evnt.stopPropagation(); }); objJqueryObjects.wrapperDiv.on('click', function (evnt) {
                        if ($(evnt.target).attr('id') !== objJqueryObjects.control.attr('id') && objJqueryObjects.control.is(':visible')) {
                            objJqueryObjects.control.parent().hide("slow", function () {
                                var strCntrlValue = ""; strCntrlValue = objJqueryObjects.control.val(); if (strCntrlValue && strCntrlValue.length > 0) { MF_HELPERS.setTextOfTextAreaLabelOfPropSheet(strCntrlValue, objJqueryObjects.textAreaLabel); }
                                objJqueryObjects.textAreaLabel.show(); objJqueryObjects.control.parents('.' + CONSTANTS.classes.propertyValue).removeClass('w_100');
                            });
                        } 
                    }); break;
                } 
            } 
        }
        function _getHtmlOfPropTextAndControlFromPropObj(group, mfControlPrefix, groupName) {
            var strHtml = _getGroupNameWrapperHtml(groupName, group.hidden); strHtml += _getDivStartTag("", CONSTANTS.classes.PropertyCollapse); for (var i = 0; i <= group.properties.length - 1; i++) { strHtml += _getCollapsiblePropWrapperHtml(group.properties[i], mfControlPrefix, group.prefixText); }
            strHtml += _getDivEndTag(); strHtml += _getDivEndTag(); return strHtml;
        }
        function _getGroupNameWrapperHtml(groupName, isHidden) {
            var strHtml = ""; if (groupName && !isHidden) { strHtml += _getDivStartTag("", CONSTANTS.classes.propertyHeader); strHtml += "<span class =\"" + CONSTANTS.classes.propertyCollapse + " " + CONSTANTS.classes.propertyHeaderImage + "\"></span>"; strHtml += _getDivStartTag("", CONSTANTS.classes.propertyHeaderText); strHtml += groupName; strHtml += _getDivEndTag(); strHtml += _getDivEndTag(); }
            return strHtml;
        }
        function _getCollapsiblePropWrapperHtml(property, mfControlPrefix, groupNamePrefix) {
            var strHtml = ""; if (property) { strHtml += _getCompleteInnerPropWrapperDiv(property, mfControlPrefix, groupNamePrefix); }
            return strHtml;
        }
        function _getCompleteInnerPropWrapperDiv(property, mfControlPrefix, groupNamePrefix) { var strHtml = ""; strHtml += _getDivStartTag(_getWrapperDivPrefix(mfControlPrefix, groupNamePrefix, property.prefixText), CONSTANTS.classes.innerPropertiesDiv); strHtml += _getPropertyNameCompleteHtml(property.text); strHtml += _getControlHtml(property, mfControlPrefix, groupNamePrefix); strHtml += _getDivEndTag(); return strHtml; }
        function _getPropertyNameCompleteHtml(propName) { return _getDivStartTag("", CONSTANTS.classes.propertyName) + '<span  style="margin-left: 20px; width: 80%;">' + propName + '</span>' + _getDivEndTag(); }
        function _getControlHtml(property, mfControlPrefix, groupNamePrefix) {
            var strHtml = _getDivStartTag("", CONSTANTS.classes.propertyValue); if (property) { switch (property.control) { case MF_PROPS_CONSTANTS.CONTROL_TYPE.label: strHtml += "<div><input id=\"" + _getPropControlPrefix(mfControlPrefix, groupNamePrefix, property.prefixText); strHtml += "\" "; strHtml += "type=\"text\" value=\"\" class=\"" + CONSTANTS.classes.txtProp + "\" maxlength=\"50\" readonly=\"readonly\"/></div>"; break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox: strHtml += "<div><input id=\"" + _getPropControlPrefix(mfControlPrefix, groupNamePrefix, property.prefixText); strHtml += "\" "; strHtml += "type=\"text\" value=\"\" class=\"" + CONSTANTS.classes.txtProp + "\" maxlength=\"" + property.init.maxlength + "\"/></div>"; break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea: strHtml += '<div><input id="' + _getTextAreaLabelPrefix(mfControlPrefix, groupNamePrefix, property.prefixText) + '"'; strHtml += ' type="text" value="" class="' + CONSTANTS.classes.txtProp + ' ' + CONSTANTS.classes.cntrlTextAreaLabel; strHtml += ' maxlength="50" readonly="readonly"/></div>'; strHtml += '<div class="fl hide w_100">'; strHtml += '<textarea id="' + _getPropControlPrefix(mfControlPrefix, groupNamePrefix, property.prefixText) + '"'; strHtml += ' value="" class="w_100 ' + CONSTANTS.classes.cntrlTextArea + ' " maxlength="' + property.init.maxlength; strHtml += '" "rows="3" cols="30"></textarea>'; strHtml += '</div>'; break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown: strHtml += "<div><select id=\"" + _getPropControlPrefix(mfControlPrefix, groupNamePrefix, property.prefixText); strHtml += "\" "; strHtml += "class=\"" + CONSTANTS.classes.ddlProp + "\"></select></div>"; break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.browse: strHtml += ""; strHtml = _getDivStartTag("", CONSTANTS.classes.propertyValue + " " + CONSTANTS.classes.propDisable); strHtml += "<div class=\"fl w_80\"><input id=\"" + _getPropControlPrefix(mfControlPrefix, groupNamePrefix, property.prefixText); strHtml += "\""; strHtml += "type=\"text\" maxlength=\"50\"  "; strHtml += "class=\"" + CONSTANTS.classes.txtPropSmall + " w_100" + "\" disabled=\"disabled\" readonly=\"readonly\" /></div>  "; strHtml += "<div class=\"fr w_20\"><input class=\"w_100\" id=\"" + _getBrowseBtnPrefix(mfControlPrefix, groupNamePrefix, property.prefixText) + "\" type=\"button\" value=\"...\" "; strHtml += "class=\"" + CONSTANTS.classes.btnProp + "\" /></div>"; break; case MF_PROPS_CONSTANTS.CONTROL_TYPE.browseOfIcon: strHtml += "<div class=\"fl \"><img alt=\"Icon\" id=\"" + _getPropControlPrefix(mfControlPrefix, groupNamePrefix, property.prefixText) + "\"" + "class=\"" + CONSTANTS.classes.imgIconClass + "\" src=\"\" style=\"margin: 2px;\"></div> "; strHtml += "<div><input  id=\"" + _getBrowseBtnPrefix(mfControlPrefix, groupNamePrefix, property.prefixText) + "\" type=\"button\" value=\"...\" "; strHtml += "class=\"" + CONSTANTS.classes.btnProp + "\" /></div>"; break; } }
            strHtml += _getDivEndTag(); return strHtml;
        }
        function _getInnerPropertyDivStartTag(id) { return _getDivStartTag(id, CONSTANTS.classes.innerPropertiesDiv); }
        function _getDivStartTag(id, cssClass) {
            if (id && cssClass) { return "<div id=\"" + id + "\" class=\"" + cssClass + "\">"; }
            else if (id && !cssClass) { return "<div id=\"" + id + "\">"; }
            else if (!id && cssClass) { return "<div class=\"" + cssClass + "\">"; }
            else if (!id && !cssClass) { return "<div>" } 
        }
        function _getDivEndTag() { return "</div>"; }
        function processGetArgumentObjectForFunc(eventArgsInfo, mfControlPrefix) {
            var objArgsObject = null; if (eventArgsInfo && $.isPlainObject(eventArgsInfo)) { objArgsObject = new Object(); $.each(eventArgsInfo, function (key, value) { switch (key) { case "cntrls": objArgsObject[key] = _getControlsToPassAsFuncArgs(value, mfControlPrefix); break; } }); }
            return objArgsObject;
        }
        function _getControlsToPassAsFuncArgs(controlDtls, mfControlPrefix) {
            var objCntrolDtls = null; if (controlDtls) { objCntrolDtls = new Object(); $.each(controlDtls, function (index, value) { var objControlHtmlInfo = new Object(); objControlHtmlInfo.controlId = _getPropControlPrefix(mfControlPrefix, value.grpPrefText, value.cntrlPrefText); objControlHtmlInfo.wrapperDiv = _getWrapperDivPrefix(mfControlPrefix, value.grpPrefText, value.cntrlPrefText); objControlHtmlInfo.browseBtn = _getBrowseBtnPrefix(mfControlPrefix, value.grpPrefText, value.cntrlPrefText); objCntrolDtls[value.rtrnPropNm] = objControlHtmlInfo; }); }
            return objCntrolDtls;
        }
        function _getPropControlPrefix(mfControlPrefix, groupPrefix, propName) { return mfControlPrefix + "_" + groupPrefix + "_" + propName + "_Control"; }
        function _getWrapperDivPrefix(mfControlPrefix, groupPrefix, propName) { return mfControlPrefix + "_" + groupPrefix + "_" + propName + "_WrapDiv"; }
        function _getBrowseBtnPrefix(mfControlPrefix, groupPrefix, propName) { return mfControlPrefix + "_" + groupPrefix + "_" + propName + "_Browse"; }
        function _getGroupWrapperDivPrefix(mfControlPrefix, groupPrefix) { return mfControlPrefix + "_" + groupPrefix; }
        function _getTextAreaLabelPrefix(mfControlPrefix, groupPrefix, propName) { return mfControlPrefix + "_" + groupPrefix + "_" + propName + "_TALabel"; }
        function _getControlDtls(mfControlPrefix, groupPrefix, propName) { var objControl = new PropertySheetControl(); objControl.wrapperDiv = $('#' + _getWrapperDivPrefix(mfControlPrefix, groupPrefix, propName)); objControl.control = $('#' + _getPropControlPrefix(mfControlPrefix, groupPrefix, propName)); objControl.browseBtn = $('#' + _getBrowseBtnPrefix(mfControlPrefix, groupPrefix, propName)); objControl.textAreaLabel = $('#' + _getTextAreaLabelPrefix(mfControlPrefix, groupPrefix, propName)); return objControl; }
        function _getAllWrapperControls() { return $self.find("." + CONSTANTS.classes.innerPropertiesDiv); } 
    };
})(jQuery); (function ($) {
    $.fn.mficientRefreshCntrlsOnChng = function (optns) {
        var CONSTANTS = { Controls: [], SelectedControls: [], arrCheckbox: [] }; var options = $.extend(optns); var $self = this; $self.html(_getRefreshControlsHtml()); $.each(CONSTANTS.arrCheckbox, function () {
            $('#' + this).unbind('click'); $('#' + this).bind('click', function () {
                if ($(this).is(":checked")) { options.SelectedControls.push(this.id.split('_')[0]); }
                else { options.SelectedControls.pop(this.id.split('_')[0]); } 
            });
        }); this.data("SelectedControls", options.SelectedControls); function _getRefreshControlsHtml() {
            var strHtml = ""; var DpOptionCount = 1; $('#OnChangeElmDiv').html(''); $.each(options.Controls, function () {
                strHtml += "<tr>"
+ "<td id=\"CtrlOnChangeText_" + DpOptionCount + "\"> "
+ this
+ "</td>"
+ "<td style=\"width: 30px; padding-left: 25px;\">"
+ "<input id=\"" + this + "_ChktrlOnChange_" + DpOptionCount + "\" type=\"checkbox\" " + ($.inArray(this.trim(), options.SelectedControls) > -1 ? " checked=\"checked\" " : "") + " />"
+ "</td>"
+ "</tr>"; CONSTANTS.arrCheckbox.push(this + "_ChktrlOnChange_" + DpOptionCount); DpOptionCount = DpOptionCount + 1;
            }); return strHtml;
        } 
    };
})(jQuery);
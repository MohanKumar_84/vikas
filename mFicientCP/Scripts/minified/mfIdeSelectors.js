﻿mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle = { groups: { ControlProperties: { Type: function (control, appObj) { $(control).val(appObj.type.UIName) }, Name: function (control, appObj) { $(control).val(appObj.userDefinedName) }, Description: function (control, appObj, options) {
    var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel; var strDescription = appObj.description; if (strDescription) {
        $(control).val(strDescription); if (strDescription.length > 25) { $textAreaLabel.val(strDescription.substring(0, 25) + '...'); }
        else { $textAreaLabel.val(strDescription); } 
    }
    else { $(control).val(''); $textAreaLabel.val(''); } 
}, eventHandlers: { NameChanged: function (evnt) {
    var evntData = evnt.data; var propObject = evnt.data.propObject; var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) { if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName; else evnt.target.value = ""; return; }
    propObject.fnRename(evnt.target.value);
}, DescChanged: function (evnt) { var evntData = evnt.data; var propObject = evnt.data.propObject; propObject.description = evnt.target.value; } 
}
}, Appearance: { BindValueToProperty: function (control, appObj) {
    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Data.getBindValueToPropertyCntrl(); if ($('[id$=hdfIsChildForm]').val() == 'true') {
        var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val()); var strOptionsHtml = '<option value="-1">Select Item</option>'; if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) { $.each(eltCntrl.properties, function () { strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>'; }); }
        $(control).html(strOptionsHtml); if (appObj.bindValueToProperty != undefined) $(control).val(appObj.bindValueToProperty); else $(control).val('-1'); $(currentCntrl.wrapperDiv).show();
    }
    else
        $(currentCntrl.wrapperDiv).hide();
}, BindTextToProperty: function (control, appObj) {
    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Data.getBindTextToPropertyCntrl(); if ($('[id$=hdfIsChildForm]').val() == 'true') {
        var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val()); var strOptionsHtml = '<option value="-1">Select Item</option>'; if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) { $.each(eltCntrl.properties, function () { strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>'; }); }
        $(control).html(strOptionsHtml); if (appObj.bindTextToProperty != undefined) $(control).val(appObj.bindTextToProperty); else $(control).val('-1'); $(currentCntrl.wrapperDiv).show();
    }
    else
        $(currentCntrl.wrapperDiv).hide();
}, LabelText: function (control, appObj) {
    $(control).val(appObj.labelText); if (appObj.labelText != undefined && appObj.labelText != null && appObj.labelText.length > 0)
        $('#' + appObj.id + '_Label').html(appObj.labelText); else
        $('#' + appObj.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");
}, SwitchText: function (control, appObj) { $(control).val(appObj.switchText) }, DefaultState: function (control, appObj) { $(control).val(appObj.defaultState) }, Disabled: function (control, appObj) { if (appObj.disabled) { $(control).val(appObj.disabled); } }, eventHandlers: { BindValueToProperty: function (evnt) {
    var evntData = evnt.data; var cntrls = evntData.cntrls; var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Data.getBindValueToPropertyCntrl(); var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); if (objform.fnAddBindProperty(evnt.target.value, propObject.bindValueToProperty))
        propObject.bindValueToProperty = evnt.target.value; else { if (propObject.bindValueToProperty != undefined) evnt.target.value = propObject.bindValueToProperty; else evnt.target.value = '-1'; } 
}, BindTextToProperty: function (evnt) {
    var evntData = evnt.data; var cntrls = evntData.cntrls; var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Data.getBindTextToPropertyCntrl(); var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); if (objform.fnAddBindProperty(evnt.target.value, propObject.bindTextToProperty))
        propObject.bindTextToProperty = evnt.target.value; else { if (propObject.bindTextToProperty != undefined) evnt.target.value = propObject.bindTextToProperty; else evnt.target.value = '-1'; } 
}, LabelTextChange: function (evnt) {
    var propObject = evnt.data.propObject; propObject.labelText = evnt.target.value; if (evnt.target.value.length > 0)
        $('#' + propObject.id + '_Label').html(evnt.target.value); else
        $('#' + propObject.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");
}, SwitchText: function (evnt) { var propObject = evnt.data.propObject; propObject.switchText = evnt.target.value; }, ToggleDefaultState: function (evnt) { var propObject = evnt.data.propObject; propObject.defaultState = evnt.target.value; }, Disabled: function (evnt) { var propObject = evnt.data.propObject; propObject.disabled = evnt.target.value; } 
}
}, Data: { Databinding: function (control, appObj) { $(control).val(appObj.bindType.id) }, DataObject: function (control, appObj) {
    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Data.getDataObjectCntrl(); if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
        $(currentCntrl.wrapperDiv).show(); else
        $(currentCntrl.wrapperDiv).hide(); if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) { $.each(appObj.databindObjs, function () { $(currentCntrl.control).val(this['name']); switch (this['bindObjType']) { case "1": mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control); break; case "2": mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control); break; case "5": mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control); break; } }); } 
}, eventHandlers: { DataObject: function (evnt) {
    var evntData = evnt.data; var cntrls = evntData.cntrls; var propObject = evnt.data.propObject; var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv); var $DataObjectControl = $('#' + cntrls.DataObject.controlId); var dataObject = new DatabindingObj(); var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None; if (evnt.target.value == "0") { $ViewProperty.hide(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None; dataObject.bindObjType = bindType.id; }
    if (evnt.target.value == "1") { $ViewProperty.show(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db; dataObject.bindObjType = bindType.id; }
    else if (evnt.target.value == "2") { $ViewProperty.show(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl; dataObject.bindObjType = bindType.id; }
    else if (evnt.target.value == "5") { $ViewProperty.show(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata; dataObject.bindObjType = bindType.id; }
    else if (evnt.target.value == "6") { $ViewProperty.show(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt; dataObject.bindObjType = bindType.id; }
    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) { $.each(propObject.databindObjs, function () { if (this.bindObjType != bindType.id) { this.id = ""; this.name = ""; this.cmdParams = []; propObject.displayText = ""; this.bindObjType = bindType.id; $($DataObjectControl).val("Select Object"); } }); }
    else { var dataObject = new DatabindingObj("", [], "0", bindType.id, "", ""); propObject.fnAddDatabindObj(dataObject); $($DataObjectControl).val("Select Object"); }
    propObject.bindType = bindType;
}, DataBinding: function (evnt) {
    var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Data.getDataObjectCntrl(); var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(), intlJson = [], isEdit = false; if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
        $.each(propObject.databindObjs, function () {
            var objDatabindObj = jQuery.extend(true, {}, this); $(currentCntrl.control).val(this['name']); switch (this['bindObjType']) {
                case "1": mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control); showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false); break; case "2": mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control); showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false); break; case "5": mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control); showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false); break; case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt.id: intlJson = MF_HELPERS.intlsenseHelper.getControlDatabindingIntellisenseJson(objCurrentForm, propObject.id, true); if (objDatabindObj != null && (objDatabindObj.id && objDatabindObj.id !== "-1")) { isEdit = true; }
                    mFicientIde.MF_DATA_BINDING.processDataBindingByObjectType({ controlType: MF_IDE_CONSTANTS.CONTROLS.TOGGLE, databindObject: objDatabindObj, objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt, isEdit: isEdit, intlsJson: intlJson, control: propObject }); showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 550, false); break;
            } 
        });
    } 
} 
}
}, Behaviour: { Update: function (control, appObj) { if ((appObj.refreshControls != undefined || appObj.refreshControls != null) && appObj.refreshControls.length > 0) $(control).val(appObj.refreshControls.length + " Control(s)"); else $(control).val("0 Control(s)"); }, Display: function (control, appObj) { $(control).val(appObj.conditionalDisplay) }, Conditions: function (control, appObj) { var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Behaviour.getConditionsCntrl(); if (appObj.conditionalDisplay == "0") $(currentCntrl.wrapperDiv).show(); else $(currentCntrl.wrapperDiv).hide(); }, eventHandlers: { RefreshControls: function (evnt) {
    var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle.propSheetCntrl.Behaviour.getRefreshControls(); var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); var controls = []; if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) { $.each(form.rowPanels, function (rowIndex, rowPanel) { if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) { $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) { if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) { $.each(columnPanel.controls, function (cntrlIndex, control) { if (control.id != propObject.id && !control.isDeleted && control.type != mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SEPARATOR) { controls.push(control.userDefinedName); } }); } }); } }); }
    if (controls.length == 0) { showMessage('Please add controls first.', DialogType.Error); return; }
    else { $('#OnChangeElmDiv').mficientRefreshCntrlsOnChng({ Controls: controls, SelectedControls: (propObject.refreshControls != undefined ? propObject.refreshControls : []) }); showModalPopUpWithOutHeader('SubProcOnChange', 'Controls', 280, false); $('#OnChangeElmDiv').data("ControlId", propObject.id); }
    $('[id$=btnSaveRefreshControls]').unbind('click'); $('[id$=btnSaveRefreshControls]').bind('click', function () { propObject.refreshControls = $('#OnChangeElmDiv').data("SelectedControls"); $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)"); $('#SubProcOnChange').dialog('close'); return false; }); $('[id$=btnCancelRefreshControl]').unbind('click'); $('[id$=btnCancelRefreshControl]').bind('click', function () { $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)"); $('#SubProcOnChange').dialog('close'); return false; });
}, Display: function (evnt) {
    var evntData = evnt.data; var cntrls = evntData.cntrls; var propObject = evnt.data.propObject; propObject.conditionalDisplay = evnt.target.value; var $Conditions = $('#' + cntrls.Conditions.wrapperDiv); if (evnt.target.value == "0") { $Conditions.show(); }
    else { $Conditions.hide(); } 
}, ConditionalDisplay: function (evnt) {
    var propObject = evnt.data.propObject; var json = "["; var controlJson = ""; var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); var controls = []; var arrIntellisenseControls = []; var objIntellisenseCntrl = {}; if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) { $.each(form.rowPanels, function (rowIndex, rowPanel) { if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) { $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) { if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) { $.each(columnPanel.controls, function (cntrlIndex, control) { objIntellisenseCntrl = {}; if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) { controls.push(control); objIntellisenseCntrl["cntrlnm"] = control.userDefinedName; objIntellisenseCntrl["type"] = control.type; arrIntellisenseControls.push(objIntellisenseCntrl); } }); } }); } }); }
    if ((controls != null || controls != undefined) && controls.length > 0) {
        $.each(controls, function (index, value) {
            if (controlJson.length > 0)
                controlJson += ","; controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
        });
    }
    json += controlJson + "]"; $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 }); showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false); $('#divConditionalControl').data("ControlId", propObject.id); $('[id$=btnCondSave]').unbind('click'); $('[id$=btnCondSave]').bind('click', function () { propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")(); $('#SubProcConditionalDisplay').dialog('close'); return false; }); $('[id$=btnCondCancel]').unbind('click'); $('[id$=btnCondCancel]').bind('click', function () { $('#SubProcConditionalDisplay').dialog('close'); return false; });
} 
}
}
}, propSheetCntrl: (function () {
    function _getDivControlHelperData() { return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper); }
    function _getRefreshControlsCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.TOGGLE.propPluginPrefix, "Behaviour", "Update"); }
        return objControl;
    }
    function _getBindValueToPropertyCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.TOGGLE.propPluginPrefix, "Data", "BindValueToProperty"); }
        return objControl;
    }
    function _getBindTextToPropertyCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.TOGGLE.propPluginPrefix, "Data", "BindTextToProperty"); }
        return objControl;
    }
    function _getDataObjectCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.TOGGLE.propPluginPrefix, "Data", "DataObject"); }
        return objControl;
    }
    function _getConditionsCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.TOGGLE.propPluginPrefix, "Behaviour", "Conditions"); }
        return objControl;
    }
    return { Data: { getBindValueToPropertyCntrl: _getBindValueToPropertyCntrl, getBindTextToPropertyCntrl: _getBindTextToPropertyCntrl, getDataObjectCntrl: _getDataObjectCntrl }, Behaviour: { getRefreshControls: _getRefreshControlsCntrl, getConditionsCntrl: _getConditionsCntrl} };
})()
}; mFicientIde.PropertySheetJson.Toggle = { "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE, "groups": [{ "name": "Control Properties", "prefixText": "ControlProperties", "hidden": true, "properties": [{ "text": "Type", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "Type", "noOfLines": "0", "validations": [], "customValidations": [], "events": [], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 50, "disabled": true, "defltValue": "ToggleSwitch", "hidden": false} }, { "text": "Name", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "Name", "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Toggle.groups.ControlProperties.eventHandlers.NameChanged, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 50, "disabled": false, "defltValue": "", "hidden": false} }, { "text": "Description", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea, "prefixText": "Description", "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Toggle.groups.ControlProperties.eventHandlers.DescChanged, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 250, "disabled": false, "defltValue": "", "hidden": false}}] }, { "name": "Appearance", "prefixText": "Appearance", "hidden": false, "properties": [{ "text": "Bind Value To Property", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "BindValueToProperty", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.BindValueToProperty, "context": "", "arguments": { "cntrls": []}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": true }, "options": [] }, { "text": "Bind Text To Property", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "BindTextToProperty", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.BindTextToProperty, "context": "", "arguments": { "cntrls": []}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": true }, "options": [] }, { "text": "Label Text", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "LabelText", "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.LabelTextChange, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "Label Text", "hidden": false} }, { "text": "Switch Text", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "SwitchText", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.ToggleText, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "ON/OFF", "value": "0" }, { "text": "YES/NO", "value": "1" }, { "text": "TRUE/FALSE", "value": "2"}] }, { "text": "Default State", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "DefaultState", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.ToggleDefaultState, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "ON", "value": "ON" }, { "text": "OFF", "value": "OFF"}] }, { "text": "Disabled", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "Disabled", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Toggle.groups.Appearance.eventHandlers.Disabled, "context": "", "arguments": { "cntrls": []}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "No", "value": "0" }, { "text": "Yes", "value": "1"}]}] }, { "name": "Data", "prefixText": "Data", "hidden": false, "properties": [{ "text": "Databinding", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "Databinding", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Toggle.groups.Data.eventHandlers.DataObject, "context": "", "arguments": { "cntrls": [{ grpPrefText: "Data", cntrlPrefText: "DataObject", rtrnPropNm: "DataObject"}]}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "None", "value": "0" }, { "text": "Database Object", "value": "1" }, { "text": "Web Service Object", "value": "2" }, { "text": "OData Object", "value": "5" }, { "text": "Offline Database", "value": "6"}] }, { "text": "Data Object", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse, "prefixText": "DataObject", "disabled": false, "hidden": true, "noOfLines": "0", "validations": [], "customValidations": [{ "func": "functionName", "context": "context"}], "events": [{ "name": "click", "func": PROP_JSON_HTML_MAP.Toggle.groups.Data.eventHandlers.DataBinding, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "Select Object", "hidden": true}}] }, { "name": "Behaviour", "prefixText": "Behaviour", "hidden": false, "properties": [{ "text": "Update", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse, "prefixText": "Update", "disabled": false, "hidden": true, "noOfLines": "0", "validations": [], "customValidations": [{ "func": "functionName", "context": "context"}], "events": [{ "name": "click", "func": PROP_JSON_HTML_MAP.Toggle.groups.Behaviour.eventHandlers.RefreshControls, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "None", "hidden": false} }, { "text": "Display", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "Display", "defltValue": "1", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Toggle.groups.Behaviour.eventHandlers.Display, "context": "", "arguments": { "cntrls": [{ grpPrefText: "Behaviour", cntrlPrefText: "Conditions", rtrnPropNm: "Conditions"}]}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "Always", "value": "1" }, { "text": "Conditional", "value": "0"}] }, { "text": "Conditions", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse, "prefixText": "Conditions", "disabled": false, "noOfLines": "0", "validations": [], "customValidations": [{ "func": "functionName", "context": "context"}], "events": [{ "name": "click", "func": PROP_JSON_HTML_MAP.Toggle.groups.Behaviour.eventHandlers.ConditionalDisplay, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "Select Conditions", "hidden": true}}]}] }; function Toggle(opts) { if (!opts) opts = {}; this.id = opts.id; this.userDefinedName = opts.userDefinedName; this.description = opts.description; this.type = opts.type; this.isDeleted = opts.isDeleted; this.oldName = opts.oldName; this.labelText = opts.labelText; this.switchText = opts.switchText; this.displayText = opts.displayText; this.defaultState = opts.defaultState; this.refreshControls = opts.refreshControls; this.bindValueToProperty = opts.bindValueToProperty; this.bindTextToProperty = opts.bindTextToProperty; this.bindToProperty = opts.bindToProperty; this.bindType = opts.bindType; this.databindObjs = opts.databindObjs; this.conditionalDisplay = opts.conditionalDisplay; this.condDisplayCntrlProps = opts.condDisplayCntrlProps; this.disabled = opts.disabled; }
Toggle.prototype = new ControlNew(); Toggle.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Toggle; Toggle.prototype.fnRename = function (name) {
    if (!this.isNew && !this.oldName && this.userDefinedName !== name) { this.oldName = this.userDefinedName; this.userDefinedName = name; }
    else if (this.userDefinedName != name) { this.userDefinedName = name; } 
}; Toggle.prototype.fnAddDatabindObjs = function (databindObjs) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) { this.databindObjs = []; }
    if ($.isArray(databindObjs)) { var self = this; $.each(databindObjs, function (index, value) { if (value && value instanceof DatabindingObj) { self.databindObjs.push(value); } }); } 
}; Toggle.prototype.fnAddDatabindObj = function (databindObj) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) { this.databindObjs = []; }
    if (databindObj && databindObj instanceof DatabindingObj) { this.databindObjs.push(databindObj); } 
}
Toggle.prototype.fnDeleteDatabindObj = function (name) { var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) { if (databindObj instanceof DatabindingObj) { return databindObj.name !== name; } }); this.databindObjs = aryNewDatabindObjs; }
Toggle.prototype.fnResetObjectDetails = function () { var databindObjs = this.databindObjs; this.databindObjs = []; var i = 0; if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) { for (i = 0; i <= databindObjs.length - 1; i++) { var objdatabindObj = new DatabindingObj(databindObjs[i].id, databindObjs[i].cmdParams, databindObjs[i].index, databindObjs[i].bindObjType, databindObjs[i].name, databindObjs[i].dsName, databindObjs[i].tempUiIndex, databindObjs[i].usrName, databindObjs[i].pwd); objdatabindObj.fnResetObjectDetails(); this.fnAddDatabindObj(objdatabindObj); } } }
Toggle.prototype.fnClearAllDatabindObjs = function () { this.databindObjs = []; }
Toggle.prototype.fnGetDisplayText = function () { return this.displayText; }; Toggle.prototype.fnSetDisplayText = function (value) { this.displayText = value; }; Toggle.prototype.fnGetDatabindingObj = function () { return this.databindObjs && this.databindObjs[0]; }; mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox = { groups: { ControlProperties: { Type: function (control, appObj) { $(control).val(appObj.type.UIName) }, Name: function (control, appObj) { $(control).val(appObj.userDefinedName) }, Description: function (control, appObj, options) {
    var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel; var strDescription = appObj.description; if (strDescription) {
        $(control).val(strDescription); if (strDescription.length > 25) { $textAreaLabel.val(strDescription.substring(0, 25) + '...'); }
        else { $textAreaLabel.val(strDescription); } 
    }
    else { $(control).val(''); $textAreaLabel.val(''); } 
}, eventHandlers: { NameChanged: function (evnt) {
    var evntData = evnt.data; var propObject = evnt.data.propObject; var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) { if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName; else evnt.target.value = ""; return; }
    propObject.fnRename(evnt.target.value);
}, DescChanged: function (evnt) { var evntData = evnt.data; var propObject = evnt.data.propObject; propObject.description = evnt.target.value; } 
}
}, Appearance: { LabelText: function (control, appObj) {
    $(control).val(appObj.labelText); if (appObj.labelText != undefined && appObj.labelText != null && appObj.labelText.length > 0)
        $('#' + appObj.id + '_Label').html(appObj.labelText); else
        $('#' + appObj.id + '_Label').html("Option1");
}, Alignment: function (control, appObj) {
    $(control).val(appObj.alignment); if (appObj.alignment == "0") { $('#' + appObj.id + '_align').addClass("IdeUi-btn-icon-left"); if ($('#' + appObj.id + '_span_Left').hasClass("hide")) $('#' + appObj.id + '_span_Left').removeClass("hide"); $('#' + appObj.id + '_span_Right').addClass("hide"); }
    else { if ($('#' + appObj.id + '_align').hasClass("IdeUi-btn-icon-left")) $('#' + appObj.id + '_align').removeClass("IdeUi-btn-icon-left"); if ($('#' + appObj.id + '_span_Right').hasClass("hide")) $('#' + appObj.id + '_span_Right').removeClass("hide"); $('#' + appObj.id + '_span_Right').css('margin-left', '6px'); $('#' + appObj.id + '_span_Right').css('margin-top', '-8px'); $('#' + appObj.id + '_span_Left').addClass("hide"); } 
}, Disabled: function (control, appObj) { if (appObj.disabled) { $(control).val(appObj.disabled); } }, eventHandlers: { LabelTextChange: function (evnt) {
    var propObject = evnt.data.propObject; propObject.labelText = evnt.target.value; if (evnt.target.value.length > 0)
        $('#' + propObject.id + '_Label').html(evnt.target.value); else
        $('#' + propObject.id + '_Label').html("Option1");
}, Alignment: function (evnt) {
    var propObject = evnt.data.propObject; propObject.alignment = evnt.target.value; if (evnt.target.value == "0") { $('#' + propObject.id + '_align').addClass("IdeUi-btn-icon-left"); if ($('#' + propObject.id + '_span_Left').hasClass("hide")) $('#' + propObject.id + '_span_Left').removeClass("hide"); $('#' + propObject.id + '_span_Right').addClass("hide"); }
    else { if ($('#' + propObject.id + '_align').hasClass("IdeUi-btn-icon-left")) $('#' + propObject.id + '_align').removeClass("IdeUi-btn-icon-left"); if ($('#' + propObject.id + '_span_Right').hasClass("hide")) $('#' + propObject.id + '_span_Right').removeClass("hide"); $('#' + propObject.id + '_span_Right').css('margin-left', '6px'); $('#' + propObject.id + '_span_Right').css('margin-top', '-8px'); $('#' + propObject.id + '_span_Left').addClass("hide"); } 
}, Disabled: function (evnt) { var propObject = evnt.data.propObject; propObject.disabled = evnt.target.value; } 
}
}, Data: { BindToProperty: function (control, appObj) {
    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox.propSheetCntrl.Data.getBindToPropertyCntrl(); if ($('[id$=hdfIsChildForm]').val() == 'true') {
        var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val()); var strOptionsHtml = '<option value="-1">Select Item</option>'; if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) { $.each(eltCntrl.properties, function () { strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>'; }); }
        $(control).html(strOptionsHtml); if (appObj.bindToProperty != undefined) $(control).val(appObj.bindToProperty); else $(control).val('-1'); $(currentCntrl.wrapperDiv).show();
    }
    else
        $(currentCntrl.wrapperDiv).hide();
}, Databinding: function (control, appObj) { $(control).val(appObj.bindType.id) }, DataObject: function (control, appObj) {
    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox.propSheetCntrl.Data.getDataObjectCntrl(); if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
        $(currentCntrl.wrapperDiv).show(); else
        $(currentCntrl.wrapperDiv).hide(); if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) { $.each(appObj.databindObjs, function () { $(currentCntrl.control).val(this['name']); switch (this['bindObjType']) { case "1": mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control); break; case "2": mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control); break; case "5": mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control); break; } }); } 
}, eventHandlers: { BindToProperty: function (evnt) {
    var evntData = evnt.data; var cntrls = evntData.cntrls; var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox.propSheetCntrl.Data.getBindToPropertyCntrl(); var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); if (objform.fnAddBindProperty(evnt.target.value, propObject.bindToProperty))
        propObject.bindToProperty = evnt.target.value; else { if (propObject.bindToProperty != undefined) evnt.target.value = propObject.bindToProperty; else evnt.target.value = '-1'; } 
}, DataObject: function (evnt) {
    var evntData = evnt.data; var cntrls = evntData.cntrls; var propObject = evnt.data.propObject; var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv); var $DataObjectControl = $('#' + cntrls.DataObject.controlId); var dataObject = new DatabindingObj(); var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None; if (evnt.target.value == "0") { $ViewProperty.hide(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None; dataObject.bindObjType = bindType.id; }
    if (evnt.target.value == "1") { $ViewProperty.show(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db; dataObject.bindObjType = bindType.id; }
    else if (evnt.target.value == "2") { $ViewProperty.show(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl; dataObject.bindObjType = bindType.id; }
    else if (evnt.target.value == "5") { $ViewProperty.show(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata; dataObject.bindObjType = bindType.id; }
    else if (evnt.target.value == "6") { $ViewProperty.show(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt; dataObject.bindObjType = bindType.id; }
    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) { $.each(propObject.databindObjs, function () { if (this.bindObjType != bindType.id) { this.id = ""; this.name = ""; this.cmdParams = []; propObject.checkedVal = ""; this.bindObjType = bindType.id; $($DataObjectControl).val("Select Object"); } }); }
    else { var dataObject = new DatabindingObj("", [], "0", bindType.id, "", ""); propObject.fnAddDatabindObj(dataObject); $($DataObjectControl).val("Select Object"); }
    propObject.bindType = bindType;
}, DataBinding: function (evnt) {
    var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox.propSheetCntrl.Data.getDataObjectCntrl(), objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(), intlJson = [], isEdit = false; if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
        $.each(propObject.databindObjs, function () {
            var objDatabindObj = jQuery.extend(true, {}, this); $(currentCntrl.control).val(this['name']); switch (this['bindObjType']) {
                case "1": mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control); showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false); break; case "2": mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control); showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false); break; case "5": mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control); showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false); break; case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt.id: intlJson = MF_HELPERS.intlsenseHelper.getControlDatabindingIntellisenseJson(objCurrentForm, propObject.id, true); if (objDatabindObj != null && (objDatabindObj.id && objDatabindObj.id !== "-1")) { isEdit = true; }
                    mFicientIde.MF_DATA_BINDING.processDataBindingByObjectType({ controlType: MF_IDE_CONSTANTS.CONTROLS.CHECKBOX, databindObject: objDatabindObj, objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt, isEdit: isEdit, intlsJson: intlJson, control: propObject }); showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 550, false); break;
            } 
        });
    } 
} 
}
}, Behaviour: { Update: function (control, appObj) { if ((appObj.refreshControls != undefined || appObj.refreshControls != null) && appObj.refreshControls.length > 0) $(control).val(appObj.refreshControls.length + " Control(s)"); else $(control).val("0 Control(s)"); }, Display: function (control, appObj) { $(control).val(appObj.conditionalDisplay) }, Conditions: function (control, appObj) {
    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox.propSheetCntrl.Behaviour.getConditionsCntrl(); if (appObj.conditionalDisplay == "0")
        $(currentCntrl.wrapperDiv).show(); else
        $(currentCntrl.wrapperDiv).hide();
}, eventHandlers: { RefreshControls: function (evnt) {
    var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox.propSheetCntrl.Behaviour.getRefreshControls(); var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); var controls = []; if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) { $.each(form.rowPanels, function (rowIndex, rowPanel) { if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) { $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) { if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) { $.each(columnPanel.controls, function (cntrlIndex, control) { if (control.id != propObject.id && !control.isDeleted && control.type != mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SEPARATOR) { controls.push(control.userDefinedName); } }); } }); } }); }
    if (controls.length == 0) { showMessage('Please add controls first.', DialogType.Error); return; }
    else { $('#OnChangeElmDiv').mficientRefreshCntrlsOnChng({ Controls: controls, SelectedControls: (propObject.refreshControls != undefined ? propObject.refreshControls : []) }); showModalPopUpWithOutHeader('SubProcOnChange', 'Controls', 280, false); $('#OnChangeElmDiv').data("ControlId", propObject.id); }
    $('[id$=btnSaveRefreshControls]').unbind('click'); $('[id$=btnSaveRefreshControls]').bind('click', function () { propObject.refreshControls = $('#OnChangeElmDiv').data("SelectedControls"); $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)"); $('#SubProcOnChange').dialog('close'); return false; }); $('[id$=btnCancelRefreshControl]').unbind('click'); $('[id$=btnCancelRefreshControl]').bind('click', function () { $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)"); $('#SubProcOnChange').dialog('close'); return false; });
}, Display: function (evnt) {
    var evntData = evnt.data; var cntrls = evntData.cntrls; var propObject = evnt.data.propObject; propObject.conditionalDisplay = evnt.target.value; var $Conditions = $('#' + cntrls.Conditions.wrapperDiv); if (evnt.target.value == "0") { $Conditions.show(); }
    else { $Conditions.hide(); } 
}, ConditionalDisplay: function (evnt) {
    var propObject = evnt.data.propObject; var json = "["; var controlJson = ""; var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); var controls = []; var arrIntellisenseControls = []; var objIntellisenseCntrl = {}; if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) { $.each(form.rowPanels, function (rowIndex, rowPanel) { if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) { $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) { if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) { $.each(columnPanel.controls, function (cntrlIndex, control) { objIntellisenseCntrl = {}; if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) { controls.push(control); objIntellisenseCntrl["cntrlnm"] = control.userDefinedName; objIntellisenseCntrl["type"] = control.type; arrIntellisenseControls.push(objIntellisenseCntrl); } }); } }); } }); }
    if ((controls != null || controls != undefined) && controls.length > 0) {
        $.each(controls, function (index, value) {
            if (controlJson.length > 0)
                controlJson += ","; controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
        });
    }
    json += controlJson + "]"; $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 }); showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false); $('#divConditionalControl').data("ControlId", propObject.id); $('[id$=btnCondSave]').unbind('click'); $('[id$=btnCondSave]').bind('click', function () { propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")(); $('#SubProcConditionalDisplay').dialog('close'); return false; }); $('[id$=btnCondCancel]').unbind('click'); $('[id$=btnCondCancel]').bind('click', function () { $('#SubProcConditionalDisplay').dialog('close'); return false; });
} 
}
}
}, propSheetCntrl: (function () {
    function _getDivControlHelperData() { return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper); }
    function _getRefreshControlsCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.propPluginPrefix, "Behaviour", "Update"); }
        return objControl;
    }
    function _getBindToPropertyCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.propPluginPrefix, "Data", "BindToProperty"); }
        return objControl;
    }
    function _getDataObjectCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.propPluginPrefix, "Data", "DataObject"); }
        return objControl;
    }
    function _getDataBindingCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.propPluginPrefix, "Data", "DataBinding"); }
        return objControl;
    }
    function _getConditionsCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.propPluginPrefix, "Behaviour", "Conditions"); }
        return objControl;
    }
    return { Behaviour: { getRefreshControls: _getRefreshControlsCntrl, getConditionsCntrl: _getConditionsCntrl }, Data: { getBindToPropertyCntrl: _getBindToPropertyCntrl, getDataObjectCntrl: _getDataObjectCntrl, getDataBindingCntrl: _getDataBindingCntrl} };
})()
}; mFicientIde.PropertySheetJson.Checkbox = { "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX, "groups": [{ "name": "Control Properties", "prefixText": "ControlProperties", "hidden": true, "properties": [{ "text": "Type", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "Type", "noOfLines": "0", "validations": [], "customValidations": [], "events": [], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 50, "disabled": true, "defltValue": "Checkbox", "hidden": false} }, { "text": "Name", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "Name", "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Checkbox.groups.ControlProperties.eventHandlers.NameChanged, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 50, "disabled": false, "defltValue": "", "hidden": false} }, { "text": "Description", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea, "prefixText": "Description", "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Checkbox.groups.ControlProperties.eventHandlers.DescChanged, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 250, "disabled": false, "defltValue": "", "hidden": false}}] }, { "name": "Appearance", "prefixText": "Appearance", "hidden": false, "properties": [{ "text": "Label Text", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "LabelText", "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Checkbox.groups.Appearance.eventHandlers.LabelTextChange, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "Label Text", "hidden": false} }, { "text": "Alignment", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "Alignment", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Checkbox.groups.Appearance.eventHandlers.Alignment, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "Left", "value": "0" }, { "text": "Right", "value": "1"}] }, { "text": "Disabled", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "Disabled", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Checkbox.groups.Appearance.eventHandlers.Disabled, "context": "", "arguments": { "cntrls": []}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "No", "value": "0" }, { "text": "Yes", "value": "1"}]}] }, { "name": "Data", "prefixText": "Data", "hidden": false, "properties": [{ "text": "Bind To Property", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "BindToProperty", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Checkbox.groups.Data.eventHandlers.BindToProperty, "context": "", "arguments": { "cntrls": []}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": true }, "options": [] }, { "text": "Databinding", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "Databinding", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Checkbox.groups.Data.eventHandlers.DataObject, "context": "", "arguments": { "cntrls": [{ grpPrefText: "Data", cntrlPrefText: "DataObject", rtrnPropNm: "DataObject"}]}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "None", "value": "0" }, { "text": "Database Object", "value": "1" }, { "text": "Web Service Object", "value": "2" }, { "text": "OData Object", "value": "5" }, { "text": "Offline Database", "value": "6"}] }, { "text": "Data Object", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse, "prefixText": "DataObject", "disabled": false, "hidden": true, "noOfLines": "0", "validations": [], "customValidations": [{ "func": "functionName", "context": "context"}], "events": [{ "name": "click", "func": PROP_JSON_HTML_MAP.Checkbox.groups.Data.eventHandlers.DataBinding, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "Select Object", "hidden": true}}] }, { "name": "Behaviour", "prefixText": "Behaviour", "hidden": false, "properties": [{ "text": "Update", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse, "prefixText": "Update", "disabled": false, "hidden": true, "noOfLines": "0", "validations": [], "customValidations": [{ "func": "functionName", "context": "context"}], "events": [{ "name": "click", "func": PROP_JSON_HTML_MAP.Checkbox.groups.Behaviour.eventHandlers.RefreshControls, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "None", "hidden": false} }, { "text": "Display", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "Display", "defltValue": "1", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Checkbox.groups.Behaviour.eventHandlers.Display, "context": "", "arguments": { "cntrls": [{ grpPrefText: "Behaviour", cntrlPrefText: "Conditions", rtrnPropNm: "Conditions"}]}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "Always", "value": "1" }, { "text": "Conditional", "value": "0"}] }, { "text": "Conditions", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse, "prefixText": "Conditions", "disabled": false, "noOfLines": "0", "validations": [], "customValidations": [{ "func": "functionName", "context": "context"}], "events": [{ "name": "click", "func": PROP_JSON_HTML_MAP.Checkbox.groups.Behaviour.eventHandlers.ConditionalDisplay, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "Select Conditions", "hidden": true}}]}] }; function Checkbox(opts) { if (!opts) opts = {}; this.id = opts.id; this.userDefinedName = opts.userDefinedName; this.description = opts.description; this.type = opts.type; this.bindType = opts.bindType; this.isDeleted = opts.isDeleted; this.oldName = opts.oldName; this.isNew = opts.isNew; this.labelText = opts.labelText; this.alignment = opts.alignment; this.checkedVal = opts.checkedVal; this.bindToProperty = opts.bindToProperty; this.databindObjs = opts.databindObjs; this.refreshControls = opts.refreshControls; this.conditionalDisplay = opts.conditionalDisplay; this.condDisplayCntrlProps = opts.condDisplayCntrlProps; this.disabled = opts.disabled; }
Checkbox.prototype = new ControlNew(); Checkbox.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Checkbox; Checkbox.prototype.fnRename = function (name) {
    if (!this.isNew && !this.oldName && this.userDefinedName !== name) { this.oldName = this.userDefinedName; this.userDefinedName = name; }
    else if (this.userDefinedName != name) { this.userDefinedName = name; } 
}; Checkbox.prototype.fnAddDatabindObjs = function (databindObjs) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) { this.databindObjs = []; }
    if ($.isArray(databindObjs)) { var self = this; $.each(databindObjs, function (index, value) { if (value && value instanceof DatabindingObj) { self.databindObjs.push(value); } }); } 
}
Checkbox.prototype.fnAddDatabindObj = function (databindObj) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) { this.databindObjs = []; }
    if (databindObj && databindObj instanceof DatabindingObj) { this.databindObjs.push(databindObj); } 
}
Checkbox.prototype.fnResetObjectDetails = function () { var databindObjs = this.databindObjs; this.databindObjs = []; var i = 0; if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) { for (i = 0; i <= databindObjs.length - 1; i++) { var objdatabindObj = new DatabindingObj(databindObjs[i].id, databindObjs[i].cmdParams, databindObjs[i].index, databindObjs[i].bindObjType, databindObjs[i].name, databindObjs[i].dsName, databindObjs[i].tempUiIndex, databindObjs[i].usrName, databindObjs[i].pwd); objdatabindObj.fnResetObjectDetails(); this.fnAddDatabindObj(objdatabindObj); } } }
Checkbox.prototype.fnDeleteDatabindObj = function (name) { var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) { if (databindObj instanceof DatabindingObj) { return databindObj.name !== name; } }); this.databindObjs = aryNewDatabindObjs; }
Checkbox.prototype.fnClearAllDatabindObjs = function () { this.databindObjs = []; }
Checkbox.prototype.fnAddBindOption = function (bindOption) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) { this.bindOptions = []; }
    if (bindOption && bindOption instanceof ManualBindOptions) { this.bindOptions.push(bindOption); } 
}
Checkbox.prototype.fnAddBindOptions = function (bindOptions) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) { this.bindOptions = []; }
    if ($.isArray(bindOptions)) { var self = this; $.each(bindOptions, function (index, value) { if (value && value instanceof ManualBindOptions) { self.databindObjs.push(value); } }); } 
}
Checkbox.prototype.fnGetCheckedVal = function () { return this.checkedVal; }; Checkbox.prototype.fnSetCheckedVal = function (value) { this.checkedVal = value; }; Checkbox.prototype.fnSetDatabindObjs = function (databindObjs) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) { this.databindObjs = []; }
    if ($.isArray(databindObjs)) { var self = this; $.each(databindObjs, function (index, value) { if (value && value instanceof DatabindingObj) { self.databindObjs.push(value); } }); } 
}; Checkbox.prototype.fnGetDatabindingObj = function () { return this.databindObjs && this.databindObjs[0]; }; mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton = { groups: { ControlProperties: { Type: function (control, appObj) { $(control).val(appObj.type.UIName) }, Name: function (control, appObj) { $(control).val(appObj.userDefinedName) }, Description: function (control, appObj, options) {
    var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel; var strDescription = appObj.description; if (strDescription) {
        $(control).val(strDescription); if (strDescription.length > 25) { $textAreaLabel.val(strDescription.substring(0, 25) + '...'); }
        else { $textAreaLabel.val(strDescription); } 
    }
    else { $(control).val(''); $textAreaLabel.val(''); } 
}, eventHandlers: { NameChanged: function (evnt) {
    var evntData = evnt.data; var propObject = evnt.data.propObject; var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) { if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName; else evnt.target.value = ""; return; }
    propObject.fnRename(evnt.target.value);
}, DescChanged: function (evnt) { var evntData = evnt.data; var propObject = evnt.data.propObject; propObject.description = evnt.target.value; } 
}
}, Appearance: { LabelText: function (control, appObj) {
    $(control).val(appObj.labelText); if (appObj.labelText != undefined && appObj.labelText != null && appObj.labelText.length > 0)
        $('#' + appObj.id + '_Label').html(appObj.labelText); else
        $('#' + appObj.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");
}, eventHandlers: { LabelTextChange: function (evnt) {
    var propObject = evnt.data.propObject; propObject.labelText = evnt.target.value; if (evnt.target.value.length > 0)
        $('#' + propObject.id + '_Label').html(evnt.target.value); else
        $('#' + propObject.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");
} 
}
}, Data: { BindValueToProperty: function (control, appObj) {
    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Data.getBindValueToPropertyCntrl(); if ($('[id$=hdfIsChildForm]').val() == 'true') {
        var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val()); var strOptionsHtml = '<option value="-1">Select Item</option>'; if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) { $.each(eltCntrl.properties, function () { strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>'; }); }
        $(control).html(strOptionsHtml); if (appObj.bindValueToProperty != undefined) $(control).val(appObj.bindValueToProperty); else $(control).val('-1'); $(currentCntrl.wrapperDiv).show();
    }
    else
        $(currentCntrl.wrapperDiv).hide();
}, BindTextToProperty: function (control, appObj) {
    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Data.getBindTextToPropertyCntrl(); if ($('[id$=hdfIsChildForm]').val() == 'true') {
        var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val()); var strOptionsHtml = '<option value="-1">Select Item</option>'; if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) { $.each(eltCntrl.properties, function () { strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>'; }); }
        $(control).html(strOptionsHtml); if (appObj.bindTextToProperty != undefined) $(control).val(appObj.bindTextToProperty); else $(control).val('-1'); $(currentCntrl.wrapperDiv).show();
    }
    else
        $(currentCntrl.wrapperDiv).hide();
}, RadioButtonOptions: function (control, appObj) {
    $(control).val(appObj.options.length + " Option(s)"); $('#' + appObj.id + 'optionsDiv').html(''); var html = ''; var valIndex = 1; $.each(appObj.options, function () {
        html += '<div class="IdeUi-radio node "><input type="radio" value="' + this.value + '" id="' + appObj.id + 'txt_' + valIndex + '"/><label class="' + (valIndex == '1' ? 'IdeUi-first-child' : '') + ' ' + (valIndex == appObj.options.length ? 'IdeUi-last-child' : '') + ' ' + (this.isDefault == '1' ? 'IdeUi-radio-on ' : 'IdeUi-radio-off ') + 'IdeUi-btn IdeUi-fullsize IdeUi-btn-icon-left IdeUi-btn-up-c node "><span class="IdeUi-btn-inner">'
+ '<span class="node IdeUi-btn-text" id="' + appObj.id + 'span_' + valIndex + '">' + this.text + '</span><span class="Checkbox-Icon node IdeUi-icon' + (this.isDefault == '1' ? ' IdeUi-icon-radio-on' : ' IdeUi-icon-radio-off') + ' IdeUi-icon-shadow">&nbsp;</span></span></label>'
+ '</div>'; valIndex = valIndex + 1;
    }); $('#' + appObj.id + 'optionsDiv').append(html);
}, eventHandlers: { BindValueToProperty: function (evnt) {
    var evntData = evnt.data; var cntrls = evntData.cntrls; var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Data.getBindValueToPropertyCntrl(); var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); if (objform.fnAddBindProperty(evnt.target.value, propObject.bindValueToProperty))
        propObject.bindValueToProperty = evnt.target.value; else { if (propObject.bindValueToProperty != undefined) evnt.target.value = propObject.bindValueToProperty; else evnt.target.value = '-1'; } 
}, BindTextToProperty: function (evnt) {
    var evntData = evnt.data; var cntrls = evntData.cntrls; var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Data.getBindTextToPropertyCntrl(); var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); if (objform.fnAddBindProperty(evnt.target.value, propObject.bindTextToProperty))
        propObject.bindTextToProperty = evnt.target.value; else { if (propObject.bindTextToProperty != undefined) evnt.target.value = propObject.bindTextToProperty; else evnt.target.value = '-1'; } 
}, RadioButtonOptions: function (evnt) { var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Data.getRadButtonOptions(); rbOptionEdit(propObject); $('#btnSaveRadioBtnOptions').unbind('click'); $('#btnSaveRadioBtnOptions').bind('click', function () { _saveRbOption(propObject); $(currentCntrl.control).val(propObject.options.length + " Option(s)"); return false; }); $('#btnCancelRadioBtnOptions').unbind('click'); $('#btnCancelRadioBtnOptions').bind('click', function () { SubProcAddOptionInRbAndChk(false); $(currentCntrl.control).val(propObject.options.length + " Option(s)"); return false; }); SubProcAddOptionInRbAndChk(true, "RadioButton Options"); } 
}
}, Validation: { Required: function (control, appObj) { $(control).val(appObj.required) }, eventHandlers: { IsRequired: function (evnt) { var propObject = evnt.data.propObject; propObject.required = evnt.target.value; } } }, Behaviour: { Update: function (control, appObj) { if ((appObj.refreshControls != undefined || appObj.refreshControls != null) && appObj.refreshControls.length > 0) $(control).val(appObj.refreshControls.length + " Control(s)"); else $(control).val("0 Control(s)"); }, Display: function (control, appObj) { $(control).val(appObj.conditionalDisplay) }, Conditions: function (control, appObj) {
    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Behaviour.getConditionsCntrl(); if (appObj.conditionalDisplay == "0")
        $(currentCntrl.wrapperDiv).show(); else
        $(currentCntrl.wrapperDiv).hide();
}, eventHandlers: { RefreshControls: function (evnt) {
    var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton.propSheetCntrl.Behaviour.getRefreshControls(); var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); var controls = []; if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) { $.each(form.rowPanels, function (rowIndex, rowPanel) { if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) { $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) { if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) { $.each(columnPanel.controls, function (cntrlIndex, control) { if (control.id != propObject.id && !control.isDeleted && control.type != mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SEPARATOR) { controls.push(control.userDefinedName); } }); } }); } }); }
    if (controls.length == 0) { showMessage('Please add controls first.', DialogType.Error); return; }
    else { $('#OnChangeElmDiv').mficientRefreshCntrlsOnChng({ Controls: controls, SelectedControls: (propObject.refreshControls != undefined ? propObject.refreshControls : []) }); showModalPopUpWithOutHeader('SubProcOnChange', 'Controls', 280, false); $('#OnChangeElmDiv').data("ControlId", propObject.id); }
    $('[id$=btnSaveRefreshControls]').unbind('click'); $('[id$=btnSaveRefreshControls]').bind('click', function () { propObject.refreshControls = $('#OnChangeElmDiv').data("SelectedControls"); $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)"); $('#SubProcOnChange').dialog('close'); return false; }); $('[id$=btnCancelRefreshControl]').unbind('click'); $('[id$=btnCancelRefreshControl]').bind('click', function () { $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)"); $('#SubProcOnChange').dialog('close'); return false; });
}, Display: function (evnt) {
    var evntData = evnt.data; var cntrls = evntData.cntrls; var propObject = evnt.data.propObject; propObject.conditionalDisplay = evnt.target.value; var $Conditions = $('#' + cntrls.Conditions.wrapperDiv); if (evnt.target.value == "0") { $Conditions.show(); }
    else { $Conditions.hide(); } 
}, ConditionalDisplay: function (evnt) {
    var propObject = evnt.data.propObject; var json = "["; var controlJson = ""; var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); var controls = []; var arrIntellisenseControls = []; var objIntellisenseCntrl = {}; if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) { $.each(form.rowPanels, function (rowIndex, rowPanel) { if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) { $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) { if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) { $.each(columnPanel.controls, function (cntrlIndex, control) { objIntellisenseCntrl = {}; if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) { controls.push(control); objIntellisenseCntrl["cntrlnm"] = control.userDefinedName; objIntellisenseCntrl["type"] = control.type; arrIntellisenseControls.push(objIntellisenseCntrl); } }); } }); } }); }
    if ((controls != null || controls != undefined) && controls.length > 0) {
        $.each(controls, function (index, value) {
            if (controlJson.length > 0)
                controlJson += ","; controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
        });
    }
    json += controlJson + "]"; $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 }); showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false); $('#divConditionalControl').data("ControlId", propObject.id); $('[id$=btnCondSave]').unbind('click'); $('[id$=btnCondSave]').bind('click', function () { propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")(); $('#SubProcConditionalDisplay').dialog('close'); return false; }); $('[id$=btnCondCancel]').unbind('click'); $('[id$=btnCondCancel]').bind('click', function () { $('#SubProcConditionalDisplay').dialog('close'); return false; });
} 
}
}
}, propSheetCntrl: (function () {
    function _getDivControlHelperData() { return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper); }
    function _getDataRadButtonOptionsCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.propPluginPrefix, "Data", "RadioButtonOptions"); }
        return objControl;
    }
    function _getBindValueToPropertyCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.propPluginPrefix, "Data", "BindValueToProperty"); }
        return objControl;
    }
    function _getBindTextToPropertyCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.propPluginPrefix, "Data", "BindTextToProperty"); }
        return objControl;
    }
    function _getRefreshControlsCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.propPluginPrefix, "Behaviour", "Update"); }
        return objControl;
    }
    function _getConditionsCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.propPluginPrefix, "Behaviour", "Conditions"); }
        return objControl;
    }
    return { Data: { getRadButtonOptions: _getDataRadButtonOptionsCntrl, getBindValueToPropertyCntrl: _getBindValueToPropertyCntrl, getBindTextToPropertyCntrl: _getBindTextToPropertyCntrl }, Behaviour: { getRefreshControls: _getRefreshControlsCntrl, getConditionsCntrl: _getConditionsCntrl} };
})()
}; mFicientIde.PropertySheetJson.RadioButton = { "type": MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON, "groups": [{ "name": "Control Properties", "prefixText": "ControlProperties", "hidden": true, "properties": [{ "text": "Type", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "Type", "noOfLines": "0", "validations": [], "customValidations": [], "events": [], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 50, "disabled": true, "defltValue": "Radio Button", "hidden": false} }, { "text": "Name", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "Name", "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.RadioButton.groups.ControlProperties.eventHandlers.NameChanged, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 50, "disabled": false, "defltValue": "", "hidden": false} }, { "text": "Description", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea, "prefixText": "Description", "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.RadioButton.groups.ControlProperties.eventHandlers.DescChanged, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 250, "disabled": false, "defltValue": "", "hidden": false}}] }, { "name": "Appearance", "prefixText": "Appearance", "hidden": false, "properties": [{ "text": "Label Text", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "LabelText", "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.RadioButton.groups.Appearance.eventHandlers.LabelTextChange, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "Label Text", "hidden": false}}] }, { "name": "Data", "prefixText": "Data", "hidden": false, "properties": [{ "text": "Bind Value To Property", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "BindValueToProperty", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.RadioButton.groups.Data.eventHandlers.BindValueToProperty, "context": "", "arguments": { "cntrls": []}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": true }, "options": [] }, { "text": "Bind Text To Property", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "BindTextToProperty", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.RadioButton.groups.Data.eventHandlers.BindTextToProperty, "context": "", "arguments": { "cntrls": []}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": true }, "options": [] }, { "text": "Radio Button Options", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse, "prefixText": "RadioButtonOptions", "disabled": false, "hidden": true, "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "click", "func": PROP_JSON_HTML_MAP.RadioButton.groups.Data.eventHandlers.RadioButtonOptions, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "3 Option(s)", "hidden": false}}] }, { "name": "Validation", "prefixText": "Validation", "hidden": false, "properties": [{ "text": "Required", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "Required", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.RadioButton.groups.Validation.eventHandlers.IsRequired, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "No", "value": "0" }, { "text": "Yes", "value": "1"}]}] }, { "name": "Behaviour", "prefixText": "Behaviour", "hidden": false, "properties": [{ "text": "Update", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse, "prefixText": "Update", "disabled": false, "hidden": true, "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "click", "func": PROP_JSON_HTML_MAP.RadioButton.groups.Behaviour.eventHandlers.RefreshControls, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "None", "hidden": false} }, { "text": "Display", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "Display", "defltValue": "1", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.RadioButton.groups.Behaviour.eventHandlers.Display, "context": "", "arguments": { "cntrls": [{ grpPrefText: "Behaviour", cntrlPrefText: "Conditions", rtrnPropNm: "Conditions"}]}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "Always", "value": "1" }, { "text": "Conditional", "value": "0"}] }, { "text": "Conditions", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse, "prefixText": "Conditions", "disabled": false, "noOfLines": "0", "validations": [], "customValidations": [{ "func": "functionName", "context": "context"}], "events": [{ "name": "click", "func": PROP_JSON_HTML_MAP.RadioButton.groups.Behaviour.eventHandlers.ConditionalDisplay, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "Select Conditions", "hidden": true}}]}] }; function RadioButton(opts) { if (!opts) opts = {}; this.id = opts.id; this.userDefinedName = opts.userDefinedName; this.description = opts.description; this.type = opts.type; this.isDeleted = opts.isDeleted; this.oldName = opts.oldName; this.isNew = opts.isNew; this.labelText = opts.labelText; this.required = opts.required; this.options = opts.options; this.refreshControls = opts.refreshControls; this.bindValueToProperty = opts.bindValueToProperty; this.bindTextToProperty = opts.bindTextToProperty; this.conditionalDisplay = opts.conditionalDisplay; this.condDisplayCntrlProps = opts.condDisplayCntrlProps; }
RadioButton.prototype = new ControlNew(); RadioButton.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.RadioButton; RadioButton.prototype.fnRename = function (name) {
    if (!this.isNew && !this.oldName && this.userDefinedName !== name) { this.oldName = this.userDefinedName; this.userDefinedName = name; }
    else if (this.userDefinedName != name) { this.userDefinedName = name; } 
}; RadioButton.prototype.fnAddRadioButtonOption = function (radioButtonOption) {
    if (!this.radioButtonOption || !$.isArray(this.radioButtonOption)) { this.radioButtonOption = []; }
    if (radioButtonOption && radioButtonOption instanceof RadioButtonOption) { this.options.push(radioButtonOption); } 
}
RadioButton.prototype.fnAddRadioButtonOptions = function (radioButtonOptions) {
    if (!this.radioButtonOptions || !$.isArray(this.radioButtonOptions)) { this.radioButtonOptions = []; }
    if ($.isArray(radioButtonOptions)) { var self = this; $.each(radioButtonOptions, function (index, value) { if (value && value instanceof RadioButtonOption) { self.options.push(value); } }); } 
}
RadioButton.prototype.fnResetObjectDetails = function () { var options = this.options; this.options = []; var i = 0; if (options && $.isArray(options) && options.length > 0) { for (i = 0; i <= options.length - 1; i++) { var objOption = new RadioButtonOption(options[i]); this.fnAddRadioButtonOption(objOption); } } }
RadioButton.prototype.fnDeleteRadioButtonOption = function (id) { var aryNewRadioButtonOptions = $.grep(this.options, function (radioButtonOption) { if (radioButtonOption instanceof RadioButtonOption) { return radioButtonOption.id !== id; } }); this.options = aryNewRadioButtonOptions; }
function RadioButtonOption(opts) { if (!opts) opts = {}; this.id = opts.id; this.userDefinedName = opts.userDefinedName; this.description = opts.description; this.type = opts.type; this.value = opts.value; this.text = opts.text; this.isDefault = opts.isDefault; this.count = opts.count; }
RadioButtonOption.prototype = new RadioButtonOption(); var PrvChecked = ""; function rbOptionEdit(cntrl) { PrvChecked = ""; $('#RbContent').html(''); var DpOptionCount = 0; if (cntrl.options != undefined && cntrl.options.length > 0) { $.each(cntrl.options, function () { DpOptionCount = DpOptionCount + 1; _addNewRbOptionRow(DpOptionCount, cntrl); $('#Rbtext_' + DpOptionCount).val(this.text); $('#RbValue_' + DpOptionCount).val(this.value); if (this.isDefault == '1') { $("#chkRbDefault_" + DpOptionCount).attr('checked', 'checked'); PrvChecked = "chkRbDefault_" + DpOptionCount; } }); } }
function _addNewRbOptionRow(DpOptionCount, cntrl) {
    $('#RbContent').append('<tr id="RbContentText_' + DpOptionCount + '"><td> <input id="Rbtext_' + DpOptionCount + '" type="text" value="" /></td>'
+ '<td> <input id="RbValue_' + DpOptionCount + '" type="text" value="" /></td><td style="padding-left: 25px;"><input id="chkRbDefault_' + DpOptionCount + '" type="checkbox" "checked"="")/></td>'
+ '<td><img id="imgRbDelete_' + DpOptionCount + '" alt="delete" src="//enterprise.mficient.com/images/cross.png" style="padding-left:10px;cursor:pointer;"  title="Delete Option"/></td><td><img id="imgRbAdd_' + DpOptionCount + '" alt="add" src="//enterprise.mficient.com/images/add.png" style="padding-left:10px;cursor:pointer;"  title="Add Option"/></td></tr>'); $('#imgRbDelete_' + DpOptionCount).bind('click', function () {
    if (parseInt($("tbody#RbContent").find("tr").length, 10) > 1) { $('#' + this.parentElement.parentElement.id).remove(); }
    else { showMessage('This option cannot be deleted. RadioButton should have at least 1 option.', DialogType.Error); }
    cntrl.fnDeleteRadioButtonOption('RbContentText_' + DpOptionCount);
}); $('#imgRbAdd_' + DpOptionCount).bind('click', function () { _addNewRbOptionRow(parseInt($("tbody#RbContent").find("tr").length, 10) + 1, cntrl); }); $('#chkRbDefault_' + DpOptionCount).bind('change', function (e) {
    if ($('#' + this.id).is(":checked")) { $('#' + PrvChecked).removeAttr('checked'); $('#' + this.id).attr('checked', 'checked'); PrvChecked = this.id; }
    else { $('#' + this.id).removeAttr('checked'); $('#' + PrvChecked).attr('checked', 'checked'); } 
});
}
function _saveRbOption(cntrl) {
    cntrl.options = []; var index = 1; $('#' + cntrl.id + 'optionsDiv').html(''); $('#RbContent').find("*").each(function () {
        switch (this.id.split('_')[0]) {
            case "RbContentText": var radioButtonOption = _getRbOption(this.id); if (radioButtonOption != null || radioButtonOption != undefined) { cntrl.fnAddRadioButtonOption(radioButtonOption); }
                break;
        } 
    }); var strHtml = ''; $.each(cntrl.options, function () {
        strHtml += '<div class="IdeUi-radio node "><input type="radio" value="' + this.value + '" id="' + cntrl.id + 'txt_' + index + '"/><label class="' + (index == '1' ? 'IdeUi-first-child' : '') + ' ' + (index == cntrl.options.length ? 'IdeUi-last-child' : '') + ' ' + (this.isDefault == '1' ? 'IdeUi-radio-on ' : 'IdeUi-radio-off ') + 'IdeUi-btn IdeUi-fullsize IdeUi-btn-icon-left IdeUi-btn-up-c node "><span class="IdeUi-btn-inner">'
+ '<span class="node IdeUi-btn-text" id="' + cntrl.id + 'span_' + index + '">' + this.text + '</span><span class="Checkbox-Icon node IdeUi-icon' + (this.isDefault == '1' ? ' IdeUi-icon-radio-on' : ' IdeUi-icon-radio-off') + ' IdeUi-icon-shadow">&nbsp;</span></span></label>'
+ '</div>'; index = index + 1;
    }); $('#' + cntrl.id + 'optionsDiv').append(strHtml); SubProcAddOptionInRbAndChk(false);
}
function _getRbOption(_Id) {
    var radioButtonOption = new RadioButtonOption(); radioButtonOption.id = _Id; radioButtonOption.count = $("tbody#RbContent").find("tr").length; $('#' + _Id).find("*").each(function () { switch (this.id.split('_')[0]) { case "Rbtext": radioButtonOption.text = this.value; break; case "RbValue": radioButtonOption.value = this.value; break; case "chkRbDefault": radioButtonOption.isDefault = (this.checked ? '1' : '0'); break; } }); if (radioButtonOption.text.length > 0 && radioButtonOption.value.length > 0) { return radioButtonOption; }
    else { return null; } 
}
function SubProcAddOptionInRbAndChk(_bol, _Title) {
    if (_bol)
        showModalPopUpWithOutHeader('SubProcAddOptionInRbAndChk', _Title, 515, false); else
        $('#SubProcAddOptionInRbAndChk').dialog('close');
}
var dropdownOptionsCntrlId = ""; mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown = { groups: { ControlProperties: { Type: function (control, appObj) { $(control).val(appObj.type.UIName) }, Name: function (control, appObj) { $(control).val(appObj.userDefinedName) }, Description: function (control, appObj, options) {
    var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel; var strDescription = appObj.description; if (strDescription) {
        $(control).val(strDescription); if (strDescription.length > 25) { $textAreaLabel.val(strDescription.substring(0, 25) + '...'); }
        else { $textAreaLabel.val(strDescription); } 
    }
    else { $(control).val(''); $textAreaLabel.val(''); } 
}, eventHandlers: { NameChanged: function (evnt) {
    var evntData = evnt.data; var propObject = evnt.data.propObject; var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) { if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName; else evnt.target.value = ""; return; }
    propObject.fnRename(evnt.target.value);
}, DescChanged: function (evnt) { var evntData = evnt.data; var propObject = evnt.data.propObject; propObject.description = evnt.target.value; } 
}
}, Appearance: { LabelText: function (control, appObj) {
    $(control).val(appObj.labelText); if (appObj.labelText != undefined && appObj.labelText != null && appObj.labelText.length > 0)
        $('#' + appObj.id + '_Label').html(appObj.labelText); else
        $('#' + appObj.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");
}, MinWidth: function (control, appObj) { $(control).val(appObj.minWidth) }, Width: function (control, appObj) { $(control).val(appObj.widthInPercent) }, eventHandlers: { LabelTextChange: function (evnt) {
    var propObject = evnt.data.propObject; propObject.labelText = evnt.target.value; $('#' + propObject.id + '_Label').html(evnt.target.value); if (evnt.target.value.length > 0)
        $('#' + propObject.id + '_Label').html(evnt.target.value); else
        $('#' + propObject.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;");
}, MinWidthChanged: function (evnt) { var propObject = evnt.data.propObject; propObject.minWidth = evnt.target.value; }, WidthChanged: function (evnt) { var propObject = evnt.data.propObject; propObject.widthInPercent = evnt.target.value; } 
}
}, Data: { BindValueToProperty: function (control, appObj) {
    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getBindValueToPropertyCntrl(); if ($('[id$=hdfIsChildForm]').val() == 'true') {
        var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val()); var strOptionsHtml = '<option value="-1">Select Item</option>'; if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) { $.each(eltCntrl.properties, function () { strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>'; }); }
        $(control).html(strOptionsHtml); if (appObj.bindValueToProperty != undefined) $(control).val(appObj.bindValueToProperty); else $(control).val('-1'); $(currentCntrl.wrapperDiv).show();
    }
    else
        $(currentCntrl.wrapperDiv).hide();
}, BindTextToProperty: function (control, appObj) {
    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getBindTextToPropertyCntrl(); if ($('[id$=hdfIsChildForm]').val() == 'true') {
        var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val()); var strOptionsHtml = '<option value="-1">Select Item</option>'; if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) { $.each(eltCntrl.properties, function () { strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>'; }); }
        $(control).html(strOptionsHtml); if (appObj.bindTextToProperty != undefined) $(control).val(appObj.bindTextToProperty); else $(control).val('-1'); $(currentCntrl.wrapperDiv).show();
    }
    else
        $(currentCntrl.wrapperDiv).hide();
}, Databinding: function (control, appObj) { $(control).val(appObj.bindType.id) }, DataObject: function (control, appObj) {
    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getDataObjectCntrl(); if (appObj.bindType.id != "0" && appObj.bindType.id != undefined)
        $(currentCntrl.wrapperDiv).show(); else
        $(currentCntrl.wrapperDiv).hide(); if ((appObj.databindObjs != null || appObj.databindObjs != undefined) && appObj.databindObjs.length > 0) { $.each(appObj.databindObjs, function () { $(currentCntrl.control).val(this['name']); switch (this['bindObjType']) { case "1": mFicientIde.MF_DATA_BINDING.databaseObjectBinding(appObj, this, currentCntrl.control); break; case "2": mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(appObj, this, currentCntrl.control); break; case "5": mFicientIde.MF_DATA_BINDING.oDataObjectBinding(appObj, this, currentCntrl.control); break; } }); } 
}, PromptText: function (control, appObj) { $(control).val(appObj.promptText) }, PromptValue: function (control, appObj) { $(control).val(appObj.promptValue) }, DropdownOptions: function (control, appObj) {
    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getDropdownOptions(); if (appObj.bindType == mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None) { $(control).val(appObj.options.length + " Option(s)"); $(currentCntrl.wrapperDiv).show(); }
    else
        $(currentCntrl.wrapperDiv).hide();
}, eventHandlers: { BindValueToProperty: function (evnt) {
    var evntData = evnt.data; var cntrls = evntData.cntrls; var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getBindValueToPropertyCntrl(); var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); if (objform.fnAddBindProperty(evnt.target.value, propObject.bindValueToProperty))
        propObject.bindValueToProperty = evnt.target.value; else { if (propObject.bindValueToProperty != undefined) evnt.target.value = propObject.bindValueToProperty; else evnt.target.value = "-1"; } 
}, BindTextToProperty: function (evnt) {
    var evntData = evnt.data; var cntrls = evntData.cntrls; var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getBindTextToPropertyCntrl(); var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); if (objform.fnAddBindProperty(evnt.target.value, propObject.bindTextToProperty))
        propObject.bindTextToProperty = evnt.target.value; else { if (propObject.bindTextToProperty != undefined) evnt.target.value = propObject.bindTextToProperty; else evnt.target.value = '-1'; } 
}, PromptText: function (evnt) { var propObject = evnt.data.propObject; propObject.promptText = evnt.target.value; }, PromptValue: function (evnt) { var propObject = evnt.data.propObject; propObject.promptValue = evnt.target.value; }, DropdownOptions: function (evnt) { var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getDropdownOptions(); dropdownOptionsCntrlId = currentCntrl.control; dpOptionEdit(propObject); $('[id$=btnSaveDp]').unbind('click'); $('[id$=btnSaveDp]').bind('click', function () { saveOption(propObject); $(dropdownOptionsCntrlId).val(propObject.options.length + " Option(s)"); return false; }); $('[id$=btnAddDpClose]').unbind('click'); $('[id$=btnAddDpClose]').bind('click', function () { saveOption(propObject); $(dropdownOptionsCntrlId).val(propObject.options.length + " Option(s)"); return false; }); $('[id$=btnUploadCsvFile]').unbind('click'); $('[id$=btnUploadCsvFile]').bind('click', function () { SubProcIframe(true); SubProcAddOptionInDropdown(false); $(dropdownOptionsCntrlId).val(propObject.options.length + " Option(s)"); return false; }); $('[id$=btnImport]').unbind('click'); $('[id$=btnImport]').bind('click', function () { SubProcAddOptionInDropdown(false); $(dropdownOptionsCntrlId).val(propObject.options.length + " Option(s)"); return false; }); SubProcAddOptionInDropdown(true, "Dropdown Options"); }, DataObject: function (evnt) {
    var evntData = evnt.data; var cntrls = evntData.cntrls; var propObject = evnt.data.propObject; var $ViewProperty = $('#' + cntrls.DataObject.wrapperDiv); var $DropdownOptions = $('#' + cntrls.DropdownOptions.wrapperDiv); var $DataObjectControl = $('#' + cntrls.DataObject.controlId); var dataObject = new DatabindingObj(); var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None; if (evnt.target.value != "0") {
        if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
            $.each(propObject.databindObjs, function () {
                if (this.bindObjType != bindType.id) { }
                else {
                    var blnConfirm = confirm("All Options will be deleted. Do you want to Proceed ?"); if (blnConfirm) {
                        if (propObject.options.length > 0)
                            propObject.fnDeleteAllDropdownOptions();
                    }
                    else { var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getDataBindingCntrl(); $(currentCntrl.control).val("0"); return; } 
                } 
            });
        }
        else {
            var blnConfirm = confirm("All Options will be deleted. Do you want to Proceed ?"); if (blnConfirm) {
                if (propObject.options.length > 0)
                    propObject.fnDeleteAllDropdownOptions();
            }
            else { var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getDataBindingCntrl(); $(currentCntrl.control).val("0"); return; } 
        } 
    }
    else { if (propObject.options.length <= 0) { var dropdownOption = new DropdownOption(); for (var i = 1; i <= 2; i++) { dropdownOption = new DropdownOption(); dropdownOption.id = 'DpContentText_' + i; dropdownOption.text = "option" + i; dropdownOption.value = "value" + i; propObject.fnAddDropdownOption(dropdownOption); } } }
    var dropdownOptionsCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getDropdownOptions(); $(dropdownOptionsCntrl.control).val(propObject.options.length + " Option(s)"); if (evnt.target.value == "0") { $ViewProperty.hide(); $DropdownOptions.show(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None; dataObject.bindObjType = bindType.id; }
    if (evnt.target.value == "1") { $ViewProperty.show(); $DropdownOptions.hide(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.db; dataObject.bindObjType = bindType.id; }
    else if (evnt.target.value == "2") { $ViewProperty.show(); $DropdownOptions.hide(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl; dataObject.bindObjType = bindType.id; }
    else if (evnt.target.value == "5") { $ViewProperty.show(); $DropdownOptions.hide(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.odata; dataObject.bindObjType = bindType.id; }
    else if (evnt.target.value == "6") { $ViewProperty.show(); bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt; dataObject.bindObjType = bindType.id; }
    if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) { $.each(propObject.databindObjs, function () { if (this.bindObjType != bindType.id) { this.id = ""; this.name = ""; this.cmdParams = []; propObject.displayText = ""; this.bindObjType = bindType.id; $($DataObjectControl).val("Select Object"); } }); }
    else { var dataObject = new DatabindingObj("", [], "0", bindType.id, "", ""); propObject.fnAddDatabindObj(dataObject); $($DataObjectControl).val("Select Object"); }
    propObject.bindType = bindType;
}, DataBinding: function (evnt) {
    var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Data.getDataObjectCntrl(); var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(), intlJson = [], isEdit = false; if ((propObject.databindObjs != null || propObject.databindObjs != undefined) && propObject.databindObjs.length > 0) {
        $.each(propObject.databindObjs, function () {
            var objDatabindObj = jQuery.extend(true, {}, this); $(currentCntrl.control).val(this['name']); switch (this['bindObjType']) {
                case "1": mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj, currentCntrl.control); showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 420, false); break; case "2": mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj, currentCntrl.control); showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 420, false); break; case "5": mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj, currentCntrl.control); showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 420, false); break; case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt.id: intlJson = MF_HELPERS.intlsenseHelper.getControlDatabindingIntellisenseJson(objCurrentForm, propObject.id, true); if (objDatabindObj != null && (objDatabindObj.id && objDatabindObj.id !== "-1")) { isEdit = true; }
                    mFicientIde.MF_DATA_BINDING.processDataBindingByObjectType({ controlType: MF_IDE_CONSTANTS.CONTROLS.DROPDOWN, databindObject: objDatabindObj, objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt, isEdit: isEdit, intlsJson: intlJson, control: propObject }); showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 550, false); break;
            } 
        });
    } 
} 
}
}, Validation: { Required: function (control, appObj) { $(control).val(appObj.required) }, eventHandlers: { IsRequired: function (evnt) { var propObject = evnt.data.propObject; propObject.required = evnt.target.value; } } }, Behaviour: { Update: function (control, appObj) { if ((appObj.refreshControls != undefined || appObj.refreshControls != null) && appObj.refreshControls.length > 0) $(control).val(appObj.refreshControls.length + " Control(s)"); else $(control).val("0 Control(s)"); }, Display: function (control, appObj) { $(control).val(appObj.conditionalDisplay) }, Conditions: function (control, appObj) {
    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Behaviour.getConditionsCntrl(); if (appObj.conditionalDisplay == "0")
        $(currentCntrl.wrapperDiv).show(); else
        $(currentCntrl.wrapperDiv).hide();
}, eventHandlers: { RefreshControls: function (evnt) {
    var propObject = evnt.data.propObject; var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown.propSheetCntrl.Behaviour.getRefreshControls(); var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); var controls = []; if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) { $.each(form.rowPanels, function (rowIndex, rowPanel) { if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) { $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) { if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) { $.each(columnPanel.controls, function (cntrlIndex, control) { if (control.id != propObject.id && !control.isDeleted && control.type != mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SEPARATOR) { controls.push(control.userDefinedName); } }); } }); } }); }
    if (controls.length == 0) { showMessage('Please add controls first.', DialogType.Error); return; }
    else { $('#OnChangeElmDiv').mficientRefreshCntrlsOnChng({ Controls: controls, SelectedControls: (propObject.refreshControls != undefined ? propObject.refreshControls : []) }); showModalPopUpWithOutHeader('SubProcOnChange', 'Controls', 280, false); $('#OnChangeElmDiv').data("ControlId", propObject.id); }
    $('[id$=btnSaveRefreshControls]').unbind('click'); $('[id$=btnSaveRefreshControls]').bind('click', function () { propObject.refreshControls = $('#OnChangeElmDiv').data("SelectedControls"); $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)"); $('#SubProcOnChange').dialog('close'); return false; }); $('[id$=btnCancelRefreshControl]').unbind('click'); $('[id$=btnCancelRefreshControl]').bind('click', function () { $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)"); $('#SubProcOnChange').dialog('close'); return false; });
}, Display: function (evnt) {
    var evntData = evnt.data; var cntrls = evntData.cntrls; var propObject = evnt.data.propObject; propObject.conditionalDisplay = evnt.target.value; var $Conditions = $('#' + cntrls.Conditions.wrapperDiv); if (evnt.target.value == "0") { $Conditions.show(); }
    else { $Conditions.hide(); } 
}, ConditionalDisplay: function (evnt) {
    var propObject = evnt.data.propObject; var json = "["; var controlJson = ""; var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); var controls = []; var arrIntellisenseControls = []; var objIntellisenseCntrl = {}; if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) { $.each(form.rowPanels, function (rowIndex, rowPanel) { if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) { $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) { if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) { $.each(columnPanel.controls, function (cntrlIndex, control) { objIntellisenseCntrl = {}; if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) { controls.push(control); objIntellisenseCntrl["cntrlnm"] = control.userDefinedName; objIntellisenseCntrl["type"] = control.type; arrIntellisenseControls.push(objIntellisenseCntrl); } }); } }); } }); }
    if ((controls != null || controls != undefined) && controls.length > 0) {
        $.each(controls, function (index, value) {
            if (controlJson.length > 0)
                controlJson += ","; controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
        });
    }
    json += controlJson + "]"; $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 }); showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false); $('#divConditionalControl').data("ControlId", propObject.id); $('[id$=btnCondSave]').unbind('click'); $('[id$=btnCondSave]').bind('click', function () { propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")(); $('#SubProcConditionalDisplay').dialog('close'); return false; }); $('[id$=btnCondCancel]').unbind('click'); $('[id$=btnCondCancel]').bind('click', function () { $('#SubProcConditionalDisplay').dialog('close'); return false; });
} 
}
}
}, propSheetCntrl: (function () {
    function _getDivControlHelperData() { return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper); }
    function _getDataDropdownOptionsCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix, "Data", "DropdownOptions"); }
        return objControl;
    }
    function _getDataBindingCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix, "Data", "Databinding"); }
        return objControl;
    }
    function _getRefreshControlsCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix, "Behaviour", "Update"); }
        return objControl;
    }
    function _getConditionsCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix, "Behaviour", "Conditions"); }
        return objControl;
    }
    function _getBindValueToPropertyCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix, "Data", "BindValueToProperty"); }
        return objControl;
    }
    function _getBindTextToPropertyCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix, "Data", "BindTextToProperty"); }
        return objControl;
    }
    function _getDataObjectCntrl() {
        var divData = _getDivControlHelperData(); var objControl = null; if (divData) { objControl = divData.getControlDtls(MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propPluginPrefix, "Data", "DataObject"); }
        return objControl;
    }
    return { Data: { getDropdownOptions: _getDataDropdownOptionsCntrl, getDataBindingCntrl: _getDataBindingCntrl, getBindValueToPropertyCntrl: _getBindValueToPropertyCntrl, getBindTextToPropertyCntrl: _getBindTextToPropertyCntrl, getDataObjectCntrl: _getDataObjectCntrl }, Behaviour: { getRefreshControls: _getRefreshControlsCntrl, getConditionsCntrl: _getConditionsCntrl} };
})()
}; mFicientIde.PropertySheetJson.Dropdown = { "type": MF_IDE_CONSTANTS.CONTROLS.DROPDOWN, "groups": [{ "name": "Control Properties", "prefixText": "ControlProperties", "hidden": true, "properties": [{ "text": "Type", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "Type", "noOfLines": "0", "validations": [], "customValidations": [], "events": [], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 50, "disabled": true, "defltValue": "Dropdown", "hidden": false} }, { "text": "Name", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "Name", "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Dropdown.groups.ControlProperties.eventHandlers.NameChanged, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 50, "disabled": false, "defltValue": "", "hidden": false} }, { "text": "Description", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea, "prefixText": "Description", "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Dropdown.groups.ControlProperties.eventHandlers.DescChanged, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 250, "disabled": false, "defltValue": "", "hidden": false}}] }, { "name": "Appearance", "prefixText": "Appearance", "hidden": false, "properties": [{ "text": "Label Text", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "LabelText", "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Dropdown.groups.Appearance.eventHandlers.LabelTextChange, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "Label Text", "hidden": false} }, { "text": "Min Width (px)", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "MinWidth", "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Dropdown.groups.Appearance.eventHandlers.MinWidthChanged, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 50, "disabled": false, "defltValue": "280", "hidden": false} }, { "text": "Width", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "Width", "defltValue": "3", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Dropdown.groups.Appearance.eventHandlers.WidthChanged, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "25%", "value": "0" }, { "text": "50%", "value": "1" }, { "text": "75%", "value": "2" }, { "text": "100%", "value": "3"}]}] }, { "name": "Data", "prefixText": "Data", "hidden": false, "properties": [{ "text": "Bind Value To Property", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "BindValueToProperty", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Dropdown.groups.Data.eventHandlers.BindValueToProperty, "context": "", "arguments": { "cntrls": []}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": true }, "options": [] }, { "text": "Bind Text To Property", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "BindTextToProperty", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Dropdown.groups.Data.eventHandlers.BindTextToProperty, "context": "", "arguments": { "cntrls": []}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": true }, "options": [] }, { "text": "Databinding", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "Databinding", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Dropdown.groups.Data.eventHandlers.DataObject, "context": "", "arguments": { "cntrls": [{ grpPrefText: "Data", cntrlPrefText: "DataObject", rtrnPropNm: "DataObject" }, { grpPrefText: "Data", cntrlPrefText: "DropdownOptions", rtrnPropNm: "DropdownOptions"}]}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "None", "value": "0" }, { "text": "Database Object", "value": "1" }, { "text": "Web Service Object", "value": "2" }, { "text": "OData Object", "value": "5" }, { "text": "Offline Database", "value": "6"}] }, { "text": "Data Object", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse, "prefixText": "DataObject", "disabled": false, "hidden": true, "noOfLines": "0", "validations": [], "customValidations": [{ "func": "functionName", "context": "context"}], "events": [{ "name": "click", "func": PROP_JSON_HTML_MAP.Dropdown.groups.Data.eventHandlers.DataBinding, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "Select Object", "hidden": true} }, { "text": "Dropdown Options", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse, "prefixText": "DropdownOptions", "disabled": false, "hidden": true, "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "click", "func": PROP_JSON_HTML_MAP.Dropdown.groups.Data.eventHandlers.DropdownOptions, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "2 Option(s)", "hidden": false} }, { "text": "Prompt Text", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "PromptText", "noOfLines": "false", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Dropdown.groups.Data.eventHandlers.PromptText, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 50, "disabled": false, "defltValue": "", "hidden": false} }, { "text": "Prompt Value", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox, "prefixText": "PromptValue", "noOfLines": "false", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Dropdown.groups.Data.eventHandlers.PromptValue, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "maxlength": 50, "disabled": false, "defltValue": "", "hidden": false}}] }, { "name": "Validation", "prefixText": "Validation", "hidden": false, "properties": [{ "text": "Required", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "Required", "defltValue": "0", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Dropdown.groups.Validation.eventHandlers.IsRequired, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "No", "value": "0" }, { "text": "Yes", "value": "1"}]}] }, { "name": "Behaviour", "prefixText": "Behaviour", "hidden": false, "properties": [{ "text": "Update", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse, "prefixText": "Update", "disabled": false, "hidden": true, "noOfLines": "0", "validations": [], "customValidations": [], "events": [{ "name": "click", "func": PROP_JSON_HTML_MAP.Dropdown.groups.Behaviour.eventHandlers.RefreshControls, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "None", "hidden": false} }, { "text": "Display", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown, "prefixText": "Display", "defltValue": "1", "validations": [], "customValidations": [], "events": [{ "name": "change", "func": PROP_JSON_HTML_MAP.Dropdown.groups.Behaviour.eventHandlers.Display, "context": "", "arguments": { "cntrls": [{ grpPrefText: "Behaviour", cntrlPrefText: "Conditions", rtrnPropNm: "Conditions"}]}}], "CntrlProp": "", "HelpText": "", "selectValueBy": "", "init": { "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual, "disabled": false, "hidden": false }, "options": [{ "text": "Always", "value": "1" }, { "text": "Conditional", "value": "0"}] }, { "text": "Conditions", "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse, "prefixText": "Conditions", "disabled": false, "noOfLines": "0", "validations": [], "customValidations": [{ "func": "functionName", "context": "context"}], "events": [{ "name": "click", "func": PROP_JSON_HTML_MAP.Dropdown.groups.Behaviour.eventHandlers.ConditionalDisplay, "context": "", "arguments": {}}], "CntrlProp": "", "HelpText": "", "init": { "disabled": false, "defltValue": "Select Conditions", "hidden": true}}]}] }; function Dropdown(opts) { if (!opts) opts = {}; this.id = opts.id; this.userDefinedName = opts.userDefinedName; this.description = opts.description; this.type = opts.type; this.bindType = opts.bindType; this.isDeleted = opts.isDeleted; this.oldName = opts.oldName; this.isNew = opts.isNew; this.labelText = opts.labelText; this.minWidth = opts.minWidth; this.widthInPercent = opts.widthInPercent; this.required = opts.required; this.options = opts.options; this.databindObjs = opts.databindObjs; this.promptText = opts.promptText; this.promptValue = opts.promptValue; this.optionText = opts.optionText; this.optionValue = opts.optionValue; this.refreshControls = opts.refreshControls; this.bindValueToProperty = opts.bindValueToProperty; this.bindTextToProperty = opts.bindTextToProperty; this.conditionalDisplay = opts.conditionalDisplay; this.condDisplayCntrlProps = opts.condDisplayCntrlProps; }
Dropdown.prototype = new ControlNew(); Dropdown.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Dropdown; Dropdown.prototype.fnRename = function (name) {
    if (!this.isNew && !this.oldName && this.userDefinedName !== name) { this.oldName = this.userDefinedName; this.userDefinedName = name; }
    else if (this.userDefinedName != name) { this.userDefinedName = name; } 
}; Dropdown.prototype.fnAddDatabindObjs = function (databindObjs) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) { this.databindObjs = []; }
    if ($.isArray(databindObjs)) { var self = this; $.each(databindObjs, function (index, value) { if (value && value instanceof DatabindingObj) { self.databindObjs.push(value); } }); } 
}
Dropdown.prototype.fnAddDatabindObj = function (databindObj) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) { this.databindObjs = []; }
    if (databindObj && databindObj instanceof DatabindingObj) { this.databindObjs.push(databindObj); } 
}
Dropdown.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs; this.databindObjs = []; var i = 0; if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) { for (i = 0; i <= databindObjs.length - 1; i++) { var objdatabindObj = new DatabindingObj(databindObjs[i].id, databindObjs[i].cmdParams, databindObjs[i].index, databindObjs[i].bindObjType, databindObjs[i].name, databindObjs[i].dsName, databindObjs[i].tempUiIndex, databindObjs[i].usrName, databindObjs[i].pwd); objdatabindObj.fnResetObjectDetails(); this.fnAddDatabindObj(objdatabindObj); } }
    var options = this.options; this.options = []; i = 0; if (options && $.isArray(options) && options.length > 0) { for (i = 0; i <= options.length - 1; i++) { var objOption = new DropdownOption(options[i]); this.fnAddDropdownOption(objOption); } } 
}
Dropdown.prototype.fnDeleteDatabindObj = function (name) { var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) { if (databindObj instanceof DatabindingObj) { return databindObj.name !== name; } }); this.databindObjs = aryNewDatabindObjs; }
Dropdown.prototype.fnClearAllDatabindObjs = function () { this.databindObjs = []; }
Dropdown.prototype.fnAddBindOption = function (bindOption) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) { this.bindOptions = []; }
    if (bindOption && bindOption instanceof ManualBindOptions) { this.bindOptions.push(bindOption); } 
}
Dropdown.prototype.fnAddBindOptions = function (bindOptions) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) { this.bindOptions = []; }
    if ($.isArray(bindOptions)) { var self = this; $.each(bindOptions, function (index, value) { if (value && value instanceof ManualBindOptions) { self.databindObjs.push(value); } }); } 
}
Dropdown.prototype.fnAddDropdownOption = function (dropdownOption) {
    if (!this.dropdownOption || !$.isArray(this.dropdownOption)) { this.dropdownOption = []; }
    if (dropdownOption && dropdownOption instanceof DropdownOption) { this.options.push(dropdownOption); } 
}
Dropdown.prototype.fnAddDropdownOptions = function (dropdownOptions) {
    if (!this.dropdownOptions || !$.isArray(this.dropdownOptions)) { this.dropdownOptions = []; }
    if ($.isArray(dropdownOptions)) { var self = this; $.each(dropdownOptions, function (index, value) { if (value && value instanceof DropdownOption) { self.options.push(value); } }); } 
}
Dropdown.prototype.fnDeleteDropdownOption = function (id) { var aryNewDropdownOptions = $.grep(this.options, function (dropdownOption) { if (dropdownOption instanceof DropdownOption) { return dropdownOption.id !== id; } }); this.options = aryNewDropdownOptions; }
Dropdown.prototype.fnDeleteAllDropdownOptions = function () { this.options = []; }
Dropdown.prototype.fnGetDisplayText = function () { return this.promptText; }; Dropdown.prototype.fnSetDisplayText = function (value) { this.promptText = value; }; Dropdown.prototype.fnGetDisplayVal = function () { return this.promptValue; }; Dropdown.prototype.fnSetDisplayVal = function (value) { this.promptValue = value; }; Dropdown.prototype.fnGetDatabindingObj = function () { return this.databindObjs && this.databindObjs[0]; }; function DropdownOption(opts) { if (!opts) opts = {}; this.id = opts.id; this.value = opts.value; this.text = opts.text; }
DropdownOption.prototype = new DropdownOption(); function dpOptionEdit(cntrl) { $('#DpContent').html(''); var DpOptionCount = 0; if (cntrl.options != undefined && cntrl.options.length > 0) { $.each(cntrl.options, function () { DpOptionCount = DpOptionCount + 1; _addNewRow(DpOptionCount, cntrl); $('#Dptext_' + DpOptionCount).val(this.text); $('#DpValue_' + DpOptionCount).val(htmlDecode(this.value)); }); } }
function _addNewRow(DpOptionCount, cntrl) {
    $('#DpContent').append('<tr id="DpContentText_' + DpOptionCount + '">'
+ '<td> <input id="Dptext_' + DpOptionCount + '" type="text" value=""/></td>'
+ '<td> <input id="DpValue_' + DpOptionCount + '" type="text" value="" /></td>'
+ '<td><img id="imgDpDelete_' + DpOptionCount + '" alt="delete" src="//enterprise.mficient.com/images/cross.png" style="padding-left:10px;"  title="Delete Option"/></td>'
+ '<td><img id="imgDpAdd_' + DpOptionCount + '" alt="add" src="//enterprise.mficient.com/images/add.png" style="padding-left:10px;"  title="Add Option"/></td></tr>'); $('#imgDpDelete_' + DpOptionCount).bind('click', function () {
    if (parseInt($("tbody#DpContent").find("tr").length, 10) > 1) { $('#' + this.parentElement.parentElement.id).remove(); cntrl.fnDeleteDropdownOption('DpContentText_' + DpOptionCount); }
    else { showMessage('This option cannot be deleted. Dropdown should have at least 1 option.', DialogType.Error); } 
}); $('#imgDpAdd_' + DpOptionCount).bind('click', function () { _addNewRow(parseInt($("tbody#DpContent").find("tr").length, 10) + 1, cntrl); });
}
function saveOption(cntrl) {
    cntrl.options = []; var index = 1; $('#' + cntrl.id + 'optionsDiv').html(''); var option = ""
    $('#DpContent').find("*").each(function () {
        switch (this.id.split('_')[0]) {
            case "DpContentText": var dropdownOption = _getDpOption(this.id)
                if (dropdownOption != null || dropdownOption != undefined) { cntrl.fnAddDropdownOption(dropdownOption); }
                break;
        } 
    }); var strHtml = ''; $.each(cntrl.options, function () { strHtml += "<option value=\"" + (htmlEncode(this.value).replace(/\"/g, '&quot;')) + "\">" + this.text + "</option>"; }); $('#' + cntrl.id + 'optionsDiv').append(strHtml); SubProcAddOptionInDropdown(false);
}
function _getDpOption(_Id) {
    var dropdownOption = new DropdownOption(); dropdownOption.id = _Id; var Dptext = ""; var DpValue = ""; $('#' + _Id).find("*").each(function () { switch (this.id.split('_')[0]) { case "Dptext": dropdownOption.text = this.value; break; case "DpValue": dropdownOption.value = this.value; break; } }); if (dropdownOption.text.length > 0 && dropdownOption.value.length > 0) { return dropdownOption; }
    else { return null; } 
}
function appendOptionInDropDown(json) {
    var strHtml = ''; if (json.length != 0) {
        var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); var cntrl = form.fnGetControlFromId($('#hdfElementName').val().split(',')[1]); var dropdownOption = new DropdownOption(); var reqJson = jQuery.parseJSON(json); $.each(reqJson, function () { if (this['text'].length > 0 && this['val'].length > 0) { dropdownOption = new DropdownOption(); dropdownOption.text = this['text']; dropdownOption.value = this['val']; cntrl.fnAddDropdownOption(dropdownOption); } }); $('#' + cntrl.id + 'optionsDiv').html(''); $('#DpContent').html(''); $.each(cntrl.options, function () {
            strHtml += "<option value=\"" + (htmlEncode(this.value).replace(/\"/g, '&quot;')) + "\">" + this.text + "</option>"; DpOptionCount = DpOptionCount + 1; $('#DpContent').append("<div id=\"DpContentText_" + DpOptionCount + "\" class=\"DpContentRow\"><div class=\"DpContentText\"> <input id=\"Dptext_" + DpOptionCount + "\" type=\"text\" class=\"txtDpOption\" value=\"\"/></div><div class=\"DpContentValue\"> <input id=\"DpValue_" + DpOptionCount + "\" type=\"text\"  class=\"txtDpOption\" value=\"\" /></div><div class=\"DpOptionContentDelete\"><img id=\"imgDpDelete_" + DpOptionCount + "\" alt=\"delete\" src=\"//enterprise.mficient.com/images/cross.png\" style=\"padding-left:10px;\"  title=\"Delete Option\"/></div> </div><div style=\"clear: both;\"></div>"); $('#Dptext_' + DpOptionCount).val(this.text); $('#DpValue_' + DpOptionCount).val(htmlDecode(this.value)); $('#imgDpDelete_' + DpOptionCount).bind('click', function () {
                if (parseInt($("tbody#DpContent").find("tr").length, 10) > 1) { $('#' + this.parentElement.parentElement.id).remove(); }
                else { showMessage('This option cannot be deleted. Dropdown should have at least 1 option.', DialogType.Error); } 
            });
        }); $('#' + cntrl.id + 'optionsDiv').append(strHtml); $(dropdownOptionsCntrlId).val(cntrl.options.length + " Option(s)");
    } 
}
function SubProcAddOptionInDropdown(_bol, _Title) {
    if (_bol)
        showModalPopUpWithOutHeader('SubProcBoxAddDropDownOption', _Title, 450, false); else
        $('#SubProcBoxAddDropDownOption').dialog('close');
}
function SubProcIframe(_bol) { SubProcCommonMethod('SubProcIframe', _bol, 'Upload CSV File', 450); }
var mfIdeSelectorControls = "mfIdeSelectorControls";
﻿(function (b) { b.support.touch = "ontouchend" in document; if (!b.support.touch) { return; } var c = b.ui.mouse.prototype, e = c._mouseInit, a; function d(g, h) { if (g.originalEvent.touches.length > 1) { return; } g.preventDefault(); var i = g.originalEvent.changedTouches[0], f = document.createEvent("MouseEvents"); f.initMouseEvent(h, true, true, window, 1, i.screenX, i.screenY, i.clientX, i.clientY, false, false, false, false, 0, null); g.target.dispatchEvent(f); } c._touchStart = function (g) { var f = this; if (a || !f._mouseCapture(g.originalEvent.changedTouches[0])) { return; } a = true; f._touchMoved = false; d(g, "mouseover"); d(g, "mousemove"); d(g, "mousedown"); }; c._touchMove = function (f) { if (!a) { return; } this._touchMoved = true; d(f, "mousemove"); }; c._touchEnd = function (f) { if (!a) { return; } d(f, "mouseup"); d(f, "mouseout"); if (!this._touchMoved) { d(f, "click"); } a = false; }; c._mouseInit = function () { var f = this; f.element.bind("touchstart", b.proxy(f, "_touchStart")).bind("touchmove", b.proxy(f, "_touchMove")).bind("touchend", b.proxy(f, "_touchEnd")); e.call(f); }; })(jQuery); (function () {
    "undefined" == typeof Math.sgn && (Math.sgn = function (a) { return 0 == a ? 0 : 0 < a ? 1 : -1 }); var q = { subtract: function (a, b) { return { x: a.x - b.x, y: a.y - b.y} }, dotProduct: function (a, b) { return a.x * b.x + a.y * b.y }, square: function (a) { return Math.sqrt(a.x * a.x + a.y * a.y) }, scale: function (a, b) { return { x: a.x * b, y: a.y * b} } }, B = Math.pow(2, -65), x = function (a, b) {
        for (var f = [], d = b.length - 1, g = 2 * d - 1, h = [], e = [], m = [], k = [], l = [[1, 0.6, 0.3, 0.1], [0.4, 0.6, 0.6, 0.4], [0.1, 0.3, 0.6, 1]], c = 0; c <= d; c++) h[c] = q.subtract(b[c], a); for (c = 0; c <= d - 1; c++) e[c] = q.subtract(b[c +
1], b[c]), e[c] = q.scale(e[c], 3); for (c = 0; c <= d - 1; c++) for (var n = 0; n <= d; n++) m[c] || (m[c] = []), m[c][n] = q.dotProduct(e[c], h[n]); for (c = 0; c <= g; c++) k[c] || (k[c] = []), k[c].y = 0, k[c].x = parseFloat(c) / g; g = d - 1; for (h = 0; h <= d + g; h++) { c = Math.max(0, h - g); for (e = Math.min(h, d); c <= e; c++) j = h - c, k[c + j].y += m[j][c] * l[j][c] } d = b.length - 1; k = u(k, 2 * d - 1, f, 0); g = q.subtract(a, b[0]); m = q.square(g); for (c = l = 0; c < k; c++) g = q.subtract(a, v(b, d, f[c], null, null)), g = q.square(g), g < m && (m = g, l = f[c]); g = q.subtract(a, b[d]); g = q.square(g); g < m && (m = g, l = 1); return { location: l, distance: m}
    }, u = function (a, b, f, d) { var g = [], h = [], e = [], m = [], k = 0, l, c; c = Math.sgn(a[0].y); for (var n = 1; n <= b; n++) l = Math.sgn(a[n].y), l != c && k++, c = l; switch (k) { case 0: return 0; case 1: if (64 <= d) return f[0] = (a[0].x + a[b].x) / 2, 1; var r, p, k = a[0].y - a[b].y; c = a[b].x - a[0].x; n = a[0].x * a[b].y - a[b].x * a[0].y; l = max_distance_below = 0; for (r = 1; r < b; r++) p = k * a[r].x + c * a[r].y + n, p > l ? l = p : p < max_distance_below && (max_distance_below = p); p = c; r = 0 * p - 1 * k; l = (1 * (n - l) - 0 * p) * (1 / r); p = c; c = n - max_distance_below; r = 0 * p - 1 * k; k = (1 * c - 0 * p) * (1 / r); c = Math.min(l, k); if (Math.max(l, k) - c < B) return e = a[b].x - a[0].x, m = a[b].y - a[0].y, f[0] = 0 + 1 * (e * (a[0].y - 0) - m * (a[0].x - 0)) * (1 / (0 * e - 1 * m)), 1 } v(a, b, 0.5, g, h); a = u(g, b, e, d + 1); b = u(h, b, m, d + 1); for (d = 0; d < a; d++) f[d] = e[d]; for (d = 0; d < b; d++) f[d + a] = m[d]; return a + b }, v = function (a, b, f, d, g) { for (var h = [[]], e = 0; e <= b; e++) h[0][e] = a[e]; for (a = 1; a <= b; a++) for (e = 0; e <= b - a; e++) h[a] || (h[a] = []), h[a][e] || (h[a][e] = {}), h[a][e].x = (1 - f) * h[a - 1][e].x + f * h[a - 1][e + 1].x, h[a][e].y = (1 - f) * h[a - 1][e].y + f * h[a - 1][e + 1].y; if (null != d) for (e = 0; e <= b; e++) d[e] = h[e][0]; if (null != g) for (e = 0; e <= b; e++) g[e] = h[b - e][e]; return h[b][0] }, y = {}, s = function (a, b) {
        var f, d = a.length - 1; f = y[d]; if (!f) {
            f = []; var g = function (a) { return function () { return a } }, h = function () { return function (a) { return a } }, e = function () { return function (a) { return 1 - a } }, m = function (a) { return function (b) { for (var c = 1, d = 0; d < a.length; d++) c *= a[d](b); return c } }; f.push(new function () { return function (a) { return Math.pow(a, d) } }); for (var k = 1; k < d; k++) { for (var l = [new g(d)], c = 0; c < d - k; c++) l.push(new h); for (c = 0; c < k; c++) l.push(new e); f.push(new m(l)) } f.push(new function () {
                return function (a) {
                    return Math.pow(1 -
a, d)
                } 
            }); y[d] = f
        } for (e = h = g = 0; e < a.length; e++) g += a[e].x * f[e](b), h += a[e].y * f[e](b); return { x: g, y: h}
    }, z = function (a, b) { return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2)) }, A = function (a) { return a[0].x == a[1].x && a[0].y == a[1].y }, t = function (a, b, f) { if (A(a)) return { point: a[0], location: b }; for (var d = s(a, b), g = 0, h = 0 < f ? 1 : -1, e = null; g < Math.abs(f); ) b += 0.005 * h, e = s(a, b), g += z(e, d), d = e; return { point: e, location: b} }, w = function (a, b) { var f = s(a, b), d = s(a.slice(0, a.length - 1), b), g = d.y - f.y, f = d.x - f.x; return 0 == g ? Infinity : Math.atan(g / f) }; window.jsBezier = { distanceFromCurve: x, gradientAtPoint: w, gradientAtPointAlongCurveFrom: function (a, b, f) { b = t(a, b, f); 1 < b.location && (b.location = 1); 0 > b.location && (b.location = 0); return w(a, b.location) }, nearestPointOnCurve: function (a, b) { var f = x(a, b); return { point: v(b, b.length - 1, f.location, null, null), location: f.location} }, pointOnCurve: s, pointAlongCurveFrom: function (a, b, f) { return t(a, b, f).point }, perpendicularToCurveAt: function (a, b, f, d) { b = t(a, b, null == d ? 0 : d); a = w(a, b.location); d = Math.atan(-1 / a); a = f / 2 * Math.sin(d); f = f / 2 * Math.cos(d); return [{ x: b.point.x + f, y: b.point.y + a }, { x: b.point.x - f, y: b.point.y - a}] }, locationAlongCurveFrom: function (a, b, f) { return t(a, b, f).location }, getLength: function (a) { if (A(a)) return 0; for (var b = s(a, 0), f = 0, d = 0, g = null; 1 > d; ) d += 0.005, g = s(a, d), f += z(g, b), b = g; return f } }
})(); ; (function () {
    "use strict"; var Biltong = this.Biltong = {}; var _isa = function (a) { return Object.prototype.toString.call(a) === "[object Array]"; }, _pointHelper = function (p1, p2, fn) { p1 = _isa(p1) ? p1 : [p1.x, p1.y]; p2 = _isa(p2) ? p2 : [p2.x, p2.y]; return fn(p1, p2); }, _gradient = Biltong.gradient = function (p1, p2) {
        return _pointHelper(p1, p2, function (_p1, _p2) {
            if (_p2[0] == _p1[0])
                return _p2[1] > _p1[1] ? Infinity : -Infinity; else if (_p2[1] == _p1[1])
                return _p2[0] > _p1[0] ? 0 : -0; else
                return (_p2[1] - _p1[1]) / (_p2[0] - _p1[0]);
        });
    }, _normal = Biltong.normal = function (p1, p2) { return -1 / _gradient(p1, p2); }, _lineLength = Biltong.lineLength = function (p1, p2) { return _pointHelper(p1, p2, function (_p1, _p2) { return Math.sqrt(Math.pow(_p2[1] - _p1[1], 2) + Math.pow(_p2[0] - _p1[0], 2)); }); }, _quadrant = Biltong.quadrant = function (p1, p2) {
        return _pointHelper(p1, p2, function (_p1, _p2) {
            if (_p2[0] > _p1[0]) { return (_p2[1] > _p1[1]) ? 2 : 1; }
            else if (_p2[0] == _p1[0]) { return _p2[1] > _p1[1] ? 2 : 1; }
            else { return (_p2[1] > _p1[1]) ? 3 : 4; } 
        });
    }, _theta = Biltong.theta = function (p1, p2) { return _pointHelper(p1, p2, function (_p1, _p2) { var m = _gradient(_p1, _p2), t = Math.atan(m), s = _quadrant(_p1, _p2); if ((s == 4 || s == 3)) t += Math.PI; if (t < 0) t += (2 * Math.PI); return t; }); }, _intersects = Biltong.intersects = function (r1, r2) { var x1 = r1.x, x2 = r1.x + r1.w, y1 = r1.y, y2 = r1.y + r1.h, a1 = r2.x, a2 = r2.x + r2.w, b1 = r2.y, b2 = r2.y + r2.h; return ((x1 <= a1 && a1 <= x2) && (y1 <= b1 && b1 <= y2)) || ((x1 <= a2 && a2 <= x2) && (y1 <= b1 && b1 <= y2)) || ((x1 <= a1 && a1 <= x2) && (y1 <= b2 && b2 <= y2)) || ((x1 <= a2 && a1 <= x2) && (y1 <= b2 && b2 <= y2)) || ((a1 <= x1 && x1 <= a2) && (b1 <= y1 && y1 <= b2)) || ((a1 <= x2 && x2 <= a2) && (b1 <= y1 && y1 <= b2)) || ((a1 <= x1 && x1 <= a2) && (b1 <= y2 && y2 <= b2)) || ((a1 <= x2 && x1 <= a2) && (b1 <= y2 && y2 <= b2)); }, _encloses = Biltong.encloses = function (r1, r2, allowSharedEdges) { var x1 = r1.x, x2 = r1.x + r1.w, y1 = r1.y, y2 = r1.y + r1.h, a1 = r2.x, a2 = r2.x + r2.w, b1 = r2.y, b2 = r2.y + r2.h, c = function (v1, v2, v3, v4) { return allowSharedEdges ? v1 <= v2 && v3 >= v4 : v1 < v2 && v3 > v4; }; return c(x1, a1, x2, a2) && c(y1, b1, y2, b2); }, _segmentMultipliers = [null, [1, -1], [1, 1], [-1, 1], [-1, -1]], _inverseSegmentMultipliers = [null, [-1, -1], [-1, 1], [1, 1], [1, -1]], _pointOnLine = Biltong.pointOnLine = function (fromPoint, toPoint, distance) { var m = _gradient(fromPoint, toPoint), s = _quadrant(fromPoint, toPoint), segmentMultiplier = distance > 0 ? _segmentMultipliers[s] : _inverseSegmentMultipliers[s], theta = Math.atan(m), y = Math.abs(distance * Math.sin(theta)) * segmentMultiplier[1], x = Math.abs(distance * Math.cos(theta)) * segmentMultiplier[0]; return { x: fromPoint.x + x, y: fromPoint.y + y }; }, _perpendicularLineTo = Biltong.perpendicularLineTo = function (fromPoint, toPoint, length) { var m = _gradient(fromPoint, toPoint), theta2 = Math.atan(-1 / m), y = length / 2 * Math.sin(theta2), x = length / 2 * Math.cos(theta2); return [{ x: toPoint.x + x, y: toPoint.y + y }, { x: toPoint.x - x, y: toPoint.y - y}]; };
}).call(this); ; (function () {
    var _isa = function (a) { return Object.prototype.toString.call(a) === "[object Array]"; }, _isnum = function (n) { return Object.prototype.toString.call(n) === "[object Number]"; }, _iss = function (s) { return typeof s === "string"; }, _isb = function (s) { return typeof s === "boolean"; }, _isnull = function (s) { return s == null; }, _iso = function (o) { return o == null ? false : Object.prototype.toString.call(o) === "[object Object]"; }, _isd = function (o) { return Object.prototype.toString.call(o) === "[object Date]"; }, _isf = function (o) { return Object.prototype.toString.call(o) === "[object Function]"; }, _ise = function (o) {
        for (var i in o) { if (o.hasOwnProperty(i)) return false; }
        return true;
    }, pointHelper = function (p1, p2, fn) { p1 = _isa(p1) ? p1 : [p1.x, p1.y]; p2 = _isa(p2) ? p2 : [p2.x, p2.y]; return fn(p1, p2); }; jsPlumbUtil = { isArray: _isa, isString: _iss, isBoolean: _isb, isNull: _isnull, isObject: _iso, isDate: _isd, isFunction: _isf, isEmpty: _ise, isNumber: _isnum, clone: function (a) {
        if (_iss(a)) return "" + a; else if (_isb(a)) return !!a; else if (_isd(a)) return new Date(a.getTime()); else if (_isf(a)) return a; else if (_isa(a)) {
            var b = []; for (var i = 0; i < a.length; i++)
                b.push(this.clone(a[i])); return b;
        }
        else if (_iso(a)) {
            var c = {}; for (var j in a)
                c[j] = this.clone(a[j]); return c;
        }
        else return a;
    }, matchesSelector: function (el, selector, ctx) {
        ctx = ctx || el.parentNode; var possibles = ctx.querySelectorAll(selector); for (var i = 0; i < possibles.length; i++) {
            if (possibles[i] === el)
                return true;
        }
        return false;
    }, merge: function (a, b) {
        var c = this.clone(a); for (var i in b) {
            if (c[i] == null || _iss(b[i]) || _isb(b[i]))
                c[i] = b[i]; else {
                if (_isa(b[i])) { var ar = []; if (_isa(c[i])) ar.push.apply(ar, c[i]); ar.push.apply(ar, b[i]); c[i] = ar; }
                else if (_iso(b[i])) {
                    if (!_iso(c[i]))
                        c[i] = {}; for (var j in b[i])
                        c[i][j] = b[i][j];
                } 
            } 
        }
        return c;
    }, replace: function (inObj, path, value) {
        var q = inObj, t = q; path.replace(/([^\.])+/g, function (term, lc, pos, str) {
            var array = term.match(/([^\[0-9]+){1}(\[)([0-9+])/), last = pos + term.length >= str.length, _getArray = function () { return t[array[1]] || (function () { t[array[1]] = []; return t[array[1]]; })(); }; if (last) {
                if (array)
                    _getArray()[array[3]] = value; else
                    t[term] = value;
            }
            else {
                if (array) { var a = _getArray(); t = a[array[3]] || (function () { a[array[3]] = {}; return a[array[3]]; })(); }
                else
                    t = t[term] || (function () { t[term] = {}; return t[term]; })();
            } 
        }); return inObj;
    }, functionChain: function (successValue, failValue, fns) {
        for (var i = 0; i < fns.length; i++) { var o = fns[i][0][fns[i][1]].apply(fns[i][0], fns[i][2]); if (o === failValue) { return o; } }
        return successValue;
    }, populate: function (model, values) {
        var getValue = function (fromString) {
            var matches = fromString.match(/(\${.*?})/g); if (matches != null) { for (var i = 0; i < matches.length; i++) { var val = values[matches[i].substring(2, matches[i].length - 1)]; if (val != null) { fromString = fromString.replace(matches[i], val); } } }
            return fromString;
        }, _one = function (d) {
            if (d != null) {
                if (_iss(d)) { return getValue(d); }
                else if (_isa(d)) {
                    var r = []; for (var i = 0; i < d.length; i++)
                        r.push(_one(d[i])); return r;
                }
                else if (_iso(d)) {
                    var s = {}; for (var j in d) { s[j] = _one(d[j]); }
                    return s;
                }
                else { return d; } 
            } 
        }; return _one(model);
    }, convertStyle: function (s, ignoreAlpha) {
        if ("transparent" === s) return s; var o = s, pad = function (n) { return n.length == 1 ? "0" + n : n; }, hex = function (k) { return pad(Number(k).toString(16)); }, pattern = /(rgb[a]?\()(.*)(\))/; if (s.match(pattern)) {
            var parts = s.match(pattern)[2].split(","); o = "#" + hex(parts[0]) + hex(parts[1]) + hex(parts[2]); if (!ignoreAlpha && parts.length == 4)
                o = o + hex(parts[3]);
        }
        return o;
    }, findWithFunction: function (a, f) {
        if (a)
            for (var i = 0; i < a.length; i++) if (f(a[i])) return i; return -1;
    }, indexOf: function (l, v) { return l.indexOf ? l.indexOf(v) : jsPlumbUtil.findWithFunction(l, function (_v) { return _v == v; }); }, removeWithFunction: function (a, f) { var idx = jsPlumbUtil.findWithFunction(a, f); if (idx > -1) a.splice(idx, 1); return idx != -1; }, remove: function (l, v) { var idx = jsPlumbUtil.indexOf(l, v); if (idx > -1) l.splice(idx, 1); return idx != -1; }, addWithFunction: function (list, item, hashFunction) { if (jsPlumbUtil.findWithFunction(list, hashFunction) == -1) list.push(item); }, addToList: function (map, key, value, insertAtStart) {
        var l = map[key]; if (l == null) { l = []; map[key] = l; }
        l[insertAtStart ? "unshift" : "push"](value); return l;
    }, consume: function (e, doNotPreventDefault) {
        if (e.stopPropagation)
            e.stopPropagation(); else
            e.returnValue = false; if (!doNotPreventDefault && e.preventDefault)
            e.preventDefault();
    }, extend: function (child, parent, _protoFn) {
        var i; parent = _isa(parent) ? parent : [parent]; for (i = 0; i < parent.length; i++) { for (var j in parent[i].prototype) { if (parent[i].prototype.hasOwnProperty(j)) { child.prototype[j] = parent[i].prototype[j]; } } }
        var _makeFn = function (name, protoFn) {
            return function () {
                for (i = 0; i < parent.length; i++) {
                    if (parent[i].prototype[name])
                        parent[i].prototype[name].apply(this, arguments);
                }
                return protoFn.apply(this, arguments);
            };
        }; var _oneSet = function (fns) { for (var k in fns) { child.prototype[k] = _makeFn(k, fns[k]); } }; if (arguments.length > 2) {
            for (i = 2; i < arguments.length; i++)
                _oneSet(arguments[i]);
        }
        return child;
    }, uuid: function () { return ('xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) { var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8); return v.toString(16); })); }, logEnabled: true, log: function () {
        if (jsPlumbUtil.logEnabled && typeof console != "undefined") {
            try { var msg = arguments[arguments.length - 1]; console.log(msg); }
            catch (e) { } 
        } 
    }, sizeElement: function (el, x, y, w, h) { if (el) { el.style.height = h + "px"; el.height = h; el.style.width = w + "px"; el.width = w; el.style.left = x + "px"; el.style.top = y + "px"; } }, wrap: function (wrappedFunction, newFunction, returnOnThisValue) {
        wrappedFunction = wrappedFunction || function () { }; newFunction = newFunction || function () { }; return function () {
            var r = null; try { r = newFunction.apply(this, arguments); } catch (e) { jsPlumbUtil.log("jsPlumb function failed : " + e); }
            if (returnOnThisValue == null || (r !== returnOnThisValue)) { try { r = wrappedFunction.apply(this, arguments); } catch (e) { jsPlumbUtil.log("wrapped function failed : " + e); } }
            return r;
        };
    }, ieVersion: /MSIE\s([\d.]+)/.test(navigator.userAgent) ? (new Number(RegExp.$1)) : -1
    }; jsPlumbUtil.oldIE = jsPlumbUtil.ieVersion > -1 && jsPlumbUtil.ieVersion < 9; jsPlumbUtil.EventGenerator = function () {
        var _listeners = {}, eventsSuspended = false, eventsToDieOn = { "ready": true }; this.bind = function (event, listener, insertAtStart) { jsPlumbUtil.addToList(_listeners, event, listener, insertAtStart); return this; }; this.fire = function (event, value, originalEvent) {
            if (!eventsSuspended && _listeners[event]) {
                var l = _listeners[event].length, i = 0, _gone = false, ret = null; if (!this.shouldFireEvent || this.shouldFireEvent(event, value, originalEvent)) {
                    while (!_gone && i < l && ret !== false) {
                        if (eventsToDieOn[event])
                            _listeners[event][i].apply(this, [value, originalEvent]); else { try { ret = _listeners[event][i].apply(this, [value, originalEvent]); } catch (e) { jsPlumbUtil.log("jsPlumb: fire failed for event " + event + " : " + e); } }
                        i++; if (_listeners == null || _listeners[event] == null)
                            _gone = true;
                    } 
                } 
            }
            return this;
        }; this.unbind = function (event) {
            if (event)
                delete _listeners[event]; else { _listeners = {}; }
            return this;
        }; this.getListener = function (forEvent) { return _listeners[forEvent]; }; this.setSuspendEvents = function (val) { eventsSuspended = val; }; this.isSuspendEvents = function () { return eventsSuspended; }; this.cleanupListeners = function () { for (var i in _listeners) { _listeners[i] = null; } };
    }; jsPlumbUtil.EventGenerator.prototype = { cleanup: function () { this.cleanupListeners(); } }; if (!Function.prototype.bind) {
        Function.prototype.bind = function (oThis) {
            if (typeof this !== "function") { throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable"); }
            var aArgs = Array.prototype.slice.call(arguments, 1), fToBind = this, fNOP = function () { }, fBound = function () { return fToBind.apply(this instanceof fNOP && oThis ? this : oThis, aArgs.concat(Array.prototype.slice.call(arguments))); }; fNOP.prototype = this.prototype; fBound.prototype = new fNOP(); return fBound;
        };
    } 
})(); ; (function () {
    var svgAvailable = !!window.SVGAngle || document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1"), vmlAvailable = function () {
        if (vmlAvailable.vml === undefined) {
            var a = document.body.appendChild(document.createElement('div')); a.innerHTML = '<v:shape id="vml_flag1" adj="1" />'; var b = a.firstChild; if (b != null && b.style != null) { b.style.behavior = "url(#default#VML)"; vmlAvailable.vml = b ? typeof b.adj == "object" : true; }
            else
                vmlAvailable.vml = false; a.parentNode.removeChild(a);
        }
        return vmlAvailable.vml;
    }, iev = (function () {
        var rv = -1; if (navigator.appName == 'Microsoft Internet Explorer') {
            var ua = navigator.userAgent, re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})"); if (re.exec(ua) != null)
                rv = parseFloat(RegExp.$1);
        }
        return rv;
    })(), isIELT9 = iev > -1 && iev < 9, _genLoc = function (e, prefix) { if (e == null) return [0, 0]; var ts = _touches(e), t = _getTouch(ts, 0); return [t[prefix + "X"], t[prefix + "Y"]]; }, _pageLocation = function (e) {
        if (e == null) return [0, 0]; if (isIELT9) { return [e.clientX + document.documentElement.scrollLeft, e.clientY + document.documentElement.scrollTop]; }
        else { return _genLoc(e, "page"); } 
    }, _screenLocation = function (e) { return _genLoc(e, "screen"); }, _clientLocation = function (e) { return _genLoc(e, "client"); }, _getTouch = function (touches, idx) { return touches.item ? touches.item(idx) : touches[idx]; }, _touches = function (e) { return e.touches && e.touches.length > 0 ? e.touches : e.changedTouches && e.changedTouches.length > 0 ? e.changedTouches : e.targetTouches && e.targetTouches.length > 0 ? e.targetTouches : [e]; }; var DragManager = function (_currentInstance) {
        var _draggables = {}, _dlist = [], _delements = {}, _elementsWithEndpoints = {}, _draggablesForElements = {}; this.register = function (el) {
            var id = _currentInstance.getId(el), parentOffset = jsPlumbAdapter.getOffset(el, _currentInstance); if (!_draggables[id]) { _draggables[id] = el; _dlist.push(el); _delements[id] = {}; }
            var _oneLevel = function (p, startOffset) {
                if (p) {
                    for (var i = 0; i < p.childNodes.length; i++) {
                        if (p.childNodes[i].nodeType != 3 && p.childNodes[i].nodeType != 8) {
                            var cEl = jsPlumb.getElementObject(p.childNodes[i]), cid = _currentInstance.getId(p.childNodes[i], null, true); if (cid && _elementsWithEndpoints[cid] && _elementsWithEndpoints[cid] > 0) { var cOff = jsPlumbAdapter.getOffset(cEl, _currentInstance); _delements[id][cid] = { id: cid, offset: { left: cOff.left - parentOffset.left, top: cOff.top - parentOffset.top} }; _draggablesForElements[cid] = id; }
                            _oneLevel(p.childNodes[i]);
                        } 
                    } 
                } 
            }; _oneLevel(el);
        }; this.updateOffsets = function (elId) { if (elId != null) { var domEl = jsPlumb.getDOMElement(elId), id = _currentInstance.getId(domEl), children = _delements[id], parentOffset = jsPlumbAdapter.getOffset(domEl, _currentInstance); if (children) { for (var i in children) { var cel = jsPlumb.getElementObject(i), cOff = jsPlumbAdapter.getOffset(cel, _currentInstance); _delements[id][i] = { id: i, offset: { left: cOff.left - parentOffset.left, top: cOff.top - parentOffset.top} }; _draggablesForElements[i] = id; } } } }; this.endpointAdded = function (el) {
            var b = document.body, id = _currentInstance.getId(el), cLoc = jsPlumbAdapter.getOffset(el, _currentInstance), p = el.parentNode, done = p == b; _elementsWithEndpoints[id] = _elementsWithEndpoints[id] ? _elementsWithEndpoints[id] + 1 : 1; while (p != null && p != b) {
                var pid = _currentInstance.getId(p, null, true); if (pid && _draggables[pid]) {
                    var idx = -1, pLoc = jsPlumbAdapter.getOffset(p, _currentInstance); if (_delements[pid][id] == null) { _delements[pid][id] = { id: id, offset: { left: cLoc.left - pLoc.left, top: cLoc.top - pLoc.top} }; _draggablesForElements[id] = pid; }
                    break;
                }
                p = p.parentNode;
            } 
        }; this.endpointDeleted = function (endpoint) { if (_elementsWithEndpoints[endpoint.elementId]) { _elementsWithEndpoints[endpoint.elementId]--; if (_elementsWithEndpoints[endpoint.elementId] <= 0) { for (var i in _delements) { if (_delements[i]) { delete _delements[i][endpoint.elementId]; delete _draggablesForElements[endpoint.elementId]; } } } } }; this.changeId = function (oldId, newId) { _delements[newId] = _delements[oldId]; _delements[oldId] = {}; _draggablesForElements[newId] = _draggablesForElements[oldId]; _draggablesForElements[oldId] = null; }; this.getElementsForDraggable = function (id) { return _delements[id]; }; this.elementRemoved = function (elementId) { var elId = _draggablesForElements[elementId]; if (elId) { delete _delements[elId][elementId]; delete _draggablesForElements[elementId]; } }; this.reset = function () { _draggables = {}; _dlist = []; _delements = {}; _elementsWithEndpoints = {}; }; this.dragEnded = function (el) { var id = _currentInstance.getId(el), ancestor = _draggablesForElements[id]; if (ancestor) this.updateOffsets(ancestor); }; this.setParent = function (el, elId, p, pId) {
            var current = _draggablesForElements[elId]; if (current) {
                if (!_delements[pId])
                    _delements[pId] = {}; _delements[pId][elId] = _delements[current][elId]; delete _delements[current][elId]; var pLoc = jsPlumbAdapter.getOffset(p, _currentInstance), cLoc = jsPlumbAdapter.getOffset(el, _currentInstance); _delements[pId][elId].offset = { left: cLoc.left - pLoc.left, top: cLoc.top - pLoc.top }; _draggablesForElements[elId] = pId;
            } 
        };
    }; if (!window.console)
        window.console = { time: function () { }, timeEnd: function () { }, group: function () { }, groupEnd: function () { }, log: function () { } }; var trim = function (str) { return str == null ? null : (str.replace(/^\s\s*/, '').replace(/\s\s*$/, '')); }, _setClassName = function (el, cn) {
            cn = trim(cn); if (typeof el.className.baseVal != "undefined")
                el.className.baseVal = cn; else
                el.className = cn;
        }, _getClassName = function (el) { return (typeof el.className.baseVal == "undefined") ? el.className : el.className.baseVal; }, _classManip = function (el, add, clazz) {
            var classesToAddOrRemove = clazz.split(/\s+/), className = _getClassName(el), curClasses = className.split(/\s+/); for (var i = 0; i < classesToAddOrRemove.length; i++) {
                if (add) {
                    if (jsPlumbUtil.indexOf(curClasses, classesToAddOrRemove[i]) == -1)
                        curClasses.push(classesToAddOrRemove[i]);
                }
                else {
                    var idx = jsPlumbUtil.indexOf(curClasses, classesToAddOrRemove[i]); if (idx != -1)
                        curClasses.splice(idx, 1);
                } 
            }
            _setClassName(el, curClasses.join(" "));
        }, _each = function (spec, fn) {
            if (spec == null) return; if (typeof spec === "string")
                fn(jsPlumb.getDOMElement(spec)); else if (spec.length != null) {
                for (var i = 0; i < spec.length; i++)
                    fn(jsPlumb.getDOMElement(spec[i]));
            }
            else
                fn(spec);
        }; window.jsPlumbAdapter = { headless: false, pageLocation: _pageLocation, screenLocation: _screenLocation, clientLocation: _clientLocation, getAttribute: function (el, attName) { return el.getAttribute(attName); }, setAttribute: function (el, a, v) { el.setAttribute(a, v); }, appendToRoot: function (node) { document.body.appendChild(node); }, getRenderModes: function () { return ["svg", "vml"]; }, isRenderModeAvailable: function (m) { return { "svg": svgAvailable, "vml": vmlAvailable()}[m]; }, getDragManager: function (_jsPlumb) { return new DragManager(_jsPlumb); }, setRenderMode: function (mode) {
            var renderMode; if (mode) {
                mode = mode.toLowerCase(); var svgAvailable = this.isRenderModeAvailable("svg"), vmlAvailable = this.isRenderModeAvailable("vml"); if (mode === "svg") { if (svgAvailable) renderMode = "svg"; else if (vmlAvailable) renderMode = "vml"; }
                else if (vmlAvailable) renderMode = "vml";
            }
            return renderMode;
        }, addClass: function (el, clazz) { _each(el, function (e) { _classManip(e, true, clazz); }); }, hasClass: function (el, clazz) { el = jsPlumb.getDOMElement(el); if (el.classList) return el.classList.contains(clazz); else { return _getClassName(el).indexOf(clazz) != -1; } }, removeClass: function (el, clazz) { _each(el, function (e) { _classManip(e, false, clazz); }); }, setClass: function (el, clazz) { _each(el, function (e) { _setClassName(e, clazz); }); }, setPosition: function (el, p) { el.style.left = p.left + "px"; el.style.top = p.top + "px"; }, getPosition: function (el) { var _one = function (prop) { var v = el.style[prop]; return v ? v.substring(0, v.length - 2) : 0; }; return { left: _one("left"), top: _one("top") }; }, getOffset: function (el, _instance, relativeToRoot) {
            el = jsPlumb.getDOMElement(el); var container = _instance.getContainer(); var l = el.offsetLeft, t = el.offsetTop, op = (relativeToRoot || (container != null && el.offsetParent != container)) ? el.offsetParent : null; while (op != null) { l += op.offsetLeft; t += op.offsetTop; op = relativeToRoot ? op.offsetParent : op.offsetParent == container ? null : op.offsetParent; }
            return { left: l, top: t };
        }, getPositionOnElement: function (evt, el, zoom) { var box = typeof el.getBoundingClientRect !== "undefined" ? el.getBoundingClientRect() : { left: 0, top: 0, width: 0, height: 0 }, body = document.body, docElem = document.documentElement, offPar = el.offsetParent, scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop, scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft, clientTop = docElem.clientTop || body.clientTop || 0, clientLeft = docElem.clientLeft || body.clientLeft || 0, pst = 0, psl = 0, top = box.top + scrollTop - clientTop + (pst * zoom), left = box.left + scrollLeft - clientLeft + (psl * zoom), cl = jsPlumbAdapter.pageLocation(evt), w = box.width || (el.offsetWidth * zoom), h = box.height || (el.offsetHeight * zoom), x = (cl[0] - left) / w, y = (cl[1] - top) / h; return [x, y]; } 
        };
})(); ; (function () {
    "use strict"; var _ju = jsPlumbUtil, _getOffset = function (el, _instance, relativeToRoot) { return jsPlumbAdapter.getOffset(el, _instance, relativeToRoot); }, _timestamp = function () { return "" + (new Date()).getTime(); }, _updateHoverStyle = function (component) {
        if (component._jsPlumb.paintStyle && component._jsPlumb.hoverPaintStyle) {
            var mergedHoverStyle = {}; jsPlumb.extend(mergedHoverStyle, component._jsPlumb.paintStyle); jsPlumb.extend(mergedHoverStyle, component._jsPlumb.hoverPaintStyle); delete component._jsPlumb.hoverPaintStyle; if (mergedHoverStyle.gradient && component._jsPlumb.paintStyle.fillStyle)
                delete mergedHoverStyle.gradient; component._jsPlumb.hoverPaintStyle = mergedHoverStyle;
        } 
    }, events = ["click", "dblclick", "mouseenter", "mouseout", "mousemove", "mousedown", "mouseup", "contextmenu"], eventFilters = { "mouseout": "mouseleave", "mouseexit": "mouseleave" }, _updateAttachedElements = function (component, state, timestamp, sourceElement) {
        var affectedElements = component.getAttachedElements(); if (affectedElements) {
            for (var i = 0, j = affectedElements.length; i < j; i++) {
                if (!sourceElement || sourceElement != affectedElements[i])
                    affectedElements[i].setHover(state, true, timestamp);
            } 
        } 
    }, _splitType = function (t) { return t == null ? null : t.split(" "); }, _applyTypes = function (component, params, doNotRepaint) {
        if (component.getDefaultType) {
            var td = component.getTypeDescriptor(); var o = _ju.merge({}, component.getDefaultType()); for (var i = 0, j = component._jsPlumb.types.length; i < j; i++)
                o = _ju.merge(o, component._jsPlumb.instance.getType(component._jsPlumb.types[i], td)); if (params) { o = _ju.populate(o, params); }
            component.applyType(o, doNotRepaint); if (!doNotRepaint) component.repaint();
        } 
    }, jsPlumbUIComponent = window.jsPlumbUIComponent = function (params) {
        jsPlumbUtil.EventGenerator.apply(this, arguments); var self = this, a = arguments, idPrefix = self.idPrefix, id = idPrefix + (new Date()).getTime(); this._jsPlumb = { instance: params._jsPlumb, parameters: params.parameters || {}, paintStyle: null, hoverPaintStyle: null, paintStyleInUse: null, hover: false, beforeDetach: params.beforeDetach, beforeDrop: params.beforeDrop, overlayPlacements: [], hoverClass: params.hoverClass || params._jsPlumb.Defaults.HoverClass, types: [] }; this.getId = function () { return id; }; if (params.events) {
            for (var i in params.events)
                self.bind(i, params.events[i]);
        }
        this.clone = function () { var o = {}; this.constructor.apply(o, a); return o; } .bind(this); this.isDetachAllowed = function (connection) {
            var r = true; if (this._jsPlumb.beforeDetach) {
                try { r = this._jsPlumb.beforeDetach(connection); }
                catch (e) { _ju.log("jsPlumb: beforeDetach callback failed", e); } 
            }
            return r;
        }; this.isDropAllowed = function (sourceId, targetId, scope, connection, dropEndpoint, source, target) {
            var r = this._jsPlumb.instance.checkCondition("beforeDrop", { sourceId: sourceId, targetId: targetId, scope: scope, connection: connection, dropEndpoint: dropEndpoint, source: source, target: target }); if (this._jsPlumb.beforeDrop) {
                try { r = this._jsPlumb.beforeDrop({ sourceId: sourceId, targetId: targetId, scope: scope, connection: connection, dropEndpoint: dropEndpoint, source: source, target: target }); }
                catch (e) { _ju.log("jsPlumb: beforeDrop callback failed", e); } 
            }
            return r;
        }; var boundListeners = [], bindAListener = function (obj, type, fn) { boundListeners.push([obj, type, fn]); obj.bind(type, fn); }, domListeners = [], bindOne = function (o, c, evt, override) { var filteredEvent = eventFilters[evt] || evt, fn = function (ee) { if (override && override(ee) === false) return; c.fire(filteredEvent, c, ee); }; domListeners.push([o, evt, fn, c]); c._jsPlumb.instance.on(o, evt, fn); }, unbindOne = function (o, evt, fn, c) { var filteredEvent = eventFilters[evt] || evt; c._jsPlumb.instance.off(o, evt, fn); }; this.bindListeners = function (obj, _self, _hoverFunction) { bindAListener(obj, "click", function (ep, e) { _self.fire("click", _self, e); }); bindAListener(obj, "dblclick", function (ep, e) { _self.fire("dblclick", _self, e); }); bindAListener(obj, "contextmenu", function (ep, e) { _self.fire("contextmenu", _self, e); }); bindAListener(obj, "mouseleave", function (ep, e) { if (_self.isHover()) { _hoverFunction(false); _self.fire("mouseleave", _self, e); } }); bindAListener(obj, "mouseenter", function (ep, e) { if (!_self.isHover()) { _hoverFunction(true); _self.fire("mouseenter", _self, e); } }); bindAListener(obj, "mousedown", function (ep, e) { _self.fire("mousedown", _self, e); }); bindAListener(obj, "mouseup", function (ep, e) { _self.fire("mouseup", _self, e); }); }; this.unbindListeners = function () {
            for (var i = 0; i < boundListeners.length; i++) { var o = boundListeners[i]; o[0].unbind(o[1], o[2]); }
            boundListeners = null;
        }; this.attachListeners = function (o, c, overrides) { overrides = overrides || {}; for (var i = 0, j = events.length; i < j; i++) { bindOne(o, c, events[i], overrides[events[i]]); } }; this.detachListeners = function () {
            for (var i = 0; i < domListeners.length; i++) { unbindOne(domListeners[i][0], domListeners[i][1], domListeners[i][2], domListeners[i][3]); }
            domListeners = null;
        }; this.reattachListenersForElement = function (o) {
            if (arguments.length > 1) {
                for (var i = 0, j = events.length; i < j; i++)
                    unbindOne(o, events[i]); for (i = 1, j = arguments.length; i < j; i++)
                    this.attachListeners(o, arguments[i]);
            } 
        };
    }; jsPlumbUtil.extend(jsPlumbUIComponent, jsPlumbUtil.EventGenerator, { getParameter: function (name) { return this._jsPlumb.parameters[name]; }, setParameter: function (name, value) { this._jsPlumb.parameters[name] = value; }, getParameters: function () { return this._jsPlumb.parameters; }, setParameters: function (p) { this._jsPlumb.parameters = p; }, addClass: function (clazz) { jsPlumbAdapter.addClass(this.canvas, clazz); }, removeClass: function (clazz) { jsPlumbAdapter.removeClass(this.canvas, clazz); }, setType: function (typeId, params, doNotRepaint) { this._jsPlumb.types = _splitType(typeId) || []; _applyTypes(this, params, doNotRepaint); }, getType: function () { return this._jsPlumb.types; }, reapplyTypes: function (params, doNotRepaint) { _applyTypes(this, params, doNotRepaint); }, hasType: function (typeId) { return jsPlumbUtil.indexOf(this._jsPlumb.types, typeId) != -1; }, addType: function (typeId, params, doNotRepaint) {
        var t = _splitType(typeId), _cont = false; if (t != null) {
            for (var i = 0, j = t.length; i < j; i++) { if (!this.hasType(t[i])) { this._jsPlumb.types.push(t[i]); _cont = true; } }
            if (_cont) _applyTypes(this, params, doNotRepaint);
        } 
    }, removeType: function (typeId, doNotRepaint) {
        var t = _splitType(typeId), _cont = false, _one = function (tt) {
            var idx = _ju.indexOf(this._jsPlumb.types, tt); if (idx != -1) { this._jsPlumb.types.splice(idx, 1); return true; }
            return false;
        } .bind(this); if (t != null) {
            for (var i = 0, j = t.length; i < j; i++) { _cont = _one(t[i]) || _cont; }
            if (_cont) _applyTypes(this, null, doNotRepaint);
        } 
    }, toggleType: function (typeId, params, doNotRepaint) {
        var t = _splitType(typeId); if (t != null) {
            for (var i = 0, j = t.length; i < j; i++) {
                var idx = jsPlumbUtil.indexOf(this._jsPlumb.types, t[i]); if (idx != -1)
                    this._jsPlumb.types.splice(idx, 1); else
                    this._jsPlumb.types.push(t[i]);
            }
            _applyTypes(this, params, doNotRepaint);
        } 
    }, applyType: function (t, doNotRepaint) {
        this.setPaintStyle(t.paintStyle, doNotRepaint); this.setHoverPaintStyle(t.hoverPaintStyle, doNotRepaint); if (t.parameters) {
            for (var i in t.parameters)
                this.setParameter(i, t.parameters[i]);
        } 
    }, setPaintStyle: function (style, doNotRepaint) { this._jsPlumb.paintStyle = style; this._jsPlumb.paintStyleInUse = this._jsPlumb.paintStyle; _updateHoverStyle(this); if (!doNotRepaint) this.repaint(); }, getPaintStyle: function () { return this._jsPlumb.paintStyle; }, setHoverPaintStyle: function (style, doNotRepaint) { this._jsPlumb.hoverPaintStyle = style; _updateHoverStyle(this); if (!doNotRepaint) this.repaint(); }, getHoverPaintStyle: function () { return this._jsPlumb.hoverPaintStyle; }, cleanup: function () { this.unbindListeners(); this.detachListeners(); }, destroy: function () { this.cleanupListeners(); this.clone = null; this._jsPlumb = null; }, isHover: function () { return this._jsPlumb.hover; }, setHover: function (hover, ignoreAttachedElements, timestamp) {
        if (this._jsPlumb && !this._jsPlumb.instance.currentlyDragging && !this._jsPlumb.instance.isHoverSuspended()) {
            this._jsPlumb.hover = hover; if (this.canvas != null) {
                if (this._jsPlumb.instance.hoverClass != null) { var method = hover ? "addClass" : "removeClass"; this._jsPlumb.instance[method](this.canvas, this._jsPlumb.instance.hoverClass); }
                if (this._jsPlumb.hoverClass != null) { this._jsPlumb.instance[method](this.canvas, this._jsPlumb.hoverClass); } 
            }
            if (this._jsPlumb.hoverPaintStyle != null) { this._jsPlumb.paintStyleInUse = hover ? this._jsPlumb.hoverPaintStyle : this._jsPlumb.paintStyle; if (!this._jsPlumb.instance.isSuspendDrawing()) { timestamp = timestamp || _timestamp(); this.repaint({ timestamp: timestamp, recalc: false }); } }
            if (this.getAttachedElements && !ignoreAttachedElements)
                _updateAttachedElements(this, hover, _timestamp(), this);
        } 
    } 
    }); var _internalLabelOverlayId = "__label", _getOverlayIndex = function (component, id) {
        var idx = -1; for (var i = 0, j = component._jsPlumb.overlays.length; i < j; i++) { if (id === component._jsPlumb.overlays[i].id) { idx = i; break; } }
        return idx;
    }, _makeLabelOverlay = function (component, params) { var _params = { cssClass: params.cssClass, labelStyle: component.labelStyle, id: _internalLabelOverlayId, component: component, _jsPlumb: component._jsPlumb.instance }, mergedParams = jsPlumb.extend(_params, params); return new jsPlumb.Overlays[component._jsPlumb.instance.getRenderMode()].Label(mergedParams); }, _processOverlay = function (component, o) {
        var _newOverlay = null; if (_ju.isArray(o)) { var type = o[0], p = jsPlumb.extend({ component: component, _jsPlumb: component._jsPlumb.instance }, o[1]); if (o.length == 3) jsPlumb.extend(p, o[2]); _newOverlay = new jsPlumb.Overlays[component._jsPlumb.instance.getRenderMode()][type](p); } else if (o.constructor == String) { _newOverlay = new jsPlumb.Overlays[component._jsPlumb.instance.getRenderMode()][o]({ component: component, _jsPlumb: component._jsPlumb.instance }); } else { _newOverlay = o; }
        component._jsPlumb.overlays.push(_newOverlay);
    }, _calculateOverlaysToAdd = function (component, params) {
        var defaultKeys = component.defaultOverlayKeys || [], o = params.overlays, checkKey = function (k) { return component._jsPlumb.instance.Defaults[k] || jsPlumb.Defaults[k] || []; }; if (!o) o = []; for (var i = 0, j = defaultKeys.length; i < j; i++)
            o.unshift.apply(o, checkKey(defaultKeys[i])); return o;
    }, OverlayCapableJsPlumbUIComponent = window.OverlayCapableJsPlumbUIComponent = function (params) {
        jsPlumbUIComponent.apply(this, arguments); this._jsPlumb.overlays = []; var _overlays = _calculateOverlaysToAdd(this, params); if (_overlays) { for (var i = 0, j = _overlays.length; i < j; i++) { _processOverlay(this, _overlays[i]); } }
        if (params.label) { var loc = params.labelLocation || this.defaultLabelLocation || 0.5, labelStyle = params.labelStyle || this._jsPlumb.instance.Defaults.LabelStyle; this._jsPlumb.overlays.push(_makeLabelOverlay(this, { label: params.label, location: loc, labelStyle: labelStyle })); } 
    }; jsPlumbUtil.extend(OverlayCapableJsPlumbUIComponent, jsPlumbUIComponent, { applyType: function (t, doNotRepaint) {
        this.removeAllOverlays(doNotRepaint); if (t.overlays) {
            for (var i = 0, j = t.overlays.length; i < j; i++)
                this.addOverlay(t.overlays[i], true);
        } 
    }, setHover: function (hover, ignoreAttachedElements, timestamp) { if (this._jsPlumb && !this._jsPlumb.instance.isConnectionBeingDragged()) { for (var i = 0, j = this._jsPlumb.overlays.length; i < j; i++) { this._jsPlumb.overlays[i][hover ? "addClass" : "removeClass"](this._jsPlumb.instance.hoverClass); } } }, addOverlay: function (overlay, doNotRepaint) { _processOverlay(this, overlay); if (!doNotRepaint) this.repaint(); }, getOverlay: function (id) { var idx = _getOverlayIndex(this, id); return idx >= 0 ? this._jsPlumb.overlays[idx] : null; }, getOverlays: function () { return this._jsPlumb.overlays; }, hideOverlay: function (id) { var o = this.getOverlay(id); if (o) o.hide(); }, hideOverlays: function () {
        for (var i = 0, j = this._jsPlumb.overlays.length; i < j; i++)
            this._jsPlumb.overlays[i].hide();
    }, showOverlay: function (id) { var o = this.getOverlay(id); if (o) o.show(); }, showOverlays: function () {
        for (var i = 0, j = this._jsPlumb.overlays.length; i < j; i++)
            this._jsPlumb.overlays[i].show();
    }, removeAllOverlays: function (doNotRepaint) {
        for (var i = 0, j = this._jsPlumb.overlays.length; i < j; i++) { if (this._jsPlumb.overlays[i].cleanup) this._jsPlumb.overlays[i].cleanup(); }
        this._jsPlumb.overlays.splice(0, this._jsPlumb.overlays.length); this._jsPlumb.overlayPositions = null; if (!doNotRepaint)
            this.repaint();
    }, removeOverlay: function (overlayId) {
        var idx = _getOverlayIndex(this, overlayId); if (idx != -1) {
            var o = this._jsPlumb.overlays[idx]; if (o.cleanup) o.cleanup(); this._jsPlumb.overlays.splice(idx, 1); if (this._jsPlumb.overlayPositions)
                delete this._jsPlumb.overlayPositions[overlayId];
        } 
    }, removeOverlays: function () {
        for (var i = 0, j = arguments.length; i < j; i++)
            this.removeOverlay(arguments[i]);
    }, moveParent: function (newParent) {
        if (this.bgCanvas) { this.bgCanvas.parentNode.removeChild(this.bgCanvas); newParent.appendChild(this.bgCanvas); }
        this.canvas.parentNode.removeChild(this.canvas); newParent.appendChild(this.canvas); for (var i = 0; i < this._jsPlumb.overlays.length; i++) { if (this._jsPlumb.overlays[i].isAppendedAtTopLevel) { this._jsPlumb.overlays[i].canvas.parentNode.removeChild(this._jsPlumb.overlays[i].canvas); newParent.appendChild(this._jsPlumb.overlays[i].canvas); } } 
    }, getLabel: function () { var lo = this.getOverlay(_internalLabelOverlayId); return lo != null ? lo.getLabel() : null; }, getLabelOverlay: function () { return this.getOverlay(_internalLabelOverlayId); }, setLabel: function (l) {
        var lo = this.getOverlay(_internalLabelOverlayId); if (!lo) { var params = l.constructor == String || l.constructor == Function ? { label: l} : l; lo = _makeLabelOverlay(this, params); this._jsPlumb.overlays.push(lo); }
        else { if (l.constructor == String || l.constructor == Function) lo.setLabel(l); else { if (l.label) lo.setLabel(l.label); if (l.location) lo.setLocation(l.location); } }
        if (!this._jsPlumb.instance.isSuspendDrawing())
            this.repaint();
    }, cleanup: function () {
        for (var i = 0; i < this._jsPlumb.overlays.length; i++) { this._jsPlumb.overlays[i].cleanup(); this._jsPlumb.overlays[i].destroy(); }
        this._jsPlumb.overlays.splice(0); this._jsPlumb.overlayPositions = null;
    }, setVisible: function (v) { this[v ? "showOverlays" : "hideOverlays"](); }, setAbsoluteOverlayPosition: function (overlay, xy) { this._jsPlumb.overlayPositions = this._jsPlumb.overlayPositions || {}; this._jsPlumb.overlayPositions[overlay.id] = xy; }, getAbsoluteOverlayPosition: function (overlay) { return this._jsPlumb.overlayPositions ? this._jsPlumb.overlayPositions[overlay.id] : null; } 
    }); var _jsPlumbInstanceIndex = 0, getInstanceIndex = function () { var i = _jsPlumbInstanceIndex + 1; _jsPlumbInstanceIndex++; return i; }; var jsPlumbInstance = window.jsPlumbInstance = function (_defaults) {
        this.Defaults = { Anchor: "BottomCenter", Anchors: [null, null], ConnectionsDetachable: true, ConnectionOverlays: [], Connector: "Bezier", Container: null, DoNotThrowErrors: false, DragOptions: {}, DropOptions: {}, Endpoint: "Dot", EndpointOverlays: [], Endpoints: [null, null], EndpointStyle: { fillStyle: "#456" }, EndpointStyles: [null, null], EndpointHoverStyle: null, EndpointHoverStyles: [null, null], HoverPaintStyle: null, LabelStyle: { color: "black" }, LogEnabled: false, Overlays: [], MaxConnections: 1, PaintStyle: { lineWidth: 8, strokeStyle: "#456" }, ReattachConnections: false, RenderMode: "svg", Scope: "jsPlumb_DefaultScope" }; if (_defaults) jsPlumb.extend(this.Defaults, _defaults); this.logEnabled = this.Defaults.LogEnabled; this._connectionTypes = {}; this._endpointTypes = {}; jsPlumbUtil.EventGenerator.apply(this); var _currentInstance = this, _instanceIndex = getInstanceIndex(), _bb = _currentInstance.bind, _initialDefaults = {}, _zoom = 1, _info = function (el) { var _el = _currentInstance.getDOMElement(el); return { el: _el, id: (jsPlumbUtil.isString(el) && _el == null) ? el : _getId(_el) }; }; this.getInstanceIndex = function () { return _instanceIndex; }; this.setZoom = function (z, repaintEverything) {
            if (!jsPlumbUtil.oldIE) { _zoom = z; _currentInstance.fire("zoom", _zoom); if (repaintEverything) _currentInstance.repaintEverything(); }
            return !jsPlumbUtil.oldIE;
        }; this.getZoom = function () { return _zoom; }; for (var i in this.Defaults)
            _initialDefaults[i] = this.Defaults[i]; var _container; this.setContainer = function (c) { c = this.getDOMElement(c); this.select().each(function (conn) { conn.moveParent(c); }); this.selectEndpoints().each(function (ep) { ep.moveParent(c); }); _container = c; }; this.getContainer = function () { return _container; }; this.bind = function (event, fn) { if ("ready" === event && initialized) fn(); else _bb.apply(_currentInstance, [event, fn]); }; _currentInstance.importDefaults = function (d) {
                for (var i in d) { _currentInstance.Defaults[i] = d[i]; }
                if (d.Container)
                    this.setContainer(d.Container); return _currentInstance;
            }; _currentInstance.restoreDefaults = function () { _currentInstance.Defaults = jsPlumb.extend({}, _initialDefaults); return _currentInstance; }; var log = null, resizeTimer = null, initialized = false, connections = [], endpointsByElement = {}, endpointsByUUID = {}, offsets = {}, offsetTimestamps = {}, floatingConnections = {}, draggableStates = {}, connectionBeingDragged = false, sizes = [], _suspendDrawing = false, _suspendedAt = null, DEFAULT_SCOPE = this.Defaults.Scope, renderMode = null, _curIdStamp = 1, _idstamp = function () { return "" + _curIdStamp++; }, _appendElement = function (el, parent) {
                if (_container)
                    _container.appendChild(el); else if (!parent)
                    _currentInstance.appendToRoot(el); else
                    jsPlumb.getDOMElement(parent).appendChild(el);
            }, _convertYUICollection = function (c) { return c._nodes ? c._nodes : c; }, _draw = function (element, ui, timestamp, clearEdits) {
                if (!jsPlumbAdapter.headless && !_suspendDrawing) {
                    var id = _getId(element), repaintEls = _currentInstance.dragManager.getElementsForDraggable(id); if (timestamp == null) timestamp = _timestamp(); var o = _updateOffset({ elId: id, offset: ui, recalc: false, timestamp: timestamp }); if (repaintEls) { for (var i in repaintEls) { _updateOffset({ elId: repaintEls[i].id, offset: { left: o.o.left + repaintEls[i].offset.left, top: o.o.top + repaintEls[i].offset.top }, recalc: false, timestamp: timestamp }); } }
                    _currentInstance.anchorManager.redraw(id, ui, timestamp, null, clearEdits); if (repaintEls) { for (var j in repaintEls) { _currentInstance.anchorManager.redraw(repaintEls[j].id, ui, timestamp, repaintEls[j].offset, clearEdits, true); } } 
                } 
            }, _elementProxy = function (element, fn) {
                var retVal = null, el, id, del; if (_ju.isArray(element)) { retVal = []; for (var i = 0, j = element.length; i < j; i++) { el = _currentInstance.getElementObject(element[i]); del = _currentInstance.getDOMElement(el); id = _currentInstance.getAttribute(del, "id"); retVal.push(fn.apply(_currentInstance, [del, id])); } } else { el = _currentInstance.getDOMElement(element); id = _currentInstance.getId(el); retVal = fn.apply(_currentInstance, [el, id]); }
                return retVal;
            }, _getEndpoint = function (uuid) { return endpointsByUUID[uuid]; }, _initDraggableIfNecessary = function (element, isDraggable, dragOptions) { if (!jsPlumbAdapter.headless) { var _draggable = isDraggable == null ? false : isDraggable; if (_draggable) { if (jsPlumb.isDragSupported(element, _currentInstance) && !jsPlumb.isAlreadyDraggable(element, _currentInstance)) { var options = dragOptions || _currentInstance.Defaults.DragOptions; options = jsPlumb.extend({}, options); var dragEvent = jsPlumb.dragEvents.drag, stopEvent = jsPlumb.dragEvents.stop, startEvent = jsPlumb.dragEvents.start; options[startEvent] = _ju.wrap(options[startEvent], function () { _currentInstance.setHoverSuspended(true); _currentInstance.select({ source: element }).addClass(_currentInstance.elementDraggingClass + " " + _currentInstance.sourceElementDraggingClass, true); _currentInstance.select({ target: element }).addClass(_currentInstance.elementDraggingClass + " " + _currentInstance.targetElementDraggingClass, true); _currentInstance.setConnectionBeingDragged(true); if (options.canDrag) return dragOptions.canDrag(); }, false); options[dragEvent] = _ju.wrap(options[dragEvent], function () { var ui = _currentInstance.getUIPosition(arguments, _currentInstance.getZoom()); _draw(element, ui, null, true); _currentInstance.addClass(element, "jsPlumb_dragged"); }); options[stopEvent] = _ju.wrap(options[stopEvent], function () { var ui = _currentInstance.getUIPosition(arguments, _currentInstance.getZoom(), true); _draw(element, ui); _currentInstance.removeClass(element, "jsPlumb_dragged"); _currentInstance.setHoverSuspended(false); _currentInstance.select({ source: element }).removeClass(_currentInstance.elementDraggingClass + " " + _currentInstance.sourceElementDraggingClass, true); _currentInstance.select({ target: element }).removeClass(_currentInstance.elementDraggingClass + " " + _currentInstance.targetElementDraggingClass, true); _currentInstance.setConnectionBeingDragged(false); _currentInstance.dragManager.dragEnded(element); }); var elId = _getId(element); draggableStates[elId] = true; var draggable = draggableStates[elId]; options.disabled = draggable == null ? false : !draggable; _currentInstance.initDraggable(element, options, false); _currentInstance.dragManager.register(element); } } } }, _prepareConnectionParams = function (params, referenceParams) {
                var _p = jsPlumb.extend({}, params); if (referenceParams) jsPlumb.extend(_p, referenceParams); if (_p.source) {
                    if (_p.source.endpoint)
                        _p.sourceEndpoint = _p.source; else
                        _p.source = _currentInstance.getDOMElement(_p.source);
                }
                if (_p.target) {
                    if (_p.target.endpoint)
                        _p.targetEndpoint = _p.target; else
                        _p.target = _currentInstance.getDOMElement(_p.target);
                }
                if (params.uuids) { _p.sourceEndpoint = _getEndpoint(params.uuids[0]); _p.targetEndpoint = _getEndpoint(params.uuids[1]); }
                if (_p.sourceEndpoint && _p.sourceEndpoint.isFull()) { _ju.log(_currentInstance, "could not add connection; source endpoint is full"); return; }
                if (_p.targetEndpoint && _p.targetEndpoint.isFull()) { _ju.log(_currentInstance, "could not add connection; target endpoint is full"); return; }
                if (!_p.type && _p.sourceEndpoint)
                    _p.type = _p.sourceEndpoint.connectionType; if (_p.sourceEndpoint && _p.sourceEndpoint.connectorOverlays) { _p.overlays = _p.overlays || []; for (var i = 0, j = _p.sourceEndpoint.connectorOverlays.length; i < j; i++) { _p.overlays.push(_p.sourceEndpoint.connectorOverlays[i]); } }
                if (!_p["pointer-events"] && _p.sourceEndpoint && _p.sourceEndpoint.connectorPointerEvents)
                    _p["pointer-events"] = _p.sourceEndpoint.connectorPointerEvents; var tid, tep, existingUniqueEndpoint, newEndpoint; if (_p.target && !_p.target.endpoint && !_p.targetEndpoint && !_p.newConnection) { tid = _getId(_p.target); tep = this.targetEndpointDefinitions[tid]; if (tep) { if (!tep.enabled) return; tep.isTarget = true; newEndpoint = tep.endpoint != null && tep.endpoint._jsPlumb ? tep.endpoint : _currentInstance.addEndpoint(_p.target, tep.def); if (tep.uniqueEndpoint) tep.endpoint = newEndpoint; _p.targetEndpoint = newEndpoint; newEndpoint._doNotDeleteOnDetach = false; newEndpoint._deleteOnDetach = true; } }
                if (_p.source && !_p.source.endpoint && !_p.sourceEndpoint && !_p.newConnection) { tid = _getId(_p.source); tep = this.sourceEndpointDefinitions[tid]; if (tep) { if (!tep.enabled) return; newEndpoint = tep.endpoint != null && tep.endpoint._jsPlumb ? tep.endpoint : _currentInstance.addEndpoint(_p.source, tep.def); if (tep.uniqueEndpoint) tep.endpoint = newEndpoint; _p.sourceEndpoint = newEndpoint; newEndpoint._doNotDeleteOnDetach = false; newEndpoint._deleteOnDetach = true; } }
                return _p;
            } .bind(_currentInstance), _newConnection = function (params) {
                var connectionFunc = _currentInstance.Defaults.ConnectionType || _currentInstance.getDefaultConnectionType(), endpointFunc = _currentInstance.Defaults.EndpointType || jsPlumb.Endpoint; params._jsPlumb = _currentInstance; params.newConnection = _newConnection; params.newEndpoint = _newEndpoint; params.endpointsByUUID = endpointsByUUID; params.endpointsByElement = endpointsByElement; params.finaliseConnection = _finaliseConnection; var con = new connectionFunc(params); con.id = "con_" + _idstamp(); _eventFireProxy("click", "click", con); _eventFireProxy("dblclick", "dblclick", con); _eventFireProxy("contextmenu", "contextmenu", con); if (con.isDetachable()) { con.endpoints[0].initDraggable(); con.endpoints[1].initDraggable(); }
                return con;
            }, _finaliseConnection = function (jpc, params, originalEvent, doInformAnchorManager) {
                params = params || {}; if (!jpc.suspendedEndpoint)
                    connections.push(jpc); if (jpc.suspendedEndpoint == null || doInformAnchorManager)
                    _currentInstance.anchorManager.newConnection(jpc); _draw(jpc.source); if (!params.doNotFireConnectionEvent && params.fireEvent !== false) { var eventArgs = { connection: jpc, source: jpc.source, target: jpc.target, sourceId: jpc.sourceId, targetId: jpc.targetId, sourceEndpoint: jpc.endpoints[0], targetEndpoint: jpc.endpoints[1] }; _currentInstance.fire("connection", eventArgs, originalEvent); } 
            }, _eventFireProxy = function (event, proxyEvent, obj) { obj.bind(event, function (originalObject, originalEvent) { _currentInstance.fire(proxyEvent, obj, originalEvent); }); }, _newEndpoint = function (params) {
                var endpointFunc = _currentInstance.Defaults.EndpointType || jsPlumb.Endpoint; var _p = jsPlumb.extend({}, params); _p._jsPlumb = _currentInstance; _p.newConnection = _newConnection; _p.newEndpoint = _newEndpoint; _p.endpointsByUUID = endpointsByUUID; _p.endpointsByElement = endpointsByElement; _p.finaliseConnection = _finaliseConnection; _p.fireDetachEvent = fireDetachEvent; _p.fireMoveEvent = fireMoveEvent; _p.floatingConnections = floatingConnections; _p.elementId = _getId(_p.source); var ep = new endpointFunc(_p); ep.id = "ep_" + _idstamp(); _eventFireProxy("click", "endpointClick", ep); _eventFireProxy("dblclick", "endpointDblClick", ep); _eventFireProxy("contextmenu", "contextmenu", ep); if (!jsPlumbAdapter.headless)
                    _currentInstance.dragManager.endpointAdded(_p.source); return ep;
            }, _operation = function (elId, func, endpointFunc) {
                var endpoints = endpointsByElement[elId]; if (endpoints && endpoints.length) {
                    for (var i = 0, ii = endpoints.length; i < ii; i++) {
                        for (var j = 0, jj = endpoints[i].connections.length; j < jj; j++) { var retVal = func(endpoints[i].connections[j]); if (retVal) return; }
                        if (endpointFunc) endpointFunc(endpoints[i]);
                    } 
                } 
            }, _setDraggable = function (element, draggable) { return _elementProxy(element, function (el, id) { draggableStates[id] = draggable; if (this.isDragSupported(el)) { this.setElementDraggable(el, draggable); } }); }, _setVisible = function (el, state, alsoChangeEndpoints) {
                state = state === "block"; var endpointFunc = null; if (alsoChangeEndpoints) { if (state) endpointFunc = function (ep) { ep.setVisible(true, true, true); }; else endpointFunc = function (ep) { ep.setVisible(false, true, true); }; }
                var info = _info(el); _operation(info.id, function (jpc) {
                    if (state && alsoChangeEndpoints) { var oidx = jpc.sourceId === info.id ? 1 : 0; if (jpc.endpoints[oidx].isVisible()) jpc.setVisible(true); }
                    else
                        jpc.setVisible(state);
                }, endpointFunc);
            }, _toggleDraggable = function (el) { return _elementProxy(el, function (el, elId) { var state = draggableStates[elId] == null ? false : draggableStates[elId]; state = !state; draggableStates[elId] = state; this.setDraggable(el, state); return state; }); }, _toggleVisible = function (elId, changeEndpoints) {
                var endpointFunc = null; if (changeEndpoints) { endpointFunc = function (ep) { var state = ep.isVisible(); ep.setVisible(!state); }; }
                _operation(elId, function (jpc) { var state = jpc.isVisible(); jpc.setVisible(!state); }, endpointFunc);
            }, _updateOffset = this.updateOffset = function (params) {
                var timestamp = params.timestamp, recalc = params.recalc, offset = params.offset, elId = params.elId, s; if (_suspendDrawing && !timestamp) timestamp = _suspendedAt; if (!recalc) { if (timestamp && timestamp === offsetTimestamps[elId]) { return { o: params.offset || offsets[elId], s: sizes[elId] }; } }
                if (recalc || !offset) { s = document.getElementById(elId); if (s != null) { sizes[elId] = _currentInstance.getSize(s); offsets[elId] = _getOffset(s, _currentInstance); offsetTimestamps[elId] = timestamp; } } else {
                    offsets[elId] = offset; if (sizes[elId] == null) { s = document.getElementById(elId); if (s != null) sizes[elId] = _currentInstance.getSize(s); }
                    offsetTimestamps[elId] = timestamp;
                }
                if (offsets[elId] && !offsets[elId].right) { offsets[elId].right = offsets[elId].left + sizes[elId][0]; offsets[elId].bottom = offsets[elId].top + sizes[elId][1]; offsets[elId].width = sizes[elId][0]; offsets[elId].height = sizes[elId][1]; offsets[elId].centerx = offsets[elId].left + (offsets[elId].width / 2); offsets[elId].centery = offsets[elId].top + (offsets[elId].height / 2); }
                return { o: offsets[elId], s: sizes[elId] };
            }, _getCachedData = function (elId) {
                var o = offsets[elId]; if (!o)
                    return _updateOffset({ elId: elId }); else
                    return { o: o, s: sizes[elId] };
            }, _getId = function (element, uuid, doNotCreateIfNotFound) {
                if (jsPlumbUtil.isString(element)) return element; if (element == null) return null; var id = _currentInstance.getAttribute(element, "id"); if (!id || id === "undefined") {
                    if (arguments.length == 2 && arguments[1] !== undefined)
                        id = uuid; else if (arguments.length == 1 || (arguments.length == 3 && !arguments[2]))
                        id = "jsPlumb_" + _instanceIndex + "_" + _idstamp(); if (!doNotCreateIfNotFound) _currentInstance.setAttribute(element, "id", id);
                }
                return id;
            }; this.setConnectionBeingDragged = function (v) { connectionBeingDragged = v; }; this.isConnectionBeingDragged = function () { return connectionBeingDragged; }; this.connectorClass = "_jsPlumb_connector"; this.hoverClass = "_jsPlumb_hover"; this.endpointClass = "_jsPlumb_endpoint"; this.endpointConnectedClass = "_jsPlumb_endpoint_connected"; this.endpointFullClass = "_jsPlumb_endpoint_full"; this.endpointDropAllowedClass = "_jsPlumb_endpoint_drop_allowed"; this.endpointDropForbiddenClass = "_jsPlumb_endpoint_drop_forbidden"; this.overlayClass = "_jsPlumb_overlay"; this.draggingClass = "_jsPlumb_dragging"; this.elementDraggingClass = "_jsPlumb_element_dragging"; this.sourceElementDraggingClass = "_jsPlumb_source_element_dragging"; this.targetElementDraggingClass = "_jsPlumb_target_element_dragging"; this.endpointAnchorClassPrefix = "_jsPlumb_endpoint_anchor"; this.hoverSourceClass = "_jsPlumb_source_hover"; this.hoverTargetClass = "_jsPlumb_target_hover"; this.dragSelectClass = "_jsPlumb_drag_select"; this.Anchors = {}; this.Connectors = { "svg": {}, "vml": {} }; this.Endpoints = { "svg": {}, "vml": {} }; this.Overlays = { "svg": {}, "vml": {} }; this.ConnectorRenderers = {}; this.SVG = "svg"; this.VML = "vml"; this.addEndpoint = function (el, params, referenceParams) {
                referenceParams = referenceParams || {}; var p = jsPlumb.extend({}, referenceParams); jsPlumb.extend(p, params); p.endpoint = p.endpoint || _currentInstance.Defaults.Endpoint; p.paintStyle = p.paintStyle || _currentInstance.Defaults.EndpointStyle; el = _convertYUICollection(el); var results = [], inputs = (_ju.isArray(el) || (el.length != null && !_ju.isString(el))) ? el : [el]; for (var i = 0, j = inputs.length; i < j; i++) { var _el = _currentInstance.getDOMElement(inputs[i]), id = _getId(_el); p.source = _el; _ensureContainer(p.source); _updateOffset({ elId: id, timestamp: _suspendedAt }); var e = _newEndpoint(p); if (p.parentAnchor) e.parentAnchor = p.parentAnchor; _ju.addToList(endpointsByElement, id, e); var myOffset = offsets[id], myWH = sizes[id], anchorLoc = e.anchor.compute({ xy: [myOffset.left, myOffset.top], wh: myWH, element: e, timestamp: _suspendedAt }), endpointPaintParams = { anchorLoc: anchorLoc, timestamp: _suspendedAt }; if (_suspendDrawing) endpointPaintParams.recalc = false; if (!_suspendDrawing) e.paint(endpointPaintParams); results.push(e); e._doNotDeleteOnDetach = true; }
                return results.length == 1 ? results[0] : results;
            }; this.addEndpoints = function (el, endpoints, referenceParams) {
                var results = []; for (var i = 0, j = endpoints.length; i < j; i++) {
                    var e = _currentInstance.addEndpoint(el, endpoints[i], referenceParams); if (_ju.isArray(e))
                        Array.prototype.push.apply(results, e); else results.push(e);
                }
                return results;
            }; this.animate = function (el, properties, options) { options = options || {}; var ele = this.getElementObject(el), del = this.getDOMElement(el), id = _getId(del), stepFunction = jsPlumb.animEvents.step, completeFunction = jsPlumb.animEvents.complete; options[stepFunction] = _ju.wrap(options[stepFunction], function () { _currentInstance.repaint(id); }); options[completeFunction] = _ju.wrap(options[completeFunction], function () { _currentInstance.repaint(id); }); _currentInstance.doAnimate(ele, properties, options); }; this.checkCondition = function (conditionName, value) {
                var l = _currentInstance.getListener(conditionName), r = true; if (l && l.length > 0) {
                    try { for (var i = 0, j = l.length; i < j; i++) { r = r && l[i](value); } }
                    catch (e) { _ju.log(_currentInstance, "cannot check condition [" + conditionName + "]" + e); } 
                }
                return r;
            }; this.checkASyncCondition = function (conditionName, value, proceed, stop) {
                var l = _currentInstance.getListener(conditionName); if (l && l.length > 0) {
                    try { l[0](value, proceed, stop); }
                    catch (e) { _ju.log(_currentInstance, "cannot asynchronously check condition [" + conditionName + "]" + e); } 
                } 
            }; this.connect = function (params, referenceParams) {
                var _p = _prepareConnectionParams(params, referenceParams), jpc; if (_p) { _ensureContainer(_p.source); jpc = _newConnection(_p); _finaliseConnection(jpc, _p); }
                return jpc;
            }; var stTypes = [{ el: "source", elId: "sourceId", epDefs: "sourceEndpointDefinitions" }, { el: "target", elId: "targetId", epDefs: "targetEndpointDefinitions"}]; var _set = function (c, el, idx, doNotRepaint) {
                var ep, _st = stTypes[idx], cId = c[_st.elId], cEl = c[_st.el], sid, sep, oldEndpoint = c.endpoints[idx]; var evtParams = { index: idx, originalSourceId: idx === 0 ? cId : c.sourceId, newSourceId: c.sourceId, originalTargetId: idx == 1 ? cId : c.targetId, newTargetId: c.targetId, connection: c }; if (el.constructor == jsPlumb.Endpoint) { ep = el; ep.addConnection(c); }
                else {
                    sid = _getId(el); sep = this[_st.epDefs][sid]; if (sid === c[_st.elId])
                        ep = null; else if (sep) { if (!sep.enabled) return; ep = sep.endpoint != null && sep.endpoint._jsPlumb ? sep.endpoint : this.addEndpoint(el, sep.def); if (sep.uniqueEndpoint) sep.endpoint = ep; ep._doNotDeleteOnDetach = false; ep._deleteOnDetach = true; ep.addConnection(c); }
                    else { ep = c.makeEndpoint(idx === 0, el, sid); ep._doNotDeleteOnDetach = false; ep._deleteOnDetach = true; } 
                }
                if (ep != null) {
                    oldEndpoint.detachFromConnection(c); c.endpoints[idx] = ep; c[_st.el] = ep.element; c[_st.elId] = ep.elementId; evtParams[idx === 0 ? "newSourceId" : "newTargetId"] = ep.elementId; fireMoveEvent(evtParams); if (!doNotRepaint)
                        c.repaint();
                }
                return evtParams;
            } .bind(this); this.setSource = function (connection, el, doNotRepaint) { var p = _set(connection, el, 0, doNotRepaint); this.anchorManager.sourceChanged(p.originalSourceId, p.newSourceId, connection); }; this.setTarget = function (connection, el, doNotRepaint) { var p = _set(connection, el, 1, doNotRepaint); this.anchorManager.updateOtherEndpoint(p.originalSourceId, p.originalTargetId, p.newTargetId, connection); }; this.deleteEndpoint = function (object, doNotRepaintAfterwards) {
                var _is = _currentInstance.setSuspendDrawing(true); var endpoint = (typeof object == "string") ? endpointsByUUID[object] : object; if (endpoint) { _currentInstance.deleteObject({ endpoint: endpoint }); }
                if (!_is) _currentInstance.setSuspendDrawing(false, doNotRepaintAfterwards); return _currentInstance;
            }; this.deleteEveryEndpoint = function () {
                var _is = _currentInstance.setSuspendDrawing(true); for (var id in endpointsByElement) { var endpoints = endpointsByElement[id]; if (endpoints && endpoints.length) { for (var i = 0, j = endpoints.length; i < j; i++) { _currentInstance.deleteEndpoint(endpoints[i], true); } } }
                endpointsByElement = {}; endpointsByUUID = {}; _currentInstance.anchorManager.reset(); _currentInstance.dragManager.reset(); if (!_is) _currentInstance.setSuspendDrawing(false); return _currentInstance;
            }; var fireDetachEvent = function (jpc, doFireEvent, originalEvent) {
                var connType = _currentInstance.Defaults.ConnectionType || _currentInstance.getDefaultConnectionType(), argIsConnection = jpc.constructor == connType, params = argIsConnection ? { connection: jpc, source: jpc.source, target: jpc.target, sourceId: jpc.sourceId, targetId: jpc.targetId, sourceEndpoint: jpc.endpoints[0], targetEndpoint: jpc.endpoints[1]} : jpc; if (doFireEvent)
                    _currentInstance.fire("connectionDetached", params, originalEvent); _currentInstance.anchorManager.connectionDetached(params);
            }; var fireMoveEvent = function (params, evt) { _currentInstance.fire("connectionMoved", params, evt); }; this.unregisterEndpoint = function (endpoint) {
                if (endpoint._jsPlumb.uuid) endpointsByUUID[endpoint._jsPlumb.uuid] = null; _currentInstance.anchorManager.deleteEndpoint(endpoint); for (var e in endpointsByElement) {
                    var endpoints = endpointsByElement[e]; if (endpoints) {
                        var newEndpoints = []; for (var i = 0, j = endpoints.length; i < j; i++)
                            if (endpoints[i] != endpoint) newEndpoints.push(endpoints[i]); endpointsByElement[e] = newEndpoints;
                    }
                    if (endpointsByElement[e].length < 1) { delete endpointsByElement[e]; } 
                } 
            }; this.detach = function () {
                if (arguments.length === 0) return; var connType = _currentInstance.Defaults.ConnectionType || _currentInstance.getDefaultConnectionType(), firstArgIsConnection = arguments[0].constructor == connType, params = arguments.length == 2 ? firstArgIsConnection ? (arguments[1] || {}) : arguments[0] : arguments[0], fireEvent = (params.fireEvent !== false), forceDetach = params.forceDetach, conn = firstArgIsConnection ? arguments[0] : params.connection; if (conn) { if (forceDetach || jsPlumbUtil.functionChain(true, false, [[conn.endpoints[0], "isDetachAllowed", [conn]], [conn.endpoints[1], "isDetachAllowed", [conn]], [conn, "isDetachAllowed", [conn]], [_currentInstance, "checkCondition", ["beforeDetach", conn]]])) { conn.endpoints[0].detach(conn, false, true, fireEvent); } }
                else { var _p = jsPlumb.extend({}, params); if (_p.uuids) { _getEndpoint(_p.uuids[0]).detachFrom(_getEndpoint(_p.uuids[1]), fireEvent); } else if (_p.sourceEndpoint && _p.targetEndpoint) { _p.sourceEndpoint.detachFrom(_p.targetEndpoint); } else { var sourceId = _getId(_currentInstance.getDOMElement(_p.source)), targetId = _getId(_currentInstance.getDOMElement(_p.target)); _operation(sourceId, function (jpc) { if ((jpc.sourceId == sourceId && jpc.targetId == targetId) || (jpc.targetId == sourceId && jpc.sourceId == targetId)) { if (_currentInstance.checkCondition("beforeDetach", jpc)) { jpc.endpoints[0].detach(jpc, false, true, fireEvent); } } }); } } 
            }; this.detachAllConnections = function (el, params) {
                params = params || {}; el = _currentInstance.getDOMElement(el); var id = _getId(el), endpoints = endpointsByElement[id]; if (endpoints && endpoints.length) { for (var i = 0, j = endpoints.length; i < j; i++) { endpoints[i].detachAll(params.fireEvent !== false); } }
                return _currentInstance;
            }; this.detachEveryConnection = function (params) {
                params = params || {}; _currentInstance.doWhileSuspended(function () {
                    for (var id in endpointsByElement) { var endpoints = endpointsByElement[id]; if (endpoints && endpoints.length) { for (var i = 0, j = endpoints.length; i < j; i++) { endpoints[i].detachAll(params.fireEvent !== false); } } }
                    connections.splice(0);
                }); return _currentInstance;
            }; this.deleteObject = function (params) {
                var result = { endpoints: {}, connections: {}, endpointCount: 0, connectionCount: 0 }, fireEvent = params.fireEvent !== false, deleteAttachedObjects = params.deleteAttachedObjects !== false; var unravelConnection = function (connection) {
                    if (connection != null && result.connections[connection.id] == null) {
                        if (connection._jsPlumb != null) connection.setHover(false); result.connections[connection.id] = connection; result.connectionCount++; if (deleteAttachedObjects) {
                            for (var j = 0; j < connection.endpoints.length; j++) {
                                if (connection.endpoints[j]._deleteOnDetach)
                                    unravelEndpoint(connection.endpoints[j]);
                            } 
                        } 
                    } 
                }; var unravelEndpoint = function (endpoint) { if (endpoint != null && result.endpoints[endpoint.id] == null) { if (endpoint._jsPlumb != null) endpoint.setHover(false); result.endpoints[endpoint.id] = endpoint; result.endpointCount++; if (deleteAttachedObjects) { for (var i = 0; i < endpoint.connections.length; i++) { var c = endpoint.connections[i]; unravelConnection(c); } } } }; if (params.connection)
                    unravelConnection(params.connection); else unravelEndpoint(params.endpoint); for (var i in result.connections) { var c = result.connections[i]; if (c._jsPlumb) { jsPlumbUtil.removeWithFunction(connections, function (_c) { return c.id == _c.id; }); fireDetachEvent(c, fireEvent, params.originalEvent); c.endpoints[0].detachFromConnection(c); c.endpoints[1].detachFromConnection(c); c.cleanup(); c.destroy(); } }
                for (var j in result.endpoints) { var e = result.endpoints[j]; if (e._jsPlumb) { _currentInstance.unregisterEndpoint(e); e.cleanup(); e.destroy(); } }
                return result;
            }; this.draggable = function (el, options) {
                var i, j, ele; if (typeof el == 'object' && el.length) { for (i = 0, j = el.length; i < j; i++) { ele = _currentInstance.getDOMElement(el[i]); if (ele) _initDraggableIfNecessary(ele, true, options); } }
                else if (el._nodes) { for (i = 0, j = el._nodes.length; i < j; i++) { ele = _currentInstance.getDOMElement(el._nodes[i]); if (ele) _initDraggableIfNecessary(ele, true, options); } }
                else { ele = _currentInstance.getDOMElement(el); if (ele) _initDraggableIfNecessary(ele, true, options); }
                return _currentInstance;
            }; var _setOperation = function (list, func, args, selector) {
                for (var i = 0, j = list.length; i < j; i++) { list[i][func].apply(list[i], args); }
                return selector(list);
            }, _getOperation = function (list, func, args) {
                var out = []; for (var i = 0, j = list.length; i < j; i++) { out.push([list[i][func].apply(list[i], args), list[i]]); }
                return out;
            }, setter = function (list, func, selector) { return function () { return _setOperation(list, func, arguments, selector); }; }, getter = function (list, func) { return function () { return _getOperation(list, func, arguments); }; }, prepareList = function (input, doNotGetIds) {
                var r = []; if (input) {
                    if (typeof input == 'string') { if (input === "*") return input; r.push(input); }
                    else {
                        if (doNotGetIds) r = input; else {
                            if (input.length) {
                                for (var i = 0, j = input.length; i < j; i++)
                                    r.push(_info(input[i]).id);
                            }
                            else
                                r.push(_info(input).id);
                        } 
                    } 
                }
                return r;
            }, filterList = function (list, value, missingIsFalse) { if (list === "*") return true; return list.length > 0 ? jsPlumbUtil.indexOf(list, value) != -1 : !missingIsFalse; }; this.getConnections = function (options, flat) {
                if (!options) { options = {}; } else if (options.constructor == String) { options = { "scope": options }; }
                var scope = options.scope || _currentInstance.getDefaultScope(), scopes = prepareList(scope, true), sources = prepareList(options.source), targets = prepareList(options.target), results = (!flat && scopes.length > 1) ? {} : [], _addOne = function (scope, obj) {
                    if (!flat && scopes.length > 1) {
                        var ss = results[scope]; if (ss == null) { ss = results[scope] = []; }
                        ss.push(obj);
                    } else results.push(obj);
                }; for (var j = 0, jj = connections.length; j < jj; j++) {
                    var c = connections[j]; if (filterList(scopes, c.scope) && filterList(sources, c.sourceId) && filterList(targets, c.targetId))
                        _addOne(c.scope, c);
                }
                return results;
            }; var _curryEach = function (list, executor) {
                return function (f) {
                    for (var i = 0, ii = list.length; i < ii; i++) { f(list[i]); }
                    return executor(list);
                };
            }, _curryGet = function (list) { return function (idx) { return list[idx]; }; }; var _makeCommonSelectHandler = function (list, executor) {
                var out = { length: list.length, each: _curryEach(list, executor), get: _curryGet(list) }, setters = ["setHover", "removeAllOverlays", "setLabel", "addClass", "addOverlay", "removeOverlay", "removeOverlays", "showOverlay", "hideOverlay", "showOverlays", "hideOverlays", "setPaintStyle", "setHoverPaintStyle", "setSuspendEvents", "setParameter", "setParameters", "setVisible", "repaint", "addType", "toggleType", "removeType", "removeClass", "setType", "bind", "unbind"], getters = ["getLabel", "getOverlay", "isHover", "getParameter", "getParameters", "getPaintStyle", "getHoverPaintStyle", "isVisible", "hasType", "getType", "isSuspendEvents"], i, ii; for (i = 0, ii = setters.length; i < ii; i++)
                    out[setters[i]] = setter(list, setters[i], executor); for (i = 0, ii = getters.length; i < ii; i++)
                    out[getters[i]] = getter(list, getters[i]); return out;
            }; var _makeConnectionSelectHandler = function (list) {
                var common = _makeCommonSelectHandler(list, _makeConnectionSelectHandler); return jsPlumb.extend(common, { setDetachable: setter(list, "setDetachable", _makeConnectionSelectHandler), setReattach: setter(list, "setReattach", _makeConnectionSelectHandler), setConnector: setter(list, "setConnector", _makeConnectionSelectHandler), detach: function () {
                    for (var i = 0, ii = list.length; i < ii; i++)
                        _currentInstance.detach(list[i]);
                }, isDetachable: getter(list, "isDetachable"), isReattach: getter(list, "isReattach")
                });
            }; var _makeEndpointSelectHandler = function (list) {
                var common = _makeCommonSelectHandler(list, _makeEndpointSelectHandler); return jsPlumb.extend(common, { setEnabled: setter(list, "setEnabled", _makeEndpointSelectHandler), setAnchor: setter(list, "setAnchor", _makeEndpointSelectHandler), isEnabled: getter(list, "isEnabled"), detachAll: function () {
                    for (var i = 0, ii = list.length; i < ii; i++)
                        list[i].detachAll();
                }, "remove": function () {
                    for (var i = 0, ii = list.length; i < ii; i++)
                        _currentInstance.deleteObject({ endpoint: list[i] });
                } 
                });
            }; this.select = function (params) { params = params || {}; params.scope = params.scope || "*"; return _makeConnectionSelectHandler(params.connections || _currentInstance.getConnections(params, true)); }; this.selectEndpoints = function (params) {
                params = params || {}; params.scope = params.scope || "*"; var noElementFilters = !params.element && !params.source && !params.target, elements = noElementFilters ? "*" : prepareList(params.element), sources = noElementFilters ? "*" : prepareList(params.source), targets = noElementFilters ? "*" : prepareList(params.target), scopes = prepareList(params.scope, true); var ep = []; for (var el in endpointsByElement) {
                    var either = filterList(elements, el, true), source = filterList(sources, el, true), sourceMatchExact = sources != "*", target = filterList(targets, el, true), targetMatchExact = targets != "*"; if (either || source || target) {
                        inner: for (var i = 0, ii = endpointsByElement[el].length; i < ii; i++) {
                            var _ep = endpointsByElement[el][i]; if (filterList(scopes, _ep.scope, true)) {
                                var noMatchSource = (sourceMatchExact && sources.length > 0 && !_ep.isSource), noMatchTarget = (targetMatchExact && targets.length > 0 && !_ep.isTarget); if (noMatchSource || noMatchTarget)
                                    continue inner; ep.push(_ep);
                            } 
                        } 
                    } 
                }
                return _makeEndpointSelectHandler(ep);
            }; this.getAllConnections = function () { return connections; }; this.getDefaultScope = function () { return DEFAULT_SCOPE; }; this.getEndpoint = _getEndpoint; this.getEndpoints = function (el) { return endpointsByElement[_info(el).id]; }; this.getDefaultEndpointType = function () { return jsPlumb.Endpoint; }; this.getDefaultConnectionType = function () { return jsPlumb.Connection; }; this.getId = _getId; this.getOffset = function (id) { var o = offsets[id]; return _updateOffset({ elId: id }); }; this.appendElement = _appendElement; var _hoverSuspended = false; this.isHoverSuspended = function () { return _hoverSuspended; }; this.setHoverSuspended = function (s) { _hoverSuspended = s; }; var _isAvailable = function (m) { return function () { return jsPlumbAdapter.isRenderModeAvailable(m); }; }; this.isSVGAvailable = _isAvailable("svg"); this.isVMLAvailable = _isAvailable("vml"); this.hide = function (el, changeEndpoints) { _setVisible(el, "none", changeEndpoints); return _currentInstance; }; this.idstamp = _idstamp; this.connectorsInitialized = false; var connectorTypes = [], rendererTypes = ["svg", "vml"]; this.registerConnectorType = function (connector, name) { connectorTypes.push([connector, name]); }; var _ensureContainer = function (candidate) { if (!_container && candidate) { var can = _currentInstance.getDOMElement(candidate); if (can.offsetParent) _container = can.offsetParent; } }; var _getContainerFromDefaults = function () {
                if (_currentInstance.Defaults.Container)
                    _container = _currentInstance.getDOMElement(_currentInstance.Defaults.Container);
            }; this.init = function () {
                var _oneType = function (renderer, name, fn) { jsPlumb.Connectors[renderer][name] = function () { fn.apply(this, arguments); jsPlumb.ConnectorRenderers[renderer].apply(this, arguments); }; jsPlumbUtil.extend(jsPlumb.Connectors[renderer][name], [fn, jsPlumb.ConnectorRenderers[renderer]]); }; if (!jsPlumb.connectorsInitialized) {
                    for (var i = 0; i < connectorTypes.length; i++) { for (var j = 0; j < rendererTypes.length; j++) { _oneType(rendererTypes[j], connectorTypes[i][1], connectorTypes[i][0]); } }
                    jsPlumb.connectorsInitialized = true;
                }
                if (!initialized) { _getContainerFromDefaults(); _currentInstance.anchorManager = new jsPlumb.AnchorManager({ jsPlumbInstance: _currentInstance }); _currentInstance.setRenderMode(_currentInstance.Defaults.RenderMode); initialized = true; _currentInstance.fire("ready", _currentInstance); } 
            } .bind(this); this.log = log; this.jsPlumbUIComponent = jsPlumbUIComponent; this.makeAnchor = function () {
                var pp, _a = function (t, p) {
                    if (jsPlumb.Anchors[t]) return new jsPlumb.Anchors[t](p); if (!_currentInstance.Defaults.DoNotThrowErrors)
                        throw { msg: "jsPlumb: unknown anchor type '" + t + "'" };
                }; if (arguments.length === 0) return null; var specimen = arguments[0], elementId = arguments[1], jsPlumbInstance = arguments[2], newAnchor = null; if (specimen.compute && specimen.getOrientation) return specimen; else if (typeof specimen == "string") { newAnchor = _a(arguments[0], { elementId: elementId, jsPlumbInstance: _currentInstance }); }
                else if (_ju.isArray(specimen)) {
                    if (_ju.isArray(specimen[0]) || _ju.isString(specimen[0])) {
                        if (specimen.length == 2 && _ju.isObject(specimen[1])) {
                            if (_ju.isString(specimen[0])) { pp = jsPlumb.extend({ elementId: elementId, jsPlumbInstance: _currentInstance }, specimen[1]); newAnchor = _a(specimen[0], pp); }
                            else { pp = jsPlumb.extend({ elementId: elementId, jsPlumbInstance: _currentInstance, anchors: specimen[0] }, specimen[1]); newAnchor = new jsPlumb.DynamicAnchor(pp); } 
                        }
                        else
                            newAnchor = new jsPlumb.DynamicAnchor({ anchors: specimen, selector: null, elementId: elementId, jsPlumbInstance: jsPlumbInstance });
                    }
                    else { var anchorParams = { x: specimen[0], y: specimen[1], orientation: (specimen.length >= 4) ? [specimen[2], specimen[3]] : [0, 0], offsets: (specimen.length >= 6) ? [specimen[4], specimen[5]] : [0, 0], elementId: elementId, jsPlumbInstance: jsPlumbInstance, cssClass: specimen.length == 7 ? specimen[6] : null }; newAnchor = new jsPlumb.Anchor(anchorParams); newAnchor.clone = function () { return new jsPlumb.Anchor(anchorParams); }; } 
                }
                if (!newAnchor.id) newAnchor.id = "anchor_" + _idstamp(); return newAnchor;
            }; this.makeAnchors = function (types, elementId, jsPlumbInstance) {
                var r = []; for (var i = 0, ii = types.length; i < ii; i++) {
                    if (typeof types[i] == "string")
                        r.push(jsPlumb.Anchors[types[i]]({ elementId: elementId, jsPlumbInstance: jsPlumbInstance })); else if (_ju.isArray(types[i]))
                        r.push(_currentInstance.makeAnchor(types[i], elementId, jsPlumbInstance));
                }
                return r;
            }; this.makeDynamicAnchor = function (anchors, anchorSelector) { return new jsPlumb.DynamicAnchor({ anchors: anchors, selector: anchorSelector, elementId: null, jsPlumbInstance: _currentInstance }); }; this.targetEndpointDefinitions = {}; var _setEndpointPaintStylesAndAnchor = function (ep, epIndex, _instance) { ep.paintStyle = ep.paintStyle || _instance.Defaults.EndpointStyles[epIndex] || _instance.Defaults.EndpointStyle; ep.hoverPaintStyle = ep.hoverPaintStyle || _instance.Defaults.EndpointHoverStyles[epIndex] || _instance.Defaults.EndpointHoverStyle; ep.anchor = ep.anchor || _instance.Defaults.Anchors[epIndex] || _instance.Defaults.Anchor; ep.endpoint = ep.endpoint || _instance.Defaults.Endpoints[epIndex] || _instance.Defaults.Endpoint; }; this.sourceEndpointDefinitions = {}; var selectorFilter = function (evt, _el, selector, _instance, negate) {
                var t = evt.target || evt.srcElement, ok = false, sel = _instance.getSelector(_el, selector); for (var j = 0; j < sel.length; j++) { if (sel[j] == t) { ok = true; break; } }
                return negate ? !ok : ok;
            }; this.makeTarget = function (el, params, referenceParams) {
                var p = jsPlumb.extend({ _jsPlumb: this }, referenceParams); jsPlumb.extend(p, params); _setEndpointPaintStylesAndAnchor(p, 1, this); var targetScope = p.scope || _currentInstance.Defaults.Scope, deleteEndpointsOnDetach = !(p.deleteEndpointsOnDetach === false), maxConnections = p.maxConnections || -1, onMaxConnections = p.onMaxConnections, _doOne = function (el) {
                    var elInfo = _info(el), elid = elInfo.id, proxyComponent = new jsPlumbUIComponent(p), dropOptions = jsPlumb.extend({}, p.dropOptions || {}); _ensureContainer(elid); this.targetEndpointDefinitions[elid] = { def: p, uniqueEndpoint: p.uniqueEndpoint, maxConnections: maxConnections, enabled: true }; var _drop = function () {
                        this.currentlyDragging = false; var originalEvent = this.getDropEvent(arguments), targetCount = this.select({ target: elid }).length, draggable = this.getDOMElement(this.getDragObject(arguments)), id = this.getAttribute(draggable, "dragId"), scope = this.getAttribute(draggable, "originalScope"), jpc = floatingConnections[id], idx = jpc.endpoints[0].isFloating() ? 0 : 1, source = jpc.endpoints[0], _endpoint = p.endpoint ? jsPlumb.extend({}, p.endpoint) : {}, def = this.targetEndpointDefinitions[elid]; if (!def.enabled || def.maxConnections > 0 && targetCount >= def.maxConnections) {
                            if (onMaxConnections) { onMaxConnections({ element: elInfo.el, connection: jpc }, originalEvent); }
                            return false;
                        }
                        source.anchor.locked = false; if (scope) this.setDragScope(draggable, scope); if (jpc.suspendedEndpoint == null && !jpc.pending)
                            return false; var _continue = proxyComponent.isDropAllowed(idx === 0 ? elid : jpc.sourceId, idx === 0 ? jpc.targetId : elid, jpc.scope, jpc, null, idx === 0 ? elInfo.el : jpc.source, idx === 0 ? jpc.target : elInfo.el); if (jpc.suspendedEndpoint) { jpc[idx ? "targetId" : "sourceId"] = jpc.suspendedEndpoint.elementId; jpc[idx ? "target" : "source"] = jpc.suspendedEndpoint.element; jpc.endpoints[idx] = jpc.suspendedEndpoint; var suspendedElement = jpc.suspendedEndpoint.getElement(), suspendedElementId = jpc.suspendedEndpoint.elementId; fireMoveEvent({ index: idx, originalSourceId: idx === 0 ? suspendedElementId : jpc.sourceId, newSourceId: idx === 0 ? elid : jpc.sourceId, originalTargetId: idx == 1 ? suspendedElementId : jpc.targetId, newTargetId: idx == 1 ? elid : jpc.targetId, connection: jpc }, originalEvent); }
                        if (_continue) {
                            var _el = this.getElementObject(elInfo.el), newEndpoint = def.endpoint; if (newEndpoint == null || newEndpoint._jsPlumb == null)
                                newEndpoint = this.addEndpoint(_el, p); if (p.uniqueEndpoint) def.endpoint = newEndpoint; newEndpoint._doNotDeleteOnDetach = false; newEndpoint._deleteOnDetach = true; if (newEndpoint.anchor.positionFinder != null) { var dropPosition = this.getUIPosition(arguments, this.getZoom()), elPosition = _getOffset(_el, this), elSize = this.getSize(_el), ap = newEndpoint.anchor.positionFinder(dropPosition, elPosition, elSize, newEndpoint.anchor.constructorParams); newEndpoint.anchor.x = ap[0]; newEndpoint.anchor.y = ap[1]; }
                            jpc[idx ? "target" : "source"] = newEndpoint.element; jpc[idx ? "targetId" : "sourceId"] = newEndpoint.elementId; jpc.endpoints[idx].detachFromConnection(jpc); if (jpc.endpoints[idx]._deleteOnDetach)
                                jpc.endpoints[idx].deleteAfterDragStop = true; newEndpoint.addConnection(jpc); jpc.endpoints[idx] = newEndpoint; jpc.deleteEndpointsOnDetach = deleteEndpointsOnDetach; if (idx == 1)
                                this.anchorManager.updateOtherEndpoint(jpc.sourceId, jpc.suspendedElementId, jpc.targetId, jpc); else
                                this.anchorManager.sourceChanged(jpc.suspendedEndpoint.elementId, jpc.sourceId, jpc); _finaliseConnection(jpc, null, originalEvent); jpc.pending = false;
                        }
                        else {
                            if (jpc.suspendedEndpoint) {
                                if (jpc.isReattach()) { jpc.setHover(false); jpc.floatingAnchorIndex = null; jpc.suspendedEndpoint.addConnection(jpc); this.repaint(source.elementId); }
                                else
                                    source.detach(jpc, false, true, true, originalEvent);
                            } 
                        } 
                    } .bind(this); var dropEvent = jsPlumb.dragEvents.drop; dropOptions.scope = dropOptions.scope || targetScope; dropOptions[dropEvent] = _ju.wrap(dropOptions[dropEvent], _drop); this.initDroppable(this.getElementObject(elInfo.el), dropOptions, true);
                } .bind(this); el = _convertYUICollection(el); var inputs = el.length && el.constructor != String ? el : [el]; for (var i = 0, ii = inputs.length; i < ii; i++) { _doOne(inputs[i]); }
                return this;
            }; this.unmakeTarget = function (el, doNotClearArrays) {
                var info = _info(el); jsPlumb.destroyDroppable(info.el); if (!doNotClearArrays) { delete this.targetEndpointDefinitions[info.id]; }
                return this;
            }; this.makeSource = function (el, params, referenceParams) {
                var p = jsPlumb.extend({}, referenceParams); jsPlumb.extend(p, params); _setEndpointPaintStylesAndAnchor(p, 0, this); var maxConnections = p.maxConnections || -1, onMaxConnections = p.onMaxConnections, _doOne = function (elInfo) {
                    var elid = elInfo.id, _el = this.getElementObject(elInfo.el), _del = this.getDOMElement(_el), parentElement = function () { return p.parent == null ? null : p.parent === "parent" ? elInfo.el.parentNode : _currentInstance.getDOMElement(p.parent); }, idToRegisterAgainst = p.parent != null ? this.getId(parentElement()) : elid; _ensureContainer(idToRegisterAgainst); this.sourceEndpointDefinitions[idToRegisterAgainst] = { def: p, uniqueEndpoint: p.uniqueEndpoint, maxConnections: maxConnections, enabled: true }; var stopEvent = jsPlumb.dragEvents.stop, dragEvent = jsPlumb.dragEvents.drag, dragOptions = jsPlumb.extend({}, p.dragOptions || {}), existingDrag = dragOptions.drag, existingStop = dragOptions.stop, ep = null, endpointAddedButNoDragYet = false; dragOptions.scope = dragOptions.scope || p.scope; dragOptions[dragEvent] = _ju.wrap(dragOptions[dragEvent], function () { if (existingDrag) existingDrag.apply(this, arguments); endpointAddedButNoDragYet = false; }); dragOptions[stopEvent] = _ju.wrap(dragOptions[stopEvent], function () {
                        if (existingStop) existingStop.apply(this, arguments); this.currentlyDragging = false; if (ep._jsPlumb != null) {
                            var anchorDef = p.anchor || this.Defaults.Anchor, oldAnchor = ep.anchor, oldConnection = ep.connections[0], newAnchor = this.makeAnchor(anchorDef, elid, this), _el = ep.element; if (newAnchor.positionFinder != null) { var elPosition = _getOffset(_el, this), elSize = this.getSize(_el), dropPosition = { left: elPosition.left + (oldAnchor.x * elSize[0]), top: elPosition.top + (oldAnchor.y * elSize[1]) }, ap = newAnchor.positionFinder(dropPosition, elPosition, elSize, newAnchor.constructorParams); newAnchor.x = ap[0]; newAnchor.y = ap[1]; }
                            ep.setAnchor(newAnchor, true); if (p.parent) { var parent = parentElement(); if (parent) { var potentialParent = p.container || _container; ep.setElement(parent, potentialParent); } }
                            ep.repaint(); this.repaint(ep.elementId); this.repaint(oldConnection.targetId);
                        } 
                    } .bind(this)); var mouseDownListener = function (e) {
                        var evt = this.getOriginalEvent(e); var def = this.sourceEndpointDefinitions[idToRegisterAgainst]; elid = this.getId(this.getDOMElement(_el)); if (!def.enabled) return; if (p.filter) { var r = jsPlumbUtil.isString(p.filter) ? selectorFilter(evt, _el, p.filter, this, p.filterExclude) : p.filter(evt, _el); if (r === false) return; }
                        var sourceCount = this.select({ source: idToRegisterAgainst }).length; if (def.maxConnections >= 0 && sourceCount >= def.maxConnections) {
                            if (onMaxConnections) { onMaxConnections({ element: _el, maxConnections: maxConnections }, e); }
                            return false;
                        }
                        var elxy = jsPlumbAdapter.getPositionOnElement(evt, _del, _zoom), pelxy = elxy; if (p.parent) { pelxy = jsPlumbAdapter.getPositionOnElement(evt, parentElement(), _zoom); }
                        var tempEndpointParams = {}; jsPlumb.extend(tempEndpointParams, p); tempEndpointParams.isSource = true; tempEndpointParams.anchor = [elxy[0], elxy[1], 0, 0]; tempEndpointParams.parentAnchor = [pelxy[0], pelxy[1], 0, 0]; tempEndpointParams.dragOptions = dragOptions; ep = this.addEndpoint(elid, tempEndpointParams); endpointAddedButNoDragYet = true; ep.endpointWillMoveTo = p.parent ? parentElement() : null; ep._doNotDeleteOnDetach = false; ep._deleteOnDetach = true; var _delTempEndpoint = function () { if (endpointAddedButNoDragYet) { endpointAddedButNoDragYet = false; _currentInstance.deleteEndpoint(ep); } }; _currentInstance.registerListener(ep.canvas, "mouseup", _delTempEndpoint); _currentInstance.registerListener(_el, "mouseup", _delTempEndpoint); _currentInstance.trigger(ep.canvas, "mousedown", e); jsPlumbUtil.consume(e);
                    } .bind(this); this.registerListener(_el, "mousedown", mouseDownListener); this.sourceEndpointDefinitions[idToRegisterAgainst].trigger = mouseDownListener; if (p.filter && jsPlumbUtil.isString(p.filter)) { _currentInstance.setDragFilter(_el, p.filter); } 
                } .bind(this); el = _convertYUICollection(el); var inputs = el.length && el.constructor != String ? el : [el]; for (var i = 0, ii = inputs.length; i < ii; i++) { _doOne(_info(inputs[i])); }
                return this;
            }; this.unmakeSource = function (el, doNotClearArrays) {
                var info = _info(el), mouseDownListener = this.sourceEndpointDefinitions[info.id].trigger; if (mouseDownListener)
                    _currentInstance.unregisterListener(info.el, "mousedown", mouseDownListener); if (!doNotClearArrays) { delete this.sourceEndpointDefinitions[info.id]; }
                return this;
            }; this.unmakeEverySource = function () {
                for (var i in this.sourceEndpointDefinitions)
                    _currentInstance.unmakeSource(i, true); this.sourceEndpointDefinitions = {}; return this;
            }; this.unmakeEveryTarget = function () {
                for (var i in this.targetEndpointDefinitions)
                    _currentInstance.unmakeTarget(i, true); this.targetEndpointDefinitions = {}; return this;
            }; var _setEnabled = function (type, el, state, toggle) {
                var a = type == "source" ? this.sourceEndpointDefinitions : this.targetEndpointDefinitions; el = _convertYUICollection(el); if (_ju.isString(el)) a[el].enabled = toggle ? !a[el].enabled : state; else if (el.length) {
                    for (var i = 0, ii = el.length; i < ii; i++) {
                        var info = _info(el[i]); if (a[info.id])
                            a[info.id].enabled = toggle ? !a[info.id].enabled : state;
                    } 
                }
                else { var id = _info(el).id; a[id].enabled = toggle ? !a[id].enabled : state; }
                return this;
            } .bind(this); var _first = function (el, fn) {
                el = _convertYUICollection(el); if (_ju.isString(el) || !el.length)
                    return fn.apply(this, [el]); else if (el.length)
                    return fn.apply(this, [el[0]]);
            } .bind(this); this.toggleSourceEnabled = function (el) { _setEnabled("source", el, null, true); return this.isSourceEnabled(el); }; this.setSourceEnabled = function (el, state) { return _setEnabled("source", el, state); }; this.isSource = function (el) { return _first(el, function (_el) { return this.sourceEndpointDefinitions[_info(_el).id] != null; }); }; this.isSourceEnabled = function (el) { return _first(el, function (_el) { var sep = this.sourceEndpointDefinitions[_info(_el).id]; return sep && sep.enabled === true; }); }; this.toggleTargetEnabled = function (el) { _setEnabled("target", el, null, true); return this.isTargetEnabled(el); }; this.isTarget = function (el) { return _first(el, function (_el) { return this.targetEndpointDefinitions[_info(_el).id] != null; }); }; this.isTargetEnabled = function (el) { return _first(el, function (_el) { var tep = this.targetEndpointDefinitions[_info(_el).id]; return tep && tep.enabled === true; }); }; this.setTargetEnabled = function (el, state) { return _setEnabled("target", el, state); }; this.ready = function (fn) { _currentInstance.bind("ready", fn); }; this.repaint = function (el, ui, timestamp) {
                if (typeof el == 'object' && el.length)
                    for (var i = 0, ii = el.length; i < ii; i++) { _draw(el[i], ui, timestamp); }
                else
                    _draw(el, ui, timestamp); return _currentInstance;
            }; this.repaintEverything = function (clearEdits) {
                var timestamp = _timestamp(); for (var elId in endpointsByElement) { _draw(elId, null, timestamp, clearEdits); }
                return this;
            }; this.removeAllEndpoints = function (el, recurse) {
                var _one = function (_el) {
                    var info = _info(_el), ebe = endpointsByElement[info.id], i, ii; if (ebe) {
                        for (i = 0, ii = ebe.length; i < ii; i++)
                            _currentInstance.deleteEndpoint(ebe[i]);
                    }
                    delete endpointsByElement[info.id]; if (recurse) { if (info.el && info.el.nodeType != 3 && info.el.nodeType != 8) { for (i = 0, ii = info.el.childNodes.length; i < ii; i++) { _one(info.el.childNodes[i]); } } } 
                }; _one(el); return this;
            }; this.remove = function (el, doNotRepaint) { var info = _info(el); _currentInstance.doWhileSuspended(function () { _currentInstance.removeAllEndpoints(info.id, true); _currentInstance.dragManager.elementRemoved(info.id); delete floatingConnections[info.id]; _currentInstance.anchorManager.clearFor(info.id); _currentInstance.anchorManager.removeFloatingConnection(info.id); }, doNotRepaint === false); if (info.el) _currentInstance.removeElement(info.el); return _currentInstance; }; var _registeredListeners = {}, _unbindRegisteredListeners = function () {
                for (var i in _registeredListeners) { for (var j = 0, jj = _registeredListeners[i].length; j < jj; j++) { var info = _registeredListeners[i][j]; _currentInstance.off(info.el, info.event, info.listener); } }
                _registeredListeners = {};
            }; this.registerListener = function (el, type, listener) { _currentInstance.on(el, type, listener); jsPlumbUtil.addToList(_registeredListeners, type, { el: el, event: type, listener: listener }); }; this.unregisterListener = function (el, type, listener) { _currentInstance.off(el, type, listener); jsPlumbUtil.removeWithFunction(_registeredListeners, function (rl) { return rl.type == type && rl.listener == listener; }); }; this.reset = function () {
                _currentInstance.deleteEveryEndpoint(); _currentInstance.unbind(); this.targetEndpointDefinitions = {}; this.sourceEndpointDefinitions = {}; connections.splice(0); _unbindRegisteredListeners(); _currentInstance.anchorManager.reset(); if (!jsPlumbAdapter.headless)
                    _currentInstance.dragManager.reset();
            }; this.setDefaultScope = function (scope) { DEFAULT_SCOPE = scope; return _currentInstance; }; this.setDraggable = _setDraggable; this.setId = function (el, newId, doNotSetAttribute) {
                var id; if (jsPlumbUtil.isString(el)) { id = el; }
                else { el = this.getDOMElement(el); id = this.getId(el); }
                var sConns = this.getConnections({ source: id, scope: '*' }, true), tConns = this.getConnections({ target: id, scope: '*' }, true); newId = "" + newId; if (!doNotSetAttribute) { el = this.getDOMElement(id); this.setAttribute(el, "id", newId); }
                else
                    el = this.getDOMElement(newId); endpointsByElement[newId] = endpointsByElement[id] || []; for (var i = 0, ii = endpointsByElement[newId].length; i < ii; i++) { endpointsByElement[newId][i].setElementId(newId); endpointsByElement[newId][i].setReferenceElement(el); }
                delete endpointsByElement[id]; this.anchorManager.changeId(id, newId); if (this.dragManager) this.dragManager.changeId(id, newId); var _conns = function (list, epIdx, type) { for (var i = 0, ii = list.length; i < ii; i++) { list[i].endpoints[epIdx].setElementId(newId); list[i].endpoints[epIdx].setReferenceElement(el); list[i][type + "Id"] = newId; list[i][type] = el; } }; _conns(sConns, 0, "source"); _conns(tConns, 1, "target"); this.repaint(newId);
            }; this.setDebugLog = function (debugLog) { log = debugLog; }; this.setSuspendDrawing = function (val, repaintAfterwards) { var curVal = _suspendDrawing; _suspendDrawing = val; if (val) _suspendedAt = new Date().getTime(); else _suspendedAt = null; if (repaintAfterwards) this.repaintEverything(); return curVal; }; this.isSuspendDrawing = function () { return _suspendDrawing; }; this.getSuspendedAt = function () { return _suspendedAt; }; this.doWhileSuspended = function (fn, doNotRepaintAfterwards) {
                var _wasSuspended = this.isSuspendDrawing(); if (!_wasSuspended)
                    this.setSuspendDrawing(true); try { fn(); }
                catch (e) { _ju.log("Function run while suspended failed", e); }
                if (!_wasSuspended)
                    this.setSuspendDrawing(false, !doNotRepaintAfterwards);
            }; this.getOffset = function (elId) { return offsets[elId]; }; this.getCachedData = _getCachedData; this.timestamp = _timestamp; this.setRenderMode = function (mode) { if (mode !== jsPlumb.SVG && mode !== jsPlumb.VML) throw new TypeError("Render mode [" + mode + "] not supported"); renderMode = jsPlumbAdapter.setRenderMode(mode); return renderMode; }; this.getRenderMode = function () { return renderMode; }; this.show = function (el, changeEndpoints) { _setVisible(el, "block", changeEndpoints); return _currentInstance; }; this.toggleVisible = _toggleVisible; this.toggleDraggable = _toggleDraggable; this.addListener = this.bind; if (!jsPlumbAdapter.headless) { _currentInstance.dragManager = jsPlumbAdapter.getDragManager(_currentInstance); _currentInstance.recalculateOffsets = _currentInstance.dragManager.updateOffsets; } 
    }; jsPlumbUtil.extend(jsPlumbInstance, jsPlumbUtil.EventGenerator, { setAttribute: function (el, a, v) { this.setAttribute(el, a, v); }, getAttribute: function (el, a) { return this.getAttribute(jsPlumb.getDOMElement(el), a); }, registerConnectionType: function (id, type) { this._connectionTypes[id] = jsPlumb.extend({}, type); }, registerConnectionTypes: function (types) {
        for (var i in types)
            this._connectionTypes[i] = jsPlumb.extend({}, types[i]);
    }, registerEndpointType: function (id, type) { this._endpointTypes[id] = jsPlumb.extend({}, type); }, registerEndpointTypes: function (types) {
        for (var i in types)
            this._endpointTypes[i] = jsPlumb.extend({}, types[i]);
    }, getType: function (id, typeDescriptor) { return typeDescriptor === "connection" ? this._connectionTypes[id] : this._endpointTypes[id]; }, setIdChanged: function (oldId, newId) { this.setId(oldId, newId, true); }, setParent: function (el, newParent) { var _el = this.getElementObject(el), _dom = this.getDOMElement(_el), _id = this.getId(_dom), _pel = this.getElementObject(newParent), _pdom = this.getDOMElement(_pel), _pid = this.getId(_pdom); _dom.parentNode.removeChild(_dom); _pdom.appendChild(_dom); this.dragManager.setParent(_el, _id, _pel, _pid); }, getSize: function (el) { return [el.offsetWidth, el.offsetHeight]; }, getWidth: function (el) { return el.offsetWidth; }, getHeight: function (el) { return el.offsetHeight; }, extend: function (o1, o2, names) {
        var i; if (names) {
            for (i = 0; i < names.length; i++)
                o1[names[i]] = o2[names[i]];
        }
        else
            for (i in o2) o1[i] = o2[i]; return o1;
    } 
    }, jsPlumbAdapter); var jsPlumb = new jsPlumbInstance(); if (typeof window != 'undefined') window.jsPlumb = jsPlumb; jsPlumb.getInstance = function (_defaults) { var j = new jsPlumbInstance(_defaults); j.init(); return j; }; if (typeof define === "function") { define("jsplumb", [], function () { return jsPlumb; }); define("jsplumbinstance", [], function () { return jsPlumb.getInstance(); }); }
    if (typeof exports !== 'undefined') { exports.jsPlumb = jsPlumb; } 
})(); ; (function () {
    "use strict"; var _makeConnectionDragHandler = function (placeholder, _jsPlumb) {
        var stopped = false; return { drag: function () {
            if (stopped) { stopped = false; return true; }
            var _ui = jsPlumb.getUIPosition(arguments, _jsPlumb.getZoom()); if (placeholder.element) { jsPlumbAdapter.setPosition(placeholder.element, _ui); _jsPlumb.repaint(placeholder.element, _ui); } 
        }, stopDrag: function () { stopped = true; } 
        };
    }; var _makeDraggablePlaceholder = function (placeholder, _jsPlumb) { var n = document.createElement("div"); n.style.position = "absolute"; var parent = _jsPlumb.getContainer() || document.body; parent.appendChild(n); var id = _jsPlumb.getId(n); _jsPlumb.updateOffset({ elId: id }); placeholder.id = id; placeholder.element = n; }; var _makeFloatingEndpoint = function (paintStyle, referenceAnchor, endpoint, referenceCanvas, sourceElement, _jsPlumb, _newEndpoint) { var floatingAnchor = new jsPlumb.FloatingAnchor({ reference: referenceAnchor, referenceCanvas: referenceCanvas, jsPlumbInstance: _jsPlumb }); return _newEndpoint({ paintStyle: paintStyle, endpoint: endpoint, anchor: floatingAnchor, source: sourceElement, scope: "__floating" }); }; var typeParameters = ["connectorStyle", "connectorHoverStyle", "connectorOverlays", "connector", "connectionType", "connectorClass", "connectorHoverClass"]; var findConnectionToUseForDynamicAnchor = function (ep, elementWithPrecedence) {
        var idx = 0; if (elementWithPrecedence != null) { for (var i = 0; i < ep.connections.length; i++) { if (ep.connections[i].sourceId == elementWithPrecedence || ep.connections[i].targetId == elementWithPrecedence) { idx = i; break; } } }
        return ep.connections[idx];
    }; var findConnectionIndex = function (conn, ep) { return jsPlumbUtil.findWithFunction(ep.connections, function (c) { return c.id == conn.id; }); }; jsPlumb.Endpoint = function (params) {
        var _jsPlumb = params._jsPlumb, _att = jsPlumbAdapter.getAttribute, _gel = jsPlumb.getElementObject, _ju = jsPlumbUtil, _newConnection = params.newConnection, _newEndpoint = params.newEndpoint, _finaliseConnection = params.finaliseConnection, _fireDetachEvent = params.fireDetachEvent, _fireMoveEvent = params.fireMoveEvent, floatingConnections = params.floatingConnections; this.idPrefix = "_jsplumb_e_"; this.defaultLabelLocation = [0.5, 0.5]; this.defaultOverlayKeys = ["Overlays", "EndpointOverlays"]; OverlayCapableJsPlumbUIComponent.apply(this, arguments); this.getDefaultType = function () { return { parameters: {}, scope: null, maxConnections: this._jsPlumb.instance.Defaults.MaxConnections, paintStyle: this._jsPlumb.instance.Defaults.EndpointStyle || jsPlumb.Defaults.EndpointStyle, endpoint: this._jsPlumb.instance.Defaults.Endpoint || jsPlumb.Defaults.Endpoint, hoverPaintStyle: this._jsPlumb.instance.Defaults.EndpointHoverStyle || jsPlumb.Defaults.EndpointHoverStyle, overlays: this._jsPlumb.instance.Defaults.EndpointOverlays || jsPlumb.Defaults.EndpointOverlays, connectorStyle: params.connectorStyle, connectorHoverStyle: params.connectorHoverStyle, connectorClass: params.connectorClass, connectorHoverClass: params.connectorHoverClass, connectorOverlays: params.connectorOverlays, connector: params.connector, connectorTooltip: params.connectorTooltip }; }; this._jsPlumb.enabled = !(params.enabled === false); this._jsPlumb.visible = true; this.element = jsPlumb.getDOMElement(params.source); this._jsPlumb.uuid = params.uuid; this._jsPlumb.floatingEndpoint = null; var inPlaceCopy = null; if (this._jsPlumb.uuid) params.endpointsByUUID[this._jsPlumb.uuid] = this; this.elementId = params.elementId; this._jsPlumb.connectionCost = params.connectionCost; this._jsPlumb.connectionsDirected = params.connectionsDirected; this._jsPlumb.currentAnchorClass = ""; this._jsPlumb.events = {}; var _updateAnchorClass = function () { jsPlumbAdapter.removeClass(this.element, _jsPlumb.endpointAnchorClassPrefix + "_" + this._jsPlumb.currentAnchorClass); this.removeClass(_jsPlumb.endpointAnchorClassPrefix + "_" + this._jsPlumb.currentAnchorClass); this._jsPlumb.currentAnchorClass = this.anchor.getCssClass(); this.addClass(_jsPlumb.endpointAnchorClassPrefix + "_" + this._jsPlumb.currentAnchorClass); jsPlumbAdapter.addClass(this.element, _jsPlumb.endpointAnchorClassPrefix + "_" + this._jsPlumb.currentAnchorClass); } .bind(this); this.setAnchor = function (anchorParams, doNotRepaint) {
            this._jsPlumb.instance.continuousAnchorFactory.clear(this.elementId); this.anchor = this._jsPlumb.instance.makeAnchor(anchorParams, this.elementId, _jsPlumb); _updateAnchorClass(); this.anchor.bind("anchorChanged", function (currentAnchor) { this.fire("anchorChanged", { endpoint: this, anchor: currentAnchor }); _updateAnchorClass(); } .bind(this)); if (!doNotRepaint)
                this._jsPlumb.instance.repaint(this.elementId); return this;
        }; var anchorParamsToUse = params.anchor ? params.anchor : params.anchors ? params.anchors : (_jsPlumb.Defaults.Anchor || "Top"); this.setAnchor(anchorParamsToUse, true); var internalHover = function (state) {
            if (this.connections.length > 0)
                this.connections[0].setHover(state, false); else
                this.setHover(state);
        } .bind(this); if (!params._transient)
            this._jsPlumb.instance.anchorManager.add(this, this.elementId); this.setEndpoint = function (ep) {
                if (this.endpoint != null) { this.endpoint.cleanup(); this.endpoint.destroy(); }
                var _e = function (t, p) {
                    var rm = _jsPlumb.getRenderMode(); if (jsPlumb.Endpoints[rm][t]) return new jsPlumb.Endpoints[rm][t](p); if (!_jsPlumb.Defaults.DoNotThrowErrors)
                        throw { msg: "jsPlumb: unknown endpoint type '" + t + "'" };
                }; var endpointArgs = { _jsPlumb: this._jsPlumb.instance, cssClass: params.cssClass, container: params.container, tooltip: params.tooltip, connectorTooltip: params.connectorTooltip, endpoint: this }; if (_ju.isString(ep))
                    this.endpoint = _e(ep, endpointArgs); else if (_ju.isArray(ep)) { endpointArgs = _ju.merge(ep[1], endpointArgs); this.endpoint = _e(ep[0], endpointArgs); }
                else { this.endpoint = ep.clone(); }
                var argsForClone = jsPlumb.extend({}, endpointArgs); this.endpoint.clone = function () {
                    if (_ju.isString(ep))
                        return _e(ep, endpointArgs); else if (_ju.isArray(ep)) { endpointArgs = _ju.merge(ep[1], endpointArgs); return _e(ep[0], endpointArgs); } 
                } .bind(this); this.type = this.endpoint.type; this.bindListeners(this.endpoint, this, internalHover);
            }; this.setEndpoint(params.endpoint || _jsPlumb.Defaults.Endpoint || jsPlumb.Defaults.Endpoint || "Dot"); this.setPaintStyle(params.paintStyle || params.style || _jsPlumb.Defaults.EndpointStyle || jsPlumb.Defaults.EndpointStyle, true); this.setHoverPaintStyle(params.hoverPaintStyle || _jsPlumb.Defaults.EndpointHoverStyle || jsPlumb.Defaults.EndpointHoverStyle, true); this._jsPlumb.paintStyleInUse = this.getPaintStyle(); jsPlumb.extend(this, params, typeParameters); this.isSource = params.isSource || false; this.isTarget = params.isTarget || false; this._jsPlumb.maxConnections = params.maxConnections || _jsPlumb.Defaults.MaxConnections; this.canvas = this.endpoint.canvas; this.addClass(_jsPlumb.endpointAnchorClassPrefix + "_" + this._jsPlumb.currentAnchorClass); jsPlumbAdapter.addClass(this.element, _jsPlumb.endpointAnchorClassPrefix + "_" + this._jsPlumb.currentAnchorClass); this.connections = params.connections || []; this.connectorPointerEvents = params["connector-pointer-events"]; this.scope = params.scope || _jsPlumb.getDefaultScope(); this.timestamp = null; this.reattachConnections = params.reattach || _jsPlumb.Defaults.ReattachConnections; this.connectionsDetachable = _jsPlumb.Defaults.ConnectionsDetachable; if (params.connectionsDetachable === false || params.detachable === false)
            this.connectionsDetachable = false; this.dragAllowedWhenFull = params.dragAllowedWhenFull || true; if (params.onMaxConnections)
            this.bind("maxConnections", params.onMaxConnections); this.addConnection = function (connection) { this.connections.push(connection); this[(this.connections.length > 0 ? "add" : "remove") + "Class"](_jsPlumb.endpointConnectedClass); this[(this.isFull() ? "add" : "remove") + "Class"](_jsPlumb.endpointFullClass); }; this.detachFromConnection = function (connection, idx, doNotCleanup) {
                idx = idx == null ? findConnectionIndex(connection, this) : idx; if (idx >= 0) { this.connections.splice(idx, 1); this[(this.connections.length > 0 ? "add" : "remove") + "Class"](_jsPlumb.endpointConnectedClass); this[(this.isFull() ? "add" : "remove") + "Class"](_jsPlumb.endpointFullClass); }
                if (!doNotCleanup && this._deleteOnDetach && this.connections.length === 0) { _jsPlumb.deleteObject({ endpoint: this, fireEvent: false, deleteAttachedObjects: false }); } 
            }; this.detach = function (connection, ignoreTarget, forceDetach, fireEvent, originalEvent, endpointBeingDeleted, connectionIndex) {
                var idx = connectionIndex == null ? findConnectionIndex(connection, this) : connectionIndex, actuallyDetached = false; fireEvent = (fireEvent !== false); if (idx >= 0) { if (forceDetach || connection._forceDetach || (connection.isDetachable() && connection.isDetachAllowed(connection) && this.isDetachAllowed(connection))) { _jsPlumb.deleteObject({ connection: connection, fireEvent: (!ignoreTarget && fireEvent), originalEvent: originalEvent, deleteAttachedObjects: false }); actuallyDetached = true; } }
                return actuallyDetached;
            }; this.detachAll = function (fireEvent, originalEvent) {
                while (this.connections.length > 0) { this.detach(this.connections[0], false, true, fireEvent !== false, originalEvent, this, 0); }
                return this;
            }; this.detachFrom = function (targetEndpoint, fireEvent, originalEvent) {
                var c = []; for (var i = 0; i < this.connections.length; i++) { if (this.connections[i].endpoints[1] == targetEndpoint || this.connections[i].endpoints[0] == targetEndpoint) { c.push(this.connections[i]); } }
                for (var j = 0; j < c.length; j++) { this.detach(c[j], false, true, fireEvent, originalEvent); }
                return this;
            }; this.getElement = function () { return this.element; }; this.setElement = function (el) { var parentId = this._jsPlumb.instance.getId(el), curId = this.elementId; _ju.removeWithFunction(params.endpointsByElement[this.elementId], function (e) { return e.id == this.id; } .bind(this)); this.element = jsPlumb.getDOMElement(el); this.elementId = _jsPlumb.getId(this.element); _jsPlumb.anchorManager.rehomeEndpoint(this, curId, this.element); _jsPlumb.dragManager.endpointAdded(this.element); _ju.addToList(params.endpointsByElement, parentId, this); return this; }; this.makeInPlaceCopy = function () { var loc = this.anchor.getCurrentLocation({ element: this }), o = this.anchor.getOrientation(this), acc = this.anchor.getCssClass(), inPlaceAnchor = { bind: function () { }, compute: function () { return [loc[0], loc[1]]; }, getCurrentLocation: function () { return [loc[0], loc[1]]; }, getOrientation: function () { return o; }, getCssClass: function () { return acc; } }; return _newEndpoint({ dropOptions: params.dropOptions, anchor: inPlaceAnchor, source: this.element, paintStyle: this.getPaintStyle(), endpoint: params.hideOnDrag ? "Blank" : this.endpoint, _transient: true, scope: this.scope }); }; this.connectorSelector = function () { var candidate = this.connections[0]; if (this.isTarget && candidate) return candidate; else { return (this.connections.length < this._jsPlumb.maxConnections) || this._jsPlumb.maxConnections == -1 ? null : candidate; } }; this.setStyle = this.setPaintStyle; this.paint = function (params) {
                params = params || {}; var timestamp = params.timestamp, recalc = !(params.recalc === false); if (!timestamp || this.timestamp !== timestamp) {
                    var info = _jsPlumb.updateOffset({ elId: this.elementId, timestamp: timestamp }); var xy = params.offset ? params.offset.o : info.o; if (xy != null) {
                        var ap = params.anchorPoint, connectorPaintStyle = params.connectorPaintStyle; if (ap == null) {
                            var wh = params.dimensions || info.s, anchorParams = { xy: [xy.left, xy.top], wh: wh, element: this, timestamp: timestamp }; if (recalc && this.anchor.isDynamic && this.connections.length > 0) { var c = findConnectionToUseForDynamicAnchor(this, params.elementWithPrecedence), oIdx = c.endpoints[0] == this ? 1 : 0, oId = oIdx === 0 ? c.sourceId : c.targetId, oInfo = _jsPlumb.getCachedData(oId), oOffset = oInfo.o, oWH = oInfo.s; anchorParams.txy = [oOffset.left, oOffset.top]; anchorParams.twh = oWH; anchorParams.tElement = c.endpoints[oIdx]; }
                            ap = this.anchor.compute(anchorParams);
                        }
                        this.endpoint.compute(ap, this.anchor.getOrientation(this), this._jsPlumb.paintStyleInUse, connectorPaintStyle || this.paintStyleInUse); this.endpoint.paint(this._jsPlumb.paintStyleInUse, this.anchor); this.timestamp = timestamp; for (var i = 0; i < this._jsPlumb.overlays.length; i++) { var o = this._jsPlumb.overlays[i]; if (o.isVisible()) { this._jsPlumb.overlayPlacements[i] = o.draw(this.endpoint, this._jsPlumb.paintStyleInUse); o.paint(this._jsPlumb.overlayPlacements[i]); } } 
                    } 
                } 
            }; this.repaint = this.paint; var draggingInitialised = false; this.initDraggable = function () {
                if (!draggingInitialised && jsPlumb.isDragSupported(this.element)) {
                    var placeholderInfo = { id: null, element: null }, jpc = null, existingJpc = false, existingJpcParams = null, _dragHandler = _makeConnectionDragHandler(placeholderInfo, _jsPlumb); var start = function () {
                        jpc = this.connectorSelector(); var _continue = true; if (!this.isEnabled()) _continue = false; if (jpc == null && !this.isSource) _continue = false; if (this.isSource && this.isFull() && !this.dragAllowedWhenFull) _continue = false; if (jpc != null && !jpc.isDetachable()) _continue = false; if (_continue === false) { if (_jsPlumb.stopDrag) _jsPlumb.stopDrag(this.canvas); _dragHandler.stopDrag(); return false; }
                        for (var i = 0; i < this.connections.length; i++)
                            this.connections[i].setHover(false); this.addClass("endpointDrag"); _jsPlumb.setConnectionBeingDragged(true); if (jpc && !this.isFull() && this.isSource) jpc = null; _jsPlumb.updateOffset({ elId: this.elementId }); inPlaceCopy = this.makeInPlaceCopy(); inPlaceCopy.referenceEndpoint = this; inPlaceCopy.paint(); _makeDraggablePlaceholder(placeholderInfo, _jsPlumb); var ipcoel = _gel(inPlaceCopy.canvas), ipco = jsPlumbAdapter.getOffset(ipcoel, this._jsPlumb.instance), canvasElement = _gel(this.canvas); jsPlumbAdapter.setPosition(placeholderInfo.element, ipco); if (this.parentAnchor) this.anchor = _jsPlumb.makeAnchor(this.parentAnchor, this.elementId, _jsPlumb); _jsPlumb.setAttribute(this.canvas, "dragId", placeholderInfo.id); _jsPlumb.setAttribute(this.canvas, "elId", this.elementId); this._jsPlumb.floatingEndpoint = _makeFloatingEndpoint(this.getPaintStyle(), this.anchor, this.endpoint, this.canvas, placeholderInfo.element, _jsPlumb, _newEndpoint); this.canvas.style.visibility = "hidden"; if (jpc == null) { this.anchor.locked = true; this.setHover(false, false); jpc = _newConnection({ sourceEndpoint: this, targetEndpoint: this._jsPlumb.floatingEndpoint, source: this.endpointWillMoveTo || this.element, target: placeholderInfo.element, anchors: [this.anchor, this._jsPlumb.floatingEndpoint.anchor], paintStyle: params.connectorStyle, hoverPaintStyle: params.connectorHoverStyle, connector: params.connector, overlays: params.connectorOverlays, type: this.connectionType, cssClass: this.connectorClass, hoverClass: this.connectorHoverClass }); jpc.pending = true; jpc.addClass(_jsPlumb.draggingClass); this._jsPlumb.floatingEndpoint.addClass(_jsPlumb.draggingClass); _jsPlumb.fire("connectionDrag", jpc); } else {
                            existingJpc = true; jpc.setHover(false); var anchorIdx = jpc.endpoints[0].id == this.id ? 0 : 1; jpc.floatingAnchorIndex = anchorIdx; this.detachFromConnection(jpc, null, true); var dragScope = _jsPlumb.getDragScope(canvasElement); _jsPlumb.setAttribute(this.canvas, "originalScope", dragScope); var dropScope = _jsPlumb.getDropScope(canvasElement); _jsPlumb.setDragScope(canvasElement, dropScope); _jsPlumb.fire("connectionDrag", jpc); if (anchorIdx === 0) { existingJpcParams = [jpc.source, jpc.sourceId, canvasElement, dragScope]; jpc.source = placeholderInfo.element; jpc.sourceId = placeholderInfo.id; } else { existingJpcParams = [jpc.target, jpc.targetId, canvasElement, dragScope]; jpc.target = placeholderInfo.element; jpc.targetId = placeholderInfo.id; }
                            jpc.endpoints[anchorIdx === 0 ? 1 : 0].anchor.locked = true; jpc.suspendedEndpoint = jpc.endpoints[anchorIdx]; jpc.suspendedElement = jpc.endpoints[anchorIdx].getElement(); jpc.suspendedElementId = jpc.endpoints[anchorIdx].elementId; jpc.suspendedElementType = anchorIdx === 0 ? "source" : "target"; jpc.suspendedEndpoint.setHover(false); this._jsPlumb.floatingEndpoint.referenceEndpoint = jpc.suspendedEndpoint; jpc.endpoints[anchorIdx] = this._jsPlumb.floatingEndpoint; jpc.addClass(_jsPlumb.draggingClass); this._jsPlumb.floatingEndpoint.addClass(_jsPlumb.draggingClass);
                        }
                        floatingConnections[placeholderInfo.id] = jpc; _jsPlumb.anchorManager.addFloatingConnection(placeholderInfo.id, jpc); _ju.addToList(params.endpointsByElement, placeholderInfo.id, this._jsPlumb.floatingEndpoint); _jsPlumb.currentlyDragging = true;
                    } .bind(this); var dragOptions = params.dragOptions || {}, defaultOpts = {}, startEvent = jsPlumb.dragEvents.start, stopEvent = jsPlumb.dragEvents.stop, dragEvent = jsPlumb.dragEvents.drag; dragOptions = jsPlumb.extend(defaultOpts, dragOptions); dragOptions.scope = dragOptions.scope || this.scope; dragOptions[startEvent] = _ju.wrap(dragOptions[startEvent], start, false); dragOptions[dragEvent] = _ju.wrap(dragOptions[dragEvent], _dragHandler.drag); dragOptions[stopEvent] = _ju.wrap(dragOptions[stopEvent], function () {
                        _jsPlumb.setConnectionBeingDragged(false); if (jpc && jpc.endpoints != null) {
                            var originalEvent = _jsPlumb.getDropEvent(arguments); var idx = jpc.floatingAnchorIndex == null ? 1 : jpc.floatingAnchorIndex; jpc.endpoints[idx === 0 ? 1 : 0].anchor.locked = false; jpc.removeClass(_jsPlumb.draggingClass); if (jpc.endpoints[idx] == this._jsPlumb.floatingEndpoint) {
                                if (existingJpc && jpc.suspendedEndpoint) {
                                    if (idx === 0) { jpc.source = existingJpcParams[0]; jpc.sourceId = existingJpcParams[1]; } else { jpc.target = existingJpcParams[0]; jpc.targetId = existingJpcParams[1]; }
                                    _jsPlumb.setDragScope(existingJpcParams[2], existingJpcParams[3]); jpc.endpoints[idx] = jpc.suspendedEndpoint; if (jpc.isReattach() || jpc._forceReattach || jpc._forceDetach || !jpc.endpoints[idx === 0 ? 1 : 0].detach(jpc, false, false, true, originalEvent)) { jpc.setHover(false); jpc.floatingAnchorIndex = null; jpc._forceDetach = null; jpc._forceReattach = null; this._jsPlumb.floatingEndpoint.detachFromConnection(jpc); jpc.suspendedEndpoint.addConnection(jpc); _jsPlumb.repaint(existingJpcParams[1]); }
                                    else
                                        jpc.suspendedEndpoint.detachFromConnection(jpc);
                                } 
                            }
                            _jsPlumb.remove(placeholderInfo.element, false); _jsPlumb.deleteObject({ endpoint: inPlaceCopy }); if (this.deleteAfterDragStop) { _jsPlumb.deleteObject({ endpoint: this }); }
                            else { if (this._jsPlumb) { this._jsPlumb.floatingEndpoint = null; this.canvas.style.visibility = "visible"; this.anchor.locked = false; this.paint({ recalc: false }); } }
                            _jsPlumb.fire("connectionDragStop", jpc, originalEvent); _jsPlumb.currentlyDragging = false; jpc = null;
                        } 
                    } .bind(this)); var i = _gel(this.canvas); _jsPlumb.initDraggable(i, dragOptions, true); draggingInitialised = true;
                } 
            }; if (this.isSource || this.isTarget)
            this.initDraggable(); var _initDropTarget = function (canvas, forceInit, isTransient, endpoint) {
                if ((this.isTarget || forceInit) && jsPlumb.isDropSupported(this.element)) {
                    var dropOptions = params.dropOptions || _jsPlumb.Defaults.DropOptions || jsPlumb.Defaults.DropOptions; dropOptions = jsPlumb.extend({}, dropOptions); dropOptions.scope = dropOptions.scope || this.scope; var dropEvent = jsPlumb.dragEvents.drop, overEvent = jsPlumb.dragEvents.over, outEvent = jsPlumb.dragEvents.out, drop = function () {
                        this.removeClass(_jsPlumb.endpointDropAllowedClass); this.removeClass(_jsPlumb.endpointDropForbiddenClass); var originalEvent = _jsPlumb.getDropEvent(arguments), draggable = _jsPlumb.getDOMElement(_jsPlumb.getDragObject(arguments)), id = _jsPlumb.getAttribute(draggable, "dragId"), elId = _jsPlumb.getAttribute(draggable, "elId"), scope = _jsPlumb.getAttribute(draggable, "originalScope"), jpc = floatingConnections[id]; if (jpc != null) {
                            var redrop = jpc.suspendedEndpoint && (jpc.suspendedEndpoint.id == this.id || this.referenceEndpoint && jpc.suspendedEndpoint.id == this.referenceEndpoint.id); if (redrop) { jpc._forceReattach = true; return; }
                            var idx = jpc.floatingAnchorIndex == null ? 1 : jpc.floatingAnchorIndex, oidx = idx === 0 ? 1 : 0; if (scope) _jsPlumb.setDragScope(draggable, scope); var endpointEnabled = endpoint != null ? endpoint.isEnabled() : true; if (this.isFull()) { this.fire("maxConnections", { endpoint: this, connection: jpc, maxConnections: this._jsPlumb.maxConnections }, originalEvent); }
                            if (!this.isFull() && !(idx === 0 && !this.isSource) && !(idx == 1 && !this.isTarget) && endpointEnabled) {
                                var _doContinue = true; if (jpc.suspendedEndpoint && jpc.suspendedEndpoint.id != this.id) {
                                    if (idx === 0) { jpc.source = jpc.suspendedEndpoint.element; jpc.sourceId = jpc.suspendedEndpoint.elementId; } else { jpc.target = jpc.suspendedEndpoint.element; jpc.targetId = jpc.suspendedEndpoint.elementId; }
                                    if (!jpc.isDetachAllowed(jpc) || !jpc.endpoints[idx].isDetachAllowed(jpc) || !jpc.suspendedEndpoint.isDetachAllowed(jpc) || !_jsPlumb.checkCondition("beforeDetach", jpc))
                                        _doContinue = false;
                                }
                                if (idx === 0) { jpc.source = this.element; jpc.sourceId = this.elementId; } else { jpc.target = this.element; jpc.targetId = this.elementId; }
                                var commonFunction = function () { jpc.floatingAnchorIndex = null; }; var continueFunction = function () {
                                    jpc.pending = false; jpc.endpoints[idx].detachFromConnection(jpc); if (jpc.suspendedEndpoint) jpc.suspendedEndpoint.detachFromConnection(jpc); jpc.endpoints[idx] = this; this.addConnection(jpc); var params = this.getParameters(); for (var aParam in params)
                                        jpc.setParameter(aParam, params[aParam]); if (!jpc.suspendedEndpoint) {
                                        if (params.draggable)
                                            jsPlumb.initDraggable(this.element, dragOptions, true, _jsPlumb);
                                    }
                                    else { var suspendedElement = jpc.suspendedEndpoint.getElement(), suspendedElementId = jpc.suspendedEndpoint.elementId; _fireMoveEvent({ index: idx, originalSourceId: idx === 0 ? suspendedElementId : jpc.sourceId, newSourceId: idx === 0 ? this.elementId : jpc.sourceId, originalTargetId: idx == 1 ? suspendedElementId : jpc.targetId, newTargetId: idx == 1 ? this.elementId : jpc.targetId, originalSourceEndpoint: idx === 0 ? jpc.suspendedEndpoint : jpc.endpoints[0], newSourceEndpoint: idx === 0 ? this : jpc.endpoints[0], originalTargetEndpoint: idx == 1 ? jpc.suspendedEndpoint : jpc.endpoints[1], newTargetEndpoint: idx == 1 ? this : jpc.endpoints[1], connection: jpc }, originalEvent); }
                                    if (idx == 1)
                                        _jsPlumb.anchorManager.updateOtherEndpoint(jpc.sourceId, jpc.suspendedElementId, jpc.targetId, jpc); else
                                        _jsPlumb.anchorManager.sourceChanged(jpc.suspendedEndpoint.elementId, jpc.sourceId, jpc); _finaliseConnection(jpc, null, originalEvent); commonFunction();
                                } .bind(this); var dontContinueFunction = function () {
                                    if (jpc.suspendedEndpoint) {
                                        jpc.endpoints[idx] = jpc.suspendedEndpoint; jpc.setHover(false); jpc._forceDetach = true; if (idx === 0) { jpc.source = jpc.suspendedEndpoint.element; jpc.sourceId = jpc.suspendedEndpoint.elementId; } else { jpc.target = jpc.suspendedEndpoint.element; jpc.targetId = jpc.suspendedEndpoint.elementId; }
                                        jpc.suspendedEndpoint.addConnection(jpc); jpc.endpoints[0].repaint(); jpc.repaint(); _jsPlumb.repaint(jpc.sourceId); jpc._forceDetach = false;
                                    }
                                    commonFunction();
                                }; _doContinue = _doContinue && this.isDropAllowed(jpc.sourceId, jpc.targetId, jpc.scope, jpc, this); if (_doContinue) { continueFunction(); }
                                else { dontContinueFunction(); } 
                            }
                            _jsPlumb.currentlyDragging = false;
                        } 
                    } .bind(this); dropOptions[dropEvent] = _ju.wrap(dropOptions[dropEvent], drop); dropOptions[overEvent] = _ju.wrap(dropOptions[overEvent], function () { var draggable = jsPlumb.getDragObject(arguments), id = _jsPlumb.getAttribute(jsPlumb.getDOMElement(draggable), "dragId"), _jpc = floatingConnections[id]; if (_jpc != null) { var idx = _jpc.floatingAnchorIndex == null ? 1 : _jpc.floatingAnchorIndex; var _cont = (this.isTarget && _jpc.floatingAnchorIndex !== 0) || (_jpc.suspendedEndpoint && this.referenceEndpoint && this.referenceEndpoint.id == _jpc.suspendedEndpoint.id); if (_cont) { var bb = _jsPlumb.checkCondition("checkDropAllowed", { sourceEndpoint: _jpc.endpoints[idx], targetEndpoint: this, connection: _jpc }); this[(bb ? "add" : "remove") + "Class"](_jsPlumb.endpointDropAllowedClass); this[(bb ? "remove" : "add") + "Class"](_jsPlumb.endpointDropForbiddenClass); _jpc.endpoints[idx].anchor.over(this.anchor, this); } } } .bind(this)); dropOptions[outEvent] = _ju.wrap(dropOptions[outEvent], function () { var draggable = jsPlumb.getDragObject(arguments), id = draggable == null ? null : _jsPlumb.getAttribute(jsPlumb.getDOMElement(draggable), "dragId"), _jpc = id ? floatingConnections[id] : null; if (_jpc != null) { var idx = _jpc.floatingAnchorIndex == null ? 1 : _jpc.floatingAnchorIndex; var _cont = (this.isTarget && _jpc.floatingAnchorIndex !== 0) || (_jpc.suspendedEndpoint && this.referenceEndpoint && this.referenceEndpoint.id == _jpc.suspendedEndpoint.id); if (_cont) { this.removeClass(_jsPlumb.endpointDropAllowedClass); this.removeClass(_jsPlumb.endpointDropForbiddenClass); _jpc.endpoints[idx].anchor.out(); } } } .bind(this)); _jsPlumb.initDroppable(canvas, dropOptions, true, isTransient);
                } 
            } .bind(this); if (!this.anchor.isFloating)
            _initDropTarget(_gel(this.canvas), true, !(params._transient || this.anchor.isFloating), this); if (params.type)
            this.addType(params.type, params.data, _jsPlumb.isSuspendDrawing()); return this;
    }; jsPlumbUtil.extend(jsPlumb.Endpoint, OverlayCapableJsPlumbUIComponent, { getTypeDescriptor: function () { return "endpoint"; }, isVisible: function () { return this._jsPlumb.visible; }, setVisible: function (v, doNotChangeConnections, doNotNotifyOtherEndpoint) { this._jsPlumb.visible = v; if (this.canvas) this.canvas.style.display = v ? "block" : "none"; this[v ? "showOverlays" : "hideOverlays"](); if (!doNotChangeConnections) { for (var i = 0; i < this.connections.length; i++) { this.connections[i].setVisible(v); if (!doNotNotifyOtherEndpoint) { var oIdx = this === this.connections[i].endpoints[0] ? 1 : 0; if (this.connections[i].endpoints[oIdx].connections.length == 1) this.connections[i].endpoints[oIdx].setVisible(v, true, true); } } } }, getAttachedElements: function () { return this.connections; }, applyType: function (t, doNotRepaint) { if (t.maxConnections != null) this._jsPlumb.maxConnections = t.maxConnections; if (t.scope) this.scope = t.scope; jsPlumb.extend(this, t, typeParameters); if (t.anchor) { this.anchor = this._jsPlumb.instance.makeAnchor(t.anchor); } }, isEnabled: function () { return this._jsPlumb.enabled; }, setEnabled: function (e) { this._jsPlumb.enabled = e; }, cleanup: function () { jsPlumbAdapter.removeClass(this.element, this._jsPlumb.instance.endpointAnchorClassPrefix + "_" + this._jsPlumb.currentAnchorClass); this.anchor = null; this.endpoint.cleanup(); this.endpoint.destroy(); this.endpoint = null; var i = jsPlumb.getElementObject(this.canvas); this._jsPlumb.instance.destroyDraggable(i); this._jsPlumb.instance.destroyDroppable(i); }, setHover: function (h) {
        if (this.endpoint && this._jsPlumb && !this._jsPlumb.instance.isConnectionBeingDragged())
            this.endpoint.setHover(h);
    }, isFull: function () { return !(this.isFloating() || this._jsPlumb.maxConnections < 1 || this.connections.length < this._jsPlumb.maxConnections); }, isFloating: function () { return this.anchor != null && this.anchor.isFloating; }, getConnectionCost: function () { return this._jsPlumb.connectionCost; }, setConnectionCost: function (c) { this._jsPlumb.connectionCost = c; }, areConnectionsDirected: function () { return this._jsPlumb.connectionsDirected; }, setConnectionsDirected: function (b) { this._jsPlumb.connectionsDirected = b; }, setElementId: function (_elId) { this.elementId = _elId; this.anchor.elementId = _elId; }, setReferenceElement: function (_el) { this.element = jsPlumb.getDOMElement(_el); }, setDragAllowedWhenFull: function (allowed) { this.dragAllowedWhenFull = allowed; }, equals: function (endpoint) { return this.anchor.equals(endpoint.anchor); }, getUuid: function () { return this._jsPlumb.uuid; }, computeAnchor: function (params) { return this.anchor.compute(params); } 
    });
})(); ; (function () {
    "use strict"; var makeConnector = function (_jsPlumb, renderMode, connectorName, connectorArgs) {
        if (!_jsPlumb.Defaults.DoNotThrowErrors && jsPlumb.Connectors[renderMode][connectorName] == null)
            throw { msg: "jsPlumb: unknown connector type '" + connectorName + "'" }; return new jsPlumb.Connectors[renderMode][connectorName](connectorArgs);
    }, _makeAnchor = function (anchorParams, elementId, _jsPlumb) { return (anchorParams) ? _jsPlumb.makeAnchor(anchorParams, elementId, _jsPlumb) : null; }; jsPlumb.Connection = function (params) {
        var _newConnection = params.newConnection, _newEndpoint = params.newEndpoint, _gel = jsPlumb.getElementObject, _ju = jsPlumbUtil; this.connector = null; this.idPrefix = "_jsplumb_c_"; this.defaultLabelLocation = 0.5; this.defaultOverlayKeys = ["Overlays", "ConnectionOverlays"]; this.previousConnection = params.previousConnection; this.source = jsPlumb.getDOMElement(params.source); this.target = jsPlumb.getDOMElement(params.target); if (params.sourceEndpoint) this.source = params.sourceEndpoint.endpointWillMoveTo || params.sourceEndpoint.getElement(); if (params.targetEndpoint) this.target = params.targetEndpoint.getElement(); OverlayCapableJsPlumbUIComponent.apply(this, arguments); this.sourceId = this._jsPlumb.instance.getId(this.source); this.targetId = this._jsPlumb.instance.getId(this.target); this.scope = params.scope; this.endpoints = []; this.endpointStyles = []; var _jsPlumb = this._jsPlumb.instance; this._jsPlumb.visible = true; this._jsPlumb.editable = params.editable === true; this._jsPlumb.params = { cssClass: params.cssClass, container: params.container, "pointer-events": params["pointer-events"], editorParams: params.editorParams }; this._jsPlumb.lastPaintedAt = null; this.getDefaultType = function () { return { parameters: {}, scope: null, detachable: this._jsPlumb.instance.Defaults.ConnectionsDetachable, rettach: this._jsPlumb.instance.Defaults.ReattachConnections, paintStyle: this._jsPlumb.instance.Defaults.PaintStyle || jsPlumb.Defaults.PaintStyle, connector: this._jsPlumb.instance.Defaults.Connector || jsPlumb.Defaults.Connector, hoverPaintStyle: this._jsPlumb.instance.Defaults.HoverPaintStyle || jsPlumb.Defaults.HoverPaintStyle, overlays: this._jsPlumb.instance.Defaults.ConnectorOverlays || jsPlumb.Defaults.ConnectorOverlays }; }; this.makeEndpoint = function (isSource, el, elId, ep) { elId = elId || this._jsPlumb.instance.getId(el); return this.prepareEndpoint(_jsPlumb, _newEndpoint, this, ep, isSource ? 0 : 1, params, el, elId); }; var eS = this.makeEndpoint(true, this.source, this.sourceId, params.sourceEndpoint), eT = this.makeEndpoint(false, this.target, this.targetId, params.targetEndpoint); if (eS) _ju.addToList(params.endpointsByElement, this.sourceId, eS); if (eT) _ju.addToList(params.endpointsByElement, this.targetId, eT); if (!this.scope) this.scope = this.endpoints[0].scope; if (params.deleteEndpointsOnDetach != null) { this.endpoints[0]._deleteOnDetach = params.deleteEndpointsOnDetach; this.endpoints[1]._deleteOnDetach = params.deleteEndpointsOnDetach; }
        else { if (!this.endpoints[0]._doNotDeleteOnDetach) this.endpoints[0]._deleteOnDetach = true; if (!this.endpoints[1]._doNotDeleteOnDetach) this.endpoints[1]._deleteOnDetach = true; }
        this.setConnector(this.endpoints[0].connector || this.endpoints[1].connector || params.connector || _jsPlumb.Defaults.Connector || jsPlumb.Defaults.Connector, true); if (params.path)
            this.connector.setPath(params.path); this.setPaintStyle(this.endpoints[0].connectorStyle || this.endpoints[1].connectorStyle || params.paintStyle || _jsPlumb.Defaults.PaintStyle || jsPlumb.Defaults.PaintStyle, true); this.setHoverPaintStyle(this.endpoints[0].connectorHoverStyle || this.endpoints[1].connectorHoverStyle || params.hoverPaintStyle || _jsPlumb.Defaults.HoverPaintStyle || jsPlumb.Defaults.HoverPaintStyle, true); this._jsPlumb.paintStyleInUse = this.getPaintStyle(); var _suspendedAt = _jsPlumb.getSuspendedAt(); _jsPlumb.updateOffset({ elId: this.sourceId, timestamp: _suspendedAt }); _jsPlumb.updateOffset({ elId: this.targetId, timestamp: _suspendedAt }); if (!_jsPlumb.isSuspendDrawing()) { var myInfo = _jsPlumb.getCachedData(this.sourceId), myOffset = myInfo.o, myWH = myInfo.s, otherInfo = _jsPlumb.getCachedData(this.targetId), otherOffset = otherInfo.o, otherWH = otherInfo.s, initialTimestamp = _suspendedAt || _jsPlumb.timestamp(), anchorLoc = this.endpoints[0].anchor.compute({ xy: [myOffset.left, myOffset.top], wh: myWH, element: this.endpoints[0], elementId: this.endpoints[0].elementId, txy: [otherOffset.left, otherOffset.top], twh: otherWH, tElement: this.endpoints[1], timestamp: initialTimestamp }); this.endpoints[0].paint({ anchorLoc: anchorLoc, timestamp: initialTimestamp }); anchorLoc = this.endpoints[1].anchor.compute({ xy: [otherOffset.left, otherOffset.top], wh: otherWH, element: this.endpoints[1], elementId: this.endpoints[1].elementId, txy: [myOffset.left, myOffset.top], twh: myWH, tElement: this.endpoints[0], timestamp: initialTimestamp }); this.endpoints[1].paint({ anchorLoc: anchorLoc, timestamp: initialTimestamp }); }
        this._jsPlumb.detachable = _jsPlumb.Defaults.ConnectionsDetachable; if (params.detachable === false) this._jsPlumb.detachable = false; if (this.endpoints[0].connectionsDetachable === false) this._jsPlumb.detachable = false; if (this.endpoints[1].connectionsDetachable === false) this._jsPlumb.detachable = false; this._jsPlumb.reattach = params.reattach || this.endpoints[0].reattachConnections || this.endpoints[1].reattachConnections || _jsPlumb.Defaults.ReattachConnections; this._jsPlumb.cost = params.cost || this.endpoints[0].getConnectionCost(); this._jsPlumb.directed = params.directed; if (params.directed == null) this._jsPlumb.directed = this.endpoints[0].areConnectionsDirected(); var _p = jsPlumb.extend({}, this.endpoints[1].getParameters()); jsPlumb.extend(_p, this.endpoints[0].getParameters()); jsPlumb.extend(_p, this.getParameters()); this.setParameters(_p); var _types = [params.type, this.endpoints[0].connectionType, this.endpoints[1].connectionType].join(" "); if (/[^\s]/.test(_types))
            this.addType(_types, params.data, true);
    }; jsPlumbUtil.extend(jsPlumb.Connection, OverlayCapableJsPlumbUIComponent, { applyType: function (t, doNotRepaint) { if (t.detachable != null) this.setDetachable(t.detachable); if (t.reattach != null) this.setReattach(t.reattach); if (t.scope) this.scope = t.scope; this.setConnector(t.connector, doNotRepaint); }, getTypeDescriptor: function () { return "connection"; }, getAttachedElements: function () { return this.endpoints; }, addClass: function (c, informEndpoints) {
        if (informEndpoints) { this.endpoints[0].addClass(c); this.endpoints[1].addClass(c); if (this.suspendedEndpoint) this.suspendedEndpoint.addClass(c); }
        if (this.connector) { this.connector.addClass(c); } 
    }, removeClass: function (c, informEndpoints) {
        if (informEndpoints) { this.endpoints[0].removeClass(c); this.endpoints[1].removeClass(c); if (this.suspendedEndpoint) this.suspendedEndpoint.removeClass(c); }
        if (this.connector) { this.connector.removeClass(c); } 
    }, isVisible: function () { return this._jsPlumb.visible; }, setVisible: function (v) {
        this._jsPlumb.visible = v; if (this.connector)
            this.connector.setVisible(v); this.repaint();
    }, cleanup: function () {
        this.endpoints = null; this.source = null; this.target = null; if (this.connector != null) { this.connector.cleanup(); this.connector.destroy(); }
        this.connector = null;
    }, isDetachable: function () { return this._jsPlumb.detachable === true; }, setDetachable: function (detachable) { this._jsPlumb.detachable = detachable === true; }, isReattach: function () { return this._jsPlumb.reattach === true; }, setReattach: function (reattach) { this._jsPlumb.reattach = reattach === true; }, setHover: function (state) { if (this.connector && this._jsPlumb && !this._jsPlumb.instance.isConnectionBeingDragged()) { this.connector.setHover(state); jsPlumbAdapter[state ? "addClass" : "removeClass"](this.source, this._jsPlumb.instance.hoverSourceClass); jsPlumbAdapter[state ? "addClass" : "removeClass"](this.target, this._jsPlumb.instance.hoverTargetClass); } }, getCost: function () { return this._jsPlumb.cost; }, setCost: function (c) { this._jsPlumb.cost = c; }, isDirected: function () { return this._jsPlumb.directed === true; }, getConnector: function () { return this.connector; }, setConnector: function (connectorSpec, doNotRepaint) {
        var _ju = jsPlumbUtil; if (this.connector != null) { this.connector.cleanup(); this.connector.destroy(); }
        var connectorArgs = { _jsPlumb: this._jsPlumb.instance, cssClass: this._jsPlumb.params.cssClass, container: this._jsPlumb.params.container, "pointer-events": this._jsPlumb.params["pointer-events"] }, renderMode = this._jsPlumb.instance.getRenderMode(); if (_ju.isString(connectorSpec))
            this.connector = makeConnector(this._jsPlumb.instance, renderMode, connectorSpec, connectorArgs); else if (_ju.isArray(connectorSpec)) {
            if (connectorSpec.length == 1)
                this.connector = makeConnector(this._jsPlumb.instance, renderMode, connectorSpec[0], connectorArgs); else
                this.connector = makeConnector(this._jsPlumb.instance, renderMode, connectorSpec[0], _ju.merge(connectorSpec[1], connectorArgs));
        }
        this.bindListeners(this.connector, this, function (state) { this.setHover(state, false); } .bind(this)); this.canvas = this.connector.canvas; this.bgCanvas = this.connector.bgCanvas; if (this._jsPlumb.editable && jsPlumb.ConnectorEditors != null && jsPlumb.ConnectorEditors[this.connector.type] && this.connector.isEditable()) { new jsPlumb.ConnectorEditors[this.connector.type]({ connector: this.connector, connection: this, params: this._jsPlumb.params.editorParams || {} }); }
        else { this._jsPlumb.editable = false; }
        if (!doNotRepaint) this.repaint();
    }, paint: function (params) {
        if (!this._jsPlumb.instance.isSuspendDrawing() && this._jsPlumb.visible) {
            params = params || {}; var elId = params.elId, ui = params.ui, recalc = params.recalc, timestamp = params.timestamp, swap = false, tId = swap ? this.sourceId : this.targetId, sId = swap ? this.targetId : this.sourceId, tIdx = swap ? 0 : 1, sIdx = swap ? 1 : 0; if (timestamp == null || timestamp != this._jsPlumb.lastPaintedAt) {
                var sourceInfo = this._jsPlumb.instance.updateOffset({ elId: sId, offset: ui, recalc: recalc, timestamp: timestamp }).o, targetInfo = this._jsPlumb.instance.updateOffset({ elId: tId, timestamp: timestamp }).o, sE = this.endpoints[sIdx], tE = this.endpoints[tIdx]; if (params.clearEdits) { this._jsPlumb.overlayPositions = null; sE.anchor.clearUserDefinedLocation(); tE.anchor.clearUserDefinedLocation(); this.connector.setEdited(false); }
                var sAnchorP = sE.anchor.getCurrentLocation({ xy: [sourceInfo.left, sourceInfo.top], wh: [sourceInfo.width, sourceInfo.height], element: sE, timestamp: timestamp }), tAnchorP = tE.anchor.getCurrentLocation({ xy: [targetInfo.left, targetInfo.top], wh: [targetInfo.width, targetInfo.height], element: tE, timestamp: timestamp }); this.connector.resetBounds(); this.connector.compute({ sourcePos: sAnchorP, targetPos: tAnchorP, sourceEndpoint: this.endpoints[sIdx], targetEndpoint: this.endpoints[tIdx], lineWidth: this._jsPlumb.paintStyleInUse.lineWidth, sourceInfo: sourceInfo, targetInfo: targetInfo, clearEdits: params.clearEdits === true }); var overlayExtents = { minX: Infinity, minY: Infinity, maxX: -Infinity, maxY: -Infinity }; for (var i = 0; i < this._jsPlumb.overlays.length; i++) { var o = this._jsPlumb.overlays[i]; if (o.isVisible()) { this._jsPlumb.overlayPlacements[i] = o.draw(this.connector, this._jsPlumb.paintStyleInUse, this.getAbsoluteOverlayPosition(o)); overlayExtents.minX = Math.min(overlayExtents.minX, this._jsPlumb.overlayPlacements[i].minX); overlayExtents.maxX = Math.max(overlayExtents.maxX, this._jsPlumb.overlayPlacements[i].maxX); overlayExtents.minY = Math.min(overlayExtents.minY, this._jsPlumb.overlayPlacements[i].minY); overlayExtents.maxY = Math.max(overlayExtents.maxY, this._jsPlumb.overlayPlacements[i].maxY); } }
                var lineWidth = parseFloat(this._jsPlumb.paintStyleInUse.lineWidth || 1) / 2, outlineWidth = parseFloat(this._jsPlumb.paintStyleInUse.lineWidth || 0), extents = { xmin: Math.min(this.connector.bounds.minX - (lineWidth + outlineWidth), overlayExtents.minX), ymin: Math.min(this.connector.bounds.minY - (lineWidth + outlineWidth), overlayExtents.minY), xmax: Math.max(this.connector.bounds.maxX + (lineWidth + outlineWidth), overlayExtents.maxX), ymax: Math.max(this.connector.bounds.maxY + (lineWidth + outlineWidth), overlayExtents.maxY) }; this.connector.paint(this._jsPlumb.paintStyleInUse, null, extents); for (var j = 0; j < this._jsPlumb.overlays.length; j++) { var p = this._jsPlumb.overlays[j]; if (p.isVisible()) { p.paint(this._jsPlumb.overlayPlacements[j], extents); } } 
            }
            this._jsPlumb.lastPaintedAt = timestamp;
        } 
    }, repaint: function (params) { params = params || {}; this.paint({ elId: this.sourceId, recalc: !(params.recalc === false), timestamp: params.timestamp, clearEdits: params.clearEdits }); }, prepareEndpoint: function (_jsPlumb, _newEndpoint, conn, existing, index, params, element, elementId) {
        var e; if (existing) { conn.endpoints[index] = existing; existing.addConnection(conn); } else {
            if (!params.endpoints) params.endpoints = [null, null]; var ep = params.endpoints[index] || params.endpoint || _jsPlumb.Defaults.Endpoints[index] || jsPlumb.Defaults.Endpoints[index] || _jsPlumb.Defaults.Endpoint || jsPlumb.Defaults.Endpoint; if (!params.endpointStyles) params.endpointStyles = [null, null]; if (!params.endpointHoverStyles) params.endpointHoverStyles = [null, null]; var es = params.endpointStyles[index] || params.endpointStyle || _jsPlumb.Defaults.EndpointStyles[index] || jsPlumb.Defaults.EndpointStyles[index] || _jsPlumb.Defaults.EndpointStyle || jsPlumb.Defaults.EndpointStyle; if (es.fillStyle == null && params.paintStyle != null)
                es.fillStyle = params.paintStyle.strokeStyle; if (es.outlineColor == null && params.paintStyle != null)
                es.outlineColor = params.paintStyle.outlineColor; if (es.outlineWidth == null && params.paintStyle != null)
                es.outlineWidth = params.paintStyle.outlineWidth; var ehs = params.endpointHoverStyles[index] || params.endpointHoverStyle || _jsPlumb.Defaults.EndpointHoverStyles[index] || jsPlumb.Defaults.EndpointHoverStyles[index] || _jsPlumb.Defaults.EndpointHoverStyle || jsPlumb.Defaults.EndpointHoverStyle; if (params.hoverPaintStyle != null) { if (ehs == null) ehs = {}; if (ehs.fillStyle == null) { ehs.fillStyle = params.hoverPaintStyle.strokeStyle; } }
            var a = params.anchors ? params.anchors[index] : params.anchor ? params.anchor : _makeAnchor(_jsPlumb.Defaults.Anchors[index], elementId, _jsPlumb) || _makeAnchor(jsPlumb.Defaults.Anchors[index], elementId, _jsPlumb) || _makeAnchor(_jsPlumb.Defaults.Anchor, elementId, _jsPlumb) || _makeAnchor(jsPlumb.Defaults.Anchor, elementId, _jsPlumb), u = params.uuids ? params.uuids[index] : null; e = _newEndpoint({ paintStyle: es, hoverPaintStyle: ehs, endpoint: ep, connections: [conn], uuid: u, anchor: a, source: element, scope: params.scope, reattach: params.reattach || _jsPlumb.Defaults.ReattachConnections, detachable: params.detachable || _jsPlumb.Defaults.ConnectionsDetachable }); conn.endpoints[index] = e; if (params.drawEndpoints === false) e.setVisible(false, true, true);
        }
        return e;
    } 
    });
})(); ; (function () {
    jsPlumb.AnchorManager = function (params) {
        var _amEndpoints = {}, continuousAnchors = {}, continuousAnchorLocations = {}, userDefinedContinuousAnchorLocations = {}, continuousAnchorOrientations = {}, Orientation = { HORIZONTAL: "horizontal", VERTICAL: "vertical", DIAGONAL: "diagonal", IDENTITY: "identity" }, connectionsByElementId = {}, self = this, anchorLists = {}, jsPlumbInstance = params.jsPlumbInstance, floatingConnections = {}, calculateOrientation = function (sourceId, targetId, sd, td, sourceAnchor, targetAnchor) {
            if (sourceId === targetId) return { orientation: Orientation.IDENTITY, a: ["top", "top"] }; var theta = Math.atan2((td.centery - sd.centery), (td.centerx - sd.centerx)), theta2 = Math.atan2((sd.centery - td.centery), (sd.centerx - td.centerx)), h = ((sd.left <= td.left && sd.right >= td.left) || (sd.left <= td.right && sd.right >= td.right) || (sd.left <= td.left && sd.right >= td.right) || (td.left <= sd.left && td.right >= sd.right)), v = ((sd.top <= td.top && sd.bottom >= td.top) || (sd.top <= td.bottom && sd.bottom >= td.bottom) || (sd.top <= td.top && sd.bottom >= td.bottom) || (td.top <= sd.top && td.bottom >= sd.bottom)), possiblyTranslateEdges = function (edges) { return [sourceAnchor.isContinuous ? sourceAnchor.verifyEdge(edges[0]) : edges[0], targetAnchor.isContinuous ? targetAnchor.verifyEdge(edges[1]) : edges[1]]; }, out = { orientation: Orientation.DIAGONAL, theta: theta, theta2: theta2 }; if (!(h || v)) {
                if (td.left > sd.left && td.top > sd.top)
                    out.a = ["right", "top"]; else if (td.left > sd.left && sd.top > td.top)
                    out.a = ["top", "left"]; else if (td.left < sd.left && td.top < sd.top)
                    out.a = ["top", "right"]; else if (td.left < sd.left && td.top > sd.top)
                    out.a = ["left", "top"];
            }
            else if (h) { out.orientation = Orientation.HORIZONTAL; out.a = sd.top < td.top ? ["bottom", "top"] : ["top", "bottom"]; }
            else { out.orientation = Orientation.VERTICAL; out.a = sd.left < td.left ? ["right", "left"] : ["left", "right"]; }
            out.a = possiblyTranslateEdges(out.a); return out;
        }, placeAnchorsOnLine = function (desc, elementDimensions, elementPosition, connections, horizontal, otherMultiplier, reverse) {
            var a = [], step = elementDimensions[horizontal ? 0 : 1] / (connections.length + 1); for (var i = 0; i < connections.length; i++) {
                var val = (i + 1) * step, other = otherMultiplier * elementDimensions[horizontal ? 1 : 0]; if (reverse)
                    val = elementDimensions[horizontal ? 0 : 1] - val; var dx = (horizontal ? val : other), x = elementPosition[0] + dx, xp = dx / elementDimensions[0], dy = (horizontal ? other : val), y = elementPosition[1] + dy, yp = dy / elementDimensions[1]; a.push([x, y, xp, yp, connections[i][1], connections[i][2]]);
            }
            return a;
        }, currySort = function (reverseAngles) {
            return function (a, b) {
                var r = true; if (reverseAngles) { r = a[0][0] < b[0][0]; }
                else { r = a[0][0] > b[0][0]; }
                return r === false ? -1 : 1;
            };
        }, leftSort = function (a, b) { var p1 = a[0][0] < 0 ? -Math.PI - a[0][0] : Math.PI - a[0][0], p2 = b[0][0] < 0 ? -Math.PI - b[0][0] : Math.PI - b[0][0]; if (p1 > p2) return 1; else return a[0][1] > b[0][1] ? 1 : -1; }, edgeSortFunctions = { "top": function (a, b) { return a[0] > b[0] ? 1 : -1; }, "right": currySort(true), "bottom": currySort(true), "left": leftSort }, _sortHelper = function (_array, _fn) { return _array.sort(_fn); }, placeAnchors = function (elementId, _anchorLists) {
            var cd = jsPlumbInstance.getCachedData(elementId), sS = cd.s, sO = cd.o, placeSomeAnchors = function (desc, elementDimensions, elementPosition, unsortedConnections, isHorizontal, otherMultiplier, orientation) {
                if (unsortedConnections.length > 0) {
                    var sc = _sortHelper(unsortedConnections, edgeSortFunctions[desc]), reverse = desc === "right" || desc === "top", anchors = placeAnchorsOnLine(desc, elementDimensions, elementPosition, sc, isHorizontal, otherMultiplier, reverse); var _setAnchorLocation = function (endpoint, anchorPos) { continuousAnchorLocations[endpoint.id] = [anchorPos[0], anchorPos[1], anchorPos[2], anchorPos[3]]; continuousAnchorOrientations[endpoint.id] = orientation; }; for (var i = 0; i < anchors.length; i++) {
                        var c = anchors[i][4], weAreSource = c.endpoints[0].elementId === elementId, weAreTarget = c.endpoints[1].elementId === elementId; if (weAreSource)
                            _setAnchorLocation(c.endpoints[0], anchors[i]); else if (weAreTarget)
                            _setAnchorLocation(c.endpoints[1], anchors[i]);
                    } 
                } 
            }; placeSomeAnchors("bottom", sS, [sO.left, sO.top], _anchorLists.bottom, true, 1, [0, 1]); placeSomeAnchors("top", sS, [sO.left, sO.top], _anchorLists.top, true, 0, [0, -1]); placeSomeAnchors("left", sS, [sO.left, sO.top], _anchorLists.left, false, 0, [-1, 0]); placeSomeAnchors("right", sS, [sO.left, sO.top], _anchorLists.right, false, 1, [1, 0]);
        }; this.reset = function () { _amEndpoints = {}; connectionsByElementId = {}; anchorLists = {}; }; this.addFloatingConnection = function (key, conn) { floatingConnections[key] = conn; }; this.removeFloatingConnection = function (key) { delete floatingConnections[key]; }; this.newConnection = function (conn) {
            var sourceId = conn.sourceId, targetId = conn.targetId, ep = conn.endpoints, doRegisterTarget = true, registerConnection = function (otherIndex, otherEndpoint, otherAnchor, elId, c) {
                if ((sourceId == targetId) && otherAnchor.isContinuous) { conn._jsPlumb.instance.removeElement(ep[1].canvas); doRegisterTarget = false; }
                jsPlumbUtil.addToList(connectionsByElementId, elId, [c, otherEndpoint, otherAnchor.constructor == jsPlumb.DynamicAnchor]);
            }; registerConnection(0, ep[0], ep[0].anchor, targetId, conn); if (doRegisterTarget)
                registerConnection(1, ep[1], ep[1].anchor, sourceId, conn);
        }; var removeEndpointFromAnchorLists = function (endpoint) { (function (list, eId) { if (list) { var f = function (e) { return e[4] == eId; }; jsPlumbUtil.removeWithFunction(list.top, f); jsPlumbUtil.removeWithFunction(list.left, f); jsPlumbUtil.removeWithFunction(list.bottom, f); jsPlumbUtil.removeWithFunction(list.right, f); } })(anchorLists[endpoint.elementId], endpoint.id); }; this.connectionDetached = function (connInfo) {
            var connection = connInfo.connection || connInfo, sourceId = connInfo.sourceId, targetId = connInfo.targetId, ep = connection.endpoints, removeConnection = function (otherIndex, otherEndpoint, otherAnchor, elId, c) {
                if (otherAnchor != null && otherAnchor.constructor == jsPlumb.FloatingAnchor) { }
                else { jsPlumbUtil.removeWithFunction(connectionsByElementId[elId], function (_c) { return _c[0].id == c.id; }); } 
            }; removeConnection(1, ep[1], ep[1].anchor, sourceId, connection); removeConnection(0, ep[0], ep[0].anchor, targetId, connection); removeEndpointFromAnchorLists(connection.endpoints[0]); removeEndpointFromAnchorLists(connection.endpoints[1]); self.redraw(connection.sourceId); self.redraw(connection.targetId);
        }; this.add = function (endpoint, elementId) { jsPlumbUtil.addToList(_amEndpoints, elementId, endpoint); }; this.changeId = function (oldId, newId) { connectionsByElementId[newId] = connectionsByElementId[oldId]; _amEndpoints[newId] = _amEndpoints[oldId]; delete connectionsByElementId[oldId]; delete _amEndpoints[oldId]; }; this.getConnectionsFor = function (elementId) { return connectionsByElementId[elementId] || []; }; this.getEndpointsFor = function (elementId) { return _amEndpoints[elementId] || []; }; this.deleteEndpoint = function (endpoint) { jsPlumbUtil.removeWithFunction(_amEndpoints[endpoint.elementId], function (e) { return e.id == endpoint.id; }); removeEndpointFromAnchorLists(endpoint); }; this.clearFor = function (elementId) { delete _amEndpoints[elementId]; _amEndpoints[elementId] = []; }; var _updateAnchorList = function (lists, theta, order, conn, aBoolean, otherElId, idx, reverse, edgeId, elId, connsToPaint, endpointsToPaint) {
            var exactIdx = -1, firstMatchingElIdx = -1, endpoint = conn.endpoints[idx], endpointId = endpoint.id, oIdx = [1, 0][idx], values = [[theta, order], conn, aBoolean, otherElId, endpointId], listToAddTo = lists[edgeId], listToRemoveFrom = endpoint._continuousAnchorEdge ? lists[endpoint._continuousAnchorEdge] : null; if (listToRemoveFrom) { var rIdx = jsPlumbUtil.findWithFunction(listToRemoveFrom, function (e) { return e[4] == endpointId; }); if (rIdx != -1) { listToRemoveFrom.splice(rIdx, 1); for (var i = 0; i < listToRemoveFrom.length; i++) { jsPlumbUtil.addWithFunction(connsToPaint, listToRemoveFrom[i][1], function (c) { return c.id == listToRemoveFrom[i][1].id; }); jsPlumbUtil.addWithFunction(endpointsToPaint, listToRemoveFrom[i][1].endpoints[idx], function (e) { return e.id == listToRemoveFrom[i][1].endpoints[idx].id; }); jsPlumbUtil.addWithFunction(endpointsToPaint, listToRemoveFrom[i][1].endpoints[oIdx], function (e) { return e.id == listToRemoveFrom[i][1].endpoints[oIdx].id; }); } } }
            for (i = 0; i < listToAddTo.length; i++) {
                if (params.idx == 1 && listToAddTo[i][3] === otherElId && firstMatchingElIdx == -1)
                    firstMatchingElIdx = i; jsPlumbUtil.addWithFunction(connsToPaint, listToAddTo[i][1], function (c) { return c.id == listToAddTo[i][1].id; }); jsPlumbUtil.addWithFunction(endpointsToPaint, listToAddTo[i][1].endpoints[idx], function (e) { return e.id == listToAddTo[i][1].endpoints[idx].id; }); jsPlumbUtil.addWithFunction(endpointsToPaint, listToAddTo[i][1].endpoints[oIdx], function (e) { return e.id == listToAddTo[i][1].endpoints[oIdx].id; });
            }
            if (exactIdx != -1) { listToAddTo[exactIdx] = values; }
            else { var insertIdx = reverse ? firstMatchingElIdx != -1 ? firstMatchingElIdx : 0 : listToAddTo.length; listToAddTo.splice(insertIdx, 0, values); }
            endpoint._continuousAnchorEdge = edgeId;
        }; this.updateOtherEndpoint = function (elId, oldTargetId, newTargetId, connection) {
            var sIndex = jsPlumbUtil.findWithFunction(connectionsByElementId[elId], function (i) { return i[0].id === connection.id; }), tIndex = jsPlumbUtil.findWithFunction(connectionsByElementId[oldTargetId], function (i) { return i[0].id === connection.id; }); if (sIndex != -1) { connectionsByElementId[elId][sIndex][0] = connection; connectionsByElementId[elId][sIndex][1] = connection.endpoints[1]; connectionsByElementId[elId][sIndex][2] = connection.endpoints[1].anchor.constructor == jsPlumb.DynamicAnchor; }
            if (tIndex > -1) { connectionsByElementId[oldTargetId].splice(tIndex, 1); jsPlumbUtil.addToList(connectionsByElementId, newTargetId, [connection, connection.endpoints[0], connection.endpoints[0].anchor.constructor == jsPlumb.DynamicAnchor]); } 
        }; this.sourceChanged = function (originalId, newId, connection) {
            if (originalId !== newId) {
                jsPlumbUtil.removeWithFunction(connectionsByElementId[originalId], function (info) { return info[0].id === connection.id; }); var tIdx = jsPlumbUtil.findWithFunction(connectionsByElementId[connection.targetId], function (i) { return i[0].id === connection.id; }); if (tIdx > -1) { connectionsByElementId[connection.targetId][tIdx][0] = connection; connectionsByElementId[connection.targetId][tIdx][1] = connection.endpoints[0]; connectionsByElementId[connection.targetId][tIdx][2] = connection.endpoints[0].anchor.constructor == jsPlumb.DynamicAnchor; }
                jsPlumbUtil.addToList(connectionsByElementId, newId, [connection, connection.endpoints[1], connection.endpoints[1].anchor.constructor == jsPlumb.DynamicAnchor]);
            } 
        }; this.rehomeEndpoint = function (ep, currentId, element) {
            var eps = _amEndpoints[currentId] || [], elementId = jsPlumbInstance.getId(element); if (elementId !== currentId) { var idx = jsPlumbUtil.indexOf(eps, ep); if (idx > -1) { var _ep = eps.splice(idx, 1)[0]; self.add(_ep, elementId); } }
            for (var i = 0; i < ep.connections.length; i++) {
                if (ep.connections[i].sourceId == currentId) { ep.connections[i].sourceId = ep.elementId; ep.connections[i].source = ep.element; self.sourceChanged(currentId, ep.elementId, ep.connections[i]); }
                else if (ep.connections[i].targetId == currentId) { ep.connections[i].targetId = ep.elementId; ep.connections[i].target = ep.element; self.updateOtherEndpoint(ep.connections[i].sourceId, currentId, ep.elementId, ep.connections[i]); } 
            } 
        }; this.redraw = function (elementId, ui, timestamp, offsetToUI, clearEdits, doNotRecalcEndpoint) {
            if (!jsPlumbInstance.isSuspendDrawing()) {
                var ep = _amEndpoints[elementId] || [], endpointConnections = connectionsByElementId[elementId] || [], connectionsToPaint = [], endpointsToPaint = [], anchorsToUpdate = []; timestamp = timestamp || jsPlumbInstance.timestamp(); offsetToUI = offsetToUI || { left: 0, top: 0 }; if (ui) { ui = { left: ui.left + offsetToUI.left, top: ui.top + offsetToUI.top }; }
                var myOffset = jsPlumbInstance.updateOffset({ elId: elementId, offset: ui, recalc: false, timestamp: timestamp }), orientationCache = {}; for (var i = 0; i < endpointConnections.length; i++) {
                    var conn = endpointConnections[i][0], sourceId = conn.sourceId, targetId = conn.targetId, sourceContinuous = conn.endpoints[0].anchor.isContinuous, targetContinuous = conn.endpoints[1].anchor.isContinuous; if (sourceContinuous || targetContinuous) {
                        var oKey = sourceId + "_" + targetId, oKey2 = targetId + "_" + sourceId, o = orientationCache[oKey], oIdx = conn.sourceId == elementId ? 1 : 0; if (sourceContinuous && !anchorLists[sourceId]) anchorLists[sourceId] = { top: [], right: [], bottom: [], left: [] }; if (targetContinuous && !anchorLists[targetId]) anchorLists[targetId] = { top: [], right: [], bottom: [], left: [] }; if (elementId != targetId) jsPlumbInstance.updateOffset({ elId: targetId, timestamp: timestamp }); if (elementId != sourceId) jsPlumbInstance.updateOffset({ elId: sourceId, timestamp: timestamp }); var td = jsPlumbInstance.getCachedData(targetId), sd = jsPlumbInstance.getCachedData(sourceId); if (targetId == sourceId && (sourceContinuous || targetContinuous)) { _updateAnchorList(anchorLists[sourceId], -Math.PI / 2, 0, conn, false, targetId, 0, false, "top", sourceId, connectionsToPaint, endpointsToPaint); }
                        else {
                            if (!o) { o = calculateOrientation(sourceId, targetId, sd.o, td.o, conn.endpoints[0].anchor, conn.endpoints[1].anchor); orientationCache[oKey] = o; }
                            if (sourceContinuous) _updateAnchorList(anchorLists[sourceId], o.theta, 0, conn, false, targetId, 0, false, o.a[0], sourceId, connectionsToPaint, endpointsToPaint); if (targetContinuous) _updateAnchorList(anchorLists[targetId], o.theta2, -1, conn, true, sourceId, 1, true, o.a[1], targetId, connectionsToPaint, endpointsToPaint);
                        }
                        if (sourceContinuous) jsPlumbUtil.addWithFunction(anchorsToUpdate, sourceId, function (a) { return a === sourceId; }); if (targetContinuous) jsPlumbUtil.addWithFunction(anchorsToUpdate, targetId, function (a) { return a === targetId; }); jsPlumbUtil.addWithFunction(connectionsToPaint, conn, function (c) { return c.id == conn.id; }); if ((sourceContinuous && oIdx === 0) || (targetContinuous && oIdx === 1))
                            jsPlumbUtil.addWithFunction(endpointsToPaint, conn.endpoints[oIdx], function (e) { return e.id == conn.endpoints[oIdx].id; });
                    } 
                }
                for (i = 0; i < ep.length; i++) { if (ep[i].connections.length === 0 && ep[i].anchor.isContinuous) { if (!anchorLists[elementId]) anchorLists[elementId] = { top: [], right: [], bottom: [], left: [] }; _updateAnchorList(anchorLists[elementId], -Math.PI / 2, 0, { endpoints: [ep[i], ep[i]], paint: function () { } }, false, elementId, 0, false, "top", elementId, connectionsToPaint, endpointsToPaint); jsPlumbUtil.addWithFunction(anchorsToUpdate, elementId, function (a) { return a === elementId; }); } }
                for (i = 0; i < anchorsToUpdate.length; i++) { placeAnchors(anchorsToUpdate[i], anchorLists[anchorsToUpdate[i]]); }
                for (i = 0; i < ep.length; i++) { ep[i].paint({ timestamp: timestamp, offset: myOffset, dimensions: myOffset.s, recalc: doNotRecalcEndpoint !== true }); }
                for (i = 0; i < endpointsToPaint.length; i++) { var cd = jsPlumbInstance.getCachedData(endpointsToPaint[i].elementId); endpointsToPaint[i].paint({ timestamp: timestamp, offset: cd, dimensions: cd.s }); }
                for (i = 0; i < endpointConnections.length; i++) {
                    var otherEndpoint = endpointConnections[i][1]; if (otherEndpoint.anchor.constructor == jsPlumb.DynamicAnchor) {
                        otherEndpoint.paint({ elementWithPrecedence: elementId, timestamp: timestamp }); jsPlumbUtil.addWithFunction(connectionsToPaint, endpointConnections[i][0], function (c) { return c.id == endpointConnections[i][0].id; }); for (var k = 0; k < otherEndpoint.connections.length; k++) {
                            if (otherEndpoint.connections[k] !== endpointConnections[i][0])
                                jsPlumbUtil.addWithFunction(connectionsToPaint, otherEndpoint.connections[k], function (c) { return c.id == otherEndpoint.connections[k].id; });
                        } 
                    } else if (otherEndpoint.anchor.constructor == jsPlumb.Anchor) { jsPlumbUtil.addWithFunction(connectionsToPaint, endpointConnections[i][0], function (c) { return c.id == endpointConnections[i][0].id; }); } 
                }
                var fc = floatingConnections[elementId]; if (fc)
                    fc.paint({ timestamp: timestamp, recalc: false, elId: elementId }); for (i = 0; i < connectionsToPaint.length; i++) { var ts = timestamp; connectionsToPaint[i].paint({ elId: elementId, timestamp: ts, recalc: false, clearEdits: clearEdits }); } 
            } 
        }; var ContinuousAnchor = function (anchorParams) {
            jsPlumbUtil.EventGenerator.apply(this); this.type = "Continuous"; this.isDynamic = true; this.isContinuous = true; var faces = anchorParams.faces || ["top", "right", "bottom", "left"], clockwise = !(anchorParams.clockwise === false), availableFaces = {}, opposites = { "top": "bottom", "right": "left", "left": "right", "bottom": "top" }, clockwiseOptions = { "top": "right", "right": "bottom", "left": "top", "bottom": "left" }, antiClockwiseOptions = { "top": "left", "right": "top", "left": "bottom", "bottom": "right" }, secondBest = clockwise ? clockwiseOptions : antiClockwiseOptions, lastChoice = clockwise ? antiClockwiseOptions : clockwiseOptions, cssClass = anchorParams.cssClass || ""; for (var i = 0; i < faces.length; i++) { availableFaces[faces[i]] = true; }
            this.verifyEdge = function (edge) { if (availableFaces[edge]) return edge; else if (availableFaces[opposites[edge]]) return opposites[edge]; else if (availableFaces[secondBest[edge]]) return secondBest[edge]; else if (availableFaces[lastChoice[edge]]) return lastChoice[edge]; return edge; }; this.compute = function (params) { return userDefinedContinuousAnchorLocations[params.element.id] || continuousAnchorLocations[params.element.id] || [0, 0]; }; this.getCurrentLocation = function (params) { return userDefinedContinuousAnchorLocations[params.element.id] || continuousAnchorLocations[params.element.id] || [0, 0]; }; this.getOrientation = function (endpoint) { return continuousAnchorOrientations[endpoint.id] || [0, 0]; }; this.clearUserDefinedLocation = function () { delete userDefinedContinuousAnchorLocations[anchorParams.elementId]; }; this.setUserDefinedLocation = function (loc) { userDefinedContinuousAnchorLocations[anchorParams.elementId] = loc; }; this.getCssClass = function () { return cssClass; }; this.setCssClass = function (c) { cssClass = c; };
        }; jsPlumbInstance.continuousAnchorFactory = { get: function (params) {
            var existing = continuousAnchors[params.elementId]; if (!existing) { existing = new ContinuousAnchor(params); continuousAnchors[params.elementId] = existing; }
            return existing;
        }, clear: function (elementId) { delete continuousAnchors[elementId]; } 
        };
    }; jsPlumb.Anchor = function (params) {
        this.x = params.x || 0; this.y = params.y || 0; this.elementId = params.elementId; this.cssClass = params.cssClass || ""; this.userDefinedLocation = null; this.orientation = params.orientation || [0, 0]; jsPlumbUtil.EventGenerator.apply(this); var jsPlumbInstance = params.jsPlumbInstance; this.lastReturnValue = null; this.offsets = params.offsets || [0, 0]; this.timestamp = null; this.compute = function (params) {
            var xy = params.xy, wh = params.wh, element = params.element, timestamp = params.timestamp; if (params.clearUserDefinedLocation)
                this.userDefinedLocation = null; if (timestamp && timestamp === self.timestamp)
                return this.lastReturnValue; if (this.userDefinedLocation != null) { this.lastReturnValue = this.userDefinedLocation; }
            else { this.lastReturnValue = [xy[0] + (this.x * wh[0]) + this.offsets[0], xy[1] + (this.y * wh[1]) + this.offsets[1]]; }
            this.timestamp = timestamp; return this.lastReturnValue;
        }; this.getCurrentLocation = function (params) { return (this.lastReturnValue == null || (params.timestamp != null && this.timestamp != params.timestamp)) ? this.compute(params) : this.lastReturnValue; };
    }; jsPlumbUtil.extend(jsPlumb.Anchor, jsPlumbUtil.EventGenerator, { equals: function (anchor) { if (!anchor) return false; var ao = anchor.getOrientation(), o = this.getOrientation(); return this.x == anchor.x && this.y == anchor.y && this.offsets[0] == anchor.offsets[0] && this.offsets[1] == anchor.offsets[1] && o[0] == ao[0] && o[1] == ao[1]; }, getUserDefinedLocation: function () { return this.userDefinedLocation; }, setUserDefinedLocation: function (l) { this.userDefinedLocation = l; }, clearUserDefinedLocation: function () { this.userDefinedLocation = null; }, getOrientation: function (_endpoint) { return this.orientation; }, getCssClass: function () { return this.cssClass; } }); jsPlumb.FloatingAnchor = function (params) { jsPlumb.Anchor.apply(this, arguments); var ref = params.reference, jsPlumbInstance = params.jsPlumbInstance, refCanvas = params.referenceCanvas, size = jsPlumb.getSize(refCanvas), xDir = 0, yDir = 0, orientation = null, _lastResult = null; this.orientation = null; this.x = 0; this.y = 0; this.isFloating = true; this.compute = function (params) { var xy = params.xy, element = params.element, result = [xy[0] + (size[0] / 2), xy[1] + (size[1] / 2)]; _lastResult = result; return result; }; this.getOrientation = function (_endpoint) { if (orientation) return orientation; else { var o = ref.getOrientation(_endpoint); return [Math.abs(o[0]) * xDir * -1, Math.abs(o[1]) * yDir * -1]; } }; this.over = function (anchor, endpoint) { orientation = anchor.getOrientation(endpoint); }; this.out = function () { orientation = null; }; this.getCurrentLocation = function (params) { return _lastResult == null ? this.compute(params) : _lastResult; }; }; jsPlumbUtil.extend(jsPlumb.FloatingAnchor, jsPlumb.Anchor); var _convertAnchor = function (anchor, jsPlumbInstance, elementId) { return anchor.constructor == jsPlumb.Anchor ? anchor : jsPlumbInstance.makeAnchor(anchor, elementId, jsPlumbInstance); }; jsPlumb.DynamicAnchor = function (params) {
        jsPlumb.Anchor.apply(this, arguments); this.isSelective = true; this.isDynamic = true; this.anchors = []; this.elementId = params.elementId; this.jsPlumbInstance = params.jsPlumbInstance; for (var i = 0; i < params.anchors.length; i++)
            this.anchors[i] = _convertAnchor(params.anchors[i], this.jsPlumbInstance, this.elementId); this.addAnchor = function (anchor) { this.anchors.push(_convertAnchor(anchor, this.jsPlumbInstance, this.elementId)); }; this.getAnchors = function () { return this.anchors; }; this.locked = false; var _curAnchor = this.anchors.length > 0 ? this.anchors[0] : null, _curIndex = this.anchors.length > 0 ? 0 : -1, _lastAnchor = _curAnchor, self = this, _distance = function (anchor, cx, cy, xy, wh) {
                var ax = xy[0] + (anchor.x * wh[0]), ay = xy[1] + (anchor.y * wh[1]), acx = xy[0] + (wh[0] / 2), acy = xy[1] + (wh[1] / 2); return (Math.sqrt(Math.pow(cx - ax, 2) + Math.pow(cy - ay, 2)) +
Math.sqrt(Math.pow(acx - ax, 2) + Math.pow(acy - ay, 2)));
            }, _anchorSelector = params.selector || function (xy, wh, txy, twh, anchors) {
                var cx = txy[0] + (twh[0] / 2), cy = txy[1] + (twh[1] / 2); var minIdx = -1, minDist = Infinity; for (var i = 0; i < anchors.length; i++) { var d = _distance(anchors[i], cx, cy, xy, wh); if (d < minDist) { minIdx = i + 0; minDist = d; } }
                return anchors[minIdx];
            }; this.compute = function (params) {
                var xy = params.xy, wh = params.wh, timestamp = params.timestamp, txy = params.txy, twh = params.twh; if (params.clearUserDefinedLocation)
                    userDefinedLocation = null; this.timestamp = timestamp; var udl = self.getUserDefinedLocation(); if (udl != null) { return udl; }
                if (this.locked || txy == null || twh == null)
                    return _curAnchor.compute(params); else
                    params.timestamp = null; _curAnchor = _anchorSelector(xy, wh, txy, twh, this.anchors); this.x = _curAnchor.x; this.y = _curAnchor.y; if (_curAnchor != _lastAnchor)
                    this.fire("anchorChanged", _curAnchor); _lastAnchor = _curAnchor; return _curAnchor.compute(params);
            }; this.getCurrentLocation = function (params) { return this.getUserDefinedLocation() || (_curAnchor != null ? _curAnchor.getCurrentLocation(params) : null); }; this.getOrientation = function (_endpoint) { return _curAnchor != null ? _curAnchor.getOrientation(_endpoint) : [0, 0]; }; this.over = function (anchor, endpoint) { if (_curAnchor != null) _curAnchor.over(anchor, endpoint); }; this.out = function () { if (_curAnchor != null) _curAnchor.out(); }; this.getCssClass = function () { return (_curAnchor && _curAnchor.getCssClass()) || ""; };
    }; jsPlumbUtil.extend(jsPlumb.DynamicAnchor, jsPlumb.Anchor); var _curryAnchor = function (x, y, ox, oy, type, fnInit) { jsPlumb.Anchors[type] = function (params) { var a = params.jsPlumbInstance.makeAnchor([x, y, ox, oy, 0, 0], params.elementId, params.jsPlumbInstance); a.type = type; if (fnInit) fnInit(a, params); return a; }; }; _curryAnchor(0.5, 0, 0, -1, "TopCenter"); _curryAnchor(0.5, 1, 0, 1, "BottomCenter"); _curryAnchor(0, 0.5, -1, 0, "LeftMiddle"); _curryAnchor(1, 0.5, 1, 0, "RightMiddle"); _curryAnchor(0.5, 0, 0, -1, "Top"); _curryAnchor(0.5, 1, 0, 1, "Bottom"); _curryAnchor(0, 0.5, -1, 0, "Left"); _curryAnchor(1, 0.5, 1, 0, "Right"); _curryAnchor(0.5, 0.5, 0, 0, "Center"); _curryAnchor(1, 0, 0, -1, "TopRight"); _curryAnchor(1, 1, 0, 1, "BottomRight"); _curryAnchor(0, 0, 0, -1, "TopLeft"); _curryAnchor(0, 1, 0, 1, "BottomLeft"); jsPlumb.Defaults.DynamicAnchors = function (params) { return params.jsPlumbInstance.makeAnchors(["TopCenter", "RightMiddle", "BottomCenter", "LeftMiddle"], params.elementId, params.jsPlumbInstance); }; jsPlumb.Anchors.AutoDefault = function (params) { var a = params.jsPlumbInstance.makeDynamicAnchor(jsPlumb.Defaults.DynamicAnchors(params)); a.type = "AutoDefault"; return a; }; var _curryContinuousAnchor = function (type, faces) { jsPlumb.Anchors[type] = function (params) { var a = params.jsPlumbInstance.makeAnchor(["Continuous", { faces: faces}], params.elementId, params.jsPlumbInstance); a.type = type; return a; }; }; jsPlumb.Anchors.Continuous = function (params) { return params.jsPlumbInstance.continuousAnchorFactory.get(params); }; _curryContinuousAnchor("ContinuousLeft", ["left"]); _curryContinuousAnchor("ContinuousTop", ["top"]); _curryContinuousAnchor("ContinuousBottom", ["bottom"]); _curryContinuousAnchor("ContinuousRight", ["right"]); _curryAnchor(0, 0, 0, 0, "Assign", function (anchor, params) { var pf = params.position || "Fixed"; anchor.positionFinder = pf.constructor == String ? params.jsPlumbInstance.AnchorPositionFinders[pf] : pf; anchor.constructorParams = params; }); jsPlumbInstance.prototype.AnchorPositionFinders = { "Fixed": function (dp, ep, es, params) { return [(dp.left - ep.left) / es[0], (dp.top - ep.top) / es[1]]; }, "Grid": function (dp, ep, es, params) { var dx = dp.left - ep.left, dy = dp.top - ep.top, gx = es[0] / (params.grid[0]), gy = es[1] / (params.grid[1]), mx = Math.floor(dx / gx), my = Math.floor(dy / gy); return [((mx * gx) + (gx / 2)) / es[0], ((my * gy) + (gy / 2)) / es[1]]; } }; jsPlumb.Anchors.Perimeter = function (params) {
        params = params || {}; var anchorCount = params.anchorCount || 60, shape = params.shape; if (!shape) throw new Error("no shape supplied to Perimeter Anchor type"); var _circle = function () {
            var r = 0.5, step = Math.PI * 2 / anchorCount, current = 0, a = []; for (var i = 0; i < anchorCount; i++) { var x = r + (r * Math.sin(current)), y = r + (r * Math.cos(current)); a.push([x, y, 0, 0]); current += step; }
            return a;
        }, _path = function (segments) {
            var anchorsPerFace = anchorCount / segments.length, a = [], _computeFace = function (x1, y1, x2, y2, fractionalLength) { anchorsPerFace = anchorCount * fractionalLength; var dx = (x2 - x1) / anchorsPerFace, dy = (y2 - y1) / anchorsPerFace; for (var i = 0; i < anchorsPerFace; i++) { a.push([x1 + (dx * i), y1 + (dy * i), 0, 0]); } }; for (var i = 0; i < segments.length; i++)
                _computeFace.apply(null, segments[i]); return a;
        }, _shape = function (faces) {
            var s = []; for (var i = 0; i < faces.length; i++) { s.push([faces[i][0], faces[i][1], faces[i][2], faces[i][3], 1 / faces.length]); }
            return _path(s);
        }, _rectangle = function () { return _shape([[0, 0, 1, 0], [1, 0, 1, 1], [1, 1, 0, 1], [0, 1, 0, 0]]); }; var _shapes = { "Circle": _circle, "Ellipse": _circle, "Diamond": function () { return _shape([[0.5, 0, 1, 0.5], [1, 0.5, 0.5, 1], [0.5, 1, 0, 0.5], [0, 0.5, 0.5, 0]]); }, "Rectangle": _rectangle, "Square": _rectangle, "Triangle": function () { return _shape([[0.5, 0, 1, 1], [1, 1, 0, 1], [0, 1, 0.5, 0]]); }, "Path": function (params) {
            var points = params.points, p = [], tl = 0; for (var i = 0; i < points.length - 1; i++) { var l = Math.sqrt(Math.pow(points[i][2] - points[i][0]) + Math.pow(points[i][3] - points[i][1])); tl += l; p.push([points[i][0], points[i][1], points[i + 1][0], points[i + 1][1], l]); }
            for (var j = 0; j < p.length; j++) { p[j][4] = p[j][4] / tl; }
            return _path(p);
        } 
        }, _rotate = function (points, amountInDegrees) {
            var o = [], theta = amountInDegrees / 180 * Math.PI; for (var i = 0; i < points.length; i++) { var _x = points[i][0] - 0.5, _y = points[i][1] - 0.5; o.push([0.5 + ((_x * Math.cos(theta)) - (_y * Math.sin(theta))), 0.5 + ((_x * Math.sin(theta)) + (_y * Math.cos(theta))), points[i][2], points[i][3]]); }
            return o;
        }; if (!_shapes[shape]) throw new Error("Shape [" + shape + "] is unknown by Perimeter Anchor type"); var da = _shapes[shape](params); if (params.rotation) da = _rotate(da, params.rotation); var a = params.jsPlumbInstance.makeDynamicAnchor(da); a.type = "Perimeter"; return a;
    };
})(); ; (function () {
    "use strict"; jsPlumb.DOMElementComponent = jsPlumbUtil.extend(jsPlumb.jsPlumbUIComponent, function (params) { this.mousemove = this.dblclick = this.click = this.mousedown = this.mouseup = function (e) { }; }); jsPlumb.Segments = { AbstractSegment: function (params) { this.params = params; this.findClosestPointOnPath = function (x, y) { return { d: Infinity, x: null, y: null, l: null }; }; this.getBounds = function () { return { minX: Math.min(params.x1, params.x2), minY: Math.min(params.y1, params.y2), maxX: Math.max(params.x1, params.x2), maxY: Math.max(params.y1, params.y2) }; }; }, Straight: function (params) {
        var _super = jsPlumb.Segments.AbstractSegment.apply(this, arguments), length, m, m2, x1, x2, y1, y2, _recalc = function () { length = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)); m = Biltong.gradient({ x: x1, y: y1 }, { x: x2, y: y2 }); m2 = -1 / m; }; this.type = "Straight"; this.getLength = function () { return length; }; this.getGradient = function () { return m; }; this.getCoordinates = function () { return { x1: x1, y1: y1, x2: x2, y2: y2 }; }; this.setCoordinates = function (coords) { x1 = coords.x1; y1 = coords.y1; x2 = coords.x2; y2 = coords.y2; _recalc(); }; this.setCoordinates({ x1: params.x1, y1: params.y1, x2: params.x2, y2: params.y2 }); this.getBounds = function () { return { minX: Math.min(x1, x2), minY: Math.min(y1, y2), maxX: Math.max(x1, x2), maxY: Math.max(y1, y2) }; }; this.pointOnPath = function (location, absolute) {
            if (location === 0 && !absolute)
                return { x: x1, y: y1 }; else if (location == 1 && !absolute)
                return { x: x2, y: y2 }; else { var l = absolute ? location > 0 ? location : length + location : location * length; return Biltong.pointOnLine({ x: x1, y: y1 }, { x: x2, y: y2 }, l); } 
        }; this.gradientAtPoint = function (_) { return m; }; this.pointAlongPathFrom = function (location, distance, absolute) { var p = this.pointOnPath(location, absolute), farAwayPoint = distance <= 0 ? { x: x1, y: y1} : { x: x2, y: y2 }; if (distance <= 0 && Math.abs(distance) > 1) distance *= -1; return Biltong.pointOnLine(p, farAwayPoint, distance); }; var within = function (a, b, c) { return c >= Math.min(a, b) && c <= Math.max(a, b); }; var closest = function (a, b, c) { return Math.abs(c - a) < Math.abs(c - b) ? a : b; }; this.findClosestPointOnPath = function (x, y) {
            var out = { d: Infinity, x: null, y: null, l: null, x1: x1, x2: x2, y1: y1, y2: y2 }; if (m === 0) { out.y = y1; out.x = within(x1, x2, x) ? x : closest(x1, x2, x); }
            else if (m == Infinity || m == -Infinity) { out.x = x1; out.y = within(y1, y2, y) ? y : closest(y1, y2, y); }
            else { var b = y1 - (m * x1), b2 = y - (m2 * x), _x1 = (b2 - b) / (m - m2), _y1 = (m * _x1) + b; out.x = within(x1, x2, _x1) ? _x1 : closest(x1, x2, _x1); out.y = within(y1, y2, _y1) ? _y1 : closest(y1, y2, _y1); }
            var fractionInSegment = Biltong.lineLength([out.x, out.y], [x1, y1]); out.d = Biltong.lineLength([x, y], [out.x, out.y]); out.l = fractionInSegment / length; return out;
        };
    }, Arc: function (params) {
        var _super = jsPlumb.Segments.AbstractSegment.apply(this, arguments), _calcAngle = function (_x, _y) { return Biltong.theta([params.cx, params.cy], [_x, _y]); }, _calcAngleForLocation = function (segment, location) {
            if (segment.anticlockwise) { var sa = segment.startAngle < segment.endAngle ? segment.startAngle + TWO_PI : segment.startAngle, s = Math.abs(sa - segment.endAngle); return sa - (s * location); }
            else { var ea = segment.endAngle < segment.startAngle ? segment.endAngle + TWO_PI : segment.endAngle, ss = Math.abs(ea - segment.startAngle); return segment.startAngle + (ss * location); } 
        }, TWO_PI = 2 * Math.PI; this.radius = params.r; this.anticlockwise = params.ac; this.type = "Arc"; if (params.startAngle && params.endAngle) { this.startAngle = params.startAngle; this.endAngle = params.endAngle; this.x1 = params.cx + (this.radius * Math.cos(params.startAngle)); this.y1 = params.cy + (this.radius * Math.sin(params.startAngle)); this.x2 = params.cx + (this.radius * Math.cos(params.endAngle)); this.y2 = params.cy + (this.radius * Math.sin(params.endAngle)); }
        else { this.startAngle = _calcAngle(params.x1, params.y1); this.endAngle = _calcAngle(params.x2, params.y2); this.x1 = params.x1; this.y1 = params.y1; this.x2 = params.x2; this.y2 = params.y2; }
        if (this.endAngle < 0) this.endAngle += TWO_PI; if (this.startAngle < 0) this.startAngle += TWO_PI; this.segment = Biltong.quadrant([this.x1, this.y1], [this.x2, this.y2]); var ea = this.endAngle < this.startAngle ? this.endAngle + TWO_PI : this.endAngle; this.sweep = Math.abs(ea - this.startAngle); if (this.anticlockwise) this.sweep = TWO_PI - this.sweep; var circumference = 2 * Math.PI * this.radius, frac = this.sweep / TWO_PI, length = circumference * frac; this.getLength = function () { return length; }; this.getBounds = function () { return { minX: params.cx - params.r, maxX: params.cx + params.r, minY: params.cy - params.r, maxY: params.cy + params.r }; }; var VERY_SMALL_VALUE = 0.0000000001, gentleRound = function (n) {
            var f = Math.floor(n), r = Math.ceil(n); if (n - f < VERY_SMALL_VALUE)
                return f; else if (r - n < VERY_SMALL_VALUE)
                return r; return n;
        }; this.pointOnPath = function (location, absolute) {
            if (location === 0) { return { x: this.x1, y: this.y1, theta: this.startAngle }; }
            else if (location == 1) { return { x: this.x2, y: this.y2, theta: this.endAngle }; }
            if (absolute) { location = location / length; }
            var angle = _calcAngleForLocation(this, location), _x = params.cx + (params.r * Math.cos(angle)), _y = params.cy + (params.r * Math.sin(angle)); return { x: gentleRound(_x), y: gentleRound(_y), theta: angle };
        }; this.gradientAtPoint = function (location, absolute) { var p = this.pointOnPath(location, absolute); var m = Biltong.normal([params.cx, params.cy], [p.x, p.y]); if (!this.anticlockwise && (m == Infinity || m == -Infinity)) m *= -1; return m; }; this.pointAlongPathFrom = function (location, distance, absolute) { var p = this.pointOnPath(location, absolute), arcSpan = distance / circumference * 2 * Math.PI, dir = this.anticlockwise ? -1 : 1, startAngle = p.theta + (dir * arcSpan), startX = params.cx + (this.radius * Math.cos(startAngle)), startY = params.cy + (this.radius * Math.sin(startAngle)); return { x: startX, y: startY }; };
    }, Bezier: function (params) {
        var _super = jsPlumb.Segments.AbstractSegment.apply(this, arguments), curve = [{ x: params.x1, y: params.y1 }, { x: params.cp1x, y: params.cp1y }, { x: params.cp2x, y: params.cp2y }, { x: params.x2, y: params.y2}], bounds = { minX: Math.min(params.x1, params.x2, params.cp1x, params.cp2x), minY: Math.min(params.y1, params.y2, params.cp1y, params.cp2y), maxX: Math.max(params.x1, params.x2, params.cp1x, params.cp2x), maxY: Math.max(params.y1, params.y2, params.cp1y, params.cp2y) }; this.type = "Bezier"; var _translateLocation = function (_curve, location, absolute) {
            if (absolute)
                location = jsBezier.locationAlongCurveFrom(_curve, location > 0 ? 0 : 1, location); return location;
        }; this.pointOnPath = function (location, absolute) { location = _translateLocation(curve, location, absolute); return jsBezier.pointOnCurve(curve, location); }; this.gradientAtPoint = function (location, absolute) { location = _translateLocation(curve, location, absolute); return jsBezier.gradientAtPoint(curve, location); }; this.pointAlongPathFrom = function (location, distance, absolute) { location = _translateLocation(curve, location, absolute); return jsBezier.pointAlongCurveFrom(curve, location, distance); }; this.getLength = function () { return jsBezier.getLength(curve); }; this.getBounds = function () { return bounds; };
    } 
    }; var AbstractComponent = function () { this.resetBounds = function () { this.bounds = { minX: Infinity, minY: Infinity, maxX: -Infinity, maxY: -Infinity }; }; this.resetBounds(); }; jsPlumb.Connectors.AbstractConnector = function (params) {
        AbstractComponent.apply(this, arguments); var segments = [], editing = false, totalLength = 0, segmentProportions = [], segmentProportionalLengths = [], stub = params.stub || 0, sourceStub = jsPlumbUtil.isArray(stub) ? stub[0] : stub, targetStub = jsPlumbUtil.isArray(stub) ? stub[1] : stub, gap = params.gap || 0, sourceGap = jsPlumbUtil.isArray(gap) ? gap[0] : gap, targetGap = jsPlumbUtil.isArray(gap) ? gap[1] : gap, userProvidedSegments = null, edited = false, paintInfo = null; this.isEditable = function () { return false; }; this.setEdited = function (ed) { edited = ed; }; this.getPath = function () { }; this.setPath = function (path) { }; this.findSegmentForPoint = function (x, y) {
            var out = { d: Infinity, s: null, x: null, y: null, l: null }; for (var i = 0; i < segments.length; i++) { var _s = segments[i].findClosestPointOnPath(x, y); if (_s.d < out.d) { out.d = _s.d; out.l = _s.l; out.x = _s.x; out.y = _s.y; out.s = segments[i]; out.x1 = _s.x1; out.x2 = _s.x2; out.y1 = _s.y1; out.y2 = _s.y2; out.index = i; } }
            return out;
        }; var _updateSegmentProportions = function () { var curLoc = 0; for (var i = 0; i < segments.length; i++) { var sl = segments[i].getLength(); segmentProportionalLengths[i] = sl / totalLength; segmentProportions[i] = [curLoc, (curLoc += (sl / totalLength))]; } }, _findSegmentForLocation = function (location, absolute) {
            if (absolute) { location = location > 0 ? location / totalLength : (totalLength + location) / totalLength; }
            var idx = segmentProportions.length - 1, inSegmentProportion = 1; for (var i = 0; i < segmentProportions.length; i++) { if (segmentProportions[i][1] >= location) { idx = i; inSegmentProportion = location == 1 ? 1 : location === 0 ? 0 : (location - segmentProportions[i][0]) / segmentProportionalLengths[i]; break; } }
            return { segment: segments[idx], proportion: inSegmentProportion, index: idx };
        }, _addSegment = function (conn, type, params) { if (params.x1 == params.x2 && params.y1 == params.y2) return; var s = new jsPlumb.Segments[type](params); segments.push(s); totalLength += s.getLength(); conn.updateBounds(s); }, _clearSegments = function () { totalLength = segments.length = segmentProportions.length = segmentProportionalLengths.length = 0; }; this.setSegments = function (_segs) { userProvidedSegments = []; totalLength = 0; for (var i = 0; i < _segs.length; i++) { userProvidedSegments.push(_segs[i]); totalLength += _segs[i].getLength(); } }; var _prepareCompute = function (params) {
            this.lineWidth = params.lineWidth; var segment = Biltong.quadrant(params.sourcePos, params.targetPos), swapX = params.targetPos[0] < params.sourcePos[0], swapY = params.targetPos[1] < params.sourcePos[1], lw = params.lineWidth || 1, so = params.sourceEndpoint.anchor.getOrientation(params.sourceEndpoint), to = params.targetEndpoint.anchor.getOrientation(params.targetEndpoint), x = swapX ? params.targetPos[0] : params.sourcePos[0], y = swapY ? params.targetPos[1] : params.sourcePos[1], w = Math.abs(params.targetPos[0] - params.sourcePos[0]), h = Math.abs(params.targetPos[1] - params.sourcePos[1]); if (so[0] === 0 && so[1] === 0 || to[0] === 0 && to[1] === 0) { var index = w > h ? 0 : 1, oIndex = [1, 0][index]; so = []; to = []; so[index] = params.sourcePos[index] > params.targetPos[index] ? -1 : 1; to[index] = params.sourcePos[index] > params.targetPos[index] ? 1 : -1; so[oIndex] = 0; to[oIndex] = 0; }
            var sx = swapX ? w + (sourceGap * so[0]) : sourceGap * so[0], sy = swapY ? h + (sourceGap * so[1]) : sourceGap * so[1], tx = swapX ? targetGap * to[0] : w + (targetGap * to[0]), ty = swapY ? targetGap * to[1] : h + (targetGap * to[1]), oProduct = ((so[0] * to[0]) + (so[1] * to[1])); var result = { sx: sx, sy: sy, tx: tx, ty: ty, lw: lw, xSpan: Math.abs(tx - sx), ySpan: Math.abs(ty - sy), mx: (sx + tx) / 2, my: (sy + ty) / 2, so: so, to: to, x: x, y: y, w: w, h: h, segment: segment, startStubX: sx + (so[0] * sourceStub), startStubY: sy + (so[1] * sourceStub), endStubX: tx + (to[0] * targetStub), endStubY: ty + (to[1] * targetStub), isXGreaterThanStubTimes2: Math.abs(sx - tx) > (sourceStub + targetStub), isYGreaterThanStubTimes2: Math.abs(sy - ty) > (sourceStub + targetStub), opposite: oProduct == -1, perpendicular: oProduct === 0, orthogonal: oProduct == 1, sourceAxis: so[0] === 0 ? "y" : "x", points: [x, y, w, h, sx, sy, tx, ty] }; result.anchorOrientation = result.opposite ? "opposite" : result.orthogonal ? "orthogonal" : "perpendicular"; return result;
        }; this.getSegments = function () { return segments; }; this.updateBounds = function (segment) { var segBounds = segment.getBounds(); this.bounds.minX = Math.min(this.bounds.minX, segBounds.minX); this.bounds.maxX = Math.max(this.bounds.maxX, segBounds.maxX); this.bounds.minY = Math.min(this.bounds.minY, segBounds.minY); this.bounds.maxY = Math.max(this.bounds.maxY, segBounds.maxY); }; var dumpSegmentsToConsole = function () { console.log("SEGMENTS:"); for (var i = 0; i < segments.length; i++) { console.log(segments[i].type, segments[i].getLength(), segmentProportions[i]); } }; this.pointOnPath = function (location, absolute) { var seg = _findSegmentForLocation(location, absolute); return seg.segment && seg.segment.pointOnPath(seg.proportion, false) || [0, 0]; }; this.gradientAtPoint = function (location, absolute) { var seg = _findSegmentForLocation(location, absolute); return seg.segment && seg.segment.gradientAtPoint(seg.proportion, false) || 0; }; this.pointAlongPathFrom = function (location, distance, absolute) { var seg = _findSegmentForLocation(location, absolute); return seg.segment && seg.segment.pointAlongPathFrom(seg.proportion, distance, false) || [0, 0]; }; this.compute = function (params) {
            if (!edited)
                paintInfo = _prepareCompute.call(this, params); _clearSegments(); this._compute(paintInfo, params); this.x = paintInfo.points[0]; this.y = paintInfo.points[1]; this.w = paintInfo.points[2]; this.h = paintInfo.points[3]; this.segment = paintInfo.segment; _updateSegmentProportions();
        }; return { addSegment: _addSegment, prepareCompute: _prepareCompute, sourceStub: sourceStub, targetStub: targetStub, maxStub: Math.max(sourceStub, targetStub), sourceGap: sourceGap, targetGap: targetGap, maxGap: Math.max(sourceGap, targetGap) };
    }; jsPlumbUtil.extend(jsPlumb.Connectors.AbstractConnector, AbstractComponent); var Straight = jsPlumb.Connectors.Straight = function () { this.type = "Straight"; var _super = jsPlumb.Connectors.AbstractConnector.apply(this, arguments); this._compute = function (paintInfo, _) { _super.addSegment(this, "Straight", { x1: paintInfo.sx, y1: paintInfo.sy, x2: paintInfo.startStubX, y2: paintInfo.startStubY }); _super.addSegment(this, "Straight", { x1: paintInfo.startStubX, y1: paintInfo.startStubY, x2: paintInfo.endStubX, y2: paintInfo.endStubY }); _super.addSegment(this, "Straight", { x1: paintInfo.endStubX, y1: paintInfo.endStubY, x2: paintInfo.tx, y2: paintInfo.ty }); }; }; jsPlumbUtil.extend(jsPlumb.Connectors.Straight, jsPlumb.Connectors.AbstractConnector); jsPlumb.registerConnectorType(Straight, "Straight"); jsPlumb.Endpoints.AbstractEndpoint = function (params) { AbstractComponent.apply(this, arguments); var compute = this.compute = function (anchorPoint, orientation, endpointStyle, connectorPaintStyle) { var out = this._compute.apply(this, arguments); this.x = out[0]; this.y = out[1]; this.w = out[2]; this.h = out[3]; this.bounds.minX = this.x; this.bounds.minY = this.y; this.bounds.maxX = this.x + this.w; this.bounds.maxY = this.y + this.h; return out; }; return { compute: compute, cssClass: params.cssClass }; }; jsPlumbUtil.extend(jsPlumb.Endpoints.AbstractEndpoint, AbstractComponent); jsPlumb.Endpoints.Dot = function (params) {
        this.type = "Dot"; var _super = jsPlumb.Endpoints.AbstractEndpoint.apply(this, arguments); params = params || {}; this.radius = params.radius || 10; this.defaultOffset = 0.5 * this.radius; this.defaultInnerRadius = this.radius / 3; this._compute = function (anchorPoint, orientation, endpointStyle, connectorPaintStyle) {
            this.radius = endpointStyle.radius || this.radius; var x = anchorPoint[0] - this.radius, y = anchorPoint[1] - this.radius, w = this.radius * 2, h = this.radius * 2; if (endpointStyle.strokeStyle) { var lw = endpointStyle.lineWidth || 1; x -= lw; y -= lw; w += (lw * 2); h += (lw * 2); }
            return [x, y, w, h, this.radius];
        };
    }; jsPlumbUtil.extend(jsPlumb.Endpoints.Dot, jsPlumb.Endpoints.AbstractEndpoint); jsPlumb.Endpoints.Rectangle = function (params) { this.type = "Rectangle"; var _super = jsPlumb.Endpoints.AbstractEndpoint.apply(this, arguments); params = params || {}; this.width = params.width || 20; this.height = params.height || 20; this._compute = function (anchorPoint, orientation, endpointStyle, connectorPaintStyle) { var width = endpointStyle.width || this.width, height = endpointStyle.height || this.height, x = anchorPoint[0] - (width / 2), y = anchorPoint[1] - (height / 2); return [x, y, width, height]; }; }; jsPlumbUtil.extend(jsPlumb.Endpoints.Rectangle, jsPlumb.Endpoints.AbstractEndpoint); var DOMElementEndpoint = function (params) { jsPlumb.DOMElementComponent.apply(this, arguments); this._jsPlumb.displayElements = []; }; jsPlumbUtil.extend(DOMElementEndpoint, jsPlumb.DOMElementComponent, { getDisplayElements: function () { return this._jsPlumb.displayElements; }, appendDisplayElement: function (el) { this._jsPlumb.displayElements.push(el); } }); jsPlumb.Endpoints.Image = function (params) {
        this.type = "Image"; DOMElementEndpoint.apply(this, arguments); jsPlumb.Endpoints.AbstractEndpoint.apply(this, arguments); var _onload = params.onload, src = params.src || params.url, clazz = params.cssClass ? " " + params.cssClass : ""; this._jsPlumb.img = new Image(); this._jsPlumb.ready = false; this._jsPlumb.initialized = false; this._jsPlumb.deleted = false; this._jsPlumb.widthToUse = params.width; this._jsPlumb.heightToUse = params.height; this._jsPlumb.endpoint = params.endpoint; this._jsPlumb.img.onload = function () { if (this._jsPlumb != null) { this._jsPlumb.ready = true; this._jsPlumb.widthToUse = this._jsPlumb.widthToUse || this._jsPlumb.img.width; this._jsPlumb.heightToUse = this._jsPlumb.heightToUse || this._jsPlumb.img.height; if (_onload) { _onload(this); } } } .bind(this); this._jsPlumb.endpoint.setImage = function (_img, onload) {
            var s = _img.constructor == String ? _img : _img.src; _onload = onload; this._jsPlumb.img.src = s; if (this.canvas != null)
                this.canvas.setAttribute("src", this._jsPlumb.img.src);
        } .bind(this); this._jsPlumb.endpoint.setImage(src, _onload); this._compute = function (anchorPoint, orientation, endpointStyle, connectorPaintStyle) { this.anchorPoint = anchorPoint; if (this._jsPlumb.ready) return [anchorPoint[0] - this._jsPlumb.widthToUse / 2, anchorPoint[1] - this._jsPlumb.heightToUse / 2, this._jsPlumb.widthToUse, this._jsPlumb.heightToUse]; else return [0, 0, 0, 0]; }; this.canvas = document.createElement("img"); this.canvas.style.margin = 0; this.canvas.style.padding = 0; this.canvas.style.outline = 0; this.canvas.style.position = "absolute"; this.canvas.className = this._jsPlumb.instance.endpointClass + clazz; if (this._jsPlumb.widthToUse) this.canvas.setAttribute("width", this._jsPlumb.widthToUse); if (this._jsPlumb.heightToUse) this.canvas.setAttribute("height", this._jsPlumb.heightToUse); this._jsPlumb.instance.appendElement(this.canvas); this.attachListeners(this.canvas, this); this.actuallyPaint = function (d, style, anchor) {
            if (!this._jsPlumb.deleted) {
                if (!this._jsPlumb.initialized) { this.canvas.setAttribute("src", this._jsPlumb.img.src); this.appendDisplayElement(this.canvas); this._jsPlumb.initialized = true; }
                var x = this.anchorPoint[0] - (this._jsPlumb.widthToUse / 2), y = this.anchorPoint[1] - (this._jsPlumb.heightToUse / 2); jsPlumbUtil.sizeElement(this.canvas, x, y, this._jsPlumb.widthToUse, this._jsPlumb.heightToUse);
            } 
        }; this.paint = function (style, anchor) {
            if (this._jsPlumb != null) {
                if (this._jsPlumb.ready) { this.actuallyPaint(style, anchor); }
                else { window.setTimeout(function () { this.paint(style, anchor); } .bind(this), 200); } 
            } 
        };
    }; jsPlumbUtil.extend(jsPlumb.Endpoints.Image, [DOMElementEndpoint, jsPlumb.Endpoints.AbstractEndpoint], { cleanup: function () { this._jsPlumb.deleted = true; if (this.canvas) this.canvas.parentNode.removeChild(this.canvas); this.canvas = null; } }); jsPlumb.Endpoints.Blank = function (params) { var _super = jsPlumb.Endpoints.AbstractEndpoint.apply(this, arguments); this.type = "Blank"; DOMElementEndpoint.apply(this, arguments); this._compute = function (anchorPoint, orientation, endpointStyle, connectorPaintStyle) { return [anchorPoint[0], anchorPoint[1], 10, 0]; }; this.canvas = document.createElement("div"); this.canvas.style.display = "block"; this.canvas.style.width = "1px"; this.canvas.style.height = "1px"; this.canvas.style.background = "transparent"; this.canvas.style.position = "absolute"; this.canvas.className = this._jsPlumb.endpointClass; jsPlumb.appendElement(this.canvas); this.paint = function (style, anchor) { jsPlumbUtil.sizeElement(this.canvas, this.x, this.y, this.w, this.h); }; }; jsPlumbUtil.extend(jsPlumb.Endpoints.Blank, [jsPlumb.Endpoints.AbstractEndpoint, DOMElementEndpoint], { cleanup: function () { if (this.canvas && this.canvas.parentNode) { this.canvas.parentNode.removeChild(this.canvas); } } }); jsPlumb.Endpoints.Triangle = function (params) { this.type = "Triangle"; var _super = jsPlumb.Endpoints.AbstractEndpoint.apply(this, arguments); params = params || {}; params.width = params.width || 55; params.height = params.height || 55; this.width = params.width; this.height = params.height; this._compute = function (anchorPoint, orientation, endpointStyle, connectorPaintStyle) { var width = endpointStyle.width || self.width, height = endpointStyle.height || self.height, x = anchorPoint[0] - (width / 2), y = anchorPoint[1] - (height / 2); return [x, y, width, height]; }; }; var AbstractOverlay = jsPlumb.Overlays.AbstractOverlay = function (params) { this.visible = true; this.isAppendedAtTopLevel = true; this.component = params.component; this.loc = params.location == null ? 0.5 : params.location; this.endpointLoc = params.endpointLocation == null ? [0.5, 0.5] : params.endpointLocation; }; AbstractOverlay.prototype = { cleanup: function () { this.component = null; this.canvas = null; this.endpointLoc = null; }, setVisible: function (val) { this.visible = val; this.component.repaint(); }, isVisible: function () { return this.visible; }, hide: function () { this.setVisible(false); }, show: function () { this.setVisible(true); }, incrementLocation: function (amount) { this.loc += amount; this.component.repaint(); }, setLocation: function (l) { this.loc = l; this.component.repaint(); }, getLocation: function () { return this.loc; } }; jsPlumb.Overlays.Arrow = function (params) {
        this.type = "Arrow"; AbstractOverlay.apply(this, arguments); this.isAppendedAtTopLevel = false; params = params || {}; var _ju = jsPlumbUtil, _jg = Biltong; this.length = params.length || 20; this.width = params.width || 20; this.id = params.id; var direction = (params.direction || 1) < 0 ? -1 : 1, paintStyle = params.paintStyle || { lineWidth: 1 }, foldback = params.foldback || 0.623; this.computeMaxSize = function () { return self.width * 1.5; }; this.draw = function (component, currentConnectionPaintStyle) {
            var hxy, mid, txy, tail, cxy; if (component.pointAlongPathFrom) {
                if (_ju.isString(this.loc) || this.loc > 1 || this.loc < 0) { var l = parseInt(this.loc, 10), fromLoc = this.loc < 0 ? 1 : 0; hxy = component.pointAlongPathFrom(fromLoc, l, false); mid = component.pointAlongPathFrom(fromLoc, l - (direction * this.length / 2), false); txy = _jg.pointOnLine(hxy, mid, this.length); }
                else if (this.loc == 1) { hxy = component.pointOnPath(this.loc); mid = component.pointAlongPathFrom(this.loc, -(this.length)); txy = _jg.pointOnLine(hxy, mid, this.length); if (direction == -1) { var _ = txy; txy = hxy; hxy = _; } }
                else if (this.loc === 0) { txy = component.pointOnPath(this.loc); mid = component.pointAlongPathFrom(this.loc, this.length); hxy = _jg.pointOnLine(txy, mid, this.length); if (direction == -1) { var __ = txy; txy = hxy; hxy = __; } }
                else { hxy = component.pointAlongPathFrom(this.loc, direction * this.length / 2); mid = component.pointOnPath(this.loc); txy = _jg.pointOnLine(hxy, mid, this.length); }
                tail = _jg.perpendicularLineTo(hxy, txy, this.width); cxy = _jg.pointOnLine(hxy, txy, foldback * this.length); var d = { hxy: hxy, tail: tail, cxy: cxy }, strokeStyle = paintStyle.strokeStyle || currentConnectionPaintStyle.strokeStyle, fillStyle = paintStyle.fillStyle || currentConnectionPaintStyle.strokeStyle, lineWidth = paintStyle.lineWidth || currentConnectionPaintStyle.lineWidth, info = { component: component, d: d, lineWidth: lineWidth, strokeStyle: strokeStyle, fillStyle: fillStyle, minX: Math.min(hxy.x, tail[0].x, tail[1].x), maxX: Math.max(hxy.x, tail[0].x, tail[1].x), minY: Math.min(hxy.y, tail[0].y, tail[1].y), maxY: Math.max(hxy.y, tail[0].y, tail[1].y) }; return info;
            }
            else return { component: component, minX: 0, maxX: 0, minY: 0, maxY: 0 };
        };
    }; jsPlumbUtil.extend(jsPlumb.Overlays.Arrow, AbstractOverlay); jsPlumb.Overlays.PlainArrow = function (params) { params = params || {}; var p = jsPlumb.extend(params, { foldback: 1 }); jsPlumb.Overlays.Arrow.call(this, p); this.type = "PlainArrow"; }; jsPlumbUtil.extend(jsPlumb.Overlays.PlainArrow, jsPlumb.Overlays.Arrow); jsPlumb.Overlays.Diamond = function (params) { params = params || {}; var l = params.length || 40, p = jsPlumb.extend(params, { length: l / 2, foldback: 2 }); jsPlumb.Overlays.Arrow.call(this, p); this.type = "Diamond"; }; jsPlumbUtil.extend(jsPlumb.Overlays.Diamond, jsPlumb.Overlays.Arrow); var _getDimensions = function (component) {
        if (component._jsPlumb.cachedDimensions == null)
            component._jsPlumb.cachedDimensions = component.getDimensions(); return component._jsPlumb.cachedDimensions;
    }; var AbstractDOMOverlay = function (params) {
        jsPlumb.DOMElementComponent.apply(this, arguments); AbstractOverlay.apply(this, arguments); this.id = params.id; this._jsPlumb.div = null; this._jsPlumb.initialised = false; this._jsPlumb.component = params.component; this._jsPlumb.cachedDimensions = null; this._jsPlumb.create = params.create; this.getElement = function () {
            if (this._jsPlumb.div == null) {
                var div = this._jsPlumb.div = jsPlumb.getDOMElement(this._jsPlumb.create(this._jsPlumb.component)); div.style.position = "absolute"; var clazz = this._jsPlumb.instance.overlayClass + " " +
(this.cssClass ? this.cssClass : params.cssClass ? params.cssClass : ""); div.className = clazz; this._jsPlumb.instance.appendElement(div); this._jsPlumb.instance.getId(div); this.attachListeners(div, this); this.canvas = div;
            }
            return this._jsPlumb.div;
        }; this.draw = function (component, currentConnectionPaintStyle, absolutePosition) {
            var td = _getDimensions(this); if (td != null && td.length == 2) {
                var cxy = { x: 0, y: 0 }; if (absolutePosition) { cxy = { x: absolutePosition[0], y: absolutePosition[1] }; }
                else if (component.pointOnPath) {
                    var loc = this.loc, absolute = false; if (jsPlumbUtil.isString(this.loc) || this.loc < 0 || this.loc > 1) { loc = parseInt(this.loc, 10); absolute = true; }
                    cxy = component.pointOnPath(loc, absolute);
                }
                else { var locToUse = this.loc.constructor == Array ? this.loc : this.endpointLoc; cxy = { x: locToUse[0] * component.w, y: locToUse[1] * component.h }; }
                var minx = cxy.x - (td[0] / 2), miny = cxy.y - (td[1] / 2); return { component: component, d: { minx: minx, miny: miny, td: td, cxy: cxy }, minX: minx, maxX: minx + td[0], minY: miny, maxY: miny + td[1] };
            }
            else return { minX: 0, maxX: 0, minY: 0, maxY: 0 };
        };
    }; jsPlumbUtil.extend(AbstractDOMOverlay, [jsPlumb.DOMElementComponent, AbstractOverlay], { getDimensions: function () { return jsPlumb.getSize(this.getElement()); }, setVisible: function (state) { this._jsPlumb.div.style.display = state ? "block" : "none"; }, clearCachedDimensions: function () { this._jsPlumb.cachedDimensions = null; }, cleanup: function () {
        if (this._jsPlumb.div != null)
            this._jsPlumb.instance.removeElement(this._jsPlumb.div);
    }, computeMaxSize: function () { var td = _getDimensions(this); return Math.max(td[0], td[1]); }, reattachListeners: function (connector) { if (this._jsPlumb.div) { this.reattachListenersForElement(this._jsPlumb.div, this, connector); } }, paint: function (p, containerExtents) {
        if (!this._jsPlumb.initialised) { this.getElement(); p.component.appendDisplayElement(this._jsPlumb.div); this.attachListeners(this._jsPlumb.div, p.component); this._jsPlumb.initialised = true; }
        this._jsPlumb.div.style.left = (p.component.x + p.d.minx) + "px"; this._jsPlumb.div.style.top = (p.component.y + p.d.miny) + "px";
    } 
    }); jsPlumb.Overlays.Custom = function (params) { this.type = "Custom"; AbstractDOMOverlay.apply(this, arguments); }; jsPlumbUtil.extend(jsPlumb.Overlays.Custom, AbstractDOMOverlay); jsPlumb.Overlays.GuideLines = function () { var self = this; self.length = 50; self.lineWidth = 5; this.type = "GuideLines"; AbstractOverlay.apply(this, arguments); jsPlumb.jsPlumbUIComponent.apply(this, arguments); this.draw = function (connector, currentConnectionPaintStyle) { var head = connector.pointAlongPathFrom(self.loc, self.length / 2), mid = connector.pointOnPath(self.loc), tail = Biltong.pointOnLine(head, mid, self.length), tailLine = Biltong.perpendicularLineTo(head, tail, 40), headLine = Biltong.perpendicularLineTo(tail, head, 20); return { connector: connector, head: head, tail: tail, headLine: headLine, tailLine: tailLine, minX: Math.min(head.x, tail.x, headLine[0].x, headLine[1].x), minY: Math.min(head.y, tail.y, headLine[0].y, headLine[1].y), maxX: Math.max(head.x, tail.x, headLine[0].x, headLine[1].x), maxY: Math.max(head.y, tail.y, headLine[0].y, headLine[1].y) }; }; }; jsPlumb.Overlays.Label = function (params) {
        this.labelStyle = params.labelStyle; var labelWidth = null, labelHeight = null, labelText = null, labelPadding = null; this.cssClass = this.labelStyle != null ? this.labelStyle.cssClass : null; var p = jsPlumb.extend({ create: function () { return document.createElement("div"); } }, params); jsPlumb.Overlays.Custom.call(this, p); this.type = "Label"; this.label = params.label || ""; this.labelText = null; if (this.labelStyle) {
            var el = this.getElement(); this.labelStyle.font = this.labelStyle.font || "12px sans-serif"; el.style.font = this.labelStyle.font; el.style.color = this.labelStyle.color || "black"; if (this.labelStyle.fillStyle) el.style.background = this.labelStyle.fillStyle; if (this.labelStyle.borderWidth > 0) { var dStyle = this.labelStyle.borderStyle ? this.labelStyle.borderStyle : "black"; el.style.border = this.labelStyle.borderWidth + "px solid " + dStyle; }
            if (this.labelStyle.padding) el.style.padding = this.labelStyle.padding;
        } 
    }; jsPlumbUtil.extend(jsPlumb.Overlays.Label, jsPlumb.Overlays.Custom, { cleanup: function () { this.div = null; this.label = null; this.labelText = null; this.cssClass = null; this.labelStyle = null; }, getLabel: function () { return this.label; }, setLabel: function (l) { this.label = l; this.labelText = null; this.clearCachedDimensions(); this.update(); this.component.repaint(); }, getDimensions: function () { this.update(); return AbstractDOMOverlay.prototype.getDimensions.apply(this, arguments); }, update: function () {
        if (typeof this.label == "function") { var lt = this.label(this); this.getElement().innerHTML = lt.replace(/\r\n/g, "<br/>"); }
        else { if (this.labelText == null) { this.labelText = this.label; this.getElement().innerHTML = this.labelText.replace(/\r\n/g, "<br/>"); } } 
    } 
    });
})(); ; (function () {
    var Bezier = function (params) {
        params = params || {}; var _super = jsPlumb.Connectors.AbstractConnector.apply(this, arguments), stub = params.stub || 50, majorAnchor = params.curviness || 150, minorAnchor = 10; this.type = "Bezier"; this.getCurviness = function () { return majorAnchor; }; this._findControlPoint = function (point, sourceAnchorPosition, targetAnchorPosition, sourceEndpoint, targetEndpoint) {
            var soo = sourceEndpoint.anchor.getOrientation(sourceEndpoint), too = targetEndpoint.anchor.getOrientation(targetEndpoint), perpendicular = soo[0] != too[0] || soo[1] == too[1], p = []; if (!perpendicular) {
                if (soo[0] === 0)
                    p.push(sourceAnchorPosition[0] < targetAnchorPosition[0] ? point[0] + minorAnchor : point[0] - minorAnchor); else p.push(point[0] - (majorAnchor * soo[0])); if (soo[1] === 0)
                    p.push(sourceAnchorPosition[1] < targetAnchorPosition[1] ? point[1] + minorAnchor : point[1] - minorAnchor); else p.push(point[1] + (majorAnchor * too[1]));
            }
            else {
                if (too[0] === 0)
                    p.push(targetAnchorPosition[0] < sourceAnchorPosition[0] ? point[0] + minorAnchor : point[0] - minorAnchor); else p.push(point[0] + (majorAnchor * too[0])); if (too[1] === 0)
                    p.push(targetAnchorPosition[1] < sourceAnchorPosition[1] ? point[1] + minorAnchor : point[1] - minorAnchor); else p.push(point[1] + (majorAnchor * soo[1]));
            }
            return p;
        }; this._compute = function (paintInfo, p) { var sp = p.sourcePos, tp = p.targetPos, _w = Math.abs(sp[0] - tp[0]), _h = Math.abs(sp[1] - tp[1]), _sx = sp[0] < tp[0] ? _w : 0, _sy = sp[1] < tp[1] ? _h : 0, _tx = sp[0] < tp[0] ? 0 : _w, _ty = sp[1] < tp[1] ? 0 : _h, _CP = this._findControlPoint([_sx, _sy], sp, tp, p.sourceEndpoint, p.targetEndpoint), _CP2 = this._findControlPoint([_tx, _ty], tp, sp, p.targetEndpoint, p.sourceEndpoint); _super.addSegment(this, "Bezier", { x1: _sx, y1: _sy, x2: _tx, y2: _ty, cp1x: _CP[0], cp1y: _CP[1], cp2x: _CP2[0], cp2y: _CP2[1] }); };
    }; jsPlumbUtil.extend(Bezier, jsPlumb.Connectors.AbstractConnector); jsPlumb.registerConnectorType(Bezier, "Bezier");
})(); ; (function () {
    "use strict"; var svgAttributeMap = { "joinstyle": "stroke-linejoin", "stroke-linejoin": "stroke-linejoin", "stroke-dashoffset": "stroke-dashoffset", "stroke-linecap": "stroke-linecap" }, STROKE_DASHARRAY = "stroke-dasharray", DASHSTYLE = "dashstyle", LINEAR_GRADIENT = "linearGradient", RADIAL_GRADIENT = "radialGradient", DEFS = "defs", FILL = "fill", STOP = "stop", STROKE = "stroke", STROKE_WIDTH = "stroke-width", STYLE = "style", NONE = "none", JSPLUMB_GRADIENT = "jsplumb_gradient_", LINE_WIDTH = "lineWidth", ns = { svg: "http://www.w3.org/2000/svg", xhtml: "http://www.w3.org/1999/xhtml" }, _attr = function (node, attributes) {
        for (var i in attributes)
            node.setAttribute(i, "" + attributes[i]);
    }, _node = function (name, attributes) { var n = document.createElementNS(ns.svg, name); attributes = attributes || {}; attributes.version = "1.1"; attributes.xmlns = ns.xhtml; _attr(n, attributes); return n; }, _pos = function (d) { return "position:absolute;left:" + d[0] + "px;top:" + d[1] + "px"; }, _clearGradient = function (parent) {
        for (var i = 0; i < parent.childNodes.length; i++) {
            if (parent.childNodes[i].tagName == DEFS || parent.childNodes[i].tagName == LINEAR_GRADIENT || parent.childNodes[i].tagName == RADIAL_GRADIENT)
                parent.removeChild(parent.childNodes[i]);
        } 
    }, _updateGradient = function (parent, node, style, dimensions, uiComponent) {
        var id = JSPLUMB_GRADIENT + uiComponent._jsPlumb.instance.idstamp(); _clearGradient(parent); var g; if (!style.gradient.offset) { g = _node(LINEAR_GRADIENT, { id: id, gradientUnits: "userSpaceOnUse" }); }
        else { g = _node(RADIAL_GRADIENT, { id: id }); }
        var defs = _node(DEFS); parent.appendChild(defs); defs.appendChild(g); for (var i = 0; i < style.gradient.stops.length; i++) { var styleToUse = uiComponent.segment == 1 || uiComponent.segment == 2 ? i : style.gradient.stops.length - 1 - i, stopColor = jsPlumbUtil.convertStyle(style.gradient.stops[styleToUse][1], true), s = _node(STOP, { "offset": Math.floor(style.gradient.stops[i][0] * 100) + "%", "stop-color": stopColor }); g.appendChild(s); }
        var applyGradientTo = style.strokeStyle ? STROKE : FILL; node.setAttribute(applyGradientTo, "url(#" + id + ")");
    }, _applyStyles = function (parent, node, style, dimensions, uiComponent) {
        node.setAttribute(FILL, style.fillStyle ? jsPlumbUtil.convertStyle(style.fillStyle, true) : NONE); node.setAttribute(STROKE, style.strokeStyle ? jsPlumbUtil.convertStyle(style.strokeStyle, true) : NONE); if (style.gradient) { _updateGradient(parent, node, style, dimensions, uiComponent); }
        else { _clearGradient(parent); node.setAttribute(STYLE, ""); }
        if (style.lineWidth) { node.setAttribute(STROKE_WIDTH, style.lineWidth); }
        if (style[DASHSTYLE] && style[LINE_WIDTH] && !style[STROKE_DASHARRAY]) { var sep = style[DASHSTYLE].indexOf(",") == -1 ? " " : ",", parts = style[DASHSTYLE].split(sep), styleToUse = ""; parts.forEach(function (p) { styleToUse += (Math.floor(p * style.lineWidth) + sep); }); node.setAttribute(STROKE_DASHARRAY, styleToUse); }
        else if (style[STROKE_DASHARRAY]) { node.setAttribute(STROKE_DASHARRAY, style[STROKE_DASHARRAY]); }
        for (var i in svgAttributeMap) { if (style[i]) { node.setAttribute(svgAttributeMap[i], style[i]); } } 
    }, _decodeFont = function (f) { var r = /([0-9].)(p[xt])\s(.*)/, bits = f.match(r); return { size: bits[1] + bits[2], font: bits[3] }; }, _appendAtIndex = function (svg, path, idx) {
        if (svg.childNodes.length > idx) { svg.insertBefore(path, svg.childNodes[idx]); }
        else svg.appendChild(path);
    }; jsPlumbUtil.svg = { node: _node, attr: _attr, pos: _pos }; var SvgComponent = function (params) {
        var pointerEventsSpec = params.pointerEventsSpec || "all", renderer = {}; jsPlumb.jsPlumbUIComponent.apply(this, params.originalArgs); this.canvas = null; this.path = null; this.svg = null; this.bgCanvas = null; var clazz = params.cssClass + " " + (params.originalArgs[0].cssClass || ""), svgParams = { "style": "", "width": 0, "height": 0, "pointer-events": pointerEventsSpec, "position": "absolute" }; this.svg = _node("svg", svgParams); if (params.useDivWrapper) { this.canvas = document.createElement("div"); this.canvas.style.position = "absolute"; jsPlumbUtil.sizeElement(this.canvas, 0, 0, 1, 1); this.canvas.className = clazz; }
        else { _attr(this.svg, { "class": clazz }); this.canvas = this.svg; }
        params._jsPlumb.appendElement(this.canvas, params.originalArgs[0].parent); if (params.useDivWrapper) this.canvas.appendChild(this.svg); var displayElements = [this.canvas]; this.getDisplayElements = function () { return displayElements; }; this.appendDisplayElement = function (el) { displayElements.push(el); }; this.paint = function (style, anchor, extents) {
            if (style != null) {
                var xy = [this.x, this.y], wh = [this.w, this.h], p; if (extents != null) { if (extents.xmin < 0) xy[0] += extents.xmin; if (extents.ymin < 0) xy[1] += extents.ymin; wh[0] = extents.xmax + ((extents.xmin < 0) ? -extents.xmin : 0); wh[1] = extents.ymax + ((extents.ymin < 0) ? -extents.ymin : 0); }
                if (params.useDivWrapper) { jsPlumbUtil.sizeElement(this.canvas, xy[0], xy[1], wh[0], wh[1]); xy[0] = 0; xy[1] = 0; p = _pos([0, 0]); }
                else
                    p = _pos([xy[0], xy[1]]); renderer.paint.apply(this, arguments); _attr(this.svg, { "style": p, "width": wh[0], "height": wh[1] });
            } 
        }; return { renderer: renderer };
    }; jsPlumbUtil.extend(SvgComponent, jsPlumb.jsPlumbUIComponent, { cleanup: function () { if (this.canvas && this.canvas.parentNode) this.canvas.parentNode.removeChild(this.canvas); this.svg = null; this.canvas = null; this.bgCanvas = null; this.path = null; this.group = null; }, setVisible: function (v) {
        if (this.canvas) { this.canvas.style.display = v ? "block" : "none"; }
        if (this.bgCanvas) { this.bgCanvas.style.display = v ? "block" : "none"; } 
    } 
    }); var SvgConnector = jsPlumb.ConnectorRenderers.svg = function (params) {
        var self = this, _super = SvgComponent.apply(this, [{ cssClass: params._jsPlumb.connectorClass, originalArgs: arguments, pointerEventsSpec: "none", _jsPlumb: params._jsPlumb}]); _super.renderer.paint = function (style, anchor, extents) {
            var segments = self.getSegments(), p = "", offset = [0, 0]; if (extents.xmin < 0) offset[0] = -extents.xmin; if (extents.ymin < 0) offset[1] = -extents.ymin; if (segments.length > 0) {
                for (var i = 0; i < segments.length; i++) { p += jsPlumb.Segments.svg.SegmentRenderer.getPath(segments[i]); p += " "; }
                var a = { d: p, transform: "translate(" + offset[0] + "," + offset[1] + ")", "pointer-events": params["pointer-events"] || "visibleStroke" }, outlineStyle = null, d = [self.x, self.y, self.w, self.h]; var mouseInOutFilters = { "mouseenter": function (e) { var rt = e.relatedTarget; return rt == null || (rt != self.path && rt != self.bgPath); }, "mouseout": function (e) { var rt = e.relatedTarget; return rt == null || (rt != self.path && rt != self.bgPath); } }; if (style.outlineColor) {
                    var outlineWidth = style.outlineWidth || 1, outlineStrokeWidth = style.lineWidth + (2 * outlineWidth); outlineStyle = jsPlumb.extend({}, style); outlineStyle.strokeStyle = jsPlumbUtil.convertStyle(style.outlineColor); outlineStyle.lineWidth = outlineStrokeWidth; if (self.bgPath == null) { self.bgPath = _node("path", a); _appendAtIndex(self.svg, self.bgPath, 0); self.attachListeners(self.bgPath, self, mouseInOutFilters); }
                    else { _attr(self.bgPath, a); }
                    _applyStyles(self.svg, self.bgPath, outlineStyle, d, self);
                }
                if (self.path == null) { self.path = _node("path", a); _appendAtIndex(self.svg, self.path, style.outlineColor ? 1 : 0); self.attachListeners(self.path, self, mouseInOutFilters); }
                else { _attr(self.path, a); }
                _applyStyles(self.svg, self.path, style, d, self);
            } 
        }; this.reattachListeners = function () { if (this.bgPath) this.reattachListenersForElement(this.bgPath, this); if (this.path) this.reattachListenersForElement(this.path, this); };
    }; jsPlumbUtil.extend(jsPlumb.ConnectorRenderers.svg, SvgComponent); jsPlumb.Segments.svg = { SegmentRenderer: { getPath: function (segment) { return ({ "Straight": function () { var d = segment.getCoordinates(); return "M " + d.x1 + " " + d.y1 + " L " + d.x2 + " " + d.y2; }, "Bezier": function () { var d = segment.params; return "M " + d.x1 + " " + d.y1 + " C " + d.cp1x + " " + d.cp1y + " " + d.cp2x + " " + d.cp2y + " " + d.x2 + " " + d.y2; }, "Arc": function () { var d = segment.params, laf = segment.sweep > Math.PI ? 1 : 0, sf = segment.anticlockwise ? 0 : 1; return "M" + segment.x1 + " " + segment.y1 + " A " + segment.radius + " " + d.r + " 0 " + laf + "," + sf + " " + segment.x2 + " " + segment.y2; } })[segment.type](); } } }; var SvgEndpoint = window.SvgEndpoint = function (params) {
        var _super = SvgComponent.apply(this, [{ cssClass: params._jsPlumb.endpointClass, originalArgs: arguments, pointerEventsSpec: "all", useDivWrapper: true, _jsPlumb: params._jsPlumb}]); _super.renderer.paint = function (style) {
            var s = jsPlumb.extend({}, style); if (s.outlineColor) { s.strokeWidth = s.outlineWidth; s.strokeStyle = jsPlumbUtil.convertStyle(s.outlineColor, true); }
            if (this.node == null) { this.node = this.makeNode(s); this.svg.appendChild(this.node); this.attachListeners(this.node, this); }
            else if (this.updateNode != null) { this.updateNode(this.node); }
            _applyStyles(this.svg, this.node, s, [this.x, this.y, this.w, this.h], this); _pos(this.node, [this.x, this.y]);
        } .bind(this);
    }; jsPlumbUtil.extend(SvgEndpoint, SvgComponent, { reattachListeners: function () { if (this.node) this.reattachListenersForElement(this.node, this); } }); jsPlumb.Endpoints.svg.Dot = function () { jsPlumb.Endpoints.Dot.apply(this, arguments); SvgEndpoint.apply(this, arguments); this.makeNode = function (style) { return _node("circle", { "cx": this.w / 2, "cy": this.h / 2, "r": this.radius }); }; this.updateNode = function (node) { _attr(node, { "cx": this.w / 2, "cy": this.h / 2, "r": this.radius }); }; }; jsPlumbUtil.extend(jsPlumb.Endpoints.svg.Dot, [jsPlumb.Endpoints.Dot, SvgEndpoint]); jsPlumb.Endpoints.svg.Rectangle = function () { jsPlumb.Endpoints.Rectangle.apply(this, arguments); SvgEndpoint.apply(this, arguments); this.makeNode = function (style) { return _node("rect", { "width": this.w, "height": this.h }); }; this.updateNode = function (node) { _attr(node, { "width": this.w, "height": this.h }); }; }; jsPlumbUtil.extend(jsPlumb.Endpoints.svg.Rectangle, [jsPlumb.Endpoints.Rectangle, SvgEndpoint]); jsPlumb.Endpoints.svg.Image = jsPlumb.Endpoints.Image; jsPlumb.Endpoints.svg.Blank = jsPlumb.Endpoints.Blank; jsPlumb.Overlays.svg.Label = jsPlumb.Overlays.Label; jsPlumb.Overlays.svg.Custom = jsPlumb.Overlays.Custom; var AbstractSvgArrowOverlay = function (superclass, originalArgs) {
        superclass.apply(this, originalArgs); jsPlumb.jsPlumbUIComponent.apply(this, originalArgs); this.isAppendedAtTopLevel = false; var self = this; this.path = null; this.paint = function (params, containerExtents) {
            if (params.component.svg && containerExtents) {
                if (this.path == null) { this.path = _node("path", { "pointer-events": "all" }); params.component.svg.appendChild(this.path); this.canvas = params.component.svg; this.attachListeners(this.path, params.component); this.attachListeners(this.path, this); }
                var clazz = originalArgs && (originalArgs.length == 1) ? (originalArgs[0].cssClass || "") : "", offset = [0, 0]; if (containerExtents.xmin < 0) offset[0] = -containerExtents.xmin; if (containerExtents.ymin < 0) offset[1] = -containerExtents.ymin; _attr(this.path, { "d": makePath(params.d), "class": clazz, stroke: params.strokeStyle ? params.strokeStyle : null, fill: params.fillStyle ? params.fillStyle : null, transform: "translate(" + offset[0] + "," + offset[1] + ")" });
            } 
        }; var makePath = function (d) { return "M" + d.hxy.x + "," + d.hxy.y + " L" + d.tail[0].x + "," + d.tail[0].y + " L" + d.cxy.x + "," + d.cxy.y + " L" + d.tail[1].x + "," + d.tail[1].y + " L" + d.hxy.x + "," + d.hxy.y; }; this.reattachListeners = function () { if (this.path) this.reattachListenersForElement(this.path, this); };
    }; jsPlumbUtil.extend(AbstractSvgArrowOverlay, [jsPlumb.jsPlumbUIComponent, jsPlumb.Overlays.AbstractOverlay], { cleanup: function () { if (this.path != null) this._jsPlumb.instance.removeElement(this.path); }, setVisible: function (v) { if (this.path != null) (this.path.style.display = (v ? "block" : "none")); } }); jsPlumb.Overlays.svg.Arrow = function () { AbstractSvgArrowOverlay.apply(this, [jsPlumb.Overlays.Arrow, arguments]); }; jsPlumbUtil.extend(jsPlumb.Overlays.svg.Arrow, [jsPlumb.Overlays.Arrow, AbstractSvgArrowOverlay]); jsPlumb.Overlays.svg.PlainArrow = function () { AbstractSvgArrowOverlay.apply(this, [jsPlumb.Overlays.PlainArrow, arguments]); }; jsPlumbUtil.extend(jsPlumb.Overlays.svg.PlainArrow, [jsPlumb.Overlays.PlainArrow, AbstractSvgArrowOverlay]); jsPlumb.Overlays.svg.Diamond = function () { AbstractSvgArrowOverlay.apply(this, [jsPlumb.Overlays.Diamond, arguments]); }; jsPlumbUtil.extend(jsPlumb.Overlays.svg.Diamond, [jsPlumb.Overlays.Diamond, AbstractSvgArrowOverlay]); jsPlumb.Overlays.svg.GuideLines = function () {
        var path = null, self = this, p1_1, p1_2; jsPlumb.Overlays.GuideLines.apply(this, arguments); this.paint = function (params, containerExtents) {
            if (path == null) { path = _node("path"); params.connector.svg.appendChild(path); self.attachListeners(path, params.connector); self.attachListeners(path, self); p1_1 = _node("path"); params.connector.svg.appendChild(p1_1); self.attachListeners(p1_1, params.connector); self.attachListeners(p1_1, self); p1_2 = _node("path"); params.connector.svg.appendChild(p1_2); self.attachListeners(p1_2, params.connector); self.attachListeners(p1_2, self); }
            var offset = [0, 0]; if (containerExtents.xmin < 0) offset[0] = -containerExtents.xmin; if (containerExtents.ymin < 0) offset[1] = -containerExtents.ymin; _attr(path, { "d": makePath(params.head, params.tail), stroke: "red", fill: null, transform: "translate(" + offset[0] + "," + offset[1] + ")" }); _attr(p1_1, { "d": makePath(params.tailLine[0], params.tailLine[1]), stroke: "blue", fill: null, transform: "translate(" + offset[0] + "," + offset[1] + ")" }); _attr(p1_2, { "d": makePath(params.headLine[0], params.headLine[1]), stroke: "green", fill: null, transform: "translate(" + offset[0] + "," + offset[1] + ")" });
        }; var makePath = function (d1, d2) { return "M " + d1.x + "," + d1.y + " L" + d2.x + "," + d2.y; };
    }; jsPlumbUtil.extend(jsPlumb.Overlays.svg.GuideLines, jsPlumb.Overlays.GuideLines);
})(); ; (function () {
    "use strict"; var vmlAttributeMap = { "stroke-linejoin": "joinstyle", "joinstyle": "joinstyle", "endcap": "endcap", "miterlimit": "miterlimit" }, jsPlumbStylesheet = null; if (document.createStyleSheet && document.namespaces) {
        var ruleClasses = [".jsplumb_vml", "jsplumb\\:textbox", "jsplumb\\:oval", "jsplumb\\:rect", "jsplumb\\:stroke", "jsplumb\\:shape", "jsplumb\\:group"], rule = "behavior:url(#default#VML);position:absolute;"; jsPlumbStylesheet = document.createStyleSheet(); for (var i = 0; i < ruleClasses.length; i++)
            jsPlumbStylesheet.addRule(ruleClasses[i], rule); document.namespaces.add("jsplumb", "urn:schemas-microsoft-com:vml");
    }
    jsPlumb.vml = {}; var scale = 1000, _atts = function (o, atts) { for (var i in atts) { o[i] = atts[i]; } }, _node = function (name, d, atts, parent, _jsPlumb, deferToJsPlumbContainer) {
        atts = atts || {}; var o = document.createElement("jsplumb:" + name); if (deferToJsPlumbContainer)
            _jsPlumb.appendElement(o, parent); else
            parent.appendChild(o); o.className = (atts["class"] ? atts["class"] + " " : "") + "jsplumb_vml"; _pos(o, d); _atts(o, atts); return o;
    }, _pos = function (o, d, zIndex) {
        o.style.left = d[0] + "px"; o.style.top = d[1] + "px"; o.style.width = d[2] + "px"; o.style.height = d[3] + "px"; o.style.position = "absolute"; if (zIndex)
            o.style.zIndex = zIndex;
    }, _conv = jsPlumb.vml.convertValue = function (v) { return Math.floor(v * scale); }, _maybeSetOpacity = function (styleToWrite, styleToCheck, type, component) {
        if ("transparent" === styleToCheck)
            component.setOpacity(type, "0.0"); else
            component.setOpacity(type, "1.0");
    }, _applyStyles = function (node, style, component, _jsPlumb) {
        var styleToWrite = {}; if (style.strokeStyle) { styleToWrite.stroked = "true"; var strokeColor = jsPlumbUtil.convertStyle(style.strokeStyle, true); styleToWrite.strokecolor = strokeColor; _maybeSetOpacity(styleToWrite, strokeColor, "stroke", component); styleToWrite.strokeweight = style.lineWidth + "px"; }
        else styleToWrite.stroked = "false"; if (style.fillStyle) { styleToWrite.filled = "true"; var fillColor = jsPlumbUtil.convertStyle(style.fillStyle, true); styleToWrite.fillcolor = fillColor; _maybeSetOpacity(styleToWrite, fillColor, "fill", component); }
        else styleToWrite.filled = "false"; if (style.dashstyle) {
            if (component.strokeNode == null) { component.strokeNode = _node("stroke", [0, 0, 0, 0], { dashstyle: style.dashstyle }, node, _jsPlumb); }
            else
                component.strokeNode.dashstyle = style.dashstyle;
        }
        else if (style["stroke-dasharray"] && style.lineWidth) {
            var sep = style["stroke-dasharray"].indexOf(",") == -1 ? " " : ",", parts = style["stroke-dasharray"].split(sep), styleToUse = ""; for (var i = 0; i < parts.length; i++) { styleToUse += (Math.floor(parts[i] / style.lineWidth) + sep); }
            if (component.strokeNode == null) { component.strokeNode = _node("stroke", [0, 0, 0, 0], { dashstyle: styleToUse }, node, _jsPlumb); }
            else
                component.strokeNode.dashstyle = styleToUse;
        }
        _atts(node, styleToWrite);
    }, VmlComponent = function () { var self = this, renderer = {}; jsPlumb.jsPlumbUIComponent.apply(this, arguments); this.opacityNodes = { "stroke": null, "fill": null }; this.initOpacityNodes = function (vml) { self.opacityNodes.stroke = _node("stroke", [0, 0, 1, 1], { opacity: "0.0" }, vml, self._jsPlumb.instance); self.opacityNodes.fill = _node("fill", [0, 0, 1, 1], { opacity: "0.0" }, vml, self._jsPlumb.instance); }; this.setOpacity = function (type, value) { var node = self.opacityNodes[type]; if (node) node.opacity = "" + value; }; var displayElements = []; this.getDisplayElements = function () { return displayElements; }; this.appendDisplayElement = function (el, doNotAppendToCanvas) { if (!doNotAppendToCanvas) self.canvas.parentNode.appendChild(el); displayElements.push(el); }; }; jsPlumbUtil.extend(VmlComponent, jsPlumb.jsPlumbUIComponent, { cleanup: function () { if (this.bgCanvas) this.bgCanvas.parentNode.removeChild(this.bgCanvas); if (this.canvas) this.canvas.parentNode.removeChild(this.canvas); } }); var VmlConnector = jsPlumb.ConnectorRenderers.vml = function (params) {
        this.strokeNode = null; this.canvas = null; VmlComponent.apply(this, arguments); var clazz = this._jsPlumb.instance.connectorClass + (params.cssClass ? (" " + params.cssClass) : ""); this.paint = function (style) {
            if (style !== null) {
                this.w = Math.max(this.w, 1); this.h = Math.max(this.h, 1); var segments = this.getSegments(), p = { "path": "" }, d = [this.x, this.y, this.w, this.h]; for (var i = 0; i < segments.length; i++) { p.path += jsPlumb.Segments.vml.SegmentRenderer.getPath(segments[i]); p.path += " "; }
                if (style.outlineColor) {
                    var outlineWidth = style.outlineWidth || 1, outlineStrokeWidth = style.lineWidth + (2 * outlineWidth), outlineStyle = { strokeStyle: jsPlumbUtil.convertStyle(style.outlineColor), lineWidth: outlineStrokeWidth }; for (var aa in vmlAttributeMap) outlineStyle[aa] = style[aa]; if (this.bgCanvas == null) { p["class"] = clazz; p.coordsize = (d[2] * scale) + "," + (d[3] * scale); this.bgCanvas = _node("shape", d, p, params.parent, this._jsPlumb.instance, true); _pos(this.bgCanvas, d); this.appendDisplayElement(this.bgCanvas, true); this.attachListeners(this.bgCanvas, this); this.initOpacityNodes(this.bgCanvas, ["stroke"]); }
                    else { p.coordsize = (d[2] * scale) + "," + (d[3] * scale); _pos(this.bgCanvas, d); _atts(this.bgCanvas, p); }
                    _applyStyles(this.bgCanvas, outlineStyle, this);
                }
                if (this.canvas == null) { p["class"] = clazz; p.coordsize = (d[2] * scale) + "," + (d[3] * scale); this.canvas = _node("shape", d, p, params.parent, this._jsPlumb.instance, true); this.appendDisplayElement(this.canvas, true); this.attachListeners(this.canvas, this); this.initOpacityNodes(this.canvas, ["stroke"]); }
                else { p.coordsize = (d[2] * scale) + "," + (d[3] * scale); _pos(this.canvas, d); _atts(this.canvas, p); }
                _applyStyles(this.canvas, style, this, this._jsPlumb.instance);
            } 
        };
    }; jsPlumbUtil.extend(VmlConnector, VmlComponent, { reattachListeners: function () { if (this.canvas) this.reattachListenersForElement(this.canvas, this); }, setVisible: function (v) {
        if (this.canvas) { this.canvas.style.display = v ? "block" : "none"; }
        if (this.bgCanvas) { this.bgCanvas.style.display = v ? "block" : "none"; } 
    } 
    }); var VmlEndpoint = window.VmlEndpoint = function (params) {
        VmlComponent.apply(this, arguments); this._jsPlumb.vml = null; this.canvas = document.createElement("div"); this.canvas.style.position = "absolute"; this._jsPlumb.clazz = this._jsPlumb.instance.endpointClass + (params.cssClass ? (" " + params.cssClass) : ""); params._jsPlumb.appendElement(this.canvas, params.parent); this.paint = function (style, anchor) {
            var p = {}, vml = this._jsPlumb.vml; jsPlumbUtil.sizeElement(this.canvas, this.x, this.y, this.w, this.h); if (this._jsPlumb.vml == null) { p["class"] = this._jsPlumb.clazz; vml = this._jsPlumb.vml = this.getVml([0, 0, this.w, this.h], p, anchor, this.canvas, this._jsPlumb.instance); this.attachListeners(vml, this); this.appendDisplayElement(vml, true); this.appendDisplayElement(this.canvas, true); this.initOpacityNodes(vml, ["fill"]); }
            else { _pos(vml, [0, 0, this.w, this.h]); _atts(vml, p); }
            _applyStyles(vml, style, this);
        };
    }; jsPlumbUtil.extend(VmlEndpoint, VmlComponent, { reattachListeners: function () { if (this._jsPlumb.vml) this.reattachListenersForElement(this._jsPlumb.vml, this); } }); jsPlumb.Segments.vml = { SegmentRenderer: { getPath: function (segment) {
        return ({ "Straight": function (segment) { var d = segment.params; return "m" + _conv(d.x1) + "," + _conv(d.y1) + " l" + _conv(d.x2) + "," + _conv(d.y2) + " e"; }, "Bezier": function (segment) { var d = segment.params; return "m" + _conv(d.x1) + "," + _conv(d.y1) + " c" + _conv(d.cp1x) + "," + _conv(d.cp1y) + "," + _conv(d.cp2x) + "," + _conv(d.cp2y) + "," + _conv(d.x2) + "," + _conv(d.y2) + " e"; }, "Arc": function (segment) {
            var d = segment.params, xmin = Math.min(d.x1, d.x2), xmax = Math.max(d.x1, d.x2), ymin = Math.min(d.y1, d.y2), ymax = Math.max(d.y1, d.y2), sf = segment.anticlockwise ? 1 : 0, pathType = (segment.anticlockwise ? "at " : "wa "), makePosString = function () {
                if (d.loopback)
                    return "0,0," + _conv(2 * d.r) + "," + _conv(2 * d.r); var xy = [null, [function () { return [xmin, ymin]; }, function () { return [xmin - d.r, ymin - d.r]; } ], [function () { return [xmin - d.r, ymin]; }, function () { return [xmin, ymin - d.r]; } ], [function () { return [xmin - d.r, ymin - d.r]; }, function () { return [xmin, ymin]; } ], [function () { return [xmin, ymin - d.r]; }, function () { return [xmin - d.r, ymin]; } ]][segment.segment][sf](); return _conv(xy[0]) + "," + _conv(xy[1]) + "," + _conv(xy[0] + (2 * d.r)) + "," + _conv(xy[1] + (2 * d.r));
            }; return pathType + " " + makePosString() + "," + _conv(d.x1) + "," + _conv(d.y1) + "," + _conv(d.x2) + "," + _conv(d.y2) + " e";
        } 
        })[segment.type](segment);
    } 
    }
    }; jsPlumb.Endpoints.vml.Dot = function () { jsPlumb.Endpoints.Dot.apply(this, arguments); VmlEndpoint.apply(this, arguments); this.getVml = function (d, atts, anchor, parent, _jsPlumb) { return _node("oval", d, atts, parent, _jsPlumb); }; }; jsPlumbUtil.extend(jsPlumb.Endpoints.vml.Dot, VmlEndpoint); jsPlumb.Endpoints.vml.Rectangle = function () { jsPlumb.Endpoints.Rectangle.apply(this, arguments); VmlEndpoint.apply(this, arguments); this.getVml = function (d, atts, anchor, parent, _jsPlumb) { return _node("rect", d, atts, parent, _jsPlumb); }; }; jsPlumbUtil.extend(jsPlumb.Endpoints.vml.Rectangle, VmlEndpoint); jsPlumb.Endpoints.vml.Image = jsPlumb.Endpoints.Image; jsPlumb.Endpoints.vml.Blank = jsPlumb.Endpoints.Blank; jsPlumb.Overlays.vml.Label = jsPlumb.Overlays.Label; jsPlumb.Overlays.vml.Custom = jsPlumb.Overlays.Custom; var AbstractVmlArrowOverlay = function (superclass, originalArgs) {
        superclass.apply(this, originalArgs); VmlComponent.apply(this, originalArgs); var self = this, path = null; this.canvas = null; this.isAppendedAtTopLevel = true; var getPath = function (d) { return "m " + _conv(d.hxy.x) + "," + _conv(d.hxy.y) + " l " + _conv(d.tail[0].x) + "," + _conv(d.tail[0].y) + " " + _conv(d.cxy.x) + "," + _conv(d.cxy.y) + " " + _conv(d.tail[1].x) + "," + _conv(d.tail[1].y) + " x e"; }; this.paint = function (params, containerExtents) {
            if (params.component.canvas && containerExtents) {
                var p = {}, d = params.d, connector = params.component; if (params.strokeStyle) { p.stroked = "true"; p.strokecolor = jsPlumbUtil.convertStyle(params.strokeStyle, true); }
                if (params.lineWidth) p.strokeweight = params.lineWidth + "px"; if (params.fillStyle) { p.filled = "true"; p.fillcolor = params.fillStyle; }
                var xmin = Math.min(d.hxy.x, d.tail[0].x, d.tail[1].x, d.cxy.x), ymin = Math.min(d.hxy.y, d.tail[0].y, d.tail[1].y, d.cxy.y), xmax = Math.max(d.hxy.x, d.tail[0].x, d.tail[1].x, d.cxy.x), ymax = Math.max(d.hxy.y, d.tail[0].y, d.tail[1].y, d.cxy.y), w = Math.abs(xmax - xmin), h = Math.abs(ymax - ymin), dim = [xmin, ymin, w, h]; p.path = getPath(d); p.coordsize = (connector.w * scale) + "," + (connector.h * scale); dim[0] = connector.x; dim[1] = connector.y; dim[2] = connector.w; dim[3] = connector.h; if (self.canvas == null) { var overlayClass = connector._jsPlumb.overlayClass || ""; var clazz = originalArgs && (originalArgs.length == 1) ? (originalArgs[0].cssClass || "") : ""; p["class"] = clazz + " " + overlayClass; self.canvas = _node("shape", dim, p, connector.canvas.parentNode, connector._jsPlumb.instance, true); connector.appendDisplayElement(self.canvas, true); self.attachListeners(self.canvas, connector); self.attachListeners(self.canvas, self); }
                else { _pos(self.canvas, dim); _atts(self.canvas, p); } 
            } 
        }; this.reattachListeners = function () { if (this.canvas) this.reattachListenersForElement(self.canvas, this); }; this.cleanup = function () { if (this.canvas != null) this._jsPlumb.instance.removeElement(this.canvas); };
    }; jsPlumbUtil.extend(AbstractVmlArrowOverlay, [VmlComponent, jsPlumb.Overlays.AbstractOverlay], { setVisible: function (state) { this.canvas.style.display = state ? "block" : "none"; } }); jsPlumb.Overlays.vml.Arrow = function () { AbstractVmlArrowOverlay.apply(this, [jsPlumb.Overlays.Arrow, arguments]); }; jsPlumbUtil.extend(jsPlumb.Overlays.vml.Arrow, [jsPlumb.Overlays.Arrow, AbstractVmlArrowOverlay]); jsPlumb.Overlays.vml.PlainArrow = function () { AbstractVmlArrowOverlay.apply(this, [jsPlumb.Overlays.PlainArrow, arguments]); }; jsPlumbUtil.extend(jsPlumb.Overlays.vml.PlainArrow, [jsPlumb.Overlays.PlainArrow, AbstractVmlArrowOverlay]); jsPlumb.Overlays.vml.Diamond = function () { AbstractVmlArrowOverlay.apply(this, [jsPlumb.Overlays.Diamond, arguments]); }; jsPlumbUtil.extend(jsPlumb.Overlays.vml.Diamond, [jsPlumb.Overlays.Diamond, AbstractVmlArrowOverlay]);
})(); ; (function ($) {
    "use strict"; var _getElementObject = function (el) { return typeof (el) == "string" ? $("#" + el) : $(el); }; $.extend(jsPlumbInstance.prototype, { getDOMElement: function (el) { if (el == null) return null; if (typeof (el) == "string") return document.getElementById(el); else if (el.context || el.length != null) return el[0]; else return el; }, getElementObject: _getElementObject, removeElement: function (element) { _getElementObject(element).remove(); }, doAnimate: function (el, properties, options) { el.animate(properties, options); }, getSelector: function (context, spec) {
        if (arguments.length == 2)
            return _getElementObject(context).find(spec); else
            return $(context);
    }, destroyDraggable: function (el) {
        if ($(el).data("draggable"))
            $(el).draggable("destroy");
    }, destroyDroppable: function (el) {
        if ($(el).data("droppable"))
            $(el).droppable("destroy");
    }, initDraggable: function (el, options, isPlumbedComponent) {
        options = options || {}; el = $(el); options.start = jsPlumbUtil.wrap(options.start, function () { $("body").addClass(this.dragSelectClass); }, false); options.stop = jsPlumbUtil.wrap(options.stop, function () { $("body").removeClass(this.dragSelectClass); }); if (!options.doNotRemoveHelper)
            options.helper = null; if (isPlumbedComponent)
            options.scope = options.scope || jsPlumb.Defaults.Scope; el.draggable(options);
    }, initDroppable: function (el, options) { options.scope = options.scope || jsPlumb.Defaults.Scope; $(el).droppable(options); }, isAlreadyDraggable: function (el) { return $(el).hasClass("ui-draggable"); }, isDragSupported: function (el, options) { return $(el).draggable; }, isDropSupported: function (el, options) { return $(el).droppable; }, getDragObject: function (eventArgs) { return eventArgs[1].helper || eventArgs[1].draggable; }, getDragScope: function (el) { return $(el).draggable("option", "scope"); }, getDropEvent: function (args) { return args[0]; }, getDropScope: function (el) { return $(el).droppable("option", "scope"); }, getUIPosition: function (eventArgs, zoom, dontAdjustHelper) {
        var ret; zoom = zoom || 1; if (eventArgs.length == 1) { ret = { left: eventArgs[0].pageX, top: eventArgs[0].pageY }; }
        else { var ui = eventArgs[1], _offset = ui.position; ret = _offset || ui.absolutePosition; if (!dontAdjustHelper) { ui.position.left /= zoom; ui.position.top /= zoom; } }
        return { left: ret.left, top: ret.top };
    }, isDragFilterSupported: function () { return true; }, setDragFilter: function (el, filter) {
        if (jsPlumb.isAlreadyDraggable(el))
            $(el).draggable("option", "cancel", filter);
    }, setElementDraggable: function (el, draggable) { $(el).draggable("option", "disabled", !draggable); }, setDragScope: function (el, scope) { $(el).draggable("option", "scope", scope); }, dragEvents: { 'start': 'start', 'stop': 'stop', 'drag': 'drag', 'step': 'step', 'over': 'over', 'out': 'out', 'drop': 'drop', 'complete': 'complete' }, animEvents: { 'step': "step", 'complete': 'complete' }, trigger: function (el, event, originalEvent) { var h = jQuery._data(_getElementObject(el)[0], "handle"); h(originalEvent); }, getOriginalEvent: function (e) { return e.originalEvent; }, on: function (el, event, callback) { el = _getElementObject(el); var a = []; a.push.apply(a, arguments); el.on.apply(el, a.slice(1)); }, off: function (el, event, callback) { el = _getElementObject(el); var a = []; a.push.apply(a, arguments); el.off.apply(el, a.slice(1)); } 
    }); $(document).ready(jsPlumb.init);
})(jQuery);
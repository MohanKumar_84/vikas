﻿var mfIdeClasses = "mfIdeClasses"; function ControlNew(options) { if (!options) options = {}; this.location = options.loc; }
function Control(options) { if (!options) options = {}; this.location = options.location; this.legend = options.legend; this.userDefinedName = options.userDefinedName; this.id = options.id; this.label = options.label; this.conditionalDisplay = options.condDisplay; this.conditionalControl = options.condCntrl; this.imageHeight = options.imageHeight; this.imageWidth = options.imageWidth; this.txtRowsCount = options.txtRowsCount; this.dfltValue = options.dfltValue; this.marginLeft = options.marginLeft; this.regularExp = options.regularExp; this.widthInPercent = options.widthInPercent; this.minWidth = options.minWidth; this.length = options.length; this.maxLength = options.maxLength; this.required = options.required; this.bindType = options.bindType; this.rowClickable = options.rowClickable; this.rowId = options.rowId; this.title = options.title; this.titlePosition = options.titlePosition; this.displayText = options.displayText; this.displayVal = options.displayVal; this.buttonType = options.buttonType; this.nextPage = options.nextPage; this.listCntrlType = options.listCntrlType; this.headerTxt = options.headerTxt; this.footerTxt = options.footerTxt; this.datePickType = options.datePickType; this.fitToWidth = options.fitToWidth; this.useOriginalSize = options.useOriginalSize; this.imageSource = this.imageSource; this.imageBindType = this.imageBindType; this.imageFixedType = this.imageFixedType; this.imageDataBindType = this.imageDataBindType; this.source = options.source; this.validationType = options.validationType; this.bindOptions = options.bindOptions; this.showLegend = options.showLegend; this.labelThreshold = options.labelThreshold; this.barGraphType = options.barGraphType; this.barGraphPadding = options.barGraphPadding; this.barGraphMargin = options.barGraphMargin; this.barGraphDir = options.barGraphDir; this.graphShowTicks = options.graphShowTicks; this.xAxisLabel = options.xAxisLabel; this.yAxisLabel = options.yAxisLabel; this.databindObjs = options.databindObjs; this.rowIdMappingCol = options.rowIdMappingCol; this.imageMappingCol = options.imageMappingCol; this.titleMappingCol = options.titleMappingCol; this.subTitle = options.subTitle; this.infoTxt = options.infoTxt; this.additionalInfoTxt = options.additionalInfoTxt; this.countBubble = options.countBubble; this.dataInsetAttr = options.dataInsetAttr; this.dataGroupingAllowed = options.dataGroupingAllowed; this.countBubbleShow = options.countBubbleShow; this.actionBtnAllowed = options.actionBtnAllowed; this.actionButton1 = options.actionButton1; this.actionButton2 = options.actionButton2; this.actionButton3 = options.actionButton3; this.minVal = options.minVal; this.maxVal = options.maxVal; this.incrementOnScroll = options.incrementOnScroll; this.promptText = options.promptText; this.promptVal = options.promptVal; this.type = options.type; this.manualEntry = options.manualEntry; this.noOfSeries = options.noOfSeries; this.series2MappingColVal = options.series2MappingColVal; this.series3MappingColVal = options.series3MappingColVal; this.series4MappingColVal = options.series4MappingColVal; this.series5MappingCol = options.series5MappingCol; this.series1Title = options.series1Title; this.series2Title = options.series2Title; this.series3Title = options.series3Title; this.series4Title = options.series4Title; this.series5Title = options.series5Title; this.series1Factor = options.series1Factor; this.series2Factor = options.series2Factor; this.series3Factor = options.series3Factor; this.series4Factor = options.series4Factor; this.series5Factor = options.series5Factor; this.noOfDecimalPoint = options.noOfDecimalPoint; this.valMask = options.valMask; this.errorMsg = options.errorMsg; this.addButtonText = options.addButtonText; this.submitButtonText = options.submitButtonText; this.editButtonText = options.editButtonText; this.addFormTitle = options.addFormTitle; this.editFormTitle = options.editFormTitle; this.viewFormTitle = options.viewFormTitle; this.fontStyle = options.fontStyle; this.fontSize = options.fontSize; this.fontAlign = options.fontAlign; this.minItems = options.minItems; this.maxItems = options.maxItems; this.confirmAddUpdate = options.confirmAddUpdate; this.editableListProps = options.editableListProps; this.propMapped2Title = options.propMapped2Title; this.titlePrefix = options.titlePrefix; this.titleSuffix = options.titleSuffix; this.titleFactor = options.titleFactor; this.additionalTitleText = options.additionalTitleText; this.additionalTitlePrefix = options.additionalTitlePrefix; this.additionalTitleSuffix = options.additionalTitleSuffix; this.additionalTitleFactor = options.additionalTitleFactor; this.propMapped2SubTitle = options.propMapped2SubTitle; this.subTitlePrefix = options.subTitlePrefix; this.subTitleSuffix = options.subTitleSuffix; this.subTitleFactor = options.subTitleFactor; this.additionalSubTtl = options.additionalSubTtl; this.additionalSubTtlPrfx = options.additionalSubTtlPrfx; this.additionalSubTtlSuffx = options.additionalSubTtlSuffx; this.additionalSubTtlFactor = options.additionalSubTtlFactor; this.propMapped2Value = options.propMapped2Value; this.propMapped2Text = options.propMapped2Text; this.labelText = options.labelText; this.multiplicationFactor = options.multiplicationFactor; this.text = options.text; this.enablePaging = options.enablePaging; this.pagingRecordCount = options.pagingRecordCount; this.validationRange = options.validationRange; this.tableColumns = options.tableColumns; this.addEditDelPermissions = options.addEditDelPermissions; this.onChange = options.onChange; this.titleAlign = options.titleAlign; this.wrap = options.wrap; this.help = options.help; this.helpTxt = options.helpTxt; this.titleAlign = options.titleAlign; this.fetchLocation = options.fetchLocation; this.refreshLink = options.refreshLink; this.autoRefreshTime = options.autoRefreshTime; this.valueAs = options.valueAs; this.dataLblThreshold = options.dataLblThreshold; this.condDisplayCntrlProps = options.condDisplayCntrlProps; this.placeholder = options.placeholder; this.dataTitle = options.dataTitle; this.smallText = options.smallText; this.countField = options.countField; this.align = options.align; this.emptyListMsg = options.emptyListMsg; this.additionalTitle = options.additionalTitle; this.isDeleted = options.isDeleted; this.isModified = options.isModified; this.xRef = options.xRef; this.switchText = this.switchText; this.defaultState = this.defaultState; this.refreshControls = this.refreshControls; this.barcodeType = this.barcodeType; this.isHidden = this.isHidden; this.allowUsages = this.allowUsages; }
Control.prototype.propSheetHtmlMapping = PROP_JSON_HTML_MAP.control; Control.prototype.fnAddDatabindObjs = function (databindObjs) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) { this.databindObjs = []; }
    if ($.isArray(databindObjs)) { var self = this; $.each(databindObjs, function (index, value) { if (value && value instanceof DatabindingObj) { self.databindObjs.push(value); } }); } 
}
Control.prototype.fnAddDatabindObj = function (databindObj) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) { this.databindObjs = []; }
    if (databindObj && databindObj instanceof DatabindingObj) { this.databindObjs.push(databindObj); } 
}
Control.prototype.fnDeleteDatabindObj = function (name) { var aryNewDatabindObjs = $.grep(this.databindObjs, function (databindObj) { if (databindObj instanceof DatabindingObj) { return databindObj.name !== name; } }); this.databindObjs = aryNewDatabindObjs; }
Control.prototype.fnClearAllDatabindObjs = function (databindObj) { this.databindObjs = []; }
Control.prototype.fnAddBindOption = function (bindOption) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) { this.bindOptions = []; }
    if (bindOption && bindOption instanceof ManualBindOptions) { this.bindOptions.push(bindOption); } 
}
Control.prototype.fnAddBindOptions = function (bindOptions) {
    if (!this.bindOptions || !$.isArray(this.bindOptions)) { this.bindOptions = []; }
    if ($.isArray(bindOptions)) { var self = this; $.each(bindOptions, function (index, value) { if (value && value instanceof ManualBindOptions) { self.databindObjs.push(value); } }); } 
}
Control.prototype.fnAddListViewButton = function (index, lstViewBtn) { this.fnDeleteListViewButton(index); if (index && lstViewBtn && lstViewBtn instanceof ListViewButton) { switch (index) { case "1": this.button1 = lstViewBtn; break; case "2": this.button2 = lstViewBtn; break; case "3": this.button3 = lstViewBtn; break; } } }
Control.prototype.fnDeleteListViewButton = function (index) { if (index) { switch (index) { case "1": this.button1 = {}; break; case "2": this.button2 = {}; break; case "3": this.button3 = {}; break; } } }
Control.prototype.fnEditListViewButton = function (index, lstViewBtn) { this.fnAddListViewButton(index, lstViewBtn); }
Control.prototype.fnAddEditableListProps = function (editListProps) {
    if (!this.editableListProps || !$.isArray(this.editableListProps)) { this.editableListProps = []; }
    if ($.isArray(editListProps)) { var self = this; $.each(editListProps, function (index, value) { if (value && value instanceof EditableListItem) { self.editableListProps.push(value); } }); } 
}
Control.prototype.fnEditEditableListProps = function (editListProp) { this.fnDeleteEditableListProps(); this.fnAddEditableListProps(editListProp); }
Control.prototype.fnClearAllEditableListProps = function () { this.editListProps = []; }
Control.prototype.fnAddTableCol = function (tableCol) {
    if (!this.tableColumns || !$.isArray(this.tableColumns)) { this.tableColumns = []; }
    if (tableCol && tableCol instanceof TableColumn) { this.tableColumns.push(tableCol); } 
}
Control.prototype.fnAddTableCols = function (tableCols) {
    this.fnDeleteAllTableCols(); if (!this.tableColumns || !$.isArray(this.tableColumns)) { this.tableColumns = []; }
    $.each(tableCols, function (index, value) { if (value && value instanceof TableColumn) { this.tableColumns.push(tableCol); } });
}
Control.prototype.fnDeleteAllTableCols = function () { this.tableColumns = []; }
Control.prototype.fnAddOnChange = function (changeVal) {
    if (!this.onChange || !$.isArray(this.onChange)) { this.onChange = []; }
    if (change) { this.onChange.push(changeVal); } 
}
Control.prototype.fnGetJsonForSavingByCntrlType = function () { }
function DatabindingObj(id, cmdParams, index, bindObjType, name, dsName, tempUiIndex, usrName, pwd) {
    this.index = index; this.bindObjType = bindObjType; this.name = name; this.dsName = dsName; this.id = id; this.cmdParams = cmdParams; this.usrName = usrName ? usrName : ""; this.pwd = pwd ? pwd : ""; if (tempUiIndex) { this.tempUiIndex = tempUiIndex; }
    else { this.tempUiIndex = index; } 
}
DatabindingObj.prototype.fillProperties = function (json) {
    if (json) {
        var jsonObj = MF_HELPERS.getJsonObject(json); var i = 0; if (jsonObj) {
            this.id = jsonObj.cmd; this.bindObjType = jsonObj.cmdtyp; this.name = jsonObj.cname; this.index = jsonObj.idx; this.tempUiIndex = jsonObj.idx; this.cmdParams = []; if (jsonObj.lp && $.isArray(jsonObj.lp) && jsonObj.lp.length > 0) { for (i = 0; i <= jsonObj.lp.length - 1; i++) { var objDatabindObjParams = new DatabindingObjParams(); objDatabindObjParams.fillProperties(jsonObj.lp[i]); this.fnAddCmdParams(objDatabindObjParams); } }
            this.dsName = jsonObj.ctrlid;
        } 
    } 
}
DatabindingObj.prototype.fnResetObjectDetails = function (json) { var cmdParams = this.cmdParams; this.cmdParams = []; var i = 0; if (cmdParams != undefined || cmdParams != null) { for (i = 0; i <= cmdParams.length - 1; i++) { var cmdParam = cmdParams[i]; var objDatabindObjParams = new DatabindingObjParams(cmdParam.id, cmdParam.name, cmdParam.type, cmdParam.val); this.fnAddCmdParams(objDatabindObjParams); } } }
DatabindingObj.prototype.fnAddCmdParams = function (databindObjParam) {
    if (!this.cmdParams || !$.isArray(this.cmdParams)) { this.cmdParams = []; }
    if (databindObjParam && databindObjParam instanceof DatabindingObjParams) { this.cmdParams.push(databindObjParam); } 
}
DatabindingObj.prototype.fnRemoveCmdParams = function () { var params = this.cmdParams; var _this = this; if (params.length > 0) { $.each(params, function () { _this.cmdParams.pop(this); }); } }
DatabindingObj.prototype.fnGetCommandName = function () { return this.name; }
DatabindingObj.prototype.fnGetBindTypeObject = function () { var iBindType = this.bindObjType; return MF_HELPERS.getCommandTypeByValue(iBindType); }
DatabindingObj.prototype.fnGetBindTypeValue = function () { return this.bindObjType; }
DatabindingObj.prototype.fnGetCmdParams = function () { return this.cmdParams; }
DatabindingObj.prototype.fnRenameViewCntrlInCmdParam = function (viewName, oldCntrlName, newCntrlName) { if (!viewName || !oldCntrlName || !newCntrlName) return; var aryCmdParams = this.fnGetCmdParams(); if (aryCmdParams && $.isArray(aryCmdParams) && aryCmdParams.length > 0) { for (var i = 0; i <= aryCmdParams.length - 1; i++) { var objCmdParam = aryCmdParams[i]; objCmdParam.fnRenameViewCntrlInVal(viewName, oldCntrlName, newCntrlName); } } }
function DatabindingObjParams(id, name, type, val) { this.id = id; this.name = name; this.type = type; this.val = val; }
DatabindingObjParams.prototype.fillProperties = function (json) { if (json) { var jsonObj = MF_HELPERS.getJsonObject(json); if (jsonObj) { this.name = jsonObj.para; this.type = ""; this.val = jsonObj.val; } } }
DatabindingObjParams.prototype.fnRenameViewCntrlInVal = function (viewName, oldName, newName) { if (!viewName || !oldName || !newName) return; var strOldValue = MF_HELPERS.intlsenseHelper.getTextForObjectProperty(viewName + '.' + oldName) + "."; var strNewValue = MF_HELPERS.intlsenseHelper.getTextForObjectProperty(viewName + '.' + newName) + "."; if (this.val && this.val.startsWith(strOldValue)) { this.val = this.val.replace(strOldValue, strNewValue); } }
function ListViewButton(text, cmdId, cmdType, refresh, color, cmdParams) { this.text = text; this.cmdId = cmdId; this.cmdType = cmdType; this.refresh = refresh; this.color = color; this.cmdParams = cmdParams; }
function EditableListItem(name, type, col, unique) { this.name = name; this.type = type; this.columns = col; this.unique = unique; }
function ConditionalLogic(column, operator, val) { this.column = column; this.operator = operator; this.val = val; }
function RowPanel(id, type, colPanels) {
    this.id = id; this.type = type; this.colmnPanels = colPanels; RowPanel.prototype.fnAddColumnPanel = function (colPanel) {
        if (!this.colmnPanels || !$.isArray(this.colmnPanels)) { this.colmnPanels = []; }
        if (colPanel && colPanel instanceof ColumnPanel) { this.colmnPanels.push(colPanel); } 
    }
    RowPanel.prototype.fnGetColumnPanel = function (colPanelId) {
        var objColPanel = null; if (this.colmnPanels != null || this.colmnPanels != undefined) {
            for (var col = 0; col <= this.colmnPanels.length - 1; col++) {
                if (this.colmnPanels[col].id == colPanelId)
                    objColPanel = this.colmnPanels[col];
            } 
        }
        return objColPanel;
    } 
}
function ColumnPanel(index, id, name, display, conditions, controls) {
    this.index = index; this.id = id; this.name = name; this.display = display; this.conditions = conditions; this.controls = controls; ColumnPanel.prototype.fnAddControl = function (control) {
        if (!this.controls || !$.isArray(this.controls)) { this.controls = []; }
        if (control && control instanceof Control) { this.controls.push(control); } 
    }
    ColumnPanel.prototype.fnGetControl = function (controlId) {
        var objControl = null; if (this.controls != null || this.controls != undefined) {
            for (var cntrl = 0; cntrl <= this.controls.length - 1; cntrl++) {
                if (this.controls[cntrl].id == controlId)
                    objControl = this.controls[cntrl];
            } 
        }
        return objControl;
    } 
}
﻿function showModalPopUp(divToOpen, popUpTitle, popUpWidth, resizable, params) {
    //{minHeight}

    var objDialogOptions = {};
    objDialogOptions.title = popUpTitle;
    objDialogOptions.width = popUpWidth ? popUpWidth : 350;
    objDialogOptions.modal = true;
    if (!mfUtil.isNullOrUndefined(params)) {
        if (params.minHeight) {
            objDialogOptions.minHeight = params.minHeight;
        }
    }

    if (!resizable) {
        objDialogOptions.resizable = false;
    }
    $('#' + divToOpen + '').aspdialog(objDialogOptions);
    if ($('#' + divToOpen)) {
        ($('#' + divToOpen).parent()).find('.ui-dialog-titlebar > a.ui-dialog-titlebar-close').show();
    }
}

var enterpriseWiFi = (function () {
    var jqDataKey = "jqDataKey";
    var _tableHelper = (function () {
        function _getInfoTable() {
            return $('#tblEnterpriseWiFiInfo');
        }

        function _getHtmlOfNewWifiAdditionRow() {
            var strHtml = "";
            strHtml += '<tr class="AdditionRow">';
            strHtml += '<td><input type="text" class="WiFiName w_90"></input></td>';
            strHtml += '<td style="padding-top:5px;"><span class="addImg16x16 AddNewWiFi" style="float:none;margin:auto;display:inline-block;"></span></td>';
            strHtml += '</tr>';
            return strHtml;
        }
        /**
        * Gets the html for the WifiInfo provided. Thorows Argmimetn null exception if WifiInfo is Null
        * @method _getHtmlOfWiFiInfoRow
        * @param {Object {id:string,name:string}} wifiInfo
        * @return {String} Returns html as string
        */
        function _getHtmlOfWiFiInfoRow(wifiInfo) {
            var strHtml = "";
            if (mfUtil.isNullOrUndefined(wifiInfo)) {
                throw "Arguments Null Exception";
            }
            strHtml += '<tr style="cursor:pointer">';
            strHtml += '<td>';
            strHtml += '<span class="WifiName">' + wifiInfo.Name + '</span>';
            strHtml += '<span class="WifiId" style="display:none;">' + wifiInfo.Id + '</span>';
            strHtml += '</td>';
            strHtml += '<td style="padding-top:5px;">';
            strHtml += '<span class="RemoveWifi crossImg16x16" style="float:none;margin:auto;display:inline-block;"></span>';
            strHtml += '</td>';
            strHtml += '</tr>';
            return strHtml;
        }
        function _setEventsOfTable() {
            var $table = _getInfoTable();
            $table.find('tbody tr .RemoveWifi').on('click', function (evnt) {
                var $tr = $([]),
                    $self = $(this),
                    strWiFiId = "";

                $tr = $self.closest('tr');
                strWiFiId = $tr.find('.WifiId').text();
                var blnConfirm = confirm("If the WiFiSSID is used in any App then the app will not work in this WiFi network.");
                if (blnConfirm) {
                    processPostbackByHtmlCntrl.postBack(
                        postBackByHtmlProcess.DeleteWiFiSSID,
                        [strWiFiId],
                        ""
                    );
                }
                //showDialogImage(DialogType.Warning);          
                evnt.stopPropagation();
            });
            $table.find('tbody tr.AdditionRow .AddNewWiFi').on('click', function (evnt) {
                var $tr = $([]),
                    $self = $(this),
                    strWiFiName = "";

                $tr = $self.closest('tr');
                strWiFiName = $tr.find('.WiFiName').val();
                if (mfUtil.isEmptyString(strWiFiName)) {
                    showMessage("Please enter a name for WiFiSSID", DialogType.Error, DialogType.Error);
                }
                else {
                    processPostbackByHtmlCntrl.postBack(postBackByHtmlProcess.AddNewWiFiSSID,
                        [strWiFiName], "");
                }
                evnt.stopPropagation();
            });
            $table.find('tbody tr:not(".AdditionRow")').on('click', function (evnt) {
                var $self = $(this);
                $('#divUpdateWiFiSSID').data(jqDataKey, { id: $self.find('.WifiId').text() });
                $('#txtUpdateWiFiSSIDName').val($self.find('.WifiName').text());
                showModalPopUp('divUpdateWiFiSSID', "Update Name", "400", false, { minHeight: 100 });
                evnt.stopPropagation();
            });
        }
        /**
        * Sets the UI for Edit and Addition Of New Wifi Info 
        *
        * @method _setUI
        * @param {Array} wifiInfos provide information of wifi already added
        * wifiInfo = {
        id : string
        name : string
        }
        * @return undefined
        */
        function _setUI(wifiInfos/*array of wifi Info*/) {
            var strHtml = "",
                i = 0,
                $table = _getInfoTable();

            $table.find('tbody').empty();
            if ($.isArray(wifiInfos) && wifiInfos.length > 0) {
                for (i = 0; i <= wifiInfos.length - 1; i++) {
                    strHtml += _getHtmlOfWiFiInfoRow(wifiInfos[i]);
                }
            }
            strHtml += _getHtmlOfNewWifiAdditionRow();
            $table.find('tbody').html(strHtml);
            _setEventsOfTable();
        }
        return {
            setUI: function (wifiInfos) {
                _setUI(wifiInfos);
            }
        };
    })();

    return {

        setUI: function () {
            var $hidWiFiInfos = $('input[id$="hidWiFiInfos"]'),
                strWifiInfos = $hidWiFiInfos.val();
            if (mfUtil.isNullOrUndefined(strWifiInfos) || mfUtil.isEmptyString(strWifiInfos)) {
                _tableHelper.setUI([]);
            }
            else {
                _tableHelper.setUI($.parseJSON(strWifiInfos));
            }
        },
        updateWiFiName: function () {

            var id = $('#divUpdateWiFiSSID').data(jqDataKey).id,
                name = $('#txtUpdateWiFiSSIDName').val();
            if (mfUtil.isEmptyString(name)) {
                showMessage("Please enter a name for WiFiSSID", DialogType.Error, DialogType.Error);
                return false;
            }
            else {
                var dataObject = {
                    processFor: postBackByHtmlProcess.UpdateWiFiSSID,
                    data: [id, name]
                };
                $('[id$="hidDtlsForPostBackByHtmlCntrl"]').val(JSON.stringify(dataObject));
                return true;
            }
        },
        closeUpdateWiFiName: function () {
            $('#divUpdateWiFiSSID').removeData(jqDataKey);
            $('#txtUpdateWiFiSSIDName').val("");
            closeModalPopUp('divUpdateWiFiSSID');
        }
    };
})();


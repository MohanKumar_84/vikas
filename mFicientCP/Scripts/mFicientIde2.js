﻿
Number.prototype.leftPad = function (len) {
    var l = this.toString().length,
    d = len - l;
    return new Array(d + 1).join('0') + this.toString();
};

var mFicientIde = {};
mFicientIde.MfApp = {};
mFicientIde.MfApp.appsContainer = null;
mFicientIde.MfApp.form ={};
mFicientIde.MF_IDE_CONSTANTS = {
    DataBindImgType: {
        BinaryPng: "0",
        BinaryJpg: "1",
        PublicUrl: "2",
        LocalUrl: "3"
    },
    ImgBindType: {
        Fixed: "0",
        DataBinding: "1"
    },
    DesignViewDefaultImgUrl: "//enterprise.mficient.com/images/DefaultImage .png",
    EnterpriseIconPrefix: "//enterprise.mficient.com/images/icon/",
    enterpriseImagesPath:"//enterprise.mficient.com/images/",
    enterpriseJqueryImagesPath:"//enterprise.mficient.com/jqueryMobileIcons/",
    DesignViewDefaultImgDim: "48",
    CntrlDataBindingOFDBOptVal: "6",
    CONTROL_WITH_OFFLINE_TBL_BIND: {
        Dropdown: 1,
        List: 2,
        EditableList: 3,
        Pie: 4,
        Line: 5,
        Bargraph: 6,
        Table: 7
    },
    MF_CONTROL_PREFIX_TEXT: {
        Dropdown: "DDL",
        List: "RPT",
        EditableList: "ELT",
        Pie: "PIE",
        Line: "LINE",
        Bargraph: "BAR",
        Table: "TBL"
    },
    MF_CONTROL_VERSION: {
        Textbox : "1.0",
        Label: "2.0",
        Image : "1.0",
        DatetimePicker : "1.0",
        Separator : "1.0",
        Slider : "1.0",
        Toggle : "1.0",
        Checkbox : "1.0",
        RadioButton : "1.0",
        Dropdown : "1.0",
        Location : "2.0",
        BarcodeReader : "1.0",
        Signature : "1.0",
        List: "2.0",
        EditableList: "1.0",
        Table: "2.0",
        Pie: "1.0",
        Line: "1.0",
        Bargraph: "1.0",
        CylinderGuage : "1.0",
        AngularGuage : "1.0",
        DataPoint : "1.0",
        Picture : "1.0",
        Spacer : "1.0",
        Button : "2.0",
        CustomControl :"2.0"
    },
    MF_EVENT_LIST_DISPLAY_FORMAT:{
        Date : "0",
        Date_Time : "1",
        Month_Year : "2",
        Day_Month : "3"
    },
    MF_LIST_CONTROL :{
        Simple_list : "0",
        Simple_list_with_transition : "1",

        Basic_list : "2",
        Basic_list_with_count_bub : "3",
        Basic_list_trans : "4",
        Basic_list_trans_with_count_bub : "5",
        Basic_list_actnbtn : "6",
        Basic_list_actnbtn_with_count_bub : "7",

        Numbered_list : "8",
        Numbered_list_with_count_bub : "9",
        Numbered_list_trans : "10",
        Numbered_list_trans_with_count_bub : "11",
        Numbered_list_actnbtn : "12",
        Numbered_list_actnbtn_with_count_bub : "13",

        Icon_list : "14",
        Icon_list_with_count_bub : "15",
        Icon_list_trans : "16",
        Icon_list_trans_with_count_bub : "17",
        Icon_list_actnbtn : "18",
        Icon_list_actnbtn_with_count_bub : "19",

        Basic_desc_list : "20",
        Basic_desc_list_with_count_bub : "21",
        Basic_desc_list_trans : "22",
        Basic_desc_list_trans_with_count_bub : "23",
        Basic_desc_list_actnbtn : "24",
        Basic_desc_list_actnbtn_with_count_bub : "25",

        Thumbnail_list : "26",
        Thumbnail_list_with_count_bub : "27",
        Thumbnail_list_trans : "28",
        Thumbnail_list_trans_with_count_bub : "29",
        Thumbnail_list_actnbtn : "30",
        Thumbnail_list_actnbtn_with_count_bub : "31",

        Image_list : "32",
        Image_list_trans : "33",

        Date_Event_list : "34",
        Date_Event_list_trans : "35",
        Date_Event_list_actnbtn : "36",

        Date_Time_Event_list : "37",
        Date_Time_Event_list_trans : "38",
        Date_Time_Event_list_actnbtn : "39",

        Mon_Year_Event_list : "40",
        Mon_Year_Event_list_trans : "41",
        Mon_Year_Event_list_actnbtn : "42",

        Day_Mon_Event_list : "43",
        Day_Mon_Event_list_trans : "44",
        Day_Mon_Event_list_actnbtn : "45",

        Custom_list:"46"
    },
    DataBindCntrlOFDBCmdText: "OfflineDBCmd",
    OFDBBindingCntrlForIntlsense: {
        prefixes: ["DDL", "LOC", "TGL", "TXT", "RDB", "CHK", "BCODE", "SLD", "DTP", "HDF"],
        Dropdown: {
            prefix: "DDL",
            prefixWithUnderscore: "DDL_",
            propForIntlSense: [{ "prop": "Text", "val": "Text" }, { "prop": "Value", "val": "Value"}]
        },
        Location: {
            prefix: "LOC",
            prefixWithUnderscore: "LOC_",
            propForIntlSense: [{ "prop": "Latitude", "val": "Latitude" }, { "prop": "Longitude", "val": "Longitude"}, { "prop": "GeoDistance", "val": "GeoDistance"}]
        },
        Toggle: {
            prefix: "TGL",
            prefixWithUnderscore: "TGL_",
            propForIntlSense: [{ "prop": "Value", "val": "Value"}]
        },
        Text: {
            prefix: "TXT",
            prefixWithUnderscore: "TXT_",
            propForIntlSense: [{ "prop": "Value", "val": "Value"}]
        },
        Radio: {
            prefix: "RDB",
            prefixWithUnderscore: "RDB_",
            propForIntlSense: [{ "prop": "Text", "val": "Text" }, { "prop": "Value", "val": "Value"}]
        },
        Checkbox: {
            prefix: "CHK",
            prefixWithUnderscore: "CHK_",
            propForIntlSense: [{ "prop": "Value", "val": "Value"}]
        },
        BarCode: {
            prefix: "BCODE",
            prefixWithUnderscore: "BCODE_",
            propForIntlSense: [{ "prop": "Value", "val": "Value"}]
        },
        Slider: {
            prefix: "SLD",
            prefixWithUnderscore: "SLD_",
            propForIntlSense: [{ "prop": "Value", "val": "Value"}]
        },
        Datetime: {
            prefix: "DTP",
            prefixWithUnderscore: "DTP_",
            propForIntlSense: [{ "prop": "Value", "val": "Value"}]
        },
        Hidden: {
            prefix: "HDF",
            prefixWithUnderscore: "HDF_",
            propForIntlSense: [{ "prop": "Value", "val": "Value"}]
        }
    },
    //CntlOFDBBindingStringOperator: [{"text" : "is", "val" : "1"}, {"text" : "is not", "val" : "2"}, {"text" : "contains", "val" : "3"}, {"text" : "starts with", "val" : "4"}, {"text" : "ends with", "val" : "5"}],
    //CntlOFDBBindingNumberOperator: [{"text" : "greater than", "val" : "6"}, {"text" : "less than", "val" : "7"}],
    ConditionalLogicOperators:[
       {"text" : "Equal (Case)", "val" : "1"}, 
       {"text" : "Equal (Ignore Case)", "val" : "2"},
       {"text" : "Equals (Numeric)", "val" : "11"},
       {"text" : "Starts (Case)", "val" : "3"},
       {"text" : "Starts (Ignore Case)", "val" : "4"},
       {"text" : "Ends (Case)", "val" : "5"},
       {"text" : "Ends (Ignore Case)", "val" : "6"},
       {"text" : "Contains (Case)", "val" : "7"},
       {"text" : "Contains (Ignore Case)", "val" : "8"},
       {"text" : "Not Contains (Case)", "val" : "9"},
       {"text" : "Not Contains (Ignore Case)", "val" : "10"},
       {"text" : "Not Equal With Case", "val" : "19"},
       {"text" : "Not Equal Ignore Case", "val" : "20"},
       {"text" : "Greater Than (Numeric)", "val" : "12"},
       {"text" : "Greater Than Equals To (Numeric)", "val" : "13"},
       {"text" : "Less Than (Numeric)", "val" : "14"},
       {"text" : "Less Than Equals To (Numeric)", "val" : "15"},
       {"text" : "Range (including End points)", "val" : "16"},
       {"text" : "Range (excluding End points)", "val" : "17"},
       {"text" : "Not Equals (Numeric)", "val" : "18"},
    ],
    OfflineDbColType: {
        String: "String",
        Number: "Number"
    },
    getConditionalOperatorJson: function () {
         var aryCondOperators = [];
         aryCondOperators = this.ConditionalLogicOperators;
        //  var aryConditionalOperatorObj = $.merge([], this.CntlOFDBBindingNumberOperator);
        //  $.merge(aryConditionalOperatorObj, this.CntlOFDBBindingStringOperator);
        //  for (var i = 0; i <= aryConditionalOperatorObj.length - 1; i++) {
        //      var objCondOperatorAsDdlOption = new Object();
        //      objCondOperatorAsDdlOption.text = aryConditionalOperatorObj[i].text;
        //      objCondOperatorAsDdlOption.val = aryConditionalOperatorObj[i].val;
        //      aryCondOperator.push(objCondOperatorAsDdlOption);
        //  }
         return JSON.stringify(aryCondOperators);
    },
    //Tanika
    getIntellisenseControlJson: function (json) {
        var aryIntellisenseControl = [];
        var objControlInellisenseJson = {};
        $.each(jQuery.parseJSON(json), function () {
            objControlInellisenseJson = {};
            objControlInellisenseJson["prop"] = this["cntrlnm"];
            objControlInellisenseJson["val"] = this["cntrlnm"];
            objControlInellisenseJson["category"] = "";
            switch (this['type'].idPrefix) {
                case "DDL":
                    objControlInellisenseJson["ip"] = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.propForIntlSense;
                    break;
                case "LOC":
                    objControlInellisenseJson["ip"] = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION.propForIntlSense;
                    break;
                case "TGL":
                    objControlInellisenseJson["ip"] = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE.propForIntlSense;
                    break;
                case "TXT":
                    objControlInellisenseJson["ip"] = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.propForIntlSense;
                    break;
                case "RDB":
                    objControlInellisenseJson["ip"] = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.propForIntlSense;
                    break;
                case "CHK":
                    objControlInellisenseJson["ip"] = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.propForIntlSense;
                    break;
                case "BCODE":
                    objControlInellisenseJson["ip"] = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARCODE.propForIntlSense;
                    break;
                case "SLD":
                    objControlInellisenseJson["ip"] = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER.propForIntlSense;
                    break;
                case "DTP":
                    objControlInellisenseJson["ip"] = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER.propForIntlSense;
                    break;
                case "HDF":
                    objControlInellisenseJson["ip"] = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN.propForIntlSense;
                    break;
            }
            aryIntellisenseControl.push(objControlInellisenseJson);
        });
        return JSON.stringify(aryIntellisenseControl);
    },
    /*Name of the data(Key for data) theat is attached to the div on which
    the conditional logix was applied.
    */
    CondLogicDataNameAttached: {
        "FinalJson": "FinalJson",
        "ValidateIntlsText": "ValidateIntlsText"
    },
    CntrlOFDBMapDataKey: "CntrlOFDBMap",
    CONTROLS: {
                  SECTION :
                  {
                      "idPrefix" : "SEC",
                      "propPluginPrefix" : "SECTION",
                      "UIName" : "Section",
                      "type" : "",
                      "propForIntlSense" : []
                  },
                  LABEL :
                  {
                      "idPrefix" : "LBL",
                      "propPluginPrefix" : "LABEL",
                      "UIName" : "Label",
                      "type" : "Label",
                      "propForIntlSense" : []
                  },
                  TEXTBOX :
                  {
                      "idPrefix" : "TXT",
                      "propPluginPrefix" : "TEXTBOX",
                      "UIName" : "Textbox",
                      "type" : "changeText",
                      "propForIntlSense" : [
                          {
                              "prop" : "Value",
                              "val" : "Value",
                              "category" : "Select Property"
                          }
                      ]
                  },
                  DROPDOWN :
                  {
                      "idPrefix" : "DDL",
                      "propPluginPrefix" : "DROPDOWN",
                      "UIName" : "Dropdown",
                      "type" : "Dropdown",
                      "propForIntlSense" : [
                          {
                              "prop" : "Text",
                              "val" : "Text",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "Value",
                              "val" : "Value",
                              "category" : "Select Property"
                          }
                      ]
                  },
                  RADIOBUTTON :
                  {
                      "idPrefix" : "RDB",
                      "propPluginPrefix" : "RADIOBUTTON",
                      "UIName" : "Radiobutton",
                      "type" : "RadioButton",
                      "propForIntlSense" : [
                          {
                              "prop" : "Text",
                              "val" : "Text",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "Value",
                              "val" : "Value",
                              "category" : "Select Property"
                          }
                      ]
                  },
                  CHECKBOX :
                  {
                      "idPrefix" : "CHK",
                      "propPluginPrefix" : "CHECKBOX",
                      "UIName" : "Checkbox",
                      "type" : "Checkbox",
                      "propForIntlSense" : [
                          {
                              "prop" : "Value",
                              "val" : "Value",
                              "category" : "Select Property"
                          }
                      ]
                  },
                  IMAGE :
                  {
                      "idPrefix" : "IMG",
                      "propPluginPrefix" : "IMAGE",
                      "UIName" : "Image",
                      "type" : "Image",
                      "propForIntlSense" : []
                  },
                  LIST :
                  {
                      "idPrefix" : "RPT",
                      "propPluginPrefix" : "LIST",
                      "UIName" : "List",
                      "type" : "Repeater",
                      "propForIntlSense" : [
                          {
                              "prop" : "Title",
                              "val" : "Title",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "RowId",
                              "val" : "RowId",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "SelectedRow",
                              "val" : "SelectedRow",
                              "category" : "Select Property"
                          }
                      ]
                  },
                  EDITABLE_LIST :
                  {
                      "idPrefix" : "ELT",
                      "propPluginPrefix" : "EDITABLE_LIST",
                      "UIName" : "Editable List",
                      "type" : "EditableList",
                      "propForIntlSense" : []
                  },
                  HIDDEN :
                  {
                      "idPrefix" : "HDF",
                      "propPluginPrefix" : "HIDDEN",
                      "UIName" : "Hidden Field",
                      "type" : "HiddenField",
                      "propForIntlSense" : [
                          {
                              "prop" : "Value",
                              "val" : "Value",
                              "category" : "Select Property"
                          }
                      ]
                  },
                  DATE_TIME_PICKER :
                  {
                      "idPrefix" : "DTP",
                      "propPluginPrefix" : "DATE_TIME_PICKER",
                      "UIName" : "Datetime Picker",
                      "type" : "DatetimePicker",
                      "propForIntlSense" : [
                          {
                              "prop" : "Day",
                              "val" : "Day",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "Month",
                              "val" : "Month",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "Year",
                              "val" : "Year",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "Hour",
                              "val" : "Hour",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "Minute",
                              "val" : "Minute",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "Second",
                              "val" : "Second",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "Format(\"dd-mm-yyyy\")",
                              "val" : "Format(\"dd-mm-yyyy\")",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "Ticks",
                              "val" : "Ticks",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "UnixFormat",
                              "val" : "UnixFormat",
                              "category" : "Select Property"
                          }
                      ]
                  },
                  TOGGLE :
                  {
                      "idPrefix" : "TGL",
                      "propPluginPrefix" : "TOGGLE",
                      "UIName" : "Toggle",
                      "type" : "Toggle",
                      "propForIntlSense" : [
                          {
                              "prop" : "Value",
                              "val" : "Value",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "Text",
                              "val" : "Text",
                              "category" : "Select Property"
                          }
                      ]
                  },
                  SLIDER :
                  {
                      "idPrefix" : "SLD",
                      "propPluginPrefix" : "SLIDER",
                      "UIName" : "Slider",
                      "type" : "Slider",
                      "propForIntlSense" : [
                          {
                              "prop" : "Value",
                              "val" : "Value",
                              "category" : "Select Property"
                          }
                      ]
                  },
                  SEPARATOR :
                  {
                      "idPrefix" : "SEP",
                      "propPluginPrefix" : "SEPARATOR",
                      "UIName" : "Separator",
                      "type" : "Separator",
                      "propForIntlSense" : []
                  },
                  BARCODE :
                  {
                      "idPrefix" : "BCODE",
                      "propPluginPrefix" : "BARCODE",
                      "UIName" : "BarCode",
                      "type" : "BarcodeReader",
                      "propForIntlSense" : [
                          {
                              "prop" : "Value",
                              "val" : "Value",
                              "category" : "Select Property"
                          }
                      ]
                  },
                  LOCATION :
                  {
                      "idPrefix" : "LOC",
                      "propPluginPrefix" : "LOCATION",
                      "UIName" : "Location",
                      "type" : "Location",
                      "propForIntlSense" : [
                          {
                              "prop" : "Latitude",
                              "val" : "Latitude",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "Longitude",
                              "val" : "Longitude",
                              "category" : "Select Property"
                          },
                          {
                               "prop" : "GeoDistance",
                               "val" : "GeoDistance",
                               "category" : "Select Property"
                          }
                      ]
                  },
                  PIECHART :
                  {
                      "idPrefix" : "PIE",
                      "propPluginPrefix" : "PIECHART",
                      "UIName" : "Pie Chart",
                      "type" : "PieChart",
                      "propForIntlSense" : []
                  },
                  BARGRAPH :
                  {
                      "idPrefix" : "BAR",
                      "propPluginPrefix" : "BARGRAPH",
                      "UIName" : "Bar Graph",
                      "type" : "BarGraph",
                      "propForIntlSense" : []
                  },
                  ANGULARGUAGE :
                  {
                      "idPrefix" : "ANGUAGE",
                      "propPluginPrefix" : "ANGULARGUAGE",
                      "UIName" : "Angular Guage",
                      "type" : "AngularGuage",
                      "propForIntlSense" : []
                  },
                  CYLINDERGUAGE :
                  {
                      "idPrefix" : "CYGUAGE",
                      "propPluginPrefix" : "CYLINDERGUAGE",
                      "UIName" : "Cylinder Guage",
                      "type" : "CylinderGuage",
                      "propForIntlSense" : []
                  },
                  LINEGRAPH :
                  {
                      "idPrefix" : "LINE",
                      "propPluginPrefix" : "LINEGRAPH",
                      "UIName" : "Line Graph",
                      "type" : "LineChart",
                      "propForIntlSense" : []
                  },
                  DATAPOINTCHART :
                  {
                      "idPrefix" : "DATAPNT",
                      "propPluginPrefix" : "DATAPOINTCHART",
                      "UIName" : "DataPoint Chart",
                      "type" : "DataPointChart",
                      "propForIntlSense" : []
                  },
                  TABLE :
                  {
                      "idPrefix" : "TBL",
                      "propPluginPrefix" : "TABLE",
                      "UIName" : "Table",
                      "type" : "Table",
                      "propForIntlSense" : []
                  },
                  SELECTORLIST :
                  {
                      "idPrefix" : "SEL",
                      "propPluginPrefix" : "SELECTORLIST",
                      "UIName" : "Selector List",
                      "type" : "SelectorList",
                      "propForIntlSense" : [
                          {
                              "prop" : "Text",
                              "val" : "Text",
                              "category" : "Select Property"
                          },
                          {
                              "prop" : "Value",
                              "val" : "Value",
                              "category:" : "Select Property"
                          }
                      ]
                  },
                  SPACER :
                   {
                       "idPrefix" : "SPC",
                       "propPluginPrefix" : "SPACER",
                       "UIName" : "Spacer",
                       "type" : "Spacer",
                       "propForIntlSense" : [
                           
                       ]
                   },
                  PICTURE :
                   {
                        "idPrefix" : "PIC",
                        "propPluginPrefix" : "PICTURE",
                        "UIName" : "Picture",
                        "type" : "Picture",
                        "propForIntlSense" : [
                            {
                                "prop" : "Filepath",
                                "val" : "Filepath",
                                "category" : "Select Property"
                            },
                            {
                                "prop" : "Binary",
                                "val" : "Binary",
                                "category" : "Select Property"
                            },
                            {
                                "prop" : "S3Key",
                                "val" : "S3Key",
                                "category" : "Select Property"
                            },
                            {
                                "prop" : "S3Bucket",
                                "val" : "S3Bucket",
                                "category" : "Select Property"
                            },
                            {
                                "prop" : "S3Region",
                                "val" : "S3Region",
                                "category" : "Select Property"
                            },
                            {
                                "prop" : "FileName",
                                "val" : "FileName",
                                "category" : "Select Property"
                            }
                         ]
                    },
                  SIGNATURE :
                    {
                         "idPrefix" : "SCRB",
                         "propPluginPrefix" : "SIGNATURE",
                         "UIName" : "Signature",
                         "type" : "Signature",
                         "propForIntlSense" : [
                            {
                                "prop" : "Filepath",
                                "val" : "Filepath",
                                "category" : "Select Property"
                            },
                            {
                                "prop" : "Binary",
                                "val" : "Binary",
                                "category" : "Select Property"
                            },
                            {
                                "prop" : "S3Key",
                                "val" : "S3Key",
                                "category" : "Select Property"
                            },
                            {
                                "prop" : "S3Bucket",
                                "val" : "S3Bucket",
                                "category" : "Select Property"
                            },
                            {
                                "prop" : "S3Region",
                                "val" : "S3Region",
                                "category" : "Select Property"
                            },
                            {
                                "prop" : "FileName",
                                "val" : "FileName",
                                "category" : "Select Property"
                            }
                          ]
                     },
                  DRILLDOWN_PIECHART :
                    {
                         "idPrefix" : "DPIE",
                         "propPluginPrefix" : "DDPIECHART",
                         "UIName" : "Pie Chart (Drilldown)",
                         "type" : "DrilldownPieChart",
                         "propForIntlSense" : []
                     },
                  BUTTON :
                   {
                      "idPrefix" : "BTN",
                      "propPluginPrefix" : "BUTTON",
                      "UIName" : "Button",
                      "type" : "Button",
                      "propForIntlSense" : []
                    },
                    CUSTOM_CONTROL :
                    {
                       "idPrefix" : "CUST",
                       "propPluginPrefix" : "CUSTOMCONTROL",
                       "UIName" : "Custom Control",
                       "type" : "CustomControl",
                       "propForIntlSense" : []
                     }
              },
    OBJECTS_WITH_PROPERTY: {
        APPS: { "idPrefix": "APP", "propPluginPrefix": "APP" },
        FORM: { "idPrefix": "FORM", "propPluginPrefix": "FORM" },
        VIEW: { "idPrefix": "VIEW", "propPluginPrefix": "VIEW" },
        CONDVIEW: { "idPrefix": "CONDVIEW", "propPluginPrefix": "CONDVIEW" }
    },
    CHILD_FORM_TYPE : {
        ADD: { "idPrefix": "ADD", "propPluginPrefix": "Add Form" },
        EDIT: { "idPrefix": "EDIT", "propPluginPrefix": "Edit Form" },
        VIEW: { "idPrefix": "VIEW", "propPluginPrefix": "View Form" },
    },
    JqueryDataKeyForPropSheet: "PropSheetControl",
    jqueryDataKeyForPropSheetHelper: "PropSheetHelper",
    exceptionMessage: {
        "argumentsNull": "Arguments null exception",
        "argumentsInvalid": "Invalid arguments passed",
        "objectNotFound": "Object should be available.But could not get the value."
    },
    IDE_APP_MODEL_TYPE: {
        phone: "0",
        tablet: "1"
    },
    IDE_APP_RUNS_ON_DEVICE_TYPE: {
        phoneAndTablet: "0",
        tabletOnly: "1"
    },
    IDE_DIV_FOR_STORING_DATA: {
        AppData: "WF_FrmInnerPropertieDiv",
        ControlData: "ControlPropetiesDiv",
        FormData : "FrmInnerPropertieDiv",
        ViewData:"WF_ControlPropetiesDiv"
    },
    draggableElementTypeEnum: {
        master: "1",
        condition: "2",
        form: "3"
    },
    ideContainerDivs: {
        appDesigner: "WF_FormDesignContainerDiv",
        appProperties: "WF_FrmInnerPropertieDiv",
        viewProperties :"WF_ControlPropetiesDiv",
        formProperties :"FrmInnerPropertieDiv",
        controlProperties:"ControlPropetiesDiv"
    },
    ideCommandObjectTypes: {
        None: { id: "0", UiName: "None" }, //Tanika
        db: { id: "1", UiName: "Database" },
        wsdl: { id: "2", UiName: "Webservice ( Soap )" },
        http: { id: "3", UiName: "Webservice ( Http )" },
        xmlRpc: { id: "4", UiName: "Webservice ( Xml RPC )" },
        odata: { id: "5", UiName: "Webservice ( ODATA )" },
        oflnDataObj:{ id: "6", UiName: "Offline Data Objects" },
        oflnDt:{ id: "7", UiName: "Offline Datatable" }
    },
    webServiceType: {
        http: { id: "1", UiName: "http", idText: "http" },
        wsdlSoap: { id: "2", UiName: "wsdl", idText: "wsdl" },
        xmlRpc: { id: "3", UiName: "xml rpc", idText: "rpc" }
    },
    errorMsgs: {
        objectProcModeChanged: "Only database connectors can be execute in batch processing mode.",
        selectOneConnTypeForCond: "Only one event type is allowed for transition to condition.",
        noConnectionTypeSelectedOnEdit:"No event type is selected. The transition and all associated settings will be removed. Are you sure you want to continue ?",
        selectOneConnTypeOnConnDrop:"Please select a event type for transition.",
        viewWithTransitionCantChgToModal :"View with forward transitions cannot have modal presentation.",
        noTransitionForModalView:"View with presentation type \"Modal\" cannot have transitions.",
        startupViewCantBeModal:"Startup View cannot have a modal presentation."
    },
    viewDestConnectionType: {
        next: { val: "1", conOverlayText: "Button :",id :"NextButton1"},
        rowClick: { val: "2", conOverlayText: "List touch : ",id :"ListTouch"},
        actionButton1: { val: "3", conOverlayText: "Action 1 : ",id :"Action1" },
        actionButton2: { val: "4", conOverlayText: "Action 2 : ",id :"Action2" },
        actionButton3: { val: "5", conOverlayText: "Action 3 : ",id :"Action3" },
        condition: { val: "6", conOverlayText: "Condition : ",id :"Condition" },
        viewActionButton1:{ val: "70", conOverlayText: "Action Button : ",id :"ActionButton70" },
        viewActionButton2:{ val: "71", conOverlayText: "Action Button : ",id :"ActionButton71" },
        viewActionButton3:{ val: "72", conOverlayText: "Action Button : ",id :"ActionButton72" },
        viewActionButton4:{ val: "73", conOverlayText: "Action Button : ",id :"ActionButton73" },
        viewActionButton5:{ val: "74", conOverlayText: "Action Button : ",id :"ActionButton74" },
        viewActionButton6:{ val: "75", conOverlayText: "Action Button : ",id :"ActionButton75" },
        multiple:{ val: "8", conOverlayText: "Multiple ",id :"Multiple" },
        button:{val:"9",conOverlayText:"Button :",id :"Button"}
    },
    cmdObjectSettingsProcMode: {
        sequential: "0",
        batch: "1"
    },
    viewSettingsType: {
        onEnter: "onEnter",
        onExit: "onExit",
        appStartUp: "appStartUp",
        actionBtn1: "actionBtn1",
        actionBtn2: "actionBtn2",
        actionBtn3: "actionBtn3",
        rowClickTask: "rowClickTask",
        cancelBtnTask: "cancelBtnTask",
        condOnExit:"condOnExit",
        viewActionButton:"viewActionButton",
        formActionButton :"formActionButton",
        customTriggers :"customTriggers"
    },
    jqueryDataKeys: {
        "actionBtnDialogSettings": "actionBtnDialogSettings",
        "viewOnEnterTblCont": "viewOnEnterTbl"
    },
    actionBtnOperationType: {
        command: "0",
        transition: "1"
    },
    intlsenseFixedParams:[{prop:"@FIRSTNAME",val:"@FIRSTNAME",category:"Fixed Values"},{prop:"@LASTNAME",val:"@LASTNAME",category:"Fixed Values"},{prop:"@FULLNAME",val:"@FULLNAME",category:"Fixed Values"},{prop:"@UID",val:"@UID",category:"Fixed Values"},{prop:"@EMAIL",val:"@EMAIL",category:"Fixed Values"},{prop:"@EMPLOYEEID",val:"@EMPLOYEEID",category:"Fixed Values"},{prop:"@DESIGNATION",val:"@DESIGNATION",category:"Fixed Values"},{prop:"@MOBILE",val:"@MOBILE",category:"Fixed Values"},{prop:"@DOB",val:"@DOB",category:"Fixed Values"},{prop:"@LOCATION",val:"@LOCATION",category:"Fixed Values"},{prop:"@REGION",val:"@REGION",category:"Fixed Values"},{prop:"@DIVISION",val:"@DIVISION",category:"Fixed Values"},{prop:"@DEPARTMENT",val:"@DEPARTMENT",category:"Fixed Values"},{prop:"@DEVICEID",val:"@DEVICEID",category:"Fixed Values"},{prop:"@DEVICEMODEL",val:"@DEVICEMODEL",category:"Fixed Values"},{prop:"@LATITUDE",val:"@LATITUDE",category:"Fixed Values"},{prop:"@LONGITUDE",val:"@LONGITUDE",category:"Fixed Values"},{prop:"@DATETIME",val:"@DATETIME",category:"Fixed Values"},{prop:"@DATE",val:"@DATE",category:"Fixed Values"},{prop:"@TIME",val:"@TIME",category:"Fixed Values"},{prop:"@DATETIMETICKS",val:"@DATETIMETICKS",category:"Fixed Values"},{prop:"@DATETICKS",val:"@DATETICKS",category:"Fixed Values"},{prop:"@DATEYYYYMMDD",val:"@DATEYYYYMMDD",category:"Fixed Values"},{prop:"@UNIQUEID",val:"@UNIQUEID",category:"Fixed Values"},{prop:"@WIFI",val:"@WIFI",category:"Fixed Values"}],
    //Tanika
    IDE_ROWPANEL_TYPE: {
        TEMP_100: { id: "1", UiName: "100%" },
        TEMP_33_34_33: { id: "2", UiName: "33%-34%-33%" },
        TEMP_66_34: { id: "3", UiName: "66%-34%" },
        TEMP_34_66: { id: "4", UiName: "34%-66%" },
        TEMP_50_50: { id: "5", UiName: "50%-50%" }
    },
    DBCommandType: {
        SELECT: "1",
        INSERT: "2",
        UPDATE: "3",
        DELETE: "4"
    },
    //Tanika
    //TOCHANGE MOHAN
    listActionBtnActionType:{
        "transition":"1",
        "command":"2"
    },
    listActionBtnType:{
        "actionBtn1":"actionBtn1",
        "actionBtn2":"actionBtn2",
        "actionBtn3":"actionBtn3"
    },
    listCntrlType:{
        "Simple": "0",
        "Basic":"1",
        "BasicWithDescription":"2",
        "Numbered":"3",
        "Icon":"4",
        "Thumbnail":"5",
        "ImageList" : "6",
        "EventList" : "7",
        "CustomList":"8"
    },
    dateFormat:{
       Unix : { UiName : "unix" , id: "0"},
       Ticks : { UiName : "ticks", id : "1"}
    },
    hiddenFieldsSection:"Sec_hd",
    jqueryCommonDataKey:"jQueryDataKey",
    appDesignMode:{
        "phone" : "0",
        "tablet":"1",
        "both":"2"
    },
    viewBtnPositions:{
        topLeft:"1",
        topRight:"2",
        bottomLeft:"3",
        bottomCenter:"4",
        bottomRight:"5"
    },
    viewActionButtonType:{
        command :"0",
        transition:"1"
    },
    jsPlumbEndpointType:{
        standard : "1",
        repeater :"2"
    },
    MF_STANDARD_APP_TYPE:{
        online :"0",
        offline:"1"
    },
    CNTRL_SIZES:{
        normal:"0",
        mini:"1"
    },
    defltVal:{
        yes:"1",
        no:"0",
        searchPlaceHolder:"Search..."
    },
    radioButtonStyle:{
        vertical : "0",
        horizontal :"1"
    },
    viewPresentationType:{
        normal :"0",
        modal :"1"
    },
    viewTransitionContextMenuType:{
        "edit":"0",
        "delete":"1"
    },
    buttonControlWidthType:{
        full : "1",
        auto : "2",
        custom :"3"
    },
    buttonControlIconPosition:{
        left : "1",
        right :"2",
        top :"3",
        bottom :"4"
    },
    procTypeForMfObjsEvents:{
        command :"0",
        transition:"1"
    },
    procTypeForCmdSettings : {
        submit : "1",
        rowClick : "2",
        actionButton1 : "3",
        actionButton2 :"4",
        actionButton3 : "5",
        condition :"6",
        viewActionButton : "7",
        formButton :"9",
        appStartup:"10",
        cancelTask :"11",
        customTriggers:"12"
    },
    customTriggers:{
        ctOne :"120",
        ctTwo :"121",
        ctThree :"122",
        ctFour :"123",
        ctFive:"124",
        ctSix :"125",
        ctSeven :"126",
        ctEight :"127",
        ctNine :"128",
        ctTen:"129"
    },
    appAvailableOnOrNotCriteria:{
        availableOn :"1",
        notAvailableOn :"2"
    }
    //TOCHANGE MOHAN
};
var MF_IDE_CONSTANTS = mFicientIde.MF_IDE_CONSTANTS;
var MF_PROPS_CONSTANTS = {
    CONTROL_TYPE: {
        "label": "label",
        "textbox": "textbox",
        "textArea": "textArea",
        "dropdown": "dropdown",
        "browse": "browse",
        "browseOfIcon": "browseOfIcon"
    },
    // VALIDATIONS :{
    // required,
    // onlyAlphabets,
    // alphaNumeric,
    // numeric,
    // decimal
    // },
    //FIXED_TYPE = ["YesNo", "TrueFalse"],
    //DDL_BIND_TYPE = ["YesNo", "TrueFalse", "database", "webservice", "odata", "others"],
    DDL_BIND_TYPE: {
        "manual": "1",
        "database": "2",
        "webservice": "3",
        "odata": "4"
    },
    DDL_SELECT_VALUE_BY: ["text", "value"]
};
mFicientIde.MF_HELPERS = (function () {
    var MF_FORMS_SAVED_IN_UI = {
        forms: [] //array of forms which are saved in ui not in database.,
    };
    function _getAllUIFormsObjAry() {
        return $.parseJSON($('[id$="hidAllForms"]').val());
    }
    function _getUIFormObj(formId) {
        var i = 0, objForm = null;
        if (formId) {
            var aryAllUIForms = _getAllUIFormsObjAry();
            if (aryAllUIForms && $.isArray(aryAllUIForms) && aryAllUIForms.length > 0) {
                for (i = 0; i <= aryAllUIForms.length - 1; i++) {
                    objForm = aryAllUIForms[i];
                    if (objForm.formId === formId) {
                        break;
                    }
                    else {
                        objForm = null;
                    }
                }
            }
        }
        return objForm;
    }
    function _getAllWsObjects() {
        var aryWsObjects = [];
        var hidFieldValue = $(':hidden[id$="hidAllWsCommands"]').val();
        if (hidFieldValue) {
            aryWsObjects = $.parseJSON(hidFieldValue);
        }
        return aryWsObjects;
    }
    function _isControlPartOfIntellisense(controlType /*MF_IDE_CONSTANTS.COnTROLS*/) {
        if (!controlType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
        var blnIsPartOfIntls = false;
        switch (controlType) {
            case MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
            case MF_IDE_CONSTANTS.CONTROLS.LOCATION:
            case MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
            case MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
            case MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON:
            case MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
            case MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
            case MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER:
            case MF_IDE_CONSTANTS.CONTROLS.SLIDER:
            case MF_IDE_CONSTANTS.CONTROLS.BARCODE:
            case MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:  
            case MF_IDE_CONSTANTS.CONTROLS.PICTURE:
            case MF_IDE_CONSTANTS.CONTROLS.SIGNATURE:
            case MF_IDE_CONSTANTS.CONTROLS.LIST :   
               blnIsPartOfIntls = true;
                break;
        }
        return blnIsPartOfIntls;
    }
    function _canControlBeInitialized(controlType /*MF_IDE_CONSTANTS.COnTROLS*/) {
        if (!controlType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
        var blnCanBeInitialized = true;
        switch (controlType) {
            case MF_IDE_CONSTANTS.CONTROLS.PICTURE:
            case MF_IDE_CONSTANTS.CONTROLS.SIGNATURE: 
            case MF_IDE_CONSTANTS.CONTROLS.LIST:
            case MF_IDE_CONSTANTS.CONTROLS.IMAGE:
            case MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
            case MF_IDE_CONSTANTS.CONTROLS.SEPARATOR:
            case MF_IDE_CONSTANTS.CONTROLS.SPACER:
            case MF_IDE_CONSTANTS.CONTROLS.TABLE:
            case MF_IDE_CONSTANTS.CONTROLS.PIECHART:
            case MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
            case MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
            case MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
            case MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
            case MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
            case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
            case MF_IDE_CONSTANTS.CONTROLS.BUTTON:    
               blnCanBeInitialized = false;
                break;
        }
        return blnCanBeInitialized;
    }
    function _canControlBeUsedForInitialization(controlType /*MF_IDE_CONSTANTS.COnTROLS*/) {
        if (!controlType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
        var blnCanBeInitializedFrom = false;
        switch (controlType) {
            case MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
            case MF_IDE_CONSTANTS.CONTROLS.LOCATION:
            case MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
            case MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
            case MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON:
            case MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
            case MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
            case MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER:
            case MF_IDE_CONSTANTS.CONTROLS.SLIDER:
            case MF_IDE_CONSTANTS.CONTROLS.BARCODE:
            case MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
            case MF_IDE_CONSTANTS.CONTROLS.LIST:    
               blnCanBeInitializedFrom = true;
                break;    
        }
        return blnCanBeInitializedFrom;
    }
    
    var _dbCommandHelpers = (function () {
        function _parseUICollectionHidField() {
            var aryDbObjects = [];
            var hidFieldValue = $(':hidden[id$="hidAllDbCommands"]').val();
            if (hidFieldValue) {
                aryDbObjects = $.parseJSON(hidFieldValue);
            }
            return aryDbObjects;
        }
        function _getObjectsOfIdAndNameForDdlBind() {
            var aryObjsOfIdAndName = [];
            var aryDbObjects = _parseUICollectionHidField();
            if (aryDbObjects && aryDbObjects.length > 0) {
                for (var i = 0; i <= aryDbObjects.length - 1; i++) {
                    var dbObject = aryDbObjects[i];
                    aryObjsOfIdAndName.push({
                        val: dbObject.commandId,
                        text: dbObject.commandName
                    });
                }
            }
            return aryObjsOfIdAndName;
        }
        function _getCommandObject(cmdId) {
            var cmdObj = null;
            var aryCmdObjects = _parseUICollectionHidField();
            if (aryCmdObjects && aryCmdObjects.length > 0) {
                for (var i = 0; i <= aryCmdObjects.length - 1; i++) {
                    cmdObj = aryCmdObjects[i];
                    if (cmdObj.commandId === cmdId) {
                        break;
                    }
                    else {
                        cmdObj = null;
                    }
                }
            }
            return cmdObj;
        }
        function _getInputParamsForUI(cmdId) {
            var aryInputParams = [];
            var cmdObj = _getCommandObject(cmdId);
            if (cmdObj && cmdObj.paramJson) {
                var objFinalInputParam = $.parseJSON(cmdObj.paramJson);
                if (objFinalInputParam) {
                    $.each(objFinalInputParam, function (index, value) {
                        var objParamDtl = value;
                        if (objParamDtl) {
                            aryInputParams.push(objParamDtl.para);
                        }
                    });
                }
            }
            return aryInputParams;
        }
        function _getOutputParams(cmdId) {
            var aryOutputParams = [],
                commaSeparatedList = "";
            var cmdObj = _getCommandObject(cmdId);
            if (cmdObj && cmdObj.columns) {
                commaSeparatedList = cmdObj.columns;
                aryOutputParams = commaSeparatedList.split(',');
            }
            return aryOutputParams;
        }
        function _getAllDbCmdOfSameConn(connectionId) {
            var aryObjsOfSameConnectionId = [];
            var aryAllDbObjects = _parseUICollectionHidField();
            aryObjsOfSameConnectionId = $.grep(aryAllDbObjects, function (value, index) {
                return value.connectorId === connectionId;
            });
            return aryObjsOfSameConnectionId;
        }
        function _getAllDbCmdOfSameConnForDdlBind(connectionId) {
            var aryObjsOfSameConnectionId = _getAllDbCmdOfSameConn(connectionId);
            var aryForDdlBind = [];
            $.each(aryObjsOfSameConnectionId, function (index, value) {
                aryForDdlBind.push({
                    val: value.commandId,
                    text: value.commandName
                });
            });
            return aryForDdlBind;
        }
        function _getConnectionIdOfCmd(cmdId) {
            var objCmd = _getCommandObject(cmdId);
            if (objCmd) {
                return objCmd.connectorId;
            }
            else {
                return "";
            }
        }
        function _getDbObjectsByCmdType(cmdType) {
            if (!cmdType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var aryAllDbCmds = _parseUICollectionHidField();
            var aryFilteredObjects = [];
            if (aryAllDbCmds && $.isArray(aryAllDbCmds) && aryAllDbCmds.length > 0) {
                switch (cmdType) {
                    case MF_IDE_CONSTANTS.DBCommandType.SELECT:
                        aryFilteredObjects = $.grep(aryAllDbCmds, function (value, index) {
                            return value.commandType === MF_IDE_CONSTANTS.DBCommandType.SELECT;
                        });
                        break;
                    case MF_IDE_CONSTANTS.DBCommandType.INSERT:
                        break;
                    case MF_IDE_CONSTANTS.DBCommandType.UPDATE:
                        break;
                    case MF_IDE_CONSTANTS.DBCommandType.DELETE:
                        break;
                }
            }
            return aryFilteredObjects;
        }
        function _getDbObjectCmdTypeText(cmdType){
            var commandType = "";
            switch (cmdType) {
                case mFicientIde.MF_IDE_CONSTANTS.DBCommandType.SELECT:
                    commandType = "SELECT";
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.DBCommandType.INSERT:
                    commandType = "INSERT";
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.DBCommandType.UPDATE:
                    ommandType = "UPDATE";
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.DBCommandType.DELETE:
                    commandType = "DELETE";
                    break;
            }
            return commandType;
        }
        return {
            getObjectsForDdlBind: function () {
                return _getObjectsOfIdAndNameForDdlBind();
            },
            getInputParamsForCmd: function (cmdId) {
                if (!cmdId) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                return _getInputParamsForUI(cmdId);
            },
            getObjectsByCmdId: function (cmdIds/*array of command ids*/) {
                var aryObjects = [];
                if (cmdIds && $.isArray(cmdIds) && cmdIds.length > 0) {
                    for (var i = 0; i <= cmdIds.length - 1; i++) {
                        aryObjects.push(_getCommandObject(cmdIds[i]));
                    }
                }
                return aryObjects;
            },
            getDbCmdsForDdlBindInBatchProcMode: function (connectionId) {
                if (connectionId) {
                    return _getAllDbCmdOfSameConnForDdlBind(connectionId);
                }
                else {
                    return this.getObjectsForDdlBind();
                }
            },
            getConnectionIdOfCmd: function (cmdId) {
                if (!cmdId) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                return _getConnectionIdOfCmd(cmdId);
            },
            getAllDbObjects: function () {
                return _parseUICollectionHidField();
            },
            getDbObjectsByCmdType: function (cmdType) {
                return _getDbObjectsByCmdType(cmdType);
            },
            getControlTypeById:function (controlId){
                if(!controlId)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                var cntrlPrefix = controlId.split('_')[0];
                var cntrlType = "";
                switch (cntrlPrefix.toUpperCase()) {
                    case MF_IDE_CONSTANTS.CONTROLS.SECTION.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.SECTION;
                        break;
                   case MF_IDE_CONSTANTS.CONTROLS.LABEL.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.LABEL;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.TEXTBOX;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.DROPDOWN;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.CHECKBOX;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.IMAGE.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.IMAGE;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.LIST.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.LIST;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.HIDDEN.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.HIDDEN;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.SECTION;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.TOGGLE.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.TOGGLE;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.SLIDER.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.SLIDER;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.SEPARATOR.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.SEPARATOR;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.BARCODE.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.BARCODE;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.LOCATION.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.LOCATION;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.PIECHART.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.PIECHART;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.BARGRAPH.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.BARGRAPH;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART;
                        break;
                        case MF_IDE_CONSTANTS.CONTROLS.TABLE.idPrefix:
                            cntrlType = MF_IDE_CONSTANTS.CONTROLS.TABLE;
                        break;
                }
                return cntrlType;
            },
            getObjectByCmdId: function (cmdId) {
                if(!cmdId)return null;
                return _getCommandObject(cmdId);
            },
            getDbObjectCmdTypeText: function (cmdType) {
               return _getDbObjectCmdTypeText(cmdType);
            },
            getOutputParamsForCmd: function(cmdId){
                if (!cmdId) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                return _getOutputParams(cmdId);
            }
        };
    })();
    var _wsCommandHelpers = (function () {
        function _parseUICollectionHidField(wsType) {
            var aryAllWsObjects = _getAllWsObjects();
            var aryObjectsByType = [];
            switch (wsType) {
                case MF_IDE_CONSTANTS.webServiceType.wsdlSoap:
                    aryObjectsByType = $.grep(aryAllWsObjects, function (value, index) {
                        return value.webserviceType.trim() === MF_IDE_CONSTANTS.webServiceType.wsdlSoap.idText;
                    });
                    break;
                case MF_IDE_CONSTANTS.webServiceType.xmlRpc:
                    aryObjectsByType = $.grep(aryAllWsObjects, function (value, index) {
                        return value.webserviceType.trim() === MF_IDE_CONSTANTS.webServiceType.xmlRpc.idText;
                    });
                    break;
                case MF_IDE_CONSTANTS.webServiceType.http:
                    aryObjectsByType = $.grep(aryAllWsObjects, function (value, index) {
                        return value.webserviceType.trim() === MF_IDE_CONSTANTS.webServiceType.http.idText;
                    });
                    break;
            }
            return aryObjectsByType;
        }
        function _getCmdObjectById(cmdId, wsType/*Constants.webserviceType*/) {
            var aryWsObjects = _parseUICollectionHidField(wsType);
            var wsObject = null;
            wsObject = $.grep(aryWsObjects, function (value, index) {
                return value.commandId === cmdId;
            });
            return wsObject[0];
        }
        function _getWsdlSimpleTypeForUI(arrayOfSimpleType, containerObjectName) {
            var aryFinalSimpleType = [];
            if (arrayOfSimpleType && $.isArray(arrayOfSimpleType) && arrayOfSimpleType.length > 0) {
                for (var i = 0; i <= arrayOfSimpleType.length - 1; i++) {
                    var strSimpleTypeForUI = containerObjectName ? containerObjectName + "." + arrayOfSimpleType[i].S_Name :
                                                arrayOfSimpleType[i].S_Name;
                    aryFinalSimpleType.push(strSimpleTypeForUI);
                }
            }
            return aryFinalSimpleType;
        }
        function _getWsdlComplexTypeForUI(complexTypes/*array*/, containerObjectName) {
            var aryFinalComplexType = [];
            if (complexTypes && $.isArray(complexTypes) && complexTypes.length > 0) {
                for (var i = 0; i <= complexTypes.length - 1; i++) {
                    var objComplexType = complexTypes[i];
                    var arySimpleTypesForUI = _getWsdlSimpleTypeForUI(objComplexType.S_Types, objComplexType.C_Name);
                    var aryInnerComplexType = _getWsdlComplexTypeForUI(objComplexType.C_Types, objComplexType.C_Name);
                    for (var j = 0; j <= arySimpleTypesForUI.length - 1; j++) {
                        var strSimpleTypeForUI = containerObjectName ? containerObjectName + "." + arySimpleTypesForUI[j] :
                                                arySimpleTypesForUI[j];
                        aryFinalComplexType.push(strSimpleTypeForUI);
                    }
                    for (var k = 0; k <= aryInnerComplexType.length - 1; k++) {
                        var strComplexTypeForUI = containerObjectName ? containerObjectName + "." + aryInnerComplexType[k] :
                                                aryInnerComplexType[k];
                        aryFinalComplexType.push(strComplexTypeForUI);
                    }
                }
            }
            return aryFinalComplexType;
        }
        var _parseXmlRpcParams = (function () {
            return {
                getAllPossibleXmlRpcParams: function (paramsArray) {
                    var aryFinalCombinationsOfText = [];
                    var aryOfFinalProperties = [];
                    var aryParamsFromJson = paramsArray;
                    var strVlue = "";
                    var aryOfPossibleCombinationFromObject = [],
                        i=0,k=0;
                    if (aryParamsFromJson.length > 0) {
                        for (i = 0; i <= aryParamsFromJson.length - 1; i++) {
                            var strPropFinalText = "";
                            aryOfPossibleCombinationFromObject = [];
                            aryOfPossibleCombinationFromObject = _getPossibleTextValsFromObject(aryParamsFromJson[i], strPropFinalText);
                            if (aryOfPossibleCombinationFromObject) {
                                if (typeof aryOfPossibleCombinationFromObject === "string") {
                                    aryOfFinalProperties.push(aryOfPossibleCombinationFromObject);
                                }
                                else if ($.isArray(aryOfPossibleCombinationFromObject)) {
                                    for (k = 0; k <= aryOfPossibleCombinationFromObject.length - 1; k++) {
                                        aryOfFinalProperties.push(aryOfPossibleCombinationFromObject[k]);
                                    }
                                }
                            }
                        }
                    }
                    i = 0;
                    for (i = 0; i <= aryOfFinalProperties.length - 1; i++) {
                        var aryOfFinalCombination = aryOfFinalProperties[i];
                        if (typeof aryOfFinalCombination === "string") {
                            aryFinalCombinationsOfText.push(aryOfFinalCombination);
                        }
                        else if ($.isArray(aryOfFinalCombination)) {
                            for (k = 0; k <= aryOfFinalCombination.length - 1; k++) {
                                aryFinalCombinationsOfText.push(aryOfFinalCombination[k]);
                            }
                        }
                    }
                    return aryFinalCombinationsOfText;
                }
            };
            function _getPossibleTextValsFromObject(propObject, previousStringEvluted /*string*/) {
                var aryFinalCombintionsOfText = [];
                var aryOfCombinationsInnerProp = [];
                if ($.isPlainObject(propObject)) {
                    previousStringEvluted = addTextFromPropObject(propObject, previousStringEvluted);
                    if (propObject.hasOwnProperty("inparam")) {
                        aryOfCombinationsInnerProp = _loopThroughInnerProperty(propObject.inparam, previousStringEvluted);
                    }
                }
                if (aryOfCombinationsInnerProp.length > 0) {
                    for (var i = 0; i <= aryOfCombinationsInnerProp.length - 1; i++) {
                        aryFinalCombintionsOfText.push(aryOfCombinationsInnerProp[i]);
                    }
                    return aryFinalCombintionsOfText;
                }
                else {
                    aryFinalCombintionsOfText.push(previousStringEvluted);
                    return aryFinalCombintionsOfText;
                }

            }
            function addTextFromPropObject(propObject, previousPropText) {
                if (previousPropText === "") {
                    return propObject.name;
                }
                else {
                    return previousPropText + "." + propObject.name;
                }

            }
            function _loopThroughInnerProperty(innerPropArryObject, previousPropTextEvaluated) {
                var aryOfCombinationsInnerProp = [];

                if ($.isArray(innerPropArryObject) && innerPropArryObject.length > 0) {
                    for (var i = 0; i <= innerPropArryObject.length - 1; i++) {
                        var strStrtString = previousPropTextEvaluated;
                        if ($.isPlainObject(innerPropArryObject[i])) {
                            var aryValsFromObject = _getPossibleTextValsFromObject(innerPropArryObject[i], strStrtString);
                            if(aryValsFromObject && $.isArray(aryValsFromObject) && aryValsFromObject.length>0){
                                for(var k=0;k<=aryValsFromObject.length-1;k++){
                                    aryOfCombinationsInnerProp.push(aryValsFromObject[k]);
                                }
                            }
                            else{
                                if(typeof aryValsFromObject === "string"){
                                    aryOfCombinationsInnerProp.push(aryValsFromObject);
                                }
                            }
                            
                        }

                    }
                }
                return aryOfCombinationsInnerProp;
            }
        })();
        function _getObjectsOfIdAndNameForDdlBind(wsType) {
            var aryObjsOfIdAndName = [];
            var aryWsObjects = _parseUICollectionHidField(wsType);
            if (aryWsObjects && aryWsObjects.length > 0) {
                for (var i = 0; i <= aryWsObjects.length - 1; i++) {
                    var wsObject = aryWsObjects[i];
                    aryObjsOfIdAndName.push({
                        val: wsObject.commandId,
                        text: wsObject.commandName
                    });
                }
            }
            return aryObjsOfIdAndName;
        }
        function _getXmlRpcInputParamsForUI(cmdId, wsType/*Constants.webserviceType*/) {
            var aryInputParams = [];
            var cmdObj = _getCmdObjectById(cmdId, wsType);
            if (cmdObj && cmdObj.parameter) {
                var objFinalInputParam = $.parseJSON(cmdObj.parameter);
                aryFinalTypesForUI = _parseXmlRpcParams.getAllPossibleXmlRpcParams(objFinalInputParam.param);
                return aryFinalTypesForUI;
            }
        }
        //Tanika*Change
        function _getXmlRpcOutputParamsForUI(tags/*json of output stored in database.*/) {
            if (tags) {
                var objFinalOutputParam = $.parseJSON(tags);
                aryFinalTypesForUI = _parseXmlRpcParams.getAllPossibleXmlRpcParams(objFinalOutputParam.param);
                return aryFinalTypesForUI;
            }
        }
        function _getHttpInputParamsForUI(cmdId, wsType) {
            var aryInputParams = [];
            var cmdObj = _getCmdObjectById(cmdId, wsType);
            if (cmdObj && cmdObj.parameter) {
                aryInputParams = cmdObj.parameter.split(',');
            }
            return aryInputParams;
        }
        //Tanika*Change
        function _getWsdlInputParamsForUI(cmdId, wsType/*Constants.webserviceType*/) {
            var aryInputParams = [];
            var cmdObj = _getCmdObjectById(cmdId, wsType);
            if (cmdObj && cmdObj.parameter) {
                var objFinalInputParam = $.parseJSON(cmdObj.parameter);
                var arySimpleTypesForUI = _getWsdlSimpleTypeForUI(objFinalInputParam.S_Types, "");
                var aryComplexTypesForUI = _getWsdlComplexTypeForUI(objFinalInputParam.C_Types, "");
                var aryFinalTypesForUI = [];
                for (var j = 0; j <= arySimpleTypesForUI.length - 1; j++) {
                    aryFinalTypesForUI.push(arySimpleTypesForUI[j]);
                }
                for (var k = 0; k <= aryComplexTypesForUI.length - 1; k++) {
                    aryFinalTypesForUI.push(aryComplexTypesForUI[k]);
                }
                //if (objFinalInputParam) {
                //$.each(objFinalInputParam, function (index, value) {
                //var objParamDtl = value;
                //if (objParamDtl) {
                //    aryInputParams.push(objParamDtl.para);
                //}
                //});
                //}
                return aryFinalTypesForUI;
            }
        }
        //Tanika*Change
        function _getWsdlOutputParamsForUI(tags/**json of output stored in database.**/) {
            if (tags) {
                var aryOutputParams = $.parseJSON(tags),
                    aryFinalTypesForUI =[],
                    i=0;
                
                if($.isArray(aryOutputParams) && aryOutputParams.length>0){
                    for(i=0;i<=aryOutputParams.length-1;i++){
                        aryFinalTypesForUI.push(aryOutputParams[i].name);
                    }
                }
                
                //PREVIOUS CODE The tag json is different.it dows not contanin S_TYpes or C_Types
                // var arySimpleTypesForUI = _getWsdlSimpleTypeForUI(objFinalOutputParam.S_Types, "");
                // var aryComplexTypesForUI = _getWsdlComplexTypeForUI(objFinalOutputParam.C_Types, "");
                // var aryFinalTypesForUI = [];
                // for (var j = 0; j <= arySimpleTypesForUI.length - 1; j++) {
                //     aryFinalTypesForUI.push(arySimpleTypesForUI[j]);
                // }
                // for (var k = 0; k <= aryComplexTypesForUI.length - 1; k++) {
                //     aryFinalTypesForUI.push(aryComplexTypesForUI[k]);
                // }
                
                
                
                //if (objFinalInputParam) {
                //$.each(objFinalInputParam, function (index, value) {
                //var objParamDtl = value;
                //if (objParamDtl) {
                //    aryInputParams.push(objParamDtl.para);
                //}
                //});
                //}
                return aryFinalTypesForUI;
            }
        }
        //Tanika*Change
        function _getWSTags(cmdId, wsType/*Constants.webserviceType*/) {
            var aryTags = [];
            var cmdObj = _getCmdObjectById(cmdId, wsType);
            if (cmdObj && cmdObj.tag) {
                switch(wsType){
                    case MF_IDE_CONSTANTS.webServiceType.xmlRpc:
                            aryTags = _getXmlRpcOutputParamsForUI(cmdObj.tag);
                    break;
                    case MF_IDE_CONSTANTS.webServiceType.http:
                            var objTags = $.parseJSON(cmdObj.tag);
                            $.each(objTags, function () {
                                aryTags.push(this["name"]);
                            });
                    break;
                    case MF_IDE_CONSTANTS.webServiceType.wsdlSoap:
                            aryTags = _getWsdlOutputParamsForUI(cmdObj.tag);
                    break;
                }
            }
            return aryTags;
        }
        //Tanika
        return {
            getObjectsForDdlBind: function (wstype) {
                return _getObjectsOfIdAndNameForDdlBind(wstype);
            },
            getInputParamsForCmd: function (cmdId, wsType/*Constants.webserviceType*/) {
                if (!cmdId && !wsType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                var aryInputParams = [];
                switch (wsType) {
                    case MF_IDE_CONSTANTS.webServiceType.wsdlSoap:
                        aryInputParams = _getWsdlInputParamsForUI(cmdId, wsType);
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.xmlRpc:
                        aryInputParams = _getXmlRpcInputParamsForUI(cmdId, wsType);
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.http:
                        aryInputParams = _getHttpInputParamsForUI(cmdId, wsType);
                        break;
                }
                return aryInputParams;
            },
            //Tanika
            getWsTags: function (cmdId, wsType/*Constants.webserviceType*/) {
                return _getWSTags(cmdId, wsType);
            },
            getWsObjectById:function(cmdId){
                if(!cmdId)return null;
                var aryAllWsObjects = _getAllWsObjects();
                var wsObject = null;
                wsObject = $.grep(aryAllWsObjects, function (value, index) {
                    return value.commandId === cmdId;
                });
                return wsObject[0];
            }    
        };

    })();
    var _odataCommandHelpers = (function () {
        function _parseUICollectionHidField() {
            var aryOdataObjects = [];
            var hidFieldValue = $(':hidden[id$="hidAllOdataCommands"]').val();
            if (hidFieldValue) {
                aryOdataObjects = $.parseJSON(hidFieldValue);
            }
            return aryOdataObjects;
        }
        function _getObjectsOfIdAndNameForDdlBind() {
            var aryObjsOfIdAndName = [];
            var aryOdataObjects = _parseUICollectionHidField();
            if (aryOdataObjects && aryOdataObjects.length > 0) {
                for (var i = 0; i <= aryOdataObjects.length - 1; i++) {
                    var odataObject = aryOdataObjects[i];
                    aryObjsOfIdAndName.push({
                        val: odataObject.commandId,
                        text: odataObject.commandName
                    });
                }
            }
            return aryObjsOfIdAndName;
        }
        function _getCommandObject(cmdId) {
            var cmdObj = null;
            var aryCmdObjects = _parseUICollectionHidField();
            if (aryCmdObjects && aryCmdObjects.length > 0) {
                for (var i = 0; i <= aryCmdObjects.length - 1; i++) {
                    cmdObj = aryCmdObjects[i];
                    if (cmdObj.commandId === cmdId) {
                        break;
                    }
                    else {
                        cmdObj = null;
                    }
                }
            }
            return cmdObj;
        }
        function _getInputParamsForUI(cmdId) {
            var aryInputParams = [];
            var cmdObj = _getCommandObject(cmdId);
            if (cmdObj && cmdObj.inputParamJson) {
                var objFinalInputParam = $.parseJSON(cmdObj.inputParamJson);
                if (objFinalInputParam) {
                    $.each(objFinalInputParam, function (index, value) {
                        var objParamDtl = value;
                        if (objParamDtl) {
                            aryInputParams.push(objParamDtl.para);
                        }
                    });
                }
            }
            return aryInputParams;
        }
        function _getOutputParams(cmdId) {
            var aryOutputParams = [];
            var cmdObj = _getCommandObject(cmdId);
            if (cmdObj && cmdObj.outputParamJson) {
                var objFinalOutputParam = $.parseJSON(cmdObj.outputParamJson);
                if (objFinalOutputParam) {
                    $.each(objFinalOutputParam, function (index, value) {
                        var objParamDtl = value;
                        if (objParamDtl) {
                            aryOutputParams.push(objParamDtl.name);
                        }
                    });
                }
            }
            return aryOutputParams;
        }
        return {
            getObjectsForDdlBind: function () {
                return _getObjectsOfIdAndNameForDdlBind();
            },
            getAllOdataObjects: function () {
                return _parseUICollectionHidField();
            },
            getInputParamsForCmd: function (cmdId) {
                if (!cmdId) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                return _getInputParamsForUI(cmdId);
            },
            getOdataObjectById:function(cmdId){
                if(!cmdId)return null;
                var aryAllOdataObjects = _parseUICollectionHidField();
                var oDataObject = null;
                oDataObject = $.grep(aryAllOdataObjects, function (value, index) {
                    return value.commandId === cmdId;
                });
                return oDataObject[0];
            },
            getOutputParamsForCmd: function(cmdId){
                if (!cmdId) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                return _getOutputParams(cmdId);
            }
        };

    })();
    var _appDetailsHelper = (function(){
        function _getAllAppsFromHiddenField(){
            var strAppsListJson = $("[id$='hidAppsList']").val();
            var i=0,aryAppDtl=[],objApp=null,aryFinalAppList=[];
            if(strAppsListJson){
                var aryApps = $.parseJSON(strAppsListJson);
                if(aryApps &&
                    $.isArray(aryApps) &&
                    aryApps.length>0){
                
                    for(i=0;i<=aryApps.length-1;i++){
                        aryAppDtl = aryApps[i];
                            if(aryAppDtl && 
                             $.isArray(aryAppDtl) && 
                                aryAppDtl.length>0){
                                aryFinalAppList.push({
                                    id:aryAppDtl[0],
                                    name:aryAppDtl[1]
                                });
                            }
                        }
                    }
                aryFinalAppList;
            }
            return aryFinalAppList;
        }
        function _getAllAppsOptionsForDdlBinding(){
            var strOptions="",
                i=0,
                objApp=null;
            var aryAllApps = _getAllAppsFromHiddenField();
            if(aryAllApps && $.isArray(aryAllApps) && aryAllApps.length>0){
                for(i=0;i<=aryAllApps.length-1;i++){
                    objApp = aryAllApps[i];
                    strOptions += "<option value= "+objApp.id+" >"+objApp.name+"</option>";
                }       
            }
            return strOptions;
        }
        function _getAppsOptionsForDdlBinding(ignoreApps/*ary of app ids*/){
            if(!ignoreApps )ignoreApps=[];
            if(!$.isArray(ignoreApps)) throw MF_IDE_CONSTANTS.exceptionMessage.invalidArguments;
            var strOptions="",
                i=0,
                objApp=null;
            var aryAllApps = _getAllAppsFromHiddenField();
            if(aryAllApps && $.isArray(aryAllApps) && aryAllApps.length>0){
                for(i=0;i<=aryAllApps.length-1;i++){
                    objApp = aryAllApps[i];
                    if($.inArray(objApp.id,ignoreApps) === -1){
                        strOptions += "<option value= "+objApp.id+" >"+objApp.name+"</option>";
                    }   
                }       
            }
            return strOptions;
        }
        return {
            getAllAppsOptionsForDdlBinding:function(){
                return _getAllAppsOptionsForDdlBinding();
            },
            getAppsOptionsForDdlBinding:function(ignoreApps/*ary of app ids*/){
                return _getAppsOptionsForDdlBinding(ignoreApps);
            }
        };
        
        })();
    var _mpluginAgentsHelper=(function(){
        function _parseUICollectionHidField() {
            var aryAgents = [];
            var hidFieldValue = $(':hidden[id$="hidmPluginAgents"]').val();
            if (hidFieldValue) {
                aryAgents = $.parseJSON(hidFieldValue);
            }
            return aryAgents;
        }
        function _getmPluginAgents() {
            var aryAgents = _parseUICollectionHidField();
            return aryAgents;
        }
        function _getmPluginAgentsAsOptionsOfDdl(addSelectOption,selectOptionValue){
            var aryAgents = _getmPluginAgents();
            var strOptions = "",i=0,objAgent = null;
            if(addSelectOption != null && addSelectOption === true){
                strOptions += '<option value='+selectOptionValue+'>Select</option>';
            }
            if(aryAgents && $.isArray(aryAgents) && aryAgents.length>0){
                for(i=0;i<=aryAgents.length-1;i++){
                    objAgent = aryAgents[i];
                    if(objAgent){
                        strOptions += 
                        '<option value='+objAgent.AgentId+'>'+
                        objAgent.AgentName+'</option>';
                    }
                }
            }
            return strOptions;
        }
        return {
            getmPluginAgentsAsOptionsOfDdl :function(addSelectOption,selectOptionValue){
               return _getmPluginAgentsAsOptionsOfDdl(addSelectOption,selectOptionValue);
            },
            getmPluginAgents :function(){
               return _getmPluginAgents();
            }
        };
    })();
    var _awsCredentialsHelper=(function(){
        function _parseUICollectionHidField() {
            var aryCredentials = [];
            var hidFieldValue = $(':hidden[id$="hidAWSCredentials"]').val();
            if (hidFieldValue) {
                aryCredentials = $.parseJSON(hidFieldValue);
            }
            return aryCredentials;
        }
        function _getawsCredentials() {
            var aryCredentials = _parseUICollectionHidField();
            return aryCredentials;
        }
        function _getawsCredentialsAsOptionsOfDdl(addSelectOption,selectOptionValue){
            var aryCredentials = _getawsCredentials();
            var strOptions = "",i=0,objCredential = null;
            if(addSelectOption != null && addSelectOption === true){
                strOptions += '<option value='+selectOptionValue+'>Select</option>';
            }
            if(aryCredentials && $.isArray(aryCredentials) && aryCredentials.length>0){
                for(i=0;i<=aryCredentials.length-1;i++){
                    objCredential = aryCredentials[i];
                    if(objCredential){
                        strOptions += 
                        '<option value='+objCredential.tag+'>'+
                        objCredential.name+'</option>';
                    }
                }
            }
            return strOptions;
        }
        return {
            getawsCredentialsAsOptionsOfDdl :function(addSelectOption,selectOptionValue){
               return _getawsCredentialsAsOptionsOfDdl(addSelectOption,selectOptionValue);
            },
            getawsCredentials :function(){
               return _getawsCredentials();
            }
        };
    })();

    var _offlineDtHelpers=(function(){
             function _getIntlsensePropsOfDatatable(){
                 return [
                     {
                             prop :"LastAddedRowId",
                             val : "LastAddedRowId",
                             category : "Select Properties",
                             ip : []
                    },
                    {
                             prop :"RowCount",
                             val : "RowCount",
                             category : "Select Properties",
                             ip : []
                    },
                    {
                             prop :"Max[\"ColumnName\"]",
                             val : "Max[\"ColumnName\"]",
                             category : "Select Properties",
                             ip : []
                    },
                    {
                             prop :"Min[\"ColumnName\"]",
                             val : "Min[\"ColumnName\"]",
                             category : "Select Properties",
                             ip : []
                    }
                    
                ];
             }
             function _parseUICollectionHidField() {
                 var aryObjects = [];
                 var hidFieldValue = $(':hidden[id$="hidAllOfflineDts"]').val();
                 if (hidFieldValue) {
                     aryObjects = $.parseJSON(hidFieldValue);
                 }
                 return aryObjects;
             }
             function _parseUIOfflineObjsCollectionHidField() {
                 var aryObjects = [];
                 var hidFieldValue = $(':hidden[id$="hidAllOfflineDataObjects"]').val();
                 if (hidFieldValue) {
                     aryObjects = $.parseJSON(hidFieldValue);
                 }
                 return aryObjects;
             }
             function _getAllOfflineDts(){
                 return _parseUICollectionHidField();
             }
             function _getAllOfflineDataObjs(){
                 return _parseUIOfflineObjsCollectionHidField();
             }
             function _getDatatablesAsOptionsForDdl(
                 addSelectOption,
                 selectOptionValue){
                 
                 var aryOfflineDts = _getAllOfflineDts(),
                    strOptions = "",i=0,objOfflnDt = null;
                 
                 if(addSelectOption != null && 
                       addSelectOption === true){
                     
                     strOptions += '<option value='+selectOptionValue+'>Select</option>';
                 }
                 if($.isArray(aryOfflineDts) && aryOfflineDts.length>0){
                     for(i=0;i<=aryOfflineDts.length-1;i++){
                         objOfflnDt = aryOfflineDts[i];
                         if(objOfflnDt){
                             strOptions += 
                             '<option value='+objOfflnDt.TableId+'>'+
                               objOfflnDt.TableName+'</option>';
                         }
                     }
                 }
                 return strOptions;
             }
             function _getOfflineDataObjectsAsOptionsForDdl(
                 addSelectOption,
                 selectOptionValue){
                 
                 var aryOfflineDataObjs = _getAllOfflineDataObjs(),
                    strOptions = "",i=0,objOfflnDataObj = null;
                 
                 if(addSelectOption != null && 
                       addSelectOption === true){
                     
                     strOptions += '<option value='+selectOptionValue+'>Select</option>';
                 }
                 if($.isArray(aryOfflineDataObjs) && aryOfflineDataObjs.length>0){
                     for(i=0;i<=aryOfflineDataObjs.length-1;i++){
                         objOfflnDataObj = aryOfflineDataObjs[i];
                         if(objOfflnDataObj){
                             strOptions += 
                             '<option value='+objOfflnDataObj.ID+'>'+
                               objOfflnDataObj.Name+'</option>';
                         }
                     }
                 }
                 return strOptions;
             }
             
             function _getDatatableByTableId(tableId){
                 var aryObjects = [],
                     objDatatable= null,i=0;
                 aryObjects = _getAllOfflineDts();
                 if(aryObjects && $.isArray(aryObjects) && aryObjects.length>0){
                     for(i=0;i<= aryObjects.length-1;i++){
                         objDatatable = aryObjects[i];
                         if(objDatatable && objDatatable.TableId === tableId){
                             break;
                         }
                         else{
                             objDatatable = null;
                         }
                     }
                 }
                 return objDatatable;
             }
             
             function _getDataObjByObjectId(objectId){
                 var aryObjects = [],
                     objDataObj= null,i=0;
                 aryObjects = _getAllOfflineDataObjs();
                 if(aryObjects && $.isArray(aryObjects) && aryObjects.length>0){
                     for(i=0;i<= aryObjects.length-1;i++){
                         objDataObj = aryObjects[i];
                         if(objDataObj && objDataObj.ID === objectId){
                             break;
                         }
                         else{
                             objDataObj = null;
                         }
                     }
                 }
                 return objDataObj;
             }
             function _getDatatableColsObjectByTableId(tableId){
                 var objDatatable = _getDatatableByTableId(tableId),
                     aryCols =[],
                     strTblJson = "",
                     objTbl=null;
                     
                 if(objDatatable){
                     strTblJson = objDatatable.TableJson;
                     if(strTblJson){
                         objTbl = $.parseJSON(strTblJson);
                         aryCols = objTbl.tbl && objTbl.tbl.col;
                     }
                 }
                 if(aryCols == null){
                     aryCols =[];
                 }
                 return aryCols;
             }
             function _getDataObjOutputColsObjectByObjectId(objectId){
                 var objDataObject = _getDataObjByObjectId(objectId),
                     aryCols =[];
                     
                 if(objDataObject){
                     if(parseInt(objDataObject.OutputParameters.length, 10) > 0){
                        $.each(objDataObject.OutputParameters.split(','), function(){
                            aryCols.push(this); 
                        });
                    }
                }
                if(aryCols == null){
                    aryCols =[];
                }
                return aryCols;
             }
             function _getInputParamsForUI(objectId) {
                var aryInputParams = [];
                var cmdObj = _getDataObjByObjectId(objectId);
                if (cmdObj && cmdObj.InputParameters) {
                    var objFinalInputParam = $.parseJSON(cmdObj.InputParameters);
                    if (objFinalInputParam) {
                        $.each(objFinalInputParam, function (index, value) {
                            var objParamDtl = value;
                            if (objParamDtl) {
                                aryInputParams.push(objParamDtl.para);
                            }
                        });
                    }
                }
                return aryInputParams;
            }
             function _getDatatableColsAsOptionsForDdl(tableId,
                         addSelectOption,
                         selectOptionValue){
                   
                   var aryCols = _getDatatableColsObjectByTableId(tableId),
                      strOptions = "",i=0,objOfflnDtCol = null;
                   
                   if(addSelectOption != null && 
                         addSelectOption === true){
                       
                       strOptions += '<option value='+selectOptionValue+'>Select</option>';
                   }
                   if(aryCols && $.isArray(aryCols) && aryCols.length>0){
                       for(i=0;i<=aryCols.length-1;i++){
                           objOfflnDtCol = aryCols[i];
                           if(objOfflnDtCol){
                               strOptions += 
                               '<option value='+objOfflnDtCol.cnm+'>'+
                                 objOfflnDtCol.cnm+'</option>';
                           }
                       }
                   }
                   return strOptions;
             }
             function _getOutputColsAsOptionsForDdl(objectId,
                         addSelectOption,
                         selectOptionValue){
                   
                   var aryCols = _getDataObjOutputColsObjectByObjectId(objectId),
                      strOptions = "",i=0,objOfflnDtCol = null;
                   
                   if(addSelectOption != null && 
                         addSelectOption === true){
                       
                       strOptions += '<option value='+selectOptionValue+'>Select</option>';
                   }
                   if(aryCols && $.isArray(aryCols) && aryCols.length>0){
                       for(i=0;i<=aryCols.length-1;i++){
                           objOfflnDtCol = aryCols[i];
                           if(objOfflnDtCol){
                               strOptions += 
                               '<option value='+objOfflnDtCol+'>'+
                                 objOfflnDtCol+'</option>';
                           }
                       }
                   }
                   return strOptions;
             }
             function _getDatatableColsNames(tableId){
                 var aryCols = _getDatatableColsObjectByTableId(tableId),
                    aryColsName =[];
                 for(var i=0;i<=aryCols.length-1;i++){
                     var objCol = aryCols[i];
                     if(objCol){
                         aryColsName.push(objCol.cnm);
                     }
                 }
                 return aryColsName;
             }
             function _getDataObjOutputCols(objectId){
                 var aryColsName = _getDataObjOutputColsObjectByObjectId(objectId);
                 return aryColsName;
             }

             function _getObjectsOfIdAndNameForDdlBind() {
                 var aryObjsOfIdAndName = [];
                 var aryObjects = _getAllOfflineDts();
                 if (aryObjects && aryObjects.length > 0) {
                     for (var i = 0; i <= aryObjects.length - 1; i++) {
                         var objObject = aryObjects[i];
                         aryObjsOfIdAndName.push({
                             val: objObject.TableId,
                             text: objObject.TableName
                         });
                     }
                 }
                 return aryObjsOfIdAndName;
             }
             return {
                 getDatatablesAsOptionsForDdl:function(addSelectOption,selectOptionValue){
                     return _getDatatablesAsOptionsForDdl(addSelectOption,
                                                          selectOptionValue);
                 },
                 getOfflineDataObjsAsOptionsForDdl:function(addSelectOption,selectOptionValue){
                     return _getOfflineDataObjectsAsOptionsForDdl(addSelectOption,
                                                          selectOptionValue);
                 },
                 getDatatableByTableId:function(tableId){
                     return _getDatatableByTableId(tableId);
                 },
                 getDataObjByObjectId:function(objectId){
                     return _getDataObjByObjectId(objectId);
                 },
                 getInputParamsForCmd: function (cmdId) {
                    if (!cmdId) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                    return _getInputParamsForUI(cmdId);
                },
                 getDatatableColsAsOptionsForDdl:function(tableId,
                      addSelectOption,
                      selectOptionValue){
                          
                    return  _getDatatableColsAsOptionsForDdl(
                            tableId,
                            addSelectOption,
                            selectOptionValue
                            );
                          
                 },
                 getDataObjectOutputColsAsOptionsForDdl:function(objectId,
                      addSelectOption,
                      selectOptionValue){
                          
                    return  _getOutputColsAsOptionsForDdl(
                            objectId,
                            addSelectOption,
                            selectOptionValue
                            );
                          
                 },
                 getDatatableColsObjects:function(tableId){
                     return _getDatatableColsObjectByTableId(tableId);
                 },
                 getDatatableColsNames:function(tableId){
                     return _getDatatableColsNames(tableId);
                 },
                 getDataObjOutputCols:function(objectId){
                     return _getDataObjOutputCols(objectId);
                 },
                 getObjectsForDdlBind: function () {
                     return _getObjectsOfIdAndNameForDdlBind();
                 },
                 getDatatablesForIntellisense:function(){
                     var aryOfflineDts = _getAllOfflineDts();
                     var aryIntlsense = [],
                        objInnerProp = null;
                     //{prop:"@FNAME",val:"@FNAME",category:"Fixed Values"}
                     if($.isArray(aryOfflineDts) && aryOfflineDts.length>0){
                         var objProp = {
                             prop : "Datatables",
                             val : "Datatables",
                             category : "Offline Datatables",
                             ip : []
                         }; 
                         for(i=0;i<=aryOfflineDts.length-1;i++){
                             objOfflnDt = aryOfflineDts[i];
                             if(objOfflnDt){
                                objInnerProp = {
                                    prop : objOfflnDt.TableName,
                                    val : objOfflnDt.TableName,
                                    category : "Datatables",
                                    ip :_getIntlsensePropsOfDatatable()
                                };
                                objProp.ip.push(objInnerProp);
                             }
                         }
                         aryIntlsense.push(objProp);
                     }
                     return aryIntlsense;
                 }
             };
         })();
         
    var _jqueryMobileIconsHelper = (function(){
        function _getHidField(){
            return $('[id$="hidAllJqueryMobIcons"]');
        }
        /**
        * Get the json string of icons from hid field
        * @return {string}
        */
        function _getStringListOfIconsFromHidField(){
            return _getHidField().val();
        }
        /**
        * Gets the list of icons from the stored values in UI
        * The list is stored in a hid field
        * @return {Array} aryIcons
        */
        function _getArrayListOfIconsFromHidField(){
            var strIcons = _getStringListOfIconsFromHidField(),
                aryIcons = [];
            if(!mfUtil.isNullOrUndefined(strIcons) &&  !mfUtil.isEmptyString(strIcons)){
                aryIcons = $.parseJSON(strIcons);
            }
            return aryIcons;
        }
        /**
        * Get the list of icons as an list of objects
        * text and val is the same as there is no id
        * @return {Array of {text,val} objects}
        */
        function _getAsTextValPairList(){
            var aryIcons = _getArrayListOfIconsFromHidField(),
                i=0,
                aryFinalList = [];
            if($.isArray(aryIcons) && aryIcons.length >0){
                for(i=0;i<= aryIcons.length-1;i++){
                    aryFinalList.push({
                        text : aryIcons[i],
                        val : aryIcons[i]
                    });
                }
            }
            return aryFinalList;
        }
        return {
            getArrayListOfIcons : function(){
                return _getArrayListOfIconsFromHidField();
            },
            getAsTextValPairList : function(){
                return _getAsTextValPairList();
            },
            getCompleteIconPath : function(iconName){
                return MF_IDE_CONSTANTS.enterpriseJqueryImagesPath+iconName;
            }
        };
    })();     
    return {
        getControlTypeByPrefix: function (prefix) {
            if (!prefix)
                return "";
            var retControl = "";
            for (var control in MF_IDE_CONSTANTS.CONTROLS) {
                if (MF_IDE_CONSTANTS.CONTROLS[control].idPrefix === prefix) {
                    retControl = MF_IDE_CONSTANTS.CONTROLS[control];
                    break;
                }
            }
            return retControl;
        },
        getAllViewButtonsFromJsonString: function (jsonValue /*Value stored in jason 1000100*/) {
            var objButtons = null;
            if (jsonValue) {
                objButtons = new Object();
                var aryBtns = jsonValue.split('');
                try {
                    objButtons.next = aryBtns[0];
                    objButtons.back = aryBtns[1];
                    objButtons.cancel = aryBtns[2];
                    objButtons.CancelBtnCmdProcMode = aryBtns[3];
                    objButtons.backNavigation = aryBtns[4];
                    objButtons.onExitProcMode = aryBtns[5];
                    objButtons.rowClickCmdProc = aryBtns[6];
                }
                catch (err) {
                    objButtons = null;
                }
            }
            return objButtons;
        },
        getJsonObject: function (json) {
            var objJson = null;
            if (json) {
                objJson = $.isPlainObject(json) ? json : $.parseJSON(json);
            }
            return objJson;
        },
        getAllFormObjAryByApp: function (appId) {
            var i = 0, aryForms = [];
            // if (appId) {
            // var aryAllUIForms = _getAllUIFormsObjAry();
            // if (aryAllUIForms && $.isArray(aryAllUIForms) && aryAllUIForms.length > 0) {
            // for (i = 0; i <= aryAllUIForms.length - 1; i++) {
            // objForm = aryAllUIForms[i];
            // if (objForm.parentId === appId) {
            // aryForms.push(objForm);
            // }
            // }
            // }
            // }
            var objPhoneApp = this.appsContainerObjectHelper.getPhoneAppFromContainer();
            var objTabletApp = this.appsContainerObjectHelper.getTabletAppFromContainer();
            if (objPhoneApp && objPhoneApp.forms && $.isArray(objPhoneApp.forms)) {
                $.merge(aryForms, objPhoneApp.forms);
            }
            if (objTabletApp && objTabletApp.forms && $.isArray(objTabletApp.forms)) {
                $.merge(aryForms, objTabletApp.forms);
            }
            return aryForms;
        },
        getAllFormObjsAryByAppAndModelType: function (appId, modelType/*value stored in json send the value not object*/) {
            var i = 0, aryForms = [];
            if (appId) {
                var aryAllUIForms = _getAllUIFormsObjAry();
                if (aryAllUIForms && $.isArray(aryAllUIForms) && aryAllUIForms.length > 0) {
                    aryAllUIForms = $.grep(aryAllUIForms, function (value, index) {
                        return value.modelType === modelType && value.parentId === appId;
                    });
                    $.each(aryAllUIForms, function (index, value) {
                        var objForm = new Form();
                        objForm.fillProperties(value);
                        aryForms.push(objForm);
                    });
                }
            }
            return aryForms;
        },
        getKeyForStoredData: function () {
            return MF_IDE_CONSTANTS.JqueryDataKeyForPropSheet;
        },
        currentObjectHelper: {
            getCurrentWorkingApp: function () {
                var objCurrentWorkingApp = null;
                var storageDiv = MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.AppData;
                objCurrentWorkingApp = $('#' + storageDiv).data(MF_HELPERS.getKeyForStoredData());
                // if(MF_CURRENTLY_WORKING_OBJECTS.app){
                // objCurrentWorkingApp = MF_CURRENTLY_WORKING_OBJECTS.app;
                // }
                // else{
                // //Div where App obejct is saved by the prop sheet plugin.
                // storageDiv = MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.AppData;
                // objCurrentWorkingApp = $('#'+storageDiv).data(this.getKeyForStoredData());
                // MF_CURRENTLY_WORKING_OBJECTS.app = objCurrentWorkingApp;
                // }
                return objCurrentWorkingApp;
            },
            getCurrentWorkingForm : function(){
                var objCurrentWorkingForm = null;
                var storageDiv = MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.FormData;
                objCurrentWorkingForm = $('#' + storageDiv).data(MF_HELPERS.getKeyForStoredData());
                return objCurrentWorkingForm;
            },
            // getForm: function(formId) {
            // var objForm = null;
            // var storageDiv = "", i = 0;
            // if (MF_FORMS_SAVED_IN_UI.forms && $.isArray(MF_FORMS_SAVED_IN_UI.forms) && MF_FORMS_SAVED_IN_UI.forms.length > 0) {
            // for (i = 0; i <= MF_FORMS_SAVED_IN_UI.forms.length - 1; i++) {
            // objForm = MF_FORMS_SAVED_IN_UI.forms[i];
            // if (objForm.formId === formId) {
            // break;
            // }
            // else{
            // objForm = null;
            // }
            // }
            // } 
            // else {
            // objForm = _getUIFormObj(formId);
            // //TODOCHANGE
            // if(formId === "0B067F9F672E2F2E88460B42AC4A9F3C"){
            // objForm.actionBtn1Setting = {"label":"testLabelOne",operationType:"1"};
            // objForm.actionBtn2Setting = {"label":"testLabelTwo",operationType:"1"};
            // objForm.actionBtn3Setting = {"label":"testLabelThree",operationType:"1"};
            // }
            // //TODOCHANGE
            // }
            // return objForm;
            // },
            getFormFromApp: function (formId) {
                var objForm = null;
                var app = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                for (var f = 0; f <= app.forms.length - 1; f++) {
                    if (app.forms[f].id === formId) {
                        objForm = app.forms[f];
                        break;
                    }
                }
                return objForm;
            },
            getCurrentWorkingControl: function () {
                var objCurrentWorkingCntrl = null;
                var storageDiv = MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData;
                objCurrentWorkingCntrl = $('#' + storageDiv).data(MF_HELPERS.getKeyForStoredData());
                return objCurrentWorkingCntrl;
            },
            getCurrentWorkingView:function(){
                var objCurrentWorkingView = null;
                var storageDiv = MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ViewData;
                objCurrentWorkingView = $('#' + storageDiv).data(MF_HELPERS.getKeyForStoredData());
                return objCurrentWorkingView;
            },
            getMainFormIfCurrFormIsChildForm:function(){
                return form;
            }
        },
        bindDdls: function (ddlIds/*array of ddls to bind*/,
                      valTextColJson/*Json String-- of array of val text object*/,
                      isDdlCollectionJqueryObj) {
            if (valTextColJson && ddlIds && $.isArray(ddlIds) && ddlIds.length > 0) {
                var aryValTxtObject = $.parseJSON(valTextColJson),
                    i=0,j=0,ddl=$([]);
                if (isDdlCollectionJqueryObj) {
                    for (i = 0; i <= ddlIds.length - 1; i++) {
                        ddl = ddlIds[i];
                        $(ddl).find('option').remove();
                        $(ddl).append('<option value="-1">Select</option>');
                        for (j = 0; j <= aryValTxtObject.length - 1; j++) {
                            $(ddl).append('<option value="' + aryValTxtObject[j].val + '">' + aryValTxtObject[j].text + '</option>');
                        }
                    }
                }
                else {
                    for (i = 0; i <= ddlIds.length - 1; i++) {
                        ddl = $('[id$=' + ddlIds[i] + ']');
                        if (ddl && ddl.length > 0) {
                            $(ddl).find('option').remove();
                            $(ddl).append('<option value="-1">Select</option>');
                            for (j = 0; j <= aryValTxtObject.length - 1; j++) {
                                $(ddl).append('<option value="' + aryValTxtObject[j].val + '">' + aryValTxtObject[j].text + '</option>');
                            }
                        }
                    }
                }
            }
        },
        /**
        * Bind the dropdowns with the options provided
        * @param {Array of jqueryObjects} ddls
        * @param {Array of {text,val} object}options
        */
        bindDropdowns: function(ddls,options){
            var i=0,
                j=0,
                $ddl =$([]);
            if($.isArray(ddls) &&  $.isArray(options) && ddls.length>0){
                for( i=0;i<=ddls.length-1;i++){
                    $ddl = ddls[i];
                    $ddl.find('option').remove();
                    for (j = 0; j <= options.length - 1; j++) {
                        $ddl.append('<option value="' + options[j].val + '">' + options[j].text + '</option>');
                    }
                }
            }
        },
        getCommandObjectsForDdlBinding: function (commandType/*ideCommandObjectTypes*/) {
            if (!commandType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var aryCmdObjs = [];
            switch (commandType) {
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                    aryCmdObjs = _dbCommandHelpers.getObjectsForDdlBind();
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                    aryCmdObjs = _wsCommandHelpers.getObjectsForDdlBind(MF_IDE_CONSTANTS.webServiceType.wsdlSoap);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                    aryCmdObjs = _wsCommandHelpers.getObjectsForDdlBind(MF_IDE_CONSTANTS.webServiceType.http);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                    aryCmdObjs = _wsCommandHelpers.getObjectsForDdlBind(MF_IDE_CONSTANTS.webServiceType.xmlRpc);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                    aryCmdObjs = _odataCommandHelpers.getObjectsForDdlBind(MF_IDE_CONSTANTS.webServiceType.xmlRpc);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                    aryCmdObjs = _offlineDtHelpers.getObjectsForDdlBind();
                break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                    aryCmdObjs = _offlineDtHelpers.getObjectsForDdlBind();
                break;    
            }
            return aryCmdObjs;
        },
        getInputParamsForCmdObject: function (cmdId, commandType/*ideCommandObjectTypes*/) {
            if (!commandType || !cmdId) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var aryInputParams = [];
            switch (commandType) {
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                    aryInputParams = _dbCommandHelpers.getInputParamsForCmd(cmdId);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                    aryInputParams = _wsCommandHelpers.getInputParamsForCmd(cmdId,
                        MF_IDE_CONSTANTS.webServiceType.wsdlSoap);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                    aryInputParams = _wsCommandHelpers.getInputParamsForCmd(cmdId,
                        MF_IDE_CONSTANTS.webServiceType.http);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                    aryInputParams = _wsCommandHelpers.getInputParamsForCmd(cmdId,
                        MF_IDE_CONSTANTS.webServiceType.xmlRpc);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                    aryInputParams = _odataCommandHelpers.getInputParamsForCmd(cmdId);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                    aryInputParams = _offlineDtHelpers.getDatatableColsNames(cmdId);
                break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                    aryInputParams = _offlineDtHelpers.getInputParamsForCmd(cmdId);
                break;    
            }
            return aryInputParams;
        },
        getCommandTypeByValue: function (val) {
            if (!val) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var type = "";
            switch (val) {
                //TOCHANGE MOHAN
                case "0":
                    type = MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                break;//TOCHANGE MOHAN
                case "1":
                    type = MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
                    break;
                case "2":
                    type = MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                    break;
                case "3":
                    type = MF_IDE_CONSTANTS.ideCommandObjectTypes.http;
                    break;
                case "4":
                    type = MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc;
                    break;
                case "5":
                    type = MF_IDE_CONSTANTS.ideCommandObjectTypes.odata;
                    break;
                case "6":
                    type = MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj;
                    break;
                case "7":
                    type = MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt;
                break;    
            }
            return type;
        },
        getCommandObjByCmdId: function (commandType/*MF_IDE_CONSTANTS.ideCommandObjectTypes*/, cmdIds) {
            if (!commandType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var aryCmdObjs = [];
            switch (commandType) {
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                    aryCmdObjs = _dbCommandHelpers.getObjectsByCmdId(cmdIds);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                    break;
            }
            return aryCmdObjs;
        },
        getCommandObjectByIdAndType:function(id,cmdType){
            var objCmdObject = null;
            switch (cmdType) {
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                        objCmdObject = _dbCommandHelpers.getObjectByCmdId(id);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                        objCmdObject = _wsCommandHelpers.getWsObjectById(id);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                        objCmdObject = _odataCommandHelpers.getOdataObjectById(id);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                    objCmdObject = _offlineDtHelpers.getDataObjByObjectId(id);
                break;    
            }
            return objCmdObject;
        },
        getViewConnectionTypeByValue: function (val) {
            if (!val) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var connType = null;
            switch (val) {
                case "1":
                    connType = MF_IDE_CONSTANTS.viewDestConnectionType.next;
                    break;
                case "2":
                    connType = MF_IDE_CONSTANTS.viewDestConnectionType.rowClick;
                    break;
                case "3":
                    connType = MF_IDE_CONSTANTS.viewDestConnectionType.actionButton1;
                    break;
                case "4":
                    connType = MF_IDE_CONSTANTS.viewDestConnectionType.actionButton2;
                    break;
                case "5":
                    connType = MF_IDE_CONSTANTS.viewDestConnectionType.actionButton3;
                    break;
                case "6":
                    connType = MF_IDE_CONSTANTS.viewDestConnectionType.condition;
                    break;
                case "70":
                    connType = MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton1;
                break;
                case "71":
                    connType = MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton2;
                break;
                case "72":
                    connType = MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton3;
                break;
                case "73":
                    connType = MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton4;
                break;
                case "74":
                    connType = MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton5;
                break;
                case "75":
                    connType = MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton6;
                break;
                case "8":
                    connType = MF_IDE_CONSTANTS.viewDestConnectionType.multiple;
                break;
                case "9":
                    connType = MF_IDE_CONSTANTS.viewDestConnectionType.button;
                break;
            }
            return connType;
        },
        getConnectionIdOfCmd: function (cmdId, commandType/*ideCommandObjectTypes*/) {
            if (!commandType || !cmdId) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var strConnectionId = [];
            switch (commandType) {
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                    strConnectionId = _dbCommandHelpers.getConnectionIdOfCmd(cmdId);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                    break;
            }
            return strConnectionId;
        },
        getDbCmdsForDdlBindInBatchProcMode: function (connectionId) {
            return _dbCommandHelpers.getDbCmdsForDdlBindInBatchProcMode(connectionId);
        },
        getActionBtnOperationTypeByVal: function (value) {
            if (!value) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var operationType = "";
            switch (value) {
                case "0":
                    operationType = MF_IDE_CONSTANTS.actionBtnOperationType.command;
                    break;
                case "1":
                    operationType = MF_IDE_CONSTANTS.actionBtnOperationType.transition;
                    break;
            }
        },
        mfErrorHelper: (function () {
            return {
                getHtmlListOfErrors: function (errors/*array*/) {
                    var strHtmlList = "";
                    if (errors && $.isArray(errors) && errors.length > 0) {
                        strHtmlList = '<ul class="errors">';
                        $.each(errors, function (index, value) {
                            strHtmlList += '<li>' + value + '</li>';
                        });
                        strHtmlList += '</ul>';
                    }
                    return strHtmlList;
                }

            };
        })(),
        appsContainerObjectHelper: (function () {
            return {
                getAppsContainerObject: function () {
                    return mFicientIde.MfApp.appsContainer;
                },
                getTabletAppFromContainer: function () {
                    return this.getAppsContainerObject().fnGetTabletApp();
                },
                getPhoneAppFromContainer: function () {
                    return this.getAppsContainerObject().fnGetPhoneApp();
                },
                setAppsContainerObject: function (contObject) {
                    if (!contObject) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                    if (contObject instanceof AppsContainer) {
                        mFicientIde.MfApp.appsContainer = contObject;
                    }
                    else {
                        throw MF_IDE_CONSTANTS.exceptionMessage.argumentsInvalid;
                    }
                },
                setAppsContainerCurrentObjectType: function (appType/*IDE_APP_MODEL_TYPE*/) {
                    if (!objectAppType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                    var appCont = this.getAppsContainerObject();
                    appCont.fnAddAppType(appType);
                },
                createInterfaceForTabletOnDiffInterfaceTrue: function () {
                    var appsContainer = this.getAppsContainerObject();
                    if (appsContainer) {
                        appsContainer.fnCreateEmptyTabletAppOnInterfaceChange();
                    }
                },
                createInterfaceForPhoneOnRunsonChange: function () {
                    var appsContainer = this.getAppsContainerObject();
                    if (appsContainer) {
                        appsContainer.fnCreateEmptyPhoneAppOnRunsOnChange();
                    }
                },
                deleteTabletApp: function () {
                    var appsContainer = this.getAppsContainerObject();
                    if (appsContainer) {
                        appsContainer.fnDeleteTabletApp();
                    }
                },
                deletePhoneApp: function () {
                    var appsContainer = this.getAppsContainerObject();
                    if (appsContainer) {
                        appsContainer.fnDeletePhoneApp();
                    }
                },
                makeTabletOnlyInterface: function () {
                    var objAppsContainer = this.getAppsContainerObject();
                    objAppsContainer.fnMakeTabletOnlyInterface();
                },
                makePhoneTabletWithSameInterface: function () {
                    var appsContainer = this.getAppsContainerObject();
                    if (appsContainer) {
                        appsContainer.fnChangeToPhnTabWithSameInterface();
                    }
                },
                destroyContainerOnAppClose:function () {
                    var objContainerObject = this.getAppsContainerObject();
                    if(objContainerObject)
                    {
                        objContainerObject.phoneApp = null;
                        objContainerObject.tabletApp = null;
                        objContainerObject.currentWorkingAppType = null;
                    }
                    mFicientIde.MfApp.appsContainer = null;
                }
            };
        })(),
        getAllWsObjects: function () {
            return _getAllWsObjects();
        },
        getAllOdataObjects: function () {
            return _odataCommandHelpers.getAllOdataObjects();
        },
        getAlldbObjects: function () {
            return _dbCommandHelpers.getAllDbObjects();
        },
        intlsenseHelper: (function ($) {
            var _numberRegex = /^[+-]?\d+(\.\d+)?$/;
            function _isValueInFinalIntlsProperties(val/*string*/, intlsArray, finalPropertiesIntls) {
                if (!intlsArray || !val) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                var aryAllPossibleIntlsValue = [],aryRegexes=[],isValid = false;
                if (!finalPropertiesIntls) {
                    finalPropertiesIntls = this.getAllPossibleFinalIntlsProperty(intlsArray); 
                    if(objIntlsValueWithRegexes){
                        aryAllPossibleIntlsValue = finalPropertiesIntls.validTxtValues;
                        aryRegexes = finalPropertiesIntls.regexes;
                    }
                }
                else {
                    aryAllPossibleIntlsValue = finalPropertiesIntls.validTxtValues;
                    aryRegexes = finalPropertiesIntls.regexes;
                }
                if ($.inArray(val, aryAllPossibleIntlsValue) !== -1) {
                    isValid = true;
                }
                else {
                    for(var i=0;i<=aryRegexes.length-1;i++){
                        var pattern = aryRegexes[i];
                        if(pattern.test(val)){
                            isValid = true;
                            break;
                        }
                    }
                    
                    //if((/\w*\.\w*\[\d\]\.Column\[\w*\]/).test(val)){
                        
                    //}
                }
                return isValid;
                // if (!finalPropertiesIntlArray || ($.isArray(finalPropertiesIntlArray) && 
                //         finalPropertiesIntlArray.length === 0)) {
                //     aryAllPossibleIntlsValue = this.getAllPossibleFinalIntlsProperty();
                // }
                // else {
                //     aryAllPossibleIntlsValue = finalPropertiesIntlArray;
                // }
                // if ($.inArray(val, aryAllPossibleIntlsValue) !== -1) {
                //     return true;
                // }
                // else {
                //     return false;
                // }
                
            }
            function _getPossibleTextValsFromObject(propObject, previousStringEvluted /*string*/) {
                var aryFinalCombintionsOfText = [];
                var aryOfCombinationsInnerProp = [];
                if ($.isPlainObject(propObject) && propObject.hasOwnProperty("val")) {
                    previousStringEvluted = addTextFromPropObject(propObject, previousStringEvluted);
                    if (propObject.hasOwnProperty("ip")) {
                        aryOfCombinationsInnerProp = _loopThroughInnerProperty(propObject.ip, previousStringEvluted);
                    }
                }
                if (aryOfCombinationsInnerProp.length > 0) {
                    for (var i = 0; i <= aryOfCombinationsInnerProp.length - 1; i++) {
                        aryFinalCombintionsOfText.push(aryOfCombinationsInnerProp[i]);
                    }
                    return aryFinalCombintionsOfText;
                }
                else {
                    //aryFinalCombintionsOfText.push(previousStringEvluted);
                    return previousStringEvluted;
                }

            }
            function addTextFromPropObject(propObject, prevuousPropText) {
                if (prevuousPropText === "") {
                    return propObject.val;
                }
                else {
                    return prevuousPropText + "." + propObject.val;
                }

            }
            function _loopThroughInnerProperty(innerPropArryObject, previousPropTextEvaluated) {
                var aryOfCombinationsInnerProp = [];

                if ($.isArray(innerPropArryObject) && innerPropArryObject.length > 0) {
                    for (var i = 0; i <= innerPropArryObject.length - 1; i++) {
                        var strStrtString = previousPropTextEvaluated;
                        if ($.isPlainObject(innerPropArryObject[i])) {
                            var aryValsFromObject = _getPossibleTextValsFromObject(innerPropArryObject[i], strStrtString);
                            if(aryValsFromObject && $.isArray(aryValsFromObject) && aryValsFromObject.length>0){
                                for(var k=0;k<=aryValsFromObject.length-1;k++){
                                    aryOfCombinationsInnerProp.push(aryValsFromObject[k]);
                                }
                            }
                            else{
                                if(typeof aryValsFromObject === "string"){
                                    aryOfCombinationsInnerProp.push(aryValsFromObject);
                                }
                            }
                            
                        }

                    }
                }
                return aryOfCombinationsInnerProp;
                //var aryOfCombinationsInnerProp = [];

                //if ($.isArray(innerPropArryObject) && innerPropArryObject.length > 0) {
                   // for (var i = 0; i <= innerPropArryObject.length - 1; i++) {
                     //   var strStrtString = previousPropTextEvaluated;
                      //  if ($.isPlainObject(innerPropArryObject[i])) {
                           // aryOfCombinationsInnerProp.push(_getPossibleTextValsFromObject(innerPropArryObject[i], strStrtString));
                        //}

                    //}
                //}
                //return aryOfCombinationsInnerProp;
            }
            function _getEnterpriseAdditionalUserProps(){
                var $hidAdditionalUserProps = $('[id $="hidCompanyAdditionalDtls"]');
                var strAdditionalProps = $hidAdditionalUserProps.val(),
                    aryUserAdditionalProps=[];
                if(strAdditionalProps){
                    aryUserAdditionalProps = $.parseJSON(strAdditionalProps);
                }
                return aryUserAdditionalProps;              
            }
            function _getEnterpriseUserList(){
                var $hidUserLists = $('[id $="hidCompanyUsers"]');
                var strHidUsers = $hidUserLists.val(),
                    aryUsers=[];
                if(strHidUsers){
                    aryUsers = $.parseJSON(strHidUsers);
                }
                return aryUsers;                
            }   
            function _getEnterpriseGroupsList(){
                var $hidUserGroupLists = $('[id $="hidCompanyGroups"]');
                var strUsersGrpList = $hidUserGroupLists.val(),
                    aryUserGroups=[];
                if(strUsersGrpList){
                    aryUserGroups = $.parseJSON(strUsersGrpList);
                }
                return aryUserGroups;               
            }
            //Helper function called from setAppIntellisenseToCache
            function _getDatasetInnerParamsAsIntellisenseProps(outputParams){
                var i=0,
                    aryInnerParams = [];
                aryInnerParams.push({
                    prop: "Column[ColumnName]",
                    val: "Column[ColumnName]",
                    category: "Select Template"
                });    
                if($.isArray(outputParams) && outputParams.length>0){
                    for(i=0;i<= outputParams.length-1;i++){
                        aryInnerParams.push({
                            prop: "Column["+outputParams[i]+"]",
                            val: "Column["+outputParams[i]+"]",
                            category: "Select Template"
                        });
                    }
                }
                aryInnerParams.push({
                    prop: "RowCount",
                    val: "RowCount",
                    category: "Select Template"
                });
                return aryInnerParams;
            }
            return {
                getAllPossibleFinalIntlsProperty: function (intlsArray) {
                    var aryFinalCombintionsOfText = [];
                    var aryArtOfFinalPropertirs = [];
                    var aryIntlsFronJson = intlsArray;
                    var strVlue = "",strViewName="",strDatasetName="",strCntrlName;
                    var i = 0,k = 0,pattern;
                    var aryValsThatNeedsRegex = [],aryRegexes =[];
                    var aryOfPossibleCombintionFromObject = [];
                    if (aryIntlsFronJson.length > 0) {
                        for (i = 0; i <= aryIntlsFronJson.length - 1; i++) {
                            var strPropFinalText = "";
                            aryOfPossibleCombintionFromObject = [];
                            
                            aryOfPossibleCombintionFromObject = 
                                    _getPossibleTextValsFromObject(
                                        aryIntlsFronJson[i],
                                        strPropFinalText);
                            
                            if (aryOfPossibleCombintionFromObject) {
                                if (typeof aryOfPossibleCombintionFromObject === "string") {
                                    aryArtOfFinalPropertirs.push(aryOfPossibleCombintionFromObject);
                                }
                                else if ($.isArray(aryOfPossibleCombintionFromObject)) {
                                    for (k = 0; k <= aryOfPossibleCombintionFromObject.length - 1; k++) {
                                        aryArtOfFinalPropertirs.push(aryOfPossibleCombintionFromObject[k]);
                                    }
                                }
                            }
                        }
                    }
                    i = 0;
                    for (i = 0; i <= aryArtOfFinalPropertirs.length - 1; i++) {
                        var aryOfFinlCOmbination = aryArtOfFinalPropertirs[i];
                        if (typeof aryOfFinlCOmbination === "string") {
                            aryFinalCombintionsOfText.push(aryOfFinlCOmbination);
                        }
                        else if ($.isArray(aryOfFinlCOmbination)) {
                            for (k = 0; k <= aryOfFinlCOmbination.length - 1; k++) {
                                aryFinalCombintionsOfText.push(aryOfFinlCOmbination[k]);
                            }
                        }
                    }
                    //return aryFinalCombintionsOfText;
                    var objFinalCombinationWithRegex = null;
                    if(aryFinalCombintionsOfText && $.isArray(aryFinalCombintionsOfText) &&
                        aryFinalCombintionsOfText.length>0){
                        
                        aryValsThatNeedsRegex = $.grep(aryFinalCombintionsOfText,function(val,index){
                                return (
                                        (/\w+\.\w+\.Column\[\w*\]/).test(val) ||
                                        (/\w+\.\w+\.Format\(\".*\"\)/).test(val) ||
                                        (/Datatables\.\w+\.\w{3}\[\"\w*\"\]/)
                                );
                        });
                    }
                    i=0;
                    if(aryValsThatNeedsRegex && aryValsThatNeedsRegex.length>0){
                        for(i=0;i<=aryValsThatNeedsRegex.length-1;i++){
                            var intlsValue = aryValsThatNeedsRegex[i];
                            var aryIntlsValue = intlsValue.split('.');
                            if((/\w+\.\w+\.Column\[\w*\]/).test(intlsValue)){
                                strViewName = aryIntlsValue[0];
                                strDatasetName = aryIntlsValue[1].split('[')[0];
                                pattern = new RegExp(strViewName+'.'+strDatasetName+'.Column'+'\\[\\w*\\]');
                                aryRegexes.push(pattern);
                            }
                            else if((/\w+\.\w+\.Format\(\".*\"\)/).test(intlsValue)){
                                strViewName = aryIntlsValue[0];
                                strCntrlName = aryIntlsValue[1];
                                pattern = new RegExp(strViewName+'.'+strCntrlName+'.Format\\(\\"\.*\\"\\)');
                                aryRegexes.push(pattern);
                            }
                            else if((/Datatables\.\w+\.\w{3}\[\"\w*\"\]/).test(intlsValue)){
                                strViewName = aryIntlsValue[0];
                                strCntrlName = aryIntlsValue[1];
                                pattern = new RegExp(strViewName+'.'+strCntrlName+'.Max\\[\\"\.*\\"\\]');
                                aryRegexes.push(pattern);
                                pattern = new RegExp(strViewName+'.'+strCntrlName+'.Min\\[\\"\.*\\"\\]');
                                aryRegexes.push(pattern);
                            }
                        }
                    }
                    objFinalCombinationWithRegex ={
                        validTxtValues : aryFinalCombintionsOfText,
                        regexes :aryRegexes
                    };
                    return objFinalCombinationWithRegex;
                },
                getTextForUI: function (val) {
                    var strReturn = "";
                    if (val) {
                        if (val.startsWith('_$')) {
                            strReturn = val.substring(2);
                        }
                        else if (val.startsWith('_@')) {
                            strReturn = val.substring(1);
                        }
                        else if(_numberRegex.test(val)){
                            strReturn = val;
                        }
                        else {
                            strReturn = "\"" + val + "\"";
                        }
                    }
                    return strReturn;
                },
                getTextForObjectProperty: function (val) {
                    var strReturnVal = "";
                    if (typeof val === 'string') {
                        val = val.trim();
                        if (val === "") {
                            strReturnVal = val;
                        }
                        else if (val.startsWith("\"") && val.endsWith("\"")) {
                            strReturnVal = val.substring(1, val.length - 1);
                        }
                        else if(_numberRegex.test(val)){
                            strReturnVal = val;
                        }
                        else if (val.startsWith('@')) {
                            strReturnVal = '_'+val; 
                        }
                        else {
                            strReturnVal = '_$' + val;
                        }
                        // else if{ //TODO CHECK REGEXES

                        // }
                        // else{
                        // var objError = new MfError(["Invalid value"]);
                        // throw objError;
                        // }
                    }
                    else {
                        throw MF_IDE_CONSTANTS.exceptionMessage.argumentsInvalid;
                    }
                    return strReturnVal;
                },
                getAppIntellisenseJson: function (app, addFixedParam) {
                    var aryIntellisense = [];
                    if (addFixedParam == null || addFixedParam === true) {
                        aryIntellisense = mFicientIde.Cache.getIntlsense(true);
                    }
                    else {
                        aryIntellisense = mFicientIde.Cache.getIntlsense(false);
                    }
                    return aryIntellisense;
                },
                getIntellisenseJsonForUsername: function (app, addFixedParam) {
                    var aryIntellisense = [];
                    aryIntellisense = mFicientIde.Cache.getIntlsenseForUsername();
                    return aryIntellisense;
                },
                getIntellisenseJsonForGroupName: function () {
                    var aryIntellisense = [];
                    aryIntellisense = mFicientIde.Cache.getIntlsenseForGroupName();
                    return aryIntellisense;
                },
                getIntellisenseJsonForViewInit: function () {
                    var aryIntellisense = [];
                    aryIntellisense = mFicientIde.Cache.getIntlsenseForViewInit();
                    return aryIntellisense;
                },
                isValueValid: function (options) {
                    //ignoreUserConst : false means userConstants are allowed
                    //THis has to be changed.It should be isUserConstAllowed. Could not change it right now without checking
                    //{val:"",ignoreUserConst:false/*constants in double qoutes*/,isEmptyValid,intlsArray,intlsAllCombination}
                    if (options.val === "") {
                        return options.isEmptyValid;
                    }
                    else if (options.val.startsWith("\"") && options.val.endsWith("\"")) {
                        if (options.ignoreUserConst == undefined || options.ignoreUserConst === false) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    else if(_numberRegex.test(options.val)){
                        if (options.ignoreUserConst == undefined || options.ignoreUserConst === false) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    else {
                        return _isValueInFinalIntlsProperties(options.val, options.intlsArray, options.intlsAllCombination);
                    }
                },
                getTextForXReference: function (val) {
                    var strReturn = "";
                    if (val && val.startsWith('_$')) {
                        strReturn = val.substring(2);
                    }
                    return strReturn;
                },
                setAppIntellisenseToCache:function(app){
                    if (!app) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                    var aryViews = app.fnGetAllViews(),
                        aryCondViews = app.fnGetAllCondViews();
                    var aryIntellisense = [],
                        aryIntellisenseForViewInit = [];
                    
                    var blnIsRptRowClickEnabled=false,
                        blnIsActBtn1Transition =false,
                        blnIsActBtn2Transition = false,
                        blnIsActBtn3Transition = false,
                        blnIsNextBtnTrue = false,
                        aryViewCmdTasksDatabindingObjs = [],
                        k = 0, i = 0,j = 0,l=0,
                        aryUserAdditionalProps=[],
                        aryIntlsAdditionalUserProps=[],
                        aryIntlsCopy =[],
                        aryUsers=[],
                        aryGroups = [],
                        aryIntlsUsers=[],
                        aryIntlsGroups=[],
                        aryClonedIntellisense=[],
                        aryEditableListProps=[],
                        aryDestinationConds=[],
                        blnIsPartOfNormalIntls = false,
                        blnIsPartOfInitIntls = false,
                        objCondView = null,
                        objDestCond = null,
                        objDatabindingObj = null,
                        objProp = null,
                        objInnerProp = null,
                        objPropForViewInit  = null,
                        objViewActionBtn= null,
                        aryViewActionBtns = [],
                        aryFormActionBtns = [],
                        objFormActionBtn = null,
                        aryCustomTriggers =[],
                        objCustomTrigger = null,
                        strStartupViewId = app.fnGetStartupViewId();
                    
                    aryUserAdditionalProps = _getEnterpriseAdditionalUserProps();
                    if($.isArray(aryUserAdditionalProps)   && 
                        aryUserAdditionalProps.length>0){
                            for(i = 0;i<=aryUserAdditionalProps.length-1;i++){ 
                                aryIntlsAdditionalUserProps.push({
                                    prop:aryUserAdditionalProps[i],
                                    val :aryUserAdditionalProps[i],
                                    category:"User Detail"
                                    });
                            }
                    }
                    aryUsers = _getEnterpriseUserList();
                    if($.isArray(aryUsers)     && 
                        aryUsers.length>0){
                            for(i = 0;i<=aryUsers.length-1;i++){ 
                                aryIntlsUsers.push({
                                    prop:aryUsers[i],
                                    val :aryUsers[i],
                                    category:"Select User"
                                    });
                            }
                    }
                    aryGroups = _getEnterpriseGroupsList();
                    if($.isArray(aryGroups)    && 
                        aryGroups.length>0){
                            for(i = 0;i<=aryGroups.length-1;i++){ 
                                aryIntlsGroups.push({
                                    prop:aryGroups[i],
                                    val :aryGroups[i],
                                    category:"Select Group"
                                    });
                            }
                        }       
                    if ($.isArray(aryViews) && aryViews.length > 0) {
                        for ( i = 0; i <= aryViews.length - 1; i++) {
                            var objView = aryViews[i];
                            var objCmd = null;
                            //For normal Intellisense
                            objProp  = new Object();
                            objProp.prop = objView.name;
                            objProp.val = objView.name;
                            objProp.category = "Select View";
                            objProp.ip = [];
                            
                            //For View Initialization Intellisense
                             objPropForViewInit  = new Object();
                             objPropForViewInit.prop = objView.name;
                             objPropForViewInit.val = objView.name;
                             objPropForViewInit.category = "Select View";
                             objPropForViewInit.ip = [];
                            
                            var cntrlType = null;
                            var objViewForm = MF_HELPERS.currentObjectHelper.getFormFromApp(objView.formId);
                            if (objViewForm) {
                                var aryControls = objViewForm.fnGetAllControls();
                                //var aryControls = getCntrlsOfForm(objView.formId);
                                if (aryControls && $.isArray(aryControls) && aryControls.length > 0) {
                                    for ( j = 0; j <= aryControls.length - 1; j++) {
                                        var objControl = aryControls[j];
                                        cntrlType =MF_HELPERS.getControlTypeById(objControl.id);
                                        
                                        if(cntrlType){
                                            if(_isControlPartOfIntellisense(cntrlType)){
                                                blnIsPartOfNormalIntls = true;
                                            }
                                            else{
                                                blnIsPartOfNormalIntls = false;
                                            }
                                            if(_canControlBeUsedForInitialization(cntrlType)){
                                                blnIsPartOfInitIntls = true;
                                            }
                                            else{
                                                blnIsPartOfInitIntls = false;
                                            }
                                            
                                            //If it is part of any one then only we need to go through that control
                                            if(blnIsPartOfNormalIntls === true || blnIsPartOfInitIntls === true){
                                                
                                                objInnerProp = new Object();
                                                objInnerProp.prop = objControl.userDefinedName;
                                                objInnerProp.val = objControl.userDefinedName;
                                                if(cntrlType === MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST){
                                                    aryEditableListProps = objControl.fnGetAllPropNames();
                                                    if(aryEditableListProps &&
                                                        $.isArray(aryEditableListProps) &&
                                                        aryEditableListProps.length>0){
                                                           
                                                           objInnerProp.ip=[];
                                                           for(l=0;l<=aryEditableListProps.length-1;l++) {
                                                               objInnerProp.ip.push({
                                                                   "prop":aryEditableListProps[l],
                                                                   "val":aryEditableListProps[l],
                                                                   "category" : "Select Property"
                                                               });
                                                           }
                                                        }
                                                        
                                                }
                                                else if (cntrlType && cntrlType === MF_IDE_CONSTANTS.CONTROLS.LIST){
                                                    
                                                    blnIsRptRowClickEnabled  = objView.fnIsRptRowClickEnabledForForm();
                                                    blnIsActBtn1Transition = objView.fnIsActionBtn1OprTypeTransition();
                                                    blnIsActBtn2Transition = objView.fnIsActionBtn2OprTypeTransition();
                                                    blnIsActBtn3Transition = objView.fnIsActionBtn3OprTypeTransition();
                                                        
                                                    if(blnIsRptRowClickEnabled ||
                                                        blnIsActBtn1Transition ||
                                                        blnIsActBtn2Transition ||
                                                        blnIsActBtn3Transition ){
                                                        
                                                            objInnerProp = new Object();
                                                            objInnerProp.prop = objControl.userDefinedName;
                                                            objInnerProp.val = objControl.userDefinedName;
                                                            objInnerProp.ip = cntrlType.propForIntlSense;
                                                            objInnerProp.category = "Select Controls";
                                                            //objProp.ip.push(objInnerProp);
                                                    }   
                                                }
                                                else{
                                                    objInnerProp.ip = cntrlType.propForIntlSense;
                                                }    
                                                objInnerProp.category = "Select Controls";
                                                if(blnIsPartOfNormalIntls === true){
                                                    objProp.ip.push(objInnerProp);
                                                }
                                                if(blnIsPartOfInitIntls === true){
                                                    objPropForViewInit.ip.push(objInnerProp);
                                                }
                                            }
                                            
                                        }
                                    }
                                }
                            }
                            
                            if(blnIsRptRowClickEnabled){
                                aryViewCmdTasksDatabindingObjs = objView.fnGetRowClickTasksDatabindingObjs();
                                if(aryViewCmdTasksDatabindingObjs &&
                                    $.isArray(aryViewCmdTasksDatabindingObjs) 
                                    && aryViewCmdTasksDatabindingObjs.length>0){
                                        
                                        for (k = 0; k <= aryViewCmdTasksDatabindingObjs.length - 1; k++) {
                                            objCmd = aryViewCmdTasksDatabindingObjs[k];
                                            if(objCmd.dsName){
                                                objInnerProp = new Object();
                                                objInnerProp.prop = objCmd.dsName;
                                                objInnerProp.val =objCmd.dsName;
                                                objInnerProp.category = "Select Dataset";
                                                //objInnerProp.ip = [];
                                                objInnerProp.ip = _getDatasetInnerParamsAsIntellisenseProps(
                                                    MF_HELPERS.getOutputParamsForCmdObject(
                                                        objCmd.fnGetId(),
                                                        objCmd.fnGetBindTypeObject()
                                                    )    
                                                );
                                                // objInnerProp.ip.push({
                                                //     prop: "Column[ColumnName]",
                                                //     val: "Column[ColumnName]",
                                                //     category: "Select Template"
                                                // },
                                                // {
                                                //     prop: "RowCount",
                                                //     val: "RowCount",
                                                //     category: "Select Template"
                                                // });
                                                objProp.ip.push(objInnerProp);
                                                objPropForViewInit.ip.push(objInnerProp);
                                                // objInnerProp = new Object();
                                                // objInnerProp.prop = objCmd.dsName;
                                                // objInnerProp.val =objCmd.dsName;
                                                // objInnerProp.category = "Select Dataset";
                                                // objInnerProp.ip = [];
                                                // objInnerProp.ip.push({
                                                //     prop: "RowCount",
                                                //     val: "RowCount",
                                                //     category: "Select Template"
                                                // });
                                                // objProp.ip.push(objInnerProp);
                                            }
                                        }
                                    
                                    }
                            }
                            blnIsNextBtnTrue = objView.fnIsNextButtonTrueForView();
                            if(blnIsNextBtnTrue){
                                aryViewCmdTasksDatabindingObjs = objView.fnGetOnExitCmdDatabindingObjs();
                                if(aryViewCmdTasksDatabindingObjs &&
                                    $.isArray(aryViewCmdTasksDatabindingObjs) 
                                    && aryViewCmdTasksDatabindingObjs.length>0){
                                        
                                        for (k = 0; k <= aryViewCmdTasksDatabindingObjs.length - 1; k++) {
                                            objCmd = aryViewCmdTasksDatabindingObjs[k];
                                            if(objCmd.dsName){
                                                objInnerProp = new Object();
                                                objInnerProp.prop = objCmd.dsName;
                                                objInnerProp.val =objCmd.dsName;
                                                objInnerProp.category = "Select Dataset";
                                                // objInnerProp.ip = [];
                                                // objInnerProp.ip.push({
                                                //     prop: "Column[ColumnName]",
                                                //     val: "Column[ColumnName]",
                                                //     category: "Select Template"
                                                // },
                                                // {
                                                //     prop: "RowCount",
                                                //     val: "RowCount",
                                                //     category: "Select Template"
                                                // });
                                                objInnerProp.ip = _getDatasetInnerParamsAsIntellisenseProps(
                                                    MF_HELPERS.getOutputParamsForCmdObject(
                                                        objCmd.fnGetId(),
                                                        objCmd.fnGetBindTypeObject()
                                                    )    
                                                );
                                                objProp.ip.push(objInnerProp);
                                                objPropForViewInit.ip.push(objInnerProp);
                                                // objInnerProp = new Object();
                                                // objInnerProp.prop = objCmd.dsName;
                                                // objInnerProp.val =objCmd.dsName;
                                                // objInnerProp.category = "Select Dataset";
                                                // objInnerProp.ip = [];
                                                // objInnerProp.ip.push({
                                                //     prop: "RowCount",
                                                //     val: "RowCount",
                                                //     category: "Select Template"
                                                // });
                                                // objProp.ip.push(objInnerProp);
                                            }
                                        }
                                    
                                    }
                            }
                            
                            if(blnIsActBtn1Transition){
                                aryViewCmdTasksDatabindingObjs = objView.fnGetActionBtn1CmdDatabindingObjs();
                                if(aryViewCmdTasksDatabindingObjs &&
                                    $.isArray(aryViewCmdTasksDatabindingObjs) 
                                    && aryViewCmdTasksDatabindingObjs.length>0){
                                        
                                        for (k = 0; k <= aryViewCmdTasksDatabindingObjs.length - 1; k++) {
                                            objCmd = aryViewCmdTasksDatabindingObjs[k];
                                            if(objCmd.dsName){
                                                objInnerProp = new Object();
                                                objInnerProp.prop = objCmd.dsName;
                                                objInnerProp.val =objCmd.dsName;
                                                objInnerProp.category = "Select Dataset";
                                                // objInnerProp.ip = [];
                                                // objInnerProp.ip.push({
                                                //     prop: "Column[ColumnName]",
                                                //     val: "Column[ColumnName]",
                                                //     category: "Select Template"
                                                // },
                                                // {
                                                //     prop: "RowCount",
                                                //     val: "RowCount",
                                                //     category: "Select Template"
                                                // });
                                                objInnerProp.ip = _getDatasetInnerParamsAsIntellisenseProps(
                                                    MF_HELPERS.getOutputParamsForCmdObject(
                                                        objCmd.fnGetId(),
                                                        objCmd.fnGetBindTypeObject()
                                                    )    
                                                );
                                                objProp.ip.push(objInnerProp);
                                                objPropForViewInit.ip.push(objInnerProp);
                                                // objInnerProp = new Object();
                                                // objInnerProp.prop = objCmd.dsName;
                                                // objInnerProp.val =objCmd.dsName;
                                                // objInnerProp.category = "Select Dataset";
                                                // objInnerProp.ip = [];
                                                // objInnerProp.ip.push({
                                                //     prop: "RowCount",
                                                //     val: "RowCount",
                                                //     category: "Select Template"
                                                // });
                                                // objProp.ip.push(objInnerProp);
                                            }
                                        }
                                    
                                    }
                            }
                            if(blnIsActBtn2Transition){
                                aryViewCmdTasksDatabindingObjs = objView.fnGetActionBtn2CmdDatabindingObjs();
                                if(aryViewCmdTasksDatabindingObjs &&
                                    $.isArray(aryViewCmdTasksDatabindingObjs) 
                                    && aryViewCmdTasksDatabindingObjs.length>0){
                                        
                                        for (k = 0; k <= aryViewCmdTasksDatabindingObjs.length - 1; k++) {
                                            objCmd = aryViewCmdTasksDatabindingObjs[k];
                                            if(objCmd.dsName){
                                                objInnerProp = new Object();
                                                objInnerProp.prop = objCmd.dsName;
                                                objInnerProp.val =objCmd.dsName;
                                                objInnerProp.category = "Select Dataset";
                                                // objInnerProp.ip = [];
                                                // objInnerProp.ip.push({
                                                //     prop: "Column[ColumnName]",
                                                //     val: "Column[ColumnName]",
                                                //     category: "Select Template"
                                                // },
                                                // {
                                                //     prop: "RowCount",
                                                //     val: "RowCount",
                                                //     category: "Select Template"
                                                // });
                                                objInnerProp.ip = _getDatasetInnerParamsAsIntellisenseProps(
                                                    MF_HELPERS.getOutputParamsForCmdObject(
                                                        objCmd.fnGetId(),
                                                        objCmd.fnGetBindTypeObject()
                                                    )    
                                                );
                                                objProp.ip.push(objInnerProp);
                                                objPropForViewInit.ip.push(objInnerProp);
                                                // objInnerProp = new Object();
                                                // objInnerProp.prop = objCmd.dsName;
                                                // objInnerProp.val =objCmd.dsName;
                                                // objInnerProp.category = "Select Dataset";
                                                // objInnerProp.ip = [];
                                                // objInnerProp.ip.push({
                                                //     prop: "RowCount",
                                                //     val: "RowCount",
                                                //     category: "Select Template"
                                                // });
                                                // objProp.ip.push(objInnerProp);
                                            }
                                        }
                                    
                                    }
                            }
                            if(blnIsActBtn2Transition){
                                aryViewCmdTasksDatabindingObjs = objView.fnGetActionBtn3CmdDatabindingObjs();
                                if(aryViewCmdTasksDatabindingObjs &&
                                    $.isArray(aryViewCmdTasksDatabindingObjs) 
                                    && aryViewCmdTasksDatabindingObjs.length>0){
                                        
                                        for (k = 0; k <= aryViewCmdTasksDatabindingObjs.length - 1; k++) {
                                            objCmd = aryViewCmdTasksDatabindingObjs[k];
                                            if(objCmd.dsName){
                                                objInnerProp = new Object();
                                                objInnerProp.prop = objCmd.dsName;
                                                objInnerProp.val =objCmd.dsName;
                                                objInnerProp.category = "Select Dataset";
                                                // objInnerProp.ip = [];
                                                // objInnerProp.ip.push({
                                                //     prop: "Column[ColumnName]",
                                                //     val: "Column[ColumnName]",
                                                //     category: "Select Template"
                                                // },{
                                                //       prop: "RowCount",
                                                //       val: "RowCount",
                                                //       category: "Select Template"
                                                //   });
                                                objInnerProp.ip = _getDatasetInnerParamsAsIntellisenseProps(
                                                    MF_HELPERS.getOutputParamsForCmdObject(
                                                        objCmd.fnGetId(),
                                                        objCmd.fnGetBindTypeObject()
                                                    )    
                                                );
                                                objProp.ip.push(objInnerProp);
                                                objPropForViewInit.ip.push(objInnerProp);
                                                // objInnerProp = new Object();
                                                // objInnerProp.prop = objCmd.dsName;
                                                // objInnerProp.val =objCmd.dsName;
                                                // objInnerProp.category = "Select Dataset";
                                                // objInnerProp.ip = [];
                                                // objInnerProp.ip.push({
                                                //     prop: "RowCount",
                                                //     val: "RowCount",
                                                //     category: "Select Template"
                                                // });
                                                // objProp.ip.push(objInnerProp);
                                            }
                                        }
                                    
                                    }
                            }
                            
                            if(objView.fnGetId() === strStartupViewId){
                                aryViewCmdTasksDatabindingObjs = app.fnGetAppStartupDatabindingObjs();
                                if(aryViewCmdTasksDatabindingObjs &&
                                    $.isArray(aryViewCmdTasksDatabindingObjs) 
                                    && aryViewCmdTasksDatabindingObjs.length>0){
                                        
                                        for (k = 0; k <= aryViewCmdTasksDatabindingObjs.length - 1; k++) {
                                            objCmd = aryViewCmdTasksDatabindingObjs[k];
                                            if(objCmd.dsName){
                                                objInnerProp = new Object();
                                                objInnerProp.prop = objCmd.dsName;
                                                objInnerProp.val =objCmd.dsName;
                                                objInnerProp.category = "Select Dataset";
                                                // objInnerProp.ip = [];
                                                // objInnerProp.ip.push({
                                                //     prop: "Column[ColumnName]",
                                                //     val: "Column[ColumnName]",
                                                //     category: "Select Template"
                                                // },
                                                // {
                                                //     prop: "RowCount",
                                                //     val: "RowCount",
                                                //     category: "Select Template"
                                                // });
                                                objInnerProp.ip = _getDatasetInnerParamsAsIntellisenseProps(
                                                    MF_HELPERS.getOutputParamsForCmdObject(
                                                        objCmd.fnGetId(),
                                                        objCmd.fnGetBindTypeObject()
                                                    )    
                                                );
                                                objProp.ip.push(objInnerProp);
                                                objPropForViewInit.ip.push(objInnerProp);
                                                
                                            }
                                        }
                                    
                                    }
                            }
                        
                            aryViewActionBtns = objView.fnGetViewActionBtns();
                            k=0;j=0;
                            if($.isArray(aryViewActionBtns) && aryViewActionBtns.length>0){
                                for(k=0;k<=aryViewActionBtns.length-1;k++){
                                    objViewActionBtn = aryViewActionBtns[k];
                                    
                                    aryViewCmdTasksDatabindingObjs = 
                                       objViewActionBtn.fnGetAllOnExitDatabindingObjs();
                                    
                                    if($.isArray(aryViewCmdTasksDatabindingObjs) 
                                       && aryViewCmdTasksDatabindingObjs.length>0){
                                        
                                        for (j = 0; j <= aryViewCmdTasksDatabindingObjs.length - 1; j++) {
                                            objDatabindingObj = aryViewCmdTasksDatabindingObjs[j];
                                            if(objDatabindingObj && objDatabindingObj.fnGetDatasetName()){
                                                objInnerProp = {
                                                   prop : objDatabindingObj.fnGetDatasetName(),
                                                   val : objDatabindingObj.fnGetDatasetName(),
                                                   category : "Select Dataset",
                                                   ip :[]
                                                };
                                                // objInnerProp.ip.push({
                                                //     prop: "Column[ColumnName]",
                                                //     val: "Column[ColumnName]",
                                                //     category: "Select Template"
                                                // },
                                                // {
                                                //     prop: "RowCount",
                                                //     val: "RowCount",
                                                //     category: "Select Template"
                                                // });
                                                
                                                objInnerProp.ip = _getDatasetInnerParamsAsIntellisenseProps(
                                                    MF_HELPERS.getOutputParamsForCmdObject(
                                                        objDatabindingObj.fnGetId(),
                                                        objDatabindingObj.fnGetBindTypeObject()
                                                    )    
                                                );
                                                objProp.ip.push(objInnerProp);
                                                objPropForViewInit.ip.push(objInnerProp);
                                            }
                                        }
                                    }   
                                }
                            }
                            
                            aryFormActionBtns = objView.fnGetFormActionBtns();
                            k=0;j=0;
                            if($.isArray(aryFormActionBtns) && aryFormActionBtns.length>0){
                                for(k=0;k<=aryFormActionBtns.length-1;k++){
                                    objFormActionBtn = aryFormActionBtns[k];
                                    
                                    aryViewCmdTasksDatabindingObjs = 
                                       objFormActionBtn.fnGetAllOnExitDatabindingObjs();
                                    
                                    if($.isArray(aryViewCmdTasksDatabindingObjs) 
                                       && aryViewCmdTasksDatabindingObjs.length>0){
                                        
                                        for (j = 0; j <= aryViewCmdTasksDatabindingObjs.length - 1; j++) {
                                            objDatabindingObj = aryViewCmdTasksDatabindingObjs[j];
                                            if(objDatabindingObj && objDatabindingObj.fnGetDatasetName()){
                                                objInnerProp = {
                                                   prop : objDatabindingObj.fnGetDatasetName(),
                                                   val : objDatabindingObj.fnGetDatasetName(),
                                                   category : "Select Dataset",
                                                   ip :[]
                                                };
                                                // objInnerProp.ip.push({
                                                //     prop: "Column[ColumnName]",
                                                //     val: "Column[ColumnName]",
                                                //     category: "Select Template"
                                                // },
                                                // {
                                                //     prop: "RowCount",
                                                //     val: "RowCount",
                                                //     category: "Select Template"
                                                // });
                                                // objProp.ip.push(objInnerProp);
                                                objInnerProp.ip = _getDatasetInnerParamsAsIntellisenseProps(
                                                    MF_HELPERS.getOutputParamsForCmdObject(
                                                        objDatabindingObj.fnGetId(),
                                                        objDatabindingObj.fnGetBindTypeObject()
                                                    )    
                                                );
                                                objProp.ip.push(objInnerProp);
                                                objPropForViewInit.ip.push(objInnerProp);
                                            }
                                        }
                                    }   
                                }
                            }
                            
                            aryCustomTriggers = objView.fnGetCustomTriggers();
                            k=0;j=0;
                            if($.isArray(aryCustomTriggers) && aryCustomTriggers.length>0){
                                for(k=0;k<=aryCustomTriggers.length-1;k++){
                                    objCustomTrigger = aryCustomTriggers[k];
                                    
                                    aryViewCmdTasksDatabindingObjs = 
                                       objCustomTrigger.fnGetAllOnExitDatabindingObjs();
                                    
                                    if($.isArray(aryViewCmdTasksDatabindingObjs) 
                                       && aryViewCmdTasksDatabindingObjs.length>0){
                                        
                                        for (j = 0; j <= aryViewCmdTasksDatabindingObjs.length - 1; j++) {
                                            objDatabindingObj = aryViewCmdTasksDatabindingObjs[j];
                                            if(objDatabindingObj && objDatabindingObj.fnGetDatasetName()){
                                                objInnerProp = {
                                                   prop : objDatabindingObj.fnGetDatasetName(),
                                                   val : objDatabindingObj.fnGetDatasetName(),
                                                   category : "Select Dataset",
                                                   ip :[]
                                                };
                                                objInnerProp.ip = _getDatasetInnerParamsAsIntellisenseProps(
                                                    MF_HELPERS.getOutputParamsForCmdObject(
                                                        objDatabindingObj.fnGetId(),
                                                        objDatabindingObj.fnGetBindTypeObject()
                                                    )    
                                                );
                                                objProp.ip.push(objInnerProp);
                                                objPropForViewInit.ip.push(objInnerProp);
                                            }
                                        }
                                    }   
                                }
                            }
                            
                            
                            aryIntellisense.push(objProp);
                            aryIntellisenseForViewInit.push(objPropForViewInit);
                        }
                        
                    }
                    
                    if($.isArray(aryCondViews) && aryCondViews.length>0){
                        i=0;
                        k=0;
                        j=0;
                        for(i=0;i<=aryCondViews.length-1;i++){
                            objCondView  = aryCondViews[i];
                            
                            objProp = {
                                prop : objCondView.fnGetName(),
                                val : objCondView.fnGetName(),
                                category : "Select Condition",
                                ip : []
                            };
                            
                            //For View Initialization Intellisense
                            objPropForViewInit  ={
                                prop : objCondView.fnGetName(),
                                val : objCondView.fnGetName(),
                                category : "Select Condition",
                                ip : []
                            };
                            
                            aryDestinationConds = objCondView.fnGetDestinationConds();
                            
                            if( $.isArray(aryDestinationConds) 
                                && aryDestinationConds.length>0){
                                    
                                for(k=0;k<=aryDestinationConds.length-1;k++){
                                    
                                    objDestCond =aryDestinationConds[k];
                                    
                                    aryViewCmdTasksDatabindingObjs = 
                                      objDestCond.fnGetAllOnExitDatabindingObjs();
                                    
                                    if($.isArray(aryViewCmdTasksDatabindingObjs) 
                                        && aryViewCmdTasksDatabindingObjs.length>0){
                                            
                                            for (j = 0; j <= aryViewCmdTasksDatabindingObjs.length - 1; j++) {
                                                objDatabindingObj = aryViewCmdTasksDatabindingObjs[j];
                                                if(objDatabindingObj && objDatabindingObj.fnGetDatasetName()){
                                                    objInnerProp = {
                                                       prop : objDatabindingObj.fnGetDatasetName(),
                                                       val : objDatabindingObj.fnGetDatasetName(),
                                                       category : "Select Dataset",
                                                       ip :[]
                                                    };
                                                    // objInnerProp.ip.push({
                                                    //     prop: "Column[ColumnName]",
                                                    //     val: "Column[ColumnName]",
                                                    //     category: "Select Template"
                                                    // },
                                                    // {
                                                    //     prop: "RowCount",
                                                    //     val: "RowCount",
                                                    //     category: "Select Template"
                                                    // });
                                                    objInnerProp.ip = _getDatasetInnerParamsAsIntellisenseProps(
                                                        MF_HELPERS.getOutputParamsForCmdObject(
                                                            objDatabindingObj.fnGetId(),
                                                            objDatabindingObj.fnGetBindTypeObject()
                                                        )    
                                                    );
                                                    objProp.ip.push(objInnerProp);
                                                    objPropForViewInit.ip.push(objInnerProp);
                                                }
                                            }
                                        }
                                }
                                    
                            }
                            
                            aryIntellisense.push(objProp);
                            aryIntellisenseForViewInit.push(objPropForViewInit);
                        }
                    }
                    
                    aryClonedIntellisense = MF_HELPERS.createCloneOfArray(aryIntellisense);
                    
                    $.merge(aryIntellisenseForViewInit,aryIntlsAdditionalUserProps);
                    //.merge(aryIntellisenseForViewInit,_offlineDtHelpers.getDatatablesForIntellisense());
                    $.merge(aryIntellisenseForViewInit, MF_IDE_CONSTANTS.intlsenseFixedParams);
                    
                    //Initialisation and PushNotification has similar intellisense values
                    mFicientIde.Cache.setIntlsenseValForViewInit(aryIntellisenseForViewInit);
                    
                    $.merge(aryIntellisense,aryIntlsAdditionalUserProps);
                    //$.merge(aryIntellisense,_offlineDtHelpers.getDatatablesForIntellisense());
                    mFicientIde.Cache.setIntlsenseVal(aryIntellisense,false);
                    
                    $.merge(aryIntellisense, MF_IDE_CONSTANTS.intlsenseFixedParams);
                    //I think this is not required.
                    //$.merge(aryIntellisense,_offlineDtHelpers.getDatatablesForIntellisense());
                    mFicientIde.Cache.setIntlsenseVal(aryIntellisense,true);
                    
                    aryIntellisense = MF_HELPERS.createCloneOfArray(aryClonedIntellisense);
                    $.merge(aryIntellisense, aryIntlsUsers);
                    //$.merge(aryIntellisense,_offlineDtHelpers.getDatatablesForIntellisense());
                    mFicientIde.Cache.setIntlsenseValForUsername(aryIntellisense);
                    
                    aryIntellisense = MF_HELPERS.createCloneOfArray(aryClonedIntellisense);
                    $.merge(aryIntellisense, aryIntlsGroups);
                    //$.merge(aryIntellisense,_offlineDtHelpers.getDatatablesForIntellisense());
                    mFicientIde.Cache.setIntlsenseValForGroupName(aryIntellisense);
                },
                changeHtmlOfTxtBoxForValidity :function(txtBox/*jquery object*/,isValid){
                    if(isValid){
                        txtBox.removeClass('errorTxtBox');
                    }
                    else{
                        txtBox.addClass('errorTxtBox');
                    }
                },
                isIntlCntrlValueValidInCondLogic:function(txtVal,intlJsonString){
                    var aryIntls = [],
                        objAllPossibleCombinationsFromIntls = null,
                        blnIsValid = false;
                    if(intlJsonString){
                        aryIntls = $.parseJSON(intlJsonString);
                    }
                    if(aryIntls && $.isArray(aryIntls) && aryIntls.length>0){
                        objAllPossibleCombinationsFromIntls = 
                            this.getAllPossibleFinalIntlsProperty(aryIntls);
                    }
                    blnIsValid = this.isValueValid(
                        {
                            val:txtVal,
                            ignoreUserConst:false,
                            isEmptyValid:true,
                            intlsArray :aryIntls,
                            intlsAllCombination : objAllPossibleCombinationsFromIntls
                        }
                    );
                    return blnIsValid;
                },
                getControlDatabindingIntellisenseJson:function(
                    form,controlId,addFixedParam){
                    
                    if(!form || !controlId ){
                        throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                    }
                    if(addFixedParam == null)addFixedParam = false;
                    var aryControls = form.fnGetControlsForIntellisense(controlId),
                        aryTemp = [],
                        i=0,
                        objControl = null,
                        strJson = "";
                    if(aryControls && $.isArray(aryControls) && aryControls.length>0){
                        for(i=0;i<=aryControls.length-1;i++){
                            objControl = aryControls[i];
                            aryTemp.push({
                                cntrlnm : objControl.userDefinedName,
                                type :objControl.type
                            });
                        }
                       strJson = MF_IDE_CONSTANTS.
                                getIntellisenseControlJson(
                                    JSON.stringify(aryTemp)
                                );
                        if(strJson){
                           aryControls = $.parseJSON(strJson); 
                        }
                        else{
                            aryControls = [];
                        }
                    }
                    else{
                        aryControls = [];
                    }
                    if(addFixedParam){
                        $.merge(aryControls,MF_IDE_CONSTANTS.intlsenseFixedParams);
                    }
                    return aryControls;
                }     
                
            };
        })(jQuery),
        xReferenceHelper: (function () {
            function _setXRefValuesForDatabindingObjs(viewId,oldSettings, newSettings, type) {
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                if (!objApp) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                var objView = null, aryCmdParams = [], xReference = null, cntrlXRef = null;
                if (oldSettings && $.isArray(oldSettings) && oldSettings.length > 0) {
                    $.each(oldSettings, function (index, value) {
                        aryCmdParams = value.cmdParams;
                        $.each(aryCmdParams, function (i, v) {
                            var mappedVal = MF_HELPERS.intlsenseHelper.getTextForXReference(v.val);
                            if (mappedVal) {
                                var arySplitVal = mappedVal.split('.');
                                if (arySplitVal.length > 2) {
                                    objView = objApp.fnGetViewByName(arySplitVal[0]);
                                    //if (!objView) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                                    //xReference = objView.xReference;
                                    //if(!xReference)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                                    if(objView){
                                        cntrlXRef = objView.fnGetCntrlXRef(arySplitVal[1]);
                                        if (cntrlXRef) {
                                            cntrlXRef.fnRemoveViewBySettingType(viewId, type);
                                        }
                                    }    
                                }
                            }
                        });
                    });
                }
                if (newSettings && $.isArray(newSettings) && newSettings.length > 0) {
                    $.each(newSettings, function (index, value) {
                        aryCmdParams = value.cmdParams;
                        $.each(aryCmdParams, function (i, v) {
                            var mappedVal = MF_HELPERS.intlsenseHelper.getTextForXReference(v.val);
                            if (mappedVal) {
                                var arySplitVal = mappedVal.split('.');
                                if (arySplitVal.length > 2) {
                                    objView = objApp.fnGetViewByName(arySplitVal[0]);
                                    //if (!objView) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                                    if(objView){
                                        cntrlXRef = objView.fnGetCntrlXRef(arySplitVal[1]);
                                        if (cntrlXRef) {
                                            cntrlXRef.fnAddViewBySettingType(viewId, type);
                                        }
                                        else {
                                            cntrlXRef = new CntrlXReference({
                                                name: arySplitVal[1],
                                                exit: [],
                                                init: [],
                                                actionBtn1: [],
                                                actionBtn2: [],
                                                actionBtn3: [],
                                                rowClickTask: [],
                                                cancelBtnTask: [],
                                                appStartup: [],
                                                cond :[],
                                                buttons:[],
                                                customTriggers:[]
                                            });
                                            cntrlXRef.fnAddViewBySettingType(viewId, type);
                                            objView.fnAddCntrlXRef(cntrlXRef);
                                        }
                                    }    
                                }
                            }
                        });
                    });
                }
            }
            function _setXRefValuesForOnEnterInits(viewId,oldOnEnterDtls,newOnEnterDtls){
                
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                if (!objApp) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                
                var objView = null, aryCntrlInits = [],
                xReference = null,
                cntrlXRef = null,
                k=0,objViewOnEnter=null,i=0,
                mappedVal = "",
                arySplitVal=[],
                viewTitle="";
                viewParameter = "";
                
                if (oldOnEnterDtls && 
                    $.isArray(oldOnEnterDtls) && 
                    oldOnEnterDtls.length > 0) {
                    
                    $.each(oldOnEnterDtls, function (index, value) {
                        objViewOnEnter = value;
                        if (objViewOnEnter) {
                            aryCntrlInits = objViewOnEnter.fnGetCntrlInits();
                            viewTitle = objViewOnEnter.fnGetTitle();
                            viewParameter = objViewOnEnter.fnGetParameter();
//                            mappedVal= MF_HELPERS.intlsenseHelper.getTextForXReference(viewTitle);
//                            if (mappedVal) {
//                                arySplitVal = mappedVal.split('.');
//                                if (arySplitVal.length > 2) {
//                                    objView = objApp.fnGetViewByName(arySplitVal[0]);
//                                    //if (!objView) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
//                                    if(objView){
//                                        cntrlXRef = objView.fnGetCntrlXRef(arySplitVal[1]);
//                                        if (cntrlXRef) {
//                                            cntrlXRef.fnRemoveViewBySettingType(
//                                                viewId,
//                                                MF_IDE_CONSTANTS.viewSettingsType.onEnter);
//                                        }
//                                    }
//                                }
//                            }

                            mappedVal= MF_HELPERS.intlsenseHelper.getTextForXReference(viewParameter);
                            if (mappedVal) {
                                arySplitVal = mappedVal.split('.');
                                if (arySplitVal.length > 2) {
                                    objView = objApp.fnGetViewByName(arySplitVal[0]);
                                    //if (!objView) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                                    if(objView){
                                        cntrlXRef = objView.fnGetCntrlXRef(arySplitVal[1]);
                                        if (cntrlXRef) {
                                            cntrlXRef.fnRemoveViewBySettingType(
                                                viewId,
                                                MF_IDE_CONSTANTS.viewSettingsType.onEnter);
                                        }
                                    }
                                }
                            }

                            if (aryCntrlInits &&
                                $.isArray(aryCntrlInits) &&
                                aryCntrlInits.length > 0) {
                                
                                $.each(aryCntrlInits, function (index, value) {
                                    if(value.controlValue != undefined && value.controlValue.length > 0) {
                                         for(i=0;i<= value.controlValue.length-1;i++){
                                            mappedVal= MF_HELPERS.intlsenseHelper.getTextForXReference(value.controlValue[i]);
                                            if (mappedVal) {
                                                arySplitVal = mappedVal.split('.');
                                                if (arySplitVal.length > 2) {
                                                    objView = objApp.fnGetViewByName(arySplitVal[0]);
                                                    //if (!objView) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                                                    //xReference = objView.xReference;
                                                    //if(!xReference)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                                                    if(objView){
                                                        cntrlXRef = objView.fnGetCntrlXRef(arySplitVal[1]);
                                                        if (cntrlXRef) {
                                                            cntrlXRef.fnRemoveViewBySettingType(
                                                                viewId,
                                                                MF_IDE_CONSTANTS.viewSettingsType.onEnter);
                                                        }
                                                    }    
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        
                        }
                    });
                }
                if (newOnEnterDtls && $.isArray(newOnEnterDtls) && newOnEnterDtls.length > 0) {
                    $.each(newOnEnterDtls, function (index, value) {
                        objViewOnEnter = value;
                        if (objViewOnEnter){ 
                            aryCntrlInits = objViewOnEnter.fnGetCntrlInits();
                            viewTitle = objViewOnEnter.fnGetTitle();
                            viewParameter = objViewOnEnter.fnGetParameter();

//                            mappedVal= MF_HELPERS.intlsenseHelper.getTextForXReference(viewTitle);
//                            if (mappedVal) {
//                                arySplitVal = mappedVal.split('.');
//                                if (arySplitVal.length > 2) {
//                                    objView = objApp.fnGetViewByName(arySplitVal[0]);
//                                    //if (!objView) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
//                                    if(objView){ 
//                                        cntrlXRef = objView.fnGetCntrlXRef(arySplitVal[1]);
//                                        if (cntrlXRef) {
//                                                cntrlXRef.fnAddViewBySettingType(
//                                                    viewId,
//                                                    MF_IDE_CONSTANTS.viewSettingsType.onEnter
//                                                );
//                                        }
//                                        else {
//                                            cntrlXRef = new CntrlXReference({
//                                                name: arySplitVal[1],
//                                                exit: [],
//                                                init: [],
//                                                actionBtn1: [],
//                                                actionBtn2: [],
//                                                actionBtn3: [],
//                                                rowClickTask: [],
//                                                cancelBtnTask: [],
//                                                appStartup: []
//                                            });
//                                            cntrlXRef.fnAddViewBySettingType(viewId, MF_IDE_CONSTANTS.viewSettingsType.onEnter);
//                                            objView.fnAddCntrlXRef(cntrlXRef);
//                                        }
//                                    }    
//                                }
//                            }

                            mappedVal= MF_HELPERS.intlsenseHelper.getTextForXReference(viewParameter);
                            if (mappedVal) {
                                arySplitVal = mappedVal.split('.');
                                if (arySplitVal.length > 2) {
                                    objView = objApp.fnGetViewByName(arySplitVal[0]);
                                    //if (!objView) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                                    if(objView){ 
                                        cntrlXRef = objView.fnGetCntrlXRef(arySplitVal[1]);
                                        if (cntrlXRef) {
                                                cntrlXRef.fnAddViewBySettingType(
                                                    viewId,
                                                    MF_IDE_CONSTANTS.viewSettingsType.onEnter
                                                );
                                        }
                                        else {
                                            cntrlXRef = new CntrlXReference({
                                                name: arySplitVal[1],
                                                exit: [],
                                                init: [],
                                                actionBtn1: [],
                                                actionBtn2: [],
                                                actionBtn3: [],
                                                rowClickTask: [],
                                                cancelBtnTask: [],
                                                appStartup: [],
                                                cond :[],
                                                buttons :[],
                                                customTriggers:[]
                                            });
                                            cntrlXRef.fnAddViewBySettingType(viewId, MF_IDE_CONSTANTS.viewSettingsType.onEnter);
                                            objView.fnAddCntrlXRef(cntrlXRef);
                                        }
                                    }    
                                }
                            }

                            if (aryCntrlInits &&
                                $.isArray(aryCntrlInits) &&
                                aryCntrlInits.length > 0) { 
                         
                            $.each(aryCntrlInits, function (index, value) {
                                if(value.controlValue != undefined && value.controlValue.length > 0) {
                                 for(i=0;i<= value.controlValue.length-1;i++){
                                        mappedVal = MF_HELPERS.intlsenseHelper.getTextForXReference(value.controlValue[i]);
                                        if (mappedVal) {
                                            arySplitVal = mappedVal.split('.');
                                            if (arySplitVal.length > 2) {
                                                objView = objApp.fnGetViewByName(arySplitVal[0]);
                                                //if (!objView) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                                                if(objView){
                                                    cntrlXRef = objView.fnGetCntrlXRef(arySplitVal[1]);
                                                    if (cntrlXRef) {
                                                        cntrlXRef.fnAddViewBySettingType(
                                                            viewId,
                                                            MF_IDE_CONSTANTS.viewSettingsType.onEnter
                                                        );
                                                    }
                                                    else {
                                                        cntrlXRef = new CntrlXReference({
                                                            name: arySplitVal[1],
                                                            exit: [],
                                                            init: [],
                                                            actionBtn1: [],
                                                            actionBtn2: [],
                                                            actionBtn3: [],
                                                            rowClickTask: [],
                                                            cancelBtnTask: [],
                                                            appStartup: [],
                                                            cond:[],
                                                            buttons:[],
                                                            customTriggers:[]
                                                        });
                                                        cntrlXRef.fnAddViewBySettingType(viewId, MF_IDE_CONSTANTS.viewSettingsType.onEnter);
                                                        objView.fnAddCntrlXRef(cntrlXRef);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                    
                            });
                        }
                    
                        }
                    });
                }
            }
            function _setXRefValuesForOnCondition(condId,
                        oldOnCondDtls/*onCondition object*/,
                        newOnCondDtls/*onCondition object*/){
                
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                if (!objApp) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                
                var objView = null, aryDestinationConds = [],
                xReference = null,
                cntrlXRef = null,
                k=0,objDestinationCond=null,
                mappedVal = "",
                arySplitVal=[];
                
                if (oldOnCondDtls) {
                    aryDestinationConds = oldOnCondDtls.destinationCond;
                    if(aryDestinationConds && 
                        $.isArray(aryDestinationConds) &&
                        aryDestinationConds.length>0){
                        
                        $.each(aryDestinationConds, function (index, value) {
                            mappedVal= MF_HELPERS.intlsenseHelper.getTextForXReference(value.compareCntrl);
                            if (mappedVal) {
                                arySplitVal = mappedVal.split('.');
                                if (arySplitVal.length > 2) {
                                    objView = objApp.fnGetViewByName(arySplitVal[0]);
                                    //if (!objView) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                                    if(objView){
                                        cntrlXRef = objView.fnGetCntrlXRef(arySplitVal[1]);
                                        if (cntrlXRef) {
                                            cntrlXRef.fnRemoveViewFromCondSettings(
                                                condId);
                                        }
                                    }    
                                }
                            }
                        });
                        
                    }
                }
                if (newOnCondDtls) {
                     aryDestinationConds = newOnCondDtls.destinationCond;
                    if(aryDestinationConds && 
                        $.isArray(aryDestinationConds) &&
                        aryDestinationConds.length>0){
                        
                        $.each(aryDestinationConds, function (index, value) {
                                mappedVal = MF_HELPERS.intlsenseHelper.getTextForXReference(value.compareCntrl);
                                if (mappedVal) {
                                    arySplitVal = mappedVal.split('.');
                                    if (arySplitVal.length > 2) {
                                        objView = objApp.fnGetViewByName(arySplitVal[0]);
                                        //if (!objView) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                                        if(objView){
                                            cntrlXRef = objView.fnGetCntrlXRef(arySplitVal[1]);
                                            if (cntrlXRef) {
                                                cntrlXRef.fnAddViewToConditionSetting(
                                                    condId
                                                );
                                            }
                                            else {
                                                cntrlXRef = new CntrlXReference({
                                                    name: arySplitVal[1],
                                                    exit: [],
                                                    init: [],
                                                    actionBtn1: [],
                                                    actionBtn2: [],
                                                    actionBtn3: [],
                                                    rowClickTask: [],
                                                    cancelBtnTask: [],
                                                    appStartup: [],
                                                    cond:[],
                                                    buttons:[],
                                                    customTriggers:[]
                                                });
                                                cntrlXRef.fnAddViewToConditionSetting(condId);
                                                objView.fnAddCntrlXRef(cntrlXRef);
                                            }
                                        }    
                                    }
                                }
                                mappedVal = MF_HELPERS.intlsenseHelper.getTextForXReference(value.condValue);
                                if (mappedVal) {
                                    arySplitVal = mappedVal.split('.');
                                    if (arySplitVal.length > 2) {
                                        objView = objApp.fnGetViewByName(arySplitVal[0]);
                                        //if (!objView) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                                        if(objView){
                                            cntrlXRef = objView.fnGetCntrlXRef(arySplitVal[1]);
                                            if (cntrlXRef) {
                                                cntrlXRef.fnAddViewToConditionSetting(
                                                    condId
                                                );
                                            }
                                            else {
                                                cntrlXRef = new CntrlXReference({
                                                    name: arySplitVal[1],
                                                    exit: [],
                                                    init: [],
                                                    actionBtn1: [],
                                                    actionBtn2: [],
                                                    actionBtn3: [],
                                                    rowClickTask: [],
                                                    cancelBtnTask: [],
                                                    appStartup: [],
                                                    cond:[],
                                                    buttons:[],
                                                    customTriggers:[]
                                                });
                                                cntrlXRef.fnAddViewToConditionSetting(condId);
                                                objView.fnAddCntrlXRef(cntrlXRef);
                                            }
                                        }
                                    }
                                }
                            });
                        
                    
                        }
                    }
            }
            return {
                setXRefValueForDatabindingObjs: function (viewId/**/,
                            oldSettings/*DatabindingObjs*/,
                            newSettings/*DatabindingObjs*/,
                            type/*MF_IDE_CONSTANTS.viewSettingsType*/) {
                       
                       _setXRefValuesForDatabindingObjs(viewId,oldSettings, newSettings, type);
                },
                setXRefValueForOnEnterInits: function (viewId/**/,
                            oldSettings/*ViewOnEnter*/,
                            newSettings/*ViewOnEnter*/) {
                       
                       _setXRefValuesForOnEnterInits(viewId,oldSettings, newSettings);
                },
                setXRefValueForOnCondition: function (viewId/*Condition view id*/,
                            oldSettings/*onCondition*/,
                            newSettings/*onCondition*/) {
                       
                       _setXRefValuesForOnCondition(viewId,oldSettings, newSettings);
                },
                setXrefValuesForApp:function(app){
                    if(!app)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                    var aryViews = app.fnGetAllViews(),
                        aryConditionViews = app.fnGetAllCondViews(),
                        objConditionView = null,
                        i = 0;
                        
                    var k = 0, j = 0, cmdParams = [], aryDatabindingObjs = [], aryXref = [],
                        objViewOnEnter = null, aryCntrlInits = [], objCntrInit = null,
                        aryOnEnterInits=[];
                    var databindingType = null,objView= null;    
                    if (aryViews && 
                        $.isArray(aryViews) && 
                        aryViews.length > 0) {
                        
                        for (i = 0; i <= aryViews.length - 1; i++) {
                            objView = aryViews[i];
                            for (k = 0; k <= 7; k++) {
                                switch (k) {
                                    case 0:
                                        aryDatabindingObjs = objView.fnGetOnExitCmdDatabindingObjs();
                                        databindingType = MF_IDE_CONSTANTS.viewSettingsType.onExit;
                                        break;
                                    case 1:
                                        aryDatabindingObjs = objView.fnGetCancelBtnCmdDatabindingObjs();
                                        databindingType = MF_IDE_CONSTANTS.viewSettingsType.cancelBtnTask;
                                        break;
                                    case 2:
                                        aryDatabindingObjs = objView.fnGetRowClickTasksDatabindingObjs();
                                        databindingType = MF_IDE_CONSTANTS.viewSettingsType.rowClickTask;
                                        break;
                                    case 3:
                                        aryDatabindingObjs = objView.fnGetActionBtn1CmdDatabindingObjs();
                                        databindingType = MF_IDE_CONSTANTS.viewSettingsType.actionBtn1;
                                        break;
                                    case 4:
                                        aryDatabindingObjs = objView.fnGetActionBtn2CmdDatabindingObjs();
                                        databindingType = MF_IDE_CONSTANTS.viewSettingsType.actionBtn2;
                                        break;
                                    case 5:
                                        aryDatabindingObjs = objView.fnGetActionBtn3CmdDatabindingObjs();
                                        databindingType = MF_IDE_CONSTANTS.viewSettingsType.actionBtn3;
                                        break;
                                    case 6:
                                        aryDatabindingObjs = objView.fnGetFormButtonsCmdDatabindingObjs();
                                        databindingType = MF_IDE_CONSTANTS.viewSettingsType.formActionButton;
                                    break;
                                    case 7:
                                        aryDatabindingObjs = objView.fnGetCustomTriggersCmdDatabindingObjs();
                                        databindingType = MF_IDE_CONSTANTS.viewSettingsType.customTriggers;
                                    break;    
                                }
                                if ($.isArray(aryDatabindingObjs) &&
                                    aryDatabindingObjs.length > 0) {
                                    
                                    this.setXRefValueForDatabindingObjs(objView.id,[],aryDatabindingObjs,databindingType);
                                }

                            }
                            //------------------------------------------------------------------------------
                            //-----On Enter----------
                            aryOnEnterInits = objView.fnGetOnEnterInits();
                            this.setXRefValueForOnEnterInits(objView.id,[],aryOnEnterInits);
                            // if (aryOnEnterInits && $.isArray(aryOnEnterInits)
                                    // && aryOnEnterInits.length > 0) {

                                // for (k = 0; k <= aryOnEnterInits.length - 1; k++) {
                                    // objViewOnEnter = aryOnEnterInits[k];
                                    // if (objViewOnEnter) {
                                        // aryCntrlInits = objViewOnEnter.fnGetCntrlInits();
                                        // if (aryCntrlInits && $.isArray(aryCntrlInits)
                                                // && aryCntrlInits.length > 0) {
                                            // this.setXRefValueForOnEnterInits(objView.id,[],aryCntrlInits);
                                        // }
                                    // }
                                // }
                            // }
                        

                        }
                    }
                
                    if(aryConditionViews &&
                        $.isArray(aryConditionViews) &&
                        aryConditionViews.length>0){
                        
                        for (i = 0; i <= aryConditionViews.length - 1; i++){
                            objConditionView = aryConditionViews[i];
                            if(objConditionView){
                                this.setXRefValueForOnCondition(objConditionView.id,null,objConditionView.fnGetOnCondition());
                            }    
                        }
                    }
                    
                    aryDatabindingObjs = app.fnGetAppStartupDatabindingObjs();
                    databindingType = MF_IDE_CONSTANTS.viewSettingsType.appStartUp;
                    
                    if ($.isArray(aryDatabindingObjs) &&
                        aryDatabindingObjs.length > 0) {
                        
                        this.setXRefValueForDatabindingObjs(objView.id,[],aryDatabindingObjs,databindingType);
                    }
                }
        
        
            };
        })(),
        createCloneOfArray:function (arrayToClone){
            return $.merge([], arrayToClone);
        },
        ideValidation: (function () {
            //Check string contains special character
            //Copied named changed. 
            function _valContainsSpecialCharacter(value) {
                var isExist = false;
                if (/[^A-Za-z\d]/.test(value)) {
                    isExist = true;
                }
                return isExist;
            }
            function _isWfFrmTitleValid(_Name) {
                var IsValid = false;
                if (/^[a-zA-Z0-9?&#()*\-\.\+,:\[\\\]\^_{}]+$/.test(_Name)) {
                    IsValid = true;
                }
                return IsValid;
            }
            //Validate name string
            function _validateNameInIde(name, testingFor) {
                var aryErrors = [];
                if (name.trim().length === 0)
                    aryErrors.push('Please enter ' + testingFor + ' name');
                if (name.trim().length > 20)
                    aryErrors.push(testingFor + ' name cannot be more than 20 charecters.');
                if (name.trim().length < 3)
                    aryErrors.push(testingFor + ' name cannot be less than 3 characters.');
                if (_valContainsSpecialCharacter(name))
                    aryErrors.push(testingFor + ' name cannot contain any special character.');
                if (!isNaN(Math.floor(name.substring(0, 1))))
                    aryErrors.push(testingFor + ' name cannot start with a number.');
                return aryErrors;
            }
            function _checkIdeAppName(name) {
                var isExist = false;
                if (!/^[a-z0-9-\s]+$/i.test(name)) {
                    isExist = true;
                }
                return isExist;
            }
            //Validate name app name
            function _validateAppName(_Name) {
                var aryErrors = [];
                if (_Name.trim().length === 0)
                    aryErrors.push('Please enter app name');
                else if (!isNaN(Math.floor(_Name.substring(0, 1))))
                    aryErrors.push('Please enter valid app name .Name cannot start with a number.');
                else if (_Name.trim().length > 30)
                    aryErrors.push('App name cannot be more than 30 characters.');
                else if (_Name.trim().length < 3)
                    aryErrors.push('App name cannot be less than 3 characters.');
                // else if (_checkIdeAppName(_Name))
                //     aryErrors.push('Please enter valid app name.');
                
                return aryErrors;
            }
            //Validate app name
            
            //Validate description in app
            function _validateAppDescription(description) {
                var aryErrors = [];
                if(description .length === 0){
                    aryErrors.push('Please enter app description.It cannot be less than 10 characters.');
                }
                else if (description.length < 10) {
                    aryErrors.push('Description cannot be less than 10 characters.');
                }
                // else if (!_isWfFrmTitleValid(description.trim())) {
                    // aryErrors.push('Please enter valid description.');
                // }
                return aryErrors;
            }
            
            //Validate title in app
            function _validateTitleInIde(_Title) {
                var aryErrors;
                if (_Title.trim().length === 0) {
                    aryErrors.push('Please enter valid title.');
                }
                else if (_Title.trim().length > 20) {
                    aryErrors.push('Title cannot be more than 20 characters.');
                }
                else if (_Title.trim().length < 3) {
                   aryErrors.push('Title name cannot be less than 3 characters.');
                }
                return aryErrors;
            }
            // //this sub is used to check title validations.
            
            // //this sub is used to validate view's button text.
            // function WfValidateButtonText(_Text) {
            //     var IsValid = false;
            //     if (/^[a-zA-Z0-9?&#()*\-\_\s]+$/.test(_Text)) {
            //         IsValid = true;
            //     }
            //     return IsValid;
            // }
            function rangeFromNegativeToNegative(val) {
                return /^\-\d+\-\-\d{0,9}$/.test(val);
            }
            function rangeFromNegativeToPositive(val) {
                return /^\-\d+\-\d{0,9}$/.test(val);
            }
            function rangeFromPositiveToPositive(val) {
                return /^\d+\-\d{0,9}$/.test(val);
            }
            
            function _validateFormName(name){
                var errors = [];
                if (!name) {
                    errors.push('Please enter valid form name.');
                }
                else if (_valContainsSpecialCharacter(name)) {
                    errors.push('Please enter valid form name.No special character allowed.');
            
                }
                else if (name.trim().length > 20) {
                    errors.push('Form name cannot be more than 20 characters.');
            
                }
                else if (name.trim().length < 3) {
                    errors.push('Form name cannot be less than 3 characters.');
                }
                return errors;
            }
            
            return {
                validateAppName: function (name/*string*/) {
                    return _validateAppName(name);
                },
                validateViewName: function (name/*string*/,
                   viewType ) {
                    
                    var strViewTypeText = "",
                        aryErrors = [];
                    switch(viewType){
                        case MF_IDE_CONSTANTS.draggableElementTypeEnum.form:
                            strViewTypeText = "View";
                            if(name && 
                                (name.toLowerCase() === "Datatables".toLowerCase())){
                               aryErrors.push('View name cannot be "Datatables".');  
                            }
                            break;
                        case MF_IDE_CONSTANTS.draggableElementTypeEnum.condition:
                            strViewTypeText = "Condition View";
                            break;
                        }
                    $.merge(aryErrors,_validateNameInIde(name,strViewTypeText));     
                    return aryErrors;
                },
                isNumericRange: function (value) {
                    var isValid = false;
                    if (rangeFromNegativeToNegative(value)) {
                        isValid = true;
                    }
                    else if (rangeFromNegativeToPositive(value)) {
                        isValid = true;
                    }
                    else if (rangeFromPositiveToPositive(value)) {
                        isValid = true;
                    }
                    return isValid;
                },
                valContainsSpecialCharacter: function (value) {
                    return _valContainsSpecialCharacter(value);
                },
                validateMultiplicationFact: function (value) {
                    var isValid = true;
                    if (value) {
                        if (!$.isNumeric(value) || parseFloat(value, 10) === 0) {
                            isValid = false;
                        }
                    }
                    else{
                        isValid = false;
                        }
                    return isValid;
                },
                validateAppDescription:function(description/*string*/){
                    return _validateAppDescription(description);
                },
                validateTitleInIde:function(title/*string*/){
                    return _validateTitleInIde(title);
                    },
                validateFormName:function(name){
                    return _validateFormName(name);
                }    
            };
        })(),
        //Tanika
        getWSTags: function (cmdId, wsType) {
            return _wsCommandHelpers.getWsTags(cmdId, wsType);
        },
        //Tanika
        getDbObjectsByCmdType: function (cmdType/*MF_IDE_CONSTANTS.DBCommandType*/) {
            return _dbCommandHelpers.getDbObjectsByCmdType(cmdType);
        },
        getControlTypeById:function (controlId){
            if(!controlId)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var cntrlPrefix = controlId.split('_')[0];
            var cntrlType = "";
            switch (cntrlPrefix.toUpperCase()) {
                case MF_IDE_CONSTANTS.CONTROLS.SECTION.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.SECTION;
                    break;
               case MF_IDE_CONSTANTS.CONTROLS.LABEL.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.LABEL;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.TEXTBOX;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.DROPDOWN;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.CHECKBOX;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.IMAGE.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.IMAGE;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.LIST.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.LIST;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.HIDDEN.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.HIDDEN;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.TOGGLE.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.TOGGLE;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.SLIDER.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.SLIDER;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.SEPARATOR.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.SEPARATOR;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.SPACER.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.SPACER;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.BARCODE.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.BARCODE;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.LOCATION.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.LOCATION;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.PIECHART.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.PIECHART;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.BARGRAPH.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.BARGRAPH;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.TABLE.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.TABLE;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.PICTURE.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.PICTURE;
                    break;
                    case MF_IDE_CONSTANTS.CONTROLS.SIGNATURE.idPrefix:
                        cntrlType = MF_IDE_CONSTANTS.CONTROLS.SIGNATURE;
                    break;
            }
            return cntrlType;
        },
        getAppModelTypeByValue:function(modelTypeVal){
            if(!modelTypeVal)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var modelType = MF_IDE_CONSTANTS.IDE_APP_MODEL_TYPE.phone;
            switch(modelTypeVal){
                case "0":
                    modelType = MF_IDE_CONSTANTS.IDE_APP_MODEL_TYPE.phone;
                break;
                case "1":
                    modelType = MF_IDE_CONSTANTS.IDE_APP_MODEL_TYPE.tablet;
                break;
            }
            return modelType;
        },
        appsFromClientStorage:{
            getAllAppsOptionsForDdlBinding :function(){
                return _appDetailsHelper.getAllAppsOptionsForDdlBinding();
            },
            getAppsOptionsForDdlBinding :function(ignoreApps/*array of app ids*/){
                return _appDetailsHelper.getAppsOptionsForDdlBinding(ignoreApps);
            },
            
        },
        getSubadminsForDdlBinding :function(){
                var $hidCompanySubAdmins = $('[id$="hidCompanySubAdmins"]'),
                strSubadminsJson = $hidCompanySubAdmins.val(),
                arySubadmins=[],
                strOptions="",
                i=0;
                if(strSubadminsJson){
                    arySubadmins = $.parseJSON(strSubadminsJson);
                    if(arySubadmins && $.isArray(arySubadmins) && arySubadmins.length>0){
                        for(i=0;i<=arySubadmins.length-1;i++){
                            var objSubAdmin = arySubadmins[i];
                            strOptions += "<option value= "+objSubAdmin.id+" >"+objSubAdmin.name+"</option>";
                        }
                    }        
                } 
                return strOptions;
        },
        showObjectDetails:{
            showDbObjectDetails:function(cmdId){
                if(!cmdId)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                var objCmd = _dbCommandHelpers.getObjectByCmdId(cmdId);
                    var $lblDbCmdName = $('[id$=lblDbCmdName]');
                    var $lblDbConnName = $('[id$=lblDbConnName]');
                    var $lblDbCmdTyp = $('[id$=lblDbCmdTyp]');
                    var $lblDbCmdTbl = $('[id$=lblDbCmdTbl]');
                    var $lblDbCmdSql = $('[id$=lblDbCmdSql]');
                    var  $lblDbDescription = $('[id$=lblDbDescription]');
                    $lblDbCmdName.html('');
                    $lblDbConnName.html('');
                    $lblDbCmdTyp.html('');
                    $lblDbCmdTbl.html('');
                    $lblDbCmdSql.html('');
                    $lblDbDescription.html('');
                if(objCmd){
                    $lblDbCmdName.html(objCmd.commandName);
                    $lblDbConnName.html(objCmd.connectionName);
                    $lblDbCmdTyp.html(_dbCommandHelpers.getDbObjectCmdTypeText(objCmd.commandType));
                    $lblDbCmdTbl.html(objCmd.tableName);
                    $lblDbCmdSql.html(objCmd.sqlQuery);
                    $lblDbDescription.html(objCmd.description);
                    showModalPopUp('SubprocDbCmdDetails', 'Object Details', 580,false,null,DialogType.Info);
                }
            },
            showWsObjectDetails :function(cmdId,wsType){
                if(!cmdId)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                var objCmd = _wsCommandHelpers.getWsObjectById(cmdId);
                $('[id$=lblWsCmdName]').html(objCmd.commandName);
                $('[id$=lblWsConnName]').html(objCmd.connectionName);
                $('[id$=lblWsSrvTyp]').html(wsType.UiName);
                $('[id$=lblWSDescription]').html(objCmd.description);
                var wsInputParams = mFicientIde.MF_HELPERS.getInputParamsForCmdObject(objCmd.commandId,wsType);
                switch (wsType) {
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                        $('[id$=lblWsSrvName]').html(objCmd.service);
                        $('[id$=lblWsMethod]').html(objCmd.method);
                        $('[id$=DivWsWsdlDet]').show();
                        $('[id$=DivWsHttpDet]').hide();
                        $('[id$=DivWsRpcDet]').hide();
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                        $('[id$=lblWsHName]').html(objCmd.service);
                        $('[id$=lblWsUrl]').html(objCmd.url);
                        var outputElementsTags = mFicientIde.MF_HELPERS.getWSTags(objCmd.commandId,MF_IDE_CONSTANTS.webServiceType.http);
                        var strTags = "";
                        $.each(outputElementsTags, function () {
                            if (strTags.length > 0)
                                strTags += ",";
                            strTags += this;
                        });
                        $('[id$=lblWsOutElm]').html(strTags);
                        $('[id$=DivWsWsdlDet]').hide();
                        $('[id$=DivWsHttpDet]').show();
                        $('[id$=DivWsRpcDet]').hide();
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                        var strInputParams = "";
                        $.each(wsInputParams, function () {
                            if (strInputParams.length > 0)
                                strInputParams += ",";
                            strInputParams += this;
                        });
                        $('[id$=lblWsInParam]').html(strInputParams);
                        $('[id$=lblWsOutParam]').html(objCmd.url);
                        $('[id$=DivWsWsdlDet]').hide();
                        $('[id$=DivWsHttpDet]').hide();
                        $('[id$=DivWsRpcDet]').show();
                        break;
                }
                showModalPopUp('SubprocWsCmdDetails', 'Object Details', 580,false,null,DialogType.Info);
            },
            showODataObjectDetails :function(cmdId){
                if(!cmdId)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                var objCmd = _odataCommandHelpers.getOdataObjectById(cmdId);
                var $lblCtrlOCmd_Name = $('[id$=lblCtrlOCmd_Name]');
                var $lblCtrlOCmd_Conn = $('[id$=lblCtrlOCmd_Conn]');
                var $lblCtrlOCmd_EndPoint = $('[id$=lblCtrlOCmd_EndPoint]');
                var $lblCtrlOCmd_Version = $('[id$=lblCtrlOCmd_Version]');
                var $lblCtrlOCmd_Description = $('[id$=lblCtrlOCmd_Description]');
                $lblCtrlOCmd_Name.html('');
                $lblCtrlOCmd_Conn.html('');
                $lblCtrlOCmd_EndPoint.html('');
                $lblCtrlOCmd_Version.html('');
                $lblCtrlOCmd_Description.html('');
                if(objCmd){
                    $lblCtrlOCmd_Name.html(objCmd.commandName);
                    $lblCtrlOCmd_Conn.html(objCmd.connectionName);
                    $lblCtrlOCmd_EndPoint.html(objCmd.endPoint);
                    $lblCtrlOCmd_Version.html(objCmd.serviceVer);
                    $lblCtrlOCmd_Description.html(objCmd.description);
                }
                showModalPopUp('SubProcBoxCtrlODataCmdDetails', 'Object Details', 580,false,null,DialogType.Info);
            },
            showOfflineObjectDetails :function(cmdId){
                if(!cmdId)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                var objCmd = _offlineDtHelpers.getDataObjByObjectId(cmdId);
                var $lblCtrlOffDataObj_Name = $('[id$=lblCtrlOffDataObj_Name]');
                var $lblCtrlOffDataObj_Description = $('[id$=lblCtrlOffDataObj_Description]');
                var $lblCtrlOffDataObj_Query = $('[id$=lblCtrlOffDataObj_Query]');
                var $lblCtrlOffDataObj_Tables = $('[id$=lblCtrlOffDataObj_Tables]');
                var $lblCtrlOffDataObj_Type = $('[id$=lblCtrlOffDataObj_Type]');
                $lblCtrlOffDataObj_Name.html('');
                $lblCtrlOffDataObj_Description.html('');
                $lblCtrlOffDataObj_Query.html('');
                $lblCtrlOffDataObj_Tables.html('');
                if(objCmd){
                    $lblCtrlOffDataObj_Name.html(objCmd.Name);
                    $lblCtrlOffDataObj_Description.html(objCmd.Description);
                    $lblCtrlOffDataObj_Query.html(objCmd.Query);
                    $lblCtrlOffDataObj_Tables.html(objCmd.OfflineTables);
                    switch(objCmd.QueryType)
                    {
                        case 1:
                            $lblCtrlOffDataObj_Type.html("SELECT");
                            break;
                        case 2:
                            $lblCtrlOffDataObj_Type.html("INSERT");
                            break;
                        case 3:
                            $lblCtrlOffDataObj_Type.html("UPDATE");
                            break;
                        case 4:
                            $lblCtrlOffDataObj_Type.html("DELETE");
                            break;
                    }
                }
                showModalPopUp('SubProcBoxCtrlOfflineDataObjDetails', 'Object Details', 580,false,null,DialogType.Info);
            },
            showObjectDetailsByType :function(cmdId,objectType/*ideCommandObjectTypes*/){
                if(!objectType)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                var objObjectDtl = null;
                switch (objectType) {
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                        objObjectDtl = this.showDbObjectDetails(cmdId);
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                        objObjectDtl = this.showWsObjectDetails(cmdId,objectType);
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                        objObjectDtl = this.showODataObjectDetails(cmdId);
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                        objObjectDtl = this.showOfflineObjectDetails(cmdId);
                    break;    
                }
            }
        },
        setTextOfTextAreaLabelOfPropSheet:function(text,textArealabel/*jquery object*/){
            var $textAreaLabel = textArealabel;
            var strLongTitle = text,
                aryAfterSplit=[];
            if(strLongTitle){
                aryAfterSplit = strLongTitle.split("\n");
                if(aryAfterSplit.length>1){
                    $textAreaLabel.val(aryAfterSplit[0].substring(0,25)+'...');
                }
                else{
                    if(strLongTitle.length>25){
                    $textAreaLabel.val(strLongTitle.substring(0,25)+'...');
                    }
                    else{
                        $textAreaLabel.val(strLongTitle);
                    }
                }    
            }
            else{
                $textAreaLabel.val('');
            }
        },
        getFormObjAryByAppModeltype: function (modelType) {
            var aryForms = [],
                objApp = null;
            switch(modelType){
                case MF_IDE_CONSTANTS.IDE_APP_MODEL_TYPE.phone:
                    objApp = this.appsContainerObjectHelper.getPhoneAppFromContainer();
                    break;
                case MF_IDE_CONSTANTS.IDE_APP_MODEL_TYPE.tablet:
                    objApp = this.appsContainerObjectHelper.getTabletAppFromContainer();
                break;    
            }    
            if (objApp && objApp.forms && $.isArray(objApp.forms)) {
                $.merge(aryForms, objApp.forms);
            }
            return aryForms;
        },
        dropDownOptionsHelpers: {
            getmPluginAgentsArray:function(addSelectOption/*bool*/,selectOptionValue/*string*/){
                return _mpluginAgentsHelper.getmPluginAgents();
            },
            getmPluginAgents:function(addSelectOption/*bool*/,selectOptionValue/*string*/){
                return _mpluginAgentsHelper.
                        getmPluginAgentsAsOptionsOfDdl(addSelectOption,
                            selectOptionValue);
            },
            getawsCredentialsArray:function(addSelectOption/*bool*/,selectOptionValue/*string*/){
                return _awsCredentialsHelper.getawsCredentials();
            },
            getawsCredentials:function(addSelectOption/*bool*/,selectOptionValue/*string*/){
                return _awsCredentialsHelper.
                        getawsCredentialsAsOptionsOfDdl(addSelectOption,
                            selectOptionValue);
            },
            getOfflineTables:function(addSelectOption/*bool*/,selectOptionValue/*string*/){
                return _offlineDtHelpers.getDatatablesAsOptionsForDdl(
                        addSelectOption,
                        selectOptionValue
                );
            },
            getOfflineTblCols:function(tableId,
                    addSelectOption/*bool*/,selectOptionValue/*string*/){
                return _offlineDtHelpers.getDatatableColsAsOptionsForDdl(
                        tableId,
                        addSelectOption,
                        selectOptionValue
                );
            },
            getOfflineDataObjects:function(addSelectOption/*bool*/,selectOptionValue/*string*/){
                return _offlineDtHelpers.getOfflineDataObjsAsOptionsForDdl(
                        addSelectOption,
                        selectOptionValue
                );
            },
            getOfflineDataObjOutputCols:function(addSelectOption/*bool*/,selectOptionValue/*string*/,objectId){
                return _offlineDtHelpers.getDataObjectOutputColsAsOptionsForDdl(
                        objectId,
                        addSelectOption,
                        selectOptionValue
                );
            },
            getViews:function(views/*ary of views*/,addSelectOption,selectOptionValue){
                var strOptions = "",
                    i=0,
                    objView = null;
                if(addSelectOption === true){
                    strOptions += '<option value='+selectOptionValue+'>'+'Select</option>';
                }
                if($.isArray(views) && views.length>0){
                    for(i=0;i<=views.length-1;i++){
                        objView = views[i];
                        strOptions += 
                        '<option value='+objView.id+'>'+
                            objView.fnGetName()+'</option>';
                    }
                }
                return strOptions;
            },
            getModalViewBackBtnOptions:function(addSelectOption,selectOptionValue){
                var strOptions = "";
                if(addSelectOption === true){
                    strOptions += '<option value='+selectOptionValue+'>'+'Select</option>';
                }
                strOptions += '<option value="0">No</option>';
                strOptions += '<option value="1">Yes (Previous view)</option>';
                return strOptions;
            },
            getNormalViewBackBtnOptions:function(addSelectOption,selectOptionValue){
                var strOptions = "";
                if(addSelectOption === true){
                    strOptions += '<option value='+selectOptionValue+'>'+'Select</option>';
                }
                strOptions += '<option value="0">No</option>';
                strOptions += '<option value="1">Yes (Previous view)</option>';
                strOptions += '<option value="2">Yes (Skip one view)</option>';
                strOptions += '<option value="3">Yes (Skip two views)</option>';
                strOptions += '<option value="4">Yes (Skip three views)</option>';
                strOptions += '<option value="5">Yes (Go to view)</option>';
                return strOptions;
            },
            getDbObjectsByCommandType:function(addSelectOption,selectOptionValue,cmdType){
               var strOptions = "",
                    aryDbObjects = _dbCommandHelpers.getDbObjectsByCmdType(cmdType),
                    i=0,
                    objDbObject = null;
               if(addSelectOption === true){
                   strOptions += '<option value='+selectOptionValue+'>'+'Select</option>';
               } 
               if($.isArray(aryDbObjects) && aryDbObjects.length>0){
                   for(i=0;i<=aryDbObjects.length-1;i++){
                       objDbObject = aryDbObjects[i];
                       strOptions += '<option value="'+objDbObject.commandId+'">'+objDbObject.commandName+'</option>';
                   }
               }
               return strOptions;
            },
            getWsObjects:function(addSelectOption,selectOptionValue){
                var strOptions = "",
                     aryObjects = _getAllWsObjects(),
                     i=0,
                     objCmdObject = null;
                if(addSelectOption === true){
                    strOptions += '<option value='+selectOptionValue+'>'+'Select</option>';
                } 
                if($.isArray(aryObjects) && aryObjects.length>0){
                    for(i=0;i<=aryObjects.length-1;i++){
                        objCmdObject = aryObjects[i];
                        strOptions += '<option value="'+objCmdObject.commandId+'">'+objCmdObject.commandName+'</option>';
                    }
                }
                return strOptions;
            }
        },
        getOfflineObjectByTableId:function(tableId){
            return _offlineDtHelpers.getDatatableByTableId(tableId);
        },
        getOfflineDataObjectByObjectId:function(objectId){
            return _offlineDtHelpers.getDataObjByObjectId(objectId);
        },
        getTransitionOverlayLabelText:function(connType/*viewDestConnectionType*/,view/*view*/,app,evntSrcId){
           var sourceName = view.fnGetName(),
               connOverlayText="",
               objForm = null,
               objControl = null;
           switch(connType){
               case MF_IDE_CONSTANTS.viewDestConnectionType.next:
                   //connOverlayText = sourceName + " : " + view.fnGetNextButtonLabel();
                   connOverlayText = view.fnGetNextButtonLabel();
               break;
               case MF_IDE_CONSTANTS.viewDestConnectionType.rowClick:
                   //connOverlayText = sourceName + " : List touch";
                   connOverlayText = "List touch";
               break;
               case MF_IDE_CONSTANTS.viewDestConnectionType.actionButton1:
                   connOverlayText = view.fnGetListCntrlActionBtnNameByType(MF_IDE_CONSTANTS.listActionBtnType.actionBtn1);
               break;
               case MF_IDE_CONSTANTS.viewDestConnectionType.actionButton2:
                   connOverlayText = view.fnGetListCntrlActionBtnNameByType(MF_IDE_CONSTANTS.listActionBtnType.actionBtn2);
               break;
               case MF_IDE_CONSTANTS.viewDestConnectionType.actionButton3:
                   connOverlayText = view.fnGetListCntrlActionBtnNameByType(MF_IDE_CONSTANTS.listActionBtnType.actionBtn3);
               break;
               case MF_IDE_CONSTANTS.viewDestConnectionType.condition:
                   connOverlayText = "Condition";
               break;
               case MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton1:
                   connOverlayText = view.fnGetViewActionBtn1().fnGetName();
               break;
               case MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton2:
                   connOverlayText = view.fnGetViewActionBtn2().fnGetName();
               break;
               case MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton3:
                   connOverlayText = view.fnGetViewActionBtn3().fnGetName();
               break;
               case MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton4:
                   connOverlayText = view.fnGetViewActionBtn4().fnGetName();
               break;
               case MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton5:
                   connOverlayText = view.fnGetViewActionBtn5().fnGetName();
               break;
               case MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton6:
                   connOverlayText = view.fnGetViewActionBtn6().fnGetName();
               break;
               case MF_IDE_CONSTANTS.viewDestConnectionType.multiple:
                   connOverlayText = connType.conOverlayText;
               break;
               case MF_IDE_CONSTANTS.viewDestConnectionType.button:
                   objForm = app.fnGetFormByFormId(view.fnGetViewFormId());
                   objControl = objForm.fnGetControlFromId(evntSrcId);
                   if(objControl){
                       connOverlayText = objControl.fnGetUserDefinedName();
                   }
               break;
           } 
           return connOverlayText;
        },
        getTransitionOverlayLabelHoverText:function(connTypes/*array of connection types (ConnectionTypeInfo)*/,view,app){
           var connOverlayHoverText="",
                strTextToAdd = "",
                objForm = null,
                objControl = null; 
           if($.isArray(connTypes) && connTypes.length>0){
               for(var i=0;i<=connTypes.length-1;i++){
                    switch(connTypes[i].fnGetConnType()){
                        case MF_IDE_CONSTANTS.viewDestConnectionType.next:
                            strTextToAdd = "Submit Button : " +view.fnGetNextButtonLabel();
                        break;
                        case MF_IDE_CONSTANTS.viewDestConnectionType.rowClick:
                            strTextToAdd = "List Touch";
                        break;
                        case MF_IDE_CONSTANTS.viewDestConnectionType.actionButton1:
                            strTextToAdd = "List Action : " +view.fnGetListCntrlActionBtnNameByType(MF_IDE_CONSTANTS.listActionBtnType.actionBtn1);
                        break;
                        case MF_IDE_CONSTANTS.viewDestConnectionType.actionButton2:
                            strTextToAdd = "List Action : "+view.fnGetListCntrlActionBtnNameByType(MF_IDE_CONSTANTS.listActionBtnType.actionBtn2);
                        break;
                        case MF_IDE_CONSTANTS.viewDestConnectionType.actionButton3:
                            strTextToAdd = "List Action : " +view.fnGetListCntrlActionBtnNameByType(MF_IDE_CONSTANTS.listActionBtnType.actionBtn3);
                        break;
                        case MF_IDE_CONSTANTS.viewDestConnectionType.condition:
                            strTextToAdd = "Condition";
                        break;
                        case MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton1:
                            strTextToAdd = "Action Button : " +
                                view.fnGetViewActionBtn1().fnGetName();
                        break;
                        case MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton2:
                            strTextToAdd = "Action Button : " +
                                view.fnGetViewActionBtn2().fnGetName();
                        break;
                        case MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton3:
                            strTextToAdd = "Action Button : " +
                                view.fnGetViewActionBtn3().fnGetName();
                        break;
                        case MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton4:
                            strTextToAdd = "Action Button : " +
                                view.fnGetViewActionBtn4().fnGetName();
                        break;
                        case MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton5:
                            strTextToAdd = "Action Button : " +
                                view.fnGetViewActionBtn5().fnGetName();
                        break;
                        case MF_IDE_CONSTANTS.viewDestConnectionType.viewActionButton6:
                            strTextToAdd = "Action Button : " +
                              view.fnGetViewActionBtn6().fnGetName();
                        break;
                        case MF_IDE_CONSTANTS.viewDestConnectionType.multiple:
                            strTextToAdd = connType.conOverlayText;
                        break;
                        case MF_IDE_CONSTANTS.viewDestConnectionType.button:
                            objForm = app.fnGetFormByFormId(view.fnGetViewFormId());
                            objControl = objForm.fnGetControlFromId(connTypes[i].fnGetEventSourceId());
                            if(objControl){
                                strTextToAdd = objControl.fnGetUserDefinedName();
                            }
                        break;
                    }
                    if(mfUtil.isEmptyString(connOverlayHoverText)){
                        connOverlayHoverText = strTextToAdd;
                    }
                    else{
                       connOverlayHoverText += "\n"+ strTextToAdd;
                    }
                }
           }
           return connOverlayHoverText;
        },
        viewInitializeHelper:{
            filterCntrlsForInitialize:function(controls/*array of controls*/){
                var aryFilteredCntrls = [],
                    i=0,
                    objControl = null;
                if($.isArray(controls) && controls.length>0){
                    for(i=0;i<=controls.length-1;i++){
                       objControl = controls[i];
                       if(objControl && objControl.type){
                           if(_canControlBeInitialized(objControl.type) === true){
                               aryFilteredCntrls.push(objControl);
                           }
                       }
                    }
                }
                return aryFilteredCntrls;
            }
            
        },
        amazonS3 : {
            getAmazonS3DomainPath : function(){
                return  "https://s3-ap-southeast-1.amazonaws.com/";
            },
            getMficientPublicBucket : function(){
                return  "mficient-public/";
            },
            getBucketNameForMediaFile : function(){
                return  "mficient-images/";
            },
            getKeyFromCompletePathForMediaFile: function(path){
                if(!mfUtil.isEmptyString(path)){
                    return path.substring(
                           (this.getAmazonS3DomainPath()+this.getBucketNameForMediaFile()).length,
                           path.length
                        );
                }
                else{
                    return "";
                }
            },
            getCompletePathForMediaFile: function(keyForFile/*S3 bucket key*/){
                return this.getAmazonS3DomainPath()+this.getBucketNameForMediaFile()+keyForFile;
            }
        },
        jqueryMobileIconsHelper:{
            getArrayListOfIcons : function(){
                return _jqueryMobileIconsHelper.getArrayListOfIcons();
            },
            getAsTextValPairList : function(){
               return _jqueryMobileIconsHelper.getAsTextValPairList();
            },
            getCompleteIconPath : function(iconName){
                return _jqueryMobileIconsHelper.getCompleteIconPath(iconName);
            }
        },
        getConnEvntSrcNameByEvntSrcId:function(connType,evntSrcId,view,app){
           var strSourceName = "",
                objForm = null,
                objControl = null; 
            switch(connType){
                case MF_IDE_CONSTANTS.viewDestConnectionType.button :
                    objForm = app.fnGetFormByFormId(view.fnGetViewFormId());
                    objControl = objForm.fnGetControlFromId(evntSrcId);
                    if(objControl){
                        strSourceName = objControl.fnGetUserDefinedName();
                    }
                    break;
                default:
                    strSourceName = this.getTransitionOverlayLabelHoverText([
                        new ConnectionTypeInfo({
                           connType :connType,
                           evntSrcId : evntSrcId,
                           evntSrcName : ""
                        })
                    ],view);
                    break
            }
            return strSourceName;
        },
        getOutputParamsForCmdObject: function (cmdId, commandType/*ideCommandObjectTypes*/) {
            if (!commandType || !cmdId) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var aryOutputParams = [];
            switch (commandType) {
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                    aryOutputParams = _dbCommandHelpers.getOutputParamsForCmd(cmdId);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                    aryOutputParams = _wsCommandHelpers.getWsTags(cmdId,
                        MF_IDE_CONSTANTS.webServiceType.wsdlSoap);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                    aryOutputParams = _wsCommandHelpers.getWsTags(cmdId,
                        MF_IDE_CONSTANTS.webServiceType.http);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                    aryOutputParams = _wsCommandHelpers.getWsTags(cmdId,
                        MF_IDE_CONSTANTS.webServiceType.xmlRpc);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                    aryOutputParams = _odataCommandHelpers.getOutputParamsForCmd(cmdId);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                    aryOutputParams = _offlineDtHelpers.getDataObjOutputCols(cmdId);
                break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                    aryOutputParams = _offlineDtHelpers.getDataObjOutputCols(cmdId);
                break;    
            }
            return aryOutputParams;
        },
        getControlAfterResetByType:function(cntrlBeforeReset){
            var objControl = null;
            switch (cntrlBeforeReset.type.idPrefix) {
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER.idPrefix:
                    objControl = new DateTimePicker(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER;
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL.idPrefix:
                    objControl = new Label(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.idPrefix:
                    objControl = new Textbox(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.idPrefix:
                    objControl = new Dropdown(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON.idPrefix: //RadioButton
                    objControl = new RadioButton(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX.idPrefix:
                    objControl = new Checkbox(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX;
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE.idPrefix:
                    objControl = new Image(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST.idPrefix:
                    objControl = new List(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST.idPrefix:
                    objControl = new EditableList(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN.idPrefix:
                    objControl = new HiddenField(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE.idPrefix:
                    objControl = new Toggle(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER.idPrefix:
                    objControl = new Slider(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER;
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SEPARATOR.idPrefix:
                    objControl = new Separator(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SEPARATOR;
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARCODE.idPrefix:
                    objControl = new Barcode(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARCODE;
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION.idPrefix:
                    objControl = new Location(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART.idPrefix:
                    objControl = new PieChart(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH.idPrefix:
                    var objBarGraph = new BarGraph(cntrlBeforeReset);
                    objBarGraph.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH;
                    objBarGraph.fnResetObjectDetails();
                    this.fnAddControl(objBarGraph);
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE.idPrefix:
                    objControl = new AngularGuage(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE.idPrefix:
                    objControl = new CylinderGuage(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH.idPrefix:
                    objControl = new LineGraph(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART.idPrefix:
                    objControl = new DataPointChart(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE.idPrefix:
                    //MOHAN
                    objControl = new Table(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE;
                    objControl.fnResetObjectDetails();
                    
                    //MOHAN
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SPACER.idPrefix:
                    objControl = new Spacer(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SPACER;
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PICTURE.idPrefix:
                    objControl = new Picture(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PICTURE;
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SIGNATURE.idPrefix:
                    objControl = new Signature(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SIGNATURE;
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART.idPrefix:
                    objControl = new DrilldownPieChart(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART;
                    objControl.fnResetObjectDetails();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BUTTON.idPrefix:
                    objControl = new Button(cntrlBeforeReset);
                    objControl.type = mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BUTTON;
                    break;
            }
            return objControl;
        },
        customTriggers :{
            getNameById:function(value){
                var strName = "";
                switch(value){
                    case MF_IDE_CONSTANTS.customTriggers.ctOne:
                            strName = "Custom Trigger 1";
                        break;
                    case MF_IDE_CONSTANTS.customTriggers.ctTwo:
                            strName = "Custom Trigger 2";
                        break;
                    case MF_IDE_CONSTANTS.customTriggers.ctThree:
                            strName = "Custom Trigger 3";
                        break;
                    case MF_IDE_CONSTANTS.customTriggers.ctFour:
                            strName = "Custom Trigger 4";
                        break;
                    case MF_IDE_CONSTANTS.customTriggers.ctFive:
                            strName = "Custom Trigger 5";
                        break;
                    case MF_IDE_CONSTANTS.customTriggers.ctSix:
                            strName = "Custom Trigger 6";
                        break;
                    case MF_IDE_CONSTANTS.customTriggers.ctSeven:
                            strName = "Custom Trigger 7";
                        break;
                    case MF_IDE_CONSTANTS.customTriggers.ctEight:
                            strName = "Custom Trigger 8";
                        break;
                    case MF_IDE_CONSTANTS.customTriggers.ctNine:
                            strName = "Custom Trigger 9";
                        break;
                    case MF_IDE_CONSTANTS.customTriggers.ctTen:
                            strName = "Custom Trigger 10";
                        break;
                }
                return strName;
            }
        },
        getAllEnterpriseWiFi : function(){
            var $hidWiFiInfos = $('input[id$="hidWiFiSSIDs"]'),
                strWifiInfos = $hidWiFiInfos.val();
            if(mfUtil.isNullOrUndefined(strWifiInfos) ||  mfUtil.isEmptyString(strWifiInfos)){
                return [];
            }
            else{
                return $.parseJSON(strWifiInfos);  
            }
        },
        getPopupHeaderForCntrlDatabinding:function(objectType){
            var strHeader = "";
            switch (objectType) {
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                    strHeader = "Database Object";
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                    strHeader = "Webservice Object";
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                    strHeader = "OData Object";
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                    strHeader = "Offline Database Object";
                break;    
            }
            return strHeader;
        }
    };
})();

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET = {};
mFicientIde.PropertySheetJson = {};
mFicientIde.Cache = {
    appIntlsWithoutFixedVals:[],
    appIntlsWithFixedVals :[],
    appIntlsForUserName:[],
    appIntlsForGroupName:[],
    appIntlsForViewInit:[],
    controlToCopy:null,
    getIntlsense:function(withFixedVals/*bool*/){
       if(withFixedVals === true){
          return  this.appIntlsWithFixedVals;
       }
       else{
          return  this.appIntlsWithoutFixedVals;
       }
    },
    getIntlsenseForUsername:function(){
       return this.appIntlsForUserName;
    },
    getIntlsenseForGroupName:function(){
       return this.appIntlsForGroupName;
    },
    getIntlsenseForViewInit:function(intlsVal){
        return this.appIntlsForViewInit;
    },
    setIntlsenseVal:function(intlsVal,withFixedVals/*bool*/){
        if(withFixedVals === true){
            this.appIntlsWithFixedVals = intlsVal;
        }
        else{
            this.appIntlsWithoutFixedVals = intlsVal;
        }
    },
    setIntlsenseValForUsername:function(intlsVal){
        this.appIntlsForUserName = intlsVal;
    },
    setIntlsenseValForGroupName:function(intlsVal){
        this.appIntlsForGroupName = intlsVal;
    },
    setIntlsenseValForViewInit:function(intlsVal){
        this.appIntlsForViewInit = intlsVal;
    },
    getControlToCopy : function(){
        return this.controlToCopy;
    },
    isControlToCopyEmpty : function(){
        if(!mfUtil.isNullOrUndefined(this.getControlToCopy())){
            return false;
        }
        else{
            return true;
        }
    },
    setControlToCopy : function(val){
        this.controlToCopy = val;
    }
};
var MF_HELPERS = mFicientIde.MF_HELPERS;
var MF_HELPERS = mFicientIde.MF_HELPERS;
var PROP_JSON_HTML_MAP = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET;
var MF_DATA_BINDING = mFicientIde.MF_DATA_BINDING;
var MF_VALIDATION = mFicientIde.MF_VALIDATION;

mFicientIde.MF_DATA_BINDING = (function () {
    //MOHAN
    var _utilities = {
        
        getMapDdlsOptionsByObjectType:function(objectId,objectType){
            var strOptions = "";
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                       strOptions =MF_HELPERS
                                .dropDownOptionsHelpers
                                .getOfflineTblCols(true,"-1",objectId);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                       strOptions =MF_HELPERS
                                .dropDownOptionsHelpers
                                .getOfflineDataObjOutputCols(true,"-1",objectId);
                    break;
            }
            return strOptions;
        },
        getCommandObjectByObjectType:function(objectId,objectType){
            var objCmdObject = null;
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                       objCmdObject = MF_HELPERS.getOfflineObjectByTableId(objectId);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                       objCmdObject = MF_HELPERS.getOfflineDataObjectByObjectId(objectId);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                    objCmdObject = MF_HELPERS.getCommandObjectByIdAndType(objectId,objectType);
                    break;
            }
            return objCmdObject;
        },
        setDatabindingGlobalDataName:function(cmdObject,databindingObj,objectType){
            if(cmdObject){
                switch(objectType){
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                          databindingObj.fnSetName(cmdObject.TableName); 
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                          databindingObj.fnSetName(cmdObject.Name); 
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                        databindingObj.fnSetName(cmdObject.commandName); 
                        break;    
                }
            }
        },
        getStringIntellisenseJson:function(intlsJson){
            var strIntls = "";
            if(intlsJson && $.isArray(intlsJson) && intlsJson.length>0){
                strIntls = JSON.stringify(intlsJson);
            }
            else{
                strIntls = JSON.stringify([]);
            }
            return strIntls;
        },
        getOfflineTableColsForCondLogicBinding:function(tableId){
            var offlineTblCols = [],
                aryTemp=[];
            offlineTblCols = MF_HELPERS
                .getInputParamsForCmdObject(
                    tableId,MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt
                );
                
            if(offlineTblCols && 
                $.isArray(offlineTblCols) &&
                offlineTblCols.length>0){
                
                for(i=0;i<= offlineTblCols.length-1;i++){
                    
                    aryTemp.push({
                        text:offlineTblCols[i],
                        val:offlineTblCols[i]
                    });
                }
            }     
            return JSON.stringify(aryTemp);    
        },
        setDatabindObjectNameInPropsheetHtml:function(propSheetCntrl/*jquery Object*/,value){
            if(value){
               propSheetCntrl.val(value) ;
            }
            else{
                propSheetCntrl.val('Select Object');
            }
        } 
    };
    var _ddlDatabindingObject = (function(){
        function _getDdlDatabindingObject(objectType/*ideCommandObjectTypes*/){
            if(!objectType)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var $databindingObjectDdl = $([]);
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                    $databindingObjectDdl = _getHtmlControls.ddlDatabindingObject();
                break;
                default :
                    $databindingObjectDdl = _getHtmlControls.ddlDatabindingObject();
                break;
            }
            return $databindingObjectDdl
        }
        function _bindObjectsByType(objectType,addSelectOption,selectOptionValue){
            var $ddlDatabindingObject = _getDdlDatabindingObject(objectType),
                strOptions = "";
                $ddlDatabindingObject.html('');
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                       strOptions =  
                       MF_HELPERS.dropDownOptionsHelpers.getOfflineTables(
                           addSelectOption,selectOptionValue  
                        );
                        $ddlDatabindingObject.html(strOptions);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                       strOptions =  
                       MF_HELPERS.dropDownOptionsHelpers.getOfflineDataObjects(
                           addSelectOption,selectOptionValue  
                        );
                        $ddlDatabindingObject.html(strOptions);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                   strOptions = MF_HELPERS.dropDownOptionsHelpers.getDbObjectsByCommandType(
                                   true,
                                   "-1",
                                   MF_IDE_CONSTANTS.DBCommandType.SELECT
                               );
                    $ddlDatabindingObject.html(strOptions);
                break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                    strOptions = MF_HELPERS.dropDownOptionsHelpers.getWsObjects(
                                true,
                                "-1");
                    $ddlDatabindingObject.html(strOptions);
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                    var aryCmdObjects = MF_HELPERS.getCommandObjectsForDdlBinding(
                            objectType);
                    MF_HELPERS.bindDdls([$ddlDatabindingObject]
                                        ,JSON.stringify(aryCmdObjects)
                                        ,addSelectOption);
                    break;
                default:
                    var aryCmdObjects = MF_HELPERS.getCommandObjectsForDdlBinding(
                            objectType);
                    MF_HELPERS.bindDdls([$ddlDatabindingObject]
                                        ,JSON.stringify(aryCmdObjects)
                                        ,addSelectOption);
                break;
            }
        }
        
        function _setEvents (objectType,cntrlType){
            
            var $ddlDatabindingObject = _getDdlDatabindingObject(objectType);
            $ddlDatabindingObject.off('change');
            $ddlDatabindingObject.on('change',function(evnt){
                var $self = $(this),
                    objectId = $self.val(),
                    objDatabindingObj = _globalVarHelpers.getDatabindingObjGlobalData(),
                    cmdObject = null,
                    strOptions = "",
                    intlsJson = _globalVarHelpers.getIntlsJsonGlobalData(),
                    strOfflineTblCols = [],
                    aryTemp = [],
                    aryInputParams=[];
                    
                    
                    if(objectId === "-1"){
                        strOptions ='<option value="-1">Select Column</option>';
                        objDatabindingObj.id = "";
                        objDatabindingObj.name ="";
                        _showHideControls.showHideCondLogicDiv(false);
                    }
                    else{
                        strOptions = _utilities.getMapDdlsOptionsByObjectType(
                            objectId,
                            objectType
                        );
                        cmdObject = _utilities.getCommandObjectByObjectType(
                                    objectId,
                                    objectType
                                );
                        objDatabindingObj.id = objectId;
                        //objDatabindingObj.name = cmdObject.TableName;
                        // if(objectType === MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt){
                        // _utilities.setDatabindingGlobalDataName(cmdObject.TableName,
                        //     objDatabindingObj,objectType); 
                        //     }
                        // else if(objectType === MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj){
                        // _utilities.setDatabindingGlobalDataName(cmdObject.Name,
                        //     objDatabindingObj,objectType); 
                        //     }
                        
                        _utilities.setDatabindingGlobalDataName(
                            cmdObject,
                            objDatabindingObj,
                            objectType); 
                        
                        // if(intlsJson && $.isArray(intlsJson) && intlsJson.length>0){
                        //     intlsJson = JSON.stringify(intlsJson);
                        // }
                        // else{
                        //     intlsJson = JSON.stringify([]);
                        // }
                        
                        if(objectType === MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt){
                            
                            intlsJson = _utilities.getStringIntellisenseJson(intlsJson);
                            
                            strOfflineTblCols = 
                                _utilities
                                .getOfflineTableColsForCondLogicBinding(objectId);
                                           
                             _conditionalLogic.makeConditionalLogicForMapping({
                                 intlsJson :intlsJson,
                                 offlineTblCols:strOfflineTblCols,
                                 editJson :"",
                                 isEdit:false
                             });
                            _showHideControls.showHideCondLogicDiv(true); 
                        } 
                        aryInputParams= MF_HELPERS.getInputParamsForCmdObject(
                            objectId,
                            objectType
                        );
                    }    
                    if(!strOptions){
                        strOptions ='<option value="-1">Select Column</option>';
                    }
                    objDatabindingObj.fnDeleteCmdParams();
                    objDatabindingObj.fnDeleteConditionalLogic();
                    _imgDatabindInputParameters.showHideByDatabindObj(objDatabindingObj);
                    
                    _setDefaultLinkParameters(aryInputParams, objDatabindingObj);
                    _imgDatabindInputParameters.bindEvent(objDatabindingObj);
                    switch (cntrlType) {
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                                _labelDatabindingHelper.bindMappingDropdown(
                                    
                                    objectType,strOptions);
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                                _checkboxDatabindingHelper.bindMappingDropdown(
                                    objectType,strOptions
                                 );
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
                        case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                            _sliderDatabindingHelper.bindMappingDropdown(
                               objectType,strOptions
                            );
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                            _angularAndCylinderDatabindingHelper.bindMappingDropdown(
                                objectType,strOptions);
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                            _textboxToggleDatabindingHelper.bindMappingDropdown(
                               objectType,strOptions
                            );
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                                _dropDownDatabindingHelper.bindMappingDropdown(
                                    objectType,strOptions
                                    );
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                                _listDataBindingHelper.bindDatabindingDdlsObjectType(
                                    null,
                                    false,
                                    objectType,
                                    strOptions);
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                                _editableListHelper.bindMappingDropdownsByObjectType(
                                    objectType,strOptions
                                );
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                            _pieDatabindingHelper.bindMappingDropdown(
                                objectType,strOptions);
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                                _barLineDatabindingHelper.bindMappingDropdown(
                                    objectType,strOptions
                                );
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                                _imageDatabindingHelper.bindMappingDropdown(
                                    objectType,strOptions);
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
                                _biTableDataBindingHelper.bindMappingDropdown(
                                    null,
                                    false,
                                    strOptions,
                                    objectType
                                );
                            break;
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
                            _locationDatabindingHelper.bindMappingDropdown(
                                objectType,strOptions
                            );
                        break;  
                        case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
                            _drilldownPieDatabindingHelper.bindMappingDropdown(
                                objectType,strOptions
                            );
                        break;
                    }
            });
        }
        return {
            bindObjectsByType:function( objectType/*ideCommandObjectTypes*/,
                addSelectOption,selectOptionValue){
                
                _bindObjectsByType(
                    objectType,
                    addSelectOption,
                    selectOptionValue
                );    
            },
            setEvents:function(objectType,cntrlType){
                _setEvents(objectType,cntrlType);
            },
            getSelectedValue:function(objectType){
                return _getDdlDatabindingObject(objectType).val();
            }
        }
    })();
    var _conditionalLogic=(function(){
        return {
            showHideCondLogicDiv :function(blnshow){
               _showHideControls.showHideCondLogicDiv(blnshow); 
            },
            makeConditionalLogicForMapping:function(options/*object*/){
                /*
                    {
                        intlsJson :string json
                        offlineTblCols:string json of {text :"",val:""},
                        editJson :stored value else empty,
                        isEdit:true,false
                    }
                
                */
                var $divCondLogicMapping = _getHtmlControls.divCntrlDatabindCondLogic();
                $divCondLogicMapping
                .mfConditionalLogicControl({
                    FirstDropDownOptions :options.offlineTblCols,
                    ConditionalLogicDdlVals:MF_IDE_CONSTANTS.getConditionalOperatorJson(),
                    IntlsJson:options.intlsJson,
                    IsEdit:options.isEdit,
                    SavedJsonIfEdit:options.editJson
                });
            },
            getConditionLogicJsonForSaving:function(){
                var $divCondLogicMapping = _getHtmlControls.divCntrlDatabindCondLogic();
                return $divCondLogicMapping.data("FinalJson")();
            }
        }
    })();
    var _showHideControls = (function(){
        function _getAllDivsByObjectType(objectType){
            var objAllDivs = null;
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                       objAllDivs = _getHtmlControls.allDivsOfCntrlsDatabinding();
                    break;
            }
            return objAllDivs;
        }
        function _hideAllContDivsOfDatabinding(allDivs){
            allDivs.textbox.hide();
            allDivs.label.hide();
            allDivs.slider.hide();
            allDivs.image.hide();
            allDivs.dropdown.hide();
            allDivs.list.hide();
            allDivs.editableList.hide();
            allDivs.pie.hide();
            allDivs.barAndLine.hide();
            allDivs.table.hide();
            allDivs.location.hide();
            allDivs.cylAndAngular.hide();
            allDivs.drillDownPie.hide();
        }
        function _showHideContDivsAndControlsByCntrlType(
                    allDivs/*object of all divs*/,
                    cntrlType/*CONSTANTS.CONTROLS*/,
                    control,
                    objectType){
            
            if(!allDivs || !cntrlType){
                throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            }
            _hideAllContDivsOfDatabinding(allDivs);
            switch (cntrlType) {
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                    allDivs.label.show();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                    allDivs.checkbox.show();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
                case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                    allDivs.slider.show();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
                    allDivs.cylAndAngular.show();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                    allDivs.textbox.show();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                    allDivs.dropdown.show();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                    allDivs.list.show();
                    _listDataBindingHelper.showHideDatabindCntrlsByListType(
                    control,objectType);
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                    allDivs.editableList.show();
                    _editableListHelper.formMappingHtml(control,objectType);
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                    allDivs.pie.show();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                    allDivs.barAndLine.show();
                    _barLineDatabindingHelper.showHideDatabindCntrlsBySeriesType(
                        objectType,control.fnGetNoOfSeries()
                        );
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                    allDivs.image.show();
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
                    allDivs.table.show();
                    _biTableDataBindingHelper.formMappingTableHtml(control,objectType);
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
                        allDivs.location.show();
                break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
                        allDivs.drillDownPie.show();
                break;
                case  MF_IDE_CONSTANTS.CONTROLS.CUSTOM_CONTROL:   
                break;   
            }
        }
        //function _showHideControlsByControlProperty(control,objectType){
        //     switch(cntrlType){
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
        //           break;
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
        //           break;
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
        //           break;
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
        //           break;
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
        //           break;
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                   
        //           break;
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
        //           break;
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
        //           break;
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
        //           break;
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
        //           break;
        //       case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
        //           break;
               
        //     }
        // }
        return {
            showHideControlsByObjectTypeAndCntrlType:function(
                    objectType,
                    cntrlType /*CONSTANTS.CONTROLS*/,
                    control){
                
                if(!objectType || !cntrlType){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                var objAllDivs = null;
                switch(objectType){
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                        objAllDivs = _getHtmlControls.allDivsOfCntrlsDatabinding();
                    break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                        objAllDivs = _getHtmlControls.allDivsOfCntrlsDatabinding();
                    break;
                    default :
                        objAllDivs = _getHtmlControls.allDivsOfCntrlsDatabinding();
                    break;
                }
                _showHideContDivsAndControlsByCntrlType(objAllDivs,
                        cntrlType
                        ,control
                        ,objectType);
            },
            showHideCondLogicDiv :function(blnshow){
                if(blnshow){
                  _getHtmlControls.divCntrlDatabindCondLogic().show();  
                }
                else{
                    _getHtmlControls.divCntrlDatabindCondLogic().hide();
                }
            }
        }
    })();
    var _getHtmlControls ={
       
       //Textbox TOOGGLE
       divTextboxCont:function(){
           return $('#DataBind_Ctrl_DisplayText_Div');
       },
       ddlTextboxToggleDisplayText:function(){
          return $('#ddlCtrlDataBindCmd_DisplayTxt');
       },
       //TEXTBOX TOGGLE
       //DROPDOWN SELCTOR LIST
       divDropdownCont:function(){
           return $('#DataBind_Dropdown_Options_Div');
       },
       ddlDropdownDisplayText:function(){
          return $('#ddlDropdownDataBind_DisplayTxt');
       },
       ddlDropdownDisplayVal:function(){
          return $('#ddlDropdownDataBind_DisplayVal');
       },
       //DROPDOWN SELECTOR LIST
       
       //SLIDE 
       divSliderCont:function(){
           return $('#DataBind_Slider_DisplayText_Div');
       },
       ddlSliderValue:function(){
           return $('#ddlSliderDataBindCmd_Val');
       },
       ddlCylAndAngValue:function(){
           return $('[id$=ddl_Offline_Cylinder_Guage_Value]');
       },
       ddlCylAndAngMaxValue:function(){
           return $('[id$=ddl_Offline_Cylinder_Guage_MaxVal]');
       },
       //SLIDER
       divLabelCont:function(){
           return $('#DataBind_Label_Command');
       },
       
       divListCont:function(){
           return $('#DataBind_List_Options_Div');
       },
       divEditableListCont:function(){
           return $('#DataBind_EditableList_Options_Div');
       },
       divPieChartCont:function(){
           return $('#DataBind_PieChart_Options_Div');
       },
       divAngAndCylChartCont:function(){
           return $('#Offline_Cylinder_Guage_Div');
       },
       divBarLineChartCont:function(){
           return $('#DataBind_Bar_LineChart_Options_Div');
       },
       divImageCont:function(){
           return $('#DataBind_Image_Div');
       },
       divTableCont:function(){
           return $('#DataBind_Table_Options_Div');
       },
       divLocationCont:function(){
           return $('#DataBind_Location_Options_Div');
       },
       ddlDatabindingObject:function(){
           return $('#ddlDatabindObject');
       },
       
       //CHECKBOX
       divCheckboxCont:function(){
           return $('#DataBind_Checkbox_DisplayText_Div');
       },
       ddlCheckboxCheckedValue:function(){
           return $('#ddlCheckboxDataBindCmd_CheckedVal');
       },
       //CHECKBOX
       //LIST
       divListRowId:function(){
           return $('#DataBind_List_RowId');
       },
       divListTitle:function(){
           return $('#DataBind_List_Title');
       },
       divListSubtitle:function(){
           return $('#DataBind_List_SubTitle');
       },
       divListInfo:function(){
           return $('#DataBind_List_Info');
       },
       divListAdditionalInfo:function(){
           return $('#DataBind_List_AddInfo');
       },
       divListImageName:function(){
           return $('#DataBind_List_ImageName');
       },
       divListCountField:function(){
           return $('#DataBind_List_CountField');
       },
       divListSelectedRow:function(){
           return $('#DataBind_List_SelectedRow');
       },
       divListDate:function(){
           return $('#DataBind_List_Date');
       },
       ddlListRowId:function(){
           return $('#ddlDataBindList_RowId');
       },
       ddlListTitle:function(){
           return $('#ddlDataBindList_Title');
       },
       ddlListSubtitle:function(){
           return $('#ddlDataBindList_SubTitle');
       },
       ddlListInfo:function(){
           return $('#ddlDataBindList_Info');
       },
       ddlListAdditionalInfo:function(){
           return $('#ddlDataBindList_AddInfo');
       },
       ddlListImageName:function(){
           return $('#ddlDataBindList_Image');
       },
       ddlListCountField:function(){
           return $('#ddlDataBindList_CountField');
       },
       ddlListSelectedRow:function(){
           return $('#ddlDataBindList_SelectedRow');
       },
       ddlListDate:function(){
           return $('#ddlDataBindList_Date');
       },
       ddlListDateFormat:function(){
           return $('#ddlDataBindList_DateFormat');
       },
       //LIST
       //BAR LINE 
       ddlBarLineXAxisData:function(){
           return $('#ddlDataBindBar_LineChart_Xaxis');
       },
       //BAR LINE
       //PIE CHART 
       ddlPieTitle:function(){
           return $('#ddlDataBindPieChart_Title');
       },
       ddlPieValue:function(){
           return $('#ddlDataBindPieChart_Value')
       },
       //PIE CHART
       //IMAGE
       ddlImageSource:function(){
           return $('#ddlImageDataBindCmd_Source');
       },
       ddlImageTitle:function(){
           return $('#ddlImageDataBindCmd_Title');
       },
       //IMAGE
       //Drilldown Pie Chart
       divDrillDownPieChartCont:function(){
           return $('#DataBind_DPieChart_Options_Div');
       },
       //DrilldownPieChart
       allDivsOfCntrlsDatabinding:function(){
           return {
               textbox:this.divTextboxCont(),
               checkbox:this.divCheckboxCont(),
               slider:this.divSliderCont(),
               label:this.divLabelCont(),
               dropdown:this.divDropdownCont(),
               list:this.divListCont(),
               editableList:this.divEditableListCont(),
               pie:this.divPieChartCont(), 
               barAndLine:this.divBarLineChartCont(),
               image:this.divImageCont(),
               table:this.divTableCont(),
               location:this.divLocationCont(),
               cylAndAngular : this.divAngAndCylChartCont(),
               drillDownPie : this.divDrillDownPieChartCont()
           }
       },
       allCheckboxDatabindingCntrls:function(){
           return {
               checkedValue:this.ddlCheckboxCheckedValue()
           };
       },
       allTextboxToggleDatabindingCntrls:function(){
           return {
               displayText:this.ddlTextboxToggleDisplayText()
           };
       },
       divCntrlsDatabindCont:function(){
           return $('#divCtrlsDataBind');
       },
       chkIgnoreCache:function(){
           return $('#chk_OfflineIgnoreCache');
       },
       divCntrlDatabindCondLogic:function(){
           return $('#divDatabindCondLogic');
       },
       allSliderDatabindingCntrls:function(){
           return {
               ddlValue :this.ddlSliderValue()
           };
       },
       allAngularAndCylinderDatabindingCntrls:function(){
           return {
               value :this.ddlCylAndAngValue(),
               maxValue :this.ddlCylAndAngMaxValue()
           };
       },
       allDropDownDatabindingCntrls:function(){
           return {
               optionText : this.ddlDropdownDisplayText(),
               optionValue :this.ddlDropdownDisplayVal()
           };
       },
       allListDatabindingCntrlsDiv:function(){
           return {
               rowId:this.divListRowId(),
               title:this.divListTitle(),
               subTitle:this.divListSubtitle(),
               info:this.divListInfo(),
               additionalInfo:this.divListAdditionalInfo(),
               imageName:this.divListImageName(),
               countField:this.divListCountField(),
               selectedRow:this.divListSelectedRow(),
               date:this.divListDate(),
               dateFormat:this.divListDate()
           };
       },
       allListDatabindingCntrls:function(){
           return {
               rowId:this.ddlListRowId(),
               title:this.ddlListTitle(),
               subtitle:this.ddlListSubtitle(),
               info:this.ddlListInfo(),
               addInfo:this.ddlListAdditionalInfo(),
               image:this.ddlListImageName(),
               countField:this.ddlListCountField(),
               selectedRow:this.ddlListSelectedRow(),
               date:this.ddlListDate(),
               dateFormat : this.ddlListDateFormat()
           };
       },
       allPieDatabindingCntrls:function(){
           return {
               title:this.ddlPieTitle(),
               value:this.ddlPieValue()
           };
       },
       allImageDatabindingCntrls:function(){
           return {
               source:this.ddlImageSource(),
               title : this.ddlImageTitle()
           };
       },
       allBarLineDatabindingCntrls:function(){
           return {};
       },
       allDbLocationDatabindingCntrls:function(){
           return {
               latitude : $('#ddlDbLocationDataBind_Latitude'),
               longitude :$('#ddlDbLocationDataBind_Longitude')
           }; 
       },
       allWsLocationDatabindingCntrls:function(){
             return {
                latitude : $('#ddlWsLocationDataBind_Latitude'),
                longitude :$('#ddlWsLocationDataBind_Longitude')
             };   
            },
       allOdataLocationDatabindingCntrls:function(){
           return {
               latitude : $('#ddlOdataLocationDataBind_Latitude'),
               longitude :$('#ddlOdataLocationDataBind_Longitude')
           }; 
        },
       allLocationDatabindingCntrls:function(){
           return {
               latitude : $('#ddlLocationDataBind_Latitude'),
               longitude :$('#ddlLocationDataBind_Longitude')
           }; 
        },
       allDrilldownPieChartDatabindCntrls:function(objectType/*CONSTANTS.ideCommandObjectTypes*/){
            if(!objectType)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var objControls = null;
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                   objControls = {
                       sliceTitle :$('#ddlDataBindPieChart_Slice_Title'),
                       subSliceTitle:$('#ddlDataBindDPieChart_SubSlice_Title'),
                       value:$('#ddlDataBindDPieChart_Value')
                   };
                break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                    objControls = {
                        sliceTitle :$('[id$=ddlDbDPieChart_Slice_Title]'),
                        subSliceTitle:$('[id$=ddlDbDPieChart_SubSlice_Title]'),
                        value:$('[id$=ddlDbDPieChart_Value]')
                    };
                break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                    objControls = {
                        sliceTitle :$('[id$=ddlWsDPieChart_Slice_Title]'),
                        subSliceTitle:$('[id$=ddlWsDPieChart_SubSlice_Title]'),
                        value:$('[id$=ddlWsDPieChart_Value]')
                    };
                break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                    objControls = {
                        sliceTitle :$('[id$=ddlODataDPieChart_Slice_Title]'),
                        subSliceTitle:$('[id$=ddlODataDPieChart_SubSlice_Title]'),
                        value:$('[id$=ddlODataDPieChart_Value]')
                    };
                break;
            }
            return objControls;
        }    
    };
    var _globalVarHelpers =(function(){
        
        function _setDataOfContainerDiv(data){
           _getHtmlControls.divCntrlsDatabindCont().data(
               MF_IDE_CONSTANTS.jqueryCommonDataKey,
               data
            ) 
        }
        function _getDataOfContainerDiv(){
            return _getHtmlControls.divCntrlsDatabindCont().data(
                MF_IDE_CONSTANTS.jqueryCommonDataKey);
        }
        function _getControlTypeGlobalData(){
            var objGlobalData = _getDataOfContainerDiv(),
                objControlType = null;
            if(objGlobalData){
                objControlType = objGlobalData.controlType;
            }
            return objControlType;
        }
        function _getDatabindingObjGlobalData(){
            var objGlobalData = _getDataOfContainerDiv(),
                objDatabindingObj = null;
            if(objGlobalData){
                objDatabindingObj = objGlobalData.databindObject;
            }
            return objDatabindingObj;
        }
        function _getObjectTypeGlobalData(){
            var objGlobalData = _getDataOfContainerDiv(),
                objectType = null;
            if(objGlobalData){
                objectType = objGlobalData.objectType;
            }
            return objectType;
        }
        function _getIsEditGlobalData(){
            var objGlobalData = _getDataOfContainerDiv(),
                isEdit = false;
            if(objGlobalData){
                isEdit = objGlobalData.isEdit;
            }
            return isEdit;
        }
        function _getIntlsJsonGlobalData(){
            var objGlobalData = _getDataOfContainerDiv(),
                intlsJson = [];
            if(objGlobalData){
                intlsJson = objGlobalData.intlsJson;
            }
            return intlsJson;
        }
        function _getControlGlobalData(){
            var objGlobalData = _getDataOfContainerDiv(),
                objControlType = null;
            if(objGlobalData){
                objControlType = objGlobalData.control;
            }
            return objControlType;
        }
        return {
          setDataOfContainerDiv:function(options){
              /*{
                  controlType : control type,
                  databindObject : cloned databind object for modifiaction,
                  objectType : MF_IDE_CONSTANTS.ideCommandObjectTypes,
                  isEdit
              }*/
             _setDataOfContainerDiv(options);
          },
          getDataOfContainerDiv:function(){
              return _getDataOfContainerDiv();
          },
          getControlTypeGlobalData:function(){
              return _getControlTypeGlobalData();
          },
          getControlTypeGlobalData:function(){
              return _getControlTypeGlobalData();
          },
          getObjectTypeGlobalData:function(){
              return _getObjectTypeGlobalData();
          },
          getIsEditGlobalData:function(){
              return _getIsEditGlobalData();
          },
          getDatabindingObjGlobalData:function(){
              return _getDatabindingObjGlobalData();
          },
          getIntlsJsonGlobalData:function(){
              return _getIntlsJsonGlobalData();
          },
          getControlGlobalData:function(){
              return _getControlGlobalData();
          }
        };
    })();
    var _saveDatabindingObject = (function(){
        function _validateDataObject(objectType,cntrlType){
            var $ddlObject=$([]),
                aryErrors =[];
            
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                   $ddlObject = _getHtmlControls.ddlDatabindingObject();
                break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                   $ddlObject = _getHtmlControls.ddlDatabindingObject();
                break; 
            }
            if($ddlObject.val() === "-1"){
                aryErrors.push("Please select a data object");
            }
            // switch(cntrlType){
            //     case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
            //             break;
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
            //             if(_getHtmlControls.ddlCheckboxCheckedValue().val()==="-1"){
            //                 aryErrors.push('Please select a value for checkedvalue.');
            //             }
            //             break;
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
                        
            //             break;
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                        
            //             break;
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
            //             // cntrl.displayText = $('[id$=ddlCtrlCmd_DisplayTxt]').val().trim();
            //             // mFicientIde.HiddenFieldControlHelper.processDatabindingObjectSaved();
            //             break;
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
            //             break;
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
            //         break;   
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
            //             break;
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
            //             break;
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
            //             break;
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
            //             break;
            //         case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
            //             break;
            //     }
            return aryErrors;
        }
        function _saveMappedDataInControlAndShowInPropHtml(
            control,
            objectType,
            cntrlType,
            newDatabindingObj
        ){
            
            switch(cntrlType){
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                    _labelDatabindingHelper.saveMappedDataInControl(control,objectType);
                    control.databindObjs = [];
                    control.fnAddDatabindObj(newDatabindingObj);
                    _labelDatabindingHelper.setBindingDtlsInPropSheetHtml(control);
                   break;
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                   _checkboxDatabindingHelper.saveMappedDataInControl(
                       control,objectType
                   );
                   control.databindObjs = [];
                   control.fnAddDatabindObj(newDatabindingObj);
                   _checkboxDatabindingHelper.setBindingDtlsInPropSheetHtml(control);
                   break;
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
               case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                   _sliderDatabindingHelper.saveMappedDataInControl(
                       control,objectType
                   );
                   control.databindObjs = [];
                   control.fnAddDatabindObj(newDatabindingObj);
                   _sliderDatabindingHelper.setBindingDtlsInPropSheetHtml(control,cntrlType);
                   break;
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
                    _angularAndCylinderDatabindingHelper.saveMappedDataInControl(
                       control,objectType
                   );
                   control.databindObjs = [];
                   control.fnAddDatabindObj(newDatabindingObj);
                   _angularAndCylinderDatabindingHelper.setBindingDtlsInPropSheetHtml(control,cntrlType);
                    break;
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:       
                   _textboxToggleDatabindingHelper.saveMappedDataInControl(
                       control,objectType
                   );
                   control.databindObjs = [];
                   control.fnAddDatabindObj(newDatabindingObj);
                   _textboxToggleDatabindingHelper.setBindingDtlsInPropSheetHtml(
                       control,
                       cntrlType
                    );
                   break;
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                   _dropDownDatabindingHelper.saveMappedDataInControl(
                       control,objectType
                   );
                   control.databindObjs = [];
                   control.fnAddDatabindObj(newDatabindingObj);
                   _dropDownDatabindingHelper.setBindingDtlsInPropSheetHtml(
                       control,
                       cntrlType
                    );
                   break;
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                    _listDataBindingHelper.processSaveMappedData(control);
                    control.databindObjs = [];
                    control.fnAddDatabindObj(newDatabindingObj);
                    _listDataBindingHelper.setDataInPropSheetHtml(control);
               break;   
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                   _editableListHelper.saveMappedDataInControl(control,objectType);
                   control.databindObjs = [];
                   control.fnAddDatabindObj(newDatabindingObj);
                   _editableListHelper.setDataInPropSheetHtml(control);
                   break;
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                   _pieDatabindingHelper.saveMappedDataInControl(
                       control,objectType
                   );
                   control.databindObjs = [];
                   control.fnAddDatabindObj(newDatabindingObj);
                   _pieDatabindingHelper.setBindingDtlsInPropSheetHtml(control);
                   break;
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                   _barLineDatabindingHelper.saveMappedDataInControl(control,objectType);
                   control.databindObjs = [];
                   control.fnAddDatabindObj(newDatabindingObj);
                   _barLineDatabindingHelper.setBindingDtlsInPropSheetHtml(control,cntrlType);
                   break;
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                   _imageDatabindingHelper.saveMappedDataInControl(
                       control,objectType
                   );
                   control.databindObjs = [];
                   control.fnAddDatabindObj(newDatabindingObj);
                   _imageDatabindingHelper.setBindingDtlsInPropSheetHtml(control);
                   break;
               case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
                    _biTableDataBindingHelper.saveMappedDataInControl(control,objectType);
                    control.databindObjs = [];
                    control.fnAddDatabindObj(newDatabindingObj);
                    _biTableDataBindingHelper.setDataInPropSheetHtml(control);
                   break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
                     _locationDatabindingHelper.saveMappedDataInControl(control,objectType);
                     control.databindObjs = [];
                     control.fnAddDatabindObj(newDatabindingObj);
                     _locationDatabindingHelper.setDataInPropSheetHtml(control);
                break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
                    _drilldownPieDatabindingHelper.saveMappedDataInControl(
                        control,objectType
                    );
                    control.fnClearAllDatabindObjs();
                    control.fnAddDatabindObj(newDatabindingObj);
                    _drilldownPieDatabindingHelper.setBindingDtlsInPropSheetHtml(control);
                break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CUSTOM_CONTROL:
                    control.fnClearAllDatabindObjs();
                    control.fnAddDatabindObj(newDatabindingObj);
                    _customControlDatabindingHelper.setBindingDtlsInPropSheetHtml(control);
                break;    
            }
             
        }
        function _save(){
            var control = null,
                objDatabindingObj = null,
                objectType = _globalVarHelpers.getObjectTypeGlobalData(),
                condLogic =null,
                cntrlType = _globalVarHelpers.getControlTypeGlobalData(),
                aryErrors = _validateDataObject(objectType,cntrlType);
            
            if(aryErrors && $.isArray(aryErrors) && aryErrors.length>0){
                throw new MfError(aryErrors);
            }
            
            //control = MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
            control = _globalVarHelpers.getControlGlobalData();
            objDatabindingObj = _globalVarHelpers.getDatabindingObjGlobalData();
            
            var objNewDatabindObject = new DatabindingObj(
                objDatabindingObj.id,
                [],
                objDatabindingObj.index,
                objDatabindingObj.bindObjType,
                objDatabindingObj.name,
                objDatabindingObj.dsName,
                objDatabindingObj.tempUiIndex,
                objDatabindingObj.usrName,
                objDatabindingObj.pwd,
                objDatabindingObj
            );
    
            if(objDatabindingObj.cmdParams != null
                && objDatabindingObj.cmdParams.length > 0){
                
                $.each(objDatabindingObj.cmdParams, function(){
                    var objCmdParam = new DatabindingObjParams(
                        this.id,
                        this.name,
                        this.type,
                        this.val);
                    objNewDatabindObject.fnAddCmdParams(objCmdParam);
                });
            }
            
        
            if($('[id$=chk_OfflineIgnoreCache]').is(':checked'))  objNewDatabindObject.fnSetCacheIgnoreValue(true);
            else  objNewDatabindObject.fnSetCacheIgnoreValue(false);
            
            if(objectType === MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj){
                
                //condLogic = _conditionalLogic.getConditionLogicJsonForSaving();
                objNewDatabindObject.fnSetConditionalLogic("");
                objNewDatabindObject.fnSetOfflineTblCmdTypeVal(
                    MF_IDE_CONSTANTS.DBCommandType.SELECT
                );
            }
            
           _saveMappedDataInControlAndShowInPropHtml(control,
                objectType,cntrlType,objNewDatabindObject);
        }
        return {
            validateSaving:function(){
                
            },
            save:function(){
                _save();
            }
        }
    })();
    var _setDataInHtml = (function(){
        function _setByObjectType(options){
           
           /*
               { 
                    objectType,
                    databindingObj,
                    objectColsForMapping string options for dropdown binding,
                    intlsJson string for conditional logic else empty
                }
           */
           var $ddlDatabindingObject = $([]),
                condLogic = null,
                objectId = ""
                objectType = options.objectType,
                databindingObj = options.databindingObj,
                objectColsForMapping = options.objectColsForMapping,
                intlsJson = options.intlsJson;
                chkIgnoreCache = null;
           
           _showHideControls.showHideCondLogicDiv(false);
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                   objectId  = databindingObj.fnGetId();
                   $ddlDatabindingObject = _getHtmlControls.ddlDatabindingObject();
                   $ddlDatabindingObject.val(objectId);
                   condLogic = databindingObj.fnGetConditionalLogic();
                   
                   _conditionalLogic.makeConditionalLogicForMapping({
                       intlsJson :intlsJson,
                       offlineTblCols:objectColsForMapping,
                       editJson :JSON.stringify(condLogic),
                       isEdit:true
                   });
                   
                   _showHideControls.showHideCondLogicDiv(true);
                   
                break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                   objectId  = databindingObj.fnGetId();
                   $ddlDatabindingObject = _getHtmlControls.ddlDatabindingObject();
                   $ddlDatabindingObject.val(objectId);
                   condLogic = databindingObj.fnGetConditionalLogic();
                   chkIgnoreCache = _getHtmlControls.chkIgnoreCache();
                   if(databindingObj.ignoreCache)
                    $(chkIgnoreCache)[0].checked = true;
                   else
                     $(chkIgnoreCache)[0].checked = false;
                   
                   _conditionalLogic.makeConditionalLogicForMapping({
                       intlsJson :intlsJson,
                       offlineTblCols:objectColsForMapping,
                       editJson :JSON.stringify(condLogic),
                       isEdit:true
                   });
                   break;
                default :
                    objectId  = databindingObj.fnGetId();
                    $ddlDatabindingObject = _getHtmlControls.ddlDatabindingObject();
                    $ddlDatabindingObject.val(objectId);
                    condLogic = databindingObj.fnGetConditionalLogic();
                    chkIgnoreCache = _getHtmlControls.chkIgnoreCache();
                    if(databindingObj.ignoreCache)
                     $(chkIgnoreCache)[0].checked = true;
                    else
                      $(chkIgnoreCache)[0].checked = false;
                break;
            }
        }
        function _setMappingDataByCntrlType(options){
            
            /*
                { 
                     control,
                     cntrlType,
                     objectType,
                     objectColsForMapping
                 }
            */
            var cntrlType = options.cntrlType,
                objectType = options.objectType,
                control = options.control,
                objectColsForMapping = options.objectColsForMapping;
            switch (cntrlType) {
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                        _labelDatabindingHelper.bindMappingDropdown(
                            objectType,
                            objectColsForMapping
                        );
                        _labelDatabindingHelper.setDataInHtmlForMapping(control);
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                    _checkboxDatabindingHelper.bindMappingDropdown(
                       objectType,objectColsForMapping
                    );
                    _checkboxDatabindingHelper.setDataInHtmlForMapping(
                        control
                     );
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
                case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                    _sliderDatabindingHelper.bindMappingDropdown(
                       objectType,objectColsForMapping
                    );
                    _sliderDatabindingHelper.setDataInHtmlForMapping(
                        control
                     );
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
                    _angularAndCylinderDatabindingHelper.bindMappingDropdown(
                       objectType,objectColsForMapping
                    );
                    _angularAndCylinderDatabindingHelper.setDataInHtmlForMapping(
                        control
                     );
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                    _textboxToggleDatabindingHelper.bindMappingDropdown(
                          objectType,objectColsForMapping
                       );
                    _textboxToggleDatabindingHelper.setDataInHtmlForMapping(
                       control
                    );   
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                    _dropDownDatabindingHelper.bindMappingDropdown(
                        objectType,objectColsForMapping);
                    _dropDownDatabindingHelper.setDataInHtmlForMapping(control);    
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                        _listDataBindingHelper.bindDatabindingDdlsObjectType(
                            control,
                            true,
                            objectType,
                            objectColsForMapping);
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                    _editableListHelper.bindMappingDropdownsByObjectType(
                            objectType,objectColsForMapping);
                    _editableListHelper.setMappedDataInHtml(control,objectType);        
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                    _pieDatabindingHelper.bindMappingDropdown(
                       objectType,objectColsForMapping
                    );
                    _pieDatabindingHelper.setDataInHtmlForMapping(
                        control
                     );
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                    _barLineDatabindingHelper.bindMappingDropdown(
                        objectType,
                        objectColsForMapping
                    );
                    _barLineDatabindingHelper.setDataInHtmlForMapping(control);
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                    _imageDatabindingHelper.bindMappingDropdown(
                       objectType,objectColsForMapping
                    );
                    _imageDatabindingHelper.setDataInHtmlForMapping(
                        control
                     );
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
                    _biTableDataBindingHelper.bindMappingDropdown(
                        control,
                        true,
                        objectColsForMapping,
                        objectType);
                    break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
                    _locationDatabindingHelper.bindMappingDropdown(
                       objectType,objectColsForMapping
                    );
                    _locationDatabindingHelper.setDataInHtmlForMapping(
                        control,objectType
                     );
                break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
                        _drilldownPieDatabindingHelper.bindMappingDropdown(
                           objectType,objectColsForMapping
                        );
                        _drilldownPieDatabindingHelper.setDataInHtmlForMapping(
                            control,objectType
                        );
                break;
                case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CUSTOM_CONTROL:
                break;    
            }
        }
        return {
            setData:function(options/*similar to globalData*/){
               var databindingObj = options.databindObject;
               var objectId = databindingObj.fnGetId(),
                    objectType = options.objectType,
                    mapDdlOptions ="",
                    strIntlsJson ="";
               
               if(objectType === MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt){
                   mapDdlOptions  = 
                       _utilities.getOfflineTableColsForCondLogicBinding(objectId);
               
                   strIntlsJson  = _utilities.getStringIntellisenseJson(
                       options.intlsJson);    
               }
               _setByObjectType({
                   objectType :objectType ,
                   databindingObj :databindingObj ,
                   objectColsForMapping :mapDdlOptions ,
                   intlsJson :strIntlsJson
               });
               
               mapDdlOptions  = _utilities.getMapDdlsOptionsByObjectType(
                   objectId,
                   objectType
                );
               _setMappingDataByCntrlType({
                   control :options.control,
                   cntrlType :options.controlType,
                   objectType :objectType ,
                   objectColsForMapping:mapDdlOptions
               });
            } 
        }
    })();    
    var _setInitialStateOfHtml = function(cntrlType,objectType){
        var allControls=null,
            allDivs =null,
            strDdlDefaultOption = '<option value="-1">Select</option>';
            _getHtmlControls.divCntrlDatabindCondLogic().html('');
            _getHtmlControls.divCntrlDatabindCondLogic().hide();
        switch (cntrlType){ 
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                    _labelDatabindingHelper.setInitialStateOfControl();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                allControls =  _getHtmlControls.allCheckboxDatabindingCntrls();
                allControls.checkedValue.html(strDdlDefaultOption);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
            case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                allControls =  _getHtmlControls.allSliderDatabindingCntrls();
                allControls.ddlValue.html(strDdlDefaultOption);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
                allControls =  _getHtmlControls.allAngularAndCylinderDatabindingCntrls();
                allControls.value.html(strDdlDefaultOption);
                allControls.maxValue.html(strDdlDefaultOption);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                allControls = _getHtmlControls.allTextboxToggleDatabindingCntrls();
                allControls.displayText.html(strDdlDefaultOption);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                allControls = _getHtmlControls.allDropDownDatabindingCntrls();
                allControls.optionText.html(strDdlDefaultOption);
                allControls.optionValue.html(strDdlDefaultOption);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                allControls =  _getHtmlControls.allListDatabindingCntrls();
                allControls.rowId.html(strDdlDefaultOption);
                allControls.title.html(strDdlDefaultOption);
                allControls.subtitle.html(strDdlDefaultOption);
                allControls.addInfo.html(strDdlDefaultOption);
                allControls.image.html(strDdlDefaultOption);
                allControls.countField.html(strDdlDefaultOption);
                allControls.selectedRow.html(strDdlDefaultOption);
                allControls.date.html(strDdlDefaultOption);
                allControls.dateFormat.html(_listDataBindingHelper.getDateFormatDropDown());
                allControls.info.html(strDdlDefaultOption);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                allControls = _getHtmlControls.allPieDatabindingCntrls();
                allControls.title.html(strDdlDefaultOption);
                allControls.value.html(strDdlDefaultOption);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                    _barLineDatabindingHelper.setInitialStateOfControl();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                allControls = _getHtmlControls.allImageDatabindingCntrls();
                allControls.source.html(strDdlDefaultOption);
                allControls.title.html(strDdlDefaultOption);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
                allControls = _getHtmlControls.allLocationDatabindingCntrls();
                allControls.latitude.html(strDdlDefaultOption);
                allControls.longitude.html(strDdlDefaultOption);
            break; 
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
                allControls = _getHtmlControls.allDrilldownPieChartDatabindCntrls(
                        objectType);
                allControls.sliceTitle.html(strDdlDefaultOption);
                allControls.value.html(strDdlDefaultOption);
                allControls.subSliceTitle.html(strDdlDefaultOption);
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CUSTOM_CONTROL:    
            break;    
        }
    }
    /**
    * Sets the label of the Dataobject Dropdownl 
    * If text is provided then that value is set otherwise default value for
    * text is considered by objectType
    *
    * @method _setLabelOfDdlDataObject
    * @param {String} text 
    * @param {Object} objectType
    */
    function _setLabelOfDdlDataObject(text,objectType){
        var strLabelText = text;
        if(!mfUtil.isNullOrUndefined(strLabelText) && !mfUtil.isEmptyString(strLabelText)){
            $('#spanCntrlsDatabindDdlObjectLabel').text(strLabelText);
        }
        else{
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                       strLabelText = "Offline Datatable Object";
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                       strLabelText = "Offline Datatable Object";
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                        strLabelText = "Database Object";
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                        strLabelText = "Web service Object";
                    break;    
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                    strLabelText = "OData Object";
                    break;
            }
            $('#spanCntrlsDatabindDdlObjectLabel').text(strLabelText);
        }
    }
    var _imgDatabindInputParameters = (function(){
        function _showHide(inputParams){
            if($.isArray(inputParams) && inputParams.length>0){
                $('#imgCntrlsDatabindObjParameters').show();
            }
            else{
                $('#imgCntrlsDatabindObjParameters').hide();
            }
        }
        function _bindEvent(databindingObj){
            var aryInputParams=[],
                objCmdObject = null,
                strCmdId = "",
                objBindType = "",
                strBindTypeLabel="";
            if(!mfUtil.isNullOrUndefined(databindingObj) &&
                !mfUtil.isNullOrUndefined(databindingObj.id) &&
                !mfUtil.isEmptyString(databindingObj.id)){
            
                strCmdId = databindingObj.id;
                objBindType = MF_HELPERS.getCommandTypeByValue(
                                  databindingObj.bindObjType
                              );
                aryInputParams= MF_HELPERS.getInputParamsForCmdObject(
                                    strCmdId,
                                    objBindType
                                );
                switch (objBindType){
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                            strBindTypeLabel = "Database Object";
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                            strBindTypeLabel = "Webservice Object";
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                            strBindTypeLabel = "OData Object";
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                            strBindTypeLabel = "Offline Data Object";
                        break;
                }
               objCmdObject = MF_HELPERS.getCommandObjectByIdAndType(strCmdId,objBindType);
               //this has to be done this way because of he earlier code written for binding.
               if($.isArray(aryInputParams) && aryInputParams.length>0){
                   _bindDataLinkParameters(
                        objCmdObject,
                        aryInputParams,
                        "imgCntrlsDatabindObjParameters",
                        strBindTypeLabel,
                        databindingObj
                    );
               }
            }
        }
        return {
            /**
            * @method showHideByDatabindObj
            * @param {DatabindingObj} databindingObj
            */
            showHideByDatabindObj : function(databindingObj){
                var aryInputParams=[];
                if(!mfUtil.isNullOrUndefined(databindingObj) &&
                    !mfUtil.isNullOrUndefined(databindingObj.id) &&
                    !mfUtil.isEmptyString(databindingObj.id)){
                
                    var aryInputParams=  MF_HELPERS.getInputParamsForCmdObject(
                                            databindingObj.id,
                                            MF_HELPERS.getCommandTypeByValue(
                                                databindingObj.bindObjType
                                            )
                                        );
                    _showHide(aryInputParams);
                }
                else{
                    _showHide(aryInputParams);
                }
            },
            showHideByInputParams : function(inputParams){
                _showHide(inputParams);
            },
            bindEvent : function(databindingObj){
                _bindEvent(databindingObj);
            }
        };
    })();
    //controls binding helpers
    var _editableListHelper = (function () {
        // function _getDivForAddingMappingHtmlForWS(){
        //     return $('#WS_EditableList_Cmd_Items');
        // }
        function _getDivForAddingMappingHtmlByType(objectType/*MF_IDE_CONSTANTS.ideCommandObjectTypes*/) {
            var mappingDiv = '';
            switch (objectType) {
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                    mappingDiv = $('#Db_EditableList_Options_Div');
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                    mappingDiv = $('#OData_EditableList_Options_Div');
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                    mappingDiv = $('#WS_EditableList_Cmd_Items');
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                    mappingDiv = $('#DataBind_EditableList_Options_Div');
                break;    
            }
            return mappingDiv;
        }
        function _formMappingHtmlWithDropDwns(cntrl, objectType/*MF_IDE_CONSTANTS.ideCommandObjectTypes*/) {
            var $mappingDiv = _getDivForAddingMappingHtmlByType(objectType);
            var aryProperties = cntrl.properties;
            var strHtml = "";
            if (aryProperties && $.isArray(aryProperties) &&
            aryProperties.length > 0) {
                strHtml += '<div class="twoColFormW_160">';
                for (var i = 0; i <= aryProperties.length - 1; i++) {
                    strHtml += '<div class="row clearfix">';
                    strHtml += '<div class="PropName">' + aryProperties[i].name + '</div>';
                    strHtml += '<div class="PropMappedVal">' + '<select class="w_150p" onkeydown=\"return enterKeyFilter(event);\"><option value="-1">Select Column</option></select>' + '</div>';
                    strHtml += '</div>';
                }
                strHtml += '</div>';
            }
            $mappingDiv.html('');
            $mappingDiv.html(strHtml);
        }
        //initially for xml rpc mapping was done with this type
        //now it is changed. 
        function _formMappingHtmlWithBrowseButton(cntrl, objectType/*MF_IDE_CONSTANTS.ideCommandObjectTypes*/) {
            var $mappingDiv = _getDivForAddingMappingHtmlByType(objectType);
            var aryProperties = cntrl.properties;
            var strHtml = "";
            if (aryProperties && $.isArray(aryProperties) &&
            aryProperties.length > 0) {
                strHtml += '<div class="twoColFormW_160">';
                for (var i = 0; i <= aryProperties.length - 1; i++) {
                    strHtml += '<div class="row clearfix">';
                    strHtml += '<div class="PropName">' + aryProperties[i].name + '</div>';
                    strHtml += '<div class="PropMappedVal">';
                    strHtml += '<input type="text" class="MappedVal w_120p" onkeydown=\"return enterKeyFilter(event);\"><input  type="button" value="..." onkeydown=\"return enterKeyFilter(event);\">';
                    strHtml += '</div></div>';
                }
                strHtml += '</div>';
            }
            $mappingDiv.html('');
            $mappingDiv.html(strHtml);
        }
        function _saveMappedDataInControl(cntrl, objectType/*MF_IDE_CONSTANTS.ideCommandObjectTypes*/) {
            if (!cntrl || !objectType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            //object id is already saved
            var $mappingDiv = _getDivForAddingMappingHtmlByType(objectType);
            var isAtleastOnePropMapped = false;
            $mappingDiv.find('div.row').each(function (index) {
                var $drpDwn = $(this).find('.PropMappedVal > select');
                if ($drpDwn.val() !== "-1") {
                    isAtleastOnePropMapped = true;
                    return false;
                }
            });
            if (!isAtleastOnePropMapped) {
                throw new MfError(["Please map atleast one property to column"]);
            }
            $mappingDiv.find('div.row').each(function (index) {
                var $drpDwn = $(this).find('.PropMappedVal > select');
                var strPropName = $(this).find('.PropName').text().trim();
                if ($drpDwn.val() !== "-1") {
                    var objProp = cntrl.fnGetPropertyByName(strPropName);
                    if (!objProp) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                    objProp.mappedDataCol = $drpDwn.val();
                }
            });
        }
        function _fillMappedDataInHtml(cntrl, objectType/*MF_IDE_CONSTANTS.ideCommandObjectTypes*/) {
            if (!cntrl || !objectType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var $mappingDiv = _getDivForAddingMappingHtmlByType(objectType);
            $mappingDiv.find('div.row').each(function (index) {
                var $drpDwn = $(this).find('.PropMappedVal > select');
                var strPropName = $(this).find('.PropName').text().trim();
                var objProp = cntrl.fnGetPropertyByName(strPropName);
                if (!objProp) throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                $drpDwn.val(objProp.mappedDataCol);
            });
        }
        function _bindMappingDropdownsByType(objectType, options) {
            if (!objectType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var $mappingDiv = _getDivForAddingMappingHtmlByType(objectType);
            $mappingDiv.find('div.row').each(function (index) {
                var $drpDwn = $(this).find('.PropMappedVal > select');
                $drpDwn.html('');
                $drpDwn.html(options);
            });
        }
        function _fillDataObjectDtlsInPropSheetHtml(cntrl) {
            if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var objPropCntrl = PROP_JSON_HTML_MAP.editableList.propSheetCntrl.Data.getDataObject();
            var objDatabindObject = cntrl.fnGetDatabindingObj();
            if (objDatabindObject) {
                if (objDatabindObject.name) {
                    objPropCntrl.control.val(objDatabindObject.name);
                }
                else {
                    objPropCntrl.control.val("Select Object");
                }
            }
        }
        return {
            formMappingHtmlForDb: function (cntrl) {
                if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _formMappingHtmlWithDropDwns(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.db);
            },
            formMappingHtmlForWebSrvc: function (cntrl, wsType/*MF_IDE_CONSTANTS.webServiceType*/) {
                if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                switch (wsType) {
                    case MF_IDE_CONSTANTS.webServiceType.http:
                        _formMappingHtmlWithDropDwns(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.http);
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.wsdlSoap:
                        _formMappingHtmlWithDropDwns(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl);
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.xmlRpc:
                        //_formMappingHtmlWithBrowseButton(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc);
                        _formMappingHtmlWithDropDwns(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl);
                        break;
                }
            },
            formMappingHtmlForOdata: function (cntrl) {
                if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _formMappingHtmlWithDropDwns(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.odata);
            },
            formMappingHtml:function(cntrl,objectType){
                if (!cntrl || !objectType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _formMappingHtmlWithDropDwns(cntrl,objectType);
            },
            processSaveMappedDataForDb: function (cntrl) {
                if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _saveMappedDataInControl(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.db);
                //_fillDataObjectDtlsInPropSheetHtml(cntrl);
            },
            processSaveMappedDataForWebServ: function (cntrl, wsType) {
                if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                if(!wsType)wsType = MF_IDE_CONSTANTS.webServiceType.http;
                switch (wsType) {
                    case MF_IDE_CONSTANTS.webServiceType.http:
                        _saveMappedDataInControl(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.http);
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.wsdlSoap:
                        _saveMappedDataInControl(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl);
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.xmlRpc:
                        _saveMappedDataInControl(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc);
                        //_formMappingHtmlWithBrowseButton(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc);
                        break;
                }
               // _fillDataObjectDtlsInPropSheetHtml(cntrl);
            },
            processSaveMappedDataForOdata: function (cntrl) {
                if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _saveMappedDataInControl(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.odata);
                //_fillDataObjectDtlsInPropSheetHtml(cntrl);
            },
            fillMappedDataInHtmlForDb: function (cntrl) {
                if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _fillMappedDataInHtml(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.db);
            },
            fillMappedDataInHtmlForWebSrv: function (cntrl, wsType) {
                if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                if(!wsType)wsType = MF_IDE_CONSTANTS.ideCommandObjectTypes.http;
                switch(wsType){
                    case MF_IDE_CONSTANTS.webServiceType.http:
                         _fillMappedDataInHtml(cntrl,MF_IDE_CONSTANTS.ideCommandObjectTypes.http);
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.wsdlSoap:
                         _fillMappedDataInHtml(cntrl,MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl);
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.xmlRpc:
                         _fillMappedDataInHtml(cntrl,MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc);
                        break;
                }
                //_fillMappedDataInHtmlWithDropDwn(cntrl,wsType);
            },
            fillMappedDataInHtmlForOdata: function (cntrl) {
                if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _fillMappedDataInHtml(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.odata);
            },
            bindMappingDropdownForDb: function (control,isEdit,options/*string*/) {
                if (!options) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _bindMappingDropdownsByType(MF_IDE_CONSTANTS.ideCommandObjectTypes.db, options);
                if(isEdit){
                    this.fillMappedDataInHtmlForDb(control);
                }
            },
            bindMappingDropdownForWs: function (control,isEdit,options/*string*/, wsType) {
                if (!options) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                //before it was different for different webservice 
                //now it is same for all webservice type.
                if(!wsType)wsType = MF_IDE_CONSTANTS.webServiceType.http;
                switch (wsType) {
                    case MF_IDE_CONSTANTS.webServiceType.http:
                        _bindMappingDropdownsByType(MF_IDE_CONSTANTS.ideCommandObjectTypes.http, options);
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.wsdlSoap:
                        _bindMappingDropdownsByType(MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl, options);
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.xmlRpc:
                        _bindMappingDropdownsByType(MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc, options);
                        break;
                }
                if(isEdit){
                    this.fillMappedDataInHtmlForWebSrv(control,wsType);
                }
            },
            bindMappingDropdownForOdata: function (control,isEdit,options/*string*/) {
                if (!options) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _bindMappingDropdownsByType(MF_IDE_CONSTANTS.ideCommandObjectTypes.odata, options);
                if(isEdit){
                    this.fillMappedDataInHtmlForOdata(control);
                }
            },
            bindMappingDropdownsByObjectType: function (objectType,options/*string*/) {
                if (!options) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _bindMappingDropdownsByType(objectType, options);
            },
            setMappedDataInHtml: function (cntrl,objectType) {
                if (!cntrl || !objectType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _fillMappedDataInHtml(cntrl,objectType);
            },
            saveMappedDataInControl: function (cntrl,objectType) {
                if (!cntrl || !objectType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _saveMappedDataInControl(cntrl,objectType);
            },
            setDataInPropSheetHtml:function(control){
                _fillDataObjectDtlsInPropSheetHtml(control);
            }
        };
    })();
    var _biTableDataBindingHelper = (function () {
        var jqueryDataKey ="divData";
        var datetimeDefaultFormat = "dd/MM/yyyy";
        function _getDivForAddingMappingHtmlByType(objectType/*MF_IDE_CONSTANTS.ideCommandObjectTypes*/) {
            if(!objectType)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var mappingDiv = '';
            switch (objectType) {
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                    mappingDiv = $('#Db_Table_Options_Mapping_Div');
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                    mappingDiv = $('#OData_Table_Options_Mapping_Div');
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                    mappingDiv = $('#WS_Table_Options_Mapping_Div');
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                    mappingDiv = $('#DataBind_Table_Options_Mapping_Div');
                break;    
            }
            return mappingDiv;
        }
        var _tableHtmlHelper = (function () {
            function _getTableStartTag() {
                return '<table class="detailsTable">';
            }
            function _getTableEndTag() {
                return '</table>';
            }
            function _getMappingTableHeadRow() {
                var strHtml = '<thead><tr>';
                strHtml += '<th class="w_170p">Header Text</th><th class="w_170p">Data Column</th><th class="w_170p">Data Type</th>';
                strHtml += '<th class="w_100p">Date Format <span class="astric">*</span></th><th class="w_100p">Display Format<span class="astric">**</span></th><th class="w_100p">Mult Factor</th>';
                strHtml += '<th class="w_100p">Prefix</th><th class="w_100p">Suffix</th><th class="w_150p"></th><th class="w_30p"></th></tr></thead>';
                return strHtml;
            } 
            function _getTBodyStartTag() {
                return '<tbody>';
            }
            function _getTBodyEndTag() {
                return '</tbody>';
            }
            function _getColumnsOfRow(isAdditionRow){
                var strHtml = "";
                strHtml += '<td class="w_170p"><input type="text" class="HeaderText w_120p" onkeydown=\"return enterKeyFilter(event);\"></td>';
                strHtml += '<td class="w_170p"><select  class="TableColumn w_120p" onkeydown=\"return enterKeyFilter(event);\"><option value="-1">Select Column</option></select></td>';
                strHtml += '<td class="w_170p"><select  class="DataType w_120p" onkeydown=\"return enterKeyFilter(event);\">' + _getOptionsForDatatype() + '</select></td>';
                strHtml += '<td class="w_100p"><input type="text" class="DataFormat w_100p hide" value="dd/MM/yyyy" onkeydown=\"return enterKeyFilter(event);\"></td>';
                strHtml += '<td class="w_100p"><input type="text" class="DisplayFormat w_100p hide" value="dd/MM/yyyy" onkeydown=\"return enterKeyFilter(event);\"/>';
                strHtml += '<select  class="DisplayFormat w_100p hide" onkeydown=\"return enterKeyFilter(event);\">' + _getOptionsForDisplayFormatDdl() + '</select></td>';
                strHtml += '<td class="w_100p"><input type="text" class="Factor w_100p" onkeydown=\"return enterKeyFilter(event);\"></td>';
                strHtml += '<td class="w_100p"><input type="text" class="Prefix w_100p" onkeydown=\"return enterKeyFilter(event);\"></td>';
                strHtml += '<td class="w_100p"><input type="text" class="Suffix w_100p" onkeydown=\"return enterKeyFilter(event);\"></td>';
                if(!isAdditionRow){
                    strHtml += '<td class="w_150p"><span class="moveUp16x16"></span><span class="moveDown16x16"></span></td>';
                    strHtml += "<td class=\"w_30p\"><span class=\"crossImg16x16\"></span></td>";
                }
                else{
                    strHtml += "<td class=\"w_150p\"><span class=\"moveUp16x16 hide\"></span><span class=\"moveDown16x16 hide\"></span></td>";
                    strHtml += "<td class=\"w_30p\"><span class=\"addImg16x16\"></span></td>";
                }
                return strHtml;
            }
            function _getMappingTableRowWithCross() {
                var strHtml = '<tr class="MoveableRow">';
                strHtml += _getColumnsOfRow(false);
                strHtml += '</tr>';
                return strHtml;
            }
            function _getMappingTableRowOnlyAdd(){
                var strHtml = '<tr class="MoveableRow AdditionRow">';
                strHtml += _getColumnsOfRow(true);
                strHtml += '</tr>';
                return strHtml;
            }
            function _getOptionsForDatatype() {
                var options = '<option value="0">String</option>';
                options += '<option value="1">Number</option>';
                options += '<option value="2">Datetime</option>';
                return options;
            }
            function _getOptionsForDisplayFormatDdl() {
                var options = "";
                for (var i = 0; i <= 9; i++) {
                    options +='<option value="' + i + '">' + i + ' ( decimal ) ' + '</option>';
                }
                return options;
            }
            function _getMappingTable(objectType) {
                var $containerDiv = _getDivForAddingMappingHtmlByType(objectType);
                return $containerDiv.find('table');
            }
            
            function _formMappingHtml(cntrl, 
                        objectType/*MF_IDE_CONSTANTS.ideCommandObjectTypes*/,
                        mappingDdlOptions/*optional*/) {
               
               if(!cntrl || !objectType)
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                
                var $mappingDiv = _getDivForAddingMappingHtmlByType(objectType);
                var aryColumns = cntrl.fnGetColumns();
                var strHtml = _getTableStartTag();
                strHtml += _getMappingTableHeadRow();
                strHtml += _getTBodyStartTag();
                
                if (aryColumns && $.isArray(aryColumns) &&
                    aryColumns.length > 0) {
                    for (var i = 0; i <= aryColumns.length - 1; i++) {
                        strHtml += _getMappingTableRowWithCross();
                    }
                    strHtml += '</div>';
                }
                
                strHtml += _getTBodyEndTag();
                
                strHtml += _getTableEndTag();
                
                $mappingDiv.html('');
                
                $mappingDiv.html(strHtml);
                
                _addAdditionRowToTable(mappingDdlOptions,objectType);
                if(mappingDdlOptions){
                    _bindMappingDdlOptions(mappingDdlOptions,objectType,false);
                }
                _showUpDownArrowInTable(objectType);
                
                _setEventsOfCompleteTable(objectType);
                
                _setDataOfContainerDiv(objectType,mappingDdlOptions);
            }
            //Events 
            function _setEventsOfRow(tblRow/*jquery object*/) {
                var $tr = tblRow;
                $tr.find('.moveUp16x16').on('click', function (evnt) {
                    _moveUpDownColDetail(this, "up");
                    evnt.stopPropagation();
                });
                $tr.find('.moveDown16x16').on('click', function (evnt) {
                    _moveUpDownColDetail(this, "down");
                    evnt.stopPropagation();
                });
                $tr.find('.crossImg16x16').on('click',function(evnt){
                   var blnConfirm = confirm('Are you sure you want to remove this column.');
                      if(blnConfirm){
                      $(this).closest('tr').remove();
                   }
                });
                $tr.find('.addImg16x16').on('click',_processAddRowToTable);
                $tr.find('.DataType').on('change',function(evnt){
                   var $tr = $(this).closest('tr');
                   var $txtDataFormat = $tr.find('.DataFormat');
                   var $txtDisplayFormat = $tr.find('input[type="text"].DisplayFormat');
                   var $ddlDisplayFormat = $tr.find('select.DisplayFormat');
                   var $txtMultFactor = $tr.find('input[type="text"].Factor');
                   switch($(this).val()){
                       case "0":
                            $txtDataFormat.hide();
                            $txtDisplayFormat.hide();
                            $ddlDisplayFormat.hide();
                            $txtMultFactor.hide();
                            break;
                        case "1" :
                            $txtDataFormat.hide();
                            $txtDisplayFormat.hide();
                            $ddlDisplayFormat.show();
                            $ddlDisplayFormat.val("0");
                            $txtMultFactor.show();
                            break;
                        case "2":
                            $txtDataFormat.show();
                            $txtDisplayFormat.show();
                            $ddlDisplayFormat.hide();
                            $txtDisplayFormat.val(datetimeDefaultFormat);
                            $txtMultFactor.hide();
                            break;
                   }
                });
            }
            function _setEventsOfCompleteTable(objectType){
                var $tbl = _getMappingTable(objectType);
                $tbl.find('tbody tr').each(function(index){
                    _setEventsOfRow($(this));
                });
            }
            function _setEventOfLastAddedRow(objectType){
                var $tbl = _getMappingTable(objectType);
                _setEventsOfRow($tbl.find('tbody tr').not('.AdditionRow').last());
            }
            //Events
            
            function _showUpDownArrowInTable(objectType) {
                var tbl = _getMappingTable(objectType);
                var noOfRows = tbl.find("tbody tr").not('.AdditionRow').length;
                var count = 0;
                if (noOfRows === 1) {
                    $(tbl).find("tbody tr").not('.AdditionRow').each(function () {
                        $(this).find(".moveUp16x16,.moveDown16x16").hide();
                    });
                }
                else if (noOfRows === 2) {
                    $(tbl).find("tbody tr").not('.AdditionRow').each(function () {
                        if (count === 0) {
                            $(this).find(".moveDown16x16").show();
                            $(this).find(".moveUp16x16").hide();
                        }
                        else {
                            $(this).find(".moveDown16x16").hide();
                            $(this).find(".moveUp16x16").show();
                        }
                        count++;
                    });
                }
                else {
                    $(tbl).find("tbody tr").not('.AdditionRow').each(function () {
                        if (count === 0) {
                            $(this).find(".moveDown16x16").show();
                            $(this).find(".moveUp16x16").hide();
                        }
                        else if (count === noOfRows - 1) {
                            $(this).find(".moveDown16x16").hide();
                            $(this).find(".moveUp16x16").show();
                        }
                        else {
                            $(this).find(".moveDown16x16").show();
                            $(this).find(".moveUp16x16").show();
                        }
                        count++;
                    });
                }
            }
            function _moveUpDownColDetail(sender, command/*up down*/) {
                var rowToMove = "";
                switch (command.toString().toLowerCase()) {
                    case "up":
                        rowToMove = $(sender).parents('tr.MoveableRow:first');
                        var prev = rowToMove.prev('tr.MoveableRow');
                        if (prev.length == 1) {
                            prev.before(rowToMove);
                        }
                        break;
                    case "down":
                        rowToMove = $(sender).parents('tr.MoveableRow:first');
                        var next = rowToMove.next('tr.MoveableRow');
                        if (next.length == 1) {
                            next.after(rowToMove);
                        }
                        break;
                }
                var cntrl = MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
                if(!cntrl)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                var databindingObj = cntrl.fnGetDatabindingObj();
                var objectType = databindingObj.fnGetBindTypeObject();
                _showUpDownArrowInTable(objectType);
            }
            function _addAdditionRowToTable(mappingDdlOptions,objectType){
                if(!objectType)
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                
                var $mappingTable = _getMappingTable(objectType);
                var $tbody = $mappingTable.find('tbody');
                var $tr = $tbody.find('tr');
                
                if($tr.length>0){
                    $tbody.find('tr:last').after(_getMappingTableRowOnlyAdd());
                }
                else{
                    $tbody.append(_getMappingTableRowOnlyAdd());
                }
                
                
            }
            function _addDetailsRowToTable(mappingDdlOptions,objectType){
                if(!objectType)
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                
                var $mappingTable = _getMappingTable(objectType);
                var $tbody = $mappingTable.find('tbody');
                var $tr = $tbody.find('tr').not('.AdditionRow');
                
                if($tr.length>0){
                    $tr.last().after(_getMappingTableRowWithCross());
                }
                else{
                    $tbody.find('tr.AdditionRow').before(_getMappingTableRowWithCross());
                }
            }
            function _bindMappingDdlOptions(mappingDdlOptions,objectType,
                            onlyAdditionRow/*boolean*/){
                
                if(!mappingDdlOptions)
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                
                var $mappingTable = _getMappingTable(objectType);
                var $tbody = $mappingTable.find('tbody');
                if(onlyAdditionRow){
                   
                    $tbody.find('tr.AdditionRow').find('.TableColumn')
                    .html('').html('mappingDdlOptions');
                }
                else{
                    $tbody.find('tr').find('.TableColumn')
                    .html('').html('mappingDdlOptions');
                }
                    
            }
            
            function _checkIfHeaderTextIsUniqueInTbl(objectType){
                var aryHeaderTexts =[];
                var blnIsUnique = true;
                var $tbl = _getMappingTable(objectType);
                $tbl.find('tbody tr').each(function(index){
                    var $headerText = $(this).find('.HeaderText');
                    if($.inArray($headerText.val(),aryHeaderTexts) !== -1){
                        blnIsUnique = false;
                        return false;
                    }
                    else{
                        aryHeaderTexts.push($headerText.val());
                    }
                });
                return blnIsUnique;
            }
            function _validateColumnDetails(tblRow/*jquery object table row*/){
                if(!tblRow)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                var errors = [];
                var $row = tblRow;
                var $ddlTblCol = $row.find('.TableColumn');
                var $headerText = $row.find('.HeaderText');
                var $dataType =$row.find('.DataType');
                var $txtDataFormat = $row.find('.DataFormat');
                var $txtDisplayFormat = $row.find('input[type="text"].DisplayFormat');
                var $ddlDisplayFormat = $row.find('select.DisplayFormat');
                var $prefix = $row.find('.Prefix');
                var $suffix = $row.find('.Suffix');
                var $factor = $row.find('.Factor');
                
                if($ddlTblCol.val() === "-1" ){
                    errors.push("Please select data column");
                }
                if($headerText.val().trim() === ""){
                    errors.push("Please enter header text.");
                }
                if($dataType.val() !== "0"){
                    if($dataType.val() === "2"){
                        var dataFormatVal = $txtDataFormat.val();;
                        if( dataFormatVal === ""){
                            errors.push("Please enter data format.");
                        }
                        else if( $.isNumeric(dataFormatVal)){ 
                            if(dataFormatVal === "0" || dataFormatVal === "1"){
                            }
                            else{
                                errors.push("Please enter valid data format.");
                            }
                        }
                    }
                }
                if($factor.val()){
                    var blnIsValid = MF_HELPERS.ideValidation.validateMultiplicationFact($factor.val());
                    if(!blnIsValid){
                        errors.push("Please enter valid multiplication factor");
                    }
                }
                return errors;
            }
            function _validateCompleteColumnDetails(objectType){
                var errors = [];
                var arySubProcessErrors=[];
                var strErrorMsg = "";
                var $tbl = _getMappingTable(objectType);
                $tbl.find('tbody tr').each(function(index){
                    var blnValidate = false,
                        $headerText = $([]),
                        $self = $(this);
                    if($self.hasClass('AdditionRow')){
                        $headerText = $self.find('.HeaderText');
                        if($headerText.val().trim()!==""){
                            blnValidate = true;
                        }
                        else{
                            blnValidate = false;
                        }
                    }
                    else{
                        blnValidate = true;
                    }
                    if(blnValidate){
                        arySubProcessErrors = _validateColumnDetails($(this));
                        if(arySubProcessErrors.length>0){
                            strErrorMsg ="";
                            strErrorMsg +="At index : "+(parseInt(index)+1).toString()+" </br>";
                            for(var i=0;i<=arySubProcessErrors.length-1;i++){
                                strErrorMsg += arySubProcessErrors[i]+" </br>";
                            }
                            errors.push(strErrorMsg);
                        }
                    }
                });
                if(errors.length === 0){
                    var blnIsHeaderTxtUnique = _checkIfHeaderTextIsUniqueInTbl(objectType);
                    if(!blnIsHeaderTxtUnique){
                        strErrorMsg ="";
                        strErrorMsg += "Header text should be unique.";
                        errors.push(strErrorMsg);
                    }
                }
                return errors;
            }
            
            function _processAddRowToTable(evnt){
                try{
                var $row = $(this).closest('tr');
                var errors = _validateColumnDetails($row);
                if(errors.length>0){
                  throw new MfError(errors);
                }
                
                var cntrl = MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
                if(!cntrl)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                var databindingObj = cntrl.fnGetDatabindingObj();
                var objectType = databindingObj.fnGetBindTypeObject();
                var $containerDiv = _getDivForAddingMappingHtmlByType(objectType);
                var divData = $containerDiv.data(jqueryDataKey);
                
                var blnIsHeaderUnique = _checkIfHeaderTextIsUniqueInTbl(divData.objectType);
                if(!blnIsHeaderUnique){
                    throw new MfError(["Header text should be unique."]);
                }
                
                
                //var $row = tblRow;
                var $ddlTblCol = $row.find('.TableColumn');
                var $headerText = $row.find('.HeaderText');
                var $dataType =$row.find('.DataType');
                var $txtDataFormat = $row.find('.DataFormat');
                var $txtDisplayFormat = $row.find('input[type="text"].DisplayFormat');
                var $ddlDisplayFormat = $row.find('select.DisplayFormat');
                var $prefix = $row.find('.Prefix');
                var $suffix = $row.find('.Suffix');
                var $factor = $row.find('.Factor'); 
                
                
                _addDetailsRowToTable(
                    divData.drpDwnOptions,
                    divData.objectType);
                
                var $mappingTable = _getMappingTable(objectType);
                var $tbody = $mappingTable.find('tbody');
                var $tr = $tbody.find('tr').not('.AdditionRow');
                
                var objTableCol = new TableColumn();
                objTableCol.column = $ddlTblCol.val();
                objTableCol.header = $headerText.val();
                objTableCol.datatype =$dataType.val();
                switch(objTableCol.datatype){
                    case "0":
                           objTableCol.displayFormat ="";
                           objTableCol.dataFormat = "";
                        break;
                    case "1":
                            objTableCol.displayFormat = $ddlDisplayFormat.val();
                            objTableCol.dataFormat ="";
                        break;
                    case "2":
                        objTableCol.displayFormat =$txtDisplayFormat.val();
                        objTableCol.dataFormat =$txtDataFormat.val();
                        break;
                }
                objTableCol.prefix = $prefix.val();
                objTableCol.suffix = $suffix.val();
                objTableCol.factor = $factor.val();
                _fillDataToHtml($tr.last(),objTableCol,divData.drpDwnOptions);
                _setEventOfLastAddedRow(objectType);
                _showUpDownArrowInTable(objectType);
                _resetAdditionRow(objectType);
                }catch(error){
                    if (error instanceof MfError){
                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                    }else{
                        console.log(error);
                    }
                } 

            }
            function _fillDataToHtml(tblRow/*jquery*/,
                    tableColumnVal/*TableColumnObject*/,dropDownOptions){
                
                if(!tblRow || !tableColumnVal || !dropDownOptions)
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                    
                var $row = tblRow;
                var $ddlTblCol = $row.find('.TableColumn');
                var $headerText = $row.find('.HeaderText');
                var $dataType =$row.find('.DataType');
                var $txtDataFormat = $row.find('.DataFormat');
                var $txtDisplayFormat = $row.find('input[type="text"].DisplayFormat');
                var $ddlDisplayFormat = $row.find('select.DisplayFormat');
                var $prefix = $row.find('.Prefix');
                var $suffix = $row.find('.Suffix');
                var $factor = $row.find('.Factor');    
                 
                $ddlTblCol.html('');
                $ddlTblCol.html(dropDownOptions);
                
                $ddlTblCol.val(tableColumnVal.column);
                $headerText.val(tableColumnVal.header);
                $dataType.val(tableColumnVal.datatype);
                _showHideHtmlOfMappingTblRowByDatatype(tblRow,tableColumnVal.datatype);
                switch(tableColumnVal.datatype){
                    case "0":
                            // $txtDataFormat.hide();
                            // $txtDisplayFormat.hide();
                            // $ddlDisplayFormat.hide();
                        break;
                    case "1":
                            //$txtDataFormat.hide();
                            $ddlDisplayFormat.val(tableColumnVal.displayFormat);
                            //$txtDisplayFormat.hide();
                            //$ddlDisplayFormat.show();
                        break;
                    case "2":
                        $txtDataFormat.val(tableColumnVal.dataFormat);
                        //$txtDataFormat.show();
                        $txtDisplayFormat.val(tableColumnVal.displayFormat);
                        //$txtDisplayFormat.show();
                        //$ddlDisplayFormat.hide();
                        break;
                }
                $prefix.val(tableColumnVal.prefix);
                $suffix.val(tableColumnVal.suffix);
                $factor.val(tableColumnVal.factor);
            }
            
            function _processbindMappingDdlsByType(cntrl,isEdit,objectType, options) {
                if (!objectType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                // var $mappingDiv = _getDivForAddingMappingHtmlByType(objectType);
                // var aryTblColumns = cntrl.fnGetColumns();
                // var blnColumnsExist = false;
                // if(aryTblColumns && $.isArray(aryTblColumns) && aryTblColumns.length>0){
                //     blnColumnsExist = true;
                // }
                // $mappingDiv.find('table tbody tr').each(function (index) {
                //     var $row = $(this);
                //     var $drpDwn = $row.find('.TableColumn');
                //     $drpDwn.html('');
                //     $drpDwn.html(options);
                //     if(isEdit &&  blnColumnsExist && !$(this).hasClass('AdditionRow')){
                //         var objTableCol = aryTblColumns[index];
                //         $drpDwn.val(objTableCol.column);
                //         $row.find('.HeaderText').val(objTableCol.header);
                //         $row.find('.DataType').val(objTableCol.datatype);
                //         _showHideHtmlOfMappingTblRowByDatatype($row,objTableCol.datatype);
                //         switch(objTableCol.datatype){
                //             case "0":
                //                   $row.find('select.DisplayFormat').val("0");
                //                   $row.find('.DataFormat').val("");
                //                   //$row.find('select.DisplayFormat').hide();
                //                     //$row.find('.DataFormat').hide();
                //                 break;
                //             case "1":
                //                     $row.find('select.DisplayFormat').val(objTableCol.displayFormat);
                //                     $row.find('.DataFormat').val("");
                //                     //$row.find('select.DisplayFormat').show();
                //                 break;
                //             case "2":
                //                 $row.find('input[type="text"].DisplayFormat').val(objTableCol.displayFormat);
                //                 $row.find('.DataFormat').val(objTableCol.dataFormat);
                //                 //$row.find('select.DisplayFormat').hide();
                //                 //$row.find('input[type="text"].DisplayFormat').show();
                //                 //$row.find('.DataFormat').hide();
                //                 break;
                //         }
                //         $row.find('.Prefix').val(objTableCol.prefix );
                //         $row.find('.Suffix').val(objTableCol.suffix);
                //         $row.find('.Factor').val(objTableCol.factor);
                //     }
                // });
                
                _bindMappingDdlsByObjectType(objectType,options);
                if(isEdit === true){
                    _setMappedDataInHtmlByObjectType(cntrl,objectType);
                }
                _removeDataOfContainerDiv();
                _setDataOfContainerDiv(objectType, options);
            }
            
            function _bindMappingDdlsByObjectType(objectType,options){
                if (!objectType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                var $mappingDiv = _getDivForAddingMappingHtmlByType(objectType);
                $mappingDiv.find('.TableColumn').html(options);
            }
            function _setMappedDataInHtmlByObjectType(control,objectType){
                if (!objectType || !control) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                var $mappingDiv = _getDivForAddingMappingHtmlByType(objectType);
                var aryTblColumns = control.fnGetColumns();
                if(aryTblColumns && $.isArray(aryTblColumns) && aryTblColumns.length>0){
                    $mappingDiv.find('table tbody tr').each(function (index) {
                        var $row = $(this);
                        var $drpDwn = $row.find('.TableColumn');
                        if(!$(this).hasClass('AdditionRow')){
                            var objTableCol = aryTblColumns[index];
                            $drpDwn.val(objTableCol.column);
                            $row.find('.HeaderText').val(objTableCol.header);
                            $row.find('.DataType').val(objTableCol.datatype);
                            _showHideHtmlOfMappingTblRowByDatatype($row,objTableCol.datatype);
                            switch(objTableCol.datatype){
                                case "0":
                                       $row.find('select.DisplayFormat').val("0");
                                       $row.find('.DataFormat').val("");
                                    break;
                                case "1":
                                        $row.find('select.DisplayFormat').val(objTableCol.displayFormat);
                                        $row.find('.DataFormat').val("");
                                    break;
                                case "2":
                                    $row.find('input[type="text"].DisplayFormat').val(objTableCol.displayFormat);
                                    $row.find('.DataFormat').val(objTableCol.dataFormat);
                                    break;
                            }
                            $row.find('.Prefix').val(objTableCol.prefix );
                            $row.find('.Suffix').val(objTableCol.suffix);
                            $row.find('.Factor').val(objTableCol.factor);
                        }
                    });
                }
            }
            
            
            function _showHideHtmlOfMappingTblRowByDatatype(tblRow/*jquery object*/,datatype/*string value*/){
                if(!tblRow || !datatype)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                
                var $row = tblRow;
                var $txtDataFormat = $row.find('.DataFormat');
                var $txtDisplayFormat = $row.find('input[type="text"].DisplayFormat');
                var $ddlDisplayFormat = $row.find('select.DisplayFormat');
                switch(datatype){
                    case "0":
                            $txtDataFormat.hide();
                            $txtDisplayFormat.hide();
                            $ddlDisplayFormat.hide();
                        break;
                    case "1":
                            $txtDataFormat.hide();
                            $txtDisplayFormat.hide();
                            $ddlDisplayFormat.show();
                        break;
                    case "2":
                            $txtDataFormat.show();
                            $txtDisplayFormat.show();
                            $ddlDisplayFormat.hide();
                        break;
                }
            }
            function _resetAdditionRow(objectType){
                var $mappingTable = _getMappingTable(objectType);
                var $additionRow = $mappingTable.find('tr.AdditionRow');
                $additionRow.find('.TableColumn').val("-1");
                $additionRow.find('.HeaderText').val("");
                $additionRow.find('.DataType').val("0");
                $additionRow.find('.DataFormat').val(datetimeDefaultFormat).hide();
                $additionRow.find('input[type="text"].DisplayFormat').val(datetimeDefaultFormat).hide();
                $additionRow.find('select.DisplayFormat').val("0").hide();
                $additionRow.find('.Prefix').val("");
                $additionRow.find('.Suffix').val("");
                $additionRow.find('.Factor').val("");
            }
            
            function _setDataOfContainerDiv(objectType,drpDwnOptions){
                var $containerDiv = _getDivForAddingMappingHtmlByType(
                                        objectType);
                    $containerDiv.data(jqueryDataKey,{
                        objectType:objectType,
                        drpDwnOptions : drpDwnOptions
                    });                  
            }
            function _removeDataOfContainerDiv(){
                var cntrl = MF_HELPERS.currentObjectHelper.getCurrentWorkingControl();
                if(!cntrl)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
                var databindingObj = cntrl.fnGetDatabindingObj();
                var objectType = databindingObj.fnGetBindTypeObject();
                var $container = _getDivForAddingMappingHtmlByType(objectType);
                $container.removeData(jqueryDataKey);
            }
            function _fillSelectedValuesInPropSheetHtml(cntrl){
                mFicientIde.BiTableControlHelper.fillDatabindingSettingsInHtml(cntrl);
            }
            function _saveMappedData(cntrl,objectType){
                var aryErrors = _validateCompleteColumnDetails(objectType);
                var aryColumns = [];
                var objTableCol = null;
                if(aryErrors.length>0){
                    throw new MfError(aryErrors);
                }
                var $tbl = _getMappingTable(objectType);
                $tbl.find('tbody tr').each(function(index){
                    var $row = $(this),
                        blnAddDtls = false,
                        $headerText = $([]);
                    
                    if($row.hasClass('AdditionRow')){
                        $headerText = $row.find('.HeaderText');
                        if($headerText.val().trim()!==""){
                            blnAddDtls = true;
                        }
                        else{
                            blnAddDtls = false;
                        }
                    }
                    else{
                        blnAddDtls = true;
                    }
                    if(blnAddDtls){
                        objTableCol = new TableColumn();
                        objTableCol.column = $row.find('.TableColumn').val();
                        objTableCol.header = $row.find('.HeaderText').val();
                        objTableCol.datatype =$row.find('.DataType').val();
                        switch(objTableCol.datatype){
                            case "0":
                                   objTableCol.displayFormat ="";
                                   objTableCol.dataFormat = "";
                                break;
                            case "1":
                                    objTableCol.displayFormat = $row.find('select.DisplayFormat').val();
                                    objTableCol.dataFormat ="";
                                break;
                            case "2":
                                objTableCol.displayFormat = $row.find('input[type="text"].DisplayFormat').val();
                                objTableCol.dataFormat =$row.find('.DataFormat').val();
                                break;
                        }
                        objTableCol.prefix = $row.find('.Prefix').val();
                        objTableCol.suffix = $row.find('.Suffix').val();
                        objTableCol.factor = $row.find('.Factor').val();
                        objTableCol.index = index;
                        aryColumns.push(objTableCol);
                    }
                });
                cntrl.fnSetColumns(aryColumns);
            }
            return {
                formHtml: function (cntrl, objectType) {
                    _formMappingHtml(cntrl, objectType);
                },
                //processAddAdditionRowToTable:function(mappingDdlOptions,objectType){
                //     _addAdditionRowToTable(mappingDdlOptions,objectType);
                // },
                processBindMappingDdlsByType:function(cntrl,isEdit,objectType,options){
                    _processbindMappingDdlsByType(cntrl,isEdit,objectType, options);
                },
                processSaveMappedData :function(cntrl,objectType){
                    _saveMappedData(cntrl,objectType);
                    //_fillSelectedValuesInPropSheetHtml(cntrl);
                },
                setDataInPropSheetHtml:function(control){
                    _fillSelectedValuesInPropSheetHtml(control);
                },
                bindMappingDdlByObjectType:function(objectType,options){
                    _bindMappingDdlsByObjectType(objectType,options);
                },
                setMappedDataInHtml:function(control,objectType){
                    _setMappedDataInHtmlByObjectType(control,objectType);
                }
            };
        })();

        return{ 
            formMappingTableHtml: function (cntrl, objectType) {
                _tableHtmlHelper.formHtml(cntrl, objectType);
            },
            formMappingTableHtmlForWs: function (cntrl, webserviceType/*MF_IDE_CONSTANTS.webserviceType*/) {
                //if(!webserviceType)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                var objectType = MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                switch(webserviceType){
                    case MF_IDE_CONSTANTS.webServiceType.http:
                            objectType = MF_IDE_CONSTANTS.ideCommandObjectTypes.http;
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.xmlRpc:
                            objectType = MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc;
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.wsdlSoap:
                            objectType = MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        break;
                }
                _tableHtmlHelper.formHtml(cntrl, objectType);
            },
            // processAddRowToTable:function(mappingDdlOptions,objectType){
            //     _tableHtmlHelper.processAddAdditionRowToTable(mappingDdlOptions,objectType);
            // },
            bindMappingDropdown: function (cntrl,isEdit,options/*string*/,objectType) {
                if (!options) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _tableHtmlHelper.processBindMappingDdlsByType(cntrl,isEdit,objectType, options);
            },
            bindMappingDropdownForWs: function (cntrl,isEdit,options/*string*/,wsType/*MF_IDE_CONSTANTS.webserviceType*/) {
                //if (!wsType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                var objectType = MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                switch(wsType){
                    case MF_IDE_CONSTANTS.webServiceType.http:
                            objectType = MF_IDE_CONSTANTS.ideCommandObjectTypes.http;
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.xmlRpc:
                            objectType = MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc;
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.wsdlSoap:
                            objectType = MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        break;
                }
               _tableHtmlHelper.processBindMappingDdlsByType(cntrl,isEdit,objectType, options);
            },
            processSaveMappedDataForDb: function (cntrl) {
                if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _tableHtmlHelper.processSaveMappedData(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.db);
                //_fillDataObjectDtlsInPropSheetHtml(cntrl);
            },
            processSaveMappedDataForOdata: function (cntrl) {
                if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _tableHtmlHelper.processSaveMappedData(cntrl, MF_IDE_CONSTANTS.ideCommandObjectTypes.odata);
                //_fillDataObjectDtlsInPropSheetHtml(cntrl);
            },
            processSaveMappedDataForWs: function (cntrl,webServiceType) {
                if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                webServiceType =MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl; 
                _tableHtmlHelper.processSaveMappedData(cntrl,webServiceType);
                //_fillDataObjectDtlsInPropSheetHtml(cntrl);
            },
            setDataInPropSheetHtml:function(control){
                _tableHtmlHelper.setDataInPropSheetHtml(control);
            },
            setMappedDataInHtml: function (cntrl,objectType) {
               if (!cntrl || !objectType) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
               _tableHtmlHelper.setMappedDataInHtml(cntrl,objectType);
            },
            saveMappedDataInControl: function (cntrl,objectType) {
                if (!cntrl) throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                _tableHtmlHelper.processSaveMappedData(cntrl,objectType);
            }
        };
    })();
    var _listDataBindingHelper =(function(){
        function _showHideDatabindCntrlsByListTypeForDb(cntrl){
           if(!cntrl)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
           var listType =cntrl.fnGetListTypeByValue();
           if(!listType)throw new MfError(["Please select list type. "]);
           $('#Db_List_RowId').show();
           $('#Db_List_Title').show();
           var $divSubtitle = $('#Db_List_SubTitle');
           var $divInfo = $('#Db_List_Info');
           var $divAdditionalInfo = $('#Db_List_AddInfo');
           var $divImageName = $('#Db_List_ImageName');
           var $divCountField = $('#Db_List_CountField');
           var $divSelectedRow = $('#Db_List_SelectedRow');
           var $divDate = $('#Db_List_Date');
           var rowClickable = cntrl.fnGetRowClickable();
           $divSubtitle.hide();
           $divInfo.hide();
           $divAdditionalInfo.hide();
           $divImageName.hide();
           $divCountField.hide();
           $divSelectedRow.hide();
           $divDate.hide();
           switch(listType){
               case MF_IDE_CONSTANTS.listCntrlType.Basic:
                case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                    break;
               case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                   $divSubtitle.show();
                   $divInfo.show();
                   $divAdditionalInfo.show();
                   break;
               case MF_IDE_CONSTANTS.listCntrlType.Icon:
               case MF_IDE_CONSTANTS.listCntrlType.Simple:
                   $divImageName.show();
                   break;
               case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                   $divSubtitle.show();
                   $divInfo.show();
                   $divAdditionalInfo.show();
                   $divImageName.show();
                   break;
               case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                   $divSubtitle.show();
                   $divImageName.show();
                   break;
               case MF_IDE_CONSTANTS.listCntrlType.EventList:
                   $divSubtitle.show();
                   $divInfo.show();
                   $divAdditionalInfo.show();
                   $divDate.show();
               break;
           }
           if(cntrl.countBubble && cntrl.countBubble === "1"){
               $divCountField.show();
           }
           else{
               $divCountField.hide();
           }
           if(cntrl.fnIsRowClickableARowSelectionType()){
                $divSelectedRow.show();
            }
            else{
                $divSelectedRow.hide();
            }    
        }
        function _showHideDatabindCntrlsByListTypeForWs(cntrl){
            if(!cntrl)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var listType =cntrl.fnGetListTypeByValue();
            if(!listType)throw new MfError(["Please select list type. "]);
            $('#List_WsRowId').show();
            $('#List_WsTitle').show();
            var $divSubtitle = $('#List_WsSubtitle');
            var $divInfo = $('#List_WsInfo');
            var $divAdditionalInfo = $('#List_WsAddInfo');
            var $divImageName = $('#List_WsImage');
            var $divCountField = $('#List_WsCountField');
            var $divSelectedRow = $('#List_WsSelectedRow');
            var $divDate = $('#List_WsDate');
            var rowClickable = cntrl.fnGetRowClickable();
            $divSubtitle.hide();
            $divInfo.hide();
            $divAdditionalInfo.hide();
            $divImageName.hide();
            $divCountField.hide();
            $divSelectedRow.hide();
            $divDate.hide();
            switch(listType){
                case MF_IDE_CONSTANTS.listCntrlType.Basic:
                 case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                     break;
                case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                    $divSubtitle.show();
                    $divInfo.show();
                    $divAdditionalInfo.show();
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.Icon:
               case MF_IDE_CONSTANTS.listCntrlType.Simple:
                    $divImageName.show();
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                    $divSubtitle.show();
                    $divInfo.show();
                    $divAdditionalInfo.show();
                    $divImageName.show();
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                    $divSubtitle.show();
                    $divImageName.show();
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.EventList:
                    $divSubtitle.show();
                    $divInfo.show();
                    $divAdditionalInfo.show();
                    $divDate.show();
                break;
            }
            if(cntrl.countBubble && cntrl.countBubble === "1"){
                $divCountField.show();
            }
            else{
                $divCountField.hide();
            }
            if(cntrl.fnIsRowClickableARowSelectionType()){
                $divSelectedRow.show();
            }
            else{
                $divSelectedRow.hide();
            }    
        }
        function _showHideDatabindCntrlsByListTypeForOdata(cntrl){
            if(!cntrl)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var listType =cntrl.fnGetListTypeByValue();
            if(!listType)throw new MfError(["Please select list type. "]);
            $('#OData_List_RowId').show();
            $('#OData_List_Title').show();
            var $divSubtitle = $('#OData_List_SubTitle');
            var $divInfo = $('#OData_List_Info');
            var $divAdditionalInfo = $('#OData_List_AddInfo');
            var $divImageName = $('#OData_List_ImageName');
            var $divCountField = $('#OData_List_CountField');
            var $divSelectedRow = $('#OData_List_SelectedRow');
            var $divDate = $('#OData_List_Date');
            var rowClickable = cntrl.fnGetRowClickable();
            $divSubtitle.hide();
            $divInfo.hide();
            $divAdditionalInfo.hide();
            $divImageName.hide();
            $divCountField.hide();
            $divSelectedRow.hide();
            $divDate.hide();
            switch(listType){
                case MF_IDE_CONSTANTS.listCntrlType.Basic:
                 case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                     break;
                case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                    $divSubtitle.show();
                    $divInfo.show();
                    $divAdditionalInfo.show();
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.Icon:
               case MF_IDE_CONSTANTS.listCntrlType.Simple:
                    $divImageName.show();
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                    $divSubtitle.show();
                    $divInfo.show();
                    $divAdditionalInfo.show();
                    $divImageName.show();
                   break;
                case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                    $divSubtitle.show();
                    $divImageName.show();
                   break;
                case MF_IDE_CONSTANTS.listCntrlType.EventList:
                    $divSubtitle.show();
                    $divInfo.show();
                    $divAdditionalInfo.show();
                    $divDate.show();
                break;
            }
            if(cntrl.countBubble && cntrl.countBubble === "1"){
                $divCountField.show();
            }
            else{
                $divCountField.hide();
            }
            if(cntrl.fnIsRowClickableARowSelectionType()){
                $divSelectedRow.show();
            }
            else{
                $divSelectedRow.hide();
            }    
        }
        function _showHideDatabindCntrlsByListType(cntrl){
            if(!cntrl)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var listType =cntrl.fnGetListTypeByValue();
            if(!listType)throw new MfError(["Please select list type. "]);
            
            var allDataBindCntrls = _getHtmlControls.allListDatabindingCntrlsDiv();
            allDataBindCntrls.rowId.show();
            allDataBindCntrls.title.show();
            
            var rowClickable = cntrl.fnGetRowClickable();
           
            allDataBindCntrls.subTitle.hide();
            allDataBindCntrls.info.hide();
            allDataBindCntrls.additionalInfo.hide();
            allDataBindCntrls.imageName.hide();
            allDataBindCntrls.countField.hide();
            allDataBindCntrls.selectedRow.hide();
            allDataBindCntrls.date.hide();
            allDataBindCntrls.dateFormat.hide();
            
            switch(listType){
                case MF_IDE_CONSTANTS.listCntrlType.Basic:
                 case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                     break;
                case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                        allDataBindCntrls.subTitle.show();
                        allDataBindCntrls.info.show();
                        allDataBindCntrls.additionalInfo.show();
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.Icon:
               case MF_IDE_CONSTANTS.listCntrlType.Simple:
                        allDataBindCntrls.imageName.show();
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                        allDataBindCntrls.subTitle.show();
                        allDataBindCntrls.info.show();
                        allDataBindCntrls.additionalInfo.show();
                        allDataBindCntrls.imageName.show();
                   break;                   
                case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                        allDataBindCntrls.subTitle.show();
                        allDataBindCntrls.imageName.show();
                   break;                   
                case MF_IDE_CONSTANTS.listCntrlType.EventList:
                        allDataBindCntrls.subTitle.show();
                        allDataBindCntrls.info.show();
                        allDataBindCntrls.additionalInfo.show();
                        allDataBindCntrls.date.show();
                        allDataBindCntrls.dateFormat.show();
                break;
            }
            if(cntrl.countBubble && cntrl.countBubble === "1"){
                allDataBindCntrls.countField.show();
            }
            else{
                allDataBindCntrls.countField.hide();
            }
            if(cntrl.fnIsRowClickableARowSelectionType()){
                allDataBindCntrls.selectedRow.show();
            }
            else{
                allDataBindCntrls.selectedRow.hide();
            }
        }    
        function _getDateFormatDropDown(){
            var options = '';
            $.each(MF_IDE_CONSTANTS.dateFormat, function(){
                options += '<option value="' + this.id + '">' + this.UiName + '</option>';
            });
            return options;
        }
        function _bindDatabindDdlsByObjectType(objectType,options){
            if(!objectType)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                       $('#Db_List_Options_Div').find('select.BindValueDdl')
                       .each(function(index){
                           $(this).html('');
                           $(this).html(options);
                       });
                       $('[id$=ddlList_DateFormat]').html(_getDateFormatDropDown());
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                        $('#WS_List_Options').find('select.BindValueDdl')
                        .each(function(index){
                            $(this).html('');
                            $(this).html(options);
                        });
                       $('[id$=ddlList_Ws_DateFormat]').html(_getDateFormatDropDown());
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                        $('#OData_List_Options_Div').find('select.BindValueDdl')
                        .each(function(index){
                            $(this).html('');
                            $(this).html(options);
                        });
                       $('[id$=ddlODataList_DateFormat]').html(_getDateFormatDropDown());
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                    _getHtmlControls.divListCont().find('select.BindValueDdl')
                    .each(function(index){
                        $(this).html('');
                        $(this).html(options);
                    });
                       $('#ddlDataBindList_DateFormat').html(_getDateFormatDropDown());
                break;    
            }   
        }
        function _validateDatabindingSaving(objectType/*MF_IDE_CONSTANTS.ideCommandObjectTypes*/,listType){
            if(!objectType)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var aryErrors=[];
            if(listType !== MF_IDE_CONSTANTS.listCntrlType.CustomList){
                var $ddlRowId ,$ddlTitle;
                switch(objectType){
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                            //$('[id$="ddlCtrlDbCommand"]');
                            $ddlRowId = $('[id$="ddlList_RowId"]');
                            $ddlTitle  =$('[id$="ddlList_Title"]');
                         break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                            $ddlRowId = $('[id$="ddlList_Ws_RowId"]');
                            $ddlTitle  =$('[id$="ddlList_Ws_Title"]');
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                            $ddlRowId = $('[id$="ddlODataList_RowId"]');
                            $ddlTitle  =$('[id$="ddlODataList_Title"]');
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                        $ddlRowId = _getHtmlControls.ddlListRowId();
                        $ddlTitle  =_getHtmlControls.ddlListTitle();
                    break;    
                }
                if($ddlRowId.val()==="-1"){
                    aryErrors.push("Please select a option for RowID");
                }
                if($ddlTitle.val()==="-1"){
                    aryErrors.push("Please select a option for Title");
                }
            }
            return aryErrors;
        }
        function _getDatabindingControlsByBindObjectType(bindObjectType){
            if(!bindObjectType)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var bindControls = {};
            switch(bindObjectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                        bindControls.rowId = $('[id$="ddlList_RowId"]');
                        bindControls.title =$('[id$="ddlList_Title"]');
                        bindControls.subtitle =$('[id$="ddlList_SubTitle"]');
                        bindControls.info =$('[id$="ddlList_Info"]');
                        bindControls.addInfo =$('[id$="ddlList_AddInfo"]');
                        bindControls.image =$('[id$="ddlList_Image"]');
                        bindControls.countField=$('[id$="ddlList_CountField"]');
                        bindControls.selectedRow=$('[id$="ddlList_SelectedRow"]');
                        bindControls.date = $('[id$="ddlList_Date"]');
                        bindControls.dateFormat = $('[id$="ddlList_DateFormat"]');
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                       bindControls.rowId = $('[id$="ddlList_Ws_RowId"]');
                       bindControls.title = $('[id$="ddlList_Ws_Title"]');
                       bindControls.subtitle = $('[id$="ddlList_Ws_Subtitle"]');
                       bindControls.info = $('[id$="ddlList_Ws_Info"]');
                       bindControls.addInfo = $('[id$="ddlList_Ws_AddInfo"]');
                       bindControls.image = $('[id$="ddlList_Ws_Image"]');
                       bindControls.countField=$('[id$="ddlList_Ws_CountField"]');
                       bindControls.selectedRow=$('[id$="ddlList_Ws_SelectedRow"]');
                       bindControls.date = $('[id$="ddlList_Ws_Date"]');
                       bindControls.dateFormat = $('[id$="ddlList_Ws_DateFormat"]');
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                       bindControls.rowId =$('[id$="ddlODataList_RowId"]');
                       bindControls.title =$('[id$="ddlODataList_Title"]');
                       bindControls.subtitle = $('[id$="ddlODataList_SubTitle"]');
                       bindControls.info = $('[id$="ddlODataList_Info"]');
                       bindControls.addInfo = $('[id$="ddlODataList_AddInfo"]');
                       bindControls.image = $('[id$="ddlODataList_Image"]');
                       bindControls.countField=$('[id$="ddlODataList_CountField"]');
                       bindControls.selectedRow=$('[id$="ddlODataList_SelectedRow"]');
                       bindControls.date = $('[id$="ddlODataList_Date"]');
                       bindControls.dateFormat = $('[id$="ddlODataList_DateFormat"]');
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                   bindControls = _getHtmlControls.allListDatabindingCntrls();
                break;    
            }
            return bindControls;
        }
        function _saveMappingDetails(cntrl){
            var aryErrors = [];
            var bindObjectType = cntrl.fnGetBindTypeOfDatabindObj();
            var listType = cntrl.fnGetListTypeByValue();
            if(!listType)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
            aryErrors = _validateDatabindingSaving(bindObjectType,listType);
            if(aryErrors.length>0){
                throw new MfError(aryErrors);
            }
            //object id is already saved
            //var $ddlRowId, $ddlTitle,$ddlSubtitle,$ddlInformation,
            //$ddlAdditionalInformation,$ddlImageName;
            
            
            var objBindControls = _getDatabindingControlsByBindObjectType(bindObjectType);
            switch(listType){
                case MF_IDE_CONSTANTS.listCntrlType.Basic:
                 case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                     cntrl.rowId = objBindControls.rowId.val();
                     cntrl.title = objBindControls.title.val();
                     break;
                case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                    cntrl.rowId = objBindControls.rowId.val();
                    cntrl.title = objBindControls.title.val();
                    cntrl.subtitle =objBindControls.subtitle.val()==="-1"?
                    "":objBindControls.subtitle.val();
                    
                    cntrl.information =objBindControls.info.val()==="-1"?
                    "":objBindControls.info.val();
                    
                    cntrl.additionalInfo =objBindControls.addInfo.val()==="-1"?
                                            "":objBindControls.addInfo.val();
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.Icon:
               case MF_IDE_CONSTANTS.listCntrlType.Simple:
                    cntrl.rowId = objBindControls.rowId.val();
                    cntrl.title = objBindControls.title.val();
                    cntrl.imageName =objBindControls.image.val()==="-1"?
                        "":objBindControls.image.val();
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                    cntrl.rowId =objBindControls.rowId.val();
                    cntrl.title = objBindControls.title.val();
                    cntrl.subtitle =objBindControls.subtitle.val()==="-1"?
                        "":objBindControls.subtitle.val();
                    
                    cntrl.information =objBindControls.info.val()==="-1"?
                        "":objBindControls.info.val();
                    
                    cntrl.additionalInfo =objBindControls.addInfo.val()==="-1"
                                            ?"":objBindControls.addInfo.val();
                    
                    cntrl.imageName =objBindControls.image.val()==="-1"?
                        "":objBindControls.image.val();                        
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                    cntrl.rowId =objBindControls.rowId.val();
                    cntrl.title = objBindControls.title.val();
                    cntrl.subtitle =objBindControls.subtitle.val()==="-1"?
                        "":objBindControls.subtitle.val();
                    
                    cntrl.imageName =objBindControls.image.val()==="-1"?
                        "":objBindControls.image.val();                        
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.EventList:
                    cntrl.rowId =objBindControls.rowId.val();
                    cntrl.title = objBindControls.title.val();
                    cntrl.subtitle =objBindControls.subtitle.val()==="-1"?
                        "":objBindControls.subtitle.val();
                    
                    cntrl.information =objBindControls.info.val()==="-1"?
                        "":objBindControls.info.val();
                    
                    cntrl.additionalInfo =objBindControls.addInfo.val()==="-1"
                                            ?"":objBindControls.addInfo.val();
                    
                    cntrl.date =objBindControls.date.val()==="-1"?
                        "":objBindControls.date.val();    
                    
                    cntrl.dateFormat =objBindControls.dateFormat.val()==="-1"?
                        "":objBindControls.dateFormat.val();    
                break;
            }
            if(cntrl.fnIsCountBubbleEnabled()){
                cntrl.countField = objBindControls.countField.val()==="-1"?
                        "":objBindControls.countField.val();
            }
            if(cntrl.fnIsRowClickableARowSelectionType()){
                cntrl.fnSetSelectedRowDatabind(
                        objBindControls.selectedRow.val()==="-1"?
                        "":objBindControls.selectedRow.val()
                    );    
            }
            else{
                cntrl.fnSetSelectedRowDatabind("");
            }    
        }
        function _fillSelectedValuesInPropSheetHtml(cntrl){
          mFicientIde.ListControlHelper.fillDatabindingSettingsInHtml(cntrl);
        }
        function _fillDatabindValuesInHtml(cntrl){
            
            var bindObjectType = cntrl.fnGetBindTypeOfDatabindObj();
            var listType = cntrl.fnGetListTypeByValue();
            if(!listType)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
            var objBindControls = _getDatabindingControlsByBindObjectType(bindObjectType);
            switch(listType){
                case MF_IDE_CONSTANTS.listCntrlType.Basic:
                 case MF_IDE_CONSTANTS.listCntrlType.Numbered:
                     objBindControls.rowId.val(cntrl.rowId);
                     objBindControls.title.val(cntrl.title );
                     break;
                case MF_IDE_CONSTANTS.listCntrlType.BasicWithDescription:
                    objBindControls.rowId.val(cntrl.rowId );
                   objBindControls.title.val(cntrl.title );
                   
                   cntrl.subtitle ?
                    objBindControls.subtitle.val(cntrl.subtitle ):
                    objBindControls.subtitle.val("-1");
                    
                    cntrl.information?
                    objBindControls.info.val(cntrl.information):
                    objBindControls.info.val("-1");
                    
                    cntrl.additionalInfo?
                    objBindControls.addInfo.val(cntrl.additionalInfo):
                    objBindControls.addInfo.val("-1");
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.Icon:
               case MF_IDE_CONSTANTS.listCntrlType.Simple:
                    objBindControls.rowId.val(cntrl.rowId );
                    objBindControls.title.val(cntrl.title );
                    
                    cntrl.imageName?objBindControls.image.val(cntrl.imageName)
                    :objBindControls.image.val(cntrl.imageName);
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.Thumbnail:
                    objBindControls.rowId.val(cntrl.rowId);
                    objBindControls.title.val(cntrl.title );
                    cntrl.subtitle ?
                    objBindControls.subtitle.val(cntrl.subtitle ):
                    objBindControls.subtitle.val("-1");
                    
                    cntrl.information?
                    objBindControls.info.val(cntrl.information):
                    objBindControls.info.val("-1");
                    
                    cntrl.additionalInfo?
                    objBindControls.addInfo.val(cntrl.additionalInfo):
                    objBindControls.addInfo.val("-1");    
                    
                    cntrl.imageName?objBindControls.image.val(cntrl.imageName)
                    :objBindControls.image.val(cntrl.imageName);
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.ImageList:
                    objBindControls.rowId.val(cntrl.rowId);
                    objBindControls.title.val(cntrl.title );
                    cntrl.subtitle ?
                    objBindControls.subtitle.val(cntrl.subtitle ):
                    objBindControls.subtitle.val("-1");
                    
                    cntrl.imageName?objBindControls.image.val(cntrl.imageName)
                    :objBindControls.image.val(cntrl.imageName);
                    break;
                case MF_IDE_CONSTANTS.listCntrlType.EventList:
                    objBindControls.rowId.val(cntrl.rowId);
                    objBindControls.title.val(cntrl.title );
                    cntrl.subtitle ?
                    objBindControls.subtitle.val(cntrl.subtitle ):
                    objBindControls.subtitle.val("-1");
                    
                    cntrl.information?
                    objBindControls.info.val(cntrl.information):
                    objBindControls.info.val("-1");
                    
                    cntrl.additionalInfo?
                    objBindControls.addInfo.val(cntrl.additionalInfo):
                    objBindControls.addInfo.val("-1");    
                    
                    cntrl.date?objBindControls.date.val(cntrl.date)
                    :objBindControls.date.val(cntrl.date);    
                    
                    cntrl.dateFormat?objBindControls.dateFormat.val(cntrl.dateFormat)
                    :objBindControls.dateFormat.val(cntrl.dateFormat);
                break;
            }
            if(cntrl.fnIsCountBubbleEnabled()){
                cntrl.countField?
                    objBindControls.countField.val(cntrl.countField):
                    objBindControls.countField.val("-1");
            }
            if(cntrl.fnIsRowClickableARowSelectionType()){
                var selectedRowDatabind = cntrl.fnGetSelectedRowDatabind();
                selectedRowDatabind ? objBindControls.selectedRow.val(selectedRowDatabind):
                    objBindControls.selectedRow.val('-1');
            }
        }
        return {
            showHideDatabindCntrlsByListType:function(cntrl,
                    objectType/*MF_IDE_CONSTANTS.ideCommandObjectTypes*/){
                        
                if(!objectType)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                switch(objectType){
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                            _showHideDatabindCntrlsByListTypeForDb(cntrl);
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                            _showHideDatabindCntrlsByListTypeForWs(cntrl);
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                            _showHideDatabindCntrlsByListTypeForOdata(cntrl);
                        break;
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                        _showHideDatabindCntrlsByListType(cntrl);
                    break;    
                }
            
            },
            bindDatabindingDdlsObjectType:function(
                cntrl,
                isEdit,
                objectType /*MF_IDE_CONSTANTS.ideCommandObjectTypes*/,
                options) {
                
                _bindDatabindDdlsByObjectType(objectType,options);
                if(isEdit){
                    _fillDatabindValuesInHtml(cntrl);
                }
            },
            getDateFormatDropDown:function(){
                _getDateFormatDropDown();
            },
            processSaveMappedData:function(cntrl){
                _saveMappingDetails(cntrl);
                //_fillSelectedValuesInPropSheetHtml(cntrl);
            },
            setDatabindingValuesInHtml:function(control){
                _fillDatabindValuesInHtml(control);
            },
            setDataInPropSheetHtml:function(control){
                _fillSelectedValuesInPropSheetHtml(control);
            }
        };
    })();
    var _labelDatabindingHelper=(function(){
        function _saveMappedDataInControl(control){
            // control.labelDataObjs = [];
            // var labelDataObj = null;
            // for (var i = 1; i <= 5; i++) {
            //     labelDataObj = new LabelDataObj();
                
            //     labelDataObj.para = $('#DataBind_Div_Para' + i).html().trim();
            //     labelDataObj.dataType = $('#DataBind_Dp_Para' + i + '_DataType').val().trim();
            //     labelDataObj.dataFormat = $('#DataBind_Dp_Para' + i + '_DataFormat').val().trim();
            //     labelDataObj.noOfDecimalPlaces = $('#DataBind_Dp_DecimalPlaces_Para' + i).val();
            //     labelDataObj.mulFactor = $('#DataBind_txt_Para' + i + '_MulFactor').val().trim();
            //     labelDataObj.selectedCol = $('#DataBind_Dp_Para' + i + '_Columns').val();
            //     control.fnAddLabelDataObj(labelDataObj);
            // }
            if(control.labelDataObjs != null 
                && control.labelDataObjs != undefined 
                && parseInt(control.labelDataObjs.length, 10) > 0){
                var i = 1;
                $.each(control.labelDataObjs, function(){
                    if(i <= 5 && this.para === i + "%")
                    {
                        this.selectedCol = $('#DataBind_Dp_Para' + i + '_Columns').val();
                        i = i +1;
                    }
                });
            }
        }
        function _enableDisableHtmlCntrlsBySelectedDatatype(templateNo,datatypeVal){
            if(datatypeVal){
                if (datatypeVal == "0") {
                    $('#DataBind_Dp_Para' + templateNo + '_DataFormat').attr("disabled", "disabled");
                    $('#DataBind_Dp_DecimalPlaces_Para' + templateNo).attr("disabled", "disabled");
                    $('#DataBind_txt_Para' + templateNo + '_MulFactor').attr("disabled", "disabled");
                }
                else if (datatypeVal == "1") {
                    $('#DataBind_Dp_Para' + templateNo + '_DataFormat').removeAttr("disabled");
                    $('#DataBind_Dp_DecimalPlaces_Para' + templateNo).prop("disabled", "disabled");
                    $('#DataBind_txt_Para' + templateNo + '_MulFactor').removeAttr("disabled");
                }
                else {
                    $('#DataBind_Dp_Para' + templateNo + '_DataFormat').removeAttr("disabled");
                    $('#DataBind_Dp_DecimalPlaces_Para' + templateNo).removeAttr("disabled");
                    $('#DataBind_txt_Para' + templateNo + '_MulFactor').removeAttr("disabled");
                }
            }
        }
        return {
            bindMappingDropdown:function(objectType,options){
                var $contDiv = $([]);
                switch(objectType){
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                            $contDiv = _getHtmlControls.divLabelCont();
                            $contDiv.find('.BindValueDdl').html(options);
                        break;
                }
            },
            setInitialStateOfControl:function(){
                var i=0,
                    strDefaultDdlOptions = '<option value="-1">Select</option>';
                for(i=1; i <= 5; i++){
                    $('#DataBind_Dp_Para' + i + '_DataType').val("0");
                    $('#DataBind_Dp_Para' + i + '_DataFormat').attr("disabled", "disabled");
                    $('#DataBind_Dp_DecimalPlaces_Para' + i).attr("disabled", "disabled");
                    $('#DataBind_txt_Para' + i + '_MulFactor').attr("disabled", "disabled");
                    $('#DataBind_Dp_Para'+i+'_Columns').html(strDefaultDdlOptions);
                }
            },
            saveMappedDataInControl:function(control){
                _saveMappedDataInControl(control);
            },
            setBindingDtlsInPropSheetHtml:function(control){
                var objPropCntrl = PROP_JSON_HTML_MAP.Label.propSheetCntrl.Data.getDataObjectCntrl();
                var objDatabindObject = control.fnGetDatabindingObj();
                if (objDatabindObject) {
                    _utilities.setDatabindObjectNameInPropsheetHtml(
                        objPropCntrl.control,
                        objDatabindObject.name
                    );
                }
            },
            setDataInHtmlForMapping:function(control){
                var row = 0,
                    labelDataObjs = null,
                    labelDataObjs = control.fnGetLabelDataObjs();
                
                if(labelDataObjs != null 
                     && labelDataObjs.length > 0){
                    
                    for(var i=0; i < labelDataObjs.length; i++){
                        row = i + 1;
                        if(labelDataObjs[i]) {
                            $('#DataBind_Dp_Para' + row + '_DataType').val(labelDataObjs[i].dataType);
                            $('#DataBind_Dp_Para' + row + '_DataFormat').val(labelDataObjs[i].dataFormat);
                            $('#DataBind_Dp_DecimalPlaces_Para' +row).val(labelDataObjs[i].noOfDecimalPlaces);
                            $('#DataBind_txt_Para' + row + '_MulFactor').val(labelDataObjs[i].mulFactor);
                            if(labelDataObjs[i].selectedCol){
                                $('#DataBind_Dp_Para' + row + '_Columns').val(
                                    labelDataObjs[i].selectedCol
                                ); 
                            }
                            _enableDisableHtmlCntrlsBySelectedDatatype(
                                row,
                                labelDataObjs[i].dataType
                            );    
                        }
                        
                    }
                } 
            }
        }
    })();
    var _checkboxDatabindingHelper=(function(){
        function _getAllControlsOfCheckboxDatabinding(objectType/*CONSTANTS.ideCommandObjectTypes*/){
            var objAllControls = null;
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                   objAllControls = _getHtmlControls.allCheckboxDatabindingCntrls();
                break;
            }
            return objAllControls;
        }
        function _bindMappingDropdown(objectType,options){
            var objAllControls =_getAllControlsOfCheckboxDatabinding(objectType);
            objAllControls.checkedValue.html('').html(options);
        }
        function _setDataInHtmlOfMappingCntrls(control){
            var objAllControls =_getAllControlsOfCheckboxDatabinding(objectType);
            objAllControls.checkedValue.val(control.fnGetCheckedVal());
        }
        function _setBindingDtlsInPropSheetHtml(control){
            var objPropCntrl = PROP_JSON_HTML_MAP.Checkbox.propSheetCntrl.Data.getDataObjectCntrl();
            var objDatabindObject = control.fnGetDatabindingObj();
            if (objDatabindObject) {
                _utilities.setDatabindObjectNameInPropsheetHtml(objPropCntrl.control,
                objDatabindObject.name);
            }
        }
        function _validateMappingSavings(allHtmlControls){
            var aryErrors =[];
            if(allHtmlControls.checkedValue.val() === "-1"){
                aryErrors.push('Please select a value for checkedvalue.');
            }
            return aryErrors;
        }
        return {
            bindMappingDropdown:function(objectType,options){
                if(!objectType || !options){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _bindMappingDropdown(objectType,options);
            },
            setDataInHtmlForMapping:function(control){
                if(!control){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _setDataInHtmlOfMappingCntrls(control);
            },
            saveMappedDataInControl:function(control,objectType){
                var objAllControls =_getAllControlsOfCheckboxDatabinding(objectType);
                var aryErrors = _validateMappingSavings(objAllControls);
                if(aryErrors.length>0){
                    throw new MfError(aryErrors);
                }
                control.fnSetCheckedVal(objAllControls.checkedValue.val());
            },
            setBindingDtlsInPropSheetHtml:function(control){
                _setBindingDtlsInPropSheetHtml(control);
            }
        }
    })();
    var _barLineDatabindingHelper=(function(){
        function _showHideDatabindCntrlsBySeriesType(objectType,noOfSeries){
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                    $('#Bar_Line_DataBindValue1').hide();
                    $('#Bar_Line_DataBindValue2').hide();
                    $('#Bar_Line_DataBindValue3').hide();
                    $('#Bar_Line_DataBindValue4').hide();
                    $('#Bar_Line_DataBindValue5').hide();
                    switch(noOfSeries){
                        case "1": 
                            $('#Bar_Line_DataBindValue1').show();
                            break;
                        case "2":
                            $('#Bar_Line_DataBindValue1').show();
                            $('#Bar_Line_DataBindValue2').show();
                            break; 
                        case "3":
                            $('#Bar_Line_DataBindValue1').show();
                            $('#Bar_Line_DataBindValue2').show();
                            $('#Bar_Line_DataBindValue3').show();
                            break;  
                        case "4":
                            $('#Bar_Line_DataBindValue1').show();
                            $('#Bar_Line_DataBindValue2').show();
                            $('#Bar_Line_DataBindValue3').show();
                            $('#Bar_Line_DataBindValue4').show();
                            break;  
                        case "5":
                            $('#Bar_Line_DataBindValue1').show();
                            $('#Bar_Line_DataBindValue2').show();
                            $('#Bar_Line_DataBindValue3').show();
                            $('#Bar_Line_DataBindValue4').show();
                            $('#Bar_Line_DataBindValue5').show();
                            break;  
                    }
                    break;
            } 
        }
        function _validateDatabindMappingSaving(
                objectType/*MF_IDE_CONSTANTS.ideCommandObjectTypes*/,
                noOfSeries/*int*/){
            
            if(!objectType)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var aryErrors=[];
            var $ddlBarLineXAxisData;
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                     break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                    break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                    $ddlBarLineXAxisData = _getHtmlControls.ddlBarLineXAxisData();
                    if($ddlBarLineXAxisData.val()==="-1"){
                        aryErrors.push("Please select a option for X Axis data.");
                    }
                    for (var n = 1; n <= noOfSeries; n++) {
                        
                        if($('#txtYDataBindLabel' + n).val().trim() === "" 
                            || $('#ddlChartDataBindCommand_Value' + n).val()=== "-1"){
                            aryErrors.push('Please add Y-axis data for the required series.');
                            break;
                        }
                    }
                    
                break;    
            }
            
            return aryErrors;
        }
        function _saveMappedDataInControl(control,objectType){
            
            var seriesObj = new SeriesObj(),
                strNoOfSeries = control.fnGetNoOfSeries(),
                iNoOfSeries = 1,
                aryErrors = [],
                arySeriesObjs = [],
                $ddlBarLineXAxisData = $([]);
            if(strNoOfSeries){
                iNoOfSeries = parseInt(strNoOfSeries);
            }    
            aryErrors = _validateDatabindMappingSaving(objectType,iNoOfSeries);
            if(aryErrors.length>0){
                throw new MfError(aryErrors);
            }
            control.fnClearAllSeriesObjs();
            $ddlBarLineXAxisData = _getHtmlControls.ddlBarLineXAxisData();
            control.fnSetXAxisData($ddlBarLineXAxisData.val());
            for (var n = 1; n <= iNoOfSeries; n++) {
                seriesObj = new SeriesObj();
                seriesObj.index = n;
                seriesObj.label = $('#txtYDataBindLabel' + n).val();
                seriesObj.value = $('#ddlChartDataBindCommand_Value' + n).val();
                seriesObj.scaleFactor = $('#txtYDataBindScaleFact' + n).val();
                control.fnAddSeriesObj(seriesObj);
            }
        }
        return {
            setInitialStateOfControl:function(){
                var i=0,
                strDefaultDdlOptions = '<option value="-1">Select</option>';
                $('#ddlDataBindBar_LineChart_Xaxis').html(strDefaultDdlOptions);
                for(i=1; i <= 5; i++){
                    $('#txtYDataBindLabel' + i).val('');
                    $('#ddlChartDataBindCommand_Value' + i).html(strDefaultDdlOptions);
                    $('#txtYDataBindScaleFact' + i).val('');
                }
            },
            showHideDatabindCntrlsBySeriesType:function(objectType,noOfSeries){
                _showHideDatabindCntrlsBySeriesType(objectType,noOfSeries);
            },
            bindMappingDropdown:function(objectType,options){
                var $contDiv = $([]);
                switch(objectType){
                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                            _getHtmlControls.divBarLineChartCont().find('select.BindValueDdl')
                            .each(function(index){
                                $(this).html('');
                                $(this).html(options);
                            });
                        break;
                }
            },
            saveMappedDataInControl:function(control,objectType){
                _saveMappedDataInControl(control,objectType);
            },
            setBindingDtlsInPropSheetHtml:function(control,cntrlType){
                var objPropCntrl = null;
                switch(cntrlType){
                    case MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
                        objPropCntrl = PROP_JSON_HTML_MAP.BarGraph.propSheetCntrl.Data.getDataObjectCntrl();
                        break;
                    case MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                        objPropCntrl = PROP_JSON_HTML_MAP.LineGraph.propSheetCntrl.Data.getDataObjectCntrl();
                        break;
                }
                var objDatabindObject = control.fnGetDatabindingObj();
                if (objDatabindObject) {
                    _utilities.setDatabindObjectNameInPropsheetHtml(
                        objPropCntrl.control,
                        objDatabindObject.name
                    );
                }
            },
            setDataInHtmlForMapping:function(control){
                var row = 0,
                    xAxisData = control.fnGetXAxisData(),
                    aryYAxisData =control.fnGetYAxisDataObjs(),
                    n=0,
                    objYAxisData = null;
                
                var index = 0;
                if( xAxisData) {
                    $('#ddlDataBindBar_LineChart_Xaxis').val(xAxisData);
                }
                if(aryYAxisData && $.isArray(aryYAxisData) && aryYAxisData.length>0){
                    for(n = 0; n < aryYAxisData.length; n++){
                        index = n + 1;
                        objYAxisData  = aryYAxisData[n];
                        $('#ddlChartDataBindCommand_Value'+ index).val(objYAxisData.value);
                        $('#txtYDataBindLabel' + index).val(objYAxisData.label); 
                        $('#txtYDataBindScaleFact' + index).val(objYAxisData.scaleFactor);  
                    }
                } 
            }
        }
    })();
    var _textboxToggleDatabindingHelper=(function(){
        function _getAllControlsOfDatabinding(
                objectType/*CONSTANTS.ideCommandObjectTypes*/){
            
            var objAllControls = null;
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                   objAllControls = _getHtmlControls.allTextboxToggleDatabindingCntrls();
                break;
            }
            return objAllControls;
        }
        function _bindMappingDropdown(objectType,options){
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            objAllControls.displayText.html('').html(options);
        }
        function _setDataInHtmlOfMappingCntrls(control){
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            var displayText = control.fnGetDisplayText();
            if(displayText){
                objAllControls.displayText.val(displayText);
            }
        }
        function _setBindingDtlsInPropSheetHtml(control,cntrlType){
            var objPropCntrl = null;
            switch(cntrlType){
                case MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
                    objPropCntrl = PROP_JSON_HTML_MAP.Textbox.propSheetCntrl.Data.getDataObjectCntrl();
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                    objPropCntrl = PROP_JSON_HTML_MAP.Toggle.propSheetCntrl.Data.getDataObjectCntrl();
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                    mFicientIde.HiddenFieldControlHelper.processDatabindingObjectSaved();
                    return ;
                break;    
            }
            var objDatabindObject = control.fnGetDatabindingObj();
            if (objDatabindObject) {
                _utilities.setDatabindObjectNameInPropsheetHtml(objPropCntrl.control,
                objDatabindObject.name);
            }
        }
        function _validateMappingSavings(allHtmlControls){
            var aryErrors =[];
            if(allHtmlControls.displayText.val() === "-1"){
                aryErrors.push('Please select a value for checkedvalue.');
            }
            return aryErrors;
        }
        return {
            bindMappingDropdown:function(objectType,options){
                if(!objectType || !options){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _bindMappingDropdown(objectType,options);
            },
            setDataInHtmlForMapping:function(control){
                if(!control){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _setDataInHtmlOfMappingCntrls(control);
            },
            saveMappedDataInControl:function(control,objectType){
                var objAllControls =_getAllControlsOfDatabinding(objectType);
                var aryErrors = _validateMappingSavings(objAllControls);
                if(aryErrors.length>0){
                    throw new MfError(aryErrors);
                }
                control.fnSetDisplayText(objAllControls.displayText.val());
            },
            setBindingDtlsInPropSheetHtml:function(control,cntrlType){
                _setBindingDtlsInPropSheetHtml(control,cntrlType);
            }
        }
    })();
    var _dropDownDatabindingHelper=(function(){
        function _getAllControlsOfDatabinding(
                objectType/*CONSTANTS.ideCommandObjectTypes*/){
            
            var objAllControls = null;
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                   objAllControls = _getHtmlControls.allDropDownDatabindingCntrls();
                break;
            }
            return objAllControls;
        }
        function _bindMappingDropdown(objectType,options){
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            objAllControls.optionText.html('').html(options);
            objAllControls.optionValue.html('').html(options);
        }
        function _setDataInHtmlOfMappingCntrls(control){
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            var optionText = control.fnGetOptionText();
            var optionValue = control.fnGetOptionVal();
            if(optionText){
                objAllControls.optionText.val(optionText);
            }
            if(optionValue){
                objAllControls.optionValue.val(optionValue);
            }
        }
        function _setBindingDtlsInPropSheetHtml(control,cntrlType){
            var objPropCntrl = null;
            switch(cntrlType){
                case MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
                    objPropCntrl = PROP_JSON_HTML_MAP.Dropdown.propSheetCntrl.Data.getDataObjectCntrl();
                    break;
            }
            var objDatabindObject = control.fnGetDatabindingObj();
            if (objDatabindObject) {
                _utilities.setDatabindObjectNameInPropsheetHtml(objPropCntrl.control,
                objDatabindObject.name);
            }
        }
        function _validateMappingSavings(allHtmlControls){
            var aryErrors =[];
            if(allHtmlControls.optionText.val() === "-1"){
                aryErrors.push('Please select a value for display text.');
            }
            if(allHtmlControls.optionValue.val() === "-1"){
                aryErrors.push('Please select a value for display val.');
            }
            return aryErrors;
        }
        return {
            bindMappingDropdown:function(objectType,options){
                if(!objectType || !options){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _bindMappingDropdown(objectType,options);
            },
            setDataInHtmlForMapping:function(control){
                if(!control){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _setDataInHtmlOfMappingCntrls(control);
            },
            saveMappedDataInControl:function(control,objectType){
                var objAllControls =_getAllControlsOfDatabinding(objectType);
                var aryErrors = _validateMappingSavings(objAllControls);
                if(aryErrors.length>0){
                    throw new MfError(aryErrors);
                }
                control.fnSetOptionText(objAllControls.optionText.val());
                control.fnSetOptionVal(objAllControls.optionValue.val());
            },
            setBindingDtlsInPropSheetHtml:function(control,cntrlType){
                _setBindingDtlsInPropSheetHtml(control,cntrlType);
            }
        }
    })();
    var _sliderDatabindingHelper=(function(){
        function _getAllControlsOfDatabinding(
                objectType/*CONSTANTS.ideCommandObjectTypes*/){
            
            var objAllControls = null;
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                   objAllControls = _getHtmlControls.allSliderDatabindingCntrls();
                break;
            }
            return objAllControls;
        }
        function _bindMappingDropdown(objectType,options){
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            objAllControls.ddlValue.html('').html(options);
        }
        function _setDataInHtmlOfMappingCntrls(control){
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            var bindValue = control.fnGetValue();
            
            if(bindValue){
                objAllControls.ddlValue.val(bindValue);
            }
            
        }
        function _setBindingDtlsInPropSheetHtml(control,cntrlType){
            var objPropCntrl = null;
            switch(cntrlType){
                case MF_IDE_CONSTANTS.CONTROLS.SLIDER:
                case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                    objPropCntrl = PROP_JSON_HTML_MAP.Slider.propSheetCntrl.Data.getDataObjectCntrl();
                    break;
                case MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                    objPropCntrl = PROP_JSON_HTML_MAP.AngularGuage.propSheetCntrl.Data.getDataObjectCntrl();
                break;
                case MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
                    objPropCntrl = PROP_JSON_HTML_MAP.CylinderGuage.propSheetCntrl.Data.getDataObjectCntrl();
                break;
            }
            var objDatabindObject = control.fnGetDatabindingObj();
            if (objDatabindObject) {
                _utilities.setDatabindObjectNameInPropsheetHtml(objPropCntrl.control,
                objDatabindObject.name);
            }
        }
        function _validateMappingSavings(allHtmlControls){
            var aryErrors =[];
            if(allHtmlControls.ddlValue.val() === "-1"){
                aryErrors.push('Please select a value for slider value.');
            }
            return aryErrors;
        }
        return {
            bindMappingDropdown:function(objectType,options){
                if(!objectType || !options){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _bindMappingDropdown(objectType,options);
            },
            setDataInHtmlForMapping:function(control){
                if(!control){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _setDataInHtmlOfMappingCntrls(control);
            },
            saveMappedDataInControl:function(control,objectType){
                var objAllControls =_getAllControlsOfDatabinding(objectType);
                var aryErrors = _validateMappingSavings(objAllControls);
                if(aryErrors.length>0){
                    throw new MfError(aryErrors);
                }
                control.fnSetValue(objAllControls.ddlValue.val());
               
            },
            setBindingDtlsInPropSheetHtml:function(control,cntrlType){
                _setBindingDtlsInPropSheetHtml(control,cntrlType);
            }
        }
    
    })();
    var _pieDatabindingHelper=(function(){
        function _getAllControlsOfDatabinding(objectType/*CONSTANTS.ideCommandObjectTypes*/){
            var objAllControls = null;
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                   objAllControls = _getHtmlControls.allPieDatabindingCntrls();
                break;
            }
            return objAllControls;
        }
        function _bindMappingDropdown(objectType,options){
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            objAllControls.title.html('').html(options);
            objAllControls.value.html('').html(options);
        }
        function _setDataInHtmlOfMappingCntrls(control){
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            objAllControls.title.val(control.fnGetTitle());
            objAllControls.value.val(control.fnGetValue());
        }
        function _setBindingDtlsInPropSheetHtml(control){
            var objPropCntrl = PROP_JSON_HTML_MAP.PieChart.propSheetCntrl.Data.getDataObjectCntrl();
            var objDatabindObject = control.fnGetDatabindingObj();
            if (objDatabindObject) {
                _utilities.setDatabindObjectNameInPropsheetHtml(objPropCntrl.control,
                objDatabindObject.name);
            }
        }
        function _validateMappingSavings(allHtmlControls){
            var aryErrors =[];
            if(allHtmlControls.title.val() === "-1"){
                aryErrors.push('Please select a value for title.');
            }
            if(allHtmlControls.value.val() === "-1"){
                aryErrors.push('Please select a value for pie chart value.');
            }
            return aryErrors;
        }
        return {
            bindMappingDropdown:function(objectType,options){
                if(!objectType || !options){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _bindMappingDropdown(objectType,options);
            },
            setDataInHtmlForMapping:function(control){
                if(!control){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _setDataInHtmlOfMappingCntrls(control);
            },
            saveMappedDataInControl:function(control,objectType){
                var objAllControls =_getAllControlsOfDatabinding(objectType);
                var aryErrors = _validateMappingSavings(objAllControls);
                if(aryErrors.length>0){
                    throw new MfError(aryErrors);
                }
                control.fnSetTitle(objAllControls.title.val());
                control.fnSetValue(objAllControls.value.val());
            },
            setBindingDtlsInPropSheetHtml:function(control){
                _setBindingDtlsInPropSheetHtml(control);
            }
        }
    })();
    var _imageDatabindingHelper=(function(){
        function _getAllControlsOfDatabinding(objectType/*CONSTANTS.ideCommandObjectTypes*/){
            var objAllControls = null;
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                   objAllControls = _getHtmlControls.allImageDatabindingCntrls();
                break;
            }
            return objAllControls;
        }
        function _bindMappingDropdown(objectType,options){
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            objAllControls.source.html('').html(options);
            objAllControls.title.html('').html(options);
        }
        function _setDataInHtmlOfMappingCntrls(control){
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            objAllControls.source.val(control.fnGetSource());
            objAllControls.title.val(control.fnGetTitle());
        }
        function _setBindingDtlsInPropSheetHtml(control){
            var objPropCntrl = PROP_JSON_HTML_MAP.Image.propSheetCntrl.Data.getDataObjectCntrl();
            var objDatabindObject = control.fnGetDatabindingObj();
            if (objDatabindObject) {
                _utilities.setDatabindObjectNameInPropsheetHtml(objPropCntrl.control,
                objDatabindObject.name);
            }
        }
        function _validateMappingSavings(allHtmlControls){
            var aryErrors =[];
            if(allHtmlControls.source.val() === "-1"){
                aryErrors.push('Please select a value for source of image.');
            }
            if(allHtmlControls.title.val() === "-1"){
                aryErrors.push('Please select a value for title of image.');
            }
            return aryErrors;
        }
        return {
            bindMappingDropdown:function(objectType,options){
                if(!objectType || !options){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _bindMappingDropdown(objectType,options);
            },
            setDataInHtmlForMapping:function(control){
                if(!control){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _setDataInHtmlOfMappingCntrls(control);
            },
            saveMappedDataInControl:function(control,objectType){
                var objAllControls =_getAllControlsOfDatabinding(objectType);
                var aryErrors = _validateMappingSavings(objAllControls);
                if(aryErrors.length>0){
                    throw new MfError(aryErrors);
                }
                control.fnSetSource(objAllControls.source.val());
                control.fnSetTitle(objAllControls.title.val());
            },
            setBindingDtlsInPropSheetHtml:function(control){
                _setBindingDtlsInPropSheetHtml(control);
            }
        }
    })();
    var _locationDatabindingHelper=(function(){
        function _getAllControlsOfDatabinding(objectType/*CONSTANTS.ideCommandObjectTypes*/){
            var objAllControls = null;
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.db:
                   objAllControls = _getHtmlControls.allDbLocationDatabindingCntrls();
                break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.http:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc:
                   objAllControls = _getHtmlControls.allWsLocationDatabindingCntrls();
                break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.odata:
                   objAllControls = _getHtmlControls.allOdataLocationDatabindingCntrls();
                break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                   objAllControls = _getHtmlControls.allLocationDatabindingCntrls();
                break;
            }
            return objAllControls;
        }
        function _bindMappingDropdown(objectType,options){
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            objAllControls.latitude.html('').html(options);
            objAllControls.longitude.html('').html(options);
        }
        function _setDataInHtmlOfMappingCntrls(control,objectType){
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            objAllControls.latitude.val(control.fnGetLatitude());
            objAllControls.longitude.val(control.fnGetLongitude());
        }
        function _setDataInPropSheetHtml(control){
            var objPropCntrl = PROP_JSON_HTML_MAP.Location.propSheetCntrl.Data.getDataObject();
            var objDatabindObject = control.fnGetDatabindingObj();
            if (objDatabindObject) {
                _utilities.setDatabindObjectNameInPropsheetHtml(objPropCntrl.control,
                objDatabindObject.name);
            }
        }
        function _validateMappingSavings(allHtmlControls){
            var aryErrors =[];
            if(allHtmlControls.latitude.val() === "-1"){
                aryErrors.push('Please select a value for latitude.');
            }
            if(allHtmlControls.longitude.val() === "-1"){
                aryErrors.push('Please select a value for longitude.');
            }
            return aryErrors;
        }
        return {
            bindMappingDropdown:function(objectType,options){
                if(!objectType || !options){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _bindMappingDropdown(objectType,options);
            },
            setDataInHtmlForMapping:function(control,objectType){
                if(!control || !objectType){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _setDataInHtmlOfMappingCntrls(control,objectType);
            },
            saveMappedDataInControl:function(control,objectType){
                var objAllControls =_getAllControlsOfDatabinding(objectType);
                var aryErrors = _validateMappingSavings(objAllControls);
                if(aryErrors.length>0){
                    throw new MfError(aryErrors);
                }
                control.fnSetLatitude(objAllControls.latitude.val());
                control.fnSetLongitude(objAllControls.longitude.val());
            },
            setDataInPropSheetHtml:function(control){
                _setDataInPropSheetHtml(control);
            }
        }
    })();
    var _angularAndCylinderDatabindingHelper=(function(){
        function _getAllControlsOfDatabinding(
                objectType/*CONSTANTS.ideCommandObjectTypes*/){
            
            var objAllControls = null;
            switch(objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                   objAllControls = _getHtmlControls.allAngularAndCylinderDatabindingCntrls();
                break;
            }
            return objAllControls;
        }
        function _bindMappingDropdown(objectType,options){
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            objAllControls.value.html('').html(options);
            objAllControls.maxValue.html('').html(options);
        }
        function _setDataInHtmlOfMappingCntrls(control){
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            var bindValue = control.fnGetValue();
            var bindMaxValue = control.fnGetMaxValue();
            
            if(bindValue){
                objAllControls.value.val(bindValue);
            }
            
            if(bindMaxValue){
                objAllControls.maxValue.val(bindMaxValue);
            }
        }
        function _setBindingDtlsInPropSheetHtml(control,cntrlType){
            var objPropCntrl = null;
            switch(cntrlType){
                case MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                    objPropCntrl = PROP_JSON_HTML_MAP.AngularGuage.propSheetCntrl.Data.getDataObjectCntrl();
                break;
                case MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
                    objPropCntrl = PROP_JSON_HTML_MAP.CylinderGuage.propSheetCntrl.Data.getDataObjectCntrl();
                break;
            }
            var objDatabindObject = control.fnGetDatabindingObj();
            if (objDatabindObject) {
                _utilities.setDatabindObjectNameInPropsheetHtml(objPropCntrl.control,
                objDatabindObject.name);
            }
        }
        function _validateMappingSavings(allHtmlControls){
            var aryErrors =[];
            if(allHtmlControls.value.val() === "-1"){
                aryErrors.push('Please select value.');
            }
            if(allHtmlControls.maxValue.val() === "-1"){
                aryErrors.push('Please select maximum value.');
            }
            if(allHtmlControls.value.val() === allHtmlControls.maxValue.val()){
                aryErrors.push('Value and maximum Value of Cylinder Guage cannot be same');
            }
            return aryErrors;
        }
        return {
            bindMappingDropdown:function(objectType,options){
                if(!objectType || !options){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _bindMappingDropdown(objectType,options);
            },
            setDataInHtmlForMapping:function(control){
                if(!control){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _setDataInHtmlOfMappingCntrls(control);
            },
            saveMappedDataInControl:function(control,objectType){
                var objAllControls =_getAllControlsOfDatabinding(objectType);
                var aryErrors = _validateMappingSavings(objAllControls);
                if(aryErrors.length>0){
                    throw new MfError(aryErrors);
                }
                control.fnSetValue(objAllControls.value.val());
                control.fnSetMaxValue(objAllControls.maxValue.val());
               
            },
            setBindingDtlsInPropSheetHtml:function(control,cntrlType){
                _setBindingDtlsInPropSheetHtml(control,cntrlType);
            }
        }
    
    })();
    var _drilldownPieDatabindingHelper=(function(){
        function _getAllControlsOfDatabinding(objectType/*CONSTANTS.ideCommandObjectTypes*/){
            if(!objectType)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var objAllControls = null;
            objAllControls = _getHtmlControls.allDrilldownPieChartDatabindCntrls(objectType);
            return objAllControls;
        }
        function _bindMappingDropdown(objectType,options){
            if(!objectType)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var objAllControls =_getAllControlsOfDatabinding(objectType);
            objAllControls.sliceTitle.html('').html(options);
            objAllControls.subSliceTitle.html('').html(options);
            objAllControls.value.html('').html(options);
        }
        function _setDataInHtmlOfMappingCntrls(control,objectType){
            if(!objectType)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            var objAllControls =_getAllControlsOfDatabinding(objectType),
                strSliceTitle = control.fnGetTitle(),
                strSubSliceTitle = control.fnGetSubSliceTitle(),
                strValue = control.fnGetValue();
            
            if(!mfUtil.isNullOrUndefined(strSliceTitle) 
            && !mfUtil.isEmptyString(strSliceTitle)){
                objAllControls.sliceTitle.val(strSliceTitle);
            }
            if(!mfUtil.isNullOrUndefined(strSubSliceTitle) 
            && !mfUtil.isEmptyString(strSubSliceTitle)){
                objAllControls.subSliceTitle.val(strSubSliceTitle);
            }
            if(!mfUtil.isNullOrUndefined(strValue) 
            && !mfUtil.isEmptyString(strValue)){
                objAllControls.value.val(strValue);
            }
        }
        function _setBindingDtlsInPropSheetHtml(control){
            var objPropCntrl = PROP_JSON_HTML_MAP.DrilldownPieChart.propSheetCntrl.Data.getDataObjectCntrl();
            var objDatabindObject = control.fnGetDatabindingObj();
            if (objDatabindObject) {
                _utilities.setDatabindObjectNameInPropsheetHtml(
                    objPropCntrl.control,
                    objDatabindObject.name);
            }
        }
        function _validateMappingSavings(allHtmlControls){
            var aryErrors =[];
            if(allHtmlControls.sliceTitle.val() === "-1"){
                aryErrors.push('Please select a value for slice title.');
            }
            if(allHtmlControls.subSliceTitle.val() === "-1"){
                aryErrors.push('Please select a value for sub slice title.');
            }
            if(allHtmlControls.value.val() === "-1"){
                aryErrors.push('Please select a value for pie chart value.');
            }
            return aryErrors;
        }
        return {
            bindMappingDropdown:function(objectType,options){
                if(!objectType || !options){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _bindMappingDropdown(objectType,options);
            },
            setDataInHtmlForMapping:function(control,objectType){
                if(!control){
                    throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
                }
                _setDataInHtmlOfMappingCntrls(control,objectType);
            },
            saveMappedDataInControl:function(control,objectType){
                var objAllControls =_getAllControlsOfDatabinding(objectType);
                var aryErrors = _validateMappingSavings(objAllControls);
                if(aryErrors.length>0){
                    throw new MfError(aryErrors);
                }
                control.fnSetTitle(objAllControls.sliceTitle.val());
                control.fnSetValue(objAllControls.value.val());
                control.fnSetSubSliceTitle(objAllControls.subSliceTitle.val());
            },
            setBindingDtlsInPropSheetHtml:function(control){
                _setBindingDtlsInPropSheetHtml(control);
            }
        }
    })();
    var _customControlDatabindingHelper=(function(){
        function _setBindingDtlsInPropSheetHtml(control){
            var objPropCntrl = null;
            objPropCntrl = PROP_JSON_HTML_MAP.customControl.propSheetCntrl.Data.getDataObject();
            var objDatabindObject = control.fnGetDatabindingObj();
            if (objDatabindObject) {
                _utilities.setDatabindObjectNameInPropsheetHtml(objPropCntrl.control,
                objDatabindObject.name);
            }
        }
        return {
            setBindingDtlsInPropSheetHtml:function(control){
                _setBindingDtlsInPropSheetHtml(control);
            }
        }
    })();
    //var _hidFieldDataBindingHelper=(function(){
        
        
        
    //     return{
    //         processSaveDatabindingSettings:function(){
                
    //         }
    //     }
    // })();
    //MOHAN
    

    function _isValidConditionalControl(controlType) {
        var isValid = false;
        switch (controlType) {
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                isValid = false;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
                isValid = true;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
                isValid = true;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                isValid = true;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.RADIOBUTTON:
                isValid = true;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                isValid = true;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                isValid = false;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                isValid = false;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                isValid = false;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                isValid = true;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DATE_TIME_PICKER:
                isValid = true;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                isValid = true;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
                isValid = true;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SEPARATOR:
                isValid = false;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARCODE:
                isValid = true;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
                isValid = true;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                isValid = false;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
                isValid = false;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                isValid = false;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
                isValid = false;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                isValid = false;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
                isValid = false;
                break;
            case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                isValid = false;
                break;
        }
        return isValid;
    }

    function _getDBCommandType(cmdType) {
        var commandType = "";
        switch (cmdType) {
            case mFicientIde.MF_IDE_CONSTANTS.DBCommandType.SELECT:
                commandType = "SELECT";
                break;
            case mFicientIde.MF_IDE_CONSTANTS.DBCommandType.INSERT:
                commandType = "INSERT";
                break;
            case mFicientIde.MF_IDE_CONSTANTS.DBCommandType.UPDATE:
                ommandType = "UPDATE";
                break;
            case mFicientIde.MF_IDE_CONSTANTS.DBCommandType.DELETE:
                commandType = "DELETE";
                break;
        }
        return commandType;
    }
    function _getWSCmdObjectType(webserviceType) {
        var wsType = MF_IDE_CONSTANTS.ideCommandObjectTypes.db;
        switch (webserviceType.trim()) {
            case "wsdl":
                wstype = MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                break;
            case "http":
                wstype = MF_IDE_CONSTANTS.ideCommandObjectTypes.http;
                break;
            case "rpc":
                wstype = MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc;
                break;
        }
        return wstype;
    }
    function _getWSType(webserviceType) {
        var wsType = MF_IDE_CONSTANTS.webServiceType.http;
        switch (webserviceType.trim()) {
            case "wsdl":
                wstype = MF_IDE_CONSTANTS.webServiceType.wsdlSoap;
                break;
            case "http":
                wstype = MF_IDE_CONSTANTS.webServiceType.http;
                break;
            case "rpc":
                wstype = MF_IDE_CONSTANTS.webServiceType.xmlRpc;
                break;
        }
        return wstype;
    }
    
    //show hide container divs
    function _setDbBindingsOptionsDisplay(cntrl) {
        $('#Db_Location_Options_Div').hide();
        $('#Db_DPieChart_Options_Div').hide();
        switch (cntrl.type) {
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                $('#Db_Label_Command').show();
                $('#Db_Slider_DisplayText_Div').hide();
                $('#Db_Image_Div').hide();
                $('#Db_Ctrl_DisplayText_Div').hide();
                $('#Db_Dropdown_Options_Div').hide();
                $('#Db_List_Options_Div').hide();
                $('#Db_EditableList_Options_Div').hide();
                $('#Db_PieChart_Options_Div').hide();
                $('#Db_Bar_LineChart_Options_Div').hide();
                $('#Db_Table_Options_Div').hide();
                $('#Db_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                $('#Db_Checkbox_DisplayText_Div').show();
                $('#Db_Slider_DisplayText_Div').hide();
                $('#Db_Ctrl_DisplayText_Div').hide();
                $('#Db_Image_Div').hide();
                $('#Db_Label_Command').hide();
                $('#Db_Dropdown_Options_Div').hide();
                $('#Db_List_Options_Div').hide();
                $('#Db_EditableList_Options_Div').hide();
                $('#Db_PieChart_Options_Div').hide();
                $('#Db_Bar_LineChart_Options_Div').hide();
                $('#Db_Table_Options_Div').hide();
                $('#Db_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
            case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                $('#Db_Slider_DisplayText_Div').show();
                $('#Db_Ctrl_DisplayText_Div').hide();
                $('#Db_Checkbox_DisplayText_Div').hide();
                $('#Db_Image_Div').hide();
                $('#Db_Label_Command').hide();
                $('#Db_Dropdown_Options_Div').hide();
                $('#Db_List_Options_Div').hide();
                $('#Db_EditableList_Options_Div').hide();
                $('#Db_PieChart_Options_Div').hide();
                $('#Db_Bar_LineChart_Options_Div').hide();
                $('#Db_Table_Options_Div').hide();
                $('#Db_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                $('#Db_Cylinder_Guage_Div').show();
                $('#Db_Slider_DisplayText_Div').hide();
                $('#Db_Ctrl_DisplayText_Div').hide();
                $('#Db_Checkbox_DisplayText_Div').hide();
                $('#Db_Image_Div').hide();
                $('#Db_Label_Command').hide();
                $('#Db_Dropdown_Options_Div').hide();
                $('#Db_List_Options_Div').hide();
                $('#Db_EditableList_Options_Div').hide();
                $('#Db_PieChart_Options_Div').hide();
                $('#Db_Bar_LineChart_Options_Div').hide();
                $('#Db_Table_Options_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                $('#Db_Ctrl_DisplayText_Div').show();
                $('#Db_Slider_DisplayText_Div').hide();
                $('#Db_Checkbox_DisplayText_Div').hide();
                $('#Db_Image_Div').hide();
                $('#Db_Label_Command').hide();
                $('#Db_Dropdown_Options_Div').hide();
                $('#Db_List_Options_Div').hide();
                $('#Db_EditableList_Options_Div').hide();
                $('#Db_PieChart_Options_Div').hide();
                $('#Db_Bar_LineChart_Options_Div').hide();
                $('#Db_Table_Options_Div').hide();
                $('#Db_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                $('#Db_Dropdown_Options_Div').show();
                $('#Db_Checkbox_DisplayText_Div').hide();
                $('#Db_Slider_DisplayText_Div').hide();
                $('#Db_Image_Div').hide();
                $('#Db_Label_Command').hide();
                $('#Db_Ctrl_DisplayText_Div').hide();
                $('#Db_List_Options_Div').hide();
                $('#Db_EditableList_Options_Div').hide();
                $('#Db_PieChart_Options_Div').hide();
                $('#Db_Bar_LineChart_Options_Div').hide();
                $('#Db_Table_Options_Div').hide();
                $('#Db_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                $('#Db_List_Options_Div').show();
                $('#Db_Checkbox_DisplayText_Div').hide();
                $('#Db_Slider_DisplayText_Div').hide();
                $('#Db_Image_Div').hide();
                $('#Db_Label_Command').hide();
                $('#Db_Ctrl_DisplayText_Div').hide();
                $('#Db_Dropdown_Options_Div').hide();
                $('#Db_EditableList_Options_Div').hide();
                $('#Db_PieChart_Options_Div').hide();
                $('#Db_Bar_LineChart_Options_Div').hide();
                $('#Db_Table_Options_Div').hide();
                $('#Db_Cylinder_Guage_Div').hide();
                //TOCHANGE MOHAN
                    _listDataBindingHelper.showHideDatabindCntrlsByListType(
                        cntrl,
                        MF_IDE_CONSTANTS.ideCommandObjectTypes.db
                    );//TOCHANGE MOHAN
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                $('#Db_EditableList_Options_Div').show();
                $('#Db_Checkbox_DisplayText_Div').hide();
                $('#Db_Slider_DisplayText_Div').hide();
                $('#Db_Image_Div').hide();
                $('#Db_Label_Command').hide();
                $('#Db_Ctrl_DisplayText_Div').hide();
                $('#Db_Dropdown_Options_Div').hide();
                $('#Db_List_Options_Div').hide();
                $('#Db_PieChart_Options_Div').hide();
                $('#Db_Bar_LineChart_Options_Div').hide();
                $('#Db_Table_Options_Div').hide();
                $('#Db_Cylinder_Guage_Div').hide();
                _editableListHelper.formMappingHtmlForDb(cntrl);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                $('#Db_PieChart_Options_Div').show();
                $('#Db_Checkbox_DisplayText_Div').hide();
                $('#Db_Slider_DisplayText_Div').hide();
                $('#Db_Image_Div').hide();
                $('#Db_Label_Command').hide();
                $('#Db_Ctrl_DisplayText_Div').hide();
                $('#Db_Dropdown_Options_Div').hide();
                $('#Db_List_Options_Div').hide();
                $('#Db_EditableList_Options_Div').hide();
                $('#Db_Bar_LineChart_Options_Div').hide();
                $('#Db_Table_Options_Div').hide();
                $('#Db_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
                $('#Db_PieChart_Options_Div').hide();
                $('#Db_Checkbox_DisplayText_Div').hide();
                $('#Db_Slider_DisplayText_Div').hide();
                $('#Db_Image_Div').hide();
                $('#Db_Label_Command').hide();
                $('#Db_Ctrl_DisplayText_Div').hide();
                $('#Db_Dropdown_Options_Div').hide();
                $('#Db_List_Options_Div').hide();
                $('#Db_EditableList_Options_Div').hide();
                $('#Db_Bar_LineChart_Options_Div').hide();
                $('#Db_Table_Options_Div').hide();
                $('#Db_Cylinder_Guage_Div').hide();
                $('#Db_DPieChart_Options_Div').show();
            break;    
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                $('#Db_Bar_LineChart_Options_Div').show();
                $('#Db_Slider_DisplayText_Div').hide();
                $('#Db_Checkbox_DisplayText_Div').hide();
                $('#Db_Image_Div').hide();
                $('#Db_Label_Command').hide();
                $('#Db_Ctrl_DisplayText_Div').hide();
                $('#Db_Dropdown_Options_Div').hide();
                $('#Db_List_Options_Div').hide();
                $('#Db_EditableList_Options_Div').hide();
                $('#Db_PieChart_Options_Div').hide();
                $('#Db_Table_Options_Div').hide();
                $('#Db_Cylinder_Guage_Div').hide();
                if (cntrl.noOfSeries == "1") {
                    $('#Bar_Line_DbValue2').hide();
                    $('#Bar_Line_DbValue3').hide();
                    $('#Bar_Line_DbValue4').hide();
                    $('#Bar_Line_DbValue5').hide();
                }
                if (cntrl.noOfSeries == "2") {
                    $('#Bar_Line_DbValue2').show();
                    $('#Bar_Line_DbValue3').hide();
                    $('#Bar_Line_DbValue4').hide();
                    $('#Bar_Line_DbValue5').hide();
                }
                if (cntrl.noOfSeries == "3") {
                    $('#Bar_Line_DbValue2').show();
                    $('#Bar_Line_DbValue3').show();
                    $('#Bar_Line_DbValue4').hide();
                    $('#Bar_Line_DbValue5').hide();
                }
                if (cntrl.noOfSeries == "4") {
                    $('#Bar_Line_DbValue2').show();
                    $('#Bar_Line_DbValue3').show();
                    $('#Bar_Line_DbValue4').show();
                    $('#Bar_Line_DbValue5').hide();
                }
                if (cntrl.noOfSeries == "5") {
                    $('#Bar_Line_DbValue2').show();
                    $('#Bar_Line_DbValue3').show();
                    $('#Bar_Line_DbValue4').show();
                    $('#Bar_Line_DbValue5').show();
                }

                for (var i = 1; i <= cntrl.noOfSeries; i++) {
                    $('#txtYDbScaleFact' + i).keypress(function (event) {
                        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                    });
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                $('#Db_Image_Div').show();
                $('#Db_Checkbox_DisplayText_Div').hide();
                $('#Db_Slider_DisplayText_Div').hide();
                $('#Db_Bar_LineChart_Options_Div').hide();
                $('#Db_Label_Command').hide();
                $('#Db_Ctrl_DisplayText_Div').hide();
                $('#Db_Dropdown_Options_Div').hide();
                $('#Db_List_Options_Div').hide();
                $('#Db_EditableList_Options_Div').hide();
                $('#Db_PieChart_Options_Div').hide();
                $('#Db_Table_Options_Div').hide();
                $('#Db_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
                $('#Db_Table_Options_Div').show();
                $('#Db_Checkbox_DisplayText_Div').hide();
                $('#Db_Slider_DisplayText_Div').hide();
                $('#Db_Image_Div').hide();
                $('#Db_Label_Command').hide();
                $('#Db_Ctrl_DisplayText_Div').hide();
                $('#Db_Dropdown_Options_Div').hide();
                $('#Db_List_Options_Div').hide();
                $('#Db_EditableList_Options_Div').hide();
                $('#Db_PieChart_Options_Div').hide();
                $('#Db_Bar_LineChart_Options_Div').hide();
                $('#Db_Cylinder_Guage_Div').hide();
                _biTableDataBindingHelper.formMappingTableHtml(cntrl,
                    MF_IDE_CONSTANTS.ideCommandObjectTypes.db);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
                $('#Db_Location_Options_Div').show();
                $('#Db_Table_Options_Div').hide();
                $('#Db_Checkbox_DisplayText_Div').hide();
                $('#Db_Slider_DisplayText_Div').hide();
                $('#Db_Image_Div').hide();
                $('#Db_Label_Command').hide();
                $('#Db_Ctrl_DisplayText_Div').hide();
                $('#Db_Dropdown_Options_Div').hide();
                $('#Db_List_Options_Div').hide();
                $('#Db_EditableList_Options_Div').hide();
                $('#Db_PieChart_Options_Div').hide();
                $('#Db_Bar_LineChart_Options_Div').hide();
                $('#Db_Cylinder_Guage_Div').hide();
             break;    
        }
    }
    function _setWSBindingsOptionsDisplay(cntrl, wsType) {
        $('#Ws_Location_Options_Div').hide();
        $('#WS_DPieChart_Options_Div').hide();
        switch (cntrl.type) {
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                $('#WS_Label_Command').show();
                $('#WS_Checkbox_Options').hide();
                $('#WS_Slider_Options').hide();
                $('#WS_Cntrl_Options').hide();
                $('#WS_Image_Options').hide();
                $('#WS_Dropdown_Options').hide();
                $('#WS_List_Options').hide();
                $('#WS_EditableList_Cmd_Items').hide();
                $('#WS_PieChart_Options_Div').hide();
                $('#WS_Bar_LineChart_Options_Div').hide();
                $('#WS_Table_Options_Div').hide();
                $('#Checkbox_WsText').show();
                $('#WS_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                $('#WS_Checkbox_Options').show();
                $('#WS_Slider_Options').hide();
                $('#WS_Cntrl_Options').hide();
                $('#WS_Image_Options').hide();
                $('#WS_Dropdown_Options').hide();
                $('#WS_List_Options').hide();
                $('#WS_EditableList_Cmd_Items').hide();
                $('#WS_PieChart_Options_Div').hide();
                $('#WS_Bar_LineChart_Options_Div').hide();
                $('#WS_Table_Options_Div').hide();
                $('#Checkbox_WsText').show();
                $('#WS_Label_Command').hide();
                $('#WS_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
            case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                $('#WS_Slider_Options').show();
                $('#WS_Cntrl_Options').hide();
                $('#WS_Checkbox_Options').hide();
                $('#WS_Image_Options').hide();
                $('#WS_Dropdown_Options').hide();
                $('#WS_List_Options').hide();
                $('#WS_EditableList_Cmd_Items').hide();
                $('#WS_PieChart_Options_Div').hide();
                $('#WS_Bar_LineChart_Options_Div').hide();
                $('#WS_Table_Options_Div').hide();
                $('#Slider_WsText').show();
                $('#WS_Label_Command').hide();
                $('#WS_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                $('#WS_Cylinder_Guage_Div').show();
                $('#WS_Slider_Options').hide();
                $('#WS_Cntrl_Options').hide();
                $('#WS_Checkbox_Options').hide();
                $('#WS_Image_Options').hide();
                $('#WS_Dropdown_Options').hide();
                $('#WS_List_Options').hide();
                $('#WS_EditableList_Cmd_Items').hide();
                $('#WS_PieChart_Options_Div').hide();
                $('#WS_Bar_LineChart_Options_Div').hide();
                $('#WS_Table_Options_Div').hide();
                $('#Slider_WsText').show();
                $('#WS_Label_Command').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                $('#WS_Cntrl_Options').show();
                $('#WS_Slider_Options').hide();
                $('#WS_Checkbox_Options').hide();
                $('#WS_Image_Options').hide();
                $('#WS_Dropdown_Options').hide();
                $('#WS_List_Options').hide();
                $('#WS_EditableList_Cmd_Items').hide();
                $('#WS_PieChart_Options_Div').hide();
                $('#WS_Bar_LineChart_Options_Div').hide();
                $('#WS_Table_Options_Div').hide();
                $('#Ctrl_WsText').show();
                $('#WS_Label_Command').hide();
                $('#WS_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                $('#WS_Dropdown_Options').show();
                $('#WS_Slider_Options').hide();
                $('#WS_Checkbox_Options').hide();
                $('#WS_Image_Options').hide();
                $('#WS_Cntrl_Options').hide();
                $('#WS_List_Options').hide();
                $('#WS_EditableList_Cmd_Items').hide();
                $('#WS_PieChart_Options_Div').hide();
                $('#WS_Bar_LineChart_Options_Div').hide();
                $('#WS_Table_Options_Div').hide();
                $('#Dropdown_WSDL_HTTP_Div').show();
                $('#WS_Label_Command').hide();
                $('#WS_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                $('#WS_List_Options').show();
                $('#WS_Slider_Options').hide();
                $('#WS_Checkbox_Options').hide();
                $('#WS_Image_Options').hide();
                $('#WS_Cntrl_Options').hide();
                $('#WS_Dropdown_Options').hide();
                $('#WS_EditableList_Cmd_Items').hide();
                $('#WS_PieChart_Options_Div').hide();
                $('#WS_Bar_LineChart_Options_Div').hide();
                $('#WS_Table_Options_Div').hide();
                $('#WS_Label_Command').hide();
                $('#WS_Cylinder_Guage_Div').hide();
                //MOHAN
                var cmdObjectType=MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                switch(wsType){
                    case MF_IDE_CONSTANTS.webServiceType.http:
                        cmdObjectType = MF_IDE_CONSTANTS.ideCommandObjectTypes.http;
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.wsdl:
                        cmdObjectType = MF_IDE_CONSTANTS.ideCommandObjectTypes.wsdl;
                        break;
                    case MF_IDE_CONSTANTS.webServiceType.xmlRpc:
                        cmdObjectType = MF_IDE_CONSTANTS.ideCommandObjectTypes.xmlRpc;
                        break;
                }
                _listDataBindingHelper.showHideDatabindCntrlsByListType(
                    cntrl,
                    cmdObjectType
                );
                //MOHAN
                    $('#List_WSDL_HTTP_Div').show();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                $('#WS_EditableList_Cmd_Items').show();
                $('#WS_Slider_Options').hide();
                $('#WS_Checkbox_Options').hide();
                $('#WS_Image_Options').hide();
                $('#WS_Cntrl_Options').hide();
                $('#WS_Dropdown_Options').hide();
                $('#WS_List_Options').hide();
                $('#WS_PieChart_Options_Div').hide();
                $('#WS_Bar_LineChart_Options_Div').hide();
                $('#WS_Table_Options_Div').hide();
                $('#WS_Label_Command').hide();
                $('#WS_Cylinder_Guage_Div').hide();
                _editableListHelper.formMappingHtmlForWebSrvc(cntrl, wsType);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                $('#WS_PieChart_Options_Div').show();
                $('#WS_Slider_Options').hide();
                $('#WS_Checkbox_Options').hide();
                $('#WS_Image_Options').hide();
                $('#WS_Cntrl_Options').hide();
                $('#WS_Dropdown_Options').hide();
                $('#WS_List_Options').hide();
                $('#WS_EditableList_Cmd_Items').hide();
                $('#WS_Bar_LineChart_Options_Div').hide();
                $('#WS_Table_Options_Div').hide();
                $('#WS_Label_Command').hide();
                $('#WS_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                $('#WS_Bar_LineChart_Options_Div').show();
                $('#WS_Slider_Options').hide();
                $('#WS_Checkbox_Options').hide();
                $('#WS_Image_Options').hide();
                $('#WS_Cntrl_Options').hide();
                $('#WS_Dropdown_Options').hide();
                $('#WS_List_Options').hide();
                $('#WS_EditableList_Cmd_Items').hide();
                $('#WS_PieChart_Options_Div').hide();
                $('#WS_Table_Options_Div').hide();
                $('#WS_Label_Command').hide();
                $('#WS_Cylinder_Guage_Div').hide();
                if (cntrl.noOfSeries == "1") {
                    $('#Bar_Line_WSValue2').hide();
                    $('#Bar_Line_WSValue3').hide();
                    $('#Bar_Line_WSValue4').hide();
                    $('#Bar_Line_WSValue5').hide();
                }
                if (cntrl.noOfSeries == "2") {
                    $('#Bar_Line_WSValue2').show();
                    $('#Bar_Line_WSValue3').hide();
                    $('#Bar_Line_WSValue4').hide();
                    $('#Bar_Line_WSValue5').hide();
                }
                if (cntrl.noOfSeries == "3") {
                    $('#Bar_Line_WSValue2').show();
                    $('#Bar_Line_WSValue3').show();
                    $('#Bar_Line_WSValue4').hide();
                    $('#Bar_Line_WSValue5').hide();
                }
                if (cntrl.noOfSeries == "4") {
                    $('#Bar_Line_WSValue2').show();
                    $('#Bar_Line_WSValue3').show();
                    $('#Bar_Line_WSValue4').show();
                    $('#Bar_Line_WSValue5').hide();
                }
                if (cntrl.noOfSeries == "5") {
                    $('#Bar_Line_WSValue2').show();
                    $('#Bar_Line_WSValue3').show();
                    $('#Bar_Line_WSValue4').show();
                    $('#Bar_Line_WSValue5').show();
                }

                for (var i = 1; i <= cntrl.noOfSeries; i++) {
                    $('#txtYWSScaleFact' + i).keypress(function (event) {
                        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                    });
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                $('#WS_Image_Options').show();
                $('#WS_Slider_Options').hide();
                $('#WS_Checkbox_Options').hide();
                $('#WS_Cntrl_Options').hide();
                $('#WS_Dropdown_Options').hide();
                $('#WS_List_Options').hide();
                $('#WS_EditableList_Cmd_Items').hide();
                $('#WS_PieChart_Options_Div').hide();
                $('#WS_Bar_LineChart_Options_Div').hide();
                $('#WS_Table_Options_Div').hide();
                $('#WS_Label_Command').hide();
                $('#Image_WsText').show();
                $('#WS_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
                $('#WS_Table_Options_Div').show();
                $('#WS_Slider_Options').hide();
                $('#WS_Checkbox_Options').hide();
                $('#WS_Image_Options').hide();
                $('#WS_Cntrl_Options').hide();
                $('#WS_Dropdown_Options').hide();
                $('#List_WS_Options').hide();
                $('#WS_EditableList_Cmd_Items').hide();
                $('#WS_PieChart_Options_Div').hide();
                $('#WS_Bar_LineChart_Options_Div').hide();
                $('#WS_Cylinder_Guage_Div').hide();
                $('#WS_Label_Command').hide();//TOCHANGE MOHAN
                _biTableDataBindingHelper.formMappingTableHtmlForWs(cntrl,
                    wsType);//TOCHANGE MOHAN
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
                $('#Ws_Location_Options_Div').show();
                $('#WS_Table_Options_Div').hide();
                $('#WS_Slider_Options').hide();
                $('#WS_Checkbox_Options').hide();
                $('#WS_Image_Options').hide();
                $('#WS_Cntrl_Options').hide();
                $('#WS_Dropdown_Options').hide();
                $('#List_WS_Options').hide();
                $('#WS_EditableList_Cmd_Items').hide();
                $('#WS_PieChart_Options_Div').hide();
                $('#WS_Bar_LineChart_Options_Div').hide();
                $('#WS_Label_Command').hide();
                $('#WS_Cylinder_Guage_Div').hide();
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
                $('#WS_PieChart_Options_Div').hide();
                $('#WS_Slider_Options').hide();
                $('#WS_Checkbox_Options').hide();
                $('#WS_Image_Options').hide();
                $('#WS_Cntrl_Options').hide();
                $('#WS_Dropdown_Options').hide();
                $('#WS_List_Options').hide();
                $('#WS_EditableList_Cmd_Items').hide();
                $('#WS_Bar_LineChart_Options_Div').hide();
                $('#WS_Table_Options_Div').hide();
                $('#WS_Label_Command').hide();
                $('#WS_Cylinder_Guage_Div').hide();
                $('#WS_DPieChart_Options_Div').show();
            break;
        }
    }
    function _setODataBindingsOptionsDisplay(cntrl) {//
        $('#Odata_Location_Options_Div').hide();
        $('#OData_DPieChart_Options_Div').hide();
        switch (cntrl.type) {
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                $('#OData_Label_Command').show();
                $('#OData_Checkbox_DisplayText_Div').hide();
                $('#OData_Slider_DisplayText_Div').hide();
                $('#OData_Image_Div').hide();
                $('#OData_Ctrl_DisplayText_Div').hide();
                $('#OData_Dropdown_Options_Div').hide();
                $('#OData_List_Options_Div').hide();
                $('#OData_EditableList_Options_Div').hide();
                $('#OData_PieChart_Options_Div').hide();
                $('#OData_Bar_LineChart_Options_Div').hide();
                $('#OData_Table_Options_Div').hide();
                $('#OData_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                $('#OData_Checkbox_DisplayText_Div').show();
                $('#OData_Slider_DisplayText_Div').hide();
                $('#OData_Ctrl_DisplayText_Div').hide();
                $('#OData_Image_Div').hide();
                $('#OData_Label_Command').hide();
                $('#OData_Dropdown_Options_Div').hide();
                $('#OData_List_Options_Div').hide();
                $('#OData_EditableList_Options_Div').hide();
                $('#OData_PieChart_Options_Div').hide();
                $('#OData_Bar_LineChart_Options_Div').hide();
                $('#OData_Table_Options_Div').hide();
                $('#OData_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
            case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                $('#OData_Slider_DisplayText_Div').show();
                $('#OData_Ctrl_DisplayText_Div').hide();
                $('#OData_Checkbox_DisplayText_Div').hide();
                $('#OData_Image_Div').hide();
                $('#OData_Label_Command').hide();
                $('#OData_Dropdown_Options_Div').hide();
                $('#OData_List_Options_Div').hide();
                $('#OData_EditableList_Options_Div').hide();
                $('#OData_PieChart_Options_Div').hide();
                $('#OData_Bar_LineChart_Options_Div').hide();
                $('#OData_Table_Options_Div').hide();
                $('#OData_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                $('#OData_Cylinder_Guage_Div').show();
                $('#OData_Slider_DisplayText_Div').hide();
                $('#OData_Ctrl_DisplayText_Div').hide();
                $('#OData_Checkbox_DisplayText_Div').hide();
                $('#OData_Image_Div').hide();
                $('#OData_Label_Command').hide();
                $('#OData_Dropdown_Options_Div').hide();
                $('#OData_List_Options_Div').hide();
                $('#OData_EditableList_Options_Div').hide();
                $('#OData_PieChart_Options_Div').hide();
                $('#OData_Bar_LineChart_Options_Div').hide();
                $('#OData_Table_Options_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                $('#OData_Ctrl_DisplayText_Div').show();
                $('#OData_Slider_DisplayText_Div').hide();
                $('#OData_Checkbox_DisplayText_Div').hide();
                $('#OData_Image_Div').hide();
                $('#OData_Label_Command').hide();
                $('#OData_Dropdown_Options_Div').hide();
                $('#OData_List_Options_Div').hide();
                $('#OData_EditableList_Options_Div').hide();
                $('#OData_PieChart_Options_Div').hide();
                $('#OData_Bar_LineChart_Options_Div').hide();
                $('#OData_Table_Options_Div').hide();
                $('#OData_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                $('#OData_Dropdown_Options_Div').show();
                $('#OData_Checkbox_DisplayText_Div').hide();
                $('#OData_Slider_DisplayText_Div').hide();
                $('#OData_Image_Div').hide();
                $('#OData_Label_Command').hide();
                $('#OData_Ctrl_DisplayText_Div').hide();
                $('#OData_List_Options_Div').hide();
                $('#OData_EditableList_Options_Div').hide();
                $('#OData_PieChart_Options_Div').hide();
                $('#OData_Bar_LineChart_Options_Div').hide();
                $('#OData_Table_Options_Div').hide();
                $('#OData_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                $('#OData_List_Options_Div').show();
                $('#OData_Checkbox_DisplayText_Div').hide();
                $('#OData_Slider_DisplayText_Div').hide();
                $('#OData_Image_Div').hide();
                $('#OData_Label_Command').hide();
                $('#OData_Ctrl_DisplayText_Div').hide();
                $('#OData_Dropdown_Options_Div').hide();
                $('#OData_EditableList_Options_Div').hide();
                $('#OData_PieChart_Options_Div').hide();
                $('#OData_Bar_LineChart_Options_Div').hide();
                $('#OData_Table_Options_Div').hide();
                $('#OData_Cylinder_Guage_Div').hide();
                _listDataBindingHelper.showHideDatabindCntrlsByListType(
                    cntrl,
                    MF_IDE_CONSTANTS.ideCommandObjectTypes.odata
                );
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                $('#OData_EditableList_Options_Div').show();
                $('#OData_Slider_DisplayText_Div').hide();
                $('#OData_Checkbox_DisplayText_Div').hide();
                $('#OData_Image_Div').hide();
                $('#OData_Label_Command').hide();
                $('#OData_Ctrl_DisplayText_Div').hide();
                $('#OData_Dropdown_Options_Div').hide();
                $('#OData_List_Options_Div').hide();
                $('#OData_PieChart_Options_Div').hide();
                $('#OData_Bar_LineChart_Options_Div').hide();
                $('#OData_Table_Options_Div').hide();
                $('#OData_Cylinder_Guage_Div').hide();
                _editableListHelper.formMappingHtmlForOdata(cntrl);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                $('#OData_PieChart_Options_Div').show();
                $('#OData_Checkbox_DisplayText_Div').hide();
                $('#OData_Slider_DisplayText_Div').hide();
                $('#OData_Image_Div').hide();
                $('#OData_Label_Command').hide();
                $('#OData_Ctrl_DisplayText_Div').hide();
                $('#OData_Dropdown_Options_Div').hide();
                $('#OData_List_Options_Div').hide();
                $('#OData_EditableList_Options_Div').hide();
                $('#OData_Bar_LineChart_Options_Div').hide();
                $('#OData_Table_Options_Div').hide();
                $('#OData_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
                $('#OData_PieChart_Options_Div').hide();
                $('#OData_Checkbox_DisplayText_Div').hide();
                $('#OData_Slider_DisplayText_Div').hide();
                $('#OData_Image_Div').hide();
                $('#OData_Label_Command').hide();
                $('#OData_Ctrl_DisplayText_Div').hide();
                $('#OData_Dropdown_Options_Div').hide();
                $('#OData_List_Options_Div').hide();
                $('#OData_EditableList_Options_Div').hide();
                $('#OData_Bar_LineChart_Options_Div').hide();
                $('#OData_Table_Options_Div').hide();
                $('#OData_Cylinder_Guage_Div').hide();
                $('#OData_DPieChart_Options_Div').show();
            break;    
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                $('#OData_Bar_LineChart_Options_Div').show();
                $('#OData_Slider_DisplayText_Div').hide();
                $('#OData_Checkbox_DisplayText_Div').hide();
                $('#OData_Image_Div').hide();
                $('#OData_Label_Command').hide();
                $('#OData_Ctrl_DisplayText_Div').hide();
                $('#OData_Dropdown_Options_Div').hide();
                $('#OData_List_Options_Div').hide();
                $('#OData_EditableList_Options_Div').hide();
                $('#OData_PieChart_Options_Div').hide();
                $('#OData_Table_Options_Div').hide();
                $('#OData_Cylinder_Guage_Div').hide();
                if (cntrl.noOfSeries == "1") {
                    $('#Bar_Line_ODataValue2').hide();
                    $('#Bar_Line_ODataValue3').hide();
                    $('#Bar_Line_ODataValue4').hide();
                    $('#Bar_Line_ODataValue5').hide();
                }
                if (cntrl.noOfSeries == "2") {
                    $('#Bar_Line_ODataValue2').show();
                    $('#Bar_Line_ODataValue3').hide();
                    $('#Bar_Line_ODataValue4').hide();
                    $('#Bar_Line_ODataValue5').hide();
                }
                if (cntrl.noOfSeries == "3") {
                    $('#Bar_Line_ODataValue2').show();
                    $('#Bar_Line_ODataValue3').show();
                    $('#Bar_Line_ODataValue4').hide();
                    $('#Bar_Line_ODataValue5').hide();
                }
                if (cntrl.noOfSeries == "4") {
                    $('#Bar_Line_ODataValue2').show();
                    $('#Bar_Line_ODataValue3').show();
                    $('#Bar_Line_ODataValue4').show();
                    $('#Bar_Line_ODataValue5').hide();
                }
                if (cntrl.noOfSeries == "5") {
                    $('#Bar_Line_ODataValue2').show();
                    $('#Bar_Line_ODataValue3').show();
                    $('#Bar_Line_ODataValue4').show();
                    $('#Bar_Line_ODataValue5').show();
                }

                for (var i = 1; i <= cntrl.noOfSeries; i++) {
                    $('#txtYODataScaleFact' + i).keypress(function (event) {
                        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                    });
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                $('#OData_Image_Div').show();
                $('#OData_Checkbox_DisplayText_Div').hide();
                $('#OData_Slider_DisplayText_Div').hide();
                $('#OData_Ctrl_DisplayText_Div').hide();
                $('#OData_Label_Command').hide();
                $('#OData_Dropdown_Options_Div').hide();
                $('#OData_List_Options_Div').hide();
                $('#OData_EditableList_Options_Div').hide();
                $('#OData_PieChart_Options_Div').hide();
                $('#OData_Bar_LineChart_Options_Div').hide();
                $('#OData_Table_Options_Div').hide();
                $('#OData_Cylinder_Guage_Div').hide();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
                $('#OData_Table_Options_Div').show();
                $('#OData_Slider_DisplayText_Div').hide();
                $('#OData_Checkbox_DisplayText_Div').hide();
                $('#OData_Image_Div').hide();
                $('#OData_Label_Command').hide();
                $('#OData_Ctrl_DisplayText_Div').hide();
                $('#OData_Dropdown_Options_Div').hide();
                $('#OData_List_Options_Div').hide();
                $('#OData_EditableList_Options_Div').hide();
                $('#OData_PieChart_Options_Div').hide();
                $('#OData_Cylinder_Guage_Div').hide();
                $('#OData_Bar_LineChart_Options_Div').hide();//TOCHANGE MOHAN
                _biTableDataBindingHelper.formMappingTableHtml(cntrl,
                    MF_IDE_CONSTANTS.ideCommandObjectTypes.odata);//TOCHANGE MOHAN
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
                $('#Odata_Location_Options_Div').show();
                $('#OData_Table_Options_Div').hide();
                $('#OData_Slider_DisplayText_Div').hide();
                $('#OData_Checkbox_DisplayText_Div').hide();
                $('#OData_Image_Div').hide();
                $('#OData_Label_Command').hide();
                $('#OData_Ctrl_DisplayText_Div').hide();
                $('#OData_Dropdown_Options_Div').hide();
                $('#OData_List_Options_Div').hide();
                $('#OData_EditableList_Options_Div').hide();
                $('#OData_PieChart_Options_Div').hide();
                $('#OData_Bar_LineChart_Options_Div').hide();
                $('#OData_Cylinder_Guage_Div').hide();
            break;    
        }
    }
    
    //Tanika * Change
    function _clearDbBindingDropdown(val){
        $('[id$=Db_Dp_Para1_Columns]').html(val);
        $('[id$=Db_Dp_Para2_Columns]').html(val);
        $('[id$=Db_Dp_Para3_Columns]').html(val);
        $('[id$=Db_Dp_Para4_Columns]').html(val);
        $('[id$=Db_Dp_Para5_Columns]').html(val);
        $('[id$=ddlCheckboxCmd_CheckedValue]').html(val);
        $('[id$=ddlSliderCmd_Val]').html(val);
        $('[id$=ddlCtrlCmd_DisplayTxt]').html(val);
        $('[id$=ddlDbCommand_DisplayTxt]').html(val);
        $('[id$=ddlDbCommand_DisplayVal]').html(val);
        $('[id$=ddlList_RowId]').html(val);
        $('[id$=ddlList_Title]').html(val);
        $('[id$=ddlList_SubTitle]').html(val);
        $('[id$=ddlList_Info]').html(val);
        $('[id$=ddlList_AddInfo]').html(val);
        $('[id$=ddlList_Image]').html(val);
        $('[id$=ddlList_Date]').html(val);
        $('[id$=ddlList_CountField]').html(val);
        $('[id$=ddlPieChart_Title]').html(val);
        $('[id$=ddlPieChart_Value]').html(val);
        $('[id$=ddlBar_LineChart_Xaxis]').html(val);
        $('[id$=ddlChartDbCommand_Value]').html(val);
        $('[id$=ddlChartDbCommand_Value2]').html(val);
        $('[id$=ddlChartDbCommand_Value3]').html(val);
        $('[id$=ddlChartDbCommand_Value4]').html(val);
        $('[id$=ddlChartDbCommand_Value5]').html(val);
        $('[id$=ddlImageCmd_DisplayTxt]').html(val);
        $('[id$=ddlImageCmd_DisplayTitle]').html(val);
    }
    function _clearOdataBindingOptionsDropDown(val){
        $('[id$=OData_Dp_Para1_Columns]').html(val);
        $('[id$=OData_Dp_Para2_Columns]').html(val);
        $('[id$=OData_Dp_Para3_Columns]').html(val);
        $('[id$=OData_Dp_Para4_Columns]').html(val);
        $('[id$=OData_Dp_Para5_Columns]').html(val);
        $('[id$=ddlCheckboxODataCmd_CheckedVal]').html(val);
        $('[id$=ddlSliderODataCmd_Val]').html(val);
        $('[id$=ddlCtrlODataCmd_DisplayTxt]').html(val);
        $('[id$=ddlODataCommand_DisplayTxt]').html(val);
        $('[id$=ddlODataCommand_DisplayVal]').html(val);
        $('[id$=ddlODataList_RowId]').html(val);
        $('[id$=ddlODataList_Title]').html(val);
        $('[id$=ddlODataList_SubTitle]').html(val);
        $('[id$=ddlODataList_Info]').html(val);
        $('[id$=ddlODataList_AddInfo]').html(val);
        $('[id$=ddlODataList_Image]').html(val);
        $('[id$=ddlODataList_Date]').html(val);
        $('[id$=ddlODataList_CountField]').html(val);
        $('[id$=ddlODataPieChart_Title]').html(val);
        $('[id$=ddlODataPieChart_Value]').html(val);
        $('[id$=ddlODataBar_LineChart_Xaxis]').html(val);
        $('[id$=ddlImageODataCmd_DisplayTxt]').html(val);
        $('[id$=ddlImageODataCmd_DisplayTitle]').html(val);
    }
    //Tanika * Change
    function _clearWSBindingDropdown(val){
        $('[id$=WS_Dp_Para1_Columns]').html(val);
        $('[id$=WS_Dp_Para2_Columns]').html(val);
        $('[id$=WS_Dp_Para3_Columns]').html(val);
        $('[id$=WS_Dp_Para4_Columns]').html(val);
        $('[id$=WS_Dp_Para5_Columns]').html(val);
        $('[id$=ddlCheckboxWsCommand_CheckedVal]').html(val);
        $('[id$=ddlSliderWsCommand_Val]').html(val);
        $('[id$=ddlCtrlWsCommand_Displaytxt]').html(val);
        $('[id$=ddlWsCommand_Displaytxt]').html(val);
        $('[id$=ddlWsCommand_DisplayVal]').html(val);
        $('[id$=ddlList_Ws_RowId]').html(val);
        $('[id$=ddlList_Ws_Image]').html(val);
        $('[id$=ddlList_Ws_Date]').html(val);
        $('[id$=ddlList_Ws_Title]').html(val);
        $('[id$=ddlList_Ws_Subtitle]').html(val);
        $('[id$=ddlList_Ws_Info]').html(val);
        $('[id$=ddlList_Ws_AddInfo]').html(val);
        $('[id$=ddlList_Ws_CountField]').html(val);
        $('[id$=ddlWsPieChart_Title]').html(val);
        $('[id$=ddlWsPieChart_Value]').html(val);
        $('[id$=ddlWsBar_LineChart_Xaxis]').html(val);
        $('[id$=ddlChartWSCommand_Value]').html(val);
        $('[id$=ddlChartWSCommand_Value2]').html(val);
        $('[id$=ddlChartWSCommand_Value3]').html(val);
        $('[id$=ddlChartWSCommand_Value4]').html(val);
        $('[id$=ddlChartWSCommand_Value5]').html(val);
        $('[id$=ddlImageWsCommand_Displaytxt]').html(val);
        $('[id$=ddlImageWsCommand_DisplayTitle]').html(val);
    }



    //set the options of mapping dropdowns
    function _setDbBindingOptionsDropDown(cntrl, columnDropdownOptions, isEdited) {
        switch (cntrl.type) {
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                $('[id$=Db_Dp_Para1_Columns]').html(columnDropdownOptions);
                $('[id$=Db_Dp_Para2_Columns]').html(columnDropdownOptions);
                $('[id$=Db_Dp_Para3_Columns]').html(columnDropdownOptions);
                $('[id$=Db_Dp_Para4_Columns]').html(columnDropdownOptions);
                $('[id$=Db_Dp_Para5_Columns]').html(columnDropdownOptions);
                if(isEdited){
                    var row = 0;
                    if((cntrl.labelDataObjs != undefined || cntrl.labelDataObjs != null) && cntrl.labelDataObjs.length > 0){
                        for(var i=0; i < cntrl.labelDataObjs.length; i++){
                                row = i + 1;
                                if(cntrl.labelDataObjs[i].selectedCol != null || cntrl.labelDataObjs[i].selectedCol != undefined || cntrl.labelDataObjs[i].selectedCol != "") $('[id$=Db_Dp_Para' + row + '_Columns]').val(cntrl.labelDataObjs[i].selectedCol);
                        }
                    }
                }

                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                $('[id$=ddlCheckboxCmd_CheckedValue]').html(columnDropdownOptions);
                if(isEdited)
                    if(cntrl.checkedVal != null || cntrl.checkedVal != undefined || cntrl.checkedVal != "") $('[id$=ddlCheckboxCmd_CheckedValue]').val(cntrl.checkedVal);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
            case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                $('[id$=ddlSliderCmd_Val]').html(columnDropdownOptions);
                if(isEdited) 
                    if(cntrl.value != null || cntrl.value != undefined || cntrl.value != "") $('[id$=ddlSliderCmd_Val]').val(cntrl.value);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                $('[id$=ddlCylinder_Guage_Value]').html(columnDropdownOptions);
                $('[id$=ddlCylinder_Guage_MaxVal]').html(columnDropdownOptions);
                if(isEdited) {
                        if(cntrl.value != null || cntrl.value != undefined || cntrl.value != "") $('[id$=ddlCylinder_Guage_Value]').val(cntrl.value);
                        if(cntrl.maxVal != null || cntrl.maxVal != undefined || cntrl.maxVal != "") $('[id$=ddlCylinder_Guage_MaxVal]').val(cntrl.maxVal);
                    }
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                $('[id$=ddlCtrlCmd_DisplayTxt]').html(columnDropdownOptions);
                if(isEdited)
                    if(cntrl.displayText != null || cntrl.displayText != undefined || cntrl.displayText != "") $('[id$=ddlCtrlCmd_DisplayTxt]').val(cntrl.displayText);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                $('[id$=ddlDbCommand_DisplayTxt]').html(columnDropdownOptions);
                $('[id$=ddlDbCommand_DisplayVal]').html(columnDropdownOptions);
                if(isEdited){
                    if(cntrl.optionText != null || cntrl.optionText != undefined || cntrl.optionText != "") $('[id$=ddlDbCommand_DisplayTxt]').val(cntrl.optionText);
                    if(cntrl.optionValue != null || cntrl.optionValue != undefined || cntrl.optionValue != "") $('[id$=ddlDbCommand_DisplayVal]').val(cntrl.optionValue);
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
            //TOCHANGE MOHAN
                _listDataBindingHelper.bindDatabindingDdlsObjectType(
                    cntrl,
                    isEdited,
                    MF_IDE_CONSTANTS.ideCommandObjectTypes.db,
                    columnDropdownOptions);//TOCHANGE MOHAN
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                _editableListHelper.bindMappingDropdownForDb(cntrl,
                    isEdited,columnDropdownOptions);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                $('[id$=ddlPieChart_Title]').html(columnDropdownOptions);
                $('[id$=ddlPieChart_Value]').html(columnDropdownOptions);
                if(isEdited){
                    if(cntrl.title != null || cntrl.title != undefined || cntrl.title != "") $('[id$=ddlPieChart_Title]').val(cntrl.title);
                    if(cntrl.value != null || cntrl.value != undefined || cntrl.value != "") $('[id$=ddlPieChart_Value]').val(cntrl.value);
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
                // $('[id$=ddlDbDPieChart_Slice_Title]').html(columnDropdownOptions);
                // $('[id$=ddlDbDPieChart_SubSlice_Title]').html(columnDropdownOptions);
                // $('[id$=ddlDbDPieChart_Value]').html(columnDropdownOptions);
                // if(isEdited){
                //     if(cntrl.title != null || cntrl.title != undefined || cntrl.title != "") $('[id$=ddlDbDPieChart_Slice_Title]').val(cntrl.title);
                //     if(cntrl.value != null || cntrl.value != undefined || cntrl.value != "") $('[id$=ddlDbDPieChart_Value]').val(cntrl.value);
                //     pieChartDrilldownTitle = cntrl.fnGetDrilldownTitle();
                //     if(!mfUtil.isNullOrUndefined(pieChartDrilldownTitle) 
                //     && !mfUtil.isEmptyString(pieChartDrilldownTitle)) {
                //         $('[id$=ddlDbDPieChart_SubSlice_Title]').val(pieChartDrilldownTitle);
                //     }
                // }
                _drilldownPieDatabindingHelper.bindMappingDropdown(
                    MF_IDE_CONSTANTS.ideCommandObjectTypes.db,
                    columnDropdownOptions);
                if(isEdited === true)   {
                    _drilldownPieDatabindingHelper.setDataInHtmlForMapping(
                        cntrl,
                        MF_IDE_CONSTANTS.ideCommandObjectTypes.db);
                }
            break;    
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                $('[id$=ddlBar_LineChart_Xaxis]').html(columnDropdownOptions);
                $('[id$=ddlChartDbCommand_Value]').html(columnDropdownOptions);
                $('[id$=ddlChartDbCommand_Value2]').html(columnDropdownOptions);
                $('[id$=ddlChartDbCommand_Value3]').html(columnDropdownOptions);
                $('[id$=ddlChartDbCommand_Value4]').html(columnDropdownOptions);
                $('[id$=ddlChartDbCommand_Value5]').html(columnDropdownOptions);
                if(isEdited){
                    var index = 0;
                    if(cntrl.xAxisData != null || cntrl.xAxisData != undefined || cntrl.xAxisData != "") $('[id$=ddlBar_LineChart_Xaxis]').val(cntrl.xAxisData);
                    if((cntrl.yAxisDataObjs != null || cntrl.yAxisDataObjs != undefined) && cntrl.yAxisDataObjs.length > 0){
                        for(var n = 0; n < cntrl.yAxisDataObjs.length; n++){
                            index = n + 1;
                            if(n == 0){
                              if(cntrl.yAxisDataObjs[n].value != null || cntrl.yAxisDataObjs[n].value != undefined || cntrl.yAxisDataObjs[n].value != "") $('[id$=ddlChartDbCommand_Value]').val(cntrl.yAxisDataObjs[n].value);  
                            }
                            else{
                              if(cntrl.yAxisDataObjs[n].value != null || cntrl.yAxisDataObjs[n].value != undefined || cntrl.yAxisDataObjs[n].value != "") $('[id$=ddlChartDbCommand_Value' + index + ']').val(cntrl.yAxisDataObjs[n].value);  
                            }
                            
                            $('#txtYDbLabel' + index).val(cntrl.yAxisDataObjs[n].label); 
                            $('#txtYDbScaleFact' + index).val(cntrl.yAxisDataObjs[n].scaleFactor);  
                        }
                    }
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                $('[id$=ddlImageCmd_DisplayTxt]').html(columnDropdownOptions);
                $('[id$=ddlImageCmd_DisplayTitle]').html(columnDropdownOptions);
                if(isEdited){
                    if(cntrl.imageSource != null || cntrl.imageSource != undefined || cntrl.imageSource != "") $('[id$=ddlImageCmd_DisplayTxt]').val(cntrl.imageSource);
                    if(cntrl.imageTitle != null || cntrl.imageTitle != undefined || cntrl.imageTitle != "") $('[id$=ddlImageCmd_DisplayTitle]').val(cntrl.imageTitle);
                    if(cntrl.imageDataBindType != null || cntrl.imageDataBindType != undefined || cntrl.imageDataBindType != "") $('[id$=ddlImage_SouceType]').val(cntrl.imageDataBindType);
                    }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
               _biTableDataBindingHelper.bindMappingDropdown(
                    cntrl,
                    isEdited,
                    columnDropdownOptions,
                    MF_IDE_CONSTANTS.ideCommandObjectTypes.db
                );
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
                    _locationDatabindingHelper.bindMappingDropdown(
                        MF_IDE_CONSTANTS.ideCommandObjectTypes.db,
                        columnDropdownOptions
                    );
                    if(isEdited === true){
                       _locationDatabindingHelper.setDataInHtmlForMapping(
                           cntrl,
                           MF_IDE_CONSTANTS.ideCommandObjectTypes.db
                       ); 
                    }
             break;    
        }
    }
    function _setODataBindingOptionsDropDown(cntrl, valueDropdownOptions, isEdited) {
        switch (cntrl.type) {
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                $('[id$=OData_Dp_Para1_Columns]').html(valueDropdownOptions);
                $('[id$=OData_Dp_Para2_Columns]').html(valueDropdownOptions);
                $('[id$=OData_Dp_Para3_Columns]').html(valueDropdownOptions);
                $('[id$=OData_Dp_Para4_Columns]').html(valueDropdownOptions);
                $('[id$=OData_Dp_Para5_Columns]').html(valueDropdownOptions);
                if(isEdited){
                    var row = 0;
                    if((cntrl.labelDataObjs != undefined || cntrl.labelDataObjs != null) && cntrl.labelDataObjs.length > 0){
                        for(var i=0; i < cntrl.labelDataObjs.length; i++){
                            row = i + 1;
                            if(cntrl.labelDataObjs[i].selectedCol != null || cntrl.labelDataObjs[i].selectedCol != undefined || cntrl.labelDataObjs[i].selectedCol != "") $('[id$=OData_Dp_Para' + row + '_Columns]').val(cntrl.labelDataObjs[i].selectedCol);
                        }
                    }
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                $('[id$=ddlCheckboxODataCmd_CheckedVal]').html(valueDropdownOptions);
                if(isEdited) 
                    if(cntrl.checkedVal != null || cntrl.checkedVal != undefined || cntrl.checkedVal != "") $('[id$=ddlCheckboxODataCmd_CheckedVal]').val(cntrl.checkedVal);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
            case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                $('[id$=ddlSliderODataCmd_Val]').html(valueDropdownOptions);
                if(isEdited) 
                    if(cntrl.value != null || cntrl.value != undefined || cntrl.displayText != "") $('[id$=ddlSliderODataCmd_Val]').val(cntrl.value);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                $('[id$=ddl_OData_Cylinder_Guage_Value]').html(valueDropdownOptions);
                $('[id$=ddl_OData_Cylinder_Guage_MaxVal]').html(valueDropdownOptions);
                if(isEdited) {
                        if(cntrl.value != null || cntrl.value != undefined || cntrl.value != "") $('[id$=ddl_OData_Cylinder_Guage_Value]').val(cntrl.value);
                        if(cntrl.maxVal != null || cntrl.maxVal != undefined || cntrl.maxVal != "") $('[id$=ddl_OData_Cylinder_Guage_MaxVal]').val(cntrl.maxVal);
                    }
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                $('[id$=ddlCtrlODataCmd_DisplayTxt]').html(valueDropdownOptions);
                if(isEdited) 
                    if(cntrl.displayText != null || cntrl.displayText != undefined || cntrl.displayText != "") $('[id$=ddlCtrlODataCmd_DisplayTxt]').val(cntrl.displayText);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                $('[id$=ddlODataCommand_DisplayTxt]').html(valueDropdownOptions);
                $('[id$=ddlODataCommand_DisplayVal]').html(valueDropdownOptions);
                if(isEdited) {
                    if(cntrl.optionText != null || cntrl.optionText != undefined || cntrl.optionText != "") $('[id$=ddlODataCommand_DisplayTxt]').val(cntrl.optionText);
                    if(cntrl.optionValue != null || cntrl.optionValue != undefined || cntrl.optionValue != "") $('[id$=ddlODataCommand_DisplayVal]').val(cntrl.optionValue);
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                //TOCHANGE MOHAN
                _listDataBindingHelper.bindDatabindingDdlsObjectType(
                    cntrl,
                    isEdited,
                    MF_IDE_CONSTANTS.ideCommandObjectTypes.odata,
                    valueDropdownOptions);//TOCHANGE MOHAN
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                 _editableListHelper.bindMappingDropdownForOdata(
                     cntrl,
                     isEdited,
                     valueDropdownOptions);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                $('[id$=ddlODataPieChart_Title]').html(valueDropdownOptions);
                $('[id$=ddlODataPieChart_Value]').html(valueDropdownOptions);
                if(isEdited) {
                    if(cntrl.title != null || cntrl.title != undefined || cntrl.title != "") $('[id$=ddlODataPieChart_Title]').val(cntrl.title);
                    if(cntrl.value != null || cntrl.value != undefined || cntrl.value != "") $('[id$=ddlODataPieChart_Value]').val(cntrl.value);
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
                // $('[id$=ddlODataDPieChart_Slice_Title]').html(valueDropdownOptions);
                // $('[id$=ddlODataDPieChart_SubSlice_Title]').html(valueDropdownOptions);
                // $('[id$=ddlODataDPieChart_Value]').html(valueDropdownOptions);
                // if(isEdited){
                //     if(cntrl.title != null || cntrl.title != undefined || cntrl.title != "") $('[id$=ddlODataDPieChart_Slice_Title]').val(cntrl.title);
                //     if(cntrl.value != null || cntrl.value != undefined || cntrl.value != "") $('[id$=ddlODataDPieChart_Value]').val(cntrl.value);
                //     pieChartDrilldownTitle = cntrl.fnGetDrilldownTitle();
                //     if(!mfUtil.isNullOrUndefined(pieChartDrilldownTitle) 
                //     && !mfUtil.isEmptyString(pieChartDrilldownTitle)) {
                //         $('[id$=ddlODataDPieChart_SubSlice_Title]').val(pieChartDrilldownTitle);
                //     }
                // }
                _drilldownPieDatabindingHelper.bindMappingDropdown(
                    MF_IDE_CONSTANTS.ideCommandObjectTypes.odata,
                    valueDropdownOptions);
                if(isEdited === true)   {
                    _drilldownPieDatabindingHelper.setDataInHtmlForMapping(
                        cntrl,
                        MF_IDE_CONSTANTS.ideCommandObjectTypes.odata);
                }
            break;    
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                $('[id$=ddlODataBar_LineChart_Xaxis]').html(valueDropdownOptions);
                $('[id$=ddlChartODataCommand_Value]').html(valueDropdownOptions);
                $('[id$=ddlChartODataCommand_Value2]').html(valueDropdownOptions);
                $('[id$=ddlChartODataCommand_Value3]').html(valueDropdownOptions);
                $('[id$=ddlChartODataCommand_Value4]').html(valueDropdownOptions);
                $('[id$=ddlChartODataCommand_Value5]').html(valueDropdownOptions);
                
                if(isEdited) {
                    var index = 0;
                    if(cntrl.xAxisData != null || cntrl.xAxisData != undefined || cntrl.xAxisData != "") $('[id$=ddlODataBar_LineChart_Xaxis]').val(cntrl.xAxisData);
                    if((cntrl.yAxisDataObjs != null || cntrl.yAxisDataObjs != undefined) && cntrl.yAxisDataObjs.length > 0){
                        for(var n = 0; n < cntrl.yAxisDataObjs.length; n++){
                              index = n + 1;
                            if(n == 0){
                              if(cntrl.yAxisDataObjs[n].value != null || cntrl.yAxisDataObjs[n].value != undefined || cntrl.yAxisDataObjs[n].value != "") $('[id$=ddlChartODataCommand_Value]').val(cntrl.yAxisDataObjs[n].value);  
                            }
                            else{
                              if(cntrl.yAxisDataObjs[n].value != null || cntrl.yAxisDataObjs[n].value != undefined || cntrl.yAxisDataObjs[n].value != "") $('[id$=ddlChartODataCommand_Value' + index + ']').val(cntrl.yAxisDataObjs[n].value);  
                            }
                            
                            $('#txtYODataLabel' + index).val(cntrl.yAxisDataObjs[n].label); 
                            $('#txtYODataScaleFact' + index).val(cntrl.yAxisDataObjs[n].scaleFactor);   
                        }
                    }
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                $('[id$=ddlImageODataCmd_DisplayTxt]').html(valueDropdownOptions);
                $('[id$=ddlImageODataCmd_DisplayTitle]').html(valueDropdownOptions);
                if(isEdited){
                    if(cntrl.imageSource != null || cntrl.imageSource != undefined || cntrl.imageSource != "") $('[id$=ddlImageODataCmd_DisplayTxt]').val(cntrl.imageSource);
                    if(cntrl.imageTitle != null || cntrl.imageTitle != undefined || cntrl.imageTitle != "") $('[id$=ddlImageODataCmd_DisplayTitle]').val(cntrl.imageTitle);
                    if(cntrl.imageDataBindType != null || cntrl.imageDataBindType != undefined || cntrl.imageDataBindType != "") $('[id$=ddlODataImage_SouceType]').val(cntrl.imageDataBindType);
                    }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
            //TOCHANGE MOHAN
                _biTableDataBindingHelper.bindMappingDropdown(
                    cntrl,
                    isEdited,
                    valueDropdownOptions,
                    MF_IDE_CONSTANTS.ideCommandObjectTypes.odata
                );//TOCHANGE MOHAN
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
                   _locationDatabindingHelper.bindMappingDropdown(
                       MF_IDE_CONSTANTS.ideCommandObjectTypes.odata,
                       valueDropdownOptions
                   );
                   if(isEdited === true){
                      _locationDatabindingHelper.setDataInHtmlForMapping(
                          cntrl,
                          MF_IDE_CONSTANTS.ideCommandObjectTypes.odata
                      ); 
                   }
            break;     
        }
    }
    function _setWSBindingOptionsDropDown(cntrl, tagDropdownOptions, wsType, isEdited ) {
        switch (cntrl.type) {
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                $('[id$=ddlCheckboxWsCommand_CheckedVal]').html(tagDropdownOptions);
                if(isEdited)
                    if(cntrl.checkedVal != null || cntrl.checkedVal != undefined || cntrl.checkedVal != "") $('[id$=ddlCheckboxWsCommand_CheckedVal]').val(cntrl.checkedVal);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
            case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                 $('[id$=ddlSliderWsCommand_Val]').html(tagDropdownOptions);
                if(isEdited)
                    if(cntrl.value != null || cntrl.value != undefined || cntrl.displayText != "") $('[id$=ddlSliderWsCommand_Val]').val(cntrl.value);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                $('[id$=ddl_WS_Cylinder_Guage_Value]').html(tagDropdownOptions);
                $('[id$=ddl_WS_Cylinder_Guage_MaxVal]').html(tagDropdownOptions);
                if(isEdited) {
                        if(cntrl.value != null || cntrl.value != undefined || cntrl.value != "") $('[id$=ddl_WS_Cylinder_Guage_Value]').val(cntrl.value);
                        if(cntrl.maxVal != null || cntrl.maxVal != undefined || cntrl.maxVal != "") $('[id$=ddl_WS_Cylinder_Guage_MaxVal]').val(cntrl.maxVal);
                    }
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                $('[id$=WS_Dp_Para1_Columns]').html(tagDropdownOptions);
                $('[id$=WS_Dp_Para2_Columns]').html(tagDropdownOptions);
                $('[id$=WS_Dp_Para3_Columns]').html(tagDropdownOptions);
                $('[id$=WS_Dp_Para4_Columns]').html(tagDropdownOptions);
                $('[id$=WS_Dp_Para5_Columns]').html(tagDropdownOptions);
                
                if(isEdited) {
                    var row = 0;
                    if((cntrl.labelDataObjs != undefined || cntrl.labelDataObjs != null) && cntrl.labelDataObjs.length > 0){
                        for(var i=0; i < cntrl.labelDataObjs.length; i++){
                            row = i + 1;
                            if(cntrl.labelDataObjs[i].selectedCol != null || cntrl.labelDataObjs[i].selectedCol != undefined || cntrl.labelDataObjs[i].selectedCol != "") $('[id$=WS_Dp_Para' + row + '_Columns]').val(cntrl.labelDataObjs[i].selectedCol);
                        }
                    }
                }
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                 $('[id$=ddlCtrlWsCommand_Displaytxt]').html(tagDropdownOptions);
                if(isEdited)
                    if(cntrl.displayText != null || cntrl.displayText != undefined || cntrl.displayText != "") $('[id$=ddlCtrlWsCommand_Displaytxt]').val(cntrl.displayText);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                $('[id$=ddlWsCommand_Displaytxt]').html(tagDropdownOptions);
                $('[id$=ddlWsCommand_DisplayVal]').html(tagDropdownOptions);
                if(isEdited) {
                    if(cntrl.optionText != null || cntrl.optionText != undefined || cntrl.optionText != "") $('[id$=ddlWsCommand_Displaytxt]').val(cntrl.optionText);
                    if(cntrl.optionValue != null || cntrl.optionValue != undefined || cntrl.optionValue != "") $('[id$=ddlWsCommand_DisplayVal]').val(cntrl.optionValue);
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                //TOCHANGE MOHAN
                _listDataBindingHelper.bindDatabindingDdlsObjectType(
                    cntrl,
                    isEdited,
                    MF_IDE_CONSTANTS.ideCommandObjectTypes.http,
                    tagDropdownOptions);//TOCHANGE MOHAN
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
               _editableListHelper.bindMappingDropdownForWs(
                    cntrl,
                    isEdited,
                    tagDropdownOptions,
                    wsType);
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                $('[id$=ddlWsPieChart_Title]').html(tagDropdownOptions);
                $('[id$=ddlWsPieChart_Value]').html(tagDropdownOptions);
                if(isEdited) {
                    if(cntrl.title != null || cntrl.title != undefined || cntrl.title != "") $('[id$=ddlWsPieChart_Title]').val(cntrl.title);
                    if(cntrl.value != null || cntrl.value != undefined || cntrl.value != "") $('[id$=ddlWsPieChart_Value]').val(cntrl.value);
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
                // $('[id$=ddlWsDPieChart_Slice_Title]').html(tagDropdownOptions);
                // $('[id$=ddlWsDPieChart_SubSlice_Title]').html(tagDropdownOptions);
                // $('[id$=ddlWsDPieChart_Value]').html(tagDropdownOptions);
                // if(isEdited){
                //     if(cntrl.title != null || cntrl.title != undefined || cntrl.title != "") $('[id$=ddlWsDPieChart_Slice_Title]').val(cntrl.title);
                //     if(cntrl.value != null || cntrl.value != undefined || cntrl.value != "") $('[id$=ddlWsDPieChart_Value]').val(cntrl.value);
                //     pieChartDrilldownTitle = cntrl.fnGetDrilldownTitle();
                //     if(!mfUtil.isNullOrUndefined(pieChartDrilldownTitle) 
                //     && !mfUtil.isEmptyString(pieChartDrilldownTitle)) {
                //         $('[id$=ddlWsDPieChart_SubSlice_Title]').val(pieChartDrilldownTitle);
                //     }
                // }
                _drilldownPieDatabindingHelper.bindMappingDropdown(
                    MF_IDE_CONSTANTS.ideCommandObjectTypes.http,
                    tagDropdownOptions);
                if(isEdited === true)   {
                    _drilldownPieDatabindingHelper.setDataInHtmlForMapping(
                        cntrl,
                        MF_IDE_CONSTANTS.ideCommandObjectTypes.http);
                }
            break;    
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                $('[id$=ddlWsBar_LineChart_Xaxis]').html(tagDropdownOptions);
                $('[id$=ddlChartWSCommand_Value]').html(tagDropdownOptions);
                $('[id$=ddlChartWSCommand_Value2]').html(tagDropdownOptions);
                $('[id$=ddlChartWSCommand_Value3]').html(tagDropdownOptions);
                $('[id$=ddlChartWSCommand_Value4]').html(tagDropdownOptions);
                $('[id$=ddlChartWSCommand_Value5]').html(tagDropdownOptions);
                
                if(isEdited) {
                    var index = 0;
                    if(cntrl.xAxisData != null || cntrl.xAxisData != undefined || cntrl.xAxisData != "") $('[id$=ddlWsBar_LineChart_Xaxis]').val(cntrl.xAxisData);
                    if((cntrl.yAxisDataObjs != null || cntrl.yAxisDataObjs != undefined) && cntrl.yAxisDataObjs.length > 0){
                        for(var n = 0; n < cntrl.yAxisDataObjs.length; n++){
                              index = n + 1;
                            if(n == 0){
                              if(cntrl.yAxisDataObjs[n].value != null || cntrl.yAxisDataObjs[n].value != undefined || cntrl.yAxisDataObjs[n].value != "") $('[id$=ddlChartWSCommand_Value]').val(cntrl.yAxisDataObjs[n].value);  
                            }
                            else{
                              if(cntrl.yAxisDataObjs[n].value != null || cntrl.yAxisDataObjs[n].value != undefined || cntrl.yAxisDataObjs[n].value != "") $('[id$=ddlChartWSCommand_Value' + index + ']').val(cntrl.yAxisDataObjs[n].value);  
                            }
                            
                            $('#txtYWSLabel' + index).val(cntrl.yAxisDataObjs[n].label); 
                            $('#txtYWSScaleFact' + index).val(cntrl.yAxisDataObjs[n].scaleFactor);     
                        }
                    }
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                $('[id$=ddlImageWsCommand_Displaytxt]').html(tagDropdownOptions);
                $('[id$=ddlImageWsCommand_DisplayTitle]').html(tagDropdownOptions);
                if(isEdited){
                    if(cntrl.imageSource != null || cntrl.imageSource != undefined || cntrl.imageSource != "") $('[id$=ddlImageWsCommand_Displaytxt]').val(cntrl.imageSource);
                    if(cntrl.imageTitle != null || cntrl.imageTitle != undefined || cntrl.imageTitle != "") $('[id$=ddlImageWsCommand_DisplayTitle]').val(cntrl.imageTitle);
                    if(cntrl.imageDataBindType != null || cntrl.imageDataBindType != undefined || cntrl.imageDataBindType != "") $('[id$=ddlWsImage_SouceType]').val(cntrl.imageDataBindType);
                    }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
                _biTableDataBindingHelper.bindMappingDropdownForWs(
                    cntrl,
                    isEdited,
                    tagDropdownOptions,
                    wsType
                );
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
                   _locationDatabindingHelper.bindMappingDropdown(
                       MF_IDE_CONSTANTS.ideCommandObjectTypes.http,
                       tagDropdownOptions
                   );
                   if(isEdited === true){
                      _locationDatabindingHelper.setDataInHtmlForMapping(
                          cntrl,
                          MF_IDE_CONSTANTS.ideCommandObjectTypes.http
                      ); 
                   }
            break;    
        }
    }
    
    //Tanika*Change
    function _showDbCmdDetails(cmd) {
        //Command Details
        if (cmd != null || cmd != undefined) {
            if (cmd.commandId != null || cmd.commandId != undefined) {
                if (cmd.commandId == "-1") {
                    showMessage('Please select database object.', DialogType.Error);
                    return false;
                }
                else {
                    $('[id$=lblDbCmdName]').html(cmd.commandName);
                    $('[id$=lblDbConnName]').html(cmd.connectionName);
                    $('[id$=lblDbCmdTyp]').html(_getDBCommandType(cmd.commandType));
                    $('[id$=lblDbCmdTbl]').html(cmd.tableName);
                    $('[id$=lblDbCmdSql]').html(cmd.sqlQuery);
                    $('[id$=lblDbDescription]').html(cmd.description);
                    showModalPopUp('SubprocDbCmdDetails', 'Object Details', 580);
                }
            }
        }
    }
    function _showWSCmdDetails(cmd) {
        if (cmd != null || cmd != undefined) {
            if (cmd.commandId != null || cmd.commandId != undefined) {
                if (cmd.commandId == "-1") {
                    showMessage('Please select webservice object.', DialogType.Error);
                    return false;
                }
                else {
                    $('[id$=lblWsCmdName]').html(cmd.commandName);
                    $('[id$=lblWsConnName]').html(cmd.connectionName);
                    $('[id$=lblWsSrvTyp]').html(_getWSType(cmd.webserviceType).UiName);
                    $('[id$=lblWSDescription]').html(cmd.description);
                    var wsInputParams = mFicientIde.MF_HELPERS.getInputParamsForCmdObject(cmd.commandId, _getWSCmdObjectType(cmd.webserviceType));
                    switch (_getWSType(cmd.webserviceType)) {
                        case MF_IDE_CONSTANTS.webServiceType.wsdlSoap:
                            $('[id$=lblWsSrvName]').html(cmd.service);
                            $('[id$=lblWsMethod]').html(cmd.method);
                            $('[id$=DivWsWsdlDet]').show();
                            $('[id$=DivWsHttpDet]').hide();
                            $('[id$=DivWsRpcDet]').hide();
                            break;
                        case MF_IDE_CONSTANTS.webServiceType.http:
                            $('[id$=lblWsHName]').html(cmd.service);
                            $('[id$=lblWsUrl]').html(cmd.url);
                            var outputElementsTags = mFicientIde.MF_HELPERS.getWSTags(cmd.commandId, _getWSType(cmd.webserviceType));
                            var strTags = "";
                            $.each(outputElementsTags, function () {
                                if (strTags.length > 0)
                                    strTags += ",";
                                strTags += this;
                            });
                            $('[id$=lblWsOutElm]').html(strTags);
                            $('[id$=DivWsWsdlDet]').hide();
                            $('[id$=DivWsHttpDet]').show();
                            $('[id$=DivWsRpcDet]').hide();
                            break;
                        case MF_IDE_CONSTANTS.webServiceType.xmlRpc:
                            var strInputParams = "";
                            $.each(wsInputParams, function () {
                                if (strInputParams.length > 0)
                                    strInputParams += ",";
                                strInputParams += this;
                            });

                            $('[id$=lblWsInParam]').html(strInputParams);
                            $('[id$=lblWsOutParam]').html(cmd.url);
                            $('[id$=DivWsWsdlDet]').hide();
                            $('[id$=DivWsHttpDet]').hide();
                            $('[id$=DivWsRpcDet]').show();
                            break;
                    }
                    showModalPopUp('SubprocWsCmdDetails', 'Object Details', 580);
                }
            }
        }
    }
    function _showODataCmdDetails(cmd) {
        //Command Details
        if (cmd != null || cmd != undefined) {
            if (cmd.commandId != null || cmd.commandId != undefined) {
                if (cmd.commandId == "-1") {
                    showMessage('Please select database object.', DialogType.Error);
                    return false;
                }
                else {
                    $('[id$=lblCtrlOCmd_Name]').html(cmd.commandName);
                    $('[id$=lblCtrlOCmd_Conn]').html(cmd.connectionName);
                    $('[id$=lblCtrlOCmd_EndPoint]').html(cmd.endPoint);
                    $('[id$=lblCtrlOCmd_Version]').html(cmd.serviceVer);
                    $('[id$=lblCtrlOCmd_Description]').html(cmd.description);
                    showModalPopUp('SubProcBoxCtrlODataCmdDetails', 'Object Details', 580);
                }
            }
        }
    }
    
    function _showOfflineDataObjDetails(offlineDataObj) {
        //Object Details
        if (offlineDataObj != null || offlineDataObj != undefined) {
            if (offlineDataObj.ID != null || offlineDataObj.ID != undefined) {
                if (offlineDataObj.ID == "-1") {
                    showMessage('Please select offline Data object.', DialogType.Error);
                    return false;
                }
                else {
                    $('[id$=lblCtrlOffDataObj_Name]').html(offlineDataObj.Name);
                    $('[id$=lblCtrlOffDataObj_Description]').html(offlineDataObj.Description);
                    $('[id$=lblCtrlOffDataObj_Query]').html(offlineDataObj.Query);
                    $('[id$=lblCtrlOffDataObj_Tables]').html(offlineDataObj.OfflineTables);

            switch(offlineDataObj.QueryType)
            {
                case 1:
                    $('[id$=lblCtrlOffDataObj_Type]').html("SELECT");
                    break;
                case 2:
                    $('[id$=lblCtrlOffDataObj_Type]').html("INSERT");
                    break;
                case 3:
                    $('[id$=lblCtrlOffDataObj_Type]').html("UPDATE");
                    break;
                case 4:
                    $('[id$=lblCtrlOffDataObj_Type]').html("DELETE");
                    break;
            }
                    showModalPopUp('SubProcBoxCtrlOfflineDataObjDetails', 'Object Details', 580);
                }
            }
        }
    }

    function _bindDataLinkParameters(cmd, inputParams, btnId, objectType, databindObj){
        $('[id$=' + btnId + ']').show();
        $('[id$=' + btnId + ']').unbind('click');
        if(cmd != null)
        {
            $('[id$=' + btnId + ']').bind('click', function () {
                var strPostFix = '';
                var LpCount = 0;
                var isEdited = false;
                $('#LpCmdType').html(objectType);
                $('#LpCmdName').html(cmd.commandName);
                if((databindObj.cmdParams != undefined || databindObj.cmdParams != null) && databindObj.cmdParams.length > 0){
                    $('#LPContainerDiv').html('');
                    isEdited = true;
                    LpCount =  _editLinkParameters(inputParams, databindObj);
                }
                else{
                    $('#LPContainerDiv').html('');
                    isEdited = false;
                    LpCount = _addDataLinkParameters(inputParams, databindObj);
                }

                for (var i = 0; i < LpCount; i++) {
                    if(!isEdited) $('#ddlLpControl_' + i).attr('disabled', 'disabled');
                    $('#ddlLPValueType_' + i).bind('change', function (e) {
                        strPostFix = "";
                        for (var i = 0; i < e.target.id.split('_').length; i++) {
                            if (i != 0) {
                                if (strPostFix.length > 0) {
                                    strPostFix += "_";
                                }
                                strPostFix += e.target.id.split('_')[i];
                            }
                        }
                        if (e.target.value == "Constant") {
                            $('#ddlLpControl_' + strPostFix).attr('disabled', 'disabled');
                            $('#LpConstantValue_' + strPostFix).removeAttr('disabled');
                        }
                        else {
                            $('#LpConstantValue_' + strPostFix).attr('disabled', 'disabled');
                            $('#ddlLpControl_' + strPostFix).removeAttr('disabled');
                        }
                    });
                }
                SubProcBoxLinkParameterToCtrl(true);
                return false;
            });
            $('[id$=btnLpPara_Save]').unbind('click');
            $('[id$=btnLpPara_Save]').bind('click', function () {
                _saveLinkParameters(databindObj);
            });
        }
    }
    function _addDataLinkParameters(inputParams, databindObj) {
        var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
        var controls = form.fnGetAllControls();
        var strContHtml = '';
        var LpCount = 0;
        var strAllControls = '<option value="None">None</option>';
        if ((controls != null || controls != undefined) && controls.length > 0) {
            $.each(controls, function () {
                strAllControls += '<option value="' + this.userDefinedName + '">' + this.userDefinedName + '</option>'
            });
        }
        $.each(inputParams, function () {
            strContHtml += '<div id="LpContentText_' + LpCount + '" class="LpContentRow">'
                                + '<div class="LpContentText">'
                                    + '<a id="Lptext_' + LpCount + '" type="Parameter">' + this + '</a>'
                                + '</div>'
                                + '<div class="LpContentValue">'
                                    + '<select id="ddlLPValueType_' + LpCount + '"   class="txtLpOption" onkeydown=\"return enterKeyFilter(event);\">'
                                        + '<option value="Constant">Constant</option>'
                                        + '<option value="Control">Control</option>'
                                    + '</select>'
                                + '</div>'
                                + '<div class="LpContentValue">'
                                    + '<select id="ddlLpControl_' + LpCount + '"   class="txtLpOption" onkeydown=\"return enterKeyFilter(event);\">'
                                        + strAllControls
                                    + '</select>'
                                + '</div>'
                                + '<div class="LpContentValue">'
                                    + '<input id="LpConstantValue_' + LpCount + '" type="text"  class="txtLpOption" onkeydown=\"return enterKeyFilter(event);\"/>'
                                + '</div>'
                            + '</div>'
                            + '<div style="clear: both;"></div>';
            LpCount = LpCount + 1;
        });

        $('#LPContainerDiv').html(strContHtml);
        return LpCount;
    }
    function _editLinkParameters(inputParams, databindObj){
        var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //($('[id$=hdfCurrentFormId]').val());
        var controls = form.fnGetAllControls();
        var LpCount = 0;
        var strContHtml = '';
        var strAllControls = '<option value="None">None</option>';
        if ((controls != null || controls != undefined) && controls.length > 0) {
            $.each(controls, function () {
                strAllControls += '<option value="' + this.userDefinedName + '">' + this.userDefinedName + '</option>'
            });
        }
        $.each(databindObj.cmdParams, function () {
            strContHtml = '<div id="LpContentText_' + LpCount + '" class="LpContentRow">'
                                + '<div class="LpContentText">'
                                    + '<a id="Lptext_' + LpCount + '" type="Parameter">' + this.name + '</a>'
                                + '</div>'
                                + '<div class="LpContentValue">'
                                    + '<select id="ddlLPValueType_' + LpCount + '"   class="txtLpOption" onkeydown=\"return enterKeyFilter(event);\">'
                                        + '<option value="Constant">Constant</option>'
                                        + '<option value="Control">Control</option>'
                                    + '</select>'
                                + '</div>'
                                + '<div class="LpContentValue">'
                                    + '<select id="ddlLpControl_' + LpCount + '"   class="txtLpOption" onkeydown=\"return enterKeyFilter(event);\">'
                                        + strAllControls
                                    + '</select>'
                                + '</div>'
                                + '<div class="LpContentValue">'
                                    + '<input id="LpConstantValue_' + LpCount + '" type="text"  class="txtLpOption" onkeydown=\"return enterKeyFilter(event);\"/>'
                                + '</div>'
                            + '</div>'
                            + '<div style="clear: both;"></div>';
                            

                $('#LPContainerDiv').append(strContHtml);
                $('#ddlLPValueType_' + LpCount).val(this.type);

                if(this.type == "Constant"){
                    $('#LpConstantValue_' + LpCount).val(this.val);
                    $('#ddlLpControl_' + LpCount).attr('disabled', 'disabled');
                    $('#LpConstantValue_' + LpCount).removeAttr('disabled');
                } 
                else {
                    $('#ddlLpControl_' + LpCount).val(this.val);
                    $('#LpConstantValue_' + LpCount).attr('disabled', 'disabled');
                    $('#ddlLpControl_' + LpCount).removeAttr('disabled');
               }
                LpCount = LpCount + 1;
        });
        return LpCount;
    }

    function _setDefaultLinkParameters(inputParams, databindObj){
        var databindingObjParams = new DatabindingObjParams();
        if(inputParams && inputParams.length > 0){
            $.each(inputParams, function(){
                databindingObjParams = new DatabindingObjParams();
                databindingObjParams.id = databindObj.id;
                databindingObjParams.name = this;
                databindingObjParams.type = "Constant";
                databindingObjParams.val = "";
                databindObj.fnAddCmdParams(databindingObjParams);
            });
        }
    }

    function _saveLinkParameters(databindObj) {
        databindObj.cmdParams = [];
        var databindingObjParams = new DatabindingObjParams();
        jQuery.each($('#LPContainerDiv')[0].children, function () {
            if (this.className == "LpContentRow") {
                databindingObjParams = new DatabindingObjParams();
                databindingObjParams.id = databindObj.id;
                var innerJson = "";
                jQuery.each(this.children, function () {
                    jQuery.each(this.children, function () {
                        switch (this.type) {
                            case "text":
                                if (!this.disabled) {
                                    databindingObjParams.val = this.value;
                                }
                                break;
                            case "select-one":
                                if (this.id.split('_')[0] == 'ddlLPValueType') {
                                    databindingObjParams.type = this.value;
                                }
                                else {
                                    if (!this.disabled) {
                                        databindingObjParams.val = this.value;
                                    }
                                }
                                break;
                            case "Parameter":
                                if (!this.disabled) {
                                    databindingObjParams.name = this.innerHTML;
                                }
                                break;
                        }
                    });
                });
                databindObj.fnAddCmdParams(databindingObjParams);
            }
        });
    }

    //Tanika*Change

    function _saveDbBindingObject(cntrl, databindObj) {
        if (databindObj.id == undefined || databindObj.id == null || databindObj.id == "" || databindObj.id == "-1") {
            showMessage('Please select the Data Object', DialogType.Error);
            return;
        }
        
        var errors = [];
        switch (cntrl.type) {
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                if(cntrl.labelDataObjs != null && cntrl.labelDataObjs != undefined && parseInt(cntrl.labelDataObjs.length, 10) > 0){
                    var i = 1;
                    $.each(cntrl.labelDataObjs, function(){
                        if(i <= 5 && this.para === i + "%")
                        {
                            this.selectedCol = $('[id$=Db_Dp_Para' + i + '_Columns]').val();
                            i = i +1;
                        }
                    });
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                if(parseInt($('[id$=ddlCheckboxCmd_CheckedValue]').val().trim(), 10) <= 0){
                    showMessage('Please select the Value', DialogType.Error);
                    return;
                }
                cntrl.checkedVal = $('[id$=ddlCheckboxCmd_CheckedValue]').val().trim();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
            case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                if(parseInt($('[id$=ddlSliderCmd_Val]').val().trim(), 10) <= 0){
                    showMessage('Please select the Value', DialogType.Error);
                    return;
                }
                cntrl.value = $('[id$=ddlSliderCmd_Val]').val();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                if(parseInt($('[id$=ddlCylinder_Guage_Value]').val().trim(), 10) <= 0){
                    errors.push('Please select the Value');
                }
                if(parseInt($('[id$=ddlCylinder_Guage_MaxVal]').val().trim(), 10) <= 0){
                    errors.push('Please select the maximum Value');
                }
                if($('[id$=ddlCylinder_Guage_Value]').val().trim() != "-1" && $('[id$=ddlCylinder_Guage_MaxVal]').val().trim() != "-1"){
                    if($('[id$=ddlCylinder_Guage_MaxVal]').val().trim() == $('[id$=ddlCylinder_Guage_Value]').val().trim()){
                        errors.push('Value and maximum Value of Cylinder Guage cannot be same');
                    }
                }

                 if(errors.length > 0){
                    showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
                    return;
                 }
                cntrl.value = $('[id$=ddlCylinder_Guage_Value]').val();
                cntrl.maxVal = $('[id$=ddlCylinder_Guage_MaxVal]').val();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                if(parseInt($('[id$=ddlCtrlCmd_DisplayTxt]').val().trim(), 10) <= 0){
                    showMessage('Please select Display Text', DialogType.Error);
                    return;
                }
                cntrl.displayText = $('[id$=ddlCtrlCmd_DisplayTxt]').val().trim();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                // cntrl.displayText = $('[id$=ddlCtrlCmd_DisplayTxt]').val().trim();
                // mFicientIde.HiddenFieldControlHelper.processDatabindingObjectSaved();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                if(parseInt($('[id$=ddlDbCommand_DisplayTxt]').val().trim(), 10) <= 0)
                    errors.push('Please select Display Text');
                if(parseInt($('[id$=ddlDbCommand_DisplayVal]').val().trim(), 10) <= 0)
                    errors.push('Please select Display Value');

                 if(errors.length > 0){
                    showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
                    return;
                 }
                cntrl.optionText = $('[id$=ddlDbCommand_DisplayTxt]').val().trim();
                cntrl.optionValue = $('[id$=ddlDbCommand_DisplayVal]').val().trim();    
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                // try{
                    // _listDataBindingHelper.processSaveDatabindingDtls(cntrl);
                // }
                // catch (error) {
                     // if (error instanceof MfError) {
                         // showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                     // } else {
                         // console.log(error);
                     // }
                     // return;
                 // }
            break;   
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                //TOCHANGE MOHAN
                // try {
                    // _editableListHelper.processSaveMappedDataForDb(cntrl);
                // }
                // catch (error) {
                    // if (error instanceof MfError) {
                        // showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                    // } else {
                        // console.log(error);
                    // }
                    // return;
                // }//TOCHANGE MOHAN
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                if(parseInt($('[id$=ddlPieChart_Title]').val().trim(), 10) <= 0)
                    errors.push('Please select Title');
                if(parseInt($('[id$=ddlPieChart_Value]').val().trim(), 10) <= 0)
                    errors.push('Please select Value');
                 if(errors.length > 0){
                    showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
                    return;
                 }
                cntrl.title = $('[id$=ddlPieChart_Title]').val().trim();
                cntrl.value = $('[id$=ddlPieChart_Value]').val().trim();    
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                 if(parseInt($('[id$=ddlBar_LineChart_Xaxis]').val().trim(), 10) <= 0)
                    errors.push('Please select X-axis Value');

                cntrl.yAxisDataObjs = [];
                var seriesObj = new SeriesObj();
                if(parseInt($('[id$=txtYDbLabel1]').val().trim().length, 10) > 0 && $('[id$=ddlChartDbCommand_Value]').val().trim() != "-1"){
                    seriesObj = new SeriesObj();
                    seriesObj.index = "1";
                    seriesObj.label = $('[id$=txtYDbLabel1]').val();
                    seriesObj.value = $('[id$=ddlChartDbCommand_Value]').val();
                    seriesObj.scaleFactor = $('[id$=txtYDbScaleFact1]').val();
                    cntrl.fnAddSeriesObj(seriesObj);
                }

                if (parseInt(cntrl.noOfSeries) > 1) {
                    for (var n = 2; n <= cntrl.noOfSeries; n++) {
                    if(parseInt($('[id$=txtYDbLabel' + n + ']').val().trim().length, 10) > 0 && $('[id$=ddlChartDbCommand_Value' + n + ']').val().trim() != "-1"){
                            seriesObj = new SeriesObj();
                            seriesObj.index = n;
                            seriesObj.label = $('[id$=txtYDbLabel' + n + ']').val();
                            seriesObj.value = $('[id$=ddlChartDbCommand_Value' + n + ']').val();
                            seriesObj.scaleFactor = $('[id$=txtYDbScaleFact' + n + ']').val();

                            cntrl.fnAddSeriesObj(seriesObj);
                        }
                    }
                }

                if(cntrl.yAxisDataObjs.length != cntrl.noOfSeries)
                   errors.push('Please add Y-axis data for the required series');
                if(errors.length > 0){
                   showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
                   return;
                } 

                cntrl.xAxisData = $('[id$=ddlBar_LineChart_Xaxis]').val();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                if(parseInt($('[id$=ddlImageCmd_DisplayTxt]').val().trim(), 10) <= 0){
                    showMessage('Please select image source', DialogType.Error);
                    return;
                }
                if(parseInt($('[id$=ddlImageCmd_DisplayTitle]').val().trim(), 10) <= 0){
                    showMessage('Please select image title', DialogType.Error);
                    return;
                }
                cntrl.imageSource = $('[id$=ddlImageCmd_DisplayTxt]').val().trim();
                cntrl.imageTitle = $('[id$=ddlImageCmd_DisplayTitle]').val().trim();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
                break;
        }

        var isValid = true;
        $.each(databindObj.cmdParams, function (val) {
            if (this.id == databindObj.id)
                isValid = true;
            else
                isvalid = false;
        });

        if (!isValid) {
            databindObj.fnRemoveCmdParams();
        }
        
        switch (cntrl.type){
           case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
               //TOCHANGE MOHAN
               try {
                   _editableListHelper.processSaveMappedDataForDb(cntrl);
               }
               catch (error) {
                   if (error instanceof MfError) {
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                   } else {
                       console.log(error);
                   }
                   return;
               }//TOCHANGE MOHAN
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
               
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
               try{
                   _biTableDataBindingHelper.processSaveMappedDataForDb(cntrl);
               }
               catch (error) {
                    if (error instanceof MfError) {
                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                    } else {
                        console.log(error);
                    }
                    return;
               }
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
               try{
                   _listDataBindingHelper.processSaveMappedData(cntrl);
               }
               catch (error) {
                    if (error instanceof MfError) {
                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                    } else {
                        console.log(error);
                    }
                    return;
                }
           break;
           case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
              try{
                  _locationDatabindingHelper.saveMappedDataInControl(
                      cntrl,
                      MF_IDE_CONSTANTS.ideCommandObjectTypes.db
                      );
              }
              catch (error) {
                   if (error instanceof MfError) {
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                   } else {
                       console.log(error);
                   }
                   return;
              }
           break;
           case MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
               try {
                   _drilldownPieDatabindingHelper.saveMappedDataInControl(
                       cntrl,
                       MF_IDE_CONSTANTS.ideCommandObjectTypes.db)
               }
               catch (error) {
                   if (error instanceof MfError) {
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                   } else {
                       console.log(error);
                   }
                   return;
               }
            break;   
        }
        
        if($('[id$=chk_DbIgnoreCache]').is(':checked'))  databindObj.fnSetCacheIgnoreValue(true);
        else  databindObj.fnSetCacheIgnoreValue(false);
        cntrl.databindObjs = [];
        var objDatabind = new DatabindingObj(databindObj.id, [], databindObj.index, databindObj.bindObjType, databindObj.name, databindObj.dsName, databindObj.tempUiIndex, databindObj.usrName, databindObj.pwd);
        
        objDatabind.fnSetCacheIgnoreValue(databindObj.ignoreCache);
        if((databindObj.cmdParams != null || databindObj.cmdParams != undefined) && databindObj.cmdParams.length > 0){
            $.each(databindObj.cmdParams, function(){
                var objCmdParam = new DatabindingObjParams(this.id, this.name, this.type, this.val);
                objDatabind.fnAddCmdParams(objCmdParam);
            });
        }
        
        cntrl.fnAddDatabindObj(objDatabind);
        
        switch (cntrl.type){
           case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                _editableListHelper.setDataInPropSheetHtml(cntrl);
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                cntrl.displayText = $('[id$=ddlCtrlCmd_DisplayTxt]').val().trim();
                mFicientIde.HiddenFieldControlHelper.processDatabindingObjectSaved();
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
               _biTableDataBindingHelper.setDataInPropSheetHtml(cntrl);
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
               _listDataBindingHelper.setDataInPropSheetHtml(cntrl);
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
               _locationDatabindingHelper.setDataInPropSheetHtml(cntrl);
            break; 
            case MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
                _drilldownPieDatabindingHelper.setBindingDtlsInPropSheetHtml(cntrl);
            break;    
        }
        
        _clearDbBindingDropdown("<option value='-1'>Select Column</option>");
        $('[id$=ddlCtrlDbCommand]').html('');
        $('#SubProcCtrlDbCmd').dialog('close');

        return objDatabind.name;
    }
    //Tanika*Change
    function _saveWSBindingObject(cntrl, databindObj, wsType) {
        if (databindObj.id == undefined || databindObj.id == null || databindObj.id == "" || databindObj.id == "-1") {
            showMessage('Please select the Data Object', DialogType.Error);
            return;
        }
        
        var errors = [];
        switch (cntrl.type) {
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                if(cntrl.labelDataObjs != null && cntrl.labelDataObjs != undefined && parseInt(cntrl.labelDataObjs.length, 10) > 0){
                    var i = 1;
                    $.each(cntrl.labelDataObjs, function(){
                        if(i <= 5 && this.para === i + "%")
                        {
                            this.selectedCol = $('[id$=WS_Dp_Para' + i + '_Columns]').val();
                            i = i +1;
                        }
                    });
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                if(parseInt($('[id$=ddlCheckboxWsCommand_CheckedVal]').val().trim(), 10) <= 0){
                    showMessage('Please select the Value', DialogType.Error);
                    return;
                }
                cntrl.checkedVal = $('[id$=ddlCheckboxWsCommand_CheckedVal]').val().trim();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
            case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                if(parseInt($('[id$=ddlSliderWsCommand_Val]').val().trim(), 10) <= 0){
                    showMessage('Please select the Value', DialogType.Error);
                    return;
                }
                cntrl.value = $('[id$=ddlSliderWsCommand_Val]').val().trim();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                if(parseInt($('[id$=ddl_WS_Cylinder_Guage_Value]').val().trim(), 10) <= 0){
                    errors.push('Please select the Value');
                }
                if(parseInt($('[id$=ddl_WS_Cylinder_Guage_MaxVal]').val().trim(), 10) <= 0){
                    errors.push('Please select the maximum Value');
                }
                if($('[id$=ddl_WS_Cylinder_Guage_Value]').val().trim() != "-1" && $('[id$=ddl_WS_Cylinder_Guage_MaxVal]').val().trim() != "-1"){
                    if($('[id$=ddl_WS_Cylinder_Guage_Value]').val().trim() == $('[id$=ddl_WS_Cylinder_Guage_MaxVal]').val().trim()){
                        errors.push('Value and maximum Value of Cylinder Guage cannot be same');
                    }
                }

                 if(errors.length > 0){
                    showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
                    return;
                 }
                cntrl.value = $('[id$=ddl_WS_Cylinder_Guage_Value]').val();
                cntrl.maxVal = $('[id$=ddl_WS_Cylinder_Guage_MaxVal]').val();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                if(parseInt($('[id$=ddlCtrlWsCommand_Displaytxt]').val().trim(), 10) <= 0){
                    showMessage('Please select Display Text', DialogType.Error);
                    return;
                }
                cntrl.displayText = $('[id$=ddlCtrlWsCommand_Displaytxt]').val().trim();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                //cntrl.displayText = $('[id$=ddlCtrlWsCommand_Displaytxt]').val().trim();
               // mFicientIde.HiddenFieldControlHelper.processDatabindingObjectSaved();
            break;    
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                if(parseInt($('[id$=ddlWsCommand_Displaytxt]').val().trim(), 10) <= 0)
                    errors.push('Please select Display Text');
                if(parseInt($('[id$=ddlWsCommand_DisplayVal]').val().trim(), 10) <= 0)
                    errors.push('Please select Display Value');

                 if(errors.length > 0){
                    showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
                    return;
                 }
                cntrl.optionText = $('[id$=ddlWsCommand_Displaytxt]').val().trim();
                cntrl.optionValue = $('[id$=ddlWsCommand_DisplayVal]').val().trim();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                //MOHAN
                // try{
                    // _listDataBindingHelper.processSaveDatabindingDtls(cntrl);
                // }
                // catch (error) {
                     // if (error instanceof MfError) {
                         // showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                     // } else {
                         // console.log(error);
                     // }
                     // return;
                 // }
                 //MOHAN
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                // try {
                    // _editableListHelper.processSaveMappedDataForWebServ(cntrl, wsType);
                // }
                // catch (error) {
                    // if (error instanceof MfError) {
                        // showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
                    // } else {
                        // console.log(error);
                    // }
                    // return;
                // }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                if(parseInt($('[id$=ddlWsPieChart_Title]').val().trim(), 10) <= 0)
                    errors.push('Please select Title');
                if(parseInt($('[id$=ddlWsPieChart_Value]').val().trim(), 10) <= 0)
                    errors.push('Please select Value');
                 if(errors.length > 0){
                    showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
                    return;
                 }
                cntrl.title = $('[id$=ddlWsPieChart_Title]').val().trim();
                cntrl.value = $('[id$=ddlWsPieChart_Value]').val().trim();    
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                cntrl.yAxisDataObjs = [];
                 if(parseInt($('[id$=ddlWsBar_LineChart_Xaxis]').val().trim(), 10) <= 0)
                    errors.push('Please select X-axis Value');

                var seriesObj = new SeriesObj();
                if(parseInt($('[id$=txtYWSLabel1]').val().trim().length, 10) > 0 && $('[id$=ddlChartWSCommand_Value]').val().trim() != "-1"){
                    seriesObj = new SeriesObj();
                    seriesObj.index = "1";
                    seriesObj.label = $('[id$=txtYWSLabel1]').val();
                    seriesObj.value = $('[id$=ddlChartWSCommand_Value]').val();
                    seriesObj.scaleFactor = $('[id$=txtYWSScaleFact1]').val();
                    cntrl.fnAddSeriesObj(seriesObj);
                }

                if (parseInt(cntrl.noOfSeries) > 1) {
                    for (var n = 2; n <= cntrl.noOfSeries; n++) {
                    if(parseInt($('[id$=txtYWSLabel' + n + ']').val().trim().length, 10) > 0 && $('[id$=ddlChartWSCommand_Value' + n + ']').val().trim() != "-1"){
                            seriesObj = new SeriesObj();
                            seriesObj.index = n;
                            seriesObj.label = $('[id$=txtYWSLabel' + n + ']').val();
                            seriesObj.value = $('[id$=ddlChartWSCommand_Value' + n + ']').val();
                            seriesObj.scaleFactor = $('[id$=txtYWSScaleFact' + n + ']').val();

                            cntrl.fnAddSeriesObj(seriesObj);
                        }
                    }
                }

                if(cntrl.yAxisDataObjs.length != cntrl.noOfSeries)
                   errors.push('Please add Y-axis data for the required series');
                if(errors.length > 0){
                   showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
                   return;
                }
                cntrl.xAxisData = $('[id$=ddlWsBar_LineChart_Xaxis]').val();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                if(parseInt($('[id$=ddlImageWsCommand_Displaytxt]').val().trim(), 10) <= 0){
                    showMessage('Please select image source', DialogType.Error);
                    return;
                }
                if(parseInt($('[id$=ddlImageWsCommand_DisplayTitle]').val().trim(), 10) <= 0){
                    showMessage('Please select image title', DialogType.Error);
                    return;
                }
                cntrl.imageSource = $('[id$=ddlImageWsCommand_Displaytxt]').val().trim();
                cntrl.imageTitle = $('[id$=ddlImageWsCommand_DisplayTitle]').val().trim();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
                break;
        }

        var isValid = true;
        $.each(databindObj.cmdParams, function (val) {
            if (this.id == databindObj.id)
                isValid = true;
            else
                isvalid = false;
        });

        if (!isValid) {
            databindObj.fnRemoveCmdParams();
        }
        
        switch (cntrl.type){
           case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
               //TOCHANGE MOHAN
               try {
                   _editableListHelper.processSaveMappedDataForWebServ(cntrl);
               }
               catch (error) {
                   if (error instanceof MfError) {
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                   } else {
                       console.log(error);
                   }
                   return;
               }//TOCHANGE MOHAN
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
               
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
               try{
                   _biTableDataBindingHelper.processSaveMappedDataForWs(cntrl,wsType);
               }
               catch (error) {
                    if (error instanceof MfError) {
                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                    } else {
                        console.log(error);
                    }
                    return;
               }
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
               try{
                   _listDataBindingHelper.processSaveMappedData(cntrl);
               }
               catch (error) {
                    if (error instanceof MfError) {
                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                    } else {
                        console.log(error);
                    }
                    return;
                }
           break;
           case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
              try{
                  _locationDatabindingHelper.saveMappedDataInControl(
                      cntrl,
                      MF_IDE_CONSTANTS.ideCommandObjectTypes.http
                      );
              }
              catch (error) {
                   if (error instanceof MfError) {
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                   } else {
                       console.log(error);
                   }
                   return;
              }
           break;
           case MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
               try {
                   _drilldownPieDatabindingHelper.saveMappedDataInControl(
                       cntrl,
                       MF_IDE_CONSTANTS.ideCommandObjectTypes.http)
               }
               catch (error) {
                   if (error instanceof MfError) {
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                   } else {
                       console.log(error);
                   }
                   return;
               }
            break;   
        } 
        
        if($('[id$=chk_WSIgnoreCache]').is(':checked'))  databindObj.fnSetCacheIgnoreValue(true);
        else  databindObj.fnSetCacheIgnoreValue(false);
        cntrl.databindObjs = [];
        var objDatabind = new DatabindingObj(databindObj.id, [], databindObj.index, databindObj.bindObjType, databindObj.name, databindObj.dsName, databindObj.tempUiIndex, databindObj.usrName, databindObj.pwd);

        objDatabind.fnSetCacheIgnoreValue(databindObj.ignoreCache);
        if((databindObj.cmdParams != null || databindObj.cmdParams != undefined) && databindObj.cmdParams.length > 0){
            $.each(databindObj.cmdParams, function(){
                var objCmdParam = new DatabindingObjParams(this.id, this.name, this.type, this.val);
                objDatabind.fnAddCmdParams(objCmdParam);
            });
        }
        
        cntrl.fnAddDatabindObj(objDatabind);
        
        switch (cntrl.type){
           case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                _editableListHelper.setDataInPropSheetHtml(cntrl);
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                cntrl.displayText = $('[id$=ddlCtrlWsCommand_Displaytxt]').val().trim();
                mFicientIde.HiddenFieldControlHelper.processDatabindingObjectSaved();
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
               _biTableDataBindingHelper.setDataInPropSheetHtml(cntrl);
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
               _listDataBindingHelper.setDataInPropSheetHtml(cntrl);
            break; 
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
               _locationDatabindingHelper.setDataInPropSheetHtml(cntrl);
            break;
            case MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
                _drilldownPieDatabindingHelper.setBindingDtlsInPropSheetHtml(cntrl);
            break;
        }
       _clearWSBindingDropdown("<option value='-1'>Select Tag</option>");
        $('[id$=ddlCtrlWsCommand]').html('');
        $('#SubProcCtrlWsCmd').dialog('close');

        return objDatabind.name;
    }

    function _saveOdataBindingObject(cntrl, databindObj) {
        if (databindObj.id == undefined || databindObj.id == null || databindObj.id == "" || databindObj.id == "-1") {
            showMessage('Please select the Data Object', DialogType.Error);
            return;
        }
        
        var errors = [];
        switch (cntrl.type) {
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LABEL:
                if(cntrl.labelDataObjs != null && cntrl.labelDataObjs != undefined && parseInt(cntrl.labelDataObjs.length, 10) > 0){
                    var i = 1;
                    $.each(cntrl.labelDataObjs, function(){
                        if(i <= 5 && this.para === i + "%")
                        {
                            this.selectedCol = $('[id$=OData_Dp_Para' + i + '_Columns]').val();
                            i = i +1;
                        }
                    });
                }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CHECKBOX:
                if(parseInt($('[id$=ddlCheckboxODataCmd_CheckedVal]').val().trim(), 10) <= 0){
                    showMessage('Please select the Value', DialogType.Error);
                    return;
                }
                cntrl.checkedVal = $('[id$=ddlCheckboxODataCmd_CheckedVal]').val().trim();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SLIDER:
            case MF_IDE_CONSTANTS.CONTROLS.DATAPOINTCHART:
                if(parseInt($('[id$=ddlSliderODataCmd_Val]').val().trim(), 10) <= 0){
                    showMessage('Please select the Value', DialogType.Error);
                    return;
                }
                cntrl.value = $('[id$=ddlSliderODataCmd_Val]').val().trim();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE:
                if(parseInt($('[id$=ddl_OData_Cylinder_Guage_Value]').val().trim(), 10) <= 0){
                    errors.push('Please select the Value');
                }
                if(parseInt($('[id$=ddl_OData_Cylinder_Guage_MaxVal]').val().trim(), 10) <= 0){
                    errors.push('Please select the maximum Value');
                }
                if($('[id$=ddl_OData_Cylinder_Guage_Value]').val().trim() != "-1" && $('[id$=ddl_OData_Cylinder_Guage_MaxVal]').val().trim() != "-1"){
                    if($('[id$=ddl_OData_Cylinder_Guage_Value]').val().trim() == $('[id$=ddl_OData_Cylinder_Guage_MaxVal]').val().trim()){
                        errors.push('Value and maximum Value of Cylinder Guage cannot be same');
                    }
                }

                 if(errors.length > 0){
                    showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
                    return;
                 }
                cntrl.value = $('[id$=ddl_OData_Cylinder_Guage_Value]').val();
                cntrl.maxVal = $('[id$=ddl_OData_Cylinder_Guage_MaxVal]').val();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TEXTBOX:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TOGGLE:
                if(parseInt($('[id$=ddlCtrlODataCmd_DisplayTxt]').val().trim(), 10) <= 0){
                    showMessage('Please select Display Text', DialogType.Error);
                    return;
                }
                cntrl.displayText = $('[id$=ddlCtrlODataCmd_DisplayTxt]').val().trim();
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                //cntrl.displayText = $('[id$=ddlCtrlODataCmd_DisplayTxt]').val().trim();
                //mFicientIde.HiddenFieldControlHelper.processDatabindingObjectSaved();
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.DROPDOWN:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SELECTORLIST:
                if(parseInt($('[id$=ddlODataCommand_DisplayTxt]').val().trim(), 10) <= 0)
                    errors.push('Please select Display Text');
                if(parseInt($('[id$=ddlODataCommand_DisplayVal]').val().trim(), 10) <= 0)
                    errors.push('Please select Display Value');

                 if(errors.length > 0){
                    showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
                    return;
                 }
                cntrl.optionText = $('[id$=ddlODataCommand_DisplayTxt]').val().trim();
                cntrl.optionValue = $('[id$=ddlODataCommand_DisplayVal]').val().trim();    
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
                //MOHAN
                // try{
                    // _listDataBindingHelper.processSaveDatabindingDtls(cntrl);
                // }
                // catch (error) {
                     // if (error instanceof MfError) {
                         // showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                     // } else {
                         // console.log(error);
                     // }
                     // return;
                 // }
                 //MOHAN
                break;
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                // try {
                    // _editableListHelper.processSaveMappedDataForOdata(cntrl);
                // }
                // catch (error) {
                    // if (error instanceof MfError) {
                        // showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                    // } else {
                        // console.log(error);
                    // }
                    // return;
                // }
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.PIECHART:
                if(parseInt($('[id$=ddlODataPieChart_Title]').val().trim(), 10) <= 0)
                    errors.push('Please select Title');
                if(parseInt($('[id$=ddlODataPieChart_Value]').val().trim(), 10) <= 0)
                    errors.push('Please select Value');
                 if(errors.length > 0){
                    showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
                    return;
                 }
                cntrl.title = $('[id$=ddlODataPieChart_Title]').val().trim();
                cntrl.value = $('[id$=ddlODataPieChart_Value]').val().trim();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARGRAPH:
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH:
                 if(parseInt($('[id$=ddlODataBar_LineChart_Xaxis]').val().trim(), 10) <= 0)
                    errors.push('Please select X-axis Value');

                cntrl.yAxisDataObjs = [];
                if(parseInt($('[id$=txtYODataLabel1]').val().trim().length, 10) > 0 && $('[id$=ddlChartODataCommand_Value]').val().trim() != "-1"){
                    seriesObj = new SeriesObj();
                    seriesObj.index = "1";
                    seriesObj.label = $('[id$=txtYODataLabel1]').val();
                    seriesObj.value = $('[id$=ddlChartODataCommand_Value]').val();
                    seriesObj.scaleFactor = $('[id$=txtYODataScaleFact1]').val();
                    cntrl.fnAddSeriesObj(seriesObj);
                }

                if (parseInt(cntrl.noOfSeries) > 1) {
                    for (var n = 2; n <= cntrl.noOfSeries; n++) {
                    if(parseInt($('[id$=txtYODataLabel' + n + ']').val().trim().length, 10) > 0 && $('[id$=ddlChartODataCommand_Value' + n + ']').val().trim() != "-1"){
                            seriesObj = new SeriesObj();
                            seriesObj.index = n;
                            seriesObj.label = $('[id$=txtYODataLabel' + n + ']').val();
                            seriesObj.value = $('[id$=ddlChartODataCommand_Value' + n + ']').val();
                            seriesObj.scaleFactor = $('[id$=txtYODataScaleFact' + n + ']').val();

                            cntrl.fnAddSeriesObj(seriesObj);
                        }
                    }

                }

                if(cntrl.yAxisDataObjs.length != cntrl.noOfSeries)
                   errors.push('Please add Y-axis data for the required series');
                if(errors.length > 0){
                   showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(errors), DialogType.Error);
                   return;
                }
                cntrl.xAxisData = $('[id$=ddlODataBar_LineChart_Xaxis]').val(); 
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.IMAGE:
                if(parseInt($('[id$=ddlImageODataCmd_DisplayTxt]').val().trim(), 10) <= 0){
                    showMessage('Please select image source', DialogType.Error);
                    return;
                }
                if(parseInt($('[id$=ddlImageODataCmd_DisplayTitle]').val().trim(), 10) <= 0){
                    showMessage('Please select image title', DialogType.Error);
                    return;
                }
                cntrl.imageSource = $('[id$=ddlImageODataCmd_DisplayTxt]').val().trim();
                cntrl.imageTitle = $('[id$=ddlImageODataCmd_DisplayTitle]').val().trim();
                break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
                break;
        }

        var isValid = true;
        $.each(databindObj.cmdParams, function (val) {
            if (this.id == databindObj.id)
                isValid = true;
            else
                isvalid = false;
        });

        if (!isValid) {
            databindObj.fnRemoveCmdParams();
        }
        
        switch (cntrl.type){
           case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
               //TOCHANGE MOHAN
               try {
                   _editableListHelper.processSaveMappedDataForOdata(cntrl);
               }
               catch (error) {
                   if (error instanceof MfError) {
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                   } else {
                       console.log(error);
                   }
                   return;
               }//TOCHANGE MOHAN
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
               
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
               try{
                   _biTableDataBindingHelper.processSaveMappedDataForOdata(cntrl);
               }
               catch (error) {
                    if (error instanceof MfError) {
                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                    } else {
                        console.log(error);
                    }
                    return;
               }
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
               try{
                   _listDataBindingHelper.processSaveMappedData(cntrl);
               }
               catch (error) {
                    if (error instanceof MfError) {
                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                    } else {
                        console.log(error);
                    }
                    return;
                }
           break; 
           case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
              try{
                  _locationDatabindingHelper.saveMappedDataInControl(
                      cntrl,
                      MF_IDE_CONSTANTS.ideCommandObjectTypes.odata
                      );
              }
              catch (error) {
                   if (error instanceof MfError) {
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                   } else {
                       console.log(error);
                   }
                   return;
              }
           break;
           case MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
              try {
                  _drilldownPieDatabindingHelper.saveMappedDataInControl(
                      cntrl,
                      MF_IDE_CONSTANTS.ideCommandObjectTypes.odata)
              }
              catch (error) {
                  if (error instanceof MfError) {
                      showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                  } else {
                      console.log(error);
                  }
                  return;
              }
           break; 
        }
        
        if($('[id$=chk_ODataIgnoreCache]').is(':checked'))  databindObj.fnSetCacheIgnoreValue(true);
        else  databindObj.fnSetCacheIgnoreValue(false);
        cntrl.databindObjs = [];
        var objDatabind = new DatabindingObj(databindObj.id, [], databindObj.index, databindObj.bindObjType, databindObj.name, databindObj.dsName, databindObj.tempUiIndex, databindObj.usrName, databindObj.pwd);
       
       objDatabind.fnSetCacheIgnoreValue(databindObj.ignoreCache);
        if((databindObj.cmdParams != null || databindObj.cmdParams != undefined) && databindObj.cmdParams.length > 0){
            $.each(databindObj.cmdParams, function(){
                var objCmdParam = new DatabindingObjParams(this.id, this.name, this.type, this.val);
                objDatabind.fnAddCmdParams(objCmdParam);
            });
        }
        
        cntrl.fnAddDatabindObj(objDatabind);
        
        switch (cntrl.type){
           case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.EDITABLE_LIST:
                _editableListHelper.setDataInPropSheetHtml(cntrl);
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.HIDDEN:
                cntrl.displayText = $('[id$=ddlCtrlODataCmd_DisplayTxt]').val().trim();
                mFicientIde.HiddenFieldControlHelper.processDatabindingObjectSaved();
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.TABLE:
               _biTableDataBindingHelper.setDataInPropSheetHtml(cntrl);
            break;
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LIST:
               _listDataBindingHelper.setDataInPropSheetHtml(cntrl);
            break; 
            case mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION:
               _locationDatabindingHelper.setDataInPropSheetHtml(cntrl);
            break;
            case MF_IDE_CONSTANTS.CONTROLS.DRILLDOWN_PIECHART:
                _drilldownPieDatabindingHelper.setBindingDtlsInPropSheetHtml(cntrl);
            break;
        }
        _clearOdataBindingOptionsDropDown("<option value='-1'>Select Value</option>");
        $('[id$=ddlCtrlOdataCmd]').html('');
        $('#SubProcCtrlODataCmd').dialog('close');

        return objDatabind.name;
    }

    function _dbObjectBinding(cntrl, databindObj, dbObjectControl) {
        var options = "";
        var cmd = null;
        var columnDropdownOptions = "";
        var columns = [];
        var inputParams
        $('[id$=btnCtrl_LinkParmeters]').hide();

        var dbCmds = mFicientIde.MF_HELPERS.getDbObjectsByCmdType(
                MF_IDE_CONSTANTS.DBCommandType.SELECT
            );
        options += "<option value='-1'>Select Object</option>";
        if ((dbCmds != null || dbCmds != undefined) && dbCmds.length > 0) {
            $.each(dbCmds, function () {
                options += "<option value='" + this.commandId + "'" +  (databindObj.id == this.commandId ? ' selected' : '') + ">" + this.commandName + "</option>";
            });
        }
        
        if($('[id$=chk_DbIgnoreCache]').is(':checked'))  $('[id$=chk_DbIgnoreCache]')[0].checked = false;

        $('[id$=ddlCtrlDbCommand]').html(options);
        _setDbBindingsOptionsDisplay(cntrl);

        if(databindObj.id != null || databindObj.id != undefined || databindObj.id != "" || databindObj.id != "-1"){
            columnDropdownOptions = "";
             $.each(dbCmds, function () {
                    if (this.commandId == databindObj.id) {
                        cmd = this;
                        columns = this.columns.split(',');
                    }
                });
                columnDropdownOptions += "<option value='-1' selected>Select Column</option>";
                if ((columns != null || columns != undefined) && columns.length > 0) {
                    $.each(columns, function () {
                        if (this.trim().length > 0) {
                            columnDropdownOptions += "<option value='" + this + "'>" + this + "</option>";
                        }
                    });
                }
            _setDbBindingOptionsDropDown(cntrl, columnDropdownOptions, true);
            if ((cmd != null || cmd != undefined) && databindObj.id != "-1") {
                    inputParams = mFicientIde.MF_HELPERS.getInputParamsForCmdObject(cmd.commandId, MF_IDE_CONSTANTS.ideCommandObjectTypes.db);
                    if (inputParams.length > 0) {
                        _bindDataLinkParameters(cmd, inputParams, "btnCtrl_LinkParmeters", 'Database Object', databindObj);
                    }
                    else
                        $('[id$=btnCtrl_LinkParmeters]').hide();
                }
            }

            if(databindObj.ignoreCache)
                $('[id$=chk_DbIgnoreCache]')[0].checked = true;
            else
                $('[id$=chk_DbIgnoreCache]')[0].checked = false;

            $('[id$=ddlCtrlDbCommand]').unbind('change');
            $('[id$=ddlCtrlDbCommand]').bind('change', function (evnt) {
                _clearDbBindingDropdown("<option value='-1' selected>Select Column</option>");
                 columnDropdownOptions = "";
                if (evnt.target.value != null || evnt.target.value != undefined) {
                    columns = [];
                    databindObj.fnRemoveCmdParams();
                    databindObj.cmdParams = [];
                    $.each(dbCmds, function () {
                        if (this.commandId == evnt.target.value) {
                            cmd = this;
                            columns = this.columns.split(',');
                        }
                    });
                    columnDropdownOptions += "<option value='-1' selected>Select Column</option>";
                    if ((columns != null || columns != undefined) && columns.length > 0) {
                        $.each(columns, function () {
                            if (this.trim().length > 0) {
                                columnDropdownOptions += "<option value='" + this + "'>" + this + "</option>";
                            }
                        });
                    }
                    _setDbBindingOptionsDropDown(cntrl, columnDropdownOptions, false);
                    //Link Parameters
                    if ((cmd != null || cmd != undefined) && evnt.target.value != "-1") {
                        inputParams = mFicientIde.MF_HELPERS.getInputParamsForCmdObject(cmd.commandId, MF_IDE_CONSTANTS.ideCommandObjectTypes.db);
                        if (inputParams.length > 0) {
                             databindObj.id = evnt.target.value;
                             databindObj.name = cmd.commandName;
                            _setDefaultLinkParameters(inputParams, databindObj);
                            _bindDataLinkParameters(cmd, inputParams, "btnCtrl_LinkParmeters", 'Database Object', databindObj);
                        }
                        else
                            $('[id$=btnCtrl_LinkParmeters]').hide();
                    }
                }

                databindObj.id = evnt.target.value;
                databindObj.name = cmd.commandName;
            });

            $('[id$=lnkShowDbCmdDetails]').unbind('click');
            $('[id$=lnkShowDbCmdDetails]').bind('click', function () {
                _showDbCmdDetails(cmd);
                return false;
            });

            $('[id$=btnSaveCtrlDbProp]').unbind('click');
            $('[id$=btnSaveCtrlDbProp]').bind('click', function () {
                var objName =_saveDbBindingObject(cntrl, databindObj);
                clearChartSeriesObjectParams();
               if(objName != undefined && objName != null && objName.length > 0) $(dbObjectControl).val(objName);
               else $(dbObjectControl).val("Select Object");
                return false;
            });

            $('[id$=btnCancelCtrlDbProp]').unbind('click');
            $('[id$=btnCancelCtrlDbProp]').bind('click', function () {
                databindObj.fnRemoveCmdParams();
                clearChartSeriesObjectParams();
//               if(databindObj.name.length > 0) $(dbObjectControl).val(databindObj.name);
//               else $(dbObjectControl).val("Select Object");
                return false;
            });
    }
    function _oDataObjectBinding(cntrl, databindObj, oDataObjectControl) {
        var options = "";
        var cmd = null;
        var valueDropdownOptions = "";
        var values = [];
        var inputParams;
        $('[id$=btnODataCtrl_LinkParmeters]').hide();

        var oDataCmds = mFicientIde.MF_HELPERS.getAllOdataObjects();
        options += "<option value='-1'>Select Object</option>";
        if ((oDataCmds != null || oDataCmds != undefined) && oDataCmds.length > 0) {
            $.each(oDataCmds, function () {
                options += "<option value='" + this.commandId + "'" +  (databindObj.id == this.commandId ? ' selected' : '') + ">" + this.commandName + "</option>";
            });
        }

        if($('[id$=chk_ODataIgnoreCache]').is(':checked'))  $('[id$=chk_ODataIgnoreCache]')[0].checked = false;

        $('[id$=ddlCtrlOdataCmd]').html(options);
        _setODataBindingsOptionsDisplay(cntrl);

        if (databindObj.id != null || databindObj.id != undefined || databindObj.id != "" || databindObj.id != "-1") {
            tagDropdownOptions = "";
                $.each(oDataCmds, function () {
                    if (this.commandId == databindObj.id) {
                        cmd = this;
                        values = mFicientIde.MF_HELPERS.getInputParamsForCmdObject(cmd.commandId, MF_IDE_CONSTANTS.ideCommandObjectTypes.odata);
                    }
                });

                valueDropdownOptions += "<option value='-1'>Select Value</option>";
                if ((values != null || values != undefined) && values.length > 0) {
                    $.each(values, function () {
                        if (this.trim().length > 0) {
                            valueDropdownOptions += "<option value='" + this + "'>" + this + "</option>";
                        }
                    });
                }

            _setODataBindingOptionsDropDown(cntrl, valueDropdownOptions, true);
            if ((cmd != null || cmd != undefined) && databindObj.id != "-1") {
                inputParams = mFicientIde.MF_HELPERS.getInputParamsForCmdObject(cmd.commandId, MF_IDE_CONSTANTS.ideCommandObjectTypes.odata);
                if (inputParams.length > 0) {
                    _bindDataLinkParameters(cmd, inputParams, "btnODataCtrl_LinkParmeters", 'OData Object', databindObj);
                }
                else
                    $('[id$=btnODataCtrl_LinkParmeters]').hide();
            }

            if(databindObj.ignoreCache)
                $('[id$=chk_ODataIgnoreCache]')[0].checked = true;
            else
                $('[id$=chk_ODataIgnoreCache]')[0].checked = false;
        }

        $('[id$=ddlCtrlOdataCmd]').unbind('change');
        $('[id$=ddlCtrlOdataCmd]').bind('change', function (evnt) {
            databindObj.fnRemoveCmdParams();
            databindObj.cmdParams = [];
            _clearOdataBindingOptionsDropDown("<option value='-1'>Select Value</option>");
            valueDropdownOptions = "";
            values = [];
            if (evnt.target.value != null || evnt.target.value != undefined) {
                $.each(oDataCmds, function () {
                    if (this.commandId == evnt.target.value) {
                        cmd = this;
                        values = mFicientIde.MF_HELPERS.getInputParamsForCmdObject(cmd.commandId, MF_IDE_CONSTANTS.ideCommandObjectTypes.odata);
                    }
                });

                valueDropdownOptions += "<option value='-1' selected>Select Value</option>";
                if ((values != null || values != undefined) && values.length > 0) {
                    $.each(values, function () {
                        if (this.trim().length > 0) {
                            valueDropdownOptions += "<option value='" + this + "'>" + this + "</option>";
                        }
                    });
                }

                _setODataBindingOptionsDropDown(cntrl, valueDropdownOptions, false);

                if ((cmd != null || cmd != undefined) && evnt.target.value != "-1") {
                    var inputParams = mFicientIde.MF_HELPERS.getInputParamsForCmdObject(cmd.commandId, MF_IDE_CONSTANTS.ideCommandObjectTypes.odata);
                    if (inputParams.length > 0) {
                         databindObj.id = evnt.target.value;
                         databindObj.name = cmd.commandName;
                        _setDefaultLinkParameters(inputParams, databindObj);
                        _bindDataLinkParameters(cmd, inputParams, "btnODataCtrl_LinkParmeters", 'OData Object', databindObj);
                    }
                    else
                        $('[id$=btnODataCtrl_LinkParmeters]').hide();
                }
            }

            databindObj.id = evnt.target.value;
            databindObj.name = cmd.commandName;
        });

        //Command Details
        $('[id$=lnkShowOdataCmd]').unbind('click');
        $('[id$=lnkShowOdataCmd]').bind('click', function () {
            _showODataCmdDetails(cmd);
            return false;
        });

        $('[id$=btnODataCmdSave]').unbind('click');
        $('[id$=btnODataCmdSave]').bind('click', function () {
            var objName = _saveOdataBindingObject(cntrl, databindObj);
            clearChartSeriesObjectParams();
            if(objName != undefined && objName != null && objName.length > 0)  $(oDataObjectControl).val(objName);
            else $(oDataObjectControl).val("Select Object");
            return false;
        });

        $('[id$=btnODataCmdCancel]').unbind('click');
        $('[id$=btnODataCmdCancel]').bind('click', function () {
            databindObj.fnRemoveCmdParams();
            clearChartSeriesObjectParams();
//            if(databindObj.name.length > 0) $(oDataObjectControl).val(databindObj.name);
//            else $(oDataObjectControl).val("Select Object");
            return false;
        });
    }
    function _webserviceObjectBinding(cntrl, databindObj, wsObjectControl) {
        var options = "";
        var cmd = null;
        var webserviceType;
        var tagDropdownOptions = "";
        var tags = [];
        var inputParams;
        $('[id$=btnCtrlWs_LinkParmeters]').hide();
        
        options += "<option value='-1'>Select Object</option>";
        var wsCmds = mFicientIde.MF_HELPERS.getAllWsObjects();
        if ((wsCmds != null || wsCmds != undefined) && wsCmds.length > 0) {
            $.each(wsCmds, function () {
                options += "<option value='" + this.commandId + "'" +  (databindObj.id == this.commandId ? ' selected' : '') + ">" + this.commandName + "</option>";
            });
        }

        if($('[id$=chk_WSIgnoreCache]').is(':checked'))  $('[id$=chk_WSIgnoreCache]')[0].checked = false;

        $('[id$=ddlCtrlWsCommand]').html(options);

        _setWSBindingsOptionsDisplay(cntrl, MF_IDE_CONSTANTS.webServiceType.http);

        if (databindObj.id != null || databindObj.id != undefined || databindObj.id != "" || databindObj.id != "-1") {
            tagDropdownOptions = "";
                $.each(wsCmds, function () {
                    if (this.commandId == databindObj.id) {
                        cmd = this;
                        tags = mFicientIde.MF_HELPERS.getWSTags(cmd.commandId, _getWSType(cmd.webserviceType));
                    }
                });

                if(cmd != null || cmd != undefined) webserviceType = _getWSType(cmd.webserviceType);

                _setWSBindingsOptionsDisplay(cntrl, webserviceType);
                tagDropdownOptions += "<option value='-1'>Select Tag</option>";
                if ((tags != null || tags != undefined) && tags.length > 0) {
                    $.each(tags, function () {
                        if (this.trim().length > 0) {
                            tagDropdownOptions += "<option value='" + this + "'>" + this + "</option>";
                        }
                    });
                }
                _setWSBindingOptionsDropDown(cntrl, tagDropdownOptions, webserviceType, true);
                if ((cmd != null || cmd != undefined) && databindObj.id != "-1") {
                    var inputParams = mFicientIde.MF_HELPERS.getInputParamsForCmdObject(cmd.commandId, _getWSCmdObjectType(cmd.webserviceType));
                    if (inputParams.length > 0) {
                        _bindDataLinkParameters(cmd, inputParams, "btnCtrlWs_LinkParmeters", 'Webservice Object', databindObj);
                    }
                    else
                        $('[id$=btnCtrlWs_LinkParmeters]').hide();
                }
            }

            if(databindObj.ignoreCache)
                $('[id$=chk_WSIgnoreCache]')[0].checked = true;
            else
                $('[id$=chk_WSIgnoreCache]')[0].checked = false;

        $('[id$=ddlCtrlWsCommand]').unbind('change');
        $('[id$=ddlCtrlWsCommand]').bind('change', function (evnt) {
            _clearWSBindingDropdown("<option value='-1'>Select Tag</option>");
            tagDropdownOptions = "";
            tags = [];
            databindObj.fnRemoveCmdParams();
            databindObj.cmdParams = [];
            if (evnt.target.value != null || evnt.target.value != undefined) {
                $.each(wsCmds, function () {
                    if (this.commandId == evnt.target.value) {
                        cmd = this;
                        tags = mFicientIde.MF_HELPERS.getWSTags(cmd.commandId, _getWSType(cmd.webserviceType));
                    }
                });

                webserviceType = _getWSType(cmd.webserviceType);

                _setWSBindingsOptionsDisplay(cntrl, webserviceType);
                tagDropdownOptions += "<option value='-1' selected>Select Tag</option>";
                if ((tags != null || tags != undefined) && tags.length > 0) {
                    $.each(tags, function () {
                        if (this.trim().length > 0) {
                            tagDropdownOptions += "<option value='" + this + "'>" + this + "</option>";
                        }
                    });
                }
                _setWSBindingOptionsDropDown(cntrl, tagDropdownOptions, webserviceType, false);

                //Link Parameters
                if ((cmd != null || cmd != undefined) && evnt.target.value != "-1") {
                    inputParams = mFicientIde.MF_HELPERS.getInputParamsForCmdObject(cmd.commandId, _getWSCmdObjectType(cmd.webserviceType));
                    if (inputParams.length > 0) {
                         databindObj.id = evnt.target.value;
                         databindObj.name = cmd.commandName;
                        _setDefaultLinkParameters(inputParams, databindObj);
                        _bindDataLinkParameters(cmd, inputParams, "btnCtrlWs_LinkParmeters", 'Webservice Object', databindObj);
                    }
                    else
                        $('[id$=btnCtrlWs_LinkParmeters]').hide();
                }
            }

            databindObj.id = evnt.target.value;
            databindObj.name = cmd.commandName;
        });

        //Command Details
        $('[id$=lnkShowWsCmdDetails]').unbind('click');
        $('[id$=lnkShowWsCmdDetails]').bind('click', function () {
            _showWSCmdDetails(cmd);
            return false;
        });

        $('[id$=btnWsCmdSave]').unbind('click');
        $('[id$=btnWsCmdSave]').bind('click', function () {
           var objName = _saveWSBindingObject(cntrl, databindObj, webserviceType);
            //_clearLabelObjectsParams('WS');
            clearChartSeriesObjectParams();
            if(objName != undefined && objName != null && objName.length > 0) $(wsObjectControl).val(objName);
            else $(wsObjectControl).val("Select Object");
            return false;
        });

        $('[id$=btnWsCmdCancel]').unbind('click');
        $('[id$=btnWsCmdCancel]').bind('click', function () {
            databindObj.fnRemoveCmdParams();
            clearChartSeriesObjectParams();
//            if(databindObj.name.length > 0) $(wsObjectControl).val(databindObj.name);
//            else $(wsObjectControl).val("Select Object");
            return false;
        });
    }
    
    
    
    function _controlsObjectBinding(control,cntrlType, databindObj, objectType,labelText) {
        var blnIsEdit = _globalVarHelpers.getIsEditGlobalData();
        _setLabelOfDdlDataObject(labelText,objectType);
        _setInitialStateOfHtml(cntrlType,objectType);
        
        var chkIgnoreCache = _getHtmlControls.chkIgnoreCache();
        if(chkIgnoreCache.is(':checked'))
            $(chkIgnoreCache)[0].checked = false;

        //object type dropdown settings 
        _ddlDatabindingObject.bindObjectsByType(
            objectType,
            true,"-1"
        );
        _ddlDatabindingObject.setEvents(
            objectType,
            cntrlType
        );
        //object type dropdown settings
        
        //_setDbBindingsOptionsDisplay(cntrl);    
            
        //TODO Ideally this method should only do show and hide controls
        //For certain controls it is also forming the html for mapping
        //this is not right.Change is required 
        //show hide divs and html contorls by control type
        _showHideControls.showHideControlsByObjectTypeAndCntrlType(
            objectType,
            cntrlType,
            control
        );
        _imgDatabindInputParameters.showHideByDatabindObj(databindObj);
        _imgDatabindInputParameters.bindEvent(databindObj);
        if(blnIsEdit){
            //TODO this should be split into two
            //first binding options of drop down
            //and then setting the value in html. 
            _setDataInHtml.setData(_globalVarHelpers.getDataOfContainerDiv());
        }
    }
    
    
    
    function clearChartSeriesObjectParams(){ 
        $('[id$=txtYDbLabel1]').val('');
        $('[id$=ddlChartDbCommand_Value]').val('-1');
        $('[id$=txtYDbScaleFact1]').val('');

        for (var n = 2; n <= 5; n++) {
            $('[id$=txtYDbLabel' + n + ']').val('');
            $('[id$=ddlChartDbCommand_Value' + n + ']').val('-1');
            $('[id$=txtYDbScaleFact' + n + ']').val('');
        }

        $('[id$=txtYWSLabel1]').val('');
        $('[id$=ddlChartWSCommand_Value]').val('-1');
        $('[id$=txtYWSScaleFact1]').val('');

        for (var n = 2; n <= 5; n++) {
            $('[id$=txtYWSLabel' + n + ']').val('');
            $('[id$=ddlChartWSCommand_Value' + n + ']').val('-1');
            $('[id$=txtYWSScaleFact' + n + ']').val('');
        }               
                
        $('[id$=txtYODataLabel1]').val('');
        $('[id$=ddlChartODataCommand_Value]').val('-1');
        $('[id$=txtYODataScaleFact1]').val('');

        for (var n = 2; n <= 5; n++) {
            $('[id$=txtYODataLabel' + n + ']').val('');
            $('[id$=ddlChartODataCommand_Value' + n + ']').val('-1');
            $('[id$=txtYODataScaleFact' + n + ']').val('');
        }
    }

    //Tanika*Change
    function _showODataListBindingProperties(listType, isCountBubble) {
        switch (listType) {
            case "1":
            case "3":
                $('[id$=OData_List_RowId]').show();
                $('[id$=OData_List_Title]').show();
                $('[id$=OData_List_SubTitle]').hide();
                $('[id$=OData_List_Info]').hide();
                $('[id$=OData_List_AddInfo]').hide();
                $('[id$=OData_List_ImageName]').hide();
                $('[id$=OData_List_CountField]').hide();
                break;
            case "2":
                $('[id$=OData_List_RowId]').show();
                $('[id$=OData_List_Title]').show();
                $('[id$=OData_List_SubTitle]').show();
                $('[id$=OData_List_Info]').show();
                $('[id$=OData_List_AddInfo]').show();
                $('[id$=OData_List_ImageName]').hide();
                $('[id$=OData_List_CountField]').hide();
                break;
            case "4":
                $('[id$=OData_List_RowId]').show();
                $('[id$=OData_List_Title]').show();
                $('[id$=OData_List_SubTitle]').hide();
                $('[id$=OData_List_Info]').hide();
                $('[id$=OData_List_AddInfo]').hide();
                $('[id$=OData_List_ImageName]').show();
                $('[id$=OData_List_CountField]').hide();
                break;
            case "5":
                $('[id$=OData_List_RowId]').show();
                $('[id$=OData_List_Title]').show();
                $('[id$=OData_List_SubTitle]').show();
                $('[id$=OData_List_Info]').show();
                $('[id$=OData_List_AddInfo]').show();
                $('[id$=OData_List_ImageName]').show();
                $('[id$=OData_List_CountField]').hide();
                break;
        }
        if (isCountBubble)
            $('[id$=OData_List_CountField]').show();
        else
            $('[id$=OData_List_CountField]').hide();
    }
    function _showWsListBindingProperties(listType, isCountBubble, wsType) {
        switch (listType) {
            case "1":
            case "3":
                $('[id$=List_WsRowId]').show();
                $('[id$=List_WsImage]').show();
                $('[id$=List_WsTitle]').hide();
                $('[id$=List_WsSubtitle]').hide();
                $('[id$=List_WsInfo]').hide();
                $('[id$=List_WsAddInfo]').hide();
                $('[id$=List_WsCountField]').hide();
                break;
            case "2":
                $('[id$=List_WsRowId]').show();
                $('[id$=List_WsImage]').show();
                $('[id$=List_WsTitle]').show();
                $('[id$=List_WsSubtitle]').show();
                $('[id$=List_WsInfo]').show();
                $('[id$=List_WsAddInfo]').hide();
                $('[id$=List_WsCountField]').hide();
                break;
            case "4":
                $('[id$=List_WsRowId]').show();
                $('[id$=List_WsImage]').show();
                $('[id$=List_WsTitle]').hide();
                $('[id$=List_WsSubtitle]').hide();
                $('[id$=List_WsInfo]').hide();
                $('[id$=List_WsAddInfo]').show();
                $('[id$=List_WsCountField]').hide();
                break;
            case "5":
                $('[id$=List_WsRowId]').show();
                $('[id$=List_WsImage]').show();
                $('[id$=List_WsTitle]').show();
                $('[id$=List_WsSubtitle]').show();
                $('[id$=List_WsInfo]').show();
                $('[id$=List_WsAddInfo]').show();
                $('[id$=List_WsCountField]').hide();
                break;
        }
        if (isCountBubble)
            $('[id$=List_WsCountField]').show();
        else 
            $('[id$=List_WsCountField]').hide();
    }
    function _showDbListBindingProperties(listType, isCountBubble) {
        switch (listType) {
            case "1":
            case "3":
                $('[id$=Db_List_RowId]').show();
                $('[id$=Db_List_Title]').show();
                $('[id$=Db_List_SubTitle]').hide();
                $('[id$=Db_List_Info]').hide();
                $('[id$=Db_List_AddInfo]').hide();
                $('[id$=Db_List_ImageName]').hide();
                $('[id$=Db_List_CountField]').hide();
                break;
            case "2":
                $('[id$=Db_List_RowId]').show();
                $('[id$=Db_List_Title]').show();
                $('[id$=Db_List_SubTitle]').show();
                $('[id$=Db_List_Info]').show();
                $('[id$=Db_List_AddInfo]').show();
                $('[id$=Db_List_ImageName]').hide();
                $('[id$=Db_List_CountField]').hide();
                break;
            case "4":
                $('[id$=Db_List_RowId]').show();
                $('[id$=Db_List_Title]').show();
                $('[id$=Db_List_SubTitle]').hide();
                $('[id$=Db_List_Info]').hide();
                $('[id$=Db_List_AddInfo]').hide();
                $('[id$=Db_List_ImageName]').show();
                $('[id$=Db_List_CountField]').hide();
                break;
            case "5":
                $('[id$=Db_List_RowId]').show();
                $('[id$=Db_List_Title]').show();
                $('[id$=Db_List_SubTitle]').show();
                $('[id$=Db_List_Info]').show();
                $('[id$=Db_List_AddInfo]').show();
                $('[id$=Db_List_ImageName]').show();
                $('[id$=Db_List_CountField]').hide();
                break;
        }
        if (isCountBubble)
            $('[id$=Db_List_CountField]').show();
        else
            $('[id$=Db_List_CountField]').hide();
    }
    
    
    function _showObjectDetails(){
        var objectType = _globalVarHelpers.getObjectTypeGlobalData();
        var strObjectId = _ddlDatabindingObject.getSelectedValue(objectType);
        if(objectType && strObjectId){
            MF_HELPERS.showObjectDetails.showObjectDetailsByType(strObjectId,objectType);
        }
    }
    return {
        isValidConditionalControl: function (controlType) {
            return _isValidConditionalControl(controlType);
        },
        databaseObjectBinding: function (cntrl, databindObj, dbObjectControl) {
            return _dbObjectBinding(cntrl, databindObj, dbObjectControl);
        },
        webserviceObjectBinding: function (cntrl, databindObj, wsObjectControl) {
            return _webserviceObjectBinding(cntrl, databindObj, wsObjectControl);
        },
        oDataObjectBinding: function (cntrl, databindObj, oDataObjectControl) {
            return _oDataObjectBinding(cntrl, databindObj, oDataObjectControl);
        },
        showODataListBindingProperties: function (listType, isCountBubble) {
            return _showODataListBindingProperties(listType, isCountBubble);
        },
        showWsListBindingProperties: function (listType, isCountBubble, wsType) {
            return _showWsListBindingProperties(listType, isCountBubble, wsType);
        },
        showDbListBindingProperties: function (listType, isCountBubble) {
            return _showDbListBindingProperties(listType, isCountBubble);
        },
        processDataBindingByObjectType:function(options){
            /*
                {
                    controlType : control type,
                    databindObject : cloned databind object for modifiaction,
                    objectType : MF_IDE_CONSTANTS.ideCommandObjectTypes,
                    isEdit:false,
                    intlsJson:json,
                    control : control object
                }
            */
            if( !options ||
                !options.controlType ||
                !options.databindObject ||
                !options.objectType ||
                !options.intlsJson ||
                !options.control){
                
                throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            }
            if(options.isEdit == null)options.isEdit = false;
            // if(options.isEdit === true){
            //     if(!options.control)throw MF_IDE_CONSTANTS.exceptionMessage.argumentsNull;
            // }
            _globalVarHelpers.setDataOfContainerDiv(options);
            switch(options.objectType){
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDt:
                        _controlsObjectBinding(
                            options.control,
                            options.controlType,
                            options.databindObject,
                            options.objectType,
                            options.ddlObjectLabel
                        );
                break;
                case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj:
                        _controlsObjectBinding(
                            options.control,
                            options.controlType,
                            options.databindObject,
                            options.objectType,
                            options.ddlObjectLabel
                        );                        
                break;
                default :
                    _controlsObjectBinding(
                        options.control,
                        options.controlType,
                        options.databindObject,
                        options.objectType,
                        options.ddlObjectLabel
                    ); 
                break;
            } 
        },
        processSaveCntrlDatabinding:function(){
            _saveDatabindingObject.save();
            closeModalPopUp('divCtrlsDataBind');
        },
        processShowOfflineDataObjDetail:function(offlineDataObj){
            _showOfflineDataObjDetails(offlineDataObj);
        },
        showObjectDetails:function(){
            _showObjectDetails();
        }
    }
})();

mFicientIde.MF_VALIDATION = (function () {
    //Numerice value validation
    function _allowOnlyNumeric(event) {
        var IsAllowed = false;
        if (event.keyCode == 8 || event.keyCode == 46) {
            IsAllowed = true;
        }
        else if (event.keyCode < 48 || event.keyCode > 57) {
            event.preventDefault();
            //showMessage('Please enter only numeric values.', DialogType.Error);
            return;
        }

        if (isNaN(event.target.value + _getKeyValue(event.keyCode))) {
            IsAllowed = false;
            event.preventDefault();
            //showMessage('Please enter only numeric values.', DialogType.Error);
            return;
        }
        return IsAllowed;
    }
    //Get keypress value
    function _getKeyValue(_Code) {
        if (_Code == 46) return ".";
        else if (_Code == 48) return "0";
        else if (_Code == 49) return "1";
        else if (_Code == 50) return "2";
        else if (_Code == 51) return "3";
        else if (_Code == 52) return "4";
        else if (_Code == 53) return "5";
        else if (_Code == 54) return "6";
        else if (_Code == 55) return "7";
        else if (_Code == 56) return "8";
        else if (_Code == 57) return "9";
    }
    //Check integer values
    function _allowOnlyIntegers(event) {
        var IsAllowed = false;
        if (event.keyCode == 8) {
            IsAllowed = true;
        }
        else {
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.preventDefault();
                //showMessage('Please enter only integers.', DialogType.Error);
            }
        }
        return IsAllowed;
    }
    function _txtValidationValue(validationType, val) {
        if (validationType == "9" || validationType == "12") {
            if (!_txtRangeValid(val.trim(), 1)) {
                showMessage('Please enter valid validation value.<br />[Validation value Example : [Numeric]-[Numeric]]', DialogType.Error);
                return;
            }
        }
        else if (validationType == "15" || validationType == "18") {
            if (!_txtRangeValid(val.trim(), 2)) {
                showMessage('Please enter valid validation value.<br />[Validation value Example : [Unsigned Integer]-[Unsigned Integer]]', DialogType.Error);
                return;
            }
        }
        else if (validationType == "14" || validationType == "17" || validationType == "19") {
            if (!_txtRangeValid(val.trim(), 3)) {
                showMessage('Please enter valid validation value.<br />[Validation value Example : [Unsigned Integer]]', DialogType.Error);
                return;
            }
        }
        else if (validationType == "13" || validationType == "16") {
            if (!_txtRangeValid(val.trim(), 4)) {
                showMessage('Please enter valid validation value.<br />[Validation value Example : [Unsigned Integer not less than 2]]', DialogType.Error);
                return;
            }
        }
        else if (validationType == "7" || val == "8" || validationType == "10" || validationType == "11" || validationType == "20") {
            if (!_txtRangeValid(val.trim(), 5)) {
                showMessage('Please enter valid validation value.<br />[Validation value Example : [Numeric]]', DialogType.Error);
                return;
            }
        }
        function _txtRangeValid(_val, _Type) {
            var IsValid = true;
            var strLeft = '';
            var strRight = '';
            var strMain = '';
            if (_Type == 1) {
                //For Numeric Range
                if (_val.indexOf('-') == -1) {
                    return false;
                }
                else {
                    if (_val.indexOf('-') == 0) {
                        strMain = _val.substring(1, _val.length);
                        if (strMain.indexOf('-') == -1) return false;
                        else {
                            if (strMain.split('-').length > 3) {
                                return false;
                            }
                            if (strMain.split('-').length == 3) {
                                strLeft = strMain.split('-')[0];
                                strRight = strMain.split('-')[1] + strMain.split('-')[2];
                                if (!$.isNumeric(strLeft)) return false;
                                if (!$.isNumeric(strRight)) return false;
                            }
                            else {
                                strLeft = strMain.split('-')[0];
                                strRight = strMain.split('-')[1];
                                if (!$.isNumeric(strLeft)) return false;
                                if (!$.isNumeric(strRight)) return false;
                            }
                        }
                    }
                    else {
                        if (_val.split('-').length > 3) {
                            return false;
                        }
                        if (_val.split('-').length == 3) {
                            strLeft = _val.split('-')[0];
                            strRight = _val.split('-')[1] + _val.split('-')[2];
                            if (!$.isNumeric(strLeft)) return false;
                            if (!$.isNumeric(strRight)) return false;
                        }
                        else {
                            strLeft = _val.split('-')[0];
                            strRight = _val.split('-')[1];
                            if (!$.isNumeric(strLeft)) return false;
                            if (!$.isNumeric(strRight)) return false;
                        }
                    }
                }
                if (!$.isNumeric(_val)) return IsValid;
            }
            else if (_Type == 2) {
                if (!(/^\d+\-+\d{0,9}$/).test(_val.trim())) return false;
                else {
                    if (_val.trim().trim().split('-')[1].length == 0) return false;
                }
            }
            else if (_Type == 3) {
                if (!(/^\d{0,9}$/).test(_val.trim())) return false;
            }
            else if (_Type == 4) {
                if (!(/^\d{0,9}$/).test(_val.trim())) return false;
                else {
                    if (parseInt(_val, 10) < 2) return false;
                }
            }
            else if (_Type == 5) {
                if (!$.isNumeric(_val)) return false;
            }
            return IsValid;
        }
    }
    return {
        allowOnlyNumeric: function (event) {
            return _allowOnlyNumeric(event);
        },
        allowOnlyIntegers: function (event) {
            return _allowOnlyIntegers(event);
        },
        txtValidationValue: function (validationType, val) {
            return _txtValidationValue(validationType, val);
        }
    }

})();
$(document).ready(function() {
    $('#btnDataBindCmdSave').on('click',function(evnt){
        try{
            mFicientIde.MF_DATA_BINDING.processSaveCntrlDatabinding();
            evnt.stopPropagation();
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
            }else{
                console.log(error);
            }
        }
    });
     $('[id$=lnkShowOfflineDataObjDetails]').on('click',function(evnt){
        try{
        
            //var offlineDataObj = MF_HELPERS.getOfflineDataObjectByObjectId($('[id$=ddlDatabindObject]').val()); 
            mFicientIde.MF_DATA_BINDING.showObjectDetails();
            evnt.stopPropagation();
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
            }else{
                console.log(error);
            }
        }
        return false;
    });
    
//    $('#DataBind_Div_Para1_DataType').bind('change', function (evnt) {
//        if (evnt.target.value == "0") {
//            $('#DataBind_Dp_Para1_DataFormat').attr("disabled", "disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para1').attr("disabled", "disabled");
//            $('#DataBind_txt_Para1_MulFactor').attr("disabled", "disabled");
//        }
//        else if (evnt.target.value == "1") {
//            $('#DataBind_Dp_Para1_DataFormat').removeAttr("disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para1').prop("disabled", "disabled");
//            $('#DataBind_txt_Para1_MulFactor').removeAttr("disabled");
//        }
//        else {
//            $('#DataBind_Dp_Para1_DataFormat').removeAttr("disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para1').removeAttr("disabled");
//            $('#DataBind_txt_Para1_MulFactor').removeAttr("disabled");
//        }
//    });

//    $('#DataBind_Dp_Para2_DataType').bind('change', function (evnt) {
//        if (evnt.target.value == "0") {
//            $('#DataBind_Dp_Para2_DataFormat').attr("disabled", "disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para2').attr("disabled", "disabled");
//            $('#DataBind_txt_Para2_MulFactor').attr("disabled", "disabled");
//        }
//        else if (evnt.target.value == "1") {
//            $('#DataBind_Dp_Para2_DataFormat').removeAttr("disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para2').attr("disabled", "disabled");
//            $('#DataBind_txt_Para2_MulFactor').removeAttr("disabled");
//        }
//        else {
//            $('#DataBind_Dp_Para2_DataFormat').removeAttr("disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para2').removeAttr("disabled");
//            $('#DataBind_txt_Para2_MulFactor').removeAttr("disabled");
//        }
//    });

//    $('#DataBind_Dp_Para3_DataType').bind('change', function (evnt) {
//        if (evnt.target.value == "0") {
//            $('#DataBind_Dp_Para3_DataFormat').attr("disabled", "disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para3').attr("disabled", "disabled");
//            $('#DataBind_txt_Para3_MulFactor').attr("disabled", "disabled");
//        }
//        else if (evnt.target.value == "1") {
//            $('#DataBind_Dp_Para3_DataFormat').removeAttr("disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para3').attr("disabled", "disabled");
//            $('#DataBind_txt_Para3_MulFactor').removeAttr("disabled");
//        }
//        else {
//            $('#DataBind_Dp_Para3_DataFormat').removeAttr("disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para3').removeAttr("disabled");
//            $('#DataBind_txt_Para3_MulFactor').removeAttr("disabled");
//        }
//    });

//    $('#DataBind_Dp_Para4_DataType').bind('change', function (evnt) {
//        if (evnt.target.value == "0") {
//            $('#DataBind_Dp_Para4_DataFormat').attr("disabled", "disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para4').attr("disabled", "disabled");
//            $('#DataBind_txt_Para4_MulFactor').attr("disabled", "disabled");
//        }
//        else if (evnt.target.value == "1") {
//            $('#DataBind_Dp_Para4_DataFormat').removeAttr("disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para4').attr("disabled", "disabled");
//            $('#DataBind_txt_Para4_MulFactor').removeAttr("disabled");
//        }
//        else {
//            $('#DataBind_Dp_Para4_DataFormat').removeAttr("disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para4').removeAttr("disabled");
//            $('#DataBind_txt_Para4_MulFactor').removeAttr("disabled");
//        }
//    });

//    $('#DataBind_Dp_Para5_DataType').bind('change', function (evnt) {
//        if (evnt.target.value == "0") {
//            $('#DataBind_Dp_Para5_DataFormat').attr("disabled", "disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para5').attr("disabled", "disabled");
//            $('#DataBind_txt_Para5_MulFactor').attr("disabled", "disabled");
//        }
//        else if (evnt.target.value == "1") {
//            $('#DataBind_Dp_Para5_DataFormat').removeAttr("disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para5').attr("disabled", "disabled");
//            $('#DataBind_txt_Para5_MulFactor').removeAttr("disabled");
//        }
//        else {
//            $('#DataBind_Dp_Para5_DataFormat').removeAttr("disabled");
//            $('#DataBind_Dp_DecimalPlaces_Para5').removeAttr("disabled");
//            $('#DataBind_txt_Para5_MulFactor').removeAttr("disabled");
//        }
//    });

//    $("#DataBind_txt_Para1_MulFactor").keypress(function (event) {
//        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
//    });

//    $("#DataBind_txt_Para2_MulFactor").keypress(function (event) {
//        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
//    });

//    $("#DataBind_txt_Para3_MulFactor").keypress(function (event) {
//        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
//    });

//    $("#DataBind_txt_Para4_MulFactor").keypress(function (event) {
//        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
//    });

//    $("#DataBind_txt_Para5_MulFactor").keypress(function (event) {
//        mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
//    });
    
});

    function initializeLabelControlFormat(cntrl){
        var n = 0;
        if((cntrl.labelDataObjs != undefined || cntrl.labelDataObjs != null) && cntrl.labelDataObjs.length > 0){
            for(var i=0; i < cntrl.labelDataObjs.length; i++){
                n = i + 1;
                if(cntrl.labelDataObjs[i] != null || cntrl.labelDataObjs[i] != undefined){
                    $('[id$=Db_Dp_Param' + n + '_DataType]').val(cntrl.labelDataObjs[i].dataType);
                    if (cntrl.labelDataObjs[i].dataType == "0") {
                        $('#Db_Dp_DecimalPlaces_Param' + n).attr("disabled", "disabled");
                        $('#Db_txt_Param' + n + '_MulFactor').attr("disabled", "disabled");
                    }
                    else if (cntrl.labelDataObjs[i].dataType == "1") {
                        $('#Db_Dp_DecimalPlaces_Param' + n).removeAttr("disabled");
                        $('#Db_txt_Param' + n + '_MulFactor').removeAttr("disabled");
                    }
                    else {
                        $('#Db_Dp_DecimalPlaces_Param' + n).prop("disabled", "disabled");
                        $('#Db_txt_Param' + n + '_MulFactor').removeAttr("disabled");
                    }
                    $('[id$=Db_Dp_DecimalPlaces_Param' + n + ']').val(cntrl.labelDataObjs[i].noOfDecimalPlaces);
                    $('[id$=Db_txt_Param' + n + '_MulFactor]').val(cntrl.labelDataObjs[i].mulFactor);
                } 
            }
        }

        $('#Db_Dp_Param1_DataType').unbind('change');
        $('#Db_Dp_Param2_DataType').unbind('change');
        $('#Db_Dp_Param3_DataType').unbind('change');
        $('#Db_Dp_Param4_DataType').unbind('change');
        $('#Db_Dp_Param5_DataType').unbind('change');

        $('#Db_Dp_Param1_DataType').bind('change', function (evnt) {
            if (evnt.target.value == "0") {
                $('#Db_Dp_DecimalPlaces_Param1').val("0");
                $('#Db_txt_Param1_MulFactor').val("1");
                $('#Db_Dp_DecimalPlaces_Param1').attr("disabled", "disabled");
                $('#Db_txt_Param1_MulFactor').attr("disabled", "disabled");
            }
            else if (evnt.target.value == "1") {
                $('#Db_Dp_DecimalPlaces_Param1').removeAttr("disabled");
                $('#Db_Dp_DecimalPlaces_Param1').val("0");
                $('#Db_txt_Param1_MulFactor').val("1");
                $('#Db_txt_Param1_MulFactor').removeAttr("disabled");
            }
            else {
                $('#Db_Dp_DecimalPlaces_Param1').removeAttr("disabled");
                $('#Db_Dp_DecimalPlaces_Param1').val("2");
                $('#Db_txt_Param1_MulFactor').val("1");
                $('#Db_Dp_DecimalPlaces_Param1').prop("disabled", "disabled");
                $('#Db_txt_Param1_MulFactor').removeAttr("disabled");
            }
        });


        $('#Db_Dp_Param2_DataType').bind('change', function (evnt) {
            if (evnt.target.value == "0") {
                $('#Db_Dp_DecimalPlaces_Param2').val("0");
                $('#Db_txt_Param2_MulFactor').val("1");
                $('#Db_Dp_DecimalPlaces_Param2').attr("disabled", "disabled");
                $('#Db_txt_Param2_MulFactor').attr("disabled", "disabled");
            }
            else if (evnt.target.value == "1") {
                $('#Db_Dp_DecimalPlaces_Param2').removeAttr("disabled");
                $('#Db_Dp_DecimalPlaces_Param2').val("0");
                $('#Db_txt_Param2_MulFactor').val("1");
                $('#Db_txt_Param2_MulFactor').removeAttr("disabled");
            }
            else {
                $('#Db_Dp_DecimalPlaces_Param2').removeAttr("disabled");
                $('#Db_Dp_DecimalPlaces_Param2').val("2");
                $('#Db_txt_Param2_MulFactor').val("1");
                $('#Db_Dp_DecimalPlaces_Param2').attr("disabled", "disabled");
                $('#Db_txt_Param2_MulFactor').removeAttr("disabled");
            }
        });

        $('#Db_Dp_Param3_DataType').bind('change', function (evnt) {
            if (evnt.target.value == "0") {
                $('#Db_Dp_DecimalPlaces_Param3').val("0");
                $('#Db_txt_Param2_MulFactor').val("1");
                $('#Db_Dp_DecimalPlaces_Param3').attr("disabled", "disabled");
                $('#Db_txt_Param3_MulFactor').attr("disabled", "disabled");
            }
            else if (evnt.target.value == "1") {
                $('#Db_Dp_DecimalPlaces_Param3').removeAttr("disabled");
                $('#Db_Dp_DecimalPlaces_Param3').val("0");
                $('#Db_txt_Param2_MulFactor').val("1");
                $('#Db_txt_Param3_MulFactor').removeAttr("disabled");
            }
            else {
                $('#Db_Dp_DecimalPlaces_Param3').removeAttr("disabled");
                $('#Db_Dp_DecimalPlaces_Param3').val("2");
                $('#Db_txt_Param2_MulFactor').val("1");
                $('#Db_Dp_DecimalPlaces_Param3').attr("disabled", "disabled");
                $('#Db_txt_Param3_MulFactor').removeAttr("disabled");
            }
        });

        $('#Db_Dp_Param4_DataType').bind('change', function (evnt) {
            if (evnt.target.value == "0") {
                $('#Db_Dp_DecimalPlaces_Param4').val("0");
                $('#Db_txt_Param4_MulFactor').val("1");
                $('#Db_Dp_DecimalPlaces_Param4').attr("disabled", "disabled");
                $('#Db_txt_Param4_MulFactor').attr("disabled", "disabled");
            }
            else if (evnt.target.value == "1") {
                $('#Db_Dp_DecimalPlaces_Param4').removeAttr("disabled");
                $('#Db_Dp_DecimalPlaces_Param4').val("0");
                $('#Db_txt_Param4_MulFactor').val("1");
                $('#Db_txt_Param4_MulFactor').removeAttr("disabled");
            }
            else {
                $('#Db_Dp_DecimalPlaces_Param4').removeAttr("disabled");
                $('#Db_Dp_DecimalPlaces_Param4').val("2");
                $('#Db_txt_Param4_MulFactor').val("1");
                $('#Db_Dp_DecimalPlaces_Param4').attr("disabled", "disabled");
                $('#Db_txt_Param4_MulFactor').removeAttr("disabled");
            }
        });

        $('#Db_Dp_Param5_DataType').bind('change', function (evnt) {
            if (evnt.target.value == "0") {
                $('#Db_Dp_DecimalPlaces_Param5').val("0");
                $('#Db_txt_Param5_MulFactor').val("1");
                $('#Db_Dp_DecimalPlaces_Param5').attr("disabled", "disabled");
                $('#Db_txt_Param5_MulFactor').attr("disabled", "disabled");
            }
            else if (evnt.target.value == "1") {
                $('#Db_Dp_DecimalPlaces_Param5').removeAttr("disabled");
                $('#Db_Dp_DecimalPlaces_Param5').val("0");
                $('#Db_txt_Param5_MulFactor').val("1");
                $('#Db_txt_Param5_MulFactor').removeAttr("disabled");
            }
            else {
                $('#Db_Dp_DecimalPlaces_Param5').removeAttr("disabled");
                $('#Db_Dp_DecimalPlaces_Param5').val("2");
                $('#Db_txt_Param5_MulFactor').val("1");
                $('#Db_Dp_DecimalPlaces_Param5').attr("disabled", "disabled");
                $('#Db_txt_Param5_MulFactor').removeAttr("disabled");
            }
        });

        $("#Db_txt_Param1_MulFactor").keypress(function (event) {
            mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
        });

        $("#Db_txt_Param1_MulFactor").blur(function (event) {
            validateMulFactorValue(event.target.id, event.target.value);
        });

        $("#Db_txt_Param2_MulFactor").keypress(function (event) {
            mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
        });

        $("#Db_txt_Param2_MulFactor").blur(function (event) {
            validateMulFactorValue(event.target.id, event.target.value);
        });

        $("#Db_txt_Param3_MulFactor").keypress(function (event) {
            mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
        });

        $("#Db_txt_Param3_MulFactor").blur(function (event) {
            validateMulFactorValue(event.target.id, event.target.value);
        });

        $("#Db_txt_Param4_MulFactor").keypress(function (event) {
            mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
        });

        $("#Db_txt_Param4_MulFactor").blur(function (event) {
            validateMulFactorValue(event.target.id, event.target.value);
        });

        $("#Db_txt_Param5_MulFactor").keypress(function (event) {
            mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
        });

        $("#Db_txt_Param5_MulFactor").blur(function (event) {
            validateMulFactorValue(event.target.id, event.target.value);
        });

        $('[id$=btnSaveLabelCtrlParaFormat]').unbind('click');
        $('[id$=btnSaveLabelCtrlParaFormat]').bind('click', function () {
            saveLabelCntrlParamFormats(cntrl);
            clearLabelObjectsParams();
            
            $('#SubProcLabelCtrlFormat').dialog('close');
            return false;
        });
    }

   function validateMulFactorValue(cntrlId, value){
        if(value.trim().length <= 0)
            $('#' + cntrlId).val('1');
        else if(parseFloat(value.trim()) <= 0){
            $('#' + cntrlId).val('1');
            showMessage('Multiplication factor cannot be less than 0.', DialogType.Error);
            return;
        }
   }

    function saveLabelCntrlParamFormats(cntrl){
        if(cntrl.labelDataObjs != null && cntrl.labelDataObjs != undefined && parseInt(cntrl.labelDataObjs.length, 10) > 0){
            var i = 1;
            $.each(cntrl.labelDataObjs, function(){
                if(i <= 5 && this.para === i + "%")
                {
                    this.dataType = $('[id$=Db_Dp_Param' + i + '_DataType]').val().trim();
                    this.noOfDecimalPlaces = $('[id$=Db_Dp_DecimalPlaces_Param' + i + ']').val();
                    this.mulFactor = $('[id$=Db_txt_Param' + i + '_MulFactor]').val().trim();
                    i = i +1;
                }
            });
        }
    }

    function changeLabelObjectBinding(cntrl){
        if(cntrl.labelDataObjs != null && cntrl.labelDataObjs != undefined && parseInt(cntrl.labelDataObjs.length, 10) > 0){
            var i = 1;
            $.each(cntrl.labelDataObjs, function(){
                if(i <= 5 && this.para === i + "%")
                {
                    this.selectedCol = "-1";
                    i = i +1;
                }
            });
        }
    }

    //Tanika*Change
    function clearLabelObjectsParams(){
        for(var i=0; i < 5; i++){
            n = i + 1;
            $('[id$=Db_Div_Param' + n + ']').val('');
            $('[id$=Db_Dp_Param' + n + '_DataType]').val('0');
            $('[id$=Db_Dp_Param' + n + '_DataFormat]').val('0');
            $('[id$=Db_txt_Param' + n + '_MulFactor]').val('1');
            $('[id$=Db_Dp_DecimalPlaces_Param' + n + ']').val('0');
            $('#Db_Dp_Param' + n + '_DataFormat').attr("disabled", "disabled");
            $('#Db_Dp_DecimalPlaces_Param' + n).attr("disabled", "disabled");
            $('#Db_txt_Param' + n + '_MulFactor').attr("disabled", "disabled");
        }
    }

    function isValidUpdateControl(control){
        var isValid = false;
        if(control)
        {
            switch (control.type.idPrefix) {
                case MF_IDE_CONSTANTS.CONTROLS.DROPDOWN.idPrefix:
                case MF_IDE_CONSTANTS.CONTROLS.LIST.idPrefix:
                case MF_IDE_CONSTANTS.CONTROLS.LOCATION.idPrefix:
                case MF_IDE_CONSTANTS.CONTROLS.LABEL.idPrefix:
                case MF_IDE_CONSTANTS.CONTROLS.IMAGE.idPrefix:
                case MF_IDE_CONSTANTS.CONTROLS.PIECHART.idPrefix:
                case MF_IDE_CONSTANTS.CONTROLS.BARGRAPH.idPrefix:
                case MF_IDE_CONSTANTS.CONTROLS.LINEGRAPH.idPrefix:
                case MF_IDE_CONSTANTS.CONTROLS.ANGULARGUAGE.idPrefix:
                case MF_IDE_CONSTANTS.CONTROLS.CYLINDERGUAGE.idPrefix:
                case MF_IDE_CONSTANTS.CONTROLS.TEXTBOX.idPrefix:
                    isValid = true;
                break;
            }
        }
        return isValid;
    }

function checkIfRowIdExist(Id, tableName) {
    var exist = 0;
    $("tbody#" + tableName).find("tr").each(function () {
        if (this.id === Id)
            exist = exist + 1;
    });

    return exist;
}

function getHighestRowIdCount(tableName) {
    var arrId = [];
    var count = 0;
    $("tbody#" + tableName).find("tr").each(function () {
        arrId.push(this.id.split('_')[1]);
    });
    if (arrId.length > 0) count = Math.max.apply(Math, arrId) + 1;
    else count = 1;
    return count;
}

/*-----------------------------------------------------------Control On change Ace Editor--------------------------------------------------------*/
var _jsAceEditorOnChang = (function () {
    function _getAceEditorInstance() {
        return jsaceEditor;
    }
    function _setTextInEditor(text) {
        if (mfUtil.isNullOrUndefined(text)) {
            throw mfExceptionMsgs.argumentsNull;
        }
        var editor = _getAceEditorInstance();
        _removeChangeEvent();
        _removeCursorChangeEvent();
        editor.setValue(text,-1);
        _setEditorContentDetails();
        _setEditorCursorDetails();
        _setOnCursorChangeEvent();
        _setOnChangeEvent();
    }
    function _setEditorContentDetails() {
        var editor = _getAceEditorInstance();
        var strValue = editor.getValue();
        var noOfLines = editor.session.getLength();
        $('#spanCntrlonChngAceEditorContentLines').text(noOfLines);
        $('#spanCntrlonChngAceEditorContentLength').text(strValue.length);
    }
    function _removeChangeEvent() {
        var editor = _getAceEditorInstance();
        editor.getSession().removeListener('change');
    }
    function _setOnChangeEvent() {
        var editor = _getAceEditorInstance();
        editor.getSession().on('change', _setEditorContentDetails);
    }
    function _removeCursorChangeEvent() {
        var editor = _getAceEditorInstance();
        editor.getSession().removeListener('changeCursor');
    }
    function _setOnCursorChangeEvent() {
        var editor = _getAceEditorInstance();
        editor.getSession().on('changeCursor', _setEditorContentDetails);
    }
    function _setEditorCursorDetails() {
        var editor = _getAceEditorInstance();
        var cursorPosition = editor.selection.getCursor();
        var row = 0, column = 0;
        if (cursorPosition) {
            row = (cursorPosition.row) + 1;
            column = cursorPosition.column;
        }
        $('#spanCntrlonChngAceEditorLineNo').text(row);
        $('#spanCntrlonChngAceEditorColumnNo').text(column);
    }
    function _getText() {
        var editor = _getAceEditorInstance();
        return editor.getSession().getValue();
    }
    return {
        setTextInEditor: function (text) {
            _setTextInEditor(text);
        },
        changeEventHandler: function () {
            _setEditorContentDetails();
        },
        setOnChangeEvent: function () {
            _setOnChangeEvent();
        },
        removeOnChangeEvent: function () {
            _removeChangeEvent();
        },
        changeCursorEventHandler: function () {
            _setEditorCursorDetails();
        },
        getText: function () {
            return _getText();
        }
    };
})();

//EXCEPTIONS OBJECT
function GlobalDataUndefinedException(){
    this.Message = "Global Data is not defined.";
}
function NewControlAdditionException(messages){
    if($.isArray(messages) && messages.length>0){
        this.Messages = messages;
    }
    else{
        this.Messages = ["The control cannot be added to the form."];
    }
}
var jsmfIdeScript = "mfIdeScript";
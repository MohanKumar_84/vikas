﻿var IMGprogress = new Image(32, 32);
IMGprogress.src = '//enterprise.mficient.com/css/images/icons/dark/progress.gif';
var IMGaddform = new Image(16, 16);
IMGaddform.src = '//enterprise.mficient.com/images/addform.png';
var IMGopen = new Image(16, 16);
IMGopen.src = '//enterprise.mficient.com/images/open.png';
var IMGadd1 = new Image(16, 16);
IMGadd1.src = '//enterprise.mficient.com/images/add1.png';
var IMGDialogWarn = new Image(16, 16);
IMGDialogWarn.src = '//enterprise.mficient.com/images/dialog_warning.png';
var IMGDialogInfo = new Image(16, 16);
IMGDialogInfo.src = '//enterprise.mficient.com/images/dialog_info.png';
var IMGDialogError = new Image(16, 16);
IMGDialogError.src = '//enterprise.mficient.com/images/dialog_error.png';
var ImgNext = new Image(32, 32);
ImgNext.src = '//enterprise.mficient.com/images/imgNext.png';
var ImgPrvs = new Image(32, 32);
ImgPrvs.src = '//enterprise.mficient.com/images/imgPrvs.png';
var IMGcondionIcon = new Image(16, 16);
IMGcondionIcon.src = '//enterprise.mficient.com/images/condionIcon.png';

var IMGgroupboxIcon = new Image(16, 16);
IMGgroupboxIcon.src = '//enterprise.mficient.com/images/groupbox.png';
var IMGlabelIcon = new Image(16, 16);
IMGlabelIcon.src = '//enterprise.mficient.com/images/label.png';
var IMGtextboxIcon = new Image(16, 16);
IMGtextboxIcon.src = '//enterprise.mficient.com/images/textbox.png';
var IMGselectIcon = new Image(16, 16);
IMGselectIcon.src = '//enterprise.mficient.com/images/select.png';
var IMGradioButtonIcon = new Image(16, 16);
IMGradioButtonIcon.src = '//enterprise.mficient.com/images/radioButton.png';
var IMGCheckboxIcon = new Image(16, 16);
IMGCheckboxIcon.src = '//enterprise.mficient.com/images/Checkbox.png';
var IMGimageIcon = new Image(16, 16);
IMGimageIcon.src = '//enterprise.mficient.com/images/image.png';
var IMGrepeaterIcon = new Image(16, 16);
IMGrepeaterIcon.src = '//enterprise.mficient.com/images/repeater.png';
var IMGhiddenFieldIcon = new Image(16, 16);
IMGhiddenFieldIcon.src = '//enterprise.mficient.com/images/hiddenField.png';
var IMGdatetimePickerIcon = new Image(16, 16);
IMGdatetimePickerIcon.src = '//enterprise.mficient.com/images/datetimePickericon.png';
var IMGToggleIcon = new Image(16, 16);
IMGToggleIcon.src = '//enterprise.mficient.com/images/ToggleIcon.png';
var IMGhyperlinkIcon = new Image(16, 16);
IMGhyperlinkIcon.src = '//enterprise.mficient.com/images/hyperlinkIcon.png';
var IMGsliderIcon = new Image(16, 16);
IMGsliderIcon.src = '//enterprise.mficient.com/images/sliderIcon.png';
var IMGbarcodeIcon = new Image(16, 16);
IMGbarcodeIcon.src = '//enterprise.mficient.com/images/barcodeIcon.png';
var IMGLocationIcon = new Image(16, 16);
IMGLocationIcon.src = '//enterprise.mficient.com/images/LocationIcon.png';
var IMGpiechartIcon = new Image(16, 16);
IMGpiechartIcon.src = '//enterprise.mficient.com/images/piecharticon.png';
var IMGbargraphIcon = new Image(16, 16);
IMGbargraphIcon.src = '//enterprise.mficient.com/images/bargraphicon.png';
var IMGlineChartIcon = new Image(16, 16);
IMGlineChartIcon.src = '//enterprise.mficient.com/images/lineChartIcon.png';

var IMGPreload1 = new Image();
IMGPreload1.src = '//enterprise.mficient.com/images/datetime.png';
var IMGPreload2 = new Image();
IMGPreload2.src = '//enterprise.mficient.com/images/UploadPhoto.png';
var IMGPreload3 = new Image();
IMGPreload3.src = '//enterprise.mficient.com/images/piechart.png';
var IMGPreload4 = new Image();
IMGPreload4.src = '//enterprise.mficient.com/images/bargarph.jpg';
var IMGPreload5 = new Image();
IMGPreload5.src = '//enterprise.mficient.com/images/lineChart.jpg';

var IMGPreload6 = new Image();
IMGPreload6.src = '//enterprise.mficient.com/images/shades_cont_top_lft.png';
var IMGPreload7 = new Image();
IMGPreload7.src = '//enterprise.mficient.com/images/shades_cont_top_rgt.png';
var IMGPreload8 = new Image();
IMGPreload8.src = '//enterprise.mficient.com/images/shades_cont_bot_lft.png';
var IMGPreload9 = new Image();
IMGPreload9.src = '//enterprise.mficient.com/images/shades_cont_bot_rgt.png';


var IMGPreload10 = new Image();
IMGPreload10.src = '//enterprise.mficient.com/images/B-T.png';
var IMGPreload11 = new Image();
IMGPreload11.src = '//enterprise.mficient.com/images/B-T-ACTIVE.png';
var IMGPreload12 = new Image();
IMGPreload12.src = '//enterprise.mficient.com/images/BL-TR-0.png';
var IMGPreload13 = new Image();
IMGPreload13.src = '//enterprise.mficient.com/images/BL-TR-2.png';
var IMGPreload14 = new Image();
IMGPreload14.src = '//enterprise.mficient.com/images/BL-TR-ACTIVE-0.png';
var IMGPreload15 = new Image();
IMGPreload15.src = '//enterprise.mficient.com/images/BL-TR-ACTIVE-2.png';

var IMGPreload16 = new Image();
IMGPreload16.src = '//enterprise.mficient.com/images/BR-TL-1.png';
var IMGPreload17 = new Image();
IMGPreload17.src = '//enterprise.mficient.com/images/BR-TL-3.png';
var IMGPreload18 = new Image();
IMGPreload18.src = '//enterprise.mficient.com/images/BR-TL-ACTIVE-1.png';
var IMGPreload19 = new Image();
IMGPreload19.src = '//enterprise.mficient.com/images/BR-TL-ACTIVE-3.png';

var IMGPreload20 = new Image();
IMGPreload20.src = '//enterprise.mficient.com/images/L-R-ACTIVE.png';
var IMGPreload21 = new Image();
IMGPreload21.src = '//enterprise.mficient.com/images/L-R.png';


var IMGPreload22 = new Image();
IMGPreload22.src = '//enterprise.mficient.com/images/T-B-ACTIVE.png';
var IMGPreload23 = new Image();
IMGPreload23.src = '//enterprise.mficient.com/images/T-B.png';
var IMGPreload24 = new Image();
IMGPreload6.src = '//enterprise.mficient.com/images/R-L-ACTIVE.png';
var IMGPreload25 = new Image();
IMGPreload25.src = '//enterprise.mficient.com/images/R-L.png';

var IMGPreload26 = new Image();
IMGPreload26.src = '//enterprise.mficient.com/images/TL-BR-1.png';
var IMGPreload27 = new Image();
IMGPreload27.src = '//enterprise.mficient.com/images/TL-BR-3.png';
var IMGPreload28 = new Image();
IMGPreload28.src = '//enterprise.mficient.com/images/TL-BR-ACTIVE-1.png';
var IMGPreload29 = new Image();
IMGPreload29.src = '//enterprise.mficient.com/images/TL-BR-ACTIVE-3.png';
var IMGPreload30 = new Image();
IMGPreload30.src = '//enterprise.mficient.com/images/TR-BL-0.png';
var IMGPreload31 = new Image();
IMGPreload31.src = '//enterprise.mficient.com/images/TR-BL-2.png';
var IMGPreload32 = new Image();
IMGPreload32.src = '//enterprise.mficient.com/images/TR-BL-ACTIVE-0.png';
var IMGPreload33 = new Image();
IMGPreload33.src = '//enterprise.mficient.com/images/TR-BL-ACTIVE-2.png';
var IMGPreload33 = new Image();
IMGPreload33.src = '//enterprise.mficient.com/images/imgNext.png';
var IMGPreload34 = new Image();
IMGPreload34.src = '//enterprise.mficient.com/images/imgPrvs.png';
var IMGPreload35 = new Image();
IMGPreload35.src = '//enterprise.mficient.com/images/TR-BL-0.png';
var IMGPreload36 = new Image();
IMGPreload36.src = '//enterprise.mficient.com/images/TR-BL-2.png';
var IMGPreload37 = new Image();
IMGPreload37.src = '//enterprise.mficient.com/images/TR-BL-ACTIVE-0.png';
var IMGPreload38 = new Image();
IMGPreload38.src = '//enterprise.mficient.com/images/TR-BL-ACTIVE-2.png';
var IMGPreload39 = new Image();
IMGPreload39.src = '//enterprise.mficient.com/images/Tablet.png';
var IMGPreload40 = new Image();
IMGPreload40.src = '//enterprise.mficient.com/images/arrowup.png';
var IMGPreload41 = new Image();
IMGPreload41.src = '//enterprise.mficient.com/images/arrowdown.png';

var IMGPreload42 = new Image();
IMGPreload42.src = '//enterprise.mficient.com/images/T-B-T.png';
var IMGPreload43 = new Image();
IMGPreload43.src = '//enterprise.mficient.com/images/L-R-L.png';
var IMGPreload44 = new Image();
IMGPreload44.src = '//enterprise.mficient.com/images/T-B-T-ACTIVE.png';
var IMGPreload45 = new Image();
IMGPreload45.src = '//enterprise.mficient.com/images/L-R-L-ACTIVE.png';

var IMGPreload46 = new Image();
IMGPreload46.src = '//enterprise.mficient.com/images/collapse.png';
var IMGPreload47 = new Image();
IMGPreload47.src = '//enterprise.mficient.com/images/expand.png';
var IMGPreload48 = new Image();
IMGPreload48.src = '//enterprise.mficient.com/images/warning.png';

var IMGPreload49 = new Image();
IMGPreload49.src = '//enterprise.mficient.com/images/icon/GRAY0295.png';
var IMGPreload50 = new Image();
IMGPreload50.src = '//enterprise.mficient.com/images/icon/GRAY0161.png';

var IMGPreload52 = new Image();
IMGPreload52.src = '//enterprise.mficient.com/images/ImgList.png';

var IMGPreload53 = new Image();
IMGPreload53.src = '//enterprise.mficient.com/images/ImgToggle.png';

var IMGPreload54 = new Image();
IMGPreload54.src = '//enterprise.mficient.com/images/ImgDropdown.png';

var IMGPreload56 = new Image();
IMGPreload56.src = '//enterprise.mficient.com/images/ImgBarcode.png';

var IMGPreload57 = new Image();
IMGPreload57.src = '//enterprise.mficient.com/images/ImgCheckbox.png';


var IMGPreload58 = new Image();
IMGPreload58.src = '//enterprise.mficient.com/images/ImgDivider.png';

var IMGPreload59 = new Image();
IMGPreload59.src = '//enterprise.mficient.com/images/ImgHyperLink.png';

var IMGPreload60 = new Image();
IMGPreload60.src = '//enterprise.mficient.com/images/ImgRadioButton.png';

var IMGPreload61 = new Image();
IMGPreload61.src = '//enterprise.mficient.com/images/ImgScroll.png';

var IMGPreload62 = new Image();
IMGPreload62.src = '//enterprise.mficient.com/images/ImgScroll.png';

var IMGPreload63 = new Image();
IMGPreload63.src = '//enterprise.mficient.com/images/Table.jpg';

var IMGPreload64 = new Image();
IMGPreload64.src = '//enterprise.mficient.com/images/ImgDatetime.png';

var IMGPreload65 = new Image();
IMGPreload65.src = '//enterprise.mficient.com/images/ImgTextbox.png';

var IMGPreload66 = new Image();
IMGPreload66.src = '//enterprise.mficient.com/images/Location.png';

var IMGPreload67 = new Image();
IMGPreload67.src = '//enterprise.mficient.com/images/DefaultImage .png';

var IMGPreload68 = new Image();
IMGPreload68.src = '//enterprise.mficient.com/images/img_barcode.png';

var IMGPreload69 = new Image();
IMGPreload69.src = '//enterprise.mficient.com/images/AngularGaugeImage.png';

var IMGPreload70 = new Image();
IMGPreload70.src = '//enterprise.mficient.com/images/imageCylinderGauge.png';

var IMGPreload71 = new Image();
IMGPreload71.src = '//enterprise.mficient.com/images/Spacer.png';

var IMGPreload72 = new Image();
IMGPreload72.src = '//enterprise.mficient.com/images/Picture.png';

var IMGPreload73 = new Image();
IMGPreload73.src = '//enterprise.mficient.com/images/Scribble.png';

var IMGPreload74 = new Image();
IMGPreload74.src = '//enterprise.mficient.com/images/scribbleimage.png';

var IMGPreload75 = new Image();
IMGPreload75.src = '//enterprise.mficient.com/images/PictureImage.png';

var IMGPreload76 = new Image();
IMGPreload76.src = '//enterprise.mficient.com/images/save2.png';

var IMGPreload77 = new Image();
IMGPreload77.src = '//enterprise.mficient.com/images/saveas2.png';

var IMGPreload78 = new Image();
IMGPreload78.src = '//enterprise.mficient.com/images/NewWindow.png';

var IMGPreload79 = new Image();
IMGPreload79.src = '//enterprise.mficient.com/images/Commit.png';

var IMGPreload80 = new Image();
IMGPreload80.src = '//enterprise.mficient.com/images/AppDeleteIcon.png';

var IMGPreload81 = new Image();
IMGPreload81.src = '//enterprise.mficient.com/images/CloseApp.png';

var IMGPreload82 = new Image();
IMGPreload82.src = '//enterprise.mficient.com/images/sorting.png';

var IMGPreload83 = new Image();
IMGPreload83.src = '//enterprise.mficient.com/images/storyboardIcon.png';

var IMGPreload84 = new Image();
IMGPreload84.src = '//enterprise.mficient.com/images/spacerIcon.png';

var IMGPreload85 = new Image();
IMGPreload85.src = '//enterprise.mficient.com/images/pictureIcon.png';

var IMGPreload86 = new Image();
IMGPreload86.src = '//enterprise.mficient.com/images/scribbleicon.pn';

var IMGPreload87 = new Image();
IMGPreload87.src = '//enterprise.mficient.com/images/iconAngularGauge.png.png';

var IMGPreload88 = new Image();
IMGPreload88.src = '//enterprise.mficient.com/images/iconCylinderGuage.png';

var IMGPreload89 = new Image();
IMGPreload89.src = '//enterprise.mficient.com/images/help.png';

var IMGPreload90 = new Image();
IMGPreload90.src = '//enterprise.mficient.com/images/CopyForm.png';
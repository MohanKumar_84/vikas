﻿/*USING GLOBAL VARIABLES html5DragDropBox aceEditor */

// var appJson = "";
// var appId = "";
// var app;
// var totalFileCount = 10;
// var isChildClicked = false;

// var TabEnum = {
//     FILES: 1,
//     PROPERTIES: 2
// };
// if (!String.prototype.endsWith) {
//   Object.defineProperty(String.prototype, 'endsWith', {
//     value: function(searchString, position) {
//       var subjectString = this.toString();
//       if (position === undefined || position > subjectString.length) {
//         position = subjectString.length;
//       }
//       position -= searchString.length;
//       var lastIndex = subjectString.indexOf(searchString, position);
//       return lastIndex !== -1 && lastIndex === position;
//     }
//   });
// }
MF_IDE_CONSTANTS.OBJECTS_WITH_PROPERTY.HTML5_APPS = { "idPrefix": "HTML5APP", "propPluginPrefix": "HTML5APP" };
MF_IDE_CONSTANTS.OBJECTS_WITH_PROPERTY.HTML5_APP_FILE = { "idPrefix": "HTML5APPFILE", "propPluginPrefix": "HTML5APPFILE" };

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.html5App={
        groups: {
            Properties: {
                AppName: function(control, appObj) {
                    $(control).val(appObj.name);
                },
                AppDescription: function(control, appObj,options/*object of cntrol realted html objects from prop sheet*/) {
                    var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                    var strDescription = appObj.description;
                    if(strDescription){
                        $(control).val(strDescription);
                        if(strDescription.length>25){
                            $textAreaLabel.val(strDescription.substring(0,25)+'...');
                        }
                        else{
                            $textAreaLabel.val(strDescription);
                        }    
                    }
                    else{
                        $(control).val('');
                        $textAreaLabel.val('');
                    }
                },
                StartUpPage: function(control, appObj) {
                    var aryHtmlFiles = appObj.fnGetHtmlFilesInRoot();
                    if($.isArray(aryHtmlFiles) && aryHtmlFiles.length>0){
                        var strOptions ="<option value=\"-1\">Select File</option>",
                            objHmtlFile = null,
                            strFileName = "";
                        for(var i=0;i<= aryHtmlFiles.length-1;i++){
                            objHmtlFile = aryHtmlFiles[i];
                            strFileName = objHmtlFile.fnGetHtmlFilename();
                            strOptions += "<option value=\""+strFileName+"\">";
                            strOptions +=  strFileName+"</option>";
                        }
                        $(control).html(strOptions);
                        if(appObj.fnDoesAppHasStartupPage()){
                            $(control).val(appObj.fnGetStartupPage());
                        }
                        else{ 
                            $(control).val("-1");
                        }    
                    }
                },
                AppIcon: function(control, appObj) {
                    $(control).attr("src", MF_IDE_CONSTANTS.EnterpriseIconPrefix + "/" + appObj.icon);
                },
                eventHandlers:{
                    AppName:function(evnt){
                        var $self = $(this);
                        var evntData = evnt.data;
                        var propObject = evnt.data.propObject;
                        propObject.fnEditName(propObject,$self.val());
                    },
                    AppDescription:function(evnt){
                        var $self = $(this);
                        var evntData = evnt.data;
                        var propObject = evnt.data.propObject;
                        //var objAppOfOtherType = null;
                        //propObject.fnEditDescription(propObject,$self.val());
                        propObject.fnSetDescription($self.val());
                    },
                    StartUpPage:function(evnt){
                       var $self = $(this);
                       var evntData = evnt.data;
                       var propObject = evnt.data.propObject;
                       if($self.val() === "-1"){
                           propObject.fnSetStartupPage("");
                       }
                       else{
                           propObject.fnSetStartupPage($self.val());
                       }
                       //propObject.fnSetStartupPage()
                    },
                    AppIcon:function(evnt){
                        mfIconsGallery.setEventOfImageSelection(mfHtml5App.appIconSelected);
                        mfIconsGallery.showIconGallery();
                    }
                }
            }
        },
        propSheetCntrl:(function(){
            function _getDivControlHelperData(){
                return $('#divHtml5AppProperties').data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
            }
            function _getAppIcon(){
                var divData = _getDivControlHelperData();
                var objControl = null;
                if(divData){
                    objControl = divData.getControlDtls(MF_IDE_CONSTANTS.OBJECTS_WITH_PROPERTY.HTML5_APPS.propPluginPrefix,
                    "Properties","Html5AppIcon");
                }
                return objControl;
            }
            function _getStartupForm(){
                var divData = _getDivControlHelperData();
                var objControl = null;
                if(divData){
                    objControl = divData.getControlDtls(MF_IDE_CONSTANTS.OBJECTS_WITH_PROPERTY.APPS.propPluginPrefix,
                    "Properties","Html5StartUpForm");
                }
                return objControl;
            }
            function _getAllWrapperDivs(){
                var divData = _getDivControlHelperData();
                var $wrapperDivs=$([]);
                if(divData){
                    $wrapperDivs = divData.getAllWrapperDivs();
                }
                return $wrapperDivs;
            }
            return {
                Properties :{
                    getAppIcon:_getAppIcon,
                    getStartupForm:_getStartupForm
                },
                AllWrapperDivs:function(){
                    return _getAllWrapperDivs();
                }
            };
        })()
};
mFicientIde.PropertySheetJson.Html5App ={
    "type" : MF_IDE_CONSTANTS.OBJECTS_WITH_PROPERTY.HTML5_APPS,
    "groups" : [
        {
            "name" : "Properties",
            "prefixText" : "Properties",
            "hidden" : true,
            "helpTextDivId":"",
            "properties" : [
                {
                    "text" : "Name",
                    "control" : MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                    "prefixText" : "AppName",
                    "noOfLines" : "0",
                    "validations" : [],
                    "customValidations" : [],
                    "events" : [{
                     "name":"change",
                     "func":PROP_JSON_HTML_MAP.html5App.groups.Properties.eventHandlers.AppName,
                     "context":"",
                     "arguments":{
                      }
                    }],
                    "CntrlProp" : "",
                    "helpText" :{
                    },
                    "init" : {
                        "maxlength" : 30,
                        "disabled" : false,
                        "defltValue" : "",
                        "hidden" : false
                    }
                },
                {
                    "text" : "Description",
                    "control" : MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                    "prefixText" : "AppDescription",
                    "noOfLines" : "0",
                    "validations" : [],
                    "customValidations" : [],
                    "events" : [{
                     "name":"change",
                     "func":PROP_JSON_HTML_MAP.html5App.groups.Properties.eventHandlers.AppDescription,
                     "context":"",
                     "arguments":{
                        }
                    }],
                    "CntrlProp" : "",
                    "helpText" : {
                    },
                    "init" : {
                        "maxlength" : 450,
                        "disabled" : false,
                        "defltValue" : "",
                        "hidden" : false
                    }
                },
                {
                     "text" : "StartUp Page",
                     "control" : MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                     "prefixText" : "Html5StartUpPage",
                     "defltValue" : "value",
                     "validations" : [],
                     "customValidations" : [],
                     "events" : [{
                      "name":"change",
                      "func": PROP_JSON_HTML_MAP.html5App.groups.Properties.eventHandlers.StartUpPage,
                      "context":"",
                      "arguments":{
                       }
                     }],
                     "CntrlProp" : "",
                     "HelpText" : "",
                     "selectValueBy" : "",
                     "init" : {
                         "bindOptionsType" : MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                         "disabled" : false,
                         "hidden" : false
                     },
                     "options" : [{
                             "text" : "Select File",
                             "value" : "-1"
                         }
                     ]
                },
                {
                    "text" : "App Icon",
                    "control" : MF_PROPS_CONSTANTS.CONTROL_TYPE.browseOfIcon,
                    "prefixText" : "Html5AppIcon",
                    "defltValue" : "value",
                    "disabled" : false,
                    "noOfLines" : "0",
                    "validations" : [],
                    "customValidations" : [],
                    "events" : [{
                     "name":"click",
                     "func":PROP_JSON_HTML_MAP.html5App.groups.Properties.eventHandlers.AppIcon,
                     "context":"",
                     "arguments":{
                        }
                    }],
                    "CntrlProp" : "",
                    "HelpText" : "",
                    "init" : {
                        "disabled" : false,
                        "iconSrc" : "//enterprise.mficient.com/images/icon/GRAY0378.png",
                        "hidden" : false
                    }
                } 
            ]
        }
    ],
    "otherPropSheetVisible":[
    ]
};

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.html5AppFile={
    groups: {
        Properties: {
            FileName: function(control, fileObj) {
                $(control).val(fileObj.fnGetHtmlFilename());
            },
            FileSize: function(control, fileObj,options/*object of cntrol realted html objects from prop sheet*/) {
                $(control).val(fileObj.fnGetFileSize());
            },
            LastEditedDate: function(control, fileObj,options) {
                $(control).val(fileObj.fnGetLastEditedDate());
            },
            EditedBy: function(control, fileObj,options) {
                $(control).val(fileObj.fnGetEditedBy());
            },
            Dimensions: function(control, fileObj,options) {
                if(fileObj.fnGetFileType() === MF_HTML5_CONSTANTS.fileTypes.image){
                    $(control).val(fileObj.fnGetDimensions());
                    options.cntrlHtmlObjects.wrapperDiv.show();
                }
                else{
                   options.cntrlHtmlObjects.wrapperDiv.hide(); 
                }
            },
            eventHandlers:{
            }
        }
    },
    propSheetCntrl:(function(){
    })()
};
mFicientIde.PropertySheetJson.Html5AppFile ={
    "type" : MF_IDE_CONSTANTS.OBJECTS_WITH_PROPERTY.HTML5_APP_FILE,
    "groups" : [
        {
            "name" : "File Properties",
            "prefixText" : "Properties",
            "hidden" : true,
            "helpTextDivId":"",
            "properties" : [
                {
                    "text" : "Name",
                    "control" : MF_PROPS_CONSTANTS.CONTROL_TYPE.label,
                    "prefixText" : "FileName",
                    "noOfLines" : "0",
                    "validations" : [],
                    "customValidations" : [],
                    "events" : [],
                    "CntrlProp" : "",
                    "helpText" :{
                        "header":"File Name",
                        "description":"File Name"
                    },
                    "init" : {
                        "hidden" : false
                    }
                },
                {
                    "text" : "Size",
                    "control" : MF_PROPS_CONSTANTS.CONTROL_TYPE.label,
                    "prefixText" : "FileSize",
                    "noOfLines" : "0",
                    "validations" : [],
                    "customValidations" : [],
                    "events" : [],
                    "CntrlProp" : "",
                    "helpText" : {
                        "header":"",
                        "description":""
                    },
                    "init" : {
                        "defltValue" : "",
                        "hidden" : false
                    }
                },
                {
                    "text" : "Last Edit Date",
                    "control" : MF_PROPS_CONSTANTS.CONTROL_TYPE.label,
                    "prefixText" : "LastEditedDate",
                    "noOfLines" : "0",
                    "validations" : [],
                    "customValidations" : [],
                    "events" : [],
                    "CntrlProp" : "",
                    "helpText" : {
                        "header":"",
                        "description":""
                    },
                    "init" : {
                        "defltValue" : "",
                        "hidden" : false
                    }
                },
                {
                    "text" : "Edited By",
                    "control" : MF_PROPS_CONSTANTS.CONTROL_TYPE.label,
                    "prefixText" : "EditedBy",
                    "noOfLines" : "0",
                    "validations" : [],
                    "customValidations" : [],
                    "events" : [],
                    "CntrlProp" : "",
                    "helpText" : {
                        "header":"",
                        "description":""
                    },
                    "init" : {
                        "defltValue" : "",
                        "hidden" : false
                    }
                },
                {
                     "text" : "Dimensions",
                     "control" : MF_PROPS_CONSTANTS.CONTROL_TYPE.label,
                     "prefixText" : "Dimensions",
                     "noOfLines" : "0",
                     "validations" : [],
                     "customValidations" : [],
                     "events" : [],
                     "CntrlProp" : "",
                     "helpText" : {
                         "header":"",
                         "description":""
                     },
                     "init" : {
                         "defltValue" : "",
                         "hidden" : false
                     }
                 }
                
            ]
        }
    ],
    "otherPropSheetVisible":[
    ]
};

function Html5App(options){
    if (!options) options = {};
    this.name = options.name;
    this.id = options.id;
    this.description = options.description;
    this.startupPage = options.startupPage;
    this.htmlFilesInRoot = options.htmlFilesInRoot;
    this.icon = options.icon;
}
Html5App.prototype.propSheetHtmlMapping = PROP_JSON_HTML_MAP.html5App;
Html5App.prototype.fnSetHtmlFilesInRoot = function (value/*array of Html5AppFile*/) {
    this.htmlFilesInRoot  = value;
};
Html5App.prototype.fnGetHtmlFilesInRoot = function () {
    return this.htmlFilesInRoot;
};
Html5App.prototype.fnGetStartupPage = function () {
    return this.startupPage;
};
Html5App.prototype.fnSetStartupPage = function (value/*string*/) {
    this.startupPage = value;
};
Html5App.prototype.fnDoesAppHasStartupPage = function () {
    var startupPage = this.fnGetStartupPage();
    if(mfUtil.isNullOrUndefined(startupPage)
        || mfUtil.isEmptyString(startupPage)){
        
        return false;
            
    }
    else{
        return true;
    }
};
Html5App.prototype.fnSetName = function (name/*string*/) {
    this.name = name;
};
Html5App.prototype.fnGetName = function () {
    return this.name;
};
Html5App.prototype.fnSetDescription = function (value/*string*/) {
    this.description = value;
};
Html5App.prototype.fnGetDescription = function () {
    return this.description;
};
Html5App.prototype.fnEditName = function (app,name) {
    var aryError = MF_HELPERS.ideValidation.validateAppName(name);
    if(aryError && $.isArray(aryError) && aryError.length>0){
        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(aryError),DialogType.Error);
        return;
    }
    app.fnSetName(name);
};
Html5App.prototype.fnEditDescription = function (app,description) {
    app.fnSetDescription(description);
};
Html5App.prototype.fnSetId = function (id/*string*/) {
    this.id = id;
};
Html5App.prototype.fnGetId = function () {
    return this.id;
};

function Html5AppFile(options){
    if (!options) options = {};
    this.name = options.name;
    this.size = options.size;
    this.lastEditedDate = options.lastEditedDate;
    this.editedBy = options.editedBy;
    this.fileType = options.fileType;//CONSTANTS.fileTypes
    this.dimensions= options.dimensions;
}
Html5AppFile.prototype.propSheetHtmlMapping = PROP_JSON_HTML_MAP.html5AppFile;
Html5AppFile.prototype.fnSetHtmlFileName = function (name/*string*/) {
    this.name  = name;
};
Html5AppFile.prototype.fnGetHtmlFilename = function () {
    return this.name;
};
Html5AppFile.prototype.fnGetFileSize = function () {
    return this.size;
};
Html5AppFile.prototype.fnSetFileSize = function (value) {
    this.size = value;
};
Html5AppFile.prototype.fnGetLastEditedDate = function () {
    return this.lastEditedDate;
};
Html5AppFile.prototype.fnSetLastEditedDate = function (value) {
    this.lastEditedDate = value;
};
Html5AppFile.prototype.fnGetEditedBy = function () {
    return this.editedBy;
};
Html5AppFile.prototype.fnSetEditedBy = function (value) {
    this.editedBy = value;
};
Html5AppFile.prototype.fnGetFileType = function () {
    return this.fileType;
};
Html5AppFile.prototype.fnSetFileType = function (value) {
    this.fileType = value;
};
Html5AppFile.prototype.fnGetDimensions = function () {
    return this.dimensions;
};
Html5AppFile.prototype.fnSetDimensions = function (value) {
    this.dimensions = value;
};


var mfExceptionMsgs = {
    "argumentsNull": "Arguments null exception",
    "argumentsInvalid": "Invalid arguments passed",
    "objectNotFound": "Object should be available.But could not get the value.",
    "filetypeNotFound":"File type was not found"
};
var mficientConstants = {
    jqueryCommonDataKey :"data"
};
var MF_HTML5_CONSTANTS ={
    fileTypes : {
        image :"1",
        html :"2",
        js:"3",
        css:"4",
        txt :"5",
        xml :"6",
        zip:"7"
    },
    errorMsgs :{
        invalidFileType :"File type is invalid"
    },
    imageTypes:{
        png:"1",
        jpg:"2",
        jpeg:"3"
    },
    rootPath:"root/",
    directorySeparator:"/"
};
var MF_HTML5_HELPERS=(function(){
    
    function _getFileTypeExtentionByEnumVal(
                    fileType/*MF_HTML5_CONSTANTS.fileTypes*/){
        var strExtension = "";
        switch (fileType) {
            case MF_HTML5_CONSTANTS.fileTypes.image:
                strExtension = ".png,.jpg,.jpeg";
                break;
            case MF_HTML5_CONSTANTS.fileTypes.js:
                strExtension =  ".js";
                break;
            case MF_HTML5_CONSTANTS.fileTypes.css:
                strExtension =  ".css";
                break;
            case MF_HTML5_CONSTANTS.fileTypes.html:
                strExtension = ".html";
                break;
            case MF_HTML5_CONSTANTS.fileTypes.xml:
                strExtension =  ".xml";
                break;
            case MF_HTML5_CONSTANTS.fileTypes.txt:
                strExtension =  ".txt";
                break;
            case MF_HTML5_CONSTANTS.fileTypes.zip:
                strExtension =  ".zip";
            break;    
        }
        return strExtension;
    }
    function _getImageTypeExtensionByEnumVal(imageType/*Contants.imageType*/){
        return ".jpeg";
    }
    return {
        
        getFileTypeEnumByStringVal:function(value){
            if(mfUtil.isNullOrUndefined(value)){
                throw mfExceptionMsgs.argumentsNull;
            }
            var fileType = "";
            switch(value){
                case "IMG" :
                    fileType = MF_HTML5_CONSTANTS.fileTypes.image;
                break;
                case "HTML" :
                    fileType = MF_HTML5_CONSTANTS.fileTypes.html;
                break;
                case "JS" :
                    fileType = MF_HTML5_CONSTANTS.fileTypes.js;
                break;
                case "CSS" :
                    fileType = MF_HTML5_CONSTANTS.fileTypes.css;
                break;
                case "TXT" :
                    fileType = MF_HTML5_CONSTANTS.fileTypes.txt;
                break;
                case "XML" :
                    fileType = MF_HTML5_CONSTANTS.fileTypes.xml;
                break;
                case "ZIP" :
                    fileType = MF_HTML5_CONSTANTS.fileTypes.zip;
                break;
            }
            return fileType;
        },
        getFileTypeExtentionByStringVal:function(value){
            if(mfUtil.isNullOrUndefined(value)){
                throw mfExceptionMsgs.argumentsNull;
            }
            var fileType = this.getFileTypeEnumByStringVal(value);
                
            if(mfUtil.isNullOrUndefined(fileType) || 
                mfUtil.isEmptyString(fileType)){
                throw MF_HTML5_CONSTANTS.errorMsgs.invalidFileType;    
            }
            return _getFileTypeExtentionByEnumVal(fileType);
        },
        getFileTypeStringValByEnumVal:function(value /*MF_HTML5_CONSTANTS.fileTypes*/){
            if(mfUtil.isNullOrUndefined(value)){
                throw mfExceptionMsgs.argumentsNull;
            }
            var fileType = "";
            switch (value) {
                case MF_HTML5_CONSTANTS.fileTypes.image:
                    fileType = "IMG";
                    break;
                case MF_HTML5_CONSTANTS.fileTypes.js:
                    fileType =  "JS";
                    break;
                case MF_HTML5_CONSTANTS.fileTypes.css:
                    fileType =  "CSS";
                    break;
                case MF_HTML5_CONSTANTS.fileTypes.html:
                    fileType = "HTML";
                    break;
                case MF_HTML5_CONSTANTS.fileTypes.xml:
                    fileType =  "XML";
                    break;
                case MF_HTML5_CONSTANTS.fileTypes.txt:
                    fileType =  "TXT";
                    break;
                case MF_HTML5_CONSTANTS.fileTypes.zip:
                    fileType =  "ZIP";
                break;    
            }
            return fileType;
        },
        getFileTypeExtentionByEnumVal:function(value){
            return _getFileTypeExtentionByEnumVal(value);
        }
    };
})();
(function ($) {
    $.uniform = {
        options: {
            selectClass: 'selector',
            radioClass: 'radio',
            checkboxClass: 'checker',
            fileClass: 'uploader',
            filenameClass: 'filename',
            fileBtnClass: 'action',
            fileDefaultText: 'No file selected',
            fileBtnText: 'Choose File',
            checkedClass: 'checked',
            focusClass: 'focus',
            disabledClass: 'disabled',
            buttonClass: 'button',
            activeClass: 'active',
            hoverClass: 'hover',
            useID: true,
            idPrefix: 'uniform',
            resetSelector: false,
            autoHide: true
        },
        elements: []
    };

    if ($.browser.msie && $.browser.version < 7) {
        $.support.selectOpacity = false;
    } else {
        $.support.selectOpacity = true;
    }

    $.fn.uniform = function (options) {

        options = $.extend($.uniform.options, options);

        var el = this;
        //code for specifying a reset button
        if (options.resetSelector != false) {
            $(options.resetSelector).mouseup(function () {
                function resetThis() {
                    $.uniform.update(el);
                }
                setTimeout(resetThis, 10);
            });
        }

        function doInput(elem) {
            $el = $(elem);
            $el.addClass($el.attr("type"));
            storeElement(elem);
        }

        function doTextarea(elem) {
            $(elem).addClass("uniform");
            storeElement(elem);
        }

        // function doButton(elem) {
        //     var $el = $(elem);
        //     if (!$el.is(":submit")) {

        //         var divTag = $("<div>"),
        //   spanTag = $("<span>");

        //         divTag.addClass(options.buttonClass);

        //         if (options.useID && $el.attr("id") != "") divTag.attr("id", options.idPrefix + "-" + $el.attr("id"));

        //         var btnText;

        //         if ($el.is("a") || $el.is("button")) {
        //             btnText = $el.text();
        //         } else if ($el.is(":submit") || $el.is(":reset") || $el.is("input[type=button]")) {
        //             btnText = $el.attr("value");
        //         }

        //         btnText = btnText == "" ? $el.is(":reset") ? "Reset" : "Submit" : btnText;

        //         spanTag.html(btnText);

        //         $el.css("opacity", 0);
        //         $el.wrap(divTag);
        //         $el.wrap(spanTag);

        //         //redefine variables
        //         divTag = $el.closest("div");
        //         spanTag = $el.closest("span");

        //         if ($el.is(":disabled")) divTag.addClass(options.disabledClass);

        //         divTag.bind({
        //             "mouseenter.uniform": function () {
        //                 divTag.addClass(options.hoverClass);
        //             },
        //             "mouseleave.uniform": function () {
        //                 divTag.removeClass(options.hoverClass);
        //                 divTag.removeClass(options.activeClass);
        //             },
        //             "mousedown.uniform touchbegin.uniform": function () {
        //                 divTag.addClass(options.activeClass);
        //             },
        //             "mouseup.uniform touchend.uniform": function () {
        //                 divTag.removeClass(options.activeClass);
        //             },
        //             "click.uniform touchend.uniform": function (e) {
        //                 if ($(e.target).is("span") || $(e.target).is("div")) {
        //                     if (elem[0].dispatchEvent) {
        //                         var ev = document.createEvent('MouseEvents');
        //                         ev.initEvent('click', true, true);
        //                         elem[0].dispatchEvent(ev);
        //                     } else {
        //                         elem[0].click();
        //                     }
        //                 }
        //             }
        //         });

        //         elem.bind({
        //             "focus.uniform": function () {
        //                 divTag.addClass(options.focusClass);
        //             },
        //             "blur.uniform": function () {
        //                 divTag.removeClass(options.focusClass);
        //             }
        //         });

        //         $.uniform.noSelect(divTag);
        //         storeElement(elem);
        //     }
        // }

        function doSelect(elem) {
            var $el = $(elem);

            var divTag = $('<div />'),
        spanTag = $('<span />');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.selectClass);

            if (options.useID && elem.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
            }

            var selected = elem.find(":selected:first");
            if (selected.length == 0) {
                selected = elem.find("option:first");
            }
            spanTag.html(selected.html());

            elem.css('opacity', 0);
            elem.wrap(divTag);
            elem.before(spanTag);

            //redefine variables
            divTag = elem.parent("div");
            spanTag = elem.siblings("span");
            //5/9/2012
            //elem.width(divTag.outerWidth(false));
            var elemWidth = 0; //9/11/2012
            if (divTag.outerWidth(false) == 10) //width of the div tag if elem initial width is zero
            {
                elemWidth = divTag.outerWidth(false) + spanTag.outerWidth(false) + 25 + 2 + 10;
                //where 25 and 2 is the padding provided to the span tag in jquery.uniform.css,10 is the padding provided to wrapperDiv in css
                //there is an additional 10px area due to the uniform image//adding that as well to work consistently
            } else {
                elemWidth = spanTag.outerWidth(false) + 25 + 2 + 10; //where 25,2 is the padding provided to the span tag in jquery.uniform.css
            }
            elem.width(elemWidth);
            elem.bind({
                "change.uniform": function () {
                    spanTag.text(elem.find(":selected").html());
                    divTag.removeClass(options.activeClass);
                },
                "focus.uniform": function () {
                    divTag.addClass(options.focusClass);
                },
                "blur.uniform": function () {
                    divTag.removeClass(options.focusClass);
                    divTag.removeClass(options.activeClass);
                },
                "mousedown.uniform touchbegin.uniform": function () {
                    divTag.addClass(options.activeClass);
                },
                "mouseup.uniform touchend.uniform": function () {
                    divTag.removeClass(options.activeClass);
                },
                "click.uniform touchend.uniform": function () {
                    divTag.removeClass(options.activeClass);
                },
                "mouseenter.uniform": function () {
                    divTag.addClass(options.hoverClass);
                },
                "mouseleave.uniform": function () {
                    divTag.removeClass(options.hoverClass);
                    divTag.removeClass(options.activeClass);
                },
                "keyup.uniform": function () {
                    spanTag.text(elem.find(":selected").html());
                }
            });

            //handle disabled state
            if ($(elem).attr("disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }
            $.uniform.noSelect(spanTag);

            storeElement(elem);
            //added on 19/12/2012 to set th width of the drop down according to the width of the selected text
            $(elem).change(function() {
                  setDropDownListWidthOnChange($(elem));
                })
            $(divTag).hover(
            function(){
                var elemSiblingSpan = $(elem).siblings("span");
                if($(elem).outerWidth() < $(elemSiblingSpan).outerWidth()){
                setDropDownListWidthOnChange($(elem)) ;
                }
            })
               
        }

        function doCheckbox(elem) {
            var $el = $(elem);
            var divTag = $('<div />'),
        spanTag = $('<span />');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.checkboxClass);

            //assign the id of the element
            if (options.useID && elem.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
            }

            //wrap with the proper elements
            $(elem).wrap(divTag);
            $(elem).wrap(spanTag);

            //redefine variables
            spanTag = elem.parent();
            divTag = spanTag.parent();

            //hide normal input and add focus classes
            $(elem)
        .css("opacity", 0)
        .bind({
            "focus.uniform": function () {
                divTag.addClass(options.focusClass);
            },
            "blur.uniform": function () {
                divTag.removeClass(options.focusClass);
            },
            "click.uniform touchend.uniform": function () {
                if (!$(this).is(":checked")) {
                    //box was just unchecked, uncheck span
                    spanTag.removeClass(options.checkedClass);
                } else {
                    //box was just checked, check span.
                    spanTag.addClass(options.checkedClass);
                }
                
                // if (!$(this).attr("checked")) {
                //     //box was just unchecked, uncheck span
                //     spanTag.removeClass(options.checkedClass);
                // } else {
                //     //box was just checked, check span.
                //     spanTag.addClass(options.checkedClass);
                // }
            },
            "mousedown.uniform touchbegin.uniform": function () {
                divTag.addClass(options.activeClass);
            },
            "mouseup.uniform touchend.uniform": function () {
                divTag.removeClass(options.activeClass);
            },
            "mouseenter.uniform": function () {
                divTag.addClass(options.hoverClass);
            },
            "mouseleave.uniform": function () {
                divTag.removeClass(options.hoverClass);
                divTag.removeClass(options.activeClass);
            }
        });

            //handle defaults
            if ($(elem).is(":checked")) {
                //box is checked by default, check our box
                spanTag.addClass(options.checkedClass);
            }

            //handle disabled state
            if ($(elem).is(":disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }

            storeElement(elem);
        }

        function doRadio(elem) {
            var $el = $(elem);

            var divTag = $('<div />'),
        spanTag = $('<span />');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.radioClass);

            if (options.useID && elem.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
            }

            //wrap with the proper elements
            $(elem).wrap(divTag);
            $(elem).wrap(spanTag);

            //redefine variables
            spanTag = elem.parent();
            divTag = spanTag.parent();

            //hide normal input and add focus classes
            $(elem)
        .css("opacity", 0)
        .bind({
            "focus.uniform": function () {
                divTag.addClass(options.focusClass);
            },
            "blur.uniform": function () {
                divTag.removeClass(options.focusClass);
            },
            "click.uniform touchend.uniform": function () {
                if (!$(elem).is(":checked")) {
                    //box was just unchecked, uncheck span
                    spanTag.removeClass(options.checkedClass);
                } else {
                    //box was just checked, check span
                    var classes = options.radioClass.split(" ")[0];
                    $("." + classes + " span." + options.checkedClass + ":has([name='" + $(elem).attr('name') + "'])").removeClass(options.checkedClass);
                    spanTag.addClass(options.checkedClass);
                }
            },
            "mousedown.uniform touchend.uniform": function () {
                if (!$(elem).is(":disabled")) {
                    divTag.addClass(options.activeClass);
                }
            },
            "mouseup.uniform touchbegin.uniform": function () {
                divTag.removeClass(options.activeClass);
            },
            "mouseenter.uniform touchend.uniform": function () {
                divTag.addClass(options.hoverClass);
            },
            "mouseleave.uniform": function () {
                divTag.removeClass(options.hoverClass);
                divTag.removeClass(options.activeClass);
            }
        });

            //handle defaults
            if ($(elem).is(":checked")) {
                //box is checked by default, check span
                spanTag.addClass(options.checkedClass);
            }
            //handle disabled state
            if ($(elem).is(":disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }

            storeElement(elem);

        }

        function doFile(elem) {
            //sanitize input
            var $el = $(elem);
            if( $el && $($el.parent()[0]).attr('class') !== "uploader"){//8/5/2013 Mohan
            
                var divTag = $('<div />'),
        filenameTag = $('<span>' + options.fileDefaultText + '</span>'),
        btnTag = $('<span>' + options.fileBtnText + '</span>');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.fileClass);
            filenameTag.addClass(options.filenameClass);
            btnTag.addClass(options.fileBtnClass);

            if (options.useID && $el.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + $el.attr("id"));
            }

            //wrap with the proper elements
            $el.wrap(divTag);
            $el.after(btnTag);
            $el.after(filenameTag);

            //redefine variables
            divTag = $el.closest("div");
            filenameTag = $el.siblings("." + options.filenameClass);
            btnTag = $el.siblings("." + options.fileBtnClass);

            //set the size
            if (!$el.attr("size")) {
                var divWidth = divTag.width();
                //$el.css("width", divWidth);
                $el.attr("size", divWidth / 10);
            }

            //actions
            var setFilename = function () {
                var filename = $el.val();
                if (filename === '') {
                    filename = options.fileDefaultText;
                } else {
                    filename = filename.split(/[\/\\]+/);
                    filename = filename[(filename.length - 1)];
                }
                filenameTag.text(filename);
            };

            // Account for input saved across refreshes
            setFilename();

            $el.css("opacity", 0)
        .bind({
            "focus.uniform": function () {
                divTag.addClass(options.focusClass);
            },
            "blur.uniform": function () {
                divTag.removeClass(options.focusClass);
            },
            "mousedown.uniform": function () {
                if (!$(elem).is(":disabled")) {
                    divTag.addClass(options.activeClass);
                }
            },
            "mouseup.uniform": function () {
                divTag.removeClass(options.activeClass);
            },
            "mouseenter.uniform": function () {
                divTag.addClass(options.hoverClass);
            },
            "mouseleave.uniform": function () {
                divTag.removeClass(options.hoverClass);
                divTag.removeClass(options.activeClass);
            }
        });

            // IE7 doesn't fire onChange until blur or second fire.
            if ($.browser.msie) {
                // IE considers browser chrome blocking I/O, so it
                // suspends tiemouts until after the file has been selected.
                $el.bind('click.uniform.ie7', function () {
                    setTimeout(setFilename, 0);
                });
            } else {
                // All other browsers behave properly
                $el.bind('change.uniform', setFilename);
            }

            //handle defaults
            if ($el.attr("disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }

            $.uniform.noSelect(filenameTag);
            $.uniform.noSelect(btnTag);

            storeElement(elem);
            }
            
            

        }

        $.uniform.restore = function (elem) {
            if (elem == undefined) {
                elem = $($.uniform.elements);
            }

            $(elem).each(function () {
                if ($(this).is(":checkbox")) {
                    //unwrap from span and div
                    $(this).unwrap().unwrap();
                } else if ($(this).is("select")) {
                    //remove sibling span
                    $(this).siblings("span").remove();
                    //unwrap parent div
                    $(this).unwrap();
                } else if ($(this).is(":radio")) {
                    //unwrap from span and div
                    $(this).unwrap().unwrap();
                } else if ($(this).is(":file")) {
                    //remove sibling spans
                    $(this).siblings("span").remove();
                    //unwrap parent div
                    $(this).unwrap();
                } else if ($(this).is("button, :submit, :reset, a, input[type='button']")) {
                    //unwrap from span and div
                    $(this).unwrap().unwrap();
                }

                //unbind events
                $(this).unbind(".uniform");

                //reset inline style
                $(this).css("opacity", "1");

                //remove item from list of uniformed elements
                var index = $.inArray($(elem), $.uniform.elements);
                $.uniform.elements.splice(index, 1);
            });
        };

        function storeElement(elem) {
            //store this element in our global array
            elem = $(elem).get();
            if (elem.length > 1) {
                $.each(elem, function (i, val) {
                    $.uniform.elements.push(val);
                });
            } else {
                $.uniform.elements.push(elem);
            }
        }

        //noSelect v1.0
        $.uniform.noSelect = function (elem) {
            function f() {
                return false;
            };
            $(elem).each(function () {
                this.onselectstart = this.ondragstart = f; // Webkit & IE
                $(this)
          .mousedown(f) // Webkit & Opera
        .css({
            MozUserSelect: 'none'
        }); // Firefox
            });
        };

        $.uniform.update = function (elem) {
            if (elem == undefined) {
                elem = $($.uniform.elements);
            }
            //sanitize input
            elem = $(elem);

            elem.each(function () {
                //do to each item in the selector
                //function to reset all classes
                var $e = $(this);

                if ($e.is("select")) {
                    //element is a select
                    var spanTag = $e.siblings("span");
                    var divTag = $e.parent("div");

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);

                    //reset current selected text
                    spanTag.html($e.find(":selected").html());

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }

                } else if ($e.is(":checkbox")) {
                    //element is a checkbox
                    var spanTag = $e.closest("span");
                    var divTag = $e.closest("div");

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);
                    spanTag.removeClass(options.checkedClass);

                    if ($e.is(":checked")) {
                        spanTag.addClass(options.checkedClass);
                    }
                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }

                } else if ($e.is(":radio")) {
                    //element is a radio
                    var spanTag = $e.closest("span");
                    var divTag = $e.closest("div");

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);
                    spanTag.removeClass(options.checkedClass);

                    if ($e.is(":checked")) {
                        spanTag.addClass(options.checkedClass);
                    }

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }
                } else if ($e.is(":file")) {
                    var divTag = $e.parent("div");
                    var filenameTag = $e.siblings(options.filenameClass);
                    btnTag = $e.siblings(options.fileBtnClass);

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);

                    filenameTag.text($e.val());

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }
                } else if ($e.is(":submit") || $e.is(":reset") || $e.is("button") || $e.is("a") || elem.is("input[type=button]")) {
                    var divTag = $e.closest("div");
                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }

                }

            });
        };

        return this.each(function () {
            if ($.support.selectOpacity) {
                var elem = $(this);

                if (elem.is("select")) {
                    //element is a select
                    if (elem.attr("multiple") != true) {
                        //element is not a multi-select
                        if (elem.attr("size") == undefined || elem.attr("size") <= 1) {
                            doSelect(elem);
                        }
                    }
                } else if (elem.is(":checkbox")) {
                    //element is a checkbox
                    doCheckbox(elem);
                } else if (elem.is(":radio")) {
                    //element is a radio
                    doRadio(elem);
                } else if (elem.is(":file")) {
                    //element is a file upload
                    doFile(elem);
                } else if (elem.is(":text, :password, input[type='email']")) {
                    doInput(elem);
                } else if (elem.is("textarea")) {
                    doTextarea(elem);
                } else if (elem.is("a") || elem.is(":submit") || elem.is(":reset") || elem.is("button") || elem.is("input[type=button]")) {
                    //doButton(elem);
                }

            }
        });
    };
})(jQuery);
/* {
appid :"",
files :{
image:[],
style:[],
script:[],
root:[]
}
}*/

function createFileTree() {
    //showProgressBar(true);
    $('[id$=txtFilePath]').val('');
    $("#lstAppFiles li").remove();
    var imageHtml = '<li class="parent collapsed" id="appImages"><a style="padding:0px 2px;">images</a>';
    var scriptHtml = '<li class="parent collapsed" id="appScripts"><a style="padding:0px 2px;">scripts</a>';
    var styleHtml = '<li class="parent collapsed" id="appStyles"><a style="padding:0px 2px;">styles</a>';
    var rootHtml = '';
    if (appJson != null || appJson != undefined) {
        var obj = jQuery.parseJSON(appJson);
        if (obj != null) {
            if (obj.files.image.length > 0) {
                imageHtml += '<ul class="child" style="list-style:none;">';
                $.each(obj.files.image, function () {
                    imageHtml += '<li class = "' + getFileImageStyle(this["ext"]) + '" id="' + this["fileid"] + '"><a style="padding:0px 2px;">' + this["filenm"] + '</a></li>';
                });

                imageHtml += '</ul>';
            }
            imageHtml += '</li>';
            $("#lstAppFiles").append(imageHtml);

            if (obj.files.script.length > 0) {
                scriptHtml += '<ul class="child" style="list-style:none;">';
                $.each(obj.files.script, function () {
                    scriptHtml += '<li class = "' + getFileImageStyle(this["ext"]) + '" id="' + this["fileid"] + '"><a style="padding:0px 2px;">' + this["filenm"] + '</a></li>';
                });

                scriptHtml += '</ul>';
            }
            scriptHtml += '</li>';
            $("#lstAppFiles").append(scriptHtml);

            if (obj.files.style.length > 0) {
                styleHtml += '<ul class="child" style="list-style:none;">';
                $.each(obj.files.style, function () {
                    styleHtml += '<li class = "' + getFileImageStyle(this["ext"]) + '" id="' + this["fileid"] + '"><a style="padding:0px 2px;">' + this["filenm"] + '</a></li>';
                });

                styleHtml += '</div>';
            }
            styleHtml += '</li>';

            $("#lstAppFiles").append(styleHtml);

            if (obj.files.root.length > 0) {
                $.each(obj.files.root, function () {
                    rootHtml += '<li class="parent ' + getFileImageStyle(this["ext"]) + '" id="' + this["fileid"] + '"><a style="padding:0px 2px;">' + this["filenm"] + '</a></li>';
                });
            }
            $("#lstAppFiles").append(rootHtml);
        }
    }

    showFileUploader(false);
    $('.child').hide();
    $('li').click(function () {
        if ($(this).hasClass('parent') && !isChildClicked) {
            $(this).find('ul').slideToggle();
            if ($(this).hasClass('collapsed') && $(this).find('ul')[0] != undefined) {
                $(this).removeClass('collapsed').addClass('expanded');
            }
            else if ($(this).hasClass('expanded')) {
                $(this).removeClass('expanded').addClass('collapsed');
            }

            $('[id$=txtFilePath]').val($(this).find('a')[0].innerText);
        }
        else
            isChildClicked = false;

    });
    $('.child li').click(function () {
        isChildClicked = true;
        switch ($(this)[0].className) {
            case "imagefile":
                $('[id$=txtFilePath]').val("images/" + $(this)[0].id);
                break;
            case "jsfile":
                $('[id$=txtFilePath]').val("scripts/" + $(this)[0].id);
                break;
            case "cssfile":
                $('[id$=txtFilePath]').val("styles/" + $(this)[0].id);
                break;
            default:
                $('[id$=txtFilePath]').val($(this)[0].id);
                break;
        }
    });
}

function getFileImageStyle(extension) {
    switch (extension) {
        case ".png":
        case ".jpeg":
        case "jpg":
            return "imagefile";
            break;
        case ".js":
            return "jsfile";
            break;
        case ".css":
            return "cssfile";
            break;
        case ".htm":
        case ".html":
            return "htmlfile";
        case ".xml":
            return "xmlfile";
        case ".txt":
            return "textfile";
            break;
    }
}

function addAppId() {
    $('[id$=hdHtml5AppObj]').val(appId);
}



function getFileExtension(fileType) {
    switch (fileType) {
        case "IMG":
            return ".png,.jpg,.jpeg";
            break;
        case "JS":
            return ".js";
            break;
        case "CSS":
            return ".css";
            break;
        case "HTML":
            return ".htm,.html";
        case "XML":
            return ".xml";
        case "TXT":
            return ".txt";
            break;
        case "ZIP":
            return ".zip";
    }
}
function makeTabAfterPostBack() {
    $('#content').find('div.tab').tabs({
        fx: {
            opacity: 'toggle',
            duration: 'fast'
        }
    });
}
function storeSelectedTabIndex(index) {
    var hidSelectedTabIndex = $('[id*=hidTabSelected]');
    $(hidSelectedTabIndex).val('');
    $(hidSelectedTabIndex).val(index);
}
function showSelectedTabOnPostBack() {

    var hidSelectedTabIndex = $('[id*=hidTabSelected]');
    //this is the first tab and associated div
    var lstFiles = $('#lstTabFiles');
    var filesDiv = $('#filesDiv');
    //this is the second tab and associated div
    var lstProperties = $('#lstTabProperties');
    var propertiesDiv = $('#propertiesDiv');


    switch (parseInt($(hidSelectedTabIndex).val(), 10)) {
        case TabEnum.FILES:
            lstFiles
                    .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");

            lstProperties
                    .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");



            filesDiv.removeClass("ui-tabs-hide");
            propertiesDiv.addClass("ui-tabs-hide");

            break;
        case TabEnum.PROPERTIES:

            lstFiles.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

            lstProperties.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");

            filesDiv.addClass("ui-tabs-hide");
            propertiesDiv.removeClass("ui-tabs-hide");

            break;
    }
}

function showHtml5AddNewFileDiv(_bol) {
    if (_bol) {
        $('[id$=rdbFileTypes]').find("input[value='ZIP']").attr("checked", "checked");
        $('[id$=rdbFileTypes]').change();
        $('[id$=rdbCreateMode]').find("input[value='UPL']").attr("checked", "checked");
        $('[id$=rdbCreateMode]').change();
        showModalPopUp('SubProcAddNewFile', " Add New File(s) ", 480, false, false);
    }
    else {
        $('#SubProcAddNewFile').dialog('close');
    }
}

function showCreateNewFileDiv(_bol) {
    if (_bol) {
        if ($('[id$=rdbFileTypes]').find(":checked").val() == "IMG" || $('[id$=rdbFileTypes]').find(":checked").val() == "ZIP")
            showFileUploader(true);
        else
            showModalPopUp('SubProcCreateNewFile', " New File ", 480, false, false);
    }
    else {
        $('#SubProcCreateNewFile').dialog('close');
    }
}
function showFileUploader(_bol) {
    if (_bol) {
        if ($('[id$=rdbCreateMode]').find(":checked").val() == "UPL")
            showModalPopUp('divFileUploaderDragDrop', "File Uploader", 480, false, false);
    }
    else {
        $('#divFileUploaderDragDrop').dialog('close');
    }
}

function showErrorBox(_bol, invalidFiles, invalidFilePaths) {
    if (_bol) {
        showFileUploader(false);
        if (invalidFiles != "") {
            $('#InvalidFilesDiv span').html('Following are Invalid Files :' + '<br /><br />' + invalidFiles);
            $('#InvalidFilesDiv').show();
        }
        if (invalidFilePaths != "") {
            $('#InvalidFilePathDiv span').html('Following Files are not having valid Paths:' + '<br /><br />' + invalidFilePaths);
            $('#InvalidFilePathDiv').show();
        }
        showModalPopUp('SubProcErrorBox', "Errors", 480, false, false);
    }
    else {
        $('#SubProcErrorBox').dialog('close');
    }
}
function showErrorMsgBox(_bol, errorMsgs) {
    if (_bol) {
        $('#ErrorMsgDiv span').html(errorMsgs);
        showModalPopUp('SubProcErrorMessageBox', "Errors", 480, false, false);
    }
    else {
        $('#SubProcErrorMessageBox').dialog('close');
    }
}

function createNewFileModeonChange(e) {
    var value = $(e).find(":checked").val()
    if (value == "CRT") {
        $('#divHtml5NewFileNameCont').show();
    }
    else if (value == "UPL") {
        $('#divHtml5NewFileNameCont').hide();
    }
    var fileType = $('[id$=rdbFileTypes]').find(":checked").val();
    $('[id$=lblFileExt]').html(getFileExtension(fileType));
}

function showProgressBar(_bol) {
    if (_bol) {
        showFileUploader(false);
        var bar = $("#progress").progressbar();
        bar.progress(1);
        $('#shadow').show();
    }
    else {
        $('#shadow').hide();
    }
}

// var mfHtml5AppDesignController=(function(){
    
    
//     function _destroyAppInDesigner(){
//         mfHtml5App.destroyHtmlObjects.destroyCompleteAppObject();
//     }
//     return {
        
//         ;
//     };
    
// })();

var mfHtml5App = (function(){
    function _handleFilesOnDrop(file) {
        //var imageType = /image.*/;
        var reader = new FileReader();
        reader.onload = (function (file) {
            return function (e) {
                _hidFieldsHelper.hidDragDropFileName.setValue(file.name);
                _hidFieldsHelper.hidDragDropFileBytes.setValue(
                    e.target.result.split(',')[1]);
            };
        })(file);
        reader.onerror = function () {
            alert('An error encountered while reading the file.Please try again.');
        };
        reader.onloadend = function () {
           if (reader.readyState === 2 &&
                mfUtil.isNullOrUndefined(reader.error)){
                _fileUploaderDragDrop.createNewFile();
            }
        };
        reader.readAsDataURL(file);
    }
    function _handleFilesOnUploadFileChange(file){
        var reader = new FileReader();
        reader.onload = (function (file) {
            return function (e) {
                _hidFieldsHelper.hidDragDropFileName.setValue(file.name);
                _hidFieldsHelper.hidDragDropFileBytes.setValue(
                    e.target.result.split(',')[1]);
            };
        })(file);
        reader.onerror = function () {
            alert('An error encountered while reading the file.Please try again.');
        };
        reader.onloadend = function () {
           if (reader.readyState === 2 &&
                mfUtil.isNullOrUndefined(reader.error)){
            }
        };
        reader.readAsDataURL(file);
    }
    function _getConfirmationMsgOfNoRootFolderExists(){
     var  strConfirmMsg = "";
     strConfirmMsg += "No root folder exists for app.";
     strConfirmMsg += "If the file is saved a root folder with ";
     strConfirmMsg += "application name will be created and the file will";
     strConfirmMsg += "be added to this root folder.Do you want to continue.";
     return strConfirmMsg;
 }
    function _getConfirmationMsgOfFileExistense(fileName,path){
        var strMessage = "";
        strMessage += "A file with name <b> \""+fileName+"\"</b> ";
        strMessage += "already exists in path <b>\" "+path+"\"</b> </br>";
        strMessage += "Do you want to overwrite it.";
        return strMessage;
     }
    
    //General helper functions
    //function _getConfirmationMsgOfFilePath(path){
    //      var  strConfirmMsg = "";
    //      if(mfUtil.isEmptyString(path)){
    //          strConfirmMsg = 'The file will be added to the root folder.Do you want to continue.';
    //      }
    //      else{
    //          strConfirmMsg = 'The file will be added to the path <b>'+path+'</b> .Do you want to continue.';
    //      }
         
    //      return strConfirmMsg;
    //  }
    function _getFileNameWithExtension(fileName,fileType){
        return fileName.trim()+MF_HTML5_HELPERS.getFileTypeExtentionByEnumVal(fileType);
    }
    /*
        Create New File
    */
    function _validateCreateNewFileName(fileName){
    
        var aryErrors = [],
            strFileName = fileName;
        if(mfUtil.isEmptyString(strFileName)){
            aryErrors.push("Please provide a file name.");
        }
        return aryErrors;
    }
    /*
        Create New File
    */
    function _getSelectedFilePathInfoForUI(){
        var strSelectedPath = _hidFieldsHelper.hidAppFolderSelected.getValue();
        if(mfUtil.isNullOrUndefined(strSelectedPath) ||
            mfUtil.isEmptyString(strSelectedPath)){
             
            return "root/";    
        }
        else{
            return strSelectedPath;  
        }
    }
    function _getOptionsForZipFolderPathDdl(){
        var aryAllFoldersInZip = _fileExplorer.getAllFoldersFromHidField(),
            options = "",
            i=0;
        if($.isArray(aryAllFoldersInZip) && aryAllFoldersInZip.length>0){
           for(i=0;i<=aryAllFoldersInZip.length-1;i++){
                options += '<option value="'+aryAllFoldersInZip[i]+'">'+aryAllFoldersInZip[i]+'</option>';
           }
        }
        else{
            options += '<option value="'+MF_HTML5_CONSTANTS.rootPath+'">'+MF_HTML5_CONSTANTS.rootPath+'</option>';
        }
        return options;
    }
    function _clearFileSelectedPath(){
        _getPageHtmlCntrls.hiddenFields.hidAppFolderSelected().val('');
    }
    
    
    //final processes
    function _processSaveNewHtml5App(){
        try{  
            var $txtHtml5AppName = _getPageHtmlCntrls.createNewHtml5App.txtHtml5AppName();
            var strAppName = $txtHtml5AppName.val(),
                 aryErrors = MF_HELPERS.ideValidation.validateAppName(strAppName);
             if($.isArray(aryErrors) && aryErrors.length>0){
                 throw new MfError(aryErrors);
             }
             processPostbackByHtmlCntrl.
             postBack(
                 postBackByHtmlProcess.AddNewHtml5App,
                 [strAppName],
             "");
        }
        catch(error){
            if (error instanceof MfError){
                showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
            }else{
                console.log(error);
            }
        }
     }
    function _processUpdateHtml5App(){
         var objApp = _html5ObjectPropSheet.getCurrentWorkingApp();
         if(objApp){
           _hidFieldsHelper.hidHtml5AppDetail.setValue(JSON.stringify(objApp));
           processPostbackByHtmlCntrl.
           postBack(
               postBackByHtmlProcess.UpdateHtml5App,
               [],
            "");
        } 
     }
    function _processSaveAsHtml5App(){
        var objApp = _html5ObjectPropSheet.getCurrentWorkingApp();
         if(objApp){
           _hidFieldsHelper.hidHtml5AppDetail.setValue(JSON.stringify(objApp));
           var $txtHtml5AppName = _getPageHtmlCntrls.saveAsHtml5App.txtHtml5AppSaveAsName();
           var strAppName = $txtHtml5AppName.val(),
                aryErrors = MF_HELPERS.ideValidation.validateAppName(strAppName);
            if($.isArray(aryErrors) && aryErrors.length>0){
                throw new MfError(aryErrors);
            }    
           processPostbackByHtmlCntrl.
           postBack(
               postBackByHtmlProcess.Html5AppSaveAs,
               [strAppName],
            "");
        } 
    }
    function _processCommitHtml5App(){
        var objApp = _html5ObjectPropSheet.getCurrentWorkingApp();
         if(objApp){
           _hidFieldsHelper.hidHtml5AppDetail.setValue(JSON.stringify(objApp));
           processPostbackByHtmlCntrl.
           postBack(
               postBackByHtmlProcess.Html5AppCommit,
               [objApp.fnGetId()],
            "");
        } 
    }
    function _processCommitHtml5AppSave(){
        var objApp = _html5ObjectPropSheet.getCurrentWorkingApp();
         if(objApp){
           _hidFieldsHelper.hidHtml5AppDetail.setValue(JSON.stringify(objApp));
           processPostbackByHtmlCntrl.
           postBack(
               postBackByHtmlProcess.Html5AppCommitSave,
               [objApp.id],
            "");
        } 
    }
    function _processCommitHtml5AppForTester(){
        var objApp = _html5ObjectPropSheet.getCurrentWorkingApp();
         if(objApp){
           _hidFieldsHelper.hidHtml5AppDetail.setValue(JSON.stringify(objApp));
           processPostbackByHtmlCntrl.
           postBack(
               postBackByHtmlProcess.Html5AppTesterCommit,
               [objApp.id],
            "");
        } 
    }
    function _processDownloadZipFile(){
        var objApp = _html5ObjectPropSheet.getCurrentWorkingApp();
         if(objApp){
           $.confirm('Would you like to save the changes before downloading.'
                    ,function(){
                      _hidFieldsHelper.hidHtml5AppDetail.setValue(JSON.stringify(objApp));
                      processPostbackByHtmlCntrl.
                      postBack(
                          postBackByHtmlProcess.Html5AppZipDownload,
                          [objApp.fnGetId(),true],
                       "");
                    },function(){
                             alert('don\'t save before downloading');
                    });
                showDialogImage(DialogType.Warning);
                
        } 
    }
    function _processSaveNewFolder(){
        var objApp = _html5ObjectPropSheet.getCurrentWorkingApp();
        if(objApp){
            
            var $txtHtml5AppNewFolderName = _getPageHtmlCntrls.divGetNewFolderName.txtHtml5AppNewFolderName();
            var $ddlHtml5AppNewFolderSavePath = _getPageHtmlCntrls.divGetNewFolderName.ddlHtml5AppNewFolderSavePath();
            var strFolderName = $txtHtml5AppNewFolderName.val(),
                strFolderSavePath =$ddlHtml5AppNewFolderSavePath.val();
            
            if(mfUtil.isNullOrUndefined(strFolderName) 
                || mfUtil.isEmptyString(strFolderName)){
                    throw new MfError(['Please enter folder name.']);
                }
           
           processPostbackByHtmlCntrl.
           postBack(
               postBackByHtmlProcess.Html5AppAddNewFolder,
               [strFolderName,strFolderSavePath,objApp.fnGetId()],
            "");
        } 
    }
    function _appIconSelected(iconName){
        var objApp = _html5ObjectPropSheet.getCurrentWorkingApp();
         if(objApp){
           var iconCntrl = PROP_JSON_HTML_MAP.html5App.propSheetCntrl.Properties.getAppIcon();
           if(! iconCntrl)throw MF_IDE_CONSTANTS.exceptionMessage.objectNotFound;
           objApp.icon = iconName;
           iconCntrl.control.attr('src',MF_IDE_CONSTANTS.EnterpriseIconPrefix+iconName);
           closeModalPopUp('divNewAppIconContainer');
        } 
    }
    function _processHtml5AppDelete(){
        var objApp = _html5ObjectPropSheet.getCurrentWorkingApp();
        if(objApp){
            var strAppId = objApp.fnGetId();
            $.confirm("Are you sure you want to delete this app.",
                    function(){
                        processPostbackByHtmlCntrl.
                        postBack(
                            postBackByHtmlProcess.Html5AppDelete,
                            [strAppId],
                         "");
                    },null);
            showDialogImage(DialogType.Warning);
        }
    }
    function _processHtml5AppClose(){
        var objApp = _html5ObjectPropSheet.getCurrentWorkingApp();
        if(objApp){
            var strAppId = objApp.fnGetId();
            var blnAppZipExists = _fileExplorer.doesFileExplorerDataExists();
            if(blnAppZipExists){
            
                $.confirm("Do you want to save the app before closing.",
                        function(){
                            processPostbackByHtmlCntrl.
                            postBack(
                                postBackByHtmlProcess.Html5AppClose,
                                [strAppId,true],
                             "");
                        },
                        function(){
                            processPostbackByHtmlCntrl.
                            postBack(
                                postBackByHtmlProcess.Html5AppClose,
                                [strAppId,false],
                             "");
                        });
                showDialogImage(DialogType.Warning);
            }
            else{
                $.confirm("No file is added yet in the app.Do you want to keep the blank app.",
                    function(){
                        processPostbackByHtmlCntrl.
                        postBack(
                            postBackByHtmlProcess.Html5AppClose,
                            [strAppId,false],
                         "");
                    },
                    function(){
                        processPostbackByHtmlCntrl.
                        postBack(
                            postBackByHtmlProcess.Html5AppDelete,
                            [strAppId],
                         "");
                    });
                showDialogImage(DialogType.Warning);
            }
            //_destroyHtmlObjects.destroyCompleteAppObject();
        }
    }
    //end final processes
    
    
    /*
        Create New File 
    */
    function _createNewFileHelper(options){
        //fileNameWithExtension,fileType,useFileBytes,confirmCancelCallBack
        
        var strSelectedFilePath =_hidFieldsHelper.hidAppFolderSelected.getValue(),
            blnAppTreeJsonExists = _fileExplorer.doesFileExplorerDataExists();
        
        var strFileNameWithExtension = options.fileNameWithExtension,
            fileType = options.fileType,
            useFileBytes = options.useFileBytes,
            confirmCancelCallBack = options.confirmCancelCallBack,
            objApp = _html5ObjectPropSheet.getCurrentWorkingApp(),
            strAppName = objApp.fnGetName(),
            strAppId = objApp.fnGetId(),
            data = [],
            strConfirmMsg = "";
             
        if( (mfUtil.isNullOrUndefined(strSelectedFilePath) 
             || mfUtil.isEmptyString(strSelectedFilePath))
             
             &&
             blnAppTreeJsonExists === true
            ){
                
            strSelectedFilePath = _fileExplorer.getRootPathOfFileExplorer();        
                
        }     
        if(!blnAppTreeJsonExists){
            //this means there is no zip file already and hence no file i s being replaced.
            data =[
                fileType,
                strFileNameWithExtension,
                "",
                useFileBytes,
                strAppName,
                false,
                strAppId
             ];
            processPostbackByHtmlCntrl.postBack(
                postBackByHtmlProcess.NewHtml5FileAdd,
                 data, "");
            //strConfirmMsg = _getConfirmationMsgOfNoRootFolderExists();
            
            // $.confirm(strConfirmMsg,
            //     function(){
                    
            //     },
            //     $.isFunction(confirmCancelCallBack)=== true?confirmCancelCallBack:null
            //     // function(){
            //     //     _createNewFile.deleteDataOfContDiv();
            //     // }
            // );
            // showDialogImage(DialogType.Warning);
        }
        else{
            
            if(_fileExplorer.doesFileExists(
                 strSelectedFilePath,
                 strFileNameWithExtension
             )){
                     
               strConfirmMsg = _getConfirmationMsgOfFileExistense(
                        strFileNameWithExtension,
                         strSelectedFilePath);  
                $.confirm(strConfirmMsg,
                    function(){
                        data =[
                            fileType,
                            strFileNameWithExtension,
                            strSelectedFilePath,
                            useFileBytes,
                            strAppName,
                            true,
                            strAppId
                         ];
                        processPostbackByHtmlCntrl.postBack(
                            postBackByHtmlProcess.NewHtml5FileAdd,
                             data, "");
                    },
                    $.isFunction(confirmCancelCallBack)=== true?confirmCancelCallBack:null
                );
                showDialogImage(DialogType.Warning);
             }
            else{
                data =[
                    fileType,
                    strFileNameWithExtension,
                    strSelectedFilePath,
                    useFileBytes,
                    strAppName,
                    false,
                    strAppId
                 ];
                processPostbackByHtmlCntrl.postBack(
                    postBackByHtmlProcess.NewHtml5FileAdd,
                     data, "");
            }
        }
    }
    
    var _hidFieldsHelper =(function(){
        
        return{
           hidAppFolderSelected : {
               getValue :function(){
                   return _getPageHtmlCntrls.hiddenFields.hidAppFolderSelected().val();
               },
               setValue :function(value){
                   _getPageHtmlCntrls.hiddenFields.hidAppFolderSelected().val(value);
               }
           },
           hidDragDropFileName:{
               getValue :function(){
                   return _getPageHtmlCntrls.hiddenFields.hidDragDropFileName().val();
               },
               setValue :function(value){
                   _getPageHtmlCntrls.hiddenFields.hidDragDropFileName().val(value);
               }
           },
           hidDragDropFileBytes:{
               getValue :function(){
                   return _getPageHtmlCntrls.hiddenFields.hidDragDropFileBytes().val();
               },
               setValue :function(value){
                   _getPageHtmlCntrls.hiddenFields.hidDragDropFileBytes().val(value);
               }
           },
           hidAppFileSelected :{
               getValue :function(){
                   return _getPageHtmlCntrls.hiddenFields.hidAppFileSelected().val();
               },
               setValue :function(value){
                   _getPageHtmlCntrls.hiddenFields.hidAppFileSelected().val(value);
               }
           },
           hidSelectedFileContent:{
               getValue :function(){
                   return _getPageHtmlCntrls.hiddenFields.hidSelectedFileContent().val();
               },
               setValue :function(value){
                   _getPageHtmlCntrls.hiddenFields.hidSelectedFileContent().val(value);
               }
           },
           hidHtml5AppDetail:{
                getValue :function(){
                    return _getPageHtmlCntrls.hiddenFields.hidHtml5AppDetail().val();
                },
                setValue :function(value){
                    _getPageHtmlCntrls.hiddenFields.hidHtml5AppDetail().val(value);
                }
            },
           hidHtml5AppFileDetail:{
                getValue :function(){
                  return _getPageHtmlCntrls.hiddenFields.hidHtml5AppFileDetail().val();
                },
                setValue :function(value){
                  _getPageHtmlCntrls.hiddenFields.hidHtml5AppFileDetail().val(value);
                }
           },
           hidFoldersForFileMove:{
                 getValue :function(){
                   return _getPageHtmlCntrls.hiddenFields.hidFoldersForFileMove().val();
                 },
                 setValue :function(value){
                   _getPageHtmlCntrls.hiddenFields.hidFoldersForFileMove().val(value);
                 }
            },
           hidHtml5AppRootHtmlFiles:{
                 getValue :function(){
                   return _getPageHtmlCntrls.hiddenFields.hidHtml5AppRootHtmlFiles().val();
                 },
                 setValue :function(value){
                   _getPageHtmlCntrls.hiddenFields.hidHtml5AppRootHtmlFiles().val(value);
                 }
            },
           hidAppZipFolders:{
                 getValue :function(){
                   return _getPageHtmlCntrls.hiddenFields.hidAppZipFolders().val();
                 },
                 setValue :function(value){
                   _getPageHtmlCntrls.hiddenFields.hidAppZipFolders().val(value);
                 }
            },
           hidHtml5AppFileMoveData:{
                getValue :function(){
                  return _getPageHtmlCntrls.hiddenFields.hidHtml5AppFileMoveData().val();
                },
                setValue :function(value){
                  _getPageHtmlCntrls.hiddenFields.hidHtml5AppFileMoveData().val(value);
                }
           }  
        };
    })();
    
    var _getPageHtmlCntrls =(function(){
        
        return {
            divAddNewFile:{
                radioFileTypes: function(){
                    return $('input[name="rdbFileTypes"]:radio');
                }
            },
            divFileCreate:{
                radioFileCreateMode:function(){
                    return $('[id$=rdbCreateMode]');
                },
                txtFileName :function(){
                    return $('[id$=txtFileName]');    
                },
                lblFileExt :function(){
                    return $('[id$=lblFileExt]'); 
                },
                spanCreateFilePathInfo:function(){
                    return $('#spanCreateFilePathInfo');
                },
                uploadHtml5File:function(){
                    return $('input[name="uploadHtml5File"]');
                },
                btnAddFile:function(){
                    return $('#btnAddFile');
                },
                ddlUploadFolderPath:function(){
                    return $('#ddlUploadFolderPath');
                },
                ddlFolderPathForCreateNewFile:function(){
                    return $('#ddlFolderPathForCreateNewFile');
                }
            },
            hiddenFields:{
                hidAppTreeView:function(){
                    return $('[id$="hidAppTreeView"]');
                },
                hidAppFolderSelected:function(){
                    return $('[id$="hidAppFolderSelected"]');
                },
                hidHtml5AppDetail:function(){
                     return $('[id$="hidHtml5AppDetail"]');
                },
                hidAppZipEntriesList:function(){
                    return $('[id$="hidAppZipEntriesList"]');
                },
                hidDragDropFileName :function(){
                    return $('[id$="hidDragDropFileName"]');
                },
                hidDragDropFileBytes:function(){
                    return $('[id$="hidDragDropFileBytes"]');
                },
                hidAppFileSelected:function(){
                    return $('[id$="hidAppFileSelected"]');
                },
                hidSelectedFileContent:function(){
                    return $('[id$="hidSelectedFileContent"]');
                },
                hidHtml5AppFileDetail:function(){
                    return $('[id$="hidHtml5AppFileDetail"]');
                },
                hidFoldersForFileMove:function(){
                  return $('[id$="hidFoldersForFileMove"]');
                },
                hidHtml5AppRootHtmlFiles :function(){
                    return $('[id$="hidHtml5AppRootHtmlFiles"]');
                },
                hidAppZipFolders :function(){
                  return $('[id$="hidAppZipFolders"]');
                },
                hidHtml5AppFileMoveData:function(){
                  return $('[id$="hidHtml5AppFileMoveData"]');
                }
            },
            fileMenu :{
                
            },
            fileViewer:{
                imageViewerImage:function(){
                    return $('[type="image"][id$=imgHtml5ImageViewer]');
                }
            },
            createNewHtml5App:{
                txtHtml5AppName:function(){
                    return $('#txtHtml5AppName');
                }
            },
            saveAsHtml5App:{
               txtHtml5AppSaveAsName:function(){
                   return $('#txtHtml5AppSaveAsName');
               }
            },
            divFilesMoveTo:{
                ddlHtml5AppFileMoveTo:function(){
                    return $('#ddlHtml5AppFileMoveTo');
                }
            },
            divGetNewFolderName:{
                txtHtml5AppNewFolderName:function(){
                    return $('#txtHtml5AppNewFolderName');
                },
                ddlHtml5AppNewFolderSavePath:function(){
                    return $('#ddlHtml5AppNewFolderSavePath');
                },
                
            },
            divMainMenuAndCnavasSeparator:{
                spanHtml5AppHeaderTitle :function(){
                    return $('#spanHtml5AppHeaderTitle');
                }
            }
        };
    })();
    
    var _addNewFile = (function(){
        
        
        
        return {
            setDefaultsOfControls : function(){
                var strOptionsForDropDown = '<option value="'+MF_HTML5_CONSTANTS.rootPath+'">'+MF_HTML5_CONSTANTS.rootPath+'</option>';
                _getPageHtmlCntrls.divAddNewFile.radioFileTypes()
                    .filter('input[value="7"]').prop('checked',true);
                
                _getPageHtmlCntrls.divFileCreate.uploadHtml5File().val("");
                _getPageHtmlCntrls.divFileCreate.txtFileName().val("");
                _getPageHtmlCntrls.divFileCreate.lblFileExt().val("Zip");
                _createEmptyNewFile.showHideTxtFileNameCont(false);
                $('#divSeparatorOrForCreateFile').hide();
                _fileUploaderDragDrop.setDataOfContDiv({
                    fileType :MF_HTML5_CONSTANTS.fileTypes.zip 
                });
                _fileUploaderInput.setDataOfContDiv({
                    fileType :MF_HTML5_CONSTANTS.fileTypes.zip
                });
                _createEmptyNewFile.setDataOfContDiv({
                    fileType :MF_HTML5_CONSTANTS.fileTypes.zip
                });
                _getPageHtmlCntrls.divFileCreate.ddlUploadFolderPath().html(strOptionsForDropDown);
                _getPageHtmlCntrls.divFileCreate.ddlFolderPathForCreateNewFile().html(strOptionsForDropDown);
            }
        };
    })();
    
    //Creating Empty file 
    var _createEmptyNewFile = (function(){
        function _getContainerDiv(){
            return $('#divHtml5NewFileNameCont');
        }
        function _getDivFileNameCont(){
            return $('#divHtml5NewFileNameCont');
         }
        return {
            setDefaultsOfControls : function(){
               var radFileCreateMode = _getPageHtmlCntrls.divFileCreate.radioFileCreateMode();
               radFileCreateMode.find("input[value='UPL']").prop("checked",true);
               _getPageHtmlCntrls.divFileCreate.txtFileName().val("");
               _getPageHtmlCntrls.divFileCreate.lblFileExt().text("zip");
            },
            setDataOfContDiv:function(data/*object*/){
               /*{fileType : MF_HTML5_CONSTANTS.fileTypes}*/
               _getContainerDiv().data(mficientConstants.jqueryCommonDataKey,data);
           },
            getDataOfContDiv :function(){
              return _getContainerDiv().data(mficientConstants.jqueryCommonDataKey);
           },
            getFileTypeFromGlobalData:function(){
               var objData = this.getDataOfContDiv();
                if(objData){
                    return objData.fileType;
                }
                else{
                    return null;
                }
           },
            deleteDataOfContDiv :function(){
               _getContainerDiv().removeData(mficientConstants.jqueryCommonDataKey);
           },
            showHideTxtFileNameCont:function(blnShow/*boolean*/){
               if(blnShow === true){
                   _getDivFileNameCont().show();
               }
               else{
                   _getDivFileNameCont().hide();
               }
           },
            getCreateFilePopupHeaderText:function(fileType/*Constants.fileTypes*/){
                if(mfUtil.isNullOrUndefined(fileType)){
                    throw mfExceptionMsgs.argumentsNull;
                }
                var strHeaderText = "New File";
                switch(fileType){
                    case MF_HTML5_CONSTANTS.fileTypes.image:
                        strHeaderText = "New Image File";
                        break;
                     case MF_HTML5_CONSTANTS.fileTypes.html:
                         strHeaderText = "New Html File";
                     break;
                     case MF_HTML5_CONSTANTS.fileTypes.js:
                         strHeaderText = "New Javascript File";
                     break;
                     case MF_HTML5_CONSTANTS.fileTypes.css:
                         strHeaderText = "New CSS File";
                     break;
                     case MF_HTML5_CONSTANTS.fileTypes.txt:
                         strHeaderText = "New Text File";
                     break;
                     case MF_HTML5_CONSTANTS.fileTypes.xml:
                         strHeaderText = "New XML File";
                     break;
                     case MF_HTML5_CONSTANTS.fileTypes.zip:
                         strHeaderText = "New Zip File";
                     break;
                }
                return strHeaderText;
            },
            createNewFile :function(){
               //data fileType name directory useFileBytes appName
               //directory path is empty no zip file already exists create ne w zip file and add the file.
               //var radFileCreateMode = _getPageHtmlCntrls.divFileCreate.radioFileCreateMode();
                
                
                var strFileName = "",
                    strFileType = this.getFileTypeFromGlobalData(),
                    $textFileName = $([]),
                    strFileNameWithExtension = "";    
               
                if( strFileType !== MF_HTML5_CONSTANTS.fileTypes.image
                    || strFileType !== MF_HTML5_CONSTANTS.fileTypes.zip){
                
                    //add new empty file
                    $textFileName = _getPageHtmlCntrls.divFileCreate.txtFileName();
                    strFileName = $textFileName.val();
                    strFileNameWithExtension = 
                          _getFileNameWithExtension(strFileName,strFileType);
                    
                    var aryErrors = _validateCreateNewFileName(strFileName);
                    if($.isArray(aryErrors) && aryErrors.length>0){
                        throw new MfError(aryErrors); 
                    }
                    _createNewFileHelper({
                        fileNameWithExtension : strFileNameWithExtension,
                        fileType : strFileType,
                        useFileBytes : false,
                        confirmCancelCallBack : null
                     });      
                }
                else{
                }
            }
        };    
        
    })();
    
    var _fileUploaderDragDrop = (function(){
        
        function _getContainerDiv(){
            return $('#divFileUploaderDragDrop');
        }
        return {
           setDataOfContDiv:function(data/*object*/){
               /*{fileType : MF_HTML5_CONSTANTS.fileTypes}*/
               _getContainerDiv().data(mficientConstants.jqueryCommonDataKey,data);
           },
           getDataOfContDiv :function(){
              return _getContainerDiv().data(mficientConstants.jqueryCommonDataKey);
           },
           getFileTypeFromGlobalData:function(){
               var objData = this.getDataOfContDiv();
                if(objData){
                    return objData.fileType;
                }
                else{
                    return null;
                }
           },
           deleteDataOfContDiv :function(){
               _getContainerDiv().removeData(mficientConstants.jqueryCommonDataKey);
           },
           getUploaderPopupHeaderText:function(fileType/*Constants.fileTypes*/){
               if(mfUtil.isNullOrUndefined(fileType)){
                   throw mfExceptionMsgs.argumentsNull;
               }
               var strHeaderText = "File Uploader";
               switch(fileType){
                   case MF_HTML5_CONSTANTS.fileTypes.image:
                       strHeaderText = "Upload Image File";
                       break;
                    case MF_HTML5_CONSTANTS.fileTypes.html:
                        strHeaderText = "Upload Html File";
                    break;
                    case MF_HTML5_CONSTANTS.fileTypes.js:
                        strHeaderText = "Upload Javascript File";
                    break;
                    case MF_HTML5_CONSTANTS.fileTypes.css:
                        strHeaderText = "Upload CSS File";
                    break;
                    case MF_HTML5_CONSTANTS.fileTypes.txt:
                        strHeaderText = "Upload Text File";
                    break;
                    case MF_HTML5_CONSTANTS.fileTypes.xml:
                        strHeaderText = "Upload XML File";
                    break;
                    case MF_HTML5_CONSTANTS.fileTypes.zip:
                        strHeaderText = "Upload Zip File";
                    break;
               }
               return strHeaderText;
           },
           createNewFile:function(){
              //TODO Validate if possible
              var strFileType = this.getFileTypeFromGlobalData(); 
              var strFileNameWithExtension = 
                    _hidFieldsHelper.hidDragDropFileName.getValue(),
                   objApp = _html5ObjectPropSheet.getCurrentWorkingApp();
              //fileNameWithExtension,fileType,useFileBytes,confirmCancelCallBack
              if(strFileType === MF_HTML5_CONSTANTS.fileTypes.zip){
                 var data =[
                      objApp.fnGetName(),
                      objApp.fnGetId()
                   ];
                 processPostbackByHtmlCntrl.postBack(
                    postBackByHtmlProcess.Html5ZipFileAdd,
                    data, "");
              }
              else{
                  _createNewFileHelper({
                      fileNameWithExtension : strFileNameWithExtension,
                      fileType : strFileType,
                      useFileBytes : true,
                      confirmCancelCallBack : null
                   });
              }
           },
           showHideContainerDiv:function(blnShow){
               if(blnShow === true){
                   _getContainerDiv().show();
               }
               else{
                  _getContainerDiv().hide(); 
               }
           }
        };
    })();
   
    var _fileViewer=(function(){
        function _getFileViewerContainer(){
            return $('#divHtml5Canvas');
        }
        
        var _mainContainerDiv =(function(){
             function _getDiv(){
                 return $('#divHtml5Canvas');
             }
            return {
                
                showHideDiv:function(blnShow){
                    var $div = _getDiv();
                    if(blnShow === true){
                        $div.show();
                    }
                    else{
                        $div.hide();
                    }
                }
            };
        })();
        
        var _fileMenu =(function(){
            function _getFileMenuContainer(){
                return $('#divFileSaveMenu');
            }
            function _getSaveLinkCont(){
                return $('#listSaveFileCont');
            }
            function _getSearchLinkCont(){
                return $('#listSearchFileCont');
            }
            function _getCloseLinkCont(){
                return $('#listCloseFileCont');
            }
            function _showHideSaveLinkCont(blnShow){
                var $container = _getSaveLinkCont();
                if(blnShow === true){
                    $container.show();
                }
                else{
                    $container.hide();
                }
            }
            function _showHideSearchLinkCont(blnShow){
                var $container = _getSearchLinkCont();
                if(blnShow === true){
                    $container.show();
                }
                else{
                    $container.hide();
                }
            }
            function _showHideCloseLinkCont(blnShow){
                var $container = _getCloseLinkCont();
                if(blnShow === true){
                    $container.show();
                }
                else{
                    $container.hide();
                }
            }
            return {
                showHideSaveLinkCont:function(blnShow){
                    _showHideSaveLinkCont(blnShow);
                },
                showHideSearchLinkCont:function(blnShow){
                    _showHideSearchLinkCont(blnShow);
                },
                showHideCloseLinkCont:function(blnShow){
                    _showHideCloseLinkCont(blnShow);
                },
                showHideEditLinksByFileType:function(fileType/*CONSTANTS.fileTypes*/){
                    switch(fileType){
                        case MF_HTML5_CONSTANTS.fileTypes.image:
                                _showHideSaveLinkCont(false);
                                _showHideSearchLinkCont(false);
                                _showHideCloseLinkCont(true);
                            break;
                         case MF_HTML5_CONSTANTS.fileTypes.html:
                         case MF_HTML5_CONSTANTS.fileTypes.js:
                         case MF_HTML5_CONSTANTS.fileTypes.css:
                         case MF_HTML5_CONSTANTS.fileTypes.txt:
                         case MF_HTML5_CONSTANTS.fileTypes.xml:
                             _showHideSaveLinkCont(true);
                             _showHideSearchLinkCont(true);
                             _showHideCloseLinkCont(true);
                            break;     
                         case MF_HTML5_CONSTANTS.fileTypes.zip:
                            break;
                    }
                }
            };
        })();
        
        var _aceEditor = (function(){
            function _getContainerDiv(){
                return $('#divAceFileEditorCont');
            }
            function _getAceEditorInstance(){
                return aceEditor;
            }
            function _setModeByFileType(fileType){
                if(mfUtil.isNullOrUndefined(fileType)){
                    throw mfExceptionMsgs.argumentsNull;
                }
                var aceEditor = _getAceEditorInstance();
                switch(fileType){
                    case MF_HTML5_CONSTANTS.fileTypes.image:
                        break;
                     case MF_HTML5_CONSTANTS.fileTypes.html:
                        aceEditor.getSession().setMode("ace/mode/html"); 
                     break;
                     case MF_HTML5_CONSTANTS.fileTypes.js:
                         aceEditor.getSession().setMode("ace/mode/javascript"); 
                     break;
                     case MF_HTML5_CONSTANTS.fileTypes.css:
                         aceEditor.getSession().setMode("ace/mode/css"); 
                     break;
                     case MF_HTML5_CONSTANTS.fileTypes.txt:
                         aceEditor.getSession().setMode("ace/mode/text"); 
                     break;
                     case MF_HTML5_CONSTANTS.fileTypes.xml:
                         aceEditor.getSession().setMode("ace/mode/xml"); 
                     break;
                     case MF_HTML5_CONSTANTS.fileTypes.zip:
                     break;
                }
            }
            function _isFileTypeValid(fileType){
                if(mfUtil.isNullOrUndefined(fileType)){
                    throw mfExceptionMsgs.argumentsNull;
                }
                var blnIsValid = false;
                switch(fileType){
                    case MF_HTML5_CONSTANTS.fileTypes.image:
                    case MF_HTML5_CONSTANTS.fileTypes.zip:    
                        blnIsValid= false;
                        break;
                     case MF_HTML5_CONSTANTS.fileTypes.html:
                     case MF_HTML5_CONSTANTS.fileTypes.js:
                     case MF_HTML5_CONSTANTS.fileTypes.css:
                     case MF_HTML5_CONSTANTS.fileTypes.txt:
                     case MF_HTML5_CONSTANTS.fileTypes.xml:
                         blnIsValid = true;
                     break;
                }
                return blnIsValid;
            }
            function _setTextInEditor(text){
                if(mfUtil.isNullOrUndefined(text)){
                    throw mfExceptionMsgs.argumentsNull;
                }
                var editor = _getAceEditorInstance();
                //editor.getSession().removeListener('change');
                _removeChangeEvent();
                _removeCursorChangeEvent();
                editor.setValue(text);
                //editor.getSession().setUndoManager(new UndoManager());
                _setEditorContentDetails();
                _setEditorCursorDetails();
                _setOnCursorChangeEvent();
                _setOnChangeEvent();
                editor.getSession().setValue(text);
            }
            function _setEditorContentDetails(){
                var editor = _getAceEditorInstance();
                var strValue =editor.getValue();
                var noOfLines = editor.session.getLength();
                $('#spanEditorContentLines').text(noOfLines);
                $('#spanEditorContentLength').text(strValue.length);
            }
            function _removeChangeEvent(){
                var editor = _getAceEditorInstance();
                editor.getSession().removeListener('change');
            }
            function _setOnChangeEvent(){
                var editor = _getAceEditorInstance();
                editor.getSession().on('change', _setEditorContentDetails);
            }
            function _removeCursorChangeEvent(){
                var editor = _getAceEditorInstance();
                editor.getSession().removeListener('changeCursor');
            }
            function _setOnCursorChangeEvent(){
                var editor = _getAceEditorInstance();
                editor.getSession().on('changeCursor', _setEditorContentDetails);
            }
            function _setEditorCursorDetails(){
                var editor = _getAceEditorInstance();
                var cursorPosition = editor.selection.getCursor();
                var row = 0, column = 0;
                if (cursorPosition) {
                    row = (cursorPosition.row) + 1;
                    column = cursorPosition.column;
                }
                $('#spanEditorLineNo').text(row);
                $('#spanEditorColumnNo').text(column);
            }
            function _getText(){
                var editor = _getAceEditorInstance();
                return editor.getSession().getValue();
            }
            return{
                showHideContainer:function(blnShow){
                    var $contDiv = _getContainerDiv();
                    if(blnShow === true){
                        $contDiv.show();
                    }
                    else{
                        $contDiv.hide();
                    }
                },
                setTextInEditor:function(text){
                    _setTextInEditor(text);
                },
                setModeByFileType:function(fileType/*constants.fileType*/){
                    _setModeByFileType(fileType);
                },
                isFileTypeValid:function(fileType){
                    return _isFileTypeValid(fileType);
                },
                changeEventHandler:function(){
                    _setEditorContentDetails();
                },
                setOnChangeEvent:function(){
                    _setOnChangeEvent();
                },
                removeOnChangeEvent:function(){
                    _removeChangeEvent();
                },
                changeCursorEventHandler:function(){
                    _setEditorCursorDetails();
                },
                executeFindAndReplace :function(){
                    var editor = _getAceEditorInstance();
                    editor.execCommand('replace');
                },
                getText:function(){
                    return _getText();
                },
                getTextForEditorFromHidField:function(){
                    var strFileContent = 
                        _hidFieldsHelper.hidSelectedFileContent.getValue();
                    var objFileContent =null;
                    if(!mfUtil.isNullOrUndefined(strFileContent) &&
                       !mfUtil.isEmptyString(strFileContent)){
                        
                        objFileContent = $.parseJSON(strFileContent);
                        if(objFileContent){
                            strFileContent =  objFileContent.content;
                        }
                    }
                    else{
                        strFileContent = "";
                    }
                    return strFileContent;
                },
                setTextFromEditorInHidField:function(){
                    var fileContent = _getText();
                    var stringifiedText = "";
                    if(!mfUtil.isNullOrUndefined(fileContent)){
                        //fileContent = _.escape(fileContent)
                        stringifiedText = JSON.stringify({
                            content :fileContent
                        });
                    }
                    stringifiedText = $('<div/>').text(stringifiedText).html();
                    _hidFieldsHelper.hidSelectedFileContent.setValue(
                         stringifiedText);
                }
            };
        })();
        
        var _imageViewer = (function(){
            
            function _getContainerDiv(){
                
                return $('#divHtml5ImageViewerCont');
                
            }
            
            return{
               showHideContainer:function(blnShow) {
                   var $contDiv = _getContainerDiv();
                   if(blnShow === true){
                       $contDiv.show();
                   }
                   else{
                       $contDiv.hide();
                   }
               },
               removeSourceOfViewImage:function(){
                   _getPageHtmlCntrls.fileViewer.imageViewerImage().attr('src',"");
               }
            };
        })();
        
        return {
          showHideFileViewerCont:function(blnShow/*boolean*/){
            _mainContainerDiv.showHideDiv(blnShow);
          },
          showHideImageViewerCont:function(blnShow/*boolean*/){
              _imageViewer.showHideContainer(blnShow);
          },
          showHideAceEditorViewerCont:function(blnShow/*boolean*/){
              _aceEditor.showHideContainer(blnShow);
          },
          setModeOfAceEditor:function(fileType/*constants.fileType*/){
              _aceEditor.setModeByFileType(fileType);
          },
          viewFileOnEdit:function(selectedFileData/*object*/){
                if(mfUtil.isNullOrUndefined(selectedFileData)){
                    throw mfExceptionMsgs.argumentsNull;
                }
                var fileType = 
                        _fileExplorer
                        .treeNodeOriginalDataHelper.getFileType(selectedFileData);
                
                var strFileContent = "";
                        
                fileType = fileType.toString();
                _mainContainerDiv.showHideDiv(false);
                
                if(fileType === MF_HTML5_CONSTANTS.fileTypes.image){
                    _aceEditor.showHideContainer(false);
                    _mainContainerDiv.showHideDiv(true);
                    _imageViewer.showHideContainer(true);
                } 
                else{
                    if(_aceEditor.isFileTypeValid(fileType)){
                       strFileContent = _aceEditor.getTextForEditorFromHidField();
                       _mainContainerDiv.showHideDiv(true);
                       _aceEditor.setModeByFileType(fileType);
                       _aceEditor.showHideContainer(true);
                       _imageViewer.showHideContainer(false);
                       _aceEditor.setTextInEditor(strFileContent);
                    }
                }
                _fileMenu.showHideEditLinksByFileType(fileType);
            },
          closeFileViewer:function(){
              _aceEditor.setTextInEditor("");
              _hidFieldsHelper.hidSelectedFileContent.setValue("");
              _hidFieldsHelper.hidAppFileSelected.setValue("");
              _imageViewer.removeSourceOfViewImage();
              _mainContainerDiv.showHideDiv(false);
          },    
          aceEditor:{
              changeEventHandler:function(){
                  _aceEditor.changeEventHandler();
              },
              setOnChangeEvent:function(){
                  _aceEditor.setOnChangeEvent();
              },
              removeOnChangeEvent:function(){
                  _aceEditor.removeChangeEvent();
              },
              changeCursorEventHandler:function(){
                 _aceEditor.changeCursorEventHandler();
              },
              executeFindAndReplace:function(){
                  _aceEditor.executeFindAndReplace();
              },
              getText:function(){
                  return _aceEditor.getText();
              },
              setTextFromEditorInHidField:function(){
                  _aceEditor.setTextFromEditorInHidField();
              },
              setText:function(){
                  _aceEditor.setTextInEditor("");
              }
          },
          imageViewer:{
              removeSourceOfViewImage:function(){
                  _imageViewer.removeSourceOfViewImage();
              }
          }
        };
    })();
    
    var _fileExplorer = (function(){
        
        function _getContainerDiv(){
            return $('#divAppFilesTree');
        }
        function _clearContainerDiv(){
            _getContainerDiv().html('');
        }
        function _getAppTreeJsonFromHidField(){
            return _getPageHtmlCntrls.hiddenFields.hidAppTreeView().val();
        }
        function _doesFileExplorerDataExists(){
            var $hidAppTreeView = _getPageHtmlCntrls.hiddenFields.hidAppTreeView();
            var treeData = $hidAppTreeView.val();
            if(mfUtil.isNullOrUndefined(treeData) ||
                mfUtil.isEmptyString(treeData)){
                    return false;
                }
                else{
                    return true;
                }
        }
        function _getAllEntriesOfTreeJsonFromHidField(){
           return _getPageHtmlCntrls.hiddenFields.hidAppZipEntriesList().val();
        }
        function _doesFileExists(path,fileName){
            var blnExists = false;
            if(_doesFileExplorerDataExists() === true){
                var strZipEntries = _getAllEntriesOfTreeJsonFromHidField();
                var strFileToSearch = path+fileName,
                    aryZipEntries = []; 
                if(!mfUtil.isNullOrUndefined(strZipEntries)&&
                   !mfUtil.isEmptyString(strZipEntries)){
                    
                   aryZipEntries =  $.parseJSON(strZipEntries); 
                   if($.inArray(strFileToSearch,aryZipEntries) !== -1){
                       blnExists = true;
                   }
                   else{
                       blnExists = false;
                   }
                }
            }
            return blnExists;
        }
        function _getRootPathOfFileExplorer(){
            // var strRootPath = "";
            // if(_doesFileExplorerDataExists() === true){
            //     var strZipEntries = _getAllEntriesOfTreeJsonFromHidField();
            //     var aryZipEntries = []; 
            //     if(!mfUtil.isNullOrUndefined(strZipEntries)&&
            //       !mfUtil.isEmptyString(strZipEntries)){
                    
            //       aryZipEntries =  $.parseJSON(strZipEntries); 
            //       strRootPath = aryZipEntries[0];
            //     }
            // }
            // return strRootPath;
            return MF_HTML5_CONSTANTS.rootPath;
        }
        function _destroyFileTree(){
            var jsTreeInstance = _getContainerDiv().jstree(true);
            if(jsTreeInstance){
                jsTreeInstance.destroy();
            }
        }
        function _getJsTreeInstanceFromContDiv(){
            var $contDiv = _getContainerDiv();
            return $contDiv.jstree(true);
        }
        function _getAllFoldersFromHidField(){
          var strZipFolders =_hidFieldsHelper.hidAppZipFolders.getValue();
          var aryZipFolders =[];
          if(strZipFolders){
            aryZipFolders = $.parseJSON(strZipFolders);
          }
          return aryZipFolders;
        }
        function _getFoldersForFileMovementFromHidField(){
          var strZipFolders =_hidFieldsHelper.hidFoldersForFileMove.getValue();
          var aryZipFolders =[];
          if(strZipFolders){
            aryZipFolders = $.parseJSON(strZipFolders);
          }
          return aryZipFolders;
        }
        function _fileBrowserMoveSave(){
            var $contDiv = _getContainerDiv();
            var jsTreeInstance = _getJsTreeInstanceFromContDiv(),
                aryNodesSelected = [],
                aryDataToSave = [],
                objNodeData = null,
                strId = "",
                strNewPath = "",
                strMoveToPath =_getPageHtmlCntrls.divFilesMoveTo.ddlHtml5AppFileMoveTo().val();
            if(jsTreeInstance){
                aryNodesSelected = jsTreeInstance.get_selected(true);
                if($.isArray(aryNodesSelected) && aryNodesSelected.length>0){
                    for(var i=0;i<= aryNodesSelected.length-1;i++){
                        objNodeData = aryNodesSelected[i].original;
                        if(objNodeData){
                            if(_treeNodeOriginalDataHelper.isFileSelectedADirectory(objNodeData)){
                               strId =  _treeNodeOriginalDataHelper.getId(objNodeData);
                               if(strId !== MF_HTML5_CONSTANTS.rootPath)
                               {
                                   strNewPath =  strId.substring(0,strId.lastIndexOf(MF_HTML5_CONSTANTS.directorySeparator));
                                   if(strNewPath){
                                       strNewPath = strNewPath.substring(
                                                        strNewPath.lastIndexOf(
                                                            MF_HTML5_CONSTANTS.directorySeparator
                                                        )+1);
                                        strNewPath = strMoveToPath+strNewPath+MF_HTML5_CONSTANTS.directorySeparator;
                                        aryDataToSave.push({
                                           oldPath:strId,
                                           newPath : strNewPath
                                        });
                                   }
                               }
                            }
                            else{
                                strId =  _treeNodeOriginalDataHelper.getId(objNodeData);
                                strNewPath = strId.substring(
                                                strId.lastIndexOf(
                                                    MF_HTML5_CONSTANTS.directorySeparator
                                                )+1);
                                if(strNewPath){
                                    strNewPath = strMoveToPath+strNewPath;
                                     aryDataToSave.push({
                                        oldPath:strId,
                                        newPath : strNewPath
                                     });
                                }
                            }
                        }
                    }
                }
            }
            _hidFieldsHelper.hidHtml5AppFileMoveData.setValue(JSON.stringify(aryDataToSave));
             var objApp = _html5ObjectPropSheet.getCurrentWorkingApp();
             if(objApp){
               processPostbackByHtmlCntrl.
               postBack(
                   postBackByHtmlProcess.Html5AppFileMove,
                   [objApp.fnGetId()],
                 "");
            }
        }
        function _fileEdit(data/*data about the file selected*/){
            var inst = $.jstree.reference(data.reference),
            obj = inst.get_node(data.reference);
            var objOriginalData =obj && obj.original; 
            var selectedFileIsDirectory = false;
            if(!mfUtil.isNullOrUndefined(objOriginalData)){
                
                selectedFileIsDirectory = 
                    _treeNodeOriginalDataHelper
                    .isFileSelectedADirectory(objOriginalData);
                
                if(selectedFileIsDirectory === true){
                    //_hidFieldsHelper.hidAppFolderSelected.setValue(
                       // _treeNodeOriginalDataHelper.getId(objOriginalData));
                    //_hidFieldsHelper.hidAppFileSelected.setValue(""); 
                    throw new MfError(["Please select a file for edit."]);
                }
                else{
                }
                $.confirm('Are you sure you want to edit this file.',
                   function(){
                      _hidFieldsHelper.hidAppFolderSelected.setValue("");
                      _hidFieldsHelper.hidAppFileSelected.setValue(
                          _treeNodeOriginalDataHelper.serialise(objOriginalData));
                      var fileSelected = _hidFieldsHelper.hidAppFileSelected.getValue();
                      if(!mfUtil.isEmptyString(fileSelected)){
                           var  objApp = _html5ObjectPropSheet.getCurrentWorkingApp(),
                               strAppName = objApp.fnGetName(),
                               strAppId = objApp.fnGetId();
                           var data =[
                               strAppId
                             ];
                           processPostbackByHtmlCntrl.postBack(
                               postBackByHtmlProcess.ViewHtml5AppFile,
                                data, "");
                       }
                       else{
                          throw new MfError(["Please select a file for edit."]);
                       } 
                   },
                   null);
                showDialogImage(DialogType.Warning);
            }
        }
        
        
        function _getInstanceFromContainerDiv(){
            return _getContainerDiv().jstree(true);
        }
        var _treeNodeOriginalDataHelper = (function(){
            
            return {
                isFileSelectedADirectory:function(data/*original data from node*/){
                    if(mfUtil.isNullOrUndefined(data)){
                        throw mfExceptionMsgs.argumentsNull;
                    }
                    return data.isDirectory;
                },
                getId:function(data){
                    if(mfUtil.isNullOrUndefined(data)){
                        throw mfExceptionMsgs.argumentsNull;
                    }
                    return data.id;
                },
                serialise:function(data){
                    if(mfUtil.isNullOrUndefined(data)){
                        throw mfExceptionMsgs.argumentsNull;
                    }
                    return JSON.stringify(data);
                },
                getFileType:function(data){
                    if(mfUtil.isNullOrUndefined(data)){
                        throw mfExceptionMsgs.argumentsNull;
                    }
                    return data.fileType;
                },
                deserialise:function(data/*string*/){
                    if(!mfUtil.isNullOrUndefined(data) && 
                        !mfUtil.isEmptyString(data)){
                        return $.parseJSON(data);        
                    }
                    else{
                        return null;
                    }
                }
            };
            
        })();
        var _fileSelectDeselectForFileMovementHelper = (function(){
            function _doSelected(nodeOriginalData,jsTreeInstance){
                
                var aryDirectoryPath = [],
                    parentFolderPath= "",
                    i=0,
                    aryFoldersForFileMovement=[],
                    pattern="",
                    strId= "";
                    
                strId =_treeNodeOriginalDataHelper.getId(nodeOriginalData) ;
                aryDirectoryPath = nodeOriginalData.id.split(MF_HTML5_CONSTANTS.directorySeparator);
                aryFoldersForFileMovement = _getFoldersForFileMovementFromHidField();
                if( $.isArray(aryFoldersForFileMovement) 
                    && aryFoldersForFileMovement.length===0){
                    
                     aryFoldersForFileMovement = _getAllFoldersFromHidField();
                }
                
                if(_treeNodeOriginalDataHelper
                    .isFileSelectedADirectory(nodeOriginalData) === true){
                  
                  if($.isArray(aryDirectoryPath) && aryDirectoryPath.length>0){
                     //after split if it is a directory then its length should be greater than or equal to 2
                      for(i=0;i<=aryDirectoryPath.length-3;i++)  {
                         parentFolderPath += aryDirectoryPath[i]+MF_HTML5_CONSTANTS.directorySeparator; 
                      }
                      //remove both selected and parent folder from list
                      //if(jsTreeInstance.is_selected({id:parentFolderPath})){
                          
                      //}
                      //pattern for child folders
                      pattern = new RegExp(strId+'\\w*');
                      aryFoldersForFileMovement = $.grep(aryFoldersForFileMovement,function(value,index){
                            if(value != MF_HTML5_CONSTANTS.rootPath){
                                return (
                                       value !== strId 
                                        && value !== parentFolderPath
                                        && !pattern.test(value)
                                );
                            }
                            else{
                                return true;
                            }
                        
                      });
                    //   if(parentFolderPath !== MF_HTML5_CONSTANTS.rootPath){
                          
                    //   }
                    //   else{
                    //       //aryFoldersForFileMovement.push( parentFolderPath);
                    //   }
                   }
                }
                else {
                    //not a directory
                    if($.isArray(aryDirectoryPath) && aryDirectoryPath.length>0){
                      //after split if it is a directory then its length should be greater than or equal to 2
                       for(i=0;i<=aryDirectoryPath.length-2;i++)  {
                          parentFolderPath += aryDirectoryPath[i]+MF_HTML5_CONSTANTS.directorySeparator; 
                       }
                       //remove both selected and parent folder from list
                       //if(jsTreeInstance.is_selected({id:parentFolderPath})){
                        //}
                       //pattern for child folders
                       aryFoldersForFileMovement = $.grep(aryFoldersForFileMovement,function(value,index){
                            if(value != MF_HTML5_CONSTANTS.rootPath){
                                return (
                                     
                                        value !== parentFolderPath 
                                );
                            }
                            else{
                                return true;
                            }
                            
                       });
                    //   if(parentFolderPath != MF_HTML5_CONSTANTS.rootPath){
                           
                    //   }
                    //   else{
                    //       //aryFoldersForFileMovement.push( parentFolderPath);
                    //   }
                    }
                }
                _hidFieldsHelper.hidFoldersForFileMove.setValue(JSON.stringify(aryFoldersForFileMovement));
            }
            function _doDeSelected(nodeOriginalData,jsTreeInstance){
                
                var aryDirectoryPath = [],
                    parentFolderPath= "",
                    i=0,
                    aryFoldersForFileMovement=[],
                    pattern="",
                    strId= "",
                    blnAddParentToAllowableList = false;
                    
                strId =_treeNodeOriginalDataHelper.getId(nodeOriginalData) ;
                aryDirectoryPath = nodeOriginalData.id.split(MF_HTML5_CONSTANTS.directorySeparator);
                aryFoldersForFileMovement = _getFoldersForFileMovementFromHidField();
                if( $.isArray(aryFoldersForFileMovement) 
                    && aryFoldersForFileMovement.length===0){
                    
                     aryFoldersForFileMovement = _getAllFoldersFromHidField();
                }
                
                if(_treeNodeOriginalDataHelper
                    .isFileSelectedADirectory(nodeOriginalData) === true){
                  
                  if($.isArray(aryDirectoryPath) && aryDirectoryPath.length>0){
                     //after split if it is a directory then its length should be greater than or equal to 2
                      for(i=0;i<=aryDirectoryPath.length-3;i++)  {
                         parentFolderPath += aryDirectoryPath[i]+MF_HTML5_CONSTANTS.directorySeparator; 
                      }
                      if(jsTreeInstance.is_selected({id:parentFolderPath})){
                          blnAddParentToAllowableList = false;
                      }else{
                          blnAddParentToAllowableList = true;
                      }
                      if(blnAddParentToAllowableList === true){
                          aryFoldersForFileMovement.push(parentFolderPath);
                          aryFoldersForFileMovement.push(strId);
                      }
                      
                   }
                }
                else {
                    //not a directory
                    if($.isArray(aryDirectoryPath) && aryDirectoryPath.length>0){
                      //after split if it is a directory then its length should be greater than or equal to 2
                       for(i=0;i<=aryDirectoryPath.length-2;i++)  {
                          parentFolderPath += aryDirectoryPath[i]+MF_HTML5_CONSTANTS.directorySeparator; 
                       }
                       if(jsTreeInstance.is_selected({id:parentFolderPath})){
                         blnAddParentToAllowableList = false;
                       }
                       else{
                           blnAddParentToAllowableList = true;
                       }
                       
                       if(blnAddParentToAllowableList === true){
                           aryFoldersForFileMovement.push(parentFolderPath);
                       }
                    }
                }
                _hidFieldsHelper.hidFoldersForFileMove.setValue(JSON.stringify(aryFoldersForFileMovement));
            }
            return {
                doSelected:function(nodeOriginalData,jsTreeInstance){
                    _doSelected(nodeOriginalData,jsTreeInstance);
                },
                doDeSelected :function(nodeOriginalData,jsTreeInstance){
                    _doDeSelected(nodeOriginalData,jsTreeInstance);
                }
            };
        })();
        
        return {
            doesFileExplorerDataExists:function(){
                return _doesFileExplorerDataExists();
            },
            clearContainerDiv:function(){
                _clearContainerDiv();
            },
            formTreeView:function()   {
                var $contDiv = _getContainerDiv();
                var treeData =_getAppTreeJsonFromHidField(); 
                if (treeData) {
                    treeData = $.parseJSON(treeData);
                    _clearContainerDiv();
                    var previousInstance = $contDiv.jstree(true);
                    if(previousInstance){
                        previousInstance.destroy();
                    }
                    
                   $.jstree.defaults.contextmenu.items ={ 
                        "edit" : {
                          "separator_before"    : false,
                          "separator_after"    : true,
                          "_disabled"            : false, //(this.check("create_node", data.reference, {}, "last")),
                          "label"                : "Edit",
                          "action"            : function (data) {
                                try{
                                    _fileEdit(data);
                                }
                                catch(error){
                                    if (error instanceof MfError){
                                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                                    }else{
                                        console.log(error);
                                    }
                                }
                            }
                       },
                       "delete" : {
                          "separator_before"    : false,
                          "separator_after"    : true,
                          "_disabled"            : false, //(this.check("create_node", data.reference, {}, "last")),
                          "label"                : "Delete",
                          "action"            : function (data) {
                                try{
                                    
                                }
                                catch(error){
                                    if (error instanceof MfError){
                                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                                    }else{
                                        console.log(error);
                                    }
                                }
                            }
                       }
                    };
                    $contDiv.on("changed.jstree", function (e, data) {
                        }).on("loaded.jstree", function(event, data) {
                             $(this).find('#root\\/ > a.jstree-anchor i.jstree-checkbox').hide();
                        }).on("open_node.jstree", function(event, data) {
                             $(this).find('#root\\/ >a.jstree-anchor i.jstree-checkbox').hide();
                        }).jstree({ 'core': {
                            'data': treeData
                        },
                         plugins:["contextmenu"]
                        });
                    }
                
                // .on("select_node.jstree", function(event, data) {
                //       var objOriginalData =data && data.node && data.node.original;
                //       _fileSelectDeselectForFileMovementHelper
                //       .doSelected(objOriginalData,data.instance);
                // }).on("deselect_node.jstree", function(event, data) {
                //      var objOriginalData =data && data.node && data.node.original;
                //      _fileSelectDeselectForFileMovementHelper
                //      .doDeSelected(objOriginalData,data.instance);
                // }),
            },
            doesFileExists:function(path,fileName){
                return _doesFileExists(path,fileName);
            },
            getRootPathOfFileExplorer:function(){
                return _getRootPathOfFileExplorer();
            },
            treeNodeOriginalDataHelper:{
                getFileType :function(data){
                    return _treeNodeOriginalDataHelper.getFileType(data);
                },
                deserialise:function(data){
                    return _treeNodeOriginalDataHelper.deserialise(data);
                }
            },
            destroyFileTree:function(){
                _destroyFileTree();
            },
            fileBrowserMoveFiles:function(){
                var strOptions ="",
                    aryAllFolders = _getAllFoldersFromHidField(),
                    aryAllowableFolders=[],
                    jsTreeInstance = null,
                    arySelectedFiles = [],
                    i=0,k=0,
                    objNodeOriginalData = null,
                    objNodeData = null,
                    aryDirectoryPath = [],
                    strNodeId = "",
                    parentFolderPath = "";
                if(aryAllFolders){
                    aryAllowableFolders = aryAllFolders;
                    jsTreeInstance  = _getInstanceFromContainerDiv();
                    if(jsTreeInstance){
                        arySelectedFiles = jsTreeInstance.get_selected(true);
                        if($.isArray(arySelectedFiles) && arySelectedFiles.length>0){
                            
                            for(i=0;i<= arySelectedFiles.length-1;i++){
                                
                                objNodeData = arySelectedFiles[i];
                                nodeOriginalData = objNodeData && objNodeData.original;
                                
                                if(nodeOriginalData){
                                    strNodeId = _treeNodeOriginalDataHelper.getId(nodeOriginalData) ;
                                    aryDirectoryPath = strNodeId.split(MF_HTML5_CONSTANTS.directorySeparator);
                                    if(_treeNodeOriginalDataHelper
                                        .isFileSelectedADirectory(nodeOriginalData) === true){
                                        
                                            if($.isArray(aryDirectoryPath) && aryDirectoryPath.length>0){
                                             //after split if it is a directory then its length should be greater than or equal to 2
                                             for(k=0;k<=aryDirectoryPath.length-3;k++)  {
                                                parentFolderPath += aryDirectoryPath[k]+MF_HTML5_CONSTANTS.directorySeparator; 
                                             }
                                        }
                                    }
                                    else{
                                        //not a directory
                                        if($.isArray(aryDirectoryPath) && aryDirectoryPath.length>0){
                                          //after split if it is a directory then its length should be greater than or equal to 2
                                           for(k=0;k<=aryDirectoryPath.length-2;k++)  {
                                              parentFolderPath += aryDirectoryPath[k]+MF_HTML5_CONSTANTS.directorySeparator; 
                                           }
                                         }
                                    }
                               }
                               aryAllowableFolders = $.grep(aryAllowableFolders,function(value,index){
                                     if(value != MF_HTML5_CONSTANTS.rootPath){
                                         return value !== parentFolderPath;
                                     }
                                     else{
                                         return true;
                                     }
                                 
                               });
                                parentFolderPath = "";
                            }
                        }
                      
                        for(i=0;i<= aryAllowableFolders.length-1;i++){
                            strOptions += '<option value="'+aryAllowableFolders[i]+'">'+aryAllowableFolders[i]+'</option>';
                        }
                        _getPageHtmlCntrls.divFilesMoveTo.ddlHtml5AppFileMoveTo().html(strOptions);
                        showModalPopUp("divMoveHtml5AppFilesCont","Move Folders / Files",400);
                    }
                }
            },
            fileBrowserMoveFilesSave:function(){
                _fileBrowserMoveSave();
            },
            getAllFoldersFromHidField:function(){
                return _getAllFoldersFromHidField();
            }
        };
    })();
    
    var _html5ObjectsDtlsHelper = (function(){
        var _app =(function(){
            function _getAppObjectJsonFromHidField(){
               return _hidFieldsHelper.hidHtml5AppDetail.getValue();
            }
            function _getAppObjectFromHidField(){
                var strAppDtl = _getAppObjectJsonFromHidField();
                var objAppDtl = null;
                if(!mfUtil.isNullOrUndefined(strAppDtl) ||
                   !mfUtil.isEmptyString(strAppDtl)){
                    
                    objAppDtl = $.parseJSON(strAppDtl);
                }
                return new Html5App(objAppDtl);
            }
            function _getHtmlFilesInRootFolderJsonFromHidField(){
                return _hidFieldsHelper.hidHtml5AppRootHtmlFiles.getValue();
            }
            function _getHtmlFilesInRootFolderFromHidField(){
                var strHtmlFiles = _getHtmlFilesInRootFolderJsonFromHidField();
                var aryHtmlFiles = [];
                if(!mfUtil.isNullOrUndefined(strHtmlFiles) ||
                   !mfUtil.isEmptyString(strHtmlFiles)){
                    
                    aryHtmlFiles = $.parseJSON(strHtmlFiles);
                }
                return aryHtmlFiles;
            }
            return {
                getAppObjectFromHidField:function(){
                    return _getAppObjectFromHidField();
                },
                getHtmlFilesInRootFolderFromHidField:function(){
                    return _getHtmlFilesInRootFolderFromHidField();
                }
            };
        })();
        var _file = (function(){
            function _getFileObjectJsonFromHidField(){
               return _hidFieldsHelper.hidHtml5AppFileDetail.getValue();
            }
            function _getFileObjectFromHidField(){
                var strFileDtl = _getFileObjectJsonFromHidField();
                var objFileDtl = null;
                if(!mfUtil.isNullOrUndefined(strFileDtl) ||
                   !mfUtil.isEmptyString(strFileDtl)){
                    
                    objFileDtl = $.parseJSON(strFileDtl);
                }
                return new Html5AppFile(objFileDtl);
            }
            return {
                getFileObjectFromHidField:function(){
                    return _getFileObjectFromHidField();
                }
            };
        })();
        return {
          app:{
            //   getAppName:function(){
            //       return "testApp";
            //   },
              getAppObjectFromHidField:function(){
                  return _app.getAppObjectFromHidField();
              },
              getHtmlFilesInRootFolderFromHidField:function(){
                  return _app.getHtmlFilesInRootFolderFromHidField();
              }
          },
          file:{
              getFileObjectFromHidField:function(){
                  return _file.getFileObjectFromHidField();
              }
          }
        };
    })();
    
    var _html5ObjectPropSheet=(function(){
        
        function _getCurrentWorkingApp(){
            return $('#divHtml5AppProperties').data(MF_HELPERS.getKeyForStoredData());
        }
        function _setOptionsOfStartupPageDdl(){
            var aryHtmlFiles = _html5ObjectsDtlsHelper.app.getHtmlFilesInRootFolderFromHidField();
            var strOptions = "";
            if($.isArray(aryHtmlFiles) && aryHtmlFiles.length>0){
                for(var i=0;i<=aryHtmlFiles.length-1;i++){
                    strOptions = '<option value="'+aryHtmlFiles[i]+'">'+aryHtmlFiles[i]+'</option>';  
                }
            }
            else{
                strOptions = "";
            }
            var ddlStartupPage = PROP_JSON_HTML_MAP.html5App.propSheetCntrl.Properties.getStartupForm();
            ddlStartupPage.control.html(strOptions);
        }
        return{
            formAppPropSheet:function(){
                var objApp = _html5ObjectsDtlsHelper.app.getAppObjectFromHidField();
                if(mfUtil.isNullOrUndefined(objApp)){
                    throw new MfError(["Application not found."]);
                }
                $('#divHtml5AppProperties').mficientCntrlProps({
                        propertySheetJson:mFicientIde.PropertySheetJson.Html5App,
                        editObject:objApp
                });
            },
            formFilePropSheet:function(){
               var objFile = _html5ObjectsDtlsHelper.file.getFileObjectFromHidField();
               if(mfUtil.isNullOrUndefined(objFile)){
                   throw new MfError(["File not found."]);
               }
               $('#divHtml5AppFileProperties').mficientCntrlProps({
                       propertySheetJson:mFicientIde.PropertySheetJson.Html5AppFile,
                       editObject:objFile
               });
            },
            clearFilePropSheet:function(){
                try{
                    $('#divHtml5AppFileProperties').removeData(MF_HELPERS.getKeyForStoredData()) ;
                }
                catch(error){
                }
                $('#divHtml5AppFileProperties').html('');
            },
            getCurrentWorkingApp:function(){
                return _getCurrentWorkingApp();
            },
            setOptionsOfStartupPageDdl:function(){
              _setOptionsOfStartupPageDdl();
            },
            setValueOfStartupPageDdl:function(){
                var objApp = _html5ObjectPropSheet.getCurrentWorkingApp();
                if(objApp){
                  var ddlStartupPage = PROP_JSON_HTML_MAP.html5App.propSheetCntrl.Properties.getStartupForm();  
                  var strStartupPage = objApp.fnGetStartupPage();
                  if(!mfUtil.isNullOrUndefined(strStartupPage) &&
                     !mfUtil.isEmptyString(str)){
                        ddlStartupPage.control.val(strStartupPage) ;
                    }
                    else{
                        
                    }
                }
            }
        };
    })();
    
    var _fileUploaderInput = (function(){
        function _getContainerDiv(){
            return $('#divFileUploaderInput');
        }
        function _removeFileSelected(){
            _getPageHtmlCntrls.divFileCreate.uploadHtml5File().val("") ;
        }
        return {
          showHideContainerDiv  :function(blnShow){
              if(blnShow === true){
                  _getContainerDiv().show();
              }
              else{
                 _getContainerDiv().hide(); 
              }
          },
          setDataOfContDiv:function(data/*object*/){
              /*{fileType : MF_HTML5_CONSTANTS.fileTypes}*/
              _getContainerDiv().data(mficientConstants.jqueryCommonDataKey,data);
          },
          getDataOfContDiv :function(){
             return _getContainerDiv().data(mficientConstants.jqueryCommonDataKey);
          },
          getFileTypeFromGlobalData:function(){
              var objData = this.getDataOfContDiv();
               if(objData){
                   return objData.fileType;
               }
               else{
                   return null;
               }
          },
          deleteDataOfContDiv :function(){
              _getContainerDiv().removeData(mficientConstants.jqueryCommonDataKey);
          },
          removeFileSelected:function()  {
             _removeFileSelected();
          },
          hasFile:function(){
            var fileName = _getPageHtmlCntrls.divFileCreate.uploadHtml5File().val(); 
            if(!mfUtil.isEmptyString(fileName)){
                return true;
            }
            else{
                return false;
            }
          },
          spanRemoveFileInFileUploader:function() {
              var fileType = this.getFileTypeFromGlobalData();
              _removeFileSelected();
              if(fileType === MF_HTML5_CONSTANTS.fileTypes.image ||
                fileType === MF_HTML5_CONSTANTS.fileTypes.zip){
                    _getPageHtmlCntrls.divFileCreate.btnAddFile().text('Upload');
                }
                else{
                    _getPageHtmlCntrls.divFileCreate.btnAddFile().text('Add');
                }
             _hidFieldsHelper.hidDragDropFileBytes.setValue("");
             _hidFieldsHelper.hidDragDropFileName.setValue("");
           },
          uploadFile:function(){
              var strFileType = this.getFileTypeFromGlobalData(),
              strFileNameWithExtension = "",
              uploaderInputHasFile = this.hasFile();
              if(uploaderInputHasFile === true){
                  strFileNameWithExtension = 
                           _hidFieldsHelper.hidDragDropFileName.getValue();
                  if(strFileType === MF_HTML5_CONSTANTS.fileTypes.zip){
                    var  objApp = _html5ObjectPropSheet.getCurrentWorkingApp(),
                         strAppName = objApp.fnGetName(),
                         strAppId = objApp.fnGetId();
                     var data =[
                         strAppName,strAppId
                       ];
                      processPostbackByHtmlCntrl.postBack(
                          postBackByHtmlProcess.Html5ZipFileAdd,
                           data, "");
                  }
                  else{
                      _createNewFileHelper({
                         fileNameWithExtension : strFileNameWithExtension,
                         fileType : strFileType,
                         useFileBytes : true,
                         confirmCancelCallBack : null
                      });
                  }
              }
              else{
                  throw new MfError(["Please select a file to upload"]);
              }
          }
        };
    })();
    
    var _html5AppCommit=(function(){
         return {
           showGetCommitAppVersionPopup:function(){
               showModalPopUp('divHtml5CommitApp','Commit',400);
           },
           closeGetCommitAppVersionPopup:function(){
                closeModalPopUp('divHtml5CommitApp');
            }
         };
     })();
     
    var _destroyHtmlObjects = (function(){
        function _destroyAppProperties(){
            $('#divHtml5AppProperties').removeData(
                        MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
            $('#divHtml5AppProperties').removeData(
                        MF_IDE_CONSTANTS.JqueryDataKeyForPropSheet);            
        }
        function _destroyFileProperties(){
            $('#divHtml5AppFileProperties').removeData(
                        MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
                        
            $('#divHtml5AppFileProperties').removeData(
                        MF_IDE_CONSTANTS.JqueryDataKeyForPropSheet);            
        }
        
        function _destroyTreeView(){
            _fileExplorer.destroyFileTree();
        }
        return {
            destroyAppProperties:function(){
                _destroyAppProperties();
            },
            destroyFileProperties:function(){
                _destroyFileProperties();
            },
            destroyTreeView:function(){
                _destroyTreeView();
            },
            destroyCompleteAppObject:function(){
                _destroyAppProperties();
                _destroyFileProperties();
                _destroyTreeView();
            }
        };
    })(); 
    
    var _mainMenuAndCanvasSeparator = (function(){
        function _setApplicationName(appName){
            var $spanHeaderTitle = 
                    _getPageHtmlCntrls
                    .divMainMenuAndCnavasSeparator
                    .spanHtml5AppHeaderTitle();
            
            if(appName){
                $spanHeaderTitle.text(appName);
            }
            else{
                $spanHeaderTitle.text("");
            }
        }
        return {
            setApplicationName:function(appName){
                _setApplicationName(appName);
            }
        };
    })();
    
    var _showHideHtml = {
        
        showHideMainHtmlIdeCont:function(blnShow){
            if(blnShow === true){
                $('#divProcessHTML5App').show();
            }
            else{
                $('#divProcessHTML5App').hide();
            }
        }
        
    }
    
    return {
        getApplicationName :function(){
            _getPageHtmlCntrls.createNewHtml5App.txtHtml5AppName().val('');   
            showModalPopUp("divGetHtml5AppName","Application Name",400,false,null,DialogType.Info); 
        },
        closeGetApplicationName:function(){
            _getPageHtmlCntrls.createNewHtml5App.txtHtml5AppName().val('');   
            closeModalPopUp("divGetHtml5AppName");
        },
        getHtmlControls:{
           radioFileTypes: function(){
             return  _getPageHtmlCntrls.radioFileTypes();
           },
           radioFileCreateMode:function(){
              return  _getPageHtmlCntrls.radioFileCreateMode();
           },
           hiddenFields:{
               hidAppTreeView:function(){
                   return _getPageHtmlCntrls.hiddenFields.hidAppTreeView();
               },
               hidAppFolderSelected:function(){
                   return _getPageHtmlCntrls.hiddenFields.hidAppFolderSelected();
               },
               hidHtml5AppDetail:function(){
                   return _getPageHtmlCntrls.hiddenFields.hidHtml5AppDetail();
               }
           }
        },
        cntrlEvntHandlers:{
           btnAddNewFile : function(sender,evnt){
              _addNewFile.setDefaultsOfControls();
              showModalPopUp('SubProcAddNewFile', " Add New File(s) ",600);
           },
           rdbCreateMode :function(sender,evnt){
            var value = sender.find(":checked").val();
            if (value == "CRT") {//Create
                $('#divHtml5NewFileNameCont').show();
                var fileType = _createEmptyNewFile.getFileTypeFromGlobalData();
                _getPageHtmlCntrls.divFileCreate.lblFileExt().text(
                    MF_HTML5_HELPERS.getFileTypeExtentionByEnumVal(fileType));
                
            }
            else if (value == "UPL") {//Upload
                $('#divHtml5NewFileNameCont').hide();
            }
           },
           btnAddFile:function(sender,evnt){
              try{
                  //_createEmptyNewFile.setDefaultsOfControls();
                  //_createEmptyNewFile.showHideTxtFileNameCont(false);
                  var fileType = 
                        _getPageHtmlCntrls.divAddNewFile
                        .radioFileTypes().filter('input:checked').val();
                        
                    var uploaderInputHasFile = _fileUploaderInput.hasFile();    
                  if(fileType === MF_HTML5_CONSTANTS.fileTypes.image 
                    || fileType === MF_HTML5_CONSTANTS.fileTypes.zip){
                        
                        if(fileType === MF_HTML5_CONSTANTS.fileTypes.image){
                           _hidFieldsHelper.hidAppFolderSelected.setValue(
                                MF_HTML5_CONSTANTS.rootPath
                           ); 
                        }
                        else{
                           _hidFieldsHelper.hidAppFolderSelected.setValue(
                                MF_HTML5_CONSTANTS.rootPath
                           );  
                        }
                        _fileUploaderInput.uploadFile();
                        
                    }
                    else{
                        if(uploaderInputHasFile === true){
                            _hidFieldsHelper.hidAppFolderSelected.setValue(
                              _getPageHtmlCntrls.divFileCreate.ddlUploadFolderPath().val()
                            );
                            _fileUploaderInput.uploadFile();
                        }
                        else{
                            _hidFieldsHelper.hidAppFolderSelected.setValue(
                                _getPageHtmlCntrls.divFileCreate.ddlFolderPathForCreateNewFile().val()
                            );
                            _createEmptyNewFile.createNewFile();
                        }
                    }
               }
               catch(error){
                   if (error instanceof MfError){
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                   }else{
                       console.log(error);
                   }
               }
           },
           onDragEnter:function(e){
              e.stopPropagation();
              e.preventDefault();
          },
           onDragOver:function(e){
            e.stopPropagation();
            e.preventDefault();
          },
           onDrop :function(e) {
              try{
                  e.stopPropagation();
                  e.preventDefault();
                  var selectedFile = e.dataTransfer.files[0];
                  //var reader = new FileReader();
                  _handleFilesOnDrop(selectedFile);
              }
              catch(error){
                  if (error instanceof MfError){
                      showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                  }else{
                      console.log(error);
                  }
              }
            },
           refreshFileBrowser:function(sender,evnt) {
              alert('refresh file browser');
            },
           fileBrowserAddNewFile:function(sender,evnt) {
            _addNewFile.setDefaultsOfControls();
            showModalPopUp('SubProcAddNewFile', " Add New File(s) ", 620);
           },
           fileBrowserAddNewFolder:function(sender,evnt) {
                var $txtHtml5AppNewFolderName = _getPageHtmlCntrls.divGetNewFolderName.txtHtml5AppNewFolderName();
                var $ddlHtml5AppNewFolderSavePath = _getPageHtmlCntrls.divGetNewFolderName.ddlHtml5AppNewFolderSavePath();
                $txtHtml5AppNewFolderName.val('');
                $ddlHtml5AppNewFolderSavePath.html('');
                var strOptionsForDdl = _getOptionsForZipFolderPathDdl();
                if(strOptionsForDdl){
                    $ddlHtml5AppNewFolderSavePath.html(strOptionsForDdl);
                    showModalPopUp('divGetHtml5AppNewFolderName',"Folder Name",450);
                }
           },
           btnCreateNewFile :function(sender,evnt){
                try{
                    var fileType = _createEmptyNewFile.getFileTypeFromGlobalData();
                    if(mfUtil.isNullOrUndefined(fileType)){
                        throw mfExceptionMsgs.filetypeNotFound;
                    }
                    var strUploaderPopupHeaderText="File Uploader";
                    _fileUploaderDragDrop.setDataOfContDiv({
                        fileType :fileType 
                    });
                    _createEmptyNewFile.createNewFile();
                    //strUploaderPopupHeaderText = _fileUploaderDragDrop.getUploaderPopupHeaderText(fileType);
                    //showModalPopUp('divFileUploaderDragDrop',strUploaderPopupHeaderText, 480, false, false);
                }
                catch(error){
                     if (error instanceof MfError){
                         showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                     }else{
                         console.log(error);
                     }
                 }
            },
           rdbFileTypes:function(sender,evnt)  {
              var strFileType = sender.val();
              var strOptionsForDropDown = "";
              switch(strFileType){
                 case MF_HTML5_CONSTANTS.fileTypes.image:
                 case MF_HTML5_CONSTANTS.fileTypes.zip:     
                     $('#divSeparatorOrForCreateFile').hide();
                     _createEmptyNewFile.showHideTxtFileNameCont(false);
                     break;
                 case MF_HTML5_CONSTANTS.fileTypes.js:
                 case MF_HTML5_CONSTANTS.fileTypes.css:
                 case MF_HTML5_CONSTANTS.fileTypes.html:
                 case MF_HTML5_CONSTANTS.fileTypes.xml:
                 case MF_HTML5_CONSTANTS.fileTypes.txt: 
                     $('#divSeparatorOrForCreateFile').show();
                     _createEmptyNewFile.showHideTxtFileNameCont(true);
                    break;
                 
              }
              _fileUploaderInput.showHideContainerDiv(true);
              _fileUploaderDragDrop.showHideContainerDiv(true);
              
              
               _fileUploaderDragDrop.setDataOfContDiv({
                   fileType :strFileType 
               });
               _fileUploaderInput.setDataOfContDiv({
                   fileType :strFileType
               });
               _createEmptyNewFile.setDataOfContDiv({
                   fileType :strFileType
               });
              _fileUploaderInput.removeFileSelected();
              _getPageHtmlCntrls.divFileCreate.btnAddFile().text('Add');
               if(strFileType ===MF_HTML5_CONSTANTS.fileTypes.image 
                    || strFileType === MF_HTML5_CONSTANTS.fileTypes.zip){
                         
                    _getPageHtmlCntrls.divFileCreate.btnAddFile().text('Upload');
                    if(strFileType ===MF_HTML5_CONSTANTS.fileTypes.image){
                       strOptionsForDropDown = _getOptionsForZipFolderPathDdl(); 
                    }
                    else{
                       strOptionsForDropDown = '<option value="'+MF_HTML5_CONSTANTS.rootPath+'">'+MF_HTML5_CONSTANTS.rootPath+'</option>'; 
                    }
                    _getPageHtmlCntrls.divFileCreate.ddlUploadFolderPath().html(strOptionsForDropDown);
                    _getPageHtmlCntrls.ddlFolderPathForCreateNewFile().html(strOptionsForDropDown);
                }
                else{
                   
                   //_createEmptyNewFile.setDataOfContDiv({
                //  fileType :strFileType 
                //}); 
                   
                   _getPageHtmlCntrls.divFileCreate.spanCreateFilePathInfo().text(
                       _getSelectedFilePathInfoForUI());
                  
                  strOptionsForDropDown = _getOptionsForZipFolderPathDdl();
                  _getPageHtmlCntrls.divFileCreate.ddlUploadFolderPath().html(strOptionsForDropDown);
                  _getPageHtmlCntrls.divFileCreate.ddlFolderPathForCreateNewFile().html(strOptionsForDropDown);
                  _getPageHtmlCntrls.divFileCreate.lblFileExt().text(
                        MF_HTML5_HELPERS.getFileTypeExtentionByEnumVal(strFileType));
               }
           },
           uploadHtml5File:function(sender,evnt){
              try{
                  evnt.stopPropagation();
                  evnt.preventDefault();
                  var fileList = sender.context && sender.context.files;
                  if(!mfUtil.isNullOrUndefined(fileList) && fileList.length>0){
                    _handleFilesOnUploadFileChange(fileList[0]);
                    _getPageHtmlCntrls.divFileCreate.btnAddFile().text('Upload');
                  }
              }
              catch(error){
                  if (error instanceof MfError){
                      showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                  }else{
                      console.log(error);
                  }
              }
          },
           lnkSaveFileOnEdit:function(sender,evnt){
               try{
                  var  objApp = _html5ObjectPropSheet.getCurrentWorkingApp(),
                      strAppName = objApp.fnGetName(),
                      strAppId = objApp.fnGetId();
                  var data =[
                      strAppId
                    ];
                  _fileViewer.aceEditor.setTextFromEditorInHidField();
                   processPostbackByHtmlCntrl.postBack(
                        postBackByHtmlProcess.Html5AppFileEditSave,
                        data, "");
               }
               catch(error){
                   if (error instanceof MfError){
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                   }else{
                       console.log(error);
                   }
               }
           },
           lnkSearchFileOnEdit:function(sender,evnt){
               try{
                   _fileViewer.aceEditor.executeFindAndReplace();
               }
               catch(error){
                   if (error instanceof MfError){
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                   }else{
                       console.log(error);
                   }
               }
           },
           lnkCloseFileOnEdit:function(sender,evnt){
               try{
                   $.confirm('Are you sure you want to close this file.',
                      function(){
                        _fileViewer.closeFileViewer();  
                      } ,
                       null);
                   showDialogImage(DialogType.Warning);
                   
               }
               catch(error){
                   if (error instanceof MfError){
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                   }else{
                       console.log(error);
                   }
               }
           },
           spanRemoveFileInFileUploader:function(sender,evnt) {
              try{
                _fileUploaderInput.spanRemoveFileInFileUploader();
              }
              catch(error){
                  if (error instanceof MfError){
                      showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                            DialogType.Error);
                  }else{
                      console.log(error);
                  }
              }
           },
           btnSaveHtml5AppName:function(sender,evnt){
               try{
                 _processSaveNewHtml5App();
               }
               catch(error){
                   if (error instanceof MfError){
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                             DialogType.Error);
                   }else{
                       console.log(error);
                   }
               }
           },
           lnkHtml5AppSave:function(sender,evnt){
               try{
                 _processUpdateHtml5App();
               }
               catch(error){
                   if (error instanceof MfError){
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                             DialogType.Error);
                   }else{
                       console.log(error);
                   }
               }
           },
           lnkHtml5AppSaveAs:function(sender,evnt){
                try{
                  _getPageHtmlCntrls.saveAsHtml5App.txtHtml5AppSaveAsName().val('');
                  showModalPopUp("divHtml5AppSaveAs","Save App",400);
                }
                catch(error){
                    if (error instanceof MfError){
                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                              DialogType.Error);
                    }else{
                        console.log(error);
                    }
                }
            },
           btnHtml5AppSaveAsSave:function(sender,evnt){
                 try{
                   _processSaveAsHtml5App();
                 }
                 catch(error){
                     if (error instanceof MfError){
                         showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                               DialogType.Error);
                     }else{
                         console.log(error);
                     }
                 }
            },
           btnHtml5AppSaveAsCancel:function(sender,evnt){
               try{
                 closeModalPopUp("divHtml5AppSaveAs");
               }
               catch(error){
                   if (error instanceof MfError){
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                             DialogType.Error);
                   }else{
                       console.log(error);
                   }
               }
            },
           lnkHtml5AppProduction:function(sender,evnt){
                 try{
                   _processCommitHtml5App();
                 }
                 catch(error){
                     if (error instanceof MfError){
                         showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                               DialogType.Error);
                     }else{
                         console.log(error);
                     }
                 }
            },
           btnHtml5CommitAppSave:function(sender,evnt){
                  try{
                    _processCommitHtml5AppSave();
                  }
                  catch(error){
                      if (error instanceof MfError){
                          showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                                DialogType.Error);
                      }else{
                          console.log(error);
                      }
                  }
             },
           lnkHtml5AppTester:function(sender,evnt){
               try{
                 _processCommitHtml5AppForTester();
               }
               catch(error){
                   if (error instanceof MfError){
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                             DialogType.Error);
                   }else{
                       console.log(error);
                   }
               }
          },
           fileBrowserMoveFiles:function(sender,evnt){
               try{
                 _fileExplorer.fileBrowserMoveFiles();
               }
               catch(error){
                   if (error instanceof MfError){
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                             DialogType.Error);
                   }else{
                       console.log(error);
                   }
               }
           },
           btnHtml5AppFileMoveToSave:function(sender,evnt){
               try{
                 _fileExplorer.fileBrowserMoveFilesSave();
               }
               catch(error){
                   if (error instanceof MfError){
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                             DialogType.Error);
                   }else{
                       console.log(error);
                   }
               }
           },
           fileBrowserDownloadZip:function(sender,evnt){
                 try{
                   _processDownloadZipFile();
                 }
                 catch(error){
                     if (error instanceof MfError){
                         showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                               DialogType.Error);
                     }else{
                         console.log(error);
                     }
                 }
            },
           btnHtml5AppSaveNewFolder:function(sender,evnt){
                try{
                   _processSaveNewFolder();
                }
                catch(error){
                    if (error instanceof MfError){
                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                            DialogType.Error);
                    }else{
                         console.log(error);
                    }
                }
            },
           lnkHtml5AppClose:function(sender,evnt){
               try{
                  _processHtml5AppClose();
               }
               catch(error){
                   if (error instanceof MfError){
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                           DialogType.Error);
                   }else{
                        console.log(error);
                   }
               }
           },
           lnkHtml5AppDelete:function(sender,evnt){
               try{
                  _processHtml5AppDelete();
               }
               catch(error){
                   if (error instanceof MfError){
                       showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                           DialogType.Error);
                   }else{
                        console.log(error);
                   }
               }
           }
        },
        fileExplorer :{
            processFormTreeView:function(){
                try{
                    if(_fileExplorer.doesFileExplorerDataExists()){
                        _fileExplorer.formTreeView();
                    }
                    else{
                        _fileExplorer.clearContainerDiv();
                    }
                }
                catch(error){
                    if (error instanceof MfError){
                        showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                    }else{
                        console.log(error);
                    }
                }
            }
        },
        fileViewer:{
            processViewFileOnEdit:function(){
                try{
                   var strFileData = _hidFieldsHelper.hidAppFileSelected.getValue();
                   if(!mfUtil.isEmptyString(strFileData)){
                      _fileViewer.viewFileOnEdit(
                          _fileExplorer.treeNodeOriginalDataHelper.deserialise(strFileData));
                      
                      _hidFieldsHelper.hidSelectedFileContent.setValue("");      
                   }
                }
                catch(error){
                    if (error instanceof MfError){
                        showMessage(
                            MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),
                            DialogType.Error);
                    }else{
                        console.log(error);
                    }
                }
            },
            aceEditor:{
                changeEventHandler:function(){
                    _fileViewer.aceEditor.changeEventHandler();
                },
                setOnChangeEvent:function(){
                    _fileViewer.aceEditor.setOnChangeEvent();
                },
                removeOnChangeEvent:function(){
                    _fileViewer.aceEditor.removeChangeEvent();
                },
                changeCursorEventHandler:function(){
                    _fileViewer.aceEditor.changeCursorEventHandler();
                }
            },
            imgViewer:{
                
            }
        },
        html5ObjectPropSheet:{
            formFilePropSheet:function(){
               _html5ObjectPropSheet.formFilePropSheet();
           },
            formAppPropSheet:function(){
               _html5ObjectPropSheet.formAppPropSheet();
           },
            setOptionsOfStartupPageDdl:function(){
              _html5ObjectPropSheet.setOptionsOfStartupPageDdl();
            },
            setValueOfStartupPageDdl:function(){
              _html5ObjectPropSheet.setOptionsOfStartupPageDdl();
            }
        },
        initialiseSelectedApp:function(){
            try{
               _fileExplorer.destroyFileTree(false);
               _fileExplorer.clearContainerDiv();
               _fileViewer.aceEditor.setText("");
               _fileViewer.imageViewer.removeSourceOfViewImage("");
               _fileViewer.showHideImageViewerCont(false);
               _fileViewer.showHideFileViewerCont(false);
               _fileViewer.showHideAceEditorViewerCont(false);
               _html5ObjectPropSheet.formAppPropSheet();
               _html5ObjectPropSheet.clearFilePropSheet();
               var objApp = _html5ObjectPropSheet.getCurrentWorkingApp();
               _mainMenuAndCanvasSeparator.setApplicationName(objApp.fnGetName());
            }
            catch(error){
                if (error instanceof MfError){
                    showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs),DialogType.Error);
                }else{
                    console.log(error);
                }
            }
        },
        html5AppCommit:{
            showGetCommitAppVersionPopup :function(){
                _html5AppCommit.showGetCommitAppVersionPopup();
            },
            closeGetCommitAppVersionPopup:function(){
                _html5AppCommit.closeGetCommitAppVersionPopup();
            }
        },
        appIconSelected:function(iconName){
            _appIconSelected(iconName);
        },
        destroyHtmlObjects:{
            destroyAppProperties:function(){
                _destroyHtmlObjects.destroyAppProperties();
            },
            destroyFileProperties:function(){
               _destroyHtmlObjects.destroyFileProperties();
            },
            destroyTreeView:function(){
               _destroyHtmlObjects.destroyTreeView();
            },
            destroyCompleteAppObject:function(){
                _destroyHtmlObjects.destroyAppProperties();
                _destroyHtmlObjects.destroyFileProperties();
                _destroyHtmlObjects.destroyTreeView();
            }
        },
        showHideHtml:{
            showHideMainHtmlIdeCont:function(blnShow){
                _showHideHtml.showHideMainHtmlIdeCont(blnShow);
            }
        },
        destroyAppContainerOnAppClose :function(){
            _destroyHtmlObjects.destroyCompleteAppObject();
            mFicientIde.MfApp.AppDesigner.processChangeHtmlHelper.showHideWfDesignerMainContDiv(false);
            mFicientIde.MfApp.AppDesigner.processChangeHtmlHelper.showHideAppListContDiv(true);
            mFicientIde.MfApp.AppDesigner.processChangeHtmlHelper.showHideMasterPageMenu(true);
            _showHideHtml.showHideMainHtmlIdeCont(false);
        }
    };
})();

$(document).ready(function(){ 
    var html5DragDropBox = document.getElementById("dragandDrop");
    html5DragDropBox.addEventListener("dragenter",mfHtml5App.cntrlEvntHandlers.onDragEnter, false);
    html5DragDropBox.addEventListener("dragover",mfHtml5App.cntrlEvntHandlers.onDragOver, false);
    html5DragDropBox.addEventListener("drop", mfHtml5App.cntrlEvntHandlers.onDrop, false);
    presetHtml5IdeSize();
    
    $('#btnAddNewFile').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.btnAddNewFile($(this),evnt);
        return false;
    });
    $('[id$="rdbCreateMode"]').on('change',function(evnt){
        mfHtml5App.cntrlEvntHandlers.rdbCreateMode($(this),evnt);
    });
    $('[id$="btnAddFile"]').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.btnAddFile($(this),evnt);
    });
    $('#refreshFileBrowser').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.refreshFileBrowser($(this),evnt);
    });
    $('#fileBrowserAddNewFile').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.fileBrowserAddNewFile($(this),evnt);
    });
    $('#fileBrowserAddNewFolder').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.fileBrowserAddNewFolder($(this),evnt);
    });
    $('#btnCreateNewFile').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.btnCreateNewFile($(this),evnt);
    });
    $('input[name="rdbFileTypes"]:radio').on('change',function(evnt){
        mfHtml5App.cntrlEvntHandlers.rdbFileTypes($(this),evnt);
    });
    $('input[type="file"][name="uploadHtml5File"]').on('change',function(evnt){
        mfHtml5App.cntrlEvntHandlers.uploadHtml5File($(this),evnt);
    });
    $('#lnkSaveFileOnEdit').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.lnkSaveFileOnEdit($(this),evnt);
    });
    $('#lnkSearchFileOnEdit').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.lnkSearchFileOnEdit($(this),evnt);
    });
    $('#lnkCloseFileOnEdit').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.lnkCloseFileOnEdit($(this),evnt);
    });
    $('#spanRemoveFileInFileUploader').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.spanRemoveFileInFileUploader($(this),evnt);
    });
    $('#btnSaveHtml5AppName').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.btnSaveHtml5AppName($(this),evnt);
        return false;
    });
    $('#lnkHtml5AppSave').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.lnkHtml5AppSave($(this),evnt);
        return false;
    });
    $('#lnkHtml5AppSaveAs').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.lnkHtml5AppSaveAs($(this),evnt);
        return false;
    });
    $('#lnkHtml5AppDelete').on('click',function(evnt){
       mfHtml5App.cntrlEvntHandlers.lnkHtml5AppDelete($(this),evnt);
       return false;
    });
    $('#btnHtml5AppSaveAsSave').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.btnHtml5AppSaveAsSave($(this),evnt);
        return false;
    });
    $('#btnHtml5AppSaveAsCancel').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.btnHtml5AppSaveAsCancel($(this),evnt);
        return false;
    });
    $('#lnkHtml5AppProduction').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.lnkHtml5AppProduction($(this),evnt);
        return false;
    });
    $('#btnHtml5CommitAppSave').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.btnHtml5CommitAppSave($(this),evnt);
        return false;
    });
    $('#lnkHtml5AppTester').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.lnkHtml5AppTester($(this),evnt);
        return false;
    });
    $('#fileBrowserMoveFiles').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.fileBrowserMoveFiles($(this),evnt);
        return false;
    });
    $('#btnHtml5AppFileMoveToSave').on('click',function(evnt){
       mfHtml5App.cntrlEvntHandlers.btnHtml5AppFileMoveToSave($(this),evnt);
       return false;
    });
    $('#fileBrowserDownloadZip').on('click',function(evnt){
       mfHtml5App.cntrlEvntHandlers.fileBrowserDownloadZip($(this),evnt);
       return false;
    });
    $('#btnHtml5AppSaveNewFolder').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.btnHtml5AppSaveNewFolder($(this),evnt);
        return false;
     });
    $('#imgNewIconSelectNext').on('click',function(evnt){
        mfIconsGallery.showNextIconsSet();
        evnt.stopPropagation();
    });
    $('#imgNewIconSelectPrevious').on('click',function(evnt){
        mfIconsGallery.showPreviousIconsSet();
        evnt.stopPropagation();
    });
    $('#lnkHtml5AppClose').on('click',function(evnt){
        mfHtml5App.cntrlEvntHandlers.lnkHtml5AppClose($(this),evnt);
        return false;
    });
});
function testDownladingZipFile(){
    //showWaitModal()
    // Create an IFRAME.
    var iframe = document.createElement("iframe");

    // Point the IFRAME to GenerateFile, with the
    //   desired region as a querystring argument.
    iframe.src = 'DownloadHtml5Zip.aspx?path='+$('[id$="hidHtml5AppZipDownloadPath"]').val();

    // This makes the IFRAME invisible to the user.
    //iframe.style.display = "none";

    // Add the IFRAME to the page.  This will trigger
    //   a request to GenerateFile now.
    //document.body.appendChild(iframe);
    $('#divHtml5AppDownload').html(iframe);
    showModalPopUp('divHtml5AppDownload',"App Download",400);
}
var mfIconsGallery = (function(){
        var IconArry;
        //Get all images from json
        function _createImageGalleryFromJson() {
            var iconListJson = $('input[type="hidden"][id$=hidAllIconsList]','#divHiddenFields').val();
            if(iconListJson){
                IconArry = $.parseJSON(iconListJson);
                PreLoadImages(IconArry);
                $('#hdfNewStartIconIndex').val('0');
                $('#hdfNewEndIconIndex').val('99');
                CreateIconGallary(IconArry, 0, 99, IconArry.length);
                $('#NewIconGalleryText').html('Page ' + GetCurrentPageInImageGallery() + ' of ' + GetTotalPagesInImageGallery());
               // _setEventOfIconClick(cbOnImageSelect);
            }
            else{
                $('#hdfNewStartIconIndex').val('0');
                $('#hdfNewEndIconIndex').val('99');
            }
        }

        //Get total pages in image gallary
        function GetTotalPagesInImageGallery() {
            var Count = Math.round( IconArry.length / 100);
            if (IconArry.length > Count) {
                Count = Count + 1;
            }
            return Count;
        }

        //get current page in image gallery
        function GetCurrentPageInImageGallery() {
            var Count = parseInt($('#hdfNewStartIconIndex').val(), 10) / 100;
            Count = Count + 1;
            return Count;
        }

        //preload all iumage
        function PreLoadImages(_ImagesArray) {
            $(_ImagesArray).each(function () {
                var img = $('<img/>');
                img[0].src = ApplicaionIconPath + '/' + this;
                img.css('width',32).css('height',32);
            });
        }
        //preload images from array
        function PreLoadImagesFromArray(_ImageArray, _StartIndex, _EndIndex, _Length) {
            for (var i = _StartIndex; i <= _EndIndex + 1; i++) {
                if (i < _Length) {
                    var img = $('<img/>');
                    img[0].src = ApplicaionIconPath + '/' + _ImageArray[i];
                    img.css('width', '32px').css('height', '32px');
                }
            }
        }

        //Create icon gallery
        function CreateIconGallary(_ArryIcon, _StartPosition, _EndPosition, _Length) {
            PreLoadImages(IconArry);
            var strHtml = '<div class="IconTable"><div class="Fleft IconRow">';
            var strCol = "";
            var rowIndex = 0;
            var colIndex = 0;
            for (var i = _StartPosition; i <= _EndPosition + 1; i++) {
                if (colIndex > 9) {
                    strHtml += strCol;
                    strHtml += '</div><div class="clear"></div><div class="Fleft IconRow">';
                    colIndex = 0;
                    rowIndex = rowIndex + 1;
                    strCol = '';
                }
                if (i < _Length) {
                    strCol += '<div id="divImgNewIcon_' + rowIndex + '_' + colIndex + '"  class="IconCol ImageIconCont"  title="' + _ArryIcon[i] + '" >'
                              + '<img id="imgNewIcon_' + rowIndex + '_' + colIndex + '" alt="" src="' + ApplicaionIconPath + '/' + _ArryIcon[i] + '" class="IconImg" />'
                              + '<input id="imgNewIconName_' + rowIndex + '_' + colIndex + '" type="hidden"  value="' + _ArryIcon[i] + '" />'
                            + '</div>';

                }
                else {
                    strCol += '<div class="IconColEmpty">'
                           + '</div>';
                }
                colIndex = colIndex + 1;
            }
            strHtml += '</div></div><div class="clear"></div>';
            $('#NewIconGallery').html(strHtml);
        }
        function _setEventOfIconClick(cbOnImageSelect){
            $('.ImageIconCont','#divNewAppIconContainer').on('click',imageIconClick(this,cbOnImageSelect));
        }
        //Next page icon click
        function NextIconCollection() {
            if (parseInt($('#hdfNewEndIconIndex').val(), 10) + 1 > IconArry.length) {
                return;
            }
            CreateIconGallary(IconArry, parseInt($('#hdfNewEndIconIndex').val(), 10) + 1, parseInt($('#hdfNewEndIconIndex').val(), 10) + 100, IconArry.length);
            $('#hdfNewStartIconIndex').val(parseInt($('#hdfNewEndIconIndex').val(), 10) + 1);
            $('#hdfNewEndIconIndex').val(parseInt($('#hdfNewEndIconIndex').val(), 10) + 100);
            $('#NewIconGalleryText').html('Page ' + GetCurrentPageInImageGallery() + ' of ' + GetTotalPagesInImageGallery());
        }

        //previous page icon click
        function PrvIconCollection() {
            if (parseInt($('#hdfNewStartIconIndex').val(), 10) - 100 < 0) {
                return;
            }
            CreateIconGallary(IconArry, parseInt($('#hdfNewStartIconIndex').val(), 10) - 100, parseInt($('#hdfNewEndIconIndex').val(), 10) - 100, IconArry.length);
            $('#hdfNewStartIconIndex').val(parseInt($('#hdfNewStartIconIndex').val(), 10) - 100);
            $('#hdfNewEndIconIndex').val(parseInt($('#hdfNewEndIconIndex').val(), 10) - 100);
            $('#NewIconGalleryText').html('Page ' + GetCurrentPageInImageGallery() + ' of ' + GetTotalPagesInImageGallery());
        }

        //image icon on click event
        function imageIconClick(e) {
            try{
                var iconName = $(e).find('[id^="imgNewIconName"]').val();
                return iconName;
            }
            catch(err){
            
            }
        }
        return {
            createImageGallery :function(){
                _createImageGalleryFromJson();
            },
            showNextIconsSet:function(){
                NextIconCollection();
            },
            showPreviousIconsSet:function(){
                PrvIconCollection();
            },
            showIconGallery:function(){
                showModalPopUp('divNewAppIconContainer','App Icons',"930");
            },
            setEventOfImageSelection:function(callBackOnImageSelect){
                $('.ImageIconCont','#divNewAppIconContainer').off('click');
                $('.ImageIconCont','#divNewAppIconContainer')
                .on('click',function(){
                    var iconName = imageIconClick(this);
                    if($.isFunction(callBackOnImageSelect)){
                        callBackOnImageSelect.apply(this,[iconName]);
                    }
                });
            }
        };

})();
// (function ($) {
//     $.fn.progressbar = function (options) {
//         var settings = $.extend({
//             width: '300px',
//             height: '25px',
//             color: '#D8D5D5',
//             padding: '0px',
//             border: '1px solid #ddd'
//         }, options);

//         //Set css to container
//         $(this).css({
//             'width': settings.width,
//             'border': settings.border,
//             'border-radius': '5px',
//             'overflow': 'hidden',
//             'display': 'inline-block',
//             'padding': settings.padding,
//             'margin': '0px 10px 5px 5px'
//         });

//         // add progress bar to container
//         var progressbar = $("<div></div>");
//         progressbar.css({
//             'height': settings.height,
//             'text-align': 'right',
//             'vertical-align': 'middle',
//             'color': '#000',
//             'font-size': 'small',
//             'width': '0px',
//             'line-height': '25px',
//             'border-radius': '3px',
//             'background-color': settings.color
//         });

//         $(this).append(progressbar);

//         this.progress = function (value) {
//             var width = $(this).width() * value / totalFileCount;
//             progressbar.width(width);
//             $('#progressValue').html(value + " file(s) uploaded successfully");
//         }
//         return this;
//     };

// } (jQuery));
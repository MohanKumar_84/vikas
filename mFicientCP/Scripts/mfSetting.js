﻿

///-----------trace Location--------------------
var _properties = [{ prop: "FIRSTNAME", val: "@FIRSTNAME", category: "Fixed Values" }, { prop: "LASTNAME", val: "@LASTNAME", category: "Fixed Values" }, { prop: "FULLNAME", val: "@FULLNAME", category: "Fixed Values" }, { prop: "UID", val: "@UID", category: "Fixed Values" }, { prop: "EMAIL", val: "@EMAIL", category: "Fixed Values" }, { prop: "EMPLOYEEID", val: "@EMPLOYEEID", category: "Fixed Values" }, { prop: "DESIGNATION", val: "@DESIGNATION", category: "Fixed Values" }, { prop: "MOBILE", val: "@MOBILE", category: "Fixed Values" }, { prop: "DOB", val: "@DOB", category: "Fixed Values" }, { prop: "DEVICEID", val: "@DEVICEID", category: "Fixed Values" }, { prop: "DEVICEMODEL", val: "@DEVICEMODEL", category: "Fixed Values" }, { prop: "LATITUDE", val: "@LATITUDE", category: "Fixed Values" }, { prop: "LONGITUDE", val: "@LONGITUDE", category: "Fixed Values" }, { prop: "DATETIME", val: "@DATETIME", category: "Fixed Values" }, { prop: "DATE", val: "@DATE", category: "Fixed Values" }, { prop: "TIME", val: "@TIME", category: "Fixed Values" }, { prop: "DATETIMETICKS", val: "@DATETIMETICKS", category: "Fixed Values" }, { prop: "DATETICKS", val: "@DATETICKS", category: "Fixed Values" }, { prop: "DATEYYYYMMDD", val: "@DATEYYYYMMDD", category: "Fixed Values" }, { prop: "UNIQUEID", val: "@UNIQUEID", category: "Fixed Values" }, { prop: "WIFI", val: "@WIFI", category: "Fixed Values"}];
var IDE_COMMAND_OBJECT_TYPES= {
    None: { id: "0", UiName: "None" },
    db: { id: "1", UiName: "Database" },
    wsdl: { id: "2", UiName: "Webservice ( Soap )" },
    http: { id: "3", UiName: "Webservice ( Http )" },
    xmlRpc: { id: "4", UiName: "Webservice ( Xml RPC )" },
    odata: { id: "5", UiName: "Webservice ( ODATA )" }
};
var DB_COMMAND_TYPE = {
    Select: "1",
    Insert: "2",
    Update: "3",
    Delete: "4"
};


var WEB_SERVICE_TYPE = {
    http: 1,
    wsdlSoap: 2,
    rpcXml: 3
};
var WEB_SERVICE_TYPE_TEXT = {
    http: "http",
    wsdlSoap: "wsdl",
    rpcXml: "rpc"
};

var isNewForm = true;
var mfTrackLocation = {
    ddlSyncCommandTypeChange: function (value/*jquery object*/) {
        var _ddlSyncOfflnOnlnCommand = $('#ddlObject');
        var cmdType = value;
        var strUpCmds = "";
        if (cmdType === "-1") {
            strUpCmds = '<option value="-1">select</option>';
            cmdType = "";
        }
        else {
            switch (cmdType) {
                case IDE_COMMAND_OBJECT_TYPES.db.id:
                    strUpCmds = this._getDbObjectsByCmdTypeByConn();
                    break;
                case IDE_COMMAND_OBJECT_TYPES.wsdl.id:
                case IDE_COMMAND_OBJECT_TYPES.http.id:
                case IDE_COMMAND_OBJECT_TYPES.xmlRpc.id:
                    strUpCmds = this._getCmdsAsOptionsOfDdlByWsType(cmdType);
                    break;
                case IDE_COMMAND_OBJECT_TYPES.odata.id:
                    strUpCmds = this._getCmdsAsOptionsOfDdlByConn();
                    break;
            }
            var _ddlobject = $('#ddlObject');
            _ddlobject.html('');
            _ddlobject.html(strUpCmds);
            _ddlobject.on('change', function (evnt) {
                try {
                    mfTrackLocation.ddlObjectChange($(this).val()); //ddlObject
                }
                catch (error) {

                }
            });
        }
    },

    ddlObjectChange: function (value/*jquery object*/, lp) {
        _colOptions = this.getInputParamsForCmdObject(value, $('#ddlSyncCommandType').val());
        this.drawCustomPropertiesTable(lp);
        this.allTableSelectCSS();
    },
    getInputParamsForCmdObject: function (cmdId, commandType/*IDE_COMMAND_OBJECT_TYPES*/) {
        var aryInputParams = [];
        switch (commandType) {
            case IDE_COMMAND_OBJECT_TYPES.db.id:
                aryInputParams = this._getInputParamsForUIdb(cmdId);
                break;
            case IDE_COMMAND_OBJECT_TYPES.wsdl.id:
                aryInputParams = this.getInputParamsForCmdWs(cmdId, WEB_SERVICE_TYPE.wsdlSoap);
                break;
            case IDE_COMMAND_OBJECT_TYPES.http.id:
                aryInputParams = this.getInputParamsForCmdWs(cmdId, WEB_SERVICE_TYPE.http);
                break;
            case IDE_COMMAND_OBJECT_TYPES.xmlRpc.id:
                aryInputParams = this.getInputParamsForCmdWs(cmdId, WEB_SERVICE_TYPE.xmlRpc);
                break;
            case IDE_COMMAND_OBJECT_TYPES.odata.id:
                aryInputParams = this._getInputParamsForUI(cmdId);
                break;
        }
        return aryInputParams;

    },
    drawCustomPropertiesTable: function (_lp) {
       $('#divProperties').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="tblProperties" style="width:100%"></table>');
        _porpOptions = '';
        $.each(_properties, function (index, obj) {
            _porpOptions += '<option value="' + obj.val + '">' + obj.prop + '</option>';
        });
        if (ExtraProperties.length > 0) {
            var _exProp = JSON.parse(ExtraProperties);
            $.each(_exProp, function (index, eobj) {
                _porpOptions += '<option value="' + eobj.val + '">' + eobj.prop + '</option>';
            });
        }
        if ($('#ddlObject').val() == '-1') _data = [];
        var data = [];
        if (_colOptions && _colOptions.length > 0) {
            $.each(_colOptions, function (index, value) {
                _selectProp = '<select id="_selProp_' + value + '" class="UseUniformCss" style="width:150px !important;" >' + _porpOptions + '</select>';
                data.push([value, _selectProp]);
            });
        }
        $('#tblProperties').dataTable({
            "data": data,
            "bPaginate": false,
            "bFilter": false,
            "bInfo": false,
            "bSort": false,
            "columns": [{ "title": "Input" }, { "title": "Property"}]
        });
        var tableAuth = $('#tblProperties').DataTable();
        if (_lp)
            $.each(_lp, function (index, lp) {
                $('#_selProp_' + lp.para).val(lp.val);
            });
    },
    allTableSelectCSS: function () {
        if (!isNewForm) {
            var _props = $("select[id^='_selProp_']");
            $.each(_props, function (index, _prop) {
                CallUnformCss(_prop);
            });
        }
    },
    removeRow: function (value) {
        var DeleteConfirmation = 'Are you sure you want to delete this.';
        var table = $('#tblProperties').DataTable();
        var row = table.row($(value).parents('tr'));
        var data = row.data();
        var confirmMsg = confirm(DeleteConfirmation);
        if (confirmMsg) {
            row.remove().draw();
        }
    },
    //-----database--------------------------
    _getDbObjectsByCmdTypeByConn: function () {
        var aryDbCmds = this._getAllDbCmdOfSameConn();
        var aryFilteredObjects = [];
        if (aryDbCmds && $.isArray(aryDbCmds) && aryDbCmds.length > 0) {
            aryFilteredObjects = $.grep(aryDbCmds, function (value, index) {
                return value.commandType === DB_COMMAND_TYPE.Insert;
            });
        }
        return this._getCommandsAsOptionsForDdl(aryFilteredObjects, true, "-1");
    },

    _getAllDbCmdOfSameConn: function () {
        return this._parseUICollectionHidFielddb();
    },

    _parseUICollectionHidFielddb: function () {
        var aryDbObjects = [];
        var hidFieldValue = $(':hidden[id$="hidAllDbCommands"]').val();
        if (hidFieldValue) {
            aryDbObjects = $.parseJSON(hidFieldValue);
        }
        return aryDbObjects;
    },

    _getCommandsAsOptionsForDdl: function (cmds) {

        var aryCmds = cmds,
            strOptions = "", i = 0, objDbConn = null;
        strOptions += '<option value=' + '-1' + '>Select</option>';
        if (aryCmds && $.isArray(aryCmds) && aryCmds.length > 0) {
            for (i = 0; i <= aryCmds.length - 1; i++) {
                objDbConn = aryCmds[i];
                if (objDbConn) {
                    strOptions +=
                        '<option value=' + objDbConn.commandId + '>' +
                        objDbConn.commandName + '</option>';
                }
            }
        }
        return strOptions;
    },
    _getCommandObjectdb: function (cmdId) {
        var cmdObj = null;
        var aryCmdObjects = this._parseUICollectionHidFielddb();
        if (aryCmdObjects && aryCmdObjects.length > 0) {
            for (var i = 0; i <= aryCmdObjects.length - 1; i++) {
                cmdObj = aryCmdObjects[i];
                if (cmdObj.commandId === cmdId) {
                    break;
                }
                else {
                    cmdObj = null;
                }
            }
        }
        return cmdObj;
    },
    _getInputParamsForUIdb: function (cmdId) {
        var aryInputParams = [];
        var cmdObj = this._getCommandObjectdb(cmdId);
        if (cmdObj && cmdObj.paramJson) {
            var objFinalInputParam = $.parseJSON(cmdObj.paramJson);
            if (objFinalInputParam) {
                $.each(objFinalInputParam, function (index, value) {
                    var objParamDtl = value;
                    if (objParamDtl) {
                        aryInputParams.push(objParamDtl.para);
                    }
                });
            }
        }
        return aryInputParams;
    },
    //--------------webservice----------------
    _getCmdsAsOptionsOfDdlByWsType: function (wsType/*IDE_COMMAND_OBJECT_TYPES*/) {

        var aryCmds = this._getAllCmdsOfSameConnByWsType(wsType),
        strOptions = "", i = 0, objCmd = null;
        strOptions += '<option value=' + -1 + '>Select</option>';
        if (aryCmds && $.isArray(aryCmds) && aryCmds.length > 0) {
            for (i = 0; i <= aryCmds.length - 1; i++) {
                objCmd = aryCmds[i];
                if (objCmd) {
                    strOptions +=
                        '<option value=' + objCmd.commandId + '>' +
                        objCmd.commandName + '</option>';
                }
            }
        }
        return strOptions;
    },

    _getAllCmdsOfSameConnByWsType: function (wsType/*IDE_COMMAND_OBJECT_TYPES*/) {
        var webServiceType = "";
        switch (wsType) {
            case IDE_COMMAND_OBJECT_TYPES.wsdl.id:
                webServiceType = WEB_SERVICE_TYPE.wsdlSoap;
                break;
            case IDE_COMMAND_OBJECT_TYPES.http.id:
                webServiceType = WEB_SERVICE_TYPE.http;
                break;
            case IDE_COMMAND_OBJECT_TYPES.xmlRpc.id:
                webServiceType = WEB_SERVICE_TYPE.rpcXml;
                break;
        }
        return this._parseUICollectionHidFieldws(webServiceType);
    },

    _parseUICollectionHidFieldws: function (wsType/*WEB_SERVICE_TYPE*/) {
        var aryAllWsObjects = this._getAllWsObjects();
        var aryObjectsByType = [];
        switch (wsType) {
            case WEB_SERVICE_TYPE.wsdlSoap:
                aryObjectsByType = $.grep(aryAllWsObjects, function (value, index) {
                    return value.webserviceType.trim() === WEB_SERVICE_TYPE_TEXT.wsdlSoap;
                });
                break;
            case WEB_SERVICE_TYPE.xmlRpc:
                aryObjectsByType = $.grep(aryAllWsObjects, function (value, index) {
                    return value.webserviceType.trim() === WEB_SERVICE_TYPE_TEXT.xmlRpc;
                });
                break;
            case WEB_SERVICE_TYPE.http:
                aryObjectsByType = $.grep(aryAllWsObjects, function (value, index) {
                    return value.webserviceType.trim() === WEB_SERVICE_TYPE_TEXT.http;
                });
                break;
        }
        return aryObjectsByType;
    },
    _getAllWsObjects: function () {
        var aryWsObjects = [];
        var hidFieldValue = $(':hidden[id$="hidAllWsCommands"]').val();
        if (hidFieldValue) {
            aryWsObjects = $.parseJSON(hidFieldValue);
        }
        return aryWsObjects;
    },

    getInputParamsForCmdWs: function (wsType) {
        var aryInputParams = [];
        switch (wsType) {
            case WEB_SERVICE_TYPE.wsdlSoap:
                aryInputParams = this._getWsdlInputParamsForUI(cmdId, wsType);
                break;
            case WEB_SERVICE_TYPE.xmlRpc:
                aryInputParams = this._getXmlRpcInputParamsForUI(cmdId, wsType);
                break;
            case WEB_SERVICE_TYPE.http:
                aryInputParams = this._getHttpInputParamsForUI(cmdId, wsType);
                break;
        }
        return aryInputParams;
    },
    //---------------http command-------------
    _getHttpInputParamsForUI: function (cmdId, wsType) {
        var aryInputParams = [];
        var cmdObj = this._getCmdObjectById(cmdId, wsType);
        if (cmdObj && cmdObj.parameter) {
            aryInputParams = cmdObj.parameter.split(',');
        }
        return aryInputParams;
    },
    //---------------WSDL command-------------

    _getCmdObjectById: function (cmdId, wsType/*Constants.webserviceType*/) {
        var aryWsObjects = this._parseUICollectionHidField(wsType);
        var wsObject = null;
        wsObject = $.grep(aryWsObjects, function (value, index) {
            return value.commandId === cmdId;
        });
        return wsObject[0];
    },

    _getWsdlInputParamsForUI: function (cmdId, wsType/*WEB_SERVICE_TYPE*/) {
        var aryInputParams = [];
        var cmdObj = this._getCmdObjectById(cmdId, wsType);
        if (cmdObj && cmdObj.parameter) {
            var objFinalInputParam = $.parseJSON(cmdObj.parameter);
            var arySimpleTypesForUI = this._getWsdlSimpleTypeForUI(objFinalInputParam.S_Types, "");
            var aryComplexTypesForUI = this._getWsdlComplexTypeForUI(objFinalInputParam.C_Types, "");
            var aryFinalTypesForUI = [];
            for (var j = 0; j <= arySimpleTypesForUI.length - 1; j++) {
                aryFinalTypesForUI.push(arySimpleTypesForUI[j]);
            }
            for (var k = 0; k <= aryComplexTypesForUI.length - 1; k++) {
                aryFinalTypesForUI.push(aryComplexTypesForUI[k]);
            }
            return aryFinalTypesForUI;
        }
    },
    _getWsdlSimpleTypeForUI: function (arrayOfSimpleType, containerObjectName) {
        var aryFinalSimpleType = [];
        if (arrayOfSimpleType && $.isArray(arrayOfSimpleType) && arrayOfSimpleType.length > 0) {
            for (var i = 0; i <= arrayOfSimpleType.length - 1; i++) {
                var strSimpleTypeForUI = containerObjectName ? containerObjectName + "." + arrayOfSimpleType[i].S_Name :
                    arrayOfSimpleType[i].S_Name;
                aryFinalSimpleType.push(strSimpleTypeForUI);
            }
        }
        return aryFinalSimpleType;
    },
    _getWsdlComplexTypeForUI: function (complexTypes/*array*/, containerObjectName) {
        var aryFinalComplexType = [];
        if (complexTypes && $.isArray(complexTypes) && complexTypes.length > 0) {
            for (var i = 0; i <= complexTypes.length - 1; i++) {
                var objComplexType = complexTypes[i];
                var arySimpleTypesForUI = this._getWsdlSimpleTypeForUI(objComplexType.S_Types, objComplexType.C_Name);
                var aryInnerComplexType = this._getWsdlComplexTypeForUI(objComplexType.C_Types, objComplexType.C_Name);
                for (var j = 0; j <= arySimpleTypesForUI.length - 1; j++) {
                    var strSimpleTypeForUI = containerObjectName ? containerObjectName + "." + arySimpleTypesForUI[j] :
                        arySimpleTypesForUI[j];
                    aryFinalComplexType.push(strSimpleTypeForUI);
                }
                for (var k = 0; k <= aryInnerComplexType.length - 1; k++) {
                    var strComplexTypeForUI = containerObjectName ? containerObjectName + "." + aryInnerComplexType[k] :
                        aryInnerComplexType[k];
                    aryFinalComplexType.push(strComplexTypeForUI);
                }
            }
        }
        return aryFinalComplexType;
    },

    //---------------xmlrpc command-------------
    _getXmlRpcInputParamsForUI: function (cmdId, wsType/*Constants.webserviceType*/) {
        var aryInputParams = [];
        var cmdObj = this._getCmdObjectById(cmdId, wsType);
        if (cmdObj && cmdObj.parameter) {
            var objFinalInputParam = $.parseJSON(cmdObj.parameter);
            aryFinalTypesForUI = this.getAllPossibleXmlRpcParams(objFinalInputParam.param);
            return aryFinalTypesForUI;
        }
    },
    getAllPossibleXmlRpcParams: function (paramsArray) {
        var aryFinalCombinationsOfText = [];
        var aryOfFinalProperties = [];
        var aryParamsFromJson = paramsArray;
        var strVlue = "";
        var aryOfPossibleCombinationFromObject = [],
            i = 0, k = 0;
        if (aryParamsFromJson.length > 0) {
            for (i = 0; i <= aryParamsFromJson.length - 1; i++) {
                var strPropFinalText = "";
                aryOfPossibleCombinationFromObject = [];
                aryOfPossibleCombinationFromObject = this._getPossibleTextValsFromObject(aryParamsFromJson[i], strPropFinalText);
                if (aryOfPossibleCombinationFromObject) {
                    if (typeof aryOfPossibleCombinationFromObject === "string") {
                        aryOfFinalProperties.push(aryOfPossibleCombinationFromObject);
                    }
                    else if ($.isArray(aryOfPossibleCombinationFromObject)) {
                        for (k = 0; k <= aryOfPossibleCombinationFromObject.length - 1; k++) {
                            aryOfFinalProperties.push(aryOfPossibleCombinationFromObject[k]);
                        }
                    }
                }
            }
        }
        i = 0;
        for (i = 0; i <= aryOfFinalProperties.length - 1; i++) {
            var aryOfFinalCombination = aryOfFinalProperties[i];
            if (typeof aryOfFinalCombination === "string") {
                aryFinalCombinationsOfText.push(aryOfFinalCombination);
            }
            else if ($.isArray(aryOfFinalCombination)) {
                for (k = 0; k <= aryOfFinalCombination.length - 1; k++) {
                    aryFinalCombinationsOfText.push(aryOfFinalCombination[k]);
                }
            }
        }
        return aryFinalCombinationsOfText;
    },
    _getPossibleTextValsFromObject: function (propObject, previousStringEvluted /*string*/) {
        var aryFinalCombintionsOfText = [];
        var aryOfCombinationsInnerProp = [];
        if ($.isPlainObject(propObject)) {
            previousStringEvluted = this.addTextFromPropObject(propObject, previousStringEvluted);
            if (propObject.hasOwnProperty("inparam")) {
                aryOfCombinationsInnerProp = this._loopThroughInnerProperty(propObject.inparam, previousStringEvluted);
            }
        }
        if (aryOfCombinationsInnerProp.length > 0) {
            for (var i = 0; i <= aryOfCombinationsInnerProp.length - 1; i++) {
                aryFinalCombintionsOfText.push(aryOfCombinationsInnerProp[i]);
            }
            return aryFinalCombintionsOfText;
        }
        else {
            aryFinalCombintionsOfText.push(previousStringEvluted);
            return aryFinalCombintionsOfText;
        }

    },
    addTextFromPropObject: function (propObject, previousPropText) {
        if (previousPropText === "") return propObject.name;
        else return previousPropText + "." + propObject.name;
    },
    _loopThroughInnerProperty: function (innerPropArryObject, previousPropTextEvaluated) {
        var aryOfCombinationsInnerProp = [];

        if ($.isArray(innerPropArryObject) && innerPropArryObject.length > 0) {
            for (var i = 0; i <= innerPropArryObject.length - 1; i++) {
                var strStrtString = previousPropTextEvaluated;
                if ($.isPlainObject(innerPropArryObject[i])) {
                    var aryValsFromObject = this._getPossibleTextValsFromObject(innerPropArryObject[i], strStrtString);
                    if (aryValsFromObject && $.isArray(aryValsFromObject) && aryValsFromObject.length > 0) {
                        for (var k = 0; k <= aryValsFromObject.length - 1; k++) {
                            aryOfCombinationsInnerProp.push(aryValsFromObject[k]);
                        }
                    }
                    else {
                        if (typeof aryValsFromObject === "string") {
                            aryOfCombinationsInnerProp.push(aryValsFromObject);
                        }
                    }

                }

            }
        }
        return aryOfCombinationsInnerProp;
    },


    //------odata----------------
    _getAllCmdsOfSameConn: function () {
        var aryOdataObjects = [];
        var hidFieldValue = $(':hidden[id$="hidAllOdataCommands"]').val();
        if (hidFieldValue) {
            aryOdataObjects = $.parseJSON(hidFieldValue);
        }
        return aryOdataObjects;
    },
    _getCmdsAsOptionsOfDdlByConn: function () {

        var aryCmds = this._getAllCmdsOfSameConn(),
            strOptions = "", i = 0, objCmd = null;

        strOptions += '<option value=' + '-1' + '>Select</option>';
        if (aryCmds && $.isArray(aryCmds) && aryCmds.length > 0) {
            for (i = 0; i <= aryCmds.length - 1; i++) {
                objCmd = aryCmds[i];
                if (objCmd) {
                    strOptions +=
                        '<option value=' + objCmd.commandId + '>' +
                        objCmd.commandName + '</option>';
                }
            }
        }
        return strOptions;
    },

    _getCommandObject: function (cmdId) {
        var cmdObj = null;
        var aryCmdObjects = this._getAllCmdsOfSameConn();
        if (aryCmdObjects && aryCmdObjects.length > 0) {
            for (var i = 0; i <= aryCmdObjects.length - 1; i++) {
                cmdObj = aryCmdObjects[i];
                if (cmdObj.commandId === cmdId) {
                    break;
                }
                else {
                    cmdObj = null;
                }
            }
        }
        return cmdObj;
    },
    _getInputParamsForUI: function (cmdId) {
        var aryInputParams = [];
        var cmdObj = this._getCommandObject(cmdId);
        if (cmdObj && cmdObj.inputParamJson) {
            var objFinalInputParam = $.parseJSON(cmdObj.inputParamJson);
            if (objFinalInputParam) {
                $.each(objFinalInputParam, function (index, value) {
                    var objParamDtl = value;
                    if (objParamDtl) {
                        aryInputParams.push(objParamDtl.para);
                    }
                });
            }
        }
        return aryInputParams;
    }
    //------return-----------------------
};
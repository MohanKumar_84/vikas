﻿var UploadedImagesName = [];
var UploadedImages = [];
// Table Object DataSet
var selectResource;
var test;
var imageIndex;
function BindResoursedata() {
    var imgTag = '';
    $('#divresdataobject').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="tableresource"></table>');
    $('#tableresource').dataTable({
        "language": {
            "lengthMenu": " _MENU_"
        },
        "data": selectResource, "autoWidth": true,
        "pageLength": 12,
        "columns": [{ "Image": "Image", "title": "Image", "orderable": false }, { "title": "Name" },
         { "title": "path" }, { "title": "Size(KB)", "type": "num" }, { "title": "Date modified" },
         { "title": "", "targets": -1, "data": null, "orderable": false, "defaultContent": "<a><img  src=../images/cross.png></a>"}],
        "columnDefs": [{
            "targets": 2,
            "visible": false
        }],
        "order": [[1, "asc"]]
    });
    searchText("tableresource");
    makeSelectUniformOnPostBack();
    $('#tableresource_filter').css("padding-bottom", "10px");
    $('#tableresource_filter label input').css("width", "60px");
}
function searchText(tableName) {
    var tablename = "#" + tableName + "_filter label input"; $(tablename).attr("placeholder", "Search"); $(tablename).addClass("InputStyle")
    $(tablename).each(function () { $(this).click(function () { $(this).val(''); }); $(this).blur(function () { if ($(this).val == '') { $(this).val($(this).attr('placeholder')); } }); });
}

function RowClick() {
    var table = $('#tableresource').DataTable(); /// <reference path="../images/cross.png" />
    $('#tableresource tbody').on('click', 'a', function () {
        var data = table.row($(this).parents('tr')).data();
        var v = data[1];
        $('[id$=hdfDelId]').val(v);
        SubProcImageDelConfirnmation(true);
    });
}



function RefreshResourceFile() {
    selectResource = jQuery.parseJSON($('[id$=hidjson]').val());
    $('[id$=hidjson]').val('');
    
}
// function OnloadData
function ResourceOnload() {
    var test = $('[id$=hidjson]').val();
    if (test == '' || test == 'undefined') {
    }
    else {
        RefreshResourceFile();
    }
    BindResoursedata();
    RowClick();
    ClickAction();
}


function SubProcImageDelConfirnmation(_bol, _title) {
    SubProcCommonMethod('SubProcImageDelConfirnmation', _bol, "Delete ResourceFile", 300);
}


//Get all images from image repeater
function GetAllUploadedImage() {
    CompleteAllData();
}

//Forward and backword image button click event
function GetNextUploadedImageClick(_IsNext) {
    if (_IsNext) {
        imageIndex++;
        if (UploadedImages.length == imageIndex) imageIndex = 0;
    }
    else {
        if (imageIndex == 0) imageIndex = UploadedImages.length;
        imageIndex--;
    }
    GetUploadedImgSize(UploadedImages[imageIndex]);
    $('#aUpdImage').html(UploadedImagesName[imageIndex]);
}

//Get image size
function GetUploadedImgSize(_Src) {
    var imgwidth;
    var imgheight;
    var theImage = new Image();
    theImage.src = _Src;
    $(theImage).load(function () {
        ImageOnLoad(this.src, theImage.width, theImage.height);
    });
    $(theImage).error(function () {
        showMessage("Image not found.", DialogType.Error);
    });
}

//set image heigth and width
function ImageOnLoad( _Src, imgWidth, imgHeight) {
    $('#imgUplodedImage').css({ "background-image": 'url("' + _Src + '")' });
    $('#aUpdSize').html(imgWidth + ' X ' + imgHeight);
    SubProcShowUplodedImage(true);
}

//show image popup
function SubProcShowUplodedImage(_bol, _Title) {
    if (_bol) {
        showModalPopUp('SubProcShowUplodedImage', 'Uploaded Image', 920);
    }
    else {
        $('#SubProcShowUplodedImage').dialog('close');
    }
}


function DeleteSelectedRow(rowId) {
    for (var i = 0; i < selectResource.length; i++) {
        if (rowId == selectResource[i][1]) {
            selectResource.splice(i, 1);
        }
    }
}


// Datatable row click for bind open popup 
function ClickAction() {
    UploadedImagesName = [];
    UploadedImages = [];
    var table = $('#tableresource').DataTable();
    var rows = $("#tableresource").dataTable().fnGetNodes();
    for (var i = 0; i < rows.length; i++) {
        // Get HTML of 2rd column (for example)
        UploadedImagesName.push($(rows[i]).find("td:eq(1)").html());
    }
    for (var i = 0; i < rows.length; i++) {
        // Get HTML of 1rd column (for example)
        var path = $('[id$=hfilepath]').val();
        var fpath = $('[id$=hfilepathdetails]').val();
        var v = path + fpath + $(rows[i]).find("td:eq(1)").html();
        UploadedImages.push(v);

    }
    $('#tableresource tbody').on('click', 'td', function () {

        if ($(this).index() === 2)
            return;
        var data1 = table.row($(this).parents('tr')).data();
        if (data1 != undefined) {
            $('#aUpdImage').html(data1[1]);
            imageIndex = $(this).parents('tr').index();
            GetUploadedImgSize(data1[2]);
        }
        else {
          //  alert("No Image exist");
        }
    });
}
//copied the messages did not have the red cross button.Using other error messages
function uploadImagesError(_MsgNo) {
    var strErrorMsg = "";
    switch (_MsgNo) {
        case 0:
            strErrorMsg = 'Image size is greater than 1 MB.';
            break;
        case 6:
            strErrorMsg = 'Only jpg and png file format accepted.';
            break;
        case 2:
             strErrorMsg = 'Width of image should be less than 2048px.';
            break;
        case 4:
            strErrorMsg = 'Height of image should be less than 2048px.';
            break;
        case 5:
            strErrorMsg = 'There was an error while uploading the image. Please try again.';
            break;
        case 7:
            strErrorMsg = 'Please select a file to upload.';
            break;
    }
    if (strErrorMsg) {
        showMessage(strErrorMsg, DialogType.Error);
    }
}
if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
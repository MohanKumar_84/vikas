﻿
/*---------------------------------------------------Barcode---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Barcode = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            LabelText: function (control, appObj) {
                $(control).val(appObj.labelText);
                if (appObj.labelText != undefined && appObj.labelText != null && appObj.labelText.length > 0){
                    $('#' + appObj.id + '_Label').html(appObj.labelText);
                    $('#' + appObj.id + '_Label').css('margin-bottom', '');
                }
                else{
                    $('#' + appObj.id + '_Label').html("");
                    $('#' + appObj.id + '_Label').css('margin-bottom', '-7px');
                }
            },
            FontStyle: function (control, appObj) {
                if (appObj.fontStyle == undefined || appObj.fontStyle == null)
                    appObj.fontStyle = "0";
                $(control).val(appObj.fontStyle);
                var lblStyle = getTextFontStyle(appObj);
                $('#' + appObj.id + '_Label').removeClass();
                $('#' + appObj.id + '_Label').addClass(lblStyle);
            },  
            FontColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Barcode.propSheetCntrl.Appearance.getFontColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelValueProperty = $('#' + appObj.id + '_Label');
                if (appObj.fontColor != undefined || appObj.fontColor != null) {
                    $(currentCntrl.control).val(appObj.fontColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.fontColor);
                    $($LabelValueProperty[0]).css('color', '#' + appObj.fontColor + '!important');
                }

                var fontCol = '';
                var fontColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            fontCol = '#' + hex;
                        }
                        $(currentCntrl.control).val(fontCol);
                        $(currentCntrl.browseBtn).css('background-color', fontCol);
                        appObj.fontColor = $(currentCntrl.control).val();
                        if (fontCol.length > 0) {
                            $($LabelValueProperty[0]).css('color', fontCol + '!important');
                        }
                        else {
                            $($LabelValueProperty[0]).css('color', '#000000' + '!important');
                        }
                    }
                });
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            Placeholder: function (control, appObj) {
                $(control).val(appObj.placeholder);
                if (appObj.placeholder != undefined && appObj.placeholder != null && appObj.placeholder.length > 0)
                    $('#' + appObj.id + '_Text').val(appObj.placeholder);
            },
            NoofLines: function (control, appObj) {
                $(control).val(appObj.rowsCount);
                if (appObj.rowsCount != undefined && appObj.rowsCount != null && parseInt(appObj.rowsCount, 10) > 1) {
                    $('#' + appObj.id + '_TextDiv').css('width', '96%');
                    $('#' + appObj.id + '_ButtonDiv').css('margin-right', '-10px');
                    $('#' + appObj.id + '_ButtonDiv').css('float', 'right');
                }
                else {
                    $('#' + appObj.id + '_TextDiv').css('width', '');
                    $('#' + appObj.id + '_ButtonDiv').css('margin-right', '');
                    $('#' + appObj.id + '_ButtonDiv').css('float', '');
                }
            },
            eventHandlers: {
                LabelTextChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.labelText = evnt.target.value;
                    if (evnt.target.value.length > 0){
                        $('#' + propObject.id + '_Label').html(evnt.target.value);
                        $('#' + propObject.id + '_Label').css('margin-bottom', '');
                    }
                    else{
                        $('#' + propObject.id + '_Label').html("");
                        $('#' + propObject.id + '_Label').css('margin-bottom', '-7px');
                    }
                },
                FontStyleChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fontStyle = evnt.target.value;
                    var lblStyle = getTextFontStyle(propObject);
                    $('#' + propObject.id + '_Label').removeClass();
                    $('#' + propObject.id + '_Label').addClass(lblStyle);
                },
                PlaceholderValChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.placeholder = evnt.target.value;
                    $('#' + propObject.id + '_Text').val(evnt.target.value);
                },
                NoOfLinesChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.rowsCount = evnt.target.value;
                    if (propObject.rowsCount != undefined && propObject.rowsCount != null && parseInt(propObject.rowsCount, 10) > 1) {
                        $('#' + propObject.id + '_TextDiv').css('width', '96%');
                        $('#' + propObject.id + '_ButtonDiv').css('margin-right', '6px');
                        $('#' + propObject.id + '_ButtonDiv').css('float', 'right');
                    }
                    else {
                        $('#' + propObject.id + '_TextDiv').css('width', '');
                        $('#' + propObject.id + '_ButtonDiv').css('margin-right', '');
                        $('#' + propObject.id + '_ButtonDiv').css('float', '');
                    }
                }
            }
        },
        Data: {
            BindToProperty: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Barcode.propSheetCntrl.Data.getBindToPropertyCntrl();
                if ($('[id$=hdfIsChildForm]').val() == 'true') {
                    var eltCntrl = form.fnGetControlFromId($('[id$=hdfEditableListId]').val());
                    var strOptionsHtml = '<option value="-1" selected>Select Item</option>';
                    if ((eltCntrl != undefined || eltCntrl != null) && (eltCntrl.properties != undefined && eltCntrl.properties.length > 0)) {
                        $.each(eltCntrl.properties, function () {
                            strOptionsHtml += '<option value="' + this.name + '">' + this.name + '</option>';
                        });
                    }
                    $(control).html(strOptionsHtml);
                    if (appObj.bindToProperty != undefined) $(control).val(appObj.bindToProperty);
                    else $(control).val('-1');
                    $(currentCntrl.wrapperDiv).show();
                }
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            AllowManual: function (control, appObj) {
                $(control).val(appObj.manualEntry)
            },
            Type: function (control, appObj) {
                $(control).val(appObj.barcodeType)
            },
            eventHandlers: {
                BindToProperty: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Barcode.propSheetCntrl.Data.getBindToPropertyCntrl();
                    var objform = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (objform.fnAddBindProperty(evnt.target.value, propObject.bindToProperty, propObject))
                        propObject.bindToProperty = evnt.target.value;
                    else {
                        if (propObject.bindToProperty != undefined) evnt.target.value = propObject.bindToProperty;
                        else evnt.target.value = '-1';
                    }
                },
                AllowManual: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.manualEntry = evnt.target.value;
                },
                BarcodeType: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.barcodeType = evnt.target.value;
                }
            }
        },
        Validation: {
            Required: function (control, appObj) {
                $(control).val(appObj.required)
            },
            eventHandlers: {
                IsRequired: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.required = evnt.target.value;
                }
            }
        },
        Advance: {
            Scripts: function (control, appObj) {
            },
            eventHandlers: {
                Scripts: function (evnt) {
                    $('#drpCntrlEvents').html();
                    $('#drpCntrlEvents').css('width', '');
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    $('#spanOnChngUserDefinedName').text(propObject.userDefinedName);

                    var scriptOnBlur = '', scriptOnChange = '', scriptOnFocus = '', script = '';

                    _jsAceEditorOnChang.setTextInEditor("");
                    var options = '<option value="onBlur">onBlur</option>'
                             + '<option value="onChange">onChange</option>'
                                + '<option value="onFocus">onFocus</option>';

                    $('#drpCntrlEvents').html(options);
                    $('#drpCntrlEvents').val("onChange");
                    $.uniform.update('#drpCntrlEvents');
                    var prevSelectedVal = $('#drpCntrlEvents')[0].value;

                    if (propObject.onChange != null && propObject.onChange != undefined) scriptOnChange = propObject.onChange;
                    if (propObject.onBlur != null && propObject.onBlur != undefined) scriptOnBlur = propObject.onBlur;
                    if (propObject.onFocus != null && propObject.onFocus != undefined) scriptOnFocus = propObject.onFocus;
                    switch ($('#drpCntrlEvents')[0].value) {
                        case "onBlur":
                            if (scriptOnBlur != null && scriptOnBlur != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnBlur));
                            }
                            break;
                        case "onChange":
                            if (scriptOnChange != null && scriptOnChange != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));
                            }
                            break;
                        case "onFocus":
                            if (scriptOnFocus != null && scriptOnFocus != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnFocus));
                            }
                            break;
                    }
                    $('#drpCntrlEvents').unbind('change');
                    $('#drpCntrlEvents').bind('change', function () {
                        $.uniform.update('#drpCntrlEvents');
                        script = _jsAceEditorOnChang.getText();
                        switch (prevSelectedVal) {
                            case "onBlur":
                                if (script != null && script != undefined) {
                                    scriptOnBlur = Base64.encode(script);
                                }
                                break;
                            case "onChange":
                                if (script != null && script != undefined) {
                                    scriptOnChange = Base64.encode(script);
                                }
                                break;
                            case "onFocus":
                                if (script != null && script != undefined) {
                                    scriptOnFocus = Base64.encode(script);
                                }
                                break;
                        }
                        _jsAceEditorOnChang.setTextInEditor("");
                        prevSelectedVal = this.value;
                        switch (this.value) {
                            case "onBlur":
                                if (scriptOnBlur != null && scriptOnBlur != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnBlur));
                                }
                                break;
                            case "onChange":
                                if (scriptOnChange != null && scriptOnChange != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));
                                }
                                break;
                            case "onFocus":
                                if (scriptOnFocus != null && scriptOnFocus != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnFocus));
                                }
                                break;
                        }
                    });

                    SubProcShowCntrlJsEditor(true, "Scripts");
                    $('[id$=popup_custom_buttom_save]').unbind('click');
                    $('[id$=popup_custom_buttom_save]').bind('click', function () {
                        script = _jsAceEditorOnChang.getText();
                        switch ($('#drpCntrlEvents')[0].value) {
                            case "onBlur":
                                if (script != null && script != undefined) {
                                    scriptOnBlur = Base64.encode(script);
                                }
                                break;
                            case "onChange":
                                if (script != null && script != undefined) {
                                    scriptOnChange = Base64.encode(script);
                                }
                                break;
                            case "onFocus":
                                if (script != null && script != undefined) {
                                    scriptOnFocus = Base64.encode(script);
                                }
                                break;
                        }

                        if (scriptOnChange != null && scriptOnChange != undefined) propObject.onChange = scriptOnChange;
                        if (scriptOnBlur != null && scriptOnBlur != undefined) propObject.onBlur = scriptOnBlur;
                        if (scriptOnFocus != null && scriptOnFocus != undefined) propObject.onFocus = scriptOnFocus;
                        SubProcShowCntrlJsEditor(false);
                        return false;
                    });
                    $('[id$=popup_custom_buttom_close]').unbind('click');
                    $('[id$=popup_custom_buttom_close]').bind('click', function () {
                        scriptOnBlur = '', scriptOnChange = '', scriptOnFocus = ''
                        SubProcShowCntrlJsEditor(false);
                        return false;
                    });
                }
            }
        },
        Behaviour: {
            Update: function (control, appObj) {
                if ((appObj.refreshControls != undefined || appObj.refreshControls != null) && appObj.refreshControls.length > 0) $(control).val(appObj.refreshControls.length + " Control(s)");
                else $(control).val("0 Control(s)");
            },
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay)
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Barcode.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();

            },
            eventHandlers: {
                RefreshControls: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Barcode.propSheetCntrl.Behaviour.getRefreshControls();
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            if (control.id != propObject.id && !control.isDeleted  && isValidUpdateControl(control)) {
                                                controls.push(control.userDefinedName);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if (controls.length == 0) {
                        showMessage('Please add controls first.', DialogType.Error);
                        return;
                    }
                    else {
                        $('#OnChangeElmDiv').mficientRefreshCntrlsOnChng({ Controls: controls, SelectedControls: (propObject.refreshControls != undefined ? propObject.refreshControls : []) });
                        showModalPopUpWithOutHeader('SubProcOnChange', 'Controls', 280, false);
                        $('#OnChangeElmDiv').data("ControlId", propObject.id);
                    }

                    $('[id$=btnSaveRefreshControls]').unbind('click');
                    $('[id$=btnSaveRefreshControls]').bind('click', function () {
                        propObject.refreshControls = $('#OnChangeElmDiv').data("SelectedControls");
                        $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)");
                        $('#SubProcOnChange').dialog('close');
                        return false;
                    });

                    $('[id$=btnCancelRefreshControl]').unbind('click');
                    $('[id$=btnCancelRefreshControl]').bind('click', function () {
                        $(currentCntrl.control).val(propObject.refreshControls != undefined ? propObject.refreshControls.length + " Control(s)" : "0" + " Control(s)");
                        $('#SubProcOnChange').dialog('close');
                        return false;
                    });
                },
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }	
        function _getFontColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.BARCODE.propPluginPrefix,
                    "Appearance", "FontColor");
            }
            return objControl;
        }
        function _getRefreshControlsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.BARCODE.propPluginPrefix,
                    "Behaviour", "Update");
            }
            return objControl;
        }
        function _getBindToPropertyCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.BARCODE.propPluginPrefix,
                    "Data", "BindToProperty");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.BARCODE.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }
        return {
            Appearance: {
                getFontColorCntrl: _getFontColorCntrl
            },
            Data: {
                getBindToPropertyCntrl: _getBindToPropertyCntrl
            },
            Behaviour: {
                getRefreshControls: _getRefreshControlsCntrl,
                getConditionsCntrl: _getConditionsCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.Barcode = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.BARCODE,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Barcode",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Label Text",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "LabelText",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.Appearance.eventHandlers.LabelTextChange,
                      "context": "",
                      "arguments": {

                      }
                  },
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.Appearance.eventHandlers.LabelTextChange,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },	
            {
                "text": "Font Style",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FontStyle",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.Appearance.eventHandlers.FontStyleChange,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Normal",
                      "value": "0"
                  },
                  {
                      "text": "Bold",
                      "value": "1"
                  },
                  {
                      "text": "Italics",
                      "value": "2"
                  },
                  {
                      "text": "Bold And Italics",
                      "value": "3"
                  }
               ]
            },
            {
                  "text": "Font Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "FontColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
            {
                "text": "Placeholder",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Placeholder",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.Appearance.eventHandlers.PlaceholderValChanged,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.Appearance.eventHandlers.PlaceholderValChanged,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "No. of Lines",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "NoofLines",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.Appearance.eventHandlers.NoOfLinesChanged,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.Appearance.eventHandlers.NoOfLinesChanged,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
              {
                  "text": "Bind To Property",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "BindToProperty",
                  "defltValue": "0",
                  "validations": [

                       ],
                  "customValidations": [

                       ],
                  "events": [
                          {
                              "name": "change",
                              "func": PROP_JSON_HTML_MAP.Barcode.groups.Data.eventHandlers.BindToProperty,
                              "context": "",
                              "arguments": {
                                  "cntrls": [
                                ]
                              }
                          }
                       ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": true
                  },
                  "options": [
                       ]
              },
            {
                "text": "Manual Entry",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "AllowManual",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.Data.eventHandlers.AllowManual,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
            },
            {
                "text": "Barcode Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Type",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.Data.eventHandlers.BarcodeType,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Auto Detect",
                      "value": "0"
                  },
                  {
                      "text": "UPC-A and UPC-E(1d barcode",
                      "value": "1"
                  },
                  {
                      "text": "EAN-8 and EAN-13(1d barcode)",
                      "value": "2"
                  },
                  {
                      "text": "Code 39 (1d barcode)",
                      "value": "3"
                  },
                  {
                      "text": ">Code 93(1d barcode)",
                      "value": "4"
                  },
                  {
                      "text": "Code 128(1d barcode)",
                      "value": "5"
                  },
                  {
                      "text": "ITF (1d barcode)",
                      "value": "6"
                  },
                  {
                      "text": "Codabar(1d barcode)",
                      "value": "7"
                  },
                  {
                      "text": "RSS-14 (all variants)",
                      "value": "8"
                  },
                  {
                      "text": "QR Code (2d barcode)",
                      "value": "9"
                  },
                  {
                      "text": "Data Matrix (2d barcode)",
                      "value": "10"
                  },
                  {
                      "text": "Aztec (beta quality)",
                      "value": "11"
                  },
                  {
                      "text": "PDF 417 (alpha quality)",
                      "value": "12"
                  }
               ]
            }
         ]
      },
      {
          "name": "Validation",
          "prefixText": "Validation",
          "hidden": false,
          "properties": [
            {
                "text": "Required",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Required",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.Validation.eventHandlers.IsRequired,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
            }
          ]
      },
      {
          "name": "Advance",
          "prefixText": "Advance",
          "hidden": false,
          "properties": [
          {
                "text": "Scripts",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Scripts",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.Advance.eventHandlers.Scripts,
                      "context": "",
                      "arguments": {}
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
          ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Refresh On Change",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Update",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.Behaviour.eventHandlers.RefreshControls,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "None",
                    "hidden": false
                }
            },
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Barcode.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};


function Barcode(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.BarcodeReader;
    this.description = opts.description;
    this.type = opts.type;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.labelText = opts.labelText;
    this.placeholder = opts.placeholder;
    this.rowsCount = opts.rowsCount;
    this.required = opts.required;
    this.manualEntry = opts.manualEntry;
    this.barcodeType = opts.barcodeType;
    this.refreshControls = opts.refreshControls;
    this.bindToProperty = opts.bindToProperty;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.onChange = opts.onChange;
    this.onBlur = opts.onBlur;
    this.onFocus = opts.onFocus;		
    this.fontStyle = opts.fontStyle;		
    this.fontColor = opts.fontColor;
}

Barcode.prototype = new ControlNew();
Barcode.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Barcode;
Barcode.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.labelText = "Label Text";
    this.manualEntry = "0";
    this.barcodeType = "0";
    this.required = "0";
    this.conditionalDisplay = "1";
    this.fontStyle = "0";
    this.isDeleted = false; 
};
Barcode.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};

/*---------------------------------------------------Location---------------------------------------------------*/

mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Location = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
                //$(control).val(appObj.description)
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            LabelText: function (control, appObj) {
                $(control).val(appObj.labelText);
                if (appObj.labelText != undefined && appObj.labelText != null && appObj.labelText.length > 0) {
                    $('#' + appObj.id + '_Label').html(appObj.labelText);
                    $('#' + 'LocationOuterDiv_' + appObj.id.split('_')[1]).css('margin-top', '');
                }
                else {
                    $('#' + appObj.id + '_Label').html("");
                    $('#' + 'LocationOuterDiv_' + appObj.id.split('_')[1]).css('margin-top', '-10px');
                }
            },
            FontStyle: function (control, appObj) {
                if (appObj.fontStyle == undefined || appObj.fontStyle == null)
                    appObj.fontStyle = "0";
                $(control).val(appObj.fontStyle);
                var lblStyle = getTextFontStyle(appObj);
                $('#' + appObj.id + '_Label').removeClass();
                $('#' + appObj.id + '_Label').addClass(lblStyle);
            },
            FontColor: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Location.propSheetCntrl.Appearance.getFontColorCntrl();
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelValueProperty = $('#' + appObj.id + '_Label');
                if (appObj.fontColor != undefined || appObj.fontColor != null) {
                    $(currentCntrl.control).val(appObj.fontColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.fontColor);
                    $($LabelValueProperty[0]).css('color', '#' + appObj.fontColor + '!important');
                }

                var fontCol = '';
                var fontColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            fontCol = '#' + hex;
                        }
                        $(currentCntrl.control).val(fontCol);
                        $(currentCntrl.browseBtn).css('background-color', fontCol);
                        appObj.fontColor = $(currentCntrl.control).val();
                        if (fontCol.length > 0) {
                            $($LabelValueProperty[0]).css('color', fontCol + '!important');
                        }
                        else {
                            $($LabelValueProperty[0]).css('color', '#000000' + '!important');
                        }
                    }
                });
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            Height: function (control, appObj) {
                if (appObj.height == undefined || appObj.height == null)
                    appObj.height = 100;
                $(control).val(appObj.height);
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyIntegers(event);
                });
                $('#LocationOuterDiv_' + appObj.id.split('_')[1]).css('height', appObj.height + 'px');
            },
            Hidden: function (control, appObj) {
                $(control).val(appObj.isHidden);
            },
            eventHandlers: {
                LabelTextChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.labelText = evnt.target.value;
                    if (evnt.target.value.length > 0) {
                        $('#' + propObject.id + '_Label').html(evnt.target.value);
                        $('#' + 'LocationOuterDiv_' + propObject.id.split('_')[1]).css('margin-top', '');
                    }
                    else {
                        $('#' + propObject.id + '_Label').html("");
                        $('#' + 'LocationOuterDiv_' + propObject.id.split('_')[1]).css('margin-top', '-10px');
                    }
                },
                FontStyleChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fontStyle = evnt.target.value;
                    var lblStyle = getTextFontStyle(propObject);
                    $('#' + propObject.id + '_Label').removeClass();
                    $('#' + propObject.id + '_Label').addClass(lblStyle);
                },
                HeightChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    if (parseFloat(evnt.target.value) < 100) {
                        showMessage('Height cannot be less than 100.', DialogType.Error);
                        evnt.target.value = propObject.height;
                        return;
                    }
                    else if (parseFloat(evnt.target.value) > 1000) {
                        showMessage('Height cannot be greater than 1000.', DialogType.Error);
                        evnt.target.value = propObject.height;
                        return;
                    }
                    propObject.height = evnt.target.value;
                    $('#LocationOuterDiv_' + propObject.id.split('_')[1]).css('height', propObject.height + 'px');
                },
                Hidden: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.isHidden = evnt.target.value;
                }
            }
        },
        Data: {
            FetchLocation: function (control, appObj) {
                $(control).val(appObj.fetchLocation)
            },
            ShowRefreshLink: function (control, appObj) {
                $(control).val(appObj.refreshLink)
            },
            Databinding: function (control, controlObj) {
                //$(control).val(appObj.type.UIName)
                var objApp = MF_HELPERS.currentObjectHelper.getCurrentWorkingApp();
                var strOptions = "";
                if (objApp.fnGetType() === MF_IDE_CONSTANTS.MF_STANDARD_APP_TYPE.offline) {
                    strOptions += ' <option value="0">None</option>';
                    strOptions += '<option value="6">Offline Database</option>';
                    control.html(strOptions);
                }
                else {
                }
                var objDatabindingObj = controlObj.fnGetDatabindingObj();
                if (objDatabindingObj) {
                    $(control).val(objDatabindingObj.fnGetBindTypeValue());
                }
            },
            DataObject: function (control, controlObj) {
                // $(control).val(appObj.userDefinedName);
                var objDatabindingObj = controlObj.fnGetDatabindingObj();
                if (objDatabindingObj) {
                    var objbindType = objDatabindingObj.fnGetBindTypeObject();
                    if (objbindType && objbindType !== MF_IDE_CONSTANTS.ideCommandObjectTypes.None) {
                        var objPropSheetCntrl = PROP_JSON_HTML_MAP.Location.propSheetCntrl.Data.getDataObject();
                        if (objPropSheetCntrl) {
                            objPropSheetCntrl.control.val(objDatabindingObj.fnGetCommandName());
                            objPropSheetCntrl.wrapperDiv.show();
                        }
                    }
                    else {
                        $(control).val("Select object");
                        objPropSheetCntrl.wrapperDiv.hide();
                    }
                }
            },
            eventHandlers: {
                FetchLocation: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fetchLocation = evnt.target.value;
                },
                ShowRefreshLink: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.refreshLink = evnt.target.value;
                },
                Databinding: function (evnt) {
                    var evntData = evnt.data;
                    //var confirm = confirm('Are you sure you want to change Databindin')
                    var $dataObjectWrapper = $('#' + evntData.cntrls.DataObject.wrapperDiv);
                    var $dataObjectCntrl = $('#' + evntData.cntrls.DataObject.controlId);
                    var bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                    var propObject = evntData.propObject;
                    var $self = $(this);
                    if ($self.val() === "0") {
                        $dataObjectWrapper.hide();
                        bindType = mFicientIde.MF_IDE_CONSTANTS.ideCommandObjectTypes.None;
                        propObject.fnRemoveDatabindObjs();
                    }
                    else {
                        $dataObjectWrapper.show();
                        propObject.fnRemoveDatabindObjs();
                        var dataObject = new DatabindingObj("", [], "0", $self.val(), "", "", "", "", "", { ignoreCache: false });
                        propObject.fnAddDatabindObj(dataObject);
                    }
                    $dataObjectCntrl.val("Select Object");
                },
                DataObject: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var objCurrentForm = MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    var intlJson = [],
                        isEdit = false;
                    try {
                        if (propObject && propObject.databindObjs
                        && $.isArray(propObject.databindObjs)) {
                            $.each(propObject.databindObjs, function (index, value) {
                                var objDatabindObj = jQuery.extend(true, {}, value);
                                switch (value.bindObjType) {
                                    case "1":
                                        //bindType = MF_HELPERS.getCommandTypeByValue(value.bindObjType);
                                        mFicientIde.MF_DATA_BINDING.databaseObjectBinding(propObject, objDatabindObj);
                                        showModalPopUpWithOutHeader('SubProcCtrlDbCmd', 'Database Object', 500);
                                        break;
                                    case "2":
                                        mFicientIde.MF_DATA_BINDING.webserviceObjectBinding(propObject, objDatabindObj);
                                        showModalPopUpWithOutHeader('SubProcCtrlWsCmd', 'Webservice Object', 500);
                                        break;
                                    case "5":
                                        mFicientIde.MF_DATA_BINDING.oDataObjectBinding(propObject, objDatabindObj);
                                        showModalPopUpWithOutHeader('SubProcCtrlODataCmd', 'OData Object', 500);
                                        break;
                                    case MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj.id:

                                        intlJson = MF_HELPERS
                                                    .intlsenseHelper
                                                    .getControlDatabindingIntellisenseJson(
                                                        objCurrentForm, propObject.id, true
                                                    );
                                        if (objDatabindObj != null
                                            && (objDatabindObj.id && objDatabindObj.id !== "-1")
                                            ) {
                                            isEdit = true;
                                        }

                                        mFicientIde
                                        .MF_DATA_BINDING
                                        .processDataBindingByObjectType({
                                            controlType: MF_IDE_CONSTANTS.CONTROLS.LOCATION,
                                            databindObject: objDatabindObj,
                                            objectType: MF_IDE_CONSTANTS.ideCommandObjectTypes.oflnDataObj,
                                            isEdit: isEdit,
                                            intlsJson: intlJson,
                                            control: propObject
                                        });
                                        showModalPopUpWithOutHeader('divCtrlsDataBind', 'Offline Datatable Object', 550, false);
                                        break;
                                }
                            });
                        }
                    }
                    catch (error) {
                        if (error instanceof MfError) {
                            showMessage(MF_HELPERS.mfErrorHelper.getHtmlListOfErrors(error.msgs), DialogType.Error);
                        } else {
                            console.log(error);
                        }
                    }
                }
            }
        },
        Validation: {
            Type: function (control, cntrlObj, args) {
                var strValidationType = cntrlObj.fnGetValidationType();

                if (!mfUtil.isNullOrUndefined(strValidationType) &&
                    !mfUtil.isEmptyString(strValidationType)) {

                    $(control).val(cntrlObj.validationType);
                }
                else {
                    // no need for setting any value
                }
            },
            ValidationValue: function (control, cntrlObj, args) {
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });

                var strValidationType = cntrlObj.fnGetValidationType();

                var blnShowContainer = false;

                if (!mfUtil.isNullOrUndefined(strValidationType) &&
                    !mfUtil.isEmptyString(strValidationType)) {

                    if (strValidationType === "-1") {
                        blnShowContainer = false;
                        $(control).val('');
                    }
                    else {
                        blnShowContainer = true;
                        $(control).val(cntrlObj.fnGetValidationVal());
                    }
                }
                else {
                    blnShowContainer = false;
                    $(control).val('');
                }
                if (blnShowContainer) {
                    args.cntrlHtmlObjects.wrapperDiv.show();
                }
                else {
                    args.cntrlHtmlObjects.wrapperDiv.hide();
                }
            },
            ErrorMsg: function (control, cntrlObj, args) {
                var strValidationType = cntrlObj.fnGetValidationType();

                var blnShowContainer = false;

                if (!mfUtil.isNullOrUndefined(strValidationType) &&
                    !mfUtil.isEmptyString(strValidationType)) {

                    if (strValidationType === "-1") {
                        blnShowContainer = false;
                        $(control).val('');
                    }
                    else {
                        blnShowContainer = true;
                        $(control).val(cntrlObj.fnGetValidationErrorMsg());
                    }
                }
                else {
                    blnShowContainer = false;
                    $(control).val('');
                }
                if (blnShowContainer) {
                    args.cntrlHtmlObjects.wrapperDiv.show();
                }
                else {
                    args.cntrlHtmlObjects.wrapperDiv.hide();
                }
            },
            eventHandlers: {
                Type: function (evnt) {
                    var $self = $(this);
                    var evntData = evnt.data;
                    //var cntrls = evntData.cntrls;
                    var propObject = evntData.propObject;
                    var validationValCtnrls = PROP_JSON_HTML_MAP
                                              .Location.propSheetCntrl
                                              .Validation.getValidationValueCntrl();

                    var validationErrMsgCtnrls = PROP_JSON_HTML_MAP
                                              .Location.propSheetCntrl
                                              .Validation.getValidationErrorMsgCntrl();


                    if ($self.val() === "-1") {
                        validationValCtnrls.control.val('');
                        validationErrMsgCtnrls.control.val('');
                        validationValCtnrls.wrapperDiv.hide();
                        validationErrMsgCtnrls.wrapperDiv.hide();
                        propObject.fnDeleteValidationErrorMsg();
                        propObject.fnDeleteValidationVal();
                    }
                    else {
                        validationValCtnrls.wrapperDiv.show();
                        validationErrMsgCtnrls.wrapperDiv.show();
                    }
                    propObject.fnSetValidationType($self.val());
                },
                ValidationValue: function (evnt) {
                    var $self = $(this);
                    evnt.data.propObject.fnSetValidationVal($self.val());
                },
                ErrorMsg: function (evnt) {
                    var $self = $(this);
                    evnt.data.propObject.fnSetValidationErrorMsg($self.val());
                }
            }
        },
        Behaviour: {
            AllowUsages: function (control, appObj) {
                $(control).val(appObj.allowUsages)
            },
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay)
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Location.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();
            },
            eventHandlers: {
                AllowUsages: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.allowUsages = evnt.target.value;
                },
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm(); //.getFormFromApp($('[id$=hdfCurrentFormId]').val());
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getFontColorCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LOCATION.propPluginPrefix,
                    "Appearance", "FontColor");
            }
            return objControl;
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LOCATION.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }
        function _getDataObjectCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LOCATION.propPluginPrefix,
                    "Data", "DataObject");
            }
            return objControl;
        }
        function _getValidationErrorMsgCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LOCATION.propPluginPrefix,
                    "Validation", "ErrorMsg");
            }
            return objControl;
        }
        function _getValidationValueCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.LOCATION.propPluginPrefix,
                    "Validation", "ValidationValue");
            }
            return objControl;
        }
        return {
            Appearance: {
                getFontColorCntrl: _getFontColorCntrl
            },
            Behaviour: {
                getConditionsCntrl: _getConditionsCntrl
            },
            Data: {
                getDataObject: _getDataObjectCntrl
            },
            Validation: {
                getValidationValueCntrl: _getValidationValueCntrl,
                getValidationErrorMsgCntrl: _getValidationErrorMsgCntrl

            }
        };
    })()
};

mFicientIde.PropertySheetJson.Location = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.LOCATION,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Location",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Location.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Location.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Label Text",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "LabelText",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Location.groups.Appearance.eventHandlers.LabelTextChange,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },	
            {
                "text": "Font Style",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FontStyle",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Location.groups.Appearance.eventHandlers.FontStyleChange,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Normal",
                      "value": "0"
                  },
                  {
                      "text": "Bold",
                      "value": "1"
                  },
                  {
                      "text": "Italics",
                      "value": "2"
                  },
                  {
                      "text": "Bold And Italics",
                      "value": "3"
                  }
               ]
            },
            {
                  "text": "Font Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "FontColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
                {
                "text": "Height",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Height",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Location.groups.Appearance.eventHandlers.HeightChange,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },
              {
                "text": "Hidden",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Hidden",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Location.groups.Appearance.eventHandlers.Hidden,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
            }
         ]
      },
      {
          "name": "Data",
          "prefixText": "Data",
          "hidden": false,
          "properties": [
            {
                "text": "Relocation",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FetchLocation",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Location.groups.Data.eventHandlers.FetchLocation,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": true
                },
                "options": [
                  {
                      "text": "On Form Close",
                      "value": "0"
                  },
                  {
                      "text": "On App Close",
                      "value": "1"
                  }
               ]
            },
            {
                "text": "Show Refresh Link",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "ShowRefreshLink",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Location.groups.Data.eventHandlers.ShowRefreshLink,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
            },
            {
                  "text": "Databinding",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                  "prefixText": "Databinding",
                  "defltValue": "0",
                  "validations": [

               ],
                  "customValidations": [

               ],
                  "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Location.groups.Data.eventHandlers.Databinding,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "DataObject",
                               rtrnPropNm: "DataObject"
                           }
                        ]
                      }
                  }
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "selectValueBy": "",
                  "init": {
                      "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                      "disabled": false,
                      "hidden": false
                  },
                  "options": [
                  {
                      "text": "None",
                      "value": "0"
                  },
                  {
                      "text": "Database Object",
                      "value": "1"
                  },
                  {
                      "text": "Web Service Object",
                      "value": "2"
                  },
                  {
                      "text": "OData Object",
                      "value": "5"
                  },
                   {
                       "text": "Offline Database",
                       "value": "6"
                   }
               ]
              },
            {
                "text": "Data Object",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "DataObject",
                "disabled": false,
                "hidden": true,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Location.groups.Data.eventHandlers.DataObject,
                      "context": "",
                      "arguments": {}
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Object",
                    "hidden": true
                }
            }
         ]
      },
      {
      "name": "Validation",
      "prefixText": "Validation",
      "hidden": false,
      "properties": [
        {
            "text": "Type",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
            "prefixText": "Type",
            "defltValue": "0",
            "validations": [
            ],
            "customValidations": [
            ],
            "events": [
              {
                  "name": "change",
                  "func": PROP_JSON_HTML_MAP.Location.groups.Validation.eventHandlers.Type,
                  "context": "",
                  "arguments": {
                      "cntrls": [
                      ]
                  }
              }
            ],
            "CntrlProp": "",
            "HelpText": "",
            "selectValueBy": "",
            "init": {
                "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                "disabled": false,
                "hidden": false
            },
            "options": [
              {
                  "text": "None",
                  "value": "-1"
              },
              {
                  "text": "Distance &le;",
                  "value": "7"
              },
              {
                  "text": "Distance &ge;",
                  "value": "8"
              }
           ]
        },
        {
            "text": "Value",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
            "prefixText": "ValidationValue",
            "defltValue": "0",
            "validations": [
            ],
            "customValidations": [
            ],
            "events": [
              {
                  "name": "blur",
                  "func": PROP_JSON_HTML_MAP.Location.groups.Validation.eventHandlers.ValidationValue,
                  "context": "",
                  "arguments": {
                  }
              }
            ],
            "CntrlProp": "",
            "HelpText": "",
            "selectValueBy": "",
            "init": {
                "maxlength": 150,
                "disabled": false,
                "defltValue": "",
                "hidden": true
            },
            "options": [
            ]
        },
        {
            "text": "Validation Error Msg.",
            "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
            "prefixText": "ErrorMsg",
            "defltValue": "0",
            "validations": [
            ],
            "customValidations": [
            ],
            "events": [
              {
                  "name": "blur",
                  "func": PROP_JSON_HTML_MAP.Location.groups.Validation.eventHandlers.ErrorMsg,
                  "context": "",
                  "arguments": {
                  }
              }

            ],
            "CntrlProp": "",
            "HelpText": "",
            "selectValueBy": "",
            "init": {
                "maxlength": 100,
                "disabled": false,
                "defltValue": "",
                "hidden": true
            },
            "options": [
            ]
        }
       ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Allow Usage",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "AllowUsages",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Location.groups.Behaviour.eventHandlers.AllowUsages,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "If GPS Available",
                      "value": "0"
                  },
                  {
                      "text": "Ignore GPS Availability",
                      "value": "1"
                  }
               ]
            },
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Location.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Location.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
    ]
};


function Location(opts) {
    if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Location;
    this.description = opts.description;
    this.type = opts.type;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.labelText = opts.labelText;
    this.fetchLocation = opts.fetchLocation;
    this.refreshLink = opts.refreshLink;
    this.allowUsages = opts.allowUsages;
    this.isHidden = opts.isHidden;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.databindObjs = opts.databindObjs;
    this.latitude = opts.latitude;
    this.longitude = opts.longitude;
    
    this.validationType = opts.validationType;
    this.errorMsg = opts.errorMsg;
    this.validationVal = opts.validationVal;		
    this.fontStyle = opts.fontStyle;
    this.fontColor = opts.fontColor;
    this.height = opts.height;
}

Location.prototype = new ControlNew();
Location.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Location;
Location.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.labelText = "Label Text";
    this.fetchLocation = "0";
    this.refreshLink = "0";
    this.allowUsages = "0";
    this.isHidden = "0";
    this.conditionalDisplay = "1";
    this.fontStyle = "0";
    this.isDeleted = false;
    this.height = 100;
    this.fnSetValidationType("-1");
};
Location.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
Location.prototype.fnGetDatabindingObj = function () {
    return this.databindObjs && this.databindObjs[0]; //only one element in array
};
Location.prototype.fnRemoveDatabindObjs = function () {
    this.databindObjs = [];
};
Location.prototype.fnAddDatabindObj = function (databindObj) {
    if (!this.databindObjs || !$.isArray(this.databindObjs)) {
        this.databindObjs = [];
    }
    if (databindObj && databindObj instanceof DatabindingObj) {
        this.databindObjs.push(databindObj);
    }
};
Location.prototype.fnGetLatitude = function () {
   return this.latitude;
};
Location.prototype.fnGetLongitude = function () {
   return this.longitude;
};
Location.prototype.fnSetLatitude = function (value) {
    this.latitude = value;
};
Location.prototype.fnSetLongitude = function (value) {
    this.longitude = value;
};
Location.prototype.fnResetObjectDetails = function () {
    var databindObjs = this.databindObjs;
    this.databindObjs = [];
    var i = 0;
    if (databindObjs && $.isArray(databindObjs) && databindObjs.length > 0) {
        for (i = 0; i <= databindObjs.length - 1; i++) {
            var objdatabindObj = new DatabindingObj(
                     databindObjs[i].id,
                     databindObjs[i].cmdParams,
                     databindObjs[i].index,
                     databindObjs[i].bindObjType,
                     databindObjs[i].name,
                     databindObjs[i].dsName,
                     databindObjs[i].tempUiIndex,
                     databindObjs[i].usrName,
                     databindObjs[i].pwd,
                     databindObjs[i]);
            objdatabindObj.fnResetObjectDetails();
            this.fnAddDatabindObj(objdatabindObj);
        }
    }
};

Location.prototype.fnSetValidationType = function (value) {
    this.validationType = value;
};
Location.prototype.fnGetValidationType = function () {
    return this.validationType;
};
Location.prototype.fnSetValidationErrorMsg = function (value) {
    this.errorMsg = value;
};
Location.prototype.fnGetValidationErrorMsg = function () {
    return this.errorMsg;
};
Location.prototype.fnDeleteValidationErrorMsg = function () {
    this.fnSetValidationErrorMsg("");
};
Location.prototype.fnSetValidationVal = function (value) {
    this.validationVal = value;
};
Location.prototype.fnGetValidationVal = function () {
    return this.validationVal;
};
Location.prototype.fnDeleteValidationVal = function () {
    this.fnSetValidationVal("");
};
/*------------------------------------------------------------Signature--------------------------------------------------------------------*/
var maxThumnbnailWith = 99;
var minThumnbnailWith = 25;
var maxHeightnWidth = 300;
var minHeightnWidth = 100;


mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Signature = {
    groups: {
        //Control Prefix Text
        ControlProperties: {
            Type: function (control, appObj) {
                $(control).val(appObj.type.UIName)
            },
            Name: function (control, appObj) {
                $(control).val(appObj.userDefinedName)
            },
            Description: function (control, appObj, options/*object of cntrol realted html objects from prop sheet*/) {
                var $textAreaLabel = options.cntrlHtmlObjects.textAreaLabel;
                var strDescription = appObj.description;
                if (strDescription) {
                    $(control).val(strDescription);
                    if (strDescription.length > 25) {
                        $textAreaLabel.val(strDescription.substring(0, 25) + '...');
                    }
                    else {
                        $textAreaLabel.val(strDescription);
                    }
                }
                else {
                    $(control).val('');
                    $textAreaLabel.val('');
                }
            },
            eventHandlers: {
                NameChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    var objForm = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    if (IsUserAllotedNameExist(objForm, propObject.id, evnt.target.value) != false) {
                        if (propObject.userDefinedName != undefined || propObject.userDefinedName != null) evnt.target.value = propObject.userDefinedName;
                        else evnt.target.value = "";
                        return;
                    }
                    propObject.fnRename(evnt.target.value);
                },
                DescChanged: function (evnt) {
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    propObject.description = evnt.target.value;
                }
            }
        },
        Appearance: {
            LabelText: function (control, appObj) {
                $(control).val(appObj.labelText);
                if (appObj.labelText != undefined && appObj.labelText != null && appObj.labelText.length > 0) {
                    $('#' + appObj.id + '_Label').html(appObj.labelText);
                    $('#' + appObj.id + '_LabelOuterDiv').show();
                }
                else {
                    $('#' + appObj.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                    $('#' + appObj.id + '_LabelOuterDiv').hide();
                }
            },
            FontStyle: function (control, appObj) {
                if (appObj.fontStyle == undefined || appObj.fontStyle == null)
                    appObj.fontStyle = "0";
                $(control).val(appObj.fontStyle);
                var lblStyle = getTextFontStyle(appObj);
                $('#' + appObj.id + '_Label').removeClass();
                $('#' + appObj.id + '_Label').addClass(lblStyle);
            },
            FontColor: function (control, appObj, options) {
                var currentCntrl = options.cntrlHtmlObjects;
                $(currentCntrl.control).removeAttr('disabled');
                $(currentCntrl.control).removeAttr('readonly');
                $(currentCntrl.control).removeClass('propDisable');
                $(currentCntrl.browseBtn).val(' ');
                var $LabelValueProperty = $('#' + appObj.id + '_Label');
                if (appObj.fontColor != undefined || appObj.fontColor != null) {
                    $(currentCntrl.control).val(appObj.fontColor);
                    $(currentCntrl.browseBtn).css('background-color', appObj.fontColor);
                    $($LabelValueProperty[0]).css('color', '#' + appObj.fontColor + '!important');
                }

                var fontCol = '';
                var fontColorPicker = $(currentCntrl.browseBtn).colpick({
                    layout: 'hex',
                    submit: 0,
                    colorScheme: 'dark',
                    onChange: function (hsb, hex, rgb, el, bySetColor) {
                        if (!bySetColor) {
                            $(currentCntrl.control).val('#' + hex);
                            fontCol = '#' + hex;
                        }
                        $(currentCntrl.control).val(fontCol);
                        $(currentCntrl.browseBtn).css('background-color', fontCol);
                        appObj.fontColor = $(currentCntrl.control).val();
                        if (fontCol.length > 0) {
                            $($LabelValueProperty[0]).css('color', fontCol + '!important');
                        }
                        else {
                            $($LabelValueProperty[0]).css('color', '#000000' + '!important');
                        }
                    }
                });
                $(currentCntrl.control).keyup(function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("change", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
                $(currentCntrl.control).bind("blur", function (evnt) {
                    appObj.fontColor = evnt.target.value;
                    fontCol = evnt.target.value;
                    $(fontColorPicker).colpickSetColor(evnt.target.value);
                });
            },
            ThumbnailWidth: function (control, appObj) {
                if (parseFloat(appObj.width).toFixed(2) > 100) appObj.width = 100;
                $(control).val(appObj.width);
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
                appObj.fnSetThumbnail(appObj.width);
            },
            BackgroundColor: function (control, appObj) {
                $(control).val(appObj.backgroundColor);
            },
            PenColor: function (control, appObj) {
                $(control).val(appObj.penColor);
            },
            eventHandlers: {
                LabelTextChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var $LabelProperty = $('#' + propObject.id);
                    if (evnt.target.value.length > 0) {
                        $('#' + propObject.id + '_Label').html(evnt.target.value);
                        $('#' + propObject.id + '_LabelOuterDiv').show();
                    }
                    else {
                        $('#' + propObject.id + '_Label').html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                        $('#' + propObject.id + '_LabelOuterDiv').hide();
                    }
                    propObject.labelText = evnt.target.value;
                },
                FontStyleChange: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.fontStyle = evnt.target.value;
                    var lblStyle = getTextFontStyle(propObject);
                    $('#' + propObject.id + '_Label').removeClass();
                    $('#' + propObject.id + '_Label').addClass(lblStyle);
                },
                WidthChanged: function (evnt) {
                    var $self = $(this);
                    var propObject = evnt.data.propObject;

                    if (evnt.target.value.trim().length <= 0 || parseInt(evnt.target.value, 10) < 20) {
                        showMessage("Thumbnail width should be greater than 20%.", DialogType.Error);
                        $self.val(propObject.width);
                        return;
                    }

                    if (parseFloat(evnt.target.value) > 100) {
                        showMessage("Thumbnail width/height should be less than 100%.", DialogType.Error);
                        $self.val(propObject.width);
                        return;
                    }

//                    if (parseFloat(evnt.target.value) > parseFloat(propObject.maxWidth)) {
//                        showMessage("Thumbnail width/height should not be greater than Maximum width.", DialogType.Error);
//                        $self.val(propObject.width);
//                        return;
//                    }

//                    if (parseFloat(evnt.target.value) > parseFloat(propObject.maxHeight)) {
//                        showMessage("Thumbnail width/height should not be greater than Maximum Height.", DialogType.Error);
//                        $self.val(propObject.width);
//                        return;
//                    }
                        propObject.width = evnt.target.value;
                        propObject.fnSetThumbnail(propObject.width);
                },
                BackgroundColorChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.backgroundColor = evnt.target.value;
                },
                PenColorChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.penColor = evnt.target.value;
                }
            }
        },
        Data: {
            MaxWidth: function (control, appObj) {
                $(control).val(appObj.maxWidth);
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
            },
            MaxHeight: function (control, appObj) {
                $(control).val(appObj.maxHeight);
                $(control).keypress(function (event) {
                    mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);
                });
            },
            UploadType: function (control, cntrlObj) {
                if (cntrlObj.uploadType != null && cntrlObj.uploadType != undefined)
                    $(control).val(cntrlObj.uploadType);
                else
                    $(control).val("0");
            },
            FilePath: function (control, cntrlObj, options) {
                var currentCntrl = options.cntrlHtmlObjects;
                if (cntrlObj.uploadType == null || cntrlObj.uploadType == undefined) {
                    cntrlObj.uploadType = "0";
                }
                if (cntrlObj.uploadType === "0") {
                    $(control).val(cntrlObj.filePath);
                    currentCntrl.wrapperDiv.show();
                }
                else {
                    $(control).val("");
                    currentCntrl.wrapperDiv.hide();
                }
            },
            AWSPath: function (control, cntrlObj, options) {
                var currentCntrl = options.cntrlHtmlObjects;
                if (cntrlObj.uploadType != null && cntrlObj.uploadType !== undefined && cntrlObj.uploadType === "1") {
                    $(control).val(cntrlObj.filePath);
                    currentCntrl.wrapperDiv.show();
                }
                else {
                    $(control).val("");
                    currentCntrl.wrapperDiv.hide();
                }
            },
            BucketName: function (control, cntrlObj, options) {
                var currentCntrl = options.cntrlHtmlObjects;
                if (cntrlObj.uploadType != null && cntrlObj.uploadType !== undefined && cntrlObj.uploadType === "1") {
                    $(control).val(cntrlObj.bucketName);
                    currentCntrl.wrapperDiv.show();
                }
                else {
                    $(control).val("");
                    currentCntrl.wrapperDiv.hide();
                }
            },
            Credential: function (control, cntrlObj, options) {
                var strOptionsForDdl =
                    MF_HELPERS.dropDownOptionsHelpers.getawsCredentials(true, "-1");
                var currentCntrl = options.cntrlHtmlObjects;
                if (strOptionsForDdl) {
                    $(control).html(strOptionsForDdl);
                    $(control).val("-1");
                }
                if (cntrlObj.uploadType != null && cntrlObj.uploadType !== undefined && cntrlObj.uploadType === "1") {
                    $(control).val(cntrlObj.credential);
                    currentCntrl.wrapperDiv.show();
                }
                else {
                    currentCntrl.wrapperDiv.hide();
                }
            },
            MPlugin: function (control, cntrlObj) {
                var strOptionsForDdl =
                    MF_HELPERS.dropDownOptionsHelpers.getmPluginAgents(true, "-1"),

                    mPlugin = cntrlObj.fnGetmPlugin();
                $(control).html('');
                if (strOptionsForDdl) {
                    $(control).html(strOptionsForDdl);
                }
                if (mPlugin) {
                    $(control).val(mPlugin);
                }
            },
            eventHandlers: {
                MaxWidthChanged: function (evnt) {
                    var $self = $(this);
                    var propObject = evnt.data.propObject;
                    if (parseFloat(evnt.target.value) < 100) {
                        showMessage("Maximum width should be greater than 100.", DialogType.Error);
                        $self.val(propObject.maxWidth);
                    }
                    else
                        propObject.maxWidth = evnt.target.value;
                        
//                    if(propObject.width != undefined && propObject.width.trim().length > 0){
//                        if(evnt.target.value.trim().length <= 0){
//                            showMessage("Maximum width should be greater than thumbnail width.", DialogType.Error);
//                            $self.val(propObject.maxWidth);
//                        }
//                    }
                },
                MaxHeightChanged: function (evnt) {
                    var $self = $(this);
                    var propObject = evnt.data.propObject;
                    if (parseFloat(evnt.target.value) < 100) {
                        showMessage("Maximum height should be greater than 100.", DialogType.Error);
                        $self.val(propObject.maxHeight);
                    }
                    else
                        propObject.maxHeight = evnt.target.value;

//                    if(propObject.width != undefined && propObject.width.trim().length > 0){
//                        if(evnt.target.value.trim().length <= 0){
//                            showMessage("Maximum width should be greater than thumbnail width.", DialogType.Error);
//                            $self.val(propObject.maxWidth);
//                        }
//                    }
                },
                UploadTypeChanged: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.uploadType = $(this).val();
                    var cntrls = evnt.data.cntrls;
                    var $FilePathWrapper = $('#' + cntrls.FilePath.wrapperDiv);
                    var $AWSPathWrapper = $('#' + cntrls.AWSPath.wrapperDiv);
                    var $BucketNameWrapper = $('#' + cntrls.BucketName.wrapperDiv);
                    var $CredentialWrapper = $('#' + cntrls.Credential.wrapperDiv);

                    var $FilePathControl = $('#' + cntrls.FilePath.controlId);
                    var $AWSPathControl = $('#' + cntrls.AWSPath.controlId);
                    var $BucketNameControl = $('#' + cntrls.BucketName.controlId);
                    var $CredentialControl = $('#' + cntrls.Credential.controlId);

                    $FilePathControl.val("");
                    $AWSPathControl.val("");
                    $BucketNameControl.val("");
                    $CredentialControl.val("-1");

                    propObject.filePath = "";
                    propObject.bucketName = "";
                    propObject.credential = "-1";

                    if (propObject.uploadType === "0") {
                        $FilePathWrapper.show();
                        $AWSPathWrapper.hide();
                        $BucketNameWrapper.hide();
                        $CredentialWrapper.hide();
                    }
                    else {
                        $FilePathWrapper.hide();
                        $AWSPathWrapper.show();
                        $BucketNameWrapper.show();
                        $CredentialWrapper.show();
                    }
                },
                FilePath: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.filePath = $(this).val();
                },
                AWSPath: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.filePath = $(this).val();
                },
                BucketName: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.bucketName = $(this).val();
                },
                Credential: function (evnt) {
                    var propObject = evnt.data.propObject,
                        $self = $(this);
                    propObject.credential = $self.val();
                },
                MPlugin: function (evnt) {
                    var propObject = evnt.data.propObject,
                        $self = $(this);
                    if ($self.val() === "-1") {
                        propObject.fnSetmPlugin("");
                    }
                    else {
                        propObject.fnSetmPlugin($self.val());
                    }
                }
            }
        },
        Validation: {
            Required: function (control, appObj) {
                $(control).val(appObj.required)
            },
            eventHandlers: {
                IsRequired: function (evnt) {
                    var propObject = evnt.data.propObject;
                    propObject.required = evnt.target.value;
                }
            }
        },
        Advance: {
            Scripts: function (control, appObj) {
            },
            eventHandlers: {
                Scripts: function (evnt) {
                    $('#drpCntrlEvents').html();
                    $('#drpCntrlEvents').css('width', '');
                    var evntData = evnt.data;
                    var propObject = evnt.data.propObject;
                    $('#spanOnChngUserDefinedName').text(propObject.userDefinedName);

                    var scriptOnChange = '', scriptOnClear = '', script = '';

                    _jsAceEditorOnChang.setTextInEditor("");
                    var options = '<option value="onChange">onChange</option>'
                                + '<option value="onClear">onClear</option>';

                    $('#drpCntrlEvents').html(options);
                    $('#drpCntrlEvents').val("onChange");
                    $.uniform.update('#drpCntrlEvents');
                    var prevSelectedVal = $('#drpCntrlEvents')[0].value;

                    if (propObject.onChange != null && propObject.onChange != undefined) scriptOnChange = propObject.onChange;
                    if (propObject.onClear != null && propObject.onClear != undefined) scriptOnClear = propObject.onClear;
                    switch ($('#drpCntrlEvents')[0].value) {
                        case "onChange":
                            if (scriptOnChange != null && scriptOnChange != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));
                            }
                            break;
                        case "onClear":
                            if (scriptOnClear != null && scriptOnClear != undefined) {
                                _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnClear));
                            }
                            break;
                    }
                    $('#drpCntrlEvents').unbind('change');
                    $('#drpCntrlEvents').bind('change', function () {
                        $.uniform.update('#drpCntrlEvents');
                        script = _jsAceEditorOnChang.getText();
                        switch (prevSelectedVal) {
                            case "onChange":
                                if (script != null && script != undefined) {
                                    scriptOnChange = Base64.encode(script);
                                }
                                break;
                            case "onClear":
                                if (script != null && script != undefined) {
                                    scriptOnClear = Base64.encode(script);
                                }
                                break;
                        }
                        _jsAceEditorOnChang.setTextInEditor("");
                        prevSelectedVal = this.value;
                        switch (this.value) {
                            case "onChange":
                                if (scriptOnChange != null && scriptOnChange != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnChange));
                                }
                                break;
                            case "onClear":
                                if (scriptOnClear != null && scriptOnClear != undefined) {
                                    _jsAceEditorOnChang.setTextInEditor(Base64.decode(scriptOnClear));
                                }
                                break;
                        }
                    });

                    SubProcShowCntrlJsEditor(true, "Scripts");
                    $('[id$=popup_custom_buttom_save]').unbind('click');
                    $('[id$=popup_custom_buttom_save]').bind('click', function () {
                        script = _jsAceEditorOnChang.getText();
                        switch ($('#drpCntrlEvents')[0].value) {
                            case "onChange":
                                if (script != null && script != undefined) {
                                    scriptOnChange = Base64.encode(script);
                                }
                                break;
                            case "onClear":
                                if (script != null && script != undefined) {
                                    scriptOnClear = Base64.encode(script);
                                }
                                break;
                        }

                        if (scriptOnChange != null && scriptOnChange != undefined) propObject.onChange = scriptOnChange;
                        if (scriptOnClear != null && scriptOnClear != undefined) propObject.onClear = scriptOnClear;
                        SubProcShowCntrlJsEditor(false);
                        return false;
                    });
                    $('[id$=popup_custom_buttom_close]').unbind('click');
                    $('[id$=popup_custom_buttom_close]').bind('click', function () {
                        scriptOnChange = '', scriptOnClear = ''
                        SubProcShowCntrlJsEditor(false);
                        return false;
                    });
                }
            }
        },
        Behaviour: {
            Display: function (control, appObj) {
                $(control).val(appObj.conditionalDisplay)
            },
            Conditions: function (control, appObj) {
                var currentCntrl = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Signature.propSheetCntrl.Behaviour.getConditionsCntrl();
                if (appObj.conditionalDisplay == "0")
                    $(currentCntrl.wrapperDiv).show();
                else
                    $(currentCntrl.wrapperDiv).hide();

            },
            eventHandlers: {
                Display: function (evnt) {
                    var evntData = evnt.data;
                    var cntrls = evntData.cntrls;
                    var propObject = evnt.data.propObject;
                    propObject.conditionalDisplay = evnt.target.value;
                    var $Conditions = $('#' + cntrls.Conditions.wrapperDiv);
                    if (evnt.target.value == "0") {
                        $Conditions.show();

                    }
                    else {
                        $Conditions.hide();
                    }
                },
                ConditionalDisplay: function (evnt) {
                    var propObject = evnt.data.propObject;
                    var json = "[";
                    var controlJson = "";
                    var form = mFicientIde.MF_HELPERS.currentObjectHelper.getCurrentWorkingForm();
                    var controls = [];
                    var arrIntellisenseControls = [];
                    var objIntellisenseCntrl = {};
                    if ((form.rowPanels != null || form.rowPanels != undefined) && form.rowPanels.length > 0) {
                        $.each(form.rowPanels, function (rowIndex, rowPanel) {
                            if ((rowPanel.colmnPanels != null || rowPanel.colmnPanels != undefined) && rowPanel.colmnPanels.length > 0) {
                                $.each(rowPanel.colmnPanels, function (colIndex, columnPanel) {
                                    if ((columnPanel.controls != null || columnPanel.controls != undefined) && columnPanel.controls.length > 0) {
                                        $.each(columnPanel.controls, function (cntrlIndex, control) {
                                            objIntellisenseCntrl = {};
                                            if (control.id != propObject.id && !control.isDeleted && mFicientIde.MF_DATA_BINDING.isValidConditionalControl(control.type)) {
                                                controls.push(control);
                                                objIntellisenseCntrl["cntrlnm"] = control.userDefinedName;
                                                objIntellisenseCntrl["type"] = control.type;
                                                arrIntellisenseControls.push(objIntellisenseCntrl);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }

                    if ((controls != null || controls != undefined) && controls.length > 0) {
                        $.each(controls, function (index, value) {
                            if (controlJson.length > 0)
                                controlJson += ",";
                            controlJson += "{\"text\":\"" + value.userDefinedName + "\",\"val\":\"" + value.userDefinedName + "\"}";
                        });
                    }
                    json += controlJson + "]";
                    $('#divConditionalControl').mfConditionalLogicControl({ FirstDropDownOptions: json, ConditionalLogicDdlVals: mFicientIde.MF_IDE_CONSTANTS.getConditionalOperatorJson(), IntlsJson: mFicientIde.MF_IDE_CONSTANTS.getIntellisenseControlJson(JSON.stringify(arrIntellisenseControls)), IsEdit: (propObject.condDisplayCntrlProps != null || propObject.condDisplayCntrlProps != undefined) ? true : false, SavedJsonIfEdit: JSON.stringify(propObject.condDisplayCntrlProps), firstColWidth: 120 });
                    showModalPopUpWithOutHeader('SubProcConditionalDisplay', 'Conditional Behaviour', 520, false);
                    $('#divConditionalControl').data("ControlId", propObject.id);

                    $('[id$=btnCondSave]').unbind('click');
                    $('[id$=btnCondSave]').bind('click', function () {
                        propObject.condDisplayCntrlProps = $('#divConditionalControl').data("FinalJson")();
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });

                    $('[id$=btnCondCancel]').unbind('click');
                    $('[id$=btnCondCancel]').bind('click', function () {
                        $('#SubProcConditionalDisplay').dialog('close');
                        return false;
                    });
                }
            }
        }
    },
    propSheetCntrl: (function () {
        function _getDivControlHelperData() {
            return $('#' + MF_IDE_CONSTANTS.IDE_DIV_FOR_STORING_DATA.ControlData).data(MF_IDE_CONSTANTS.jqueryDataKeyForPropSheetHelper);
        }
        function _getConditionsCntrl() {
            var divData = _getDivControlHelperData();
            var objControl = null;
            if (divData) {
                objControl = divData.getControlDtls(
                    MF_IDE_CONSTANTS.CONTROLS.SIGNATURE.propPluginPrefix,
                    "Behaviour", "Conditions");
            }
            return objControl;
        }
        return {
            Behaviour: {
                getConditionsCntrl: _getConditionsCntrl
            }
        };
    })()
};

mFicientIde.PropertySheetJson.Signature = {
    "type": mFicientIde.MF_IDE_CONSTANTS.CONTROLS.SIGNATURE,
    "groups": [
      {
          "name": "Control Properties",
          "prefixText": "ControlProperties",
          "hidden": true,
          "properties": [
            {
                "text": "Type",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Type",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [

               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": true,
                    "defltValue": "Signature",
                    "hidden": false
                }
            },
            {
                "text": "Name",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "Name",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.ControlProperties.eventHandlers.NameChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Description",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textArea,
                "prefixText": "Description",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.ControlProperties.eventHandlers.DescChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 250,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            }
         ]
      },
      {
          "name": "Appearance",
          "prefixText": "Appearance",
          "hidden": false,
          "properties": [
            {
                "text": "Label Text",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "LabelText",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Appearance.eventHandlers.LabelTextChange,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Appearance.eventHandlers.LabelTextChange,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Label Text",
                    "hidden": false
                }
            },	
            {
                "text": "Font Style",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "FontStyle",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Appearance.eventHandlers.FontStyleChange,
                      "context": "",
                      "arguments": {
                      }
                  }

               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Normal",
                      "value": "0"
                  },
                  {
                      "text": "Bold",
                      "value": "1"
                  },
                  {
                      "text": "Italics",
                      "value": "2"
                  },
                  {
                      "text": "Bold And Italics",
                      "value": "3"
                  }
               ]
              },
              {
                  "text": "Font Color",
                  "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                  "prefixText": "FontColor",
                  "disabled": false,
                  "hidden": true,
                  "noOfLines": "0",
                  "validations": [

               ],
                  "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                  "events": [
               ],
                  "CntrlProp": "",
                  "HelpText": "",
                  "init": {
                      "disabled": false,
                      "defltValue": "",
                      "hidden": false
                  }
              },
            {
                "text": "Thumbnail (%)",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "ThumbnailWidth",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Appearance.eventHandlers.WidthChanged,
                      "context": "",
                      "arguments": {

                      }
                  },
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Appearance.eventHandlers.WidthChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Background Color",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "BackgroundColor",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Appearance.eventHandlers.BackgroundColorChanged,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Appearance.eventHandlers.BackgroundColorChanged,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": "",
                    "disabled": true,
                    "defltValue": "",
                    "hidden": true
                }
            },
            {
                "text": "Pen Color",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "PenColor",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Appearance.eventHandlers.PenColorChanged,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Appearance.eventHandlers.PenColorChanged,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": "",
                    "disabled": true,
                    "defltValue": "",
                    "hidden": true
                }
            }
         ]
      },
      {
           "name": "Data",
           "prefixText": "Data",
           "hidden": false,
           "properties": [
            {
                "text": "Max Width (px)",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MaxWidth",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Data.eventHandlers.MaxWidthChanged,
                      "context": "",
                      "arguments": {

                      }
                  },
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Data.eventHandlers.MaxWidthChanged,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Max Height (px)",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "MaxHeight",
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Data.eventHandlers.MaxHeightChanged,
                      "context": "",
                      "arguments": {
                      }
                  },
                  {
                      "name": "blur",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Data.eventHandlers.MaxHeightChanged,
                      "context": "",
                      "arguments": {
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "maxlength": 50,
                    "disabled": false,
                    "defltValue": "",
                    "hidden": false
                }
            },
            {
                "text": "Upload to AWS S3",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "UploadType",
                "defltValue": "0",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Data.eventHandlers.UploadTypeChanged,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "FilePath",
                               rtrnPropNm: "FilePath"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "AWSPath",
                               rtrnPropNm: "AWSPath"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "BucketName",
                               rtrnPropNm: "BucketName"
                           },
                           {
                               grpPrefText: "Data",
                               cntrlPrefText: "Credential",
                               rtrnPropNm: "Credential"
                           }
					  ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
            },
            {
               "text": "File Path",
               "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
               "prefixText": "FilePath",
               "defltValue": "1",
               "validations": [

                ],
               "customValidations": [

                ],
               "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Signature.groups.Data.eventHandlers.FilePath,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
               "CntrlProp": "",
               "HelpText": "",
               "selectValueBy": "",
               "init": {
                   "bindOptionsType": "",
                   "disabled": false,
                   "hidden": true
               },
               "options": [
                ]
           },
            {
                "text": "S3 Path",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "AWSPath",
                "defltValue": "1",
                "validations": [

                ],
                "customValidations": [

                ],
                "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Signature.groups.Data.eventHandlers.AWSPath,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": "",
                    "disabled": false,
                    "hidden": true
                },
                "options": [
                ]
            },
            {
                "text": "S3 Bucket",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.textbox,
                "prefixText": "BucketName",
                "defltValue": "1",
                "validations": [

                ],
                "customValidations": [

                ],
                "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Signature.groups.Data.eventHandlers.BucketName,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "maxlength": 256,
                    "bindOptionsType": "",
                    "disabled": false,
                    "hidden": true
                },
                "options": [
                ]
            },
            {
                "text": "Credential",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Credential",
                "defltValue": "1",
                "validations": [

                ],
                "customValidations": [

                ],
                "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Signature.groups.Data.eventHandlers.Credential,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": "",
                    "disabled": false,
                    "hidden": true
                },
                "options": [
                ]
            },
           {
               "text": "mPlugin Agent",
               "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
               "prefixText": "MPlugin",
               "defltValue": "1",
               "validations": [

                ],
               "customValidations": [

                ],
               "events": [
                   {
                       "name": "change",
                       "func": PROP_JSON_HTML_MAP.Signature.groups.Data.eventHandlers.MPlugin,
                       "context": "",
                       "arguments": {

                       }
                   }
                ],
               "CntrlProp": "",
               "HelpText": "",
               "selectValueBy": "",
               "init": {
                   "bindOptionsType": "",
                   "disabled": false,
                   "hidden": true
               },
               "options": [
                ]
           }
         ]
       },
       {
          "name": "Validation",
          "prefixText": "Validation",
          "hidden": false,
          "properties": [
          {
              "text": "Required",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
              "prefixText": "Required",
              "defltValue": "0",
              "validations": [

               ],
              "customValidations": [

               ],
              "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Validation.eventHandlers.IsRequired,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "selectValueBy": "",
              "init": {
                  "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                  "disabled": false,
                  "hidden": false
              },
              "options": [
                  {
                      "text": "No",
                      "value": "0"
                  },
                  {
                      "text": "Yes",
                      "value": "1"
                  }
               ]
             }
          ]
          },
          {
          "name": "Advance",
          "prefixText": "Advance",
          "hidden": false,
          "properties": [
          {
              "text": "Scripts",
              "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
              "prefixText": "Scripts",
              "disabled": false,
              "hidden": true,
              "noOfLines": "0",
              "validations": [

               ],
              "customValidations": [
               ],
              "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Advance.eventHandlers.Scripts,
                      "context": "",
                      "arguments": {}
                  }
               ],
              "CntrlProp": "",
              "HelpText": "",
              "init": {
                  "disabled": false,
                  "defltValue": "",
                  "hidden": false
              }
          }
          ]
      },
      {
          "name": "Behaviour",
          "prefixText": "Behaviour",
          "hidden": false,
          "properties": [
            {
                "text": "Display",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.dropdown,
                "prefixText": "Display",
                "defltValue": "1",
                "validations": [

               ],
                "customValidations": [

               ],
                "events": [
                  {
                      "name": "change",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Behaviour.eventHandlers.Display,
                      "context": "",
                      "arguments": {
                          "cntrls": [
                           {
                               grpPrefText: "Behaviour",
                               cntrlPrefText: "Conditions",
                               rtrnPropNm: "Conditions"
                           }
                        ]
                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "selectValueBy": "",
                "init": {
                    "bindOptionsType": MF_PROPS_CONSTANTS.DDL_BIND_TYPE.manual,
                    "disabled": false,
                    "hidden": false
                },
                "options": [
                  {
                      "text": "Always",
                      "value": "1"
                  },
                  {
                      "text": "Conditional",
                      "value": "0"
                  }
               ]
            },
            {
                "text": "Conditions",
                "control": MF_PROPS_CONSTANTS.CONTROL_TYPE.browse,
                "prefixText": "Conditions",
                "disabled": false,
                "noOfLines": "0",
                "validations": [

               ],
                "customValidations": [
                  {
                      "func": "functionName",
                      "context": "context"
                  }
               ],
                "events": [
                  {
                      "name": "click",
                      "func": PROP_JSON_HTML_MAP.Signature.groups.Behaviour.eventHandlers.ConditionalDisplay,
                      "context": "",
                      "arguments": {

                      }
                  }
               ],
                "CntrlProp": "",
                "HelpText": "",
                "init": {
                    "disabled": false,
                    "defltValue": "Select Conditions",
                    "hidden": true
                }
            }
         ]
      }
   ]
};


function Signature (opts){
     if (!opts) opts = {};
    this.id = opts.id;
    this.userDefinedName = opts.userDefinedName;
    this.ver = mFicientIde.MF_IDE_CONSTANTS.MF_CONTROL_VERSION.Signature;
    this.description = opts.description;
    this.type = opts.type;
    this.isDeleted = opts.isDeleted;
    this.oldName = opts.oldName;
    this.isNew = opts.isNew;
    this.labelText = opts.labelText;
    this.width = opts.width;
    this.maxHeight = opts.maxHeight;
    this.maxWidth = opts.maxWidth;
    this.backgroundColor = opts.backgroundColor;
    this.penColor = opts.penColor;
    this.filePath = opts.filePath;
    this.mPlugin = opts.mPlugin;
    this.required = opts.required;
    this.conditionalDisplay = opts.conditionalDisplay;
    this.condDisplayCntrlProps = opts.condDisplayCntrlProps;
    this.uploadType = opts.uploadType;
    this.bucketName = opts.bucketName;
    this.credential = opts.credential;
    this.fontStyle = opts.fontStyle;
    this.fontColor = opts.fontColor;
    this.onChange = opts.onChange;
    this.onClear = opts.onClear;
}

Signature.prototype = new ControlNew();
Signature.prototype.propSheetHtmlMapping = mFicientIde.JSON_TO_HTML_MAPPING_BY_PROPSHEET.Signature;
Signature.prototype.fnSetDefaults = function () {
    this.isNew = true;
    this.labelText = "Label Text";
    this.width = "100";
    this.maxWidth = "150";
    this.maxHeight = "150";
    this.backgroundColor = "#FFFFFF";
    this.penColor = "#000000";
    this.required = "0";
    this.conditionalDisplay = "1";
    this.mPlugin = "-1";
    this.fontStyle = "0";
    this.isDeleted = false; 
};
Signature.prototype.fnRename = function (name) {
    if (!this.isNew &&
        !this.oldName &&
        this.userDefinedName !== name) {

        this.oldName = this.userDefinedName;
        this.userDefinedName = name;
    }
    else if (this.userDefinedName != name) {
        this.userDefinedName = name;
    }
};
Signature.prototype.fnGetmPlugin = function () {
    return this.mPlugin;
};
Signature.prototype.fnSetmPlugin = function (value) {
    this.mPlugin = value;
};
Signature.prototype.fnSetThumbnail = function (value) {
    this.width = value;
    $('#' + this.id + '_OuterDiv').css('width', value + '%');
    var calWidth = $('#' + this.id + '_OuterDiv').width();
    $('#' + this.id + '_OuterDiv').css('height', calWidth + 'px');
};



var mfIdeAdvanControls = "mfIdeAdvanControls";
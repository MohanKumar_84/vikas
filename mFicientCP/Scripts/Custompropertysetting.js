﻿var namebtn = "<a data-tooltip=Updatename onclick=CredentialAuthenticationSaveName(this); ><img  src=\css/images/pencil16x16.png\ title=Set credential/  class=btnstyle ></a>";
var SettingButtonHtml = '<a  style="float:left;"  onclick="SettingOfProp(this);"><img  src=\"images/settings_grey.png\" title="Setting"/></a>';
var RemoveButtonHtml = '<a   style="float:right;" onclick="AddPropDelete(this);"><img  src=\"images/remove3_grey.png\" title="Remove Addtional Property"/></a>';
var AddButtonHtml = '<a style="float:left" onclick="AddPropSave(this);"><img  src=\"images/add3_grey.png\" title="Save Addtional Property"/></a>';
var TypeHtml = '<select id="selTypeHtml" class="UseUniformCss"><option value="1">Text ( free Entry )</option><option value="2">Selector</option><option value="3">User Selector</option><option value="4">Group Selector</option></select>';
var AddPropNameHtml = '<input type="text" id="AddPropName" class="Intext" style="width:200px;"  maxlength="50" value="">';
var AddProptagHtml = '<input type="text" id="AddProptag" class="Intext" style="width:200px;"  maxlength="15" value="">';
var AddTextInputHtml = '<input type="text" id="AddText"  maxlength="50" value="">';
var AddValueInputHtml = '<input type="text" id="AddValue"  maxlength="50" value="">';
var RemoveOptionButton = '<a onclick="DeleteOption(this);"><img  src=\"images/remove3_grey.png\" title="Remove Option"/></a>';
var AddOptionButton = '<a  onclick="AddOptions(this);"><img  src=\"images/add3_grey.png\" title="Add Option"/></a>';
var SaveOptionButton = '<a onclick="SaveOptions(this);"><img  src=\"images/check.png\" title="Save Option"/></a>';
var DeleteConfirmationMessage = 'Are you sure you want to delete this Additional Property';
var AddPropertyListData = [];
var OptionsBindigData;
var NewOptionInSetting = [];
var IsAddOptionRowOpen = false;
var ResetValidSetting = { req: 1, vtype: 1, allowNumStart: 0, allowSpace: 1, specialchar: "", selectopt: [] };
var EditTag = "";
var vcredentialtag;

function CredentialAuthenticationSaveName(sender) {
    showModalPopUp('divupdateCredential', " Name Setting  ", '350', false);

    vcredentialtag = sender;
    $('#txtupdateCredentialname').val('');
    $('#txtupdateCredentialname').val($(sender).parents('tr').find('td::nth-child(1)').text());
}
function UpdateAuthenticationnames() {
    $('#divupdateCredential').dialog('close');
    var table = $('#tblAddProp').DataTable();
    var row = table.row($(vcredentialtag).parents('tr'));
    var datarow = row.data();
    $.each(AddPropertyListData, function (i, val) {
        if (AddPropertyListData[i].tag == datarow[1]) {
            var newName = $('#txtupdateCredentialname').val();
            AddPropertyListData[i].name = newName;
            $('#txtupdateCredentialname').val('');
            datarow[0] = newName + namebtn;
            vcredentialtag = '';
            UpdateToDatabase(AddPropertyListData[i].tag, "2");
            row.data(datarow).draw();
            return true;
        }
    });
    return false;
}



//bind additional properties
function LoadProperyPage() {
    var AddPropertyBindingData = [];
    var strData = $('[id$=hidData]').val();
    $('[id$=hidData]').val("");
    if (strData.length > 0) {
        AddPropertyListData = jQuery.parseJSON(strData);
    }
    jQuery.each(AddPropertyListData, function (i, val) {
        AddPropertyBindingData.push([val.name + namebtn, val.tag, val.type, SettingButtonHtml + "&nbsp;&nbsp;" + RemoveButtonHtml]);

    });
    AddPropertyBindingData.push([AddPropNameHtml, AddProptagHtml, TypeHtml, AddButtonHtml]);
    BindDatabasedata(AddPropertyBindingData);
}

function BindDatabasedata(AddPropertyBindingData) {
    $('#AddProp').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="tblAddProp"></table>');
    $('#tblAddProp').dataTable({
        "data": AddPropertyBindingData,
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "bSort": false,
        "columns": [{ "title": "Name" }, { "title": "Key" }, { "title": "Type" }, { "title": ""}]
    });
    totalRows = 1;
    var table = $('#tblAddProp').DataTable();

    $('#tblAddProp tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
     
    });
}
//Delete Additional Property
function AddPropDelete(sender) {
    var table = $('#tblAddProp').DataTable();
    var row = table.row($(sender).parents('tr'));
    var data = row.data();
    var tag = data[1];
    var confirmMsg = confirm(DeleteConfirmationMessage);
    if (confirmMsg) {
      
        row.remove().draw();
        AddPropertyListData.splice(row.index(), 1);
        totalRows--;
        UpdateToDatabase(tag, "3");
        return true;
    }
    else return false;
}
function UpdateToDatabase(tag, opration) {
    $('[id$=hidData]').val(JSON.stringify(AddPropertyListData));

    $('[id$=hidUpdateTag]').val(tag + "/" + opration + "/1");
    UpdateUpdBtnRowIndex();
}

//Save Additional Property
function AddPropSave(sender) {
    var table = $('#tblAddProp').DataTable();
    var row = table.row($(sender).parents('tr'));
    var AddProperty = {};

    var sel = document.getElementById("selTypeHtml");
    var option = sel.options[sel.selectedIndex];

    AddProperty.type = option.text;
    AddProperty.name = $('#AddPropName').val();
    AddProperty.tag = $('#AddProptag').val();
    if (totalRows < 6) {
        var errorMessage = CheckAddPropertyName(AddProperty.name.toLowerCase());
        if (errorMessage.length <= 0) errorMessage = CheckAddPropertyTag(AddProperty.tag.toLowerCase());

        if (errorMessage.length > 0) { alert(errorMessage); return; }
        else {

            EditTag = AddProperty.tag;
            AddProperty.typeval = option.value;
            AddProperty.validationSetting = ResetValidSetting;
            AddPropertyListData.push(AddProperty);

            totalRows++; row.remove().draw();
            if (AddProperty.type == '1' || AddProperty.type == '2')
                table.row.add([AddProperty.name, AddProperty.tag, AddProperty.type, SettingButtonHtml + "&nbsp;&nbsp;" + RemoveButtonHtml]).draw();
            else
                table.row.add([AddProperty.name, AddProperty.tag, AddProperty.type, RemoveButtonHtml]).draw();

            table.row.add([AddPropNameHtml, AddProptagHtml, TypeHtml, AddButtonHtml]).draw();
            UpdateToDatabase(EditTag, "1");

        }
    }
    else {
        alert("Max Property Already Added");
    }
}
function CheckAddPropertyName(PropName, rowData) {
    var table = $('#tblAddProp').DataTable();
    var errorMessage = '';if (PropName.length <= 0) {
        errorMessage = 'Enter Property Name';
    }
    else {
        table.rows().indexes().each(function (idx) {
            var rowData = table.row(idx).data();
            if (rowData[0].toLowerCase().indexOf(PropName + '<') === 0)
                errorMessage = 'Additional Property Name Already Exists';
        });
        if (errorMessage.length <= 0)
            errorMessage = (/^[A-Za-z][a-zA-Z0-9 &':,]*$/.test(PropName.toLowerCase())) ? '' : 'Invalid Additional Property Name';
    }
    return errorMessage;
}
function CheckAddPropertyTag(PropTag) {
    var table = $('#tblAddProp').DataTable();
    var errorMessage = '';
    if (PropTag.length <= 0) {
        errorMessage = 'Enter Property Tag';
    }
    else {
        table.rows().indexes().each(function (idx) {
            var rowData = table.row(idx).data();
            if (PropTag == rowData[1].toLowerCase())
                errorMessage = 'Additional Property Tag Already Exists';
        });
        if (errorMessage.length <= 0)
            errorMessage = (/^[A-Za-z][a-zA-Z0-9_]*$/.test(PropTag.toLowerCase())) ? '' : 'Invalid Additional Property Tag';
    }
    return errorMessage;
}
//Show Additional Property Setting
function SettingOfProp(sender) {
    var table = $('#tblAddProp').DataTable();
    var data = table.row($(sender).parents('tr')).data();
    var key = data[1];
    jQuery.each(AddPropertyListData, function (i, val) {
        if (val.tag === key) {
            if (val.typeval != '3' && val.typeval != '4') {
                EditTag = key;

                $('#selTextValid').val(val.validationSetting.vtype);
                $('#txtSpecialCharacters').val(val.validationSetting.specialchar);
                if (val.validationSetting.req == 1) $('#CheckReq').prop('checked', true);
                else $('#CheckReq').prop('checked', false);

                if (val.validationSetting.allowNumStart == 1) $('#CheckNumeric').prop('checked', true);
                else $('#CheckNumeric').prop('checked', false);
                if (val.validationSetting.allowSpace == 1) $('#CheckSpace').prop('checked', true);
                else $('#CheckSpace').prop('checked', false);

                $('[id$=lblType]').text(data[1]);
                if (val.typeval == "1") {
                    $("#SelectValidationDiv").hide();
                    $("#txtValidationDiv").show();
                    TextTypeChange();
                }
                else {
                    var dataBind = [];
                    jQuery.each(val.validationSetting.selectopt, function (i, value) {
                        dataBind.push([value[0], value[1], AddOptionButton + "&nbsp;&nbsp;" + RemoveOptionButton])
                        NewOptionInSetting.push([value[0], value[1]]);
                    });
                    BindOption(dataBind, key);
                    $("#txtValidationDiv").hide();
                    $("#SelectValidationDiv").show();
                }
                showModalPopUp('divModalContainer', " Setting ", '500', false);
                return true;
            }
        }
    });
    return false;
}
//Save Validation Setting
function SaveValidation(sender) {
    var CheckNumeric = $('#CheckNumeric').is(":checked") ? "1" : "0";
    var CheckSpace = $('#CheckSpace').is(":checked") ? "1" : "0";
    var CheckReq = $('#CheckReq').is(":checked") ? "1" : "0";
    var txtSpecialCharacters = $('#txtSpecialCharacters').is(":checked") ? "1" : "0";
    var selTextValid = $('#selTextValid').val();
    jQuery.each(AddPropertyListData, function (i, val) {
        if (val.tag === EditTag) {
            if (val.typeval == "2" && NewOptionInSetting.length <= 0) {
                alert("Atleast Add One Option");
            }
            else {
                val.validationSetting.req = CheckReq;
                val.validationSetting.allowNumStart = CheckNumeric;
                val.validationSetting.allowSpace = CheckSpace;
                val.validationSetting.specialchar = txtSpecialCharacters;
                val.validationSetting.vtype = selTextValid;
                val.validationSetting.selectopt = NewOptionInSetting;
                NewOptionInSetting = [];
                $('#divModalContainer').dialog('close');
                UpdateToDatabase(EditTag, "2");
                return true;

            }
        }
    });
    return false;
}




function BindOption(data, tag) {
    $('#SelectoptionsDiv').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="tblDdlOption"></table>');
    if (data.length <= 0) data.push([AddTextInputHtml, AddValueInputHtml, SaveOptionButton + "&nbsp;&nbsp;" + RemoveOptionButton]);
    $('#tblDdlOption').dataTable({
        "data": data,
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "bSort": false,
        "columns": [{ "title": "Text" }, { "title": "Value" }, { "title": ""}]
    });
    OptionsBindigData = data;
    var table = $('#tblDdlOption').DataTable();
    EditTag = tag;
    $('#tblDdlOption tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });
}
function DeleteOption(sender) {
    var table = $('#tblDdlOption').DataTable();
    var row = table.row($(sender).parents('tr'));
    var data = row.data();
    if (NewOptionInSetting.length > 0) {
        var confirmMsg = confirm('Are you sure you want to delete this option');
        if (confirmMsg) {
            for (var i = 0; i < NewOptionInSetting.length; i++) {
                if (data[0].toLowerCase() == NewOptionInSetting[i][0].toLowerCase()) {
                    row.remove().draw();
                    OptionsBindigData.splice(row.index(), 1);
                    BindOption(OptionsBindigData, EditTag);
                    NewOptionInSetting.splice(i, 1);
                    return true;
                }
            }
        }
        row.remove().draw();
        OptionsBindigData.splice(row.index(), 1);
        BindOption(OptionsBindigData, EditTag);
        IsAddOptionRowOpen = false;
    }
    else {
        alert("Atleast Add One Option");
    }

}
function AddOptions(sender) {
    if (!IsAddOptionRowOpen) {
        if (NewOptionInSetting.length < 100) {
            IsAddOptionRowOpen = true;
            var table = $('#tblDdlOption').DataTable();
            var row = table.row($(sender).parents('tr'));
            OptionsBindigData.splice(row.index() + 1, 0, [AddTextInputHtml, AddValueInputHtml, SaveOptionButton + "&nbsp;&nbsp;" + RemoveOptionButton]);
            BindOption(OptionsBindigData, EditTag);
        }
        else {
            alert("Max Options Already Added");
        }
    }
    else {
        alert("Already Open Save Row.");
    }
}
function SaveOptions(sender) {

    var table = $('#tblDdlOption').DataTable();
    var row = table.row($(sender).parents('tr'));
    var AddTextVal = $('#AddText').val();
    var AddValueVal = $('#AddValue').val();
    var errorMessage = CheckOptionText(AddTextVal.toLowerCase());
    if (errorMessage.length <= 0) errorMessage = CheckOptionValue(AddValueVal.toLowerCase());

    if (errorMessage.length > 0) { alert(errorMessage); return; }
    else {
        var newdata = [AddTextVal, AddValueVal, AddOptionButton + "&nbsp;&nbsp;" + RemoveOptionButton];
        row.data(newdata).draw();
        OptionsBindigData[row.index()] = newdata;
        NewOptionInSetting.push([AddTextVal, AddValueVal]);
        IsAddOptionRowOpen = false;
    }
}
function CheckOptionText(PropName, rowData) {
    var errorMessage = '';
    jQuery.each(NewOptionInSetting, function (i, val) {
        if (PropName.toLowerCase() == val[0].toLowerCase())
            errorMessage = 'Option Text Already Exists';
    });
//    if (errorMessage.length <= 0)
//        errorMessage = (/^[a-zA-Z0-9 -&]*$/.test(PropName.toLowerCase())) ? '' : 'Invalid Text';
    return errorMessage;
}
function CheckOptionValue(PropTag) {
    var errorMessage = '';
    jQuery.each(NewOptionInSetting, function (i, val) {
        if (PropTag == val[0].toLowerCase())
            errorMessage = 'Option Value Already Exists';
    });
    if (errorMessage.length <= 0)
        errorMessage = (/^[a-zA-Z0-9 ]*$/.test(PropTag.toLowerCase())) ? '' : 'Invalid Value';
    return errorMessage;
}
function TextTypeChange() {
    $("#CheckNumericdiv").hide();
    $("#txtSpecialCharactersdiv").hide();
    if ($('#selTextValid').val() == "2") { $("#CheckNumericdiv").show(); }
    else if ($('#selTextValid').val() == "3") { $("#txtSpecialCharactersdiv").show(); $("#CheckNumericdiv").show(); }

}
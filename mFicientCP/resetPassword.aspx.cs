﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
//using mficientCP;
namespace mFicientCP
{
    public partial class resetPassword : System.Web.UI.Page
    {
        public enum ResetPassword_ERROR_CODES
        {
            InvalidErrorCode,
            InvalidUserType,
            InvalidQueryString,
        }
        //string _queryStringData;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                if (Request.QueryString != null)
                {
                    pnlResetPasswordInfo.Visible = true;
                    //pnlResetPassword.Visible = false;
                    if (Request.QueryString.Count > 0)
                    {
                        setHidQueryStringValue(Request.QueryString.Get(0));
                        if (String.IsNullOrEmpty(hidQueryString.Value))
                        {
                            //Utilities.showAlertOnPageStartUp(
                            //"Invalid Link.",
                            //"content", this.Page, "Invalid link");
                            //return;
                            lblMessage.Text = "Invalid Link.";
                            pnlResetPassword.Visible = true;
                            pnlResetPasswordInfo.Visible = false;
                            return;
                        }
                        else
                        {
                            if (getCountOfQueryStringParams(hidQueryString.Value) < 3)
                            {
                            //    Utilities.showAlertOnPageStartUp(
                            //"Invalid Link.",
                            //"content", this.Page, "Invalid link");
                                lblMessage.Text = "Invalid Link.";
                                pnlResetPassword.Visible = true;
                                pnlResetPasswordInfo.Visible = false;
                                return;
                            }
                            else
                            {
                                processQueryStringRequest(hidQueryString.Value);
                            }
                        }
                    }
                    else
                    {
                        //Utilities.showAlertOnPageStartUp(
                        //"Invalid Link.",
                        //"content", this.Page, "Invalid link");
                        lblMessage.Text = "Invalid Link.";
                        pnlResetPassword.Visible = true;
                        pnlResetPasswordInfo.Visible = false;
                        return;
                    }
                }
                else
                {
                    //Utilities.showAlertOnPageStartUp(
                    //    "Invalid Link.",
                    //    "content", this.Page, "Invalid link");
                    lblMessage.Text = "Invalid Link.";
                    pnlResetPassword.Visible = true;
                    pnlResetPasswordInfo.Visible = false;
                    return;
                }

            }
        }

        //Reset Password 
        GetUserDetailsForResetPassword getNewPwdDtlsByUserNameAndCompanyId(string queryString)
        {
            string strUserName, strUserType, strCompanyId = String.Empty;
            getValuesFromQueryString(queryString, out strUserName, out strUserType, out strCompanyId);

            if (!String.IsNullOrEmpty(queryString))
            {
                GetUserDetailsForResetPassword objUsrDtlForResetPwd =
                   new GetUserDetailsForResetPassword(
                       strUserName,
                       strCompanyId,
                       this.getUserOfRequest(strUserType)
                       );
                objUsrDtlForResetPwd.Process();
                return objUsrDtlForResetPwd;
            }
            else
            {
                throw new MficientException(((int)ResetPassword_ERROR_CODES.InvalidQueryString).ToString());
            }
        }

        RESET_PASSWORD_REQUEST_BY getUserOfRequest(string userType)
        {
            if (String.IsNullOrEmpty(userType)) throw new ArgumentNullException();
            RESET_PASSWORD_REQUEST_BY eReqByUser =
                (RESET_PASSWORD_REQUEST_BY)Enum.Parse(typeof(RESET_PASSWORD_REQUEST_BY), userType);
            if (Enum.IsDefined(typeof(RESET_PASSWORD_REQUEST_BY), eReqByUser))
                return eReqByUser;
            else
                throw new MficientException(((int)ResetPassword_ERROR_CODES.InvalidUserType).ToString());

        }

        string[] getSplitValuesFromQueryString(string queryString)
        {
            return Utilities.DecryptString(queryString).Split('~');
        }

        void getValuesFromQueryString(string queryString, out string userName, out string usertype, out string companyId)
        {
            userName = string.Empty;
            usertype = string.Empty;
            companyId = string.Empty;
            string[] arySplitValues = this.getSplitValuesFromQueryString(queryString);
            if (arySplitValues.Length >= 3)
            {
                userName = returnUsernameFromSplitValue(arySplitValues);
                companyId = returnCompanyIdFromSplitValue(arySplitValues);
                usertype = returnUserTypeFromSplitValue(arySplitValues);
            }
        }
        string getCmpIdFromQueryString(string queryString)
        {
            string strCompanyId = string.Empty;
            string[] arySplitValues = this.getSplitValuesFromQueryString(queryString);
            if (arySplitValues.Length >= 3)
            {
                strCompanyId = returnCompanyIdFromSplitValue(arySplitValues);
            }
            return strCompanyId;
        }

        string returnCompanyIdFromSplitValue(string[] aryQueryStringSplitValues)
        {
            string strCompanyId = String.Empty;
            if (aryQueryStringSplitValues.Length >= 3)
            {
                strCompanyId = aryQueryStringSplitValues[1];
            }
            return strCompanyId;
        }
        string returnUsernameFromSplitValue(string[] aryQueryStringSplitValues)
        {
            string strUsername = String.Empty;
            if (aryQueryStringSplitValues.Length >= 3)
            {
                strUsername = aryQueryStringSplitValues[0];
            }
            return strUsername;
        }
        string returnUserTypeFromSplitValue(string[] aryQueryStringSplitValues)
        {
            string strUsertype = String.Empty;
            if (aryQueryStringSplitValues.Length >= 3)
            {
                strUsertype = aryQueryStringSplitValues[2];
            }
            return strUsertype;
        }

        int getCountOfQueryStringParams(string queryString)
        {
            return getSplitValuesFromQueryString(queryString).Length;
        }
        void processQueryStringRequest(string queryString)
        {
            try
            {
                GetUserDetailsForResetPassword objUserDtlWithNewResetPwd =
                    getNewPwdDtlsByUserNameAndCompanyId(queryString);

                if (objUserDtlWithNewResetPwd.StatusCode != 0 || String.IsNullOrEmpty(objUserDtlWithNewResetPwd.UserID))
                {
                    //Utilities.showAlertOnPageStartUp("Invalid Link.",
                    //    "content", this.Page, "Invalid link");
                    lblMessage.Text = "Invalid Link.";
                    pnlResetPassword.Visible = true;
                    pnlResetPasswordInfo.Visible = false;
                    setHidQueryStringValue(String.Empty);
                }
                else if (objUserDtlWithNewResetPwd.IsUserBlocked)
                {
                    //Utilities.showAlertOnPageStartUp(
                    //    "The user is blocked.Resetting the password is not allowed.",
                    //    "content", this.Page, "UserBlocked");
                    lblMessage.Text = "User ID '" + objUserDtlWithNewResetPwd.UserName + "' Blocked.";
                            pnlResetPassword.Visible = true;
                            pnlResetPasswordInfo.Visible = false;
                    setHidQueryStringValue(String.Empty);
                }
                else if (objUserDtlWithNewResetPwd.IsExpired)
                {
                    lblMessage.Text = "This link has expired.";
                    pnlResetPassword.Visible = true;
                    pnlResetPasswordInfo.Visible = false;
                }
                else if (isResetPwdRequestWithinStipulatedTime(objUserDtlWithNewResetPwd.AccessCodeResetDatetime))
                {
                    hidUsrAndEmail.Value = Utilities.EncryptString(objUserDtlWithNewResetPwd.UserID + "&" + objUserDtlWithNewResetPwd.EmailID);
                    showDetailsInControls();
                    pnlResetPasswordInfo.Visible = true;
                    //pnlResetPassword.Visible = false;
                }
                else
                {
                    //TODO COMPLETE THE ELSE STATEMENT
                    //show error message //show pop up for reset again
                    //hidUsrAndEmail.Value = Utilities.EncryptString((string)dtblUserNewPwdDtl.Rows[0]["USER_ID"] + "&" + (string)dtblUserNewPwdDtl.Rows[0]["EMAIL_ID"]);
                    //pnlResetPasswordInfo.Visible = false;
                    //pnlResetPassword.Visible = true;
                    //Utilities.showAlertOnPageStartUp(
                    //    "Your reset password code has expired.",
                    //    "content", this.Page, "Code expired.");
                    lblMessage.Text = "This link has expired.";
                    pnlResetPassword.Visible = true;
                    pnlResetPasswordInfo.Visible = false;
                }
            }
            catch
            {
                lblMessage.Text = "Invalid Link.";
                pnlResetPassword.Visible = true;
                pnlResetPasswordInfo.Visible = false;
                setHidQueryStringValue(String.Empty);
            }

        }
        /// <summary>
        /// This Reset Pwd Request should be within 48 hrs 
        /// </summary>
        /// <param name="newPwdDtlsDTable"></param>
        /// <returns></returns>
        bool isResetPwdRequestWithinStipulatedTime(long pwdResetDatetime)
        {
            // DateTime dtNewPwdReqDateTime = new DateTime(lngNewPwdRequestedTime);
            //TimeSpan tsTimeIntervalBtwnReqAndUsage = DateTime.UtcNow - dtNewPwdReqDateTime;
            DateTime dt = new DateTime(pwdResetDatetime).AddHours(48);
            if (DateTime.UtcNow < dt)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Modal pop up reset button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(hidQueryString.Value))
                {
                    Utilities.showAlert(
                        "Invalid Username and / or Reset code and / or Company Id",
                        "content", updResetPwdInfo, "ShowErrorMsg");
                }
                else
                {
                    GetUserDetailsForResetPassword objUserDtlWithNewResetPwd =
                        getNewPwdDtlsByUserNameAndCompanyId(hidQueryString.Value);
                    if (objUserDtlWithNewResetPwd.StatusCode == 0)
                    {
                        if (!String.IsNullOrEmpty(objUserDtlWithNewResetPwd.UserID))
                        {
                            if (objUserDtlWithNewResetPwd.NewAccessCode == Utilities.GetMd5Hash(txtResetCode.Value.Trim()))
                            {
                                Utilities.showModalPopup(
                                    "divModalContainer",
                                    updResetPwdInfo,
                                    "Change Password",
                                    Convert.ToString(Utilities.getModalPopUpWidth(MODAL_POP_UP_NAME.CHANGE_PASSWORD)), false);
                            }
                            else
                            {
                                Utilities.showAlert(
                                    "Reset Code is incorrect",
                                    "content", updResetPwdInfo,
                                    "ShowErrorMsg");
                            }
                        }
                        else
                        {
                            Utilities.showAlert(
                        "Invalid Username and / or Reset code and / or Company Id",
                        "content", updResetPwdInfo, "ShowErrorMsg");
                        }
                    }
                    else
                    {
                        //show error message that there was some internal error
                        Utilities.showAlert(
                            "Internal server error.",
                            "content", updResetPwdInfo,
                                    "ShowErrorMsg");
                    }
                }
            }
            catch (MficientException ex)
            {
                if (isErrorADefinedErrorCode(ex.Message))
                {
                    Utilities.showAlert(
                          getErrorMessageFromCode(ex.Message),
                         "content", updResetPwdInfo, "Error"); ;
                }
                else
                {
                    Utilities.showAlert(
                          ex.Message,
                         "content", updResetPwdInfo, "Error");
                }
            }
            catch
            {
                Utilities.showAlert(
                        "Internal server error.",
                        "content", updResetPwdInfo, "Error");
            }
        }

        protected void btnSavePassword_Click(object sender, EventArgs e)
        {
            try
            {
                //!Utilities.IsValidString(txtPassword.Text, true, true, true, @"!@#$%^&*()_-", 6, 50, false, false)
                if (!Utilities.IsValidString(txtNewPassword.Text, true, true, true, @"!@#$%^&*()_-", 6, 50, false, false))
                {
                    Utilities.showAlert("Please enter a valid password.",
                        "divChangePwdError", updModalContainer);
                }
                else if (txtNewPassword.Text != txtRetypeNewPassword.Text)
                {
                    Utilities.showAlert("Password entered does not match.",
                        "divChangePwdError", updModalContainer);
                }
                else
                {
                    GetUserDetailsForResetPassword objUserDtlWithNewResetPwd =
                        getNewPwdDtlsByUserNameAndCompanyId(hidQueryString.Value);
                    if (objUserDtlWithNewResetPwd.StatusCode == 0)
                    {
                        if (!String.IsNullOrEmpty(objUserDtlWithNewResetPwd.UserID))
                        {

                            SaveNewPassword objSaveNewPassword =
                                new SaveNewPassword(
                                    objUserDtlWithNewResetPwd.UserID,
                                    objUserDtlWithNewResetPwd.UserName,
                                    objUserDtlWithNewResetPwd.CompanyId,
                                    txtNewPassword.Text.Trim(),
                                    objUserDtlWithNewResetPwd.EmailID,
                                    objUserDtlWithNewResetPwd.PwdResetRequestedBy,
                                    this.Context);
                            objSaveNewPassword.Process();

                            if (objSaveNewPassword.StatusCode == 0)
                            {
                                Utilities.closeModalPopUp("divModalContainer", updModalContainer);
                                Utilities.showAlert(
                                    "Password changed successfully.",
                                    "content",
                                    updModalContainer);
                            }
                            else
                            {
                                //show error message that there was some internal error
                                Utilities.showAlert(
                                    "Internal server error.Please try again.",
                                    "content",
                                    updModalContainer);
                            }

                        }
                    }
                }
            }
            catch (MficientException ex)
            {
                if (isErrorADefinedErrorCode(ex.Message))
                {
                    Utilities.showAlert(
                          getErrorMessageFromCode(ex.Message),
                         "content", updResetPwdInfo, "Error"); ;
                }
                else
                {
                    Utilities.showAlert(
                          ex.Message,
                         "content", updResetPwdInfo, "Error");
                }
            }
            catch
            {
                Utilities.showAlert(
                        "Internal server error.",
                        "content", updResetPwdInfo, "Error");
            }
        }

        void showDetailsInControls()
        {
            string strUserName, strCompanyId, strUserType = String.Empty;
            getValuesFromQueryString(hidQueryString.Value, out strUserName, out strUserType, out strCompanyId);
            lblUserName.Text = strUserName;
            lblCompanyId.Text = strCompanyId;
        }
        /// <summary>
        /// Reset Password button when user resets his password again
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnResetPassword_Click(object sender, EventArgs e)
        {
            //string strUserIdAndEmail = Utilities.DecryptString(hidUsrAndEmail.Value);
            //_queryStringData = Utilities.DecryptString(hidUsrAndEmail.Value);

            //ResetMobileUserPassword objRstMobileUsrPwd = 
            //    new ResetMobileUserPassword(
            //        _queryStringData.Split(new char[] { '~' })[0], 
            //        _queryStringData.Split(new char[] { '~' })[1], 
            //        strUserIdAndEmail.Split(new char[] { '&' })[0], 
            //        strUserIdAndEmail.Split(new char[] { '&' })[1]
            //        );
            //objRstMobileUsrPwd.Process();

            //if (objRstMobileUsrPwd.StatusCode == 0)
            //{
            //    showAlert("Password changed successfully.", "content");
            //}
            //else
            //{
            //    Utilities.showMessage("Internal Error.Please try again", UpdatePanel1, DIALOG_TYPE.Error);
            //}
        }
        #region Error Message Helpers
        string getErrorMessageFromCode(string errorCode)
        {
            string strErrorMsg = String.Empty;
            ResetPassword_ERROR_CODES eErrorCode =
               (ResetPassword_ERROR_CODES)Enum.Parse(typeof(ResetPassword_ERROR_CODES), errorCode);
            if (Enum.IsDefined(typeof(ResetPassword_ERROR_CODES), eErrorCode))
            {
                switch (eErrorCode)
                {
                    case ResetPassword_ERROR_CODES.InvalidUserType:
                        strErrorMsg = "Invalid link.";
                        break;
                    case ResetPassword_ERROR_CODES.InvalidQueryString:
                        strErrorMsg = "Invalid link.";
                        break;
                }
            }
            else
            {
                throw new MficientException(((int)ResetPassword_ERROR_CODES.InvalidErrorCode).ToString());
            }
            return strErrorMsg;
        }
        bool isErrorADefinedErrorCode(string error)
        {
            bool isDefined = false;
            try
            {
                ResetPassword_ERROR_CODES eErrorCode =
               (ResetPassword_ERROR_CODES)Enum.Parse(typeof(ResetPassword_ERROR_CODES), error);
                if (Enum.IsDefined(typeof(ResetPassword_ERROR_CODES), eErrorCode))
                {
                    isDefined = true;
                }
                else
                    isDefined = false;
            }
            catch
            {
                isDefined = false;
            }
            return isDefined;
        }
        #endregion
        #region Set HTML Controls Value
        void setHidQueryStringValue(string value)
        {
            hidQueryString.Value = value;
        }
        #endregion
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCSubAdminDetails.ascx.cs"
    Inherits="mFicientCP.UserControls.UCSubAdminDetails" %>
<div id="divSubAdminDetails" style="width: 100%;">
    <div class="modalPopUpDetails">
        <div id="DivSubAdminDetails" style="width: 100%;">
            <div id="divSubAdminDtlsHeader" class="innerHeader" style="border-bottom: 1px solid">
                <asp:Label ID="lblSubAdminDtlsHeader" runat="server" Text="Details"></asp:Label>
                <div id="divUCActivityButtons" style="float: right; margin-bottom: 2px;">
                    <asp:LinkButton ID="lnkEditSubAdmin" runat="server" OnClientClick="isCookieCleanUpRequired('false');" OnClick="lnkEditSubAdmin_Click">edit</asp:LinkButton><asp:Label
                        ID="ucSeparatorOne" runat="server" Text="|" style="margin:0 1px 0px 5px;" OnClientClick="isCookieCleanUpRequired('false');"></asp:Label>
                    <asp:LinkButton ID="lnkBlockSubAdmin" runat="server" OnClick="lnkBlockSubAdmin_Click"
                        OnClientClick="return showConfirmation('Are you sure you want to block this User')">block</asp:LinkButton>
                    <asp:LinkButton ID="lnkUnblockSubAdmin" runat="server" OnClick="lnkUnblockSubAdmin_Click"
                        OnClientClick="return showConfirmation('Are you sure you want to unblock this SubAdmin')">unblock</asp:LinkButton>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="singleSection" style="float: left; width: 100%;">
                <fieldset>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForName" runat="server" Text="Name"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label2" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblName" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForEmail" runat="server" Text="Email"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label3" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblEmail" runat="server"></asp:Label>
                      
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForUserName" runat="server" Text="Username"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label6" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblUserName" runat="server"></asp:Label>
                        </div>
                    </section>

                    <section>
                        <div class="column1">
                            <asp:Label ID="lblmappeduser" runat="server" Text="Mapped to User"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label9" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblmappedtheuser" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForAccountType" runat="server" Text="Account Type"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label18" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblAccountType" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForPushMessage" runat="server" Text="Push Message"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label7" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblPushMessage" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForFrmManager" runat="server" Text="Form Manager"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label5" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblFormManagement" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForUsrManager" runat="server" Text="User Manager"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label8" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblUserManagement" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section style="display:none">
                        <div class="column1">
                            <asp:Label ID="lblForDivisionDept" runat="server" Text="Division / Department"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label4" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <%--<asp:Label ID="lblDivisionDept" runat="server"></asp:Label>--%>
                            <asp:LinkButton ID="lnkDivisionDepartment" runat="server" Text="[view]" OnClientClick="isCookieCleanUpRequired('false');" OnClick="lnkDivisionDepartment_Click"></asp:LinkButton>
                        </div>
                    </section>
                    
                </fieldset>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidSubAdminId" runat="server" />
    <asp:HiddenField ID="hidAdminId" runat="server" />
    <asp:HiddenField ID="hidCompany" runat="server" />
</div>

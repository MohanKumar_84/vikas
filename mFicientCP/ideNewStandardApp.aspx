﻿<%@ Page Title="mStudio" Language="C#" MasterPageFile="~/master/IdeMaster.Master"
    AutoEventWireup="true" CodeBehind="ideNewStandardApp.aspx.cs" Inherits="mFicientCP.ideNewStandardApp"
    EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        window.paceOptions = {
            ajax: true, // disabled
            document: true, // disabled
            eventLag: true, // disabled 
            elements: true
        }
    </script>
    <link rel="stylesheet" href="css/IdeControls.css" />
    <link href="colorPicker/colpick.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/minified/pace.min.js" type="text/javascript"></script>
    <link href="../css/pace-theme-center-simple.css" rel="stylesheet" />
    <script src="ace/ace.js" type="text/javascript" charset="utf-8"></script>
    <script src="ace/ext-language_tools.js" type="text/javascript"></script>
    <script src="Scripts/mFicientIde2.js" type="text/javascript"></script>
    <script src="Scripts/mfIdeAppDesigner.js" type="text/javascript"></script>
    <script src="Scripts/IdeClasses2.js" type="text/javascript"></script>
    <script src="Scripts/MficientCntrlProps.js" type="text/javascript"></script>
    <script src="Scripts/mfIdeFormDesigner.js" type="text/javascript"></script>
    <script src="colorPicker/colpick.js" type="text/javascript"></script>
    <script src="Scripts/mfIdeSimpleControls.js" type="text/javascript"></script>
    <script src="Scripts/mfIdeSelectors.js" type="text/javascript"></script>
    <script src="Scripts/mfIdeAdvanceControls.js" type="text/javascript"></script>
    <script src="Scripts/mfIdeCharts.js" type="text/javascript"></script>
    <script src="Scripts/mfIdeEditableList.js" type="text/javascript"></script>
    <script src="Scripts/mfIdeBITable.js" type="text/javascript"></script>
    <script src="Scripts/mfIdeList.js" type="text/javascript"></script>
    <script src="Scripts/mfIdeButton.js" type="text/javascript"></script>
    <script src="Scripts/mfIdePicture.js" type="text/javascript"></script>
    <script src="Scripts/IdeNewFormDesigner.js" type="text/javascript"></script>
    <script src="Scripts/IdeChildFormDesigner.js" type="text/javascript"></script>
    <script type="text/javascript">
        var images = [
	'//enterprise.mficient.com/css/images/icons/dark/progress.gif',
	'//enterprise.mficient.com/images/addform.png',
	'//enterprise.mficient.com/images/open.png',
	'//enterprise.mficient.com/images/add1.png',
	'//enterprise.mficient.com/images/dialog_warning.png',
	'//enterprise.mficient.com/images/dialog_info.png',
	'//enterprise.mficient.com/images/dialog_error.png',
	'//enterprise.mficient.com/images/imgNext.png',
	'//enterprise.mficient.com/images/imgPrvs.png',
	'//enterprise.mficient.com/images/condionIcon.png',
	'//enterprise.mficient.com/images/groupbox.png',
	'//enterprise.mficient.com/images/label.png',
	'//enterprise.mficient.com/images/textbox.png',
	'//enterprise.mficient.com/images/select.png',
	'//enterprise.mficient.com/images/Checkbox.png',
	'//enterprise.mficient.com/images/image.png',
	'//enterprise.mficient.com/images/repeater.png',
	'//enterprise.mficient.com/images/hiddenField.png',
	'//enterprise.mficient.com/images/datetimePickericon.png',
	'//enterprise.mficient.com/images/ToggleIcon.png',
	'//enterprise.mficient.com/images/hyperlinkIcon.png',
	'//enterprise.mficient.com/images/sliderIcon.png',
	'//enterprise.mficient.com/images/barcodeIcon.png',
	'//enterprise.mficient.com/images/LocationIcon.png',
	'//enterprise.mficient.com/images/piecharticon.png',
	'//enterprise.mficient.com/images/bargraphicon.png',
	'//enterprise.mficient.com/images/lineChartIcon.png',
	'//enterprise.mficient.com/images/drilldownpiecharticon.png',
	'//enterprise.mficient.com/images/datetime.png',
	'//enterprise.mficient.com/images/UploadPhoto.png',
	'//enterprise.mficient.com/images/piechart.png',
	'//enterprise.mficient.com/images/bargarph.jpg',
	'//enterprise.mficient.com/images/lineChart.jpg',

	'//enterprise.mficient.com/images/shades_cont_top_lft.png',
	'//enterprise.mficient.com/images/shades_cont_top_rgt.png',
	'//enterprise.mficient.com/images/shades_cont_bot_lft.png',
	'//enterprise.mficient.com/images/shades_cont_bot_rgt.png',
    '//enterprise.mficient.com/images/shades_cont_top_bg.png',
    '//enterprise.mficient.com/images/shades_cont_bot_bg.png',
    '//enterprise.mficient.com/images/shades_cont_lft.png',
	'//enterprise.mficient.com/images/shades_cont_rgt.png',

	
	
	'//enterprise.mficient.com/images/T-B-ACTIVE.png',
	'//enterprise.mficient.com/images/T-B.png',
	'//enterprise.mficient.com/images/R-L-ACTIVE.png',
	'//enterprise.mficient.com/images/R-L.png',
	'//enterprise.mficient.com/images/TL-BR-1.png',
	'//enterprise.mficient.com/images/TL-BR-3.png',
	'//enterprise.mficient.com/images/TL-BR-ACTIVE-1.png',
	'//enterprise.mficient.com/images/TL-BR-ACTIVE-3.png',
	'//enterprise.mficient.com/images/TR-BL-0.png',
	'//enterprise.mficient.com/images/TR-BL-2.png',
	'//enterprise.mficient.com/images/TR-BL-ACTIVE-0.png',
	'//enterprise.mficient.com/images/TR-BL-ACTIVE-2.png',
	'//enterprise.mficient.com/images/TR-BL-0.png',
	'//enterprise.mficient.com/images/TR-BL-2.png',
	'//enterprise.mficient.com/images/TR-BL-ACTIVE-0.png',
	'//enterprise.mficient.com/images/TR-BL-ACTIVE-2.png',
	'//enterprise.mficient.com/images/T-B-T.png',
	'//enterprise.mficient.com/images/T-B-T-ACTIVE.png',


	'//enterprise.mficient.com/images/imgNext.png',
	'//enterprise.mficient.com/images/imgPrvs.png',
	'//enterprise.mficient.com/images/Tablet.png',
	'//enterprise.mficient.com/images/arrowup.png',
	'//enterprise.mficient.com/images/arrowdown.png',
	'//enterprise.mficient.com/images/collapse.png',
	'//enterprise.mficient.com/images/expand.png',
	'//enterprise.mficient.com/images/warning.png',
	'//enterprise.mficient.com/images/icon/GRAY0295.png',
	'//enterprise.mficient.com/images/icon/GRAY0161.png',
	'//enterprise.mficient.com/images/ImgList.png',
	'//enterprise.mficient.com/images/ImgToggle.png',
	'//enterprise.mficient.com/images/ImgDropdown.png',
	'//enterprise.mficient.com/images/ImgBarcode.png',
	'//enterprise.mficient.com/images/ImgCheckbox.png',
	'//enterprise.mficient.com/images/ImgDivider.png',
	'//enterprise.mficient.com/images/ImgHyperLink.png',
	'//enterprise.mficient.com/images/ImgRadioButton.png',
	'//enterprise.mficient.com/images/ImgScroll.png',
	'//enterprise.mficient.com/images/ImgScroll.png',
	'//enterprise.mficient.com/images/Table.jpg',
	'//enterprise.mficient.com/images/ImgDatetime.png',
	'//enterprise.mficient.com/images/ImgTextbox.png',
	'//enterprise.mficient.com/images/Location.png',
	'//enterprise.mficient.com/images/DefaultImage .png',
	'//enterprise.mficient.com/images/img_barcode.png',
	'//enterprise.mficient.com/images/AngularGaugeImage.png',
	'//enterprise.mficient.com/images/imageCylinderGauge.png',
	'//enterprise.mficient.com/images/Spacer.png',
	'//enterprise.mficient.com/images/Picture.png',
	'//enterprise.mficient.com/images/Scribble.png',
	'//enterprise.mficient.com/images/scribbleimage.png',
	'//enterprise.mficient.com/images/PictureImage.png',
	'//enterprise.mficient.com/images/save2.png',
	'//enterprise.mficient.com/images/saveas2.png',
	'//enterprise.mficient.com/images/NewWindow.png',
	'//enterprise.mficient.com/images/Commit.png',
	'//enterprise.mficient.com/images/AppDeleteIcon.png',
	'//enterprise.mficient.com/images/CloseApp.png',
	'//enterprise.mficient.com/images/sorting.png',
	'//enterprise.mficient.com/images/storyboardIcon.png',
	'//enterprise.mficient.com/images/spacerIcon.png',
	'//enterprise.mficient.com/images/pictureIcon.png',
	'//enterprise.mficient.com/images/scribbleicon.png',
	'//enterprise.mficient.com/images/iconAngularGauge.png',
	'//enterprise.mficient.com/images/iconCylinderGuage.png',
	'//enterprise.mficient.com/images/help.png',
	'//enterprise.mficient.com/images/CopyForm.png',
	'//enterprise.mficient.com/images/drilldownpiechart.png',
    '//enterprise.mficient.com/images/sliderIcon.png'
]

        var IMGPreload = new Image();
        images.forEach(function (img) {
            IMGPreload = new Image();
            IMGPreload.src = img;
        });

        var isDebugMode = false;
        var postBackByHtmlProcess = {
            "GetAppDetails": "1",
            "AppSave": "2",
            "AppCommit": "3",
            "AppCommitSave": "4",
            "AddNewApp": "5",
            "AppSaveAs": "6",
            "FormCreateCopy": "7",
            "TransferApp": "8",
            "DeleteApp": "9",
            "TesterAppCommit": "10",
            "TesterAppCommitSave": "11",
            "NewHtml5FileAdd": "12",
            "Html5ZipFileAdd": "13",
            "ViewHtml5AppFile": "14",
            "Html5AppFileEditSave": "15",
            "AddNewHtml5App": "16",
            "UpdateHtml5App": "17",
            "Html5AppSaveAs": "18",
            "Html5AppCommit": "19",
            "Html5AppCommitSave": "20",
            "Html5AppTesterCommit": "21",
            "Html5AppDelete": "22",
            "Html5AppClose": "23",
            "Html5AppFileMove": "24",
            "Html5AppZipDownload": "25",
            "Html5AppAddNewFolder": "26",
            "Html5AppTreeViewRefresh": "27",
            "Html5AppFileDelete": "28",
            "SaveAndCloseApp": "29",
            "GetArchivedApp": "30",
            "SaveArchivedAppAsWorkingCopy": "31"
        };
        var APP_LIST_SOURCE = {
            current: "0",
            archived: "1"
        };
        Pace.on("start", function () {
            $('<div class="paceLoaderCover"></div>').appendTo('body').show();
        });
        Pace.on("done", function () {
            $(".paceLoaderCover").fadeOut(2000, function () {
                $(".paceLoaderCover").remove();
            });
            doIfAllScriptsNotLoaded();
        });
        $(document).ready(function () {
            $("#drpCntrlEvents").uniform();
            $("#drpFormEvents").uniform();
            $("#divToolboxHeadDiv").click(function () {
                $(".WfmainOuterToolboxDiv").hide("fast", function () {
                    $("#divCompleteToolbox").hide();
                    $(".WfmainOuterToolboxDiv").width("20px");
                    $(".WfmainOuterToolboxDiv").show();
                    $('#divMinimisedToolbox').show();
                    $("#WF_mainFormbox").width("78.3%");
                    $("#Wf_FormDesigner").width("98.4%");
                });
            });
            $("#divMinimisedToolbox").click(function () {
                $("#divMinimisedToolbox").hide("fast", function () {
                    $("#divCompleteToolbox").show();
                    $(".WfmainOuterToolboxDiv").show();
                    $(".WfmainOuterToolboxDiv").width("12%");
                    $("#WF_mainFormbox").width("67.8%");
                    $("#Wf_FormDesigner").width("87.98%");
                });
            });
            //ImageFolderPath = "//enterprise.mficient.com/CompanyImages/" + $('[id$=hdfCompanyId]').val() + "/mediafiles/";
            ImageFolderPath = "https://s3-ap-southeast-1.amazonaws.com/mficient-images/companyImages/" + $('[id$=hdfCompanyId]').val() + "/mediaFiles/";
            if (isDebugMode === true) {
                $('#divDebug').show();
            }
            else {
                $('#divDebug').hide();
            }

            $('#divHtml5FilesTreeView').resizable({
                handles: "e",
                maxWidth: 350,
                minWidth: 200,
                stop: function (event, ui) {
                    resizeHtml5Ide();
                },
                resize: function (event, ui) {
                    resizeHtml5Ide();
                }
            });
        });
        function bindAppsListTable(source) {
            if (mfUtil.isNullOrUndefined(source)
                || mfUtil.isEmptyString(source)
                || source === APP_LIST_SOURCE.current) {

                _bindCurrentAppListTable();
            }
            else {
                _bindArchivedAppListTable();
            }
        }
        function _bindCurrentAppListTable() {
            var selectdataset = $("[id$='hidAppsList']").val();
            if (selectdataset.length > 0) {
                $('#divTblApps').html('<table cellpadding="0" cellspacing="0" border="0" class="display"  id="tblApps"></table>');
                $('#tblApps').dataTable({
                    "data": jQuery.parseJSON(selectdataset),
                    "pageLength": 10,
                    "lengthMenu": [[10, 20, 25, 30, 45, 55, -1], [10, 20, 25, 30, 45, 55, "All"]],
                    "columns": [{ "id": "id" }, { "title": "App", "width": "25%" }, { "title": "Type", "width": "5%" }, { "title": "Description", "width": "35%" }, { "title": "Date Modified", "width": "15%" }, { "title": "", "width": "10%"}],
                    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        var imgTag = '';
                        //if (aData[5] == "0") imgTag = '<img src="images/phone_16.png" class="tinted-image" />';
                        //else if (aData[5] == "1") imgTag = '<img src="images/tablet_16.png" class="tinted-image" />';
                        //else imgTag = '<img src="images/phone_16.png" /><img src="images/tablet_16.png" class="tinted-image" />';
                        if (aData[5] == "0") {
                            if (aData[7] === 1) {
                                imgTag = '<i class="fa fa-mobile" style="color:#05E805;font-size:22px;"></i>';
                            }
                            else {
                                imgTag = '<i class="fa fa-mobile" style="font-size:22px;color:#6F6F6F;"></i>';
                            }
                        }
                        else if (aData[5] == "1") {
                            if (aData[7] === 1) {
                                imgTag = '<i class="fa fa-tablet" style="color:#05E805;font-size:25px;"></i>';
                            }
                            else {
                                imgTag = '<i class="fa fa-tablet" style="font-size:25px;color:#6F6F6F;"></i>';
                            }
                        }
                        else {
                            if (aData[7] === 1) {
                                imgTag = '<i class="fa fa-mobile" style="font-size:22px;color:#05E805;"></i><i class="fa fa-tablet" style="margin-left:3px;color:#05E805;font-size:25px;"></i>';
                            }
                            else {
                                imgTag = '<i class="fa fa-mobile" style="font-size:22px;color:#6F6F6F;"></i><i class="fa fa-tablet" style="margin-left:3px;font-size:25px;color:#6F6F6F;"></i>';
                            }
                        }
                        $('td:eq(4)', nRow).html(imgTag);
                        $('td:eq(0)', nRow).html('<img style="width:16px" src="https://enterprise.mficient.com/images/icon/' + aData[6] + '" /><span style="margin-left:5px;position:relative;top:-4px;">' + aData[1] + '</span>');
                        //if (aData[7] === 1) {
                        // $(nRow).addClass("highlightBold");
                        // }
                        //else {
                        //$(nRow).removeClass("highlightBold");
                        //}
                        if (aData[3] && typeof aData[3] === "string" && aData[3].length > 20) {
                            $('td:eq(2)', nRow).html(aData[3].substring(0, 20) + '...');
                            $('td:eq(2)', nRow).attr("title", aData[3]);
                        }
                        else {
                            $('td:eq(2)', nRow).html(aData[3]);
                            $('td:eq(2)', nRow).removeAttr("title");
                        }
                        return nRow;
                    },
                    "columnDefs": [
                        {
                            "render": function (data, type, row) {
                                return data + ' (' + row[0] + ')';
                            }, "targets": 0
                        },
			            { "visible": false, "targets": [0] },
                        { "width": "50%", "targets": 2 }
		            ]
                });
                var table = $('#tblApps').DataTable();
                searchText("tblApps");
                $('#tblApps tbody').on('click', 'tr', function () {
                    if ($(this).hasClass('selected')) {
                        $(this).removeClass('selected');
                    }
                    else {
                        table.$('tr.selected').removeClass('selected');
                        //$(this).addClass('selected');
                        $(this).removeClass('selected');
                    }
                    var rowData = table.row(this).data();
                    if (rowData != undefined) {
                        //alert(rowData[0]);
                        var data = [];
                        data.push(rowData[0]);
                        processPostbackByHtmlCntrl.postBack(postBackByHtmlProcess.GetAppDetails,
                           data, "");
                    }
                    else {
                        //alert('No Row Data Selected');
                    }
                });
            }
        }
        function _bindArchivedAppListTable() {
            var $hidAppList = $("[id$='hidAppsList']");
            var strAppListJson = $hidAppList.val();
            var aryApps = [];
            if (strAppListJson) {
                aryApps = $.parseJSON(strAppListJson);
            }
            var table = $('#tblApps').DataTable();
            if (table) {
                table.destroy();
            }
            $('#divTblApps').html('').html('<table cellpadding="0" cellspacing="0" border="0" class="display"  id="tblApps"><thead><tr><th>Name</th><th class="ClsAppType">Type</th><th class="ClsVersions">Versions</th><th class="ClsModelTypeIcon"></th></tr></thead><tbody></tbody></table>');
            table = $('#tblApps').DataTable({
                dom: 'T<"clear">lfrtip',
                tableTools: {
                    "aButtons": []
                },
                data: aryApps,
                "pageLength": 10,
                lengthMenu: [[20, 25, 30, 45, 55, -1], [20, 25, 30, 45, 55, "All"]],
                columns: [
                    { data: 'Name' },
                    { data: 'Type' },
                    { data: 'Versions' },
                    { data: 'ModelTypes' }
                ],
                "columnDefs": [
                    {
                        "targets": "ClsVersions",
                        "createdCell": function (td, cellData, rowData, row, col) {
                            var aryVersions = [],
                                strOptionsForDdl = "";
                            if (typeof cellData === "string" && !mfUtil.isEmptyString(cellData)) {
                                aryVersions = cellData.split(',');
                                aryVersions = aryVersions.reverse();
                                for (var i = 0; i <= aryVersions.length - 1; i++) {
                                    if (mfUtil.isEmptyString(strOptionsForDdl)) {
                                        strOptionsForDdl = '<option value=' + aryVersions[i] + '>' + aryVersions[i] + '</option>';
                                    }
                                    else {
                                        strOptionsForDdl += '<option value=' + aryVersions[i] + '>' + aryVersions[i] + '</option>';
                                    }
                                }
                                $(td).html('<select class="ClsDdlVersions">' + strOptionsForDdl + '</select>');
                                $(td).find('.ClsDdlVersions').on('click', function (event) {
                                    event.stopPropagation();
                                });
                            }
                            else {// this case should never come.
                                $(td).html('No committed versions');
                            }
                        }
                    },
                    {
                        "targets": "ClsModelTypeIcon",
                        "createdCell": function (td, cellData, rowData, row, col) {
                            var imgTag = '',
                                aryModelTypes = [];
                            aryModelTypes = cellData.split(',');
                            if (aryModelTypes.length === 1) {
                                if (aryModelTypes[0] === "0") {
                                    imgTag = '<i class="fa fa-mobile" style="font-size:22px;color:#05E805;"></i>';
                                }
                                else {
                                    imgTag = '<i class="fa fa-tablet" style="font-size:25px;color:#05E805;"></i>';
                                }
                            }
                            else if (aryModelTypes.length === 2) {
                                imgTag = '<i class="fa fa-mobile" style="font-size:22px;color:#05E805;"></i><i class="fa fa-tablet" style="margin-left:3px;font-size:25px;color:#05E805;"></i>';
                            }

                            $(td).html(imgTag);
                        }
                    }
                ],
                "createdRow": function (row, data, index) {

                }
            });
            searchText("tblApps");
            $('#tblApps tbody').off('click', 'tr');
            $('#tblApps tbody').on('click', 'tr', function () {
                var $self = $(this),
                    trData = table.row(this).data(),
                    $ddlVersion = $([]);

                if ($self.hasClass('selected')) {
                    $self.removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $self.removeClass('selected');
                }

                if (trData != undefined) {
                    var data = [];
                    $ddlVersion = $self.find('.ClsDdlVersions');
                    data.push(trData.Id);
                    if ($ddlVersion.length > 0) {
                        data.push($ddlVersion.val());
                    }
                    else {
                        data.push("0.00");
                    }
                    processPostbackByHtmlCntrl.postBack(postBackByHtmlProcess.GetArchivedApp,
                           data, "");
                }
                else {
                }
            });
        }
        function UpdateUpdBtnRowIndex() {
            $('[id$=<%= btnDbConnRowIndex.ClientID %>]').click();
        }

        function btnImportClick() {
            SubProcIframe(false);
            $('[id$=<%= btnTestClick.ClientID %>]').click();
        }
        function resizeHtml5Ide() {

            var iIdeContWidth = Math.floor($('#divHtml5IdeCont').outerWidth()),
                $propsCont = $('#divHtml5PropsContainer'),
                iPropsContWidth = Math.floor(0.159 * iIdeContWidth),
                $Html5FilesTreeView = $('#divHtml5FilesTreeView'),
                iHtml5FilesTreeViewWidth = $Html5FilesTreeView.outerWidth(),

                iTreeViewSizerWidth = $('#TreeViewSizer').outerWidth(),
                $TreeViewToolbar = $('#divHtml5TreeViewToolbar'),
                $Html5Canvas = $('#divHtml5Canvas');

            $propsCont.width(iPropsContWidth - 1);
            $('#divTreeCanvasCont').width(iIdeContWidth - iPropsContWidth);
            $('#TreeViewSizer').width(iTreeViewSizerWidth);
            $TreeViewToolbar.width(iHtml5FilesTreeViewWidth - iTreeViewSizerWidth);
            $Html5Canvas.width(iIdeContWidth - iPropsContWidth - iHtml5FilesTreeViewWidth);
        }

        function presetHtml5IdeSize() {

            var iIdeContWidth = Math.floor($('#divHtml5IdeCont').outerWidth()),
                $propsCont = $('#divHtml5PropsContainer'),
                iPropsContWidth = Math.floor(0.159 * iIdeContWidth),
                $Html5FilesTreeView = $('#divHtml5FilesTreeView'),
                iHtml5FilesTreeViewWidth = 200,

                iTreeViewSizerWidth = $('#TreeViewSizer').outerWidth(),
                $TreeViewToolbar = $('#divHtml5TreeViewToolbar'),
                $Html5Canvas = $('#divHtml5Canvas');

            $Html5FilesTreeView.width(iHtml5FilesTreeViewWidth);
            $propsCont.width(iPropsContWidth - 1);
            $('#divTreeCanvasCont').width(iIdeContWidth - iPropsContWidth);
            $('#TreeViewSizer').width(iTreeViewSizerWidth);
            $TreeViewToolbar.width(iHtml5FilesTreeViewWidth - iTreeViewSizerWidth);
            $Html5Canvas.width(iIdeContWidth - iPropsContWidth - iHtml5FilesTreeViewWidth);
        }
    </script>
    <style type="text/css">
        .selectUniformCss
        {
            position: relative;
            padding-left: 10px;
            display: inline-block;
            zoom: 1;
            font-weight: bold;
            color: #464545;
            background-position: -483px -160px;
            line-height: 32px;
            height: 32px;
            background-image: url(css/themes/images/uniform/sprite.png);
            background-repeat: no-repeat;
            margin: 0;
            padding: 0;
        }
        #aceeditor
        {
            margin: 0;
            position: absolute;
            top: 0;
            bottom: 25px;
            left: 0;
            right: 0;
        }
        #acestatusBar
        {
            margin: 0;
            padding: 0;
            position: absolute;
            left: 0;
            right: 0;
            bottom: 0;
            height: 25px;
            background-color: rgb(245, 245, 245);
            color: gray;
        }
        #jsAceEditor
        {
            width: 100%;
            height: 520px;
            margin-left: -3px;
        }
        #jsAceStatusBar
        {
            margin: 0;
            padding: 0;
            position: relative;
            left: 0;
            right: 0;
            bottom: 0;
            height: 14px;
            background-color: rgb(245, 245, 245);
            color: gray;
            top: -14px;
        }
        #jsCntrlonChngAceEditor
        {
            height: 470px;
        }
        #drpFormEvents, #drpCntrlEvents
        {
            height: 22px;
        }
        #jsCntrlonChngAceStatusBar
        {
            margin: 0;
            padding: 0;
            position: relative;
            left: 0;
            right: 0;
            bottom: 0;
            height: 14px;
            background-color: rgb(245, 245, 245);
            color: gray;
            top: -14px;
        }
        .ace_status-indicator
        {
            color: gray;
            position: absolute;
            right: 0;
            border-left: 1px solid;
        }
        table.dataTable.hover tbody tr:hover, table.dataTable.hover tbody tr.odd:hover, table.dataTable.hover tbody tr.even:hover, table.dataTable.display tbody tr:hover, table.dataTable.display tbody tr.odd:hover, table.dataTable.display tbody tr.even:hover
        {
            color: Blue;
        }
        
        table.dataTable.hover tbody tr.odd:hover, table.dataTable.display tbody tr.odd:hover
        {
            background-color: whitesmoke;
        }
        
        table.dataTable.hover tbody tr.even:hover, table.dataTable.display tbody tr.even:hover
        {
            background-color: #FAFAFA;
        }
        table.dataTable.display tbody tr:hover > .sorting_1, table.dataTable.display tbody tr.odd:hover > .sorting_1, table.dataTable.order-column.hover tbody tr.odd:hover > .sorting_1
        {
            background-color: #F1F1F1;
        }
        table.dataTable.display tbody tr:hover > .sorting_1, table.dataTable.display tbody tr.even:hover > .sorting_1, table.dataTable.order-column.hover tbody tr.even:hover > .sorting_1
        {
            background-color: #fcfcfc;
        }
        [id$="rdbAppCommitMinor"] + label, [id$="rdbAppCommitMajor"] + label, [id$="chkPublishApp"] + label
        {
            position: relative;
            top: -2px;
        }
        .click-nav ul.menuList li.ui-menu-item.ui-state-focus
        {
            border: none;
            background: none;
            font-weight: inherit;
            color: inherit;
        }
        .click-nav ul.menuList li.ui-menu-item.ui-state-focus > a
        {
            border: solid 1px #aecff7 !important;
            background-color: #E8F1FC !important;
            cursor: pointer;
            background-image: none;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
        }
        .click-nav ul.menuList li.ui-menu-item
        {
            width: 97%;
        }
        .click-nav ul.menuList li.ui-menu-item.ui-state-active
        {
            border: 0px;
            background: none;
            font-weight: normal;
            color: #3f3f3f;
        }
        #tblApps thead tr th
        {
            text-shadow: 0px 1px 1px rgba(255, 255, 255, 0.70);
        }
        .ui-tabs .ui-state-default.ui-tabs-active
        {
            background: #ffffff url(images/ui-bg_flat_90_ffffff_40x100.png) 50% 50% repeat-x;
        }
        .ui-tabs .ui-state-default
        {
            background: #eeeeee url(images/ui-bg_flat_90_eeeeee_40x100.png) 50% 50% repeat-x;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainCanvas" runat="server">
    <div id="divWorkflowListContainer" class="fl p15p w_98 hide" style="margin-top: 15px;">
        <div id="divTblApps" class="datalistContainer">
        </div>
    </div>
    <div id="ProcessWorkflowDiv" class="workflowContainer hide">
        <div>
            <div id="ToolboxHeadOuterDiv" class="ToolboxHeadOuterDiv">
                <div id="WfApplNameHeaderDiv" class="wfToolboxHeadDiv hide">
                </div>
                <div id="WfToolboxHeadDiv" class="WfToolboxHead2Div">
                    <div>
                        <div class="fl formMenu" style="width: 80%;" id="divCurrentAppMainDesignerMenu">
                            <ul class="nav" style="padding-bottom: 0px;">
                                <li><a id="WfAddViewDiv" onclick="SubProcAddNewForm();"><i class="icon-file "></i>Add
                                    Form</a> </li>
                                <li><a id="WfSaveDiv"><i class="icon-save"></i>Save</a> </li>
                                <li><a id="WfSaveAsDiv"><i class="icon-copy"></i>Save As</a> </li>
                                <li><a><i class="icon-globe"></i>Commit</a>
                                    <ul>
                                        <li><a id="WfCommitDiv"><i class="icon-angle-right"></i>Production</a></li>
                                        <li><a id="WfCommitTesterDiv"><i class="icon-angle-right"></i>Tester</a></li>
                                    </ul>
                                </li>
                                <li><a id="WfTransferAppDiv"><i class="icon-exchange"></i>Transfer</a></li>
                                <li><a id="WfDeleteDiv"><i class="icon-trash"></i>Delete</a> </li>
                                <li><a id="WfCloseAppDiv"><i class="icon-signout"></i>Close</a></li>
                            </ul>
                        </div>
                        <div class="fl formMenu hide" style="width: 80%;" id="divArchivedAppMainDesignerMenu">
                            <ul class="nav" style="padding-bottom: 0px;">
                                <li><a id="lnkWfArchivedAppSaveAsWorking"><i class="icon-save"></i>Save As Working</a>
                                </li>
                                <li><a id="lnkWfCloseArchivedApp"><i class="icon-signout"></i>Close</a></li>
                            </ul>
                        </div>
                        <div id="divMainMenuAppSwitchCont" style="float: right;">
                            <select id="ddlMainMenuAppSwitch" onkeydown="return enterKeyFilter(event);">
                                <option value="0">Phone</option>
                                <option value="1">Tablet</option>
                            </select>
                        </div>
                        <!--<div id="WfPhoneTypeMenu" style="float: right;" class="formDesignerMenuInnerDiv formMenuMouseOut"
                            title="Phone Interface">
                            <div class="formDesignerMenuImgDiv">
                                <img id="img1" alt="" src="//enterprise.mficient.com/images/add1.png" title="New Workflow" />
                                <span class="phone16by16 fl"></span>
                            </div>
                            <div class="formDesignerMenuTextDiv">
                                <a class="formDesignerMenuTexta">Phone</a>
                            </div>
                        </div>
                        <div id="WfSep1" class="formDesignerMenuSepDiv" style="float: right;">
                        </div>-->
                        <!--<div id="WfTabletTypeMenu" style="float: right;" class="formDesignerMenuInnerDiv formMenuMouseOut"
                            title="Tablet Interface">
                            <div class="formDesignerMenuImgDiv">
                                <!--<img id="img7" alt="" src="//enterprise.mficient.com/images/open.png" title="Open" />
                                <span class="tablet16by16 fl"></span>
                            </div>
                            <div class="formDesignerMenuTextDiv">
                                <a class="formDesignerMenuTexta">Tablet</a>
                            </div>
                        </div>
                        <div id="WfSep2" class="formDesignerMenuSepDiv" style="float: right;">
                        </div>-->
                        <div style="clear: both;">
                        </div>
                    </div>
                </div>
                <div id="FormToolboxHeadDiv" style="display: none; width: 100%; float: left;">
                    <div id="ToolboxHeadDiv" class="FormToolboxDiv">
                        <div class="FormDesToolboxHeadDiv hide">
                            <div class="HeadInnerDiv">
                                Toolbox</div>
                        </div>
                        <div class="FormDesMainContHeadDiv">
                            <div class="fl w_100 formMenu" id="divCurrentAppFormMenu">
                                <ul class="nav" style="padding-bottom: 0px;">
                                    <li><a id="FromSaveDiv" onclick="SaveCurrentForm(false);"><i class="icon-save"></i>Save</a>
                                    </li>
                                    <li><a id="FromSaveAsDiv" onclick="SaveAsCurrentForm();"><i class="icon-copy"></i>Save
                                        As</a> </li>
                                    <li><a id="FormDeleteDiv" onclick="deleteForm();"><i class="icon-trash"></i>Delete</a>
                                    </li>
                                    <li><a id="FormCloseDiv" onclick="closeCurrentForm();"><i class="icon-signout"></i>Close</a></li>
                                </ul>
                            </div>
                            <div class="fl w_100 formMenu hide" id="divArchivedAppFormMenu">
                                <ul class="nav" style="padding-bottom: 0px;">
                                    <li><a id="lnkArchivedAppFormClose"><i class="icon-signout"></i>Close</a></li>
                                </ul>
                            </div>
                            <%--<div class="MenuHeadInnerDiv">
                                <div id="FromSaveDiv" class="formDesignerMenuInnerDiv formMenuMouseOut" onclick="SaveCurrentForm(false);">
                                    <div class="formDesignerMenuImgDiv">
                                        <img alt="Close" src="//enterprise.mficient.com/images/save2.png" />
                                    </div>
                                    <div class="formDesignerMenuTextDiv">
                                        <a class="formDesignerMenuTexta">Save</a>
                                    </div>
                                </div>
                                <div id="formSaveAsSep" class="formDesignerMenuSepDiv">
                                </div>
                                <div id="FromSaveAsDiv" class="formDesignerMenuInnerDiv formMenuMouseOut" onclick="SaveAsCurrentForm();">
                                    <div class="formDesignerMenuImgDiv">
                                        <img alt="Close" src="//enterprise.mficient.com/images/saveas2.png" />
                                    </div>
                                    <div class="formDesignerMenuTextDiv">
                                        <a class="formDesignerMenuTexta">Save As</a>
                                    </div>
                                </div>
                                <div id="frmSep1" class="formDesignerMenuSepDiv">
                                </div>
                                <div id="FormDeleteDiv" class="formDesignerMenuInnerDiv formMenuMouseOut" onclick="deleteForm();">
                                    <div class="formDesignerMenuImgDiv">
                                        <img alt="Close" src="//enterprise.mficient.com/images/AppDeleteIcon.png" />
                                    </div>
                                    <div class="formDesignerMenuTextDiv">
                                        <a class="formDesignerMenuTexta">Delete Form</a>
                                    </div>
                                </div>
                                <div id="frmSep2" class="formDesignerMenuSepDiv">
                                </div>
                                <div id="FormCloseDiv" class="formDesignerMenuInnerDiv formMenuMouseOut" onclick="closeCurrentForm();">
                                    <div class="formDesignerMenuImgDiv">
                                        <img alt="Close" src="//enterprise.mficient.com/images/CloseApp.png" />
                                    </div>
                                    <div class="formDesignerMenuTextDiv">
                                        <a class="formDesignerMenuTexta">Close Form</a>
                                    </div>
                                </div>
                                <div id="frmSep3" class="formDesignerMenuSepDiv">
                                </div>--%>
                            <div style="clear: both;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both">
        </div>
        <div id="WfMainContainerDiv" class="WfMainContainerDiv">
            <div class="WfmainOuterToolboxDiv">
                <div id="divCompleteToolbox">
                    <div id="divToolboxHeadDiv" class="ToolboxHeadDiv">
                        <div id="Div9" class="HeadInnerDiv FLeft DefaultCursor" style="font-size: 12px; font-weight: bold">
                            Forms
                        </div>
                        <div style="float: right; margin-top: 2px; margin-right: 20%">
                            <img class="handle hide" alt="move" src="//enterprise.mficient.com/images/sorting.png"
                                onclick="ApplictionAddNewView();" />
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div id="WF_ToolboxDiv" class="WfmainToolboxDiv">
                        <%--<div id="WfmasterDiv" style="clear: both;" class="draggableElement" ondblclick="ApplictionMasterDivClick();">
                                <img id="Form " alt="Label" src="//enterprise.mficient.com/images/masterPageIcon.png"
                                    class="ToolboxImage" />
                                <a id="aAppMasterName" class="ToolboxMasterInner">Master</a>
                                <asp:HiddenField ID="hdfAppMAsterId" runat="server" Value="-1" />
                            </div>
                            <div class="clear">
                            </div>
                            <div id="toolSep1" class="node WftoolboxSeprator">
                            </div>
                            <div class="clear">
                            </div>--%>
                        <div id="Wf_ConditionBox">
                        </div>
                        <div class="clear">
                        </div>
                        <div id="WF_ControlsContainer" style="clear: both; height: 510px; float: left; width: 100%;
                            overflow-y: scroll;">
                        </div>
                        <div class="clear">
                        </div>
                        <div id="WF_TBControlsContainer" style="clear: both; float: left; width: 100%; overflow-y: scroll;">
                        </div>
                    </div>
                    <div class="ToolboxHeadDiv hide">
                        <div id="Div6" class="HeadInnerDiv FLeft">
                            App Flow
                        </div>
                        <div style="float: right; margin-top: 2px; margin-right: 20%">
                            <img class="hide" alt="move" src="//enterprise.mficient.com/images/sorting.png" />
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div id="Div7" class="WfmainToolboxDiv" style="height: 222px; display: none;">
                        <div id="MainAppDiv" style="clear: both;" class="draggableElement">
                            <img id="Img4" alt="Label" src="//enterprise.mficient.com/images/storyboardIcon.png"
                                class="ToolboxImage" />
                            <a class="ToolboxInner">Main App Flow</a> <a id="MainAppId" class="hide">Main</a>
                        </div>
                        <div class="clear">
                        </div>
                        <div id="WfChildAppContDiv" class="hide" style="width: 100%;">
                        </div>
                    </div>
                </div>
                <div id="divMinimisedToolbox" style="width: 5px; float: left; height: 30px; -webkit-transform: rotate(90deg);
                    transform: rotate(90deg); letter-spacing: 2px; display: none;">
                    <p style="font-weight: bold">
                        Toolbox</p>
                </div>
            </div>
            <div id="WF_mainPropertiesbox" class="WfmainPropertiesboxDiv">
                <div class="WfOuterPropDiv">
                    <div id="WF_FormPropetiesDiv" class="WFCtrlPropetiesDiv" style="max-height: 160px;">
                        <div class="ToolboxHeadDiv">
                            <div id="Div18" class="HeadInnerDiv DefaultCursor">
                                App Properties
                            </div>
                        </div>
                        <div id="WF_FrmInnerPropertieDiv">
                            <div id="AppPropDiv_Name" class="InnerPropertieDiv" style="border-top: 0px;" onclick="ControlPropOuterClick(this);">
                                <div class="propertyName">
                                    Name</div>
                                <div class="propertyValue">
                                    <asp:TextBox ID="txtWfName" runat="server" class="txtProp" onchange="AppNameTextChange(this);"
                                        onkeydown="return enterKeyFilter(event);"></asp:TextBox>
                                </div>
                            </div>
                            <div id="AppPropDiv_Des" class="InnerPropertieDiv" onclick="ControlPropOuterClick(this);">
                                <div id="Div82" class="propertyName">
                                    App Description</div>
                                <div class="propertyValue">
                                    <input id="txtWorlFlow" type="text" class=" txtProp" readonly="readonly" onkeydown="return enterKeyFilter(event);" /></div>
                            </div>
                            <div id="txtaWorlFlowDiv" class="InnerPropertieDiv" style="display: none;">
                                <asp:TextBox ID="txtaWorlFlow" runat="server" TextMode="MultiLine" Columns="30" Width="98%"
                                    MaxLength="30" Rows="2" onkeydown="return enterKeyLinefeed(this.id, event);"></asp:TextBox>
                            </div>
                            <div id="AppPropDiv_Tablet" class="InnerPropertieDiv" onclick="ControlPropOuterClick(this);">
                                <div class="propertyName">
                                    Runs On</div>
                                <div class="propertyValue">
                                    <select id="ddlAppTabletOnly" class="ddlProp" onchange="AppTabletOnlyOnChange(this);"
                                        onkeydown="return enterKeyFilter(event);">
                                        <option value="0">Phone & Tablet</option>
                                        <option value="1">Tablet Only</option>
                                    </select>
                                </div>
                            </div>
                            <div id="AppPropDiv_Type" class="InnerPropertieDiv" onclick="ControlPropOuterClick(this);">
                                <div class="propertyName">
                                    Diff Interface For Tablet</div>
                                <div class="propertyValue">
                                    <select id="ddlAppInterface" class="ddlProp" onchange="AppInterfaceOnChange(this);"
                                        onkeydown="return enterKeyFilter(event);">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                            </div>
                            <div id="AppPropDiv_AppFor" class="InnerPropertieDiv" onclick="ControlPropOuterClick(this);">
                                <div class="propertyName">
                                    Design Interface</div>
                                <div class="propertyValue">
                                    <asp:TextBox ID="txtWfAppFor" runat="server" class="txtProp" ReadOnly="true" onkeydown="return enterKeyFilter(event);"></asp:TextBox>
                                </div>
                            </div>
                            <div id="AppPropDiv_StartUp" class="InnerPropertieDiv" onclick="ControlPropOuterClick(this);">
                                <div class="propertyName">
                                    StartUp Form</div>
                                <div class="propertyValue">
                                    <select id="ddlStartUpForm" class="ddlProp" onchange="ddlStartUpFormOnClick();" onkeydown="return enterKeyFilter(event);">
                                        <option value="-1">Select StartUp Form</option>
                                    </select>
                                </div>
                            </div>
                            <%--//MOHAN18/3/2014--%>
                            <div id="AppPropDiv_APP_Icon" class="InnerPropertieDiv" onclick="ControlPropOuterClick(this);">
                                <div class="propertyName">
                                    App Icon</div>
                                <div class="propertyValue">
                                    <img alt="" class="MenuIconHead" src="" id="imgProcessIcon" style="margin: 2px;" />
                                    <input id="btnChangeAppIconView" type="button" value="..." class="btnProp" style="margin: 0px 0px 0px 3px"
                                        onclick="ShowAppMenuIcon();" />
                                </div>
                            </div>
                            <div class="InnerPropertieDiv">
                            </div>
                        </div>
                    </div>
                    <div class="ToolboxHeadDiv">
                        <div id="WfFrmPropDiv" class="HeadInnerDiv DefaultCursor">
                            View Properties
                        </div>
                    </div>
                    <div id="WF_MainControlPropetiesDiv" class="WFCtrlPropetiesDiv" style="max-height: 380px;
                        overflow: auto;">
                        <div id="WF_ControlPropetiesDiv">
                        </div>
                    </div>
                </div>
                <div id="WfHelpDiv" class="PropertiesHelp">
                    <div>
                        <div style="float: right;">
                            <img src="//enterprise.mficient.com/images/help.png" alt="" />
                        </div>
                    </div>
                    <div id="WF_PropertiesHelpDiv">
                        <div class="HelpHeadDiv">
                        </div>
                        <div class="HelpContentDiv">
                        </div>
                    </div>
                </div>
            </div>
            <div id="WF_mainFormbox" class="WfmainFormboxDiv">
                <div id="WF_FormDesignContainerDiv" class="wfformDesignDiv">
                </div>
            </div>
            <%--<div id="WF_StyleFormContentDiv" style="display: block">
                       <div id="WF_OuterFormDesignContainerDiv" style="height: 615px; width: 100%; border-width: 0px;
                            border-style: solid; border-color: Gray; overflow-y: scroll; overflow-x: scroll;">
                          </div>
                           
                    </div>--%>
            <div id="Wf_FormDesigner" style="width: 87.98%; display: none; float: left;">
                <div id="FormDesignerDiv" style="display: none;">
                    <div style="clear: both">
                    </div>
                    <div class="mainContainerDiv">
                        <div id="ToolboxDiv" class="mainToolboxDiv">
                            <div class="ToolboxHeadDiv">
                                <div id="Div27" class="HeadInnerDiv FLeft DefaultCursor" style="font-size: 12px;
                                    font-weight: bold">
                                    Toolbox
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div style="margin-top: 5px;">
                            </div>
                            <div id="ControlsContainer" style="clear: both; max-height: 575px; overflow-y: auto;">
                                <div id="draggableGroupboxDiv" class="draggableGroupboxDiv parentDivDraggable" title="Panel&#13;HTML Fragment&#13;Display a frame around of controls.">
                                    <div style="float: left;">
                                        <img id="Groupbox" alt="GroupBox" src="//enterprise.mficient.com/images/groupbox.png"
                                            class="ToolboxImage" /></div>
                                    <div style="float: left; margin-top: 2px;">
                                        Section</div>
                                </div>
                                <div id="draggableLabelDiv" class="draggableElement parentDivDraggable" title="Label&#13;HTML Fragment&#13;">
                                    <div style="float: left;">
                                        <img id="Label" alt="Label" src="//enterprise.mficient.com/images/label.png" class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Label
                                    </div>
                                </div>
                                <div id="draggableTextboxDiv" class="draggableElement parentDivDraggable" title="Textbox&#13;HTML Fragment&#13;Enable user to enter text.">
                                    <div style="float: left;">
                                        <img id="changeText" alt="Textbox" src="//enterprise.mficient.com/images/textbox.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Textbox
                                    </div>
                                </div>
                                <div id="draggableSubmitButtonDiv" class="draggableElement parentDivDraggable" title="Input Button&#13;HTML Fragment">
                                    <div style="float: left;">
                                        <img id="Button" alt="Submit Button" src="//enterprise.mficient.com/images/InputButton.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Button
                                    </div>
                                </div>
                                <div id="draggableTextareaDiv" class="draggableElement hide parentDivDraggable" title="Textarea&#13;HTML Fragment&#13;Enable user to enter text&#13;in multiline.">
                                    <div style="float: left;">
                                        <img id="Textarea" alt="Textarea" src="//enterprise.mficient.com/images/textarea.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Textarea
                                    </div>
                                </div>
                                <div id="draggableDropdownDiv" class="draggableElement parentDivDraggable" title="Dropdown&#13;HTML Fragment&#13;Enable user to select single&#13;option from list.">
                                    <div style="float: left;">
                                        <img id="Dropdown" alt="Dropdown" src="//enterprise.mficient.com/images/select.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Dropdown
                                    </div>
                                </div>
                                <div id="draggableSelectorListDiv" class="draggableElement hide parentDivDraggable"
                                    title="SelectorList&#13;HTML Fragment&#13;Enable user to select single&#13;option from list.">
                                    <div style="float: left;">
                                        <img id="SelectorList" alt="SelectorList" src="//enterprise.mficient.com/images/select.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        SelectorList
                                    </div>
                                </div>
                                <div id="draggableRadioButtonDiv" class="draggableElement parentDivDraggable" title="Radio Button&#13;HTML Fragment&#13;Enable user to select option.">
                                    <div style="float: left;">
                                        <img id="RadioButton" alt="RadioButton" src="//enterprise.mficient.com/images/radioButton.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        RadioButton
                                    </div>
                                </div>
                                <div id="draggableCheckboxDiv" class="draggableElement parentDivDraggable" title="Checkbox&#13;HTML Fragment&#13;Enable user to select multiple option.">
                                    <div style="float: left;">
                                        <img id="Checkbox" alt="Checkbox" src="//enterprise.mficient.com/images/Checkbox.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Checkbox
                                    </div>
                                </div>
                                <div id="draggableImageDiv" class="draggableElement parentDivDraggable" title="Image&#13;HTML Fragment&#13;">
                                    <div style="float: left;">
                                        <img id="Image" alt="Image" src="//enterprise.mficient.com/images/image.png" class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Image
                                    </div>
                                </div>
                                <div id="draggableRepeaterDiv" class="draggableElement parentDivDraggable" title="List&#13;HTML Fragment&#13;Display data in row format.">
                                    <div style="float: left;">
                                        <img id="Repeater" alt="Repeater" src="//enterprise.mficient.com/images/repeater.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        List
                                    </div>
                                </div>
                                <%--<div id="draggableEditableListDiv" class="draggableElement parentDivDraggable" title="List&#13;HTML Fragment&#13;Display data in row format and editable">
                                    <div style="float: left;">
                                        <img id="EditableList" alt="EditableList" src="//enterprise.mficient.com/images/repeater.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        EditableList
                                    </div>
                                </div>--%>
                                <div id="draggableHiddenFieldDiv" class="draggableElement hide parentDivDraggable"
                                    title="Repeater&#13;HTML Fragment&#13;Display data in row format.">
                                    <div style="float: left;">
                                        <img id="HiddenField" alt="HiddenField" src="//enterprise.mficient.com/images/hiddenField.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        HiddenVariable
                                    </div>
                                </div>
                                <div id="draggableDatetimePickerDiv" class="draggableElement parentDivDraggable"
                                    title="Datetime Picker&#13;HTML Fragment&#13;Enable user to select date.&#13;">
                                    <div style="float: left;">
                                        <img id="DatetimePicker" alt="Datetime" src="//enterprise.mficient.com/images/datetimePickericon.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        DatetimePicker
                                    </div>
                                </div>
                                <div id="draggableToggleDiv" class="draggableElement parentDivDraggable" title="Toggle Switch&#13;HTML Fragment&#13;Toggle Switch.&#13;">
                                    <div style="float: left;">
                                        <img id="Toggle" alt="ToggleSwitch" src="//enterprise.mficient.com/images/ToggleIcon.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        ToggleSwitch
                                    </div>
                                </div>
                                <div id="draggableSliderDiv" class="draggableElement parentDivDraggable" title="Slider&#13;HTML Fragment&#13;Slider.&#13;">
                                    <div style="float: left;">
                                        <img id="Slider" alt="Slider" src="//enterprise.mficient.com/images/sliderIcon.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Slider
                                    </div>
                                </div>
                                <div id="draggableSeparatorDiv" class="draggableElement parentDivDraggable" title="Separator&#13;HTML Fragment&#13;Display row separator.">
                                    <div style="float: left;">
                                        <img id="Separator" alt="Separator" src="//enterprise.mficient.com/images/separator.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Separator
                                    </div>
                                </div>
                                <div id="SpacerDiv" class="draggableElement parentDivDraggable" title="Spacer&#13;HTML Fragment&#13;Spacer.&#13;">
                                    <div style="float: left;">
                                        <img id="Spacer" alt="Spacer" src="//enterprise.mficient.com/images/spacerIcon.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Spacer
                                    </div>
                                </div>
                                <div id="draggableCustomControlDiv" class="draggableElement parentDivDraggable" title="Custom Control">
                                    <div style="float: left;">
                                        <img id="CustomControl" alt="Custom Control" src="//enterprise.mficient.com/images/iconCustom.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Custom Control
                                    </div>
                                </div>
                                <div id="Div13" class="node WftoolboxSeprator" disabled="disabled">
                                </div>
                                <div id="draggableBarcodeDiv" class="draggableElement parentDivDraggable" title="BarcodeReader&#13;HTML Fragment&#13;BarcodeReader.&#13;">
                                    <div style="float: left;">
                                        <img id="BarcodeReader" alt="Barcode" src="//enterprise.mficient.com/images/barcodeIcon.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        BarcodeReader
                                    </div>
                                </div>
                                <div id="draggableLocationDiv" class="draggableElement parentDivDraggable" title="Location&#13;HTML Fragment&#13;Location.&#13;">
                                    <div style="float: left;">
                                        <img id="Location" alt="Location" src="//enterprise.mficient.com/images/LocationIcon.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Location
                                    </div>
                                </div>
                                <div id="PictureDiv" class="draggableElement parentDivDraggable" title="Picture&#13;HTML Fragment&#13;Picture.&#13;">
                                    <div style="float: left;">
                                        <img id="Picture" alt="Picture" src="//enterprise.mficient.com/images/PictureIcon.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Picture
                                    </div>
                                </div>
                                <div id="SignatureDiv" class="draggableElement parentDivDraggable" title="Signature&#13;HTML Fragment&#13;Signature.&#13;">
                                    <div style="float: left;">
                                        <img id="Signature" alt="Signature" src="//enterprise.mficient.com/images/ScribbleIcon.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Signature
                                    </div>
                                </div>
                                <div id="Div14" class="node WftoolboxSeprator" disabled="disabled">
                                </div>
                                <div id="PieChartDiv" class="draggableElement parentDivDraggable" title="PieChart&#13;HTML Fragment&#13;Pie Chart.&#13;">
                                    <div style="float: left;">
                                        <img id="PieChart" alt="PieChart" src="//enterprise.mficient.com/images/piecharticon.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        PieChart
                                    </div>
                                </div>
                                <div id="DrilldownPieChartDiv" class="draggableElement parentDivDraggable" title="PieChart (Drilldown) &#13;HTML Fragment&#13;PieChart (Drilldown).&#13;">
                                    <div style="float: left;">
                                        <img id="DrilldownPieChart" alt="PieChart (Drilldown)" src="//enterprise.mficient.com/images/drilldownpiecharticon.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        DrilldownPie
                                    </div>
                                </div>
                                <div id="BarGraphDiv" class="draggableElement parentDivDraggable" title="BarGraph&#13;HTML Fragment&#13;Bar Graph.&#13;">
                                    <div style="float: left;">
                                        <img id="BarGraph" alt="BarGraph" src="//enterprise.mficient.com/images/bargraphicon.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        BarGraph
                                    </div>
                                </div>
                                <div id="LineChartDiv" class="draggableElement parentDivDraggable" title="LineChart&#13;HTML Fragment&#13;Line Chart.&#13;">
                                    <div style="float: left;">
                                        <img id="LineChart" alt="LineChart" src="//enterprise.mficient.com/images/lineChartIcon.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        LineChart
                                    </div>
                                </div>
                                <div id="AngularGuageDiv" class="draggableElement parentDivDraggable" title="AngularGuage&#13;HTML Fragment&#13;Angular Guage.&#13;">
                                    <div style="float: left;">
                                        <img id="AngularGuage" alt="AngularGuage" src="//enterprise.mficient.com/images/iconAngularGauge.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        AngularGuage
                                    </div>
                                </div>
                                <div id="CylinderGuageDiv" class="draggableElement parentDivDraggable" title="CylinderGuage&#13;HTML Fragment&#13;Cylinder Guage.&#13;">
                                    <div style="float: left;">
                                        <img id="CylinderGuage" alt="CylinderGuage" src="//enterprise.mficient.com/images/iconCylinderGuage.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        CylinderGuage
                                    </div>
                                </div>
                               <%-- <div id="DataPointChartDiv" class="draggableElement parentDivDraggable" title="DataPointChart&#13;HTML Fragment&#13;DataPoint Chart.&#13;">
                                    <div style="float: left;">
                                        <img id="Img1" alt="DataPointChart" src="//enterprise.mficient.com/images/iconCylinderGuage.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        DataPointChart
                                    </div>
                                </div>--%>
                                <div id="TableDiv" class="draggableElement parentDivDraggable" title="BarGraph&#13;HTML Fragment&#13;Table.&#13;">
                                    <div style="float: left;">
                                        <img id="Table" alt="Table" src="//enterprise.mficient.com/images/tableicon.png"
                                            class="ToolboxImage" />
                                    </div>
                                    <div style="float: left; margin-top: 2px;">
                                        Table
                                    </div>
                                </div>
                                <div id="Div15" class="node WftoolboxSeprator" disabled="disabled">
                                </div>
                            </div>
                        </div>
                        <div id="mainFormbox" class="mainFormboxDiv">
                            <div id="StyleFormContentDiv">
                                <div>
                                    <div style="width: 336px; margin: auto; padding-right: 0px; margin-top: 25px;">
                                        <div id="form_Div">
                                            <div class="topcont">
                                                <img src="//enterprise.mficient.com/images/shades_cont_top_rgt.png" alt="" class="toprgtImg" />
                                                <img src="//enterprise.mficient.com/images/shades_cont_top_lft.png" alt="" class="toplftImg" />
                                            </div>
                                            <div class="MbScreenContainer">
                                                <div class="MbScreenContainerIn">
                                                    <div class="content">
                                                        <div id="OuterFormDesignContainerDiv" style="height: 450px; width: 100%; border-width: 0px;
                                                            border-style: solid; border-color: Gray; overflow-y: scroll;">
                                                            <div id="MstFormHeader" style="background-color: #000">
                                                            </div>
                                                            <div id="FormDesignContainerDiv" style="min-height: 450px; width: 280px; overflow: auto;
                                                                font-size: 11px; font-family: Arial; color: Black; background-color: White;">
                                                            </div>
                                                            <div id="MstFormFooter">
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="botcont">
                                                <img src="//enterprise.mficient.com/images/shades_cont_bot_lft.png" alt="" class="botlftImg" />
                                                <img src="//enterprise.mficient.com/images/shades_cont_bot_rgt.png" alt="" class="botrgtImg" />
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                </div>
                            </div>
                        </div>
                        <div id="TBmainFormbox" class="mainTbFormboxDiv">
                            <div id="TbStyleFormContentDiv">
                                <div>
                                    <div style="width: 486px; margin: auto; padding-right: 0px; padding-bottom: 3px;
                                        padding-top: 3px;">
                                        <div id="tablet_Div">
                                            <div class="TBtopcont">
                                                <img src="//enterprise.mficient.com/images/shades_cont_top_rgt.png" alt="" class="toprgtImg" />
                                                <img src="//enterprise.mficient.com/images/shades_cont_top_lft.png" alt="" class="toplftImg" />
                                            </div>
                                            <div class="TbScreenContainer">
                                                <div class="TbScreenContainerIn">
                                                    <div class="Tbcontent">
                                                        <div id="TBOuterFormDesignContainerDiv" style="height: 480px; width: 100%; border-width: 0px;
                                                            border-style: solid; border-color: Gray; overflow-y: scroll;">
                                                            <div id="TbMstFormHeader" style="background-color: #000">
                                                            </div>
                                                            <div id="TbFormDesignContainerDiv" style="min-height: 500px; width: 430px; overflow: auto;
                                                                font-size: 11px; font-family: Arial; color: Black; background-color: White;">
                                                            </div>
                                                            <div id="TbMstFormFooter">
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="TBbotcont">
                                                <img src="//enterprise.mficient.com/images/shades_cont_bot_lft.png" alt="" class="botlftImg" />
                                                <img src="//enterprise.mficient.com/images/shades_cont_bot_rgt.png" alt="" class="botrgtImg" />
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                </div>
                            </div>
                        </div>
                        <div id="mainPropertiesbox" class="mainPropertiesboxDiv">
                            <div class="OuterPropDiv">
                                <div class="ToolboxHeadDiv">
                                    <div id="Div21" class="HeadInnerDiv DefaultCursor">
                                        Form Properties
                                    </div>
                                </div>
                                <div id="FormPropetiesDiv">
                                    <div id="FrmInnerPropertieDiv">
                                        <%--<div class="HeadInnerDiv FLeft DefaultCursor" style="width: 46.7%">
                                            <div class="FLeft">
                                                <a id="A1" class="DefaultCursor" style="font-weight: bold;">Form Properties</a></div>
                                            </div>--%>
                                        <div class="InnerPropertieDiv" id="Form_Master" onclick="ControlPropOuterClick(this);">
                                            <%--<div class="propertyName">
                                                    Master Page</div>--%>
                                            <div class="propertyValue">
                                                <asp:UpdatePanel ID="updFrmMasterpage" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <%-- <asp:DropDownList ID="ddlFrmMasterpage" runat="server" class="ddlProp" OnSelectedIndexChanged="ddlFrmMasterpage_SelectedIndexChanged"
                                                                AutoPostBack="true">
                                                            </asp:DropDownList>--%>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                        <div id="MstPropertyDiv">
                                        </div>
                                        <div id="Form_LastRow" class="InnerPropertieDiv">
                                        </div>
                                    </div>
                                </div>
                                <div class="ToolboxHeadDiv">
                                    <div class="HeadInnerDiv FLeft DefaultCursor" style="width: 46.7%">
                                        <div class="FLeft">
                                            <a id="lnkCtrlProperties" class="DefaultCursor" style="font-weight: bold;">Control Properties</a></div>
                                    </div>
                                </div>
                                <div id="MainControlPropetiesDiv">
                                    <div id="ControlPropetiesDiv">
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <div class="PropertiesHelp">
                                <div>
                                    <div style="float: right;">
                                        <img alt="help" src="//enterprise.mficient.com/images/help.png" />
                                    </div>
                                </div>
                                <div id="PropertiesHelpDiv">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="divProcessHTML5App" class="fl w_100 hide">
        <div id="divHtml5MainMenu" class="fl w_100">
            <div id="Div17" class="ToolboxHeadOuterDiv">
                <div id="Div19" class="WfToolboxHead2Div">
                    <div>
                        <div class="formMenu fl" style="width: 93.5%;">
                            <ul class="nav">
                                <li><a id="lnkHtml5AppSave"><i class="icon-save"></i>Save</a> </li>
                                <li><a id="lnkHtml5AppSaveAs"><i class="icon-copy"></i>Save As</a> </li>
                                <li><a><i class="icon-globe"></i>Commit</a>
                                    <ul>
                                        <li><a id="lnkHtml5AppProduction"><i class="icon-wrench"></i>Production</a></li>
                                        <li><a id="lnkHtml5AppTester"><i class="icon-wrench"></i>Tester</a></li>
                                    </ul>
                                </li>
                                <li><a id="lnkHtml5AppDelete"><i class="icon-trash"></i>Delete</a> </li>
                                <li><a id="lnkHtml5AppClose"><i class="icon-signout"></i>Close</a></li>
                            </ul>
                        </div>
                        <div style="clear: both;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="divHtml5MainMenuAndCanvasSeparator" class="fl w_100" style="height: 23px;
            text-align: center;">
            <h5>
                <span id="spanHtml5AppHeaderTitle"></span>
            </h5>
        </div>
        <div id="divHtml5IdeCont" class="w_100 fl bgWhite" style="border-top: 1px solid #AAAAAA;">
            <div id="divTreeCanvasCont" class="fl">
                <div id="divHtml5FilesTreeView" class="WfmainOuterToolboxDiv">
                    <div id="divHtml5TreeViewToolbar" class="fl " style="text-align: center;">
                        <div class="ui-widget-header pt3p pb3p">
                            <i id="refreshFileBrowser" title="Refresh" class="icon-refresh mrgr20 mrgl20 cursorClickable"
                                style="font-size: 15px"></i><i id="fileBrowserAddNewFile" class="icon-plus mrgr20 cursorClickable"
                                    title="Add new
    file" style="font-size: 15px"></i><i id="fileBrowserAddNewFolder" title="Add new
    folder" class="fa fa-folder-o cursorClickable mrgr20" style="font-size: 15px"></i><i
        id="fileBrowserMoveFiles" title="Move" class="fa fa-exchange cursorClickable mrgr20"
        style="font-size: 15px"></i><i id="fileBrowserDownloadZip" title="Download Zip" class="fa fa-cloud-download cursorClickable mrgr20"
            style="font-size: 15px"></i>
                        </div>
                        <div id="divAppFilesTree" style="min-width: 200px; overflow: auto; height: 600px;
                            max-height: 600px; text-align: left">
                        </div>
                    </div>
                    <div id="TreeViewSizer" class="DivSizer">
                    </div>
                </div>
                <div id="divHtml5Canvas" class="fl hide">
                    <div id="divFileMenuBar" style="height: 31px; border: 1px solid #bbbbbb; background-color: #f5f5f5;
                        color: #3f3f3f; font-weight: bold;">
                        <%--<div style="float: left; width: 40%; line-height:
    31px; vertical-align: middle; text-align: center;"> <div style="float: left; width:
    20%; font-size: small;"> File Path:</div> <div style="float: left; width: 80%;">
    <asp:TextBox ID="txtFilePath" runat="server" CssClass="txtFilePath"></asp:TextBox></div>
    </div>--%>
                        <%--<div id="fileEdit" style="float: right; width: 40%; line-height:
    31px; vertical-align: middle; text-align: center; display: none;"> <asp:UpdatePanel
    ID="updEditPanel" runat="server" UpdateMode="Conditional"> <ContentTemplate> <ul
    style="list-style: none; width: 90%;"> <li style="float: right; width: 20%; margin-top:
    -10px; line-height: 31px; font-size: small;"> <asp:LinkButton ID="lnkDelete" runat="server">Delete</asp:LinkButton></li>
    <li style="float: right; width: 2%; margin-top: -10px; line-height: 31px; font-size:
    small;"> |</li> <li style="float: right; width: 20%; margin-top: -10px; line-height:
    31px; font-size: small;"> <asp:LinkButton ID="lnkEditFile" runat="server">Edit</asp:LinkButton></li>
    </ul> </ContentTemplate> </asp:UpdatePanel> </div>--%>
                        <div id="divFileSaveMenu" class="editObjectMenu fr mrgl10 mrgr10" style="line-height: 31px;
                            vertical-align: middle; text-align: center; display: block;">
                            <ul class="editObjectMenu">
                                <li id="listSaveFileCont"><a id="lnkSaveFileOnEdit">save</a><span class="separator">|</span></li>
                                <li id="listSearchFileCont"><a id="lnkSearchFileOnEdit">search</a><span class="separator">|</span></li>
                                <li id="listCloseFileCont"><a id="lnkCloseFileOnEdit">close</a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="viewFileDiv" class="fl " style="position: relative; width: 100%; height: 580px;">
                        <div id="divHtml5ImageViewerCont" class="hide">
                            <asp:UpdatePanel ID="updHtml5ImageViewer" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <asp:Image ID="imgHtml5ImageViewer" runat="server" Width="250px" Height="250px">
                                    </asp:Image></ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div id="divAceFileEditorCont" class="aceContainer">
                            <pre id="aceeditor"></pre>
                            <div id="acestatusBar">
                                <div class="fr w_80">
                                    <div class="fr w_30">
                                        <div class="g5 p5p">
                                            line : <span id="spanEditorLineNo">0</span></div>
                                        <div class="g5 p5p">
                                            column : <span id="spanEditorColumnNo">0</span></div>
                                    </div>
                                    <div class="border1LeftRight fr w_30">
                                        <div class="g5 p5p">
                                            length : <span id="spanEditorContentLength">0</span></div>
                                        <div class="g5 p5p">
                                            lines : <span id="spanEditorContentLines">0</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="divHtml5PropsContainer" class="fr">
                <div id="divHtml5AppPropsContainer" class="WfmainPropertiesboxDiv w_100">
                    <div class="WfOuterPropDiv">
                        <div id="divHtml5AppProperties" class="WFCtrlPropetiesDiv">
                        </div>
                        <div class="ToolboxHeadDiv">
                            <div id="Div35" class="HeadInnerDiv DefaultCursor">
                                File Properties
                            </div>
                        </div>
                        <div id="divHtml5AppFileProperties" class="WFCtrlPropetiesDiv">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Popup Divs -->
    <div id="SubProcAddNewFile" class="hide">
        <div class="w_100" style="margin-bottom: 10px;">
            <%--<asp:RadioButtonList ID="rdbFileTypes"
    runat="server" RepeatDirection="Horizontal" RepeatColumns="3" RepeatLayout="Table">
    <asp:ListItem Value="IMG" Text="Image"></asp:ListItem> <asp:ListItem Value="HTML"
    Text="HTML"></asp:ListItem> <asp:ListItem Value="JS" Text="Javascript (.js)"></asp:ListItem>
    <asp:ListItem Value="CSS" Text="Stylesheet (.css)"></asp:ListItem> <asp:ListItem
    Value="TXT" Text="Text (.txt)"></asp:ListItem> <asp:ListItem Value="XML" Text="XML
    "></asp:ListItem> <asp:ListItem Value="ZIP" Text="Application Zip File (drag and
    drop)" Selected="True"></asp:ListItem> </asp:RadioButtonList>--%>
            <div class="w_100">
                <div class="g4 p2p">
                    <input type="radio" name="rdbFileTypes" value="1" /><span>Image</span></div>
                <div class="g3 p2p">
                    <input type="radio" name="rdbFileTypes" value="2" /><span>HTML (.html)</span></div>
                <div class="g3 p2p">
                    <input type="radio" name="rdbFileTypes" value="3" /><span>Javascript (.js)</span></div>
            </div>
            <div class="w_100">
                <div class="g4 p2p">
                    <input type="radio" name="rdbFileTypes" value="4" /><span>Stylesheet (.css)</span></div>
                <div class="g3 p2p">
                    <input type="radio" name="rdbFileTypes" value="5" /><span>Text (.txt)</span></div>
                <div class="g3 p2p">
                    <input type="radio" name="rdbFileTypes" value="6" /><span>XML (.xml)</span></div>
            </div>
            <div class="w_100">
                <div class="g12 p2p">
                    <input type="radio" name="rdbFileTypes" value="7" checked="checked" /><span>Application
                        File (.zip)</span></div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="g12">
            <span id="spanNewFileAddFilePath">Add File to :</span>
            <select id="ddlFolderPathForCreateNewFile" class="w_100p" onkeydown="return enterKeyFilter(event);">
            </select>
        </div>
        <div id="divFileUploaderDragDrop" class="w_100 fl">
            <div>
                <div id="dragandDrop">
                    Drag file here</div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="p2p w_100" style="text-align: center; clear: left;">
            <p>
                <b>OR</b></p>
        </div>
        <div id="divFileUploaderInput" class="mfFieldSet fl w_95" style="text-align: center">
            <div class="w_50 fl">
                <div style="padding-left: 10px">
                    <input type="file" name="uploadHtml5File" />
                </div>
            </div>
            <%--<div class="w_40 fl"> <div style="padding-left:
    10px"> <span style="position: relative; top: 2px" class="">Upload to</span> <select
    id="ddlUploadFolderPath" class="w_100p"> </select> </div> </div>--%>
            <div class="w_10 fl">
                <div style="padding-right: 10px">
                    <span id="spanRemoveFileInFileUploader" style="position: relative; top: 3px;"><i
                        class="fa fa-times"></i></span>
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div id="divSeparatorOrForCreateFile" class="p2p w_100 hide" style="text-align: center">
            <p>
                <b>OR</b></p>
        </div>
        <div class="clear">
        </div>
        <div id="divHtml5NewFileNameCont" class="mfFieldSet fl w_95">
            <div class="w_95 fl">
                <div class="fl p10p">
                    <p>
                        Create empty file
                    </p>
                </div>
                <div class="g4 p10p">
                    <asp:TextBox ID="txtFileName" runat="server" CssClass="InputStyle w_90" onkeydown="return enterKeyFilter(event);"></asp:TextBox>
                </div>
                <div class="g1 p10p">
                    <select id="ddlHtml5FileType" onkeydown="return enterKeyFilter(event);">
                        <option value="4">.css</option>
                        <option value="2">.html</option>
                        <option value="8">.htm</option>
                        <option value="3">.js</option>
                        <option value="5">.txt</option>
                        <option value="6">.xml</option>
                    </select>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <button type="button" id="btnCancel" class="InputStyle" onclick="$('#SubProcAddNewFile').dialog('close');return false;">
                    Cancel</button>
                <button type="button" id="btnAddFile" class="InputStyle">
                    Upload</button>
            </div>
        </div>
    </div>
    <div id="divGetHtml5AppNewFolderName" class="hide">
        <div style="float: left; width: 100%">
            <div class="DbCmdRow" style="margin-left: 5px">
                <div class="DbConnLeftDiv">
                    Folder Name
                </div>
                <div class="DbConnRightDiv">
                    <input type="text" id="txtHtml5AppNewFolderName" class="InputStyle w_80" maxlength="30"
                        onkeydown="return enterKeyFilter(event);" />
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="DbCmdRow mrgt5" style="margin-left: 5px">
                <div class="DbConnLeftDiv">
                    Save In
                </div>
                <div class="DbConnRightDiv" style="padding-left: 2px">
                    <select id="ddlHtml5AppNewFolderSavePath" class="w_50" onkeydown="return enterKeyFilter(event);">
                    </select>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="btnHtml5AppSaveNewFolder" type="button" value=" Create " class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="divGetApplicationName" class="hide">
        <div style="float: left; width: 100%">
            <div class="DbCmdRow" style="margin-left: 5px">
                <div class="DbConnLeftDiv">
                    Application Name
                </div>
                <div class="DbConnRightDiv">
                    <asp:TextBox ID="txtNewAppName" runat="server" CssClass="InputStyle" MaxLength="30"
                        Width="80%" onkeydown="return enterKeyFilter(event);"></asp:TextBox>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="DbCmdRow mrgt5" style="margin-left: 5px">
                <div class="DbConnLeftDiv">
                    Runs On
                </div>
                <div class="DbConnRightDiv" style="padding-left: 2px">
                    <select id="ddlAppRunsOn" onkeydown="return enterKeyFilter(event);">
                        <option value="0">Phone & Tablet</option>
                        <option value="1">Tablet Only</option>
                    </select>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="DbCmdRow mrgt5" style="margin-left: 5px">
                <div class="DbConnLeftDiv">
                    Design Mode
                </div>
                <div class="DbConnRightDiv pa" style="padding-left: 2px">
                    <select id="ddlAppDesignMode" onkeydown="return enterKeyFilter(event);">
                        <option value="0">Phone</option>
                        <option value="1">Tablet</option>
                        <option value="2">Both</option>
                    </select>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="DbCmdRow mrgt5" style="margin-left: 5px">
                <div class="DbConnLeftDiv">
                </div>
                <div class="DbConnRightDiv" style="padding-left: 2px">
                    <input type="checkbox" id="chkStandardAppType" class="mrg0" checked="checked" /><span
                        id="spanAppType" class="mrg2" style="position: relative; top: -2px;">Online app</span>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="SubProcborderDiv">
                <%--<asp:Button ID="btnAddNewApp" runat="server" Text=" Create " OnClick="btnAddNewApp_Click"
    CssClass="InputStyle" OnClientClick="btnAddViewClick();ValidateCreateNewAppFlow();"
    />--%>
                <div class="SubProcBtnDiv" align="center">
                    <input id="btnAddNewApp" type="button" value=" Create " class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="divGetHtml5AppName" class="hide">
        <div style="float: left; width: 100%">
            <div class="DbCmdRow" style="margin-left: 5px">
                <div class="DbConnLeftDiv">
                    Application Name
                </div>
                <div class="DbConnRightDiv">
                    <%--<asp:TextBox ID="txtHtml5AppName" runat="server" CssClass="InputStyle" MaxLength="30"
    Width="80%"></asp:TextBox>--%>
                    <input type="text" id="txtHtml5AppName" class="InputStyle w_80" maxlength="30" onkeydown="return enterKeyFilter(event);" />
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <input id="btnSaveHtml5AppName" type="button" value=" Create " class="InputStyle" />
            </div>
        </div>
    </div>
    <div id="divHtml5AppSaveAs" class="hide">
        <div style="margin-top: 18px;">
            <div class="DbConnLeftDiv">
                App Name :
            </div>
            <div class="DbConnRightDiv">
                <input type="text" id="txtHtml5AppSaveAsName" class="InputStyle w_80" maxlength="30"
                    onkeydown="return enterKeyFilter(event);" />
            </div>
        </div>
        <div class="clear">
        </div>
        <div style="margin-top: 17px;">
        </div>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <input id="btnHtml5AppSaveAsSave" type="button" value=" Save " class="InputStyle" />
                <input id="btnHtml5AppSaveAsCancel" type="button" value=" Cancel " class="InputStyle" />
            </div>
        </div>
    </div>
    <div id="divHtml5CommitApp" class="hide">
        <asp:UpdatePanel runat="server" ID="updHtml5CommitApp" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin-top: 10px;">
                    <div class="DbCmdRow" style="margin-left: 10px">
                        <asp:Label ID="lblHtml5CommitAppMsg" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="DbCmdRow" style="margin-left: 10px">
                        <div class="DbConnLeftDiv">
                            Version Upgrade
                        </div>
                        <div class="DbConnRightDiv">
                            <asp:RadioButton ID="rdbHtml5CommitMinor" runat="server" Text="Minor" GroupName="Html5Commit" />&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rdbHtml5CommitMajor" runat="server" GroupName="Html5Commit"
                                Text="Major" />
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="DbCmdRow" style="margin-left: 10px">
                        <div class="DbConnLeftDiv">
                            Version History
                        </div>
                        <div class="DbConnRightDiv">
                            <asp:TextBox ID="txtHtml5CommitVersionHistory" runat="server" CssClass="InputStyle"
                                TextMode="MultiLine" Width="90%" Rows="5" MaxLength="450" onkeydown="return enterKeyLinefeed(this.id, event);"></asp:TextBox>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <input type="button" value=" Commit " class="InputStyle" id="btnHtml5CommitAppSave" />
            </div>
        </div>
    </div>
    <div id="divHtml5AppCommitTesterApp" class="hide">
        <asp:UpdatePanel runat="server" ID="updCommitHtml5AppTester" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin-top: 10px;">
                    <div class="DbCmdRow" style="margin-left: 10px">
                        <asp:Label ID="lblCommitTesterHtml5AppMsg" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="DbCmdRow" style="margin-left: 10px">
                        <div class="DbConnLeftDiv">
                            Version Upgrade
                        </div>
                        <div class="DbConnRightDiv">
                            <asp:RadioButton ID="radCommitTesterHtml5AppMinor" runat="server" Text="Minor" GroupName="upgrade" />&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton
                                ID="radCommitTesterHtml5AppMajor" runat="server" GroupName="upgrade" Text="Major" />
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="DbCmdRow" style="margin-left: 10px">
                        <div class="DbConnLeftDiv">
                            Version History
                        </div>
                        <div class="DbConnRightDiv">
                            <asp:TextBox ID="txtCommitTesterHtml5AppDescription" runat="server" CssClass="InputStyle"
                                TextMode="MultiLine" Width="90%" Rows="5" MaxLength="450" onkeydown="return enterKeyLinefeed(this.id, event);"></asp:TextBox>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <input type="button" value=" Commit " class="InputStyle" id="btnCommitTesterHtml5AppSave" />
            </div>
        </div>
    </div>
    <div id="divMoveHtml5AppFilesCont" class="hide">
        <div style="float: left; width: 100%">
            <div class="DbCmdRow" style="margin-left: 5px">
                <div class="DbConnLeftDiv">
                    Move To
                </div>
                <div class="DbConnRightDiv">
                    <select id="ddlHtml5AppFileMoveTo" class="CmdDropDown
    txtTblCol" onkeydown="return enterKeyFilter(event);">
                    </select>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <input id="btnHtml5AppFileMoveToSave" type="button" value=" Move
    " class="InputStyle" />
            </div>
        </div>
    </div>
    <div id="divNewAppIconContainer" class="hide">
        <div class="borderNone">
            <div class="borderNone">
                <div style="width: 50px; vertical-align: middle; height: 500px;" class="borderNone FLeft">
                    <div align="left" style="width: 100%; margin-top: 220px;" class="FLeft">
                        <img alt="" id="imgNewIconSelectPrevious" src="//enterprise.mficient.com/images/imgPrvs.png"
                            style="cursor: pointer;" />
                    </div>
                </div>
                <div align="center" style="vertical-align: middle; height: 500px; width: 800px; max-height: 500px;"
                    class="borderNone FLeft">
                    <div id="NewIconGallery" align="center" style="vertical-align: middle; margin-top: 25px;">
                    </div>
                    <div class="clear">
                    </div>
                    <div id="NewIconGalleryText" style="width: 100%; text-align: center;" align="center"
                        class="borderNone FLeft">
                    </div>
                </div>
                <div style="width: 50px; vertical-align: middle; height: 500px;" class="borderNone FLeft">
                    <div align="right" style="width: 100%; margin-top: 220px;">
                        <img alt="" id="imgNewIconSelectNext" src="//enterprise.mficient.com/images/imgNext.png"
                            style="cursor: pointer;" />
                    </div>
                </div>
            </div>
            <div class="borderNone">
                <input id="hdfNewStartIconIndex" type="hidden" />
                <input id="hdfNewEndIconIndex" type="hidden" />
            </div>
        </div>
        <div id="NewMenuIconOuterDiv" class="hide" style="min-height: 20px">
        </div>
    </div>
    <!-- Tanika -->
    <div id="SubProcBoxMessage">
        <div>
            <div class="MessageDiv">
                <a id="a2">Please check following points : </a>
                <br />
                <a id="aMessage"></a>
            </div>
            <div style="margin-top: 10px">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="btnOkErrorMsg" type="button" value=" OK " onclick="SubProcBoxMessage(false);"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcViewSaveAs" class="hide">
        <div style="margin-top: 18px;">
            <div class="DbConnLeftDiv">
                Form Name :
            </div>
            <div class="DbConnRightDiv">
                <asp:TextBox ID="txtViewName" runat="server" CssClass="InputStyle" Width="80%" onkeydown="return enterKeyFilter(event);"></asp:TextBox>
            </div>
        </div>
        <div class="clear">
        </div>
        <div style="margin-top: 17px;">
        </div>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <input id="Button60" type="button" value=" Save " class="InputStyle" onclick="btnVeiwSaveClick();" />
                <input id="Button61" type="button" value=" Cancel " class="InputStyle" onclick="SubProcViewSaveAs(false);" />
            </div>
        </div>
    </div>
    <div id="SubProcCtrlDbCmd" class="hide" style="overflow-x: hidden;">
        <div>
            <div>
                <asp:UpdatePanel ID="updCtrlDbCmd" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnDbConnRowIndex" EventName="Click" />
                    </Triggers>
                    <ContentTemplate>
                        <div>
                            <div id="Ctrl_DbCommand" class="DCmdRow" style="width: 400px;">
                                <div class="DCmdRowLeft dcMrg5">
                                    Database Object</div>
                                <div class="DCmdRowRight dcMrg5">
                                    <asp:DropDownList ID="ddlCtrlDbCommand" runat="server" CssClass="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                                    </asp:DropDownList>
                                </div>
                                <div class="FLeft dcMrg15">
                                    <asp:ImageButton ID="btnCtrl_LinkParmeters" runat="server" ToolTip="Set Object Parameters"
                                        ImageUrl="~/images/dataObjectInputs.png" CssClass="InputStyle hide" Width="18px"
                                        Height="18px" />
                                    <asp:LinkButton ID="lnkShowDbCmdDetails" runat="server" ToolTip="View Object Details"><img alt="" src="//enterprise.mficient.com/images/icon/GRAY0295.png"
    class="dbCmdimg"/></asp:LinkButton>
                                </div>
                            </div>
                            <div id="div_DbIgnoreCache" class="DCmdRow">
                                <div class="DCmdRowLeft" style="margin-top: 2px;">
                                    Ignore Cache</div>
                                <div class="DCmdRowRight">
                                    <input type="checkbox" id="chk_DbIgnoreCache" class="CmdDropDown" />
                                </div>
                                <%--Cache
    Ignore--%>
                            </div>
                            <div id="Db_Label_Command" class="hide">
                                <table class="detailsTable">
                                    <thead>
                                        <th>
                                            Parameter
                                        </th>
                                        <%--<th> Data Type </th> <th> Data Format </th> <th>
    Decimal </th> <th> Factor </th>--%>
                                        <th>
                                            Data Columns
                                        </th>
                                    </thead>
                                    <tbody id="Db_Div_Label_Columns">
                                        <tr id="Db_Lbl_Col_1">
                                            <td id="Db_Div_Para1" style="padding-left: 34px;">
                                                %1
                                            </td>
                                            <td id="Db_Div_Para1_Columns">
                                                <asp:DropDownList runat="server" ID="Db_Dp_Para1_Columns" Style="width: 120px;" onkeydown="return enterKeyFilter(event);">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="Db_Lbl_Col_2">
                                            <td id="Db_Div_Para2" style="padding-left: 34px;">
                                                %2
                                            </td>
                                            <td id="Db_Div_Para2_Columns">
                                                <asp:DropDownList runat="server" ID="Db_Dp_Para2_Columns" Style="width: 120px;" onkeydown="return enterKeyFilter(event);">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="Db_Lbl_Col_3">
                                            <td id="Db_Div_Para3" style="padding-left: 34px;">
                                                %3
                                            </td>
                                            <td id="Db_Div_Para3_Columns">
                                                <asp:DropDownList runat="server" ID="Db_Dp_Para3_Columns" Style="width: 120px;" onkeydown="return enterKeyFilter(event);">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="Db_Lbl_Col_4">
                                            <td id="Db_Div_Para4" style="padding-left: 34px;">
                                                %4
                                            </td>
                                            <td id="Db_Div_Para4_Columns">
                                                <asp:DropDownList runat="server" ID="Db_Dp_Para4_Columns" Style="width: 120px;" onkeydown="return enterKeyFilter(event);">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="Db_Lbl_Col_5">
                                            <td id="Db_Div_Para5" style="padding-left: 34px;">
                                                %5
                                            </td>
                                            <td id="Db_Div_Para5_Columns">
                                                <asp:DropDownList runat="server" ID="Db_Dp_Para5_Columns" Style="width: 120px;" onkeydown="return enterKeyFilter(event);">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="Db_Ctrl_DisplayText_Div" class="DCmdRow hide">
                                <div class="DCmdRowLeft">
                                    Display Text</div>
                                <div class="DCmdRowRight">
                                    <asp:DropDownList ID="ddlCtrlCmd_DisplayTxt" runat="server" CssClass="CmdDropDown"
                                        onkeydown="return enterKeyFilter(event);">
                                        <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div id="Db_Checkbox_DisplayText_Div" class="DCmdRow hide">
                                <div class="DCmdRowLeft">
                                    Checked Value</div>
                                <div class="DCmdRowRight">
                                    <asp:DropDownList ID="ddlCheckboxCmd_CheckedValue" runat="server" CssClass="CmdDropDown"
                                        onkeydown="return enterKeyFilter(event);">
                                        <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div id="Db_Slider_DisplayText_Div" class="DCmdRow hide">
                                <div class="DCmdRowLeft">
                                    Value</div>
                                <div class="DCmdRowRight">
                                    <asp:DropDownList ID="ddlSliderCmd_Val" runat="server" CssClass="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                                        <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div id="Db_Dropdown_Options_Div" class="hide">
                                <div id="Db_Dropdown_DbText" class="DCmdRow">
                                    <div class="DCmdRowLeft">
                                        Option Text</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlDbCommand_DisplayTxt" runat="server" CssClass="CmdDropDown"
                                            onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="Db_Dropdown_DbValue" class="DCmdRow">
                                    <div class="DCmdRowLeft">
                                        Option Value</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlDbCommand_DisplayVal" runat="server" CssClass="CmdDropDown"
                                            onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div id="Db_List_Options_Div" class="hide">
                                <div id="Db_List_RowId" class="DCmdRow hide">
                                    <div class="DCmdRowLeft">
                                        Row ID</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlList_RowId" runat="server" class="CmdDropDown BindValueDdl"
                                            onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="Db_List_Title" class="DCmdRow hide">
                                    <div class="DCmdRowLeft">
                                        Title</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlList_Title" runat="server" class="CmdDropDown BindValueDdl"
                                            onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="Db_List_SubTitle" class="DCmdRow hide">
                                    <div class="DCmdRowLeft">
                                        Subtitle</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlList_SubTitle" runat="server" class="CmdDropDown BindValueDdl"
                                            onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="Db_List_Info" class="DCmdRow hide">
                                    <div class="DCmdRowLeft">
                                        Information</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlList_Info" runat="server" class="CmdDropDown BindValueDdl"
                                            onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="Db_List_AddInfo" class="DCmdRow hide">
                                    <div class="DCmdRowLeft">
                                        Additional Information</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlList_AddInfo" runat="server" class="CmdDropDown BindValueDdl"
                                            onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="Db_List_ImageName" class="DCmdRow">
                                    <div class="DCmdRowLeft">
                                        Image Source</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlList_Image" runat="server" class="CmdDropDown BindValueDdl"
                                            onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="Db_List_CountField" class="DCmdRow hide">
                                    <div class="DCmdRowLeft">
                                        Count Field</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlList_CountField" runat="server" class="CmdDropDown BindValueDdl"
                                            onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="Db_List_SelectedRow" class="DCmdRow hide">
                                    <div class="DCmdRowLeft">
                                        Selected Row</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlList_SelectedRow" runat="server" class="CmdDropDown BindValueDdl"
                                            onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="Db_List_Date" class="DCmdRow hide" style="width: auto;">
                                    <div class="DCmdRowLeft">
                                        Date</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlList_Date" runat="server" class="CmdDropDown BindValueDdl"
                                            onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="FLeft dcMrg15">
                                        <asp:DropDownList ID="ddlList_DateFormat" runat="server" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Format</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div id="Db_EditableList_Options_Div" class="fl" style="max-height: 300px; overflow-y: auto;
                                overflow-x: hidden; margin-top: 6px;">
                            </div>
                            <div id="Db_PieChart_Options_Div" class="hide">
                                <div id="Db_PieChart_Title" class="DCmdRow">
                                    <div class="DCmdRowLeft">
                                        Title</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlPieChart_Title" runat="server" CssClass="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="Db_PieChart_Value" class="DCmdRow">
                                    <div class="DCmdRowLeft">
                                        Value</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlPieChart_Value" runat="server" CssClass="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div id="Db_Bar_LineChart_Options_Div" class="hide">
                                <div id="Db_Bar_LineChart_Xaxis" class="DCmdRow">
                                    <div class="DCmdRowLeft">
                                        X-Axis Data</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlBar_LineChart_Xaxis" runat="server" CssClass="CmdDropDown"
                                            onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="Db_Bar_LineChart_Yaxis" class="DCmdRow">
                                    <div class="DCmdRowLeft">
                                        Y-Axis Data</div>
                                    <br />
                                    <div style="float: left; width: 500px;">
                                        <div id="DbSeriesHeadRow" class="Scrollable">
                                            <table class="normaldetailsTable">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            Series
                                                        </th>
                                                        <th>
                                                            Label
                                                        </th>
                                                        <th>
                                                            Value
                                                        </th>
                                                        <th>
                                                            Scale Factor
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr id="Bar_Line_DbValue1">
                                                        <td id="DbYSeriesCol1">
                                                            Series 1
                                                        </td>
                                                        <td id="DbYLabelCol1">
                                                            <input id="txtYDbLabel1" type="text" onkeydown="return enterKeyFilter(event);" />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlChartDbCommand_Value" runat="server" Style="width: 120px;"
                                                                onkeydown="return enterKeyFilter(event);">
                                                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td id="DbYScaleCol1">
                                                            <input id="txtYDbScaleFact1" type="text" style="width: 40px; margin-left: 12.5px;"
                                                                onkeydown="return enterKeyFilter(event);" />
                                                        </td>
                                                    </tr>
                                                    <tr id="Bar_Line_DbValue2">
                                                        <td id="DbYSeriesCol2">
                                                            Series 2
                                                        </td>
                                                        <td id="DbYLabelCol2">
                                                            <input id="txtYDbLabel2" type="text" onkeydown="return enterKeyFilter(event);" />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlChartDbCommand_Value2" runat="server" Style="width: 120px;"
                                                                onkeydown="return enterKeyFilter(event);">
                                                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td id="DbYScaleCol2">
                                                            <input id="txtYDbScaleFact2" type="text" style="width: 40px; margin-left: 12.5px;"
                                                                onkeydown="return enterKeyFilter(event);" />
                                                        </td>
                                                    </tr>
                                                    <tr id="Bar_Line_DbValue3">
                                                        <td id="DbYSeriesCol3">
                                                            Series 3
                                                        </td>
                                                        <td id="DbYLabelCol3">
                                                            <input id="txtYDbLabel3" type="text" onkeydown="return enterKeyFilter(event);" />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlChartDbCommand_Value3" runat="server" Style="width: 120px;"
                                                                onkeydown="return enterKeyFilter(event);">
                                                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td id="DbYScaleCol3">
                                                            <input id="txtYDbScaleFact3" type="text" style="width: 40px; margin-left: 12.5px;"
                                                                onkeydown="return enterKeyFilter(event);" />
                                                        </td>
                                                    </tr>
                                                    <tr id="Bar_Line_DbValue4">
                                                        <td id="DbYSeriesCol4">
                                                            Series 4
                                                        </td>
                                                        <td id="DbYLabelCol4">
                                                            <input id="txtYDbLabel4" type="text" onkeydown="return enterKeyFilter(event);" />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlChartDbCommand_Value4" runat="server" Style="width: 120px;"
                                                                onkeydown="return enterKeyFilter(event);">
                                                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td id="DbYScaleCol4">
                                                            <input id="txtYDbScaleFact4" type="text" style="width: 40px; margin-left: 12.5px;"
                                                                onkeydown="return enterKeyFilter(event);" />
                                                        </td>
                                                    </tr>
                                                    <tr id="Bar_Line_DbValue5">
                                                        <td id="DbYSeriesCol5">
                                                            Series 5
                                                        </td>
                                                        <td id="DbYLabelCol5">
                                                            <input id="txtYDbLabel5" type="text" onkeydown="return enterKeyFilter(event);" />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlChartDbCommand_Value5" runat="server" Style="width: 120px;"
                                                                onkeydown="return enterKeyFilter(event);">
                                                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td id="DbYScaleCol5">
                                                            <input id="txtYDbScaleFact5" type="text" style="width: 40px; margin-left: 12.5px;"
                                                                onkeydown="return enterKeyFilter(event);" />
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Db_Image_Div" class="DCmdRow hide">
                                <div class="DCmdRow">
                                    <div class="DCmdRowLeft">
                                        Image Source</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlImageCmd_DisplayTxt" runat="server" CssClass="CmdDropDown"
                                            onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Source</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="clear" style="margin-top: 10px;">
                                </div>
                                <div class="DCmdRow">
                                    <div class="DCmdRowLeft">
                                        Title</div>
                                    <div class="DCmdRowRight">
                                        <asp:DropDownList ID="ddlImageCmd_DisplayTitle" runat="server" CssClass="CmdDropDown"
                                            onkeydown="return enterKeyFilter(event);">
                                            <asp:ListItem Value="-1">Select Title</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="Db_Table_Options_Div">
                            <div id="Db_Table_Options_Mapping_Div" class="fl" style="max-height: 300px; overflow-y: auto;
                                overflow-x: hidden; margin-top: 6px;">
                            </div>
                            <div class="clear" style="margin-top: 10px;">
                            </div>
                            <hr />
                            <div style="margin-top: 10px;">
                                <span class="astric">*&nbsp;&nbsp;&nbsp;</span>0 : Unix Datetime; 1 : .Net Ticks;
                                Or Custom Format (eg. dd/MM/yyyy).
                            </div>
                            <div style="margin-top: 3px;">
                                <span class="astric">**&nbsp;&nbsp;</span>Represents decimal places for numeric
                                data type or datetime format for datatime datatype (eg. dd/MM/yyyy).
                            </div>
                        </div>
                        <div id="Db_Location_Options_Div" class="hide">
                            <div id="Db_Location_Latitude" class="DCmdRow">
                                <div class="DCmdRowLeft">
                                    Latitude</div>
                                <div class="DCmdRowRight">
                                    <select id="ddlDbLocationDataBind_Latitude" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                                    </select>
                                </div>
                            </div>
                            <div id="Db_Location_Longitude" class="DCmdRow">
                                <div class="DCmdRowLeft">
                                    Longitude</div>
                                <div class="DCmdRowRight">
                                    <select id="ddlDbLocationDataBind_Longitude" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="Db_Cylinder_Guage_Div" class="hide">
                            <div id="Db_Cylinder_Guage_Value" class="DCmdRow">
                                <div class="DCmdRowLeft">
                                    Value</div>
                                <div class="DCmdRowRight">
                                    <asp:DropDownList ID="ddlCylinder_Guage_Value" runat="server" CssClass="CmdDropDown"
                                        onkeydown="return enterKeyFilter(event);">
                                        <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div id="Db_Cylinder_Guage_MaxVal" class="DCmdRow">
                                <div class="DCmdRowLeft">
                                    Max Value</div>
                                <div class="DCmdRowRight">
                                    <asp:DropDownList ID="ddlCylinder_Guage_MaxVal" runat="server" CssClass="CmdDropDown"
                                        onkeydown="return enterKeyFilter(event);">
                                        <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div id="Db_DPieChart_Options_Div" class="hide">
                            <div id="Db_DPieChart_Title" class="DCmdRow">
                                <div class="DCmdRowLeft">
                                    Slice Title</div>
                                <div class="DCmdRowRight">
                                    <asp:DropDownList ID="ddlDbDPieChart_Slice_Title" runat="server" CssClass="CmdDropDown"
                                        onkeydown="return enterKeyFilter(event);">
                                        <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div id="Db_DPieChart_Value" class="DCmdRow">
                                <div class="DCmdRowLeft">
                                    Sub Slice Title</div>
                                <div class="DCmdRowRight">
                                    <asp:DropDownList ID="ddlDbDPieChart_SubSlice_Title" runat="server" CssClass="CmdDropDown"
                                        onkeydown="return enterKeyFilter(event);">
                                        <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div id="Db_DPieChart_Drilldown_Value" class="DCmdRow">
                                <div class="DCmdRowLeft">
                                    Value</div>
                                <div class="DCmdRowRight">
                                    <asp:DropDownList ID="ddlDbDPieChart_Value" runat="server" CssClass="CmdDropDown"
                                        onkeydown="return enterKeyFilter(event);">
                                        <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <asp:UpdatePanel ID="updCtrlDbLinkPara" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <input id="btnSaveCtrlDbProp" type="button" value=" Save " class="InputStyle" />
                            <input id="btnCancelCtrlDbProp" type="button" value=" Cancel " onclick="$('#SubProcCtrlDbCmd').dialog('close');"
                                class="InputStyle" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <div id="SubprocDbCmdDetails" class="hide">
        <div>
            <asp:UpdatePanel runat="server" ID="updDbCmdDet" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="Div43">
                        <div>
                            <div class="modalPopUpDetHeaderDiv">
                                <div class="modalPopUpDetHeader">
                                    <asp:Label ID="lblDbCmdName" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="modalPopUpDetRow" style="border: none; padding: 0px 0px 0px 0px;">
                                <div class="modalPopUpDetRow">
                                    <div class="modalPopUpDetColumn1">
                                        Connector
                                    </div>
                                    <div class="modalPopUpDetColumn2">
                                        :
                                    </div>
                                    <div class="modalPopUpDetColumn3">
                                        <asp:Label ID="lblDbConnName" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="modalPopUpDetRow">
                                    <div class="modalPopUpDetColumn1">
                                        Description
                                    </div>
                                    <div class="modalPopUpDetColumn2">
                                        :
                                    </div>
                                    <div class="modalPopUpDetColumn3">
                                        <asp:Label ID="lblDbDescription" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div class="modalPopUpDetRow">
                                    <div class="modalPopUpDetColumn1">
                                        Command Type
                                    </div>
                                    <div class="modalPopUpDetColumn2">
                                        :
                                    </div>
                                    <div class="modalPopUpDetColumn3">
                                        <asp:Label ID="lblDbCmdTyp" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div id="DivDbCmdTbl" runat="server" class="modalPopUpDetRow" visible="false">
                                    <div class="modalPopUpDetColumn1">
                                        Table Name
                                    </div>
                                    <div class="modalPopUpDetColumn2">
                                        :
                                    </div>
                                    <div class="modalPopUpDetColumn3">
                                        <asp:Label ID="lblDbCmdTbl" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div class="modalPopUpDetRow" style="border-bottom: none;">
                                    <div class="modalPopUpDetColumn1">
                                        Sql Query
                                    </div>
                                    <div class="modalPopUpDetColumn2">
                                        :
                                    </div>
                                    <div class="modalPopUpDetColumn3">
                                        <asp:Label ID="lblDbCmdSql" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="SubProcborderDiv">
                            <div class="SubProcBtnDiv" align="center">
                                <input id="Button39" type="button" value=" Close " class="InputStyle" onclick="SubprocDbCmdDetails(false);" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="SubProcCtrlWsCmd" class="hide">
        <div>
            <div>
                <div id="Ctrl_WsCommand" class="DCmdRow" style="width: 400px;">
                    <div class="DCmdRowLeft dcMrg5">
                        Web service Object</div>
                    <div class="DCmdRowRight dcMrg5">
                        <asp:DropDownList ID="ddlCtrlWsCommand" runat="server" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                        </asp:DropDownList>
                    </div>
                    <div class="FLeft dcMrg15">
                        <asp:ImageButton ID="btnCtrlWs_LinkParmeters" runat="server" ToolTip="Set Object Parameters"
                            ImageUrl="~/images/dataObjectInputs.png" CssClass="InputStyle
                            hide" Width="18px" Height="18px" />
                        <asp:LinkButton ID="lnkShowWsCmdDetails" runat="server" ToolTip="View Object Details">
                        <img alt="" src="//enterprise.mficient.com/images/icon/GRAY0295.png" class="dbCmdimg"/></asp:LinkButton>
                    </div>
                </div>
                <div id="divWSIgnoreCache" class="DCmdRow">
                    <div class="DCmdRowLeft" style="margin-top: 2px;">
                        Ignore Cache</div>
                    <div class="DCmdRowRight">
                        <input type="checkbox" id="chk_WSIgnoreCache" class="CmdDropDown" />
                    </div>
                </div>
                <div id="WS_Cntrl_Options" class="hide">
                    <div id="Ctrl_WsText" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Display Text</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlCtrlWsCommand_Displaytxt" runat="server" class="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Tag</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="WS_Label_Command" class="hide">
                    <table class="detailsTable">
                        <thead>
                            <th>
                                Parameter
                            </th>
                            <%-- <th> Data Type </th> <th> Data Format </th> <th>
    Decimal </th> <th> Factor </th>--%>
                            <th>
                                Data Columns
                            </th>
                        </thead>
                        <tbody id="WS_Div_Label_Columns">
                            <tr id="WS_Lbl_Col_1">
                                <td id="WS_Div_Para1" style="padding-left: 34px;">
                                    %1
                                </td>
                                <td id="WS_Div_Para1_Columns">
                                    <asp:DropDownList runat="server" ID="WS_Dp_Para1_Columns" Style="width: 120px;" onkeydown="return enterKeyFilter(event);">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="WS_Lbl_Col_2">
                                <td id="WS_Div_Para2" style="padding-left: 34px;">
                                    %2
                                </td>
                                <td id="WS_Div_Para2_Columns">
                                    <asp:DropDownList runat="server" ID="WS_Dp_Para2_Columns" Style="width: 120px;" onkeydown="return enterKeyFilter(event);">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="WS_Lbl_Col_3">
                                <td id="WS_Div_Para3" style="padding-left: 34px;">
                                    %3
                                </td>
                                <td id="WS_Div_Para3_Columns">
                                    <asp:DropDownList runat="server" ID="WS_Dp_Para3_Columns" Style="width: 120px;" onkeydown="return enterKeyFilter(event);">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="WS_Lbl_Col_4">
                                <td id="WS_Div_Para4" style="padding-left: 34px;">
                                    %4
                                </td>
                                <td id="WS_Div_Para4_Columns">
                                    <asp:DropDownList runat="server" ID="WS_Dp_Para4_Columns" Style="width: 120px;" onkeydown="return enterKeyFilter(event);">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="WS_Lbl_Col_5">
                                <td id="WS_Div_Para5" style="padding-left: 34px;">
                                    %5
                                </td>
                                <td id="WS_Div_Para5_Columns">
                                    <asp:DropDownList runat="server" ID="WS_Dp_Para5_Columns" Style="width: 120px;" onkeydown="return enterKeyFilter(event);">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="WS_Checkbox_Options" class="hide">
                    <div id="Checkbox_WsText" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Checked Value</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlCheckboxWsCommand_CheckedVal" runat="server" class="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Tag</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="WS_Slider_Options" class="hide">
                    <div id="Slider_WsText" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Value</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlSliderWsCommand_Val" runat="server" class="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Tag</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="WS_Dropdown_Options" class="hide">
                    <div id="Dropdown_WSDL_HTTP_Div">
                        <div id="Dp_WsText" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Option Text</div>
                            <div class="DCmdRowRight">
                                <asp:DropDownList ID="ddlWsCommand_Displaytxt" runat="server" class="CmdDropDown"
                                    onkeydown="return enterKeyFilter(event);">
                                    <asp:ListItem Value="-1">Select Tag</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div id="Dp_WsValue" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Option Value</div>
                            <div class="DCmdRowRight">
                                <asp:DropDownList ID="ddlWsCommand_DisplayVal" runat="server" class="CmdDropDown"
                                    onkeydown="return enterKeyFilter(event);">
                                    <asp:ListItem Value="-1">Select Tag</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="WS_List_Options" class="hide">
                    <div id="List_WSDL_HTTP_Div" class="hide">
                        <div id="List_WsRowId" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Row ID</div>
                            <div class="DCmdRowRight">
                                <asp:DropDownList ID="ddlList_Ws_RowId" runat="server" CssClass="CmdDropDown BindValueDdl"
                                    onkeydown="return enterKeyFilter(event);">
                                    <asp:ListItem Value="-1">Select Tag</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div id="List_WsTitle" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Title</div>
                            <div class="DCmdRowRight">
                                <asp:DropDownList ID="ddlList_Ws_Title" runat="server" CssClass="CmdDropDown BindValueDdl"
                                    onkeydown="return enterKeyFilter(event);">
                                    <asp:ListItem Value="-1">Select Tag</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div id="List_WsSubtitle" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Subtitle</div>
                            <div class="DCmdRowRight">
                                <asp:DropDownList ID="ddlList_Ws_Subtitle" runat="server" CssClass="CmdDropDown BindValueDdl"
                                    onkeydown="return enterKeyFilter(event);">
                                    <asp:ListItem Value="-1">Select Tag</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div id="List_WsInfo" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Information</div>
                            <div class="DCmdRowRight">
                                <asp:DropDownList ID="ddlList_Ws_Info" runat="server" CssClass="CmdDropDown BindValueDdl"
                                    onkeydown="return enterKeyFilter(event);">
                                    <asp:ListItem Value="-1">Select Tag</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div id="List_WsAddInfo" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Additional Information</div>
                            <div class="DCmdRowRight">
                                <asp:DropDownList ID="ddlList_Ws_AddInfo" runat="server" CssClass="CmdDropDown BindValueDdl"
                                    onkeydown="return enterKeyFilter(event);">
                                    <asp:ListItem Value="-1">Select Tag</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div id="List_WsImage" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Image Url</div>
                            <div class="DCmdRowRight">
                                <asp:DropDownList ID="ddlList_Ws_Image" runat="server" CssClass="CmdDropDown BindValueDdl"
                                    onkeydown="return enterKeyFilter(event);">
                                    <asp:ListItem Value="-1">Select Tag</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div id="List_WsCountField" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Count Field</div>
                            <div class="DCmdRowRight">
                                <asp:DropDownList ID="ddlList_Ws_CountField" runat="server" CssClass="CmdDropDown BindValueDdl"
                                    onkeydown="return enterKeyFilter(event);">
                                    <asp:ListItem Value="-1">Select Tag</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div id="List_WsSelectedRow" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Selected Row</div>
                            <div class="DCmdRowRight">
                                <asp:DropDownList ID="ddlList_Ws_SelectedRow" runat="server" CssClass="CmdDropDown BindValueDdl"
                                    onkeydown="return enterKeyFilter(event);">
                                    <asp:ListItem Value="-1">Select Tag</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div id="List_WsDate" class="DCmdRow hide" style="width: auto;">
                            <div class="DCmdRowLeft">
                                Date</div>
                            <div class="DCmdRowRight">
                                <asp:DropDownList ID="ddlList_Ws_Date" runat="server" CssClass="CmdDropDown BindValueDdl"
                                    onkeydown="return enterKeyFilter(event);">
                                    <asp:ListItem Value="-1">Select Tag</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="FLeft dcMrg15">
                                <asp:DropDownList ID="ddlList_Ws_DateFormat" runat="server" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                                    <asp:ListItem Value="-1">Select Format</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div id="List_Ws_Rpc_Div" class="hide">
                        <div id="List_RpcRowId" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Row ID</div>
                            <div class="DCmdRowRight" style="display: inline;">
                                <asp:TextBox ID="txtList_Rpc_RowId" runat="server" CssClass="Cmdtxt" onkeydown="return enterKeyFilter(event);"></asp:TextBox>
                                <input id="btnList_Rpc_RowId" type="button" value="..." class="txtPropBtn" onclick="SelectTxtAndValFromRpc(false,0);" />
                            </div>
                        </div>
                        <div id="List_RpcImage" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Image Url</div>
                            <div class="DCmdRowRight" style="display: inline;">
                                <asp:TextBox ID="txtList_Rpc_Image" runat="server" CssClass="Cmdtxt" onkeydown="return enterKeyFilter(event);"></asp:TextBox>
                                <input id="btnList_Rpc_Image" type="button" value="..." class="txtPropBtn" onclick="SelectTxtAndValFromRpc(false,1);" />
                            </div>
                        </div>
                        <div id="List_RpcBoldText" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Title</div>
                            <div class="DCmdRowRight" style="display: inline;">
                                <asp:TextBox ID="txtList_Rpc_Boldtext" runat="server" CssClass="Cmdtxt" onkeydown="return enterKeyFilter(event);"></asp:TextBox>
                                <input id="btnList_Rpc_Boldtext" type="button" value="..." class="txtPropBtn" onclick="SelectTxtAndValFromRpc(false,2);" />
                            </div>
                        </div>
                        <div id="List_RpcSmallText" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Subtitle</div>
                            <div class="DCmdRowRight" style="display: inline;">
                                <asp:TextBox ID="txtList_Rpc_Smalltext" runat="server" CssClass="Cmdtxt" onkeydown="return enterKeyFilter(event);"></asp:TextBox>
                                <input id="btnList_Rpc_Smalltext" type="button" value="..." class="txtPropBtn" onclick="SelectTxtAndValFromRpc(false,3);" />
                            </div>
                        </div>
                        <div id="List_RpcInfo" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Information</div>
                            <div class="DCmdRowRight" style="display: inline;">
                                <asp:TextBox ID="txtList_Rpc_Info" runat="server" CssClass="Cmdtxt" onkeydown="return enterKeyFilter(event);"></asp:TextBox>
                                <input id="btnList_Rpc_Info" type="button" value="..." class="txtPropBtn" onclick="SelectTxtAndValFromRpc(false,5);" />
                            </div>
                        </div>
                        <div id="List_RpcAddInfo" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Additional Information</div>
                            <div class="DCmdRowRight" style="display: inline;">
                                <asp:TextBox ID="txtList_Rpc_AddInfo" runat="server" CssClass="Cmdtxt" onkeydown="return enterKeyFilter(event);"></asp:TextBox>
                                <input id="btnList_Rpc_AddInfo" type="button" value="..." class="txtPropBtn" onclick="SelectTxtAndValFromRpc(false,6);" />
                            </div>
                        </div>
                        <div id="List_RpcCountField" class="DCmdRow hide">
                            <div class="DCmdRowLeft">
                                Count Field</div>
                            <div class="DCmdRowRight" style="display: inline;">
                                <asp:TextBox ID="txtList_Rpc_CountField" runat="server" CssClass="Cmdtxt" onkeydown="return enterKeyFilter(event);"></asp:TextBox>
                                <input id="btnList_Rpc_CountField" type="button" value="..." class="txtPropBtn" onclick="SelectTxtAndValFromRpc(false,4);" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="WS_EditableList_Cmd_Items" class="fl" style="max-height: 300px; overflow-y: auto;
                    overflow-x: hidden; margin-top: 6px;">
                </div>
                <div id="WS_PieChart_Options_Div" class="hide">
                    <div id="WS_PieChart_Title" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Title</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlWsPieChart_Title" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="WS_PieChart_Value" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Value</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlWsPieChart_Value" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="WS_Bar_LineChart_Options_Div" class="hide">
                    <div id="WS_Bar_LineChart_Xaxis" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            X-Axis Data</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlWsBar_LineChart_Xaxis" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="WS_Bar_LineChart_Yaxis" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Y-Axis Data</div>
                        <br />
                        <div style="float: left; width: 500px;">
                            <div id="WSSeriesHeadRow" class="Scrollable">
                                <table class="normaldetailsTable">
                                    <thead>
                                        <tr>
                                            <th>
                                                Series
                                            </th>
                                            <th>
                                                Label
                                            </th>
                                            <th>
                                                Value
                                            </th>
                                            <th>
                                                Scale Factor
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="Bar_Line_WSValue1">
                                            <td id="WSYSeriesCol1">
                                                Series 1
                                            </td>
                                            <td id="WSYLabelCol1">
                                                <input id="txtYWSLabel1" type="text" onkeydown="return
    enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlChartWSCommand_Value" runat="server" Style="width: 120px;"
                                                    onkeydown="return enterKeyFilter(event);">
                                                    <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td id="WSYScaleCol1">
                                                <input id="txtYWSScaleFact1" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                        <tr id="Bar_Line_WSValue2">
                                            <td id="WSYSeriesCol2">
                                                Series 2
                                            </td>
                                            <td id="WSYLabelCol2">
                                                <input id="txtYWSLabel2" type="text" onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlChartWSCommand_Value2" runat="server" Style="width: 120px;"
                                                    onkeydown="return enterKeyFilter(event);">
                                                    <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td id="WSYScaleCol2">
                                                <input id="txtYWSScaleFact2" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                        <tr id="Bar_Line_WSValue3">
                                            <td id="WSYSeriesCol3">
                                                Series 3
                                            </td>
                                            <td id="WSYLabelCol3">
                                                <input id="txtYWSLabel3" type="text" onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlChartWSCommand_Value3" runat="server" Style="width: 120px;"
                                                    onkeydown="return enterKeyFilter(event);">
                                                    <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td id="WSYScaleCol3">
                                                <input id="txtYWSScaleFact3" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                        <tr id="Bar_Line_WSValue4">
                                            <td id="WSYSeriesCol4">
                                                Series 4
                                            </td>
                                            <td id="WSYLabelCol4">
                                                <input id="txtYWSLabel4" type="text" onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlChartWSCommand_Value4" runat="server" Style="width: 120px;"
                                                    onkeydown="return enterKeyFilter(event);">
                                                    <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td id="WSYScaleCol4">
                                                <input id="txtYWSScaleFact4" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                        <tr id="Bar_Line_WSValue5">
                                            <td id="WSYSeriesCol5">
                                                Series 5
                                            </td>
                                            <td id="WSYLabelCol5">
                                                <input id="txtYWSLabel5" type="text" onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlChartWSCommand_Value5" runat="server" Style="width: 120px;"
                                                    onkeydown="return enterKeyFilter(event);">
                                                    <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td id="WSYScaleCol5">
                                                <input id="txtYWSScaleFact5" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="WS_Image_Options" class="hide">
                    <div id="Image_WsText" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Image Source</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlImageWsCommand_Displaytxt" runat="server" class="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Source</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="clear" style="margin-top: 10px;">
                    </div>
                    <div class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Title</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlImageWsCommand_DisplayTitle" runat="server" class="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Title</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="WS_Table_Options_Div">
                    <div id="WS_Table_Options_Mapping_Div" class="fl" style="max-height: 300px; overflow-y: auto;
                        overflow-x: hidden; margin-top: 6px;">
                    </div>
                    <div class="clear" style="margin-top: 10px;">
                    </div>
                    <hr />
                    <div style="margin-top: 10px;">
                        <span class="astric">*&nbsp;&nbsp;&nbsp;</span>0 : Unix Datetime; 1 : .Net Ticks;
                        Or Custom Format (eg. dd/MM/yyyy).
                    </div>
                    <div style="margin-top: 3px;">
                        <span class="astric">**&nbsp;&nbsp;</span>Represents decimal places for numeric
                        data type or datetime format for datatime datatype (eg. dd/MM/yyyy).
                    </div>
                </div>
                <div id="WS_Cylinder_Guage_Div" class="hide">
                    <div id="WS_Cylinder_Guage_Value" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Value</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddl_WS_Cylinder_Guage_Value" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="WS_Cylinder_Guage_MaxVal" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Max Value</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddl_WS_Cylinder_Guage_MaxVal" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="Ws_Location_Options_Div" class="hide">
                    <div id="Ws_Location_Latitude" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Latitude</div>
                        <div class="DCmdRowRight">
                            <select id="ddlWsLocationDataBind_Latitude" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                    <div id="Ws_Location_Longitude" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Longitude</div>
                        <div class="DCmdRowRight">
                            <select id="ddlWsLocationDataBind_Longitude" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                </div>
                <div id="WS_DPieChart_Options_Div" class="hide">
                    <div id="WS_DPieChart_Title" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Slice Title</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlWsDPieChart_Slice_Title" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="WS_DPieChart_Value" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Sub Slice Title</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlWsDPieChart_SubSlice_Title" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="WS_DPieChart_Drilldown_Value" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Value</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlWsDPieChart_Value" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <asp:UpdatePanel ID="updCtrlWsLinkPara" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <input id="btnWsCmdSave" type="button" value=" Save " class="InputStyle" />
                            <input id="btnWsCmdCancel" type="button" value=" Cancel " onclick="$('#SubProcCtrlWsCmd').dialog('close');"
                                class="InputStyle" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <div id="SubprocWsCmdDetails" class="hide">
        <asp:UpdatePanel runat="server" ID="updWsCmdDet" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modalPopUpDetails" style="width: 97%;">
                    <div class="modalPopUpDetHeaderDiv">
                        <div class="modalPopUpDetHeader">
                            <asp:Label ID="lblWsCmdName" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="modalPopUpDetRow" style="border: none; padding: 0px;">
                        <div class="modalPopUpDetRow">
                            <div class="modalPopUpDetColumn1">
                                Connector Name
                            </div>
                            <div class="modalPopUpDetColumn2">
                                :
                            </div>
                            <div class="modalPopUpDetColumn3">
                                <asp:Label ID="lblWsConnName" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="modalPopUpDetRow">
                            <div class="modalPopUpDetColumn1">
                                Service Type
                            </div>
                            <div class="modalPopUpDetColumn2">
                                :
                            </div>
                            <div class="modalPopUpDetColumn3">
                                <asp:Label ID="lblWsSrvTyp" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div id="DivWsWsdlDet" runat="server" class="FLeft hide" style="width: 100%">
                            <div class="modalPopUpDetRow">
                                <div class="modalPopUpDetColumn1">
                                    Service Name
                                </div>
                                <div class="modalPopUpDetColumn2">
                                    :
                                </div>
                                <div class="modalPopUpDetColumn3">
                                    <asp:Label ID="lblWsSrvName" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="modalPopUpDetRow">
                                <div class="modalPopUpDetColumn1">
                                    Description
                                </div>
                                <div class="modalPopUpDetColumn2">
                                    :
                                </div>
                                <div class="modalPopUpDetColumn3">
                                    <asp:Label ID="lblWSDescription" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="modalPopUpDetRow" style="border-bottom: none;">
                                <div class="modalPopUpDetColumn1">
                                    Method
                                </div>
                                <div class="modalPopUpDetColumn2">
                                    :
                                </div>
                                <div class="modalPopUpDetColumn3">
                                    <asp:Label ID="lblWsMethod" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div id="DivWsHttpDet" runat="server" class="modalPopUpDetRow hide" style="padding-left: 0px;">
                            <div class="modalPopUpDetRow">
                                <div class="modalPopUpDetColumn1">
                                    Host Name
                                </div>
                                <div class="modalPopUpDetColumn2">
                                    :
                                </div>
                                <div class="modalPopUpDetColumn3">
                                    <asp:Label ID="lblWsHName" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="modalPopUpDetRow">
                                <div class="modalPopUpDetColumn1">
                                    Url/Parameter Template
                                </div>
                                <div class="modalPopUpDetColumn2">
                                    :
                                </div>
                                <div class="modalPopUpDetColumn3">
                                    <asp:Label ID="lblWsUrl" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="modalPopUpDetRow" style="border-bottom: none;">
                                <div class="modalPopUpDetColumn1">
                                    Output Elements
                                </div>
                                <div class="modalPopUpDetColumn2">
                                    :
                                </div>
                                <div class="modalPopUpDetColumn3">
                                    <asp:Label ID="lblWsOutElm" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div id="DivWsRpcDet" runat="server" class="modalPopUpDetRow hide">
                            <div class="modalPopUpDetRow">
                                <div class="modalPopUpDetColumn1">
                                    Input Parameters
                                </div>
                                <div class="modalPopUpDetColumn2">
                                    :
                                </div>
                                <div class="modalPopUpDetColumn3">
                                    <asp:Label ID="lblWsInParam" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="modalPopUpDetRow" style="border-bottom: none;">
                                <div class="modalPopUpDetColumn1">
                                    Output Parameters
                                </div>
                                <div class="modalPopUpDetColumn2">
                                    :
                                </div>
                                <div class="modalPopUpDetColumn3">
                                    <asp:Label ID="lblWsOutParam" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="SubProcborderDiv">
                        <div class="SubProcBtnDiv" align="center">
                            <input id="Button40" type="button" value=" Close " class="InputStyle" onclick="SubprocWsCmdDetails(false);" />
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="SubProcCtrlODataCmd" class="hide">
        <div>
            <div>
                <div class="DCmdRow" style="width: 400px;">
                    <div class="DCmdRowLeft dcMrg5">
                        OData Object</div>
                    <div class="DCmdRowRight dcMrg5">
                        <asp:DropDownList ID="ddlCtrlOdataCmd" runat="server" CssClass="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                        </asp:DropDownList>
                    </div>
                    <div class="FLeft dcMrg15">
                        <asp:ImageButton ID="btnODataCtrl_LinkParmeters" runat="server" ToolTip="Set Object Parameters"
                            ImageUrl="~/images/dataObjectInputs.png" CssClass="InputStyle hide" Width="18px"
                            Height="18px" />
                        <asp:LinkButton ID="lnkShowOdataCmd" runat="server" ToolTip="View Object Details">
                        <img alt="" src="//enterprise.mficient.com/images/icon/GRAY0295.png" class="dbCmdimg"/></asp:LinkButton>
                    </div>
                </div>
                <div id="divODataIgnoreCache" class="DCmdRow">
                    <div class="DCmdRowLeft" style="margin-top: 2px;">
                        Ignore Cache</div>
                    <div class="DCmdRowRight">
                        <input type="checkbox" id="chk_ODataIgnoreCache" class="CmdDropDown" />
                    </div>
                </div>
                <div id="OData_Ctrl_DisplayText_Div" class="DCmdRow hide">
                    <div class="DCmdRowLeft">
                        Display Text</div>
                    <div class="DCmdRowRight">
                        <asp:DropDownList ID="ddlCtrlODataCmd_DisplayTxt" runat="server" CssClass="CmdDropDown"
                            onkeydown="return enterKeyFilter(event);">
                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div id="OData_Checkbox_DisplayText_Div" class="DCmdRow hide">
                    <div class="DCmdRowLeft">
                        Checked Value</div>
                    <div class="DCmdRowRight">
                        <asp:DropDownList ID="ddlCheckboxODataCmd_CheckedVal" runat="server" CssClass="CmdDropDown"
                            onkeydown="return enterKeyFilter(event);">
                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div id="OData_Slider_DisplayText_Div" class="DCmdRow hide">
                    <div class="DCmdRowLeft">
                        Value</div>
                    <div class="DCmdRowRight">
                        <asp:DropDownList ID="ddlSliderODataCmd_Val" runat="server" CssClass="CmdDropDown"
                            onkeydown="return enterKeyFilter(event);">
                            <asp:ListItem Value="-1">Select Column</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div id="OData_Label_Command" class="hide">
                    <table class="detailsTable">
                        <thead>
                            <th>
                                Parameter
                            </th>
                            <th>
                                Data Columns
                            </th>
                        </thead>
                        <tbody id="OData_Div_Label_Columns">
                            <tr id="OData_Lbl_Col_1">
                                <td id="OData_Div_Para1" style="padding-left: 34px;">
                                    %1
                                </td>
                                <td id="OData_Div_Para1_Columns">
                                    <asp:DropDownList runat="server" ID="OData_Dp_Para1_Columns" Style="width: 120px;"
                                        onkeydown="return enterKeyFilter(event);">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="OData_Lbl_Col_2">
                                <td id="OData_Div_Para2" style="padding-left: 34px;">
                                    %2
                                </td>
                                <td id="OData_Div_Para2_Columns">
                                    <asp:DropDownList runat="server" ID="OData_Dp_Para2_Columns" Style="width: 120px;"
                                        onkeydown="return enterKeyFilter(event);">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="OData_Lbl_Col_3">
                                <td id="OData_Div_Para3" style="padding-left: 34px;">
                                    %3
                                </td>
                                <td id="OData_Div_Para3_Columns">
                                    <asp:DropDownList runat="server" ID="OData_Dp_Para3_Columns" Style="width: 120px;"
                                        onkeydown="return enterKeyFilter(event);">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="OData_Lbl_Col_4">
                                <td id="OData_Div_Para4" style="padding-left: 34px;">
                                    %4
                                </td>
                                <td id="OData_Div_Para4_Columns">
                                    <asp:DropDownList runat="server" ID="OData_Dp_Para4_Columns" Style="width: 120px;"
                                        onkeydown="return enterKeyFilter(event);">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="OData_Lbl_Col_5">
                                <td id="OData_Div_Para5" style="padding-left: 34px;">
                                    %5
                                </td>
                                <td id="OData_Div_Para5_Columns">
                                    <asp:DropDownList runat="server" ID="OData_Dp_Para5_Columns" Style="width: 120px;"
                                        onkeydown="return enterKeyFilter(event);">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="OData_Dropdown_Options_Div" class="hide">
                    <div id="OData_Dropdown_DisplayTxt" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Option Text</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataCommand_DisplayTxt" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="OData_Dropdown_DisplayVal" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Option Value</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataCommand_DisplayVal" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="OData_List_Options_Div" class="hide">
                    <div id="OData_List_RowId" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Row ID</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataList_RowId" runat="server" class="CmdDropDown BindValueDdl"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="OData_List_Title" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Title</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataList_Title" runat="server" class="CmdDropDown BindValueDdl"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="OData_List_SubTitle" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Subtitle</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataList_SubTitle" runat="server" class="CmdDropDown BindValueDdl"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="OData_List_Info" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Information</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataList_Info" runat="server" class="CmdDropDown BindValueDdl"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="OData_List_AddInfo" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Additional Information</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataList_AddInfo" runat="server" class="CmdDropDown BindValueDdl"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="OData_List_ImageName" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Image Source</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataList_Image" runat="server" class="CmdDropDown BindValueDdl"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="OData_List_CountField" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Count Field</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataList_CountField" runat="server" class="CmdDropDown BindValueDdl"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="OData_List_SelectedRow" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Selected Row</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataList_SelectedRow" runat="server" class="CmdDropDown BindValueDdl"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="OData_List_Date" class="DCmdRow hide" style="width: auto;">
                        <div class="DCmdRowLeft">
                            Date</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataList_Date" runat="server" class="CmdDropDown BindValueDdl"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="FLeft dcMrg15">
                            <asp:DropDownList ID="ddlODataList_DateFormat" runat="server" class="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Format</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="OData_EditableList_Options_Div" class="fl" style="max-height: 300px; overflow-y: auto;
                    overflow-x: hidden; margin-top: 6px;">
                </div>
                <div id="OData_PieChart_Options_Div" class="hide">
                    <div id="OData_PieChart_Title" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Title</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataPieChart_Title" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="OData_PieChart_Value" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Value</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataPieChart_Value" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="OData_Bar_LineChart_Options_Div" class="hide">
                    <div id="OData_Bar_LineChart_Xaxis" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            X-Axis Data</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataBar_LineChart_Xaxis" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="OData_Bar_LineChart_Yaxis" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Y-Axis Data</div>
                        <br />
                        <div style="float: left; width: 500px;">
                            <div id="ODataSeriesHeadRow" class="Scrollable">
                                <table class="normaldetailsTable">
                                    <thead>
                                        <tr>
                                            <th>
                                                Series
                                            </th>
                                            <th>
                                                Label
                                            </th>
                                            <th>
                                                Value
                                            </th>
                                            <th>
                                                Scale Factor
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="Bar_Line_ODataValue1">
                                            <td id="ODataYSeriesCol1">
                                                Series 1
                                            </td>
                                            <td id="ODataYLabelCol1">
                                                <input id="txtYODataLabel1" type="text" onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlChartODataCommand_Value" runat="server" Style="width: 120px;"
                                                    onkeydown="return enterKeyFilter(event);">
                                                    <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td id="ODataYScaleCol1">
                                                <input id="txtYODataScaleFact1" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                        <tr id="Bar_Line_ODataValue2">
                                            <td id="ODataYSeriesCol2">
                                                Series 2
                                            </td>
                                            <td id="ODataYLabelCol2">
                                                <input id="txtYODataLabel2" type="text" onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlChartODataCommand_Value2" runat="server" Style="width: 120px;"
                                                    onkeydown="return enterKeyFilter(event);">
                                                    <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td id="ODataYScaleCol2">
                                                <input id="txtYODataScaleFact2" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                        <tr id="Bar_Line_ODataValue3">
                                            <td id="ODataYSeriesCol3">
                                                Series 3
                                            </td>
                                            <td id="ODataYLabelCol3">
                                                <input id="txtYODataLabel3" type="text" onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlChartODataCommand_Value3" runat="server" Style="width: 120px;"
                                                    onkeydown="return enterKeyFilter(event);">
                                                    <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td id="ODataYScaleCol3">
                                                <input id="txtYODataScaleFact3" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                        <tr id="Bar_Line_ODataValue4">
                                            <td id="ODataYSeriesCol4">
                                                Series 4
                                            </td>
                                            <td id="ODataYLabelCol4">
                                                <input id="txtYODataLabel4" type="text" onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlChartODataCommand_Value4" runat="server" Style="width: 120px;"
                                                    onkeydown="return enterKeyFilter(event);">
                                                    <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td id="ODataYScaleCol4">
                                                <input id="txtYODataScaleFact4" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                        <tr id="Bar_Line_ODataValue5">
                                            <td id="ODataYSeriesCol5">
                                                Series 5
                                            </td>
                                            <td id="ODataYLabelCol5">
                                                <input id="txtYODataLabel5" type="text" onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlChartODataCommand_Value5" runat="server" Style="width: 120px;"
                                                    onkeydown="return enterKeyFilter(event);">
                                                    <asp:ListItem Value="-1">Select Column</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td id="ODataYScaleCol5">
                                                <input id="txtYODataScaleFact5" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="OData_Image_Div" class="DCmdRow hide">
                    <div class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Image Source</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlImageODataCmd_DisplayTxt" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Source</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="clear" style="margin-top: 10px;">
                    </div>
                    <div class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Title</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlImageODataCmd_DisplayTitle" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Title</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="OData_Table_Options_Div">
                    <div id="OData_Table_Options_Mapping_Div" class="fl" style="max-height: 300px; overflow-y: auto;
                        overflow-x: hidden; margin-top: 6px;">
                    </div>
                    <div class="clear" style="margin-top: 10px;">
                    </div>
                    <hr />
                    <div style="margin-top: 10px;">
                        <span class="astric">*&nbsp;&nbsp;&nbsp;</span>0 : Unix Datetime; 1 : .Net Ticks;
                        Or Custom Format (eg. dd/MM/yyyy).
                    </div>
                    <div style="margin-top: 3px;">
                        <span class="astric">**&nbsp;&nbsp;</span>Represents decimal places for numeric
                        data type or datetime format for datatime datatype (eg. dd/MM/yyyy).
                    </div>
                </div>
                <div id="Odata_Location_Options_Div" class="hide">
                    <div id="Odata_Location_Latitude" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Latitude</div>
                        <div class="DCmdRowRight">
                            <select id="ddlOdataLocationDataBind_Latitude" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                    <div id="Odata_Location_Longitude" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Longitude</div>
                        <div class="DCmdRowRight">
                            <select id="ddlOdataLocationDataBind_Longitude" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                </div>
                <div id="OData_Cylinder_Guage_Div" class="hide">
                    <div id="OData_Cylinder_Guage_Value" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Value</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddl_OData_Cylinder_Guage_Value" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="OData_Cylinder_Guage_MaxVal" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Max Value</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddl_OData_Cylinder_Guage_MaxVal" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="OData_DPieChart_Options_Div" class="hide">
                    <div id="OData_DPieChart_Title" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Slice Title</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataDPieChart_Slice_Title" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="OData_DPieChart_Value" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Sub Slice Title</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataDPieChart_SubSlice_Title" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="OData_DPieChart_Drilldown_Value" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Value</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddlODataDPieChart_Value" runat="server" CssClass="CmdDropDown"
                                onkeydown="return enterKeyFilter(event);">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <asp:HiddenField ID="hdfCtrlOdataCmdId" runat="server" />
                    <input id="btnODataCmdSave" type="button" value=" Save " class="InputStyle" />
                    <input id="btnODataCmdCancel" type="button" value=" Cancel " onclick="$('#SubProcCtrlODataCmd').dialog('close');"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcBoxCtrlODataCmdDetails" class="hide">
        <asp:UpdatePanel runat="server" ID="UpdCtrlODataCmdDetails" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="Div67">
                    <div class="modalPopUpDetHeaderDiv">
                        <div class="modalPopUpDetHeader">
                            <asp:Label ID="lblCtrlOCmd_Name" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="modalPopUpDetRow" style="border: none; padding: 0px 0px 0px 0px;">
                        <div class="modalPopUpDetRow">
                            <div class="modalPopUpDetColumn1">
                                Connector
                            </div>
                            <div class="modalPopUpDetColumn2">
                                :
                            </div>
                            <div class="modalPopUpDetColumn3">
                                <asp:Label ID="lblCtrlOCmd_Conn" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="modalPopUpDetRow">
                            <div class="modalPopUpDetColumn1">
                                Description
                            </div>
                            <div class="modalPopUpDetColumn2">
                                :
                            </div>
                            <div class="modalPopUpDetColumn3">
                                <asp:Label ID="lblCtrlOCmd_Description" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="modalPopUpDetRow">
                            <div class="modalPopUpDetColumn1">
                                Service EndPoint
                            </div>
                            <div class="modalPopUpDetColumn2">
                                :
                            </div>
                            <div class="modalPopUpDetColumn3">
                                <asp:Label ID="lblCtrlOCmd_EndPoint" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="modalPopUpDetRow">
                            <div class="modalPopUpDetColumn1">
                                Service Version
                            </div>
                            <div class="modalPopUpDetColumn2">
                                :
                            </div>
                            <div class="modalPopUpDetColumn3">
                                <asp:Label ID="lblCtrlOCmd_Version" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="SubProcDbConnBtnDiv">
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="SubProcBoxCtrlOfflineDataObjDetails" class="hide">
        <asp:UpdatePanel runat="server" ID="UpdCtrlOfflineDataObjDetails" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="Div3">
                    <div class="modalPopUpDetHeaderDiv">
                        <div class="modalPopUpDetHeader">
                            <asp:Label ID="lblCtrlOffDataObj_Name" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="modalPopUpDetRow" style="border: none; padding: 0px 0px 0px 0px;">
                        <div class="modalPopUpDetRow">
                            <div class="modalPopUpDetColumn1">
                                Description
                            </div>
                            <div class="modalPopUpDetColumn2">
                                :
                            </div>
                            <div class="modalPopUpDetColumn3">
                                <asp:Label ID="lblCtrlOffDataObj_Description" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="modalPopUpDetRow">
                            <div class="modalPopUpDetColumn1">
                                Object Type
                            </div>
                            <div class="modalPopUpDetColumn2">
                                :
                            </div>
                            <div class="modalPopUpDetColumn3">
                                <asp:Label ID="lblCtrlOffDataObj_Type" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="modalPopUpDetRow">
                            <div class="modalPopUpDetColumn1">
                                Query
                            </div>
                            <div class="modalPopUpDetColumn2">
                                :
                            </div>
                            <div class="modalPopUpDetColumn3">
                                <asp:Label ID="lblCtrlOffDataObj_Query" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="modalPopUpDetRow">
                            <div class="modalPopUpDetColumn1">
                                Offline Tables Used
                            </div>
                            <div class="modalPopUpDetColumn2">
                                :
                            </div>
                            <div class="modalPopUpDetColumn3">
                                <asp:Label ID="lblCtrlOffDataObj_Tables" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="SubProcDbConnBtnDiv">
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="SubProcBoxLinkParameterToCtrl">
        <div class="FLeft">
            <div class="FLeft">
                <div class="FLeft LpHeaderText" style="width: 120px; margin-top: 5px;">
                    Databinding Type</div>
                <div id="LpCmdType" class="FLeft" style="margin-left: 10px; margin-top: 5px;">
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="FLeft">
                <div class="FLeft LpHeaderText" style="width: 120px; margin-top: 5px;">
                    Object Name</div>
                <div id="LpCmdName" class="FLeft" style="margin-left: 10px; margin-top: 5px;">
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="SubProcborderDiv">
        </div>
        <div class="clear">
        </div>
        <div class="ProcCanvasAddDpOption">
            <div id="OuterSaveLpJsonDiv">
                <div id="OuterSaveLpJsonInnerDiv">
                    <div class="LpHeader">
                        <div class="LpTextHeader">
                            <div class="LpHeaderText">
                                Variable
                            </div>
                        </div>
                        <div class="LpValueHeader">
                            <div class="LpHeaderText">
                                Type
                            </div>
                        </div>
                        <div class="LpValueHeader">
                            <div class="LpHeaderText">
                                Value
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div id="SaveLpJsonDiv" class="LpContent">
                    </div>
                </div>
            </div>
            <div id="OuterEditLpJsonDiv" style="min-height: 330px; overflow-y: auto;">
                <div style="min-height: 330px; overflow-y: auto;">
                    <div class="LpHeader" style="background-color: #cdc9c9;">
                        <div class="LpTextHeader">
                            <div class="LpHeaderText">
                                Variable
                            </div>
                        </div>
                        <div class="LpValueHeader">
                            <div class="LpHeaderText">
                            </div>
                        </div>
                        <div class="LpValueHeader">
                            <div class="LpHeaderText">
                                Control
                            </div>
                        </div>
                        <div class="LpValueHeader">
                            <div class="LpHeaderText">
                                Constant
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div id="LPContainerDiv" class="LpContentCtrl">
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div id="LPWsdlContainerDiv" class="LpContentCtrl">
                        <div id="LPWsdlContainerDiv1" class="FLeft LpContentText3">
                        </div>
                        <div id="LPWsdlContainerDiv2" class="FLeft">
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div id="LPRpcContainerDiv" class="LpContentCtrl">
                        <div id="LPRpcContainerDiv1" class="FLeft LpContentText3">
                        </div>
                        <div id="LPRpcContainerDiv2" class="FLeft">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <input id="btnLpPara_Save" type="button" value=" OK " onclick="SubProcBoxLinkParameterToCtrl(false);"
                    class="InputStyle" />
            </div>
        </div>
    </div>
    <div id="SubProcLabelCtrlFormat" class="hide">
        <div>
            <div id="Db_Label_Parameter_Formats">
                <table class="detailsTable">
                    <thead>
                        <th>
                            Parameter
                        </th>
                        <th>
                            Data Type
                        </th>
                        <th>
                            Decimal
                        </th>
                        <th>
                            Factor
                        </th>
                    </thead>
                    <tbody id="Db_Div_Label_Param_Columns">
                        <tr id="Db_Lbl_Col_Param_1">
                            <td id="Db_Div_Param1" style="padding-left: 34px;">
                                %1
                            </td>
                            <td id="Db_Div_Param1_DataType">
                                <select id="Db_Dp_Param1_DataType" onkeydown="return enterKeyFilter(event);">
                                    <option value="0" selected="selected">String</option>
                                    <option value="1">Numeric</option>
                                    <option value="2">Accounting</option>
                                </select>
                            </td>
                            <td id="Db_Div_Param1_DecimalPlaces">
                                <select id="Db_Dp_DecimalPlaces_Param1" disabled="disabled" style="width: 60px;"
                                    onkeydown="return enterKeyFilter(event);">
                                    <option value="0" selected>0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </td>
                            <td id="Db_Div_Param1_MulFactor">
                                <input type="text" id="Db_txt_Param1_MulFactor" disabled="disabled" style="width: 60px;"
                                    onkeydown="return enterKeyFilter(event);" />
                            </td>
                        </tr>
                        <tr id="Db_Lbl_Param_Col_2">
                            <td id="Db_Div_Param2" style="padding-left: 34px;">
                                %2
                            </td>
                            <td id="Db_Div_Param2_DataType">
                                <select id="Db_Dp_Param2_DataType" onkeydown="return enterKeyFilter(event);">
                                    <option value="0" selected="selected">String</option>
                                    <option value="1">Numeric</option>
                                    <option value="2">Accounting</option>
                                </select>
                            </td>
                            <td id="Db_Div_Param2_DecimalPlaces">
                                <select id="Db_Dp_DecimalPlaces_Param2" disabled="disabled" style="width: 60px;"
                                    onkeydown="return enterKeyFilter(event);">
                                    <option value="0" selected="selected">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </td>
                            <td id="Db_Div_Param2_MulFactor">
                                <input type="text" id="Db_txt_Param2_MulFactor" disabled="disabled" style="width: 60px;"
                                    onkeydown="return enterKeyFilter(event);" />
                            </td>
                        </tr>
                        <tr id="Db_Lbl_Col_Param_3">
                            <td id="Db_Div_Param3" style="padding-left: 34px;">
                                %3
                            </td>
                            <td id="Db_Div_Param3_DataType">
                                <select id="Db_Dp_Param3_DataType" onkeydown="return enterKeyFilter(event);">
                                    <option value="0" selected="selected">String</option>
                                    <option value="1">Numeric</option>
                                    <option value="2">Accounting</option>
                                </select>
                            </td>
                            <td id="Db_Div_Param3_DecimalPlaces">
                                <select id="Db_Dp_DecimalPlaces_Param3" disabled="disabled" style="width: 60px;"
                                    onkeydown="return enterKeyFilter(event);">
                                    <option value="0" selected="selected">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </td>
                            <td id="Db_Div_Param3_MulFactor">
                                <input type="text" id="Db_txt_Param3_MulFactor" disabled="disabled" style="width: 60px;"
                                    onkeydown="return enterKeyFilter(event);" />
                            </td>
                        </tr>
                        <tr id="Db_Lbl_Col_Param_4">
                            <td id="Db_Div_Param4" style="padding-left: 34px;">
                                %4
                            </td>
                            <td id="Db_Div_Param4_DataType">
                                <select id="Db_Dp_Param4_DataType" onkeydown="return enterKeyFilter(event);">
                                    <option value="0" selected="selected">String</option>
                                    <option value="1">Numeric</option>
                                    <option value="2">Accounting</option>
                                </select>
                            </td>
                            <td id="Db_Div_Param4_DecimalPlaces">
                                <select id="Db_Dp_DecimalPlaces_Param4" disabled="disabled" style="width: 60px;"
                                    onkeydown="return enterKeyFilter(event);">
                                    <option value="0" selected="selected">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </td>
                            <td id="Db_Div_Param4_MulFactor">
                                <input type="text" id="Db_txt_Param4_MulFactor" disabled="disabled" style="width: 60px;"
                                    onkeydown="return enterKeyFilter(event);" />
                            </td>
                        </tr>
                        <tr id="Db_Lbl_Col_Param_5">
                            <td id="Db_Div_Param5" style="padding-left: 34px;">
                                %5
                            </td>
                            <td id="Db_Div_Param5_DataType">
                                <select id="Db_Dp_Param5_DataType" onkeydown="return enterKeyFilter(event);">
                                    <option value="0" selected="selected">String</option>
                                    <option value="1">Numeric</option>
                                    <option value="2">Accounting</option>
                                </select>
                            </td>
                            <td id="Db_Div_Param5_DecimalPlaces">
                                <select id="Db_Dp_DecimalPlaces_Param5" disabled="disabled" style="width: 60px;"
                                    onkeydown="return enterKeyFilter(event);">
                                    <option value="0" selected="selected">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </td>
                            <td id="Db_Div_Param5_MulFactor">
                                <input type="text" id="Db_txt_Param5_MulFactor" disabled="disabled" style="width: 60px;"
                                    onkeydown="return enterKeyFilter(event);" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <asp:UpdatePanel ID="updLabelCtrlParamFormat" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <input id="btnSaveLabelCtrlParaFormat" type="button" value=" Save " class="InputStyle" />
                            <input id="btnCancelLabelCtrlParaFormat" type="button" value=" Cancel " onclick="$('#SubProcLabelCtrlFormat').dialog('close');"
                                class="InputStyle" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcAddOptionInRbAndChk" class="hide">
        <div id="divProcAddOptionInRbAndChk" class="Scrollable">
            <table class="detailsTable">
                <thead>
                    <tr>
                        <th>
                            Option Text
                        </th>
                        <th>
                            Option Value
                        </th>
                        <th>
                            Default
                        </th>
                        <th class="w_30p">
                        </th>
                        <th class="w_30p">
                        </th>
                    </tr>
                </thead>
                <tbody id="RbContent">
                </tbody>
            </table>
        </div>
        <div class="clear">
        </div>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <div style="text-align: center; margin-top: 15px;">
                    <input id="btnSaveRadioBtnOptions" type="button" value=" Save " class="InputStyle" />
                    <input id="btnCancelRadioBtnOptions" type="button" value=" Cancel " class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="divCtrlsDataBind" class="hide">
        <div>
            <div>
                <div class="DCmdRow" style="width: 400px;">
                    <div class="DCmdRowLeft dcMrg5">
                        <span id="spanCntrlsDatabindDdlObjectLabel">Offline Datatable Object</span></div>
                    <div class="DCmdRowRight dcMrg5">
                        <select id="ddlDatabindObject" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                        </select>
                    </div>
                    <div class="FLeft dcMrg15">
                        <img id="imgCntrlsDatabindObjParameters" class="CursorPointer" src="images/dataObjectInputs.png" alt="Object Parameters" title="Set Object Parameters" height="18" width="18"/>
                        <asp:LinkButton ID="lnkShowOfflineDataObjDetails" runat="server" ToolTip="View Object Details"><img alt="" src="//enterprise.mficient.com/images/icon/GRAY0295.png"
    class="dbCmdimg"/></asp:LinkButton>
                    </div>
                </div>
                <div id="divIgnore_Cache" class="DCmdRow">
                    <div class="DCmdRowLeft" style="margin-top: 2px;">
                        Ignore Cache</div>
                    <div class="DCmdRowRight">
                        <input type="checkbox" id="chk_OfflineIgnoreCache" class="CmdDropDown" />
                    </div>
                </div>
                <div id="DataBind_Ctrl_DisplayText_Div" class="DCmdRow hide">
                    <div class="DCmdRowLeft">
                        Display Text</div>
                    <div class="DCmdRowRight">
                        <select id="ddlCtrlDataBindCmd_DisplayTxt" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                        </select>
                    </div>
                </div>
                <div id="DataBind_Checkbox_DisplayText_Div" class="DCmdRow hide">
                    <div class="DCmdRowLeft">
                        Checked Value</div>
                    <div class="DCmdRowRight">
                        <select id="ddlCheckboxDataBindCmd_CheckedVal" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            <option value="-1">Select Column</option>
                        </select>
                    </div>
                </div>
                <div id="DataBind_Slider_DisplayText_Div" class="DCmdRow hide">
                    <div class="DCmdRowLeft">
                        Value</div>
                    <div class="DCmdRowRight">
                        <select id="ddlSliderDataBindCmd_Val" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                        </select>
                    </div>
                </div>
                <div id="DataBind_Label_Command" class="hide">
                    <table class="detailsTable">
                        <thead>
                            <th>
                                Parameter
                            </th>
                            <%--<th> Data Type </th> <th> Data Format </th> <th>
    Decimal </th> <th> Factor </th>--%>
                            <th>
                                Data Columns
                            </th>
                        </thead>
                        <tbody id="DataBind_Div_Label_Columns">
                            <tr id="DataBind_Lbl_Col_1">
                                <td id="DataBind_Div_Para1" style="padding-left: 34px;">
                                    %1
                                </td>
                                <%-- <td id="DataBind_Div_Para1_DataType"> <select id="DataBind_Dp_Para1_DataType">
    <option value="0" selected="selected">String</option> <option value="1">Integer</option>
    <option value="2">Decimal</option> </select> </td> <td id="DataBind_Div_Para1_DataFormat">
    <select id="DataBind_Dp_Para1_DataFormat" disabled="disabled"> <option value="0"
    selected="selected">Default</option> <option value="1">Accounting</option> </select>
    </td> <td id="DataBind_Div_Para1_DecimalPlaces"> <select id="DataBind_Dp_DecimalPlaces_Para1"
    disabled="disabled" style="width: 60px;"> <option value="0" selected>0</option>
    <option value="1">1</option> <option value="2">2</option> <option value="3">3</option>
    <option value="4">4</option> <option value="5">5</option> <option value="6">6</option>
    <option value="7">7</option> <option value="8">8</option> <option value="9">9</option>
    <option value="10">10</option> </select> </td> <td id="DataBind_Div_Para1_MulFactor">
    <input type="text" id="DataBind_txt_Para1_MulFactor" disabled="disabled" style="width:
    60px;" /> </td>--%>
                                <td id="DataBind_Div_Para1_Columns">
                                    <select id="DataBind_Dp_Para1_Columns" style="width: 120px;" class="CmdDropDown BindValueDdl"
                                        onkeydown="return enterKeyFilter(event);">
                                    </select>
                                </td>
                            </tr>
                            <tr id="DataBind_Lbl_Col_2">
                                <td id="DataBind_Div_Para2" style="padding-left: 34px;">
                                    %2
                                </td>
                                <%--<td id="DataBind_Div_Para2_DataType"> <select id="DataBind_Dp_Para2_DataType">
    <option value="0" selected="selected">String</option> <option value="1">Integer</option>
    <option value="2">Decimal</option> </select> </td> <td id="DataBind_Div_Para2_DataFormat">
    <select id="DataBind_Dp_Para2_DataFormat" disabled="disabled"> <option value="0"
    selected="selected">Default</option> <option value="1">Accounting</option> </select>
    </td> <td id="DataBind_Div_Para2_DecimalPlaces"> <select id="DataBind_Dp_DecimalPlaces_Para2"
    disabled="disabled" style="width: 60px;"> <option value="0" selected="selected">0</option>
    <option value="1">1</option> <option value="2">2</option> <option value="3">3</option>
    <option value="4">4</option> <option value="5">5</option> <option value="6">6</option>
    <option value="7">7</option> <option value="8">8</option> <option value="9">9</option>
    <option value="10">10</option> </select> </td> <td id="DataBind_Div_Para2_MulFactor">
    <input type="text" id="DataBind_txt_Para2_MulFactor" disabled="disabled" style="width:
    60px;" /> </td>--%>
                                <td id="DataBind_Div_Para2_Columns">
                                    <select id="DataBind_Dp_Para2_Columns" style="width: 120px;" class="CmdDropDown BindValueDdl"
                                        onkeydown="return enterKeyFilter(event);">
                                    </select>
                                </td>
                            </tr>
                            <tr id="DataBind_Lbl_Col_3">
                                <td id="DataBind_Div_Para3" style="padding-left: 34px;">
                                    %3
                                </td>
                                <%--<td id="DataBind_Div_Para3_DataType"> <select id="DataBind_Dp_Para3_DataType">
    <option value="0" selected="selected">String</option> <option value="1">Integer</option>
    <option value="2">Decimal</option> </select> </td> <td id="DataBind_Div_Para3_DataFormat">
    <select id="DataBind_Dp_Para3_DataFormat" disabled="disabled"> <option value="0"
    selected="selected">Default</option> <option value="1">Accounting</option> </select>
    </td> <td id="DataBind_Div_Para3_DecimalPlaces"> <select id="DataBind_Dp_DecimalPlaces_Para3"
    disabled="disabled" style="width: 60px;"> <option value="0" selected="selected">0</option>
    <option value="1">1</option> <option value="2">2</option> <option value="3">3</option>
    <option value="4">4</option> <option value="5">5</option> <option value="6">6</option>
    <option value="7">7</option> <option value="8">8</option> <option value="9">9</option>
    <option value="10">10</option> </select> </td> <td id="DataBind_Div_Para3_MulFactor">
    <input type="text" id="DataBind_txt_Para3_MulFactor" disabled="disabled" style="width:
    60px;" /> </td>--%>
                                <td id="DataBind_Div_Para3_Columns">
                                    <select id="DataBind_Dp_Para3_Columns" style="width: 120px;" class="CmdDropDown BindValueDdl"
                                        onkeydown="return enterKeyFilter(event);">
                                    </select>
                                </td>
                            </tr>
                            <tr id="DataBind_Lbl_Col_4">
                                <td id="DataBind_Div_Para4" style="padding-left: 34px;">
                                    %4
                                </td>
                                <%--<td id="DataBind_Div_Para4_DataType"> <select id="DataBind_Dp_Para4_DataType">
    <option value="0" selected="selected">String</option> <option value="1">Integer</option>
    <option value="2">Decimal</option> </select> </td> <td id="DataBind_Div_Para4_DataFormat">
    <select id="DataBind_Dp_Para4_DataFormat" disabled="disabled"> <option value="0"
    selected="selected">Default</option> <option value="1">Accounting</option> </select>
    </td> <td id="DataBind_Div_Para4_DecimalPlaces"> <select id="DataBind_Dp_DecimalPlaces_Para4"
    disabled="disabled" style="width: 60px;"> <option value="0" selected="selected">0</option>
    <option value="1">1</option> <option value="2">2</option> <option value="3">3</option>
    <option value="4">4</option> <option value="5">5</option> <option value="6">6</option>
    <option value="7">7</option> <option value="8">8</option> <option value="9">9</option>
    <option value="10">10</option> </select> </td> <td id="DataBind_Div_Para4_MulFactor">
    <input type="text" id="DataBind_txt_Para4_MulFactor" disabled="disabled" style="width:
    60px;" /> </td>--%>
                                <td id="DataBind_Div_Para4_Columns">
                                    <select id="DataBind_Dp_Para4_Columns" style="width: 120px;" class="CmdDropDown BindValueDdl"
                                        onkeydown="return enterKeyFilter(event);">
                                    </select>
                                </td>
                            </tr>
                            <tr id="DataBind_Lbl_Col_5">
                                <td id="DataBind_Div_Para5" style="padding-left: 34px;">
                                    %5
                                </td>
                                <%-- <td id="DataBind_Div_Para5_DataType"> <select id="DataBind_Dp_Para5_DataType">
    <option value="0" selected="selected">String</option> <option value="1">Integer</option>
    <option value="2">Decimal</option> </select> </td> <td id="DataBind_Div_Para5_DataFormat">
    <select id="DataBind_Dp_Para5_DataFormat" disabled="disabled"> <option value="0"
    selected="selected">Default</option> <option value="1">Accounting</option> </select>
    </td> <td id="DataBind_Div_Para5_DecimalPlaces"> <select id="DataBind_Dp_DecimalPlaces_Para5"
    disabled="disabled" style="width: 60px;"> <option value="0" selected="selected">0</option>
    <option value="1">1</option> <option value="2">2</option> <option value="3">3</option>
    <option value="4">4</option> <option value="5">5</option> <option value="6">6</option>
    <option value="7">7</option> <option value="8">8</option> <option value="9">9</option>
    <option value="10">10</option> </select> </td> <td id="DataBind_Div_Para5_MulFactor">
    <input type="text" id="DataBind_txt_Para5_MulFactor" disabled="disabled" style="width:
    60px;" /> </td>--%>
                                <td id="DataBind_Div_Para5_Columns">
                                    <select id="DataBind_Dp_Para5_Columns" style="width: 120px;" class="CmdDropDown BindValueDdl"
                                        onkeydown="return enterKeyFilter(event);">
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="DataBind_Dropdown_Options_Div" class="hide">
                    <div id="DataBind_Dropdown_DisplayTxt" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Option Text</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDropdownDataBind_DisplayTxt" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                    <div id="DataBind_Dropdown_DisplayVal" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Option Value</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDropdownDataBind_DisplayVal" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                </div>
                <div id="DataBind_List_Options_Div" class="hide">
                    <div id="DataBind_List_RowId" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Row ID</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindList_RowId" class="CmdDropDown BindValueDdl" onkeydown="return
    enterKeyFilter(event);">
                                <option value="-1">Select</option>
                            </select>
                        </div>
                    </div>
                    <div id="DataBind_List_Title" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Title</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindList_Title" class="CmdDropDown
    BindValueDdl" onkeydown="return enterKeyFilter(event);">
                                <option value="-1">Select</option>
                            </select>
                        </div>
                    </div>
                    <div id="DataBind_List_SubTitle" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Subtitle</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindList_SubTitle" class="CmdDropDown BindValueDdl" onkeydown="return enterKeyFilter(event);">
                                <option value="-1">Select</option>
                            </select>
                        </div>
                    </div>
                    <div id="DataBind_List_Info" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Information</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindList_Info" class="CmdDropDown BindValueDdl" onkeydown="return
    enterKeyFilter(event);">
                                <option value="-1">Select</option>
                            </select>
                        </div>
                    </div>
                    <div id="DataBind_List_AddInfo" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Additional Information</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindList_AddInfo" class="CmdDropDown BindValueDdl" onkeydown="return enterKeyFilter(event);">
                                <option value="-1">Select</option>
                            </select>
                        </div>
                    </div>
                    <div id="DataBind_List_ImageName" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Image Source</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindList_Image" class="CmdDropDown BindValueDdl" onkeydown="return
    enterKeyFilter(event);">
                                <option value="-1">Select</option>
                            </select>
                        </div>
                    </div>
                    <div id="DataBind_List_CountField" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Count Field</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindList_CountField" class="CmdDropDown BindValueDdl" onkeydown="return enterKeyFilter(event);">
                                <option value="-1">Select</option>
                            </select>
                        </div>
                    </div>
                    <div id="DataBind_List_SelectedRow" class="DCmdRow hide">
                        <div class="DCmdRowLeft">
                            Selected Row</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindList_SelectedRow" class="CmdDropDown BindValueDdl" onkeydown="return
    enterKeyFilter(event);">
                                <option value="-1">Select</option>
                            </select>
                        </div>
                    </div>
                    <div id="DataBind_List_Date" class="DCmdRow hide" style="width: auto;">
                        <div class="DCmdRowLeft">
                            Date</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindList_Date" class="CmdDropDown BindValueDdl" onkeydown="return
    enterKeyFilter(event);">
                                <option value="-1">Select</option>
                            </select>
                        </div>
                        <div class="FLeft dcMrg15">
                            <select id="ddlDataBindList_DateFormat" runat="server" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                                <option value="-1">Select Format</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div id="DataBind_EditableList_Options_Div" class="fl" style="max-height: 300px;
                    overflow-y: auto; overflow-x: hidden; margin-top: 6px;">
                </div>
                <div id="DataBind_PieChart_Options_Div" class="hide">
                    <div id="DataBind_PieChart_Title" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Title</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindPieChart_Title" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                    <div id="DataBind_PieChart_Value" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Value</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindPieChart_Value" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                </div>
                <div id="DataBind_Bar_LineChart_Options_Div" class="hide">
                    <div id="DataBind_Bar_LineChart_Xaxis" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            X-Axis Data</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindBar_LineChart_Xaxis" class="CmdDropDown BindValueDdl" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                    <div id="DataBind_Bar_LineChart_Yaxis" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Y-Axis Data</div>
                        <br />
                        <div style="float: left; width: 500px;">
                            <div id="DataBindSeriesHeadRow" class="Scrollable">
                                <table class="normaldetailsTable">
                                    <thead>
                                        <tr>
                                            <th>
                                                Series
                                            </th>
                                            <th>
                                                Label
                                            </th>
                                            <th>
                                                Value
                                            </th>
                                            <th>
                                                Scale Factor
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="Bar_Line_DataBindValue1">
                                            <td id="DataBindYSeriesCol1">
                                                Series 1
                                            </td>
                                            <td id="DataBindYLabelCol1">
                                                <input id="txtYDataBindLabel1" type="text" onkeydown="return
    enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <select id="ddlChartDataBindCommand_Value1" style="width: 120px;" class="CmdDropDown BindValueDdl"
                                                    onkeydown="return enterKeyFilter(event);">
                                                </select>
                                            </td>
                                            <td id="DataBindYScaleCol1">
                                                <input id="txtYDataBindScaleFact1" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeypress='mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);' onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                        <tr id="Bar_Line_DataBindValue2">
                                            <td id="DataBindYSeriesCol2">
                                                Series 2
                                            </td>
                                            <td id="DataBindYLabelCol2">
                                                <input id="txtYDataBindLabel2" type="text" onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <select id="ddlChartDataBindCommand_Value2" style="width: 120px;" class="CmdDropDown
    BindValueDdl" onkeydown="return enterKeyFilter(event);">
                                                </select>
                                            </td>
                                            <td id="DataBindYScaleCol2">
                                                <input id="txtYDataBindScaleFact2" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeypress='mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);' onkeydown="return
    enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                        <tr id="Bar_Line_DataBindValue3">
                                            <td id="DataBindYSeriesCol3">
                                                Series 3
                                            </td>
                                            <td id="DataBindYLabelCol3">
                                                <input id="txtYDataBindLabel3" type="text" onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <select id="ddlChartDataBindCommand_Value3" style="width: 120px;" class="CmdDropDown BindValueDdl"
                                                    onkeydown="return enterKeyFilter(event);">
                                                </select>
                                            </td>
                                            <td id="DataBindYScaleCol3">
                                                <input id="txtYDataBindScaleFact3" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeypress='mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);' onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                        <tr id="Bar_Line_DataBindValue4">
                                            <td id="DataBindYSeriesCol4">
                                                Series 4
                                            </td>
                                            <td id="DataBindYLabelCol4">
                                                <input id="txtYDataBindLabel4" type="text" onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <select id="ddlChartDataBindCommand_Value4" style="width: 120px;" class="CmdDropDown
    BindValueDdl" onkeydown="return enterKeyFilter(event);">
                                                </select>
                                            </td>
                                            <td id="DataBindYScaleCol4">
                                                <input id="txtYDataBindScaleFact4" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeypress='mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);' onkeydown="return
    enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                        <tr id="Bar_Line_DataBindValue5">
                                            <td id="DataBindYSeriesCol5">
                                                Series 5
                                            </td>
                                            <td id="DataBindYLabelCol5">
                                                <input id="txtYDataBindLabel5" type="text" onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                            <td>
                                                <select id="ddlChartDataBindCommand_Value5" style="width: 120px;" class="CmdDropDown BindValueDdl"
                                                    onkeydown="return enterKeyFilter(event);">
                                                </select>
                                            </td>
                                            <td id="DataBindYScaleCol5">
                                                <input id="txtYDataBindScaleFact5" type="text" style="width: 40px; margin-left: 12.5px;"
                                                    onkeypress='mFicientIde.MF_VALIDATION.allowOnlyNumeric(event);' onkeydown="return enterKeyFilter(event);" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="DataBind_Image_Div" class="DCmdRow hide">
                    <div class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Image Source</div>
                        <div class="DCmdRowRight">
                            <select id="ddlImageDataBindCmd_Source" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                    <div class="clear" style="margin-top: 10px;">
                    </div>
                    <div class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Title</div>
                        <div class="DCmdRowRight">
                            <select id="ddlImageDataBindCmd_Title" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                </div>
                <div id="DataBind_Table_Options_Div">
                    <div id="DataBind_Table_Options_Mapping_Div" class="fl" style="max-height: 300px;
                        overflow-y: auto; overflow-x: hidden; margin-top: 6px;">
                    </div>
                    <div class="clear" style="margin-top: 10px;">
                    </div>
                    <hr />
                    <div style="margin-top: 10px;">
                        <span class="astric">*&nbsp;&nbsp;&nbsp;</span>0 : Unix Datetime; 1 : .Net Ticks;
                        Or Custom Format (eg. dd/MM/yyyy).
                    </div>
                    <div style="margin-top: 3px;">
                        <span class="astric">**&nbsp;&nbsp;</span>Represents decimal places for numeric
                        data type or datetime format for datatime datatype (eg. dd/MM/yyyy).
                    </div>
                </div>
                <div id="DataBind_Location_Options_Div" class="hide">
                    <div id="DataBind_Location_Latitude" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Latitude</div>
                        <div class="DCmdRowRight">
                            <select id="ddlLocationDataBind_Latitude" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                    <div id="DataBind_Location_Longitude" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Longitude</div>
                        <div class="DCmdRowRight">
                            <select id="ddlLocationDataBind_Longitude" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                </div>
                <div id="Offline_Cylinder_Guage_Div" class="hide">
                    <div id="Offline_Cylinder_Guage_Value" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Value</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddl_Offline_Cylinder_Guage_Value" runat="server" CssClass="CmdDropDown">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div id="Offline_Cylinder_Guage_MaxVal" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Max Value</div>
                        <div class="DCmdRowRight">
                            <asp:DropDownList ID="ddl_Offline_Cylinder_Guage_MaxVal" runat="server" CssClass="CmdDropDown">
                                <asp:ListItem Value="-1">Select Column</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="divDatabindCondLogic" class="g12 border1 mrgt10 hide" style="max-height: 150px;
                    overflow: auto">
                </div>
                <div id="DataBind_DPieChart_Options_Div" class="hide">
                    <div id="DataBind_DPieChart_Slice_Title" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Slice Title</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindPieChart_Slice_Title" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                    <div id="DataBind_DPieChart_SubSlice_Title" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Sub Slice Title</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindDPieChart_SubSlice_Title" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                    <div id="DataBind_DPieChart_Value" class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Value</div>
                        <div class="DCmdRowRight">
                            <select id="ddlDataBindDPieChart_Value" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="btnDataBindCmdSave" type="button" value=" Save " class="InputStyle" />
                    <input id="btnDataBindCmdCancel" type="button" value=" Cancel " onclick="$('#divCtrlsDataBind').dialog('close');"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcBoxAddDropDownOption" class="hide">
        <div style="float: right;">
            <asp:UpdatePanel runat="server" ID="UpdatePanel13" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:LinkButton ID="btnUploadCsvFile" runat="server" Text=" Import File "></asp:LinkButton>
                    <asp:Button ID="btnTestClick" runat="server" Text="Test" OnClick="btnImport_Click"
                        CssClass="hide" />
                    <asp:Button ID="btnImport" runat="server" Text=" Import Uploaded File " CssClass="hide"
                        OnClick="btnImport_Click" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="clear">
        </div>
        <br />
        <div id="ProcBoxAddDropDownOption" class="Scrollable">
            <table class="detailsTable">
                <thead>
                    <tr>
                        <th>
                            Option Text
                        </th>
                        <th>
                            Option Value
                        </th>
                        <th class="w_30p">
                        </th>
                        <th class="w_30p">
                        </th>
                    </tr>
                </thead>
                <tbody id="DpContent">
                </tbody>
            </table>
        </div>
        <div class="clear">
        </div>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <div style="text-align: center; margin-top: 15px;">
                    <input id="btnSaveDp" type="button" value=" Save " class="InputStyle" />
                    <input id="btnAddDpClose" type="button" value=" Cancel " class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcBoxAddListData" class="hide">
        <div id="ProcBoxAddListDataOption" class="Scrollable">
            <table class="detailsTable">
                <thead>
                    <tr>
                        <th>
                            Row ID
                        </th>
                        <th>
                            Title
                        </th>
                        <th>
                            Description
                        </th>
                        <th style="width: 15px !important;">
                        </th>
                        <th class="w_30p">
                        </th>
                    </tr>
                </thead>
                <tbody id="ListContent">
                </tbody>
            </table>
        </div>
        <div class="clear">
        </div>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <div style="text-align: center; margin-top: 15px;">
                    <input id="btnSaveListData" type="button" value=" Save " class="InputStyle" />
                    <input id="btnAddListDataClose" type="button" value=" Cancel " class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcConditionalDisplay" class="hide">
        <div id="divConditionalControl">
        </div>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Button ID="btnCondSave" runat="server" Text=" Save " CssClass="InputStyle" />
                        <asp:Button ID="btnCondCancel" runat="server" Text=" Cancel " CssClass="InputStyle" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div> 
    <div id="SubProcAceEditor" class="aceContainer hide"  style="padding : 0px;">
        <div id="divFormEditors" style="overflow: hidden;">
            <div style="margin-top: 0px; float: right;" id="divFormEditorsEvents">
                <select id="drpFormEvents" style="width: 140px !important;">
                    <option value="0">Common Scripts</option>
                    <option value="1">View Appeared</option>
                    <option value="2">View Reappeared</option>
                    <option value="3">View will Disappear</option>
                    <option value="4">Submit Button</option>
                    <option value="5">Back Button</option>
                    <option value="6">Cancel Button</option>
                    <option value="7">Action Sheet Button</option>
                </select>
            </div>
            <div style="margin-top: 6px; margin-bottom: 20px;" id="divCntrlEditorsEvents">
                <div style="float: left; margin-top: 4px;">
                    Control : <span id="spanOnChngUserDefinedName"></span>
                </div>
                <div style="float: right;">
                    <div style="margin-top: 4px; float: left;">
                        Events :</div>
                    <div style="margin-top: -5px; float: left;">
                        <select id="drpCntrlEvents">
                        </select></div>
                </div>
            </div>
            <br />
            <div class="clear">
            </div>
            <div style="margin-right: 6px; margin-left: 6px;">
                <pre id="jsAceEditor"></pre>
                <div id="jsAceStatusBar">
                    <div class="fr w_80">
                        <div class="fr w_30">
                            <div class="g5 padding0">
                                line : <span id="spanAceEditorLineNo">0</span></div>
                            <div class="g5 padding0">
                                column : <span id="spanAceEditorColumnNo">0</span></div>
                        </div>
                        <div class="border1LeftRight fr w_30">
                            <div class="g5 padding0">
                                length : <span id="spanAceEditorContentLength">0</span></div>
                            <div class="g5 padding0">
                                lines : <span id="spanAceEditorContentLines">0</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--<div id="SubProcCntrlAceEditor" class="aceContainer hide"  style="padding : 0px;">
        <div id="divCntrlEditors" style="overflow: hidden;">
            <div style="margin-top: 6px; margin-bottom: 20px;" id="divCntrlEditorsEvents">
                <div style="float: left; margin-top: 4px;">
                    Control : <span id="spanOnChngUserDefinedName"></span>
                </div>
                <div style="float: right;">
                    <div style="margin-top: 4px; float: left;">
                        Events :</div>
                    <div style="margin-top: -5px; float: left;">
                        <select id="drpCntrlEvents">
                        </select></div>
                </div>
            </div>
            <br />
            <div class="clear">
            </div>
            <div id="divCntrlonChngAceEditor">
                <pre id="jsCntrlonChngAceEditor"></pre>
                <div id="jsCntrlonChngAceStatusBar">
                    <div class="fr w_80">
                        <div class="fr w_30">
                            <div class="g5 padding0">
                                line : <span id="spanCntrlonChngAceEditorLineNo">0</span></div>
                            <div class="g5 padding0">
                                column : <span id="spanCntrlonChngAceEditorColumnNo">0</span></div>
                        </div>
                        <div class="border1LeftRight fr w_30">
                            <div class="g5 padding0">
                                length : <span id="spanCntrlonChngAceEditorContentLength">0</span></div>
                            <div class="g5 padding0">
                                lines : <span id="spanCntrlonChngAceEditorContentLines">0</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>--%>
    <div>
        <asp:Button ID="btnDbConnRowIndex" OnClick="btnDbConnRowIndex_Click" runat="server"
            CssClass="hide" />
    </div>
    <div id="SubProcOnChange" class="hide">
        <div class="ProcCanvasAddDpOption" visible="true" class="Scrollable">
            <table class="detailsTable">
                <thead>
                    <th>
                        Controls
                    </th>
                    <th>
                        Refresh
                    </th>
                </thead>
                <tbody id="OnChangeElmDiv">
                </tbody>
            </table>
        </div>
        <div class="clear">
        </div>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <div style="text-align: center; margin-top: 15px;">
                    <input type="button" id="btnSaveRefreshControls" value=" Save " class="InputStyle" />
                    <input type="button" id="btnCancelRefreshControl" value=" Cancel " class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcAngularGuageData" class="hide">
        <div>
            <div>
                <asp:UpdatePanel ID="updAngularGuageData" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="DCmdRowLeft">
                        </div>
                        <div id="divAngularGuageHeader">
                            <table class="detailsTable">
                                <thead>
                                    <th>
                                        Scale
                                    </th>
                                    <th>
                                        Ticks
                                    </th>
                                    <th>
                                        Minimum
                                    </th>
                                    <%--<th> End </th>--%>
                                </thead>
                                <tbody id="Angular_Guage_Col_div">
                                    <tr id="Angular_Axis1">
                                        <td id="Angular_Axis1_Index">
                                            1
                                        </td>
                                        <td id="Angular_Axis1_TickInterval">
                                            <input id="txt_Angular_Axis1_TickInterval" type="text" value="10" style="width: 60px;"
                                                onkeydown="return enterKeyFilter(event);" />
                                        </td>
                                        <td id="Angular_Axis1_StartVal">
                                            <input id="txt_Angular_Axis1_StartVal" type="text" value="0" style="width: 60px;"
                                                onkeydown="return enterKeyFilter(event);" />
                                        </td>
                                    </tr>
                                    <tr id="Angular_Axis2">
                                        <td id="Angular_Axis2_Index">
                                            2
                                        </td>
                                        <td id="Angular_Axis2_TickInterval">
                                            <input id="txt_Angular_Axis2_TickInterval" type="text" value="10" style="width: 60px;"
                                                onkeydown="return enterKeyFilter(event);" />
                                        </td>
                                        <td id="Angular_Axis2_StartVal">
                                            <input id="txt_Angular_Axis2_StartVal" type="text" value="0" style="width: 60px;"
                                                onkeydown="return enterKeyFilter(event);" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="clear">
                        </div>
                        <div id="Angular_Axis1_Band" class="DCmdRowLeft">
                            <br />
                            <br />
                            Color Bands
                            <br />
                            <br />
                        </div>
                        <div id="Angular_Axis1_BandsHeader">
                            <table class="detailsTable">
                                <thead>
                                    <th>
                                        Start (%)
                                    </th>
                                    <th>
                                        Radius (%)
                                    </th>
                                    <th>
                                        Color
                                    </th>
                                    <th>
                                    </th>
                                    <th>
                                    </th>
                                </thead>
                                <tbody id="Angular_Axis1_Band_div">
                                </tbody>
                            </table>
                        </div>
                        <div class="clear">
                        </div>
                        <div id="Angular_Axis2_Band" class="DCmdRowLeft hide">
                            <br />
                            <br />
                            Color Bands
                            <br />
                            <br />
                        </div>
                        <div id="Angular_Axis2_BandsHeader">
                            <br />
                            <br />
                            <table class="detailsTable">
                                <thead>
                                    <th>
                                        Start(%)
                                    </th>
                                    <th>
                                        Radius (%)
                                    </th>
                                    <th>
                                        Color
                                    </th>
                                    <th>
                                    </th>
                                    <th>
                                    </th>
                                </thead>
                                <tbody id="Angular_Axis2_Band_div">
                                </tbody>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <asp:UpdatePanel ID="updSaveAngularGuageData" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <input id="btnSaveAngularGuageData" type="button" value=" Save " class="InputStyle" />
                            <input id="btnCancelAngularGuageData" type="button" value=" Cancel " onclick="$('#SubProcAngularGuageData').dialog('close');"
                                class="InputStyle" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcFormChange">
        <div>
            <div class="MessageDiv">
                <a id="aFormChangeMsg" class="DefaultCursor">The Form has been modified and there are
                    some changes in the form.
                    <br />
                    Do you want to save it ?</a>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <input id="btnSaveCurrentForm" class="InputStyle" type="button" value=" Yes " onclick="$('#SubProcFormChange').dialog('close');SaveCurrentForm(true);" />
                    <input id="btnfrmDiscard" type="button" value=" No " onclick="$('#SubProcFormChange').dialog('close');closeAndDiscardCurrentForm();"
                        class="InputStyle" />
                    <input id="btnCancelSaveCurrentForm" type="button" value=" Cancel " onclick="$('#SubProcFormChange').dialog('close');"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="subProcScriptConfirmation">
        <div>
            <div class="MessageDiv">
                <a id="a1" class="DefaultCursor">
                    Do you want to save it ?</a>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <input id="btnSaveScript" class="InputStyle" type="button" value=" Yes " />                    
                    <input id="btnDiscartScript" type="button" value=" No " class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcAddNewForm" class="hide">
        <div>
            <div class="DbCmdRow" style="margin-top: 10px;">
                <div class="DbConnLeftDiv">
                    <a>Design Form For.</a>
                </div>
                <div class="DbConnRightDiv">
                    <div class="FLeft">
                        <input id="rdbForFrmPhone" type="radio" name="rdbForFrmTab" checked="checked" onclick="TabletTempRadioClick(this,false);" />
                    </div>
                    <div class="FLeft" style="margin-top: 3px; margin-left: 5px;">
                        Phone
                    </div>
                    <div class="clear">
                    </div>
                    <div class="FLeft">
                        <input id="rdbForFrmTablet" type="radio" name="rdbForFrmTab" onclick="TabletTempRadioClick(this,true);" />
                    </div>
                    <div class="FLeft" style="margin-top: 3px; margin-left: 5px;">
                        Tablet
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="clear">
            </div>
            <div id="AddFormTempDiv" class="hide">
                <div>
                    <div class="DbConnLeftDiv">
                        Section Name
                    </div>
                    <div class="DbConnRightDiv">
                        <input id="txtAddViewSection" type="text" style="width: 150px;" maxlength="20" onkeydown="return enterKeyFilter(event);" />
                    </div>
                </div>
                <div class="DbConnLeftDiv">
                    Section Template
                </div>
                <div class="DbConnRightDiv">
                    <select id="ddlAddVeiwSecTemp" style="width: 150px" onkeydown="return enterKeyFilter(event);">
                        <option value="1">100%</option>
                        <option value="4">34%-66%</option>
                        <option value="5">50%-50%</option>
                        <option value="3">66%-34%</option>
                        <option value="2">33%-34%-33%</option>
                    </select>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="Button15" type="button" value=" Ok " class="InputStyle" onclick="btnAddNewFormClick(true, false);" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcGroupBoxTemp" class="hide">
        <div>
            <div>
                <a>Please enter a unique name for this element.</a>
            </div>
            <div style="margin-top: 10px;">
                <input id="txtGbUserName" type="text" style="width: 240px;" maxlength="20" onkeydown="return enterKeyFilter(event);" />
            </div>
            <div id="SectionNameDiv" class="hide">
                <div>
                    <a>Please select a template.</a>
                </div>
                <div style="margin-top: 10px;">
                    <select id="ddlPanelTemp" style="width: 150px" onkeydown="return enterKeyFilter(event);">
                        <option value="1">100%</option>
                        <option value="2">33%-34%-33%</option>
                        <option value="3">66%-34%</option>
                        <option value="4">34%-66%</option>
                        <option value="5">50%-50%</option>
                    </select>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="Button14" type="button" value=" Ok " class="InputStyle" onclick="btnPanelTempSelect();" />
                    <input id="Button5" type="button" value=" Cancel " onclick="btnSectionCancelClick();"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcChangeTemp" class="hide">
        <div>
            <div id="Div34">
                <div class="DbConnLeftDiv">
                    Section Template
                </div>
                <div class="DbConnRightDiv">
                    <select id="ddlNewSectionTemp" style="width: 150px" onkeydown="return enterKeyFilter(event);">
                    </select>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="btnChangeSecTempClick" type="button" value=" Ok " class="InputStyle" onclick="SubProcTempPromt(true);SubProcChangeTemp(false);" />
                    <input id="btnCancelTempChange" type="button" value=" Cancel " onclick="SubProcChangeTemp(false);"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcTempPromt" class="hide">
        <div>
            <div class="MarginT10">
            </div>
            <div class="MessageDiv">
                Are you sure you want to change this template ?
            </div>
            <div id="warningMsg" class="hide">
                <br />
                <br />
                * Data in right most column will be deleted.
            </div>
            <div class="MarginT10">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel5" UpdateMode="Conditional">
                        <ContentTemplate>
                            <input id="btnChangeTemp" type="button" value=" Yes " class="InputStyle" />
                            <input id="btnCancelChangeTemp" type="button" value=" No " class="InputStyle" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcElementName" class="hide">
        <div>
            <div>
                <a>Please enter a unique name for this element.</a>
            </div>
            <div style="margin-top: 10px;">
                <input id="txtElementUserName" type="text" style="width: 240px;" maxlength="20" onkeydown="return enterKeyFilter(event);" />
            </div>
            <div class="clear">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="btnElementNameOk" type="button" value=" Ok " class="InputStyle" onclick="btnElementNameOkClick();" />
                    <input id="btnElementNameCancel" type="button" value=" Cancel " onclick="btnElementNameCancelClick();"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcBoxDeleteMessage" class="hide">
        <div class="ProcBoxMessage">
            <div style="margin-top: 10px">
            </div>
            <div class="MessageDiv" id="deleteCntrlDiv">
                <a id="a4" class="DefaultCursor">If this control value is used anywhere in the app then
                    it will not work properly.
                    <br />
                    Are you sure you want to continue ?</a>
            </div>
            <div style="margin-top: 10px">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="btnDeleteCntrl" type="button" value=" Yes " onclick="SubProcBoxDeleteMessage(false);DeleteControl();"
                        class="InputStyle" />
                    <input id="btnCancelDeleteCntrl" type="button" value=" No " onclick="SubProcBoxDeleteMessage(false);"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcDeletSpacerOrSepControl" class="hide">
        <div class="ProcBoxMessage">
            <div style="margin-top: 10px">
            </div>
            <div class="MessageDiv" id="Div20">
                <a id="a6" class="DefaultCursor">Delete this Control ?</a>
            </div>
            <div style="margin-top: 10px">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="Button1" type="button" value=" Yes " onclick="SubProcBoxDeleteMessage(false);DeleteControl();"
                        class="InputStyle" />
                    <input id="Button6" type="button" value=" No " onclick="SubProcBoxDeleteMessage(false);"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcBoxDeleteSectionMessage" class="hide">
        <div class="ProcBoxMessage">
            <div style="margin-top: 10px">
            </div>
            <div class="MessageDiv">
                <a id="a5">Do you want to delete this control ?</a>
            </div>
            <div style="margin-top: 10px">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="Button4" type="button" value=" Yes " onclick="SubProcBoxDeleteSectionMessage(false);DeleteSectionControl();"
                        class="InputStyle" />
                    <input id="Button12" type="button" value=" No " onclick="SubProcBoxDeleteSectionMessage(false);"
                        class="InputStyle" />
                </div>
                <div>
                    <input id="hdfDelSection" type="hidden" value="" /></div>
            </div>
        </div>
    </div>
    <div id="SubProcBoxUldWsdlCfm">
        <div>
            <div>
                <asp:HiddenField ID="hdfWsdlUploadId" runat="server" />
            </div>
            <div id="UploadWsdlCfnDiv" style="margin-top: 10px;">
                <a id="a12">Wsdl could not found.Do you want to upload wsdl file mannually ?</a>
            </div>
            <div>
                <div id="GetWsdlMannual" class="DbCmdRow" style="margin-top: 10px; display: none;">
                    <div class="DbConnRightDiv">
                        Only .xml or .txt files are allowed
                    </div>
                    <div class="clear">
                    </div>
                    <div class="DbConnRightDiv">
                        <asp:UpdatePanel runat="server" ID="updWsdlIfrm" UpdateMode="Conditional">
                            <ContentTemplate>
                                <iframe id="Iframe1" runat="server" width="320px" height="55px"></iframe>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div id="Div25" class="SubProcborderDiv">
                        <div class="SubProcBtnMrgn" align="center">
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="MarginT10">
            </div>
            <div id="btnUploadWsdlCfn" class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <input id="Button31" type="button" value=" Yes " onclick="ShowGetWsdlMannualDiv();"
                        class="InputStyle" />
                    <input id="Button32" type="button" value=" No " onclick="SubProcBoxUldWsdlCfm(false);"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcIframe">
        <asp:UpdatePanel runat="server" ID="updUploadCsvFile" UpdateMode="Conditional">
            <ContentTemplate>
                <div>
                    <div id="ProcIframeCanvas">
                        <iframe id="ifUpload" runat="server" width="350px" height="50px"></iframe>
                    </div>
                    <div>
                    </div>
                    <div class="OkBtnBorderTop">
                        <div id="btnUpdFileCancelDiv">
                            <input id="btnHtmUpdFileCancel" type="button" value=" Cancel " onclick="SubProcIframe(false);"
                                class="InputStyle" />
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="SubProcImageUploadIFrame" class="hide">
        <asp:UpdatePanel runat="server" ID="upUploadNewImage" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="ProcImgUpd">
                    <div class="UploadImageDiv">
                        <iframe id="ifUploadImage" runat="server" width="350px" height="50px"></iframe>
                    </div>
                    <div>
                    </div>
                    <div class="SubProcborderDiv">
                        <div id="btnImgUpdCloseDiv" class="SubProcBtnDiv">
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!-- Tanika -->
    <!-- End Pop up Divs -->
    <!--MOHAN --->
    <!-- POP UP DIVS -->
    <div id="divEditableListProps" class="hide">
        <div id="divEditListPropsTableCont" class="g12 mrgb5" style="max-height: 350px; overflow: auto;">
            <table id="tblEditListProps" class="detailsTable">
                <thead>
                    <tr>
                        <th>
                            Primary Key
                        </th>
                        <th class="w_150p">
                            Name
                        </th>
                        <th class="w_180p">
                            Type
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="g12 border1top mrgb5 mrgt5" style="text-align: center">
            <input type="button" id="btnEditListPropsSave" value=" Save " class="InputStyle" />
            <%--<input id="btnAddEditListOption" type="button" value=" Add New Option " class="InputStyle"
    />--%>
            <input type="button" id="btnEditListPropCancel" value=" Cancel " class="InputStyle" />
        </div>
    </div>
    <div id="divEditListItemDispSetting" class="hide">
        <div>
            <div>
                <div class="DCmdCont">
                    <div class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Title Text</div>
                        <div class="DCmdRowRight">
                            <select id="ddlEtlTitleText" class="ddlDpOption" onkeydown="return enterKeyFilter(event);">
                                <option value="-1">Select Text</option>
                            </select>
                        </div>
                    </div>
                    <div class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Prefix</div>
                        <div class="DCmdRowRight">
                            <input id="txtEtlTitlePre" type="text" class="txtDpOption" onkeydown="return enterKeyFilter(event);" />
                        </div>
                    </div>
                    <div class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Suffix</div>
                        <div class="DCmdRowRight">
                            <input id="txtEtlTitleSuff" type="text" class="txtDpOption" onkeydown="return enterKeyFilter(event);" />
                        </div>
                    </div>
                    <div class="DCmdRow">
                        <div class="DCmdRowLeft">
                            Multiplication Factor</div>
                        <div class="DCmdRowRight">
                            <input id="txtEtlTitlefact" type="text" class="txtDpOption" onkeydown="return enterKeyFilter(event);" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <div id="Div32">
                        <input id="btnEditListItemDispSettingSave" type="button" value=" Save " class="InputStyle" />
                        <input id="btnEditListItemDispSettingClose" type="button" value=" Close " class="InputStyle" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="divListCntrlActionBtnSettings" class="hide">
        <div class="modalPopUpDetHeaderDiv hide">
            <div id="RptButtonHead" class="modalPopUpDetHeader">
                Action Button Details
            </div>
        </div>
        <div class="twoColFormW_160">
            <div class="row clearfix mrgb10 mrgt10">
                <div>
                    Text</div>
                <div>
                    <input id="txtRptButtonText" type="text" maxlength="50" value="" class="CmdDropDown"
                        style="height: 15px; width: 147px;" onkeydown="return enterKeyFilter(event);" /></div>
            </div>
            <div class="clear">
            </div>
            <div class="row clearfix mrgb10">
                <div>
                    Refresh List</div>
                <div>
                    <select id="ddlRptBtnRefresh" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select></div>
            </div>
            <div class="clear">
            </div>
            <div class="row clearfix mrgb10">
                <div>
                    Color</div>
                <div>
                    <select id="ddlRptBtnColor" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                        <option value="0">Default</option>
                        <option value="1">Red</option>
                    </select></div>
            </div>
            <div class="clear">
            </div>
            <div class="row clearfix mrgb10">
                <div>
                    Action Type</div>
                <div>
                    <select id="ddlListCntrlActionType" class="CmdDropDown" onkeydown="return enterKeyFilter(event);">
                        <option value="1">Transition</option>
                        <option value="2">Command</option>
                    </select></div>
            </div>
            <div class="clear">
            </div>
            <div style="text-align: center; margin-top: 15px;">
                <input id="btnListActionBtnSettingsSave" type="button" value=" Save " class="InputStyle" />
            </div>
        </div>
    </div>
    <div id="divFormHiddenFieldContainer" class="hide">
        <div id="divDetailsTableContainer">
        </div>
        <div class="clear">
        </div>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <div style="text-align: center; margin-top: 15px;">
                    <input id="btnAddHiddenFieldsToForm" type="button" value=" Save " class="InputStyle" />
                    <input id="btnHidFieldsAdditionCancel" type="button" value=" Cancel " class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="divChildFormHidFieldPropBinding" class="hide">
        <div id="divChildFormHidFldPropBindTableCont" class="g12 mrgb5" style="max-height: 350px;
            overflow: auto;">
            <table id="tblChildFormHdfPropBind" class="detailsTable">
                <thead>
                    <tr>
                        <th>
                            Hidden Variable
                        </th>
                        <th>
                            Property
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="g12 border1top mrgb5 mrgt5" style="text-align: center">
            <input type="button" id="btnChildFormHdfPropBindSave" value=" Save " class="InputStyle" />
            <input type="button" id="btnChildFormHdfPropBindCancel" value=" Cancel " class="InputStyle" />
        </div>
    </div>
    <!-- END POP Up DIVS -->
    <!-- END MOHAN WORK -->
    <div id="SubprocFormContext" class="hide">
        <div class="wfFrmMenuDiv">
            <div id="wfFrmCreateCopyDiv" class="wfFrmMenuIm formMenuMouseOut">
                <div>
                    <img id="img10" alt="delete" src="//enterprise.mficient.com/images/edit.png" title="Create Copy"
                        class="imgwflinkForm" />&nbsp;&nbsp;&nbsp;Create Copy</div>
            </div>
            <div class="WfContextSeprator">
            </div>
        </div>
    </div>
    <div id="divPromptViewNameByType" class="hide">
        <div>
            <div>
                <a id="aViewNameMsg">Please enter a unique name for this view.</a>
            </div>
            <div style="margin-top: 10px;">
                <input id="txtAddViewName" type="text" style="width: 240px;" maxlength="20" onkeydown="return enterKeyFilter(event);" />
            </div>
            <div class="clear">
                <input id="hdfAddViewName" type="hidden" />
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="btnAddViewName" type="button" value=" Ok " class="InputStyle" />
                    <input id="btnAddViewNameCancel" type="button" value=" Cancel " class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="divOnEnterSetting" class="hide">
        <div id="divOnEnterTable" class="onEnterSetting" style="">
        </div>
        <div class="g12 border1top mrgb5 mrgt5" style="text-align: center">
            <input id="btnSaveOnEnter" type="button" value=" Save " class="InputStyle" />
        </div>
    </div>
    <div id="divConnectionTypesCont" class="hide">
        <div id="divConnDeleteMessage" class="hide">
            <span>Uncheck checkboxes to delete event types.</span></div>
        <div>
            <div id="divConnectionTypes" class="g12">
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="g12 border1top mrgb5 mrgt5" style="text-align: center">
            <input type="button" id="btnSaveConnectionInView" value=" Save " class="InputStyle" />
            <input type="button" value=" Cancel " id="btnCancelConnType" class="InputStyle" />
        </div>
    </div>
    <div id="divCommandTasksObjectsView" class="hide">
        <div class="g12 mrgl20" id="divCmdTasksSettingForCont">
            <div class="fr">
                <select id="ddlCmdTasksSettingsFor" style="width: 200px">
                </select>
            </div>
        </div>
        <div class="dialogContentWrapper" id="divCommandObjectsSettingsTab">
            <ul>
                <li id="liCmdDialogSettingsCont"><a href="#divCmdDialogSettingsCont">Alert/Confirm Dialogs</a></li>
                <li id="liCmdTasksObjectsDtls"><a href="#divCommandTasksObjectsDtls">Data Objects</a></li>
                <li id="liOnExitPushNotification"><a href="#divOnExitPushNotification">Push Notification</a></li>
            </ul>
            <div id="divCmdDialogSettingsCont" class="g11 border1 mrgb5" style="padding-left: 2px;">
                <%--<div style="padding-left: 3px;">
                    <h6 class="font-bold">
                        Alert/Confirm Dialogs</h6>
                </div>--%>
                <div id="divCommandsDialogSettings" style="padding-left: 2px;">
                </div>
            </div>
            <div class="g11 border1" id="divCommandTasksObjectsDtls" style="padding-left: 2px;">
                <%--<div style="padding-left: 3px;">
                    <h6 class="font-bold">
                        Execute Data Objects</h6>
                </div>--%>
                <div class="g12 hide" style="background-color: #F2F2F2;">
                    <div class="g4 padding0">
                        <span style="position: relative; top: 5px;">Object Processing Mode</span></div>
                    <div class="g5 padding0">
                        <asp:DropDownList ID="ddlCmdProcessMode" CssClass="w_150p" runat="server" onkeydown="return enterKeyFilter(event);">
                            <asp:ListItem Value="0" Selected="True">Sequential Processing</asp:ListItem>
                            <asp:ListItem Value="1">Batch Processing</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div id="WfOnLoadCmdLinkDiv" class="mrgb5" style="max-height: 200px; overflow: auto;">
                    <table id="tblCommandForTasksView" class="detailsTable">
                        <thead>
                            <tr>
                                <th>
                                    Object
                                </th>
                                <th>
                                    Type
                                </th>
                                <th>
                                </th>
                                <th>
                                    <span id="spanAddCommandForTasks" style="vertical-align: middle; cursor: pointer;">
                                        <img alt="Add Object" src="//enterprise.mficient.com/images/sorting.png" title="Add Object" />
                                    </span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="divOnExitPushNotification" class="g11 border1 mrgt5" style="padding-left: 2px;">
                <%--<div style="padding-left: 3px;">
                    <h6 class="font-bold">
                        Push Notification</h6>
                </div>--%>
                <div>
                    <div class="clearfix mrgb2">
                        <div class="g4 padding0">
                            <span style="position: relative; top: 5px;">Send To</span></div>
                        <div id="divPushNoticeSendTo" class="g8 padding0">
                            <select id="ddlPushNoticeSendTo" class="w_80p" style="float: left; margin-right: 25px;"
                                onkeydown="return enterKeyFilter(event);">
                                <option value="1">User</option>
                                <option value="2">Group</option>
                            </select>
                            <input id="txtPushNoticeSendTo" type="text" class="intlsense" style="width: 75%"
                                onkeydown="return enterKeyFilter(event);" />
                        </div>
                    </div>
                    <div class="clearfix mrgb2">
                        <div class="g4 padding0">
                            <span style="position: relative; top: 5px;">Template</span></div>
                        <div id="divPushNoticeText" class="g8 padding0">
                            <textarea id="txtPushNoticeText" cols="20" rows="2" style="width: 98%;" onkeydown="return enterKeyLinefeed(this.id, event);"></textarea>
                        </div>
                    </div>
                    <div class="clearfix mrgb2">
                        <div class="g4 padding0">
                            <p>
                                Parameter 1 ( %1 )</p>
                            <div id="Div4" class="g9 padding0">
                                <input id="txtVariable1" type="text" class="w_150p intlsense" onkeydown="return enterKeyFilter(event);" />
                            </div>
                        </div>
                        <div class="g4 padding0">
                            <p>
                                Parameter 2 ( %2 )</p>
                            <div id="Div5" class="g9 padding0">
                                <input id="txtVariable2" type="text" class="w_150p intlsense" onkeydown="return enterKeyFilter(event);" />
                            </div>
                        </div>
                        <div class="g4 padding0">
                            <p>
                                Parameter 3 ( %3 )</p>
                            <div id="Div8" class="g9 padding0">
                                <input id="txtVariable3" type="text" class="w_150p intlsense" onkeydown="return enterKeyFilter(event);" />
                            </div>
                        </div>
                        <div class="g4 padding0">
                            <p>
                                Parameter 4 ( %4 )</p>
                            <div id="Div10" class="g9 padding0">
                                <input id="txtVariable4" type="text" class="w_150p intlsense" onkeydown="return enterKeyFilter(event);" />
                            </div>
                        </div>
                        <div class="g4 padding0">
                            <p>
                                Parameter 5 ( %5 )</p>
                            <div id="Div11" class="g9 padding0">
                                <input id="txtVariable5" type="text" class="w_150p intlsense" onkeydown="return enterKeyFilter(event);" />
                            </div>
                        </div>
                        <div class="g4 padding0">
                            <p>
                                Parameter 6 ( %6 )</p>
                            <div id="Div12" class="g9 padding0">
                                <input id="txtVariable6" type="text" class="w_150p intlsense" onkeydown="return enterKeyFilter(event);" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="g12 mrgb5 mrgt5" style="text-align: center">
            <input type="button" id="btnSaveCommandTasksObject" value=" Save " class="InputStyle" />
            <input type="button" id="btnApplyCommandTasksObject" value=" Apply " class="InputStyle" />
            <input type="button" id="btnCmdTasksObjectCancel" value=" Cancel " class="InputStyle" />
        </div>
    </div>
    <div id="divWfOnExitParamMapping" class="hide">
        <div class="dialogContentWrapper">
            <div id="wfProcessCmdsCommandType" class="g12 mrgt5">
                <div class="g12" style="background-color: #F2F2F2;">
                    <div class="g4 padding0">
                        <span style="position: relative; top: 5px;">Object Type</span></div>
                    <div id="ddlWfCmdTypeDiv" class="g5 padding0">
                        <select id="ddlWf_CommandType" class="w_150p" onkeydown="return enterKeyFilter(event);">
                            <option value="1">Database Object</option>
                            <option value="2">Web service Object(WSDL)</option>
                            <option value="3">Web service Object(HTTP)</option>
                            <option value="4">Web service Object(XML-RPC)</option>
                            <option value="5">OData Object</option>
                            <option value="6">Offline Data Objects</option>
                        </select>
                        <%--<asp:DropDownList ID="ddlWf_CommandType" runat="server" CssClass="w_150p" onkeydown="return enterKeyFilter(event);">
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                            <asp:ListItem Value="6"></asp:ListItem>
                        </asp:DropDownList>--%>
                    </div>
                </div>
            </div>
            <div id="WfParaLnkDiv" class="g12 border1 mrgb5 mrgt5">
                <div class="g4 padding0">
                    <span style="position: relative; top: 5px;">Object</span></div>
                <div id="Div1" class="g5 padding0">
                    <asp:DropDownList ID="ddlWf_DbCommand" runat="server" class="w_150p" onkeydown="return enterKeyFilter(event);">
                    </asp:DropDownList>
                </div>
                <div class="g2 padding0">
                    <%--<asp:LinkButton ID="lnkObjectParamMapObjectDtl" runat="server"
    OnClientClick="return false;" ToolTip="View Object Details"></asp:LinkButton>--%>
                    <div id="divObjectParamMapObjectDtl" title="View Object Details">
                        <img alt="" src="//enterprise.mficient.com/images/icon/GRAY0295.png" class="dbCmdimg"
                            style="width: 16px; height: 16px;" />
                    </div>
                </div>
                <div class="clear">
                </div>
                <div id="divOfflineTableCmdType" class="g12 hide">
                    <div class="g4 padding0 mrgl0">
                        <span style="position: relative; top: 5px;">Process Type</span></div>
                    <div id="Div2" class="g5 padding0">
                        <select id="ddlOfflineTblCmdType" class="w_150p" onkeydown="return enterKeyFilter(event);">
                            <option value="1">Select Command</option>
                            <option value="2">Insert Command</option>
                            <option value="3">Update Command</option>
                            <option value="4">Delete Command</option>
                        </select>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div id="divParamsMapping" class="g12 hide" style="max-height: 200px; overflow: auto;">
                </div>
                <div class="clear">
                </div>
                <div id="divOfflineTableConditions" class="g12 hide mrgt20 border1" style="max-height: 100px;
                    overflow: auto;">
                </div>
            </div>
            <div id="divOnExitWsCredential" class="g12 border1 mrgb5 mrgt5 hide">
                <div class="g8" style="padding-left: 3px; padding-top: 0px">
                    <div>
                        <h6>
                            Webservice Credentials</h6>
                    </div>
                    <table style="width: 100%; background: none;">
                        <tr>
                            <td style="width: 180px">
                                <span>Username</span>
                            </td>
                            <td>
                                <input id="txtOnExitParamMapWsUsername" type="text" class="w_150p" onkeydown="return enterKeyFilter(event);" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 180px">
                                <span>Password</span>
                            </td>
                            <td>
                                <input id="txtOnExitParamMapWsPwd" type="text" class="w_150p" onkeydown="return enterKeyFilter(event);" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="g12 border1 mrgb5 mrgt5">
                <div class="g1" style="width: 10px; position: relative; top: 1px;">
                    <input id="chkSaveResultAs" type="checkbox" />
                </div>
                <div class="g3" style="padding-top: 2px;">
                    <span style="position: relative; top: 11px;">Save Result As DataSet</span>
                </div>
                <div class="g5" style="margin-left: 30px;">
                    <input id="txtSaveResultAs" type="text" disabled="disabled" class="w_150p" onkeydown="return enterKeyFilter(event);" />
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
        <div class="g12 border1top mrgb5 mrgt5" style="text-align: center">
            <input type="button" id="btnSaveWfParamMap" value=" Save " class="InputStyle" />
            <input type="button" value=" Cancel " class="InputStyle" id="btnCancelWfParamMap" />
        </div>
    </div>
    <div id="divConditionBoxCondition" class="hide">
        <div id="divConditionDetails" class="g12 mrgb5" style="max-height: 250px; overflow: auto;">
            <table id="tblConditionDetails" class="detailsTable">
                <thead>
                    <tr>
                        <th class="w_180p">
                            Controls To Compare
                        </th>
                        <th class="w_150p">
                            Condition
                        </th>
                        <th class="w_180p">
                            Value / Control
                        </th>
                        <th>
                            Go To View
                        </th>
                        <th>
                            On Submit
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="g12">
            <div class="g3 padding0">
                <span style="position: relative; top: 5px;">Default transition to</span></div>
            <div class="g6 padding0" style="margin-left: 20px;">
                <select id="ddlConditionDefaultTransition" class="txtDmOption w_140p" onkeydown="return enterKeyFilter(event);">
                </select>
            </div>
        </div>
        <div class="g12 border1top mrgb5 mrgt5" style="text-align: center">
            <input type="button" id="btnConditionsSave" value=" Save " class="InputStyle" />
            <input type="button" id="btnConditionsCancel" value=" Cancel " class="InputStyle" />
        </div>
    </div>
    <div id="divAppIconContainer" class="hide">
        <div class="borderNone">
            <div class="borderNone">
                <div style="width: 50px; vertical-align: middle; height: 500px;" class="borderNone FLeft">
                    <div align="left" style="width: 100%; margin-top: 220px;" class="FLeft">
                        <img alt="" id="imgIconSelectPrevious" src="//enterprise.mficient.com/images/imgPrvs.png"
                            style="cursor: pointer;" />
                    </div>
                </div>
                <div align="center" style="vertical-align: middle; height: 500px; width: 800px; max-height: 500px;"
                    class="borderNone FLeft">
                    <div id="IconGallery" align="center" style="vertical-align: middle; margin-top: 25px;">
                    </div>
                    <div class="clear">
                    </div>
                    <div id="IconGalleryText" style="width: 100%; text-align: center;" align="center"
                        class="borderNone FLeft">
                    </div>
                </div>
                <div style="width: 50px; vertical-align: middle; height: 500px;" class="borderNone FLeft">
                    <div align="right" style="width: 100%; margin-top: 220px;">
                        <img alt="" id="imgIconSelectNext" src="//enterprise.mficient.com/images/imgNext.png"
                            style="cursor: pointer;" />
                    </div>
                </div>
            </div>
            <div class="borderNone">
                <input id="hdfStartIconIndex" type="hidden" />
                <input id="hdfEndIconIndex" type="hidden" />
            </div>
        </div>
        <div id="MenuIconOuterDiv" class="hide" style="min-height: 20px">
        </div>
    </div>
    <div id="divTransitionContextMenu" class="hide">
        <div id="wfTransitionRemoveDiv" class="menuLink" style="border: 1px solid #fafafa;">
            <div class="p2p" title="Delete Transition">
                <i class="fa fa-trash-o fa-lg"></i><span class="mrgl15">Delete</span>
            </div>
            <%--<div class="WfContextSepratorDash"> </div>--%>
        </div>
    </div>
    <div id="divTransitionEditContextMenu" class="hide">
        <div id="wfTransitionEditDiv" class="menuLink" style="border: 1px solid #fafafa;">
            <div class="p2p" title="Edit Transition">
                <i class="fa fa-pencil-square-o fa-lg"></i><span class="mrgl15">Edit</span>
            </div>
        </div>
        <div class="WfContextSepratorDash">
        </div>
        <div id="wfTransitionDeleteDiv" class="menuLink" style="border: 1px solid #fafafa;">
            <div class="p2p" title="Delete Transition">
                <i class="fa fa-trash-o fa-lg"></i><span class="mrgl15">Delete</span>
            </div>
        </div>
    </div>
    <div id="divFormCntrlCopyPasteContextMenu" class="hide">
        <div id="divFrmCntrlCopyContextMenu" class="menuLink" style="border: 1px solid #fafafa;">
            <div class="p2p" title="Copy">
                <i class="fa fa-copy fa-lg"></i><span class="mrgl15">Copy</span>
            </div>
        </div>
        <div class="WfContextSepratorDash">
        </div>
        <div id="divFrmCntrlPasteAboveContextMenu" class="menuLink" style="border: 1px solid #fafafa;">
            <div class="p2p" title="Paste Above">
                <i class="fa fa-clipboard fa-lg"></i><span class="mrgl15">Paste Above</span>
            </div>
        </div>
        <div class="WfContextSepratorDash">
        </div>
        <div id="divFrmCntrlPasteBelowContextMenu" class="menuLink" style="border: 1px solid #fafafa;">
            <div class="p2p" title="Paste Below">
                <i class="fa fa-clipboard fa-lg"></i><span class="mrgl15">Paste Below</span>
            </div>
        </div>
        <div class="WfContextSepratorDash">
        </div>
        <div id="divFrmCntrlPasteStyleContextMenu" class="menuLink" style="border: 1px solid #fafafa;">
            <div class="p2p" title="Paste Style">
                <i class="fa fa-clipboard fa-lg"></i><span class="mrgl15">Paste Style</span>
            </div>
        </div>
    </div>
    <div id="divCommitWorkflow" class="hide">
        <asp:UpdatePanel runat="server" ID="updCommitWorkFlow" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin-top: 10px;">
                    <div class="DbCmdRow" style="margin-left: 10px">
                        <asp:Label ID="lblmsg" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="DbCmdRow" style="margin-left: 10px">
                        <div class="DbConnLeftDiv">
                            Version Upgrade
                        </div>
                        <div class="DbConnRightDiv w_60">
                            <div class="fl w_25">
                                <asp:RadioButton ID="rdbAppCommitMinor" runat="server" Text="Minor" GroupName="AppCommit" />
                            </div>
                            <div class="fl w_25">
                                <asp:RadioButton ID="rdbAppCommitMajor" runat="server" Text="Major" GroupName="AppCommit" />
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="DbCmdRow" style="margin-left: 10px">
                        <div class="DbConnLeftDiv">
                            Version History
                        </div>
                        <div class="DbConnRightDiv">
                            <asp:TextBox ID="txtWfVersionHistroy" runat="server" CssClass="InputStyle" TextMode="MultiLine"
                                Width="90%" Rows="5" MaxLength="450" onkeydown="return enterKeyLinefeed(this.id, event);"></asp:TextBox>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="DbCmdRow" style="margin-left: 10px">
                        <div class="DbConnLeftDiv">
                        </div>
                        <div class="DbConnRightDiv">
                            <div class="fl w_60">
                                <asp:CheckBox ID="chkPublishApp" runat="server" Text="Publish this version" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <input type="button" value=" Commit " class="InputStyle" id="btnCommitWorkFlowSave" />
            </div>
        </div>
    </div>
    <div id="divApplicationSaveAs" class="hide">
        <div style="margin-top: 18px;">
            <div class="DbConnLeftDiv">
                App Name :
            </div>
            <div class="DbConnRightDiv">
                <asp:TextBox ID="txtSaveAsApp" runat="server" CssClass="InputStyle" Width="80%" onkeydown="return enterKeyFilter(event);"></asp:TextBox>
            </div>
        </div>
        <div class="clear">
        </div>
        <div style="margin-top: 17px;">
        </div>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <input id="btnAppSaveAs" type="button" value=" Save " class="InputStyle" />
                <input id="btnAppSaveAsCancel" type="button" value=" Cancel " class="InputStyle" />
            </div>
        </div>
    </div>
    <div id="divFormContextMenu" class="hide">
        <div id="divFormDuplicateCont" class="menuLink" style="border: 1px solid #fafafa;">
            <div class="p2p" title="Create Form Copy">
                <div>
                    <i class="fa fa-files-o fa-lg"></i><span class="mrgl15">Create Copy</span>
                </div>
            </div>
        </div>
        <div class="WfContextSepratorDash">
        </div>
        <div id="divFormDeleteContextMenuCont" class="menuLink" style="border: 1px solid #fafafa;">
            <div class="p2p" title="Delete Form">
                <i class="fa fa-trash-o fa-lg"></i><span class="mrgl15">Delete</span>
            </div>
        </div>
    </div>
    <div id="divFormCreateCopy" class="hide">
        <div>
            <div>
                <div class="DbConnLeftDiv">
                    Form Name
                </div>
                <div class="DbConnRightDiv">
                    <asp:TextBox ID="txtFrmCpyName" runat="server" Width="150px" onkeydown="return enterKeyFilter(event);"></asp:TextBox>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="DbConnLeftDiv">
                Application
            </div>
            <div class="DbConnRightDiv">
                <select id="ddlCopyToApp" class="w_155p" style="height: 20px;" onkeydown="return enterKeyFilter(event);">
                </select>
            </div>
            <div class="clear">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <input type="button" id="btnSaveCopyAppForm" value=" Create " class="InputStyle" />
                    <input type="button" value=" Cancel " id="btnCancelSaveCopyAppForm" class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="divTransferAppCont" class="hide">
        <div>
            <div class="DbConnLeftDiv">
                Subadmin
            </div>
            <div class="DbConnRightDiv">
                <select id="ddlTransferAppSubadmin" class="w_155p" style="height: 20px;" onkeydown="return enterKeyFilter(event);">
                </select>
            </div>
            <div class="clear">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <input type="button" id="btnTrasferApp" value=" Transfer " class="InputStyle" />
                    <input type="button" value=" Cancel " id="btnCancelTransferApp" class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="divCommitTesterApp" class="hide">
        <asp:UpdatePanel runat="server" ID="updCommitTesterApp">
            <ContentTemplate>
                <div style="margin-top: 10px;">
                    <div class="DbCmdRow" style="margin-left: 10px">
                        <asp:Label ID="lblCommitTesterAppMsg" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="DbCmdRow" style="margin-left: 10px">
                        <div class="DbConnLeftDiv">
                            Version Upgrade
                        </div>
                        <div class="DbConnRightDiv">
                            <asp:RadioButton ID="radCommitTesterMinor" runat="server" Text="Minor" GroupName="upgrade"
                                Checked="true" />&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="radCommitTesterMajor"
                                    runat="server" GroupName="upgrade" Text="Major" />
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="DbCmdRow" style="margin-left: 10px">
                        <div class="DbConnLeftDiv">
                            Version History
                        </div>
                        <div class="DbConnRightDiv">
                            <asp:TextBox ID="txtCommitTesterDescription" runat="server" CssClass="InputStyle"
                                TextMode="MultiLine" Width="90%" Rows="5" MaxLength="450" onkeydown="return enterKeyLinefeed(this.id, event);"></asp:TextBox>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <input type="button" value=" Commit " class="InputStyle" id="btnCommitTesterAppSave" />
            </div>
        </div>
    </div>
    <div id="SubProcRenewSession" class="hide">
        <div style="margin-left: 10px;">
            <div style="width: 90%; margin: auto;">
                <asp:UpdatePanel runat="server" ID="updLbl" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="margin-top: 10px;">
                            <a>Session expired due to inactivity. Please enter password.</a>
                        </div>
                        <div class="clear">
                        </div>
                        <div style="width: 100%; float: left">
                            <div class="FLeft" style="margin-top: 10px; float: left;">
                                <asp:TextBox ID="txtReEnterPassword" runat="server" TextMode="Password" CssClass="InputStyle"
                                    Width="200px"></asp:TextBox>
                            </div>
                            <div style="float: left; width: auto; margin-left: 3px; margin-top: 10px;">
                                <asp:Button ID="btnRenewSession" runat="server" Text=" Renew Session " OnClick="btnRenewSession_Click"
                                    CssClass="InputStyle" />
                            </div>
                            <div style="margin-top: 5px; margin-left: 3px; clear: both;">
                                <asp:Label ID="lblReAttempt" runat="server" Text="First attempt"></asp:Label>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="clear">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <div style="width: 47%; margin: auto;">
                        <asp:UpdatePanel runat="server" ID="updReNewSession" UpdateMode="Conditional">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnRenewSession" EventName="Click" />
                            </Triggers>
                            <ContentTemplate>
                                <asp:Button ID="btnLogOut" runat="server" Text=" Logout " OnClick="btnLogout_Click"
                                    CssClass="InputStyle" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="divViewActionBtnSettings" class="hide">
        <div id="div16" class="g12 mrgb5" style="max-height: 250px; overflow: auto;">
            <table id="tblViewActionBtnSettings" class="detailsTable">
                <thead>
                    <tr>
                        <th>
                            Name
                        </th>
                        <th>
                            Type
                        </th>
                        <th>
                            On Submit
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="g12 border1top mrgb5 mrgt5" style="text-align: center">
            <input type="button" id="btnViewActionBtnsSave" value=" Save " class="InputStyle" />
            <input type="button" id="btnViewActionBtnCancel" value=" Cancel " class="InputStyle" />
        </div>
    </div>
    <div id="divFormButtonSettings" class="hide">
        <div id="div22" class="g12 mrgb5" style="max-height: 250px; overflow: auto;">
            <table id="tblFormButtonSettings" class="detailsTable">
                <thead>
                    <tr>
                        <th>
                            Name
                        </th>
                        <th>
                            Type
                        </th>
                        <th>
                            On Submit
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="g12 border1top mrgb5 mrgt5" style="text-align: center">
            <input type="button" id="btnFormButtonExitTasksSave" value=" Save " class="InputStyle" />
            <input type="button" id="btnFormButtonExitTasksCancel" value=" Cancel " class="InputStyle" />
        </div>
    </div>
    <div id="divAppCloseConfirm" class="hide">
        <div>
            <div class="MessageDiv">
                <a class="DefaultCursor" style="margin-left: 20px;">Close this App ?</a>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <input id="btnSaveAndCloseApp" class="InputStyle" type="button" value=" Save & Close " />
                    <input id="btnCloseApp" type="button" value=" Close " class="InputStyle" />
                    <input id="btnCancelAppClose" type="button" value=" Cancel " class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="divOpenFormConfirmation" class="hide">
        <div>
            <div class="MessageDiv">
                <a class="DefaultCursor" style="margin-left: 20px;">Save changes ?</a>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <input id="btnSaveFormAndOpenOther" class="InputStyle" type="button" value=" Yes " />
                    <input id="btnCloseFormAndOpenOther" type="button" value=" No " class="InputStyle" />
                    <input id="btnCancelOpenForm" type="button" value=" Cancel " class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="divAppRestrictionsAvailabilityTime" class="hide">
        <div class="g12 p2p">
            <select id="ddlAppRestrictAvailableOnOrNotCriteria">
                <option value="1">Available On</option>
                <option value="2">Not Available On</option>
            </select>
        </div>
        <div class="g12 p2p">
            <div class="g12 p2p">
                <p>
                    Days</p>
            </div>
            <div class="g12 p2p">
                <div class="g2 p2p">
                    <input type="checkbox" name="AppRestrictionsDay" value="1" />Mon</div>
                <div class="g2 p2p">
                    <input type="checkbox" name="AppRestrictionsDay" value="2" />Tue</div>
                <div class="g2 p2p">
                    <input type="checkbox" name="AppRestrictionsDay" value="3" />Wed</div>
                <div class="g2 p2p">
                    <input type="checkbox" name="AppRestrictionsDay" value="4" />Thu</div>
            </div>
            <div class="g12 p2p">
                <div class="g2 p2p">
                    <input type="checkbox" name="AppRestrictionsDay" value="5" />Fri</div>
                <div class="g2 p2p">
                    <input type="checkbox" name="AppRestrictionsDay" value="6" />Sat</div>
                <div class="g2 p2p">
                    <input type="checkbox" name="AppRestrictionsDay" value="7" />Sun</div>
            </div>
        </div>
        <div class="g12 p2p">
            <div class="g3">
                <span>From Time :</span></div>
            <div class="g4">
                <select id="ddlAppRestrictionFromTimeHr" class="w_90p">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                </select>
                <span>hr</span>
            </div>
            <div class="g4">
                <select id="ddlAppRestrictionFromTimeMin" class="w_90p">
                    <option value="0">0</option>
                    <option value="15">15</option>
                    <option value="30">30</option>
                    <option value="45">45</option>
                </select>
                <span>min</span>
            </div>
        </div>
        <div class="g12 p2p">
            <div class="g3">
                <span>To Time :</span></div>
            <div class="g4">
                <select id="ddlAppRestrictionToTimeHr" class="w_90p">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                </select>
                <span>hr</span>
            </div>
            <div class="g4">
                <select id="ddlAppRestrictionToTimeMin" class="w_90p">
                    <option value="0">0</option>
                    <option value="15">15</option>
                    <option value="30">30</option>
                    <option value="45">45</option>
                </select>
                <span>min</span>
            </div>
        </div>
        <div class="g12 SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <input id="btnAppAvailabilitySave" type="button" value=" Save " class="InputStyle" />
                <input id="btnAppAvailabilityCancel" type="button" value=" Cancel " class="InputStyle" />
            </div>
        </div>
    </div>
    <div id="divAppRestrictionsWiFiCont" class="hide">
        <div id="divAppRestrictionsWiFi" style="max-height: 300px; overflow: auto;">
        </div>
        <div class="clear">
        </div>
        <div class="g12 border1top mrgb5 mrgt5" style="text-align: center">
            <input type="button" id="btnAppRestrictionsWiFiSave" value=" Save " class="InputStyle" />
            <input type="button" value=" Cancel " id="btnAppRestrictionsWiFiCancel" class="InputStyle" />
        </div>
    </div>
    <!-- End Pop up Divs
    -->
    <!-- Hidden Fields -->
    <div id="divHiddenFields" style="display: none">
        <asp:UpdatePanel ID="updHiddenFields" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:HiddenField ID="hdfIconname" runat="server" />
                <asp:HiddenField ID="hdfWFparentId" runat="server" />
                <asp:HiddenField ID="hdfWfIsTablet" runat="server" Value="0" />
                <asp:HiddenField ID="hdfWfSingleRecord" runat="server" Value="0" />
                <asp:HiddenField ID="hdfCmdIds" runat="server" />
                <asp:HiddenField ID="hdfModalType" runat="server" Value="0" />
                <asp:HiddenField ID="hidAllForms" runat="server" Value="0" />
                <asp:HiddenField ID="hidAllWfs" runat="server" Value="0" />
                <asp:HiddenField ID="hidAllMaster" runat="server" Value="0" />
                <asp:HiddenField ID="hidAllDbCommands" runat="server" Value="" />
                <asp:HiddenField ID="hidAllWsCommands" runat="server" Value="" />
                <asp:HiddenField ID="hidAllOdataCommands" runat="server" Value="" />
                <asp:HiddenField ID="hidUICurrentWorkingAppDtl" runat="server" Value="" />
                <asp:HiddenField ID="hidAllIconsList" runat="server" Value="" />
                <asp:HiddenField ID="hidAppsList" runat="server" />
                <asp:HiddenField ID="hidDtlsForPostBackByHtmlCntrl" runat="server" />
                <asp:HiddenField ID="hidPhoneAppForSaving" runat="server" />
                <asp:HiddenField ID="hidTabletAppForSaving" runat="server" />
                <input type="hidden" id="hdfElementName" />
                <input type="hidden" id="hdfEditListItem" />
                <asp:HiddenField ID="hdfIsEditList" runat="server" />
                <asp:HiddenField ID="hdfCurrentFormId" runat="server" />
                <asp:HiddenField ID="hdfAllCtrlInFrm" runat="server" />
                <asp:HiddenField ID="hdfBindingtype" runat="server" />
                <asp:HiddenField ID="hdfImages" runat="server" />
                <asp:HiddenField ID="hdfCompanyId" runat="server" />
                <asp:HiddenField ID="hdfIsChildForm" runat="server" />
                <asp:HiddenField ID="hdfEditableListId" runat="server" />
                <asp:HiddenField ID="hidCompanyAdditionalDtls" runat="server" />
                <asp:HiddenField ID="hidCompanyUsers" runat="server" />
                <asp:HiddenField ID="hidCompanyGroups" runat="server" />
                <asp:HiddenField ID="hidCompanySubAdmins" runat="server" />
                <asp:HiddenField ID="hidmPluginAgents" runat="server" />
                <asp:HiddenField ID="hidAWSCredentials" runat="server" />
                <asp:HiddenField ID="HiddenField2" runat="server" />
                <asp:HiddenField ID="hidFormForSaving" runat="server" />
                <asp:HiddenField ID="hidAllOfflineDts" runat="server" Value="" />
                <img id="img_tool" class="hide" />
                <asp:HiddenField ID="hidDragDropFileBytes" runat="server" />
                <asp:HiddenField ID="hidDragDropFileName" runat="server" />
                <asp:HiddenField ID="hidTabSelected" runat="server" />
                <asp:HiddenField ID="hidAppTreeView" runat="server" />
                <asp:HiddenField ID="hidAppFolderSelected" runat="server" />
                <asp:HiddenField ID="hidAppFileSelected" runat="server" />
                <asp:HiddenField ID="hidSelectedFileContent" runat="server" />
                <asp:HiddenField ID="hidHtml5AppDetail" runat="server" />
                <asp:HiddenField ID="hidHtml5AppFileDetail" runat="server" />
                <asp:HiddenField ID="hidAppZipEntriesList" runat="server" />
                <asp:HiddenField ID="hidAppZipFolders" runat="server" />
                <asp:HiddenField ID="hidHtml5AppRootHtmlFiles" runat="server" />
                <asp:HiddenField ID="hidFoldersForFileMove" runat="server" />
                <asp:HiddenField ID="hidHtml5AppFileMoveData" runat="server" />
                <asp:HiddenField ID="hidHtml5AppZipDownloadPath" runat="server" />
                <asp:HiddenField ID="hTmpAppFileK" runat="server" />
                <asp:HiddenField ID="hidAllOfflineDataObjects" runat="server" />
                <asp:HiddenField ID="hidOpenedArchivedApp" runat="server" />
                <asp:HiddenField ID="hidAllJqueryMobIcons" runat="server" Value="" />
                <asp:HiddenField ID="hidWiFiSSIDs" runat="server" Value="" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!-- End Hidden Fields -->
    <!--
    POST BACK BUTTON FOR HTML CONTROLS-->
    <div class="hide">
        <asp:UpdatePanel ID="updPostbackByHtmlCntrl" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnPostbackByHtmlCntrl" runat="server" Text="" OnClick="btnPostbackByHtmlCntrl_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!--End POST BACK BUTTON FOR HTML CONTROLS -->
    <!--WAIT BOX =-->
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <!-- END WAIT BOX -->
    <div id="divHtml5AppDownload">
    </div>
    <asp:HiddenField ID="hfs" runat="server" />
    <asp:HiddenField ID="hfbid" runat="server" />
    <div id="divDebug" class="hide" style="height: 300px; overflow: scroll; background-color: White">
        <textarea id="txtDivDebug" style="width: 100%; height: 100%;"> </textarea>
    </div>
    <script type="text/javascript">
        //Form scripts Editor
        $('#jsAceEditor').css('height', $(window).height() - 105);
        $('#jsCntrlonChngAceEditor').css('height', $(window).height() - 105);

        var useWebWorker = window.location.search.toLowerCase().indexOf('noworker') == -1;

        var jsaceEditor = ace.edit("jsAceEditor");
        jsaceEditor.getSession().setUseWorker(useWebWorker);
        
        jsaceEditor.setTheme("ace/theme/vibrant_ink");
        jsaceEditor.getSession().setMode("ace/mode/javascript");
        jsaceEditor.getSession().setUseWrapMode(true);
        jsaceEditor.getSession().setWrapLimitRange(null, null);
        jsaceEditor.setShowPrintMargin(false);
        jsaceEditor.setDisplayIndentGuides(true);      

        jsaceEditor.getSession().on('change', function (e) {
            _jsAceEditor.changeEventHandler();
        });
        jsaceEditor.getSession().selection.on('changeCursor', function (e) {
            _jsAceEditor.changeCursorEventHandler();
        });

        //Control scripts Editor
//        var jsCntrlonChngaceEditor = ace.edit("jsCntrlonChngAceEditor");
//        jsCntrlonChngaceEditor.setTheme("ace/theme/vibrant_ink");
//        jsCntrlonChngaceEditor.getSession().setMode("ace/mode/javascript");
//        jsCntrlonChngaceEditor.getSession().setUseWrapMode(true);
//        jsCntrlonChngaceEditor.getSession().setWrapLimitRange(null, null);
//        jsCntrlonChngaceEditor.setShowPrintMargin(false);
//        jsCntrlonChngaceEditor.getSession().on('change', function (e) {
//            _jsAceEditorOnChang.changeEventHandler();
//        });
//        jsCntrlonChngaceEditor.getSession().selection.on('changeCursor', function (e) {
//            _jsAceEditorOnChang.changeCursorEventHandler();
//        });
//        jsCntrlonChngaceEditor.setOptions({
//            enableBasicAutocompletion: true,
//            enableLiveAutocompletion: true
//        });

        ace.config.loadModule('ace/ext/tern', function () {
            jsaceEditor.setOptions({
                /**
                * Either `true` or `false` or to enable with custom options pass object that
                * has options for tern server: http://ternjs.net/doc/manual.html#server_api
                * If `true`, then default options will be used
                */
                enableTern: {
                    /* http://ternjs.net/doc/manual.html#option_defs */
                    defs: ['browser', 'ecma5', 'jquery', 'mFicient'],
                    /* http://ternjs.net/doc/manual.html#plugins */
                    plugins: {
                        doc_comment: {
                            fullDocs: true
                        }
                    },
                    /**
                    * (default is true) If web worker is used for tern server.
                    * This is recommended as it offers better performance, but prevents this from working in a local html file due to browser security restrictions
                    */
                    useWorker: useWebWorker,
                    /* if your editor supports switching between different files (such as tabbed interface) then tern can do this when jump to defnition of function in another file is called, but you must tell tern what to execute in order to jump to the specified file */
                    switchToDoc: function (name, start) {
                        console.log('switchToDoc called but not defined. name=' + name + '; start=', start);
                    },
                    /**
                    * if passed, this function will be called once ternServer is started.
                    * This is needed when useWorker=false because the tern source files are loaded asynchronously before the server is started.
                    */
                    startedCb: function () {
                        //once tern is enabled, it can be accessed via editor.ternServer
                        console.log('editor.ternServer:', editor.ternServer);
                    }
                },
                /**
                * when using tern, it takes over Ace's built in snippets support.
                * this setting affects all modes when using tern, not just javascript.
                */
                enableSnippets: true,
                /**
                * when using tern, Ace's basic text auto completion is enabled still by deafult.
                * This settings affects all modes when using tern, not just javascript.
                * For javascript mode the basic auto completion will be added to completion results if tern fails to find completions or if you double tab the hotkey for get completion (default is ctrl+space, so hit ctrl+space twice rapidly to include basic text completions in the result)
                */
                enableBasicAutocompletion: true
            });

//            jsCntrlonChngaceEditor.setOptions({
//                /**
//                * Either `true` or `false` or to enable with custom options pass object that
//                * has options for tern server: http://ternjs.net/doc/manual.html#server_api
//                * If `true`, then default options will be used
//                */
//                enableTern: {
//                    /* http://ternjs.net/doc/manual.html#option_defs */
//                    defs: ['browser', 'ecma5'],
//                    /* http://ternjs.net/doc/manual.html#plugins */
//                    plugins: {
//                        doc_comment: {
//                            fullDocs: true
//                        }
//                    },
//                    /**
//                    * (default is true) If web worker is used for tern server.
//                    * This is recommended as it offers better performance, but prevents this from working in a local html file due to browser security restrictions
//                    */
//                    useWorker: useWebWorker,
//                    /* if your editor supports switching between different files (such as tabbed interface) then tern can do this when jump to defnition of function in another file is called, but you must tell tern what to execute in order to jump to the specified file */
//                    switchToDoc: function (name, start) {
//                        console.log('switchToDoc called but not defined. name=' + name + '; start=', start);
//                    },
//                    /**
//                    * if passed, this function will be called once ternServer is started.
//                    * This is needed when useWorker=false because the tern source files are loaded asynchronously before the server is started.
//                    */
//                    startedCb: function () {
//                        //once tern is enabled, it can be accessed via editor.ternServer
//                        console.log('editor.ternServer:', editor.ternServer);
//                    }
//                },
//                /**
//                * when using tern, it takes over Ace's built in snippets support.
//                * this setting affects all modes when using tern, not just javascript.
//                */
//                enableSnippets: true,
//                /**
//                * when using tern, Ace's basic text auto completion is enabled still by deafult.
//                * This settings affects all modes when using tern, not just javascript.
//                * For javascript mode the basic auto completion will be added to completion results if tern fails to find completions or if you double tab the hotkey for get completion (default is ctrl+space, so hit ctrl+space twice rapidly to include basic text completions in the result)
//                */
//                enableBasicAutocompletion: true
//            });
        });
    </script>
    <script type="text/javascript">
        var isLoaded = false;
        var startTime = Date.now();
        var intervalId = 0;
        Sys.Application.add_init(application_init);

        //showWaitModal();
        function application_init() {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
            //Pace.start();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            hideWaitModal();
            //Pace.stop();
            isCookieCleanUpRequired('true');
        }
        function doIfAllScriptsNotLoaded() {
            if (typeof jsmfIdeScript == "undefined"
                || typeof jsmfIdeApp == "undefined"
                || typeof mfIdeClasses == "undefined"
                || typeof mfIdeChildForm == "undefined"
                || typeof mfIdeDesigner == "undefined"
                || typeof mIdeChartControls == "undefined"
                || typeof mfIdeSimpleCntrls == "undefined"
                || typeof mfIdeSelectorControls == "undefined"
                || typeof mfIdeListControl == "undefined"
                || typeof mfIdeForm == "undefined"
                || typeof mfIdeEditList == "undefined"
                || typeof mfIdeTableControl == "undefined"
                || typeof mfIdeAdvanControls == "undefined") {

                $('#divWorkflowListContainer').hide();
                if ($('#divGetApplicationName').is(':visible'))
                    $('#divGetApplicationName').dialog('close');

                //alert("Could not load mPower.Please try again.");
                showMessage("Could not load mPower.Please try again.", DialogType.Error);
            }
        }
    </script>
</asp:Content>

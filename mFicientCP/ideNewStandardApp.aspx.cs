﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using System.Collections;
using Ionic.Zip;
using System.Drawing;
using System.Drawing.Drawing2D;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
namespace mFicientCP
{
    public partial class ideNewStandardApp : System.Web.UI.Page
    {
        string SubAdminid = "", CompanyId = "", Sessionid = "", Adminid = "";
        enum PAGE_MODE
        {
            NEW = 1,
            EDIT = 2
        }
        enum APP_LIST_SOURCE
        {
            Current = 0,
            Archived = 1
        }
        enum POST_BACK_BY_HTML_PROCESS
        {
            GetAppDetails = 1,
            AppSave = 2,
            AppCommit = 3,
            AppCommitSave = 4,
            AddNewApp = 5,
            AppSaveAs = 6,
            FormCreateCopy = 7,
            TransferApp = 8,
            DeleteApp = 9,
            TesterAppCommit = 10,
            TesterAppCommitSave = 11,
            NewHtml5FileAdd = 12,
            Html5ZipFileAdd = 13,
            ViewHtml5AppFile = 14,
            Html5AppFileEditSave = 15,
            AddNewHtml5App = 16,
            UpdateHtml5App = 17,
            Html5AppSaveAs = 18,
            Html5AppCommit = 19,
            Html5AppCommitSave = 20,
            Html5AppTesterCommit = 21,
            Html5AppDelete = 22,
            Html5AppClose = 23,
            Html5AppFileMove = 24,
            Html5AppZipDownload = 25,
            Html5AppAddNewFolder = 26,
            Html5AppTreeViewRefresh = 27,
            Html5AppFileDelete = 28,
            SaveAndCloseApp = 29,
            GetArchivedApp = 30,
            SaveArchivedAppAsWorkingCopy = 31
        }
        PAGE_MODE _pageMode = PAGE_MODE.NEW;
        MF_APP_TYPE _appType = MF_APP_TYPE.StandardApp;
        //we have two links in menu for showing apps list.
        //One is the normal app list in the current table
        //Other is the list for committed apps.Sources are different for these lists.Using this variable for this purpose.
        string strhfs, strhfbid, strPageMode = "", strApplicationType = "", strAppListSourceType = "";
        const string Html5RootFolder = "root";
        const char Html5FilePathSeparator = '/';
        string bucketName = "mficient-apps";
        string publicBucketName = "mficient-public";
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            MasterPage mainMaster = Page.Master;
            if (Page.IsPostBack)
            {
                strhfs = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
                strPageMode = ((HiddenField)mainMaster.FindControl("hidAppNewOrEdit")).Value;
                strApplicationType = ((HiddenField)mainMaster.FindControl("hidCreateNewAppType")).Value;
                strAppListSourceType = ((HiddenField)mainMaster.FindControl("hidAppListSource")).Value;
            }
            else
            {
                // System.Web.UI.Page previousPage = Page.PreviousPage;
                if (Page.PreviousPage == null)
                {
                    Response.End();
                    return;
                }
                string strPrevPageMasterPath = Page.PreviousPage.Master.AppRelativeVirtualPath;
                if (strPrevPageMasterPath.Substring(strPrevPageMasterPath.LastIndexOf('/')).ToLower() == "/Canvas.master".ToLower())
                {
                    strPageMode = "edit";
                    strApplicationType = "0";
                    strAppListSourceType = "0";
                }
                else
                {
                    strPageMode = ((HiddenField)Page.PreviousPage.Master.FindControl("hidAppNewOrEdit")).Value;
                    strApplicationType = ((HiddenField)Page.PreviousPage.Master.FindControl("hidCreateNewAppType")).Value;
                    strAppListSourceType = ((HiddenField)Page.PreviousPage.Master.FindControl("hidAppListSource")).Value;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;
                //hfs.Value = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                //hfbid.Value = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
                strhfs = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;

                ((HiddenField)mainMaster.FindControl("hfs")).Value = strhfs;
                ((HiddenField)mainMaster.FindControl("hfbid")).Value = strhfbid;
            }

            if (string.IsNullOrEmpty(strhfs) || string.IsNullOrEmpty(strhfbid)) return;

            context.Items["hfs"] = strhfs;
            context.Items["hfbid"] = strhfbid;
            string str = Utilities.DecryptString(strhfs);
            SubAdminid = str.Split(',')[0];
            CompanyId = str.Split(',')[3].ToLower();
            Sessionid = str.Split(',')[1];
            Adminid = str.Split(',')[2];

            context.Items["hfs1"] = SubAdminid;
            context.Items["hfs2"] = Sessionid;
            context.Items["hfs3"] = Adminid;
            context.Items["hfs4"] = CompanyId;

            if (!Page.IsPostBack || strPageMode.Length > 0)
            {
                try
                {
                    try
                    {
                        _pageMode = (PAGE_MODE)Enum.Parse(typeof(PAGE_MODE), strPageMode.ToUpper());
                    }
                    catch
                    {
                        throw new MficientException("Internal server error.");
                    }

                    //GetCreateContainers();
                    switch (_pageMode)
                    {
                        case PAGE_MODE.NEW:
                            try
                            {
                                _appType = (MF_APP_TYPE)Enum.Parse(typeof(MF_APP_TYPE), strApplicationType.ToUpper());
                            }
                            catch
                            {
                                throw new MficientException("Internal server error.");
                            }
                            processPageLoadForAdd(_appType);
                            break;
                        case PAGE_MODE.EDIT:
                            processPageLoadForEdit();
                            break;
                    }
                }
                catch (MficientException ex)
                {
                    Utilities.showMessage(ex.Message, this.Page, "Page Load Error Meaage", DIALOG_TYPE.Error);
                }

                ifUploadImage.Attributes["src"] = "UploadImage.aspx?id=" + strhfs;
                ifUpload.Attributes["src"] = "UploadFile.aspx?id=" + strhfs;
            }

        }


        #region Page Mode Add
        void processPageLoadForAdd(MF_APP_TYPE appType)
        {
            // setAllFormHidField(getJsonOfAllForms());
            bindAppsList(APP_LIST_SOURCE.Current);
            hdfCompanyId.Value = this.CompanyId;
            getAllImages();
            setAllAppIconsHidField(getAllIconListJson());
            setAppJqueryMobileIconsHidField(getAllJqueryMobileIconListJson());
            updateHiddenFieldsUpdPanel();
            switch (appType)
            {
                case MF_APP_TYPE.StandardApp:
                    setAllWfDtlsHidField(getJsonOfAllWorkFlows());
                    setAllDbCmdHidField(getJsonOfAllDbCommands());
                    setAllWsCmdHidField(getJsonOfAllWsCommands());
                    setAllOdataHidField(getJsonOfAllOdataCommands());
                    setmPluginAgentsHidField(getJsonOfMPluginAgents());
                    //setCurrentWorkingAppHidField((getJsonOfCurrentWorkingApp()));

                    setOfflineTablesHidField(getJsonOfAllOfflineTables());
                    setOfflineDataObjectsHidField(getJsonOfAllOfflineDataObjects());
                    setEnterpireWiFiSSIDsInHidField(getEnterpriseWiFiSSIDs());
                    Utilities.runPageStartUpScript(this.Page,
                    "Add new App Dialog", @"mFicientIde.MfApp.AppDesigner.createNewApp.processGetAppName();
                    mFicientIde.MfApp.AppDesigner.appIconSelectorHelper.createImageGallery();
                    bindAppsListTable();");
                    break;
                case MF_APP_TYPE.Html5App:
                    Utilities.runPageStartUpScript(this.Page,
                    "Add NEW Html5 App Dialog", @"mfHtml5App.getApplicationName();
                    mfIconsGallery.createImageGallery();
                    bindAppsListTable();");
                    break;
            }
        }
        void bindAppsList(APP_LIST_SOURCE source)
        {
            GetAppCompleteData obj = new GetAppCompleteData(this.CompanyId, this.SubAdminid);
            switch (source)
            {
                case APP_LIST_SOURCE.Current:
                    setAllAppsListHidField(obj.GetAppsList());
                    break;
                case APP_LIST_SOURCE.Archived:
                    setAllAppsListHidField(obj.GetArchivedAppsList());
                    break;
            }
        }
        #endregion

        #region Page Mode Edit
        void processPageLoadForEdit()
        {
            //setAllFormHidField(getJsonOfAllForms());

            //setAllDbCmdHidField(getJsonOfAllDbCommands());
            //setAllWsCmdHidField(getJsonOfAllWsCommands());
            //setAllOdataHidField(getJsonOfAllOdataCommands());
            //setmPluginAgentsHidField(getJsonOfMPluginAgents());
            ////setCurrentWorkingAppHidField((getJsonOfCurrentWorkingApp()));
            //setAllAppIconsHidField(getAllIconListJson());
            //setOfflineTablesHidField(getJsonOfAllOfflineTables());
            setAllWfDtlsHidField(getJsonOfAllWorkFlows());
            setAllDbCmdHidField(getJsonOfAllDbCommands());
            setAllWsCmdHidField(getJsonOfAllWsCommands());
            setAllOdataHidField(getJsonOfAllOdataCommands());
            setOfflineTablesHidField(getJsonOfAllOfflineTables());
            setOfflineDataObjectsHidField(getJsonOfAllOfflineDataObjects());
            setmPluginAgentsHidField(getJsonOfMPluginAgents());
            setAllAppIconsHidField(getAllIconListJson());
            setAppJqueryMobileIconsHidField(getAllJqueryMobileIconListJson());
            setEnterpireWiFiSSIDsInHidField(getEnterpriseWiFiSSIDs());
            APP_LIST_SOURCE eAppListSrc = (APP_LIST_SOURCE)Enum.Parse(typeof(APP_LIST_SOURCE), strAppListSourceType);
            bindAppsList(eAppListSrc);
            updateHiddenFieldsUpdPanel();
            hdfCompanyId.Value = this.CompanyId;
            getAllImages();
            Utilities.runPageStartUpScript(this.Page,
                "Add new App Dialog", @"
                    mFicientIde.MfApp.AppDesigner.appIconSelectorHelper.createImageGallery();
                    bindAppsListTable('" + ((int)eAppListSrc).ToString() + @"');
                    mFicientIde.MfApp.AppDesigner.processChangeHtmlHelper.showHideAppListContDiv(true);");
        }
        #endregion

        #region Events
        protected void btnAddNewApp_Click(object sender, EventArgs e)
        {
            if (txtNewAppName.Text.Length == 0)
            {
                //ScriptManager.RegisterStartupScript(UpdAddNewApp, typeof(UpdatePanel),
                //    Guid.NewGuid().ToString(), "$('#WfSep9').hide();$('#WfSwitchAppDiv').hide();$('#WfSwitchAppDiv').hide();CommonErrorMessages(44);", true);
                //Utilities.runPostBackScript("$('#WfSep9').hide();$('#WfSwitchAppDiv').hide();$('#WfSwitchAppDiv').hide();CommonErrorMessages(44);",
                //UpdAddNewApp, "Invalid Length");
                //return;
            }
            else
            {

                //IdeGetWorkflowCount objWfCoun = new IdeGetWorkflowCount(this.CompanyId);
                //if (objWfCoun.WorkFlowCount >= objWfCoun.MaxWorkFlow)
                //{
                //ScriptManager.RegisterStartupScript(UpdAddNewApp, typeof(UpdatePanel),
                //    Guid.NewGuid().ToString(), "$('#WfSep9').hide();$('#WfSwitchAppDiv').hide();CommonErrorMessages(93);", true);
                // Utilities.runPostBackScript("$('#WfSep9').hide();$('#WfSwitchAppDiv').hide();CommonErrorMessages(93);",
                // UpdAddNewApp, "Workflow Count Exceeded");
                // return;
                // }
                //else
                // {
                //adding App ind database and adding details in JSON
                // //CreateNewWorkFlow("", this.SubAdminid, txtNewAppName.Text.Trim(), "", "", "", "", this.CompanyId, UpdAddNewApp, false, false, true);
                // lblSpanFrmPropAppNameHeader.Text = txtNewAppName.Text.Trim();
                // hdfIconname.Value = MficientConstants.IDE_APPLICATION_DEFAULT_ICON;
                //ScriptManager.RegisterStartupScript(UpdAddNewApp,
                //    typeof(UpdatePanel), Guid.NewGuid().ToString(),
                //    ";setApplicationIconFromHidInViewAppDtl();", true);
                // Utilities.runPostBackScript("setApplicationIconFromHidInViewAppDtl();",
                // UpdAddNewApp, "Set Application Image Name");
                // updateHiddenFieldsUpdPanel();
                //}
            }
        }

        //private Boolean CreateNewWorkFlow(string _WFId, string _SubAdminId, string _WfName,
        //    string _WfDescription, string _Json, string _Forms, string _ParentId,
        //    string _CompanyId, UpdatePanel _Upd, Boolean _IsDiffForTablet, Boolean _OnlyForTablet, Boolean _IsMainApp)
        //{
        //    GetWfDetails objGetWfDetails = new GetWfDetails(false, true, _IsMainApp, "", _SubAdminId, _WfName,
        //        hdfWFparentId.Value.Split('_')[0], _CompanyId, hdfWfIsTablet.Value.Trim(), false);
        //    objGetWfDetails.Process();

        //    if (objGetWfDetails.StatusCode == 0)
        //    {
        //        //ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "CommonErrorMessages(45);", true);
        //        Utilities.runPostBackScript("CommonErrorMessages(45);",
        //           _Upd, "Error Message");
        //        return false;
        //    }
        //    else
        //    {
        //        AddWorkFlow objAddWorkFlow = new AddWorkFlow(_WFId, _WfName, _WfDescription, _Json,
        //            _Forms, _SubAdminId, true, _ParentId, _CompanyId, _IsDiffForTablet, _OnlyForTablet, hdfCmdIds.Value.Trim());
        //        objAddWorkFlow.Process();
        //        if (objAddWorkFlow.StatusCode == 0)
        //        {
        //            _ParentId = objAddWorkFlow.WFId;
        //            GetWFDetail(objAddWorkFlow.WFId, _SubAdminId, _CompanyId, true, _ParentId, true, false, _Upd);

        //            List<IdeAppsForJson> lstAllAps = Utilities.DeserialiseJson<List<IdeAppsForJson>>(hidAllWfs.Value);

        //            IdeAppsForJson objApp = new IdeAppsForJson();
        //            objApp.compId = _CompanyId;
        //            objApp.wfid = objAddWorkFlow.WFId;
        //            objApp.wfName = _WfName;
        //            objApp.wfDescription = "";
        //            objApp.wfJson = "";
        //            objApp.parentId = objAddWorkFlow.WFId;
        //            objApp.modelType = Convert.ToString(MficientConstants.IDE_MODEL_TYPE_PHONE);
        //            objApp.icon = MficientConstants.IDE_APPLICATION_DEFAULT_ICON;
        //            objApp.runsOn = "0";
        //            objApp.diffInterfaceForTab = "0";
        //            objApp.designInterface = MficientStringsUsed.AppDesignInterfacePhoneAndTablet;
        //            lstAllAps.Add(objApp);
        //            setAllWfDtlsHidField(Utilities.SerializeJson<List<IdeAppsForJson>>(lstAllAps));
        //            updateHiddenFieldsUpdPanel();

        //            ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel),
        //                Guid.NewGuid().ToString(),
        //                "IsFormChange = false;WfsetWorkFlowId('" + objAddWorkFlow.WFId + "');SubprocAddApplicationName(false);" + "$('#aAppMasterName').html('Master');$('[id$=hdfAppMAsterId]').val('" + objAddWorkFlow.MasterPageId + "');$('#WfCommitDiv').show();$('#WfSep7').show();", true);
        //            bindWorkFlowRpt();
        //            //UpdProcessSet.Update();
        //            //UpdProcessSet.Update();
        //            return true;
        //        }
        //        else
        //        {
        //            ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "CommonErrorMessages(46);", true);
        //            return false;
        //        }
        //    }
        //}
        //private void GetWFDetail(string _WfId, string _SubAdminAdmin, string _CompanyId,
        //    Boolean _IsMain, string _ParentId, Boolean _IsUpdateApps, Boolean _Start, UpdatePanel _Upd)
        //{
        //    string strOut = "", strScript = "";
        //    IdeAppFlowJsonParser obj = new IdeAppFlowJsonParser(_WfId, _SubAdminAdmin, _CompanyId, _IsMain, _ParentId, _IsUpdateApps, _Start, hdfWfSingleRecord.Value, hdfWfIsTablet.Value.Trim(), out strOut);
        //    if (obj.StatusCode == 0)
        //    {
        //        if (_IsMain)
        //        {
        //            BindMasterPageDropbdwn(_ParentId);
        //            updFrmMasterpage.Update();
        //        }
        //        else { }
        //        strScript = WorkFlowToolBoxBinding(_SubAdminAdmin, _CompanyId, _ParentId, _Start, "", strOut);
        //    }
        //    ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), strScript + obj.AppFlowScript + obj.Script, true);
        //}

        protected void BindMasterPageDropbdwn(string _ParentId)
        {
            //GetMasterPageDetails objGetMasterPageDetails = new GetMasterPageDetails(true, "", this.SubAdminid, _ParentId);
            //objGetMasterPageDetails.Process();
            //if (objGetMasterPageDetails.StatusCode == 0)
            //{
            //ddlFrmMasterpage.DataSource = objGetMasterPageDetails.ResultTable;
            //ddlFrmMasterpage.DataTextField = "MASTER_NAME";
            //ddlFrmMasterpage.DataValueField = "MASTER_PAGE_ID";
            //ddlFrmMasterpage.DataBind();
            //ddlFrmMasterpage.Items.Insert(0, new ListItem("None", "-1"));
            //}
            //else
            //{
            //ddlFrmMasterpage.Items.Clear();
            //ddlFrmMasterpage.Items.Insert(0, new ListItem("None", "-1"));
            //}

        }
        void bindWorkFlowRpt()
        {
            //For testing;
            GetWorkFlowDetails objGetWorkFlowDtls = new GetWorkFlowDetails(this.SubAdminid, this.CompanyId);
            objGetWorkFlowDtls.Process();
            DataTable dtblWorkFlowDtls = objGetWorkFlowDtls.ResultTable;
            //if (dtblWorkFlowDtls != null)
            //{
            //    if (dtblWorkFlowDtls.Rows.Count > 0)
            //    {
            //        rptWFDetails.Visible = true;
            //        rptWFDetails.DataSource = dtblWorkFlowDtls;
            //        rptWFDetails.DataBind();
            //        showRptHdrInfoAndEnableDisableAdd("App Details", true, "WF");
            //        pnlEmptyApps.Visible = false;
            //    }
            //    else
            //    {
            //        pnlEmptyApps.Visible = true;
            //        rptWFDetails.Visible = false;
            //        //showRptHdrInfoAndEnableDisableAdd("No app added yet", true, "WF");
            //    }
            //}
            //else
            //{
            //    pnlEmptyApps.Visible = true;
            //    rptWFDetails.Visible = false;
            //    //showRptHdrInfoAndEnableDisableAdd("No app added yet", true, "WF");
            //}
        }
        //private string WorkFlowToolBoxBinding(string _SubAdminId, string _ComapnyId, string _ParentId, Boolean _Start, string _NewFormId, string _AdditionalScript)
        //{
        //    StringBuilder sbScript = new StringBuilder();
        //    StringBuilder sbFrmHtml = new StringBuilder();
        //    StringBuilder sbTabHtml = new StringBuilder();
        //    StringBuilder sbPhnFrm = new StringBuilder();
        //    StringBuilder sbTabFrm = new StringBuilder();
        //    string strSelectedDiv = "", strFrmIcon = "//enterprise.mficient.com/images/addform.png", strCondHtml = "", strHeadFrm = "", strHeadTab = "", strAddScript = "";
        //    int count = 1;
        //    Boolean IsTablet = false;
        //    Boolean IsPhone = false;
        //    GetFormDetails objGetFormDetails = new GetFormDetails(true, false, "", _SubAdminId, "", _ParentId, _ComapnyId);
        //    objGetFormDetails.Process();

        //    strCondHtml = "<div style=\"clear: both;\" id=\"draggableElementCndBox\"  class=\"draggableElement\" title=\"ConditionBox&#13; \">" +
        //                                "<img id=\"ConditonalBox\" alt=\"ConditonalBox\" src=\"//enterprise.mficient.com/images/condionIcon.png\" class=\"ToolboxImage\" />" +
        //                                "Condition Box" +
        //                            "</div><div id=\"toolSep\" class=\"node dargBottom\"></div>";

        //    strHeadFrm = "<div class=\"node ToolboxHeadDiv\">" +
        //                       "<div id=\"Div9\" class=\"node HeadInnerDiv FLeft DefaultCursor\">" +
        //                           "Phone Forms" +
        //                       "</div>" +
        //                   "</div>";

        //    strHeadTab = "<div class=\"node ToolboxHeadDiv\">" +
        //                   "<div id=\"Div9\" class=\"node HeadInnerDiv FLeft DefaultCursor\">" +
        //                       "Tablet Forms" +
        //                   "</div>" +
        //               "</div>";

        //    sbScript.Append("WfToolboxFormMouseOver('#draggableElementCndBox');$('#toolSep').attr('disabled', 'disabled'); $('#toolSep').draggable({ disabled: true });");

        //    if (objGetFormDetails.StatusCode == 0)
        //    {
        //        foreach (DataRow dr in objGetFormDetails.ResultTable.Rows)
        //        {
        //            strSelectedDiv = (_NewFormId.Trim().Length > 0 ? ((Convert.ToString(dr["FORM_ID"]) == _NewFormId) ? "draggableElementForm_" + Convert.ToString(count) : strSelectedDiv) : strSelectedDiv);
        //            if (Convert.ToInt32(dr["MODEL_TYPE"]) == MficientConstants.IDE_MODEL_TYPE_TABLET)
        //            {
        //                IsTablet = true;
        //                strFrmIcon = "images/Tablet.png";
        //                sbTabHtml.Append("<div style=\"clear: both;\" id=\"draggableElementForm_" + Convert.ToString(count) + "\"  class=\"draggableElement\" title=\"" + Convert.ToString(dr["FORM_NAME"]) + "&#13;Description : &#13;" + ReplaceSingleCourseAndBackSlash(Convert.ToString(dr["FORM_DESCRIPTION"])).Replace("\n", "\\n") + "&#13;\">" +
        //                                   "<img id=\"Form_" + Convert.ToString(count) + "_" + Convert.ToString(dr["FORM_NAME"]) + "\" alt=\"Label\" src=\"" + strFrmIcon + "\" class=\"ToolboxImage\" />" +
        //                                   "<a class=\"ToolboxInner\">" + (Convert.ToString(dr["FORM_NAME"]).Length > 16 ? Convert.ToString(dr["FORM_NAME"]).Substring(0, 14) + ".." : Convert.ToString(dr["FORM_NAME"])) + "</a><a id=\"wffrmId_" + Convert.ToString(count) + "\" type=\"wffrmId\" class=\"wffrmId\">" + Convert.ToString(dr["FORM_ID"]) + "</a><a  id=\"wffrmType_" + Convert.ToString(count) + "\" style=\"display:none\">F</a><a  id=\"wffrmContRpt_" + Convert.ToString(count) + "\" style=\"display:none\">" + (Convert.ToBoolean(dr["IS_REPEATER_ROWCLICK"]) ? "1" : "0") + "</a><a  id=\"wffrmMtype_" + Convert.ToString(count) + "\" style=\"display:none\">" + Convert.ToString(dr["MODEL_TYPE"]) + "</a>" +
        //                               "</div>");
        //                sbTabFrm.Append("<option value=\"" + Convert.ToString(dr["FORM_ID"]) + "\" >" + Convert.ToString(dr["FORM_NAME"]) + "</option>");
        //            }
        //            else
        //            {
        //                IsPhone = true;
        //                strFrmIcon = "//enterprise.mficient.com/images/addform.png";
        //                sbFrmHtml.Append("<div style=\"clear: both;\" id=\"draggableElementForm_" + Convert.ToString(count) + "\"  class=\"draggableElement\" title=\"" + Convert.ToString(dr["FORM_NAME"]) + "&#13;Description : &#13;" + ReplaceSingleCourseAndBackSlash(Convert.ToString(dr["FORM_DESCRIPTION"])).Replace("\n", "\\n") + "&#13;\">" +
        //                                    "<img id=\"Form_" + Convert.ToString(count) + "_" + Convert.ToString(dr["FORM_NAME"]) + "\" alt=\"Label\" src=\"" + strFrmIcon + "\" class=\"ToolboxImage\" />" +
        //                                    "<a class=\"ToolboxInner\">" + (Convert.ToString(dr["FORM_NAME"]).Length > 16 ? Convert.ToString(dr["FORM_NAME"]).Substring(0, 14) + ".." : Convert.ToString(dr["FORM_NAME"])) + "</a><a id=\"wffrmId_" + Convert.ToString(count) + "\" type=\"wffrmId\" class=\"wffrmId\">" + Convert.ToString(dr["FORM_ID"]) + "</a><a  id=\"wffrmType_" + Convert.ToString(count) + "\" style=\"display:none\">F</a><a  id=\"wffrmContRpt_" + Convert.ToString(count) + "\" style=\"display:none\">" + (Convert.ToBoolean(dr["IS_REPEATER_ROWCLICK"]) ? "1" : "0") + "</a><a  id=\"wffrmMtype_" + Convert.ToString(count) + "\" style=\"display:none\">" + Convert.ToString(dr["MODEL_TYPE"]) + "</a>" +
        //                                "</div>");
        //                sbPhnFrm.Append("<option value=\"" + Convert.ToString(dr["FORM_ID"]) + "\" >" + Convert.ToString(dr["FORM_NAME"]) + "</option>");
        //            }
        //            sbScript.Append("WfToolboxFormMouseOver('#");
        //            sbScript.Append("draggableElementForm_");
        //            sbScript.Append(Convert.ToString(count));
        //            sbScript.Append("');WfFormDoubleClick('#");
        //            sbScript.Append("draggableElementForm_");
        //            sbScript.Append(Convert.ToString(count));
        //            sbScript.Append("');WfFormRightClick('#");
        //            sbScript.Append("draggableElementForm_");
        //            sbScript.Append(Convert.ToString(count));
        //            sbScript.Append("');");
        //            count = count + 1;
        //        }
        //    }
        //    else
        //    {
        //        DataTable objDataTable = new DataTable();
        //        DataColumn dc1 = new DataColumn("FORM_ID");
        //        DataColumn dc2 = new DataColumn("FORM_NAME");
        //        objDataTable.Columns.Add(dc1);
        //        objDataTable.Columns.Add(dc2);
        //        objDataTable.Rows.Add(objDataTable.NewRow());
        //    }

        //    if (!IsTablet && !IsPhone)
        //    {
        //        strHeadFrm = "<div class=\"node ToolboxHeadDiv\">" +
        //                       "<div id=\"Div9\" class=\"node HeadInnerDiv FLeft DefaultCursor\">" +
        //                           "Forms" +
        //                       "</div>" +
        //                   "</div>";

        //        strAddScript = "$('#WF_ControlsContainer').css('height','520px');" +
        //                        "$('#WF_TBControlsContainer').css('height','0px');" +
        //                        "$('#Wf_TabFormHead').html('');" +
        //                        "$('#WF_ControlsContainer').html('');" +
        //                        "$('#WF_TBControlsContainer').html('');" +
        //                        "$('#Wf_PhoneFormHead').html('" + strHeadFrm + "');";
        //    }
        //    else if (IsTablet && !IsPhone)
        //    {
        //        strHeadTab = "<div class=\"node ToolboxHeadDiv\">" +
        //                       "<div id=\"Div9\" class=\"node HeadInnerDiv FLeft DefaultCursor\">" +
        //                           "Tablet Forms" +
        //                       "</div>" +
        //                   "</div>";

        //        strAddScript = "$('#WF_TBControlsContainer').css('height','520px');" +
        //                        "$('#WF_ControlsContainer').css('height','0px');" +
        //                        "$('#WF_ControlsContainer').html('');" +
        //                        "$('#WF_TBControlsContainer').html('" + sbTabHtml.ToString() + "');" +
        //                        "$('#Wf_TabFormHead').html('" + strHeadTab + "');" +
        //                        "$('#Wf_PhoneFormHead').html('');";
        //    }
        //    else if (IsPhone && !IsTablet)
        //    {
        //        strAddScript = "$('#WF_ControlsContainer').css('height','520px');" +
        //                        "$('#WF_TBControlsContainer').css('height','0px');" +
        //                        "$('#WF_ControlsContainer').html('" + sbFrmHtml.ToString() + "');" +
        //                        "$('#WF_TBControlsContainer').html('');" +
        //                        "$('#Wf_TabFormHead').html('');" +
        //                        "$('#Wf_PhoneFormHead').html('" + strHeadFrm + "');";
        //    }
        //    else
        //    {
        //        strAddScript = "$('#WF_ControlsContainer').css('height','250px');" +
        //          "$('#WF_TBControlsContainer').css('height','250px');" +
        //          "$('#WF_ControlsContainer').html('" + sbFrmHtml.ToString() + "');" +
        //          "$('#WF_TBControlsContainer').html('" + sbTabHtml.ToString() + "');" +
        //          "$('#Wf_TabFormHead').html('" + strHeadTab + "');" +
        //          "$('#Wf_PhoneFormHead').html('" + strHeadFrm + "');";
        //    }

        //    _AdditionalScript += "$('#Wf_ConditionBox').html('" + strCondHtml + "');" +
        //                         "$('#WF_TBControlsContainer').html('');" +
        //                         "$('#Wf_TabFormHead').html('');" + strAddScript + (_Start ? "" : "Wfinitialization();") + sbScript.ToString() +
        //                         (strSelectedDiv.Length > 0 ? ";ApplicationSelectAppDiv('#" + strSelectedDiv + "');" : "");

        //    _AdditionalScript += ";WfChildFrmHash = { PhoneOp: '" + sbPhnFrm.ToString() + "', TabletOp: '" + sbTabFrm.ToString() + "' };";

        //    return _AdditionalScript;

        //}
        #endregion

        #region ViewContainers

        public string GetCreateContainers()
        {
            StringWriter stringWriter = new StringWriter();
            HtmlTextWriter writer = new HtmlTextWriter(stringWriter);
            int WfContRowCount = 10;
            int TotalCol = (WfContRowCount * 2);
            int StartContId = 101;

            for (var i = 1; i < TotalCol; i++)
            {
                if (i % 2 != 0)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "WfColDiv");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    CreateContColHtml(StartContId, TotalCol, writer);
                    writer.RenderEndTag();
                }
                else
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "WfColDiv");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    CreateTransColHtml(StartContId, TotalCol, writer);
                    writer.RenderEndTag();
                    StartContId = StartContId + 1;
                }
            }
            System.Web.UI.HtmlControls.HtmlGenericControl hTag = new System.Web.UI.HtmlControls.HtmlGenericControl();
            hTag.InnerHtml = stringWriter.ToString();
            //rptViewContainers.Controls.Add(hTag);

            return stringWriter.ToString();
        }

        private void CreateContColHtml(int _StartCont, int _TotalRow, HtmlTextWriter writer)
        {
            int IntSatart = _StartCont;
            for (var i = 1; i < _TotalRow; i++)
            {
                if (i % 2 != 0)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, "F" + GetFourDigitViewId(IntSatart));
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "WfRowDiv");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.RenderEndTag();
                }
                else
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, GetTransId(IntSatart));
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "WfSepRowDiv");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.RenderEndTag();
                    IntSatart = IntSatart + 100;
                }
            }
        }

        private void CreateTransColHtml(int _StartCont, int _TotalRow, HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "WfColTop");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();
            int IntSatart = _StartCont;
            string strTransId = "";
            for (var i = 1; i < _TotalRow; i++)
            {
                if (i % 2 != 0)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, "C_" + GetFourDigitViewId(IntSatart) + "_" + GetFourDigitViewId(IntSatart + 1));
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "WfColMiddle");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.RenderEndTag();
                }
                else
                {
                    strTransId = "C_" + GetFourDigitViewId(IntSatart) + "_" + GetFourDigitViewId(IntSatart + 101);
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, strTransId);
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "WfColBottom");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "FLeft WfColBottomInner");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, strTransId + "_1");
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "FLeft WfColBottomInner1");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.RenderEndTag();
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, strTransId + "_2");
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "FLeft WfColBottomInner1");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.RenderEndTag();
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "clear");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.RenderEndTag();
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, strTransId + "_0");
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "FLeft WfColBottomInner1");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.RenderEndTag();
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, strTransId + "_3");
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "FLeft WfColBottomInner1");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                    IntSatart = IntSatart + 100;
                }
            }
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "WfColBottomLast");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();
        }

        private string GetFourDigitViewId(int _IntSatart)
        {
            string strId = "";
            if (Convert.ToString(_IntSatart).Length <= 3)
            {
                strId = "0" + Convert.ToString(_IntSatart);
            }
            else
            {
                strId = Convert.ToString(_IntSatart);
            }
            return strId;
        }

        private string GetTransId(int _IntSatart)
        {
            return "C_" + GetFourDigitViewId(_IntSatart) + "_" + GetFourDigitViewId(_IntSatart + 100);
        }

        #endregion
        #region Update Panels
        void updateHiddenFieldsUpdPanel()
        {
            updHiddenFields.Update();
        }
        #endregion
        #region Helper Functions
        private string ReplaceBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return Regex.Replace(_Input, @"\\", @"\\");
            }
        }

        private string ReplaceDoubleCourse(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return Regex.Replace(_Input, "\"", "\\\"");
            }
        }

        private string ReplaceDoubleCourseAndBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return ReplaceDoubleCourse(ReplaceBackSlash(_Input));
            }
        }

        private string ReplaceSingleCourse(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return Regex.Replace(_Input, @"'", @"\'");
            }
        }

        private string ReplaceSingleCourseAndBackSlash(string _Input)
        {
            if (_Input == null)
                return "";
            else
            {
                return ReplaceSingleCourse(ReplaceBackSlash(_Input));
            }
        }
        #endregion
        #region Create JSONS
        string getJsonOfAllWorkFlows()
        {
            string strJson = "";
            DataTable dtblWfDtls = GetWfDetails.getAllWfAndAdditionalDtlForCmp(this.SubAdminid,
                this.CompanyId);
            if (dtblWfDtls != null)
            {
                List<IdeAppsForJson> lstWfDtls = new List<IdeAppsForJson>();
                foreach (DataRow row in dtblWfDtls.Rows)
                {
                    IdeAppsForJson objApp = new IdeAppsForJson();
                    objApp.compId = Convert.ToString(row["COMPANY_ID"]);
                    objApp.wfid = Convert.ToString(row["WF_ID"]);
                    objApp.wfName = Convert.ToString(row["WF_NAME"]);
                    objApp.wfDescription = Convert.ToString(row["WF_DESCRIPTION"]);
                    objApp.wfJson = Convert.ToString(row["WF_JSON"]);
                    //objApp.forms = Convert.ToString(row["FORMS"]);
                    objApp.parentId = Convert.ToString(row["PARENT_ID"]);
                    objApp.modelType = Convert.ToString(row["MODEL_TYPE"]);
                    //objApp.commandIds = Convert.ToString(row["COMMAND_IDS"]);
                    objApp.icon = Convert.ToString(row["WF_ICON"]);
                    objApp.noOfRecordsInDb = Convert.ToString(row["WF_COUNT"]);
                    if (objApp.noOfRecordsInDb == "1")
                    {
                        if (objApp.modelType == "0")
                        {
                            objApp.runsOn = "0";
                            objApp.diffInterfaceForTab = "0";
                            objApp.designInterface = MficientStringsUsed.AppDesignInterfacePhoneAndTablet;
                        }
                        else
                        {
                            objApp.runsOn = "1";
                            objApp.diffInterfaceForTab = "1";
                            objApp.designInterface = MficientStringsUsed.AppDesignInterfaceTablet;
                        }
                    }
                    else if (objApp.noOfRecordsInDb == "2")
                    {
                        objApp.runsOn = "1";
                        //There are two different interfaces for tablet and phone.
                        //The first time we will be showing only Phone interface
                        objApp.designInterface = MficientStringsUsed.AppDesignInterfacePhone;
                    }
                    lstWfDtls.Add(objApp);
                }
                strJson = Utilities.SerializeJson<List<IdeAppsForJson>>(lstWfDtls);
            }
            return strJson;
        }
        //string getJsonOfAllForms()
        //{
        //    string strJson = "";
        //    DataTable dtblForms = GetFormDetails.getAllFormDetailsForCmp(this.SubAdminid,
        //        this.CompanyId);
        //    if (dtblForms != null)
        //    {
        //        List<IdeFormsForJson> lstFormDtls = new List<IdeFormsForJson>();
        //        foreach (DataRow row in dtblForms.Rows)
        //        {
        //            IdeFormsForJson objForm = new IdeFormsForJson();
        //            objForm.compId = Convert.ToString(row["COMPANY_ID"]);
        //            objForm.formId = Convert.ToString(row["FORM_ID"]);
        //            objForm.formName = Convert.ToString(row["FORM_NAME"]);
        //            objForm.isActive = Convert.ToString(row["IS_ACTIVE"]);
        //            objForm.formJson = Convert.ToString(row["FORM_JSON"]);
        //            objForm.formDesc = Convert.ToString(row["FORM_DESCRIPTION"]);
        //            objForm.cmdIds = Convert.ToString(row["COMMAND_IDS"]);
        //            objForm.masterid = Convert.ToString(row["MASTER_ID"]);
        //            objForm.parentId = Convert.ToString(row["PARENT_ID"]);
        //            objForm.rptRowClick = Convert.ToBoolean(row["IS_REPEATER_ROWCLICK"]) == true ? "1" : "0";
        //            objForm.modelType = Convert.ToString(row["MODEL_TYPE"]);
        //            lstFormDtls.Add(objForm);
        //        }
        //        strJson = Utilities.SerializeJson<List<IdeFormsForJson>>(lstFormDtls);
        //    }
        //    return strJson;
        //}
        string getJsonOfAllDbCommands()
        {
            string strJson = "";
            GetDbCommand objDbCommand = new GetDbCommand(true, false, this.SubAdminid, "", "", this.CompanyId);

            strJson = Utilities.SerializeJson<List<IdeDbCmdsForJson>>(objDbCommand.getCommondDetail());
            //string test = getInputOutputParamJsonForCommand(COMMAND_TYPE.DatabaseCommand, objDbCommand.ResultTable);
            return strJson;
        }
        string getJsonOfAllWsCommands()
        {
            GetWsCommand objWsCommand = new GetWsCommand(true, false, this.SubAdminid, "", "", this.CompanyId);
            objWsCommand.Process();
            //DataTable dtblWsCommands = objWsCommand.ResultTable;            
            //if (dtblWsCommands != null)
            //{
                
            //    DataTable dtblFilteredTable = null;
            //    string filterType = "rpc";
            //    string filter = String.Format("WEBSERVICE_TYPE = '{0}'", filterType);
            //    DataView dtView = new DataView(dtblWsCommands,
            //                            filter,
            //                            "WEBSERVICE_TYPE",
            //                            DataViewRowState.CurrentRows);

            //    if (dtView != null)
            //    {
            //        dtblFilteredTable = dtView.ToTable();
            //        string test = getInputOutputParamJsonForCommand(COMMAND_TYPE.XmlRpc, dtblFilteredTable);
            //    }
            //    filterType = "wsdl";
            //    filter = String.Format("WEBSERVICE_TYPE = '{0}'", filterType);
            //    dtView = new DataView(dtblWsCommands,
            //                           filter,
            //                           "WEBSERVICE_TYPE",
            //                           DataViewRowState.CurrentRows);

            //    if (dtView != null)
            //    {
            //        dtblFilteredTable = dtView.ToTable();
            //        string test2 = getInputOutputParamJsonForCommand(COMMAND_TYPE.Wsdl, dtblFilteredTable);
            //    }
            //    filterType = "http";
            //    filter = String.Format("WEBSERVICE_TYPE = '{0}'", filterType);
            //    dtView = new DataView(dtblWsCommands,
            //                           filter,
            //                           "WEBSERVICE_TYPE",
            //                           DataViewRowState.CurrentRows);

            //    if (dtView != null)
            //    {
            //        dtblFilteredTable = dtView.ToTable();
            //        string test3 = getInputOutputParamJsonForCommand(COMMAND_TYPE.Http, dtblFilteredTable);
            //    }
            //}
            return Utilities.SerializeJson<List<IdeWsCmdsForJson>>(objWsCommand.GetWsCommandDetail());
        }
        string getJsonOfAllOdataCommands()
        {
            string strJson = "";
            GetOdataCommand objOdataCmds = new GetOdataCommand(true, false, this.SubAdminid, "", "", this.CompanyId);
            return Utilities.SerializeJson<List<IdeOdataCmdsForJson>>(objOdataCmds.getOdataCommandDetail()); 
        }
        /// <summary>
        /// Get Input and output params for command.
        /// </summary>
        /// <param name="cmdType"></param>
        /// <param name="selectCmd"></param>
        /// <param name="dtlTable"></param>
        /// <returns></returns>
        string getInputOutputParamJsonForCommand(COMMAND_TYPE cmdType, DataTable dtlTable)
        {
            string strJson = String.Empty;
            if (dtlTable.Rows.Count > 0)
            {
                List<OfllineParam> lstInputOutputParam = new List<OfllineParam>();
                switch (cmdType)
                {
                    case COMMAND_TYPE.DatabaseCommand:
                        foreach (DataRow row in dtlTable.Rows)
                        {
                            OfllineParam objParam = new OfllineParam();
                            objParam.cmdid = Convert.ToString(row["DB_COMMAND_ID"]);
                            objParam.cmdnm = Convert.ToString(row["DB_COMMAND_NAME"]);
                            objParam.cmdType = Convert.ToString(row["DB_COMMAND_TYPE"]);
                            objParam.inputprm = Convert.ToString(row["PARAMETER_JSON"]);
                            objParam.outputprm = Convert.ToString(row["COLUMNS"]);
                            objParam.wstype = String.Empty;
                            lstInputOutputParam.Add(objParam);
                        }
                        break;
                    case COMMAND_TYPE.Wsdl:
                        foreach (DataRow row in dtlTable.Rows)
                        {
                            OfllineParam objParam = new OfllineParam();
                            objParam.cmdid = Convert.ToString(row["WS_COMMAND_ID"]);
                            objParam.cmdnm = Convert.ToString(row["WS_COMMAND_NAME"]);
                            objParam.cmdType = Convert.ToString(row["HTTP_TYPE"]);
                            objParam.inputprm = Convert.ToString(row["PARAMETER"]);
                            objParam.outputprm = Convert.ToString(row["TAG"]);
                            objParam.wstype = Convert.ToString((int)WebserviceType.WSDL_SOAP);
                            lstInputOutputParam.Add(objParam);
                        }
                        break;
                    case COMMAND_TYPE.Http:
                        foreach (DataRow row in dtlTable.Rows)
                        {
                            OfllineParam objParam = new OfllineParam();
                            objParam.cmdid = Convert.ToString(row["WS_COMMAND_ID"]);
                            objParam.cmdnm = Convert.ToString(row["WS_COMMAND_NAME"]);
                            objParam.cmdType = Convert.ToString(row["HTTP_TYPE"]);
                            objParam.inputprm = Convert.ToString(row["PARAMETER"]);
                            objParam.outputprm = Convert.ToString(row["TAG"]);
                            objParam.wstype = Convert.ToString((int)WebserviceType.HTTP);
                            lstInputOutputParam.Add(objParam);
                        }
                        break;
                    case COMMAND_TYPE.XmlRpc:
                        string strInParams = String.Empty;
                        string strOutParam = String.Empty;
                        foreach (DataRow row in dtlTable.Rows)
                        {
                            strInParams = String.Empty;
                            strOutParam = String.Empty;
                            Utilities.ParseRpcParaJson(Convert.ToString(row["PARAMETER"]), "In", out strInParams);
                            Utilities.ParseRpcParaJson(Convert.ToString(row["TAG"]), "Out", out strOutParam);
                            OfllineParam objParam = new OfllineParam();
                            objParam.cmdid = Convert.ToString(row["WS_COMMAND_ID"]);
                            objParam.cmdnm = Convert.ToString(row["WS_COMMAND_NAME"]);
                            objParam.cmdType = Convert.ToString(row["HTTP_TYPE"]);
                            objParam.inputprm = Utilities.getFormOfflineInputParamJsonForXmlRpc(strInParams);
                            objParam.outputprm = Utilities.getFormOfflineOutputParamJsonForXmlRpc(strOutParam);
                            objParam.wstype = Convert.ToString((int)WebserviceType.RPC_XML);
                            lstInputOutputParam.Add(objParam);
                        }
                        break;
                    case COMMAND_TYPE.Odata:
                        foreach (DataRow row in dtlTable.Rows)
                        {
                            OfllineParam objParam = new OfllineParam();
                            objParam.cmdid = Convert.ToString(row["ODATA_COMMAND_ID"]);
                            objParam.cmdnm = Convert.ToString(row["ODATA_COMMAND_NAME"]);
                            objParam.cmdType = Convert.ToString(row["HTTP_TYPE"]);
                            objParam.inputprm = Convert.ToString(row["INPUT_PARAMETER_JSON"]);
                            objParam.outputprm = Convert.ToString(row["OUTPUT_PARAMETER_JSON"]);
                            objParam.wstype = "";
                            lstInputOutputParam.Add(objParam);
                        }
                        break;
                }

                OfflineInputOutPutParamByCommand objInputOutputPrmByCmd = new OfflineInputOutPutParamByCommand();
                objInputOutputPrmByCmd.inptOut = lstInputOutputParam;
                strJson = Utilities.SerializeJson<OfflineInputOutPutParamByCommand>(objInputOutputPrmByCmd);
                // strJson = Utilities.getFinalJsonForAutoCompleteTextBox(strJson);

            }
            return strJson;
        }
        string getJsonOfCurrentWorkingApp()
        {
            string strJson = String.Empty;
            string wfId = "8A54EB517D427CD4A11CC43340BECCC8";
            DataTable dtblWfDtls = GetWfDetails.getWorkflowDtlById(wfId, this.SubAdminid, this.CompanyId);
            if (dtblWfDtls != null)
            {
                List<IdeAppsForJson> lstWfDtls = new List<IdeAppsForJson>();
                string strNoOfRecInDb = dtblWfDtls.Rows.Count.ToString();
                foreach (DataRow row in dtblWfDtls.Rows)
                {
                    IdeAppsForJson objApp = new IdeAppsForJson();
                    objApp.compId = Convert.ToString(row["COMPANY_ID"]);
                    objApp.wfid = Convert.ToString(row["WF_ID"]);
                    objApp.wfName = Convert.ToString(row["WF_NAME"]);
                    objApp.wfDescription = Convert.ToString(row["WF_DESCRIPTION"]);
                    objApp.wfJson = Convert.ToString(row["WF_JSON"]);
                    objApp.parentId = Convert.ToString(row["PARENT_ID"]);
                    objApp.modelType = Convert.ToString(row["MODEL_TYPE"]);
                    objApp.icon = Convert.ToString(row["ICON"]);
                    objApp.noOfRecordsInDb = strNoOfRecInDb;
                    objApp.runsOn = "0";
                    objApp.diffInterfaceForTab = "0";
                    lstWfDtls.Add(objApp);
                }
                strJson = Utilities.SerializeJson<List<IdeAppsForJson>>(lstWfDtls);
            }
            return strJson;
            //strJson = Utilities.SerializeJson<OfflineInputOutPutParamByCommand>(objInputOutputPrmByCmd);
        }
        protected string getAllIconListJson()
        {
            string sJSON = "";
            DataTable objDatatable = new DataTable();
            objDatatable.Columns.Add("Name", typeof(string));
            objDatatable.Columns.Add("Url", typeof(string));
            objDatatable.Rows.Add("Select Icon", "-1");
            if (System.IO.Directory.Exists(Server.MapPath("~\\images\\icon")))
            {
                string strBasePath = Server.MapPath("~\\images\\icon");
                System.Collections.Generic.List<string> oList = new System.Collections.Generic.List<string>();
                List<string> files = new List<string>();
                files.AddRange(System.IO.Directory.GetFiles(strBasePath));

                List<string> lstfilesName = new List<string>();
                foreach (string strimgPath in files)
                {
                    lstfilesName.Add(System.IO.Path.GetFileName(strimgPath));
                }
                var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                sJSON = oSerializer.Serialize(lstfilesName);
            }
            else
            {
                // hidAllIconsList.Value = "";
            }
            //ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "GetAllImagesJson('" + sJSON + "');", true);
            return sJSON;
        }
        protected string getAllJqueryMobileIconListJson()
        {
            string sJSON = "";
            string strIconName = "";
            DataTable objDatatable = new DataTable();
            objDatatable.Columns.Add("Name", typeof(string));
            objDatatable.Columns.Add("Url", typeof(string));
            objDatatable.Rows.Add("Select Icon", "-1");
            if (System.IO.Directory.Exists(Server.MapPath("~\\jqueryMobileIcons")))
            {
                string strBasePath = Server.MapPath("~\\jqueryMobileIcons");
                System.Collections.Generic.List<string> oList = new System.Collections.Generic.List<string>();
                List<string> files = new List<string>();
                files.AddRange(System.IO.Directory.GetFiles(strBasePath));

                List<string> lstfilesName = new List<string>();
                foreach (string strimgPath in files)
                {
                    strIconName = System.IO.Path.GetFileName(strimgPath);
                    lstfilesName.Add(strIconName.Substring(0, strIconName.LastIndexOf('-')));
                }
                IEnumerable<string> lstDistinct = lstfilesName.Distinct();
                var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                sJSON = oSerializer.Serialize(lstDistinct);
            }
            else
            {
                // hidAllIconsList.Value = "";
            }
            //ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "GetAllImagesJson('" + sJSON + "');", true);
            return sJSON;
        }
        string getJsonOfMPluginAgents()
        {
            List<MFEmPluginAgent> lstAgents = getAllMPlugInAgents();
            if (lstAgents == null) lstAgents = new List<MFEmPluginAgent>();
            return Utilities.SerializeJson<List<MFEmPluginAgent>>(lstAgents);
        }
        string getJsonOfAllOfflineTables()
        {
            string strJson = "";
            List<MFEOfflineDataTable> lstOflnDts = new List<MFEOfflineDataTable>();
            GetOfflineDataTableDtl getOfflineData = new GetOfflineDataTableDtl(this.CompanyId);
            getOfflineData.Process();
            if (getOfflineData.StatusCode == 0)
            {
                lstOflnDts = getOfflineData.offlineDatatables;
                if (lstOflnDts == null) lstOflnDts = new List<MFEOfflineDataTable>();
                strJson = Utilities.SerializeJson<List<MFEOfflineDataTable>>(lstOflnDts);
            }
            return strJson;
        }
        string getEnterpriseWiFiSSIDs()
        {
            EnterpriseWiFiProcess objEnterpriseWiFi = new EnterpriseWiFiProcess(this.CompanyId);
            List<MFEEnterpriseWiFi> lstWiFis = objEnterpriseWiFi.GetWiFiDtlsByCompanyId();
            if (lstWiFis == null) lstWiFis = new List<MFEEnterpriseWiFi>();
            return Utilities.SerializeJson<List<MFEEnterpriseWiFi>>(lstWiFis);
        }
        //Tanika
        string getJsonOfAllOfflineDataObjects()
        {
            string strJson = "";
            try
            {
                GetOfflineDataObject objGetOfflineDataObjects = new GetOfflineDataObject(this.CompanyId);
                foreach(OfflineDataObject obj in objGetOfflineDataObjects.DataObjects)
                {
                    string cols = string.Empty;
                    if (obj.QueryType == DatabaseCommandType.SELECT)
                    {
                        List<Offline_object_Col> colsJson=Utilities.DeserialiseJson<List<Offline_object_Col>>(obj.OutputParameters);
                        foreach (Offline_object_Col col in colsJson)
                            cols += "," + col.col;

                        cols = cols.Length > 0 ? cols.Substring(1) : "";
                    }
                    obj.OutputParameters = cols;
                }
                strJson = JsonConvert.SerializeObject(objGetOfflineDataObjects.DataObjects);
            }
            catch
            {
            }
            return strJson;
        }
        #endregion
        
        #region Set Value In Hidden Field
        void setAllWfDtlsHidField(string value)
        {
            hidAllWfs.Value = value;
        }
        void setAllFormHidField(string value)
        {
            hidAllForms.Value = value;
        }
        void setAllOdataHidField(string value)
        {
            hidAllOdataCommands.Value = value;
        }
        void setAllDbCmdHidField(string value)
        {
            hidAllDbCommands.Value = value;
        }
        void setAllWsCmdHidField(string value)
        {
            hidAllWsCommands.Value = value;
        }
        void setCurrentWorkingAppHidField(string value)
        {
            hidUICurrentWorkingAppDtl.Value = value;
        }
        void setAllAppIconsHidField(string value)
        {
            hidAllIconsList.Value = value;
        }
        void setAppJqueryMobileIconsHidField(string value)
        {
            hidAllJqueryMobIcons.Value = value;
        }
        void setAllAppsListHidField(string value)
        {
            hidAppsList.Value = value;
        }
        void setEnterpriseAdditionalPropsHidField(string value)
        {
            hidCompanyAdditionalDtls.Value = value;
        }
        void setEnterpireWiFiSSIDsInHidField(string value)
        {
            hidWiFiSSIDs.Value = value;
        }
        void setCompanyUserListHidField(string value)
        {
            hidCompanyUsers.Value = value;
        }
        void setCompanyGroupsHidField(string value)
        {
            hidCompanyGroups.Value = value;
        }
        void setCompanySubadminsHidField(string value)
        {
            hidCompanySubAdmins.Value = value;
        }
        void setmPluginAgentsHidField(string value)
        {
            hidmPluginAgents.Value = value;
        }
        void setAWSCredentialsHidField(string value)
        {
            hidAWSCredentials.Value = value;
        }
        void setOfflineTablesHidField(string value)
        {
            hidAllOfflineDts.Value = value;
        }
        void setOfflineDataObjectsHidField(string value)
        {
            hidAllOfflineDataObjects.Value = value;
        }
        void setHtml5AppTreeJson(string value)
        {
            hidAppTreeView.Value = value;
        }
        void setHtml5AppZipEntryHidField(string value)
        {
            hidAppZipEntriesList.Value = value;
        }
        void setHtml5AppZipFoldersHidField(string value)
        {
            hidAppZipFolders.Value = value;
        }
        void setHtml5AppRootHtmlFilesHidField(string value)
        {
            hidHtml5AppRootHtmlFiles.Value = value;
        }
        void setHtml5AppHtmlFileDtlHidField(string value)
        {
            hidHtml5AppFileDetail.Value = value;
        }
        void setHtml5AppDetailHidField(string value)
        {
            hidHtml5AppDetail.Value = value;
        }
        void setHtml5AppTempFilesKeyHidField(string value)
        {
            hTmpAppFileK.Value = value;
        }
        void setFileContentInHidField(string value)
        {
            hidSelectedFileContent.Value = value;
        }
        Html5AppTempFileKeyForUI getHtml5AppTempFileKeyFromHidField()
        {
            Html5AppTempFileKeyForUI objTempKey = null;
            if (!String.IsNullOrEmpty(hTmpAppFileK.Value))
            {
                string strTempFileKey = Utilities.DecryptString(hTmpAppFileK.Value);
                objTempKey = Utilities.DeserialiseJson<Html5AppTempFileKeyForUI>(strTempFileKey);
            }
            return objTempKey;
        }
        #endregion
        #region XmlRpcCollection

       


        private void GetRpcParamBaseType(string _Type)
        {

        }

        //private void LinkRpcParameters(string _Json, out string _LeftHtml, out string _RightHtml)
        //{
        //    XmlRpcParser obj = new XmlRpcParser();
        //    MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(_Json));
        //    System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());
        //    obj = serializer.ReadObject(ms) as XmlRpcParser;
        //    ms.Close();

        //    string strMarginLeft = "";
        //    string strLevel = "0";
        //    string ItemType = "";
        //    string strAllControl = hdfAllCtrlInFrm.Value;
        //    string strLeftHtml = "";
        //    string strRightHtml = "";
        //    string strOutLeftHtml = "", strOutRigthHtml = "";
        //    foreach (param paramItem in obj.param)
        //    {
        //        ItemType = paramItem.typ.Trim();
        //        if (Convert.ToInt32(ItemType) <= 6)
        //        {
        //            strRightHtml += "<div id=\"LpContentText_" + paramItem.name + "\" class=\"LpContentRow\" style=\"height:26px;\"><div class=\"LpContentValue\"> <select id=\"ddlLPValueType_" + paramItem.name + "\"   class=\"txtLpOption\"><option>Constant</option><option>Control</option></select></div><div class=\"LpContentValue\"><select id=\"ddlLpControl_" + paramItem.name + "\"   class=\"txtLpOption\" disabled=\"disabled\">" + strAllControl + "</select></div><div class=\"LpContentValue\"><input id=\"LpConstantValue_" + paramItem.name + "\" type=\"text\"  class=\"txtLpOption\" /></div> </div><div style=\"clear: both;\"></div>";

        //            strLeftHtml += "<div id=\"RpcLpContentText_" + paramItem.name + "\" class=\"rpcContText\" " + strMarginLeft + ">" +
        //                                                                            paramItem.name +
        //                                                                      "</div>" +
        //                                                                      "<div class=\"clear\"></div>";

        //        }
        //        else if (Convert.ToInt32(ItemType) == 7)
        //        {
        //            strOutLeftHtml = ""; strOutRigthHtml = "";
        //            GetRpcParamHtmlForStruct(paramItem.name, paramItem, strLevel, "", out strOutLeftHtml, out strOutRigthHtml);
        //            strLeftHtml += strOutLeftHtml;
        //            strRightHtml += strOutRigthHtml;
        //        }
        //        else if (Convert.ToInt32(ItemType) > 7)
        //        {
        //            strOutLeftHtml = ""; strOutRigthHtml = "";
        //            GetRpcParamHtmlForStruct(paramItem.name, paramItem, strLevel, "", out strOutLeftHtml, out strOutRigthHtml);
        //            strLeftHtml += strOutLeftHtml;
        //            strRightHtml += strOutRigthHtml;
        //        }
        //    }
        //    _LeftHtml = strLeftHtml;
        //    _RightHtml = strRightHtml;
        //}

        //private void GetRpcParamHtmlForStruct(string _StructName, param _Inpara, string _Level, string _ParentPara, out string _LeftHtml, out string _RightHtml)
        //{
        //    string strLevel = (Convert.ToInt32(_Level) + 1).ToString();
        //    string ItemType = "";
        //    string strLeftHtml = "";
        //    string strRightHtml = "";
        //    string strAllControl = hdfAllCtrlInFrm.Value;
        //    string strMarginLeft = "style=\"margin-left:" + (Convert.ToInt32(strLevel) * 20).ToString() + "px;\"";

        //    strRightHtml += "<div id=\"LpContentText_" + _StructName + "\" class=\"LpContentRow\"  style=\"height:26px;\"></div><div style=\"clear: both;\"></div>";

        //    strLeftHtml += "<div id=\"RpcLpContentText_" + _StructName + "\" class=\"rpcContText\" style=\"margin-left:" + (Convert.ToInt32(_Level) * 20).ToString() + "px;\">" +
        //                                                                    _StructName +
        //                                                              "</div>" +
        //                                                              "<div class=\"clear\"></div>";



        //    foreach (param paramItem in _Inpara.inparam)
        //    {
        //        ItemType = paramItem.typ.Trim();
        //        if (Convert.ToInt32(ItemType) <= 6)
        //        {
        //            strRightHtml += "<div id=\"LpContentText_" + paramItem.name + "\" class=\"LpContentRow\"  style=\"height:26px;\"><div class=\"LpContentValue\"> <select id=\"ddlLPValueType_" + paramItem.name + "\"   class=\"txtLpOption\"><option>Constant</option><option>Control</option></select></div><div class=\"LpContentValue\"><select id=\"ddlLpControl_" + paramItem.name + "\"   class=\"txtLpOption\" disabled=\"disabled\">" + strAllControl + "</select></div><div class=\"LpContentValue\"><input id=\"LpConstantValue_" + paramItem.name + "\" type=\"text\"  class=\"txtLpOption\" /></div> </div><div style=\"clear: both;\"></div>";
        //            strLeftHtml += "<div id=\"RpcLpContentText_" + paramItem.name + "\" class=\"rpcContText\" " + strMarginLeft + ">" +
        //                                                                            paramItem.name +
        //                                                                      "</div>" +
        //                                                                      "<div class=\"clear\"></div>";
        //        }
        //        else if (Convert.ToInt32(ItemType) == 7)
        //        {
        //            string strOutLeftHtml = "", strOutRigthHtml = "";
        //            GetRpcParamHtmlForStruct(paramItem.name, paramItem, strLevel, _StructName, out strOutLeftHtml, out strOutRigthHtml);
        //            strLeftHtml += strOutLeftHtml;
        //            strRightHtml += strOutRigthHtml;
        //        }
        //        else if (Convert.ToInt32(ItemType) == 8)
        //        {
        //            string strOutLeftHtml = "", strOutRigthHtml = "";
        //            GetRpcParamHtmlForStruct(paramItem.name, paramItem, strLevel, _StructName, out strOutLeftHtml, out strOutRigthHtml);
        //            strLeftHtml += strOutLeftHtml;
        //            strRightHtml += strOutRigthHtml;
        //        }
        //    }
        //    _LeftHtml = strLeftHtml;
        //    _RightHtml = strRightHtml;
        //}

        //protected void ddlWf_RpcWsCommand_IndexChanged(object sender, EventArgs e)
        //{
        //    GetWsCommand objGetWsCommand = new GetWsCommand(false, false, this.SubAdminid, ddlWf_RpcWsCommand.SelectedValue, "", this.CompanyId);
        //    objGetWsCommand.Process();
        //    if (objGetWsCommand.ResultTable == null || objGetWsCommand.ResultTable.Rows.Count == 0 || Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["PARAMETER"]).Length == 0)
        //    {
        //        ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), @"$('#WfInnerEditDiv').hide();", true);
        //        return;
        //    }
        //    else
        //    {
        //        int IntCount;
        //        WfLinkRcpParam(Convert.ToString(objGetWsCommand.ResultTable.Rows[0]["PARAMETER"]), updWsRpcCmd, out IntCount);
        //    }
        //}

        //private void WfLinkRcpParam(string _Json, UpdatePanel _Upd, out int _LpCount)
        //{
        //    XmlRpcParser obj = new XmlRpcParser();
        //    MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(_Json));
        //    System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());
        //    obj = serializer.ReadObject(ms) as XmlRpcParser;
        //    ms.Close();

        //    string strMarginLeft = "";
        //    string strLevel = "0";
        //    string ItemType = "";
        //    string strAllControl = hdfAllCtrlInFrm.Value;
        //    string strLeftHtml = "";
        //    string strRightHtml = "";
        //    string strOutLeftHtml = "", strOutRigthHtml = "";
        //    _LpCount = 0;
        //    int NewCount;
        //    foreach (param paramItem in obj.param)
        //    {
        //        ItemType = paramItem.typ.Trim();
        //        if (Convert.ToInt32(ItemType) <= 6)
        //        {
        //            strRightHtml += "<div id=\"LpContentText_" + _LpCount + "\" class=\"LpContentRow\"><div class=\"OnClsContentText\" style=\"width: 50px;\"  align=\"center\">" + "<input id=\"WfWsLpChkCtrlPick_" + _LpCount + "\" type=\"checkbox\" checked=\"checked\" />" + "</div><div class=\"OnClsContentText\"><input id=\"LpConstantValue_" + _LpCount + "\" type=\"text\"  class=\"txtOnCls\" /></div> </div><div style=\"clear: both;\"></div>";
        //            strLeftHtml += "<div class=\"LpContentText1\"><div style=\"margin-left:" + strMarginLeft + "px;\"><a id=\"Lptext_" + _LpCount + "\" type=\"Parameter\">" + paramItem.name + "</a><a id=\"LpTextAdd_" + _LpCount + "\" style=\"display:none;\">" + paramItem.name + "</a></div></div><div style=\"clear: both;\"></div>";
        //            _LpCount = _LpCount + 1;
        //        }
        //        else if (Convert.ToInt32(ItemType) == 7)
        //        {
        //            strOutLeftHtml = ""; strOutRigthHtml = "";
        //            WfParamHtmlForStruct(paramItem.name, paramItem, strLevel, "", out strOutLeftHtml, out strOutRigthHtml, _LpCount, out NewCount);
        //            strLeftHtml += strOutLeftHtml;
        //            strRightHtml += strOutRigthHtml;
        //            _LpCount = NewCount;
        //        }
        //        else if (Convert.ToInt32(ItemType) > 7)
        //        {
        //            strOutLeftHtml = ""; strOutRigthHtml = "";
        //            WfParamHtmlForStruct(paramItem.name, paramItem, strLevel, "", out strOutLeftHtml, out strOutRigthHtml, _LpCount, out NewCount);
        //            strLeftHtml += strOutLeftHtml;
        //            strRightHtml += strOutRigthHtml;
        //            _LpCount = NewCount;
        //        }
        //    }

        //    ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#WfLPWsdlContainerDiv1').html('" + strLeftHtml + "');$('#WfLPWsdlContainerDiv2').html('" + strRightHtml + "');WfInnerEditDivShow();", true);

        //}

        //private void WfParamHtmlForStruct(string _StructName, param _Inpara, string _Level, string _ParentPara, out string _LeftHtml, out string _RightHtml, int _LpCount, out int NewCount)
        //{
        //    string strLevel = (Convert.ToInt32(_Level) + 1).ToString();
        //    string strLeftHtml = "";
        //    string strRightHtml = "";
        //    string strAllControl = hdfAllCtrlInFrm.Value;
        //    string strMarginLeft = (Convert.ToInt32(strLevel) * 20).ToString();

        //    strRightHtml += "<div class=\"LpContentRow\"><div class=\"OnClsContentText\" style=\"width: 50px;\"></div><div class=\"OnClsContentText\"></div> </div><div style=\"clear: both;\"></div>";
        //    strLeftHtml += "<div class=\"LpContentText1\"><div style=\"color:#000;font-weight:bold;margin-left:" + (Convert.ToInt32(_Level) * 20).ToString() + "px;\">" + _StructName + "</div></div><div style=\"clear: both;\"></div>";
        //    _LpCount = _LpCount + 1;

        //    foreach (param paramItem in _Inpara.inparam)
        //    {
        //        if (Convert.ToInt32(paramItem.typ) <= 6)
        //        {
        //            strRightHtml += "<div id=\"LpContentText_" + _LpCount + "\" class=\"LpContentRow\"><div class=\"OnClsContentText\" style=\"width: 50px;\"  align=\"center\">" + "<input id=\"WfWsLpChkCtrlPick_" + _LpCount + "\" type=\"checkbox\" checked=\"checked\" />" + "</div><div class=\"OnClsContentText\"><input id=\"LpConstantValue_" + _LpCount + "\" type=\"text\"  class=\"txtOnCls\" /></div> </div><div style=\"clear: both;\"></div>";
        //            strLeftHtml += "<div class=\"LpContentText1\"><div style=\"margin-left:" + strMarginLeft + "px;\"><a id=\"Lptext_" + _LpCount + "\" type=\"Parameter\">" + paramItem.name + "</a><a id=\"LpTextAdd_" + _LpCount + "\" style=\"display:none;\">" + paramItem.name + "</a></div></div><div style=\"clear: both;\"></div>";
        //            _LpCount = _LpCount + 1;

        //        }
        //        else if (Convert.ToInt32(paramItem.typ) == 7)
        //        {
        //            string strOutLeftHtml = "", strOutRigthHtml = "";
        //            WfParamHtmlForStruct(paramItem.name, paramItem, strLevel, _StructName, out strOutLeftHtml, out strOutRigthHtml, _LpCount, out NewCount);
        //            strLeftHtml += strOutLeftHtml;
        //            strRightHtml += strOutRigthHtml;
        //            _LpCount = NewCount;
        //        }
        //        else if (Convert.ToInt32(paramItem.typ) > 7)
        //        {
        //            string strOutLeftHtml = "", strOutRigthHtml = "";
        //            WfParamHtmlForStruct(paramItem.name, paramItem, strLevel, _StructName, out strOutLeftHtml, out strOutRigthHtml, _LpCount, out NewCount);
        //            strLeftHtml += strOutLeftHtml;
        //            strRightHtml += strOutRigthHtml;
        //            _LpCount = NewCount;
        //        }
        //    }
        //    _LeftHtml = strLeftHtml;
        //    _RightHtml = strRightHtml;
        //    NewCount = _LpCount;
        //}

        private string LinkRpcOutParameters(string _Json, out int _Count)
        {
            XmlRpcParser obj = new XmlRpcParser();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(_Json));
            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());
            obj = serializer.ReadObject(ms) as XmlRpcParser;
            ms.Close();

            string strLevel = "1";
            string ItemType = "";
            string strLeftHtml = "";
            int count = 0;
            int intOutCount = 0;
            foreach (param paramItem in obj.param)
            {
                ItemType = paramItem.typ.Trim();
                if (Convert.ToInt32(ItemType) <= 6)
                {
                    strLeftHtml += "<div class=\"WsdlContentText\"><div style=\"margin-left:40px;\"><a id=\"OutElementtext_" + count + "\" type=\"Parameter\" style=\"font-weight:bold;\">" + paramItem.name + "</a><a id=\"OutElementAdd_" + count + "\" style=\"display:none;\">" + paramItem.name + "</a></div></div><div style=\"clear: both;\"></div>";
                    count = count + 1;
                }
                else if (Convert.ToInt32(ItemType) == 7)
                {
                    intOutCount = 0;
                    strLeftHtml += GetRpcOutParamHtmlForStruct(paramItem.name, paramItem, strLevel, "", count, out intOutCount);
                    count = intOutCount;
                }
                else if (Convert.ToInt32(ItemType) > 7)
                {
                    intOutCount = 0;
                    strLeftHtml += GetRpcOutParamHtmlForStruct(paramItem.name, paramItem, strLevel, "", count, out intOutCount);
                    count = intOutCount;
                }
            }
            _Count = count;
            return strLeftHtml;
        }

        private string GetRpcOutParamHtmlForStruct(string _StructName, param _Inpara, string _Level, string _ParentPara, int _Count, out int _OutCount)
        {
            string strLevel = (Convert.ToInt32(_Level) + 1).ToString();
            string ItemType = "";
            string strLeftHtml = "";
            string strMarginLeft = "style=\"margin-left:" + (Convert.ToInt32(strLevel) * 40).ToString() + "px;\"";

            int intOutCount = 0;

            strLeftHtml += "<div class=\"WsdlContentText\"><div style=\"margin-left:" + (Convert.ToInt32(_Level) * 40).ToString() + "px;\"><a id=\"OutElementtext_" + _Count + "\" type=\"Parameter\" style=\"font-weight:bold;\">" + _StructName + "</a><a id=\"OutElementAdd_" + _Count + "\" style=\"display:none;\">" + (_ParentPara.Length > 0 ? _ParentPara + "." + _StructName : _StructName) + "</a></div></div><div style=\"clear: both;\"></div>";

            _Count = _Count + 1;
            foreach (param paramItem in _Inpara.inparam)
            {
                ItemType = paramItem.typ.Trim();
                if (Convert.ToInt32(ItemType) <= 6)
                {
                    strLeftHtml += "<div class=\"WsdlContentText\"><div " + strMarginLeft + "><a id=\"OutElementtext_" + _Count + "\" type=\"Parameter\" style=\"font-weight:bold;\">" + paramItem.name + "</a><a id=\"OutElementAdd_" + _Count + "\" style=\"display:none;\">" + (_ParentPara.Length > 0 ? _ParentPara + "." + _StructName + "." + paramItem.name : _StructName + "." + paramItem.name) + "</a></div></div><div style=\"clear: both;\"></div>";
                    _Count = _Count + 1;

                }
                else if (Convert.ToInt32(ItemType) == 7)
                {
                    intOutCount = 0;
                    strLeftHtml += GetRpcOutParamHtmlForStruct(paramItem.name, paramItem, strLevel, (_ParentPara.Length > 0 ? _ParentPara + "." + _StructName : _StructName), _Count, out intOutCount);
                    _Count = intOutCount;
                }
                else if (Convert.ToInt32(ItemType) == 8)
                {
                    intOutCount = 0;
                    strLeftHtml += GetRpcOutParamHtmlForStruct(paramItem.name, paramItem, strLevel, (_ParentPara.Length > 0 ? _ParentPara + "." + _StructName : _StructName), _Count, out intOutCount);
                    _Count = intOutCount;
                }
            }
            _OutCount = _Count;
            return strLeftHtml;
        }

        #endregion

        #region Renew Session
        protected void btnRenewSession_Click(object sender, EventArgs e)
        {
            if (txtReEnterPassword.Text.Trim().Length == 0)
            {
                //ScriptManager.RegisterStartupScript(updReNewSession, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aMessage').html('" + "Please enter password." + "');SubProcBoxMessage(true);", true);
                Utilities.runPostBackScript("bindRenewSessTxtPwdEvent();", updReNewSession, "BindTxtPwdEvnetOnPostback");
                Utilities.showMessage("Please enter password.", updReNewSession, DIALOG_TYPE.Error);
                return;
            }
            else
            {
                try
                {
                    ReNewSession();
                }
                catch
                {
                    //ScriptManager.RegisterStartupScript(updReNewSession, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"$('#aMessage').html('" + "* Internal server error." + "');SubProcBoxMessage(true);", true);
                    Utilities.runPostBackScript("bindRenewSessTxtPwdEvent();", updReNewSession, "BindTxtPwdEvnetOnPostback");
                    Utilities.showMessage("Internal server error.", updReNewSession, DIALOG_TYPE.Error);
                }
            }
        }

        private void ReNewSession()
        {
            UserLogin obj = new UserLogin(this.CompanyId, this.SubAdminid, this.Adminid, Utilities.GetMd5Hash(txtReEnterPassword.Text), this.Sessionid);
            MasterPage mainMaster = Page.Master;
            obj.RenewSession(((HiddenField)mainMaster.FindControl("hfbid")).Value);

            string strIdeSessionKey = CacheManager.GetKey(CacheManager.CacheType.mFicientIDE, this.CompanyId, this.SubAdminid, MficientConstants.WEB_DEVICE_TYPE_AND_ID, MficientConstants.WEB_DEVICE_TYPE_AND_ID, this.Sessionid, "");
            int? iAttemptCount = CacheManager.Get<Int32>(strIdeSessionKey);
            if (iAttemptCount == null || iAttemptCount == 0)//this case occurs if renew session is happening for first time.
            {
                iAttemptCount = 1;
            }

            if (obj.StatusCode == 0)
            {
                string strHdf = Utilities.EncryptString(this.SubAdminid + "," + obj.SessionId + "," + this.Adminid + "," + this.CompanyId);
                //ScriptManager.RegisterStartupScript(updReNewSession, typeof(UpdatePanel), Guid.NewGuid().ToString(), "SubProcRenewSession(false);", true);
                try
                {
                    CacheManager.Insert<int>(strIdeSessionKey, 1, DateTime.UtcNow, TimeSpan.FromSeconds(MficientConstants.SESSION_VALIDITY_SECONDS));
                    //Cache[strIdeSessionKey] = 1;
                }
                catch
                {
                    //Cache[strIdeSessionKey] = 1;
                }
                if (!String.IsNullOrEmpty(hidDtlsForPostBackByHtmlCntrl.Value))
                {
                    PostbackByHtmlControlData objPostbackData =
                            Utilities.DeserialiseJson<PostbackByHtmlControlData>(hidDtlsForPostBackByHtmlCntrl.Value);
                    if (objPostbackData != null)
                    {
                        POST_BACK_BY_HTML_PROCESS eProcFor = (POST_BACK_BY_HTML_PROCESS)Enum.Parse(typeof(POST_BACK_BY_HTML_PROCESS),
                                    objPostbackData.processFor);
                        doTasksByProcessType(eProcFor, objPostbackData, updReNewSession);
                    }
                }
                ScriptManager.RegisterStartupScript(updReNewSession, typeof(UpdatePanel), Guid.NewGuid().ToString(), "SubProcRenewSession(false);$('[id$=hfbid]').val('" + obj.BrowserWindowId + "');$('[id$=hfs]').val('" + strHdf + "');", true);
                Utilities.runPostBackScript("bindRenewSessTxtPwdEvent();", updReNewSession, "BindTxtPwdEvnetOnPostback");
                lblReAttempt.Text = "First attempt";
                updLbl.Update();
            }
            else
            {
                try
                {
                    CacheManager.Insert<int>(strIdeSessionKey,
                                             iAttemptCount + 1,
                                             DateTime.UtcNow,
                                             TimeSpan.FromSeconds(MficientConstants.SESSION_VALIDITY_SECONDS));

                    if (iAttemptCount == 3)
                    {
                        CacheManager.Remove(strIdeSessionKey);
                        Response.Redirect("~/Default.aspx");
                    }
                    else
                    {
                        if (iAttemptCount + 1 == 2)
                        {
                            lblReAttempt.Text = "Second attempt";
                        }
                        else if (iAttemptCount + 1 == 3)
                        {
                            lblReAttempt.Text = "Last attempt";
                        }
                        //lblReAttempt.Text = "Attempt " + (Convert.ToInt16(Cache[strIdeSessionKey]) + 1) + " of 3";
                        updLbl.Update();
                    }

                    #region Previous Code
                    //if (Convert.ToInt16(Cache[strIdeSessionKey]) == 3)
                    //{
                    //    CacheManager.Remove(strIdeSessionKey);
                    //    Response.Redirect("~/Default.aspx");
                    //}
                    //else
                    //{
                    //    if (Convert.ToInt16(Cache[strIdeSessionKey]) + 1 == 2)
                    //    {
                    //        lblReAttempt.Text = "Second attempt";
                    //    }
                    //    else if (Convert.ToInt16(Cache[strIdeSessionKey]) + 1 == 3)
                    //    {
                    //        lblReAttempt.Text = "Last attempt";
                    //    }
                    //    //lblReAttempt.Text = "Attempt " + (Convert.ToInt16(Cache[strIdeSessionKey]) + 1) + " of 3";
                    //    updLbl.Update();
                    //}
                    #endregion
                }
                catch
                {
                    CacheManager.Remove(strIdeSessionKey);
                    Utilities.removeCurrentCookie(String.Empty);
                    Response.Redirect("~/Default.aspx");
                }
                //ScriptManager.RegisterStartupScript(updReNewSession, typeof(UpdatePanel), Guid.NewGuid().ToString(), "  $('#aMessage').html('Please enter correct password.');SubProcBoxMessage(true);", true);
                Utilities.runPostBackScript("bindRenewSessTxtPwdEvent();", updReNewSession, "BindTxtPwdEvnetOnPostback");
                Utilities.showMessage("Please enter correct password.", updReNewSession, DIALOG_TYPE.Error);
            }

        }

        /// <summary>
        /// Checks the session and does the process for renew session.(asks for password in ui).
        /// </summary>
        /// <returns>bool true if session is valid else false .</returns>
        private bool processCheckSession()
        {
            bool isSessionValid = false;
            try
            {
                MasterPage mainMaster = Page.Master;
                string strHfbid = (((HiddenField)mainMaster.FindControl("hfbid")).Value);
                string strHfs = (((HiddenField)mainMaster.FindControl("hfs")).Value);

                bool deviceIdExistsInCookie, cookieExists;

                bool sessionExists = Utilities.AuthenticateUserSessionFromCache(this.SubAdminid, this.Adminid
                                                            , this.CompanyId, this.Sessionid
                                                            , strHfbid, Cache, out deviceIdExistsInCookie, out cookieExists);
                /**
                * Session      DeviceId
                * n            n    //unlikely no need to do anything
                * n            y    // remove all the cookie and redirect
                * y            n   // some other user is logged in.Don't remove cookie but redirect,there is no need to remove anything as device already does not exist
                 *                  //there could be the case where cookie has expired befored the session in that case ask for renewal.
                * y            y   //every things perfect
                * **/
                if (!sessionExists || !cookieExists || !deviceIdExistsInCookie)
                {
                    isSessionValid = false;
                    txtReEnterPassword.Text = "";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "SubProcRenewSession(true);", true);
                }
                else
                {
                    isSessionValid = true;
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "setIfp('" + "//enterprise.mficient.com/CompanyImages/" + this.CompanyId + "/mediafiles" + "');setExpiryDateOfCookie();", true);
            }
            catch
            {
            }
            return isSessionValid;
        }
        #endregion
        #region Log out
        protected void btnLogout_Click(object sender, EventArgs e)
        {
            UserLogOut objUserLogOut = new UserLogOut(this.SubAdminid,
                this.Sessionid, this.Adminid, this.CompanyId,
                Cache, LogOutReason.NORMAL);
            objUserLogOut.Process();


            Utilities.removeCurrentCookie(String.Empty);
            Response.Redirect(@"Default.aspx");

        }
        #endregion
        #region WorkFlowCommit

        private Boolean CheckWorkFlowJsonValidation(string _CompanyId, string _WfId, string _SubAdminId, out string _ErrorMsg, out DataTable _WfCommitedDataTable)
        {
            _ErrorMsg = "";
            _WfCommitedDataTable = null;
            GetDetailsToValidateWfJson obj = new GetDetailsToValidateWfJson(_WfId, _SubAdminId, _CompanyId);
            obj.Process();
            if (obj.StatusCode != 0)
            {
                _ErrorMsg = "<br />App flow does not exists.";
                return false;
            }
            else
            {
                _WfCommitedDataTable = obj.WfCommitedResultTable;
                if (obj.WfResultTable == null || obj.WfResultTable.Rows.Count == 0)
                {
                    _ErrorMsg = "<br />App flow does not exists.";
                    return false;
                }
                else
                {
                    //DataRow[] dr;
                    //WorkFlow ObjWorkFlow;
                    //MemoryStream ms;
                    //System.Runtime.Serialization.Json.DataContractJsonSerializer serializer;
                    //Boolean bolTablet = (obj.WfResultTable.Rows.Count > 1 ? true : false);
                    //foreach (DataRow row in obj.WfResultTable.Rows)
                    //{
                    //    if (Convert.ToString(row["WF_JSON"]).Trim().Length == 0)
                    //        return false;
                    //    else
                    //    {
                    //        ObjWorkFlow = new WorkFlow();
                    //        ms = new MemoryStream(Encoding.Unicode.GetBytes(Convert.ToString(row["WF_JSON"])));
                    //        serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(ObjWorkFlow.GetType());
                    //        ObjWorkFlow = serializer.ReadObject(ms) as WorkFlow;
                    //        ms.Close();
                    //        if (ObjWorkFlow.wf.views.Count == 0)
                    //        {
                    //            if (bolTablet)
                    //            {
                    //                if (Convert.ToString(row["MODEL_TYPE"]) == "0") _ErrorMsg = "<br />App flow for phone does not have any view.";
                    //                else _ErrorMsg = "<br />App flow for tablet does not have any view.";
                    //            }
                    //            else _ErrorMsg = "<br />App flow does not have any view.";
                    //            return false;
                    //        }
                    //        foreach (var frm in ObjWorkFlow.wf.views)
                    //        {
                    //            if (frm.id.StartsWith("F"))
                    //            {
                    //                dr = obj.FrmResultTable.Select("FORM_ID='" + frm.fid + "'");
                    //                if (dr.Length == 0)
                    //                {
                    //                    _ErrorMsg = "<br />Form does not exists.";
                    //                    return false;
                    //                }
                    //                if (frm.cmds != null)
                    //                {
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    return true;
                }
            }
        }

        private void CommitWorkFlowDetails(string _WfId, string _SubAdminId, UpdatePanel _Upd, string _CompanyId)
        {
            string str = "";
            DataTable WfCommitedDataTable;
            if (!CheckWorkFlowJsonValidation(_CompanyId, _WfId, _SubAdminId, out str, out WfCommitedDataTable))
            {
                //ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aMessage').html('App connot be committed.</ br>" + str + "');SubProcBoxMessage(true);", true);
                //return;
                throw new MficientException("App cannot be committed.</br>" + str + " ");
            }
            else
            {
                //hdfCommitedWfId.Value = _WfId;
                txtWfVersionHistroy.Text = "";
                //rdbMajor.Checked = false;
                rdbAppCommitMinor.Visible = true;
                rdbAppCommitMinor.Checked = true;
                chkPublishApp.Checked = false;
                rdbAppCommitMajor.Checked = false;

                lblmsg.Text = "";
                if (WfCommitedDataTable != null)
                {

                    if (WfCommitedDataTable.DefaultView.ToTable(true, "VERSION").Rows.Count >= MficientConstants.MAXIMUM_COMITTED_VERSIONS)
                    {
                        lblmsg.Text = "<img src=\"//enterprise.mficient.com/images/warning.png\"/>&nbsp;&nbsp;&nbsp;At any point of time only " +
                                MficientConstants.MAXIMUM_COMITTED_VERSIONS + " version of any app can be exists. If you commit then oldest unpublished app version " +
                                GetMinVersion(WfCommitedDataTable).ToString() + " will be deleted.";
                    }
                    else if (WfCommitedDataTable.Rows.Count == 0)
                    {
                        rdbAppCommitMinor.Visible = false;
                        rdbAppCommitMajor.Checked = true;
                    }
                    else
                    {
                        //rdbCommitMinor.Checked = true;
                        //rdbMajor.Checked = false;
                        //txtWfVersionHistroy.Text = "This is testing.";
                    }
                }
                //updCommitWorkFlow.Update();
                //ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel),
                //Guid.NewGuid().ToString(),
                //"mFicientIde.MfApp.AppDesigner.mainMenuHelper.showGetCommitAppVersionPopup();", true);
                updCommitWorkFlow.Update();
                Utilities.runPostBackScript("mFicientIde.MfApp.AppDesigner.mainMenuHelper.showGetCommitAppVersionPopup();",
                                _Upd, "ShowGetCommitAppVersion");
            }
        }
        private void ProcessCommitHtml5AppDetails(string appId, string subAdminId, UpdatePanel _Upd, string companyId)
        {

            GetHtml5AppPublishDetail objGetHtml5PrevPublishDtl = new GetHtml5AppPublishDetail(this.SubAdminid, this.CompanyId);
            objGetHtml5PrevPublishDtl.GetHtml5PreviousPublishedDtls(appId);
            if (objGetHtml5PrevPublishDtl.StatusCode != 0)
            {
                throw new MficientException("Internal server error");
            }
            List<MFEHtml5AppPublishDetail> lstPrevPublishedDtls = objGetHtml5PrevPublishDtl.Html5AppPublishDtls;
            List<string> lstVersions = new List<string>();
            txtHtml5CommitVersionHistory.Text = "";
            rdbHtml5CommitMajor.Checked = false;
            rdbHtml5CommitMinor.Visible = true;
            rdbHtml5CommitMinor.Checked = true;
            lblHtml5CommitAppMsg.Text = "";
            if (lstPrevPublishedDtls != null)
            {
                if (lstPrevPublishedDtls.Count == 0)
                {
                    rdbHtml5CommitMinor.Visible = false;
                    rdbHtml5CommitMajor.Checked = true;
                    rdbHtml5CommitMajor.Visible = true;
                }
                else if (lstPrevPublishedDtls.Count == MficientConstants.MAXIMUM_COMITTED_VERSIONS)
                {
                    foreach (MFEHtml5AppPublishDetail publishDtl in lstPrevPublishedDtls)
                    {
                        lstVersions.Add(publishDtl.Version);
                    }
                    lblHtml5CommitAppMsg.Text = "<img src=\"//enterprise.mficient.com/images/warning.png\"/>&nbsp;&nbsp;&nbsp;At any point of time only " +
                            MficientConstants.MAXIMUM_COMITTED_VERSIONS + " version of any app can be exists. If you commit then oldest unpublished app version " +
                            getMinVersionOfPublishedApp(lstVersions).ToString() + " will be deleted.";
                }
            }
            updHtml5CommitApp.Update();
            Utilities.runPostBackScript("mfHtml5App.html5AppCommit.showGetCommitAppVersionPopup();",
                            updPostbackByHtmlCntrl, "ShowGetCommitAppVersion");

        }
        private string GetMinVersion(DataTable _dt)
        {
            DataRow[] dr = _dt.Select("PUBLISH_ON='0'");

            string versions = "";
            if (dr == null || dr.Length == 0)
                return versions;

            decimal decMinVersion = 0;
            int index = 0;
            foreach (DataRow itemRow in dr)
            {
                if (decMinVersion == Convert.ToDecimal(0))
                {
                    decMinVersion = Convert.ToDecimal(itemRow["VERSION"]);
                    index++;
                }
                else
                {
                    if (decMinVersion > Convert.ToDecimal(itemRow["VERSION"]))
                    {
                        decMinVersion = Convert.ToDecimal(itemRow["VERSION"]);
                        index++;
                    }
                    else { }
                }
                if (index > MficientConstants.MAXIMUM_COMITTED_VERSIONS)
                    versions += versions.Length == 0 ? decMinVersion.ToString() : (" ," + decMinVersion.ToString());
            }
            return versions;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="versions"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        private decimal getMinVersionOfPublishedApp(List<string> versions)
        {
            if (versions == null) throw new ArgumentNullException();
            decimal decMinVersion = 0;
            if (versions.Count == 0)
            {
                decMinVersion = 0;
            }
            else
            {
                foreach (string version in versions)
                {
                    if (decMinVersion == Convert.ToDecimal(0))
                    {
                        decMinVersion = Convert.ToDecimal(version);
                    }
                    else
                    {
                        if (decMinVersion > Convert.ToDecimal(version))
                        {
                            decMinVersion = Convert.ToDecimal(version);
                        }
                        else { }
                    }
                }
            }
            return decMinVersion;
        }
        private double getMaxVersionOfPublishedApp(List<string> versions)
        {
            if (versions == null) throw new ArgumentNullException();
            double decMaxVersion = 1.00;
            if (versions.Count == 0)
            {
                decMaxVersion = 1.00;
            }
            else
            {
                foreach (string version in versions)
                {
                    if (decMaxVersion == Convert.ToDouble(0))
                    {
                        decMaxVersion = Convert.ToDouble(version);
                    }
                    else
                    {
                        if (decMaxVersion < Convert.ToDouble(version))
                        {
                            decMaxVersion = Convert.ToDouble(version);
                        }
                        else { }
                    }
                }
            }
            return decMaxVersion;
        }
        protected void btnCommitWorkFlow_Click(object sender, EventArgs e)
        {
            //if (txtWfVersionHistroy.Text.Trim().Length == 0)
            //{
            //    ScriptManager.RegisterStartupScript(updCommitWorkFlow, typeof(UpdatePanel), Guid.NewGuid().ToString(), "CommonErrorMessages(38);", true);
            //    return;
            //}
            //else
            //{
            //    GetDetailsToValidateWfJson obj = new GetDetailsToValidateWfJson(hdfCommitedWfId.Value, this.SubAdminid, this.CompanyId);
            //    obj.Process();
            //    if (obj.StatusCode != 0)
            //        return;
            //    else
            //    {
            //        string str = "1.0";
            //        if (obj.WfCommitedResultTable != null)
            //        {
            //            if (obj.WfCommitedResultTable.Rows.Count != 0)
            //            {
            //                str = "";
            //                if (rdbMajor.Checked)
            //                    str = (Convert.ToInt32(obj.WfCommitedResultTable.Select("VERSION = MAX(VERSION)")[0]["VERSION"]) + 1).ToString();
            //                else
            //                    str = (Convert.ToDecimal(obj.WfCommitedResultTable.Select("VERSION = MAX(VERSION)")[0]["VERSION"]) + Convert.ToDecimal("0.01")).ToString();

            //            }
            //            else { }
            //        }
            //        else { }
            //        Boolean IsDeleteApp = false;
            //        if (obj.WfCommitedResultTable != null)
            //        {
            //            if (obj.WfCommitedResultTable.DefaultView.ToTable(true, "VERSION").Rows.Count == MficientConstants.MAXIMUM_COMITTED_VERSIONS) //if (obj.WfCommitedResultTable.Rows.Count == 6)
            //            {
            //                IsDeleteApp = true;
            //            }
            //            else { }
            //        }
            //        else { }

            //        CommitWorkFlow objCommitWorkFlow = new CommitWorkFlow(obj.WfResultTable, str, txtWfVersionHistroy.Text, IsDeleteApp, (IsDeleteApp ? GetMinVersion(obj.WfCommitedResultTable).ToString().ToString() : ""));
            //        objCommitWorkFlow.Process();

            //        if (objCommitWorkFlow.StatusCode == 0)
            //        {
            //            ScriptManager.RegisterStartupScript(updCommitWorkFlow, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubprocCommitWorkFlow(false);$('#aCfmMessage').html('App (ver " + (str.Contains('.') ? str : str + ".00") + ") committed successfully.');SubProcConfirmBoxMessage(true,'  Saved',250);", true);
            //        }
            //        else
            //        {
            //            ScriptManager.RegisterStartupScript(updCommitWorkFlow, typeof(UpdatePanel), Guid.NewGuid().ToString(), "CommonErrorMessages(39);", true);
            //        }
            //    }
            //}
        }
        void appCommitSave(string appId)
        {
            if (txtWfVersionHistroy.Text.Trim().Length == 0)
            {
                //ScriptManager.RegisterStartupScript(updCommitWorkFlow, typeof(UpdatePanel), Guid.NewGuid().ToString(), "CommonErrorMessages(38);", true);
                //return;
                throw new MficientException("Please enter version history.");
            }
            else
            {
                GetDetailsToValidateWfJson obj = new GetDetailsToValidateWfJson(appId, this.SubAdminid, this.CompanyId);
                obj.Process();
                if (obj.StatusCode != 0)
                    return;
                else
                {
                    string strNewVersion = "1.0";
                    bool blnAppPublishSuccessfull = false;
                    if (obj.WfCommitedResultTable != null)
                    {
                        if (obj.WfCommitedResultTable.Rows.Count != 0)
                        {
                            strNewVersion = "";
                            if (rdbAppCommitMajor.Checked)
                                strNewVersion = (Convert.ToInt32(obj.WfCommitedResultTable.Select("VERSION = MAX(VERSION)")[0]["VERSION"]) + 1).ToString();
                            else
                                strNewVersion = (Convert.ToDecimal(obj.WfCommitedResultTable.Select("VERSION = MAX(VERSION)")[0]["VERSION"]) + Convert.ToDecimal("0.01")).ToString();

                        }
                        else { }
                    }
                    else { }
                    Boolean IsDeleteApp = false;
                    if (obj.WfCommitedResultTable != null)
                    {
                        if (obj.WfCommitedResultTable.DefaultView.ToTable(true, "VERSION").Rows.Count >= MficientConstants.MAXIMUM_COMITTED_VERSIONS) //if (obj.WfCommitedResultTable.Rows.Count == 6)
                        {
                            IsDeleteApp = true;
                        }
                        else { }
                    }
                    else { }

                    CommitWorkFlow objCommitWorkFlow = new CommitWorkFlow(obj.WfResultTable, strNewVersion,
                        txtWfVersionHistroy.Text, IsDeleteApp,
                        (IsDeleteApp ? GetMinVersion(obj.WfCommitedResultTable).ToString().ToString() : ""));

                    objCommitWorkFlow.NewProcess();

                    if (objCommitWorkFlow.StatusCode == 0)
                    {
                        //ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel),
                        //Guid.NewGuid().ToString(),
                        //"mFicientIde.MfApp.AppDesigner.mainMenuHelper.showGetCommitAppVersionPopup();", true);

                        try
                        {
                            if (obj.WfPublishedTable != null && chkPublishApp.Checked)
                            {
                                string strPreviousVersion = String.Empty;
                                if (obj.WfPublishedTable.Rows.Count > 0)
                                {
                                    strPreviousVersion = Convert.ToString(obj.WfPublishedTable.Rows[0]["VERSION"]);
                                }
                                else
                                {
                                    strPreviousVersion = "-1";
                                }
                                PublishApplication objPublishApp = new PublishApplication(appId, this.SubAdminid, strPreviousVersion, strNewVersion, this.CompanyId);
                                objPublishApp.Process();
                                if (objPublishApp.StatusCode == 0)
                                {
                                    Utilities.SendNotificationMenuUpdate(this.CompanyId,"1");
                                    blnAppPublishSuccessfull = true;
                                }
                                else
                                {
                                    blnAppPublishSuccessfull = false;
                                }
                            }
                        }
                        catch
                        {
                            blnAppPublishSuccessfull = false;
                        }
                        string strMessageForUI = "App (ver " + (strNewVersion.Contains('.') ? strNewVersion : strNewVersion + ".00") + ") committed successfully.";
                        if (chkPublishApp.Checked)
                        {
                            if (blnAppPublishSuccessfull)
                            {
                                strMessageForUI += "</br></br>The App was published successfully.";
                            }
                            else
                            {
                                strMessageForUI += "</br></br>An error occured while publishing the App.Please try again.";
                            }
                        }
                        Utilities.runPostBackScript("mFicientIde.MfApp.AppDesigner.mainMenuHelper.closeGetCommitAppVersionPopup();",
                                updPostbackByHtmlCntrl, "CloseGetCommitAppVersion");
                        Utilities.showMessage(strMessageForUI, updPostbackByHtmlCntrl, "SucessMessageCommit", DIALOG_TYPE.Info);
                        //ScriptManager.RegisterStartupScript(updCommitWorkFlow, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"SubprocCommitWorkFlow(false);$('#aCfmMessage').html('App (ver " + (str.Contains('.') ? str : str + ".00") + ") committed successfully.');SubProcConfirmBoxMessage(true,'  Saved',250);", true);
                    }
                    else
                    {
                        //ScriptManager.RegisterStartupScript(updCommitWorkFlow, typeof(UpdatePanel), Guid.NewGuid().ToString(), "CommonErrorMessages(39);", true);
                        throw new MficientException("Internal server error");
                    }
                }
            }
        }
        void html5AppCommitSave(MFEHtml5AppDetail html5AppDtl)
        {
            if (txtHtml5CommitVersionHistory.Text.Trim().Length == 0)
            {
                throw new MficientException("Please enter version history.");
            }
            else
            {
                string strVersionToSave = "1.0";
                string strOldestVersion = "";
                GetHtml5AppPublishDetail objGetHtml5PrevPublishDtl = new GetHtml5AppPublishDetail(this.SubAdminid, this.CompanyId);
                objGetHtml5PrevPublishDtl.GetHtml5PreviousPublishedDtls(html5AppDtl.AppId);
                if (objGetHtml5PrevPublishDtl.StatusCode != 0)
                {
                    throw new MficientException("Internal server error");
                }
                List<MFEHtml5AppPublishDetail> lstPrevPublishedDtls = objGetHtml5PrevPublishDtl.Html5AppPublishDtls;
                Boolean blnDeleteOldestVersion = false;
                if (lstPrevPublishedDtls != null)
                {
                    if (lstPrevPublishedDtls.Count > 0)
                    {
                        strVersionToSave = "";
                        List<string> lstVersions = new List<string>();
                        foreach (MFEHtml5AppPublishDetail publishDtl in lstPrevPublishedDtls)
                        {
                            lstVersions.Add(publishDtl.Version);
                        }
                        if (lstPrevPublishedDtls.Count == MficientConstants.MAXIMUM_COMITTED_VERSIONS) //if (obj.WfCommitedResultTable.Rows.Count == 6)
                        {
                            blnDeleteOldestVersion = true;
                            strOldestVersion = Convert.ToString(getMinVersionOfPublishedApp(lstVersions));
                        }
                        double dblMaxVersion = getMaxVersionOfPublishedApp(lstVersions);
                        if (rdbHtml5CommitMajor.Checked)
                            strVersionToSave = (dblMaxVersion + 1).ToString();
                        else
                            strVersionToSave = (dblMaxVersion + 0.01).ToString();
                    }
                }

                MFEHtml5AppPublishDetail objAppPublishDetl = new MFEHtml5AppPublishDetail();
                objAppPublishDetl.CompanyId = this.CompanyId;
                objAppPublishDetl.SubadminId = this.SubAdminid;
                objAppPublishDetl.AppId = html5AppDtl.AppId;
                objAppPublishDetl.AppName = html5AppDtl.AppName;
                objAppPublishDetl.Description = html5AppDtl.Description;
                objAppPublishDetl.AppJson = html5AppDtl.AppJson;
                objAppPublishDetl.Version = strVersionToSave;
                objAppPublishDetl.WorkedBy = this.SubAdminid;
                objAppPublishDetl.PublishedOn = 0;
                objAppPublishDetl.CommittedOn = DateTime.UtcNow.Ticks;
                objAppPublishDetl.VersionHistory = txtHtml5CommitVersionHistory.Text;
                objAppPublishDetl.Icon = html5AppDtl.Icon;
                objAppPublishDetl.BucketFilePath = html5AppDtl.BucketFilePath;


                CommitHtml5App objCommitApp = new CommitHtml5App(this.SubAdminid, this.CompanyId);
                objCommitApp.Commit(objAppPublishDetl, blnDeleteOldestVersion, strOldestVersion);

                if (objCommitApp.StatusCode == 0)
                {
                    Utilities.runPostBackScript("mfHtml5App.html5AppCommit.closeGetCommitAppVersionPopup();",
                                                updPostbackByHtmlCntrl, "CloseGetCommitAppVersion");
                    Utilities.showMessage("App (ver " + (strVersionToSave.Contains('.') ? strVersionToSave : strVersionToSave + ".00") + ") committed successfully.", updPostbackByHtmlCntrl, "SucessMessageCommit", DIALOG_TYPE.Info);
                }
                else
                {
                    //ScriptManager.RegisterStartupScript(updCommitWorkFlow, typeof(UpdatePanel), Guid.NewGuid().ToString(), "CommonErrorMessages(39);", true);
                    throw new MficientException("Internal server error");
                }
            }
        }
        void testerAppCommitSave(string appId)
        {
            if (String.IsNullOrEmpty(appId)) throw new ArgumentNullException();
            List<jqueryAppClass> lstApps = new List<jqueryAppClass>();
            if (!String.IsNullOrEmpty(hidPhoneAppForSaving.Value))
            {
                lstApps.Add(Utilities.DeserialiseJson<jqueryAppClass>(hidPhoneAppForSaving.Value));
            }
            if (!String.IsNullOrEmpty(hidTabletAppForSaving.Value))
            {
                lstApps.Add(Utilities.DeserialiseJson<jqueryAppClass>(hidTabletAppForSaving.Value));
            }
            //This check is not required.//This is not an ideal situation.
            if (lstApps.Count > 0)
            {
                CommitWorkFlow objCommitWorkFlow = new CommitWorkFlow();
                objCommitWorkFlow.commitAppForTester(this.SubAdminid, this.CompanyId, appId, lstApps);
                if (objCommitWorkFlow.StatusCode != 0)
                {
                    throw new MficientException("Internal server error");
                }
            }
            else
            {
                throw new MficientException("No Apps found for saving");
            }
        }
        void testerHtml5AppCommitSave(MFEHtml5AppDetail html5AppDtl)
        {
            if (html5AppDtl == null) throw new ArgumentNullException();
            GetHtml5AppPublishDetail objGetHtml5PrevPublishDtl = new GetHtml5AppPublishDetail(this.SubAdminid, this.CompanyId);
            objGetHtml5PrevPublishDtl.GetLatestHtml5PreviousPublishedDtls(html5AppDtl.AppId);
            if (objGetHtml5PrevPublishDtl.StatusCode != 0)
            {
                throw new MficientException("Internal server error");
            }
            List<MFEHtml5AppPublishDetail> lstPrevPublishedDtls = objGetHtml5PrevPublishDtl.Html5AppPublishDtls;
            MFEHtml5AppPublishDetail objAppPrevPublishDtl = null;
            string strVersionToSave = "1.0";
            //This check is not required.//This is not an ideal situation.
            if (lstPrevPublishedDtls.Count > 0)
            {
                objAppPrevPublishDtl = lstPrevPublishedDtls[0];
                strVersionToSave = objAppPrevPublishDtl.Version;
            }
            objAppPrevPublishDtl = new MFEHtml5AppPublishDetail();
            objAppPrevPublishDtl.CompanyId = this.CompanyId;
            objAppPrevPublishDtl.SubadminId = this.SubAdminid;
            objAppPrevPublishDtl.AppId = html5AppDtl.AppId;
            objAppPrevPublishDtl.AppName = html5AppDtl.AppName;
            objAppPrevPublishDtl.Description = html5AppDtl.Description;
            objAppPrevPublishDtl.AppJson = html5AppDtl.AppJson;
            objAppPrevPublishDtl.Version = strVersionToSave;
            objAppPrevPublishDtl.WorkedBy = this.SubAdminid;
            objAppPrevPublishDtl.PublishedOn = 0;
            objAppPrevPublishDtl.CommittedOn = DateTime.UtcNow.Ticks;
            objAppPrevPublishDtl.VersionHistory = String.Empty;
            objAppPrevPublishDtl.Icon = html5AppDtl.Icon;
            objAppPrevPublishDtl.BucketFilePath = html5AppDtl.BucketFilePath;
            CommitHtml5App objCommitHtml5App = new CommitHtml5App(this.SubAdminid, this.CompanyId);
            objCommitHtml5App.CommitAppForTester(objAppPrevPublishDtl);
            if (objCommitHtml5App.StatusCode != 0)
            {
                throw new MficientException("Internal server error.");
            }
        }

        private void CommitTesterWorkFlowDetails(string _WfId, string _SubAdminId, UpdatePanel _Upd, string _CompanyId)
        {
            string str = "";
            DataTable WfCommitedDataTable;
            if (!CheckWorkFlowJsonValidation(_CompanyId, _WfId, _SubAdminId, out str, out WfCommitedDataTable))
            {
                //ScriptManager.RegisterStartupScript(_Upd, typeof(UpdatePanel), Guid.NewGuid().ToString(), "$('#aMessage').html('App connot be committed.</ br>" + str + "');SubProcBoxMessage(true);", true);
                //return;
                throw new MficientException("App cannot be committed.</br>" + str + " ");
            }
            else
            {
                //hdfCommitedWfId.Value = _WfId;
                txtCommitTesterDescription.Text = "";
                radCommitTesterMajor.Checked = false;
                radCommitTesterMinor.Checked = true;
                radCommitTesterMinor.Visible = true;
                lblCommitTesterAppMsg.Text = "";
                if (WfCommitedDataTable != null)
                {

                    if (WfCommitedDataTable.DefaultView.ToTable(true, "VERSION").Rows.Count >= MficientConstants.MAXIMUM_COMITTED_VERSIONS)
                    {
                        lblCommitTesterAppMsg.Text = "<img src=\"//enterprise.mficient.com/images/warning.png\"/>&nbsp;&nbsp;&nbsp;At any point of time only " +
                                MficientConstants.MAXIMUM_COMITTED_VERSIONS + " version of any app can be exists. If you commit then oldest unpublished app version " +
                                GetMinVersion(WfCommitedDataTable).ToString() + " will be deleted.";
                    }
                    else if (WfCommitedDataTable.Rows.Count == 0)
                    {
                        radCommitTesterMinor.Visible = false;
                    }
                    else { }
                }
                Utilities.runPostBackScript("mFicientIde.MfApp.AppDesigner.mainMenuHelper.showGetTesterCommitAppVersionPopup();",
                                updPostbackByHtmlCntrl, "ShowGetTesterCommitAppVersion");
            }
        }


        #endregion
        void getAllImages()
        {
            try
            {
                MFES3BucketDetails objS3BucketDtl = S3Bucket.GetS3BucketDetails(BucketType.MFICIENT_WEBSERVICE);
                S3BucketFileManager objS3FileManager = new S3BucketFileManager(
                        MficientConstants.MF_S3_MEDIA_FILES_BUCKET_NAME,
                        objS3BucketDtl.AccessKey,
                        objS3BucketDtl.SecretAccessKey);

                List<List<string>> lstObjectsUploaded = objS3FileManager.GetObjectList(S3Bucket.GetCompanyMediaFilesFolderPath(this.CompanyId));
                if (lstObjectsUploaded != null && lstObjectsUploaded.Count > 0)
                {
                    foreach (List<string> imagePath in lstObjectsUploaded)
                    {
                        if (hdfImages.Value.Trim().Length > 0)
                        {
                            hdfImages.Value += ",";
                        }
                        hdfImages.Value += imagePath[0].Substring(imagePath[0].LastIndexOf('/') + 1);
                    }
                }
            }
            catch
            {
            }

        }
        List<MFEmPluginAgent> getAllMPlugInAgents()
        {
            mPluginAgents objMpluginAgents = new mPluginAgents();
            DataSet dsAgents = objMpluginAgents.GetMpluginAgents(this.CompanyId);
            DataTable dtblAgents = null;
            List<MFEmPluginAgent> lstAgents = new List<MFEmPluginAgent>();
            if (dsAgents != null)
            {
                dtblAgents = dsAgents.Tables[0];
                if (dtblAgents.Rows.Count > 0)
                {
                    foreach (DataRow row in dtblAgents.Rows)
                    {
                        MFEmPluginAgent objAgent = new MFEmPluginAgent();
                        objAgent.CompanyId = Convert.ToString(row["COMPANY_ID"]);
                        objAgent.AgentId = Convert.ToString(row["MP_AGENT_ID"]);
                        objAgent.AgentName = Convert.ToString(row["MP_AGENT_NAME"]);
                        objAgent.AgentPassword = "";
                        lstAgents.Add(objAgent);
                    }
                }
            }
            return lstAgents;
        }
        void processSaveApp(string appId)
        {
            if (String.IsNullOrEmpty(appId)) throw new ArgumentNullException();
            SaveCompleteApp objSaveCompleteApp = new SaveCompleteApp();
            string[] aryApps;
            if (String.IsNullOrEmpty(hidPhoneAppForSaving.Value) &&
                   !String.IsNullOrEmpty(hidTabletAppForSaving.Value))
            {
                aryApps = new string[1];
                aryApps[0] = hidTabletAppForSaving.Value;
            }
            else if (!String.IsNullOrEmpty(hidPhoneAppForSaving.Value) &&
                    String.IsNullOrEmpty(hidTabletAppForSaving.Value))
            {
                aryApps = new string[1];
                aryApps[0] = hidPhoneAppForSaving.Value;
            }
            else if (!String.IsNullOrEmpty(hidPhoneAppForSaving.Value) &&
                    !String.IsNullOrEmpty(hidTabletAppForSaving.Value))
            {
                aryApps = new string[2];
                aryApps[0] = hidPhoneAppForSaving.Value;
                aryApps[1] = hidTabletAppForSaving.Value;
            }
            else if (String.IsNullOrEmpty(hidPhoneAppForSaving.Value) &&
                    String.IsNullOrEmpty(hidTabletAppForSaving.Value))
            {
                aryApps = new String[0];
            }
            else
            {
                aryApps = new String[0];
            }
            //This check is not required.//This is not an ideal situation.
            if (aryApps.Length > 0)
            {
                objSaveCompleteApp.ProcessUpdateCompleteApp(
                    this.SubAdminid,
                    this.CompanyId,
                    appId, aryApps);

                if (objSaveCompleteApp.StatusCode != 0)
                {
                    throw new MficientException("Internal server error.");
                }
            }
            else
            {
                throw new MficientException("No App found for saving.");
            }
        }
        void processUpdateHtml5App(MFEHtml5AppDetail appDetail)
        {

            //Create zip File and UPLOAD 
            List<Html5AppEntryMetaData> lstAppEntries = getZipEntriesInfoFromTempTable(appDetail.AppId);
            ZipFile objZipText = createHtml5AppZipFileToUpload(appDetail, lstAppEntries);
            uploadAppZipFileToS3Bucket(objZipText, appDetail.BucketFilePath);
            SaveHtml5App objSaveNewHtml5App = new SaveHtml5App(this.SubAdminid, this.CompanyId);
            //not getting correct json from returned value.
            objSaveNewHtml5App.UpdateApp(appDetail);
            if (objSaveNewHtml5App.StatusCode != 0)
            {
                throw new MficientException("Internal server error.");
            }
            setHtml5AppDetailHidField(appDetail.AppJson);
            updateHiddenFieldsUpdPanel();
        }
        void processGetAppDetails(APP_LIST_SOURCE appListSource, PostbackByHtmlControlData postbackData)
        {
            GetAppCompleteData objAppCmpltData = new GetAppCompleteData(this.CompanyId, this.SubAdminid);
            //string strAppDtl = Utilities.SerializeJson<List<IdeAppsForJson>>(objAppCmpltData.GetAppDetail(objPostbackData.data[0]));
            string strAppDtl = "";
            switch (appListSource)
            {
                case APP_LIST_SOURCE.Current:
                    strAppDtl = objAppCmpltData.GetAppJson(postbackData.data[0]);// Utilities.SerializeJson<List<IdeAppsForJson>>(objAppCmpltData.GetAppDetail(objPostbackData.data[0]));
                    break;
                case APP_LIST_SOURCE.Archived:
                    strAppDtl = objAppCmpltData.GetArchivedAppJson(postbackData.data[0], postbackData.data[1]);
                    break;

            }
            GetEnterpriseAdditionalDefinition objAdditionalInfoDefs = new GetEnterpriseAdditionalDefinition(this.CompanyId, this.SubAdminid);
            string strAdditionalDtls = objAdditionalInfoDefs.GetUserAdditionalProperties();
            List<string> lstModifiedAdditionalDtls = new List<string>();
            if (!String.IsNullOrEmpty(strAdditionalDtls))
            {
                List<string> lstAdditionalDtls = Utilities.DeserialiseJson<List<string>>(strAdditionalDtls);
                if (lstAdditionalDtls.Count > 0)
                {
                    foreach (string addDtl in lstAdditionalDtls)
                    {
                        if (!String.IsNullOrEmpty(addDtl))
                        {
                            lstModifiedAdditionalDtls.Add("@" + addDtl);
                        }
                    }
                }
            }

            List<UserAdditionalProperties> lstAdditionalProperties = objAdditionalInfoDefs.GetUserAmazonAWSProperties();
            if (lstAdditionalProperties == null) lstAdditionalProperties = new List<UserAdditionalProperties>();
            setAWSCredentialsHidField(Utilities.SerializeJson<List<UserAdditionalProperties>>(lstAdditionalProperties));

            strAdditionalDtls = Utilities.SerializeJson<List<string>>(lstModifiedAdditionalDtls);
            string strUsersList = objAdditionalInfoDefs.GetUserList();
            string strGroupList = objAdditionalInfoDefs.GetUserGroupsList();
            string strSubadminList = objAdditionalInfoDefs.GetSubadminList(this.SubAdminid);
            setCurrentWorkingAppHidField(strAppDtl);
            setEnterpriseAdditionalPropsHidField(strAdditionalDtls);
            setCompanyUserListHidField(strUsersList);
            setCompanyGroupsHidField(strGroupList);
            setCompanySubadminsHidField(strSubadminList);
            updateHiddenFieldsUpdPanel();

        }
        void doTasksByProcessType(POST_BACK_BY_HTML_PROCESS procType, PostbackByHtmlControlData postBackData, UpdatePanel updPanel)
        {
            //UpdatePanel thisUpdPanel = updPanel;
            #region Variables
            string strNewWfId = "";
            //IdeAppsForJson objAppForJson = null;
            //jqueryAppClass objApp = null;
            string strFormId = "";
            SaveCompleteApp objSaveCompleteApp = new SaveCompleteApp();
            string[] aryApps;
            APP_RUNS_ON eAppRunsOn = APP_RUNS_ON.PhoneAndTablet;
            //APP_MODEL_TYPE eAppMOdelType = APP_MODEL_TYPE.Phone;
            APP_DESIGN_MODE eAppDesignMode = APP_DESIGN_MODE.Phone;
            MF_STANDARD_APP_TYPE eStandardAppType = MF_STANDARD_APP_TYPE.Online;
            GetAppCompleteData objAppCmpltData = null;
            JObject jo = null;
            #endregion
            #region Switch Case
            switch (procType)
            {
                case POST_BACK_BY_HTML_PROCESS.GetAppDetails:
                    setAllDbCmdHidField(getJsonOfAllDbCommands());
                    setAllWsCmdHidField(getJsonOfAllWsCommands());
                    setAllOdataHidField(getJsonOfAllOdataCommands());
                    setOfflineTablesHidField(getJsonOfAllOfflineTables());
                    setOfflineDataObjectsHidField(getJsonOfAllOfflineDataObjects());
                    processGetAppDetails(APP_LIST_SOURCE.Current, postBackData);
                    Utilities.runPostBackScript(@"mFicientIde.MfApp.AppDesignController.initialiseSelectedApp();
                                                  mFicientIde.MfApp.GlobalData.setAppListingTypeSafe(APP_LIST_SOURCE.current);
                                                  mFicientIde.MfApp.AppDesigner.mainMenuHelper.appDesignerMenuSwitcher.doByAppListingType(APP_LIST_SOURCE.current);",
                            updPanel, "InitialiseSelectedApp");
                    break;
                case POST_BACK_BY_HTML_PROCESS.GetArchivedApp:
                    processGetAppDetails(APP_LIST_SOURCE.Archived, postBackData);
                    jo = new JObject();
                    jo["id"] = postBackData.data[0];
                    jo["version"] = postBackData.data[1];
                    hidOpenedArchivedApp.Value = jo.ToString();
                    Utilities.runPostBackScript(@"mFicientIde.MfApp.AppDesignController.initialiseSelectedApp();
                                                  mFicientIde.MfApp.GlobalData.setAppListingTypeSafe(APP_LIST_SOURCE.archived);
                                                    mFicientIde.MfApp.AppDesigner.mainMenuHelper.appDesignerMenuSwitcher.doByAppListingType(APP_LIST_SOURCE.archived);",
                            updPanel, "InitialiseSelectedApp");
                    break;
                case POST_BACK_BY_HTML_PROCESS.AppSave:
                    setAllDbCmdHidField(getJsonOfAllDbCommands());
                    setAllWsCmdHidField(getJsonOfAllWsCommands());
                    setAllOdataHidField(getJsonOfAllOdataCommands());
                    setOfflineTablesHidField(getJsonOfAllOfflineTables());
                    setOfflineDataObjectsHidField(getJsonOfAllOfflineDataObjects());

                    processSaveApp(postBackData.data[0]);
                    bindAppsList(APP_LIST_SOURCE.Current);
                    updateHiddenFieldsUpdPanel();
                    Utilities.showMessage("App saved successfully.", updPanel, DIALOG_TYPE.Info);
                    Utilities.runPostBackScript(@"bindAppsListTable();",
                    updPanel, "bindAppListAgain");
                    //aryApps[0] = hidPhoneAppForSaving.Value;
                    //aryApps[1] = hidTabletAppForSaving.Value;
                    break;
                case POST_BACK_BY_HTML_PROCESS.SaveAndCloseApp:

                    processSaveApp(postBackData.data[0]);
                    bindAppsList(APP_LIST_SOURCE.Current);
                    updateHiddenFieldsUpdPanel();
                    Utilities.showMessage("App saved successfully.", updPanel, DIALOG_TYPE.Info);
                    Utilities.runPostBackScript(@"bindAppsListTable();
                                    mFicientIde.MfApp.AppDesignController.destroyAppContainerOnAppClose();
                                    closeModalPopUp('divAppCloseConfirm');",
                    updPanel, "bindAppListAgainOnSaveAndAppClose");

                    break;
                case POST_BACK_BY_HTML_PROCESS.SaveArchivedAppAsWorkingCopy:
                    jo = JObject.Parse(hidOpenedArchivedApp.Value);
                    objSaveCompleteApp = new SaveCompleteApp();
                    objSaveCompleteApp.ProcessUpdateAppFromArchivedApp(this.SubAdminid, this.CompanyId, Convert.ToString(jo["id"]), Convert.ToString(jo["version"]));
                    if (objSaveCompleteApp.StatusCode == 0)
                    {
                        bindAppsList(APP_LIST_SOURCE.Archived);
                        hidOpenedArchivedApp.Value = "";
                        updateHiddenFieldsUpdPanel();
                        Utilities.showMessage("App saved successfully.", updPanel, DIALOG_TYPE.Info);
                        Utilities.runPostBackScript(@"bindAppsListTable(APP_LIST_SOURCE.archived);
                                    mFicientIde.MfApp.AppDesignController.destroyAppContainerOnAppClose();
                                    closeModalPopUp('divAppCloseConfirm');",
                        updPanel, "bindAppListAgainOnSaveAndAppClose");
                    }
                    else
                    {
                        Utilities.showMessage("Internal server error.", updPanel, DIALOG_TYPE.Error);
                    }
                    break;
                case POST_BACK_BY_HTML_PROCESS.AppCommit:
                    CommitWorkFlowDetails(postBackData.data[0], this.SubAdminid, updPanel, this.CompanyId);
                    break;
                case POST_BACK_BY_HTML_PROCESS.TesterAppCommit:
                    processSaveApp(postBackData.data[0]);
                    testerAppCommitSave(postBackData.data[0]);
                    Utilities.showMessage("App committed successfully for tester.", updPanel, DIALOG_TYPE.Info);
                    //CommitTesterWorkFlowDetails(objPostbackData.data[0], this.SubAdminid, updPanel, this.CompanyId);
                    break;
                case POST_BACK_BY_HTML_PROCESS.AppCommitSave:
                    if (txtWfVersionHistroy.Text.Trim().Length == 0)
                    {
                        throw new MficientException("Please enter version history.");
                    }
                    processSaveApp(postBackData.data[0]);
                    appCommitSave(postBackData.data[0]);
                    bindAppsList(APP_LIST_SOURCE.Current);
                    updateHiddenFieldsUpdPanel();
                    Utilities.runPostBackScript(@"bindAppsListTable();",
                    updPanel, "bindAppListAgain");
                    break;
                case POST_BACK_BY_HTML_PROCESS.TesterAppCommitSave:
                    setAllDbCmdHidField(getJsonOfAllDbCommands());
                    setAllWsCmdHidField(getJsonOfAllWsCommands());
                    setAllOdataHidField(getJsonOfAllOdataCommands());
                    setOfflineTablesHidField(getJsonOfAllOfflineTables());
                    setOfflineDataObjectsHidField(getJsonOfAllOfflineDataObjects());
                    testerAppCommitSave(postBackData.data[0]);
                    break;
                case POST_BACK_BY_HTML_PROCESS.AddNewApp:
                    if (String.IsNullOrEmpty(postBackData.data[0])) throw new MficientException("Please enter app name");
                    eAppRunsOn = (APP_RUNS_ON)Enum.Parse(typeof(APP_RUNS_ON), postBackData.data[1]);
                    eAppDesignMode = (APP_DESIGN_MODE)Enum.Parse(typeof(APP_DESIGN_MODE), postBackData.data[2]);
                    eStandardAppType = (MF_STANDARD_APP_TYPE)Enum.Parse(typeof(MF_STANDARD_APP_TYPE), postBackData.data[3]);
                    SaveCompleteApp objSaveNewApp = new SaveCompleteApp();
                    //not getting correct json from returned value.
                    string strAppId =
                        objSaveNewApp.ProcessSaveCompleteApp(postBackData.data[0],
                            this.SubAdminid,
                            this.CompanyId,
                            eAppRunsOn,
                            eAppDesignMode,
                            eStandardAppType);
                    //for correct json calling this process.//temporay work change it after testing is done.
                    objAppCmpltData = new GetAppCompleteData(this.CompanyId, this.SubAdminid);
                    //string strAppDetails = Utilities.SerializeJson<List<IdeAppsForJson>>(objAppCmpltData.GetAppDetail(strAppId));
                    string strAppDetails = objAppCmpltData.GetAppJson(strAppId);//Utilities.SerializeJson<List<IdeAppsForJson>>(objAppCmpltData.GetAppDetail(strAppId));
                    if (objSaveNewApp.StatusCode == 0)
                    {
                        bindAppsList(APP_LIST_SOURCE.Current);
                        setCurrentWorkingAppHidField(strAppDetails);
                        updateHiddenFieldsUpdPanel();
                        Utilities.runPostBackScript(@"
                                            mFicientIde.MfApp.AppDesigner.createNewApp.closeGetAppNamePopup();
                                            bindAppsListTable();
                                            mFicientIde.MfApp.AppDesignController.initialiseSelectedApp();
                                            mFicientIde.MfApp.GlobalData.setAppListingTypeSafe(APP_LIST_SOURCE.current);
                                            mFicientIde.MfApp.AppDesigner.mainMenuHelper.appDesignerMenuSwitcher.doByAppListingType(APP_LIST_SOURCE.current);",
                           updPanel, "InitialiseSelectedApp");
                    }
                    else if (objSaveNewApp.StatusCode == 1001)
                    {
                        Utilities.showMessage("An app with same name already exists", updPanel, DIALOG_TYPE.Error);
                    }
                    break;
                case POST_BACK_BY_HTML_PROCESS.AppSaveAs:
                    DataTable dtblWfDtl = GetWfDetails.getWorkflowDtlByName(
                            postBackData.data[0],
                            this.SubAdminid,
                            this.CompanyId);
                    if (dtblWfDtl == null) throw new MficientException("Internal server error.");
                    if (dtblWfDtl.Rows.Count > 0)
                    {
                        throw new MficientException("App name already exists.Please enter another name for app.");
                    }
                    objSaveCompleteApp = new SaveCompleteApp();
                    aryApps = new string[1];
                    if (String.IsNullOrEmpty(hidPhoneAppForSaving.Value) &&
                           !String.IsNullOrEmpty(hidTabletAppForSaving.Value))
                    {
                        aryApps = new string[1];
                        aryApps[0] = hidTabletAppForSaving.Value;
                    }
                    else if (!String.IsNullOrEmpty(hidPhoneAppForSaving.Value) &&
                            String.IsNullOrEmpty(hidTabletAppForSaving.Value))
                    {
                        aryApps = new string[1];
                        aryApps[0] = hidPhoneAppForSaving.Value;
                    }
                    else if (!String.IsNullOrEmpty(hidPhoneAppForSaving.Value) &&
                            !String.IsNullOrEmpty(hidTabletAppForSaving.Value))
                    {
                        aryApps = new string[2];
                        aryApps[0] = hidPhoneAppForSaving.Value;
                        aryApps[1] = hidTabletAppForSaving.Value;
                    }
                    else if (String.IsNullOrEmpty(hidPhoneAppForSaving.Value) &&
                            String.IsNullOrEmpty(hidTabletAppForSaving.Value))
                    {
                        aryApps = new String[0];
                    }
                    else
                    {
                        aryApps = new String[0];
                    }
                    //This check is not required.//This is not an ideal situation.
                    if (aryApps.Length > 0)
                    {
                        strNewWfId = objSaveCompleteApp.ProcessSaveAsCompleteApp(this.SubAdminid, this.CompanyId,
                            postBackData.data[0], aryApps);
                        if (objSaveCompleteApp.StatusCode == 0)
                        {
                            objAppCmpltData = new GetAppCompleteData(this.CompanyId, this.SubAdminid);
                            //strAppDetails = Utilities.SerializeJson<List<IdeAppsForJson>>(objAppCmpltData.GetAppDetail(strNewWfId));
                            strAppDetails = objAppCmpltData.GetAppJson(strNewWfId);// Utilities.SerializeJson<List<IdeAppsForJson>>(objAppCmpltData.GetAppDetail(strNewWfId));
                            bindAppsList(APP_LIST_SOURCE.Current);
                            setCurrentWorkingAppHidField(strAppDetails);
                            updateHiddenFieldsUpdPanel();
                            Utilities.runPostBackScript(@"
                                             bindAppsListTable();
                                             mFicientIde.MfApp.AppDesignController.initialiseSelectedApp();                                           
                                             mFicientIde.MfApp.AppDesigner.mainMenuHelper.closeSaveAsAppPopup();
                                             mFicientIde.MfApp.GlobalData.setAppListingTypeSafe(APP_LIST_SOURCE.current);
                                                mFicientIde.MfApp.AppDesigner.mainMenuHelper.appDesignerMenuSwitcher.doByAppListingType(APP_LIST_SOURCE.current);",
                               updPanel, "InitialiseSelectedApp");
                        }
                        else
                        {
                            throw new MficientException("Internal server error.");
                        }
                    }
                    else
                    {
                        throw new MficientException("No Apps found for saving");
                    }
                    //aryApps[0] = hidPhoneAppForSaving.Value;
                    //aryApps[1] = hidTabletAppForSaving.Value;


                    break;
                case POST_BACK_BY_HTML_PROCESS.FormCreateCopy:
                    string strFormJson = hidFormForSaving.Value;
                    if (String.IsNullOrEmpty(strFormJson))
                    {
                        throw new MficientException("Form not found.Please try again");
                    }
                    objAppCmpltData = new GetAppCompleteData(this.CompanyId, this.SubAdminid);
                    //List<IdeAppsForJson> lstAppsForJson = objAppCmpltData.GetAppDetail(objPostbackData.data[0]);
                    JArray lstApps = objAppCmpltData.GetAppDetailAsJArray(postBackData.data[0]);
                    JObject objFormToSave = JObject.Parse(strFormJson);

                    //JArray lstApps = new JArray();
                    //AppForm objForm = Utilities.DeserialiseJson<AppForm>(strFormJson);
                    //JObject objAppForJson = new JObject();

                    JObject objApp = new JObject();
                    if (objFormToSave != null)
                    {
                        if (lstApps != null)
                        {
                            bool blnDoesFrmWithSameNameExist = true;
                            foreach (JObject joApp in lstApps)
                            {
                                if (Convert.ToString(joApp["modelType"]) == Convert.ToString(objFormToSave["formModelType"]))
                                {
                                    //objAppForJson = appForJson;
                                    objApp = JObject.Parse(Convert.ToString(joApp["appJsJson"]));

                                    if (objApp["forms"] != null)
                                    {
                                        JArray jaAppForm = JArray.Parse(Convert.ToString(objApp["forms"]));
                                        bool blnExists = false;
                                        foreach (JObject joForm in jaAppForm)
                                        {
                                            if (Convert.ToString(joForm["name"]) == Convert.ToString(objFormToSave["name"]))
                                            {
                                                blnExists = true;
                                                break;
                                            }
                                        }
                                        if (blnExists)
                                        {
                                            throw new MficientException("A form with same name already exists in the application.Please provide a different name.");
                                        }
                                        else
                                        {
                                            strFormId = Utilities.GetMd5Hash(
                                                    Convert.ToString(objFormToSave["name"])
                                                    + Convert.ToString(objApp["name"])
                                                    + DateTime.UtcNow.Ticks.ToString()
                                                    );
                                            objFormToSave["id"] = strFormId;
                                            objFormToSave["appId"] = objApp["id"];
                                            //objApp.addForm(objForm);
                                            jaAppForm.Add(objFormToSave);
                                        }

                                        objApp["forms"] = jaAppForm;
                                        joApp["appJsJson"] = objApp;
                                    }
                                    blnDoesFrmWithSameNameExist = false;
                                    break;
                                }
                            }
                            if (blnDoesFrmWithSameNameExist)
                            {
                                throw new MficientException("The application does not contain an interface similar to that of the form.");
                            }
                            //foreach (JObject appForJson in lstAppsForJson)
                            //{
                            //    lstApps.Add(appForJson["appJsJson"]);
                            //}

                            string[] aryAppJson = new string[lstApps.Count];
                            int iLoopCount = 0;
                            foreach (JObject joApp in lstApps)
                            {
                                aryAppJson[iLoopCount] = Convert.ToString(joApp["appJsJson"]);
                                iLoopCount++;
                            }

                            objSaveCompleteApp = new SaveCompleteApp();
                            objSaveCompleteApp.ProcessUpdateCompleteApp(
                                    this.SubAdminid,
                                    this.CompanyId,
                                    postBackData.data[0],
                                    aryAppJson);
                            if (objSaveCompleteApp.StatusCode != 0)
                            {
                                throw new MficientException("Internal server error");
                            }
                            else
                            {
                                Utilities.showMessage("Form saved successfully.", updPanel, DIALOG_TYPE.Info);
                                Utilities.runPostBackScript(
                                    @"mFicientIde.MfApp.AppDesigner.contextMenuProcess.processCancelCopyFormToApp();",
                                    updPanel,
                                    "Close Modal popup Opened");
                            }
                        }
                        else
                        {
                            throw new MficientException("App not found.Please try again later.");
                        }
                    }
                    else
                    {
                        throw new MficientException("Invalid form.Please try again.");
                    }
                    break;
                case POST_BACK_BY_HTML_PROCESS.TransferApp:
                    objSaveCompleteApp = new SaveCompleteApp();
                    objSaveCompleteApp.TransferAppToAnotherSubadmin(postBackData.data[0],
                        this.CompanyId, postBackData.data[1]);
                    if (objSaveCompleteApp.StatusCode != 0)
                    {
                        throw new MficientException("Internal server error.");
                    }
                    else
                    {
                        bindAppsList(APP_LIST_SOURCE.Current);
                        updateHiddenFieldsUpdPanel();
                        Utilities.showMessage("App transferred successfully", updPanel, DIALOG_TYPE.Info);
                        Utilities.runPostBackScript(
                                    @"mFicientIde.MfApp.AppDesignController.destroyAppContainerOnAppClose();
                                      mFicientIde.MfApp.AppDesigner.mainMenuHelper.cancelTransferAppPopup();
                                      bindAppsListTable();",
                                    updPanel,
                                    "Close Modal popup Opened");
                    }
                    break;
                case POST_BACK_BY_HTML_PROCESS.DeleteApp:
                    DeleteWfDetails objDeleteApp = new DeleteWfDetails(postBackData.data[0], this.SubAdminid, this.CompanyId);
                    objDeleteApp.Process();
                    if (objDeleteApp.StatusCode != 0)
                    {
                        throw new MficientException("Internal server error.");
                    }
                    else
                    {
                        bindAppsList(APP_LIST_SOURCE.Current);
                        updateHiddenFieldsUpdPanel();
                        Utilities.SendNotificationMenuUpdate(this.CompanyId, "2");
                        Utilities.showMessage("App deleted successfully", updPanel, DIALOG_TYPE.Info);
                        Utilities.runPostBackScript(
                                    @"mFicientIde.MfApp.AppDesignController.destroyAppContainerOnAppClose();
                                                bindAppsListTable();",
                                    updPanel,
                                    "App Deleted process end.");
                    }
                    break;
                case POST_BACK_BY_HTML_PROCESS.NewHtml5FileAdd:
                    //data fileType name directory isDragDropped overwrite
                    //HTML5_FILE_TYPES eFileType = (HTML5_FILE_TYPES)Enum.Parse(typeof(HTML5_FILE_TYPES),objPostbackData.data[0]);
                    processNewHtml5AppFileAddition(postBackData.data);
                    Utilities.showMessage("File added successfully.", updPanel, DIALOG_TYPE.Info);
                    Utilities.runPostBackScript(
                            @"mfHtml5App.fileExplorer.processFormTreeView();
                                                mfHtml5App.html5ObjectPropSheet.setOptionsOfStartupPageDdl();
                                                    mfHtml5App.html5ObjectPropSheet.setValueOfStartupPageDdl();",
                           updPanel,
                           "New Html5 FIleAdded process End.");
                    break;
                case POST_BACK_BY_HTML_PROCESS.Html5ZipFileAdd:
                    //data fileType name directory isDragDropped
                    //HTML5_FILE_TYPES eFileType = (HTML5_FILE_TYPES)Enum.Parse(typeof(HTML5_FILE_TYPES),objPostbackData.data[0]);
                    processHtml5ZipFileAddition(postBackData.data);
                    hidDragDropFileBytes.Value = "";
                    hidDragDropFileName.Value = "";
                    updHiddenFields.Update();
                    Utilities.showMessage("File added successfully.", updPanel, DIALOG_TYPE.Info);
                    Utilities.runPostBackScript(
                            @"mfHtml5App.fileExplorer.processFormTreeView();
                                      mfHtml5App.html5ObjectPropSheet.setOptionsOfStartupPageDdl();
                                      mfHtml5App.html5ObjectPropSheet.setValueOfStartupPageDdl();",
                           updPanel,
                           "New Html5 FIleAdded process End.");
                    break;
                case POST_BACK_BY_HTML_PROCESS.ViewHtml5AppFile:
                    //stored data in ui is what is being pased
                    processHtml5AppFileEdit(postBackData.data);
                    //Utilities.showMessage("File added successfully.", updPanel, DIALOG_TYPE.Info);
                    Utilities.runPostBackScript(
                        @"mfHtml5App.fileViewer.processViewFileOnEdit();
                                          mfHtml5App.html5ObjectPropSheet.formFilePropSheet();",
                        updPanel,
                        "Show Html5 FIle Content");
                    break;
                case POST_BACK_BY_HTML_PROCESS.Html5AppFileEditSave:
                    processHtml5AppFileEditSave(postBackData.data);
                    Utilities.showMessage("File updated successfully.", updPanel, DIALOG_TYPE.Info);
                    Utilities.runPostBackScript(
                        @"mfHtml5App.fileViewer.processViewFileOnEdit();
                                          mfHtml5App.html5ObjectPropSheet.formFilePropSheet();",
                        updPanel,
                        "Show Html5 File Content On Edit Save");
                    break;
                case POST_BACK_BY_HTML_PROCESS.AddNewHtml5App:
                    if (String.IsNullOrEmpty(postBackData.data[0])) throw new MficientException("Please enter app name");
                    GetHtml5App objGetHtml5App = new GetHtml5App(this.SubAdminid, this.CompanyId);
                    //List<string >test = new List<string>();
                    //test.Add("057179EA360D341FDE3449AA161DF2A5");
                    bool doesAppExists = objGetHtml5App.DoesHtml5AppWithAppNameExists(postBackData.data[0]);
                    if (doesAppExists)
                    {
                        Utilities.showMessage("An app with same name already exists", updPanel, DIALOG_TYPE.Error);
                        return;
                    }
                    SaveHtml5App objSaveNewHtml5App = new SaveHtml5App(this.SubAdminid, this.CompanyId);
                    //not getting correct json from returned value.
                    MfHtml5App objHtml5App = objSaveNewHtml5App.SaveNewApp(postBackData.data[0]);
                    if (objSaveNewHtml5App.StatusCode != 0)
                    {
                        throw new MficientException("Internal server error.");
                    }
                    strAppDetails = Utilities.SerializeJson<MfHtml5App>(objHtml5App);
                    Html5AppTempFileKeyForUI objAppTempFileKeyForUI = new Html5AppTempFileKeyForUI();
                    objAppTempFileKeyForUI.S = this.Sessionid;
                    setHtml5AppTempFilesKeyHidField(
                        Utilities.EncryptString(
                         Utilities.SerializeJson<Html5AppTempFileKeyForUI>(objAppTempFileKeyForUI))
                         );
                    bindAppsList(APP_LIST_SOURCE.Current);
                    setHtml5AppDetailHidField(strAppDetails);
                    updateHiddenFieldsUpdPanel();
                    Utilities.runPostBackScript(@"
                                            mfHtml5App.closeGetApplicationName();
                                            bindAppsListTable();
                                            mfHtml5App.initialiseSelectedApp();",
                       updPanel, "InitialiseSelectedApp");
                    Utilities.showMessage("App was saved successfully", updPanel, DIALOG_TYPE.Info);
                    break;
                case POST_BACK_BY_HTML_PROCESS.UpdateHtml5App:
                    strAppDetails = hidHtml5AppDetail.Value;
                    if (String.IsNullOrEmpty(strAppDetails))
                    {
                        throw new MficientException("Internal server error.");
                    }
                    MfHtml5App objHtml5AppFromUI = Utilities.DeserialiseJson<MfHtml5App>(strAppDetails);
                    objGetHtml5App = new GetHtml5App(this.SubAdminid, this.CompanyId);
                    List<string> lstIgnoreIds = new List<string>();
                    lstIgnoreIds.Add(objHtml5AppFromUI.id);
                    doesAppExists = objGetHtml5App.DoesHtml5AppWithAppNameExists(objHtml5AppFromUI.name, lstIgnoreIds);
                    if (doesAppExists)
                    {
                        throw new MficientException("An app with same name already exists");
                    }
                    objGetHtml5App.GetHtml5AppById(objHtml5AppFromUI.id);
                    if (objGetHtml5App.StatusCode != 0)
                    {
                        throw new MficientException(objGetHtml5App.StatusDescription);
                    }
                    //Update previous record
                    MFEHtml5AppDetail objMfeHtml5App = objGetHtml5App.Html5App;
                    if (objMfeHtml5App == null)
                    {
                        throw new MficientException("Could not find Html5 App.Please try again later.");
                    }
                    objMfeHtml5App.AppJson = Utilities.SerializeJson<MfHtml5App>(objHtml5AppFromUI);
                    objMfeHtml5App.AppName = objHtml5AppFromUI.name;
                    objMfeHtml5App.Description = objHtml5AppFromUI.description;
                    objMfeHtml5App.Icon = objHtml5AppFromUI.icon;
                    processUpdateHtml5App(objMfeHtml5App);
                    bindAppsList(APP_LIST_SOURCE.Current);
                    Utilities.showMessage("App updated successfully.", updPanel, DIALOG_TYPE.Info);
                    break;
                case POST_BACK_BY_HTML_PROCESS.Html5AppCommitSave:
                    strAppDetails = hidHtml5AppDetail.Value;
                    if (String.IsNullOrEmpty(strAppDetails))
                    {
                        throw new MficientException("Internal server error.");
                    }
                    objHtml5AppFromUI = Utilities.DeserialiseJson<MfHtml5App>(strAppDetails);
                    objGetHtml5App = new GetHtml5App(this.SubAdminid, this.CompanyId);
                    lstIgnoreIds = new List<string>();
                    lstIgnoreIds.Add(objHtml5AppFromUI.id);
                    doesAppExists = objGetHtml5App.DoesHtml5AppWithAppNameExists(objHtml5AppFromUI.name, lstIgnoreIds);
                    if (doesAppExists)
                    {
                        throw new MficientException("An app with same name already exists");
                    }
                    objGetHtml5App.GetHtml5AppById(objHtml5AppFromUI.id);
                    if (objGetHtml5App.StatusCode != 0)
                    {
                        throw new MficientException(objGetHtml5App.StatusDescription);
                    }
                    //Update record
                    objMfeHtml5App = objGetHtml5App.Html5App;
                    if (objMfeHtml5App == null)
                    {
                        throw new MficientException("Could not find Html5 App.Please try again later.");
                    }
                    objMfeHtml5App.AppJson = Utilities.SerializeJson<MfHtml5App>(objHtml5AppFromUI);
                    objMfeHtml5App.AppName = objHtml5AppFromUI.name;
                    objMfeHtml5App.Description = objHtml5AppFromUI.description;
                    objMfeHtml5App.Icon = objHtml5AppFromUI.icon;
                    processUpdateHtml5App(objMfeHtml5App);
                    html5AppCommitSave(objMfeHtml5App);
                    //processUpdateHtml5App(objHtml5AppFromUI, objMfeHtml5App);
                    bindAppsList(APP_LIST_SOURCE.Current);
                    break;
                case POST_BACK_BY_HTML_PROCESS.Html5AppTesterCommit:
                    strAppDetails = hidHtml5AppDetail.Value;
                    if (String.IsNullOrEmpty(strAppDetails))
                    {
                        throw new MficientException("Internal server error.");
                    }
                    objHtml5AppFromUI = Utilities.DeserialiseJson<MfHtml5App>(strAppDetails);
                    objGetHtml5App = new GetHtml5App(this.SubAdminid, this.CompanyId);
                    lstIgnoreIds = new List<string>();
                    lstIgnoreIds.Add(objHtml5AppFromUI.id);
                    doesAppExists = objGetHtml5App.DoesHtml5AppWithAppNameExists(objHtml5AppFromUI.name, lstIgnoreIds);
                    if (doesAppExists)
                    {
                        throw new MficientException("An app with same name already exists");
                    }
                    objGetHtml5App.GetHtml5AppById(objHtml5AppFromUI.id);
                    if (objGetHtml5App.StatusCode != 0)
                    {
                        throw new MficientException(objGetHtml5App.StatusDescription);
                    }
                    //Update record
                    objMfeHtml5App = objGetHtml5App.Html5App;
                    if (objMfeHtml5App == null)
                    {
                        throw new MficientException("Could not find Html5 App.Please try again later.");
                    }
                    objMfeHtml5App.AppJson = Utilities.SerializeJson<MfHtml5App>(objHtml5AppFromUI);
                    objMfeHtml5App.AppName = objHtml5AppFromUI.name;
                    objMfeHtml5App.Description = objHtml5AppFromUI.description;
                    objMfeHtml5App.Icon = objHtml5AppFromUI.icon;
                    processUpdateHtml5App(objMfeHtml5App);
                    testerHtml5AppCommitSave(objMfeHtml5App);
                    bindAppsList(APP_LIST_SOURCE.Current);
                    break;
                case POST_BACK_BY_HTML_PROCESS.Html5AppSaveAs:
                    if (String.IsNullOrEmpty(postBackData.data[0])) throw new MficientException("Please enter app name");
                    //string strA
                    objGetHtml5App = new GetHtml5App(this.SubAdminid, this.CompanyId);
                    doesAppExists = objGetHtml5App.DoesHtml5AppWithAppNameExists(postBackData.data[0]);
                    if (doesAppExists)
                    {
                        Utilities.showMessage("An app with same name already exists.", updPanel, DIALOG_TYPE.Error);
                        return;
                    }
                    MfHtml5App objAppToCopy = null;
                    strAppDetails = hidHtml5AppDetail.Value;
                    if (String.IsNullOrEmpty(strAppDetails))
                    {
                        throw new MficientException("Internal server error.");
                    }
                    objAppToCopy = Utilities.DeserialiseJson<MfHtml5App>(strAppDetails);
                    // MFEHtml5AppDetail objMfeHtml5AppToCopy = objGetHtml5App.GetHtml5AppById(objAppToCopy.id);
                    objSaveNewHtml5App = new SaveHtml5App(this.SubAdminid, this.CompanyId);

                    MFEHtml5AppDetail objMfeNewHtml5AppDtl = objSaveNewHtml5App.SaveAsApp(postBackData.data[0], objAppToCopy);
                    //not getting correct json from returned value.
                    if (objSaveNewHtml5App.StatusCode != 0)
                    {
                        throw new MficientException("Internal server error.");
                    }
                    //TODO REVERT CHANGES IN DATABASE IF THIS FAILS.

                    try
                    {
                        List<Html5AppEntryMetaData> lstAppEntries = getZipEntriesInfoFromTempTable(objAppToCopy.id);
                        ZipFile objZip = createHtml5AppZipFileToUpload(objMfeNewHtml5AppDtl, lstAppEntries);
                        uploadAppZipFileToS3Bucket(objZip, objMfeNewHtml5AppDtl.BucketFilePath);
                    }
                    catch (Exception)
                    {
                        throw new MficientException("Internal server error.");
                    }
                    strAppDetails = objMfeNewHtml5AppDtl.AppJson;
                    bindAppsList(APP_LIST_SOURCE.Current);
                    setHtml5AppDetailHidField(strAppDetails);
                    updateHiddenFieldsUpdPanel();
                    Utilities.runPostBackScript(@"
                                            mfHtml5App.closeGetApplicationName();
                                            bindAppsListTable();
                                            mfHtml5App.initialiseSelectedApp();",
                       updPanel, "InitialiseSelectedApp");
                    Utilities.showMessage("App was saved successfully", updPanel, DIALOG_TYPE.Info);
                    break;
                case POST_BACK_BY_HTML_PROCESS.Html5AppCommit:
                    ProcessCommitHtml5AppDetails(postBackData.data[0], this.SubAdminid, updPanel, this.CompanyId);
                    break;
                case POST_BACK_BY_HTML_PROCESS.Html5AppDelete:
                    processHtml5AppDelete(postBackData.data);
                    bindAppsList(APP_LIST_SOURCE.Current);
                    Utilities.showMessage("App deleted successfully.", updPanel, DIALOG_TYPE.Info);
                    Utilities.runPostBackScript(
                        @"mfHtml5App.destroyAppContainerOnAppClose();",
                        updPanel,
                        "Html5 Application Close");
                    break;
                case POST_BACK_BY_HTML_PROCESS.Html5AppClose:
                    strAppId = postBackData.data[0];
                    bool saveDataOnClose = Convert.ToBoolean(postBackData.data[1]);
                    processHtml5AppClose(strAppId, saveDataOnClose);
                    bindAppsList(APP_LIST_SOURCE.Current);
                    Utilities.showMessage("App updated successfully.", updPanel, DIALOG_TYPE.Info);
                    Utilities.runPostBackScript(
                        @"mfHtml5AppDesignController.destroyAppContainerOnAppClose();",
                        updPanel,
                        "Html5 Application Close");
                    break;
                case POST_BACK_BY_HTML_PROCESS.Html5AppFileMove:
                    processHtml5AppFileMove(postBackData.data);
                    Utilities.showMessage("File(s) moved successfully.", updPanel, DIALOG_TYPE.Info);
                    Utilities.runPostBackScript(
                            @"mfHtml5App.fileExplorer.processFormTreeView();
                                                mfHtml5App.html5ObjectPropSheet.setOptionsOfStartupPageDdl();
                                                mfHtml5App.html5ObjectPropSheet.setValueOfStartupPageDdl();",
                           updPanel,
                           "New Html5 FIleAdded process End.");
                    break;
                case POST_BACK_BY_HTML_PROCESS.Html5AppZipDownload:
                    processHtml5AppZipDownload(postBackData.data);
                    //Utilities.showMessage("File(s) moved successfully.", updPanel, DIALOG_TYPE.Info);
                    Utilities.runPostBackScript(
                            @"testDownladingZipFile();",
                           updPanel,
                           "Zip File Downloading.");
                    break;
                case POST_BACK_BY_HTML_PROCESS.Html5AppAddNewFolder:
                    processHtml5AppNewFolderAddition(postBackData.data);
                    Utilities.showMessage("File added successfully.", updPanel, DIALOG_TYPE.Info);
                    Utilities.runPostBackScript(
                            @"mfHtml5App.fileExplorer.processFormTreeView();
                                            mfHtml5App.html5ObjectPropSheet.setOptionsOfStartupPageDdl();
                                            mfHtml5App.html5ObjectPropSheet.setValueOfStartupPageDdl();",
                           updPanel,
                           "New Html5 FIleAdded process End.");
                    break;
                case POST_BACK_BY_HTML_PROCESS.Html5AppTreeViewRefresh:
                    processHtml5AppTreeViewRefresh(postBackData.data);
                    Utilities.runPostBackScript(
                            @"mfHtml5App.fileExplorer.processFormTreeView();
                                              mfHtml5App.html5ObjectPropSheet.setOptionsOfStartupPageDdl();
                                              mfHtml5App.html5ObjectPropSheet.setValueOfStartupPageDdl();",
                           updPanel,
                           "Tree view Refresh.");
                    break;
                case POST_BACK_BY_HTML_PROCESS.Html5AppFileDelete:
                    processHtml5AppFileDelete(postBackData.data);
                    Utilities.runPostBackScript(
                            @"mfHtml5App.fileExplorer.processFormTreeView();
                                              mfHtml5App.html5ObjectPropSheet.setOptionsOfStartupPageDdl();
                                              mfHtml5App.html5ObjectPropSheet.setValueOfStartupPageDdl();",
                           updPanel,
                           "Tree view Refresh.");
                    break;

                default:
                    break;
            }
            #endregion testDownladingZipFile
        }

        protected void btnPostbackByHtmlCntrl_Click(object sender, EventArgs e)
        {
            try
            {
                bool isSessionValid = processCheckSession();
                if (isSessionValid)
                {
                    if (!String.IsNullOrEmpty(hidDtlsForPostBackByHtmlCntrl.Value))
                    {
                        PostbackByHtmlControlData objPostbackData =
                                Utilities.DeserialiseJson<PostbackByHtmlControlData>(hidDtlsForPostBackByHtmlCntrl.Value);
                        if (objPostbackData != null)
                        {
                            POST_BACK_BY_HTML_PROCESS eProcFor = (POST_BACK_BY_HTML_PROCESS)Enum.Parse(typeof(POST_BACK_BY_HTML_PROCESS),
                                        objPostbackData.processFor);
                            doTasksByProcessType(eProcFor, objPostbackData, updPostbackByHtmlCntrl);
                        }
                    }
                }
            }
            catch (MficientException mfEx)
            {
                Utilities.showMessage(mfEx.Message, updPostbackByHtmlCntrl, DIALOG_TYPE.Error);
            }
            catch
            {
                Utilities.showMessage("Internal server error.", updPostbackByHtmlCntrl, DIALOG_TYPE.Error);
            }
            ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "setExpiryDateOfCookie();", true);
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdfWsdlUploadId.Value.Trim().Length == 0)
                    return;
                else
                {
                    string strDpOptions = "";
                    WsdlFileContent objContent = new WsdlFileContent(false, hdfWsdlUploadId.Value, "", "dpOption", this.SubAdminid, this.CompanyId);
                    objContent.Process();
                    if (objContent.StatusCode == 0)
                        strDpOptions = Convert.ToString(objContent.ResultTable.Rows[0]["FILE_CONTENT"]);
                    else
                        return;

                    string strOption = "";
                    string json = "[";
                    DataTable dt = CsvParser.Parse(strDpOptions);
                    if (dt != null)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (strOption.Length > 0)
                                strOption += ",";
                            strOption += @"{""text"" : """ + Convert.ToString(dr[0]) + @""", ""val"" : """ + Convert.ToString(dr[1]) + @"""}";
                        }
                    }
                    else { }
                    json += strOption + "]";
                    //string str = "<script type=\"text/javascript\">appendOptionInDropDown1(){ $('input[id$=hdf2]').attr('value', '" + strOption + "');$('input[id$=hdf3]').attr('value', '" + strTextAreaText + "');$('input[id$=hdf4]').attr('value', '" + strSelectedIndexOption + "');}</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), string.Format("javascript:appendOptionInDropDown('{0}')", json), true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString(), "ChangeWsdlFileId('');", true);
                }
            }
            catch
            {
            }
        }

        protected void btnDbConnRowIndex_Click(object sender, EventArgs e)
        {
            if (hdfBindingtype.Value.Trim().Length != 0)
            {
                switch (hdfBindingtype.Value.Trim())
                {
                    case "1":
                        break;
                    case "2":
                        break;
                    case "5":
                        break;
                }
            }
        }


        //public static byte[] ReadFully(Stream input)
        //{
        //    byte[] buffer = new byte[16 * 1024];
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        int read;
        //        while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
        //        {
        //            ms.Write(buffer, 0, read);
        //        }
        //        return ms.ToArray();
        //    }
        //}

        #region HTML5 App
        void processNewHtml5AppFileAddition(List<string> dataFromClientEnd)
        {
            //data fileType name directory isDragDropped appName
            //directory path is empty no zip file already exists create ne w zip file and add the file.
            HTML5_FILE_TYPES eFileType = (HTML5_FILE_TYPES)Enum.Parse(typeof(HTML5_FILE_TYPES), dataFromClientEnd[0]);
            string strFileName = dataFromClientEnd[1];
            string strDirectoryPath = dataFromClientEnd[2];
            bool isDraggedDropped = Convert.ToBoolean(dataFromClientEnd[3]);
            bool isFileTypeSupported = false;
            //ZipFile objZippedApp = null;
            string strAppName = dataFromClientEnd[4];
            string strOverWrite = dataFromClientEnd[5];
            string strAppId = dataFromClientEnd[6];
            bool blnIsOverWrite = false;
            Html5AppEntryMetaData objAppEntryMetaData = null;
            List<Html5AppEntryMetaData> lstHtml5AppEntries = new List<Html5AppEntryMetaData>();
            List<MFEHtml5AppTempFileDetail> lstMfeHtml5AppTempFiles = new List<MFEHtml5AppTempFileDetail>();
            List<Html5AppEntryMetaData> lstAppEntryMetaData = new List<Html5AppEntryMetaData>();
            if (!String.IsNullOrEmpty(strOverWrite))
            {
                blnIsOverWrite = Convert.ToBoolean(strOverWrite);
            }
            byte[] fileContent = new byte[0];
            MemoryStream msFileContent = null;
            string strCompleteEntryPath = "";

            GetHtml5App objGetHtml5App = new GetHtml5App(this.SubAdminid, this.CompanyId);
            objGetHtml5App.GetHtml5AppById(strAppId);
            if (objGetHtml5App.StatusCode != 0)
            {
                throw new MficientException("Application Not found.");
            }
            MFEHtml5AppDetail objHtml5AppDtl = objGetHtml5App.Html5App;

            if (isDraggedDropped)
            {
                isFileTypeSupported = isFileTypeSupportedForHtml5App(hidDragDropFileName.Value.Trim().Substring(hidDragDropFileName.Value.LastIndexOf('.')));
                if (!isFileTypeSupported)
                {
                    throw new MficientException(@"This is an invalid file." + "<br/>" + @"Only(.jpg, .jpeg, .png, .txt, .xml, .htm, .html, .js, .css) files are allowed.");
                }
            }
            else
            {
            }
            if (String.IsNullOrEmpty(strFileName))
            {
                throw new MficientException(@"Please provide a file name.");
            }

            if (String.IsNullOrEmpty(strDirectoryPath))
            {
                strCompleteEntryPath = Html5RootFolder + Html5FilePathSeparator;
                objAppEntryMetaData = new Html5AppEntryMetaData();
                objAppEntryMetaData.FileContent = "";
                objAppEntryMetaData.FilePath = strCompleteEntryPath;
                objAppEntryMetaData.FileSize = 0;
                objAppEntryMetaData.Id = strCompleteEntryPath;
                objAppEntryMetaData.FileType = int.MinValue;
                objAppEntryMetaData.IsDirectory = true;
                objAppEntryMetaData.Name = Html5RootFolder;
                objAppEntryMetaData.LastEditedBy = this.SubAdminid;
                objAppEntryMetaData.LastEditedDate = DateTime.UtcNow.Ticks;
                lstHtml5AppEntries.Add(objAppEntryMetaData);
                strCompleteEntryPath = Html5RootFolder + Html5FilePathSeparator + strFileName;
            }
            else
            {
                strCompleteEntryPath = strDirectoryPath + strFileName;
            }

            if (!String.IsNullOrEmpty(hTmpAppFileK.Value))//should get this. Session is sotored in this
            {
                Html5AppTempFileKeyForUI objTempFileKey = getHtml5AppTempFileKeyFromHidField();

                if (isDraggedDropped)
                {
                    fileContent = getDraggedFileDataFromHidField();
                    msFileContent = getDraggedFileDataAsStream(fileContent);
                    if (msFileContent != null)
                    {
                        objAppEntryMetaData = new Html5AppEntryMetaData();
                        objAppEntryMetaData.FileContent = StreamToString(msFileContent);
                        objAppEntryMetaData.FilePath = strCompleteEntryPath;
                        objAppEntryMetaData.FileSize = StringSizeInAsciiEncoding(objAppEntryMetaData.FileContent);
                        objAppEntryMetaData.Id = strCompleteEntryPath;
                        objAppEntryMetaData.FileType = (int)eFileType;
                        objAppEntryMetaData.IsDirectory = false;
                        objAppEntryMetaData.Name = strFileName;
                        objAppEntryMetaData.LastEditedBy = this.SubAdminid;
                        objAppEntryMetaData.LastEditedDate = DateTime.UtcNow.Ticks;
                        if (blnIsOverWrite == false)
                        {
                            //Add new Entry
                            //If this is a completely new entry then a check applied above also adds a root foldr to htmlAppEntrie List
                            lstHtml5AppEntries.Add(objAppEntryMetaData);
                            SaveHtml5AppEntriesInTempTable objSaveAppEntryInTempTable = new SaveHtml5AppEntriesInTempTable(this.SubAdminid, this.CompanyId);
                            objSaveAppEntryInTempTable.InsertAppEntries(lstHtml5AppEntries, objHtml5AppDtl, objTempFileKey.S);
                        }
                        else
                        {
                            //Update new entry
                            GetHtml5AppTempEntry objGetHtml5AppTempEntry = new GetHtml5AppTempEntry(this.SubAdminid, this.CompanyId);
                            objGetHtml5AppTempEntry.GetHtml5AppTempFileByPath(strCompleteEntryPath, strAppId, objTempFileKey.S);
                            if (objGetHtml5AppTempEntry.StatusCode != 0)
                            {
                                throw new MficientException(objGetHtml5AppTempEntry.StatusDescription);
                            }
                            MFEHtml5AppTempFileDetail objMfeAppTempFileDetail = objGetHtml5AppTempEntry.Html5AppTempFile;
                            SaveHtml5AppEntriesInTempTable objSaveHtml5AppEntry = new SaveHtml5AppEntriesInTempTable(this.SubAdminid, this.CompanyId);
                            objSaveHtml5AppEntry.UpdateAppEntry(objAppEntryMetaData, objHtml5AppDtl, objTempFileKey.S);
                            if (objSaveHtml5AppEntry.StatusCode != 0)
                            {
                                throw new MficientException(objSaveHtml5AppEntry.StatusDescription);
                            }
                        }

                    }
                }
                else
                {
                    objAppEntryMetaData = new Html5AppEntryMetaData();
                    objAppEntryMetaData.FileContent = "";
                    objAppEntryMetaData.FilePath = strCompleteEntryPath;
                    objAppEntryMetaData.FileSize = 0;
                    objAppEntryMetaData.Id = strCompleteEntryPath;
                    objAppEntryMetaData.FileType = (int)eFileType;
                    objAppEntryMetaData.IsDirectory = false;
                    objAppEntryMetaData.Name = strFileName;
                    objAppEntryMetaData.LastEditedBy = this.SubAdminid;
                    objAppEntryMetaData.LastEditedDate = DateTime.UtcNow.Ticks;
                    if (blnIsOverWrite == false)
                    {
                        //Add new entry
                        lstHtml5AppEntries.Add(objAppEntryMetaData);
                        SaveHtml5AppEntriesInTempTable objSaveAppEntryInTempTable = new SaveHtml5AppEntriesInTempTable(this.SubAdminid, this.CompanyId);
                        objSaveAppEntryInTempTable.InsertAppEntries(lstHtml5AppEntries, objHtml5AppDtl, objTempFileKey.S);
                    }
                    else
                    {
                        //update entry
                        GetHtml5AppTempEntry objGetHtml5AppTempEntry = new GetHtml5AppTempEntry(this.SubAdminid, this.CompanyId);
                        objGetHtml5AppTempEntry.GetHtml5AppTempFileByPath(strCompleteEntryPath, strAppId, objTempFileKey.S);
                        if (objGetHtml5AppTempEntry.StatusCode != 0)
                        {
                            throw new MficientException(objGetHtml5AppTempEntry.StatusDescription);
                        }
                        MFEHtml5AppTempFileDetail objMfeAppTempFileDetail = objGetHtml5AppTempEntry.Html5AppTempFile;
                        SaveHtml5AppEntriesInTempTable objSaveHtml5AppEntry = new SaveHtml5AppEntriesInTempTable(this.SubAdminid, this.CompanyId);
                        objSaveHtml5AppEntry.UpdateAppEntry(objAppEntryMetaData, objHtml5AppDtl, objTempFileKey.S);
                        if (objSaveHtml5AppEntry.StatusCode != 0)
                        {
                            throw new MficientException(objSaveHtml5AppEntry.StatusDescription);
                        }
                    }
                }
                GetHtml5AppTempEntries objGetHtml5AppTempEntries = new GetHtml5AppTempEntries(this.SubAdminid, this.CompanyId);
                objGetHtml5AppTempEntries.GetHtml5AppTempFiles(strAppId, objTempFileKey.S);
                if (objGetHtml5AppTempEntries.StatusCode != 0)
                {
                    throw new MficientException("Error Updating the file browser.");
                }
                lstMfeHtml5AppTempFiles = objGetHtml5AppTempEntries.Html5AppTempFiles;
                foreach (MFEHtml5AppTempFileDetail fd in lstMfeHtml5AppTempFiles)
                {
                    lstAppEntryMetaData.Add(Utilities.DeserialiseJson<Html5AppEntryMetaData>(fd.FileMeta));
                }
                setHtml5AppTreeJson(getHtml5AppFileJsonForTreeView(lstAppEntryMetaData));
                setHtml5AppZipEntryHidField(Utilities.SerializeJson<List<string>>(getEntryFileNames(lstAppEntryMetaData)));
                setHtml5AppZipFoldersHidField(Utilities.SerializeJson<List<string>>(getZipFolders(lstAppEntryMetaData)));
                setHtml5AppRootHtmlFilesHidField(Utilities.SerializeJson<List<string>>(this.getHtmlFilesInRootFolder(lstAppEntryMetaData)));
                updateHiddenFieldsUpdPanel();
            }
            #region Previous Code
            //if (String.IsNullOrEmpty(strDirectoryPath))
            //{
            //    objZippedApp = createNewZipFileForApp(strAppName);
            //    this.addDirectoryToZip(objZippedApp, Html5RootFolder + Html5FilePathSeparator, "", this.SubAdminid, this.CompanyId);
            //    strCompleteEntryPath = Html5RootFolder + Html5FilePathSeparator + strFileName;
            //}
            //else
            //{
            //    objZippedApp = getAppZipFileFromS3Bucket(objHtml5AppDtl.BucketFilePath);
            //    strCompleteEntryPath = strDirectoryPath + strFileName;
            //}
            //using (objZippedApp)
            //{
            //    if (isDraggedDropped)
            //    {
            //        fileContent = getDraggedFileDataFromHidField();
            //        msFileContent = getDraggedFileDataAsStream(fileContent);
            //        if (msFileContent != null)
            //        {
            //            if (blnIsOverWrite == false)
            //            {
            //                this.addEntryToZip(objZippedApp, strCompleteEntryPath, msFileContent, this.SubAdminid, this.CompanyId);
            //            }
            //            else
            //            {
            //                this.updateEntryOfZip(objZippedApp, strCompleteEntryPath, msFileContent, this.SubAdminid, this.CompanyId);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        byte[] byteArray = Encoding.UTF8.GetBytes("");
            //        MemoryStream stream = new MemoryStream(byteArray);
            //        if (!blnIsOverWrite)
            //        {
            //            this.addEntryToZip(objZippedApp, strCompleteEntryPath, stream, this.SubAdminid, this.CompanyId);
            //        }
            //        else
            //        {
            //            this.updateEntryOfZip(objZippedApp, strCompleteEntryPath, stream, this.SubAdminid, this.CompanyId);
            //        }
            //    }
            //    //objZippedApp.Save();
            //    //uploadAppZipFileToS3Bucket(objZippedApp, objHtml5AppDtl.BucketFilePath);
            //    setHtml5AppTreeJson(getHtml5AppFileJsonForTreeView(objZippedApp));
            //    setHtml5AppZipEntryHidField(Utilities.SerializeJson<List<string>>(getEntryFileNames(objZippedApp)));
            //    setHtml5AppZipFoldersHidField(Utilities.SerializeJson<List<string>>(getZipFolders(objZippedApp)));
            //    setHtml5AppRootHtmlFilesHidField(Utilities.SerializeJson<List<string>>(this.getHtmlFilesInRootFolder(objZippedApp)));

            //}
            //updateHiddenFieldsUpdPanel();
            #endregion
        }
        void processHtml5ZipFileAddition(List<string> dataFromClientEnd)
        {
            #region testing
            //byte[] fileContent = new byte[0];
            //MemoryStream msFileContent = null;
            //fileContent = getDraggedFileDataFromHidField();
            //msFileContent = getDraggedFileDataAsStream(fileContent);
            ////List<Html5AppNodeForTreeView> lstAppTree = new List<Html5AppNodeForTreeView>();
            //char filePathSeparator = Html5FilePathSeparator;
            //using (var zip = ZipFile.Read(msFileContent))
            //{

            //    List<Html5AppNodeForTreeView> lstAppTree = new List<Html5AppNodeForTreeView>();
            //    ZipEntry entry = zip["ZipFileDemo/scripts/testing.js"];
            //    Stream str = entry.OpenReader();
            //    setFileContentInHidField(getContentOfZipEntry(entry));
            //    updHiddenFields.Update();
            //    //System.Drawing.Image image = System.Drawing.Image.FromStream();
            //    //int iWidth = 0, iHeight = 1;
            //    //if (image.Width > 800 || image.Height > 600)
            //    //{
            //    //    Image1.Width = 800;
            //    //    Image1.Height = 600;
            //    //}
            //    //else
            //    //{
            //    //    Image1.Width = image.Width;
            //    //    Image1.Height = image.Height;
            //    //}
            //    //Image1.ImageUrl = string.Format("data:image/gif;base64,{0}", Convert.ToBase64String(ReadFully(entry.OpenReader())));
            //    ////System.Drawing.Image imgToResize = System.Drawing.Image.FromStream(entry.OpenReader());
            //    ////System.Drawing.Image imgResized = imgToResize;
            //    ////int iOriginalWidth = imgToResize.Width;
            //    ////int iOriginalHeight = imgToResize.Height;
            //    ////if (iOriginalWidth > 768|| iOriginalHeight > 500)
            //    ////{
            //    ////    imgResized = ResizeImage(imgToResize, new Size(768, 500), true);
            //    ////}
            //    ////string mimeType = "";
            //    ////System.Drawing.Imaging.ImageFormat imgFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
            //    ////getImageMimeTypeAndFormatType(imgToResize, out mimeType, out imgFormat);
            //    ////Image1.ImageUrl = string.Format("data:"+mimeType+";base64,{0}", Convert.ToBase64String(imageToByteArray(imgResized, imgFormat)));
            //    //UpdatePanel1.Update();
            //    foreach (ZipEntry zipEntry in zip.Entries)
            //    {
            //        //HTML5AppFile appFile = null;
            //        //FileObject objFile = new FileObject();

            //        using (var entryms = new MemoryStream())
            //        {
            //            string strFileNameWithPath = zipEntry.FileName;
            //            string fileName = Path.GetFileName(strFileNameWithPath);
            //            string extension = Path.GetExtension(strFileNameWithPath);
            //            string strDirectoryName = Path.GetDirectoryName(strFileNameWithPath);
            //            string strNodeText = "", strNodeIcon = "";
            //            Html5AppNodeForTreeView objAppTree = null;
            //            List<Html5AppNodeForTreeView> lstTempAppTree = new List<Html5AppNodeForTreeView>();
            //            string[] tempArray;
            //            lstTempAppTree = lstAppTree;
            //            bool isDirectory = false;
            //            tempArray = strFileNameWithPath.Split(filePathSeparator);
            //            if (!zipEntry.IsDirectory)
            //            {

            //                for (int i = 0; i <= tempArray.Length - 2; i++)
            //                {
            //                    lstTempAppTree = getChildrenOfAppNode(tempArray[i], lstTempAppTree);
            //                }
            //                strNodeText = fileName;
            //                //strNodeIcon = "imagefile";
            //                //objAppTree = getAppNode(tempArray[tempArray.Length - 1], lstTempAppTree);
            //                //objAppTree = new Html5AppNodeForTreeView();
            //                //objAppTree.id = strFileNameWithPath;
            //                //tempArray = strFileNameWithPath.Split(filePathSeparator);
            //                //objAppTree.text = tempArray[tempArray.Length - 2];//check this potentially an exception throwing area.
            //                //objAppTree.state = new Html5AppNodeForTreeViewState();
            //                //objAppTree.children = new List<Html5AppNodeForTreeView>();
            //                //objAppTree.icon = String.Empty;
            //                //lstTempAppTree.Add(objAppTree);
            //                #region Previous Code
            //                //bool isValidFile = false;
            //                //bool isValidPath = checkForValidFilePath(hdFileName.Value.Trim().Substring(0, hdFileName.Value.Trim().LastIndexOf('.')), extension, Path.DirectorySeparatorChar, Path.GetDirectoryName(file), out isValidFile);
            //                //if (isValidFile && isValidPath)
            //                //{
            //                //    zipEntry.Extract(entryms);
            //                //    zipEntryBytes = entryms.ToArray();
            //                //    if (fileName.Length > 15)
            //                //        objFile.filenm = fileName.Substring(0, 15) + "...." + extension;
            //                //    else
            //                //        objFile.filenm = fileName;
            //                //    objFile.fileid = fileName;
            //                //    objFile.ext = extension;
            //                //    appFile = new HTML5AppFile(fileName, Convert.ToBase64String(zipEntryBytes), extension);
            //                //    switch (Html5AppFileContent.GetFileTypeinZippedFile(extension))
            //                //    {
            //                //        case FileType.Image:
            //                //            app.Imagefiles.Add(appFile);
            //                //            appJson.files.image.Add(objFile);
            //                //            break;
            //                //        case FileType.Script:
            //                //            app.Scriptfiles.Add(appFile);
            //                //            appJson.files.script.Add(objFile);
            //                //            break;
            //                //        case FileType.Style:
            //                //            app.Stylefiles.Add(appFile);
            //                //            appJson.files.style.Add(objFile);
            //                //            break;
            //                //        case FileType.Root:
            //                //            app.Rootfiles.Add(appFile);
            //                //            appJson.files.root.Add(objFile);
            //                //            break;
            //                //    }
            //                //}
            //                //else if (!isValidFile)
            //                //{
            //                //    strInvalidFiles += "*" + fileName + "<br/>";
            //                //}
            //                //else if (!isValidPath)
            //                //{
            //                //    strInvalidFilePath += "*" + fileName + "<br/>";
            //                //}
            //                #endregion
            //            }
            //            else
            //            {
            //                //tempArray = strFileNameWithPath.Split(filePathSeparator);
            //                strNodeText = tempArray[tempArray.Length - 2];
            //                if (tempArray.Length == 2)
            //                {
            //                    //objAppTree = new Html5AppNodeForTreeView();
            //                    //objAppTree.id = strFileNameWithPath;
            //                    //objAppTree.text = tempArray[tempArray.Length - 2];//check this potentially an exception throwing area.
            //                    //objAppTree.state = new Html5AppNodeForTreeViewState();
            //                    //objAppTree.children = new List<Html5AppNodeForTreeView>();
            //                    //objAppTree.icon = String.Empty;
            //                    //lstAppTree.Add(objAppTree);
            //                }
            //                else
            //                {
            //                    for (int i = 0; i <= tempArray.Length - 3; i++)
            //                    {
            //                        lstTempAppTree = getChildrenOfAppNode(tempArray[i], lstTempAppTree);
            //                    }

            //                    //objAppTree = new Html5AppNodeForTreeView();
            //                    //objAppTree.id = strFileNameWithPath;
            //                    //objAppTree.text = tempArray[tempArray.Length - 2];//check this potentially an exception throwing area.
            //                    //objAppTree.state = new Html5AppNodeForTreeViewState();
            //                    //objAppTree.children = new List<Html5AppNodeForTreeView>();
            //                    //objAppTree.icon = String.Empty;
            //                    //lstTempAppTree.Add(objAppTree);
            //                }
            //                isDirectory = true;

            //            }
            //            objAppTree = new Html5AppNodeForTreeView();
            //            objAppTree.id = strFileNameWithPath;
            //            //tempArray = strFileNameWithPath.Split(filePathSeparator);
            //            objAppTree.text = strNodeText;//check this potentially an exception throwing area.
            //            objAppTree.state = new Html5AppNodeForTreeViewState();
            //            objAppTree.children = new List<Html5AppNodeForTreeView>();
            //            objAppTree.icon = getAppNodeIconImageByFileExtension(extension);
            //            objAppTree.isDirectory = isDirectory;
            //            lstTempAppTree.Add(objAppTree);
            //            //lstTempAppTree.OrderBy(s => s.isDirectory);

            //        }
            //        //var lengths = from element in list
            //        //orderby element.Length
            //        //select element;

            //    }
            //    //lstAppTree.OrderBy(s => s.isDirectory);
            //    lstAppTree = sortAppNodesForTreeView(lstAppTree);
            //    hidAppTreeView.Value = Utilities.SerializeJson<List<Html5AppNodeForTreeView>>(lstAppTree);
            //    updHiddenFields.Update();
            //}
            #endregion
            #region Previous
            //data fileType name directory isDragDropped appName
            //directory path is empty no zip file already exists create ne w zip file and add the file.
            //HTML5_FILE_TYPES eFileType = (HTML5_FILE_TYPES)Enum.Parse(typeof(HTML5_FILE_TYPES), dataFromClientEnd[0]);
            //string strFileName = dataFromClientEnd[1];
            //string strDirectoryPath = dataFromClientEnd[2];
            //bool isDraggedDropped = Convert.ToBoolean(dataFromClientEnd[3]);
            //ZipFile objZippedApp = null;
            //string strAppName = dataFromClientEnd[0],
            //        strAppId = dataFromClientEnd[1];
            //byte[] fileContent = new byte[0];
            //MemoryStream msFileContent = null;
            ////string strCompleteEntryPath = "";
            //ZipFile objTempZippedFile = null;
            //GetHtml5App objHtml5App = new GetHtml5App(this.SubAdminid, this.CompanyId);
            //objHtml5App.GetHtml5AppById(strAppId);
            //if (objHtml5App.StatusCode != 0)
            //{
            //    throw new MficientException("Application Not found.");
            //}
            //MFEHtml5AppDetail objHtml5AppDtl = objHtml5App.Html5App;

            //objZippedApp = createNewZipFileForApp(strAppId);
            //this.addDirectoryToZip(objZippedApp,
            //        Html5RootFolder + Html5FilePathSeparator,
            //        "",
            //        this.SubAdminid,
            //        this.CompanyId);
            //string strFolderName = "", strFileName = "";
            //string[] aryTempArray = new string[0];
            //List<string> lstFolderNames = new List<string>();
            //using (objZippedApp)
            //{
            //    fileContent = getDraggedFileDataFromHidField();
            //    msFileContent = getDraggedFileDataAsStream(fileContent);
            //    using (msFileContent)
            //    {
            //        objTempZippedFile = ZipFile.Read(msFileContent);
            //        foreach (ZipEntry zipEntry in objTempZippedFile.Entries)
            //        {
            //            //using ()
            //            //{
            //            var entryms = new MemoryStream();
            //            string strFileNameWithPath = zipEntry.FileName;
            //            string fileName = Path.GetFileName(strFileNameWithPath);
            //            string extension = Path.GetExtension(strFileNameWithPath);
            //            string strDirectoryName = Path.GetDirectoryName(strFileNameWithPath);
            //            string tempPath = "";
            //            zipEntry.Extract(entryms);
            //            if (zipEntry.IsDirectory)
            //            {
            //                aryTempArray = zipEntry.FileName.Split(Html5FilePathSeparator);
            //                strFolderName = aryTempArray[aryTempArray.Length - 2];
            //                this.addDirectoryToZip(
            //                        objZippedApp,
            //                        strFolderName,
            //                        Html5RootFolder + Html5FilePathSeparator + zipEntry.FileName,
            //                        this.SubAdminid,
            //                        this.CompanyId);
            //                lstFolderNames.Add(strFolderName);
            //            }
            //            else
            //            {
            //                aryTempArray = strFileNameWithPath.Split(Html5FilePathSeparator);
            //if (aryTempArray.Length > 1)
            //{
            //    tempPath = Html5RootFolder + Html5FilePathSeparator;
            //    for (int i = 0; i <= aryTempArray.Length - 2; i++)
            //    {
            //        if (!objZippedApp.ContainsEntry(tempPath + aryTempArray[i] + Html5FilePathSeparator))
            //        {
            //            this.addDirectoryToZip(objZippedApp,
            //                aryTempArray[i],
            //                tempPath,
            //        this.SubAdminid,
            //        this.CompanyId);
            //        }
            //        tempPath = tempPath + aryTempArray[i] + Html5FilePathSeparator;
            //    }
            //    entryms.Position = 0;
            //    this.addEntryToZip(objZippedApp,
            //        Html5RootFolder + Html5FilePathSeparator + zipEntry.FileName,
            //        entryms,
            //        this.SubAdminid,
            //        this.CompanyId);
            //}
            //                else
            //                {
            //                    entryms.Position = 0;
            //                    this.addEntryToZip(objZippedApp,
            //                        Html5RootFolder + Html5FilePathSeparator + zipEntry.FileName,
            //                        entryms,
            //                        this.SubAdminid,
            //                        this.CompanyId);
            //                }
            //            }

            //            // }
            //        }
            //        //uploadAppZipFileToS3Bucket(objZippedApp, objHtml5AppDtl.BucketFilePath);
            //    }
            //    //objZippedApp.Save();

            //    setHtml5AppTreeJson(getHtml5AppFileJsonForTreeView(objZippedApp));
            //    setHtml5AppZipEntryHidField(Utilities.SerializeJson<List<string>>(getEntryFileNames(objZippedApp)));
            //    setHtml5AppZipFoldersHidField(Utilities.SerializeJson<List<string>>(getZipFolders(objZippedApp)));
            //    setHtml5AppRootHtmlFilesHidField(Utilities.SerializeJson<List<string>>(this.getHtmlFilesInRootFolder(objZippedApp)));
            //}
            //updateHiddenFieldsUpdPanel();
            #endregion
            string strAppName = dataFromClientEnd[0],
                   strAppId = dataFromClientEnd[1];
            //TempFile Key
            if (!String.IsNullOrEmpty(hTmpAppFileK.Value))
            {
                Html5AppTempFileKeyForUI objTempKey = getHtml5AppTempFileKeyFromHidField();
                DeleteHtml5AppTempFiles objDeleteOldFiles = new DeleteHtml5AppTempFiles(this.SubAdminid, this.CompanyId);
                objDeleteOldFiles.DeleteTempFiles(strAppId, objTempKey.S);
            }
            ZipFile objZippedApp = null;

            byte[] fileContent = getDraggedFileDataFromHidField();
            MemoryStream msFileContent = getDraggedFileDataAsStream(fileContent);
            MFEHtml5AppDetail objMfeHtml5App = null;
            using (msFileContent)
            {
                objZippedApp = ZipFile.Read(msFileContent);
                List<Html5AppEntryMetaData> lstZipEntries = getZipEntriesInfo(objZippedApp);
                if (lstZipEntries != null && lstZipEntries.Count > 0)
                {
                    GetHtml5App objGetHtml5App = new GetHtml5App(this.SubAdminid, this.CompanyId);
                    objGetHtml5App.GetHtml5AppById(strAppId);
                    if (objGetHtml5App.StatusCode != 0)
                    {
                        throw new MficientException(objGetHtml5App.StatusDescription);
                    }
                    objMfeHtml5App = objGetHtml5App.Html5App;
                    if (objMfeHtml5App == null)
                    {
                        throw new MficientException("App details not found.");
                    }
                    SaveHtml5AppEntriesInTempTable objSaveEntriesInTempTbl = new SaveHtml5AppEntriesInTempTable(this.SubAdminid, this.CompanyId);
                    objSaveEntriesInTempTbl.InsertAppEntries(lstZipEntries, objMfeHtml5App, this.Sessionid);
                    if (objSaveEntriesInTempTbl.StatusCode != 0)
                    {
                        throw new MficientException(objSaveEntriesInTempTbl.StatusDescription);
                    }
                    setHtml5AppTreeJson(getHtml5AppFileJsonForTreeView(lstZipEntries));
                    setHtml5AppZipEntryHidField(Utilities.SerializeJson<List<string>>(getEntryFileNames(lstZipEntries)));
                    setHtml5AppZipFoldersHidField(Utilities.SerializeJson<List<string>>(getZipFolders(lstZipEntries)));
                    setHtml5AppRootHtmlFilesHidField(Utilities.SerializeJson<List<string>>(this.getHtmlFilesInRootFolder(lstZipEntries)));
                    Html5AppTempFileKeyForUI objAppTempFileKeyForUI = new Html5AppTempFileKeyForUI();
                    objAppTempFileKeyForUI.S = this.Sessionid;
                    setHtml5AppTempFilesKeyHidField(
                        Utilities.EncryptString(
                         Utilities.SerializeJson<Html5AppTempFileKeyForUI>(objAppTempFileKeyForUI))
                         );
                    updateHiddenFieldsUpdPanel();

                }
                else
                {
                    throw new MficientException("Internal server error");// a root folder should be created
                }
            }



        }
        void processHtml5AppFileEdit(List<string> dataFromClientEnd)
        {
            string strFileDataFromUI = hidAppFileSelected.Value;
            string strFileContent = String.Empty;
            if (!String.IsNullOrEmpty(strFileDataFromUI))
            {
                string strAppId = dataFromClientEnd[0];
                GetHtml5App objGetHtml5App = new GetHtml5App(this.SubAdminid, this.CompanyId);
                objGetHtml5App.GetHtml5AppById(strAppId);
                if (objGetHtml5App.StatusCode != 0)
                {
                    throw new MficientException("Application not found.");
                }
                //ZipFile objZipFile = this.getAppZipFileFromS3Bucket(objHtml5AppDtl.BucketFilePath);
                MFEHtml5AppDetail objHtml5AppDtl = objGetHtml5App.Html5App;
                Html5AppNodeForTreeView objFileDataFromUI = Utilities.DeserialiseJson<Html5AppNodeForTreeView>(strFileDataFromUI);

                Html5AppFileDtlForUIPropSheet objFileProperties = null;

                if (!String.IsNullOrEmpty(hTmpAppFileK.Value))
                {
                    Html5AppTempFileKeyForUI objAppTempFileKey = this.getHtml5AppTempFileKeyFromHidField();
                    GetHtml5AppTempEntry objGetTempEntry = new GetHtml5AppTempEntry(this.SubAdminid, this.CompanyId);
                    objGetTempEntry.GetHtml5AppTempFileByPath(objFileDataFromUI.id, objHtml5AppDtl.AppId, objAppTempFileKey.S);
                    if (objGetTempEntry.StatusCode != 0)
                    {
                        throw new MficientException("Internal server error");
                    }
                    if (objGetTempEntry.Html5AppTempFile == null)
                    {
                        throw new MficientException("Could not get file.Please try again later");
                    }
                    MFEHtml5AppTempFileDetail objHtml5AppTempFileDtl = objGetTempEntry.Html5AppTempFile;
                    Html5AppEntryMetaData objAppEntryMetaData = null;
                    if (objFileDataFromUI.fileType == (int)HTML5_FILE_TYPES.Image)
                    {
                        //System.Drawing.Image imgToResize = System.Drawing.Image.FromStream(entry.OpenReader());
                        System.Drawing.Image imgToResize = this.Base64ToImage(objHtml5AppTempFileDtl.FileContent);
                        System.Drawing.Image imgResized = imgToResize;
                        int iOriginalWidth = imgToResize.Width;
                        int iOriginalHeight = imgToResize.Height;
                        if (iOriginalWidth > 800 || iOriginalHeight > 600)
                        {
                            imgResized = ResizeImage(imgToResize, new Size(800, 600), true);
                        }
                        imgHtml5ImageViewer.Width = imgResized.Width;
                        imgHtml5ImageViewer.Height = imgResized.Height;
                        string mimeType = "";
                        System.Drawing.Imaging.ImageFormat imgFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
                        getImageMimeTypeAndFormatType(imgToResize, out mimeType, out imgFormat);
                        imgHtml5ImageViewer.ImageUrl = string.Format("data:" + mimeType + ";base64,{0}", Convert.ToBase64String(imageToByteArray(imgResized, imgFormat)));
                        updHtml5ImageViewer.Update();
                        objAppEntryMetaData = Utilities.DeserialiseJson<Html5AppEntryMetaData>(objHtml5AppTempFileDtl.FileMeta);
                        objFileProperties = getZipEntryDetailsForUI(objAppEntryMetaData, objFileDataFromUI, imgToResize);
                        imgToResize.Dispose();
                    }
                    else
                    {
                        //strFileContent = getContentOfZipEntry(entry);
                        //if(!String.IsNullOrEmpty(strFileContent)){
                        //strFileContent = Utilities.SerializeJson<Html5AppFileContentTxt>(
                        strFileContent = objHtml5AppTempFileDtl.FileContent;
                        Html5AppFileContentTxt objFileContent = new Html5AppFileContentTxt();
                        objFileContent.content = strFileContent;
                        setFileContentInHidField(Utilities.SerializeJson<Html5AppFileContentTxt>(objFileContent));
                        //objFileProperties = getZipEntryDetailsForUI(entry, objFileData, null);
                        objAppEntryMetaData = Utilities.DeserialiseJson<Html5AppEntryMetaData>(objHtml5AppTempFileDtl.FileMeta);
                        objFileProperties = getZipEntryDetailsForUI(objAppEntryMetaData, objFileDataFromUI, null);
                    }
                    //setHtml5App
                    setHtml5AppHtmlFileDtlHidField(Utilities.SerializeJson<Html5AppFileDtlForUIPropSheet>(objFileProperties));
                    updHiddenFields.Update();
                    //using (objZipFile)
                    //{
                    //    ZipEntry entry = objZipFile[objFileData.id];

                    //}
                }
                else
                {
                    //this will happen only if some one tampers at ui end.
                    throw new MficientException("No file selected.");
                }
            }
        }
        void processHtml5AppFileEditSave(List<string> dataFromClientEnd)
        {
            string strFileMetaDataFromUI = hidAppFileSelected.Value;
            string strFileContent = hidSelectedFileContent.Value;
            Html5AppFileContentTxt objFileContent = null;
            string strHtml5AppEntryKey = String.Empty;
            if (!String.IsNullOrEmpty(strFileMetaDataFromUI))
            {
                string strAppId = dataFromClientEnd[0];
                GetHtml5App objGetHtml5App = new GetHtml5App(this.SubAdminid, this.CompanyId);
                objGetHtml5App.GetHtml5AppById(strAppId);
                if (objGetHtml5App.StatusCode != 0)
                {
                    throw new MficientException("Application Not found.");
                }
                MFEHtml5AppDetail objHtml5AppDtl = objGetHtml5App.Html5App;
                Html5AppNodeForTreeView objFileDataFromUI = Utilities.DeserialiseJson<Html5AppNodeForTreeView>(strFileMetaDataFromUI);
                //ZipFile objZipFile = this.getAppZipFileFromS3Bucket(objHtml5AppDtl.BucketFilePath);
                if (!String.IsNullOrEmpty(strFileContent))
                {
                    objFileContent = Utilities.DeserialiseJson<Html5AppFileContentTxt>(strFileContent);
                    strFileContent = HttpUtility.HtmlDecode(objFileContent.content);
                }
                else
                {
                    strFileContent = "";
                }
                if (!String.IsNullOrEmpty(hTmpAppFileK.Value))
                {
                    Html5AppTempFileKeyForUI objAppTempFileKey = this.getHtml5AppTempFileKeyFromHidField();
                    GetHtml5AppTempEntry objGetHtml5AppTempEntry = new GetHtml5AppTempEntry(this.SubAdminid, this.CompanyId);
                    objGetHtml5AppTempEntry.GetHtml5AppTempFileByPath(objFileDataFromUI.id, strAppId, objAppTempFileKey.S);
                    if (objGetHtml5AppTempEntry.StatusCode != 0)
                    {
                        throw new MficientException(objGetHtml5AppTempEntry.StatusDescription);
                    }
                    MFEHtml5AppTempFileDetail objMfeAppTempFileDetail = objGetHtml5AppTempEntry.Html5AppTempFile;
                    Html5AppEntryMetaData objAppEntryMetaData = Utilities.DeserialiseJson<Html5AppEntryMetaData>(objMfeAppTempFileDetail.FileMeta);
                    objAppEntryMetaData.FileContent = strFileContent;
                    objAppEntryMetaData.FileSize = StringSizeInAsciiEncoding(strFileContent);
                    objAppEntryMetaData.LastEditedBy = this.SubAdminid;
                    objAppEntryMetaData.LastEditedDate = DateTime.UtcNow.Ticks;
                    SaveHtml5AppEntriesInTempTable objSaveHtml5AppEntry = new SaveHtml5AppEntriesInTempTable(this.SubAdminid, this.CompanyId);
                    objSaveHtml5AppEntry.UpdateAppEntry(objAppEntryMetaData, objHtml5AppDtl, objAppTempFileKey.S);
                    if (objSaveHtml5AppEntry.StatusCode != 0)
                    {
                        throw new MficientException(objSaveHtml5AppEntry.StatusDescription);
                    }
                    objFileContent = new Html5AppFileContentTxt();
                    objFileContent.content = strFileContent;
                    setFileContentInHidField(Utilities.SerializeJson<Html5AppFileContentTxt>(objFileContent));
                    Html5AppFileDtlForUIPropSheet objFileProperties = null;
                    objFileProperties = getZipEntryDetailsForUI(objAppEntryMetaData, objFileDataFromUI, null);
                    setHtml5AppHtmlFileDtlHidField(Utilities.SerializeJson<Html5AppFileDtlForUIPropSheet>(objFileProperties));
                    updHiddenFields.Update();
                }
                //using (objZipFile)
                //{
                //    objZipFile.UpdateEntry(objFileData.id, strFileContent);
                //    //uploadAppZipFileToS3Bucket(objZipFile, objHtml5AppDtl.BucketFilePath);
                //}
            }
            else
            {
                //this will happen only if some one tampers at ui end.
                throw new MficientException("No file selected.");
            }
        }
        void processHtml5AppDelete(List<string> dataFromClientEnd)
        {
            string strAppId = dataFromClientEnd[0]; ;
            string strHtml5AppEntryKey = String.Empty;
            GetHtml5App objGetHtml5App = new GetHtml5App(this.SubAdminid, this.CompanyId);
            objGetHtml5App.GetHtml5AppById(strAppId);
            if (objGetHtml5App.StatusCode != 0)
            {
                throw new MficientException("Internal server error.");
            }
            MFEHtml5AppDetail objHtml5AppDtl = objGetHtml5App.Html5App;
            if (objHtml5AppDtl == null)
            {
                throw new MficientException("Application not found.");
            }
            if (!String.IsNullOrEmpty(hTmpAppFileK.Value))
            {
                Html5AppTempFileKeyForUI objAppTempFileKey = this.getHtml5AppTempFileKeyFromHidField();
                DeleteHtml5App objDeleteApp = new DeleteHtml5App(this.SubAdminid, this.CompanyId);
                objDeleteApp.DeleteApp(strAppId);
                if (objDeleteApp.StatusCode != 0)
                {
                    throw new MficientException("Internal server error.");
                }
                this.deleteAppZipFileFromS3Bucket(objHtml5AppDtl.BucketFilePath);
                DeleteHtml5AppTempFiles objDeleteTempFiles = new DeleteHtml5AppTempFiles(this.SubAdminid, this.CompanyId);
                objDeleteTempFiles.DeleteTempFiles(strAppId, objAppTempFileKey.S);
            }
        }
        void processHtml5AppClose(string appId, bool saveBeforeClosing)
        {
            if (saveBeforeClosing)
            {
                string strAppDetails = hidHtml5AppDetail.Value;
                if (String.IsNullOrEmpty(strAppDetails))
                {
                    throw new MficientException("Internal server error.");
                }
                MfHtml5App objHtml5AppFromUI = Utilities.DeserialiseJson<MfHtml5App>(strAppDetails);
                GetHtml5App objGetHtml5App = new GetHtml5App(this.SubAdminid, this.CompanyId);
                List<string> lstIgnoreIds = new List<string>();
                lstIgnoreIds.Add(objHtml5AppFromUI.id);
                bool doesAppExists = objGetHtml5App.DoesHtml5AppWithAppNameExists(objHtml5AppFromUI.name, lstIgnoreIds);
                if (doesAppExists)
                {
                    throw new MficientException("An app with same name already exists");
                }
                objGetHtml5App.GetHtml5AppById(objHtml5AppFromUI.id);
                if (objGetHtml5App.StatusCode != 0)
                {
                    throw new MficientException(objGetHtml5App.StatusDescription);
                }
                //Update previous record
                MFEHtml5AppDetail objMfeHtml5App = objGetHtml5App.Html5App;
                if (objMfeHtml5App == null)
                {
                    throw new MficientException("Could not find Html5 App.Please try again later.");
                }
                objMfeHtml5App.AppJson = Utilities.SerializeJson<MfHtml5App>(objHtml5AppFromUI);
                objMfeHtml5App.AppName = objHtml5AppFromUI.name;
                objMfeHtml5App.Description = objHtml5AppFromUI.description;
                objMfeHtml5App.Icon = objHtml5AppFromUI.icon;
                processUpdateHtml5App(objMfeHtml5App);
            }
            if (!String.IsNullOrEmpty(hTmpAppFileK.Value))
            {
                Html5AppTempFileKeyForUI objAppTempFileKey = this.getHtml5AppTempFileKeyFromHidField();
                DeleteHtml5AppTempFiles objDeleteTempFiles = new DeleteHtml5AppTempFiles(this.SubAdminid, this.CompanyId);
                objDeleteTempFiles.DeleteTempFiles(appId, objAppTempFileKey.S);
            }

        }
        void processHtml5AppFileMove(List<string> dataFromClientEnd)
        {
            string strAppId = dataFromClientEnd[0]; ;
            string strHtml5AppEntryKey = String.Empty;
            string strFileMoveData = hidHtml5AppFileMoveData.Value;
            List<Html5AppFileMoveDataForUI> lstFileMoveData = new List<Html5AppFileMoveDataForUI>();
            if (!String.IsNullOrEmpty(strFileMoveData))
            {
                lstFileMoveData = Utilities.DeserialiseJson<List<Html5AppFileMoveDataForUI>>(strFileMoveData);
            }

            if (lstFileMoveData != null && !String.IsNullOrEmpty(hTmpAppFileK.Value))
            {
                Html5AppTempFileKeyForUI objAppTempFileKey = this.getHtml5AppTempFileKeyFromHidField();
                GetHtml5AppTempEntries objGetHtml5AppTempEntries = new GetHtml5AppTempEntries(this.SubAdminid, this.CompanyId);
                objGetHtml5AppTempEntries.GetHtml5AppTempFiles(strAppId, objAppTempFileKey.S);
                if (objGetHtml5AppTempEntries.StatusCode != 0)
                {
                    throw new MficientException("Internal server error.");
                }
                List<MFEHtml5AppTempFileDetail> lstMfeHtml5AppTempFiles = objGetHtml5AppTempEntries.Html5AppTempFiles;

                foreach (Html5AppFileMoveDataForUI moveData in lstFileMoveData)
                {
                    MFEHtml5AppTempFileDetail objOldTempFileDtl = lstMfeHtml5AppTempFiles.Find(s => s.FilePath == moveData.oldPath);
                    Html5AppEntryMetaData objAppEntryMetaDt = Utilities.DeserialiseJson<Html5AppEntryMetaData>(objOldTempFileDtl.FileMeta);
                    objAppEntryMetaDt.FilePath = moveData.newPath;
                    objAppEntryMetaDt.Id = moveData.newPath;
                    moveData.metaData = Utilities.SerializeJson<Html5AppEntryMetaData>(objAppEntryMetaDt);
                }

                SaveHtml5AppEntriesInTempTable objSaveHtml5AppEntries = new SaveHtml5AppEntriesInTempTable(this.SubAdminid, this.CompanyId);
                objSaveHtml5AppEntries.UpdateAppEntryFilePath(lstFileMoveData, strAppId, objAppTempFileKey.S);
                if (objSaveHtml5AppEntries.StatusCode != 0)
                {
                    throw new MficientException("Internal server error.");
                }

                objGetHtml5AppTempEntries = new GetHtml5AppTempEntries(this.SubAdminid, this.CompanyId);
                objGetHtml5AppTempEntries.GetHtml5AppTempFiles(strAppId, objAppTempFileKey.S);
                if (objGetHtml5AppTempEntries.StatusCode != 0)
                {
                    throw new MficientException("Internal server error.");
                }
                lstMfeHtml5AppTempFiles = objGetHtml5AppTempEntries.Html5AppTempFiles;
                List<Html5AppEntryMetaData> lstAppEntryMetaData = new List<Html5AppEntryMetaData>();
                foreach (MFEHtml5AppTempFileDetail fd in lstMfeHtml5AppTempFiles)
                {
                    lstAppEntryMetaData.Add(Utilities.DeserialiseJson<Html5AppEntryMetaData>(fd.FileMeta));
                }
                setHtml5AppTreeJson(getHtml5AppFileJsonForTreeView(lstAppEntryMetaData));
                setHtml5AppZipEntryHidField(Utilities.SerializeJson<List<string>>(getEntryFileNames(lstAppEntryMetaData)));
                setHtml5AppZipFoldersHidField(Utilities.SerializeJson<List<string>>(getZipFolders(lstAppEntryMetaData)));
                setHtml5AppRootHtmlFilesHidField(Utilities.SerializeJson<List<string>>(this.getHtmlFilesInRootFolder(lstAppEntryMetaData)));
                updateHiddenFieldsUpdPanel();
            }

        }
        void processHtml5AppZipDownload(List<string> dataFromClientEnd)
        {
            string strAppId = dataFromClientEnd[0];
            string strHtml5AppEntryKey = String.Empty;
            string strAppDetails = hidHtml5AppDetail.Value;
            if (String.IsNullOrEmpty(strAppDetails))
            {
                throw new MficientException("Internal server error.");
            }
            MfHtml5App objHtml5AppFromUI = Utilities.DeserialiseJson<MfHtml5App>(strAppDetails);
            GetHtml5App objGetHtml5App = new GetHtml5App(this.SubAdminid, this.CompanyId);
            List<string> lstIgnoreIds = new List<string>();
            lstIgnoreIds.Add(objHtml5AppFromUI.id);
            bool doesAppExists = objGetHtml5App.DoesHtml5AppWithAppNameExists(objHtml5AppFromUI.name, lstIgnoreIds);
            if (doesAppExists)
            {
                throw new MficientException("An app with same name already exists");
            }
            objGetHtml5App.GetHtml5AppById(objHtml5AppFromUI.id);
            if (objGetHtml5App.StatusCode != 0)
            {
                throw new MficientException(objGetHtml5App.StatusDescription);
            }
            //Update previous record
            MFEHtml5AppDetail objMfeHtml5App = objGetHtml5App.Html5App;
            if (objMfeHtml5App == null)
            {
                throw new MficientException("Could not find the Html5 App.Please try again later.");
            }
            objMfeHtml5App.AppJson = Utilities.SerializeJson<MfHtml5App>(objHtml5AppFromUI);
            objMfeHtml5App.AppName = objHtml5AppFromUI.name;
            objMfeHtml5App.Description = objHtml5AppFromUI.description;
            objMfeHtml5App.Icon = objHtml5AppFromUI.icon;
            processUpdateHtml5App(objMfeHtml5App);
            string destinationPathOfZip = moveZipToPublic(objMfeHtml5App.AppName, objMfeHtml5App.BucketFilePath);
            //hidHtml5AppZipDownloadPath.Value = S3Bucket.GetAmazonCompletePathForFile(destinationPathOfZip);
            hidHtml5AppZipDownloadPath.Value = destinationPathOfZip;
            bindAppsList(APP_LIST_SOURCE.Current);

        }
        void processHtml5AppNewFolderAddition(List<string> dataFromClientEnd)
        {
            //data fileType name directory isDragDropped appName
            //directory path is empty no zip file already exists create ne w zip file and add the file.
            string strFolderName = dataFromClientEnd[0];
            string strDirectoryPath = dataFromClientEnd[1];
            string strAppId = dataFromClientEnd[2];
            string strCompleteEntryPath;

            Html5AppEntryMetaData objAppEntryMetaData = null;
            List<Html5AppEntryMetaData> lstHtml5AppEntries = new List<Html5AppEntryMetaData>();
            List<MFEHtml5AppTempFileDetail> lstMfeHtml5AppTempFiles = new List<MFEHtml5AppTempFileDetail>();
            List<Html5AppEntryMetaData> lstAppEntryMetaData = new List<Html5AppEntryMetaData>();

            GetHtml5App objGetHtml5App = new GetHtml5App(this.SubAdminid, this.CompanyId);
            objGetHtml5App.GetHtml5AppById(strAppId);
            if (objGetHtml5App.StatusCode != 0)
            {
                throw new MficientException("Application Not found.");
            }
            MFEHtml5AppDetail objHtml5AppDtl = objGetHtml5App.Html5App;

            if (String.IsNullOrEmpty(strFolderName))
            {
                throw new MficientException(@"Please provide a folder name.");
            }
            if (!String.IsNullOrEmpty(hTmpAppFileK.Value))//should get this. Session is sotored in this
            {
                Html5AppTempFileKeyForUI objTempFileKey = getHtml5AppTempFileKeyFromHidField();
                GetHtml5AppTempEntries objGetHtml5AppTempEntries = new GetHtml5AppTempEntries(this.SubAdminid, this.CompanyId);
                objGetHtml5AppTempEntries.GetHtml5AppTempFiles(strAppId, objTempFileKey.S);
                if (objGetHtml5AppTempEntries.StatusCode != 0)
                {
                    throw new MficientException("Internal server error.");
                }
                lstMfeHtml5AppTempFiles = objGetHtml5AppTempEntries.Html5AppTempFiles;
                if (lstMfeHtml5AppTempFiles.Count == 0)//no folder exists need to put root folder as well
                {
                    strCompleteEntryPath = Html5RootFolder + Html5FilePathSeparator;
                    objAppEntryMetaData = new Html5AppEntryMetaData();
                    objAppEntryMetaData.FileContent = "";
                    objAppEntryMetaData.FilePath = strCompleteEntryPath;
                    objAppEntryMetaData.FileSize = 0;
                    objAppEntryMetaData.Id = strCompleteEntryPath;
                    objAppEntryMetaData.FileType = int.MinValue;
                    objAppEntryMetaData.IsDirectory = true;
                    objAppEntryMetaData.Name = Html5RootFolder;
                    objAppEntryMetaData.LastEditedBy = this.SubAdminid;
                    objAppEntryMetaData.LastEditedDate = DateTime.UtcNow.Ticks;
                    lstHtml5AppEntries.Add(objAppEntryMetaData);
                    strCompleteEntryPath = Html5RootFolder + Html5FilePathSeparator + strFolderName + Html5FilePathSeparator;
                }
                else
                {
                    strCompleteEntryPath = strDirectoryPath + strFolderName + Html5FilePathSeparator;
                }
                objAppEntryMetaData = new Html5AppEntryMetaData();
                objAppEntryMetaData.FileContent = String.Empty;
                objAppEntryMetaData.FilePath = strCompleteEntryPath;
                objAppEntryMetaData.FileSize = 0;
                objAppEntryMetaData.Id = strCompleteEntryPath;
                objAppEntryMetaData.FileType = int.MinValue;
                objAppEntryMetaData.IsDirectory = true;
                objAppEntryMetaData.Name = strFolderName;
                objAppEntryMetaData.LastEditedBy = this.SubAdminid;
                objAppEntryMetaData.LastEditedDate = DateTime.UtcNow.Ticks;

                lstHtml5AppEntries.Add(objAppEntryMetaData);
                SaveHtml5AppEntriesInTempTable objSaveAppEntryInTempTable = new SaveHtml5AppEntriesInTempTable(this.SubAdminid, this.CompanyId);
                objSaveAppEntryInTempTable.InsertAppEntries(lstHtml5AppEntries, objHtml5AppDtl, objTempFileKey.S);

                //form the json for tree view in UI
                objGetHtml5AppTempEntries = new GetHtml5AppTempEntries(this.SubAdminid, this.CompanyId);
                objGetHtml5AppTempEntries.GetHtml5AppTempFiles(strAppId, objTempFileKey.S);
                if (objGetHtml5AppTempEntries.StatusCode != 0)
                {
                    throw new MficientException("Internal server error.");
                }
                lstMfeHtml5AppTempFiles = objGetHtml5AppTempEntries.Html5AppTempFiles;
                foreach (MFEHtml5AppTempFileDetail fd in lstMfeHtml5AppTempFiles)
                {
                    lstAppEntryMetaData.Add(Utilities.DeserialiseJson<Html5AppEntryMetaData>(fd.FileMeta));
                }
                setHtml5AppTreeJson(getHtml5AppFileJsonForTreeView(lstAppEntryMetaData));
                setHtml5AppZipEntryHidField(Utilities.SerializeJson<List<string>>(getEntryFileNames(lstAppEntryMetaData)));
                setHtml5AppZipFoldersHidField(Utilities.SerializeJson<List<string>>(getZipFolders(lstAppEntryMetaData)));
                setHtml5AppRootHtmlFilesHidField(Utilities.SerializeJson<List<string>>(this.getHtmlFilesInRootFolder(lstAppEntryMetaData)));
                updateHiddenFieldsUpdPanel();
            }

        }
        void processHtml5AppTreeViewRefresh(List<string> dataFromClientEnd)
        {
            string strAppId = dataFromClientEnd[0];


            //Html5AppEntryMetaData objAppEntryMetaData = null;
            List<Html5AppEntryMetaData> lstHtml5AppEntries = new List<Html5AppEntryMetaData>();
            List<MFEHtml5AppTempFileDetail> lstMfeHtml5AppTempFiles = new List<MFEHtml5AppTempFileDetail>();
            List<Html5AppEntryMetaData> lstAppEntryMetaData = new List<Html5AppEntryMetaData>();


            if (!String.IsNullOrEmpty(hTmpAppFileK.Value))//should get this. Session is sotored in this
            {
                Html5AppTempFileKeyForUI objTempFileKey = getHtml5AppTempFileKeyFromHidField();
                GetHtml5AppTempEntries objGetHtml5AppTempEntries = new GetHtml5AppTempEntries(this.SubAdminid, this.CompanyId);
                objGetHtml5AppTempEntries.GetHtml5AppTempFiles(strAppId, objTempFileKey.S);
                if (objGetHtml5AppTempEntries.StatusCode != 0)
                {
                    throw new MficientException("Internal server error.");
                }
                lstMfeHtml5AppTempFiles = objGetHtml5AppTempEntries.Html5AppTempFiles;
                foreach (MFEHtml5AppTempFileDetail fd in lstMfeHtml5AppTempFiles)
                {
                    lstAppEntryMetaData.Add(Utilities.DeserialiseJson<Html5AppEntryMetaData>(fd.FileMeta));
                }
                setHtml5AppTreeJson(getHtml5AppFileJsonForTreeView(lstAppEntryMetaData));
                setHtml5AppZipEntryHidField(Utilities.SerializeJson<List<string>>(getEntryFileNames(lstAppEntryMetaData)));
                setHtml5AppZipFoldersHidField(Utilities.SerializeJson<List<string>>(getZipFolders(lstAppEntryMetaData)));
                setHtml5AppRootHtmlFilesHidField(Utilities.SerializeJson<List<string>>(this.getHtmlFilesInRootFolder(lstAppEntryMetaData)));
                updateHiddenFieldsUpdPanel();
            }

        }
        void processHtml5AppFileDelete(List<string> dataFromClientEnd)
        {
            string strAppId = dataFromClientEnd[0];
            string strFileId = dataFromClientEnd[1];
            string strHtml5AppEntryKey = String.Empty;
            if (!String.IsNullOrEmpty(hTmpAppFileK.Value))
            {
                Html5AppTempFileKeyForUI objAppTempFileKey = this.getHtml5AppTempFileKeyFromHidField();
                DeleteHtml5AppTempFiles objDeleteAppFile = new DeleteHtml5AppTempFiles(this.SubAdminid, this.CompanyId);
                objDeleteAppFile.DeleteTempFile(strAppId, objAppTempFileKey.S, strFileId);
                if (objDeleteAppFile.StatusCode != 0)
                {
                    throw new MficientException("Internal server error.");
                }
                GetHtml5AppTempEntries objGetHtml5AppTempEntries = new GetHtml5AppTempEntries(this.SubAdminid, this.CompanyId);
                objGetHtml5AppTempEntries.GetHtml5AppTempFiles(strAppId, objAppTempFileKey.S);
                if (objGetHtml5AppTempEntries.StatusCode != 0)
                {
                    throw new MficientException("Internal server error.");
                }
                List<MFEHtml5AppTempFileDetail> lstMfeHtml5AppTempFiles = objGetHtml5AppTempEntries.Html5AppTempFiles;
                List<Html5AppEntryMetaData> lstAppEntryMetaData = new List<Html5AppEntryMetaData>();
                foreach (MFEHtml5AppTempFileDetail fd in lstMfeHtml5AppTempFiles)
                {
                    lstAppEntryMetaData.Add(Utilities.DeserialiseJson<Html5AppEntryMetaData>(fd.FileMeta));
                }
                setHtml5AppTreeJson(getHtml5AppFileJsonForTreeView(lstAppEntryMetaData));
                setHtml5AppZipEntryHidField(Utilities.SerializeJson<List<string>>(getEntryFileNames(lstAppEntryMetaData)));
                setHtml5AppZipFoldersHidField(Utilities.SerializeJson<List<string>>(getZipFolders(lstAppEntryMetaData)));
                setHtml5AppRootHtmlFilesHidField(Utilities.SerializeJson<List<string>>(this.getHtmlFilesInRootFolder(lstAppEntryMetaData)));
                updateHiddenFieldsUpdPanel();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileExtension"></param>
        /// <returns></returns>
        /// <exception cref="UnSupportedHtml5AppFile">Thrown when the file type is not supported</exception>
        private HTML5_FILE_TYPES getFileTypeByExtension(string fileExtension)
        {
            bool blnIsFileTypeSupported = isFileTypeSupportedForHtml5App(fileExtension);
            if (!blnIsFileTypeSupported)
            {
                throw new UnSupportedHtml5AppFileException();
            }
            HTML5_FILE_TYPES fileType = HTML5_FILE_TYPES.Css;
            switch (fileExtension)
            {
                case "png":
                case "jpeg":
                case "jpg":
                case "gif":
                    fileType = HTML5_FILE_TYPES.Image;
                    break;
                case "js":
                    fileType = HTML5_FILE_TYPES.Js;
                    break;
                case "css":
                    fileType = HTML5_FILE_TYPES.Css;
                    break;
                case "htm":
                case "html":
                    fileType = HTML5_FILE_TYPES.Html;
                    break;
                case "xml":
                    fileType = HTML5_FILE_TYPES.Xml;
                    break;
                case "txt":
                    fileType = HTML5_FILE_TYPES.Txt;
                    break;
                case "zip":
                    fileType = HTML5_FILE_TYPES.Zip;
                    break;
            }
            return fileType;
        }
        int getFileTypeValueByExtension(string extension)
        {
            int iFileType = 0;
            switch (extension)
            {
                case ".png":
                case ".jpeg":
                case ".jpg":
                case ".gif":
                    iFileType = (int)Html5AppFileType.Image;
                    break;
                case ".js":
                    iFileType = (int)Html5AppFileType.Js;
                    break;
                case ".css":
                    iFileType = (int)Html5AppFileType.Css;
                    break;
                case ".htm":
                case ".html":
                    iFileType = (int)Html5AppFileType.Html;
                    break;
                case ".xml":
                    iFileType = (int)Html5AppFileType.Xml;
                    break;
                case ".txt":
                    iFileType = (int)Html5AppFileType.Txt;
                    break;
            }
            return iFileType;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="extension">file extension complete example .zip</param>
        /// <returns></returns>
        bool isFileTypeSupportedForHtml5App(string extension)
        {
            bool isSupported = false;
            switch (extension)
            {
                case ".png":
                case ".jpeg":
                case ".jpg":
                case ".gif":
                case ".js":
                case ".css":
                case ".htm":
                case ".html":
                case ".xml":
                case ".txt":
                case ".zip":
                    isSupported = true;
                    break;

            }
            return isSupported;
        }
        public List<Html5AppEntryMetaData> getZipEntriesInfo(ZipFile zip)
        {

            ZipFile objZippedApp = zip;
            byte[] fileContent = new byte[0];
            string strFolderName = String.Empty,
                    strFileName = String.Empty,
                    strFileRootDirectory = Html5RootFolder + Html5FilePathSeparator,
                    strTempPath = String.Empty;
            string[] aryTempArray = new string[0];
            //List<string> lstFolderNames = new List<string>();


            List<Html5AppEntryMetaData> lstZipEntriesMetaData = new List<Html5AppEntryMetaData>();
            Html5AppEntryMetaData objAppEntryMeta = null;

            //Add root/
            objAppEntryMeta = new Html5AppEntryMetaData();
            objAppEntryMeta.Name = Html5RootFolder;
            objAppEntryMeta.Id = strFileRootDirectory;
            objAppEntryMeta.FilePath = strFileRootDirectory;
            objAppEntryMeta.IsDirectory = true;
            objAppEntryMeta.LastEditedDate = DateTime.UtcNow.Ticks;
            objAppEntryMeta.LastEditedBy = this.SubAdminid;
            lstZipEntriesMetaData.Add(objAppEntryMeta);
            //lstFolderNames.Add(Html5RootFolder);
            //End Add root/


            using (objZippedApp)
            {
                foreach (ZipEntry zipEntry in objZippedApp.Entries)
                {
                    var entryms = new MemoryStream();
                    string strFileNameWithPath = zipEntry.FileName;
                    zipEntry.Extract(entryms);
                    if (zipEntry.IsDirectory)
                    {
                        aryTempArray = zipEntry.FileName.Split(Html5FilePathSeparator);
                        if (aryTempArray.Length > 1)
                        {
                            strTempPath = strFileRootDirectory;
                            for (int i = 0; i < aryTempArray.Length - 2; i++)
                            {
                                if (!lstZipEntriesMetaData.Any(entry => entry.Name == aryTempArray[i]))
                                {
                                    objAppEntryMeta = new Html5AppEntryMetaData();
                                    objAppEntryMeta.Name = aryTempArray[i];
                                    objAppEntryMeta.Id = strTempPath + aryTempArray[i] + Html5FilePathSeparator;
                                    objAppEntryMeta.FilePath = strTempPath + aryTempArray[i] + Html5FilePathSeparator;
                                    objAppEntryMeta.IsDirectory = true;
                                    objAppEntryMeta.LastEditedDate = DateTime.UtcNow.Ticks;
                                    objAppEntryMeta.LastEditedBy = this.SubAdminid;
                                    lstZipEntriesMetaData.Add(objAppEntryMeta);
                                    //lstFolderNames.Add(strFolderName);
                                }
                                strTempPath = strTempPath + aryTempArray[i] + Html5FilePathSeparator;
                            }
                            strFolderName = aryTempArray[aryTempArray.Length - 2];
                            objAppEntryMeta = new Html5AppEntryMetaData();
                            objAppEntryMeta.Name = strFolderName;
                            objAppEntryMeta.Id = strFileRootDirectory + zipEntry.FileName;
                            objAppEntryMeta.FilePath = strFileRootDirectory + zipEntry.FileName;
                            objAppEntryMeta.IsDirectory = true;
                            objAppEntryMeta.LastEditedDate = DateTime.UtcNow.Ticks;
                            objAppEntryMeta.LastEditedBy = this.SubAdminid;
                            objAppEntryMeta.FileSize = zipEntry.UncompressedSize;
                            lstZipEntriesMetaData.Add(objAppEntryMeta);
                            //lstFolderNames.Add(strFolderName);
                        }
                        else
                        {
                            strFolderName = aryTempArray[aryTempArray.Length - 2];
                            objAppEntryMeta = new Html5AppEntryMetaData();
                            objAppEntryMeta.Name = strFolderName;
                            objAppEntryMeta.Id = strFileRootDirectory + zipEntry.FileName;
                            objAppEntryMeta.FilePath = strFileRootDirectory + zipEntry.FileName;
                            objAppEntryMeta.IsDirectory = true;
                            objAppEntryMeta.LastEditedBy = this.SubAdminid;
                            objAppEntryMeta.LastEditedDate = DateTime.UtcNow.Ticks;
                            objAppEntryMeta.FileSize = zipEntry.UncompressedSize;
                            lstZipEntriesMetaData.Add(objAppEntryMeta);
                            //lstFolderNames.Add(strFolderName);
                        }

                    }
                    else
                    {
                        string fileName = Path.GetFileName(strFileNameWithPath);
                        string extension = Path.GetExtension(strFileNameWithPath);
                        string strDirectoryName = Path.GetDirectoryName(strFileNameWithPath);
                        aryTempArray = strFileNameWithPath.Split(Html5FilePathSeparator);
                        if (aryTempArray.Length > 1)
                        {
                            strTempPath = strFileRootDirectory;
                            for (int i = 0; i <= aryTempArray.Length - 2; i++)
                            {
                                if (!lstZipEntriesMetaData.Any(entry => entry.Name == aryTempArray[i]))
                                {
                                    objAppEntryMeta = new Html5AppEntryMetaData();
                                    objAppEntryMeta.Name = aryTempArray[i];
                                    objAppEntryMeta.Id = strTempPath + aryTempArray[i] + Html5FilePathSeparator;
                                    objAppEntryMeta.FilePath = strTempPath + aryTempArray[i] + Html5FilePathSeparator;
                                    objAppEntryMeta.IsDirectory = true;
                                    objAppEntryMeta.LastEditedDate = DateTime.UtcNow.Ticks;
                                    objAppEntryMeta.LastEditedBy = this.SubAdminid;
                                    lstZipEntriesMetaData.Add(objAppEntryMeta);
                                    //lstFolderNames.Add(strFolderName);
                                }
                                strTempPath = strTempPath + aryTempArray[i] + Html5FilePathSeparator;
                            }
                            if (this.isFileTypeSupportedForHtml5App(extension))
                            {
                                objAppEntryMeta = new Html5AppEntryMetaData();
                                objAppEntryMeta.Name = fileName;
                                objAppEntryMeta.Id = strFileRootDirectory + zipEntry.FileName;
                                objAppEntryMeta.FilePath = strFileRootDirectory + zipEntry.FileName;
                                objAppEntryMeta.IsDirectory = false;
                                objAppEntryMeta.FileType = getFileTypeValueByExtension(extension);
                                //objAppEntryMeta.FileContent = this.getContentOfZipEntryFile(zipEntry);
                                if (objAppEntryMeta.FileType == (int)HTML5_FILE_TYPES.Image)
                                {
                                    objAppEntryMeta.FileContent = StreamToBase64(zipEntry.OpenReader());

                                }
                                else
                                {
                                    objAppEntryMeta.FileContent = this.getContentOfZipEntryFile(zipEntry);
                                }
                                objAppEntryMeta.FileSize = zipEntry.UncompressedSize;
                                objAppEntryMeta.LastEditedBy = this.SubAdminid;
                                objAppEntryMeta.LastEditedDate = DateTime.UtcNow.Ticks;
                                lstZipEntriesMetaData.Add(objAppEntryMeta);
                            }
                        }
                        else
                        {
                            if (this.isFileTypeSupportedForHtml5App(extension))
                            {
                                objAppEntryMeta = new Html5AppEntryMetaData();
                                objAppEntryMeta.Name = fileName;
                                objAppEntryMeta.Id = strFileRootDirectory + zipEntry.FileName;
                                objAppEntryMeta.FilePath = strFileRootDirectory + zipEntry.FileName;
                                objAppEntryMeta.IsDirectory = false;
                                objAppEntryMeta.FileType = getFileTypeValueByExtension(extension);

                                if (objAppEntryMeta.FileType == (int)HTML5_FILE_TYPES.Image)
                                {
                                    objAppEntryMeta.FileContent = StreamToBase64(zipEntry.OpenReader());

                                }
                                else
                                {
                                    objAppEntryMeta.FileContent = this.getContentOfZipEntryFile(zipEntry);
                                }
                                objAppEntryMeta.FileSize = zipEntry.UncompressedSize;
                                objAppEntryMeta.LastEditedDate = DateTime.UtcNow.Ticks;
                                objAppEntryMeta.LastEditedBy = this.SubAdminid;
                                lstZipEntriesMetaData.Add(objAppEntryMeta);
                            }
                        }

                    }
                }
                //setHtml5AppTreeJson(getHtml5AppFileJsonForTreeView(objZippedApp));
                //setHtml5AppZipEntryHidField(Utilities.SerializeJson<List<string>>(getEntryFileNames(objZippedApp)));
                //setHtml5AppZipFoldersHidField(Utilities.SerializeJson<List<string>>(getZipFolders(objZippedApp)));
                //setHtml5AppRootHtmlFilesHidField(Utilities.SerializeJson<List<string>>(this.getHtmlFilesInRootFolder(objZippedApp)));
            }
            //folders = lstFolderNames;
            return lstZipEntriesMetaData;
        }

        public List<Html5AppEntryMetaData> getZipEntriesInfoFromTempTable(string appId)
        {

            GetHtml5AppTempEntries objGetAppTempEntries = new GetHtml5AppTempEntries(this.SubAdminid, this.CompanyId);
            List<Html5AppEntryMetaData> lstAppEntriesMetaData = new List<Html5AppEntryMetaData>();
            if (!String.IsNullOrEmpty(hTmpAppFileK.Value))//should get this. Session is sotored in this
            {
                Html5AppTempFileKeyForUI objTempFileKey = getHtml5AppTempFileKeyFromHidField();
                objGetAppTempEntries.GetHtml5AppTempFiles(appId, objTempFileKey.S);
                if (objGetAppTempEntries.StatusCode != 0)
                {
                    throw new MficientException(objGetAppTempEntries.StatusDescription);
                }

                List<MFEHtml5AppTempFileDetail> lstMfeAppEntriesDtls = objGetAppTempEntries.Html5AppTempFiles;
                Html5AppEntryMetaData objAppEntryMetaData = null;
                foreach (MFEHtml5AppTempFileDetail tmpFileDtl in lstMfeAppEntriesDtls)
                {
                    objAppEntryMetaData = Utilities.DeserialiseJson<Html5AppEntryMetaData>(tmpFileDtl.FileMeta);
                    objAppEntryMetaData.LastEditedDate = DateTime.UtcNow.Ticks;
                    objAppEntryMetaData.LastEditedBy = this.SubAdminid;
                    lstAppEntriesMetaData.Add(objAppEntryMetaData);
                }
            }
            return lstAppEntriesMetaData;
        }
        public ZipFile createHtml5AppZipFileToUpload(MFEHtml5AppDetail appInfo, List<Html5AppEntryMetaData> appEntries)
        {
            if (appEntries == null || appInfo == null)
            {
                throw new ArgumentNullException();
            }

            ZipFile objZip = createNewZipFileForApp(appInfo.AppName);
            foreach (Html5AppEntryMetaData entryMetaData in appEntries)
            {
                ZipEntry objEntry = new ZipEntry();
                if (entryMetaData.IsDirectory)
                {
                    addDirectoryToZip(objZip, entryMetaData.FilePath, this.SubAdminid, this.CompanyId);
                }
                else
                {
                    MemoryStream msFileContent = null;
                    string strFileContent = String.Empty;
                    if (entryMetaData.FileType == (int)HTML5_FILE_TYPES.Image)
                    {
                        System.Drawing.Image tempImage = Base64ToImage(entryMetaData.FileContent);
                        string mimeType = "";
                        System.Drawing.Imaging.ImageFormat imgFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
                        getImageMimeTypeAndFormatType(tempImage, out mimeType, out imgFormat);
                        byte[] imgInBytes = imageToByteArray(tempImage, imgFormat);
                        msFileContent = new MemoryStream(imgInBytes);
                        addEntryToZip(objZip, entryMetaData.FilePath, msFileContent, this.SubAdminid, this.CompanyId);
                    }
                    else
                    {
                        addEntryToZip(objZip, entryMetaData.FilePath, entryMetaData.FileContent, this.SubAdminid, this.CompanyId);
                    }
                }
            }
            return objZip;
        }

        #region ZipFile Helpers

        Byte[] getDraggedFileDataFromHidField()
        {
            string fileData = hidDragDropFileBytes.Value;
            byte[] fileBytes = new byte[0];
            if (!String.IsNullOrEmpty(fileData))
            {
                fileBytes = Convert.FromBase64String(hidDragDropFileBytes.Value);
            }
            return fileBytes;
        }

        MemoryStream getDraggedFileDataAsStream(byte[] data)
        {
            MemoryStream ms = new MemoryStream(data);
            return ms;
        }

        ZipFile getHtml5AppFromS3Bucket(string companyId, string subAdminId)
        {
            return new ZipFile();
        }
        ZipFile getZipFileFromStream(MemoryStream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException();
            }
            return ZipFile.Read(stream);
        }


        ZipEntry getZipEntryByPath(ZipFile zip, string path)
        {
            if (zip == null || String.IsNullOrEmpty(path)) throw new ArgumentNullException();
            ZipEntry entry = null;
            entry = zip[path];
            return entry;
        }

        string getContentOfZipEntryFile(ZipEntry zipEntry)
        {
            if (zipEntry == null) throw new ArgumentNullException();
            Stream zipEntryStream = zipEntry.OpenReader();
            string strContent = "";
            using (zipEntryStream)
            {

                //zipEntryStream.Position = 0;
                StreamReader sr = new StreamReader(zipEntryStream);
                strContent = sr.ReadToEnd();
            }
            return strContent;
        }

        Html5AppFileDtlForUIPropSheet getZipEntryDetailsForUI(

                    Html5AppEntryMetaData fileMetaData,
                    Html5AppNodeForTreeView fileDtlFromUI,
                    System.Drawing.Image originalImage)
        {
            if (fileMetaData == null) throw new ArgumentNullException();
            string strSubAdminId = fileMetaData.LastEditedBy;
            string strSubAdminName = String.Empty;
            GetSubAdminDetail objSubadminDtl = new GetSubAdminDetail(this.Adminid, this.CompanyId);
            objSubadminDtl.getSubAdminById(strSubAdminId);
            if (objSubadminDtl.StatusCode == 0)
            {
                DataTable dtblSubAdminDtl = objSubadminDtl.ResultTable;
                if (dtblSubAdminDtl != null && dtblSubAdminDtl.Rows.Count > 0)
                {
                    strSubAdminName = Convert.ToString(dtblSubAdminDtl.Rows[0]["FULL_NAME"]);
                }
            }
            TimeZoneInfo tzi = TimeZoneInfo.Local;
            try
            {
                tzi = CompanyTimezone.getTimezoneInfo(this.CompanyId);
            }
            catch
            { }

            string strFullDate = Utilities.getCompanyLocalFormattedDateTime(tzi, fileMetaData.LastEditedDate);
            Html5AppFileDtlForUIPropSheet objFileDtl = new Html5AppFileDtlForUIPropSheet();
            try
            {
                objFileDtl.name = fileMetaData.Name;
                objFileDtl.size = Convert.ToDouble(Convert.ToDouble(fileMetaData.FileSize) / 1024).ToString() + " Kb";
                objFileDtl.editedBy = strSubAdminName;
                objFileDtl.fileType = Convert.ToString(fileDtlFromUI.fileType);
                objFileDtl.lastEditedDate = strFullDate;
                if (fileDtlFromUI.fileType == (int)HTML5_FILE_TYPES.Image)
                {
                    objFileDtl.dimensions = originalImage.Width + " px X " + originalImage.Height + " px";
                }
            }
            catch { }
            return objFileDtl;
        }
        Html5AppFileDtlForUIPropSheet getZipEntryDetailsForUI(ZipEntry entry, Html5AppNodeForTreeView fileDtlFromUI, System.Drawing.Image originalImage)
        {
            if (entry == null) throw new ArgumentNullException();
            string strSubAdminId = entry.Comment;
            string strSubAdminName = String.Empty;
            GetSubAdminDetail objSubadminDtl = new GetSubAdminDetail(this.Adminid, this.CompanyId);
            objSubadminDtl.getSubAdminById(strSubAdminId);
            if (objSubadminDtl.StatusCode == 0)
            {
                DataTable dtblSubAdminDtl = objSubadminDtl.ResultTable;
                if (dtblSubAdminDtl != null && dtblSubAdminDtl.Rows.Count > 0)
                {
                    strSubAdminName = Convert.ToString(dtblSubAdminDtl.Rows[0]["FULL_NAME"]);
                }
            }
            TimeZoneInfo tzi = TimeZoneInfo.Local;
            try
            {
                tzi = CompanyTimezone.getTimezoneInfo(this.CompanyId);
            }
            catch
            { }

            string strFullDate = Utilities.getCompanyLocalFormattedDateTime(tzi, entry.LastModified.Ticks);
            Html5AppFileDtlForUIPropSheet objFileDtl = new Html5AppFileDtlForUIPropSheet();
            try
            {
                objFileDtl.name = entry.FileName;
                objFileDtl.size = Convert.ToDouble(entry.UncompressedSize / 1024).ToString() + " Kb";
                objFileDtl.editedBy = strSubAdminName;
                objFileDtl.fileType = Convert.ToString(fileDtlFromUI.fileType);
                objFileDtl.lastEditedDate = strFullDate;
                if (fileDtlFromUI.fileType == (int)HTML5_FILE_TYPES.Image)
                {
                    objFileDtl.dimensions = originalImage.Width + " px X " + originalImage.Height + " px";
                }
            }
            catch { }
            return objFileDtl;
        }

        ZipFile createNewZipFileForApp(string appName)
        {
            if (String.IsNullOrEmpty(appName)) throw new ArgumentNullException();
            ZipFile objNewZipFile = new ZipFile(appName);
            return objNewZipFile;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zip"></param>
        /// <param name="fileName">Complete name with directory pah</param>
        /// <param name="content"></param>
        /// <param name="path"></param>
        void addEntryToZip(ZipFile zip, string fileName, Stream content, string subAdminId, string companyId)
        {
            ZipEntry entry = zip.AddEntry(fileName, content);
            entry.Comment = subAdminId;
            entry.LastModified = DateTime.UtcNow;
        }
        void addEntryToZip(ZipFile zip, string fileName, string content, string subAdminId, string companyId)
        {
            ZipEntry entry = zip.AddEntry(fileName, content);
            entry.Comment = subAdminId;
            entry.LastModified = DateTime.UtcNow;
        }
        void addDirectoryToZip(ZipFile zip, string folderName, string path, string subAdminId, string companyId)
        {
            ZipEntry entry = null;
            if (String.IsNullOrEmpty(path))
            {
                entry = zip.AddDirectoryByName(folderName);
            }
            else
            {
                entry = zip.AddDirectoryByName(path + folderName);
            }
            entry.Comment = subAdminId;
            entry.LastModified = DateTime.UtcNow;
        }
        void addDirectoryToZip(ZipFile zip, string pathWithFolderName, string subAdminId, string companyId)
        {
            ZipEntry entry = null;
            entry = zip.AddDirectoryByName(pathWithFolderName);
            entry.Comment = subAdminId;
            entry.LastModified = DateTime.UtcNow;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="zip"></param>
        /// <param name="fileName">complete file namw with path.</param>
        /// <param name="content"></param>
        void updateEntryOfZip(ZipFile zip, string fileName, string content, string subAdminId, string companyId)
        {
            if (zip == null || String.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException();
            }
            ZipEntry entry = zip.UpdateEntry(fileName, content);
            entry.Comment = subAdminId;
            entry.LastModified = DateTime.UtcNow;
        }
        void updateEntryOfZip(ZipFile zip, string fileName, Stream content, string subAdminId, string companyId)
        {
            if (zip == null || String.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException();
            }
            ZipEntry entry = zip.UpdateEntry(fileName, content);
            entry.Comment = subAdminId;
            entry.LastModified = DateTime.UtcNow;
        }
        #endregion

        #region HTML5 Tree View Json Helpers

        string getHtml5AppFileJsonForTreeView(ZipFile zip)
        {
            if (zip == null)
            {
                throw new ArgumentNullException();
            }
            string strAppFileJson = "";
            List<Html5AppNodeForTreeView> lstAppNodes = getHtml5AppFileListForTreeView(zip);
            if (lstAppNodes != null)
            {
                strAppFileJson = Utilities.SerializeJson<List<Html5AppNodeForTreeView>>(lstAppNodes);
            }
            return strAppFileJson;
        }

        List<Html5AppNodeForTreeView> getHtml5AppFileListForTreeView(ZipFile zip)
        {
            if (zip == null)
            {
                throw new ArgumentNullException();
            }
            char filePathSeparator = Html5FilePathSeparator;
            List<Html5AppNodeForTreeView> lstAppTree = new List<Html5AppNodeForTreeView>();
            using (zip)
            {
                System.Collections.Generic.ICollection<ZipEntry> entries = zip.Entries;
                IEnumerable ientries = entries.OrderByDescending(s => s.IsDirectory);
                foreach (ZipEntry entry in ientries)
                {

                }
                foreach (ZipEntry zipEntry in zip.Entries)
                {
                    using (var entryms = new MemoryStream())
                    {
                        string strFileNameWithPath = zipEntry.FileName;
                        string fileName = Path.GetFileName(strFileNameWithPath);
                        string extension = Path.GetExtension(strFileNameWithPath);
                        string strDirectoryName = Path.GetDirectoryName(strFileNameWithPath);
                        string strNodeText = "";
                        Html5AppNodeForTreeView objAppTree = null;
                        List<Html5AppNodeForTreeView> lstTempAppTree = new List<Html5AppNodeForTreeView>();
                        string[] tempArray;
                        lstTempAppTree = lstAppTree;
                        bool isDirectory = false;
                        tempArray = strFileNameWithPath.Split(filePathSeparator);
                        if (!zipEntry.IsDirectory)
                        {
                            for (int i = 0; i <= tempArray.Length - 2; i++)
                            {
                                lstTempAppTree = getChildrenOfAppNode(tempArray[i], lstTempAppTree);
                            }
                            strNodeText = fileName;
                        }
                        else
                        {
                            strNodeText = tempArray[tempArray.Length - 2];
                            if (tempArray.Length == 2)
                            {
                            }
                            else
                            {
                                for (int i = 0; i <= tempArray.Length - 3; i++)
                                {
                                    lstTempAppTree = getChildrenOfAppNode(tempArray[i], lstTempAppTree);
                                }
                            }
                            isDirectory = true;
                        }
                        objAppTree = new Html5AppNodeForTreeView();
                        objAppTree.id = strFileNameWithPath;
                        objAppTree.text = strNodeText;//check this potentially an exception throwing area.
                        objAppTree.state = new Html5AppNodeForTreeViewState();
                        objAppTree.children = new List<Html5AppNodeForTreeView>();
                        objAppTree.icon = getAppNodeIconImageByFileExtension(extension);
                        objAppTree.fileType = getFileTypeValueByExtension(extension);
                        objAppTree.isDirectory = isDirectory;
                        lstTempAppTree.Add(objAppTree);
                    }

                }
                lstAppTree = sortAppNodesForTreeView(lstAppTree);
            }
            return lstAppTree;
        }
        string getHtml5AppFileJsonForTreeView(List<Html5AppEntryMetaData> appEntries)
        {
            if (appEntries == null)
            {
                throw new ArgumentNullException();
            }
            string strAppFileJson = "";
            List<Html5AppNodeForTreeView> lstAppNodes = getHtml5AppFileListForTreeView(appEntries);
            if (lstAppNodes != null)
            {
                strAppFileJson = Utilities.SerializeJson<List<Html5AppNodeForTreeView>>(lstAppNodes);
            }
            return strAppFileJson;
        }
        List<Html5AppNodeForTreeView> getHtml5AppFileListForTreeView(List<Html5AppEntryMetaData> appEntries)
        {
            if (appEntries == null)
            {
                throw new ArgumentNullException();
            }
            char filePathSeparator = Html5FilePathSeparator;
            List<Html5AppNodeForTreeView> lstAppTreeNodes = new List<Html5AppNodeForTreeView>();
            //Doing this so that all directories are found first.
            //so that no logic is to be applied when we get to files.
            //ie, we don't have to checkif directory exists,becasue if i does'nt exist then we will have to first
            //create the directory.
            IEnumerable sortedAppEntries = appEntries.OrderByDescending(s => s.IsDirectory);

            foreach (Html5AppEntryMetaData appEntry in sortedAppEntries)
            {
                Html5AppNodeForTreeView objAppTreeNode = null;
                Html5AppNodeForTreeView objTempAppNode = null;
                List<Html5AppNodeForTreeView> lstTempAppTreeNodes = new List<Html5AppNodeForTreeView>();
                string[] tempArray;
                //reinitialise-- move to original point
                lstTempAppTreeNodes = lstAppTreeNodes;
                bool isDirectory = false;
                string strNodeText = "",
                       strIconImage = "";
                int iFileType = int.MinValue;
                tempArray = appEntry.FilePath.Split(filePathSeparator);

                if (!appEntry.IsDirectory)
                {
                    string fileName = Path.GetFileName(appEntry.FilePath);
                    string extension = Path.GetExtension(appEntry.FilePath);
                    string strDirectoryName = Path.GetDirectoryName(appEntry.FilePath);
                    for (int i = 0; i <= tempArray.Length - 2; i++)
                    {
                        lstTempAppTreeNodes = getChildrenOfAppNode(tempArray[i], lstTempAppTreeNodes);
                    }
                    strNodeText = fileName;
                    strIconImage = getAppNodeIconImageByFileExtension(extension);
                    iFileType = getFileTypeValueByExtension(extension);
                    objAppTreeNode = new Html5AppNodeForTreeView();
                    objAppTreeNode.id = appEntry.Id;
                    objAppTreeNode.text = appEntry.Name;//check this potentially an exception throwing area.
                    objAppTreeNode.state = new Html5AppNodeForTreeViewState();
                    objAppTreeNode.children = new List<Html5AppNodeForTreeView>();
                    objAppTreeNode.icon = strIconImage;
                    objAppTreeNode.fileType = iFileType;
                    objAppTreeNode.isDirectory = isDirectory;
                    lstTempAppTreeNodes.Add(objAppTreeNode);
                }
                else
                {
                    strNodeText = tempArray[tempArray.Length - 2];
                    if (tempArray.Length == 2)
                    {
                        objTempAppNode = lstTempAppTreeNodes.Find(appNode => appNode.id == appEntry.Id);
                        if (objTempAppNode == null)
                        {
                            isDirectory = true;
                            strIconImage = "";
                            iFileType = int.MinValue;

                            objAppTreeNode = new Html5AppNodeForTreeView();
                            objAppTreeNode.id = appEntry.Id;
                            objAppTreeNode.text = appEntry.Name;//check this potentially an exception throwing area.
                            objAppTreeNode.state = new Html5AppNodeForTreeViewState();
                            objAppTreeNode.children = new List<Html5AppNodeForTreeView>();
                            objAppTreeNode.icon = strIconImage;
                            objAppTreeNode.fileType = iFileType;
                            objAppTreeNode.isDirectory = isDirectory;
                            lstTempAppTreeNodes.Add(objAppTreeNode);
                        }
                    }
                    else
                    {
                        //for case if root/x/y comes before root/
                        //then in that case we should create a directory first
                        string strTempFolderPath = String.Empty;
                        for (int i = 0; i <= tempArray.Length - 3; i++)
                        {
                            strTempFolderPath = strTempFolderPath + tempArray[i] + Html5FilePathSeparator;
                            objTempAppNode = lstTempAppTreeNodes.Find(appNode => appNode.id == strTempFolderPath);
                            if (objTempAppNode == null)
                            {
                                objAppTreeNode = new Html5AppNodeForTreeView();
                                objAppTreeNode.id = strTempFolderPath;
                                objAppTreeNode.text = tempArray[i];
                                objAppTreeNode.state = new Html5AppNodeForTreeViewState();
                                objAppTreeNode.children = new List<Html5AppNodeForTreeView>();
                                objAppTreeNode.icon = "";
                                objAppTreeNode.fileType = Int32.MinValue;
                                objAppTreeNode.isDirectory = true;
                                lstTempAppTreeNodes.Add(objAppTreeNode);
                            }
                            lstTempAppTreeNodes = getChildrenOfAppNode(tempArray[i], lstTempAppTreeNodes);
                            //strTempFolderPath = tempArray[i] + Html5FilePathSeparator;
                        }
                        //reinitialise again -- move to original point
                        lstTempAppTreeNodes = lstAppTreeNodes;
                        strTempFolderPath = String.Empty;
                        for (int i = 0; i <= tempArray.Length - 3; i++)
                        {
                            lstTempAppTreeNodes = getChildrenOfAppNode(tempArray[i], lstTempAppTreeNodes);
                        }


                        isDirectory = true;
                        strIconImage = "";
                        iFileType = int.MinValue;

                        objAppTreeNode = new Html5AppNodeForTreeView();
                        objAppTreeNode.id = appEntry.Id;
                        objAppTreeNode.text = appEntry.Name;//check this potentially an exception throwing area.
                        objAppTreeNode.state = new Html5AppNodeForTreeViewState();
                        objAppTreeNode.children = new List<Html5AppNodeForTreeView>();
                        objAppTreeNode.icon = strIconImage;
                        objAppTreeNode.fileType = iFileType;
                        objAppTreeNode.isDirectory = isDirectory;
                        lstTempAppTreeNodes.Add(objAppTreeNode);
                    }

                }

            }
            //lstAppTreeNodes = sortAppNodesForTreeView(lstAppTreeNodes);
            return lstAppTreeNodes;
        }

        List<Html5AppNodeForTreeView> getChildrenOfAppNode(string nodeName, List<Html5AppNodeForTreeView> appNodes)
        {
            List<Html5AppNodeForTreeView> lstAppNodes = new List<Html5AppNodeForTreeView>();
            Html5AppNodeForTreeView objAppNode = null;
            foreach (Html5AppNodeForTreeView appNode in appNodes)
            {
                if (appNode.text == nodeName)
                {
                    objAppNode = appNode;
                    break;
                }
            }
            if (objAppNode != null)
            {
                lstAppNodes = objAppNode.children;
            }
            return lstAppNodes;
        }
        Html5AppNodeForTreeView getAppNode(string nodeName, List<Html5AppNodeForTreeView> appNodes)
        {
            Html5AppNodeForTreeView objAppNode = null;
            foreach (Html5AppNodeForTreeView appNode in appNodes)
            {
                if (appNode.text == nodeName)
                {
                    objAppNode = appNode;
                    break;
                }
            }

            return objAppNode;
        }


        void addDirectoryAppNodeToList(Html5AppNodeForTreeView appTreeNode, List<Html5AppNodeForTreeView> listToAdd)
        {
            listToAdd.Add(appTreeNode);
        }
        void addFileAppNodeToList(Html5AppNodeForTreeView appTreeNode, List<Html5AppNodeForTreeView> listToAdd)
        {
            listToAdd.Add(appTreeNode);
        }

        string getAppNodeIconImageByFileExtension(string extension)
        {
            string strNodeIcon = "";
            switch (extension)
            {
                case ".png":
                case ".jpeg":
                case "jpg":
                case "gif":
                    strNodeIcon = "fileTreeImageFile";
                    break;
                case ".js":
                    strNodeIcon = "fileTreeJsFile";
                    break;
                case ".css":
                    strNodeIcon = "fileTreeCssFile";
                    break;
                case ".htm":
                case ".html":
                    strNodeIcon = "fileTreeHtmlFile";
                    break;
                case ".xml":
                    strNodeIcon = "fileTreeXmlFile";
                    break;
                case ".txt":
                    strNodeIcon = "fileTreeTxtFile";
                    break;
            }
            return strNodeIcon;
        }

        List<Html5AppNodeForTreeView> sortAppNodesForTreeView(List<Html5AppNodeForTreeView> appNodes)
        {
            List<Html5AppNodeForTreeView> lstTempNodes = new List<Html5AppNodeForTreeView>();
            IEnumerable<Html5AppNodeForTreeView> sortedTree;
            if (appNodes != null)
            {
                sortedTree = appNodes.OrderBy(s => s.isDirectory);
                lstTempNodes = new List<Html5AppNodeForTreeView>();
                foreach (Html5AppNodeForTreeView node in sortedTree)
                {
                    lstTempNodes.Add(node);
                }
                appNodes = lstTempNodes;
                foreach (Html5AppNodeForTreeView node in appNodes)
                {
                    if (node.isDirectory)
                    {
                        if (node.children.Count > 0)
                        {
                            lstTempNodes = sortAppNodesForTreeView(node.children);
                        }
                        node.children = lstTempNodes;
                    }
                }
            }
            return appNodes;
        }

        Html5AppNodeForTreeView getSelectedFileDataFromHidField()
        {
            string strFileSelectedJson = hidAppFolderSelected.Value;
            Html5AppNodeForTreeView objAppNode = null;
            if (!String.IsNullOrEmpty(strFileSelectedJson))
            {
                objAppNode = Utilities.DeserialiseJson<Html5AppNodeForTreeView>(strFileSelectedJson);
            }
            return objAppNode;
        }

        List<string> getEntryFileNames(List<Html5AppEntryMetaData> appEntries)
        {
            if (appEntries == null) throw new ArgumentNullException();
            List<string> lstFileNames = new List<string>();

            foreach (Html5AppEntryMetaData appEntry in appEntries)
            {
                lstFileNames.Add(appEntry.FilePath);
            }
            return lstFileNames;
        }

        List<string> getZipFolders(List<Html5AppEntryMetaData> appEntries)
        {
            if (appEntries == null)
            {
                throw new ArgumentNullException();
            }
            List<string> lstAppFolders = new List<string>();
            foreach (Html5AppEntryMetaData appEntry in appEntries)
            {
                if (appEntry.IsDirectory)
                {
                    lstAppFolders.Add(appEntry.FilePath);
                }
            }
            return lstAppFolders;
        }
        List<string> getHtmlFilesInRootFolder(List<Html5AppEntryMetaData> appEntries)
        {
            if (appEntries == null)
            {
                throw new ArgumentNullException();
            }
            List<string> lstHtmlFiles = new List<string>();
            string pattern = Html5RootFolder + @"\/\w+\.[html,htm]";
            foreach (Html5AppEntryMetaData appEntry in appEntries)
            {
                if (!appEntry.IsDirectory)
                {
                    Regex htmlRegex = new Regex(pattern);
                    bool isHtmlInRoot = htmlRegex.IsMatch(appEntry.FilePath);
                    if (isHtmlInRoot)
                    {
                        lstHtmlFiles.Add(appEntry.Name);
                    }
                }
            }
            return lstHtmlFiles;
        }
        List<string> getEntryFileNames(ZipFile zip)
        {
            if (zip == null) throw new ArgumentNullException();
            List<string> lstFileNames = new List<string>();

            ICollection<string> fileNames = zip.EntryFileNames;
            foreach (string name in fileNames)
            {
                lstFileNames.Add(name);
            }
            return lstFileNames;
        }
        List<string> getZipFolders(ZipFile zip)
        {
            if (zip == null)
            {
                throw new ArgumentNullException();
            }
            List<string> lstAppFolders = new List<string>();
            string[] aryTempArray = new string[0];
            string strFolderName = "";
            using (zip)
            {
                foreach (ZipEntry zipEntry in zip.Entries)
                {
                    string strFileNameWithPath = zipEntry.FileName;
                    if (zipEntry.IsDirectory)
                    {
                        aryTempArray = strFileNameWithPath.Split(Html5FilePathSeparator);
                        strFolderName = aryTempArray[aryTempArray.Length - 2];
                        lstAppFolders.Add(strFolderName);
                    }
                }
            }
            return lstAppFolders;
        }
        List<string> getHtmlFilesInRootFolder(ZipFile zip)
        {
            if (zip == null)
            {
                throw new ArgumentNullException();
            }
            List<string> lstHtmlFiles = new List<string>();
            string strFileName = "";
            string[] aryTempArray = new string[0];
            string pattern = Html5RootFolder + @"\/\w+\.[html,htm]";
            using (zip)
            {
                foreach (ZipEntry zipEntry in zip.Entries)
                {
                    string strFileNameWithPath = zipEntry.FileName;
                    if (!zipEntry.IsDirectory)
                    {
                        Regex htmlRegex = new Regex(pattern);
                        bool isHtmlInRoot = htmlRegex.IsMatch(strFileNameWithPath);
                        if (isHtmlInRoot)
                        {
                            aryTempArray = strFileNameWithPath.Split(Html5FilePathSeparator);
                            strFileName = aryTempArray[aryTempArray.Length - 1];
                            lstHtmlFiles.Add(strFileName);
                        }
                    }
                }
            }
            return lstHtmlFiles;
        }
        #endregion

        #region Utilities
        public static byte[] streamToByteArray(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        public static string StreamToBase64(Stream input)
        {
            byte[] byteFromStream = streamToByteArray(input);
            string str = Convert.ToBase64String(byteFromStream);
            return str;
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            //using (ms)
            //{

            //}
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }
        public static string StreamToString(Stream input)
        {
            string strContent = String.Empty;
            StreamReader sr = new StreamReader(input);
            strContent = sr.ReadToEnd();
            return strContent;
        }
        public static int StringSizeInAsciiEncoding(string content)
        {
            return System.Text.ASCIIEncoding.ASCII.GetByteCount(content);
        }
        #endregion
        #region HTML5 Uploaded Images Helper

        public byte[] imageToByteArray(System.Drawing.Image imageIn, System.Drawing.Imaging.ImageFormat imgFormat)
        {
            Byte[] data;
            using (MemoryStream ms = new MemoryStream())
            {
                imageIn.Save(ms, imgFormat);
                data = streamToByteArray(ms);
                data = ms.ToArray();
            }
            return data;
        }

        public System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
        {
            using (MemoryStream ms = new MemoryStream(byteArrayIn))
            {
                System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
                return new Bitmap(returnImage);
            }
        }

        public static System.Drawing.Image ResizeImage(System.Drawing.Image image, Size size,
            bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }
            System.Drawing.Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }

        public void getImageMimeTypeAndFormatType(System.Drawing.Image image, out string mimeType, out System.Drawing.Imaging.ImageFormat imgFormat)
        {
            mimeType = "";
            imgFormat = null;
            try
            {
                var file_image = image;

                //list image formats
                var image_formats = typeof(System.Drawing.Imaging.ImageFormat)
                                    .GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static)
                                    .ToList().ConvertAll(property => property.GetValue(null, null));

                //get image format
                var file_image_format = typeof(System.Drawing.Imaging.ImageFormat)
                                        .GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static)
                                        .ToList().ConvertAll(property => property.GetValue(null, null))
                                        .Single(image_format => image_format.Equals(file_image.RawFormat));
                //list image codecs
                var image_codecs = System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders().ToList();

                //get image codec
                var file_image_format_codec = System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders()
                                              .ToList().Single(image_codec => image_codec.FormatID == file_image.RawFormat.Guid);
                //System.Diagnostics.Debug.WriteLine(file_image_format_codec.CodecName + ", mime: " + file_image_format_codec.MimeType + ", extension: " + file_image_format_codec.FilenameExtension, "image_codecs", "file_image_format_type");
                mimeType = file_image_format_codec.MimeType;
                imgFormat = (System.Drawing.Imaging.ImageFormat)file_image_format;
            }
            catch
            {
            }
        }

        #endregion
        #region S3 Helpers
        void uploadAppZipFileToS3Bucket(ZipFile zip, string key)
        {
            //using (zip)
            //{
            //    zip.Save(@"D:\Downloads\test.zip");
            //}

            MemoryStream testStream = new MemoryStream();
            using (testStream)
            {

                zip.Save(testStream);
                //ZipFile obj = ZipFile.Read(testStream);
                MFES3BucketDetails objS3BucketDtl = S3Bucket.GetS3BucketDetails(BucketType.MFICIENT_WEBSERVICE);
                S3BucketFileManager objS3FileManager = new S3BucketFileManager(
                        bucketName,
                        objS3BucketDtl.AccessKey,
                        objS3BucketDtl.SecretAccessKey);
                //Utilities.currentZipFile = testStream.ToArray();
                objS3FileManager.UploadFile(key, testStream);
            }
        }
        ZipFile getAppZipFileFromS3Bucket(string key)
        {
            MFES3BucketDetails objS3BucketDtl = S3Bucket.GetS3BucketDetails(BucketType.MFICIENT_WEBSERVICE);
            S3BucketFileManager objS3FileManager = new S3BucketFileManager(
                    bucketName,
                    objS3BucketDtl.AccessKey,
                    objS3BucketDtl.SecretAccessKey);
            Stream strm = objS3FileManager.Download(key);
            byte[] zipInBytes = new byte[0];
            using (strm)
            {
                zipInBytes = streamToByteArray(strm);
            }
            return ZipFile.Read(new MemoryStream(zipInBytes));
        }
        void deleteAppZipFileFromS3Bucket(string key)
        {
            MFES3BucketDetails objS3BucketDtl = S3Bucket.GetS3BucketDetails(BucketType.MFICIENT_WEBSERVICE);
            S3BucketFileManager objS3FileManager = new S3BucketFileManager(
                    bucketName,
                    objS3BucketDtl.AccessKey,
                    objS3BucketDtl.SecretAccessKey);
            objS3FileManager.DeleteFile(key);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="appName"></param>
        /// <param name="sourceKey"></param>
        /// <returns>The destination key that was formed</returns>
        string moveZipToPublic(string appName, string sourceKey)
        {
            MFES3BucketDetails objS3BucketDtl = S3Bucket.GetS3BucketDetails(BucketType.MFICIENT_WEBSERVICE);
            S3BucketFileManager objS3FileManager = new S3BucketFileManager(
                    bucketName,
                    objS3BucketDtl.AccessKey,
                    objS3BucketDtl.SecretAccessKey);
            string destinationKey = Utilities.GetMd5Hash(this.CompanyId + this.SubAdminid + DateTime.UtcNow.Ticks + appName) + "/" + appName + ".zip";
            objS3FileManager.CopyingFile(sourceKey, destinationKey, bucketName, publicBucketName);
            return destinationKey;
        }

        #endregion

        #endregion
    }
}
﻿<%@ Page Title="mStudio | Data Source" Language="C#" MasterPageFile="~/master/IdeMaster.Master" EnableViewState="true"
    EnableEventValidation="false" AutoEventWireup="true" CodeBehind="ideDataSource.aspx.cs"
    Inherits="mFicientCP.ideDataSource" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="css/IdeToken-input-facebook.css" rel="stylesheet" type="text/css" />
    
    <style>
        .ui-icon-closethick
        {
            margin: 0px;
        }
        .ClCmdDMHeader
        {
            width: 200px;
            float: left;
            border-right: solid 1px silver;
            padding-left: 3px;
            padding-top: 3px;
            height: auto;
        }
        .CmdAppRoot1
        {
            max-height: 30px;
            overflow-x: hidden;
            overflow-y: auto;
            width: 100%;
        }
        
       /* .addform
        {
        float: right;
         margin-left:8px;
          margin-right:7px; 
          width: 49%;
           min-width: 450px;
        }
        .datatablebind
        {
            width: 48%;
            float: left; 
            margin-left:7px;
        } */
        .DbConnLeftDiv
        {
            padding-top:10px !important;
        }
            </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=btnTestConnectionOnEdit.ClientID %>").bind('click', function (e) {
                e.preventDefault();
            });
        });
        function testConnection() {
            $('[id$=<%= btnTestConnectionOnEdit.ClientID %>]').click();
        }

        function SubProcBoxConnectedMessageqqq(_bol) {
            if (_bol) {
                showModalPopUp('SubProcBoxMessage', 'Error', 400);
                $('#btnOkErrorMsg').focus();
            }
            else {
                $('#SubProcBoxMessage').dialog('close');
            }
        }


    </script>
    <link rel="stylesheet" type="text/css" href="css/Jquerydatatablerowclick.css" />
    <style type="text/css">
    .DbConnLeftDiv{float:left;padding-top:3px;padding-left:10px;width:18%;margin-top:5px;cursor:default}
    
    
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainCanvas" runat="server">
    <div style="padding: 10px;">
        <section style="margin: 5px 5px 5px 3px;">
            <%-- //***********************Database Start Connector***********************************//--%>
            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                    <div>
                        <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>Database Sources</h1>">
                        </asp:Label>
                    </div>
                    <div style="position: relative; top: 15px; right: 15px;">
                        <asp:UpdatePanel ID="upaddbtn" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="lnkAddNewMobileUser" runat="server" Text="Add" CssClass="repeaterLink fr" OnClientClick="Dbaddbtndiscard();"
                                    OnClick="lnkAddNewMobileUser_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
                <div id="divcomplete" runat="server" class="divcomplete" style="padding-top: 5px">
                    <asp:UpdatePanel runat="server" ID="updatedatasource" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divleft" runat="server" class="g5" style="width:49% !important ;padding:0 !important">
                                <div id="divDataSources" class="g12" style="padding:0 !important">
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="updatedbright" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divright" runat="server" class="g7" style="width:45% !important; padding-left:10px;">
                               
                                    <asp:UpdatePanel ID="updViewDbConnDetails" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="ProcPriview" style="width: 98%; padding: 5px;">
                                                    <div id="DbConnDetailsDiv" style="display: none;">
                                                    <div class="modalPopUpDetHeaderDiv">
                                                        <div class="modalPopUpDetHeader">
                                                            <asp:Label ID="lblDbConn_Name" runat="server" Text=""></asp:Label>
                                                        </div>
                                                        <div style="float: right;">
                                                            <asp:UpdatePanel ID="updatedbdelete" runat="server">
                                                                <ContentTemplate>
                                                                    <div class="FLeft">
                                                                        <a onclick="showConndivOnEdit();">Edit</a>
                                                                    </div>
                                                                    <div style="float: left;">
                                                                        &nbsp;|&nbsp;
                                                                    </div>
                                                                    <div style="float: left;">
                                                                        <asp:LinkButton ID="lnkdelete" OnClick="btnDeleteClick" runat="server">Delete</asp:LinkButton>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div style="border: none; padding: 0px;">
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">
                                                            Name
                                                        </div>
                                                        <div class="modalPopUpDetColumn2">
                                                            :
                                                        </div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lbldbconnector" runat="server" Text=""></asp:Label>
                                                          
                                                        </div>
                                                    </div>
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">
                                                            mPlugin
                                                        </div>
                                                        <div class="modalPopUpDetColumn2">
                                                            :
                                                        </div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lblDbConn_UseMplugin" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">
                                                            Type
                                                        </div>
                                                        <div class="modalPopUpDetColumn2">
                                                            <asp:Label ID="lbltype" runat="server" Text=":"></asp:Label>
                                                        </div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lblDbConn_Type" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">
                                                            Database
                                                        </div>
                                                        <div class="modalPopUpDetColumn2">
                                                            :
                                                        </div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lblDbConn_Database" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="NonDsnDetails">
                                                        <div class="modalPopUpDetRow">
                                                            <div class="modalPopUpDetColumn1">
                                                                Host
                                                            </div>
                                                            <div class="modalPopUpDetColumn2">
                                                                :
                                                            </div>
                                                            <div class="modalPopUpDetColumn3">
                                                                <asp:Label ID="lblDbConn_Host" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="modalPopUpDetRow">
                                                            <div class="modalPopUpDetColumn1">
                                                                Timeout (Sec)
                                                            </div>
                                                            <div class="modalPopUpDetColumn2">
                                                                :
                                                            </div>
                                                            <div class="modalPopUpDetColumn3">
                                                                <asp:Label ID="lblDbConn_DatabaseTimeOut" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="modalPopUpDetRow">
                                                            <div class="modalPopUpDetColumn1">
                                                                Additional String
                                                            </div>
                                                            <div class="modalPopUpDetColumn2">
                                                                :
                                                            </div>
                                                            <div class="modalPopUpDetColumn3">
                                                                <asp:Label ID="lblDbConn_DatabaseAdditionalString" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </div>
                                                
                                                         <div class="modalPopUpDetRow">
                                                            <div class="modalPopUpDetColumn1">
                                                                Credential
                                                            </div>
                                                            <div class="modalPopUpDetColumn2">
                                                                :
                                                            </div>
                                                            <div class="modalPopUpDetColumn3">
                                                                <asp:Label ID="lbldbCredential" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </div>
                                                        <asp:HiddenField ID="hiddatabaseobject" runat="server" />
                                                    </div>
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">
                                                            Created By
                                                        </div>
                                                        <div class="modalPopUpDetColumn2">
                                                            :
                                                        </div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lblcreatedby" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">
                                                            Last Updated By
                                                        </div>
                                                        <div class="modalPopUpDetColumn2">
                                                            :
                                                        </div>
                                                        <div class="modalPopUpDetColumn3">
                                                            <asp:Label ID="lblupdatedby" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="modalPopUpDetRow">
                                                        <div class="modalPopUpDetColumn1">
                                                            <img id="img14" alt="Expand" src="//enterprise.mficient.com/images/expand.png" style="cursor: pointer;"
                                                                title="Expand" onclick="imgWsCmdAppClick(this);" />
                                                            Objects
                                                        </div>
                                                        <div class="CmdAppRoot1 hide" id="divtest">
                                                            <div id="WsCmdAppDiv">
                                                            </div>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="SubProcborderDiv" style="border: none;">
                                                    <asp:UpdatePanel runat="server" ID="updDbConnTestBtn" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div class="SubProcBtnDiv">
                                                                <asp:Button ID="btnTestConnectionOnEdit"  runat="server" OnClientClick="textviewConnectionbtnclick();"  OnClick="btnTestConnectionOnEdit_Click"
                                                                    Text="Test Connection" CssClass="InputStyle" UseSubmitBehavior="false" />
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdatePanel ID="updateadddbconnector" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                        <div id="Adddetails">
                                            <div id="AddNewConnDiv" runat="server" class="test">
                                                <div style="width: 100%;">
                                                    <div>
                                                        <asp:HiddenField ID="hdfDbConnId" runat="server" />
                                                        <asp:HiddenField ID="hidedit" runat="server" />
                                                    </div>
                                                    <div id="EditDbConnlblDiv" visible="false" runat="server">
                                                        <div class="modalPopUpDetHeader">
                                                            <asp:Label ID="lblDbConnectionName" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                    <div id="EditDbConnTxtDiv" class="DbCmdRow">
                                                        <div class="Headerfontsize" id="add">
                                                            Add New Data Source</div>
                                                        <div class="Headernewfontsize" id="Edit">
                                                            Edit Data Source
                                                        </div>
                                                        <div class="DbConnRightDiv">
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                        <div class="DbConnLeftDiv" id="divconnectionname">
                                                            Name
                                                        </div>
                                                        <div class="DbConnRightDiv" id="divtxtconnectionname">
                                                            <asp:TextBox ID="txtConnectionName" runat="server" Width="80%" class="InputStylepad"
                                                                MaxLength="20"></asp:TextBox>
                                                         
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                          <div class="DbConnLeftDiv" id="divsourcelabel">
                                                            Name
                                                        </div>
                                                        <div class="DbConnRightDiv" id="divsourcelabelname" style="padding-top:8px">
                                                              <asp:Label ID="lblconname" runat="server"></asp:Label>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                        <div class="DbConnLeftDiv" style="margin-top:10px !important">
                                                            mPlugin
                                                        </div>
                                                        <div class="DbConnRightDiv1">
                                                            <asp:UpdatePanel runat="server" ID="updDdlMplugin" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="ddlUseMplugin" runat="server" class="UseUniformCss" OnSelectedIndexChanged="ddlUseMplugin_SelectedIndexChanged"
                                                                        AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                    <asp:HiddenField ID="hdfUseMplugin" runat="server" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                        <div class="DbConnLeftDiv" id="divdbtype"  style="margin-top:10px !important">
                                                            Type
                                                        </div>
                                                        <div class="DbConnRightDiv1" id="divdbval">
                                                            <asp:UpdatePanel runat="server" ID="updDdlDbConn" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="ddlDbConnectorDbType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDbConnectorDbType_SelectedIndexChanged"
                                                                        class="UseUniformCss">
                                                                        <asp:ListItem Value="-1"> Select Database Type </asp:ListItem>
                                                                        <asp:ListItem Value="1">MS SQL Server</asp:ListItem>
                                                                        <asp:ListItem Value="2">Oracle</asp:ListItem>
                                                                        <asp:ListItem Value="3">MySQL</asp:ListItem>
                                                                        <asp:ListItem Value="4">PostgreSQL</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                            <asp:HiddenField ID="hiddrpconnectortype" runat="server" />
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                        <div id="ddlDbDsnDiv" class="hide">
                                                            <div class="DbConnLeftDiv">
                                                                DSN List
                                                            </div>
                                                            <div class="DbConnRightDiv1" id="dd1dbDsndivitem">
                                                                <asp:UpdatePanel runat="server" ID="updDdlDbDsns" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:DropDownList ID="ddlDbDsns" runat="server" class="UseUniformCss">
                                                                            <asp:ListItem Value="-1"> Select DSN </asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:Button ID="btnDsnGet" runat="server" OnClick="btnDsnGet_Click" style="display:none"/>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                        <div id="ddlDbNonDsnDiv">
                                                            <div class="DbConnLeftDiv">
                                                                Host
                                                            </div>
                                                            <div class="DbConnRightDivhide">
                                                                <asp:TextBox ID="txtHostName" runat="server" Width="80%" class="InputStylepad"></asp:TextBox>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                            <div class="DbConnLeftDiv" id="dbconleftdatabase">
                                                                Database
                                                            </div>
                                                            <div class="DbConnRightDivhide">
                                                                <asp:TextBox ID="txtDatabase" runat="server" Width="80%" class="InputStylepad"></asp:TextBox>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                            <div id="Div8" runat="server" class="DbCmdRow">
                                                                <div class="DbConnLeftDiv">
                                                                    Timeout
                                                                </div>
                                                                <div class="DbConnRightDivhide">
                                                                    <asp:TextBox ID="txtDatabaseTimeOut" runat="server" Width="80%" onkeypress="DbConnTimeoutKeypress(event);"
                                                                        class="InputStylepad"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                            <div id="Div10" runat="server" class="DbCmdRow">
                                                                <div class="DbConnLeftDiv">
                                                                    Additional String
                                                                </div>
                                                                <div class="DbConnRightDiv">
                                                                    <asp:TextBox ID="txtDatabaseAdditionalString" runat="server" Width="80%" class="InputStylepad"></asp:TextBox>
                                                                <asp:HiddenField ID="hidupdateconid" runat="server" />
                                                                </div>
                                                            </div>
                                                            <div class="clear">
                                                            <asp:HiddenField ID="hidusernamet" runat="server" />
                                                            <asp:HiddenField ID="hiddbpassword" runat="server" />
                                                            </div>
                                                         
                                                             <div id="divDBCredential" class="DbCmdRow ">
                                                <div class="DbConnLeftDiv" id="divcreden">
                                                Credential
                                                </div>
                                                <div class="DbConnRightDiv1" style="padding-left: 2px" id="divdbcre">
                                                    <asp:DropDownList ID="dbcredential" runat="server" class="UseUniformCss"    EnableViewState="true"   onchange="CredentialOnchange();">
                                                       <asp:ListItem Value="-1">Select Credential</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:HiddenField ID="hidallcredential" runat="server" />
                                                   <asp:HiddenField ID="hidcreuid" runat="server" />
                                                    <asp:HiddenField ID="hidcrepwd" runat="server" />
                                                    <asp:HiddenField ID="hidtypevalcredential" runat="server" />
                                                 <asp:HiddenField ID="hiddbconnectornames" runat="server" />
                                            </div>
                                            <div class="clear">
                                            </div>

                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                        <div style="color: Red;">
                                                            <asp:Label ID="lblTestConnRslt" runat="server"></asp:Label>
                                                        </div>
                                                        <div class="clear">
                                                        </div>
                                                        <div style="align: center" class="SubProcborderDiv">
                                                            <div class="SubProcBtnDiv">
                                                                <asp:UpdatePanel runat="server" ID="updDbConnBtn" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:HiddenField ID="hiddataset" runat="server" />
                                                                        <asp:HiddenField ID="hiddatasource" runat="server" />
                                                                        <asp:Button ID="btnSubmita" runat="server" Text="  Save  " OnClientClick="return DbValidation();" OnClick="btnSaveDbConnectorDetail_Click"
                                                                            class="InputStyle" />
                                                                        <asp:Button ID="btnTestConnection" runat="server" Text="  Test Connection  " OnClientClick="texteditConnectionbtnclick();return DbValidation();" OnClick="btnTestConnection_Click"
                                                                            class="InputStyle" />
                                                                 
                                                                       <asp:Button ID="btnreset" runat="server" Text="  Cancel  " OnClientClick="initializeDbcontrol();return false;" 
                                                                            class="InputStyle" />

                                                                        <asp:Button ID="btnCancelDbConnBack" runat="server" Text="  Cancel  " class="InputStyle hide"
                                                                            OnClientClick="cancelUpdate();return false;" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:HiddenField ID="hidtestconnection" runat="server" />
                          
                            </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:Panel>
            <div class="hide">
            </div>
            <%-- //***********************DataBase  Complete Connector***********************************//--%>
            <%-- //***********************Webservice  Start Connector***********************************//--%>
            <asp:Panel ID="pnlwsRepeterBox" CssClass="repeaterBox" runat="server">
                <asp:Panel ID="pnlwsrepeterboxheader" CssClass="repeaterBox-header" runat="server">
                    <div>
                        <asp:Label ID="lblwsinfo" runat="server" Text="<h1>Web Service Source</h1>">
                        </asp:Label>
                    </div>
                    <div style="position: relative; top: 10px; right: 15px;">
                        <asp:UpdatePanel ID="upadd" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="WsAddNewConnector" runat="server" Text="Add" OnClientClick="Wsaddbtndiscard();" OnClick="WsAddNewConnector_Click"
                                     CssClass="repeaterLink fr"></asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
                <div id="divwscomplete" runat="server" style="padding-top: 5px">
                    <div id="divwsleft" runat="server" class="g5" style="width:47% !important; padding:0 !important">
                        <div id="divwssource"   class="g12" style="padding:0 !important">
                        </div>
                    </div>
                    <div id="divwsright" runat="server"  class="g7" style="width:49% !important">
                        <asp:UpdatePanel ID="updateaddwebservice" runat="server" UpdateMode="Conditional"
                            ChildrenAsTriggers="false">
                            <ContentTemplate>
                                <div style="margin: 0px auto;">
                                    <div class="ProcPriview" style="width: 98%; padding: 5px;">
                                        <div id="WsConnAddDetailsDiv" runat="server" class="wsConn">
                                            <div id="wsheaderconnector" class="DbCmdRow">
                                                <div class="Headerfontsize" id="adddivheader">
                                                    Add WebService Source
                                                </div>
                                                <div class="Headernewfontsize" id="editwsdivheader">
                                                    Edit WebService Source
                                                </div>
                                            </div>
                                            <div class="DbConnRightDiv">
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="wsEiditedtxtDiv" class="DbCmdRowows" style="display: none" runat="server">
                                                <div class="DbConnLeftDiv">
                                                    Name
                                                </div>
                                                <div class="DbConnRightDiv2" style="padding-top: 8px">
                                                    <asp:Label ID="lblwsconname" runat="server"></asp:Label>
                                                    <asp:HiddenField ID="hidconnames" runat="server" />
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="divWsConName" class="DbCmdRow">
                                                <div class="DbConnLeftDiv">
                                                    Name
                                                </div>
                                                <div class="DbConnRightDiv" style="padding-left: 2px">
                                                    <asp:TextBox ID="txtWsConnectionName" runat="server" Width="80%" class="InputStyle"
                                                        MaxLength="20"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="DbCmdRow">
                                                <div class="DbConnLeftDiv">
                                                    Type
                                                </div>
                                                <div class="DbConnRightDiv">
                                                    <asp:DropDownList ID="ddlWebSrcConnectorType" runat="server" class="UseUniformCss"
                                                        onchange="WebSrcConnTypeOnChange();">
                                                        <asp:ListItem Value="-1">SELECT  TYPE </asp:ListItem>
                                                        <asp:ListItem Value="http">REST</asp:ListItem>
                                                        <asp:ListItem Value="wsdl">WSDL</asp:ListItem>
                                                        <asp:ListItem Value="rpc">XML-RPC</asp:ListItem>
                                                    </asp:DropDownList>
                                              
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="DbConnLeftDiv">
                                                Url
                                            </div>
                                            <div class="DbConnRightDiv" style="padding-left: 2px">
                                                <asp:TextBox ID="txtWsConnectorUrl" runat="server" Width="80%" class="InputStyle"></asp:TextBox>
                                                <asp:HiddenField ID="hdfWsSavedUrl" runat="server" />
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="DbConnLeftDiv">
                                                mPlugin
                                            </div>
                                            <div class="DbConnRightDiv">
                                                <asp:DropDownList ID="ddlWsUseMplugin" runat="server" class="UseUniformCss">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdfWsUseMplugin" runat="server" />
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="MimeTypeDiv" class="DbCmdRow hide">
                                                <div class="DbConnLeftDiv">
                                                    <asp:Label ID="Label12" runat="server" Text="Response Type"></asp:Label>
                                                </div>
                                                <div class="DbConnRightDiv">
                                                    <asp:DropDownList ID="ddlWebSrcConnectorRsp" runat="server" class="UseUniformCss">
                                                        <asp:ListItem Value="XML">XML</asp:ListItem>
                                                        <asp:ListItem Value="JSON">JSON</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="DbConnLeftDiv">
                                            </div>
                                            <div class="DbConnLeftDivChk">
                                 <%--      <input type='checkbox' onchange='chkhttpOnChange(this);'>--%>
                                               <asp:CheckBox ID="chkhttp" runat="server" onclick="chkhttpOnChange(this);" />
                                                Http Authentication
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="httpAuthenticationDiv" class="DbCmdRow hide">
                                                <div class="DbConnLeftDiv">
                                                    Type
                                                </div>
                                                <div class="DbConnRightDiv">
                                                    <asp:DropDownList ID="drphttpauthenticationtype" runat="server" class="UseUniformCss">
                                                        <asp:ListItem Value="0">Basic</asp:ListItem>
                                                        <asp:ListItem Value="1">Digest</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="DbConnLeftDiv" id="divCredential">
                                                Credential
                                                </div>
                                                <div class="DbConnRightDiv" id="divCtlCredential">
                                                    <asp:DropDownList ID="drpCredentiail" runat="server" class="UseUniformCss"  onchange="wscredential();">
                                                       <asp:ListItem Value="-1">Select Credential</asp:ListItem>
                                                    </asp:DropDownList>
                                                     <asp:HiddenField ID="hidwscreatedtial" runat="server" />
                                                    <asp:HiddenField ID="hidwsusername" runat="server" />
                                                     <asp:HiddenField ID="hidwsusernamepassword" runat="server" />
                                                     <asp:HiddenField ID= "hidwstypeofcredential" runat="server" />
                                                 
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="SubProcborderDiv">
                                                <div class="SubProcBtnDiv" align="center">
                                                    <asp:HiddenField ID="hidwsdataset" runat="server" />
                                                    <asp:HiddenField ID="hidwscdataset" runat="server" />
                                                    <asp:UpdatePanel ID="updatesave" runat="server">
                                                        <ContentTemplate>
                                                         <asp:Button ID="btnWsConnectorSave" runat="server" Text="  Save  "  OnClick="btnSaveWsConnectorDetail_Click"  class="InputStyle" />
                                                           <%-- <asp:Button ID="btnWsConnectorSave" runat="server" Text="  Save  " OnClientClick="return  WsValidation();" OnClick="btnSaveWsConnectorDetail_Click"--%>
                                                               
                                                                 <asp:Button ID="btnwsreset" runat="server" Text="  Cancel  " OnClientClick="InitializeControls();return  false;" 
                                                                class="InputStyle" />
                                                            <asp:Button ID="btnHtmWsConnectorBack" runat="server" Text="  Cancel  " class="InputStyle hide"
                                                                OnClientClick="CancelUpdate();return false;" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modalPopUpDetHeaderDiv hide" id="WsConnDetailsDivview">
                                            <div id="WsConnDetailsDiv" style="width: 98%; padding: 12px;">
                                                <div>
                                                    <asp:HiddenField ID="hdfWsConnId" runat="server" />
                                                </div>
                                                <div class="modalPopUpDetHeaderDiv">
                                                    <div class="modalPopUpDetHeader">
                                                        <asp:Label ID="lblwsname" runat="server" Text=""></asp:Label>
                                                    </div>
                                                    <div class="modalPopUpDetHeader">
                                                        <asp:Label ID="lblWsConn_Name" runat="server" CssClass="hide" Text=""></asp:Label>
                                                    </div>
                                                    <div style="float: right;">
                                                        <asp:UpdatePanel ID="updatewsdelete" runat="server">
                                                            <ContentTemplate>
                                                                <div class="FLeft">
                                                                    <a onclick="ShowWsConnDivOnEdit();">Edit</a>
                                                                </div>
                                                                <div style="float: left;">
                                                                    &nbsp;|&nbsp;
                                                                </div>
                                                                <div style="float: left;">
                                                                    <asp:LinkButton ID="lnkwsdelete" runat="server" Text="Delete" OnClick="btnWs_DeleteClick"></asp:LinkButton>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div style="border: none; padding: 0px;">
                                                <div class="clear">
                                                </div>
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Source
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblwsconnector" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Type
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblWsConn_Type" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        mPlugin
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblWsConn_UseMplugin" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Url
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblWsConn_Url" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div id="WsConnDetails_RType" runat="server" class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Response Type
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblWsConn_RType" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Created By
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblwscretedby" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Last Updated By
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblwsupdatedby" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                             
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        <img id="img1" alt="Expand" src="//enterprise.mficient.com/images/expand.png" style="cursor: pointer;"
                                                            title="Expand" onclick="imgWsCmdAppClickStart(this);" />
                                                        Objects
                                                    </div>
                                                    <div class="CmdAppRoot1 hide" id="divwstest">
                                                        <div id="WsCmdAppdtl">
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                    <asp:HiddenField ID="hidwebdataobj" runat="server" />
                                                    <asp:HiddenField ID="hidWsAuthencationMeta" runat="server" />
                                                    <asp:HiddenField ID="hidwscredebttial" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </asp:Panel>

            <%-- //***********************Webservice Complete Connector***********************************//--%>
            <%-- //***********************ODATA   Connector***********************************//--%>
            <asp:Panel ID="pnlOdataRepeterBox" CssClass="repeaterBox" runat="server">
                <asp:Panel ID="pnlodatarepeterboxheader" CssClass="repeaterBox-header" runat="server">
                    <div>
                        <asp:Label ID="lblodataheader" runat="server" Text="<h1>Odata Source</h1>">
                        </asp:Label>
                    </div>
                    <div style="position: relative; top: 10px; right: 15px;">
                        <asp:UpdatePanel ID="updateOdata" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="lnkAddodata" runat="server" Text="Add" OnClientClick="Odataaddbtndiscard();" CssClass="repeaterLink fr"
                                    OnClick="lnkAddodata_Click"></asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
                <div id="divodatacomplete" runat="server" style="padding-top: 5px">
                    <div id="divodataleft" runat="server" class="g5" style="width:47%;padding:0 !important">
                        <div id="divodatasource"  class="g12" style="padding:0 !important">
                        </div>
                    </div>
                    <div id="divodataright" runat="server" class="g7" style="width:49% !important">
                        <asp:UpdatePanel runat="server" ID="updateodataright" UpdateMode="Conditional">
                            <ContentTemplate>
                                         <div id="addodataconnector" >
                                            <div id="Div4" class="DbCmdRow">
                                                <div class="Headerfontsize" id="divaddodataheader">
                                                    Add Odata Source
                                                </div>
                                                <div class="Headernewfontsize" id="diveditodataheader">
                                                    Edit Odata Source
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="ODataAddDiv" runat="server" class="showDetails">
                                                <div class="ConnectorDivBox">
                                                    <div id="ODatalblConnDiv" class="modalPopUpDetHeaderDiv hide">
                                                        <div class="modalPopUpDetHeader">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="ODatatxtConnDiv" class="DbCmdRow">
                                                <div class="DbConnLeftDiv">
                                                    Source Name
                                                </div>
                                                <div class="DbConnRightDiv" style="padding-top: 3px; padding-left: 3px;">
                                                   
                                                    <asp:TextBox ID="txtOdataConnName" runat="server" Width="80%" class="InputStyle"
                                                        MaxLength="20"></asp:TextBox>

                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        <div id="Divlabelsource" class="DbCmdRow hide">
                                                <div class="DbConnLeftDiv">
                                                    Source Name
                                                </div>
                                                <div class="DbConnRightDiv" style="padding-top: 8px; padding-left: 3px;">
                                                     <asp:Label ID="lblOdataConnName" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>


                                             <div class="clear">
                                            </div>

                                            <div class="DbConnLeftDiv">
                                                Service EndPoint
                                            </div>
                                            <div class="DbConnRightDiv" style="padding-left: 3px">
                                                <asp:TextBox ID="txtOdataService" runat="server"  Width="80%"  class="InputStyle"></asp:TextBox>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="DbConnLeftDiv">
                                                mPlugin
                                            </div>
                                            <div class="DbConnRightDiv" style="padding-left: 0px">
                                                <asp:DropDownList ID="ddlOdataMpluginConn" runat="server" class="UseUniformCss">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="FLeft" style="width: 100%;">
                                                <div class="DbConnLeftDiv">
                                                </div>
                                                <div class="DbConnLeftDivChk" style="padding-left: 0px">
                                                    <asp:CheckBox ID="chkODataHttp" CssClass="checkbox" runat="server" onchange="chkODataHttpOnChange(this)" />
                                                    Http Authentication
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div id="OdataHttpAuthDiv" class="DbCmdRow hide">
                                                    <div class="DbConnLeftDiv">
                                                        Authentication Type
                                                    </div>
                                                    <div class="DbConnRightDiv">
                                                        <asp:DropDownList ID="drpOdataAutenticationtype" runat="server" class="UseUniformCss">
                                                            <asp:ListItem Value="0">Basic</asp:ListItem>
                                                            <asp:ListItem Value="1">Digest</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:HiddenField ID="hidautenticationtype" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divOdataCrPre" class="DbCmdRow hide">
                                                <div class="DbConnLeftDiv" id="divAvilableCre">
                                                Credential
                                                </div>
                                                <div class="DbConnRightDiv"  id="divSelectCre">
                                                    <asp:DropDownList ID="drpOCredentiail" runat="server"  EnableViewState="true" onchange="Odatacredential();">
                                                    <asp:ListItem Value="-1">Select Credential</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:HiddenField ID="hidcredentialtype" runat="server" />
                                                      <asp:HiddenField ID="hidodatacredential" runat="server" />
                                                    <asp:HiddenField ID="hidodatacredentialusername" runat="server" />
                                                     <asp:HiddenField ID="hidodatacredentialpassword" runat="server" />
                                                     <asp:HiddenField ID="hidodatatypleofcredential" runat="server" />
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="SubProcborderDiv">
                                                <div class="SubProcBtnDiv" align="center">
                                                    <asp:Button ID="btnSaveODataConn" runat="server" Text="  Save  " OnClientClick="return OdataValidation();" class="InputStyle"
                                                        OnClick="btnSaveODataConn_Click" />
                                                        <input id="btnodatareset" type="button" class="InputStyle" value="  Cancel  "
                                                        onclick="intializeOdataSource();" />
                                                    <input id="btnBackODataCancel" type="button" class="InputStyle hide" value="  Cancel  "
                                                        onclick="Cancelupdate();" />
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: none;" id="editodata" >
                                            <div>
                                                <asp:HiddenField ID="hdfODataConnId" runat="server" />
                                                <asp:Label ID="lbloconnectorid" CssClass="hide" runat="server"></asp:Label>
                                            </div>
                                            <div class="modalPopUpDetHeaderDiv">
                                                <div class="modalPopUpDetHeader">
                                                    <asp:Label ID="lblheaderConnector" runat="server"></asp:Label>
                                                </div>
                                                <div style="float: right;">
                                                    <asp:UpdatePanel ID="updateOdataDelete" runat="server">
                                                        <ContentTemplate>
                                                            <div class="FLeft">
                                                                <a onclick="ShowODataConnEdit()">Edit</a>
                                                            </div>
                                                            <div style="float: left;">
                                                                &nbsp;|&nbsp;
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:LinkButton ID="lnkodatadelete" runat="server" Text="Delete" OnClick="btnOdata_DeleteClick"></asp:LinkButton>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                            <div class="modalPopUpDetRow">
                                                <div class="modalPopUpDetColumn1">
                                                    Source
                                                </div>
                                                <div class="modalPopUpDetColumn2">
                                                    :
                                                </div>
                                                <div class="modalPopUpDetColumn3">
                                                    <asp:Label ID="lblviewConnector" runat="server" Text=""></asp:Label>
                                                    <asp:HiddenField ID="hidviewconnector" runat="server" />
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="modalPopUpDetRow">
                                                <div class="modalPopUpDetColumn1">
                                                    mPlugin
                                                </div>
                                                <div class="modalPopUpDetColumn2">
                                                    :
                                                </div>
                                                <div class="modalPopUpDetColumn3">
                                                    <asp:Label ID="lblODataConn_UseMplugin" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="modalPopUpDetRow">
                                                <div class="modalPopUpDetColumn1">
                                                    Url
                                                </div>
                                                <div class="modalPopUpDetColumn2">
                                                    :
                                                </div>
                                                <div class="modalPopUpDetColumn3">
                                                    <asp:Label ID="lblODataConn_Url" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="modalPopUpDetRow">
                                                <div class="modalPopUpDetColumn1">
                                                    Version
                                                </div>
                                                <div class="modalPopUpDetColumn2">
                                                    :
                                                </div>
                                                <div class="modalPopUpDetColumn3">
                                                    <asp:Label ID="lblODataConn_Version" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                           <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Created By
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblOdatacreatedby" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                          <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Last Updated By
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblOdataupdatedby" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                         <div class="modalPopUpDetRow">
                                                <div class="modalPopUpDetColumn1">
                                                    <img id="img2" alt="Expand" src="//enterprise.mficient.com/images/expand.png" style="cursor: pointer;"
                                                        title="Expand" onclick="imgOdataCmdAppClickStart(this);" />
                                                    Objects
                                                </div>
                                                <div class="CmdAppRoot1 hide" id="divodataobj">
                                                    <div id="OdataCmdAppDiv">
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <asp:HiddenField ID="hidOdataobjdts" runat="server" />
                                            </div>
                                            <div class="clear">
                                                </div>
                                                </div>
                                        <div class="clear">
                                        </div>
                                <asp:HiddenField ID="hidodataset" runat="server" />
                                <asp:HiddenField ID="hidodatacompletedataset" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </asp:Panel>
            <%-- //***********************ODATA Connector Complete***********************************//--%>
            <div id="SubProcBoxMessage">
                <div>
                    <div class="MessageDiv">
                        <a id="a2">Please check following points : </a>
                        &nbsp;&nbsp;
                        <br />
                        <%--  <a id="aCfmMessage"></a>--%>
                        
                    </div>
                    <div style="margin-top: 5px">
                    <a id="aMessage"></a>
                    </div>
                    <div class="SubProcborderDiv">
                        <div class="SubProcBtnDiv">
                            <input id="btnOkErrorMsg" type="button" value="  OK  " onclick="SubProcBoxMessage(false);"
                                class="InputStyle" />
                        </div>
                    </div>
                </div>
            </div>
            <div id="SubProcBoxDeleteMessage" style="display: none">
                <div class="ProcBoxMessage">
                    <div style="margin-top: 10px">
                    </div>
                    <div class="MessageDiv">
                        <a id="a4" class="DefaultCursor">Do you want to delete this control ?</a>
                    </div>
                    <div style="margin-top: 10px">
                    </div>
                    <div class="SubProcborderDiv">
                        <div class="SubProcBtnDivcenter">
                            <input id="Button2" type="button" value="  Yes  " onclick="SubProcBoxDeleteMessage(false);"
                                class="InputStyle" />
                            <input id="Button3" type="button" value="  No  " onclick="SubProcBoxDeleteMessage(false);"
                                class="InputStyle" />
                        </div>
                    </div>
                </div>
            </div>
            <div id="SubProcImageDelConfirnmation" style="display: none; z-index: 21px;">
                <div>
                    <div>
                        <asp:HiddenField ID="hdfDelId" runat="server" />
                    </div>
                    <div>
                        <asp:HiddenField ID="hdfDelType" runat="server" />
                    </div>
                    <div class="MarginT10">
                    </div>
                    <div class="MessageDiv">
                        <a id="aDelCfmMsg"></a>
                    </div>
                    <div class="MarginT10">
                    </div>
                    <div class="SubProcborderDiv">
                        <div class="SubProcBtnMrgn1">
                            <asp:UpdatePanel runat="server" ID="UpdDeleteConfirm" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Button ID="btnDeleteCfm" runat="server" Text="  Yes  " OnClick="btnDeleteCfm_Click"
                                        CssClass="InputStyle" />
                                    <input id="Button13" type="button" value="  No  " onclick="SubProcImageDelConfirnmation(false,'');"
                                        class="InputStyle" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SubProcImageNotDelConfirmation">
                <div class="MessageDiv">
                    <a id="a1"></a>
                </div>
            </div>
            <div id="divCredentialBymobile" class="hide">
                <asp:UpdatePanel ID="updatecredentialwithoutsave" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="DbConnLeftDiv " id="div2">
                            Username
                        </div>
                        <div class="DbConnRightDiv " style="padding-left: 12px" id="div3">
                            <asp:TextBox ID="txtodatapredefinedusername" runat="server" Width="90%" class="InputStyle"
                                MaxLength="50"></asp:TextBox>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="DbConnLeftDiv" id="div5">
                            Password
                        </div>
                        <div class="DbConnRightDiv " style="padding-left: 12px" id="div6">
                            <asp:TextBox ID="txtodatapredefinedpass" TextMode="Password" runat="server" Width="90%"
                                class="InputStyle" MaxLength="50"></asp:TextBox>
                                
                        </div>
                        <div class="clear">
                        </div>
                        <div class="SubProcborderDiv">
                            <div class="SubProcBtnMrgn" align="center">
                                <asp:Button ID="SaveCredentialbyphone" runat="server" Text="  Next  " OnClientClick="return OdataCredentialvalidation();" OnClick="btnSaveODataConn1_Click"
                                    class="InputStyle" />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="divWsPredefined" class="hide">
                <asp:UpdatePanel ID="UpdatePanelwspredefined" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="DbConnLeftDiv " id="divwsusernamepre">
                            Username
                        </div>
                        <div class="DbConnRightDiv " style="padding-left:12px" id="divwspredefined">
                            <asp:TextBox ID="txtwsspredefined" runat="server" Width="90%" class="InputStyle"
                                MaxLength="50"></asp:TextBox>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="DbConnLeftDiv" id="div12">
                            Password
                        </div>
                        <div class="DbConnRightDiv " style="padding-left: 12px" id="divwspredefinedpass">
                            <asp:TextBox ID="txtwspredefinedpassword" TextMode="Password" runat="server" Width="90%"
                                class="InputStyle" MaxLength="50"></asp:TextBox>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="SubProcborderDiv">
                            <div class="SubProcBtnMrgn" align="center">
                                <asp:Button ID="Button1" runat="server" Text="  Next  " OnClientClick="return wsdataCredentialvalidation();"
                                    OnClick="btnSaveWsConnectorDetail1_Click" class="InputStyle" />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
             <div id="divdb" class="hide">
                <asp:UpdatePanel ID="updatedb" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="DbConnLeftDiv " id="divdbusernamehdetail">
                            Username
                        </div>
                        <div class="DbConnRightDiv " style="padding-left: 15px" id="divdbusernamedetail">
                            <asp:TextBox ID="txtdbusercreatial" runat="server"  Width="90%" class="InputStyle"
                                MaxLength="50"></asp:TextBox>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="DbConnLeftDiv" id="divdbusernamep">
                            Password
                        </div>
                        <div class="DbConnRightDiv " style="padding-left: 15px" id="divdbusernamepassword">
                            <asp:TextBox ID="txtdbpasswordcreatial" TextMode="Password" runat="server" Width="90%"
                                class="InputStyle" MaxLength="50"></asp:TextBox>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="SubProcborderDiv">
                            <div class="SubProcBtnMrgn" align="center">
                                <asp:Button ID="btnnextdb" runat="server" Text="  Next  " OnClientClick="return Credentialvalidation();" OnClick="btnnextdb_click"
                                    class="InputStyle" />
                            </div>
                        </div>
                 </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="SubProcConfirmBoxMessage">
                <div>
                    <div>
                        <div class="ConfirmBoxMessage1">
                            <a id="aCFmessage"></a>
                        
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="SubProcborderDiv">
                        <div class="SubProcBtnMrgn" align="center">
                            <input id="btnCnfFormSave" type="button" value="   OK   " onclick="SubProcConfirmBoxMessage(false);"
                                class="InputStyle" />
                        </div>
                    </div>
                </div>
            </div>
            <div id="divWaitBox" class="waitModal">
                <div id="WaitAnim">
                    <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                        AlternateText="Please Wait" BorderWidth="0px" />
                </div>
            </div>
            <div id="SubProcBoxUldWsdlCfm">
                <div>
                    <div>
                        <asp:HiddenField ID="hdfWsdlUploadId" runat="server" />
                    </div>
                    <div id="UploadWsdlCfnDiv" style="margin-top: 10px;">
                        <a id="a12">Wsdl could not found.
                            <br />
                            <br />
                            Do you want to upload wsdl file manually ?</a>
                    </div>
                    <div>
                        <div id="GetWsdlMannual" class="DbCmdRow" style="margin-top: 10px; display: none;">
                            <div class="DbConnRightDiv">
                                Only .xml or .txt files are allowed
                            </div>
                            <div class="clear">
                            </div>
                            <div class="DbConnRightDiv">
                                <asp:UpdatePanel runat="server" ID="updWsdlIfrm" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <iframe id="Iframe1" runat="server" width="320px" height="55px"></iframe>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div id="Div25" class="SubProcborderDiv">
                                <div class="SubProcBtnMrgn" align="center">
                                </div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="MarginT10">
                    </div>
                    <div id="btnUploadWsdlCfn" class="SubProcborderDiv">
                        <div class="SubProcBtnMrgn" align="center">
                            <input id="Button31" type="button" value="  Yes  " onclick="ShowGetWsdlMannualDiv();"
                                class="InputStyle" />
                            <input id="Button32" type="button" value="  No  " onclick="SubProcBoxUldWsdlCfm(false);"
                                class="InputStyle" />
                        </div>
                    </div>
                </div>
            </div>

            <asp:HiddenField ID="hideditdone" runat="server" />
            <asp:HiddenField ID="hiddiscardcommadtype" runat="server" />
            <asp:HiddenField ID="hidodatasavediscard" runat="server" />
            <div id="SubProobjectChange" class="hide">
      <asp:UpdatePanel ID="updatediscard" runat="server">
      <ContentTemplate>
            <div class="MessageDiv">
                <a id="aFormChangeMsg" class="DefaultCursor">
                 Discard any changes made?
                 </a>
                  
            </div>
            <div class="SubProcborderDiv" style="margin-top:30px !important;">
                <div class="SubProcBtnMrgn" align="center">
        
                <asp:Button ID="btnfrmDiscard" runat="server" text="  Yes  " OnClientClick="$('[id$=hideditdone]').val('');$('#SubProobjectChange').dialog('close');discardedit();return false;"
                        class="InputStyle" />
                <asp:Button ID="btnsaveCurrentobject" runat="server"  class="InputStyle" text="  No  "  OnClick="btnSaveDb_Click"/>

                    <input id="btnCancelSaveCurrentForm" type="button" value="  Cancel " onclick="$('#SubProobjectChange').dialog('close');Discarddataobjectcancel();"
                        class="InputStyle" />
                </div>
            </div>
            </ContentTemplate>
      </asp:UpdatePanel>
        
    </div>




        </section>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {

            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            makeSelectUniformOnPostBack();
            hideWaitModal();
            isCookieCleanUpRequired('true');

        }
    </script>
 
    <asp:HiddenField ID="hidheader" runat="server" />
    <asp:HiddenField ID="hdfPostBackPageMode" runat="server" />
   <asp:HiddenField ID="hidcommadagin" runat="server" />
</asp:Content>

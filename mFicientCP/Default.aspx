﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="mFicientCP._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title>mFicient | Login</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1" />
    <!-- Google Font and style definitions -->
    <link rel="stylesheet" href="css/CanvasStyle.css" id="Link2" />
    <link rel="stylesheet" href="css/themes/CanvasTheme.css" id="Link1" />
    <link rel="stylesheet" href="css/themes/jquery-CP-Theme-ui.css" id="Link3" />
    <link rel="stylesheet" href="css/themes/jquery.uniform.css" id="Link4" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/themes/theme.css" id="themestyle" />
    <!--[if lt IE 9]>
	<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    <script src="Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script src="Scripts/login.js" type="text/javascript"></script>
    <script src="Scripts/WLJS.js" type="text/javascript"></script>
    <script src="Scripts/Scripts.js" type="text/javascript"></script>
    <script type="text/javascript">
        if (document.images) {
            var dialogError = new Image(16, 16);
            dialogError.src = '//enterprise.mficient.com/images/dialog_error.png';
            var dialogInfo = new Image(16, 16);
            dialogInfo.src = '//enterprise.mficient.com/images/dialog_info.png';
            var dialogWarning = new Image(16, 16);
            dialogWarning.src = '//enterprise.mficient.com/images/dialog_warning.png';
            var progressImage = new Image(32, 32);
            progressImage.src = '//enterprise.mficient.com/css/images/icons/dark/progress.gif';
            var checkBoxImage = new Image(493, 700);
            progressImage.src = '//enterprise.mficient.com/css/themes/images/uniform/sprite.png';
        }

        function clearResetPasswordFields() {
            $('#' + '<%=txtForgotPwdCompanyId.ClientID %>').val('');
            $('#' + '<%=txtForgotPwdUserName.ClientID %>').val('');
        }
        
    </script>
</head>
<body id="login">
    <form id="loginform" runat="server" style="border: 0px;">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <header id="loginLogo">
                <div id="logo">
                    <a href="Default.aspx">mficient</a>
                </div>
            </header>
            <section id="content" class="loginContent">
                <div id="formDiv" style="margin-bottom: 1px;">
                    <fieldset>
                        <section>
                            <label for='<%=txtCompanyId.ClientID%>'>
                                Enterprise Id</label>
                            <div class="rememberParticularField">
                                <asp:CheckBox ID="chkRememberCompanyId" runat="server" Text="remember" />
                            </div>
                            <div>
                                <asp:TextBox ID="txtCompanyId" runat="server"></asp:TextBox>
                            </div>
                        </section>
                        <section>
                            <label for='<%=txtUserName.ClientID%>'>
                                Username</label>
                            <div class="rememberParticularField">
                                <asp:CheckBox ID="chkRememberUserName" runat="server" Text="remember" />
                            </div>
                            <div>
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox></div>
                        </section>
                        <section>
                            <label for='<%=txtPassword.ClientID%>'>
                                Password
                            </label>
                            <div>
                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                            </div>
                        </section>
                        <section>
                            <asp:LinkButton ID="lnkForgotPassword" runat="server" OnClientClick="clearResetPasswordFields();showModalPopUp('divModalContainer','Reset password',300,false);return false;"
                                OnClick="lnkForgotPassword_Click" Style="font-size: 12px; height: 13px; padding-left: 4%;">lost password?</asp:LinkButton>
                            <div style="float: right; width: auto; padding-top: 2%;">
                                <asp:Button ID="btnSubmit" runat="server" Text="login" CssClass="fr aspButton" OnClick="btnSubmit_Click" />
                            </div>
                        </section>
                </div>
            </fieldset>
            <asp:HiddenField ID="hidQueryStrFromEmail" runat="server" />
            </div>
             </section>
            
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="text-align:center;margin-top:15px;font-size:11px;color:#B2B2B2;">© Emficient Technologies Pvt Ltd | All Rights Reserved</div>
    <asp:UpdatePanel runat="server" ID="TransferUpdatePanel" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:HiddenField ID="hfs" runat="server" />
            <asp:HiddenField ID="hfp" runat="server" />
            <asp:HiddenField ID="hfbid" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <section>
        <div id="divModalContainer">
            <div id="divForgotPwdContainer" style="display: block">
                <asp:UpdatePanel ID="updModalContainer" runat="server">
                    <ContentTemplate>
                        <div id="divForgotPwdAlertMsgs">
                        </div>
                        <div id="modalPopUpContent" class="manageGroupPopUpCont" style="width:95%;overflow:hidden">
                            <div>
                                <label for='<%=txtForgotPwdCompanyId.ClientID %>'>
                                    Enterprise Id</label>
                                <div>
                                    <asp:TextBox ID="txtForgotPwdCompanyId" runat="server" CssClass="required"></asp:TextBox>
                                </div>
                            </div>
                            <div>
                                <label for='<%=txtForgotPwdUserName.ClientID %>'>
                                    Username</label>
                                <div>
                                    <asp:TextBox ID="txtForgotPwdUserName" runat="server" CssClass="required"></asp:TextBox>
                                </div>
                            </div>
                            <div class="modalPopUpAction">
                                <div id="divPopUpActionCont" class="popUpActionCont" style="width: 60%;">
                                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="aspButton" OnClick="btnReset_Click" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="aspButton" OnClientClick="clearResetPasswordFields();$('#divModalContainer').dialog('close');return false;" />
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </section>
    <div id="divWaitBox">
        <img src="css/images/icons/dark/progress.gif" alt="Loading" />
    </div>
    </form>
    <%--<footer>Footer</footer>--%>
</body>
<script type="text/javascript">
    Sys.Application.add_init(application_init);
    var idOfPostBackElement = "";
    function application_init() {
        //Sys.Debug.trace("Application.Init");
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_initializeRequest);
        prm.add_beginRequest(prm_beginRequest);
        prm.add_endRequest(prm_endRequest);
    }
    function prm_initializeRequest() {

    }
    function prm_beginRequest(sender, args) {
        idOfPostBackElement = args.get_postBackElement().id;
        showWaitModal();
    }
    function prm_endRequest() {
        if (idOfPostBackElement != 'btnReset' && idOfPostBackElement != 'btnCancel') {
            $("select").uniform(); $("input").uniform();
        }
        idOfPostBackElement = "";

    }
    function txtCompanyId_onclick() {

    }

</script>
</html>

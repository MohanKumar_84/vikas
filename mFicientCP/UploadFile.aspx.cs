﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;

namespace mFicientCP
{
    public partial class UploadFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                hdf.Value = Convert.ToString(Utilities.DecryptString(Request.QueryString["id"]).Split(',')[0]);
                hdfCid.Value = Convert.ToString(Utilities.DecryptString(Request.QueryString["id"]).Split(',')[3]);
            }
        }
        protected void btnImportFile_Click(object sender, EventArgs e)
        {
            if (!fupd.HasFile)
            {
                return;
            }

            if (fupd.FileName.Split('.')[1] == "csv")
            {
                //File size grater tha 500kb can not be uploaded.
                if (fupd.PostedFile.ContentLength > (500 * 1024))
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.CommonErrorMessages(9);", true);
                    //lblResult.Text = "File can not be uploaded.File size greater than 500KB."; 
                    return;
                }
                string strContent = "";
                using (StreamReader stRead = new StreamReader(fupd.PostedFile.InputStream))
                {
                    string line1;
                    while ((line1 = stRead.ReadLine()) != null)
                        strContent += "\n" + line1;
                }
                Guid gid;
                gid = Guid.NewGuid();
                WsdlFileContent objWsdlFileContent = new WsdlFileContent(true, gid.ToString(), strContent, "dpOption",hdf.Value,hdfCid.Value);
                objWsdlFileContent.Process();
                if (objWsdlFileContent.StatusCode != 0)
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.CommonErrorMessages(8);", true);
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.ChangeWsdlFileId('" + gid + "');parent.btnImportClick();", true); //parent.SubProcBoxUldWsdlCfm(false);
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), Guid.NewGuid().ToString(), "parent.CommonErrorMessages(10);", true);
                // lblResult.Text = "Only xml or txt file format is allowed.";
            }

        }

    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="adminAppMenu.aspx.cs" Inherits="mFicientCP.adminAppMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script type="text/javascript">
        var idPrependString = { "checkbox": "chkUser", "spanFirst": "lblCatName" };
        var idOfTemplate = { "checkbox": "@checkbox", "spanFirstid": "@spanFirstid", "spanLastid": "@spanLastid", "spanUserNmid": "@spanUserNmid", "a": "@atag", "spanFirstInnerHtml": "@spanFirstInnerHtml", "spanLastInnerHtml": "@spanLastInnerHtml", "spanUsrNmInnerHtml": "@spanUserNmInnerHtml" };
        var templateForGroups = "<div><span id=\"@spanFirstid\">@spanFirstInnerHtml</span>&nbsp;</div><div class=\"clear\"></div>";

        function CategoryDetail(catId, catName) {
            var Category = Object.create(Object.prototype); ;
            Category.catId = catId;
            Category.catName = catName;
            // Finally return the new object
            return Category;
        }
        //this function is used at starting when the html for combo box is formed
        function getMenuCategories() {
            var aryObjCategoryList = new Array();
            var hidMenuCategories = document.getElementById('<%=hidMenuCategories.ClientID %>');
            if (hidMenuCategories.value.toString().length > 0) {
                var aryCategoriesFromPostBack = hidMenuCategories.value.toString().split('|');
                for (var i = 0; i <= aryCategoriesFromPostBack.length - 1; i++) {
                    var aryCategoryIdandName = aryCategoriesFromPostBack[i].split(',');
                    var objCategories = CategoryDetail(aryCategoryIdandName[0], aryCategoryIdandName[1], aryCategoryIdandName[2], aryCategoryIdandName[3]);
                    aryObjCategoryList.push(objCategories);
                }
            }
            return aryObjCategoryList;
        }
        function formTheHTMLForComboBox() {
            var iCountOfLoop = 0;
            var strIdOfUICheckBox, strIdOfUISpan, strIdOfUIAnchor;
            var aryMenuCategories = getMenuCategories();
            if (aryMenuCategories && aryMenuCategories.length > 0) {
                for (var i = 0; i <= aryMenuCategories.length - 1; i++) {
                    var htmlToInsert = templateForGroups;
                    strIdOfCatNameSpan = idPrependString.spanFirst + "_" + iCountOfLoop;
                    strIdOfUIAnchor = idPrependString.a + "_" + aryMenuCategories[i].catId + "_" + aryMenuCategories[i].catName;
                    var finalHtmlToInsert = new String(htmlToInsert.replace(idOfTemplate.checkbox, strIdOfUICheckBox));
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.spanFirstid, strIdOfCatNameSpan);
                    finalHtmlToInsert = finalHtmlToInsert.replace(idOfTemplate.spanFirstInnerHtml, aryMenuCategories[i].catName);
                    $('#divMenuCategories').append(finalHtmlToInsert);
                    iCountOfLoop++;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <div style="margin: auto; width: 500px;">
                    <asp:UpdatePanel ID="updRepeater" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlGroupRptHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblGroupRptHeader" runat="server" Text="<h1>Menu Categories</h1>"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptMenuCategories" runat="server" Visible="true">
                                    <HeaderTemplate>
                                        <table class="repeaterTable rowClickable" style="width: 98%">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem" onclick="rptRowClick(this,0);">
                                                <td style="display: none" title="id">
                                                    <asp:Label ID="lblMenuCatId" runat="server" Text='<%#Eval("MENU_CATEGORY_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblCatNme" runat="server" Text='<%# Eval("CATEGORY") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCatName" runat="server" Text='<%# Eval("CATEGORY") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDisplayIndex" runat="server" Text='<%# Eval("DISPLAY_INDEX") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem" onclick="rptRowClick(this,0);">
                                                <td style="display: none" title="id">
                                                    <asp:Label ID="lblMenuCatId" runat="server" Text='<%#Eval("MENU_CATEGORY_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblCatNme" runat="server" Text='<%# Eval("CATEGORY") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCatName" runat="server" Text='<%# Eval("CATEGORY") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDisplayIndex" runat="server" Text='<%# Eval("DISPLAY_INDEX") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                            <div class="clear">
                            </div>
                            <div id="updRepeaterHidVal">
                                <asp:HiddenField ID="hidMenuCategories" runat="server" />
                                <asp:HiddenField ID="hfdMenuCatId" runat="server" />
                                <asp:HiddenField ID="hidRowValuesUsedAfterPostBack" runat="server" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </section>
    </div>


    <div id="divAddRemoveUsers" style="display: none; width: 200px;">
        <asp:UpdatePanel runat="server" ID="updAddRemoveUsers" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin-bottom: 20px;">
                    <div class="comboselectbox searchable" style="height: 250px; margin-left: 30px;">
                        <div class="comwraper">
                            <div id="divMenuCategories" class="comboRepeater">
                                <div class="comboHeader">
                                    <h5>
                                        Groups</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div style="text-align: center">
                        <asp:Button ID="btnOK" runat="server" Text="OK" OnClick="btnOK_Click" OnClientClick="$('#divAddRemoveUsers').dialog('close');return false;" />&nbsp;&nbsp;
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="$('#divAddRemoveUsers').dialog('close');return false;" />&nbsp;&nbsp;
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div id="divWaitBox" class="waitModal">
            <div id="WaitAnim">
                <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                    AlternateText="Please Wait" BorderWidth="0px" />
            </div>
        </div>
    </div>
    <div>
        <asp:UpdatePanel runat="server" ID="UpdatePanel11" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRowClickPostBack" EventName="Click" />
            </Triggers>
            <ContentTemplate>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div>
        <asp:Button ID="btnRowClickPostBack" runat="server" OnClick="btnRowClickPostBack_Click"
            Width="1px" Height="1px" Style="display: none;" />
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>

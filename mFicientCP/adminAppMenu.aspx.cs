﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientCP
{
    public partial class adminAppMenu : System.Web.UI.Page
    {
        public static DataTable MenuCategoryCollection = new DataTable();
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;
        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!Page.IsPostBack)
            {
                if (hfsValue != string.Empty)
                    bindMenuCategoryRpt(false);
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    bindMenuCategoryRpt(false);
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, String.Empty);
                    return;
                }
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, String.Empty);
            }
        }

        protected void bindMenuCategoryRpt(Boolean _BindByCollection)
        {
            if (_BindByCollection)
            {
                rptMenuCategories.DataSource = MenuCategoryCollection;
                rptMenuCategories.DataBind();
            }
            else
            {
                MenuCategoryCollection = new DataTable();
                GetMenuCategoryNew objGetMenuCategory = new GetMenuCategoryNew(hfsPart3);
                objGetMenuCategory.Process();
                if (objGetMenuCategory.StatusCode == 0)
                {
                    MenuCategoryCollection = objGetMenuCategory.ResultTable;
                    rptMenuCategories.DataSource = objGetMenuCategory.ResultTable;
                    rptMenuCategories.DataBind();
                }
                else
                {
                    DataTable objDataTable = new DataTable();
                    DataColumn dc1 = new DataColumn("MENU_CATEGORY_ID");
                    DataColumn dc2 = new DataColumn("CATEGORY");
                    DataColumn dc3 = new DataColumn("DISPLAY_INDEX");
                    objDataTable.Columns.Add(dc1);
                    objDataTable.Columns.Add(dc2);
                    objDataTable.Columns.Add(dc3);
                    objDataTable.Rows.Add(objDataTable.NewRow());
                    rptMenuCategories.DataSource = objDataTable;
                    rptMenuCategories.DataBind();
                    rptMenuCategories.Items[0].Visible = false;
                }
            }
        }

        protected void fillMenuCategoriesInHiddenField()
        {

            GetGroupDetails objGroupDetails = new GetGroupDetails(hfsPart3);
            objGroupDetails.Process();
            DataTable dtblGroupDetails = objGroupDetails.ResultTable;
            if (dtblGroupDetails != null && dtblGroupDetails.Rows.Count > 0)
            {
                hidMenuCategories.Value = "";
                foreach (DataRow row in dtblGroupDetails.Rows)
                {
                    if (string.IsNullOrEmpty(hidMenuCategories.Value))
                    {
                        hidMenuCategories.Value = Convert.ToString(row["GROUP_ID"]) + "," + Convert.ToString(row["GROUP_NAME"]);
                    }
                    else
                    {
                        hidMenuCategories.Value += "|" + Convert.ToString(row["GROUP_ID"]) + "," + Convert.ToString(row["GROUP_NAME"]);
                    }
                }
            }
        }

        private void categoryList(string catId, string catName)
        {
            GetMenuCategoryDtlsByMenuCatId objGetCatDtls = new GetMenuCategoryDtlsByMenuCatId(hfsPart3, catId);
            objGetCatDtls.Process();
            fillMenuCategoriesInHiddenField();
            updAddRemoveUsers.Update();

            //lblCatNme.Text = catName;
            Utilities.showModalPopup("divAddRemoveUsers", updRepeater, "Menu Category : " + catName, "400", true, false);
            updAddRemoveUsers.Update();
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            Utilities.closeModalPopUp("divAddRemoveUsers", updAddRemoveUsers);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Utilities.closeModalPopUp("divAddRemoveUsers", updAddRemoveUsers);
        }

        private void showTheHTMLComboBoxAfterPostback()
        {
            ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "Html Combo box Rendering", "formTheHTMLForComboBox();$(\"input\").uniform();", true);
        }

        protected void btnRowClickPostBack_Click(object sender, EventArgs e)
        {
            if (hidRowValuesUsedAfterPostBack.Value.Trim().Length != 0)
            {
                string[] aryValuesFromHidField = hidRowValuesUsedAfterPostBack.Value.Split(';');
                categoryList(aryValuesFromHidField[0], aryValuesFromHidField[1]);
                hidRowValuesUsedAfterPostBack.Value = "";
                showTheHTMLComboBoxAfterPostback();
            }

        }
    }
}
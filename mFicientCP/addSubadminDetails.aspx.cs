﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace mFicientCP
{
    public partial class addSubadminDetails : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;

        enum PAGE_MODE
        {
            EDIT,
            NEW_ADDITION,
            LISTING
        }
        enum PanelToShowHide
        {
            Repeater = 0,
            Form = 1
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;
            if (!Page.IsPostBack)
            {
                BindRoleCheckboxList();
                BindSubAdminReapeater(null);
                GetUserDetail(hfsPart3);
                Utilities.makeInputFieldsUniform(this.Page, "Make Uniform On Page Load", false, "doProcessForFormHelp();");
            }
            else
            {
                try
                {
                    if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                    {
                        BindRoleCheckboxList();
                        BindSubAdminReapeater(null);
                        enableDisablePanelsUserNameAndPassword(PAGE_MODE.LISTING);
                        Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, "hideShowPanelForCancel(Panels.Repeater);");
                        return;
                    }
                    Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, String.Empty);
                
                    fillPasswordOnPostBack();
                }
                catch
                { }
                ucSubAdminDetails.OnEditClick += new UserControls.UCSubAdminDetails.ActionClick(ucSubAdminDetails_Edit);
                ucSubAdminDetails.OnBlockClick += new UserControls.UCSubAdminDetails.ActionClick(ucSubAdminDetails_Block);
                ucSubAdminDetails.OnUnBlockClick += new UserControls.UCSubAdminDetails.ActionClick(ucSubAdminDetails_UnBlock);
                ucSubAdminDetails.OnDivDeptEditClick += new UserControls.UCSubAdminDetails.ActionClick(ucSubAdminDetails_DivDeptView);
            }
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "FillPasswordAfterPostBack", "test();", true);
           



        }

        private void GetUserDetail(string CompanyId)
        {
            GetUserDetail objgetdtls1 = new GetUserDetail(CompanyId);
            if (objgetdtls1.StatusCode == 0)
            {
                if (objgetdtls1.ResultTable.Rows.Count > 0)
                {
                    drpmappeduser.Items.Clear();
                    drpmappeduser.DataSource = objgetdtls1.ResultTable;
                    drpmappeduser.DataTextField = "username";
                    drpmappeduser.DataValueField = "USER_ID";
                    drpmappeduser.DataBind();
                    drpmappeduser.Items.Insert(0, new ListItem(" Select User ", "-1"));
                  
                }
            }
        }


        private void  GetUserDetail(string mobileuser,string CompanyId,out bool result)
        {
            if (mobileuser != "-1" && mobileuser != null )
            {
                GetUserDetail objgetdtls1 = new GetUserDetail(mobileuser, CompanyId);
                if (objgetdtls1.StatusCode == 0)
                {
                    if (objgetdtls1.ResultTable.Rows.Count > 0)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                result = false;
            }
        }



        protected void ucSubAdminDetails_Edit(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkEdit = (LinkButton)sender;
                string[] arycommandArgument = lnkEdit.CommandArgument.Split(';');
                processEdit(arycommandArgument[0], arycommandArgument[1], Convert.ToBoolean(arycommandArgument[2]));
                Utilities.closeModalPopUp("divSubAdminDtlsModal", updSubAdminDtlsModal, "CloseSubAdminDetail");

                setArgumentAndCmdNameForEditActionLinks(arycommandArgument[0], arycommandArgument[1], Convert.ToBoolean(arycommandArgument[2]));
                showHideResetPasswordForBlockedUnBlockedUsers(Convert.ToBoolean(arycommandArgument[2]));
                this.updSubAdminDtlsModal.Update();
            }
            catch (Exception ex)
            {
                Utilities.showMessage(ex.Message, updSubAdminDtlsModal, DIALOG_TYPE.Error);
            }
        }

        void ucSubAdminDetails_Block(object sender, EventArgs e)
        {
            processBlockUnblockFormUserControl(sender, "Block");
        }
        void ucSubAdminDetails_UnBlock(object sender, EventArgs e)
        {
            //command argument value userId,userName
            processBlockUnblockFormUserControl(sender, "UnBlock");
        }
        void processBlockUnblockFormUserControl(object sender , string process)
        {
            try
            {
                string[] args=((LinkButton)sender).CommandArgument.Split(';');
                processBlockUnblock(args[0], process, updSubAdminDtlsModal, args[2]);
                Utilities.closeModalPopUp("divSubAdminDtlsModal", updSubAdminDtlsModal);
                enableDisablePanelsUserNameAndPassword(PAGE_MODE.LISTING);
            }
            catch (Exception ex)
            {
                Utilities.showMessage(ex.Message, updSubAdminDtlsModal, DIALOG_TYPE.Error);
                enableDisablePanelsUserNameAndPassword(PAGE_MODE.LISTING);
            }
        }
        private void ucSubAdminDetails_DivDeptView(object sender, EventArgs e)
        {
            //command argument value subadminid
            try
            {
                //bindDivisionDepartmentRepeater();
                //showDivisionDeptRepeaterForEdit(((LinkButton)sender).CommandArgument, true);
                Utilities.showModalPopup("divModalContainer", updSubAdminDtlsModal, "Division / Department", "600", true);
                Utilities.runPostBackScript(@"hideDivisionListTblHead();", updSubAdminDtlsModal, "HideTblHead");
                enableDisablePanelsUserNameAndPassword(PAGE_MODE.LISTING);
            }
            catch (Exception ex)
            {
                Utilities.showMessage(ex.Message, updSubAdminDtlsModal, DIALOG_TYPE.Error);
                if (String.IsNullOrEmpty(hidEditSubAdminId.Value))
                {
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.NEW_ADDITION);
                }
                else
                {
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.EDIT);
                }
            }

        }
        private void fillPasswordOnPostBack()
        {
            try
            {
                
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "FillPasswordAfterPostBack", "fillPasswordEntered();", true);
               
            }
            catch
            {

            }
        }
        private void setArgumentAndCmdNameForEditActionLinks(string subAdminId, string userName, bool isBlocked)
        {
            try
            {
                string strCommandArgument = subAdminId + ";" + userName + ";" + Convert.ToString(isBlocked);
                lnkResetPassword.CommandArgument = strCommandArgument;
                lnkBlockUnblock.CommandArgument = strCommandArgument;
                lnkDelete.CommandArgument = strCommandArgument;
                if (isBlocked)
                {
                    lnkBlockUnblock.Text = "unblock";
                    lnkBlockUnblock.CommandName = "unblock";
                }
                else
                {
                    lnkBlockUnblock.Text = "block";
                    lnkBlockUnblock.CommandName = "block";
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private void showHideResetPasswordForBlockedUnBlockedUsers(bool isBlocked)
        {
            if (isBlocked)
            {
                lnkResetPassword.Visible = false;
                lblSeparatorTwo.Visible = false;
            }
            else
            {
                lnkResetPassword.Visible = true;
                lblSeparatorTwo.Visible = true;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strCompanyId = hfsPart3;

                if (hidEditSubAdminId.Value.Length > 0)
                {
                    UpdateSubDetails(strUserId, strCompanyId, Utilities.DecryptString(hidEditSubAdminId.Value));
                    BindSubAdminReapeater(updRepeater);
                }
                else
                {                   
                    GetSubAdminDetail getsubadmindetail = new
                    GetSubAdminDetail(strUserId, strCompanyId);
                    getsubadmindetail.getSubAdminByUserName(txtUserName.Text);
                    DataTable dt = getsubadmindetail.ResultTable;
                    if (getsubadmindetail.StatusCode == 0)
                    {
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            showAlert("User name already exist.");
                            enableDisablePanelsUserNameAndPassword(PAGE_MODE.NEW_ADDITION);
                            return;
                        }
                        SaveUserDetails(strUserId, strCompanyId);
                        BindSubAdminReapeater(updRepeater);
                    }
                    else
                    {
                        Utilities.showMessage(getsubadmindetail.StatusDescription, updRepeater, DIALOG_TYPE.Error);
                        enableDisablePanelsUserNameAndPassword(PAGE_MODE.NEW_ADDITION);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.showMessage(ex.Message, updRepeater, DIALOG_TYPE.Error);
                if (String.IsNullOrEmpty(hidEditSubAdminId.Value))
                {
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.NEW_ADDITION);
                }
                else
                {
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.EDIT);
                }
            }

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {

            try
            {
                clearControls();
                pnlSubAdminForm.Visible = false;
                pnlRepeaterBox.Visible = true;
            }
            catch (Exception ex)
            {
                Utilities.showMessage(ex.Message, updRepeater, DIALOG_TYPE.Error); 
                if (String.IsNullOrEmpty(hidEditSubAdminId.Value))
                {
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.NEW_ADDITION);
                }
                else
                {
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.EDIT);
                }
            }
        }

        private void SaveUserDetails(string _AdminId, string _CompanyId)
        {
            try
            {
                string strMessage = Validate(_AdminId, false);
                string strRoleId = GetRoleId();
                //string strDivDepId = GetDivisionAndDeptId();
                if (String.IsNullOrEmpty(strRoleId) )
                {
                    if (String.IsNullOrEmpty(strRoleId) )
                    {

                        strMessage += "Please select atleast one role. <br/>";
                    }

                }
                if (!String.IsNullOrEmpty(strMessage))
                {
                    showAlert(strMessage);
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.NEW_ADDITION);
                    return;
                }
                if (drpmappeduser.SelectedIndex > 0)
                {
                    bool result = false;
                    GetUserDetail(drpmappeduser.SelectedValue, hfsPart3, out result);
                    if (result == true)
                    {
                        Utilities.showMessage("Mapped user already exist for any other subadmin.", updRepeater, DIALOG_TYPE.Error);
                        enableDisablePanelsUserNameAndPassword(PAGE_MODE.NEW_ADDITION);
                        return;
                    }
                }
                AddSubAdminDetail addsubadmindetail =
                        new AddSubAdminDetail(_AdminId,
                            txtFirstName.Text, txtUserName.Text,
                            txtPassword.Text, txtEmailId.Text,
                            "", strRoleId,
                            chkPushMessage.Checked ? Convert.ToByte(1) : Convert.ToByte(0),
                            hfsPart3,drpmappeduser.SelectedValue,
                            this.Context);
                addsubadmindetail.Process();
                int intStatusCode = addsubadmindetail.StatusCode;
                string strDescription = addsubadmindetail.StatusDescription;
                if (intStatusCode == 0)
                {

                    Utilities.showMessage("Sub admin added  successfully.", updRepeater, DIALOG_TYPE.Info);
                    Utilities.saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.SUBADMIN_CREATED, hfsPart3, _AdminId, addsubadmindetail.SubAdminId, txtFirstName.Text, "", "", "", "", "", "");
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.LISTING);
              
                    this.setValOfHidPasswordEntered(String.Empty);
                    this.setValOfHidRetypePassEntered(String.Empty);
                    clearControls();
                    hidEditSubAdminId.Value = "";
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.LISTING);
                }
                else
                {
                    Utilities.showMessage("Internal error.Please try again", updRepeater, DIALOG_TYPE.Error);
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.NEW_ADDITION);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        //private string GetDivisionAndDeptId()
        //{
        //    try
        //    {
        //        string str = "";
        //        foreach (RepeaterItem item in rptDivisionList.Items)
        //        {
        //            bool isDivisionSelected = false;
        //            Label lblDivisionId = (Label)item.FindControl("lblDivisionId");
        //            CheckBox chkDivision = (CheckBox)item.FindControl("chkDivision");
        //            CheckBoxList chkDeptList = (CheckBoxList)item.FindControl("chkDepartmentList");
        //            foreach (ListItem deptItem in chkDeptList.Items)
        //            {
        //                if (!(deptItem == chkDeptList.Items[0]))//the first one all
        //                {
        //                    if (deptItem.Selected)
        //                    {
        //                        str += "#" + deptItem.Value;
        //                        isDivisionSelected = true;
        //                    }
        //                }
        //            }
        //            if (isDivisionSelected)
        //            {
        //                if (!String.IsNullOrEmpty(lblDivisionId.Text))
        //                {
        //                    str += "@" + lblDivisionId.Text + "&";
        //                }
        //            }
        //        }
        //        return str;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        protected void UpdateSubDetails(string _AdminId, string _CompanyId, string _SubAdminId)
        {
            try
            {
                string strMessage = Validate(_AdminId, true);
                string strRoleId = GetRoleId();
                //string strDivDepId = GetDivisionAndDeptId();
                if (String.IsNullOrEmpty(strRoleId) )
                {
                    if (String.IsNullOrEmpty(strRoleId) )
                    {
                        strMessage += "Please select atleast one role. <br/>";
                    }
                }
                if (!String.IsNullOrEmpty(strMessage))
                {
                    showAlert(strMessage);
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.EDIT);
                    return;
                }
                if (drpmappeduser.SelectedIndex > 0)
                {
                    bool result = false;
                    if (hidmapped.Value == drpmappeduser.SelectedValue) result = false;
                    else GetUserDetail(drpmappeduser.SelectedValue, hfsPart3, out result);
                    if (result == true)
                    {
                        Utilities.showMessage("Mapped user already exist for any other subadmin.", updRepeater, DIALOG_TYPE.Error);
                        enableDisablePanelsUserNameAndPassword(PAGE_MODE.EDIT);
                        return;
                    }
                }
                UpdateSubadminDetails updatesubadmindetails = new UpdateSubadminDetails(_SubAdminId, _AdminId, txtFirstName.Text, txtUserName.Text, txtPassword.Text, txtEmailId.Text, "", strRoleId, chkPushMessage.Checked ? Convert.ToByte(1) : Convert.ToByte(0), hfsPart3,drpmappeduser.SelectedValue);
                updatesubadmindetails.Process();
                if (updatesubadmindetails.StatusCode == 0)
                {
                    Utilities.saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.SUBADMIN_MODIFIED, hfsPart3, _AdminId, _SubAdminId, txtFirstName.Text, "", "", "",  "", "", "");
                    
                    this.setValOfHidPasswordEntered(String.Empty);
                    this.setValOfHidRetypePassEntered(String.Empty);
                    Utilities.showMessage("Sub admin details updated successfully.", updRepeater, DIALOG_TYPE.Info);
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.LISTING);
                    hidEditSubAdminId.Value = "";
                    hidmapped.Value = "";
                    clearControls();
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private string Validate(string _AdminId, Boolean IsEdit)
        {
            try
            {
                string strMessage = "";

                if (!Utilities.IsValidString(txtFirstName.Text, true, true, false, "- ", 1, 50, false, false))
                {
                    strMessage += "Please enter valid name." + "<br />";
                }
                if (!Utilities.IsValidEmail(txtEmailId.Text) || txtEmailId.Text.Length > 50)
                {
                    strMessage += "Please enter valid email." + "<br />";
                }
                if (!Utilities.isValidUserName(txtUserName.Text, 50, false))
                {
                    strMessage += "Please enter valid username." + "<br />";
                   
                }
                else
                {

                    if (IsUserNameExists(txtUserName.Text, _AdminId))
                    {
                        if (IsEdit)
                        {

                        }
                        else
                        {
                            strMessage += "Username exists for another user." + "<br />";
                        }
                    }
                }
                if (!Utilities.IsValidString(txtPassword.Text, true, true, true, @"!@#$%^&*()_-", 6, 50, false, false))
                {
                    if (IsEdit)
                    {
                    }
                    else
                    {
                        strMessage += "Please enter valid password." + "<br />";
                    }
                }
                if (hidEditSubAdminId.Value == "")
                {
                    if (txtPassword.Text != txtRetypePassword.Text)
                    {
                        strMessage += "Password entered does not match." + "<br />";
                    }
                }
                return strMessage;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        private void BindRoleCheckboxList()
        {
            try
            {
                GetRoleList getrolelist = new GetRoleList();
                getrolelist.Process();
                if (getrolelist.StatusCode == 0)
                {
                    DataTable dt = getrolelist.ResultTable;
                    chkRoles.DataSource = dt;
                    chkRoles.DataTextField = "ROLE_NAME";
                    chkRoles.DataValueField = "ROLE_ID";
                    chkRoles.DataBind();
                    int intStatusCode = getrolelist.StatusCode;
                    string strDescription = getrolelist.StatusDescription;
                }
                else
                {
                    Utilities.showMessage(getrolelist.StatusDescription, updRepeater, DIALOG_TYPE.Error);
                }
            }
            catch
            {
                //throw ex;
            }
        }

        private Boolean IsUserNameExists(string _UserName, string _AdminId)
        {
            try
            {
                Boolean bolUserExist = false;
                GetSubAdminDetail getsubadmindetail =
                    new GetSubAdminDetail(_AdminId, hfsPart3);
                getsubadmindetail.getSubAdminByUserName(_UserName);
                DataTable dt = getsubadmindetail.ResultTable;
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {

                        bolUserExist = true;
                    }
                }

                return bolUserExist;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetRoleId()
        {
            try
            {
                string strRoleId = "";
                for (int index = 0; index < chkRoles.Items.Count; index++)
                {
                    if (chkRoles.Items[index].Selected)
                    {
                        strRoleId += chkRoles.Items[index].Value + "&";
                    }
                }
                return strRoleId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ddlDivision_SelectedIndexChanged(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "testing", " $(\"select\").uniform();$(\"input\").uniform();", true);
        }

        private void BindSubAdminReapeater(UpdatePanel updPanel)
        {
            try
            {
               
                GetSubAdminDetail getsubadmindetail = new GetSubAdminDetail(strUserId, hfsPart3);
                getsubadmindetail.getAllSubAdminsOfCmpByAdminId();
                DataTable dt = getsubadmindetail.ResultTable;
                if (getsubadmindetail.StatusCode == 0 && dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        string filter = "";
                        if (chkShowBlockedUser.Checked)
                        {
                            filter = String.Format("IS_BLOCKED = '{0}'", true);
                        }
                        else
                        {
                            filter = String.Format("IS_BLOCKED = '{0}'", false);
                        }
                        DataRow[] rows = dt.Select(filter);
                        if (rows.Length > 0)
                        {
                            dt = rows.CopyToDataTable<DataRow>();
                            rptSubAdminDtls.DataSource = dt;
                            rptSubAdminDtls.DataBind();
                            showRptHdrInfoAndEnableDisableAdd("Sub Admin", true);
                            rptSubAdminDtls.Visible = true;
                        }
                        else
                        {
                            if (chkShowBlockedUser.Checked)
                            {
                                showRptHdrInfoAndEnableDisableAdd("There are no blocked sub admin.", true);
                            }
                            else
                            {
                                showRptHdrInfoAndEnableDisableAdd("There are no unblocked sub admin.", true);
                            }
                            rptSubAdminDtls.DataSource = null;
                            rptSubAdminDtls.DataBind();
                            rptSubAdminDtls.Visible = false;
                        }

                    }
                    else
                    {
                        showRptHdrInfoAndEnableDisableAdd("No sub admin is added yet.", true);
                        rptSubAdminDtls.DataSource = null;
                        rptSubAdminDtls.DataBind();
                        rptSubAdminDtls.Visible = false;
                    }
                }
                else
                {
                    if (updPanel == null)
                        Page.ClientScript.RegisterStartupScript(this.GetType()
                            , this.ClientID
                            , @"$.alert(" + "'" + "Internal server error" + "'" + "," + "$.alert.Error" + ");showDialogImage(" + "DialogType.Error" + ");"
                            , true);
                    else
                        Utilities.showMessage("Internal server error", updRepeater, DIALOG_TYPE.Error);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void chkShowBlockedUser_CheckedChanged(object sender, EventArgs e)
        {
            BindSubAdminReapeater(updRepeater);
            enableDisablePanelsUserNameAndPassword(PAGE_MODE.LISTING);
        }
        private void showRptHdrInfoAndEnableDisableAdd(string headerInfo, bool enableAddButton)
        {
            lblHeaderInfo.Text = "<h1>" + headerInfo + "</h1>";
            if (enableAddButton)
                lnkAddNewSubAdmin.Visible = true;
            else
                lnkAddNewSubAdmin.Visible = false;
        }
        protected void imgAddNewRow_Click(object sender, EventArgs e)
        {
            clearControls();
            hidEditSubAdminId.Value = "";
            txtUserName.Enabled = true;
        }

        private void ShowSubadminDetails(string _AdminId, string _UserName)
        {
            try
            {
                GetSubAdminDetail getsubadmindetail = new GetSubAdminDetail(_AdminId, hfsPart3);
                getsubadmindetail.getSubAdminByUserName(_UserName);
                DataTable dt = getsubadmindetail.ResultTable;
                if (getsubadmindetail.StatusCode == 0)
                {
                    if (dt.Rows.Count > 0)
                    {
                        clearControls();

                        txtFirstName.Text = Convert.ToString(dt.Rows[0]["FULL_NAME"]);
                        txtEmailId.Text = Convert.ToString(dt.Rows[0]["EMAIL"]);
                        txtUserName.Text = Convert.ToString(dt.Rows[0]["USER_NAME"]);
                        FillRolesCheckBoxList(_AdminId, Convert.ToString(dt.Rows[0]["SUBADMIN_ID"]));
                        //FillDivisionDropDown(Convert.ToString(dt.Rows[0]["SUBADMIN_ID"]));
                        txtUserName.Text = Convert.ToString(dt.Rows[0]["USER_NAME"]);
                        if (Convert.ToByte(dt.Rows[0]["PUSH_MESSAGE"]) == 1)
                        {
                            chkPushMessage.Checked = true;
                        }
                        else
                        {
                            chkPushMessage.Checked = false;
                        }
                        //bindDivisionDepartmentRepeater();
                        if (String.IsNullOrEmpty(hidProcess.Value))
                        {
                            //showDivisionDeptRepeaterForEdit(Convert.ToString(dt.Rows[0]["SUBADMIN_ID"]), false);
                        }
                        enableDisablePanelsUserNameAndPassword(PAGE_MODE.EDIT);
                        lblHeaderUserName.Text = "( " + Convert.ToString(dt.Rows[0]["USER_NAME"]) + " " + ")";
                        lblHeaderUserName.Visible = true;
                        object obj=Convert.ToString(dt.Rows[0]["MOBILE_USER"]);
                        string strresult = obj.ToString();

                        if (!String.IsNullOrEmpty(strresult))
                        {
                            drpmappeduser.SelectedValue = Convert.ToString(dt.Rows[0]["MOBILE_USER"]);
                            hidmapped.Value = Convert.ToString(dt.Rows[0]["MOBILE_USER"]);
                        }
                    }
                }
                else
                {
                    Utilities.showMessage(getsubadmindetail.StatusDescription, updSubAdminDtlsModal, DIALOG_TYPE.Error);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //private void FillDivisionDropDown(string _SubAdminId)
        //{
        //    try
        //    {
        //        GetSubAdminDivision getsubadmindivision = new GetSubAdminDivision(_SubAdminId, "");
        //        getsubadmindivision.Process();
        //        DataTable dt = getsubadmindivision.ResultTable;
        //        if (dt != null)
        //        {
        //            if (dt.Rows.Count > 0)
        //            {
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        private void clearControls()
        {
            txtFirstName.Text = "";
            txtEmailId.Text = "";
            txtUserName.Text = "";
            txtPassword.Text = "";
            BindRoleCheckboxList();
            chkPushMessage.Checked = false;
            hidProcess.Value = "";
            drpmappeduser.SelectedIndex = 0;
        }

        protected void DeleteSubAdmin(string _AdminId, string _UserName)
        {
            try
            {
               
                GetSubAdminDetail getsubadmindetail = new GetSubAdminDetail(_AdminId, hfsPart3);
                getsubadmindetail.getSubAdminByUserName(_UserName);
                DataTable dt = getsubadmindetail.ResultTable;
                if (getsubadmindetail.StatusCode == 0)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        DeleteSubAdmin deletesubadmin = new DeleteSubAdmin(_AdminId, Convert.ToString(dt.Rows[0]["SUBADMIN_ID"]));
                        deletesubadmin.Process();
                        if (deletesubadmin.StatusCode == 0)
                        {
                            BindSubAdminReapeater(updRepeater);
                            Utilities.saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.SUBADMIN_DELETE, hfsPart3, _AdminId, Convert.ToString(dt.Rows[0]["SUBADMIN_ID"]), Convert.ToString(dt.Rows[0]["FULL_NAME"]), "", "", "", "", "", "");
                    
                            Utilities.showMessage("Sub Admin deleted successfully", updRepeater, DIALOG_TYPE.Info);
                        }
                        else
                        {
                            Utilities.showMessage("Internal server error", updRepeater, DIALOG_TYPE.Error);

                        }
                    }
                }
                else
                {
                    Utilities.showMessage(getsubadmindetail.StatusDescription, updRepeater, DIALOG_TYPE.Error);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void FillRolesCheckBoxList(string _AdminId, string _SubAdminId)
        {
            try
            {
                GetSubAdminRoles getsubadminroles = new GetSubAdminRoles(_AdminId, _SubAdminId);
                getsubadminroles.Process();
                DataTable dt = getsubadminroles.ResultTable;
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (ListItem li in chkRoles.Items)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (Convert.ToString(dr["ROLE_ID"]) == li.Value)
                                {
                                    li.Selected = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        protected void lnkAddSubAdmin_Click(object sender, EventArgs e)
        {
            clearControls();
            hidEditSubAdminId.Value = "";
            txtUserName.Enabled = true;
        }


        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#formDiv'" + ");", true);
        }

        protected void lnkAddNewSubAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                clearControls();
                hidEditSubAdminId.Value = "";
                txtUserName.Enabled = true;
                lblHeaderUserName.Visible = false;
                //bindDivisionDepartmentRepeater();
                enableDisablePanelsUserNameAndPassword(PAGE_MODE.NEW_ADDITION);
                this.setValOfHidPasswordEntered(String.Empty);
                this.setValOfHidRetypePassEntered(String.Empty);
            }
            catch (Exception ex)
            {
                Utilities.showMessage(ex.Message, updRepeater, DIALOG_TYPE.Error);
            }
        }

        private void enableDisablePanelsUserNameAndPassword(PAGE_MODE pageMode)
        {

            switch (pageMode)
            {
                case PAGE_MODE.EDIT:
                    pnlRepeaterBox.Visible = true;
                    pnlSubAdminForm.Visible = true;
                    lnkResetPassword.Visible = true;
                    pnlPassword.Visible = false;
                    pnlRetypePassword.Visible = false;
                    pnlUserName.Visible = false;
                    hideShowHeaderActionButtons(PAGE_MODE.EDIT);
                    showHidePanelUsingJavascript(PanelToShowHide.Form, updRepeater);
                    break;
                case PAGE_MODE.NEW_ADDITION:
                    pnlRepeaterBox.Visible = true;
                    pnlSubAdminForm.Visible = true;
                    lnkResetPassword.Visible = false;
                    pnlPassword.Visible = true;
                    pnlRetypePassword.Visible = true;
                    pnlUserName.Visible = true;
                    hideShowHeaderActionButtons(PAGE_MODE.NEW_ADDITION);
                    showHidePanelUsingJavascript(PanelToShowHide.Form, updRepeater);
                    break;
                case PAGE_MODE.LISTING:
                    clearControls();
                    pnlRepeaterBox.Visible = true;
                    pnlSubAdminForm.Visible = true;
                    showHidePanelUsingJavascript(PanelToShowHide.Repeater, updRepeater);
                    break;

            }

        }
        private void hideShowHeaderActionButtons(PAGE_MODE pageMode)
        {
            switch (pageMode)
            {
                case PAGE_MODE.NEW_ADDITION:
                    lnkDelete.Visible = false;
                    lnkBlockUnblock.Visible = false;
                    lnkResetPassword.Visible = false;
                    lblSeparatorOne.Visible = false;
                    lblSeparatorTwo.Visible = false;
                    break;
                case PAGE_MODE.EDIT:
                    lnkDelete.Visible = true;
                    lnkBlockUnblock.Visible = true;
                    lnkResetPassword.Visible = true;
                    lblSeparatorOne.Visible = true;
                    lblSeparatorTwo.Visible = true;
                    break;
            }
        }

        void showHidePanelUsingJavascript(PanelToShowHide panelName, UpdatePanel updPanel)
        {
            string strScript = "";
            switch (panelName)
            {
                case PanelToShowHide.Repeater:
                    strScript = "hideShowPanelForCancel(" + (int)PanelToShowHide.Repeater + ")";
                    break;
                case PanelToShowHide.Form:
                    strScript = "hideShowPanelForCancel(" + (int)PanelToShowHide.Form + ")";
                    break;
            }
            if (!(updPanel == null))
            {
                Utilities.runPostBackScript(strScript, updPanel, "Show Hide Panel");
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "onload", strScript, true);
            }
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton delete = (LinkButton)sender;
                //command argument has subAdminId,userName
                processDelete(delete.CommandArgument.Split(';')[0], delete.CommandArgument.Split(';')[1]);
                BindSubAdminReapeater(updRepeater);
                enableDisablePanelsUserNameAndPassword(PAGE_MODE.LISTING);
            }
            catch (Exception ex)
            {
                Utilities.showMessage(ex.Message, updRepeater, DIALOG_TYPE.Error);
            }
        }
        protected void lnkBlockUnblock_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton blockUnblock = (LinkButton)sender;
                if (blockUnblock.CommandName.ToLower() == "block")
                {
                    processBlockUnblock(blockUnblock.CommandArgument.Split(';')[0], "block", updRepeater,txtFirstName.Text);
                }
                else
                {
                    processBlockUnblock(blockUnblock.CommandArgument.Split(';')[0], "unblock", updRepeater, txtFirstName.Text);
                }
                BindSubAdminReapeater(updRepeater);
                enableDisablePanelsUserNameAndPassword(PAGE_MODE.LISTING);
            }
            catch (Exception ex)
            {
                Utilities.showMessage(ex.Message, updRepeater, DIALOG_TYPE.Error);

                if (String.IsNullOrEmpty(hidEditSubAdminId.Value))
                {
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.LISTING);
                }
                else
                {
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.EDIT);
                }
            }
        }
        //protected void btnOk_Click(object sender, EventArgs e)
        //{
        //    Utilities.closeModalPopUp("divModalContainer", updModalContainer, "CloseDivDeptModal");
        //}

        private void processEdit(string subAdminId, string userName, bool isBlocked)
        {
            try
            {
                hidEditSubAdminId.Value = Utilities.EncryptString(subAdminId);
                ShowSubadminDetails(strUserId, userName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void processDelete(string subAdminId, string userName)
        {
            try
            {
                DeleteSubAdmin(strUserId, userName);
                BindSubAdminReapeater(updRepeater);
                enableDisablePanelsUserNameAndPassword(PAGE_MODE.LISTING);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void processBlockUnblock(string subAdminId, string blockOrUnblock, UpdatePanel updPanelUpdated,string subAdminName)
        {
            try
            {
                BlockUnblockSubAdmin objBlockUnblock = new BlockUnblockSubAdmin(strUserId, subAdminId, blockOrUnblock);
                objBlockUnblock.Process();
                if (objBlockUnblock.StatusCode == 0)
                {
                    BindSubAdminReapeater(updRepeater);
                    mFicientCommonProcess.ACTIVITYENUM process = mFicientCommonProcess.ACTIVITYENUM.SUBADMIN_BLOCKED;
                    if (blockOrUnblock.ToLower() == "block")
                    {
                        Utilities.showMessage("User blocked successfully", updPanelUpdated, DIALOG_TYPE.Info);
                    }
                    else
                    {
                        process = mFicientCommonProcess.ACTIVITYENUM.SUBADMIN_UNBLOCK;
                        Utilities.showMessage("User Unblocked successfully", updPanelUpdated, DIALOG_TYPE.Info);
                    }
                    Utilities.saveActivityLog(null, process, hfsPart3, strUserId, subAdminId, subAdminName, "", "", "", "", "", "");
                    enableDisablePanelsUserNameAndPassword(PAGE_MODE.LISTING);
                }
                else
                {
                    Utilities.showMessage("Internal errror.Please try again", updPanelUpdated, DIALOG_TYPE.Error);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void rptSubAdminDtls_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Label lblSubAdminName = (Label)e.Item.FindControl("lblSubAdminFullName");
                    if (!(bool)DataBinder.Eval(e.Item.DataItem, "IS_BLOCKED"))
                    { }
                    else
                    {
                        lblSubAdminName.ForeColor = System.Drawing.Color.Red;
                        lblSubAdminName.ToolTip = "Blocked";
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.showMessage(ex.Message, updRepeater, DIALOG_TYPE.Error);
            }
        }

        protected void lnkResetPassword_Click(object sender, EventArgs e)
        {
            try
            {
                //command argument of this button is set in the edit in the user cotrol pop up
                LinkButton lnkResetPassword =
                    (LinkButton)sender;
                ResetPasword obj = new ResetPasword(
                   lnkResetPassword.CommandArgument.Split(';')[1],
                   hfsPart3, this.Context);
                //obj.UserName = lnkResetPassword.CommandArgument.Split(';')[1];
                //obj.CompanyID = hfsPart3;
                obj.Process();
                if (obj.StatusCode == 0)
                {
                    Utilities.showMessage("Password has been reset.", updRepeater, DIALOG_TYPE.Info);
                }
                else
                {
                    Utilities.showMessage("Invalid company name or username.", updRepeater, DIALOG_TYPE.Error);
                }
                enableDisablePanelsUserNameAndPassword(PAGE_MODE.EDIT);
            }
            catch (Exception ex)
            {
                Utilities.showMessage(ex.Message, updRepeater, DIALOG_TYPE.Error);
            }
        }

        protected void lnkDivDeptOfSubAdmin_Click(object sender, EventArgs e)
        {
            Utilities.showModalPopup("divModalContainer", updRepeater, "Division / Department", "600", true);
        }
        protected void btnRowClickPostBack_Click(object sender, EventArgs e)
        {
            try
            {
                if (hidRowValuesUsedAfterPostBack.Value.Trim().Length != 0)
                {
                    string[] detail = Utilities.DecryptString(hfsValue).Split(',');
                    string[] aryValuesFromHidField = hidRowValuesUsedAfterPostBack.Value.Split(';');
                    ucSubAdminDetails.SubAdminId = aryValuesFromHidField[0];
                    ucSubAdminDetails.AdminId = detail[0];
                    ucSubAdminDetails.IsBlocked = Convert.ToBoolean(aryValuesFromHidField[1]);
                    ucSubAdminDetails.CompanyID = detail[2];
                    ucSubAdminDetails.rememberSubAdminId();
                    ucSubAdminDetails.Mobileuser = "";
                    ucSubAdminDetails.getSubAdminDetailsAndFillData();
                    Utilities.showModalPopup("divSubAdminDtlsModal", updRepeater, "User Detail", Convert.ToString(Utilities.getModalPopUpWidth(MODAL_POP_UP_NAME.SUB_ADMIN_DETAILS)), true);
                    updSubAdminDtlsModal.Update();
                    hidRowValuesUsedAfterPostBack.Value = "";
                    showHidePanelUsingJavascript(PanelToShowHide.Repeater, updRepeater);
                }
            }
            catch
            {

            }
        }
        void setValOfHidRetypePassEntered(string value)
        {
            hidReTypePasswordEntered.Value = value;
        }
        void setValOfHidPasswordEntered(string value)
        {
            hidPasswordEntered.Value = value;
        }


    }
}
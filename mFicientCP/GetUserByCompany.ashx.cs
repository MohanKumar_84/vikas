﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
namespace mFicientCP
{
    /// <summary>
    /// Summary description for GetUserByCompany
    /// </summary>
    public class GetUserByCompany : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string strCompanyId = context.Request.QueryString.Get(1);
            //string strFilter = context.Request.QueryString.Get(1);
            string strJson = "";
            //GetUserDetail objUserDetail = new GetUserDetail(false, true, false, strCompanyId, context.Request.QueryString.Get(0));
            //objUserDetail.Process();
            GetUserDetail objUserDetail = new GetUserDetail();
            List<MFEMobileUser> lstMobileUsers = objUserDetail.getUserDtlsByName(context.Request.QueryString.Get(0), strCompanyId);
            if (lstMobileUsers != null && lstMobileUsers.Count > 0)
            {
                List<UserDetailMembersForAutoCmptTextBox> lstUserDetailResponse = new List<UserDetailMembersForAutoCmptTextBox>();
                foreach (MFEMobileUser user in lstMobileUsers)
                {

                    UserDetailMembersForAutoCmptTextBox objUserDetailMembers = new UserDetailMembersForAutoCmptTextBox();
                    objUserDetailMembers.id = user.UserId;
                    objUserDetailMembers.name = user.FirstName+" "+user.LastName;
                    objUserDetailMembers.fnm = user.FirstName;
                    objUserDetailMembers.lnm = user.LastName;
                    lstUserDetailResponse.Add(objUserDetailMembers);
                }
                UserDetailResponseForAutoCompleteTextBox objUserDetailResponse = new UserDetailResponseForAutoCompleteTextBox();
                objUserDetailResponse.data = lstUserDetailResponse;
                strJson = Utilities.SerializeJson<UserDetailResponseForAutoCompleteTextBox>(objUserDetailResponse);
                strJson = Utilities.getFinalJsonForAutoCompleteTextBox(strJson);
            }
            //DataTable dtblUserDetail = objUserDetail.ResultTable;
            //if (dtblUserDetail != null && dtblUserDetail.Rows.Count > 0)
            //{
            //    List<UserDetailMembersForAutoCmptTextBox> lstUserDetailResponse = new List<UserDetailMembersForAutoCmptTextBox>();
            //    foreach (DataRow row in dtblUserDetail.Rows)
            //    {

            //        UserDetailMembersForAutoCmptTextBox objUserDetailMembers = new UserDetailMembersForAutoCmptTextBox();
            //        objUserDetailMembers.id = Convert.ToString(row["USER_ID"]);
            //        objUserDetailMembers.name = Convert.ToString(row["USER_NAME"]);
            //        lstUserDetailResponse.Add(objUserDetailMembers);
            //    }
            //    UserDetailResponseForAutoCompleteTextBox objUserDetailResponse = new UserDetailResponseForAutoCompleteTextBox();
            //    objUserDetailResponse.data = lstUserDetailResponse;
            //    strJson = Utilities.SerializeJson<UserDetailResponseForAutoCompleteTextBox>(objUserDetailResponse);
            //    strJson = Utilities.getFinalJsonForAutoCompleteTextBox(strJson);
            //}
            context.Response.ContentType = "text/json";
            context.Response.Write(strJson);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    
}
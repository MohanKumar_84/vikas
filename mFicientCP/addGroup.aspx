﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="addGroup.aspx.cs" Inherits="mFicientCP.addGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="upd" runat="server">
        <ContentTemplate>
            <div id="PageCanvasContent">
                <div>
                    <asp:Label ID="Label1" runat="server" Text="<strong>GROUP NAME :</strong>"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtGroup" runat="server" Width="150"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:LinkButton ID="lnkBackToList" runat="server" Text="back to list" ToolTip="Group List"
                        PostBackUrl="~/groupList.aspx" OnClientClick="isCookieCleanUpRequired('false');showWaitModal();"></asp:LinkButton>
                </div>
                <br />
                <div id="divRepeater">
                    <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                        <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                            <div>
                                <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>Add Group</h1>"></asp:Label>
                            </div>
                        </asp:Panel>
                        <asp:Repeater ID="rptUsers" runat="server"  >
                            <HeaderTemplate>
                                <table class="repeaterTable">
                                    <thead>
                                        <tr>
                                            <th>
                                                Username
                                            </th>
                                            <th>
                                                Name
                                            </th>
                                            <th>
                                                Email
                                            </th>
                                            <th>
                                            </th>
                                        </tr>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tbody>
                                    <tr class="repeaterItem">
                                        <td>
                                            <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                            <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("USER_ID") %>' Visible="false"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("FIRST_NAME") %>'></asp:Label>
                                            <asp:Label ID="lblLastName" runat="server" Text='<%# Eval("LAST_NAME") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("EMAIL_ID") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkUser" runat="server"></asp:CheckBox>
                                        </td>
                                    </tr>
                                </tbody>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tbody>
                                    <tr class="repeaterAlternatingItem">
                                        <td>
                                            <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                            <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("USER_ID") %>' Visible="false"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("FIRST_NAME") %>'></asp:Label>&nbsp;
                                            <asp:Label ID="lblLastName" runat="server" Text='<%# Eval("LAST_NAME") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("EMAIL_ID") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkUser" runat="server"></asp:CheckBox>
                                        </td>
                                    </tr>
                                </tbody>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        
                    </asp:Panel>
                </div>
                <br />
                <div style="float: right;">
                    <asp:Button ID="btnAdd" runat="server" Text="Add Users" OnClick="btnAdd_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait"  BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>

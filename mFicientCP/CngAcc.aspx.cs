﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace mFicientCP
{
    public partial class CngAcc : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;
        }

        void showPasswordModalPopUp(string modalPopUpHeader)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "ShowPopUp", @"showModalPopUp('divChangePasswordContainer','" + modalPopUpHeader + "',400);", true);
        }

        protected void BttnChangePwd_Click(object sender, EventArgs e)
        {
            string strFieldBorder = "";
            string strMessage = Validate();
            if (!String.IsNullOrEmpty(strMessage))
            {
                ShowErrors(strMessage, strFieldBorder);
            }
            else
            {
                changeAccesscode obj = new changeAccesscode();
                obj.UserId = strUserId;

                obj.process();
                if (obj.StatusCode > 0)
                {
                    ShowErrors("Try Again", "");
                }
                else
                {
                    ShowErrors("Password Changed Successfully", "");

                }
            }
        }
        public void ShowErrors(string _Message, string _FieldBorder)
        {
            string strColor = @"inherit";
            string strAlign = @"left";
            string TextMessage = _Message;

            string FieldBorder = @"";
            FieldBorder += _FieldBorder;

            if (strColor == @"inherit")
            {
                strColor = @"";
            }

            string ScriptToRun = @"showWait(false);showProcessing(true, '" + TextMessage + @"', '" + strColor + @"', '" + strAlign + @"');showFormErrors('" + FieldBorder + @"');";



        }

        public string Validate()
        {
            string strMessage = "";
            if (!Utilities.IsValidString(txtNewPassword.Text, true, true, true, @".,#%^&*:;()[]{}?/\+=-_|~@", 6, 64, false, false))
            {
                strMessage += "* Please enter valid password." + "<br />";
            }
            else if (txtNewPassword.Text != txtReEnterPassword.Text)
            {
                strMessage += "* Please Re-enter same password." + "<br />";
            }
            return strMessage;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string strMessage = Validate();
            if (!String.IsNullOrEmpty(strMessage))
            {

                showAlert(strMessage);
            }
            else
            {
                changeAccesscode obj = new changeAccesscode();
                obj.UserId = strUserId;
                obj.OldAccessCode = txtOldPassword.Text;
                obj.NewAccessCode = txtNewPassword.Text;
                obj.process();
                if (obj.StatusCode > 0)
                {

                    Utilities.showMessage("Password could not be changed.Please try again.", UpdatePanel1, DIALOG_TYPE.Error);
                }
                else
                {
                    Utilities.showMessage("Password Changed Successfully", UpdatePanel1, DIALOG_TYPE.Info);
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#formDiv'" + ");", true);
        }


    }
}
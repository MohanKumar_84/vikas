﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientCP
{
    public partial class registeredDeviceList : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;

        enum REPEATER_NAME
        {
            RegisteredDevices = 1,
            PendingDevices = 2,
            ModalRegisteredDevices = 3,
            ModalPendingDevices = 4,
        }
        enum DEVICE_DETAILS_SHOW_PURPOSE
        {
            Processing,//when delete and approve button is clicked in pending request repeater
            Information//when we have to just show the device information
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;


            if (!Page.IsPostBack)
            {

                if (hfsValue != string.Empty)
                {
                    #region setting the sort of repeater first time
                    initialiseCurrentSortOrder(REPEATER_NAME.RegisteredDevices);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Sort Repeater", "changeImageOfSortedCol();clearSortingDtlHiddenField();", true);
                    #endregion
                    bindRegisteredDevicesRepeater();
                    bindSubAdminDropDown();
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "$(\"input\").uniform();", true);
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, "$(\"#content\").find('div.widgets').wl_Widget();makeTabAfterPostBack();$('#aspnetForm').wl_Form();");
                    return;
                    // ScriptManager.RegisterStartupScript(Page, typeof(Page), "testing", "$(\"input\").uniform();$(\"#content\").find('div.widgets').wl_Widget();makeTabAfterPostBack();$('#aspnetForm').wl_Form();", true);
                    //return;
                }
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, "$(\"#content\").find('div.widgets').wl_Widget();makeTabAfterPostBack();$('#aspnetForm').wl_Form();");
                //ScriptManager.RegisterStartupScript(Page, typeof(Page), "testing", "$(\"input\").uniform(); $(\"select\").uniform();$(\"#content\").find('div.widgets').wl_Widget();makeTabAfterPostBack();$('#aspnetForm').wl_Form();", true);
            }

            try
            {
                if (!String.IsNullOrEmpty(hfsValue))
                    CompanyTimezone.getTimezoneInfo(hfsPart3);
            }
            catch
            { }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headerInfo"></param>
        /// <param name="repeaterName">Pass "Device" for registered device repeater else empty</param>
        private void showRepeaterHeaderInfo(string headerInfo, string repeaterName)
        {
            if (repeaterName == "Device")
            {
                lblHeaderInfo.Text = "<h1>" + headerInfo + "</h1>";
            }
            else
            {

            }
        }

        private void bindSubAdminDropDown()
        {
            //GetSubAdminDetail objSubAdminDtls = new GetSubAdminDetail(true, false, hfsPart4, String.Empty, String.Empty, hfsPart3);
            //objSubAdminDtls.Process();
            GetSubAdminDetail objGetSubAdminDtls =
                new GetSubAdminDetail(hfsPart4, hfsPart3);
            objGetSubAdminDtls.getAllSubAdminsOfCmpByAdminId();
            DataTable dtblSubAdminDtls = objGetSubAdminDtls.ResultTable;
            if (objGetSubAdminDtls.StatusCode == 0)
            {
                if (dtblSubAdminDtls != null)
                {
                    if (dtblSubAdminDtls.Rows.Count > 0)
                    {
                        ddlSubAdmin.DataSource = dtblSubAdminDtls;
                        ddlSubAdmin.DataValueField = "SUBADMIN_ID";
                        ddlSubAdmin.DataTextField = "FULL_NAME";
                        ddlSubAdmin.DataBind();
                    }
                }
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, @"$.alert(" + "'" + "Internal server error.Could not load the subadmins" + "'" + "," + "$.alert.Information" + ");showDialogImage(" + "DialogType.Info" + ");", true);
            }
        }

        protected void ddlSubAdmin_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubAdmin.SelectedValue == "0")
            {
                if (radListSelection.SelectedValue == "1")
                {
                    bindRegisteredDevicesRepeater();
                }
                else
                {
                    bindPendingRequestRepeater();
                }
            }
            else
            {
                if (radListSelection.SelectedValue == "1")
                {
                    bindRegisteredDevicesRepeaterBySubAdminId();
                }
                else
                {
                    bindPendingDevicesRequestBySubAdminId();
                }
            }
        }
        private void fillInformationLabels(DataTable registeredDeviceCountDTable, DataTable pendingDeviceCountDTable, DataTable maxRegisterationAllowedDTable)
        {
            if (registeredDeviceCountDTable != null && registeredDeviceCountDTable.Rows.Count > 0)
            {
                lblApprovedDevice.Text = ((int)registeredDeviceCountDTable.Rows[0]["TOTAL"]).ToString();
            }
            else
            {
                lblApprovedDevice.Text = "0";
            }

            if (pendingDeviceCountDTable != null && pendingDeviceCountDTable.Rows.Count > 0)
            {
                lblPendingRequest.Text = ((int)pendingDeviceCountDTable.Rows[0]["TOTAL"]).ToString();
            }
            else
            {
                lblPendingRequest.Text = "0";
            }

            if (maxRegisterationAllowedDTable != null && maxRegisterationAllowedDTable.Rows.Count > 0)
            {
                lblMaxRegisteredDev.Text = ((int)maxRegisterationAllowedDTable.Rows[0]["MAX_USER"]).ToString();
            }
            else
            {
                lblMaxRegisteredDev.Text = "0";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headerInfo"></param>
        /// <param name="repeaterName">Pending Request (Pass "Pending") Registered Devices (Pass "Registered") pass empty string no rpt is to be shown</param>
        private void showHideRepeaterAndShowRptHeaderInfo(string headerInfo, string repeaterName)
        {
            lblHeaderInfo.Text = "<h1>" + headerInfo + "</h1>";
            if (repeaterName == "Pending")
            {
                rptPendingRequest.Visible = true;
                rptRegisteredDeviceDtls.Visible = false;
            }
            else if (repeaterName == "Registered")
            {
                rptPendingRequest.Visible = false;
                rptRegisteredDeviceDtls.Visible = true;
            }
            else
            {
                rptPendingRequest.Visible = false;
                rptRegisteredDeviceDtls.Visible = false;
            }
        }




        protected void radListSelection_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (radListSelection.SelectedValue == "1")
            {
                if (ddlSubAdmin.SelectedValue == "0")
                {
                    bindRegisteredDevicesRepeater();
                }
                else
                {
                    bindRegisteredDevicesRepeaterBySubAdminId();
                }
            }
            else
            {
                if (ddlSubAdmin.SelectedValue == "0")
                {
                    bindPendingRequestRepeater();
                }
                else
                {
                    bindPendingDevicesRequestBySubAdminId();
                }
            }
        }

        #region Bind, Show Header And Show hide approve
        /// <summary>
        /// 
        /// </summary>
        /// <returns>No of rows bounded</returns>
        private int bindRegisteredDevicesRepeater()
        {
            int iRowsCount = 0;
            GetRegisteredOrPendingDeviceDtlsForAdmin objRegtDeviceDtls = new GetRegisteredOrPendingDeviceDtlsForAdmin(hfsPart3, hfsPart4, "Registered");
            objRegtDeviceDtls.Process();
            DataSet dsRegtDeviceDtls = objRegtDeviceDtls.ResultTables;
            if (dsRegtDeviceDtls != null)
            {
                if (dsRegtDeviceDtls.Tables.Count > 0)
                {
                    if (dsRegtDeviceDtls.Tables[0] != null)
                    {
                        if (dsRegtDeviceDtls.Tables[0].Rows.Count > 0)
                        {
                            DataTable dtblSortedDataSource = sortDatatableBySortingColmnInHidField(
                                REPEATER_NAME.RegisteredDevices,
                                dsRegtDeviceDtls.Tables[0]
                                );

                            rptRegisteredDeviceDtls.DataSource = dtblSortedDataSource;
                            rptRegisteredDeviceDtls.DataBind();
                            showRepeaterHeaderInfo("Registered Devices", REPEATER_NAME.RegisteredDevices);
                            rptRegisteredDeviceDtls.Visible = true;
                            rptPendingRequest.Visible = false;
                            iRowsCount = dtblSortedDataSource.Rows.Count;
                        }
                        else
                        {
                            showRepeaterHeaderInfo("There are no registered devices.", REPEATER_NAME.RegisteredDevices);
                            rptRegisteredDeviceDtls.DataSource = null;
                            rptRegisteredDeviceDtls.DataBind();
                            rptPendingRequest.Visible = false;
                            rptRegisteredDeviceDtls.Visible = false;
                        }
                    }

                    fillInformationLabels(dsRegtDeviceDtls.Tables[1], dsRegtDeviceDtls.Tables[2], dsRegtDeviceDtls.Tables[3]);
                }
                else
                {
                    showRepeaterHeaderInfo("There are no registered devices.", REPEATER_NAME.RegisteredDevices);
                    rptPendingRequest.Visible = false;
                    rptRegisteredDeviceDtls.Visible = false;
                }
            }
            return iRowsCount;
        }
        private int bindPendingRequestRepeater()
        {
           int  iRowsCount = 0;
            hidPendingDoubleAsterix.Value = "";//using it for showing the meaning of double asterix set in repeater databound
            GetRegisteredOrPendingDeviceDtlsForAdmin objPendingDeviceDtls = new GetRegisteredOrPendingDeviceDtlsForAdmin(hfsPart3, hfsPart4, "Pending");
            objPendingDeviceDtls.Process();

            DataSet dsPendingRequestDtls = objPendingDeviceDtls.ResultTables;

            if (dsPendingRequestDtls != null)
            {
                if (dsPendingRequestDtls.Tables.Count > 0)
                {
                    if (dsPendingRequestDtls.Tables[0].Rows.Count > 0)
                    {
                        DataTable dtblSortedDataSource = sortDatatableBySortingColmnInHidField(
                                REPEATER_NAME.PendingDevices,
                                dsPendingRequestDtls.Tables[0]
                                );

                        rptPendingRequest.DataSource = dtblSortedDataSource;
                        rptPendingRequest.DataBind();

                        bindRegisteredAndPendingDevicesCntInPendingRpt(dsPendingRequestDtls.Tables[5], dsPendingRequestDtls.Tables[7]);
                        showRepeaterHeaderInfo("Pending Requests", REPEATER_NAME.PendingDevices);
                        rptPendingRequest.Visible = true;
                        rptRegisteredDeviceDtls.Visible = false;
                        showDoubleAsterixInfoForPendingRpt();
                        iRowsCount = dsPendingRequestDtls.Tables[0].Rows.Count;
                    }
                    else
                    {
                        showRepeaterHeaderInfo("There are no pending request.", REPEATER_NAME.PendingDevices);
                        rptPendingRequest.Visible = false;
                        rptRegisteredDeviceDtls.Visible = false;
                        rptPendingRequest.DataSource = null;
                        rptPendingRequest.DataBind();
                    }
                    fillInformationLabels(dsPendingRequestDtls.Tables[2], dsPendingRequestDtls.Tables[3], dsPendingRequestDtls.Tables[4]);
                }
            }
            return iRowsCount;
        }
        private int bindRegisteredDevicesRepeaterBySubAdminId()
        {
           int  iRowsCount = 0;
            GetRegisteredDeviceDetails objRegtDeviceDtls = new GetRegisteredDeviceDetails(hfsPart3, ddlSubAdmin.SelectedValue
                , REGISTERED_DEVICES_ORDER_BY.RequestDate, SORT_TYPE.DESC);
            objRegtDeviceDtls.Process();
            DataSet dsRegtDeviceDtls = objRegtDeviceDtls.ResultTables;
            if (dsRegtDeviceDtls != null)
            {
                if (dsRegtDeviceDtls.Tables.Count > 0)
                {
                    if (dsRegtDeviceDtls.Tables[0] != null)
                    {
                        if (dsRegtDeviceDtls.Tables[0].Rows.Count > 0)
                        {
                            DataTable dtblSortedDataSource = sortDatatableBySortingColmnInHidField(
                                REPEATER_NAME.RegisteredDevices,
                                dsRegtDeviceDtls.Tables[0]
                                );

                            rptRegisteredDeviceDtls.DataSource = dtblSortedDataSource;
                            rptRegisteredDeviceDtls.DataBind();
                            showRepeaterHeaderInfo("Registered Devices", REPEATER_NAME.RegisteredDevices);

                            rptRegisteredDeviceDtls.Visible = true;
                            rptPendingRequest.Visible = false;
                            iRowsCount = dtblSortedDataSource.Rows.Count;
                        }
                        else
                        {
                            showRepeaterHeaderInfo("There are no registered devices.", REPEATER_NAME.RegisteredDevices);
                            rptRegisteredDeviceDtls.Visible = false;
                            rptPendingRequest.Visible = false;
                            rptRegisteredDeviceDtls.DataSource = null;
                            rptRegisteredDeviceDtls.DataBind();
                        }
                    }
                    fillInformationLabels(dsRegtDeviceDtls.Tables[1], dsRegtDeviceDtls.Tables[2], dsRegtDeviceDtls.Tables[3]);
                }
                else
                {
                    showRepeaterHeaderInfo("There are no registered devices.", REPEATER_NAME.RegisteredDevices);
                    rptPendingRequest.Visible = false;
                    rptRegisteredDeviceDtls.Visible = false;
                }
            }
            return iRowsCount;
        }
        private int bindPendingDevicesRequestBySubAdminId()
        {
            int iRowsCount = 0;
            GetRequestForRegisteringDevice objPendingRequest = new GetRequestForRegisteringDevice(hfsPart3, ddlSubAdmin.SelectedValue
                , PENDING_DEVICES_ORDER_BY.RequestDate, SORT_TYPE.ASC);
            objPendingRequest.Process();
            DataSet dsPendingRequestDtls = objPendingRequest.ResultTables;
            if (dsPendingRequestDtls != null && dsPendingRequestDtls.Tables.Count > 0)
            {
                if (dsPendingRequestDtls.Tables[0] != null && dsPendingRequestDtls.Tables[0].Rows.Count > 0)
                {
                    DataTable dtblSortedDataSource = sortDatatableBySortingColmnInHidField(
                                REPEATER_NAME.PendingDevices,
                                dsPendingRequestDtls.Tables[0]
                                );
                    rptPendingRequest.DataSource = dtblSortedDataSource;
                    rptPendingRequest.DataBind();

                    bindRegisteredAndPendingDevicesCntInPendingRpt(dsPendingRequestDtls.Tables[5], dsPendingRequestDtls.Tables[7]);
                    showRepeaterHeaderInfo("Pending Requests", REPEATER_NAME.PendingDevices);
                    rptPendingRequest.Visible = true;
                    rptRegisteredDeviceDtls.Visible = false;
                    showDoubleAsterixInfoForPendingRpt();
                    iRowsCount = dtblSortedDataSource.Rows.Count;
                }
                else
                {
                    showRepeaterHeaderInfo("There are no pending request", REPEATER_NAME.PendingDevices);
                    rptPendingRequest.Visible = false;
                    rptRegisteredDeviceDtls.Visible = false;
                    rptPendingRequest.DataSource = null;
                    rptPendingRequest.DataBind();
                }
                fillInformationLabels(dsPendingRequestDtls.Tables[2], dsPendingRequestDtls.Tables[3], dsPendingRequestDtls.Tables[4]);
            }
            else
            {
                showHideRepeaterAndShowRptHeaderInfo("There are no pending request.", "");
            }
            return iRowsCount;
        }
        
        
        private void showDoubleAsterixInfoForPendingRpt()
        {
            if (!String.IsNullOrEmpty(hidPendingDoubleAsterix.Value))
            {
                try
                {
                    if (Convert.ToBoolean(hidPendingDoubleAsterix.Value))
                    {
                        Label lblDblAsterix = (Label)rptPendingRequest.Controls[rptPendingRequest.Controls.Count - 1].Controls[0].FindControl("lblDoubleAsterixInfo");
                        lblDblAsterix.Visible = true;
                        lblDblAsterix.Text = "** Device registered for other user.Please delete registered device first to approve.";
                        hidPendingDoubleAsterix.Value = "";
                    }
                }
                catch
                {

                }
            }
        }


        private void bindRegisteredAndPendingDevicesCntInPendingRpt(DataTable registeredDevicesCntForUser, DataTable pendingReqCountForUser)
        {
            try
            {
                foreach (RepeaterItem item in rptPendingRequest.Items)
                {
                    LinkButton lnkCount = (LinkButton)item.FindControl("lnkCntRegisteredPendingDevices");
                    Label lblUserId = (Label)item.FindControl("lblUserId");
                    string strFilter = String.Format("USER_ID ='{0}'", lblUserId.Text);
                    DataRow[] row = registeredDevicesCntForUser.Select(strFilter);
                    if (row.Length > 0)
                    {
                        lnkCount.Text = Convert.ToString(row[0]["REGISTERED_DEVICE_COUNT"]);
                    }
                    else
                    {
                        lnkCount.Text = "0";
                    }
                    row = pendingReqCountForUser.Select(strFilter);
                    if (row.Length > 0)
                    {
                        lnkCount.Text += " / " + Convert.ToString(row[0]["REQUEST_COUNT"]);
                    }
                    else
                    {
                        lnkCount.Text += " / " + "0";
                    }
                }
            }
            catch
            {

            }
        }
        private void showHideApproveFromRepeater(DataTable registeredDevicesCountDTable, DataTable exceptionalUsersAndMaxDevicesDTable)
        {
            //Checking if device is already registered for another user in Repeater Item Data bound
            //this is to check if the user is an exceptional user,checking no of max device for him ,and if not set for the user then
            //check for general company setting,ie,max allowed devices for any user in the company
            if (exceptionalUsersAndMaxDevicesDTable != null && exceptionalUsersAndMaxDevicesDTable.Rows.Count > 0)
            {
                foreach (DataRow row in exceptionalUsersAndMaxDevicesDTable.Rows)
                {
                    foreach (RepeaterItem item in rptPendingRequest.Items)
                    {
                        LinkButton lnkApprove = (LinkButton)item.FindControl("lnkApprove");
                        Label lblUserId = (Label)item.FindControl("lblUserId");
                        if ((lblUserId.Text == (string)row["USER_ID"] && Convert.ToString(row["REGISTERED_DEVICE_COUNT"]) == Convert.ToString(row["MAX_DEVICE"])) || (lblUserId.Text == (string)row["USER_ID"] && Convert.ToString(row["REGISTERED_DEVICE_COUNT"]) == Convert.ToString(row["MAX_DEVICE_PER_USER"])))
                        {
                            //check if the request is only for approval
                            //if it is for delete any earlier device and then approve the new one then we can allow the request to be approved
                            if (lnkApprove.CommandArgument == ((int)REGISTRATION_DEVICE_REQUEST_TYPE.ALLOW_USER_DEVICE).ToString())
                            {
                                lnkApprove.Visible = false;
                                lblUserId.ForeColor = System.Drawing.Color.Red;
                                lblUserId.ToolTip = "Reached max device registration for the user.";
                            }
                        }
                    }
                }
            }

            //this checks how many devices can totally be allotted according to the plan details the company has taken
            if (registeredDevicesCountDTable != null && registeredDevicesCountDTable.Rows.Count > 0)
            {
                if ((int)registeredDevicesCountDTable.Rows[0]["Total"] == (int)registeredDevicesCountDTable.Rows[0]["MAX_USER"])
                {
                    foreach (RepeaterItem item in rptPendingRequest.Items)
                    {
                        LinkButton lnkApprove = (LinkButton)item.FindControl("lnkApprove");
                        //if the request type is other than this then we first will be deleting a device and then registering 
                        //so the count would remain same.and hence we could allow this
                        if (lnkApprove.CommandArgument == ((int)REGISTRATION_DEVICE_REQUEST_TYPE.ALLOW_USER_DEVICE).ToString())
                        {
                            lnkApprove.Visible = false;
                        }
                    }
                    Utilities.showAlert("Since you have reached your max allowed registration,you have to first delete existing devices to approve any new request.", "divInfomationForApproveDisable", updRepeater);
                }

            }
        }
        private int bindAndShowHideModalRegisteredDeviceRpt(string userId, string subAdminId, DEVICE_DETAILS_SHOW_PURPOSE showPurpose)
        {
            int iStatus = 0;
            GetRegisteredDeviceDetails objRegisteredDeviceDetails = new GetRegisteredDeviceDetails(hfsPart3, subAdminId
                , REGISTERED_DEVICES_ORDER_BY.RequestDate, SORT_TYPE.DESC);
            objRegisteredDeviceDetails.Process();
            if (objRegisteredDeviceDetails.StatusCode == 0)
            {
                DataTable dtblUserRegisteredDevice = objRegisteredDeviceDetails.ResultTables.Tables[0];
                string strFilter = String.Format("USER_ID = '{0}'", userId.Trim());
                DataRow[] userDtlRows = dtblUserRegisteredDevice.Select(strFilter);
                if (userDtlRows.Length > 0)
                {
                    try
                    {
                        dtblUserRegisteredDevice = userDtlRows.CopyToDataTable<DataRow>();
                        dtblUserRegisteredDevice = getModifiedTblForPopUpRpts(dtblUserRegisteredDevice, showPurpose);
                        rptModalRegisteredDevicesDtls.DataSource = dtblUserRegisteredDevice;
                        rptModalRegisteredDevicesDtls.DataBind();
                        pnlRegisteredDevicesRptBox.Visible = true;

                        showHideRptHeaderForDeviceInfoPopUp(showPurpose, REPEATER_NAME.ModalRegisteredDevices);//if datasource is not null then don't show repeater
                    }
                    catch 
                    {
                        iStatus = -1000;
                    }
                }
                else
                {
                    rptModalRegisteredDevicesDtls.DataSource = null;
                    rptModalRegisteredDevicesDtls.DataBind();
                    showRepeaterHeaderInfo("There are no registered devices.", REPEATER_NAME.ModalRegisteredDevices);

                }
            }
            else
            {
                iStatus = -1000;
            }
            return iStatus;
        }
        private int bindAndShowHideModalPendingDeviceRpt(string userId, string subAdminId, DEVICE_DETAILS_SHOW_PURPOSE showPurpose)
        {
            int iStatus = 0;
            GetRequestForRegisteringDevice objPendingRequest = new GetRequestForRegisteringDevice(hfsPart3, subAdminId,
                PENDING_DEVICES_ORDER_BY.RequestDate, SORT_TYPE.ASC);
            objPendingRequest.Process();
            DataSet dsPendingRequestDtls = objPendingRequest.ResultTables;
            DataTable dtblPendingRequests = objPendingRequest.ResultTables.Tables[0];
            if (dtblPendingRequests != null)
            {
                string strFilter = String.Format("USER_ID = '{0}'", userId.Trim());
                DataRow[] userDtlRows = dtblPendingRequests.Select(strFilter);
                //        select user;
                if (userDtlRows.Length > 0)
                {
                    try
                    {
                        dtblPendingRequests = userDtlRows.CopyToDataTable<DataRow>();
                        dtblPendingRequests = getModifiedTblForPopUpRpts(dtblPendingRequests, showPurpose);
                        rptModalPendingRequestDtls.DataSource = dtblPendingRequests;
                        rptModalPendingRequestDtls.DataBind();
                        pnlPendingDevicesRptBox.Visible = true;
                        showHideRptHeaderForDeviceInfoPopUp(showPurpose, REPEATER_NAME.ModalPendingDevices);
                    }
                    catch
                    {
                        iStatus = -1000;
                    }
                }
                else
                {
                    showRepeaterHeaderInfo("There are no pending devices", REPEATER_NAME.ModalPendingDevices);
                    rptModalPendingRequestDtls.DataSource = null;
                    rptModalPendingRequestDtls.DataBind();
                }
            }
            else
            {
                iStatus = -1000;
            }
            return iStatus;
        }
        private DataTable getModifiedTblForPopUpRpts(DataTable deviceDetailsDtbl, DEVICE_DETAILS_SHOW_PURPOSE showPurpose)
        {
            try
            {
                bool blnShowProcessingChkBox = false;
                switch (showPurpose)
                {
                    case DEVICE_DETAILS_SHOW_PURPOSE.Information:
                        blnShowProcessingChkBox = false;
                        break;
                    case DEVICE_DETAILS_SHOW_PURPOSE.Processing:
                        blnShowProcessingChkBox = true;
                        break;
                }
                deviceDetailsDtbl.Columns.Add("FOR_PROCESSING", typeof(bool));
                foreach (DataRow row in deviceDetailsDtbl.Rows)
                {
                    row["FOR_PROCESSING"] = blnShowProcessingChkBox;
                }
                deviceDetailsDtbl.AcceptChanges();
                return deviceDetailsDtbl;
            }
            catch
            {
                throw new Exception();
            }
        }
        private void showHideRptHeaderForDeviceInfoPopUp(DEVICE_DETAILS_SHOW_PURPOSE showPurpose, REPEATER_NAME repeater)
        {
            switch (showPurpose)
            {
                case DEVICE_DETAILS_SHOW_PURPOSE.Processing:

                    pnlTabPendingDevicesHeader.Visible = true;
                    pnlTabRegisteredDevicesHeader.Visible = true;

                    showRepeaterHeaderInfo("Select device to delete", REPEATER_NAME.ModalRegisteredDevices);
                    break;
                case DEVICE_DETAILS_SHOW_PURPOSE.Information:
                    switch (repeater)
                    {
                        case REPEATER_NAME.ModalPendingDevices:
                            pnlTabPendingDevicesHeader.Visible = false;
                            break;
                        case REPEATER_NAME.ModalRegisteredDevices:
                            pnlTabRegisteredDevicesHeader.Visible = false;
                            break;
                    }
                    break;
            }
        }

        private void showRepeaterHeaderInfo(string headerInfo, REPEATER_NAME rptName)
        {

            string strHeaderText = "<h1>" + headerInfo + "</h1>";
            switch (rptName)
            {
                case REPEATER_NAME.ModalPendingDevices:
                    lblModalPendingDevicesHeader.Text = strHeaderText;
                    lblModalPendingDevicesHeader.Visible = true;
                    break;
                case REPEATER_NAME.ModalRegisteredDevices:
                    lblModalRegtredHdr.Text = strHeaderText;
                    lblModalRegtredHdr.Visible = true;
                    break;
                case REPEATER_NAME.PendingDevices:
                case REPEATER_NAME.RegisteredDevices:
                    lblHeaderInfo.Text = strHeaderText;
                    lblHeaderInfo.Visible = true;
                    break;
            }
        }
        #endregion

        #region registered and pending requests repeater
        protected void rptRegisteredDeviceDtls_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblRegistrationDate = (Label)e.Item.FindControl("lblRegistrationDate");
                long lngRegistrationDate = (long)DataBinder.Eval(e.Item.DataItem, "REGISTRATION_DATE");
                lblRegistrationDate.Text = Utilities.getCompanyLocalFormattedDate(CompanyTimezone.CmpTimeZoneInfo, lngRegistrationDate);
            }
        }

        protected void rptPendingRequest_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            showUserNameAndDblAsterixOnItemBound(e, (Repeater)sender);
        }

        protected void rptPendingRequest_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Details":
                    processShowDetail(e);
                    break;
                case "DetailsOfDevices":
                    processShowDetailOfDevices(e, (Repeater)sender, DEVICE_DETAILS_SHOW_PURPOSE.Information);
                    break;
            }
        }
        protected void rptRegisteredDeviceDtls_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Details":
                    processShowDetail(e);
                    break;
            }
        }

        private void processShowDetail(RepeaterCommandEventArgs e)
        {
            Label lblUserId = (Label)e.Item.FindControl("lblUserId");
            Label lblUserName = (Label)e.Item.FindControl("lblUserName");
            ucUserDetails.UserId = lblUserId.Text;
            ucUserDetails.UserName = lblUserName.Text;
            ucUserDetails.rememberUserId();
            ucUserDetails.getUserDetailsAndFillData(lblUserId.Text, hfsPart3);
            ucUserDetails.FindControl("lnkEditUser").Visible = false;
            ucUserDetails.FindControl("lnkBlockUser").Visible = false;
            ucUserDetails.FindControl("lnkUnblockUser").Visible = false;
            Utilities.showModalPopup("divUserDtlsModal", updRepeater, "User Detail", Convert.ToString(Utilities.getModalPopUpWidth(MODAL_POP_UP_NAME.USER_DETAIL)), true);
            ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "HideSeparator", "hideHTMLElements('ucSeparatorOne');", true);
            updUserDtlsModal.Update();
        }

        private void processShowDetailOfDevices(RepeaterCommandEventArgs e, Repeater rpt, DEVICE_DETAILS_SHOW_PURPOSE showPurpose)
        {
            Label lblUserId = (Label)e.Item.FindControl("lblUserId");
            Label lblUserName = (Label)e.Item.FindControl("lblUserName");
            LinkButton lnkName = (LinkButton)e.Item.FindControl("lnkName");
            Label lblSubAdminId = (Label)e.Item.FindControl("lblSubAdminId");
            showDetailsOfDevices(lblUserId.Text.Trim(), lnkName.Text.Trim(), lblSubAdminId.Text, rpt, showPurpose);

        }
        private void showDetailsOfDevices(string userId, string fullNameOfUser, string subAdminId, Repeater eventGenerationRepeater, DEVICE_DETAILS_SHOW_PURPOSE showPurpose)
        {
            //returning -1000 for error
            int iStatus = bindAndShowHideModalRegisteredDeviceRpt(userId.Trim(), subAdminId, showPurpose);
            if (iStatus == 0)
            {
                iStatus = bindAndShowHideModalPendingDeviceRpt(userId.Trim(), subAdminId, showPurpose);
                if (iStatus == 0)
                {
                    if (eventGenerationRepeater.ID.Trim() == "rptPendingRequest".Trim())
                    {
                        Utilities.showModalPopup("divModalRegisteredPendingDevicesDtls", updRepeater, "Device Details ( " + fullNameOfUser + " )", Utilities.getModalPopUpWidth(MODAL_POP_UP_NAME.DEVICE_DETAILS_REPEATER).ToString(), true);
                    }
                    switch (showPurpose)
                    {
                        case DEVICE_DETAILS_SHOW_PURPOSE.Processing:
                            ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "HidePendingTab", "hideCompleteTab(true);", true);
                            break;
                        case DEVICE_DETAILS_SHOW_PURPOSE.Information:
                            ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "ShowPendingTab", "hideCompleteTab(false);", true);
                            break;
                    }
                    updRegtdPendingDtls.Update();
                }
                else
                {
                    Utilities.showMessage("Internal server error.Please try again", updRepeater, DIALOG_TYPE.Error);
                }
            }
            else
            {
                Utilities.showMessage("Internal error.Please try again", updRepeater, DIALOG_TYPE.Error);
            }
        }
        private void showUserNameAndDblAsterixOnItemBound(RepeaterItemEventArgs e, Repeater repeater)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblDeviceType = (Label)e.Item.FindControl("lblDeviceType");
                Label lblDeviceModel = (Label)e.Item.FindControl("lblDeviceModel");
                LinkButton lnkName = (LinkButton)e.Item.FindControl("lnkName");
                Label lblDoubleAsterix = (Label)e.Item.FindControl("lblItemDoubleAsterix");
                lblDoubleAsterix.Visible = false;
                if ((string)DataBinder.Eval(e.Item.DataItem, "DEVICE_ID") == Convert.ToString(DataBinder.Eval(e.Item.DataItem, "RegDeviceId")))
                {
                    lblDoubleAsterix.Visible = true;
                    hidPendingDoubleAsterix.Value = "True";
                }
                if (repeater.ID.ToLower() == "rptPendingRequest".ToLower())
                {
                    Label lblRequestDate = (Label)e.Item.FindControl("lblRequestDate");
                    long lngRequestDate = (long)DataBinder.Eval(e.Item.DataItem, "REQUEST_DATETIME");
                    lblRequestDate.Text = Utilities.getCompanyLocalFormattedDate(CompanyTimezone.CmpTimeZoneInfo, lngRequestDate);
                    lnkName.Text = (string)DataBinder.Eval(e.Item.DataItem, "FIRST_NAME") + " " + (string)DataBinder.Eval(e.Item.DataItem, "LAST_NAME");
                }
            }

        }

        #endregion

        protected void lnkRefreshList_Click(object sender, EventArgs e)
        {
            if (radListSelection.SelectedValue == "1")
            {
                if (ddlSubAdmin.SelectedValue == "0")
                {
                    bindRegisteredDevicesRepeater();
                }
                else
                {
                    bindRegisteredDevicesRepeaterBySubAdminId();
                }
            }
            else
            {
                if (ddlSubAdmin.SelectedValue == "0")
                {
                    bindPendingRequestRepeater();
                }
                else
                {
                    bindPendingDevicesRequestBySubAdminId();
                }
            }
        }

        #region modal repeater region
        protected void rptModalRegisteredDevicesDtls_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox chkDevices = (CheckBox)e.Item.FindControl("chkRegisteredDevices");
                if (!(bool)DataBinder.Eval(e.Item.DataItem, "FOR_PROCESSING"))
                {
                    chkDevices.Visible = false;
                }
                else
                {
                    chkDevices.Visible = true;
                }
            }
        }

        protected void rptModalPendingRequestDtls_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblFullName = (Label)e.Item.FindControl("lblFullName");
                lblFullName.Text = (string)DataBinder.Eval(e.Item.DataItem, "FIRST_NAME") + " " + (string)DataBinder.Eval(e.Item.DataItem, "LAST_NAME");
            }
        }
        #endregion

        protected void btnRowClickPostBack_Click(object sender, EventArgs e)
        {
            if (hidSortingDetail.Value.Trim().Length != 0)//for sorting
            {
                //values repeatername,columnIndex,sorttype
                string[] aryValuesFromHidField = hidSortingDetail.Value.Split(',');
                string[] hfsValues = Utilities.DecryptString(hfsValue).Split(',');
                try
                {
                    int iRowsCount = 0;
                    REPEATER_NAME eRepeater = (REPEATER_NAME)Enum.Parse(typeof(REPEATER_NAME), aryValuesFromHidField[0]);
                    if (Enum.IsDefined(typeof(REPEATER_NAME), eRepeater))
                    {
                        switch (eRepeater)
                        {
                            case REPEATER_NAME.RegisteredDevices:
                                if (ddlSubAdmin.SelectedValue == "0")
                                {
                                  iRowsCount=  bindRegisteredDevicesRepeater();
                                }
                                else
                                {
                                   iRowsCount= bindRegisteredDevicesRepeaterBySubAdminId();
                                }
                                break;
                            case REPEATER_NAME.PendingDevices:
                                if (ddlSubAdmin.SelectedValue == "0")
                                {
                                   iRowsCount= bindPendingRequestRepeater();
                                }
                                else
                                {
                                  iRowsCount=  bindPendingDevicesRequestBySubAdminId();
                                }
                                break;
                        }

                    }
                    if (iRowsCount == 0)
                    {
                        hidSortingDetail.Value = "";//if no record found or no sorting took place then previous sorting should not change
                    }
                    else
                    {
                        Utilities.runPostBackScript("changeImageOfSortedCol();clearSortingDtlHiddenField();", updRepeater, "Change Sort Image");
                    }
                }
                catch
                {
                    Utilities.showMessage("Internal server error", updRowPostBack, DIALOG_TYPE.Error);
                    hidSortingDetail.Value = "";
                }
            }
        }

        DataTable sortDatatableBySortingColmnInHidField(REPEATER_NAME rptName, DataTable tblToSort)
        {
            DataTable dtblReturn = null;
            string[] aryValuesFromHidField;
            //values repeatername,columnIndex,sorttype
            if (String.IsNullOrEmpty(hidSortingDetail.Value))
            {
                //This will be called only when the binding is happening for the first time
                //Or if sorting is not reocorded when sorting is done at clients end.
                aryValuesFromHidField = hidCurrentSortOrder.Value.Split(',');
            }
            else
            {
                aryValuesFromHidField = hidSortingDetail.Value.Split(',');
            }

            REGISTERED_DEVICES_ORDER_BY orderBy = REGISTERED_DEVICES_ORDER_BY.RequestDate;
            PENDING_DEVICES_ORDER_BY pendingOrderBy = PENDING_DEVICES_ORDER_BY.RequestDate;
            SORT_TYPE sortType = SORT_TYPE.DESC;
            if (aryValuesFromHidField.Length > 0)
            {
                switch (rptName)
                {
                    case REPEATER_NAME.RegisteredDevices:
                        orderBy = (REGISTERED_DEVICES_ORDER_BY)Enum.Parse(typeof(REGISTERED_DEVICES_ORDER_BY), aryValuesFromHidField[1]);
                        sortType = (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), aryValuesFromHidField[2]);
                        tblToSort.DefaultView.Sort = getOrderByColumnName(orderBy) + " " + getSortTypeInString(sortType);
                        dtblReturn = tblToSort.DefaultView.Table;
                        break;
                    case REPEATER_NAME.PendingDevices:
                        pendingOrderBy = (PENDING_DEVICES_ORDER_BY)Enum.Parse(typeof(PENDING_DEVICES_ORDER_BY), aryValuesFromHidField[1]);
                        sortType = (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), aryValuesFromHidField[2]);
                        tblToSort.DefaultView.Sort = getOrderByColumnName(pendingOrderBy) + " " + getSortTypeInString(sortType);
                        dtblReturn = tblToSort.DefaultView.Table;
                        break;
                }
            }
            return dtblReturn;
        }

        string getOrderByColumnName(REGISTERED_DEVICES_ORDER_BY orderBy)
        {
            string strOrderByName = "";
            switch (orderBy)
            {
                case REGISTERED_DEVICES_ORDER_BY.Name:
                    strOrderByName = "FULL_NAME";
                    break;
                case REGISTERED_DEVICES_ORDER_BY.OS:
                    strOrderByName = "DEVICE_TYPE";
                    break;
                case REGISTERED_DEVICES_ORDER_BY.RequestDate:
                    strOrderByName = "REGISTRATION_DATE";
                    break;
            }
            return strOrderByName;
        }
        string getOrderByColumnName(PENDING_DEVICES_ORDER_BY orderBy)
        {
            string strOrderByName = "";
            switch (orderBy)
            {
                case PENDING_DEVICES_ORDER_BY.Name:
                    strOrderByName = "FULL_NAME";
                    break;
                case PENDING_DEVICES_ORDER_BY.OS:
                    strOrderByName = "DEVICE_TYPE";
                    break;
                case PENDING_DEVICES_ORDER_BY.RequestDate:
                    strOrderByName = "REQUEST_DATETIME";
                    break;
                case PENDING_DEVICES_ORDER_BY.RequestType:
                    strOrderByName = "REQUEST_DESCRIPTION";
                    break;
            }
            return strOrderByName;
        }
        string getSortTypeInString(SORT_TYPE sortType)
        {
            string strSortType = "";
            switch (sortType)
            {
                case SORT_TYPE.ASC:
                    strSortType = "ASC";
                    break;
                case SORT_TYPE.DESC:
                    strSortType = "DESC";
                    break;
            }
            return strSortType;
        }

        #region Sorting Helper Functions
        void getCurrentSortOrderOfRegisteredDevRpt(out REGISTERED_DEVICES_ORDER_BY orderBy, out SORT_TYPE sortType)
        {
            orderBy = REGISTERED_DEVICES_ORDER_BY.RequestDate;
            sortType = SORT_TYPE.DESC;
            try
            {
                //values repeatername,columnIndex,sorttype
                string[] aryCurrentSortOrder = hidCurrentSortOrder.Value.Split(',');
                orderBy = (REGISTERED_DEVICES_ORDER_BY)Enum.Parse(typeof(REGISTERED_DEVICES_ORDER_BY), aryCurrentSortOrder[1]);
                sortType = (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), aryCurrentSortOrder[2]);
            }
            catch
            {

            }
        }
        void getCurrSortOrderOfPendingDevRpt(out PENDING_DEVICES_ORDER_BY orderBy, out SORT_TYPE sortType)
        {
            orderBy = PENDING_DEVICES_ORDER_BY.RequestDate;
            sortType = SORT_TYPE.DESC;
            try
            {
                //values repeatername,columnIndex,sorttype
                string[] aryCurrentSortOrder = hidCurrentSortOrder.Value.Split(',');
                orderBy = (PENDING_DEVICES_ORDER_BY)Enum.Parse(typeof(PENDING_DEVICES_ORDER_BY), aryCurrentSortOrder[1]);
                sortType = (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), aryCurrentSortOrder[2]);
            }
            catch
            {

            }
        }
        void initialiseCurrentSortOrder(REPEATER_NAME rpt)
        {
            switch (rpt)
            {
                case REPEATER_NAME.PendingDevices:
                    hidCurrentSortOrder.Value = "2" + "," + ((int)PENDING_DEVICES_ORDER_BY.RequestDate).ToString() + "," + "1";
                    break;
                case REPEATER_NAME.RegisteredDevices:
                    hidCurrentSortOrder.Value = "1" + "," + ((int)REGISTERED_DEVICES_ORDER_BY.RequestDate).ToString() + "," + "1";
                    break;
            }
        }
        #endregion
    }
}
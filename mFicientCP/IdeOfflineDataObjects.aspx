﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/IdeMaster.Master" AutoEventWireup="true"
    CodeBehind="IdeOfflineDataObjects.aspx.cs" Inherits="mFicientCP.IdeOfflineDataObjects"
    EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/IdeToken-input-facebook.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery.datatables.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery.atwho.css" rel="stylesheet" type="text/css" />
    <link href="css/styles-TokenInput/token-input-mf.css" rel="stylesheet" type="text/css" />
    <%-- <script src="Scripts/IdeTokeninput201402140951.js" type="text/javascript"></script>--%>
    <script src="Scripts/ideOfflineDataObjects.js" type="text/javascript"></script>
    <script src="Scripts/jquery.atwho.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.caret.js" type="text/javascript"></script>
    <script src="Scripts/jquery.tokeninput.js" type="text/javascript"></script>
    <style type="text/css">
        .datatablebind
        {
            width: 49%;
            float: left;
            margin-left: 7px;
        }
        .addform
        {
            float: right;
            margin-left: 8px;
            margin-right: 7px;
            width: 49%;
            min-width: 450px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="css/Jquerydatatablerowclick.css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainCanvas" runat="server">
    <div id="divMainContainer" class="g12">
        <%--<section style="margin: 5px 5px 5px 3px;">--%>
        <asp:Panel ID="pnlDataObjects" CssClass="repeaterBox" runat="server">
            <asp:Panel ID="pnlDataObjectsHeader" CssClass="repeaterBox-header" runat="server">
                <div>
                    <asp:Label ID="lblDbObject" runat="server" Text="<h1>Offline Data Objects</h1>">
                    </asp:Label>
                </div>
                <div style="position: relative; top: 10px; right: 15px;">
                    <asp:LinkButton ID="lnkdivListDataObjectAddNewDataObject" runat="server" Text="Add"
                        CssClass="repeaterLink fr" OnClientClick="isCookieCleanUpRequired('false');bindOfflineDataObjectsTable();addNewOfflineDataObject();return false;"></asp:LinkButton>
                </div>
            </asp:Panel>
            <div id="divOfflineDataObjects" style="padding-top: 5px">
                <asp:UpdatePanel runat="server" ID="updDataObjectsList">
                    <ContentTemplate>
                        <div id="divListAndSearchObjects" class="g5" style="padding: 0px !important; width: 47% !important;">
                            <div id="divListDataObject" class="g12">
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="updAddNewDataObject" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divAddEditDataObj" runat="server" class="g7" style="width: 49% !important;">
                            <div style="margin: 0px auto;">
                                <div class="ProcPriview" style="width: 98%; padding: 5px;">
                                    <div id="divDataObjectDetails" class="hide">
                                        <div id="divViewDetails">
                                            <div class="modalPopUpDetHeaderDiv" id="divHeader">
                                                <div class="modalPopUpDetHeader" id="divObjectName">
                                                    <asp:Label ID="lblDataObjectName" runat="server" Text=""></asp:Label>
                                                </div>
                                                <div style="float: right;">
                                                    <div style="float: left;">
                                                        <div class="FLeft">
                                                            <asp:LinkButton ID="lnkCopy" runat="server" Text="Copy" OnClientClick="isCookieCleanUpRequired('false');ShowCopyObjectModalPopup(true);return false;"></asp:LinkButton>
                                                        </div>
                                                        <div style="float: left;">
                                                            &nbsp;|&nbsp;
                                                        </div>
                                                        <div class="FLeft">
                                                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" OnClientClick="isCookieCleanUpRequired('false');editOfflineDataObject();return false;"></asp:LinkButton>
                                                        </div>
                                                        <div style="float: left;">
                                                            &nbsp;|&nbsp;
                                                        </div>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClientClick="isCookieCleanUpRequired('false');deleteOfflineDataObject();return false;"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modalPopUpDetRow" style="border: none; padding: 0px 0px 0px 0px;">
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Description
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblDesc" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Object Type
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblDataObjType" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Offline Table
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblTablesUsed" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div><div class="clear">
                                                </div>
                                                <div id="dtlDivInputParams" class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Input(s)
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblInputs" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div id="dtlDivColumns" class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Columns
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblColumns" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                
                                                <div class="clear">
                                                </div>
                                                <div id="dtlDivWhere" class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Where
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblQuery" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div id="dtlDivOutputColumns" class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Output Columns
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblOutputColumns" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Error Message
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblErrorMessage" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Allow Retry
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblAllowRetry" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Exit On Error
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblExitOnError" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="divEditDataObject">
                                        <div id="divEditDataObjLbl" class="modalPopUpDetHeaderDiv hide">
                                            <div class="modalPopUpDetHeader">
                                                <asp:Label ID="lblEditDataObj_Name" runat="server" Text=""></asp:Label>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="Headerfontsize" id="addDataObjHeader">
                                            Add New Offline Data Object</div>
                                        <div class="Headernewfontsize" id="editDataObjHeader">
                                            Edit Offline Data Object
                                        </div>
                                        <div class="DbConnRightDiv">
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div id="divDataObjName" runat="server" class="DbCmdRow">
                                            <div class="DbCmdLeftDiv">
                                                Object Name
                                            </div>
                                            <div class="DbCmdRightDescDiv">
                                                <asp:TextBox ID="txtDataObj_Name" runat="server" class="CmdDescInputStyle1"></asp:TextBox>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div id="divDataObjDesc" runat="server" class="DbCmdRow">
                                            <div class="DbCmdLeftDiv">
                                                Description
                                            </div>
                                            <div class="DbCmdRightDescDiv">
                                                <asp:TextBox ID="txtDataObj_Desc" runat="server" TextMode="MultiLine" class="CmdDescInputStyle1"
                                                    Rows="2"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div id="divDataObjType" runat="server" class="DbCmdRow">
                                            <div class="DbCmdLeftDiv">
                                                Object Type
                                            </div>
                                            <div class="DbCmdRightDiv SelectDiv">
                                                <asp:DropDownList ID="ddl_DataObjType" runat="server" class="UseUniformCss" AutoPostBack="false">
                                                    <asp:ListItem Value="0">Select Object Type</asp:ListItem>
                                                    <asp:ListItem Value="1">SELECT</asp:ListItem>
                                                    <asp:ListItem Value="2">INSERT</asp:ListItem>
                                                    <asp:ListItem Value="3">UPDATE</asp:ListItem>
                                                    <asp:ListItem Value="4">DELETE</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div id="divInputParams" class=" hide">
                                            <div id="btnDbcmdParaAddDiv" class="DbCmdRow" style="margin-top: 10px;">
                                                <div class="DbCmdLeftDiv">
                                                    Input Parameters
                                                </div>
                                                <div class="DbCmdRightDiv">
                                                    <asp:LinkButton ID="Button29" runat="server" OnClientClick="SubProcDbCmdAddPara(true);AddParameterInDbCommand();return false;"
                                                        CssClass="lnkNoBorder"><img src="css/images/icons/dark/pencil.png" alt="Edit" title="Edit"/></asp:LinkButton>
                                                </div>
                                            </div>
                                            <div id="clearDivAddParameter" runat="server" class="clear">
                                            </div>
                                            <div id="lblInputParametersDiv" class="InputStyle" style="padding-top: 5px; padding-bottom: 5px;
                                                margin-top: 5px; float: left; overflow-y: scroll">
                                                <label id="lblInputParameters">
                                                    No Parameters defined yet</label>
                                            </div>
                                        </div>
                                        <div id="divOfflineTableUsed" class="DbCmdRow hide">
                                            <div class="DbCmdLeftDiv">
                                                Table
                                            </div>
                                            <div class="DbCmdRightDiv SelectDiv">
                                                <asp:DropDownList ID="ddlOfflineTable" runat="server" CssClass="UseUniformCss" AutoPostBack="false">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div id="divColumns" class="DbCmdRow hide">
                                            <div class="DbCmdLeftDiv">
                                                Columns
                                            </div>
                                            <div class="hide">
                                                <asp:Label ID="lblColJson" runat="server"></asp:Label>
                                            </div>
                                            <div class="DbCmdRightDiv SelectDiv">
                                                <div visible="true" class="Scrollable" style="min-height: 80px; max-height: 200px;
                                                    border: 1px solid #bbbbbb; border-radius: 4px; overflow: auto">
                                                    <table id="tblOfflineCols" class="detailsTable">
                                                        <thead>
                                                            <th>
                                                                Select Columns
                                                            </th>
                                                            <th>
                                                            </th>
                                                        </thead>
                                                        <tbody id="tblOfllineDataCols">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div id="divDataObjQueryCondition" class="hide">
                                            <div id="divDataObjQuery" class="DbCmdRow">
                                                <div class="DbCmdLeftDiv">
                                                    Where
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="DbCmdCompleteDiv">
                                                <asp:TextBox ID="txtDataObj_Query" runat="server" TextMode="MultiLine" class="InputStyle"
                                                    Height="80px" Width="99%"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div id="divQueryOutput" class="hide">
                                            <div id="divOutputParam" runat="server" class="DbCmdRow">
                                                <div class="DbCmdLeftDiv">
                                                    Output Columns
                                                </div>
                                                <div class="DbCmdRightDescDiv">
                                                    <asp:TextBox ID="txtDataObj_QueryPara" runat="server" class="InputStyle"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div id="divErrorMessages" runat="server" class="DbCmdRow">
                                            <div class="DbCmdLeftDiv">
                                                Error Message
                                            </div>
                                            <div class="DbCmdRightDescDiv">
                                                <asp:TextBox ID="txtErrorMessages" runat="server" TextMode="MultiLine" class="CmdDescInputStyle1"
                                                    Rows="2"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div id="divAllowRetry" runat="server" class="DbCmdRow">
                                            <div class="DbCmdLeftDiv">
                                                Allow Retry
                                            </div>
                                            <div class="DbCmdRightDiv SelectDiv">
                                                <asp:DropDownList ID="ddlAllowRetry" runat="server" class="UseUniformCss" AutoPostBack="false">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div id="divExitOnError" runat="server" class="DbCmdRow">
                                            <div class="DbCmdLeftDiv">
                                                Exit On Error
                                            </div>
                                            <div class="DbCmdRightDiv SelectDiv">
                                                <asp:DropDownList ID="ddlExitOnError" runat="server" class="UseUniformCss" AutoPostBack="false">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div class="SubProcborderDiv">
                                            <div class="SubProcBtnDiv" align="center">
                                                <asp:Button ID="btnDataObj_Save" runat="server" Text="  Save  " OnClientClick="return validateDataObject();"
                                                    class="InputStyle" OnClick="btnDataObj_Save_Click" />
                                                <asp:Button ID="btnDataObj_Cancel" runat="server" CssClass="InputStyle hide" OnClientClick="cancelButtonClick();return false;"
                                                    Text="  Cancel  " />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <asp:HiddenField ID="hdfDataObjectId" runat="server" />
                            <asp:HiddenField ID="hdfCompleteData" runat="server" />
                            <asp:HiddenField ID="hdfSelectedData" runat="server" />
                            <asp:HiddenField ID="hdfOfflineTables" runat="server" />
                            <asp:HiddenField ID="hdfDbCmdPara" runat="server" />
                            <asp:HiddenField ID="hdfInsertQueryPara" runat="server" />
                            <asp:HiddenField ID="hidintellegence" runat="server" />
                            <asp:HiddenField ID="hdfOfflineTablesUsed" runat="server" />
                            <asp:HiddenField ID="hdfAutoboxMannulPara" runat="server" />
                            <asp:HiddenField ID="hdfOfflineColumns" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>
        <%--</section>--%>
    </div>
    <div id="SubProcDbCmdAddPara">
        <div>
            <div id="AddParameter" class="AddParameter" style="margin-left: -5px;">
                <div style="">
                    <div class="QPHeader">
                        <div class="QPTextHeader" align="center">
                            <div class="QPHeaderText">
                                Parameter
                            </div>
                        </div>
                        <div class="QPDelete">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div id="AddParaDiv" class="QPContent">
                    </div>
                    <div class="clear">
                    </div>
                    <div class="QPContent">
                        <div class="QPContentRow">
                            <div class="QPContentText">
                                <input id="txtAddParaName" type="text" class="txtQPOption" maxlength="30" />
                            </div>
                            <div class="QPContentDelete" align="center">
                                <img id="imgAddQueryPara" alt="" src="//enterprise.mficient.com/images/add.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcAddOfflineTables" class="hide">
        <div class="ProcCanvasAddDpOption" visible="true" class="Scrollable">
            <table class="detailsTable">
                <thead>
                    <th>
                        Offline Tables
                    </th>
                    <th>
                    </th>
                </thead>
                <tbody id="offlineTablesDiv">
                </tbody>
            </table>
        </div>
        <div class="clear">
        </div>
        <div class="SubProcborderDiv">
            <div class="SubProcBtnDiv" align="center">
                <div style="text-align: center; margin-top: 15px;">
                    <input type="button" id="btnSaveOfflineTables" value="  Save  " class="InputStyle"
                        onclick="saveSelectedOfflineDataTables();return false;" />
                </div>
            </div>
        </div>
    </div>
    <div id="divCopyObject" class="hide" style="height: 100px">
        <div>
            <div class="DbCmdRow">
                <div class="DbConnLeftDiv">
                    Object Name
                </div>
                <div class="DbConnRightDiv">
                    <asp:TextBox ID="txt_CopyObject" runat="server" class="InputStyle" Width="80%"></asp:TextBox>
                </div>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <asp:UpdatePanel runat="server" ID="updSaveCopiedObject">
                        <ContentTemplate>
                            <asp:Button ID="btnCopyObject" runat="server" Text="  Yes  " OnClientClick="return validateCopiedObject();"
                                OnClick="CommandCopy_Save_Click" CssClass="InputStyle" />
                            <input type="button" value="  No  " onclick="ShowCopyObjectModalPopup(false);" class="InputStyle" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <div id="SubProcBoxMessage">
        <div>
            <div class="MessageDiv">
                <a id="a2">Please check following points : </a>
                <br />
            </div>
            <div style="margin-top: 5px">
                <a id="aMessage"></a>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="btnOkErrorMsg" type="button" value="  OK  " onclick="SubProcBoxMessage(false);"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcConfirmBoxMessage">
        <div>
            <div>
                <div class="ConfirmBoxMessage1">
                    <a id="aCFmessage"></a>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <input id="btnCnfObjectSave" type="button" value="   OK   " onclick="SubProcConfirmBoxMessage(false);"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcDeleteCmdConfrmation">
        <div>
            <div class="MarginT10">
            </div>
            <div class="MessageDiv">
                <a id="aMsgCmdDelCmf"></a>
            </div>
            <div class="MarginT10">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <asp:Button ID="btnDeleteObject" runat="server" Text="  Yes  " OnClientClick="SubProcDeleteCmdConfrmation(false,'');"
                        OnClick="lnkDelete_Click" CssClass="InputStyle" />
                    <input type="button" value=" No " onclick="SubProcDeleteCmdConfrmation(false,'');"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="dialogPara">
        <div id="ShowDnCmdParaDiv" style="max-height: 200px; overflow-y: scroll;">
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);

        function application_init() {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>

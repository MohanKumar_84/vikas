﻿<%@ Page Title="mFicient | Mobility Settings" Language="C#" MasterPageFile="~/master/Canvas.master"
    AutoEventWireup="true" CodeBehind="Setting.aspx.cs" Inherits="mFicientCP.Setting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
 <link href="css/IdeDialog.css" rel="stylesheet" type="text/css" />
<style type="text/css">
   #tblAddProp td, #tblDdlOption td, #tblAuthentication td
        {
            text-align: left;
            vertical-align: middle;
        }
        #tblAddProp td:last-child, #tblAuthentication td:last-child
        {
            text-align: right;
            vertical-align: middle;
            width: 45px;
        }
        .SubProcBtnMrgn1
        {
            margin-top: 7px;
            width: 100%;
            text-align: center;
        }
        .Intext
        {
            float: left;
            margin-top: 4px;
        }
        .btnstyle
        {
           float:right;
            padding-right:10px;          
           
        }
        
  #tblAddProp th
  {
      text-align:left;
  }
    </style>
    <script src="Scripts/jquery.dataTables.js" type="text/javascript"></script>
    <script src="Scripts/Custompropertysetting.js" type="text/javascript"></script>
    <script type="text/javascript">
        var TabEnum = {
            DEVICE: 1,
            DATA_COMMAND: 2,
            ACTIVE_DIR_SETTINGS: 3,
            TESTER_USER: 4,
            CUS_PROPETY: 5
        }
        function makeTextBoxAutoComplete() {
            var companyId = document.getElementById('<%=hidCId.ClientID %>').value.toString();
            var hidAutoCmpltAllUserList = $('#' + '<%=hidAutoCmpltAllUserList.ClientID %>');
            var hidAlreadySelectedVIPUsers = $('#' + '<%=hidAlreadySelectedVIPUsers.ClientID %>');

            if (hidAutoCmpltAllUserList && $(hidAutoCmpltAllUserList).val()) {
                var lstUserDetail = JSON.parse($(hidAutoCmpltAllUserList).val());
            }
            if (hidAlreadySelectedVIPUsers && $(hidAlreadySelectedVIPUsers).val()) {
                var lstAlreadySelectdVIPUsers = JSON.parse($(hidAlreadySelectedVIPUsers).val());
            }
            if (lstUserDetail) {
                if (lstAlreadySelectdVIPUsers) {
                    $("#" + "<% = txtAutoCompleteUserName.ClientID %>").tokenInput(lstUserDetail, {
                        theme: "facebook",
                        preventDuplicates: true,
                        prePopulate: lstAlreadySelectdVIPUsers
                    });
                }
                else {
                    $("#" + "<% = txtAutoCompleteUserName.ClientID %>").tokenInput(lstUserDetail, {
                        theme: "facebook",
                        preventDuplicates: true
                    });
                }
            }

        }
        function makeUserTextBoxAutoComplete() {
            var hidAutoCmpltAllUserList = $('#' + '<%=hidAutoCmpltAllUserList.ClientID %>');
            var hidAlreadySelectedUsers = $('[id$=hidexistingautoappoveduserlist]').val();
            if (hidAutoCmpltAllUserList && $(hidAutoCmpltAllUserList).val()) {
                var lstUserDetail = JSON.parse($(hidAutoCmpltAllUserList).val());
            }
            if (hidAlreadySelectedUsers && hidAlreadySelectedUsers != '') {
                var lstAlreadySelectdUsers = JSON.parse(hidAlreadySelectedUsers);
            }
            if (lstUserDetail) {
                if (hidAlreadySelectedUsers) {
                    $("#" + "<% = txtuser.ClientID %>").tokenInput(lstUserDetail, {
                        theme: "facebook",
                        preventDuplicates: true,
                        prePopulate: lstAlreadySelectdUsers
                    });
                }
                else {
                    $("#" + "<% = txtuser.ClientID %>").tokenInput(lstUserDetail, {
                        theme: "facebook",
                        preventDuplicates: true
                    });
                }
            }

        }

        function makeUsergroupAutoComplete() {
            var hidAutoCmpltAllGroup = $('#' + '<%=hidgroupid.ClientID %>');
            var hidAlreadySelectedGroup = ''
            hidAlreadySelectedGroup = $('[id$=hidexistingautoappovedgrouplist]').val();
            if (hidAutoCmpltAllGroup && $(hidAutoCmpltAllGroup).val()) {
                var lstgrpDetail = JSON.parse($(hidAutoCmpltAllGroup).val());
            }
            if (hidAlreadySelectedGroup && hidAlreadySelectedGroup != '') {
                var lstAlreadySelectdgroup = JSON.parse(hidAlreadySelectedGroup);
            }
            if (lstgrpDetail) {
                if (hidAlreadySelectedGroup) {
                    $("#" + "<% = txtgroups.ClientID %>").tokenInput(lstgrpDetail, {
                        theme: "facebook",
                        preventDuplicates: true,
                        prePopulate: lstAlreadySelectdgroup
                    });
                }
                else {
                    $("#" + "<% = txtgroups.ClientID %>").tokenInput(lstgrpDetail, {
                        theme: "facebook",
                        preventDuplicates: true
                    });
                }
            }
        }

        function fillUserIdSelected() {
            var companyId = document.getElementById('<%=hidCId.ClientID %>').value.toString()
            var autoCompleteTextBoxValue = document.getElementById('<% = txtAutoCompleteUserName.ClientID %>').value.toString();
            var hidUserId = document.getElementById('<% = hidUserId.ClientID %>');
            hidUserId.value = autoCompleteTextBoxValue;
        }
        function makeTabAfterPostBack() {
            $('#content').find('div.tab').tabs({
                fx: {
                    opacity: 'toggle',
                    duration: 'fast'
                }
            });
        }

        function storeSelectedTabIndex(index) {
            var hidSelectedTabIndex = document.getElementById('<%=hidTabSelected.ClientID %>');
            hidSelectedTabIndex.value = '';
            hidSelectedTabIndex.value = index;
        }
        function showSelectedTabOnPostBack() {
             
            var hidSelectedTabIndex = document.getElementById('<%=hidTabSelected.ClientID %>');
            //this is the first tab and associated div
            var lstDeviceAccount = $('#lstDeviceAccount');
            var divDeviceSetting = $('#DeviceSetting');
            //this is the second tab and associated div
            var lstDataCommandSetting = $('#lstDataCommandSetting');
            var divDataCommandSetting = $('#DataCommandSettingsDiv');

            //this is the third tab and associated div
            var lstActiveDirUsrSetting = $('#lstActiveDirUsrSetting');
            var divActvDirUserSetting = $('#ActiveDirUserSetting');

            //this is the fouth tab and associated div
            var lstTesterUserSetting = $('#lstTesterUser');
            var divTestActvDirUserSetting = $('#TestUser');

            //this is the fIFTH tab and associated div
            var lstCustomSetting = $('#lstcustom');
            var divcustomrSetting = $('#divcustome');


            switch (parseInt(hidSelectedTabIndex.value, 10)) {
                case TabEnum.DEVICE:
                    lstDeviceAccount
                    .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");

                    lstDataCommandSetting
                    .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

                    lstActiveDirUsrSetting
                    .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

                    lstTesterUserSetting
                    .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

                    lstCustomSetting.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                  .addClass("ui-state-default ui-corner-top");

                    divDeviceSetting.removeClass("ui-tabs-hide");
                    divDataCommandSetting.addClass("ui-tabs-hide");
                    divActvDirUserSetting.addClass("ui-tabs-hide");
                    divTestActvDirUserSetting.addClass("ui-tabs-hide");
                    divcustomrSetting.addClass("ui-tabs-hide");

                    break;
                case TabEnum.DATA_COMMAND:
                    lstDataCommandSetting.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");

                    lstDeviceAccount.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");
                    lstTesterUserSetting.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

                    lstActiveDirUsrSetting
                    .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");
                    lstCustomSetting.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                  .addClass("ui-state-default ui-corner-top");


                    divDataCommandSetting.removeClass("ui-tabs-hide");
                    divDeviceSetting.addClass("ui-tabs-hide");
                    divActvDirUserSetting.addClass("ui-tabs-hide");
                    divTestActvDirUserSetting.addClass("ui-tabs-hide");
                    divcustomrSetting.addClass("ui-tabs-hide");
                    break;
                case TabEnum.ACTIVE_DIR_SETTINGS:
                    lstActiveDirUsrSetting
                    .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");

                    lstDataCommandSetting.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

                    lstDeviceAccount.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

                    lstTesterUserSetting.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

                    lstCustomSetting.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                  .addClass("ui-state-default ui-corner-top");

                    divActvDirUserSetting.removeClass("ui-tabs-hide");
                    divDataCommandSetting.addClass("ui-tabs-hide");
                    divDeviceSetting.addClass("ui-tabs-hide");
                    divTestActvDirUserSetting.addClass("ui-tabs-hide");
                    divcustomrSetting.addClass("ui-tabs-hide");
                    break;
                case TabEnum.TESTER_USER:
                    lstTesterUserSetting
                     .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");


                    lstDataCommandSetting.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

                    lstDeviceAccount.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

                    lstActiveDirUsrSetting
                    .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

                    lstCustomSetting.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                  .addClass("ui-state-default ui-corner-top");


                    divDataCommandSetting.addClass("ui-tabs-hide");
                    divDeviceSetting.addClass("ui-tabs-hide");
                    divActvDirUserSetting.addClass("ui-tabs-hide");
                    divTestActvDirUserSetting.removeClass("ui-tabs-hide");
                    divcustomrSetting.addClass("ui-tabs-hide");
                    break;
                case TabEnum.CUS_PROPETY:

                    lstTesterUserSetting
                     .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

                    lstDataCommandSetting.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

                    lstDeviceAccount.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");

                    lstActiveDirUsrSetting
                    .removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top");
                    lstCustomSetting.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active")
                    .addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");

                    divDataCommandSetting.addClass("ui-tabs-hide");
                    divDeviceSetting.addClass("ui-tabs-hide");
                    divActvDirUserSetting.addClass("ui-tabs-hide");
                    divTestActvDirUserSetting.addClass("ui-tabs-hide");
                    divcustomrSetting.removeClass("ui-tabs-hide");
            }
        }
        function confirmDelete(sender, msg) {
            var confirmMsg = confirm(msg);
            if (confirmMsg) {
                isCookieCleanUpRequired('false');
                return true;
            }
            else {
                return false;
            }
        }
        function confirmUncheckingAllowActDir() {
            var chkAllowActDirUser = $('#' + '<%=chkAllowActiveDirUsers.ClientID %>');
            if ($(chkAllowActDirUser).is(':checked')) {
                return true;
            } else {
                var confirmMsg =
               confirm('All the account settings will be deleted. Are you sure you want to proceed.');
                if (confirmMsg) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        $(document).ready(function () {
            LoadProperyPage();
            // Uniform();

        });


        function Uniform() {
            CallUnformCss($('#tblAddProp:last tr:last td:eq(2) select'));
        }

    </script>
   
    <script type="text/javascript">

        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Are you sure you want to save the user as tester?")) {
                confirm_value.value = "Yes";
                return true;
            } else {
                confirm_value.value = "No";
                return false;
            }
            document.forms[0].appendChild(confirm_value);
        }

        function Onchange() {
            $('[id$=drpuserid]').parent().children("span").css('width', '150px');

        }

        function UpdateUpdBtnRowIndex() {
            $('[id$=<%= btnDbConnRowIndex.ClientID %>]').click();
        }

    </script>
    <script type="text/javascript">
        function fillUserIdwithgroupSelected() {
            var companyId = document.getElementById('<%=hidCId.ClientID %>').value.toString()
            var autoCompleteTextBoxValue1 = document.getElementById('<% = txtuser.ClientID %>').value.toString();
            var groupid = document.getElementById('<% = txtgroups.ClientID %>').value.toString();
            $('[id$=hidUserIdautoapproval]').val(autoCompleteTextBoxValue1);
            $('[id$=hidgroupIdautoapproval]').val(groupid);
        }
    </script>
    <style type="text/css">
        .settingActionButton
        {
            padding: 0px;
            margin-top: 36px;
            margin-left: 4px;
        }
        div.token-input-dropdown-facebook
        {
            width: 46.9%;
        }
        
        ul.token-input-list-facebook
        {
            margin: 0px 0px 0px 0px;
            width: 100%;
        }
        div.agntDmnDdl div.selector > span
        {
            width: 200px;
        }
        
        
.aspButton
{
    background-color: #E6E6E6;
    border-color: #C7C7C7 #B2B2B2 #B2B2B2 #C7C7C7;
    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.6) inset, 0 2px 5px rgba(255, 255, 255, 0.5) inset, 0 -2px 5px rgba(0, 0, 0, 0.1) inset;
    color: #555555;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.7);
    width: auto !important;
    background-position: 4px 5px;
    background-repeat: no-repeat;
    border-radius: 4px 4px 4px 4px;
    cursor: pointer;
    display: inline-block;
    font-size: 11px;
    font-weight: 700; /*margin: 2px;*/
    margin: 6px; /*4/10/2012*/
    min-height: 22px;
    min-width: 8px;
    outline: 0 none; /*padding: 9px 12px;*/ /*4/10/2012*/
    padding: 7px 15px;
    position: relative;
    text-align: center;
    text-decoration: none;
    text-transform: uppercase;
    vertical-align: baseline;
    white-space: pre-line;
    font-family: 'PT Sans' ,sans-serif;
    margin-right: 0px; /*4/10/2012*/
}
        
        .display
        {
            text-align:left;
            float:left;
        }
  
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="updContainer" runat="server">
        <ContentTemplate>
            <div id="PageCanvasContent" style="display:none;">
                <div id="divAccountSettings">
                    <div class=" widget" id="widget_tabs">
                        <h3 class="handle">
                            Account Settings</h3>
                        <div id="divTab" class="tab">
                            <ul id="tabList">
                                <li id="lstDeviceAccount"><a id="lnkDeviceAccount" href="#DeviceSetting" onclick="storeSelectedTabIndex(TabEnum.DEVICE);">
                                    Device</a></li>
                                <li id="lstDataCommandSetting" style="display:none;"><a id="lnkDataCommandSetting" href="#DataCommandSettingsDiv"
                                    onclick="storeSelectedTabIndex(TabEnum.DATA_COMMAND);">Data Objects</a></li>

                                <li id="lstcustom"><a id="lstcustomsetting" href="#divcustome"
                                    onclick="storeSelectedTabIndex(TabEnum.CUS_PROPETY);">Custom Property</a></li>

                                <li id="lstActiveDirUsrSetting"><a id="lnkActvDirSetting" href="#ActiveDirUserSetting"
                                    onclick="storeSelectedTabIndex(TabEnum.ACTIVE_DIR_SETTINGS);">Active Directory User</a></li>
                                <li id="lstTesterUser"><a id="lnkTesterUser" href="#TestUser" onclick="storeSelectedTabIndex(TabEnum.TESTER_USER);">
                                    Tester User</a></li>
                            </ul>
                            <div id="DeviceSetting">
                                <div style="margin: auto; width: 95%; margin-top: 10px; margin-bottom: 35px;">
                                    <div class="searchRow g12" style="padding: 0px;">
                                        <div class="g12">
                                            <h6 style="font-weight: bold">
                                                Set Max Devices</h6>
                                            <div>
                                                <asp:Label ID="lblMaxDevice" runat="server" Text="Max device per user :" CssClass="label"
                                                    AssociatedControlID="ddlMaxDevicePerUser"></asp:Label>
                                            </div>
                                            <div>
                                                <asp:DropDownList ID="ddlMaxDevicePerUser" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="g12" style="width: 100%; border: none; padding-left: 0px; padding-right: 0px;
                                                text-align: center;">
                                            <asp:Button ID="btnMaxDeviceSave" runat="server" Text="Save" OnClick="btnMaxDeviceSave_Click" />
                                        </div>
                                    </div>
                                    <div id="divVIPUsers" style="margin: auto; width: 100%; margin-top: 10px; margin-bottom: 5px;">
                                        <div class="searchRow g12" style="padding: 0px 0px 0px 0px">
                                            <div class="g12">
                                                <h6 style="font-weight: bold">
                                                    Select VIP Users</h6>
                                            </div>
                                            <div class="g12" style="padding-top: 0px;">
                                                <div style="float: left">
                                                    <asp:Label ID="lblVIPUsers" runat="server" Text="VIP users :" CssClass="label" AssociatedControlID="ddlMaxDevicePerUser"></asp:Label>
                                                </div>
                                                <div style="float: left; margin-left: 25px; width: 85%">
                                                    <asp:TextBox ID="txtAutoCompleteUserName" runat="server" Width="50%"></asp:TextBox>
                                                </div>
                                                <div class="clear" style="height: 0px;">
                                                </div>
                                            </div>
                                            <div class="g12" style="width: 100%; border: none; padding-left: 0px; padding-right: 0px;
                                                text-align: center;">
                                                <asp:Button ID="btnSaveVipUsers" runat="server" Text="Save" OnClick="btnSaveVipUsers_Click"
                                                    OnClientClick="fillUserIdSelected();" />
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                               
                                    <div id="div1" style="margin: auto; width: 100%;  margin-top: 10px; margin-bottom: 5px;">
                                        <div class="searchRow g12" style="padding: 0px 0px 0px 0px;height:200px;">
                                            <div class="g12">
                                                <h6 style="font-weight: bold">
                                                    Device Auto Approved</h6>
                                            </div>
                                            <div class="g12" style="padding-top: 0px;">
                                                <div style="float: left">
                                                    <asp:Label ID="Label2" runat="server" Text="Users :" CssClass="label"></asp:Label>
                                                </div>
                                                <div style="float: left; margin-left: 45px; width: 85%">
                                                    <asp:TextBox ID="txtuser" runat="server" Width="50%"></asp:TextBox>
                                                </div>
                                                <div class="clear" style="height: 0px;">
                                                </div>
                                            </div>
                                            <div class="g12" style="padding-top: 0px;">
                                                <div style="float: left">
                                                    <asp:Label ID="Label3" runat="server" Text="Groups :" CssClass="label"></asp:Label>
                                                </div>
                                                <div style="float: left; margin-left: 40px; width: 85%">
                                                    <asp:TextBox ID="txtgroups" runat="server" Width="50%"></asp:TextBox>
                                                </div>
                                                <div class="clear" style="height: 0px;">
                                                </div>
                                            </div>
                                            <div class="g12" style="padding-top: 0px;">
                                                <div style="float: left">
                                                    <asp:Label ID="Label4" runat="server" Text="Setting :" CssClass="label"></asp:Label>
                                                </div>
                                                <div style="float: left; margin-left: 30px; width: 85%">
                                                <asp:DropDownList ID="drpregistrationsetting" runat="server">
                                                <asp:ListItem Value="0">Approved all device</asp:ListItem>
                                                <asp:ListItem Value="1">Remove oldest register device</asp:ListItem>
                                                </asp:DropDownList>
                                             
                                                </div>
                                                <div class="clear" style="height: 0px;">
                                                </div>
                                            </div>

                                            <div id="divButtonCont" style="width: 100%; border: none; padding-left: 0px; padding-right: 0px; padding-bottom:5px;
                                                text-align: center;">
                                                <asp:Button ID="btnautoapprove" runat="server"   Text="Save" OnClientClick="fillUserIdwithgroupSelected();" OnClick="btn_saveapprovalldetails" />
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="searchRow g12" style="padding: 0px;">
                                        <div class="g12">
                                            <h6 style="font-weight: bold">
                                                Auto Login</h6>
                                            <div>
                                                <asp:Label ID="lblAutoLogin" runat="server" Text="Mobile App Auto Login :" CssClass="label"
                                                    AssociatedControlID="chkAutoLogin"></asp:Label>
                                            </div>
                                            <div>
                                                <asp:Checkbox ID="chkAutoLogin" runat="server" style="position:relative;top:4px;" AutoPostBack="true" OnCheckedChanged="chkAutoLogin_CheckedChanged">
                                                </asp:Checkbox>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <div id="DataCommandSettingsDiv">
                                <div style="margin: auto; width: 95%; margin-top: 10px; margin-bottom: 35px;">
                                    <div class="searchRow g12" style="padding: 0px;">
                                        <div class="g5">
                                            <h6 style="font-weight: bold">
                                                Data Object Settings</h6>
                                            <div style="margin-top: 10px;">
                                                <asp:CheckBox ID="chkAllowDeleteCommand" Style="margin-left: 8px;" Text="Allow delete objects"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="g5 settingActionButton">
                                            <asp:Button ID="btnAllowDelCmdSave" runat="server" Text="Save" OnClick="btnAllowDelCmdSave_Click" />
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <div id="ActiveDirUserSetting">
                                <div style="margin: auto; width: 95%; margin-top: 10px; margin-bottom: 35px;">
                                    <div class="searchRow g12" style="padding: 0px;">
                                        <div class="g5">
                                            <h6 style="font-weight: bold">
                                                Active Directory Users Settings</h6>
                                            <div style="margin-top: 10px;">
                                                <asp:CheckBox ID="chkAllowActiveDirUsers" Style="margin-left: 8px;" Text="Allow active directory users"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="g5 settingActionButton">
                                            <asp:Button ID="btnAllowActvDirUserSave" runat="server" Text="Save" OnClientClick="return confirmUncheckingAllowActDir();"
                                                OnClick="btnAllowActvDirUserSave_Click" />
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div class="g8">
                                            <asp:Panel ID="pnlActiveDirSettingsRptBox" CssClass="repeaterBox" runat="server"
                                                Style="margin: auto; width: 99%; margin-bottom: 5px;" Visible="false">
                                                <asp:Panel ID="pnlActvDirRptHeader" CssClass="repeaterBox-header" runat="server">
                                                    <div>
                                                        <asp:Label ID="lblActvDirRptHeader" runat="server" Text="<h1>Active Directory Settings</h1>"></asp:Label>
                                                    </div>
                                                    <div style="position: relative; top: 10px; right: 15px;">
                                                        <asp:LinkButton ID="lnkActvDirAddNewDomain" runat="server" Text="add" CssClass="repeaterLink fr"
                                                            OnClick="lnkActvDirAddNewDomain_Click" OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                                    </div>
                                                    <div style="height: 0px; clear: both">
                                                    </div>
                                                </asp:Panel>
                                                <asp:Repeater ID="rptActvDirSettings" runat="server" OnItemCommand="rptActvDirSettings_ItemCommand">
                                                    <HeaderTemplate>
                                                        <table class="repeaterTable">
                                                            <thead>
                                                                <tr>
                                                                    <th style="display: none">
                                                                        DOMAIN ID
                                                                    </th>
                                                                    <th>
                                                                        mPlugin Agent
                                                                    </th>
                                                                    <th>
                                                                        Domain
                                                                    </th>
                                                                    <th>
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tbody>
                                                            <tr class="repeaterItem">
                                                                <td style="display: none">
                                                                    <asp:Label ID="lblDomainId" runat="server" Text='<%# Eval("DOMAIN_ID")%>'></asp:Label>
                                                                    <asp:Label ID="lblMpluginId" runat="server" Text='<%# Eval("MPLUGIN_ID")%>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblMplugin" runat="server" Text='<%#Eval("MP_AGENT_NAME") %>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblDomainName" runat="server" Text='<%# Eval("DOMAIN_NAME") %>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CssClass="repeaterLink" CommandArgument='<%# Eval("DOMAIN_ID")%>'
                                                                        CommandName="Edit" OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                                                    &nbsp;
                                                                    <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CssClass="repeaterLink"
                                                                        CommandArgument='<%# Eval("DOMAIN_ID")%>' CommandName="Delete" OnClientClick="return confirmDelete(this,'Are you sure you want to delete the setting.');"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <tbody>
                                                            <tr class="repeaterAlternatingItem">
                                                                <td style="display: none">
                                                                    <asp:Label ID="lblDomainId" runat="server" Text='<%# Eval("DOMAIN_ID") %>'></asp:Label>
                                                                    <asp:Label ID="lblMpluginId" runat="server" Text='<%# Eval("MPLUGIN_ID") %>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblMplugin" runat="server" Text='<%#Eval("MP_AGENT_NAME") %>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblDomainName" runat="server" Text='<%# Eval("DOMAIN_NAME") %>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CssClass="repeaterLink" CommandArgument='<%# Eval("DOMAIN_ID")%>'
                                                                        CommandName="Edit" OnClientClick="isCookieCleanUpRequired('false');"></asp:LinkButton>
                                                                    &nbsp;
                                                                    <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CssClass="repeaterLink"
                                                                        CommandArgument='<%# Eval("DOMAIN_ID")%>' CommandName="Delete" OnClientClick="return confirmDelete(this,'Are you sure you want to delete the setting.');"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </AlternatingItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <div id="TestUser">
                                <div style="margin: auto; width: 95%; margin-top: 10px; margin-bottom: 35px;">
                                    <div class="searchRow g12" style="padding: 0px;">
                                        <div class="g12">
                                            <h6 style="font-weight: bold">
                                                Tester User</h6>
                                            <div style="">
                                                <asp:Label ID="Label1" runat="server" Text="User :" CssClass="label"></asp:Label>
                                            </div>
                                           
                                                <asp:DropDownList ID="drpuserid" runat="server" Width="250px" onchange="Onchange();">
                                                </asp:DropDownList>
                        <asp:Button ID="btntest" runat="server" Text="Save" OnClientClick = "return Confirm()" style="float:right; margin-top:6px; margin-right:400px;"  onclick="btntest_Click"  />

                                         

                                        </div>
                                    
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>

                            <div id="divcustome">
                            <div style="margin: auto; width: 95%; margin-top: 10px; margin-bottom: 35px;">
                                    <div class="searchRow g12" style="padding: 0px;">
                                        <div class="g12">
                                            <h6 style="font-weight: bold">
                                               Custom Property </h6>
                                        </div>
                                    


                                       <div id="AdditionalProreties">
                                       <%-- <div style="min-height: 300px; margin: auto; width: 95%; margin-top: 10px; margin-bottom: 35px;">--%>
                                            <div id="AddProp">
                                            </div>
                                        <%--</div>--%>
                                    </div>
                                    </div>





                                    <div class="clear">
                                    </div>
                                </div>
                            
                            </div>



                        </div>
                    </div>
                </div>
            </div>
            <div>
                <asp:HiddenField ID="hidCId" runat="server" />
                <asp:HiddenField ID="hidUserId" runat="server" />
                <asp:HiddenField ID="hidTabSelected" runat="server" />
                <asp:HiddenField ID="hidAutoCmpltAllUserList" runat="server" />
                <asp:HiddenField ID="hidAlreadySelectedVIPUsers" runat="server" />
                <asp:HiddenField ID="hidgroupid" runat="server" />
                <asp:HiddenField ID="hidUserIdautoapproval" runat="server" />
                <asp:HiddenField ID="hidgroupIdautoapproval" runat="server" />
                <asp:HiddenField ID="hidexistingautoappoveduserlist" runat="server" />
                <asp:HiddenField ID="hidexistingautoappovedgrouplist" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divSelectAgentAndDomain" style="display: none;">
        <asp:UpdatePanel runat="server" ID="updSelectAgntAndDomain">
            <ContentTemplate>
                <div id="divSelectAgntDomError">
                </div>
                <div class="agntDmnDdl">
                    <div class="modalPopUpLabel">
                        <asp:Label ID="lblForMplugAgnt" runat="server" Text="Mplugin Agents"></asp:Label>
                    </div>
                    <div>
                        <asp:DropDownList ID="ddlMpluginAgents" runat="server" OnSelectedIndexChanged="ddlMpluginAgents_SelectedIndexChanged"
                            AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="agntDmnDdl" style="margin-top: 15px;">
                    <div class="modalPopUpLabel">
                        <asp:Label ID="lblForDomains" runat="server" Text="Domains"></asp:Label>
                    </div>
                    <div>
                        <asp:DropDownList ID="ddlDomains" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div>
                    <div id="divActDirSettingsActionBtns" style="text-align: center; margin-top: 15px;">
                        <asp:Button ID="btnActDirSttngsSave" runat="server" Text="Save" OnClick="btnActDirSttngsSave_Click" />&nbsp;&nbsp;
                        <asp:Button ID="btnActDirSttngsCancel" runat="server" Text="Cancel" OnClientClick="closeModalPopUp('divSelectAgentAndDomain');return false;" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="divModelCredential" style="display: none">
                <div id="divModelCredentialError">
                </div>
                <div class="modalPopUpDetRowo" style="width: auto">
                    <div class="clear">
                    </div>
                    <div id="divNameCr" class="modalPopUpDetRow" style="width: 97%;">
                        <div class="modalPopUpDetColumn1">
                            Name
                        </div>
                        <div class="modalPopUpDetColumn2">
                            :
                            <asp:Label ID="cr_name" runat="server"></asp:Label><input type="hidden" id="cr_tag" />
                        </div>
                    </div>
                    <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px;">
                        <div class="modalPopUpDetColumn1">
                            User Id :</div>
                        <div class="modalPopUpDetColumn2">
                            <input type="text" id="txt_FixedUserId" style="width: 100%" autocomplete="false" />
                        </div>
                    </div>
                    <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px;">
                        <div class="modalPopUpDetColumn1">
                            Password :</div>
                        <div class="modalPopUpDetColumn2">
                            <input type="password" name="pwd" style="width: 100%" autocomplete="false" id="txt_FixedPwd">
                        </div>
                    </div>
                    <div class="modalPopUpAction">
                        <div id="div11" class="popUpActionCont">


                            <input id="Button1" onclick="UpdateFixedForAllUserCeredential();" type="button" value="Ok"
                                style="margin-top: 10px" />
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hidusername" runat="server" />
                <asp:HiddenField ID="hidpassword" runat="server" />
            </div>

<div style="display:none">


                <div>
                    <div id="SubProcConfirmBoxMessage">
                        <div>
                            <div>
                                <div class="ConfirmBoxMessage1">
                                    <a id="aCFmessage"></a></a> <a id="aCfmMessage"></a><a id="aCfmmgs"></a>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="SubProcborderDiv">
                                <div class="SubProcBtnMrgn" align="center">
                                    <input id="btnCnfFormSave" type="button" value="   OK   " onclick="SubProcConfirmBoxMessage(false);"
                                        class="InputStyle" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel runat="server" ID="updDbConnRowIndex" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnDbConnRowIndex" EventName="Click" />
                           
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <asp:Button ID="btnDbConnRowIndex" OnClick="btnDbConnRowIndex_Click" runat="server"
                    CssClass="hide" />
               
                <asp:HiddenField ID="hidData" runat="server" />
                <asp:HiddenField ID="hidUpdateTag" runat="server" />
            </div>
<div id="divupdateCredential" style="display: none; min-height: 70px !important;">
                <div class="modalPopUpDetRow" style="width: 97%; padding-top: 10px;">
                    <div class="modalPopUpDetColumn1" style="padding-top: 5px !important; width: 75px">
                        Name :</div>
                    <div class="modalPopUpDetColumn2">
                        <input type="text" id="txtupdateCredentialname" style="width: 100%" autocomplete="false" />
                    </div>
                </div>
                <div class="modalPopUpAction" style="margin-top: 10px">
                    <div id="div2" class="popUpActionCont">
                     <%--   <input id="Button3" onclick="UpdateAuthenticationnames();" type="button" value="Ok"
                            style="margin-top: 10px" formmethod="post" />--%>

                            <asp:Button ID="btndetail" runat="server" OnClientClick="UpdateAuthenticationnames();return false;" Text="Ok" />


                    </div>
                </div>
            </div>

            <div id="divModalContainer">
                <div id="divLocContainerError">
                </div>
                <div id="divAddLocContainer" class="modalPopUpDetRowo" style="width: auto">
                    <div class="clear">
                    </div>
                    <div class="modalPopUpDetRow" style="width: 97%;">
                        <div class="modalPopUpDetColumn1">
                            Name
                        </div>
                        <div class="modalPopUpDetColumn2">
                            :
                            <asp:Label ID="lblType" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="modalPopUpDetRow" style="width: 97%;">
                        <div class="modalPopUpDetColumn1">
                            Validations
                        </div>
                        <div class="modalPopUpDetColumn2">
                            :
                            <input type="checkbox" id="CheckReq" checked="checked" />
                            &nbsp; (Required)
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div id="txtValidationDiv">
                        <div class="modalPopUpDetRow" id="txtValidationDiv1" style="width: 97%;">
                            <div class="modalPopUpDetColumn1">
                                Code
                            </div>
                            <div class="modalPopUpDetColumn2">
                                :
                            </div>
                            <div class="modalPopUpDetColumn3">
                                <select id="selTextValid" onchange="TextTypeChange();">
                                    <option value="1">Alphabets</option>
                                    <option value="2">Alphanumeric</option>
                                    <option value="3">Alphanumeric With Special Characters</option>
                                    <option value="4">Integer</option>
                                    <option value="5">Decimal</option>
                                </select>
                            </div>
                        </div>
                        <div class="modalPopUpDetRow" id="CheckNumericdiv" style="width: 97%;">
                            <div class="modalPopUpDetColumn1">
                                Allow numeric First Character</div>
                            <div class="modalPopUpDetColumn2">
                                :
                            </div>
                            <div class="modalPopUpDetColumn3">
                                <input type="checkbox" id="CheckNumeric">
                                <br>
                            </div>
                        </div>
                        <div id="CheckSpacediv" class="modalPopUpDetRow" style="width: 97%;">
                            <div class="modalPopUpDetColumn1">
                                Space Allowed
                            </div>
                            <div class="modalPopUpDetColumn2">
                                :
                            </div>
                            <div class="modalPopUpDetColumn3">
                                <input type="checkbox" id="CheckSpace">
                            </div>
                            <br>
                        </div>
                        <div id="txtSpecialCharactersdiv" class="modalPopUpDetRow" style="width: 97%;">
                            <div class="modalPopUpDetColumn1">
                                Special Characters</div>
                            <div class="modalPopUpDetColumn2">
                                :
                            </div>
                            <div class="modalPopUpDetColumn3">
                                <input type="text" id="txtSpecialCharacters" />
                            </div>
                        </div>
                    </div>
                    <div id="SelectValidationDiv">
                        <div class="modalPopUpDetColumn1" style="padding: 5px; float: none;">
                            Options
                        </div>
                        <div id="SelectoptionsDiv" style="max-height: 200px; overflow: auto; font-weight: normal;
                            color: rgb(111, 111, 111);">
                        </div>
                    </div>
                    <div class="modalPopUpAction">
                        <div id="divPopUpActionCont" class="popUpActionCont">
                        <asp:Button ID="btnsave" runat="server" OnClientClick="SaveValidation(this);return false;" Text="Save" style="margin-top: 10px" />
                           <%-- <input id="btnSave" onclick="SaveValidation(this)" type="button" value="Save" style="margin-top: 10px" />--%>
                        </div>
                    </div>
                </div>
            </div>


    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            hideWaitModal();

            isCookieCleanUpRequired('true');

        }
    </script>
</asp:Content>

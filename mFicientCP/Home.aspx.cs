﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace mFicientCP
{
    public partial class Home : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        string hfsPart5 = string.Empty;
        string hfsPart6 = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack) return;

            Literal ltlUserType = (Literal)this.Master.Master.FindControl("ltUserType");
             Literal ltFullNAME = (Literal)this.Master.Master.FindControl("ltFullNAME");
        

            if (context.Items.Contains("hfs") && context.Items.Contains("hfbid"))
            {
                hfsValue = context.Items["hfs"].ToString();
                hfbidValue = context.Items["hfbid"].ToString();
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                bool isPreviousPageIDE = false;
                if (((System.Web.UI.TemplateControl)(Page.PreviousPage)).AppRelativeVirtualPath == "~/ide.aspx")
                {
                    isPreviousPageIDE = true;
                }
                if (isPreviousPageIDE)
                {
                    hfsValue = ((HiddenField)previousPageForm.FindControl("MainCanvas").FindControl("hfs")).Value;
                    hfbidValue = ((HiddenField)previousPageForm.FindControl("MainCanvas").FindControl("hfbid")).Value;
                }
                else
                {
                    hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                    hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
                }
            }

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            if (string.IsNullOrEmpty(hfsValue)) return;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');
            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];
            int lengthA = hfsParts.Length;
            if (lengthA > 4)
            {
                hfsPart5 = hfsParts[4];
                hfsPart6 = hfsParts[5];
                context.Items["hfs5"] = hfsPart5;
                context.Items["hfs6"] = hfsPart6;
                ltFullNAME.Text = hfsPart5 + " (" + hfsPart6 + ")";
            }
            
            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;
           
            try
            {
                if (hfsPart4.Trim().ToUpper() == strUserId.Trim().ToUpper())
                {
                    try
                    {
                        CompanyTimezone.getTimezoneInfo(hfsPart3);
                    }
                    catch
                    { }
                    getCompanyAndUserDetails();
                    getModulesCountByAdminId();
                    divMain.Visible = true;
                    divSubadmin.Visible = false;
                    ltlUserType.Text = "Admin";
                    ltFullNAME.Text = "";
                    divImportantAnnouncements.Visible = false;

                }
                else
                {
                    try
                    {
                        CompanyTimezone.getTimezoneInfo(hfsPart4);
                    }
                    catch
                    { }
                    getCompanyAndUserDetails();
                    fillInformationLabels();
                    divMain.Visible = false;
                    divSubadmin.Visible = true;
                    ltlUserType.Text = "Sub Admin";
                   
                    divImportantAnnouncements.Visible = false;
                }
            }
            catch
            { }


        }

        void fillInformationLabels()
        {
            GetRequestForRegisteringDevice objRequestDtls = new GetRequestForRegisteringDevice(hfsPart4, strUserId
                , PENDING_DEVICES_ORDER_BY.RequestDate
                , SORT_TYPE.DESC);
            objRequestDtls.Process();
            DataSet dsPendingRequestDtls = objRequestDtls.ResultTables;
            if (dsPendingRequestDtls != null)
            {
                if (dsPendingRequestDtls.Tables[2] != null && dsPendingRequestDtls.Tables[2].Rows.Count > 0)
                {
                    ltapp.Text = Convert.ToString(dsPendingRequestDtls.Tables[2].Rows[0]["Total"]);
                }
                else
                {
                    ltapp.Text = "0";
                }
                if (dsPendingRequestDtls.Tables[3] != null && dsPendingRequestDtls.Tables[3].Rows.Count > 0)
                {
                    ltpnd.Text = Convert.ToString(dsPendingRequestDtls.Tables[3].Rows[0]["Total"]);
                }
                else
                {
                    ltpnd.Text = "0";
                }
                if (dsPendingRequestDtls.Tables[4] != null && dsPendingRequestDtls.Tables[4].Rows.Count > 0)
                {
                    ltall.Text = Convert.ToString(dsPendingRequestDtls.Tables[4].Rows[0]["MAX_USER"]);
                }
                else
                {
                    ltall.Text = "0";
                }
                if (dsPendingRequestDtls.Tables[6] != null && dsPendingRequestDtls.Tables[6].Rows.Count > 0)
                {
                    ltalap.Text = Convert.ToString(dsPendingRequestDtls.Tables[6].Rows[0]["REGISTERED_DEVICE_COUNT"]);
                }
                else
                {
                    ltalap.Text = "0";
                }
            }
        }

        void getModulesCountByAdminId()
        {
            GetModuleCount objModuleCount = new GetModuleCount(hfsPart4, hfsPart3);
            objModuleCount.Process();
            DataSet dsModuleCounts = objModuleCount.ResultTables;
            if (dsModuleCounts != null)
            {
                DataTable dtbl;
                if (dsModuleCounts.Tables[0].Rows.Count > 0)
                {
                    dtbl = dsModuleCounts.Tables[0];
                    ltrlSubAdmin.Text = Convert.ToString(dtbl.Rows[0]["subAdminCount"]);
                }
                if (dsModuleCounts.Tables[1].Rows.Count > 0)
                {
                    dtbl = dsModuleCounts.Tables[1];
                    ltrlUser.Text = Convert.ToString(dtbl.Rows[0]["USER_COUNT"]);
                    ltrlRegisteredDevices.Text = Convert.ToString(dtbl.Rows[0]["DEVICE_COUNT"]);
                }
                if (dsModuleCounts.Tables[2].Rows.Count > 0)
                {
                    dtbl = dsModuleCounts.Tables[2];
                    ltrlFormReportCount.Text = Convert.ToString(dtbl.Rows[0]["WORK_FLOW_COUNT"]);
                }
                if (dsModuleCounts.Tables[3].Rows.Count > 0)
                {
                    dtbl = dsModuleCounts.Tables[3];
                    ltrlDocCategoryCount.Text = Convert.ToString(dtbl.Rows[0]["MENU_CATEGORY_cnt"]);
                }
            }
        }

        void getCompanyAndUserDetails()
        {
            if (strUserId.ToUpper() == hfsPart4.ToUpper())
            {
                GetCompanyAndAdminDetail objGetCompanyAndAdminDetail = new GetCompanyAndAdminDetail(strUserId);
                objGetCompanyAndAdminDetail.Process();
                ltCompany.Text = objGetCompanyAndAdminDetail.CompanyName;
                //ltRegi.Text = objGetCompanyAndAdminDetail.RegistrationNumber;
                ltAdmin.Text = objGetCompanyAndAdminDetail.AdminName;
                ltcode.Text = hfsPart3;
                CurrentPlanDetail objCurrentPlanDetail = new CurrentPlanDetail(hfsPart3);
                objCurrentPlanDetail.Process();
                if (objCurrentPlanDetail.PlanDetails != null && objCurrentPlanDetail.PlanDetails.Rows.Count > 0)
                {
                    if (objCurrentPlanDetail.PlanDetails.TableName == "Plan")
                    {
                        ltPlanCode.Text = objCurrentPlanDetail.PlanDetails.Rows[0]["PLAN_CODE"].ToString();
                        ltpMobile.Text = objCurrentPlanDetail.PlanDetails.Rows[0]["MAX_USER"].ToString();
                        ltpWork.Text = objCurrentPlanDetail.PlanDetails.Rows[0]["MAX_WORKFLOW"].ToString();
                        DateTime dt = new DateTime(Convert.ToInt64(objCurrentPlanDetail.PlanDetails.Rows[0]["PURCHASE_DATE"]), DateTimeKind.Utc)
                                         .AddMonths(Convert.ToInt32(objCurrentPlanDetail.PlanDetails.Rows[0]["VALIDITY"]));
                        ltpVali.Text = Utilities.getCompanyLocalFormattedDate(CompanyTimezone.CmpTimeZoneInfo, dt.Ticks);
                    }
                    else
                    {
                        ltPlanCode.Text = objCurrentPlanDetail.PlanDetails.Rows[0]["PLAN_CODE"].ToString()+"(Trial)";
                        ltpMobile.Text = objCurrentPlanDetail.PlanDetails.Rows[0]["MAX_USER"].ToString();
                        ltpWork.Text = objCurrentPlanDetail.PlanDetails.Rows[0]["MAX_WORKFLOW"].ToString();
                        DateTime dt = new DateTime(Convert.ToInt64(objCurrentPlanDetail.PlanDetails.Rows[0]["PURCHASE_DATE"]), DateTimeKind.Utc)
                                         .AddMonths(Convert.ToInt32(objCurrentPlanDetail.PlanDetails.Rows[0]["VALIDITY"]));
                        ltpVali.Text = Utilities.getCompanyLocalFormattedDate(CompanyTimezone.CmpTimeZoneInfo, dt.Ticks);

                    }
                }
                else
                {

                }
            }
            else
            {
                GetCompanyAndAdminDetail objGetCompanyAndAdminDetail = new GetCompanyAndAdminDetail(hfsPart3);
                objGetCompanyAndAdminDetail.Process();
                ltComs.Text = objGetCompanyAndAdminDetail.CompanyName;
                ltAdmins.Text = objGetCompanyAndAdminDetail.AdminName;
                ltComcds.Text = hfsPart4;
                ltEmail.Text = objGetCompanyAndAdminDetail.Email;
            }
        }
    }
}
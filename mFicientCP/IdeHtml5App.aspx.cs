﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Ionic.Zip;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
namespace mFicientCP
{
    public partial class IdeHtml5App : System.Web.UI.Page
    {
        private Html5AppJson appJson = new Html5AppJson();
        private Html5App app = new Html5App();
        private string strhfs, strhfbid, subAdminid = "", companyId = "", sessionid = "", adminId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master;
                strhfs = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                strhfs = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
                app.AppId = Html5AppFileContent.GetAppId();
                hdHtml5AppObj.Value = app.AppId;
                Utilities.AddHtml5AppToCollection(app);
            }

            if (string.IsNullOrEmpty(strhfs) || string.IsNullOrEmpty(strhfbid)) return;

            context.Items["hfs"] = strhfs;
            context.Items["hfbid"] = strhfbid;
            if (!string.IsNullOrEmpty(strhfs))
            {
                string str = Utilities.DecryptString(strhfs);
                subAdminid = str.Split(',')[0];
                companyId = str.Split(',')[3].ToLower();
                sessionid = str.Split(',')[1];
                adminId = str.Split(',')[2];
            }
            if (!IsPostBack)
            {
                //BindAppsRepeater();
                hidTabSelected.Value = "1";
            }
            else
            {
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback2", true,
                    "makeTabAfterPostBack();showSelectedTabOnPostBack();");
            }
            Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback2", true,
                    "makeTabAfterPostBack();showSelectedTabOnPostBack();mfHtml5App.getApplicationName();");

            //GetSubAdminDetail getSubadminDetail = new GetSubAdminDetail(adminId, companyId);
            //getSubadminDetail.getSubAdminById(subAdminid);
            //DataTable dt = getSubadminDetail.ResultTable;
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    txtCreateBy.Text = dt.Rows[0]["FULL_NAME"].ToString();
            //    txtModifiedBy.Text = dt.Rows[0]["FULL_NAME"].ToString();
            //}

            //txtLastModifiedDate.Text = DateTime.UtcNow.ToShortDateString();
            //txtFilePath.Text = string.Empty;
        }

        //private void BindAppsRepeater()
        //{
        //    GetHtml5Apps htmlApps = new GetHtml5Apps(companyId);
        //    htmlApps.Process();
        //    if (htmlApps.ResultTable != null && htmlApps.ResultTable.Rows.Count > 0)
        //    {
        //        rptApps.DataSource = htmlApps.ResultTable;
        //        rptApps.DataBind();
        //        rptApps.Visible = true;
        //    }
        //}

        private void ExtractZipFile()
        {
            try
            {
                byte[] fileBytes = Convert.FromBase64String(hdFileBytes.Value);
                MemoryStream ms = new MemoryStream(fileBytes);
                byte[] zipEntryBytes = null;
                string ErrorMessage = string.Empty;
                string strInvalidFiles = string.Empty;
                string strInvalidFilePath = string.Empty;
                appJson = new Html5AppJson();
                app = new Html5App();
                char filePathSeparator = '/';
                using (var zip = ZipFile.Read(ms))
                {
                    app.AppId = Html5AppFileContent.GetAppId();
                    app.AppName = txtAppName.Text.Trim();
                    app.Description = txtDescription.Text.Trim();
                    app.CompanyId = companyId;
                    appJson.appid = app.AppId;
                    appJson.enid = companyId;
                    appJson.appnm = hdFileName.Value.Trim().Substring(0, hdFileName.Value.Trim().LastIndexOf('.'));
                    app.AppName = hdFileName.Value.Trim().Substring(0, hdFileName.Value.Trim().LastIndexOf('.'));
                    List<Html5AppNodeForTreeView> lstAppTree = new List<Html5AppNodeForTreeView>();
                    ZipEntry entry = zip["ZipFileDemo/scripts/testing.js"];
                    Stream str = entry.OpenReader();
                    setFileContentInHidField(getContentOfZipEntry(entry));
                    updHiddenFields.Update();
                    //System.Drawing.Image image = System.Drawing.Image.FromStream();
                    //int iWidth = 0, iHeight = 1;
                    //if (image.Width > 800 || image.Height > 600)
                    //{
                    //    Image1.Width = 800;
                    //    Image1.Height = 600;
                    //}
                    //else
                    //{
                    //    Image1.Width = image.Width;
                    //    Image1.Height = image.Height;
                    //}
                    //Image1.ImageUrl = string.Format("data:image/gif;base64,{0}", Convert.ToBase64String(ReadFully(entry.OpenReader())));
                    ////System.Drawing.Image imgToResize = System.Drawing.Image.FromStream(entry.OpenReader());
                    ////System.Drawing.Image imgResized = imgToResize;
                    ////int iOriginalWidth = imgToResize.Width;
                    ////int iOriginalHeight = imgToResize.Height;
                    ////if (iOriginalWidth > 768|| iOriginalHeight > 500)
                    ////{
                    ////    imgResized = ResizeImage(imgToResize, new Size(768, 500), true);
                    ////}
                    ////string mimeType = "";
                    ////System.Drawing.Imaging.ImageFormat imgFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
                    ////getImageMimeTypeAndFormatType(imgToResize, out mimeType, out imgFormat);
                    ////Image1.ImageUrl = string.Format("data:"+mimeType+";base64,{0}", Convert.ToBase64String(imageToByteArray(imgResized, imgFormat)));
                    //UpdatePanel1.Update();
                    foreach (ZipEntry zipEntry in zip.Entries)
                    {
                        //HTML5AppFile appFile = null;
                        //FileObject objFile = new FileObject();

                        using (var entryms = new MemoryStream())
                        {
                            string strFileNameWithPath = zipEntry.FileName;
                            string fileName = Path.GetFileName(strFileNameWithPath);
                            string extension = Path.GetExtension(strFileNameWithPath);
                            string strDirectoryName = Path.GetDirectoryName(strFileNameWithPath);
                            string strNodeText = "";
                            Html5AppNodeForTreeView objAppTree = null;
                            List<Html5AppNodeForTreeView> lstTempAppTree = new List<Html5AppNodeForTreeView>();
                            string[] tempArray;
                            lstTempAppTree = lstAppTree;
                            bool isDirectory = false;
                            tempArray = strFileNameWithPath.Split(filePathSeparator);
                            if (!zipEntry.IsDirectory)
                            {

                                for (int i = 0; i <= tempArray.Length - 2; i++)
                                {
                                    lstTempAppTree = getChildrenOfAppNode(tempArray[i], lstTempAppTree);
                                }
                                strNodeText = fileName;
                                //strNodeIcon = "imagefile";
                                //objAppTree = getAppNode(tempArray[tempArray.Length - 1], lstTempAppTree);
                                //objAppTree = new Html5AppNodeForTreeView();
                                //objAppTree.id = strFileNameWithPath;
                                //tempArray = strFileNameWithPath.Split(filePathSeparator);
                                //objAppTree.text = tempArray[tempArray.Length - 2];//check this potentially an exception throwing area.
                                //objAppTree.state = new Html5AppNodeForTreeViewState();
                                //objAppTree.children = new List<Html5AppNodeForTreeView>();
                                //objAppTree.icon = String.Empty;
                                //lstTempAppTree.Add(objAppTree);
                                #region Previous Code
                                //bool isValidFile = false;
                                //bool isValidPath = checkForValidFilePath(hdFileName.Value.Trim().Substring(0, hdFileName.Value.Trim().LastIndexOf('.')), extension, Path.DirectorySeparatorChar, Path.GetDirectoryName(file), out isValidFile);
                                //if (isValidFile && isValidPath)
                                //{
                                //    zipEntry.Extract(entryms);
                                //    zipEntryBytes = entryms.ToArray();
                                //    if (fileName.Length > 15)
                                //        objFile.filenm = fileName.Substring(0, 15) + "...." + extension;
                                //    else
                                //        objFile.filenm = fileName;
                                //    objFile.fileid = fileName;
                                //    objFile.ext = extension;
                                //    appFile = new HTML5AppFile(fileName, Convert.ToBase64String(zipEntryBytes), extension);
                                //    switch (Html5AppFileContent.GetFileTypeinZippedFile(extension))
                                //    {
                                //        case FileType.Image:
                                //            app.Imagefiles.Add(appFile);
                                //            appJson.files.image.Add(objFile);
                                //            break;
                                //        case FileType.Script:
                                //            app.Scriptfiles.Add(appFile);
                                //            appJson.files.script.Add(objFile);
                                //            break;
                                //        case FileType.Style:
                                //            app.Stylefiles.Add(appFile);
                                //            appJson.files.style.Add(objFile);
                                //            break;
                                //        case FileType.Root:
                                //            app.Rootfiles.Add(appFile);
                                //            appJson.files.root.Add(objFile);
                                //            break;
                                //    }
                                //}
                                //else if (!isValidFile)
                                //{
                                //    strInvalidFiles += "*" + fileName + "<br/>";
                                //}
                                //else if (!isValidPath)
                                //{
                                //    strInvalidFilePath += "*" + fileName + "<br/>";
                                //}
                                #endregion
                            }
                            else
                            {
                                //tempArray = strFileNameWithPath.Split(filePathSeparator);
                                strNodeText = tempArray[tempArray.Length - 2];
                                if (tempArray.Length == 2)
                                {
                                    //objAppTree = new Html5AppNodeForTreeView();
                                    //objAppTree.id = strFileNameWithPath;
                                    //objAppTree.text = tempArray[tempArray.Length - 2];//check this potentially an exception throwing area.
                                    //objAppTree.state = new Html5AppNodeForTreeViewState();
                                    //objAppTree.children = new List<Html5AppNodeForTreeView>();
                                    //objAppTree.icon = String.Empty;
                                    //lstAppTree.Add(objAppTree);
                                }
                                else
                                {
                                    for (int i = 0; i <= tempArray.Length - 3; i++)
                                    {
                                        lstTempAppTree = getChildrenOfAppNode(tempArray[i], lstTempAppTree);
                                    }

                                    //objAppTree = new Html5AppNodeForTreeView();
                                    //objAppTree.id = strFileNameWithPath;
                                    //objAppTree.text = tempArray[tempArray.Length - 2];//check this potentially an exception throwing area.
                                    //objAppTree.state = new Html5AppNodeForTreeViewState();
                                    //objAppTree.children = new List<Html5AppNodeForTreeView>();
                                    //objAppTree.icon = String.Empty;
                                    //lstTempAppTree.Add(objAppTree);
                                }
                                isDirectory = true;

                            }
                            objAppTree = new Html5AppNodeForTreeView();
                            objAppTree.id = strFileNameWithPath;
                            //tempArray = strFileNameWithPath.Split(filePathSeparator);
                            objAppTree.text = strNodeText;//check this potentially an exception throwing area.
                            objAppTree.state = new Html5AppNodeForTreeViewState();
                            objAppTree.children = new List<Html5AppNodeForTreeView>();
                            objAppTree.icon = getAppNodeIconImageByFileExtension(extension);
                            objAppTree.isDirectory = isDirectory;
                            lstTempAppTree.Add(objAppTree);
                            //lstTempAppTree.OrderBy(s => s.isDirectory);

                        }
                        //var lengths = from element in list
                        //orderby element.Length
                        //select element;

                    }
                    //lstAppTree.OrderBy(s => s.isDirectory);
                    lstAppTree = sortAppNodesForTreeView(lstAppTree);
                    hidAppTreeView.Value = Utilities.SerializeJson<List<Html5AppNodeForTreeView>>(lstAppTree);
                    updHiddenFields.Update();
                }

                //Utilities.AddHtml5AppToCollection(app);

                //if (!string.IsNullOrEmpty(strInvalidFiles) || !string.IsNullOrEmpty(strInvalidFilePath))
                //  ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"appId='" + app.AppId + @"';appJson='" + Utilities.SerializeJson<Html5AppJson>(appJson) + "';showErrorBox(true,'" + strInvalidFiles + "','" + strInvalidFilePath + "')", true);
                //else if (string.IsNullOrEmpty(strInvalidFiles) && string.IsNullOrEmpty(strInvalidFilePath))
                //  ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"appId='" + app.AppId + @"';appJson='" + Utilities.SerializeJson<Html5AppJson>(appJson) + "';createFileTree()", true);
            }
            catch
            {
            }
        }
        
        
        #region ZipFile Helpers
        
        Byte[] getDraggedFileDataFromHidField()
        {
            string fileData = hdFileBytes.Value;
            byte[] fileBytes = new byte[0];
            if (!String.IsNullOrEmpty(fileData))
            {
                fileBytes = Convert.FromBase64String(hdFileBytes.Value);
            }
            return fileBytes;
        }
        MemoryStream getDraggedFileDataAsStream(byte[] data)
        {
            MemoryStream ms = new MemoryStream(data);
            return ms;
        }
        
        ZipFile getHtml5AppFromS3Bucket(string companyId, string subAdminId)
        {
            return new ZipFile();
        }
        ZipFile getZipFileFromStream(MemoryStream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException();
            }
            return ZipFile.Read(stream);
        }
        
        
        ZipEntry getZipEntryByPath(ZipFile zip, string path)
        {
            if(zip == null || String.IsNullOrEmpty(path))throw new ArgumentNullException();
            ZipEntry entry = null;
            entry = zip[path];
            return entry;
        }
        string getContentOfZipEntry(ZipEntry zipEntry)
        {
            if (zipEntry == null) throw new ArgumentNullException();
            Stream zipEntryStream = zipEntry.OpenReader();
            string strContent = "";
            using (zipEntryStream)
            {

                //zipEntryStream.Position = 0;
                StreamReader sr = new StreamReader(zipEntryStream);
                strContent = sr.ReadToEnd();
            }
            return strContent;
        }
        void setFileContentInHidField(string value)
        {
            hidSelectedFileContent.Value = value;
        }
        ZipFile createNewZipFileForApp(string appName)
        {
            if (String.IsNullOrEmpty(appName)) throw new ArgumentNullException();
            ZipFile objNewZipFile = new ZipFile(appName);
            return objNewZipFile;
        }
        #endregion
        
        #region HTML5 Tree View Json Helpers
        
        string getHtml5AppFileJsonForTreeView(ZipFile zip)
        {
            if (zip == null)
            {
                throw new ArgumentNullException();
            }
            string strAppFileJson = "";
            List<Html5AppNodeForTreeView> lstAppNodes = getHtml5AppFileListForTreeView(zip);
            if (lstAppNodes != null)
            {
                strAppFileJson = Utilities.SerializeJson<List<Html5AppNodeForTreeView>>(lstAppNodes);
            }
            return strAppFileJson;
        }
        
        List<Html5AppNodeForTreeView> getHtml5AppFileListForTreeView(ZipFile zip)
        {
            if (zip == null)
            {
                throw new ArgumentNullException();
            }
            char filePathSeparator = '/';
            List<Html5AppNodeForTreeView> lstAppTree = new List<Html5AppNodeForTreeView>();
            using (zip)
            {
                foreach (ZipEntry zipEntry in zip.Entries)
                {
                    using (var entryms = new MemoryStream())
                    {
                        string strFileNameWithPath = zipEntry.FileName;
                        string fileName = Path.GetFileName(strFileNameWithPath);
                        string extension = Path.GetExtension(strFileNameWithPath);
                        string strDirectoryName = Path.GetDirectoryName(strFileNameWithPath);
                        string strNodeText = "";
                        Html5AppNodeForTreeView objAppTree = null;
                        List<Html5AppNodeForTreeView> lstTempAppTree = new List<Html5AppNodeForTreeView>();
                        string[] tempArray;
                        lstTempAppTree = lstAppTree;
                        bool isDirectory = false;
                        tempArray = strFileNameWithPath.Split(filePathSeparator);
                        if (!zipEntry.IsDirectory)
                        {
                            for (int i = 0; i <= tempArray.Length - 2; i++)
                            {
                                lstTempAppTree = getChildrenOfAppNode(tempArray[i], lstTempAppTree);
                            }
                            strNodeText = fileName;
                        }
                        else
                        {
                            strNodeText = tempArray[tempArray.Length - 2];
                            if (tempArray.Length == 2)
                            {
                            }
                            else
                            {
                                for (int i = 0; i <= tempArray.Length - 3; i++)
                                {
                                    lstTempAppTree = getChildrenOfAppNode(tempArray[i], lstTempAppTree);
                                }
                            }
                            isDirectory = true;
                        }
                        objAppTree = new Html5AppNodeForTreeView();
                        objAppTree.id = strFileNameWithPath;
                        objAppTree.text = strNodeText;//check this potentially an exception throwing area.
                        objAppTree.state = new Html5AppNodeForTreeViewState();
                        objAppTree.children = new List<Html5AppNodeForTreeView>();
                        objAppTree.icon = getAppNodeIconImageByFileExtension(extension);
                        objAppTree.fileType = getAppNodeFileTypeByFileExtension(extension);
                        objAppTree.isDirectory = isDirectory;
                        lstTempAppTree.Add(objAppTree);
                    }

                }
                lstAppTree = sortAppNodesForTreeView(lstAppTree);
            }
            return lstAppTree;
        }
        
        List<Html5AppNodeForTreeView> getChildrenOfAppNode(string nodeName, List<Html5AppNodeForTreeView> appNodes)
        {
            List<Html5AppNodeForTreeView> lstAppNodes = new List<Html5AppNodeForTreeView>();
            Html5AppNodeForTreeView objAppNode = null;
            foreach (Html5AppNodeForTreeView appNode in appNodes)
            {
                if (appNode.text == nodeName)
                {
                    objAppNode = appNode;
                    break;
                }
            }
            if (objAppNode != null)
            {
                lstAppNodes = objAppNode.children;
            }
            return lstAppNodes;
        }
        
        Html5AppNodeForTreeView getAppNode(string nodeName, List<Html5AppNodeForTreeView> appNodes)
        {
            Html5AppNodeForTreeView objAppNode = null;
            foreach (Html5AppNodeForTreeView appNode in appNodes)
            {
                if (appNode.text == nodeName)
                {
                    objAppNode = appNode;
                    break;
                }
            }

            return objAppNode;
        }
        
        int getAppNodeFileTypeByFileExtension(string extension)
        {
            int iFileType =0;
            switch (extension)
            {
                case ".png":
                case ".jpeg":
                case "jpg":
                case "gif":
                    iFileType = (int)Html5AppFileType.Image;
                    break;
                case ".js":
                    iFileType = (int)Html5AppFileType.Js;
                    break;
                case ".css":
                    iFileType = (int)Html5AppFileType.Css;
                    break;
                case ".htm":
                case ".html":
                    iFileType = (int)Html5AppFileType.Html;
                    break;
                case ".xml":
                    iFileType = (int)Html5AppFileType.Xml;
                    break;
                case ".txt":
                    iFileType = (int)Html5AppFileType.Txt;
                    break;
            }
            return iFileType;
        }
        
        string getAppNodeIconImageByFileExtension(string extension)
        {
            string strNodeIcon = "";
            switch (extension)
            {
                case ".png":
                case ".jpeg":
                case "jpg":
                case "gif":
                    strNodeIcon = "fileTreeImageFile";
                    break;
                case ".js":
                    strNodeIcon = "fileTreeJsFile";
                    break;
                case ".css":
                    strNodeIcon = "fileTreeCssFile";
                    break;
                case ".htm":
                case ".html":
                    strNodeIcon = "fileTreeHtmlFile";
                    break;
                case ".xml":
                    strNodeIcon = "fileTreeXmlFile";
                    break;
                case ".txt":
                    strNodeIcon = "fileTreeTxtFile";
                    break;
            }
            return strNodeIcon;
        }
        
        List<Html5AppNodeForTreeView> sortAppNodesForTreeView(List<Html5AppNodeForTreeView> appNodes)
        {
            List<Html5AppNodeForTreeView> lstTempNodes = new List<Html5AppNodeForTreeView>();
            IEnumerable<Html5AppNodeForTreeView> sortedTree;
            if (appNodes != null)
            {
                sortedTree = appNodes.OrderBy(s => s.isDirectory);
                lstTempNodes = new List<Html5AppNodeForTreeView>();
                foreach (Html5AppNodeForTreeView node in sortedTree)
                {
                    lstTempNodes.Add(node);
                }
                appNodes = lstTempNodes;
                foreach (Html5AppNodeForTreeView node in appNodes)
                {
                    if (node.isDirectory)
                    {
                        if (node.children.Count > 0)
                        {
                            lstTempNodes = sortAppNodesForTreeView(node.children);
                        }
                        node.children = lstTempNodes;
                    }
                }
            }
            return appNodes;
        }

        Html5AppNodeForTreeView getSelectedFileDataFromHidField()
        {
            string strFileSelectedJson = hidAppFileSelected.Value;
            Html5AppNodeForTreeView objAppNode = null;
            if (!String.IsNullOrEmpty(strFileSelectedJson))
            {
                objAppNode = Utilities.DeserialiseJson<Html5AppNodeForTreeView>(strFileSelectedJson);
            }
            return objAppNode;
        }
        #endregion
        
        #region Utilities
        public static byte[] streamToByteArray(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        #endregion
        #region HTML5 Uploaded Images Helper
        
        public byte[] imageToByteArray(System.Drawing.Image imageIn, System.Drawing.Imaging.ImageFormat imgFormat)
        {
            Byte[] data;
            using (MemoryStream ms = new MemoryStream())
            {
                imageIn.Save(ms, imgFormat);
                data = streamToByteArray(ms);
                data = ms.ToArray();
            }
            return data;
        }
        
        public System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
        {
            using (MemoryStream ms = new MemoryStream(byteArrayIn))
            {
                System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
                return returnImage;
            }
        }
        
        public static System.Drawing.Image ResizeImage(System.Drawing.Image image, Size size,
            bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }
            System.Drawing.Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }
        
        public void getImageMimeTypeAndFormatType(System.Drawing.Image image, out string mimeType, out System.Drawing.Imaging.ImageFormat imgFormat)
        {
            mimeType = "";
            imgFormat = null;
            try
            {
                var file_image = image;

                //list image formats
                var image_formats = typeof(System.Drawing.Imaging.ImageFormat)
                                    .GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static)
                                    .ToList().ConvertAll(property => property.GetValue(null, null));

                //get image format
                var file_image_format = typeof(System.Drawing.Imaging.ImageFormat)
                                        .GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static)
                                        .ToList().ConvertAll(property => property.GetValue(null, null))
                                        .Single(image_format => image_format.Equals(file_image.RawFormat));
                //list image codecs
                var image_codecs = System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders().ToList();

                //get image codec
                var file_image_format_codec = System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders()
                                              .ToList().Single(image_codec => image_codec.FormatID == file_image.RawFormat.Guid);
                //System.Diagnostics.Debug.WriteLine(file_image_format_codec.CodecName + ", mime: " + file_image_format_codec.MimeType + ", extension: " + file_image_format_codec.FilenameExtension, "image_codecs", "file_image_format_type");
                mimeType = file_image_format_codec.MimeType;
                imgFormat = (System.Drawing.Imaging.ImageFormat)file_image_format;
            }
            catch
            {
            }
        }
        
        #endregion
        #region S3 Helpers
        void uploadAppZipFileToS3Bucket(ZipFile zip)
        {
            //Utilities.currentZipFile = zip;
        }
        ZipFile getAppZipFileFromS3Bucket(string subAdminId, string companyid)
        {
            //return Utilities.currentZipFile;
            return new ZipFile();
        }
        #endregion

        private bool checkForValidFilePath(string baseDirectory, string fileExtension, char pathSeperator, string directoryPath, out bool isValidFile)
        {
            bool isValidPath = false;
            isValidFile = false;
            switch (fileExtension)
            {
                case ".png":
                case ".jpeg":
                case "jpg":
                case "gif":
                    if (String.Equals((baseDirectory + pathSeperator + "images"), directoryPath, StringComparison.CurrentCultureIgnoreCase))
                        isValidPath = true;
                    isValidFile = true;
                    break;
                case ".js":
                    if (String.Equals((baseDirectory + pathSeperator + "scripts"), directoryPath, StringComparison.CurrentCultureIgnoreCase))
                        isValidPath = true;
                    isValidFile = true;
                    break;
                case ".css":
                    if (String.Equals((baseDirectory + pathSeperator + "styles"), directoryPath, StringComparison.CurrentCultureIgnoreCase))
                        isValidPath = true;
                    isValidFile = true;
                    break;
                case ".htm":
                case ".html":
                case ".xml":
                case ".txt":
                    if (String.Equals((baseDirectory), directoryPath, StringComparison.CurrentCultureIgnoreCase))
                        isValidPath = true;
                    isValidFile = true;
                    break;
            }
            return isValidPath;
        }

        protected void btnPostbackByHtmlCntrl_Click(object sender, EventArgs e)
        {
            FileType fileType = Html5AppFileContent.GetFileType(hdFileName.Value.Trim().Substring(hdFileName.Value.LastIndexOf('.') + 1));
            if (fileType == FileType.Zip)
            {
                ExtractZipFile();
            }
            else if (fileType != FileType.Other)
            {
                if (string.IsNullOrEmpty(hdHtml5AppObj.Value))
                {
                    // app.AppId = Html5AppFileContent.GetAppId();
                    //ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"appId='" + app.AppId + @"'", true);
                    //Utilities.AddHtml5AppToCollection(app);
                }
                Html5App objApp = Utilities.GetHtml5AppFromCollection(hdHtml5AppObj.Value);
                HTML5AppFile objFile = new HTML5AppFile(hdFileName.Value, hdFileBytes.Value, hdFileName.Value.Trim().Substring(hdFileName.Value.LastIndexOf('.') + 1));
                switch (fileType)
                {
                    case FileType.Image:
                        objApp.Imagefiles.Add(objFile);
                        break;
                    case FileType.Script:
                        objApp.Scriptfiles.Add(objFile);
                        break;
                    case FileType.Style:
                        objApp.Stylefiles.Add(objFile);
                        break;
                    case FileType.Root:
                        objApp.Rootfiles.Add(objFile);
                        break;
                }
            }
            else
            {
                string strErrorMsgs = @"This is an invalid file." + "<br/>" + @"Only(.jpg, .jpeg, .png, .txt, .xml, .htm, .html, .js, .css) files are allowed.";
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"showErrorMsgBox(true,'" + strErrorMsgs + "')", true);
            }

            updHiddenFields.Update();
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"createFileTree()", true);
            }
            catch
            {
            }
        }

        void hideWaitModal()
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "HideWaitModal", "hideWaitModal();", true);
        }

        protected void lnkSaveApp_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtAppName.Text.Trim()) && !string.IsNullOrEmpty(txtDescription.Text.Trim()) && !string.IsNullOrEmpty(txtHomeView.Text.Trim()))
                {
                    SaveHtml5App saveApp = new SaveHtml5App(Utilities.GetHtml5AppFromCollection(hdHtml5AppObj.Value), subAdminid, (DisplayView)Convert.ToInt16(ddlShowOn.SelectedValue), txtHomeView.Text.Trim());
                    saveApp.Process();
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"createFileTree()", true);
                }
                else
                {
                    string strErrorMsgs = string.Empty;
                    if (string.IsNullOrEmpty(txtAppName.Text.Trim()))
                        strErrorMsgs += "Please enter App Name." + "<br/>";
                    if (string.IsNullOrEmpty(txtDescription.Text.Trim()))
                        strErrorMsgs += "Please enter App Description." + "<br/>";
                    if (string.IsNullOrEmpty(txtHomeView.Text.Trim()))
                        strErrorMsgs += "Please enter App HomeView." + "<br/>";

                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), Guid.NewGuid().ToString(), @"showErrorMsgBox(true,'" + strErrorMsgs + "')", true);
                }
            }
            catch
            {
            }
            finally
            {
                Utilities.RemoveHtml5AppFromCollection(hdHtml5AppObj.Value);
            }
        }
    }


    
    
}
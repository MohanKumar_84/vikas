﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using System.Collections;
using System.Reflection;

namespace mFicientCP
{
    public partial class ResourceFiles : System.Web.UI.Page
    {

        string strhfs, strhfbid, SubAdminid = "", CompanyId = "", Sessionid = "", Adminid = "", strPageMode = "", strPostbackPageMode = "";


        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            MasterPage mainMaster = Page.Master;
            if (Page.IsPostBack)
            {

                strhfs = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
                strPostbackPageMode = ((HiddenField)mainMaster.FindControl("hidAppNewOrEdit")).Value;
            }
            else
            {
                if (Page.PreviousPage==null) return;
                strPageMode = ((HiddenField)Page.PreviousPage.Master.FindControl("hidAppNewOrEdit")).Value;
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                strhfs = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                strhfbid = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
                ((HiddenField)mainMaster.FindControl("hfs")).Value = strhfs;
                ((HiddenField)mainMaster.FindControl("hfbid")).Value = strhfbid;
            }

            if (string.IsNullOrEmpty(strhfs) || string.IsNullOrEmpty(strhfbid)) return;

            context.Items["hfs"] = strhfs;
            context.Items["hfbid"] = strhfbid;

            //string ServerName = Request.ServerVariables["server_name"];
            //string PortNo = Request.ServerVariables["SERVER_PORT"];
            //string url = "http://" + ServerName + ":" + PortNo;
            hfilepath.Value = "https://s3-ap-southeast-1.amazonaws.com/mficient-images";
            Utilities.SaveInContext(context, strhfs, strhfbid, out SubAdminid, out Sessionid, out CompanyId, out Adminid);
            hfilepathdetails.Value = CompanyId;
            //if (!string.IsNullOrEmpty(strhfs))
            //{
            //    //string str = Utilities.DecryptString(strhfs);
            //    //SubAdminid = str.Split(',')[0];
            //    //CompanyId = str.Split(',')[3].ToLower();
            //    //Sessionid = str.Split(',')[1];
            //    //Adminid = str.Split(',')[2];
                
            //}
            try
            {
                ifUploadImage.Attributes["src"] = "UploadImage.aspx?id=" + strhfs;
                SetValuesInPropreties(strhfs);
                SetImagePath();
                AddImagesToDatatable();
            }
            catch { }
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ResourceOnload();", true);

        }
        private void SetValuesInPropreties(string _EncryptedValue)
        {
            string str = Utilities.DecryptString(_EncryptedValue);
            try
            {
                this.SubAdminid = str.Split(',')[0];
                this.CompanyId = str.Split(',')[3].ToLower();
                this.Sessionid = str.Split(',')[1];
                this.Adminid = str.Split(',')[2];
            }
            catch
            {
                this.SubAdminid = "";
                this.CompanyId = "";
                this.Adminid = "";
                this.Sessionid = "";
            }
        }

        private void SetImagePath()
        {
            hfilepathdetails.Value = "/companyImages/" + this.CompanyId + "/mediaFiles/";
        }

        protected void btnDeleteCfm_Click(object sender, EventArgs e)
        {

            if (hdfDelId.Value != "")
            {
                DeleteImageFile(hdfDelId.Value);
                AddImagesToDatatable();
                DeleteRowResourceTable();
                // ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), @"ResourceOnload();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alert('Image Deleted Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alert('Internal Server Error ');", true);
            }
        }

        private void DeleteImageFile(string _ImageName)
        {
            //File.Delete(Server.MapPath("~/CompanyImages/" + this.CompanyId + "/mediafiles/" + _ImageName));
            MFES3BucketDetails objS3BucketDtl = S3Bucket.GetS3BucketDetails(BucketType.MFICIENT_WEBSERVICE);
            S3BucketFileManager objS3FileManager = new S3BucketFileManager(
                    MficientConstants.MF_S3_MEDIA_FILES_BUCKET_NAME,
                    objS3BucketDtl.AccessKey,
                    objS3BucketDtl.SecretAccessKey);

            objS3FileManager.DeleteFile(
                S3Bucket.GetCompanyMediaFilesFolderPath(
                            this.CompanyId,
                            _ImageName
                 )
            );
        }
        private void AddImagesToDatatable()
        {
            hidjson.Value = "[]";
            MFES3BucketDetails objS3BucketDtl = S3Bucket.GetS3BucketDetails(BucketType.MFICIENT_WEBSERVICE);
            S3BucketFileManager objS3FileManager = new S3BucketFileManager(
                    MficientConstants.MF_S3_MEDIA_FILES_BUCKET_NAME,
                    objS3BucketDtl.AccessKey,
                    objS3BucketDtl.SecretAccessKey);

           List< List<string>> lstObjectsUploaded = objS3FileManager.GetObjectList(S3Bucket.GetCompanyMediaFilesFolderPath(this.CompanyId));
            if (lstObjectsUploaded != null && lstObjectsUploaded.Count > 0)
            {
                List<string[]> rows = new List<string[]>();
                foreach (List<string> imagePath in lstObjectsUploaded)
                {
                    rows.Add(
                            new string[]{
                                "<img src=\""+S3Bucket.GetCompleteAmazonS3Path(MficientConstants.MF_S3_MEDIA_FILES_BUCKET_NAME,imagePath[0])+"\" style=\"height:16px;\"//>",
                                imagePath[0].Substring(imagePath[0].LastIndexOf('/')+1),
                                S3Bucket.GetCompleteAmazonS3Path(MficientConstants.MF_S3_MEDIA_FILES_BUCKET_NAME,imagePath[0]),
                                imagePath[1],imagePath[2]
                            }
                    );
                }
                string strJsonWithDetails = Utilities.SerializeJson<List<string[]>>(rows);
                hidjson.Value = strJsonWithDetails;
            }
        }


        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }


        public class clsImg
        {
            public string imgurl { get; set; }
            public string imgname { get; set; }
        }

        private void DeleteRowResourceTable()
        {
            ScriptManager.RegisterStartupScript(this, typeof(UpdatePanel), Guid.NewGuid().ToString(), "DeleteSelectedRow('" + hdfDelId.Value + "');BindResoursedata();RowClick();ClickAction();", true);
        }


    }
}

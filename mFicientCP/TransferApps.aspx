﻿<%@ Page Title="mFicient | App Transfer" Language="C#" MasterPageFile="~/master/Canvas.master"
    AutoEventWireup="true" CodeBehind="TransferApps.aspx.cs" Inherits="mFicientCP.TransferApps" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
<style type="text/css">
.hide{
    display : none;
}
</style>
<script type="text/javascript">
    function CallUnformCss(_id) {
        if ($(_id).length == 0) {
            return;
        }
        if ($(_id)[0].className == "UseUniformCss") {
            $(_id).uniform();
            $(_id).removeClass("UseUniformCss");
            $(_id).addClass("UsedUniformCss");
        }
        if ($(_id).width() != 0) {
            if ($(_id).width() < 300) {
                $(_id).width($(_id).width() + 10);
            }
            //$($(_id)[0].parentElement).width('auto');
        }
        else {
        }
    }

    function UniformControlDesign() {
        CallUnformCss('[id$=<%= ddlApps.ClientID %>]');
        CallUnformCss('[id$=<%= ddlCompanies.ClientID %>]');
        CallUnformCss('[id$=<%= ddlSubadmins.ClientID %>]');
    }

    function hideControls() {
        $('#fldDbConnections').html('');
        $('#fldWSConnections').html('');
        $('#fldODataConnections').html('');
//        $('#fldOfflineDataConnections').html('');
        $('[id$=<%= pnlLinkconnections.ClientID %>]').hide();
        $('#fldDbConnections').hide();
        $('#fldWSConnections').hide();
        $('#fldODataConnections').hide();
//        $('#fldOfflineDataConnections').hide();
    }

    function initializeControls() {
        hideControls();
        $('[id$=<%= hdfDbCon.ClientID %>]').val('');
        $('[id$=<%= hdfWsCon.ClientID %>]').val('');
        $('[id$=<%= hdfODataCon.ClientID %>]').val('');
        $('[id$=<%= hdfOfflineDataTbl.ClientID %>]').val('');

        $('[id$=<%= hdfDbLinks.ClientID %>]').val('');
        $('[id$=<%= hdfWsLinks.ClientID %>]').val('');
        $('[id$=<%= hdfODataLinks.ClientID %>]').val('');
        $('[id$=<%= hdfOfflineLinks.ClientID %>]').val('');

        $('[id$=<%= hdfDbCommands.ClientID %>]').val(''); 
        $('[id$=<%= hdfWsCommands.ClientID %>]').val('');
        $('[id$=<%= hdfODataCommands.ClientID %>]').val('');
        $('[id$=<%= hdfOfflineCommands.ClientID %>]').val('');

        $('[id$=<%= hdfWfDetails.ClientID %>]').val('');

        makeSelectUniformOnPostBack();
        UniformControlDesign();
    }


    (function ($) {
        $.fn.createDataConLinksHtml = function (optns) {
            var CONSTANTS = {
                DbConnections: [],
                WsConnections: [],
                ODataConnections: [],
                OfflineDataConnections: []
            };

            hideControls();
            var options = $.extend(optns);
            if (options.DbConnections.length > 0) {
                _getDbConLinksHtml();
                if (!$('[id$=<%= pnlLinkconnections.ClientID %>]').is(':visible')) $('[id$=<%= pnlLinkconnections.ClientID %>]').show();
                $('#fldDbConnections').show();
            }
            if (options.WsConnections.length > 0) {
                _getWsConLinksHtml();
                if (!$('[id$=<%= pnlLinkconnections.ClientID %>]').is(':visible')) $('[id$=<%= pnlLinkconnections.ClientID %>]').show();
                $('#fldWSConnections').show();
            }
            if (options.ODataConnections.length > 0) {
                _getODataConLinksHtml();
                if (!$('[id$=<%= pnlLinkconnections.ClientID %>]').is(':visible')) $('[id$=<%= pnlLinkconnections.ClientID %>]').show();
                $('#fldODataConnections').show();
            }
//            if (options.OfflineDataConnections.length > 0) {
//                _getOfflineDataConLinksHtml();
//                if (!$('[id$=<%= pnlLinkconnections.ClientID %>]').is(':visible')) $('[id$=<%= pnlLinkconnections.ClientID %>]').show();
//                $('#fldOfflineDataConnections').show();
//            }

            makeSelectUniformOnPostBack();

            UniformControlDesign();
            function _getDbConLinksHtml() {
                var strDbHtml = "";
                var DbOptionCount = 1;
                var dbDrpHtml = "";
                $('#fldDbConnections').html('');
                var strDbHeaderHtml = " <label style=\"font-size:12px;text-align:center;\">"
                           + "Database Connections"
                           + "</label>";
                $.each(options.DbConnections, function () {
                    dbDrpHtml = bindCurrentCompDbConDropdown(DbOptionCount);
                    if (parseInt(dbDrpHtml.length, 10) > 0) {
                        strDbHtml += "<section>"
                                + "<label id=\"lbl_" + DbOptionCount + "_" + this["id"] + "\" for=\"" + this["id"] + "\"> " + this["name"]+ "</label>"
                                + "<div style=\"width:50%;\">"
                                + dbDrpHtml
                                + "</div></section>";

                    }
                        DbOptionCount = DbOptionCount + 1;
                });

                 if(strDbHtml.length > 0)  $('#fldDbConnections').html(strDbHeaderHtml + strDbHtml);
            }

            function _getWsConLinksHtml() {
                var strWsHtml = "";
                var WsOptionCount = 1;
                var wsDrpHtml = "";
                $('#fldWSConnections').html('');
                var strWsHeaderHtml = " <label style=\"font-size:12px;text-align:center;\">"
                           + "Webservice Connections"
                           + "</label>";
                $.each(options.WsConnections, function () {
                    wsDrpHtml = bindCurrentCompWsConDropdown(WsOptionCount);
                    if (parseInt(wsDrpHtml.length, 10) > 0) {
                        strWsHtml += "<section>"
                                + "<label id=\"lbl_" + WsOptionCount + "_" + this["id"] + "\" for=\"" + this["id"] + "\"> "
                                + this["name"]
                                + "</label>"
                                + "<div style=\"width:50%;\">"
                                + wsDrpHtml
                                + "</div>";
                    }
                    WsOptionCount = WsOptionCount + 1;
                });

               if(strWsHtml.length > 0 ) $('#fldWSConnections').html(strWsHeaderHtml + strWsHtml);
            }

            function _getODataConLinksHtml() {
                var strODataHtml = "";
                var ODataOptionCount = 1;
                var oDataDrpHtml = "";
                $('#fldODataConnections').html('');
                var strODataHeaderHtml = " <label style=\"font-size:12px;text-align:center;\">"
                           + "OData Connections"
                           + "</label>";
                $.each(options.ODataConnections, function () {
                    oDataDrpHtml = bindCurrentCompODataConDropdown(ODataOptionCount);
                    if (parseInt(oDataDrpHtml.length, 10) > 0) {
                        strODataHtml += "<section>"
                                + "<label id=\"lbl_" + ODataOptionCount + "_" + this["id"] + "\" for=\"" + this["id"] + "\"> "
                                + this["name"]
                                + "</label>"
                                + "<div style=\"width:50%;\">"
                                + oDataDrpHtml
                                + "</div>";
                            }
                    ODataOptionCount = ODataOptionCount + 1;
                });

                if(strODataHtml.length > 0 ) $('#fldODataConnections').html(strODataHeaderHtml + strODataHtml);
            }

//            function _getOfflineDataConLinksHtml() {
//                var strOfflineDataHtml = "";
//                var OfflineDataOptionCount = 1;
//                var offlineDrpHtml = "";
//                $('#fldOfflineDataConnections').html('');
//                var strOfflineDataHeaderHtml = " <label style=\"font-size:12px;text-align:center;\">"
//                           + "Offline Data Connections"
//                           + "</label>";
//                $.each(options.OfflineDataConnections, function () {
//                    offlineDrpHtml = bindCurrentCompOfflineDataConDropdown(OfflineDataOptionCount);
//                    if (parseInt(offlineDrpHtml.length, 10) > 0) {
//                        strOfflineDataHtml += "<section>"
//                                + "<label id=\"lbl_" + OfflineDataOptionCount + "_" + this["id"] + "\" for=\"" + this["id"] + "\"> "
//                                + this["name"]
//                                + "</label>"
//                                + "<div style=\"width:50%;\">"
//                                + offlineDrpHtml
//                                + "</div>";
//                            }
//                    OfflineDataOptionCount = OfflineDataOptionCount + 1;
//                });

//                if(strOfflineDataHtml.length > 0 ) $('#fldOfflineDataConnections').html(strOfflineDataHeaderHtml + strOfflineDataHtml);
//            }
        };
    })(jQuery);

    function bindCurrentCompDbConDropdown(DbCount) {
        var strDrpDwnHtml = "";
        if (parseInt($('[id$=<%= hdfDbCon.ClientID %>]').val().length, 10) > 0) {
            strDrpDwnHtml += "<div><select id=\"ddl_DbCon_" + DbCount + "\" class=\"UsedUniformCss\">"
                                + "<option value=\"-1\">Select Connection</option>";
            $.each(jQuery.parseJSON($('[id$=<%= hdfDbCon.ClientID %>]').val()), function () {
                strDrpDwnHtml += "<option value=\"" + this["id"] + "\">" + this["name"] + "</option>";
            });
            strDrpDwnHtml += "</select></div>";
        }

        return strDrpDwnHtml;
    }

    function bindCurrentCompWsConDropdown(WsCount) {
        var strWsDrpDwnHtml = "";
        if (parseInt($('[id$=<%= hdfWsCon.ClientID %>]').val().length, 10) > 0) {
            strWsDrpDwnHtml += "<div><select id=\"ddl_WsCon_" + WsCount + "\" class=\"UsedUniformCss\">"
                                + "<option value=\"-1\">Select Connection</option>";
            $.each(jQuery.parseJSON($('[id$=<%= hdfWsCon.ClientID %>]').val()), function () {
                strWsDrpDwnHtml += "<option value=\"" + this["id"] + "\">" + this["name"] + "</option>";
            });
            strWsDrpDwnHtml += "</select></div>";
        }

        return strWsDrpDwnHtml;
    }

    function bindCurrentCompODataConDropdown(ODataCount) {
        var strODataDrpDwnHtml = "";
        if (parseInt($('[id$=<%= hdfODataCon.ClientID %>]').val().length, 10) > 0) {
            strODataDrpDwnHtml += "<div><select id=\"ddl_ODataCon_" + ODataCount + "\" class=\"UsedUniformCss\">"
                                + "<option value=\"-1\">Select Connection</option>";
            $.each(jQuery.parseJSON($('[id$=<%= hdfODataCon.ClientID %>]').val()), function () {
                strODataDrpDwnHtml += "<option value=\"" + this["id"] + "\">" + this["name"] + "</option>";
            });
            strODataDrpDwnHtml += "</select></div>";
        }

        return strODataDrpDwnHtml;
    }

    function bindCurrentCompOfflineDataConDropdown(OfflineDataCount) {
        var strOfflineDataDrpDwnHtml = "";
        if (parseInt($('[id$=<%= hdfOfflineDataTbl.ClientID %>]').val().length, 10) > 0) {
            strOfflineDataDrpDwnHtml += "<div><select id=\"ddl_OfflineDataCon_" + OfflineDataCount + "\" class=\"UsedUniformCss\">"
                                + "<option value=\"-1\">Select Connection</option>";
            $.each(jQuery.parseJSON($('[id$=<%= hdfOfflineDataTbl.ClientID %>]').val()), function () {
                strOfflineDataDrpDwnHtml += "<option value=\"" + this["id"] + "\">" + this["name"] + "</option>";
            });
            strOfflineDataDrpDwnHtml += "</select></div>";
        }

        return strOfflineDataDrpDwnHtml;
    }

    function saveLinkConnections() {
        try {
            var error = "";
            error = saveDbLinkConnections();
            error += saveWsLinkConnections();
            error += saveOdataLinkConnections();
            if (error.length > 0) alert("Following connection are not linked ("+error.substring(1) + ")");
            else
                $('[id$=<%= btnHidden.ClientID %>]').click();
        }
        catch (Error) { 
        }
    }

    function saveDbLinkConnections() {
        var strDbLinkJson = "", error = "",value ="",Con="";
        $('#fldDbConnections').find("*").each(function () {
            if (this.id.trim().split('_')[0] == "lbl") {
                value = $("#ddl_DbCon_" + this.id.trim().split('_')[1]).val();
                Con = this.id.trim().split('_')[2];
                if (value != "-1") {
                    if (strDbLinkJson.length > 0) strDbLinkJson += ",";
                    strDbLinkJson += "{\"con\":\"" + Con + "\", \"link\" : \"" + value + "\"}";
                } else {
                    error += "," + this.innerText;
                }
            }
        });
        $('[id$=<%= hdfDbLinks.ClientID %>]').val('[' + strDbLinkJson + ']');
        return error;
    }

    function saveWsLinkConnections() {
        var strWsLinkJson = "", error = "", value = "", Con = "";
        $('#hdfWsLinks').find("*").each(function () {
            if (this.id.trim().split('_')[0] == "lbl") {
                value = $("#ddl_WsCon_" + this.id.trim().split('_')[1]).val();
                Con = this.id.trim().split('_')[2];
                if (value != "-1") {
                    if (strWsLinkJson.length > 0) strWsLinkJson += ",";
                    strWsLinkJson += "{\"con\":\"" + Con + "\", \"link\" : \"" + value + "\"}";
                } else {
                    error += "," + this.innerText;
                }

                //if (strWsLinkJson.length > 0) strWsLinkJson += ",";
                //strWsLinkJson += "{\"con\":\"" + this.id.trim().split('_')[2] + "\", \"link\" : \"" + $("#ddl_WsCon_" + this.id.trim().split('_')[1]).val() + "\"}";
            }
        });
        $('[id$=<%= hdfWsLinks.ClientID %>]').val('[' + strWsLinkJson + ']');
        return error;
    }

    function saveOdataLinkConnections() {
        var strOdataLinkJson = "", error = "", value = "", Con = "";
        $('#fldODataConnections').find("*").each(function () {
            if (this.id.trim().split('_')[0] == "lbl") {

                value = $("#ddl_ODataCon_" + this.id.trim().split('_')[1]).val();
                Con = this.id.trim().split('_')[2];
                if (value != "-1") {
                    if (strOdataLinkJson.length > 0) strOdataLinkJson += ",";
                    strOdataLinkJson += "{\"con\":\"" + Con + "\", \"link\" : \"" + value + "\"}";
                } else {
                    error += "," + this.innerText;
                }
//                if (strOdataLinkJson.length > 0) strOdataLinkJson += ",";
//                strOdataLinkJson += "{\"con\":\"" + this.id.trim().split('_')[2] + "\", \"link\" : \"" + $("#ddl_ODataCon_" + this.id.trim().split('_')[1]).val() + "\"}";
            }
        });
        $('[id$=<%= hdfODataLinks.ClientID %>]').val('[' + strOdataLinkJson + ']');
        return error;
    }

//    function saveOfflineLinkConnections() {
//        var strOfflineLinkJson = "";
//        $('#fldOfflineDataConnections').find("*").each(function () {
//            if (this.id.trim().split('_')[0] == "lbl") {
//                if (strOfflineLinkJson.length > 0) strOfflineLinkJson += ",";
//                strOfflineLinkJson += "{\"con\":\"" + this.id.trim().split('_')[2] + "\", \"link\" : \"" + $("#ddl_OfflineDataCon_" + this.id.trim().split('_')[1]).val() + "\"}";
//            }
//        });
//        $('[id$=<%= hdfOfflineLinks.ClientID %>]').val('[' + strOfflineLinkJson + ']');
//    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
            <div id="PageCanvasContent">
                <section>
                        <div class="g12">
                            <div id="formDiv">
                            <asp:UpdatePanel ID="updAppTransfer" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="hide">
                                        <asp:Button ID="btnHidden" runat="server" OnClick="btnSave_Click" />
                                    </div>
                                    <fieldset>
                                        <section>
                                            <label for="ddlApps">
                                                Apps</label>
                                            <div>
                                                <asp:DropDownList ID="ddlApps" CssClass="UseUniformCss" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="ddlCompanies">
                                                Companies</label>
                                            <div>
                                                <asp:DropDownList ID="ddlCompanies" CssClass="UseUniformCss" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCompanies_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="ddlSubadmins">
                                                Subadmins</label>
                                            <div>
                                                <asp:DropDownList ID="ddlSubadmins" CssClass="UseUniformCss" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </section>
                                    </fieldset>
                                    <section id="buttons">
                                        <asp:Button ID="btnOK" runat="server" CssClass="aspButton" OnClick="btnOK_Click"
                                            Text="OK" />
                                    </section>
                                    <div>
                                        <asp:HiddenField ID="hdfWfDetails" runat="server" />
                                        <asp:HiddenField ID="hdfDbCon" runat="server" />
                                        <asp:HiddenField ID="hdfWsCon" runat="server" />
                                        <asp:HiddenField ID="hdfODataCon" runat="server" />
                                        <asp:HiddenField ID="hdfOfflineDataTbl" runat="server" />
                                        <asp:HiddenField ID="hdfDbLinks" runat="server" />
                                        <asp:HiddenField ID="hdfWsLinks" runat="server" />
                                        <asp:HiddenField ID="hdfODataLinks" runat="server" />
                                        <asp:HiddenField ID="hdfOfflineLinks" runat="server" />
                                        <asp:HiddenField ID="hdfDbCommands" runat="server" />
                                        <asp:HiddenField ID="hdfWsCommands" runat="server" />
                                        <asp:HiddenField ID="hdfODataCommands" runat="server" />
                                        <asp:HiddenField ID="hdfOfflineCommands" runat="server" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:Panel ID="pnlLinkconnections" runat="server" CssClass="hide">
                                        <div id="divLinkConnections" style="border:1px solid #e7e7e7;border-radius:4px;">
                                        <label style="color:#444444;">
                                            Link Connections
                                        </label>
                                        <div style="border:1px solid #e7e7e7;border-radius:4px;overflow-y:auto; min-height:160px; max-height:300px;">
                                            <fieldset id="fldDbConnections" class="hide">
                                            </fieldset>
                                            <fieldset id="fldWSConnections" class="hide">
                                            </fieldset>
                                            <fieldset id="fldODataConnections" class="hide">
                                            </fieldset>
                                            <%--<fieldset id="fldOfflineDataConnections" class="hide">
                                            </fieldset>--%>
                                        </div>
                                        <section>
                                            <div id="divLinkConnectionsButton" style="width: 100%; border: none; padding-left: 0px; padding-right: 0px;
                                                text-align: center;">
                                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="aspButton"  OnClientClick="saveLinkConnections();return false;"/>
                                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="aspButton" OnClientClick="initializeControls(); return false;"/>
                                            </div>
                                        </section>
                                        </div>
                            </asp:Panel>
                            </div>
                        </div>
                </section>
            </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {

            showWaitModal();
            isCookieCleanUpRequired('false');
            //storeJsonOfUserSelectedForPostback();
        }
        function prm_endRequest() {
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>

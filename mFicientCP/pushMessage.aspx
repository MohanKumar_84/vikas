﻿<%@ Page Title="mFicient | Automatic Messaging" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="pushMessage.aspx.cs" Inherits="mFicientCP.pushMessage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script type="text/javascript">
        DB_TYPE =
        {
            MSSQL: 1,
            ORACLE: 2,
            MYSQL: 3,
            POSTGRESQL: 4,
            ODBCDSN: 5
        }
        function showTableNameFormatForPostgre() {
            var ddlDbType = $('#' + '<%= ddlDBType.ClientID %>');
            var spnTblNameInfo = $('#spnTblNameInfo');
            if (ddlDbType) {
                var dbTypeSelected = $(ddlDbType).val();
                if (dbTypeSelected === (DB_TYPE.POSTGRESQL).toString()) {
                    $(spnTblNameInfo).show();
                    $(spnTblNameInfo).text('( Table name for postgresql should be like Schema.Tablename. )');
                }
                else {
                    $(spnTblNameInfo).hide();
                    $(spnTblNameInfo).text('');
                }
            }
        }
        function processFillPasswordDuringLoad() {
            var hidPassword = $('#' + '<%=hidPwdSvdBefore.ClientID %>');
            var txtPassword = $('#' + '<%=txtPassword.ClientID %>');
            if (txtPassword && hidPassword) {
                $(txtPassword).val($(hidPassword).val());
            }
        }
        function rememberPasswordEntered() {
            var hidPassword = $('#' + '<%=hidNewPwdEntered.ClientID %>');
            var txtPassword = $('#' + '<%=txtPassword.ClientID %>');
            $(hidPassword).val($(txtPassword).val());
        }
        function fillPasswordEntered() {
            var hidPassword = $('#' + '<%=hidNewPwdEntered.ClientID %>');
            var txtPassword = $('#' + '<%=txtPassword.ClientID %>');
            if (hidPassword) {
                if (hidPassword.value !== "") {
                    if (txtPassword)
                        $(txtPassword).val($(hidPassword).val());
                }
            }
        }
        function showTableInfo(infoSpan) {
            if (infoSpan) {
                var $helpbox = $('#divInfoTblCont');
                $helpbox.css({
                    'left': ($(infoSpan).offset().left + $(infoSpan).width()) - 140,
                    'top': ($(infoSpan).offset().top) - 300
                }).fadeIn('fast');
            }
        }
        function setEventsOfInfoSpan() {
            var $infoSpan = $('#spanInfo');
            $infoSpan.on('mouseover', function () {
                showTableInfo(this);
            });
            $infoSpan.on('mouseout', function () {
                var $helpbox = $('#divInfoTblCont');
                $helpbox.fadeOut('fast'); ;
            });
        }
        $(document).ready(function () {
            // Handler for .ready() called.
            setEventsOfInfoSpan();
        });
    </script>
    <style type="text/css">
        .tableInfoContent
        {
            padding: 5px 10px 10px 5px;
            background-color: #eeeeee;
            width: auto;
            border: solid 1px #f8f8f8;
            border-radius: 3px;
            box-shadow: 1px 1px 3px #222;
            opacity: 0.95;
        }
        .selector span
        {
            width: 140px;
            overflow: hidden;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divPushMessageDetails">
                <asp:UpdatePanel ID="updContainer" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <section>
                            <asp:Panel ID="pnlPushMessageDetails" runat="server">
                                <div class="g12">
                                    <div id="formDiv">
                                        <fieldset>
                                            <label>
                                                Push Message Settings</label>
                                            <div id="divDatabasePullError" class="errorMessage">
                                            </div>
                                            <section>
                                                <label for="<%=ddlMplugin.ClientID %>">
                                                    mPlugin</label>
                                                <div>
                                                    <asp:DropDownList ID="ddlMplugin" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </section>
                                            <section>
                                                <label for="<%=ddlDBType.ClientID %>">
                                                    Database Type</label>
                                                <div>
                                                    <asp:DropDownList ID="ddlDBType" runat="server" onchange="showTableNameFormatForPostgre();">
                                                        <asp:ListItem Value="1">MSSql</asp:ListItem>
                                                        <asp:ListItem Value="2">Oracle</asp:ListItem>
                                                        <asp:ListItem Value="3">MySql</asp:ListItem>
                                                        <asp:ListItem Value="4">PostgreSql</asp:ListItem>
                                                        <%--<asp:ListItem Value="5">DSN</asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                </div>
                                            </section>
                                            <section>
                                                <label for="<%= txtConnectionName.ClientID %>">
                                                    Connection Name</label>
                                                <div>
                                                    <asp:TextBox ID="txtConnectionName" runat="server" CssClass="required" Width="50%"></asp:TextBox>
                                                </div>
                                            </section>
                                            <section>
                                                <label for="<%=txtHostName.ClientID %>">
                                                    Host Name</label>
                                                <div>
                                                    <asp:TextBox ID="txtHostName" runat="server" CssClass="required" Width="50%"></asp:TextBox>
                                                </div>
                                            </section>
                                            <section>
                                                <label for="<%= txtDatabaseName.ClientID %>">
                                                    Database Name</label>
                                                <div>
                                                    <asp:TextBox ID="txtDatabaseName" runat="server" CssClass="required" Width="50%"></asp:TextBox>
                                                </div>
                                            </section>
                                            <section>
                                                <label for="<%= txtUserName.ClientID %>">
                                                    Username</label>
                                                <div>
                                                    <asp:TextBox ID="txtUserName" runat="server" CssClass="required" Width="25%"></asp:TextBox>
                                                </div>
                                            </section>
                                            <section>
                                                <label for="<%=txtPassword.ClientID %>">
                                                    Password</label>
                                                <div>
                                                    <asp:TextBox ID="txtPassword" onblur="rememberPasswordEntered();" runat="server"
                                                        CssClass="required" Width="25%" TextMode="Password"></asp:TextBox>
                                                </div>
                                            </section>
                                            <section>
                                                <label for="<%=txtTableName.ClientID %>">
                                                    Table Name</label>
                                                <div>
                                                    <asp:TextBox ID="txtTableName" runat="server" CssClass="required fl" Width="25%"></asp:TextBox>
                                                    <span id="spanInfo" class="i_information fl" style="margin-left: 5px; width: 24px;
                                                        height: 24px; position: relative; top: 2px;"></span>
                                                </div>
                                            </section>
                                            <section>
                                                <label for="<%=txtAdditionalInfo.ClientID %>">
                                                    Other Info</label>
                                                <div>
                                                    <asp:TextBox ID="txtAdditionalInfo" runat="server" CssClass="required" Width="25%"></asp:TextBox>
                                                </div>
                                            </section>
                                            <section>
                                                <asp:Button ID="btnSaveSettings" runat="server" Text="Save" CssClass="aspButton"
                                                    OnClick="btnSaveSettings_Click" />
                                                <%--<asp:Button ID="btnTestConnection" runat="server" Text="Test Database Connection"
                                                    CssClass="aspButton" OnClick="btnTestConnection_Click" />
                                                <span>
                                                    <asp:Label ID="lblConnectionStatus" class="wl_formstatus" runat="server"></asp:Label></span>--%>
                                            </section>
                                        </fieldset>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                    <div id="divInfoTblCont" class="tableInfoContent" style="margin-top: 5px; display: none;
                                        position: absolute;">
                                        <h3>
                                            Table Information</h3>
                                        <div id="divInformationTable" runat="server">
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </section>
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidProcess" runat="server" />
                            <asp:HiddenField ID="hidPwdSvdBefore" runat="server" />
                            <asp:HiddenField ID="hidNewPwdEntered" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <div id="divPostBackConfirm" style="display: none;">
        <asp:UpdatePanel ID="updConfirmation" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="modalPopUpConfirm">
                    <div>
                        <asp:Label ID="lblConfirmationMessage" runat="server" Text=""></asp:Label></div>
                    <div style="margin-top: 30px; margin-bottom: 5px;">
                        <div style="text-align: center">
                            <span class="ui-button-text">
                                <asp:Button ID="btnConfirmYes" runat="server" Text='Yes' CssClass="aspButton" OnCommand="btnConfirmYes_Click" /></span>&nbsp;
                            <span class="ui-button-text">
                                <asp:Button ID="btnConfirmNo" runat="server" Text='No' OnClientClick="closeModalPopUp('divPostBackConfirm');removeDialogImage('divPostBackConfirm');return false;"
                                    CssClass="aspButton" /></span>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            $("input:checkbox").uniform();
            hideWaitModal();
            isCookieCleanUpRequired('true');
            setEventsOfInfoSpan();
        }
    </script>
</asp:Content>

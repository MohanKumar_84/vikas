﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientCP
{

    public enum PENDING_DEVICES_ORDER_BY
    {
        Name = 1,
        OS = 2,
        Devices = 3,
        RequestDate = 4,
        RequestType = 5
    }
    public enum REGISTERED_DEVICES_ORDER_BY
    {
        Name = 1,
        OS = 2,
        RequestDate = 3,
    }
    public enum SORT_TYPE
    {
        ASC,
        DESC
    }
    public partial class registerDevice : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;
        string hfsPart5 = string.Empty;
        string hfsPart6 = string.Empty;
 //       
        enum DEVICE_DETAILS_SHOW_PURPOSE
        {
            Processing,//when delete and approve button is clicked in pending request repeater
            Information//when we have to just show the device information
        }
        enum REPEATER_NAME
        {
            RegisteredDevices = 1,
            PendingDevices = 2,
            ModalRegisteredDevices = 3,
            ModalPendingDevices = 4,
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if the Previous Page is Home.aspx
            HiddenField hidHomePgDeviceLinkClicked = null;

            HttpContext context = HttpContext.Current;
            Literal ltFullNAME = (Literal)this.Master.Master.FindControl("ltFullNAME");
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;


                //if the previous page is home.aspx
                string prvPageName = previousPage.AppRelativeVirtualPath;
                prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                if (prvPageName.ToLower() == "home.aspx")
                {
                    hidHomePgDeviceLinkClicked = ((HiddenField)previousPageForm.FindControl("MainCanvas").FindControl("PageCanvas").FindControl("hidHomePgDeviceLinkClicked"));
                }
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];
            int lengthA = hfsParts.Length;
            if (lengthA > 4)
            {
                hfsPart5 = hfsParts[4];
                hfsPart6 = hfsParts[5];
                context.Items["hfs5"] = hfsPart5;
                context.Items["hfs6"] = hfsPart6;
                ltFullNAME.Text = hfsPart5 + " (" + hfsPart6 + ")";
            }
            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!Page.IsPostBack)
            {
                try
                {
                    if (!String.IsNullOrEmpty(hfsValue))
                        CompanyTimezone.getTimezoneInfo(hfsPart4);
                }
                catch
                { }
                if (Request.QueryString.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Request.QueryString.Get("d1")))
                    {
                        hidQueryStrFromEmail.Value = Request.QueryString.Get("d1").ToString();
                        radListSelection.SelectedValue = "2";
                        initialiseCurrentSortOrder(REPEATER_NAME.PendingDevices);
                        int iRowsCount;
                        bindPendingRequestRepeater(PENDING_DEVICES_ORDER_BY.RequestDate, SORT_TYPE.DESC, out iRowsCount);
                    }
                }
                else
                {
                    if (hfsValue != string.Empty)
                    {
                        if (hidHomePgDeviceLinkClicked != null && !String.IsNullOrEmpty(hidHomePgDeviceLinkClicked.Value) && hidHomePgDeviceLinkClicked.Value == "4")
                        {
                            radListSelection.SelectedValue = "2";
                            initialiseCurrentSortOrder(REPEATER_NAME.PendingDevices);
                            int iRowsCount;
                            bindPendingRequestRepeater(PENDING_DEVICES_ORDER_BY.RequestDate, SORT_TYPE.DESC, out iRowsCount);
                        }
                        else
                        {
                            #region setting the sort of repeater first time
                            initialiseCurrentSortOrder(REPEATER_NAME.RegisteredDevices);
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "Sort Repeater", "changeImageOfSortedCol();clearSortingDtlHiddenField();", true);
                            #endregion
                            int iRowsCount;
                            bindRegisteredDevicesRepeater(REGISTERED_DEVICES_ORDER_BY.RequestDate, SORT_TYPE.DESC, out iRowsCount);
                        }

                    }
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "$(\"input\").uniform();", true);
            }
            else
            {
                try
                {
                    if (!String.IsNullOrEmpty(hfsValue))
                        CompanyTimezone.getTimezoneInfo(hfsPart4);
                }
                catch
                { }
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    #region setting the sort of repeater first time
                    initialiseCurrentSortOrder(REPEATER_NAME.RegisteredDevices);
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "Sort Repeater", "changeImageOfSortedCol();clearSortingDtlHiddenField();", true);
                    #endregion
                    int iRowsCount;
                    bindRegisteredDevicesRepeater(REGISTERED_DEVICES_ORDER_BY.RequestDate, SORT_TYPE.DESC, out  iRowsCount);
                    radListSelection.SelectedValue = "1";
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "$(\"input\").uniform();", true);

                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, "changeImageOfSortedCol();clearSortingDtlHiddenField();$(\"#content\").find('div.widgets').wl_Widget();makeTabAfterPostBack();$('#aspnetForm').wl_Form();");
                    return;


                }

                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback1", true, "$(\"#content\").find('div.widgets').wl_Widget();makeTabAfterPostBack();$('#aspnetForm').wl_Form();");
            }

        }

        #region Bind, Show Header And Show hide approve
        void bindRegisteredDevicesRepeater(REGISTERED_DEVICES_ORDER_BY orderBy, SORT_TYPE sortType, out int rowsCount)
        {
            rowsCount = 0;
            GetRegisteredDeviceDetails objRegtDeviceDtls = new GetRegisteredDeviceDetails(hfsPart4
                , strUserId
                , (REGISTERED_DEVICES_ORDER_BY)Enum.Parse(typeof(REGISTERED_DEVICES_ORDER_BY), ((int)orderBy).ToString())
                , (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), ((int)sortType).ToString()));

            objRegtDeviceDtls.Process();
            DataSet dsRegtDeviceDtls = objRegtDeviceDtls.ResultTables;
            if (dsRegtDeviceDtls != null)
            {
                if (dsRegtDeviceDtls.Tables.Count > 0)
                {
                    if (dsRegtDeviceDtls.Tables[0] != null)
                    {
                        if (dsRegtDeviceDtls.Tables[0].Rows.Count > 0)
                        {
                            rptRegisteredDeviceDtls.DataSource = dsRegtDeviceDtls.Tables[0];
                            rptRegisteredDeviceDtls.DataBind();
                            showRepeaterHeaderInfo("Registered Devices", REPEATER_NAME.RegisteredDevices);
                            rptRegisteredDeviceDtls.Visible = true;
                            rptPendingRequest.Visible = false;
                            rowsCount = dsRegtDeviceDtls.Tables[0].Rows.Count;
                        }
                        else
                        {
                            showRepeaterHeaderInfo("There are no registered devices", REPEATER_NAME.RegisteredDevices);
                            rptRegisteredDeviceDtls.DataSource = null;
                            rptRegisteredDeviceDtls.DataBind();
                            rptPendingRequest.Visible = false;
                            rptRegisteredDeviceDtls.Visible = false;
                        }
                    }
                    fillInformationLabels(dsRegtDeviceDtls.Tables[1], dsRegtDeviceDtls.Tables[2], dsRegtDeviceDtls.Tables[3]);
                }
                else
                {
                    showRepeaterHeaderInfo("There are no registered devices", REPEATER_NAME.RegisteredDevices);
                    rptPendingRequest.Visible = false;
                    rptRegisteredDeviceDtls.Visible = false;
                }
            }
        }
        void bindPendingRequestRepeater(PENDING_DEVICES_ORDER_BY orderBy, SORT_TYPE sortType, out int rowsCount)
        {
            rowsCount = 0;
            GetRequestForRegisteringDevice objRequestDtls;
            hidPendingDoubleAsterix.Value = "";//using it for showing the meaning of double asterix set in repeater databound
            if (String.IsNullOrEmpty(hidQueryStrFromEmail.Value))
            {
                objRequestDtls = new GetRequestForRegisteringDevice(hfsPart4, strUserId
                    , (PENDING_DEVICES_ORDER_BY)Enum.Parse(typeof(PENDING_DEVICES_ORDER_BY), ((int)orderBy).ToString())
                    , (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), ((int)sortType).ToString()));
            }
            else
            {
                string[] userIdCompanyIdAndSubAdminName = Utilities.DecryptString(Request.QueryString.Get("d1")).Split('@');
                objRequestDtls = new GetRequestForRegisteringDevice(hfsPart4, strUserId
                    , userIdCompanyIdAndSubAdminName[0]
                    , (PENDING_DEVICES_ORDER_BY)Enum.Parse(typeof(PENDING_DEVICES_ORDER_BY), ((int)orderBy).ToString())
                    , (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), ((int)sortType).ToString()));
                hidQueryStrFromEmail.Value = "";
            }
            objRequestDtls.Process();
            DataSet dsPendingRequestDtls = objRequestDtls.ResultTables;
            if (dsPendingRequestDtls != null)
            {
                if (dsPendingRequestDtls.Tables.Count > 0)
                {
                    if (dsPendingRequestDtls.Tables[0].Rows.Count > 0)
                    {
                        rptPendingRequest.DataSource = dsPendingRequestDtls.Tables[0];
                        rptPendingRequest.DataBind();

                        bindRegisteredAndPendingDevicesCntInPendingRpt(dsPendingRequestDtls.Tables[5], dsPendingRequestDtls.Tables[7]);
                        showRepeaterHeaderInfo("Pending Requests", REPEATER_NAME.PendingDevices);
                        rptPendingRequest.Visible = true;
                        rptRegisteredDeviceDtls.Visible = false;
                        showHideApproveFromRepeater(dsPendingRequestDtls.Tables[2], dsPendingRequestDtls.Tables[5], rptPendingRequest);
                        showDoubleAsterixInfoForPendingRpt();
                        rowsCount = dsPendingRequestDtls.Tables[0].Rows.Count;
                    }
                    else
                    {
                        showRepeaterHeaderInfo("There are no pending request", REPEATER_NAME.PendingDevices);
                        rptPendingRequest.Visible = false;
                        rptRegisteredDeviceDtls.Visible = false;
                    }
                    fillInformationLabels(dsPendingRequestDtls.Tables[2], dsPendingRequestDtls.Tables[3], dsPendingRequestDtls.Tables[4]);
                }
            }
        }
        void bindRegisteredDevicesRepeaterForSearch(REGISTERED_DEVICES_ORDER_BY orderBy, SORT_TYPE sortType, out int rowsCount)
        {
            rowsCount = 0;
            string strSearchText = txtFilterSearch.Text.Trim();
            if (strSearchText.Contains("/"))
            {
                string[] arySearchText = strSearchText.Split('/');
                strSearchText = arySearchText[0].Trim() + '/' + arySearchText[1].Trim();
            }
            SearchUserByUserName objSearchUser = new SearchUserByUserName(hfsPart4, strUserId, strSearchText
                , SearchUserByUserName.REQUEST_TYPE.Registered
                , (REGISTERED_DEVICES_ORDER_BY)Enum.Parse(typeof(REGISTERED_DEVICES_ORDER_BY), ((int)orderBy).ToString())
                , PENDING_DEVICES_ORDER_BY.RequestDate
                , (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), ((int)sortType).ToString()));
            objSearchUser.Process();
            DataSet dsUserDetails = objSearchUser.UserDetails;

            if (dsUserDetails != null && dsUserDetails.Tables.Count > 0)
            {
                if (dsUserDetails.Tables[0].Rows.Count > 0)
                {
                    rptRegisteredDeviceDtls.DataSource = dsUserDetails.Tables[0];
                    rptRegisteredDeviceDtls.DataBind();
                    showRepeaterHeaderInfo("Registered Devices", REPEATER_NAME.RegisteredDevices);
                    rowsCount = dsUserDetails.Tables[0].Rows.Count;

                }
                else
                {
                    Utilities.showMessage("No results found.", updRepeater, DIALOG_TYPE.Info);
                }
            }
            else
            {
                Utilities.showMessage("Internal error.Please try again.", updRepeater, DIALOG_TYPE.Error);
            }
        }
        void bindPendingRequestRepeaterForSearch(PENDING_DEVICES_ORDER_BY orderBy, SORT_TYPE sortType, out int rowsCount)
        {
            rowsCount = 0;
            SearchUserByUserName objSearchUser = new SearchUserByUserName(hfsPart4, strUserId,
                txtFilterSearch.Text, SearchUserByUserName.REQUEST_TYPE.Pending
                , REGISTERED_DEVICES_ORDER_BY.RequestDate
                , (PENDING_DEVICES_ORDER_BY)Enum.Parse(typeof(PENDING_DEVICES_ORDER_BY), ((int)orderBy).ToString())
                , (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), ((int)sortType).ToString()));
            objSearchUser.Process();
            DataSet dsUserDetails = objSearchUser.UserDetails;
            hidPendingDoubleAsterix.Value = "";//using it for showing the meaning of double asterix set in repeater databound
            if (dsUserDetails != null && dsUserDetails.Tables.Count > 0)
            {
                if (dsUserDetails.Tables[0].Rows.Count > 0)
                {
                    rptPendingRequest.DataSource = dsUserDetails.Tables[0];
                    rptPendingRequest.DataBind();
                    bindRegisteredAndPendingDevicesCntInPendingRpt(dsUserDetails.Tables[5], dsUserDetails.Tables[7]);
                    showRepeaterHeaderInfo("Pending Requests", REPEATER_NAME.PendingDevices);
                    showHideApproveFromRepeater(dsUserDetails.Tables[2], dsUserDetails.Tables[5]);
                    showDoubleAsterixInfoForPendingRpt();
                    rowsCount = dsUserDetails.Tables[0].Rows.Count;
                }
                else
                {
                    Utilities.showMessage("No results found.", updRepeater, DIALOG_TYPE.Info);
                }
            }
            else
            {
                Utilities.showMessage("Internal error.Please try again.", updRepeater, DIALOG_TYPE.Error);
            }
        }

        void showDoubleAsterixInfoForPendingRpt()
        {
            if (!String.IsNullOrEmpty(hidPendingDoubleAsterix.Value))
            {
                try
                {
                    if (Convert.ToBoolean(hidPendingDoubleAsterix.Value))
                    {
                        Label lblDblAsterix = (Label)rptPendingRequest.Controls[rptPendingRequest.Controls.Count - 1].Controls[0].FindControl("lblDoubleAsterixInfo");
                        lblDblAsterix.Visible = true;
                        lblDblAsterix.Text = "** Device registered for other user.Please delete registered device first to approve";
                        hidPendingDoubleAsterix.Value = "";
                    }
                }
                catch
                {

                }
            }
        }


        void bindRegisteredAndPendingDevicesCntInPendingRpt(DataTable registeredDevicesCntForUser, DataTable pendingReqCountForUser)
        {
            try
            {
                foreach (RepeaterItem item in rptPendingRequest.Items)
                {
                    LinkButton lnkCount = (LinkButton)item.FindControl("lnkCntRegisteredPendingDevices");
                    Label lblUserId = (Label)item.FindControl("lblUserId");
                    string strFilter = String.Format("USER_ID ='{0}'", lblUserId.Text);
                    DataRow[] row = registeredDevicesCntForUser.Select(strFilter);
                    if (row.Length > 0)
                    {
                        lnkCount.Text = Convert.ToString(row[0]["REGISTERED_DEVICE_COUNT"]);
                    }
                    else
                    {
                        lnkCount.Text = "0";
                    }
                    row = pendingReqCountForUser.Select(strFilter);
                    if (row.Length > 0)
                    {
                        lnkCount.Text += " / " + Convert.ToString(row[0]["REQUEST_COUNT"]);
                    }
                    else
                    {
                        lnkCount.Text += " / " + "0";
                    }
                }
            }
            catch 
            {

            }
        }
        void showHideApproveFromRepeater(DataTable registeredDevicesCountDTable, DataTable exceptionalUsersAndMaxDevicesDTable)
        {
            //Checking if device is already registered for another user in Repeater Item Data bound
            //this is to check if the user is an exceptional user,checking no of max device for him ,and if not set for the user then
            //check for general company setting,ie,max allowed devices for any user in the company
            if (exceptionalUsersAndMaxDevicesDTable != null && exceptionalUsersAndMaxDevicesDTable.Rows.Count > 0)
            {
                foreach (DataRow row in exceptionalUsersAndMaxDevicesDTable.Rows)
                {
                    foreach (RepeaterItem item in rptPendingRequest.Items)
                    {
                        LinkButton lnkApprove = (LinkButton)item.FindControl("lnkApprove");
                        Label lblUserId = (Label)item.FindControl("lblUserId");
                        if ((lblUserId.Text == (string)row["USER_ID"] && Convert.ToString(row["REGISTERED_DEVICE_COUNT"]) == Convert.ToString(row["MAX_DEVICE"]))
                            || (lblUserId.Text == (string)row["USER_ID"] && Convert.ToString(row["REGISTERED_DEVICE_COUNT"]) == Convert.ToString(row["MAX_DEVICE_PER_USER"])))
                        {
                            //check if the request is only for approval
                            //if it is for delete any earlier device and then approve the new one then we can allow the request to be approved
                            if (lnkApprove.CommandArgument == ((int)REGISTRATION_DEVICE_REQUEST_TYPE.ALLOW_USER_DEVICE).ToString())
                            {
                                lnkApprove.Visible = false;
                                lblUserId.ForeColor = System.Drawing.Color.Red;
                                lblUserId.ToolTip = "Reached max device registration for the user";
                            }
                        }
                    }
                }
            }

            //this checks how many devices can totally be allotted according to the plan details the company has taken
            if (registeredDevicesCountDTable != null && registeredDevicesCountDTable.Rows.Count > 0)
            {
                if ((int)registeredDevicesCountDTable.Rows[0]["Total"] == (int)registeredDevicesCountDTable.Rows[0]["MAX_USER"])
                {
                    foreach (RepeaterItem item in rptPendingRequest.Items)
                    {
                        LinkButton lnkApprove = (LinkButton)item.FindControl("lnkApprove");
                        //if the request type is other than this then we first will be deleting a device and then registering 
                        //so the count would remain same.and hence we could allow this
                        if (lnkApprove.CommandArgument == ((int)REGISTRATION_DEVICE_REQUEST_TYPE.ALLOW_USER_DEVICE).ToString())
                        {
                            lnkApprove.Visible = false;
                        }
                    }
                    Utilities.showAlert("Since you have reached your max allowed registration,you have to first delete existing devices to approve any new request", "divInfomationForApproveDisable", updRepeater);
                }
            }
        }
        int bindAndShowHideModalRegisteredDeviceRpt(string userId, DEVICE_DETAILS_SHOW_PURPOSE showPurpose)
        {
            int iStatus = 0;
            GetRegisteredDeviceDetails objRegisteredDeviceDetails = new GetRegisteredDeviceDetails(hfsPart4, strUserId
                , REGISTERED_DEVICES_ORDER_BY.RequestDate, SORT_TYPE.DESC);
            objRegisteredDeviceDetails.Process();
            if (objRegisteredDeviceDetails.StatusCode == 0)
            {
                DataTable dtblUserRegisteredDevice = objRegisteredDeviceDetails.ResultTables.Tables[0];
                string strFilter = String.Format("USER_ID = '{0}'", userId.Trim());
                DataRow[] userDtlRows = dtblUserRegisteredDevice.Select(strFilter);
                if (userDtlRows.Length > 0)
                {
                    try
                    {
                        dtblUserRegisteredDevice = userDtlRows.CopyToDataTable<DataRow>();
                        dtblUserRegisteredDevice = getModifiedTblForPopUpRpts(dtblUserRegisteredDevice, showPurpose);
                        rptModalRegisteredDevicesDtls.DataSource = dtblUserRegisteredDevice;
                        rptModalRegisteredDevicesDtls.DataBind();
                        pnlRegisteredDevicesRptBox.Visible = true;
                        showHideRptHeaderForDeviceInfoPopUp(showPurpose, REPEATER_NAME.ModalRegisteredDevices);//if datasource is not null then don't show repeater
                        if (rptModalRegisteredDevicesDtls.Items.Count > 0)
                        {
                            showRepeaterHeaderInfo("Select devices to delete", REPEATER_NAME.ModalRegisteredDevices);
                        }
                        else
                        {
                            showRepeaterHeaderInfo("There are no registered device to select.Please Proceed the process.", REPEATER_NAME.ModalRegisteredDevices);
                        }
                    }
                    catch 
                    {
                        //show error 
                        iStatus = -1000;
                    }
                }
                else
                {
                    //bind a datatable with no rows
                    rptModalRegisteredDevicesDtls.DataSource = null;//if datasource is null then show header
                    rptModalRegisteredDevicesDtls.DataBind();
                    showRepeaterHeaderInfo("No registered devices", REPEATER_NAME.ModalRegisteredDevices);
                    if (rptModalRegisteredDevicesDtls.Items.Count == 0 && showPurpose == DEVICE_DETAILS_SHOW_PURPOSE.Processing)
                    {
                        showRepeaterHeaderInfo("There are no registered device to select.Please Proceed the process.", REPEATER_NAME.ModalRegisteredDevices);
                    }

                }
            }
            else
            {
                iStatus = -1000;
            }
            return iStatus;
        }



        int bindAndShowHideModalPendingDeviceRpt(string userId, DEVICE_DETAILS_SHOW_PURPOSE showPurpose)
        {
            int iStatus = 0;
            GetRequestForRegisteringDevice objPendingRequest = new GetRequestForRegisteringDevice(hfsPart4, strUserId
                , PENDING_DEVICES_ORDER_BY.RequestDate, SORT_TYPE.ASC);
            objPendingRequest.Process();
            DataSet dsPendingRequestDtls = objPendingRequest.ResultTables;
            DataTable dtblPendingRequests = objPendingRequest.ResultTables.Tables[0];
            if (dtblPendingRequests != null)
            {
                string strFilter = String.Format("USER_ID = '{0}'", userId.Trim());
                DataRow[] userDtlRows = dtblPendingRequests.Select(strFilter);

                if (userDtlRows.Length > 0)
                {
                    try
                    {
                        dtblPendingRequests = userDtlRows.CopyToDataTable<DataRow>();
                        dtblPendingRequests = getModifiedTblForPopUpRpts(dtblPendingRequests, showPurpose);
                        rptModalPendingRequestDtls.DataSource = dtblPendingRequests;
                        rptModalPendingRequestDtls.DataBind();
                        pnlPendingDevicesRptBox.Visible = true;
                        showHideRptHeaderForDeviceInfoPopUp(showPurpose, REPEATER_NAME.ModalPendingDevices);
                    }
                    catch
                    {
                        iStatus = -1000;
                    }
                }
                else
                {
                    showRepeaterHeaderInfo("No pending devices", REPEATER_NAME.ModalPendingDevices);
                    rptModalPendingRequestDtls.DataSource = null;
                    rptModalPendingRequestDtls.DataBind();
                }
            }
            else
            {
                iStatus = -1000;
            }
            return iStatus;
        }
        DataTable getModifiedTblForPopUpRpts(DataTable deviceDetailsDtbl, DEVICE_DETAILS_SHOW_PURPOSE showPurpose)
        {
            try
            {
                bool blnShowProcessingChkBox = false;
                switch (showPurpose)
                {
                    case DEVICE_DETAILS_SHOW_PURPOSE.Information:
                        blnShowProcessingChkBox = false;
                        break;
                    case DEVICE_DETAILS_SHOW_PURPOSE.Processing:
                        blnShowProcessingChkBox = true;
                        break;
                }
                deviceDetailsDtbl.Columns.Add("FOR_PROCESSING", typeof(bool));
                foreach (DataRow row in deviceDetailsDtbl.Rows)
                {
                    row["FOR_PROCESSING"] = blnShowProcessingChkBox;
                }
                deviceDetailsDtbl.AcceptChanges();
                return deviceDetailsDtbl;
            }
            catch
            {
                throw new Exception();
            }
        }
        void showHideRptHeaderForDeviceInfoPopUp(DEVICE_DETAILS_SHOW_PURPOSE showPurpose, REPEATER_NAME repeater)
        {
            switch (showPurpose)
            {
                case DEVICE_DETAILS_SHOW_PURPOSE.Processing:

                    pnlTabPendingDevicesHeader.Visible = true;
                    pnlTabRegisteredDevicesHeader.Visible = true;

                    break;
                case DEVICE_DETAILS_SHOW_PURPOSE.Information:
                    switch (repeater)
                    {
                        case REPEATER_NAME.ModalPendingDevices:
                            pnlTabPendingDevicesHeader.Visible = false;
                            break;
                        case REPEATER_NAME.ModalRegisteredDevices:
                            pnlTabRegisteredDevicesHeader.Visible = false;
                            break;
                    }
                    break;
            }
        }

        void showHideApproveFromRepeater(DataTable registeredDevicesCountDTable, DataTable exceptionalUsersAndMaxDevicesDTable, Repeater repeaterToHideApprove)
        {
            //Checking if device is already registered for another user in Repeater Item Data bound
            //this is to check if the user is an exceptional user,checking no of max device for him ,and if not set for the user then
            //check for general company setting,ie,max allowed devices for any user in the company
            if (exceptionalUsersAndMaxDevicesDTable != null && exceptionalUsersAndMaxDevicesDTable.Rows.Count > 0)
            {
                foreach (DataRow row in exceptionalUsersAndMaxDevicesDTable.Rows)
                {
                    foreach (RepeaterItem item in repeaterToHideApprove.Items)
                    {
                        Boolean blnDisableLnkApprove = false;
                        LinkButton lnkApprove = (LinkButton)item.FindControl("lnkApprove");
                        Label lblUserId = (Label)item.FindControl("lblUserId");
                        if (!String.IsNullOrEmpty(Convert.ToString(row["MAX_DEVICE"])))
                        {
                            if (lblUserId.Text == (string)row["USER_ID"] &&
                                (Convert.ToString(row["REGISTERED_DEVICE_COUNT"]) ==
                                Convert.ToString(row["MAX_DEVICE"]))
                                )
                            {
                                blnDisableLnkApprove = true;
                            }
                            else blnDisableLnkApprove = false;
                        }
                        else
                        {
                            if ((lblUserId.Text == (string)row["USER_ID"] &&
                                Convert.ToString(row["REGISTERED_DEVICE_COUNT"]) == Convert.ToString(row["MAX_DEVICE_PER_USER"])))
                            {
                                blnDisableLnkApprove = true;
                            }
                            else blnDisableLnkApprove = false;
                        }
                        if (blnDisableLnkApprove)
                        {
                            //check if the request is only for approval
                            //if it is for delete any earlier device and then approve the new one then we can allow the request to be approved
                            if (lnkApprove.CommandArgument ==
                                ((int)REGISTRATION_DEVICE_REQUEST_TYPE.ALLOW_USER_DEVICE).ToString())
                            {
                                lnkApprove.Visible = false;
                                lblUserId.ForeColor = System.Drawing.Color.Red;
                                lblUserId.ToolTip = "Reached max device registration for the user";

                            }
                        }
                    }
                }
            }

            //this checks how many devices can totally be allotted according to the plan details the company has taken
            if (registeredDevicesCountDTable != null && registeredDevicesCountDTable.Rows.Count > 0)
            {
                if ((int)registeredDevicesCountDTable.Rows[0]["Total"] ==
                    ((int)registeredDevicesCountDTable.Rows[0]["MAX_USER"]) *
                    Convert.ToInt32(registeredDevicesCountDTable.Rows[0]["MAX_DEVICE_PER_USER"]))
                {
                    foreach (RepeaterItem item in repeaterToHideApprove.Items)
                    {
                        LinkButton lnkApprove = (LinkButton)item.FindControl("lnkApprove");
                        //if the request type is other than this then we first will be deleting a device and then registering 
                        //so the count would remain same.and hence we could allow this
                        if (lnkApprove.CommandArgument == ((int)REGISTRATION_DEVICE_REQUEST_TYPE.ALLOW_USER_DEVICE).ToString())
                        {
                            lnkApprove.Visible = false;
                        }
                    }
                    Utilities.showAlert("Since you have reached your max allowed registration,you have to first delete existing devices to approve any new request", "divInfomationForApproveDisable", updRepeater);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="headerInfo"></param>
        /// <param name="repeaterName">Pass "Device" for registered device repeater else empty</param>
        void showRepeaterHeaderInfo(string headerInfo, REPEATER_NAME rptName)
        {
            string strHeaderText = "<h1>" + headerInfo + "</h1>";
            switch (rptName)
            {
                case REPEATER_NAME.ModalPendingDevices:
                    lblModalPendingDevicesHeader.Text = strHeaderText;
                    lblModalPendingDevicesHeader.Visible = true;
                    break;
                case REPEATER_NAME.ModalRegisteredDevices:
                    lblModalRegtredHdr.Text = strHeaderText;
                    lblModalRegtredHdr.Visible = true;
                    break;
                case REPEATER_NAME.PendingDevices:
                case REPEATER_NAME.RegisteredDevices:
                    lblHeaderInfo.Text = strHeaderText;
                    lblHeaderInfo.Visible = true;
                    break;
            }
        }
        #endregion




        #region registered and pending requests repeater
        protected void rptRegisteredDeviceDtls_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblRegistrationDate = (Label)e.Item.FindControl("lblRegistrationDate");
                long lngRegistrationDate = (long)DataBinder.Eval(e.Item.DataItem, "REGISTRATION_DATE");

                try
                {

                    lblRegistrationDate.Text = Utilities.getCompanyLocalFormattedDate(CompanyTimezone.CmpTimeZoneInfo, lngRegistrationDate);
                }
                catch
                {
                    Utilities.showMessage("Internal server error", updRepeater, DIALOG_TYPE.Error);
                    return;
                }

            }
        }

        protected void rptPendingRequest_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            showHideApproveOnItemBound(e, (Repeater)sender);
        }
        void showHideApproveOnItemBound(RepeaterItemEventArgs e, Repeater repeater)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblDeviceType = (Label)e.Item.FindControl("lblDeviceType");
                Label lblDeviceModel = (Label)e.Item.FindControl("lblDeviceModel");
                LinkButton lnkName = (LinkButton)e.Item.FindControl("lnkName");

                Label lblDoubleAsterix = (Label)e.Item.FindControl("lblItemDoubleAsterix");
                lblDoubleAsterix.Visible = false;
                if ((string)DataBinder.Eval(e.Item.DataItem, "DEVICE_ID") == Convert.ToString(DataBinder.Eval(e.Item.DataItem, "RegDeviceId")))
                {
                    LinkButton lnkApprove = (LinkButton)e.Item.FindControl("lnkApprove");
                    lnkApprove.Visible = false;
                    lblDoubleAsterix.Visible = true;
                    hidPendingDoubleAsterix.Value = "True";
                }
                if (repeater.ID.ToLower() == "rptPendingRequest".ToLower())
                {
                    Label lblRequestDate = (Label)e.Item.FindControl("lblRequestDate");
                    long lngRequestDate = (long)DataBinder.Eval(e.Item.DataItem, "REQUEST_DATETIME");
                    lblRequestDate.Text = Utilities.getCompanyLocalFormattedDate(CompanyTimezone.CmpTimeZoneInfo, lngRequestDate);
                    lnkName.Text = (string)DataBinder.Eval(e.Item.DataItem, "FIRST_NAME") + " " + (string)DataBinder.Eval(e.Item.DataItem, "LAST_NAME");
                }
            }

        }
        protected void rptPendingRequest_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Approve":
                    processApprove(e, (Repeater)sender);
                    break;
                case "Deny":
                    processDeny(e);
                    break;
                case "Details":
                    processShowDetail(e);
                    break;
                case "DetailsOfDevices":
                    processShowDetailOfDevices(e, (Repeater)sender, DEVICE_DETAILS_SHOW_PURPOSE.Information);
                    break;
            }
        }
        protected void rptRegisteredDeviceDtls_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Delete":
                    processDeleteRegisteredDevice(e);
                    break;
                case "Details":
                    processShowDetail(e);
                    break;
            }
        }
        void processApprove(RepeaterCommandEventArgs e, Repeater pendingRequestRpt)
        {
            Label lblUserId = (Label)e.Item.FindControl("lblUserId");
            Label lblDeviceType = (Label)e.Item.FindControl("lblDeviceType");
            Label lblDeviceId = (Label)e.Item.FindControl("lblDeviceId");
            Label lblPushMsgId = (Label)e.Item.FindControl("lblPushMsgId");
            Label lblOSVersion = (Label)e.Item.FindControl("lblOSVersion");
            //if process is delete and approve then we only have to show the pop up and further processing is after he selects his registered device to delete
            if (Convert.ToInt32(((LinkButton)e.Item.FindControl("lnkApprove")).CommandArgument) == (int)REGISTRATION_DEVICE_REQUEST_TYPE.DELETE_AND_APPROVE)//now its not delete all devices.only selected devices 12/10/2012
            {
                processShowDetailOfDevices(e, pendingRequestRpt, DEVICE_DETAILS_SHOW_PURPOSE.Processing);
                hidDeviceToApprove.Value = lblUserId.Text + ";" + lblDeviceType.Text + ";" + lblDeviceId.Text + ";" + lblPushMsgId.Text + ";" + lblOSVersion.Text;
                return;
            }

            approveRequestsByType(REGISTRATION_DEVICE_REQUEST_TYPE.ALLOW_USER_DEVICE, lblUserId.Text, lblDeviceType.Text, lblDeviceId.Text, lblPushMsgId.Text, lblOSVersion.Text, null);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestType"></param>
        /// <param name="userId"></param>
        /// <param name="deviceType"></param>
        /// <param name="deviceId"></param>
        /// <param name="devicesToDelete">Pass null in case of approve device</param>
        void approveRequestsByType(REGISTRATION_DEVICE_REQUEST_TYPE requestType, string userId
            , string deviceType, string deviceId, string devPushMsgId
            , string osVersion, List<MFEDevice> devicesToDelete)
        {
            PENDING_DEVICES_ORDER_BY ePendingDevicesOrderBy;
            SORT_TYPE eSortType;
            try
            {
                string strMessage = "";
                GetRequestForRegisteringDevice objRequestDtls = new GetRequestForRegisteringDevice(hfsPart4, strUserId,
                    PENDING_DEVICES_ORDER_BY.RequestDate, SORT_TYPE.ASC);
                objRequestDtls.Process();
                DataSet dsPendingRequestDtls = objRequestDtls.ResultTables;
                if (dsPendingRequestDtls != null && dsPendingRequestDtls.Tables.Count > 0)
                {
                    if (dsPendingRequestDtls.Tables[0] != null && dsPendingRequestDtls.Tables[0].Rows.Count > 0)
                    {
                        string strFilter = String.Format("USER_ID ='{0}' AND DEVICE_ID ='{1}'", userId, deviceId);
                        DataRow[] rows = dsPendingRequestDtls.Tables[0].Select(strFilter);
                        if (rows.Length > 0)
                        {

                            //check for the total  registration count is valid
                            if (dsPendingRequestDtls.Tables[2].Rows.Count>0 && Convert.ToInt32(dsPendingRequestDtls.Tables[2].Rows[0]["TOTAL"]) == Convert.ToInt32(dsPendingRequestDtls.Tables[2].Rows[0]["MAX_USER"]))
                            {
                                Utilities.showMessage("Reached the maximum allowed registration limit.No more devices can be approved", updRepeater, DIALOG_TYPE.Error);
                                return;
                            }
                            //check for max devices allowed for the user
                            if (!(requestType == REGISTRATION_DEVICE_REQUEST_TYPE.DELETE_AND_APPROVE && devicesToDelete.Count != 0))
                            {
                                string strFilterForMaxDevices = String.Format("USER_ID ='{0}'", userId);
                                DataRow[] rowsForMaxDevices = dsPendingRequestDtls.Tables[5].Select(strFilterForMaxDevices);

                                if (rowsForMaxDevices.Length > 0)
                                {
                                    if (String.IsNullOrEmpty(Convert.ToString(rowsForMaxDevices[0]["MAX_DEVICE"])))
                                    {
                                        if (Convert.ToInt32(rowsForMaxDevices[0]["REGISTERED_DEVICE_COUNT"]) == Convert.ToInt32(rowsForMaxDevices[0]["MAX_DEVICE_PER_USER"]))
                                        {
                                            if (requestType == REGISTRATION_DEVICE_REQUEST_TYPE.DELETE_AND_APPROVE)
                                            {
                                                strMessage = "Reached the maximum allowed registration limit for the user.Please select atleast one device to remove before proceeding";
                                            }
                                            else
                                            {
                                                strMessage = "Reached the maximum allowed registration limit for the user.No more devices can be approved.";
                                            }
                                            Utilities.showMessage(strMessage, updRepeater, DIALOG_TYPE.Error);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(rowsForMaxDevices[0]["REGISTERED_DEVICE_COUNT"]) == Convert.ToInt32(rowsForMaxDevices[0]["MAX_DEVICE"]))
                                        {
                                            if (requestType == REGISTRATION_DEVICE_REQUEST_TYPE.DELETE_AND_APPROVE)
                                            {
                                                strMessage = "Reached the maximum allowed registration limit for the user.Please select atleast one device to remove before proceeding";
                                            }
                                            else
                                            {
                                                strMessage = "Reached the maximum allowed registration limit for the user.No more devices can be approved.";
                                            }
                                            Utilities.showMessage(strMessage, updRepeater, DIALOG_TYPE.Error);
                                            return;
                                        }
                                    }
                                }
                            }



                            DeviceRegistrationProcessByRqstType objDeviceReg = null;
                            switch (requestType)
                            {
                                case REGISTRATION_DEVICE_REQUEST_TYPE.ALLOW_USER_DEVICE:
                                    objDeviceReg =
                                        new DeviceRegistrationProcessByRqstType(
                                            hfsPart4, strUserId, userId, hfsPart3,
                                            Convert.ToInt32(rows[0]["REQUEST_TYPE"]),
                                            deviceType, deviceId, (string)rows[0]["DEVICE_MODEL"],
                                            devPushMsgId, osVersion,
                                            (DEVICE_OR_ICON_SIZE)Enum.Parse( typeof(DEVICE_OR_ICON_SIZE),Convert.ToString(rows[0]["DEVICE_SIZE"])),
                                            this.Context, Convert.ToString(rows[0]["APP_VERSION"]));
                                    strMessage = "Device " + "(" + (string)rows[0]["DEVICE_MODEL"] + ") approved for user";
                                    break;
                                case REGISTRATION_DEVICE_REQUEST_TYPE.DELETE_AND_APPROVE:
                                    objDeviceReg = new
                                        DeviceRegistrationProcessByRqstType(hfsPart4,
                                        strUserId, userId, hfsPart3,
                                        Convert.ToInt32(rows[0]["REQUEST_TYPE"]),
                                        deviceType, deviceId,
                                        (string)rows[0]["DEVICE_MODEL"], devPushMsgId,
                                        osVersion,
                                       (DEVICE_OR_ICON_SIZE)Enum.Parse(typeof(DEVICE_OR_ICON_SIZE), Convert.ToString(rows[0]["DEVICE_SIZE"])),
                                        devicesToDelete,
                                        this.Context, Convert.ToString(rows[0]["APP_VERSION"]));


                                    if (devicesToDelete.Count > 0)
                                        strMessage = "Selected device(s) were deleted and " + "Device " + "(" + (string)rows[0]["DEVICE_MODEL"] + ") approved for user";
                                    else
                                        strMessage = "Device " + "(" + (string)rows[0]["DEVICE_MODEL"] + ") approved for user";
                                    break;
                            }

                            objDeviceReg.Process();
                            if (objDeviceReg.StatusCode == 0)
                            {
                                Utilities.closeModalPopUp("divModalRegisteredPendingDevicesDtls", updRepeater, "Closing Pop Up");
                                Utilities.showMessage(strMessage, updRepeater, DIALOG_TYPE.Info);
                                getCurrSortOrderOfPendingDevRpt(out ePendingDevicesOrderBy, out eSortType);
                                int iRowsCount;
                                bindPendingRequestRepeater(ePendingDevicesOrderBy, eSortType, out iRowsCount);

                            }
                            else if (objDeviceReg.StatusCode == 1)
                            {
                                Utilities.showMessage(objDeviceReg.StatusDescription, updRepeater, DIALOG_TYPE.Error);

                                Utilities.runPostBackScript(@"hideCompleteTab(true);", updRepeater, "hideCompleteTab");
                            }
                            else
                            {
                                Utilities.showMessage("Internal Error", updRepeater, DIALOG_TYPE.Error);

                                Utilities.runPostBackScript(@"hideCompleteTab(true);", updRepeater, "hideCompleteTab");
                            }
                        }
                    }
                    else
                    {
                        Utilities.showMessage("Internal Error", updRepeater, DIALOG_TYPE.Error);
                        Utilities.runPostBackScript(@"hideCompleteTab(true);", updRepeater, "hideCompleteTab");
                    }
                }
                else
                {
                    Utilities.showMessage("Internal Error", updRepeater, DIALOG_TYPE.Error);

                    Utilities.runPostBackScript(@"hideCompleteTab(true);", updRepeater, "hideCompleteTab");
                }
            }
            catch
            {
                Utilities.showMessage("Internal error.", updRepeater, DIALOG_TYPE.Error);

                Utilities.runPostBackScript(@"hideCompleteTab(true);", updRepeater, "hideCompleteTab");
            }
        }


        void processDeny(RepeaterCommandEventArgs e)
        {
            PENDING_DEVICES_ORDER_BY ePendingDevicesOrderBy;
            SORT_TYPE eSortType;
            Label lblUserId = (Label)e.Item.FindControl("lblUserId");
            Label lblDeviceType = (Label)e.Item.FindControl("lblDeviceType");
            Label lblDeviceId = (Label)e.Item.FindControl("lblDeviceId");
            Label lblPushMsgId = (Label)e.Item.FindControl("lblPushMsgId");
            Label lblOSVersion = (Label)e.Item.FindControl("lblOSVersion");
            GetRequestForRegisteringDevice objRequestDtls = new GetRequestForRegisteringDevice(hfsPart4, strUserId
                , PENDING_DEVICES_ORDER_BY.RequestDate, SORT_TYPE.ASC);
            objRequestDtls.Process();
            DataSet dsPendingRequestDtls = objRequestDtls.ResultTables;
            if (dsPendingRequestDtls != null && dsPendingRequestDtls.Tables.Count > 0)
            {
                if (dsPendingRequestDtls.Tables[0] != null && dsPendingRequestDtls.Tables[0].Rows.Count > 0)
                {
                    string filter = String.Format("USER_ID ='{0}' AND DEVICE_ID ='{1}'", lblUserId.Text, lblDeviceId.Text);
                    DataRow[] rows = dsPendingRequestDtls.Tables[0].Select(filter);
                    if (rows.Length > 0)
                    {
                        DeviceRegistrationProcessByRqstType objDeviceReg = new DeviceRegistrationProcessByRqstType(hfsPart4, strUserId, lblUserId.Text, hfsPart3
                            , (int)REGISTRATION_DEVICE_REQUEST_TYPE.DENY, lblDeviceType.Text, lblDeviceId.Text, (string)rows[0]["DEVICE_MODEL"], lblPushMsgId.Text, lblOSVersion.Text
                            , (DEVICE_OR_ICON_SIZE)Enum.Parse(typeof(DEVICE_OR_ICON_SIZE), Convert.ToString(rows[0]["DEVICE_SIZE"])), this.Context, Convert.ToString(rows[0]["APP_VERSION"]));
                        objDeviceReg.Process();
                        if (objDeviceReg.StatusCode == 0)
                        {
                            Utilities.showMessage("Denied successfully", updRepeater, DIALOG_TYPE.Info);
                            getCurrSortOrderOfPendingDevRpt(out ePendingDevicesOrderBy, out eSortType);
                            int iRowsCount;
                            bindPendingRequestRepeater(ePendingDevicesOrderBy, eSortType, out iRowsCount);
                        }
                        else
                            Utilities.showMessage("Internal Error", updRepeater, DIALOG_TYPE.Error);
                    }
                }
                else
                    Utilities.showMessage("Internal Error", updRepeater, DIALOG_TYPE.Error);
            }
            else
                Utilities.showMessage("Internal Error", updRepeater, DIALOG_TYPE.Error);
        }

        void processShowDetail(RepeaterCommandEventArgs e)
        {
            Label lblUserId = (Label)e.Item.FindControl("lblUserId");
            Label lblUserName = (Label)e.Item.FindControl("lblUserName");
            ucUserDetails.UserId = lblUserId.Text;
            ucUserDetails.UserName = lblUserName.Text;
            ucUserDetails.rememberUserId();
            ucUserDetails.getUserDetailsAndFillData(lblUserId.Text, hfsPart4);
            ucUserDetails.FindControl("lnkEditUser").Visible = false;
            ucUserDetails.FindControl("lnkBlockUser").Visible = false;
            ucUserDetails.FindControl("lnkUnblockUser").Visible = false;
            Utilities.showModalPopup("divUserDtlsModal", updRepeater, "User Detail", Convert.ToString(Utilities.getModalPopUpWidth(MODAL_POP_UP_NAME.USER_DETAIL)), true, false);
            ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "HideSeparator", "hideHTMLElements('ucSeparatorOne');", true);
            updUserDtlsModal.Update();
        }

        void processShowDetailOfDevices(RepeaterCommandEventArgs e, Repeater rpt, DEVICE_DETAILS_SHOW_PURPOSE showPurpose)
        {
            Label lblUserId = (Label)e.Item.FindControl("lblUserId");
            Label lblUserName = (Label)e.Item.FindControl("lblUserName");
            LinkButton lnkName = (LinkButton)e.Item.FindControl("lnkName");
            showDetailsOfDevices(lblUserId.Text.Trim(), lnkName.Text.Trim(), rpt, showPurpose);

        }
        void showDetailsOfDevices(string userId, string fullNameOfUser, Repeater eventGenerationRepeater, DEVICE_DETAILS_SHOW_PURPOSE showPurpose)
        {
            //returning -1000 for error
            int iStatus = bindAndShowHideModalRegisteredDeviceRpt(userId.Trim(), showPurpose);
            if (iStatus == 0)
            {
                iStatus = bindAndShowHideModalPendingDeviceRpt(userId.Trim(), showPurpose);
                if (iStatus == 0)
                {
                    if (eventGenerationRepeater.ID.Trim() == "rptPendingRequest".Trim())
                    {
                        Utilities.showModalPopup("divModalRegisteredPendingDevicesDtls", updRepeater, "Device Details ( " + fullNameOfUser + " )", Utilities.getModalPopUpWidth(MODAL_POP_UP_NAME.DEVICE_DETAILS_REPEATER).ToString(), true);
                    }
                    switch (showPurpose)
                    {
                        case DEVICE_DETAILS_SHOW_PURPOSE.Processing:

                            btnDelete.Visible = true;
                            ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "HidePendingTab", "hideCompleteTab(true);", true);
                            break;
                        case DEVICE_DETAILS_SHOW_PURPOSE.Information:

                            btnDelete.Visible = false;
                            ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "ShowPendingTab", "hideCompleteTab(false);", true);
                            break;
                    }
                    updRegtdPendingDtls.Update();
                }
                else
                {
                    Utilities.showMessage("Internal server error.Please try again", updRepeater, DIALOG_TYPE.Error);
                }
            }
            else
            {
                Utilities.showMessage("Internal error.Please try again", updRepeater, DIALOG_TYPE.Error);
            }
        }

        void processDeleteRegisteredDevice(RepeaterCommandEventArgs e)
        {
            Label lblUserId = (Label)e.Item.FindControl("lblUserId");
            Label lblDeviceType = (Label)e.Item.FindControl("lblDeviceType");
            Label lblDeviceId = (Label)e.Item.FindControl("lblDeviceId");
            Label lblPushMsgId = (Label)e.Item.FindControl("lblPushMsgId");
            Label lblOSVersion = (Label)e.Item.FindControl("lblOSVersion");
            Label lblUserName = (Label)e.Item.FindControl("lblUserName");
            GetRegisteredDeviceDetails objRegtDeviceDtls = new GetRegisteredDeviceDetails(hfsPart4, strUserId
                , REGISTERED_DEVICES_ORDER_BY.RequestDate
                , SORT_TYPE.DESC);
            objRegtDeviceDtls.Process();
            DataSet dsRegtDeviceDtls = objRegtDeviceDtls.ResultTables;
            if (dsRegtDeviceDtls != null && dsRegtDeviceDtls.Tables.Count > 0)
            {
                if (dsRegtDeviceDtls.Tables[0] != null && dsRegtDeviceDtls.Tables[0].Rows.Count > 0)
                {
                    string filter = String.Format("USER_ID ='{0}' AND DEVICE_ID ='{1}'", lblUserId.Text, lblDeviceId.Text);
                    DataRow[] rows = dsRegtDeviceDtls.Tables[0].Select(filter);
                    if (rows.Length > 0)
                    {
                        DeviceRegistrationProcessByRqstType objDeviceReg = new
                            DeviceRegistrationProcessByRqstType(hfsPart4,
                            strUserId, lblUserId.Text, hfsPart3
                            , (int)REGISTRATION_DEVICE_REQUEST_TYPE.DELETE_REGISTERED_USER_DEVICE, lblDeviceType.Text
                            , lblDeviceId.Text, (string)rows[0]["DEVICE_MODEL"], lblPushMsgId.Text, lblOSVersion.Text
                            , (DEVICE_OR_ICON_SIZE)Enum.Parse(typeof(DEVICE_OR_ICON_SIZE), Convert.ToString(rows[0]["DEVICE_SIZE"])),
                            this.Context, Convert.ToString(rows[0]["APP_VERSION"]));
                        objDeviceReg.Process();
                        if (objDeviceReg.StatusCode == 0)
                        {
                            Utilities.showMessage("Deleted Successfully", updRepeater, DIALOG_TYPE.Info);
                            REGISTERED_DEVICES_ORDER_BY eRegisteredDeviceOrderBy;
                            SORT_TYPE eSortType;
                            getCurrentSortOrderOfRegisteredDevRpt(out eRegisteredDeviceOrderBy, out eSortType);
                            int iRowsCount;
                            bindRegisteredDevicesRepeater(eRegisteredDeviceOrderBy, eSortType, out  iRowsCount);

                            try
                            {
                                SavePushMsgOutboxInMgram objSavePshMsgOutbox = new
                                                        SavePushMsgOutboxInMgram(hfsPart4,
                                                        lblUserName.Text,
                                                        lblDeviceId.Text,
                                                        lblPushMsgId.Text,
                                                        lblDeviceType.Text,
                                                        SavePushMsgOutboxInMgram.NOTIFICATION_TYPE.MF_USER_BLOCK,"");
                                objSavePshMsgOutbox.Process();
                            }
                            catch
                            { }
                        }
                        else
                        {
                            Utilities.showMessage("Internal Error", updRepeater, DIALOG_TYPE.Error);
                        }
                    }
                }
                else
                {
                    Utilities.showMessage("Internal Error", updRepeater, DIALOG_TYPE.Error);
                }
            }
            else
            {
                Utilities.showMessage("Internal Error", updRepeater, DIALOG_TYPE.Error);
            }
        }


        #endregion
        #region Search And Button commands

        protected void lnkViewCompleteList_Click(object sender, EventArgs e)
        {
            SORT_TYPE eSortType;
            if (radListSelection.SelectedValue == "1")
            {
                REGISTERED_DEVICES_ORDER_BY eRegisteredDeviceOrderBy;
                getCurrentSortOrderOfRegisteredDevRpt(out eRegisteredDeviceOrderBy, out eSortType);
                int iRowsCount;
                bindRegisteredDevicesRepeater(eRegisteredDeviceOrderBy, eSortType, out iRowsCount);
            }
            else
            {
                PENDING_DEVICES_ORDER_BY ePendingDevicesOrderBy;
                getCurrSortOrderOfPendingDevRpt(out ePendingDevicesOrderBy, out eSortType);
                int iRowsCount;
                bindPendingRequestRepeater(ePendingDevicesOrderBy, eSortType, out iRowsCount);
            }
        }

        protected void lnkRefreshList_Click(object sender, EventArgs e)
        {
            txtFilterSearch.Text = "";
            SORT_TYPE eSortType;
            int iRowsCount;
            if (radListSelection.SelectedValue == "1")
            {
                REGISTERED_DEVICES_ORDER_BY eRegisteredDevOrderBy;
                getCurrentSortOrderOfRegisteredDevRpt(out eRegisteredDevOrderBy, out eSortType);

                bindRegisteredDevicesRepeater(eRegisteredDevOrderBy, eSortType, out iRowsCount);
            }
            else
            {
                PENDING_DEVICES_ORDER_BY ePendingDevicesOrderBy;
                getCurrSortOrderOfPendingDevRpt(out ePendingDevicesOrderBy, out eSortType);
                bindPendingRequestRepeater(ePendingDevicesOrderBy, eSortType, out iRowsCount);
            }
        }
        #endregion


        void fillInformationLabels(DataTable registeredDeviceCountDTable, DataTable pendingRequestCountDTable, DataTable maxAllowedRegisteredDeviceDTable)
        {
            if (registeredDeviceCountDTable != null && registeredDeviceCountDTable.Rows.Count > 0)
            {
                lblApprovedDevice.Text = Convert.ToString(registeredDeviceCountDTable.Rows[0]["Total"]);
            }
            else
            {
                lblApprovedDevice.Text = "0";
            }
            if (pendingRequestCountDTable != null && pendingRequestCountDTable.Rows.Count > 0)
            {
                lblPendingRequest.Text = Convert.ToString(pendingRequestCountDTable.Rows[0]["Total"]);
            }
            else
            {
                lblPendingRequest.Text = "0";
            }
            if (maxAllowedRegisteredDeviceDTable != null && maxAllowedRegisteredDeviceDTable.Rows.Count > 0)
            {
                lblMaxRegisteredDev.Text = Convert.ToString(maxAllowedRegisteredDeviceDTable.Rows[0]["MAX_USER"]);
            }
            else
            {
                lblMaxRegisteredDev.Text = "0";
            }
        }

        #region modal repeater region
        protected void rptModalRegisteredDevicesDtls_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox chkDevices = (CheckBox)e.Item.FindControl("chkRegisteredDevices");
                if (!(bool)DataBinder.Eval(e.Item.DataItem, "FOR_PROCESSING"))
                {
                    chkDevices.Visible = false;
                }
                else
                {
                    chkDevices.Visible = true;
                }
            }
        }

        protected void rptModalPendingRequestDtls_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblFullName = (Label)e.Item.FindControl("lblFullName");
                lblFullName.Text = (string)DataBinder.Eval(e.Item.DataItem, "FIRST_NAME") + " " + (string)DataBinder.Eval(e.Item.DataItem, "LAST_NAME");
            }
        }
        protected void rptModalPendingRequestDtls_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Approve":
                    processApprove(e, (Repeater)sender);
                    Label lblUserId = (Label)e.Item.FindControl("lblUserId");
                    Label lblFullName = (Label)e.Item.FindControl("lblFullName");
                    showDetailsOfDevices(lblUserId.Text.Trim(), lblFullName.Text.Trim(), (Repeater)sender, DEVICE_DETAILS_SHOW_PURPOSE.Processing);//binding the repeater again
                    break;
            }
        }
        #endregion


        void clearControlsUsedInEdit()
        {

            hidDesignationIdForEdit.Value = "";
            hidPopUpMode.Value = "";
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (rptModalRegisteredDevicesDtls.Items.Count > 0)//if the admin has deleted all the device before approving (the delete and approve) request
            {
                bool blnIsAnyDeviceSelectedForDeleting = false;
                try
                {
                    string userId = ((Label)rptModalRegisteredDevicesDtls.Items[0].FindControl("lblUserId")).Text;
                    foreach (RepeaterItem item in rptModalRegisteredDevicesDtls.Items)
                    {
                        CheckBox chkRegisteredDevices = (CheckBox)item.FindControl("chkRegisteredDevices");
                        if (chkRegisteredDevices.Checked)
                        {
                            blnIsAnyDeviceSelectedForDeleting = true;
                            break;
                        }
                    }
                    if (blnIsAnyDeviceSelectedForDeleting)
                    {

                    }
                    else
                    {
                        GetAccountSettingsByCompanyId objAccSett =
                            new GetAccountSettingsByCompanyId(hfsPart4);
                        objAccSett.Process();

                        if (objAccSett.StatusCode != 0)
                        {
                            throw new Exception("");
                        }
                        GetDeviceSettingsForExceptionalUser objDevSetngsForUser =
                            new GetDeviceSettingsForExceptionalUser(userId, hfsPart4);
                        objDevSetngsForUser.Process();

                        if (objDevSetngsForUser.StatusCode == 0)
                        {
                            DataTable dtblDevStnsForUser = objDevSetngsForUser.ResultTable;
                            if (dtblDevStnsForUser == null) { throw new Exception(); }
                            if (dtblDevStnsForUser.Rows.Count > 0)
                            {
                                /**
                                 * if max_device is not set then that means he not an exceptional user.
                                 * so we have toc compare with the account settings for the company.
                                 * COmpare Aleady registered devices with Max device if exceptional user.
                                 * else compare already registered devices with  max device per userd.
                                 * **/
                                int iAlreadyRegisteredDevcs =
                                    Convert.ToInt32(
                                    dtblDevStnsForUser.Rows[0]["ALREADY_REGISTERED"]);

                                if (!String.IsNullOrEmpty(Convert.ToString(dtblDevStnsForUser.Rows[0]["MAX_DEVICE"])))
                                {
                                    //already registerd will never be greater than max_device.can be equal to or less than
                                    if (iAlreadyRegisteredDevcs ==
                                        Convert.ToInt32(dtblDevStnsForUser.Rows[0]["MAX_DEVICE"]))
                                    {
                                        Utilities.showAlert("Please select a device to delete.The user cannot have more than " +
                                            Convert.ToString(dtblDevStnsForUser.Rows[0]["MAX_DEVICE"]) +
                                            " devices registered against his name", "divAlertStatus", updRegtdPendingDtls);
                                        return;
                                    }
                                }
                                else
                                {
                                    //registered devices should be less than MAX_DEVICE_PER_USER for a company.
                                    if (iAlreadyRegisteredDevcs ==
                                        objAccSett.MaxDevicePerUser)
                                    {
                                        Utilities.showAlert("Please select a device to delete.The user cannot have more than " +
                                            Convert.ToString(objAccSett.MaxDevicePerUser) +
                                            " devices registered against his name", "divAlertStatus", updRegtdPendingDtls);
                                        return;
                                    }
                                }
                            }
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }
                    processDeleteRegDeviceAndApproveRequest();
                }
                catch
                {
                    Utilities.showMessage("Internal server error", updRegtdPendingDtls, DIALOG_TYPE.Error);
                }
            }
            else
            {
                try
                {
                    processDeleteRegDeviceAndApproveRequest();
                }
                catch
                {
                    Utilities.showMessage("Internal server error", updRegtdPendingDtls, DIALOG_TYPE.Error);
                }
            }
        }
        void processDeleteRegDeviceAndApproveRequest()
        {
            //deviceId;deviceType;deviceModel;IMEI;OsVersion
            //deviceId;deviceType;deviceModel;DeviceSize;OsVersion
            List<MFEDevice> lstDevicesToDelete = new List<MFEDevice>();
            List<string> lstDevicesToDeny = new List<string>();
            foreach (RepeaterItem item in rptModalRegisteredDevicesDtls.Items)
            {
                Label lblDeviceId = (Label)item.FindControl("lblDeviceId");
                Label lblDeviceSize = (Label)item.FindControl("lblDeviceSize");
                Label lblDeviceType = (Label)item.FindControl("lblDeviceType");
                Label lblDeviceModel = (Label)item.FindControl("lblDeviceModel");
                Label lblOSVersion = (Label)item.FindControl("lblOSVersion");
                Label lblPushMsgId = (Label)item.FindControl("lblPushMsgId");
                CheckBox chkRegisteredDevices = (CheckBox)item.FindControl("chkRegisteredDevices");
                if (chkRegisteredDevices.Checked)
                {
                    //lstDevicesToDelete.Add(lblDeviceId.Text + ";" + lblDeviceType.Text + ";" + lblDeviceModel.Text + ";" + lblDeviceSize.Text + ";" + lblOSVersion.Text);
                    MFEDevice objDevice = new MFEDevice();
                    objDevice.DeviceId = lblDeviceId.Text;
                    objDevice.DeviceType = lblDeviceType.Text;
                    objDevice.DeviceModel = lblDeviceModel.Text;
                    objDevice.DeviceSize = Convert.ToInt32(lblDeviceSize.Text);
                    objDevice.DevicePushMsgId = lblPushMsgId.Text;
                    lstDevicesToDelete.Add(objDevice);
                }
            }

            string[] aryDeviceToApprove = hidDeviceToApprove.Value.Split(';');//userId;DeviceType;DeviceId,PushMessageId,OSVersion
            approveRequestsByType(REGISTRATION_DEVICE_REQUEST_TYPE.DELETE_AND_APPROVE,
                aryDeviceToApprove[0], aryDeviceToApprove[1],
                aryDeviceToApprove[2], aryDeviceToApprove[3],
                aryDeviceToApprove[4], lstDevicesToDelete);
        }

        protected void radListSelection_SelectedIndexChanged1(object sender, EventArgs e)
        {
            txtFilterSearch.Text = "";
            int iRowsCount;
            if (radListSelection.SelectedValue == "1")
            {
                initialiseCurrentSortOrder(REPEATER_NAME.RegisteredDevices);

                bindRegisteredDevicesRepeater(REGISTERED_DEVICES_ORDER_BY.RequestDate, SORT_TYPE.DESC, out  iRowsCount);
                rptPendingRequest.Visible = false;
            }
            else
            {
                initialiseCurrentSortOrder(REPEATER_NAME.PendingDevices);
                bindPendingRequestRepeater(PENDING_DEVICES_ORDER_BY.RequestDate, SORT_TYPE.DESC, out  iRowsCount);
                rptRegisteredDeviceDtls.Visible = false;
            }
        }

        protected void lnkImageSearch_Click(object sender, EventArgs e)
        {
            if (txtFilterSearch.Text == "")
            {
                Utilities.showAlert("Please enter some data to search", "divSearchError", updRepeater);
                return;
            }
            SORT_TYPE eSortType;
            int iRowsCount;
            if (radListSelection.SelectedValue == "1")
            {
                REGISTERED_DEVICES_ORDER_BY eRegisteredDeviceOrderBy;
                getCurrentSortOrderOfRegisteredDevRpt(out eRegisteredDeviceOrderBy, out eSortType);
                bindRegisteredDevicesRepeaterForSearch(eRegisteredDeviceOrderBy, eSortType, out  iRowsCount);
            }
            else
            {
                PENDING_DEVICES_ORDER_BY ePendingDevicesOrderBy;
                getCurrSortOrderOfPendingDevRpt(out ePendingDevicesOrderBy, out eSortType);
                bindPendingRequestRepeaterForSearch(ePendingDevicesOrderBy, eSortType, out  iRowsCount);
            }
        }
        protected void btnRowClickPostBack_Click(object sender, EventArgs e)
        {
            if (hidSortingDetail.Value.Trim().Length != 0)//for sorting
            {
                //values repeatername,columnIndex,sorttype
                string[] aryValuesFromHidField = hidSortingDetail.Value.Split(',');
                string[] hfsValues = Utilities.DecryptString(hfsValue).Split(',');
                try
                {
                    int iRowsCount = 0;
                    REPEATER_NAME eRepeater = (REPEATER_NAME)Enum.Parse(typeof(REPEATER_NAME), aryValuesFromHidField[0]);
                    if (Enum.IsDefined(typeof(REPEATER_NAME), eRepeater))
                    {
                        switch (eRepeater)
                        {
                            case REPEATER_NAME.RegisteredDevices:
                                if (String.IsNullOrEmpty(txtFilterSearch.Text))
                                {
                                    bindRegisteredDevicesRepeater((REGISTERED_DEVICES_ORDER_BY)Enum.Parse(typeof(REGISTERED_DEVICES_ORDER_BY), aryValuesFromHidField[1])
                                        , (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), aryValuesFromHidField[2]), out  iRowsCount);
                                }
                                else
                                {
                                    bindRegisteredDevicesRepeaterForSearch((REGISTERED_DEVICES_ORDER_BY)Enum.Parse(typeof(REGISTERED_DEVICES_ORDER_BY), aryValuesFromHidField[1])
                                        , (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), aryValuesFromHidField[2]), out  iRowsCount);
                                }
                                break;
                            case REPEATER_NAME.PendingDevices:
                                if (String.IsNullOrEmpty(txtFilterSearch.Text))
                                {
                                    bindPendingRequestRepeater((PENDING_DEVICES_ORDER_BY)Enum.Parse(typeof(PENDING_DEVICES_ORDER_BY), aryValuesFromHidField[1]),
                                        (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), aryValuesFromHidField[2]), out  iRowsCount);
                                }
                                else
                                {
                                    bindPendingRequestRepeaterForSearch((PENDING_DEVICES_ORDER_BY)Enum.Parse(typeof(PENDING_DEVICES_ORDER_BY), aryValuesFromHidField[1]),
                                        (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), aryValuesFromHidField[2]), out  iRowsCount);
                                }
                                break;
                        }

                    }
                    if (iRowsCount == 0)
                    {
                        hidSortingDetail.Value = "";//if no record found or no sorting took place then previous sorting should not change
                    }
                    else
                    {
                        Utilities.runPostBackScript("changeImageOfSortedCol();clearSortingDtlHiddenField();", updRepeater, "Change Sort Image");
                    }
                }
                catch
                {
                    Utilities.showMessage("Internal server error", updRowPostBack, DIALOG_TYPE.Error);
                    hidSortingDetail.Value = "";
                }
            }
        }

        #region Sorting Helper Functions
        void getCurrentSortOrderOfRegisteredDevRpt(out REGISTERED_DEVICES_ORDER_BY orderBy, out SORT_TYPE sortType)
        {
            orderBy = REGISTERED_DEVICES_ORDER_BY.RequestDate;
            sortType = SORT_TYPE.DESC;
            try
            {
                //values repeatername,columnIndex,sorttype
                string[] aryCurrentSortOrder = hidCurrentSortOrder.Value.Split(',');
                orderBy = (REGISTERED_DEVICES_ORDER_BY)Enum.Parse(typeof(REGISTERED_DEVICES_ORDER_BY), aryCurrentSortOrder[1]);
                sortType = (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), aryCurrentSortOrder[2]);
            }
            catch
            {

            }
        }
        void getCurrSortOrderOfPendingDevRpt(out PENDING_DEVICES_ORDER_BY orderBy, out SORT_TYPE sortType)
        {
            orderBy = PENDING_DEVICES_ORDER_BY.RequestDate;
            sortType = SORT_TYPE.DESC;
            try
            {
                //values repeatername,columnIndex,sorttype
                string[] aryCurrentSortOrder = hidCurrentSortOrder.Value.Split(',');
                orderBy = (PENDING_DEVICES_ORDER_BY)Enum.Parse(typeof(PENDING_DEVICES_ORDER_BY), aryCurrentSortOrder[1]);
                sortType = (SORT_TYPE)Enum.Parse(typeof(SORT_TYPE), aryCurrentSortOrder[2]);
            }
            catch
            {

            }
        }
        void initialiseCurrentSortOrder(REPEATER_NAME rpt)
        {
            switch (rpt)
            {
                case REPEATER_NAME.PendingDevices:
                    hidCurrentSortOrder.Value = "2" + "," + ((int)PENDING_DEVICES_ORDER_BY.RequestDate).ToString() + "," + "1";
                    break;
                case REPEATER_NAME.RegisteredDevices:
                    hidCurrentSortOrder.Value = "1" + "," + ((int)REGISTERED_DEVICES_ORDER_BY.RequestDate).ToString() + "," + "1";
                    break;
            }
        }
        #endregion

    }
}
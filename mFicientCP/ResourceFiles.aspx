﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/master/IdeMaster.Master"
    CodeBehind="ResourceFiles.aspx.cs" Inherits="mFicientCP.ResourceFiles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .deletebtn
        {
            border: 0;
            background-color: #FFFFFF;
        }
        .imgContain
        {
            background-repeat:no-repeat;
            background-size:contain;
            background-position:center center;
        }
    </style>
    <script src="Scripts/ResouceFile.js" type="text/javascript"></script>
    <script type="text/javascript">
        function UpdateUpdUploadedImage() {

            UpdateUpdBtnRowIndex();
        }
        function UpdateUpdBtnRowIndex() {
            $('[id$=<%= btnDbConnRowIndex.ClientID %>]').click();
        }

        function test() {
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainCanvas" runat="server">
    <div style="padding: 10px;">
        <section style="margin: 5px 5px 5px 3px;">
            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                    <div>
                        <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>Resource Files</h1>">
                        </asp:Label>
                    </div>
                    <div style="position: relative; top: 10px; right: 15px;">
                        <asp:LinkButton ID="lnkAddNewMobileUser" runat="server" Text="Add" OnClientClick="SubProcImageUploadIFrame(true);return false;"
                            CssClass="repeaterLink fr" />
                    </div>
                </asp:Panel>
                <div id="divResourceComplete" style="padding-top: 5px">
                    <asp:UpdatePanel runat="server" ID="upresourceobject" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divresobjleft" style="width: 100%; float: none;">
                                <div id="divresdataobject">
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:Panel ID="pnlEmptyImages" CssClass="repeaterBox-header" runat="server" Visible="false">
                    <div>
                        <asp:Label ID="lblEmptyImages" runat="server" Text="<h1>No Images uploaded yet</h1>"> </asp:Label>
                    </div>
                </asp:Panel>
                <div id="SubProcImageUploadIFrame" class="hide">
                    <asp:UpdatePanel runat="server" ID="upUploadNewImage" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="ProcImgUpd">
                                <div class="UploadImageDiv">
                                    <iframe id="ifUploadImage" runat="server" width="350px" height="50px"></iframe>
                                </div>
                                <div>
                                </div>
                                <div class="SubProcborderDiv">
                                    <div id="btnImgUpdCloseDiv" class="SubProcBtnDiv">
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:Button ID="btnDbConnRowIndex" runat="server" CssClass="hide" />
                <div id="SubProcImageDelConfirnmation">
                    <div>
                        <div>
                            <asp:HiddenField ID="hdfDelId" runat="server" />
                        </div>
                        <div>
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                        </div>
                        <div class="MarginT10">
                        </div>
                        <div class="MessageDiv">
                            <a id="aDelCfmMsg">Do you want to delete this Image File?</a>
                        </div>
                        <div class="MarginT10">
                        </div>
                        <div class="SubProcborderDiv">
                            <div class="SubProcBtnMrgn" align="center">
                                <asp:UpdatePanel runat="server" ID="UpdDeleteConfirm" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:HiddenField ID="hidjson" runat="server" />
                                        <asp:Button ID="btnDeleteCfm" runat="server" Text="  Yes  " OnClientClick="SubProcImageDelConfirnmation(false,'');"
                                            OnClick="btnDeleteCfm_Click" CssClass="InputStyle" />
                                        <input id="Button13" type="button" value="  No  " onclick="SubProcImageDelConfirnmation(false,'');"
                                            class="InputStyle" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
               
                <div id="SubProcShowUplodedImage" class="hide">
                    <div>
                        <input id="hdfUploadedImage" type="hidden" />
                    </div>
                    <div style="display: table-cell; width: 50px; vertical-align: middle;">
                        <div align="left" style="width: 100%">
                            <img id="img5" src="//enterprise.mficient.com/images/imgPrvs.png" style="cursor: pointer;"
                                onclick="GetNextUploadedImageClick(false)" />
                        </div>
                    </div>
                    <div align="center" style="display: table-cell; vertical-align: middle; height: 500px;
                        width: 800px; max-height: 500px;">
                        <div class="imgContain" style="width: 100%;height: 500px;" id="imgUplodedImage">
                        </div>
                    </div>
                    <div style="display: table-cell; width: 50px; vertical-align: middle;">
                        <div align="right" style="width: 100%">
                            <img id="img6" src="//enterprise.mficient.com/images/imgNext.png" style="cursor: pointer;"
                                onclick="GetNextUploadedImageClick(true);" />
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="SubProcborderDiv">
                        <div class="SubProcBtnDiv" align="center" style="margin-bottom: 5px;">
                            <a id="aUpdImage"></a>&nbsp;(<a id="aUpdSize"></a>)
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hfs" runat="server" />
                <asp:HiddenField ID="hfilepath" runat="server" />
                <asp:HiddenField ID="hfilepathdetails" runat="server" />
            </asp:Panel>
        </section>
    </div>
    <div id="SubProcBoxMessage">
        <div>
            <div class="MessageDiv">
                <a id="a2">Please check following points : </a>
                <br />
            </div>
            <div style="margin-top: 5px">
                <a id="aMessage"></a>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="btnOkErrorMsg" type="button" value="  OK  " onclick="SubProcBoxMessage(false);"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <!--WAIT BOX =-->
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <!-- END WAIT BOX -->
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {

            showWaitModal();
            // isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {

            hideWaitModal();
            //  isCookieCleanUpRequired('true');

        }
    </script>
    <%--<script type="text/javascript">
        function isCookieCleanUpRequired(value) {
            if (typeof value === "undefined") {
                $('#' + '<%=hidIsCleanUpRequired.ClientID %>').val('false'); ;
            }
            else {
                $('#' + '<%=hidIsCleanUpRequired.ClientID %>').val(value);
            }
        }
    </script>--%>
    <asp:HiddenField ID="hidIsCleanUpRequired" runat="server" />
</asp:Content>

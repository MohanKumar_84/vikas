﻿<%@ Page Title="mFicient | Users" Language="C#" MasterPageFile="~/master/Canvas.Master"
    AutoEventWireup="true" CodeBehind="addUserDetails.aspx.cs" Inherits="mFicientCP.addUserDetails" %>

<%@ Register TagPrefix="userDetails" TagName="Details" Src="~/UserControls/UserDetails.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script type="text/javascript">
        function rememberPasswordEntered() {
            var hidPassword = $('#' + '<%= hidPasswordEntered.ClientID %>');
            var hidReTypePasswordEntered = $('#' + '<%=hidReTypePasswordEntered.ClientID %>');
            var txtPassword = $('#' + '<%=txtPassword.ClientID %>');
            var txtRetypePassword = $('#' + '<%=txtRetypePassword.ClientID %>');
            $(hidPassword).val($(txtPassword).val());
            $(hidReTypePasswordEntered).val($(txtRetypePassword).val());
        }
        function fillPasswordEntered() {
            var hidPassword = document.getElementById('<%= hidPasswordEntered.ClientID %>');
            var hidReTypePasswordEntered = $('#' + '<%=hidReTypePasswordEntered.ClientID %>');
            var txtPassword = document.getElementById('<%=txtPassword.ClientID %>');
            var txtRetypePassword = document.getElementById('<%=txtRetypePassword.ClientID %>');
            if (hidPassword && hidReTypePasswordEntered) {
                if (hidPassword.value !== "") {
                    if (txtPassword)
                        $(txtPassword).val($(hidPassword).val());
                }
                else {
                    if (txtPassword)
                        $(txtPassword).val(""); //done for adding trimmed value.
                }
                if ($(hidReTypePasswordEntered).val() !== "") {
                    if (txtRetypePassword)
                        $(txtRetypePassword).val($(hidReTypePasswordEntered).val());
                }
                else {
                    if (txtRetypePassword)
                        $(txtRetypePassword).val(""); //done for adding trimmed value
                }
            }

        }
        function confirmBlockUnblock(lnkButton) {
            var strlnkButtonText = "";
            if ($.browser.msie) {
                strlnkButtonText = lnkButton.innerHTML;
            }
            else {
                strlnkButtonText = lnkButton.text;
            }
            var blockUnblockText = 'Are you sure you want to ' + strlnkButtonText + ' the user';
            return showConfirmation(blockUnblockText);
        }
        function stopDefualtOfDOB(txtDOB) {
            if (txtDOB) {
                txtDOB.addEventListener(
                                        'keydown', stopDefAction, false
                                    );
            }
        }
        Panels = { Repeater: 0, Form: 1 }
        function hideShowPanelForCancel(panelToShow) {
            var searchPanel = $('#' + '<%= pnlSearchRow.ClientID%>');
            var repeater = $('#' + '<%= pnlRepeaterBox.ClientID%>');
            var form = $('#' + '<%=pnlMobileUserForm.ClientID %>');
            switch (panelToShow) {
                case Panels.Repeater:
                    repeater.show();
                    searchPanel.show();
                    form.hide();
                    break;
                case Panels.Form:
                    repeater.hide();
                    searchPanel.hide();
                    form.show();
                    break;
            }
        }
        function setHiddenPostBackPanel(panel) {
            var hidField = $('#' + '<%=hidPostBackPanel.ClientID %>');
            switch (panel) {
                case Panels.Repeater:
                    $(hidField).val(0);
                    break;
                case Panels.Form:
                    $(hidField).val(1);
                    break;
            }
        }
        function showSelectedUserGroups() {
            var aryChkGroupListCheckBox = $('#' + '<%=chkLstUserGroups.ClientID %>').find('input[type="checkbox"]');
            var strGroupNameSelected = "";
            for (var i = 0; i <= aryChkGroupListCheckBox.length - 1; i++) {
                if ($(aryChkGroupListCheckBox[i]).is(':checked')) {
                    var element = $(aryChkGroupListCheckBox[i]);

                    //find the label to get the text associated
                    var $label = $("label[for='" + $(element).attr('id') + "']")
                    if ($label.length == 0) {
                        $label = $(element).parent().parent().siblings('label');
                    }
                    if ($label.length != 0) {
                        // label was found
                        if (strGroupNameSelected == "") {

                            strGroupNameSelected += $label.html() + ' ';
                        }
                        else {
                            strGroupNameSelected += ',' + ' ' + $label.html();
                        }
                    }
                }
            }
            var lblGroupList = $('#' + '<%=lblUserGroupList.ClientID %>');
            if (strGroupNameSelected === "") {
                $(lblGroupList).html('No groups selected yet');
                $(lblGroupList).attr('Text', 'No groups selected yet');
            }
            else {
                $(lblGroupList).html(strGroupNameSelected);
                $(lblGroupList).attr('Text', strGroupNameSelected);
            }

        }

        USER_DETAIL_REPEATER_COLUMNS =
        {
            NAME: 1,
            EMAIL: 2,
            LOGIN:3
        }
        REPEATER_NAME =
       {
           UserDetail: 1
       }
        SORT_TYPE =
       {
           ASC: 0,
           DESC: 1,
           NONE: 3
       }
        USER_TYPE =
       {
           CloudUser: 0,
           ActvDirUser: 1
       }
        function getIdOfTableToFind(RepeaterName) {
            var idOfTable = "";
            switch (RepeaterName) {
                case REPEATER_NAME.UserDetail:
                    idOfTable = "tblUserDtl";
                    break;
            }
            return idOfTable;
        }
        function getSortTypeClass(sortType) {
            var clsName = "";
            switch (parseInt(sortType)) {
                case SORT_TYPE.ASC:
                    clsName = "sortable_asc";
                    break;
                case SORT_TYPE.DESC:
                    clsName = "sortable_desc";
            }
            return clsName;
        }
        function changeImageOfSortedCol() {
            var hidSortingDtl = document.getElementById('<%=hidSortingDetail.ClientID %>');
            var hidCurrentSort = document.getElementById('<%=hidCurrentSortOrder.ClientID %>');
            var arySortingDtl = "";
            if ($(hidSortingDtl).val() == "") {
                arySortingDtl = $(hidCurrentSort).val().split(',');
            }
            else {
                //split will give repeatername,columnIndex,sorttype
                arySortingDtl = $(hidSortingDtl).val().split(',');
            }
            var className = getSortTypeClass(arySortingDtl[2]);
            $($('#' + getIdOfTableToFind(parseInt(arySortingDtl[0]))).find('thead tr th')[parseInt(arySortingDtl[1])]).addClass(className);
        }
        function setInfoReqAfterPostBackForSorting(header, columnIndex, rptName) {
            var hidSortingDtl = document.getElementById('<%=hidSortingDetail.ClientID %>');
            var sortType = $(header).hasClass('sortable_asc') ? SORT_TYPE.DESC : SORT_TYPE.ASC
            $(hidSortingDtl).val(rptName + "," + columnIndex + "," + sortType);
        }
        function clearSortingDtlHiddenField() {
            var hidSortingDtl = document.getElementById('<%=hidSortingDetail.ClientID %>');
            var hidCurrentSort = document.getElementById('<%=hidCurrentSortOrder.ClientID %>');
            $(hidCurrentSort).val($(hidSortingDtl).val());
            $(hidSortingDtl).val("");
        }

        function setValueOfHidGotUsrDtlIfUsrNmChanged() {
            var ddlUserType = $('#' + '<%= ddlUserType.ClientID %>');
            var hidGotUserDetail = $('#' + '<%= hidGotUserDtls.ClientID %>');
            var hidGotDtlsOfUsrNm = $('#' + '<%= hidGotDtlsOfUsrNm.ClientID %>');
            var txtUsername = $('#' + '<%= txtUserName.ClientID %>');
            var userType = $(ddlUserType).val();
            if (userType == USER_TYPE.ActvDirUser) {
                if ($(hidGotDtlsOfUsrNm).val().toLowerCase() !== $(txtUsername).val().toLowerCase()) {
                    $(hidGotUserDetail).val('false');
                }
            }
        }
        function wasGetDtlsCalledIfActvDirUser() {
            var hidDtlsFormMode = $('#' + '<%=hidDetailsFormMode.ClientID %>');
            if ($(hidDtlsFormMode).val().toLowerCase() == 'edit') {
                return true;
            }
            else {
                var ddlUserType = $('#' + '<%= ddlUserType.ClientID %>');
                var hidGotUserDetail = $('#' + '<%= hidGotUserDtls.ClientID %>');
                var userType = $(ddlUserType).val();
                if (userType == USER_TYPE.ActvDirUser) {
                    if ($(hidGotUserDetail).val().toLowerCase() === 'false') {
                        $.wl_Alert('You have to get the details of the user first before saving.', 'info', '#formDiv');
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                else
                    return true;
            }
        }
        /*
        in Edit mode showing User Type and domain
        in labels hence have to change the height of 
        the div.
        */
        function setHeightOfUserTypeAndDomainInEditMode() {
            var hidDtlsFormMode = $('#' + '<%=hidDetailsFormMode.ClientID %>');
            var divUserTypeControls = $('#divUserTypeControls');
            var divDomainControls = $('#divDomainControls');
            if ($(hidDtlsFormMode).val().toLowerCase() == 'edit') {
                if ($(divUserTypeControls)) {
                    $(divUserTypeControls).addClass("containingLabel");
                }
                if ($(divDomainControls)) {
                    $(divDomainControls).addClass("containingLabel");
                }
            }
            else {
                if ($(divUserTypeControls)) {
                    $(divUserTypeControls).removeClass("containingLabel");
                }
                if ($(divDomainControls)) {
                    $(divDomainControls).removeClass("containingLabel");
                }
            }
        }
        function setWidthOfDropDown() {
            var ddlUserType = $('#' + '<%=ddlUserType.ClientID %>');
            var ddlDomainList = $('#' + '<%=ddlDomainList.ClientID %>');

            if (ddlUserType) {
                $(ddlUserType).siblings('span').width(220);
                $(ddlUserType).parent().css('margin-left', '-3px');
            }
            if (ddlDomainList) {
                $(ddlDomainList).siblings('span').width(220);
                $(ddlDomainList).parent().css('margin-left', '-3px');
            }
        }

        function doProcessForFormHelp() {
            $.formHelp(
            { classPrefix: 'myprefix', pushpinEnabled: false });
        }
        function changeHelpTextOnUserDDChange() {
            var ddlUserType = $('#' + '<%= ddlUserType.ClientID %>');
            setUserTypeSelectedForDDChange($(ddlUserType).val());
        }
        function setUserTypeSelectedInHidField(value) {
            var $hidUserTypeSelected = $('#' + '<%=hidUserTypeSelected.ClientID %>');
            if (value) {
                $($hidUserTypeSelected).val(value);
            }
            else {
                $($hidUserTypeSelected).val("0");
            }
        }
        function setUserTypeSelectedForDDChange() {
            var $ddlUserType = $('#' + '<%= ddlUserType.ClientID %>');
            if ($ddlUserType) {
                setUserTypeSelectedInHidField($($ddlUserType).val());
            }
            else {
                setUserTypeSelectedInHidField(0);
            }
        }
        function getHtmlStringFOrHelpByUserType(userType/*int value 0,1*/) {
            var strHtml = "";
            if (userType == 0) {
                strHtml = "<span class=\"myprefix-helptext\" data-for='#<%=txtUserName.ClientID %>'>" +
                    "<ul>" +
                        "<li>Username should start with an alphabet</li>" +
                        "<li>Allowed special characters ! # $ % ^ * ( ) _ -</li>" +
                    "</ul>" +
                "</span>"
            }
            else if (userType == 1) {
                strHtml = " <span class=\"myprefix-helptext\" data-for='#<%=txtUserName.ClientID %>'>" +
                "<ul>" +
                    "<li>Use text, numbers, special Characters except \" / \ [ ] : ; | = , + * ? < ></li>" +
                "</ul>" +
                "</span>"
            }
            return strHtml;
        }
        function addHtmlForUserNameHelpAfterPostBack() {
            var $hidUserTypeSelected = $('#' + '<%=hidUserTypeSelected.ClientID %>');
            var strHtmlToAdd = getHtmlStringFOrHelpByUserType($($hidUserTypeSelected).val());
            var txtUserName = $('#' + '<%= txtUserName.ClientID %>');
            var divHelpText = undefined;
            if (txtUserName) {
                divHelpText = $(('#' + $(txtUserName).attr("id")) + "+ div.myprefix-form-helpbox");
                var spanHelpText = $(('#' + $(txtUserName).attr("id")) + "+ span.myprefix-helptext");
                if (divHelpText) {
                    $(divHelpText).remove();
                }
                if (spanHelpText) {
                    $(spanHelpText).remove();
                }
                $(txtUserName)
                        .parent()
                        .append(strHtmlToAdd);
            }

        }
        function scrollToErrorMsg() {
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }
    </script>
    <script type="text/javascript">
        function ViewInfo(sender) {
            var v = $(sender).parents("tr:first").find('[id*=lbldeviceinfo]');
            var deviceversion = $(sender).parents("tr:first").find('[id*=lbldeviceversion]');
            var devicetype = $(sender).parents("tr:first").find('[id*=lbldevicetype]');

            if ($(v).text() == "No Device Registered Yet") {
                $('[id$=NoDetails]').show();
                $('#Tab').hide();
            }
            else {
                $('[id$=NoDetails]').hide();
                CreateTable($(v).text(), $(deviceversion).text(), $(devicetype).text());
                $('#Tab').show();
            }
            suboverview(true);
            return false;
        }

        function suboverview(_bol) {
            if (_bol) {
                showModalPopUp('SubProcAppInfo', " Device Info", 435, false);
            }
            else {
                $('#SubProcAppInfo').dialog('close');
            }
        }

        function CreateTable(csvversion, csvdevice, csvdeviceType) {
            var dverstion = csvversion.split(',');
            var ddevice = csvdevice.split(',');
            var devicetype = csvdeviceType.split(',');
            var aryVersions = dverstion;
            if (aryVersions.length > 0) {
                var strHtml = "<table><thead class=Dynamicheader><tr><td>Device </td><td>OS Version</td></tr></thead><tbody>";
                for (var i = 0; i <= aryVersions.length - 1; i++) {

                    strHtml += "<tr><td> " + dverstion[i] + "</td> <td>" + devicetype[i] + "(" + ddevice[i] + ")" + " </td> </tr>";
                }
                strHtml += "</tbody></table>";
            }
            $('#Tab').html(strHtml);
        }
        var SetAuntication;
        var SetCredential;
        function CustomProperty() {
            if ($('[id$=hidData]').val() != "") {
                SetAuntication = jQuery.parseJSON($('[id$=hidData]').val());
                $('[id$=hidData]').val("");
            }
            if (SetAuntication === undefined || SetAuntication.length == 0) {
                $('#fidsetCustomUserproperty').hide();
            }
            else {
                $('#fidsetCustomUserproperty').show();
                EnterpriseCreatentiaDetails(SetAuntication);
            }
        }
        function CredentialProperty() {
            if ($('[id$=hidAuthenticationData]').val() != "") {
                SetCredential = jQuery.parseJSON($('[id$=hidAuthenticationData]').val());
                $('[id$=hidAuthenticationData]').val("");
                var tempSetCredential = $.grep(SetCredential, function (e) {
                    return (e.typeval != Authenticationtype.NotFixed && e.typeval != Authenticationtype.FixedCredential);
                });
                SetCredential = tempSetCredential;
            }
            if (SetCredential === undefined || SetCredential.length <= 0) {
                $('#fidsetCredential').hide();
            }
            else {
                $('#fidsetCredential').show();
                Credentials(SetCredential);
            }
        }
        function EnterpriseCreatentiaDetails(SetAuntication) {
            var newsectionno = '';
            $.each(SetAuntication, function (idx, obj) {
                var i = idx + 1;
                if (i % 2 == 1) {
                    newsectionno = 'dynamicustomproperties' + i;
                    $('<section/>', { id: newsectionno, class: 'multipleFormFields' }).appendTo($('#fidsetCustomUserproperty'));

                    DesignFinalHtml(obj.name, obj.tag, obj.typeval, i, newsectionno, obj.validationSetting.selectopt);
                }
                else {
                    DesignFinalHtml(obj.name, obj.tag, obj.typeval, i, newsectionno, obj.validationSetting.selectopt);
                }
            })
        }
        var ControlType = { dropdown: "2", textbox: "1",dropdownUser: "3", dropdownGroup: "4"}
        function DesignFinalHtml(name, tag, ctrltype, cls, sectiono, drop) {
            var $Ctrltype;
            var newSection = '#' + sectiono;
            if (ctrltype == ControlType.textbox) {
                var $Ctrltype = $('<input/>').attr({ type: 'text', name: 'text', id: "cust_" + tag }).addClass("text");
            }
            else {
                var $Ctrltype = $('<select/>').attr({ type: 'dropdown', name: 'dropdown', id: "cust_" + tag, class: "UseUniformCss" });
            }
            var gridClass = 'secondField';
            if (cls % 2 == 1) {
                gridClass = 'withoutLeftBorder';
            }
            else {
            }
            $('<div/>', { id: sectiono + cls, class: 'elementsWrapper g6 ' + gridClass }).appendTo(newSection);
            var newlblid = '#' + sectiono + cls;
            $('<label/>', { id: 'labelprop' + cls }).appendTo(newlblid);
            var vid = '#' + 'labelprop' + cls;
            $(vid).text(name);
            var nid = sectiono + cls;
            var vt = '#' + nid;
            $('<div/>', { id: 'inputCls' + cls, class: 'inputCls' }).appendTo(vt);

            var id = '#' + 'inputCls' + cls;
            $(id).append($Ctrltype);
            if (ctrltype != ControlType.textbox) {
                var Ctrlid = '#' + "cust_" + tag;
                var drOpations;
                if (ctrltype == ControlType.dropdown) drOpations = drop;
                else if (ctrltype == ControlType.dropdownUser) drOpations = JSON.parse($('[id$=hidusers]').val());
                else drOpations = JSON.parse($('[id$=hidgroups]').val());
                $(Ctrlid).append('<option value="">' + 'Select' + '</option>');
                $.each(drOpations, function (index, Option) {
                    $(Ctrlid).append('<option value="' + Option[1] + '">' + Option[0] + '</option>');
                });
            }

            $Ctrltype = '';
        }

        var Authenticationtype = { FixedCredential: 0, Fixed: 1, UserFixed: 3, NotFixed: 2 };
        function Credentials(SetCredential) {
            var newsectionno = '';
            $.each(SetCredential, function (idx, obj) {
                var i = idx + 1;
                if (i % 2 == 1) {
                    newsectionno = 'secauntication' + i;
                    $('<section/>', { id: newsectionno, class: 'multipleFormFields' }).appendTo($('#fidsetCredential'));
                    DesignAuntication(obj.name, obj.tag, obj.typeval, i, newsectionno);
                }
                else {
                    DesignAuntication(obj.name, obj.tag, obj.typeval, i, newsectionno);
                }
            })
        }
        function DesignAuntication(name, tag, typeval, i, sectionno) {
            var $Ctrltype;
            var newSection = '#' + sectionno;
            var $Ctrltype = $('<input/>').attr({ type: 'text', name: 'text', id: "cust_a" + tag, title: "Username", placeholder: "User Name" }).addClass("text");
            if (typeval == Authenticationtype.Fixed)
                var $Ctrltype1 = $('<input/>').attr({ type: 'password', name: 'text', id: "cust_p" + tag, title: "Password", placeholder: "Password" }).addClass("text");
            var id = '#' + 'inputAuCls' + i;

            if (i % 2 == 1) {
                $('<div/>', { id: sectionno + i,
                    class: 'elementsWrapper g6 withoutLeftBorder'
                }).appendTo(newSection);
                var newlblid = '#' + sectionno + i;
                $('<label/>', { id: 'labelAu' + i }).appendTo(newlblid);
                var labelid = '#' + 'labelAu' + i;
                $(labelid).text(name);
                var newdiv = '#' + sectionno + i;
                $('<div/>', { id: 'inputAuCls' + i, class: 'inputCls' }).appendTo(newdiv);
            }
            else {
                var nid = sectionno + i;
                $('<div/>', { id: nid, class: 'elementsWrapper g6 secondField' }).appendTo(newSection);
                var newlblid = '#' + sectionno + i;
                $('<label/>', { id: 'labelAu' + i }).appendTo(newlblid);
                var labelid = '#' + 'labelAu' + i;
                $(labelid).text(name);
                var newdiv = '#' + sectionno + i;
                $('<div/>', { id: 'inputAuCls' + i, class: 'inputCls' }).appendTo(newdiv);
                var id = '#' + 'inputAuCls' + i;
            }
            if (typeval == Authenticationtype.Fixed) {
                $(id).append($Ctrltype);
                $(id).append($Ctrltype1);
            }
            else {
                $(id).append($Ctrltype);
            }
        }

        function CallUnformCss(_id) {
            if ($(_id).length == 0) {
                return;
            }
            if ($(_id)[0].className == "UseUniformCss") {
                $(_id).uniform();
                $(_id).removeClass("UseUniformCss");
                $(_id).addClass("UsedUniformCss");
            }
            if ($(_id).width() != 0) {
                if ($(_id).width() < 300) {
                    $(_id).width($(_id).width() + 10);
                }
                $($(_id)[0].parentElement).width('auto');
            }
            else {
            }
        }
        var TextType = { Alphabets: 1, Alphanumeric: 2, Alphanumeric_Special_Characters: 3, Integer: 4, Decimal: 5 }

        function Custom() {
            var strError = "";
            var matches = [];
            if (SetAuntication != undefined) {
                $.each(SetAuntication, function (idx, obj) {
                    var ctagname = $('#cust_' + obj.tag).get(0).tagName;
                    var jsobj = {};
                    jsobj.name = obj.tag;
                    jsobj.value = $('#cust_' + obj.tag).val();
                    strError += ValidateProperty(obj, jsobj.value);
                    matches.push(jsobj);
                });
            }
            if (strError.length <= 0) {
                $('[id$=hidData]').val(JSON.stringify(matches));
            }
            else {
                $('[id$=hidData]').val("Error");
            }
            $('#divcustom').html('');
            if (strError.length > 0) {
                $.wl_Alert('<ul style="position:relative;right:-3px;">' + strError + '</ul>', 'info', '#divcustom');
            }
            Credential();
        }
        function CustomOrCredentialControlClear() {
            var strError = "";
            var matches = [];
            if (SetAuntication != undefined) {
                $.each(SetAuntication, function (idx, objAp) {
                    if (objAp.typeval == ControlType.textbox)
                        $('#cust_' + objAp.tag).val('');
                    else
                        $('#cust_' + objAp.tag).val('');
                });
            }
            if (SetCredential != undefined) {
                $.each(SetCredential, function (idx, objCr) {
                    if (objCr.typeval != Authenticationtype.NotFixed && objCr.typeval != Authenticationtype.FixedCredential) {
                        $('#cust_a' + objCr.tag).val('');
                        if (objCr.typeval == Authenticationtype.Fixed)
                            $('#cust_p' + objCr.tag).val('');
                    }
                });
            }
        }

        function CustomOrCredentialControlOnEdit(objCustomProp, objCredentialProp) {
            var strError = "";
            var matches = [];
            if (SetAuntication != undefined) {
                $.each(SetAuntication, function (idx, objAp) {
                    var value = '';
                    $.each(objCustomProp, function (index, objprp) {
                        if (objAp.tag == objprp.name) {
                            value = objprp.value;
                        }
                    });
                    if (objAp.typeval == ControlType.textbox)
                        $('#cust_' + objAp.tag).val(value);
                    else {
                        //$('#cust_' + objAp.tag).val(value);
                        $("#cust_" + objAp.tag + " > option[value=" + value + "]").attr("selected", true);
                        var sel = document.getElementById('cust_' + objAp.tag);
                        var option = sel.options[sel.selectedIndex];
                        $('#cust_' + objAp.tag).siblings("span").text(option.text);
                    }
                });
            }
            if (SetCredential != undefined) {
                $.each(SetCredential, function (idx, objCr) {
                    var unm = '', pwd = '';
                    $.each(objCredentialProp, function (index, objucr) {
                        if (objCr.tag == objucr.id) {
                            unm = objucr.unm;
                            pwd = objucr.pwd;
                        }
                    });
                    if (objCr.typeval != Authenticationtype.NotFixed && objCr.typeval != Authenticationtype.FixedCredential) {
                        $('#cust_a' + objCr.tag).val(unm);
                        if (objCr.typeval == Authenticationtype.Fixed)
                            $('#cust_p' + objCr.tag).val(pwd);
                    }
                });
            }
        }

        function ValidateProperty(PropTypeJson, Value) {
            var strLITag = "<li>", strLIEndTag = "</li>";
            if (PropTypeJson.validationSetting.req == 1 && Value.trim().length == 0)
                return strLITag + "Please enter " + PropTypeJson.name + "." + strLIEndTag;
            else if (PropTypeJson.typeval == ControlType.textbox && IsValidString(PropTypeJson.validationSetting.vtype, PropTypeJson.validationSetting.allowSpace, PropTypeJson.validationSetting.allowNumStart, PropTypeJson.validationSetting.specialchar, Value)) {

                return strLITag + "Please enter valid" + ' ' + PropTypeJson.name + "." + strLIEndTag;
            }
            return "";
        }
        function IsValidString(vtype, isSpace, startNumb, _SpecialChars, Value) {
            var patt = "";
            if (vtype == "0") return false;
            if (vtype == "1" || vtype == "2" || vtype == "3") {
                patt += "a-zA-Z";
            }
            if (vtype == "2" || vtype == "3" || vtype == "4" || vtype == "5") {
                patt += "0-9";
            }
            if (vtype == "5") {
                _SpecialChars = ".";
            }
            if (_SpecialChars != '') {
                patt += _SpecialChars;
            }
            patt = "[" + patt + "]" + "{1,25}$";
            if (!startNumb) {
                patt = "[a-zA-Z]" + patt;
            }
            if (!Value.match("^" + patt)) return true;
            else return false;
        }

        function Credential() {
            var strError = "";
            var strLITag = "<li>", strLIEndTag = "</li>";
            var Cmatches = [];
            if (SetCredential != undefined) {
                $.each(SetCredential, function (idx, obj) {
                    if (obj.typeval != Authenticationtype.NotFixed && obj.typeval != Authenticationtype.FixedCredential) {
                        var jsobj = {};
                        jsobj.id = obj.tag;
                        jsobj.unm = $('#cust_a' + obj.tag).val();
                        if (jsobj.unm.trim().length == 0)
                            strError += strLITag + "Please enter user name of " + obj.name + "." + strLIEndTag;
                        //username Check empty
                        if (obj.typeval == Authenticationtype.Fixed) {
                            jsobj.pwd = $('#cust_p' + obj.tag).val();
                            if (jsobj.pwd.trim().length == 0)
                                strError += strLITag + "Please enter password of " + obj.name + "." + strLIEndTag;
                            //PasswordCheck empty
                        }
                        else
                            jsobj.pwd = "";
                        Cmatches.push(jsobj);
                    }
                });
            }
            if (strError.length <= 0) {
                $('[id$=hidAuthenticationData]').val(JSON.stringify(Cmatches));
            }
            else {
                $('[id$=hidAuthenticationData]').val("Error");
            }
            $('#divCred').html('');
            if (strError.length > 0) {
                $.wl_Alert('<ul style="position:relative;right:-3px;">' + strError + '</ul>', 'info', '#divCred');
            };
        }


        //        function cleartextValue() {
        //            $('#dynamicustomproperties :input').val('');
        //            $('#divCred :input').val('');
        //        }
    </script>
    <style type="text/css">
        fieldset section div.containingLabel span
        {
            float: left;
        }
        fieldset section div.containingLabel span.i_pencil
        {
            float: left;
            margin-left: 20px;
            position: relative;
            top: -4px;
        }
        div.alert.info ul
        {
            margin-bottom: 0px;
        }
        #ui-datepicker-div
        {
            width: 20.5em;
        }
        .DbCmdRow
        {
            width: 100%;
            float: left;
        }
        .DbConnLeftDiv
        {
            float: left;
            padding-top: 5px;
            width: 30%;
            margin-top: 5px;
            cursor: default;
        }
        .DbConnLeftDiv1
        {
            float: left;
            padding-top: 5px;
            width: 45%;
            margin-top: 5px;
            cursor: default;
        }
        .Dynamicheader
        {
            font-size: 13px;
            font-family: "Tahoma" ,sans-serif;
            font-weight: 700;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <div class="widget">
                    <div class="customWidgetDirectDiv">
                        <asp:UpdatePanel ID="updRepeater" runat="server">
                            <ContentTemplate>
                                <section>
                                    <div id="divInformation">
                                    </div>
                                </section>
                                <asp:Panel ID="pnlSearchRow" runat="server" CssClass="searchRow g12" Style="margin: 5px 5px 5px 3px;">
                                    <div class="g5 radioButtonList">
                                        <asp:CheckBox ID="chkShowBlockedUser" Text="Show blocked users" runat="server" OnCheckedChanged="chkShowBlockedUser_CheckedChanged"
                                            AutoPostBack="true" onclick="setHiddenPostBackPanel(Panels.Repeater)" />
                                    </div>
                                    <div class="g5" style="float: right; padding: 0px; margin: 0px;">
                                        <div style="float: right; width: 70%;" class="searchImageCont">
                                            <asp:LinkButton ID="imgSearch" ToolTip="search" Style="margin: 5px 4px 0px 4px;"
                                                CssClass="searchLink16by16 fr" OnClientClick="isCookieCleanUpRequired('false');"
                                                OnClick="imgSearch_Click" runat="server"></asp:LinkButton>
                                            <asp:TextBox ID="txtFilter" runat="server" Width="80%" Style="float: right"></asp:TextBox>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="clear">
                                </div>
                                <section style="margin: 5px 5px 5px 3px;">
                                    <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                        <section>
                                            <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                                <div>
                                                    <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>Mobile User Details</h1>">
                                                    </asp:Label>
                                                </div>
                                                <div style="float: left; margin-left: 6px; position: relative; top: 14px;">
                                                    <asp:LinkButton ID="lnkShowCompleteList" runat="server" OnClientClick="isCookieCleanUpRequired('false');"
                                                        Text="( view all )" OnClick="lnkShowCompleteList_Click" Visible="false"></asp:LinkButton>
                                                </div>
                                                <div style="position: relative; top: 10px; right: 15px;">
                                                    <asp:LinkButton ID="lnkAddNewMobileUser" runat="server" Text="add user" CssClass="repeaterLink fr"
                                                        OnClick="lnkAddNewMobileUser_Click" OnClientClick="setUserTypeSelectedInHidField(0);isCookieCleanUpRequired('false');CustomOrCredentialControlClear();"></asp:LinkButton>
                                                </div>
                                            </asp:Panel>
                                        </section>
                                        <asp:Repeater ID="rptUserDetails" runat="server" OnItemDataBound="rptUserDetails_ItemDataBound">
                                            <HeaderTemplate>
                                                <table class="repeaterTable rowClickable rptWithoutImageAndButton" id="tblUserDtl">
                                                    <thead class="sortable">
                                                        <tr>
                                                            <th style="display: none" class="sortable">
                                                                USER ID
                                                            </th>
                                                            <th onclick="setInfoReqAfterPostBackForSorting(this,USER_DETAIL_REPEATER_COLUMNS.NAME,REPEATER_NAME.UserDetail);rptHeaderClickForSorting(this);">
                                                                Name ( username )
                                                            </th>
                                                            <th onclick="setInfoReqAfterPostBackForSorting(this,USER_DETAIL_REPEATER_COLUMNS.EMAIL,REPEATER_NAME.UserDetail);rptHeaderClickForSorting(this);">
                                                                Email
                                                            </th>
                                                            <th onclick="setInfoReqAfterPostBackForSorting(this,USER_DETAIL_REPEATER_COLUMNS.LOGIN,REPEATER_NAME.UserDetail);rptHeaderClickForSorting(this);">
                                                                Last Logon
                                                            </th>
                                                            <th style="width: 30px;" class="notClickable">
                                                            </th>
                                                        </tr>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tbody>
                                                    <tr class="repeaterItem">
                                                        <td style="display: none" title="id">
                                                            <asp:Label ID="lblUserId" runat="server" Text='<%#Eval("USER_ID") %>'></asp:Label>
                                                            <asp:Label ID="lblIsBlocked" runat="server"></asp:Label>
                                                            <asp:Label ID="lblLoginName" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                                            <asp:Label ID="lbldeviceinfo" runat="server" Text='<%#Eval("DEVICE") %>'></asp:Label>
                                                            <asp:Label ID="lbldeviceversion" runat="server" Text='<%#Eval("DeviceVersion") %>'></asp:Label>
                                                            <asp:Label ID="lbldevicetype" runat="server" Text='<%#Eval("DeviceType") %>'></asp:Label>
                                                        </td>
                                                        <td onclick="rptTableCellClick(this,Repeater.ADD_USER_USER_DETAIL);">
                                                            <asp:Label ID="lblNameAndUserName" runat="server"></asp:Label>
                                                        </td>
                                                        <td onclick="rptTableCellClick(this,Repeater.ADD_USER_USER_DETAIL);">
                                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("EMAIL_ID") %>'></asp:Label>
                                                        </td>
                                                         <td onclick="rptTableCellClick(this,Repeater.ADD_USER_USER_DETAIL);">
                                                            <asp:Label ID="lblLogon" runat="server" Text=''></asp:Label>
                                                        </td>
                                                        <td>
                                                            <a id="ainfo" onclick="ViewInfo(this);"><span>
                                                                <img src="css/images/DeviceInfo.png" alt="" /></span></a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <tbody>
                                                    <tr class="repeaterAlternatingItem">
                                                        <td style="display: none" title="id">
                                                            <asp:Label ID="lblUserId" runat="server" Text='<%#Eval("USER_ID") %>'></asp:Label>
                                                            <asp:Label ID="lblIsBlocked" runat="server"></asp:Label>
                                                            <asp:Label ID="lblLoginName" runat="server" Text='<%# Eval("USER_NAME") %>'></asp:Label>
                                                            <asp:Label ID="lbldeviceinfo" runat="server" Text='<%#Eval("DEVICE") %>'></asp:Label>
                                                            <asp:Label ID="lbldeviceversion" runat="server" Text='<%#Eval("DeviceVersion") %>'></asp:Label>
                                                            <asp:Label ID="lbldevicetype" runat="server" Text='<%#Eval("DeviceType") %>'></asp:Label>
                                                        </td>
                                                        <td onclick="rptTableCellClick(this,Repeater.ADD_USER_USER_DETAIL);">
                                                            <asp:Label ID="lblNameAndUserName" runat="server"></asp:Label>
                                                        </td>
                                                        <td onclick="rptTableCellClick(this,Repeater.ADD_USER_USER_DETAIL);">
                                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("EMAIL_ID") %>'></asp:Label>
                                                        </td>
                                                         <td onclick="rptTableCellClick(this,Repeater.ADD_USER_USER_DETAIL);">
                                                            <asp:Label ID="lblLogon" runat="server" Text=''></asp:Label>
                                                        </td>
                                                        <td>
                                                            <a id="a1" onclick="ViewInfo(this);"><span>
                                                                <img src="css/images/DeviceInfo.png" alt="" /></span></a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </AlternatingItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <div>
                                        </div>
                                        <div style="clear: both">
                                            <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                            <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                                        </div>
                                    </asp:Panel>
                                </section>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <section style="margin: 5px 5px 5px 5px;">
                            <asp:Panel ID="pnlMobileUserForm" Style="display: none;" runat="server">
                                <div>
                                    <div id="formDiv">
                                        <asp:UpdatePanel ID="updBasicpnl" runat="server">
                                            <ContentTemplate>
                                                <fieldset id="fldsetBasicInfo">
                                                    <label>
                                                        Basic Information &nbsp;&nbsp;<asp:Label ID="lblHeaderUserName" runat="server" Visible="false"></asp:Label>
                                                        <asp:LinkButton ID="lnkDelete" runat="server" Text="delete" OnClick="lnkDelete_Click"
                                                            CssClass="FormHeaderActionLinks" OnClientClick="return showConfirmation('Are you sure you want to delete this user');"></asp:LinkButton>&nbsp;&nbsp;
                                                        <asp:Label ID="lblSeparatorOne" runat="server" Text="&nbsp;&nbsp; | &nbsp;&nbsp;"
                                                            CssClass="FormHeaderActionLinksSeparator"></asp:Label>&nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkBlockUnblock" runat="server" Text="block" OnClick="lnkBlockUnblock_Click"
                                                            CssClass="FormHeaderActionLinks" OnClientClick="return confirmBlockUnblock(this);"></asp:LinkButton>&nbsp;&nbsp;
                                                        <asp:Label ID="lblSeparatorTwo" runat="server" Text="&nbsp;&nbsp; | &nbsp;&nbsp;"
                                                            CssClass="FormHeaderActionLinksSeparator"></asp:Label>
                                                        <asp:LinkButton ID="lnkResetPassword" runat="server" Text="reset password" OnClick="lnkResetPassword_Click"
                                                            CssClass="FormHeaderActionLinks" OnClientClick="return showConfirmation('Are you sure you want to reset password of the user.');"></asp:LinkButton>
                                                    </label>
                                                    <div id="divBasicInfoError" style="border: none">
                                                    </div>
                                                    <section>
                                                        <label for='<%=ddlUserType.ClientID %>'>
                                                            Authentication</label>
                                                        <div id="divUserTypeControls">
                                                            <asp:Label ID="lblEditUserType" Style="margin-left: 3px;" runat="server" Text=""></asp:Label>
                                                            <asp:DropDownList runat="server" ID="ddlUserType" onchange="setHiddenPostBackPanel(Panels.Form);changeHelpTextOnUserDDChange();"
                                                                AutoPostBack="true" Style="margin-left: -4px;" OnSelectedIndexChanged="ddlUserType_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </section>
                                                    <asp:Panel ID="pnlDomain" runat="server" Visible="false">
                                                        <section>
                                                            <label for='<%=ddlDomainList.ClientID %>'>
                                                                Domain (mPlugin)</label>
                                                            <div id="divDomainControls">
                                                                <asp:Label ID="lblEditDomain" runat="server" Text=""></asp:Label>
                                                                <asp:DropDownList ID="ddlDomainList" Style="margin-left: -3px;" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </section>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlUserName" runat="server">
                                                        <section>
                                                            <label for='<%=txtUserName.ClientID %>'>
                                                                Username</label>
                                                            <div>
                                                                <asp:TextBox ID="txtUserName" runat="server" CssClass="required" Width="38%" onblur="setValueOfHidGotUsrDtlIfUsrNmChanged();"
                                                                    MaxLength="50"></asp:TextBox>
                                                                <span class="myprefix-helptext" data-for='#<%=txtUserName.ClientID %>'>
                                                                    <ul>
                                                                        <li>You can use lettes and numbers</li>
                                                                        <li>Username should start with a letter</li>
                                                                        <li>Allowed special characters ! # $ % ^ * ( ) _ -</li>
                                                                    </ul>
                                                                </span>
                                                                <asp:LinkButton ID="lnkActvDirUserDetail" runat="server" Text="( get details )" OnClick="lnkActvDirUserDetail_Click"
                                                                    Style="margin-left: 5px;" OnClientClick="setHiddenPostBackPanel(Panels.Form);isCookieCleanUpRequired('false');"></asp:LinkButton>
                                                            </div>
                                                        </section>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlPasswordContainer" CssClass="multipleFormField" runat="server">
                                                        <asp:Panel ID="pnlPassword" CssClass="elementsWrapper g6 withoutLeftBorder" runat="server">
                                                            <label for='<%=txtPassword.ClientID %>'>
                                                                Password</label>
                                                            <div>
                                                                <asp:TextBox ID="txtPassword" runat="server" CssClass="required" TextMode="Password"
                                                                    onblur="rememberPasswordEntered();" MaxLength="50"></asp:TextBox>
                                                                <span class="myprefix-helptext" data-for='#<%=txtPassword.ClientID %>'>
                                                                    <ul>
                                                                        <li>Use atleast 6 characters</li>
                                                                        <li>Allowed special characters ! @ # $ % ^ * ( ) _ -</li>
                                                                    </ul>
                                                                </span>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlRetypePassword" CssClass="elementsWrapper g6 secondField" runat="server">
                                                            <label for='<%=txtRetypePassword.ClientID %>'>
                                                                Retype</label>
                                                            <div>
                                                                <asp:TextBox ID="txtRetypePassword" runat="server" CssClass="required " TextMode="Password"
                                                                    onblur="rememberPasswordEntered();" MaxLength="50"></asp:TextBox>
                                                            </div>
                                                        </asp:Panel>
                                                    </asp:Panel>
                                                    <section class="multipleFormFields">
                                                        <div class="elementsWrapper g6 withoutLeftBorder">
                                                            <label for='<%=txtFirstName.ClientID %>'>
                                                                First Name</label>
                                                            <div class="inputCls">
                                                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="required " MaxLength="50"></asp:TextBox>
                                                                <span class="myprefix-helptext" data-for='#<%=txtFirstName.ClientID %>'>
                                                                    <ul>
                                                                        <li>You can use letters</li>
                                                                        <li>Allowed special characters - space</li>
                                                                    </ul>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="elementsWrapper g6 secondField">
                                                            <label for='<%=txtLastName.ClientID %>'>
                                                                Last Name</label>
                                                            <div>
                                                                <asp:TextBox ID="txtLastName" runat="server" CssClass="required " MaxLength="50"></asp:TextBox>
                                                                <span class="myprefix-helptext" data-for='#<%=txtLastName.ClientID %>'>
                                                                    <ul>
                                                                        <li>You can use letters</li>
                                                                        <li>Allowed special characters - space</li>
                                                                    </ul>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <section class="multipleFormFields">
                                                        <div class="elementsWrapper g6 withoutLeftBorder">
                                                            <label for='<%=txtEmailId.ClientID %>'>
                                                                Email</label>
                                                            <div class="inputCls">
                                                                <asp:TextBox ID="txtEmailId" runat="server" CssClass="required" MaxLength="50"></asp:TextBox>
                                                                <span class="myprefix-helptext" data-for='#<%=txtEmailId.ClientID %>'>eg. abc@xyz.com</span>
                                                            </div>
                                                        </div>
                                                        <div class="elementsWrapper g6 secondField" style="height: auto;">
                                                            <label for='<%=txtMobile.ClientID %>'>
                                                                Mobile</label>
                                                            <div style="height: auto;">
                                                                <asp:TextBox ID="txtMobile" runat="server" CssClass="required" MaxLength="20"></asp:TextBox>
                                                                <span class="myprefix-helptext" data-for='#<%=txtMobile.ClientID %>'>with country code
                                                                    eg. +919123456789 </span>
                                                            </div>
                                                        </div>
                                                    </section>
                                                    <section>
                                                        <label for='lnkUserShowGroupList'>
                                                            User Groups</label>
                                                        <div class="containingLabel">
                                                            <asp:Label ID="lblUserGroupList" runat="server" Text="No Group selected yet"></asp:Label>
                                                            <asp:Label ID="lblUserGroupIds" runat="server" Visible="false"></asp:Label>
                                                            <span class="fl i_pencil" id="lnkUserShowGroupList" title="edit" onclick="showModalPopUpWithOutHeader('divUsrGroupModalPopUp','User Groups','350',false);return false;">
                                                            </span>
                                                        </div>
                                                    </section>
                                                </fieldset>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <%--<asp:UpdatePanel ID="updOtherpnl" runat="server">
                                            <ContentTemplate>
                                                <fieldset id="fldsetOtherDet">
                                                    <label>
                                                        Other Details</label>
                                                    <div id="divOtherDtlError" style="border: none">
                                                    </div>
                                                    <section>
                                                        <label for='lnkUserShowGroupList'>
                                                            User Groups</label>
                                                        <div class="containingLabel">
                                                            <asp:Label ID="lblUserGroupList" runat="server" Text="No Group selected yet"></asp:Label>
                                                            <asp:Label ID="lblUserGroupIds" runat="server" Visible="false"></asp:Label>
                                                            <span class="fl i_pencil" id="lnkUserShowGroupList" title="edit" onclick="showModalPopUpWithOutHeader('divUsrGroupModalPopUp','User Groups','350',false);return false;">
                                                            </span>
                                                        </div>
                                                    </section>
                                                </fieldset>                                                
                                            </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                                        <fieldset id="fidsetCustomUserproperty">
                                            <label>
                                                Custom User Properties</label><div id="divcustom">
                                                </div>
                                            <div id="divCustompropties" style="border: none">
                                            </div>
                                        </fieldset>
                                        <fieldset id="fidsetCredential">
                                            <label>
                                                Credential</label><div id="divCred">
                                                </div>
                                            <div id="div2" style="border: none">
                                            </div>
                                        </fieldset>
                                        <asp:UpdatePanel ID="updSave" runat="server">
                                            <ContentTemplate>
                                                <fieldset id="fldsetMessenger">
                                                     <section>
                                                        <div id="buttons" style="width: 100%; border: none; padding-left: 0px; padding-right: 0px;
                                                            text-align: center;">
                                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="aspButton" OnClientClick="Custom();return wasGetDtlsCalledIfActvDirUser();"
                                                                OnClick="btnSave_Click" />
                                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="aspButton" OnClientClick="hideShowPanelForCancel(0);setUserTypeSelectedInHidField(0);return false;" />
                                                            <asp:HiddenField ID="hdi" runat="server" />
                                                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                                                            <asp:HiddenField ID="hidReTypePasswordEntered" runat="server" />
                                                            <asp:HiddenField ID="hidSortingDetail" runat="server" />
                                                            <asp:HiddenField ID="hidCurrentSortOrder" runat="server" />
                                                            <asp:HiddenField ID="hidGotUserDtls" runat="server" Value="false" />
                                                            <asp:HiddenField ID="hidGotDtlsOfUsrNm" runat="server" Value="" />
                                                            <asp:HiddenField ID="hidUserTypeSelected" runat="server" Value="" />
                                                        </div>
                                                    </section>
                                                </fieldset>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:HiddenField ID="hidAuthenticationData" runat="server" />                                        
                                        <asp:HiddenField ID="hidusers" runat="server" /> 
                                        <asp:HiddenField ID="hidgroups" runat="server" />
                                        <asp:HiddenField ID="hidData" runat="server" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </section>
                    </div>
                </div>
                <asp:HiddenField ID="hidPostBackPanel" runat="server" />
            </div>
        </section>
        <div>
            <asp:HiddenField ID="hidRowValuesUsedAfterPostBack" runat="server" />
        </div>
        <div id="divUserDtlsModal" style="display: none;">
            <asp:UpdatePanel runat="server" ID="upUserDtlsModal" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="ProcImgUpd">
                        <div id="divUserDetailUC">
                            <userDetails:Details ID="ucUserDetails" runat="server" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
      <%--  <section>
            <div id="divGroupModalPopUp" style="display: none;">
                <asp:UpdatePanel ID="updGroupModal" runat="server">
                    <ContentTemplate>
                        <div id="divGroupList" style="display: block; height: 250px; overflow: scroll;">
                            <div id="modalGroupContent" class="modalPopUp">
                                <fieldset>
                                    <section>
                                        <div>
                                            <asp:CheckBoxList ID="chkLstGroups" runat="server" RepeatColumns="1">
                                            </asp:CheckBoxList>
                                        </div>
                                    </section>
                                    <div style="height: 5px;">
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="modalPopUpAction center" style="text-align: center">
                            <asp:Button ID="btnGroupsOk" runat="server" Text="Ok" CssClass="aspButton" OnClientClick="showSelectedGroups();closeModalPopUp('divGroupModalPopUp');return false;" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
       --%>
        <section>
            <div id="divUsrGroupModalPopUp" style="display: none;">
                <asp:UpdatePanel ID="updUsrGroupModal" runat="server">
                    <ContentTemplate>
                        <div id="divUsrGroupList" style="display: block; height: 250px; overflow: scroll;">
                            <div id="modalUsrGroupContent" class="modalPopUp">
                                <fieldset>
                                    <section>
                                        <div>
                                            <asp:CheckBoxList ID="chkLstUserGroups" runat="server" RepeatColumns="1">
                                            </asp:CheckBoxList>
                                        </div>
                                    </section>
                                    <div style="height: 5px;">
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="modalPopUpAction center" style="text-align: center">
                            <asp:Button ID="btnUsrGroupsOk" runat="server" Text="Ok" CssClass="aspButton" OnClientClick="showSelectedUserGroups();closeModalPopUp('divUsrGroupModalPopUp');return false;" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <div>
        <asp:UpdatePanel runat="server" ID="updRowPostBack" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRowClickPostBack" EventName="Click" />
            </Triggers>
            <ContentTemplate>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div>
        <asp:Button ID="btnRowClickPostBack" runat="server" OnClick="btnRowClickPostBack_Click"
            Width="1px" Height="1px" Style="display: none;" />
    </div>
    <div id="SubProcAppInfo" style="display: none">
        <asp:UpdatePanel runat="server" ID="updDeviceInfo" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="div1">
                    <div id="Overview">
                        <div style="margin-top: 5px;">
                            <div id="NoDetails" style="display: none">
                                <asp:Label ID="lblmsg" runat="server" Text="No device  Registered Yet"></asp:Label>
                            </div>
                            <div id="Tab">
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="hidoverview" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest(sender, args) {
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            // makeDatePickerWithMonthYear();
            hideWaitModal();
            var hidField = $('#' + '<%=hidPostBackPanel.ClientID %>');
            if (hidField || $(hidField).val() !== "") {
                hideShowPanelForCancel(parseInt($(hidField).val()));
                $(hidField).val("");
            }
            changeImageOfSortedCol();
            isCookieCleanUpRequired('true');
            addHtmlForUserNameHelpAfterPostBack();
            doProcessForFormHelp();
            //scrollToErrorMsg();
        }
    </script>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true" ClientIDMode="AutoID"
    CodeBehind="~/AppManager.aspx.cs" Inherits="mFicientCP.AppManager1" Title="mFicient | Manage Apps" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <link href="css/Appmanager.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
    .hides
    {
        display:none;
    }
    .bottom
    {
      text-align:center;
    }
    </style>

    <link href="css/Appmanager.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="Scripts/Appmanager.js"></script>
      <script type="text/javascript">
          var categoryOldAppList = [];
          //this is used for save button for final saving of the users
          function getExistingUsersForSaving() {
              var divExistingUsers = $('#' + '<%=divExistingUsers.ClientID %>');
              var hidExistingUsers = document.getElementById('<%=hidExistingUsers.ClientID %>');
              hidExistingUsers.value = "";
              var NewCategory = ""; RemovedCategory = "";
              $(divExistingUsers).each(function (index) {
                  $(this).children().each(function (index) {
                      var anchorTag = $(this).children('a');
                      var wfId = $(this).children('[id^="@WF_ID"]').text();
                      var wfNm = $(this).children('[id^="@WF_NAME"]').text();
                      var catId = $(this).children('[id^="@CATEGORY_ID"]').text();
                      if (!(anchorTag.length === 0)) {
                          var aryAnchorTagValues = anchorTag.attr("id").split('_');
                          if (hidExistingUsers.value === "") {
                              hidExistingUsers.value = wfId + "," + wfNm + "," + catId;
                          }
                          else {
                              hidExistingUsers.value += "|" + wfId + "," + wfNm + "," + catId;
                          }
                          var Exist = false;
                          $(categoryOldAppList).each(function (catIndx) {
                              if (wfNm == this) {
                                  categoryOldAppList.splice(catIndx, 1);
                                  Exist = true;
                              }
                          });
                          if (!Exist) NewCategory = NewCategory + ',' + wfNm;
                      }
                  });
              });
              $(categoryOldAppList).each(function (catIndx) { RemovedCategory = RemovedCategory + ',' + this; });
              var FinalVal = { "add": NewCategory, "remove": RemovedCategory };
              var hidOtherExistingUsers = document.getElementById('<%=hidOtherAvailableUsers.ClientID %>');
              hidOtherExistingUsers.value = JSON.stringify(FinalVal);
          }
          //this is used for save button for final saving of the users
          //          function getOtherAvailableUsersForSaving() {
          //              var divOtherAvailableUsers = $('#' + '<%= divOtherAvailableUsers.ClientID%>');
          //              var hidOtherExistingUsers = document.getElementById('<%=hidOtherAvailableUsers.ClientID %>');
          //              hidOtherExistingUsers.value = "";
          //              $(divOtherAvailableUsers).each(function (index) {
          //                  $(this).children().each(function (index) {
          //                      var anchorTag = $(this).children('a');
          //                      var wfId = $(this).children('[id^="@WF_ID"]').text();
          //                      var wfNm = $(this).children('[id^="@WF_NAME"]').text();
          //                      var catId = $(this).children('[id^="@CATEGORY_ID"]').text();
          //                      if (!(anchorTag.length === 0)) {
          //                          var aryAnchorTagValues = anchorTag.attr("id").split('_');
          //                          if (hidOtherExistingUsers) {
          //                              if (hidOtherExistingUsers.value === "") {

          //                                  hidOtherExistingUsers.value = wfId + "," + wfNm + "," + catId;
          //                              }
          //                              else {
          //                                  hidOtherExistingUsers.value += "|" + wfId + "," + wfNm + "," + catId;
          //                              }
          //                          }
          //                      }
          //                  });
          //              });
          //          }
          //this is used in the a tag of the div 
          function moveUserToOtherAvailableUser(anchorTag) {
              var divOtherAvailableUsers = $('#' + '<%= divOtherAvailableUsers.ClientID%>');
              var divToTransfer = anchorTag.parentElement;
              var divClearSibling = undefined;
              if (divToTransfer.nextElementSibling)
                  divClearSibling = divToTransfer.nextElementSibling;
              $(anchorTag).attr("onclick", "moveUserToExistingUser(this)");
              $(anchorTag).attr("class", "add");
              $(divOtherAvailableUsers).append(divToTransfer);

          }
          //this is used in the a tag of the div
          function moveUserToExistingUser(anchorTag) {
              var divExistingUsers = $('#' + '<%=divExistingUsers.ClientID %>');
              var divToTransfer = anchorTag.parentElement;
              var divClearSibling = undefined;
              if (divToTransfer.nextElementSibling)
                  divClearSibling = divToTransfer.nextElementSibling;
              $(anchorTag).attr("onclick", "moveUserToOtherAvailableUser(this)");
              $(anchorTag).attr("class", "remove");
              $(divExistingUsers).append(divToTransfer);
          }
          function clearGroupNameText() {
              $('#' + '<%=txtcatagoryname.ClientID %>').val("");
          }
          function addToGroup() {
              var divExistingUsers = $('#' + '<%=divExistingUsers.ClientID %>');

              var divOtherAvailableUsers = $('#' + '<%= divOtherAvailableUsers.ClientID%>');

              $(divOtherAvailableUsers).each(function (index) {
                  $(this).children().each(function (index) {
                      var chkbox = $(this).find("input:checkbox");
                      if (chkbox.is(':checked')) {
                          var divToTransfer = this;
                          var divClearSibling = this.nextElementSibling;
                          chkbox.removeAttr('checked');
                          var chkBoxParent = $(chkbox).parent();
                          if ($(chkBoxParent).is("span")) {
                              chkBoxParent.removeAttr('class');
                          }
                          var anchorTag = $(this).find('a');
                          if (anchorTag) {
                              $(anchorTag).attr("onclick", "moveUserToOtherAvailableUser(this)");
                              $(anchorTag).attr("class", "remove");
                          }
                          $(divExistingUsers).append(divToTransfer);
                          $(divExistingUsers).append(divClearSibling);
                      }
                  });
              });
          }

    </script>
    <script type="text/javascript">
        function moveappliedToOtherAvailableapp(anchorTag) {
            var divOtherAvailableUsers = $('#' + '<%= divassigned.ClientID%>');
            var divToTransfer = anchorTag.parentElement;
            var divClearSibling = undefined;
            if (divToTransfer.nextElementSibling)
                divClearSibling = divToTransfer.nextElementSibling;
            $(anchorTag).attr("onclick", "moveAvilableAppToExistingApps(this)");
            $(anchorTag).attr("class", "add");
            var chkbox = $(divToTransfer).find('input:checkbox');
            var chkBoxParent = $(chkbox).parent();
            if ($(chkBoxParent).is("span")) {
                chkBoxParent.removeAttr('class');
            }
            $(divOtherAvailableUsers).append(divToTransfer);
            if (divClearSibling != undefined)
                $(divOtherAvailableUsers).append(divClearSibling);

        }
        //this is used in the a tag of the div
        function moveAvilableAppToExistingApps(anchorTag) {
            var divExistingUsers = $('#' + '<%= divavailable.ClientID %>');
            var divToTransfer = anchorTag.parentElement;
            var divClearSibling = undefined;
            if (divToTransfer.nextElementSibling)
                divClearSibling = divToTransfer.nextElementSibling;
            $(anchorTag).attr("onclick", "moveappliedToOtherAvailableapp(this)");
            $(anchorTag).attr("class", "remove");
            var chkbox = $(divToTransfer).find('input:checkbox');
            var chkBoxParent = $(chkbox).parent();
            if ($(chkBoxParent).is("span")) {
                chkBoxParent.removeAttr('class');
            }
            $(divExistingUsers).append(divToTransfer);
            if (divClearSibling != undefined)
                $(divExistingUsers).append(divClearSibling);

        }
        function getExistingAppForSaving() {//ctl00_ctl00_MainCanvas_PageCanvas_divavailable
            var divExistingUsers = $('#' + '<%= divavailable.ClientID %>');
            var hidExistingUsers = document.getElementById('<%= hfappliedgroup.ClientID %>');
            hidExistingUsers.value = "";
            var NewCategory = ""; RemovedCategory = "";
            $(divExistingUsers).each(function (index) {
                $(this).children().each(function (index) {
                    var anchorTag = $(this).children('a');
                    var wfId = $(this).children('[id^="@WF_ID"]').text();
                    var wfNm = $(this).children('[id^="GROUP_NAME"]').text();
                    var groupId = $(this).children('[id^="GROUP_ID"]').text();
                    if (!(anchorTag.length === 0)) {
                        if (hidExistingUsers) {
                            if (hidExistingUsers.value === "") {
                                hidExistingUsers.value = wfId + "," + groupId;
                            }
                            else {
                                hidExistingUsers.value += "|" + wfId + "," + groupId;
                            }
                        }
                        var Exist = false;
                        $(categoryOldAppList).each(function (catIndx) {
                            if (wfNm == this) {
                                categoryOldAppList.splice(catIndx, 1);
                                Exist = true;
                            }
                        });
                        if (!Exist) NewCategory = NewCategory + ',' + wfNm;
                    }
                });
            });

            $(categoryOldAppList).each(function (catIndx) { RemovedCategory = RemovedCategory + ',' + this; });
            var FinalVal = { "add": NewCategory, "remove": categoryOldAppList };
            var hidOtherExistingUsers = document.getElementById('<%=hidOtherAvailableUsers.ClientID %>');
            hidOtherExistingUsers.value = JSON.stringify(FinalVal);
        }
        //this is used for save button for final saving of the users
        //        function getOtherAvailableAppForSaving() {
        //            var divOtherAvailableUsers = $('#' + '<%= divavailable.ClientID%>');
        //            var hidOtherExistingUsers = document.getElementById('<%= hfavilablegroup.ClientID %>');
        //            $('[id$="hidaddappingroup"]').val('');
        //            hidOtherExistingUsers.value = "";
        //            var addapp = '';
        //            $(divOtherAvailableUsers).each(function (index) {
        //                $(this).children().each(function (index) {
        //                    var anchorTag = $(this).children('a');
        //                    var wfId = $(this).children('[id^="@WF_ID"]').text();
        //                    var hidGroupIdInEditMode = $('[id$="hidGrpIdInEditMode"]');
        //                    var groupNAME = $(this).children('[id^="GROUP_NAME"]').text();
        //                    var groupId = $(hidGroupIdInEditMode).val();
        //                    if (!(anchorTag.length === 0)) {
        //                        var aryAnchorTagValues = anchorTag.attr("id").split('_');
        //                        if (hidOtherExistingUsers) {
        //                            if (hidOtherExistingUsers.value === "") {
        //                                hidOtherExistingUsers.value = wfId + "," + groupId;
        //                                addapp = groupNAME;
        //                            }
        //                            else {
        //                                hidOtherExistingUsers.value += "|" + wfId + "," + groupId;
        //                                addapp += "," + groupNAME;
        //                            }
        //                        }
        //                    }
        //                });
        //                $('[id$="hidaddappingroup"]').val(addapp);
        //            });
        //        }
    </script>
  <%--  ---Correct--%>
    <script type="text/javascript">

        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="updRepeater" runat="server">
        <ContentTemplate>
            <div id="PageCanvasContent" class="hides">
                <div id="divTab" class="tab">
                    <ul id="tabList">
                        <li id="lsttabCategory"><a id="lnktabCategory" href="#DeviceSetting" onclick="storeSelectedTabIndex(TabEnum.DEVICE);">Category</a></li>
                        <li id="lstTabUsersGroups"><a id="lnkDataCommandSetting" href="#DataCommandSettingsDiv" onclick="storeSelectedTabIndex(TabEnum.DATA_COMMAND);">User Groups</a></li>
                        <li id="lsttabPublished"><a id="lnktabPublished" href="#DataCommandSettingsDiv2" onclick="storeSelectedTabIndex(TabEnum.DATA_COMMAND1);">Publish</a></li>
                    </ul>
                <div id="DeviceSetting" class="ui-tabs-hide">
                    <div class="clear"></div>
                    <div id="MenuDefinitionDiv">
                        <div>
                        <asp:UpdatePanel runat="server" ID="updMenuCat" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                <ContentTemplate>
                                    <div id="divMenuCatRepeater">
                                        <div class="MenuDefinitionRptDiv1">
                                            <asp:Panel ID="pnlMenuCatRptBox" runat="server">
                                                <asp:Panel ID="pnlEmptyMenuCat" CssClass="repeaterBox-header" runat="server" Visible="false">
                                                    <div>
                                                    <div style="position: relative; top: 10px; right: 15px;">
                                            <asp:LinkButton ID="lnkAddNew" runat="server" Text="Add" CssClass="repeaterLink fr"
                                                OnClientClick="AddNewCategoryClick();return false;"></asp:LinkButton>
                                                </div>


                                                        <asp:Label ID="lblEmptyMenuCat" runat="server" Text="<h1>No Menu Categories defined  yet</h1>"> </asp:Label>
                                                    </div>
                                                </asp:Panel>
                                                <div style="padding-top:1px;">
                                                    <asp:Repeater ID="rptMenuCategoryDtls" runat="server" OnItemCommand="rptMenuCategoryDtls_ItemCommand"
                                                        OnItemDataBound="rptMenuCategoryDtls_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblMenuCatDtls" class="repeaterTable">
                                                            <thead>
                                                                <tr>
                                                                <th style="width:25px;">
                                                                        </th>
                                                                    <th style="width: 111px;" class="repeterCategory">
                                                                        <b>Category Name</b>
                                                                    </th>
                                                                    <th style="width: 276px;" class="repeterCategory">
                                                                        <b>Apps</b>
                                                                    </th>
                                                                    <th>
                                                                        <a class="repeaterLink" onclick="AddNewCategoryClick();">Add</a>
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tbody class="MoveableBody">
                                                                <tr class="repeaterItem">
                                                                    <td style="display: none">
                                                                        <asp:Label ID="lblDisplayIndex" runat="server" Text='<%# Eval("DISPLAY_INDEX") %>'></asp:Label>
                                                                        <asp:Label ID="lblMenuCategoryId" runat="server" Text='<%#Eval("MENU_CATEGORY_ID") %>'></asp:Label>
                                                                    </td>
                                                                    <td style="display: none" title="id">
                                                                        <asp:Literal ID="ltrlMenuCategoryId" runat="server" Text='<%#Eval("MENU_CATEGORY_ID") %>'></asp:Literal>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="imgMoveUp" runat="server" ToolTip="Move Up" CssClass="moveUp16x16"
                                                                            OnClientClick="isCookieCleanUpRequired('false');moveUp(this);return false;">
                                                    <span><img src="//enterprise.mficient.com/images/arrowup.png" alt=""  /></span>
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton ID="imgMoveDown" runat="server" CssClass="moveDown16x16" ToolTip="Move Down"
                                                                            OnClientClick="isCookieCleanUpRequired('false');moveDown(this);return false;">
                                                    <span><img src="//enterprise.mficient.com/images/arrowdown.png" alt=""  /></span>
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                    <td style="cursor:default;" >
                                                                        <asp:Label ID="lblCategoryNm" runat="server" Text='<%#Eval("CATEGORY") %>'></asp:Label>
                                                                    </td>
                                                                    <td style="max-width:300px; cursor:default;">
                                                                            <asp:Label ID="LiteralName" runat="server" Text='<%#Eval("WF_NAME") %>'></asp:Label>
                                                                    </td>
                                                                        <td class="hide">
                                                                        <asp:Label ID="literralwdids" runat="server" Text='<%#Eval("WF_IDS") %>'></asp:Label>
                                                                    </td>
                                                                        <td  style="padding:7px" class="hide">
                                                                        <asp:Label ID="litNotApp" runat="server" Text='<%#Eval("NOTWF_NAME") %>'></asp:Label>
                                                                                
                                                                    </td>
                                                                        <td  style="padding:7px" class="hide">
                                                                        <asp:Label ID="Literal3" runat="server" Text='<%#Eval("WF_ID") %>'></asp:Label>
                                                                                 
                                                                    </td>
                                                                    <td style="width: 50px; ">
                                                                        <%--<a id="aedit" onclick="EditCategory(this);"><span>--%>
                                                                        <a id="aedit" style="text-align:center" onclick="RptMenuCatRowClick(this,8);"><span>
                                                                            <img src="css/images/pencil16x16.png" alt="" /></span></a> &nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:LinkButton ID="lnkdeletecategory" runat="server" CommandName="Delete" ToolTip="Delete Category"
                                                                            OnClientClick="if (!confirm('Are you sure you want to sure Delete Category?')) return false;">
                                                        <span><img src="images/Cross1.png" alt=""  /></span>
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </ItemTemplate>
                                                        <AlternatingItemTemplate>
                                                            <tbody class="MoveableBody">
                                                                <tr class="repeaterAlternatingItem">
                                                                    <td style="display: none">
                                                                        <asp:Label ID="lblMenuCategoryId" runat="server" Text='<%#Eval("MENU_CATEGORY_ID") %>'></asp:Label>
                                                                    </td>
                                                                    <td style="display: none" title="id">
                                                                        <asp:Literal ID="ltrlMenuCategoryId" runat="server" Text='<%#Eval("MENU_CATEGORY_ID") %>'></asp:Literal>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="imgMoveUp" runat="server" CssClass="moveUp16x16" ToolTip="Move Up"
                                                                            OnClientClick="isCookieCleanUpRequired('false');moveUp(this);return false;">
                                                    <span><img src="//enterprise.mficient.com/images/arrowup.png" alt=""  /></span>
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton ID="imgMoveDown" CssClass="moveDown16x16" runat="server" ToolTip="Move Down"
                                                                            OnClientClick="isCookieCleanUpRequired('false');moveDown(this);return false;">
                                                    <span><img src="//enterprise.mficient.com/images/arrowdown.png" alt=""  /></span>
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                    <td  style="padding:7px;cursor:default; ">
                                                                        <asp:Label ID="lblCategoryNm" runat="server" Text='<%#Eval("CATEGORY") %>'></asp:Label>
                                                                    </td>
                                                                               
                                                                    <td  style="padding:7px;max-width:300px; cursor:default;">
                                                                                  
                                                                        <asp:Label ID="LiteralName" runat="server" Text='<%#Eval("WF_NAME") %>'></asp:Label>
                                                                    </td>
                                                                        <td class="hide">
                                                                        <asp:Label ID="literralwdids" runat="server" Text='<%#Eval("WF_IDS") %>'></asp:Label>
                                                  
                                                                    </td>
                                                                        <td  style="padding:7px" class="hide">
                                                                        <asp:Label ID="litNotApp" runat="server" Text='<%#Eval("NOTWF_NAME") %>'></asp:Label>
                                                                                   
                                                                    </td>
                                                                        <td  style="padding:7px" class="hide">
                                                                        <asp:Label ID="Literal3" runat="server" Text='<%#Eval("WF_ID") %>'></asp:Label>
                                                                               
                                                                    </td>
                                                                    <td style="width: 50px;">
                                                                        <%-- //<a id="aedit" onclick="EditCategory(this);"><span>--%>
                                                                            <a id="a1" style="text-align:center" onclick="RptMenuCatRowClick(this,8);"><span>
                                                                            <img src="css/images/pencil16x16.png" alt="" /></span></a> &nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:LinkButton ID="lnkdeletecategory"  runat="server" CommandName="Delete" ToolTip="Delete Category"
                                                                            OnClientClick="if (!confirm('Are you sure you want to sure Delete Category?')) return false;">
                                                        <span><img src="images/Cross1.png" alt=""  /></span>
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </AlternatingItemTemplate>
                                                        <FooterTemplate>
                                                            <table id="tblMenuCatDtls1" class="repeaterTable">
                                                                <tr>
                                                                    <td style="width: 20px; font-size: 1.1em">
                                                                        <asp:LinkButton ID="lnksubmit" runat="server" Style="float: right;  color: #777777;" Text="Apply" OnClick="btnApply_Click"
                                                                            OnClientClick="isCookieCleanUpRequired('false');getTheFinalDiaplayIndexOfCatForDBSave();"
                                                                            CssClass="repeaterLink1"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </asp:Panel>
                                            <asp:HiddenField ID="hidmenucategory" runat="server" />
                                            <asp:HiddenField ID="hidFinalDisplayIndexOfCat" runat="server" />
                                        </div>
                                    </div>
                                    <div id="SubProcConfirmBoxMessage" style="display: none">
                                        <div>
                                            <div>
                                                <div class="ConfirmBoxMessage1">
                                                    <a id="aCfmMessage"></a>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="SubProcborderDiv">
                                                            
                                                <div align="center">
                                                    <input id="btnCnfFormSave" type="button" value=" OK" onclick="SubProcConfirmBoxMessage(false);"
                                                        class="InputStyle" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div id="DataCommandSettingsDiv2" class="ui-tabs-hide"> 
                    <div class="clear"></div>
                    <div id="PublishAppDiv">
                        <asp:UpdatePanel runat="server" ID="updPublishApp" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="MenuDefinitionRptDiv">
                                    <asp:Panel ID="Panel7" runat="server">
                                        <div id="div64" style="padding-top:1px;">
                                            <asp:Repeater ID="rptWFPublishApp"  runat="server"
                                                    OnItemDataBound="rptWFPublishApp_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table class="repeaterTable">
                                                        <thead>
                                                            <tr>
                                                            <th style="width:25px;">
                                                            </th>
                                                    <th style="width:381px;" class="repeterCategory"> <b>App Name</b>
                                                                </th>
                                                                <th style="width: 216px;" class="repeterCategory">
                                                                    <b> Version</b>
                                                                </th>
                                                                <th>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr class="repeaterItem">
                                                            <td style="display: none">
                                                                            
                                                                <asp:Label ID="lblWorkflowId" runat="server" Text='<%#Eval("WF_ID") %>'></asp:Label>
                                                                <asp:Label ID="lblcategorynamedetails" runat="server" Text='<%#Eval("CATEGORY") %>'></asp:Label>

                                                            </td>
                                                            <td class="nocursor">
                                                                    <img src="//enterprise.mficient.com/images/icon/<%#Eval("WF_ICON") %>" style="position: inherit;"
                                                                        width="16px" alt="" /></td>
                                                            <td class="nocursor">
                                                                <asp:Label runat="server" ID="lblname" Text='<%# Eval("WF_NAME") %>' />
                                                            </td>
                                                            <td class="nocursor">
                                                                <asp:Label runat="server" ID="lblversion" Text='<%# Eval("VERSION") %>' />
                                                                                    
                                                                </td>
                                                                <td style="display:none">
                                                                <asp:Label runat="server"  ID="lblpreversion" Text='<%#Eval("preversion") %>'></asp:Label>
                                                                <asp:Label runat="server"   ID="lbldeviceinterface" Text='<%#Eval("Device") %>'></asp:Label>
                                                                    <asp:Label runat="server" ID="lblAllVersions" Text='<%#Eval("lastallversion") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <a id="ainfo" onclick="ViewInfo(this);"><span>
                                                                        <img src="css/images/info.png" alt="" /></span></a>

                                                                                &nbsp;
                                                                    <a id="apublish" onclick="ViewpublishInfo(this);"><span>
                                                                        <img src="css/images/publish1.png" alt="" /></span></a>
                                                                           
                                                                                   
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                    <tr class="repeaterAlternatingItem">
                                                            <td style="display: none">
                                                                    <asp:Label ID="lblWorkflowId" runat="server" Text='<%#Eval("WF_ID") %>'></asp:Label>
                                                                    <asp:Label ID="lblcategorynamedetails" runat="server" Text='<%#Eval("CATEGORY") %>'></asp:Label>
                                                            </td>
                                                            <td class="nocursor">
                                                                    <img src="//enterprise.mficient.com/images/icon/<%#Eval("WF_ICON") %>" style="position: inherit;"
                                                                        width="16px" alt="" /></td>
                                                            <td class="nocursor">
                                                                <asp:Label runat="server" ID="lblname" Text='<%# Eval("WF_NAME") %>' />
                                                            </td>
                                                            <td class="nocursor">
                                                                <asp:Label runat="server" ID="lblversion" Text='<%# Eval("VERSION") %>' />

                                                                    </td>
                                                                    <td style="display:none">
                                                                    <asp:Label runat="server"  ID="lblpreversion" Text='<%#Eval("preversion") %>'></asp:Label>
                                                                <asp:Label runat="server" ID="lbldeviceinterface"  Text='<%#Eval("Device") %>'></asp:Label>
                                                                <asp:Label runat="server" ID="lblAllVersions" Text='<%#Eval("lastallversion") %>'></asp:Label>
                                                                               
                                                                </td>

                                                            <td>

                                                                    <a id="ainfo" onclick="ViewInfo(this);"><span>
                                                                        <img src="css/images/info.png" alt="" /></span></a>
                                                                        &nbsp;
                                                            <a id="apublish" onclick="ViewpublishInfo(this);"><span>
                                                                        <img src="css/images/publish1.png" alt="" /></span></a>

                                                            </td>
                                                        </tr>
                                                    </AlternatingItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                            
                                        </div>
                                    </asp:Panel>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>                                    
                    </div>
                </div>                            
                <div id="DataCommandSettingsDiv" class="ui-tabs-hide">
                    <div class="clear"></div>                            
                    <div id="DivGroup">
                            <div>
                                <asp:UpdatePanel runat="server" ID="updAppInfoGroup" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="MenuDefinitionRptDiv">
                                            <asp:Panel ID="pnlEmptyPublishApps" runat="server">
                                                <div id="div39" style="padding-top:1px;">
                                                            
                                                        <asp:Repeater ID="repetergroup" runat="server" OnItemCommand="repetergroup_ItemCommand">
                                                            <HeaderTemplate>
                                                                <table class="repeaterTable">
                                                                <thead>
                                                                        <tr>
                                                            <th style="width: 151px;" class="repeterCategory">
                                                                            <b> Group Name</b>
                                                                        </th>
                                                                        <th class="repeterCategory">
                                                                            <b> App Name</b>
                                                                        </th>
                                                                        <th style="width: 51px;" class="repeterCategory">
                                                                                  
                                                                        </th>
                                                                               
                                                                    </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tbody class="MoveableBody">
                                                                <tr class="repeaterItem">
                                                                    <td style="display: none">
                                                                        <asp:Label ID="lblworkflowid" runat="server" Text='<%#Eval("WF_IDS") %>'></asp:Label>
                                                                    </td>
                                                                    <td class="nocursor">
                                                                        <asp:Label runat="server" ID="lblgroupname1" Text='<%#Eval("GROUP_NAME") %>' />
                                                                        <asp:Label runat="server" ID="lblgroupiddetails" Text='<%#Eval("GROUP_ID") %>' Visible="false"></asp:Label>
                                                                    </td>
                                                                    <td class="nocursor" style="max-width:200px">
                                                                        <asp:Label runat="server" ID="lblasignApps1" Text='<%# Eval("WF_NAMES") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="lnkedit"    runat="server" ToolTip="Manages Apps"  CommandName="Edit"
                                                                            OnClientClick="isCookieCleanUpRequired('false');">
                                                                                                <span><img src="css/images/pencil16x16.png" alt=""  /></span>
                                                                                   
                                                                            </asp:LinkButton>&nbsp;
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </ItemTemplate>
                                                            <AlternatingItemTemplate>
                                                                <tbody class="MoveableBody">
                                                                <tr class="repeaterItem">
                                                                    <td style="display: none">
                                                                        <asp:Label ID="lblworkflowid" runat="server" Text='<%#Eval("WF_IDS") %>'></asp:Label>
                                                                    </td>
                                                                    <td class="nocursor">
                                                                        <asp:Label runat="server" ID="lblgroupname1" Text='<%#Eval("GROUP_NAME") %>' />
                                                                        <asp:Label runat="server" ID="lblgroupiddetails" Text='<%#Eval("GROUP_ID") %>' Visible="false"></asp:Label>
                                                                    </td>
                                                                    <td class="nocursor" style="max-width:200px">
                                                                        <asp:Label runat="server" ID="lblasignApps1" Text='<%# Eval("WF_NAMES") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="lnkedit"   runat="server" ToolTip="Manages Apps"   CommandName="Edit"
                                                                            OnClientClick="isCookieCleanUpRequired('false');">
                                                                                <span><img src="css/images/pencil16x16.png" alt=""  /></span>
                                                                                    
                                                                            </asp:LinkButton>&nbsp;
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </AlternatingItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>                            
                    <asp:HiddenField ID="hgorupid" runat="server" />
                    <asp:HiddenField ID="hfappliedgroup" runat="server" />
                    <asp:HiddenField ID="hfavilablegroup" runat="server" />
                </div>
                <div id="SubProcImageDelConfirnmation">
                    <div>
                        <div>
                            <asp:HiddenField ID="hdfDelId" runat="server" />
                        </div>
                        <div>
                            <asp:HiddenField ID="hdfDelType" runat="server" />
                        </div>
                        <div class="MarginT10">
                        </div>
                        <div class="MessageDiv">
                            <a id="aDelCfmMsg"></a>
                        </div>
                        <div class="MarginT10">
                        </div>
                    </div>
                </div>
            </div>
                       
            </div>
            <div>
                <asp:HiddenField ID="hidCId" runat="server" />
                <asp:HiddenField ID="hidUserId" runat="server" />
                <asp:HiddenField ID="hidTabSelected" runat="server" />
                <asp:HiddenField ID="hidOtherAvailableUsers" runat="server" />
                <asp:HiddenField ID="hidExistingUsers" runat="server" />
                <asp:HiddenField ID="hidexistingappingroup" runat="server" />
                <asp:HiddenField ID="hidaddappingroup" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="SubProcPublishApp" class="hide">
        <asp:UpdatePanel runat="server" ID="updSubProcPublichApp" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="margin-top: 10px;">
                    <asp:Label ID="lblworkflow" Visible="false" runat="server"></asp:Label>
                    <div class="DbCmdRow" style="margin-left: 10px">
                        <asp:HiddenField ID="hdfPubAppWfId" runat="server" />
                        <asp:HiddenField ID="hdfselectedVersion" runat="server" />
                        <asp:HiddenField ID="hdfPrvVersion" runat="server" />
                    </div>
                    <div class="clear"></div>
                    <div id="PubAppCurrStatusDiv" class="DbCmdRow" style="margin-left: 10px" runat="server">
                        <div class="DbConnLeftDiv1" style="padding-left: 2px">
                        </div>
                    </div>
                    <div id="lblmsg56"  style="display:none">
                        <asp:Label ID="lblnoverstion" Width="100%" runat="server"  Text="There is no unpublished committed version available for publishing!"></asp:Label>
                    </div>
                    <div id="divPublishedVersions" class="htmlRadioList" style="width: 100%"></div>
                    <div class="SubProcborderDiv">
                        <div class="SubProcBtnDiv" align="center">
                            <asp:Button ID="btnCancelapppublish" runat="server" OnClientClick="$('#SubProcPublishApp').dialog('close');return false;"
                                Text="Cancel" CssClass="InputStyle" />
                            <asp:Button ID="btnPulishAppSave" OnClick="btnPulishAppSave_Click" OnClientClick="return processSaveNewVersion();"
                                runat="server" Text="Apply" CssClass="InputStyle" />
                            <asp:Button ID="btnUnpublish" runat="server" Text="Unpublish" OnClientClick="if(!confirm('Are you sure you want to sure Unpublish App?')) return false;"
                                OnClick="btnUnpublish_Click" CssClass="InputStyle" />
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="SubProcMenuCategory" class="hide">
        <div class="FLeft" style="width: 474px;">
            <asp:UpdatePanel runat="server" ID="updSubProcCat" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlMenuCategory1" runat="server">
                        <div class="clear"></div>
                        <div id="divAddRemoveUsersDelete" style="float: right"></div>
                        <div class="clear" style="height: 0px;"></div>
                        <div id="divAddRemoveUsersError"></div>
                        <div style="margin-bottom: 10px;">
                            <div style="float: left; margin-right: 10px; margin-left: 30px; position: relative; top: 5px;">
                                <asp:Label ID="lblGroupName" Visible="false" runat="server" Text="Category Name :"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtcatagoryname" Visible="false" runat="server" Width="250px" MaxLength="50"></asp:TextBox>
                                <asp:Label ID="lblcategoryidedit" runat="server" Visible="false"></asp:Label>
                                <asp:HiddenField ID="hidMenuCategoryForEdit" runat="server" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div style="margin-left: 30px; margin-bottom: 5px;">
                            <asp:Label ID="lblUsersLabel" Visible="false" runat="server" Text="Select Apps :"></asp:Label>
                        </div>
                        <div style="margin-bottom: 20px;">
                            <div class="comboselectbox searchable" style="height: 250px; margin-left: 30px;">
                            <div class="MenuLeftDiv">
                        <div style="font-size: 14px">
                            Category Name</div>
                    </div>
                    <div class="MenuCenterDiv">
                        :</div>
                    <div class="MenuRightDiv">
                        <asp:TextBox ID="txtupdatectegryinfo" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        <asp:HiddenField ID="hidupdatecategoryinfoname" runat="server" />
                        <asp:HiddenField ID="hidupdatecategoryinfoid" runat="server" />
                    </div>

                                <div class="combowrap" style="margin-top:15px">
                                    <div id="divOtherAvailableUsers" class="comboRepeater" runat="server">
                                        <div class="comboHeader" id="divavilableappchild">
                                            <h3>
                                                Available Apps</h3>
                                        </div>
                                
                                </div>
                                </div>
                                <div class="comboselectbuttons">
                                </div>
                                <div class="combowrap"  style="margin-top:15px">
                                    <div id="divExistingUsers" class="comboRepeater" runat="server">
                                        <div class="comboHeader" id="divexistappchild">
                                            <h3>
                                                Existing Apps</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="bottom" >
                                <asp:Button ID="btncancelCategorybyApp" style="margin-top:10px" runat="server" Text="Cancel" OnClientClick="$('#SubProcMenuCategory').dialog('close');return false;" />&nbsp;&nbsp;
                                <asp:Button ID="btnCategorybyAppSave" style="margin-top:10px" runat="server" Text="Apply" OnClick="btnSave_Click" OnClientClick="return processSaveGroup();" />&nbsp;&nbsp;
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="SubProcMenuicon">
        <div>
            <asp:UpdatePanel runat="server" ID="UpdIconname" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:HiddenField ID="hdfIconname" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="EditMenuCatDetailsDiv123" style="display: none">
        <div style="margin-top: 20px;">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div id="MenuCatDetailHeaderDiv123" class="modalPopUpDetHeaderDiv hide">
                        <div class="modalPopUpDetHeader">
                            <asp:Label ID="lblMenucatEdit" runat="server" Text=""></asp:Label>
                        </div>
                        <div style="float: right;">
                            <div style="float: left;"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="MenuLeftDiv" style="padding-left: 10px;">
                        <div style="font-size: 14px">Category Name</div>
                    </div>
                    <div class="MenuCenterDiv">:</div>
                    <div class="MenuRightDiv">
                        <asp:TextBox ID="txtCategoryName" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        <asp:TextBox ID="txtcategoryid" Visible="false" runat="server"></asp:TextBox>
                        <asp:HiddenField ID="hidMenuCategoryForEdit123" runat="server" />
                        <asp:HiddenField ID="hidMenuCategoryForEditCategoryid" runat="server" />
                    </div>
                    <div class="MenuLeftIconDiv hide"> Icon
                    </div>
                    <div class="MenuCenterDiv  hide">:</div>
                    <div class="MenuCategory2  hide" title="Click to change icon">
                        <img alt="" class="MenuIcon" src="//enterprise.mficient.com/images/icon/GRAY0207.png"
                            id="imgmenuIcon" onclick="SubProcMenuCatIcon(true);" />
                    </div>
                    <div class="MenuCenterDiv  hide">
                        <a id="MenuIconName"></a>
                    </div>
                    <div class="MenuCategory2 hide">
                        <asp:UpdatePanel runat="server" ID="UpdatePanel9" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Button ID="btnMenuCatIconClear" runat="server" Text="Clear" CssClass="InputStyle"
                                    OnClick="btnMenuCatIconClear_Click" />&nbsp;
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="clear">
                    </div>
                    <div class="SubProcborderDiv">
                        <div class="SubProcBtnDiv" style="text-align: center">
                            <asp:Button ID="btncanceleditcategory" runat="server" Text="Cancel" OnClientClick="SubProcMenuCategory123(false);return false;"
                                CssClass="InputStyle" />
                            &nbsp;
                            <asp:Button ID="btnupdatedCategory" runat="server" Text="Save" CssClass="InputStyle" OnClick="Add_Click"
                                OnClientClick="return ValidateMenuCategory();" />
                            &nbsp;
                        </div>
                    </div>
                    <asp:HiddenField ID="hidQueryStrFromEmail" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>    
    <div id="divgroupsinformation" class="hide" >
        <asp:UpdatePanel ID="updGrpsInformation" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="DbConnLeftDiv">
                    <asp:HiddenField ID="hidGrpIdInEditMode" runat="server" />
                      <asp:HiddenField ID="hidgroupname" runat="server"  />
                </div>
                <div style="margin-bottom: 20px;">
                    <div class="comboselectbox searchable" style="height: 250px;">
                        <div class="combowrap" style="margin-left:14px">
                            <div id="divassigned" class="comboRepeater"  runat="server">
                                <div class="comboHeader">
                                    <h5>
                                        Assigned Groups</h5>
                                </div>
                            </div>
                        </div>
                        <div class="comboselectbuttons">
                            <div style="margin-top: 100px;">
                            </div>
                        </div>                        <div class="combowrap">
                            <div id="divavailable" class="comboRepeater" runat="server">
                                <div class="comboHeader">
                                    <h5>
                                        Available Groups</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div style="text-align: center">
                        <asp:Button ID="btncancelupdategroupapps" runat="server" Text="Cancel" OnClientClick="$('#divgroupsinformation').dialog('close');return false;"
                            CssClass="InputStyle" />
                        <asp:Button ID="btnSavegroupApps" runat="server" Text="Apply" OnClientClick="return processSaveAppsviaGroup();"
                            OnClick="btnSavegroup_Click" />&nbsp;&nbsp;
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <div>
    </div>
    <div>
        <div>
                <asp:Button ID="btnDbConnRowIndex1" runat="server" CssClass="hide" />
        </div>
        <div>
            <asp:Button ID="brnBackToUiDetails" PostBackUrl="~/UiDetail.aspx" runat="server"
                CssClass="hide" />
        </div>
    </div>
    <div>
        <asp:UpdatePanel runat="server" ID="updValidWs" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:HiddenField ID="hdfvalidWs" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="hdfRptRowIndex" runat="server" />
    <div id="divModalContainer" style="display: none;">
        <asp:UpdatePanel ID="updModalContainer" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divAddWorkFlow" style="display: block">
                    <div id="modalPopUpContent">
                        <div>
                            <div id="Div4" class="modalPopUpDetHeaderDiv" visible="true" runat="server">
                                <div class="modalPopUpDetHeader">
                                    <asp:Label ID="lblWfCat_Head" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="FLeft">
                                    <img alt="" class="MenuIconHead" src="//enterprise.mficient.com/images/icon/GRAY0161.png"
                                        id="imgProcessIcon" />
                                </div>
                                <div class="MenuIconHeadTxt" onclick="ShowAppMenuIcon();">
                                    ( Click to change icon )
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="MenuLeftDiv">
                                Menu Apps
                            </div>
                            <div class="MenuCenterDiv">
                                :</div>
                            <div class="MenuRightDiv">
                                <div class="FLeft">
                                    <asp:DropDownList ID="ddlCategory" runat="server" AppendDataBoundItems="true" class="UseUniformCss">
                                        <asp:ListItem Text="None" Value="None"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="MenuLeftIconDiv hide">
                                Icon
                            </div>
                            <div class="MenuCenterDiv hide">
                                :</div>
                            <div class="MenuCategory2 hide" title="Click to change icon">
                            </div>
                            <div class="MenuCenterDiv hide">
                                <a id="ProcessIconName"></a>
                            </div>
                            <div class="MenuCategory2 hide">
                                <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Button ID="btnProcessIconClear" runat="server" Text="Clear" CssClass="InputStyle"
                                            OnClick="btnProcessIconClear_Click" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="clear">
                            </div>
                            <div>
                                <div class="FLeft" style="width: 100%;">
                                    <div class="MenuLeftDiv">
                                        User Permissions
                                    </div>
                                    <div class="MenuCenterDiv">
                                        :</div>
                                    <div class="MenuCategory7">
                                        Select All
                                    </div>
                                    <div id="chkWFSelectAllDiv" class="MenuCategory3">
                                        <asp:CheckBox ID="chkWFSelectAll" runat="server" onclick="CheckBoxSelectAllChange();" />
                                    </div>
                                </div>
                                <div style="clear: both;">
                                </div>
                                <div id="chkLstWFGroupDiv" class="MenuCategory4">
                                    <asp:CheckBoxList ID="chkLstWFGroup" runat="server" RepeatColumns="2" CssClass="chklistGroup">
                                    </asp:CheckBoxList>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="SubProcborderDiv">
                                <div class="SubProcBtnDiv" align="center">
                                    <asp:Button ID="btnWFSave" runat="server" Text="Save" CssClass="InputStyle" OnClientClick="appSettingValidation();" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="SubProcAppInfo" class="hide">
        <asp:UpdatePanel runat="server" ID="updAppInfo" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="div1">
                    <div id="Overview">
                        <div style="margin-top: 5px;">
                            <div id="Div12" class="DbCmdRow" style="margin-left: 10px" runat="server">
                                <div class="DbConnLeftDiv">
                                  <b> Categories</b>
                                </div>
                                <div class="DbConnLeftDiv" style="width: 2px; margin-right: 5px;">
                                    :</div>
                                    <span id="Menu Categories"></span>
                                <div class="DbConnLeftDiv">
                                    <asp:Label ID="lblAppInfoCat" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="DbCmdRow" style="margin-left: 10px">
                                <div class="DbConnLeftDiv">
                                 <b>   App Interface </b>
                                </div>
                                <div class="DbConnLeftDiv" style="width: 2px; margin-right: 5px;">
                                    :</div>
                                <div class="DbConnRightDiv" style="width: 50%; padding-top: 8px">
                                    <asp:Label ID="lblappinterface" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="DbCmdRow" style="margin-left: 10px">
                                <div class="DbConnLeftDiv">
                            <b>        Published Version </b>
                                </div>
                                <div class="DbConnLeftDiv" style="width: 2px; margin-right: 5px;">
                                    :</div>
                                <div class="DbConnRightDiv" style="width: 50%; padding-top: 7px">
                                    <asp:Label ID="lblpublishversion" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="DbCmdRow" style="margin-left: 10px">
                                <div class="DbConnLeftDiv">
                                <b>    Last Version </b>
                                </div>
                                <div class="DbConnLeftDiv" style="width: 2px; margin-right: 5px;">
                                    :</div>
                                <div class="DbConnRightDiv" style="width: 60%; padding-top: 7px">
                                    <asp:Label ID="lbllastversion" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="DbCmdRow" style="margin-left: 10px">
                            </div>
                            <div class="clear">
                            </div>
                            <div style="margin-top: 15px;">
                            </div>
                        </div>
                    </div>
      
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--<asp:HiddenField ID="hidexistingapp" runat="server" />--%>




    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {

            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {

            hideWaitModal();
            isCookieCleanUpRequired('true');

        }
    </script>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/IdeMaster.Master" AutoEventWireup="true"
    CodeBehind="ODataObjects.aspx.cs" Inherits="mFicientCP.ODataObjects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/jquery.datatables.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/Jquerydatatablerowclick.css" />
    <script src="Scripts/IdeTokeninput201409161055.js" type="text/javascript"></script>
    <script src="Scripts/mfIdeODataObjects.js" type="text/javascript"></script>
    <style type="text/css">
        .datatablebind
        {
            width: 49%;
            float: left;
            margin-left: 7px;
        }
        .addform
        {
            float: right;
            margin-left: 8px;
            margin-right: 7px;
            width: 49%;
            min-width: 450px;
        }
        .QPContentText1
        {
            padding-left: 3px;
            padding-top: 3px;
            padding-bottom: 3px;
            width: 48px;
            float: left; /*  border-bottom: solid 1px silver;
    border-right: solid 1px silver; */
            height: 20px;
        }
        .QPContentType1
        {
            padding-left: 13px;
            padding-top: 3px;
            padding-bottom: 3px;
            width: 190px;
            float: left; /*  border-bottom: solid 1px silver;
    border-right: solid 1px silver; */
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainCanvas" runat="server">
    <div id="divMainContainer" class="g12">
        <%--<section style="margin: 5px 5px 5px 3px;">--%>
        <asp:Panel ID="pnlDataObjects" CssClass="repeaterBox" runat="server">
            <asp:Panel ID="pnlDataObjectsHeader" CssClass="repeaterBox-header" runat="server">
                <div>
                    <asp:Label ID="lblDbObject" runat="server" Text="<h1>OData Objects</h1>">
                    </asp:Label>
                </div>
                <div style="position: relative; top: 10px; right: 15px;">
                    <a class="repeaterLink fr" onclick="isCookieCleanUpRequired('false');addInitializeNewObject();">
                        Add</a>
                    <%-- <asp:LinkButton ID="lnkdivListDataObjectAddNewDataObject" runat="server" Text="Add"
                            CssClass="repeaterLink fr" OnClientClick="isCookieCleanUpRequired('false');initializeDataObjects();return false;"></asp:LinkButton>--%>
                </div>
            </asp:Panel>
            <%--<div id="divODataObjects" style="padding-top: 5px">--%>
            <asp:UpdatePanel runat="server" ID="updDataObjectsList" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divListAndSearchObjects" class="g5">
                        <div id="divListDataObject" class="g12">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="updAddNewDataObject" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divAddEditDataObj" runat="server" class="g7">
                        <div style="margin: 0px auto;">
                            <div class="ProcPriview" style="width: 98%; padding: 5px;">
                                <div id="divDataObjectDetails" class="hide">
                                    <div id="divViewDetails">
                                        <div class="modalPopUpDetHeaderDiv" id="divHeader">
                                            <div class="modalPopUpDetHeader" id="divObjectName">
                                                <asp:Label ID="lblDataObjectName" runat="server" Text=""></asp:Label>
                                            </div>
                                            <div style="float: right;">
                                                <div style="float: left;">
                                                    <div style="float: left;" id="Divcopy">
                                                        <asp:LinkButton ID="lnkCopy" runat="server" Text="Copy" OnClientClick="isCookieCleanUpRequired('false');showODataCopyObjectPopUp(true); return false;"></asp:LinkButton>
                                                    </div>
                                                    <div style="float: left;">
                                                        &nbsp;|&nbsp;
                                                    </div>
                                                    <div class="FLeft">
                                                        <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" OnClientClick="isCookieCleanUpRequired('false');"
                                                            OnClick="lnkEdit_Click"></asp:LinkButton>
                                                    </div>
                                                    <div style="float: left;">
                                                        &nbsp;|&nbsp;</div>
                                                    <div class="FLeft">
                                                        <asp:Button ID="savebtn" runat="server" OnClick="savebtn_Click" Style="display: none" />
                                                        <asp:LinkButton ID="lnktest" runat="server" Text="Test" OnClientClick="isCookieCleanUpRequired('false');dialogtest();return false;"></asp:LinkButton>
                                                        <asp:HiddenField ID="hdflp" runat="server" />
                                                    </div>
                                                    <div style="float: left;">
                                                        &nbsp;|&nbsp;
                                                    </div>
                                                    <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" OnClientClick="isCookieCleanUpRequired('false');deleteODataObject();return false;"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modalPopUpDetRow" style="border: none; padding: 0px 0px 0px 0px;">
                                            <div class="modalPopUpDetRow">
                                                <div class="modalPopUpDetColumn1">
                                                    Description
                                                </div>
                                                <div class="modalPopUpDetColumn2">
                                                    :
                                                </div>
                                                <div class="modalPopUpDetColumn3">
                                                    <asp:Label ID="lblDesc" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="modalPopUpDetRow">
                                                <div class="modalPopUpDetColumn1">
                                                    Connector
                                                </div>
                                                <div class="modalPopUpDetColumn2">
                                                    :
                                                </div>
                                                <div class="modalPopUpDetColumn3">
                                                    <asp:Label ID="lblConnector" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="modalPopUpDetRow">
                                                <div class="modalPopUpDetColumn1">
                                                    Resource Type
                                                </div>
                                                <div class="modalPopUpDetColumn2">
                                                    :
                                                </div>
                                                <div class="modalPopUpDetColumn3">
                                                    <asp:Label ID="lblResourceType" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="modalPopUpDetRow">
                                                <div class="modalPopUpDetColumn1">
                                                    Return Type
                                                </div>
                                                <div class="modalPopUpDetColumn2">
                                                    :
                                                </div>
                                                <div class="modalPopUpDetColumn3">
                                                    <asp:Label ID="lblReturnType" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="OdataCmdDet_EntDiv" class="FLeft" style="width: 100%;">
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Entity Type
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblEntityType" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        HTTP Type
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblHttpType" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="OdataCmdDet_FuncDiv" class="FLeft hide" style="width: 100%;">
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Function
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblFunctionName" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="modalPopUpDetRow">
                                                    <div class="modalPopUpDetColumn1">
                                                        Function Type
                                                    </div>
                                                    <div class="modalPopUpDetColumn2">
                                                        :
                                                    </div>
                                                    <div class="modalPopUpDetColumn3">
                                                        <asp:Label ID="lblFunctionType" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="modalPopUpDetRow">
                                                <div class="modalPopUpDetColumn1">
                                                    Created By
                                                </div>
                                                <div class="modalPopUpDetColumn2">
                                                    :
                                                </div>
                                                <div class="modalPopUpDetColumn3">
                                                    <asp:Label ID="lblCreatedBy" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div class="modalPopUpDetRow">
                                                <div class="modalPopUpDetColumn1">
                                                    Updated By
                                                </div>
                                                <div class="modalPopUpDetColumn2">
                                                    :
                                                </div>
                                                <div class="modalPopUpDetColumn3">
                                                    <asp:Label ID="lblUpdatedBy" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div>
                                                <div class="modalPopUpDetRow">
                                                    Apps using this object :
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="CmdAppRoot" style="overflow: hidden;">
                                                    <div id="DbCmdAppDiv" style="overflow: hidden;">
                                                        This object is not used any where.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="divEditDataObject">
                                    <div id="divEditDataObjLbl" class="modalPopUpDetHeaderDiv hide">
                                        <div class="modalPopUpDetHeader">
                                            <asp:Label ID="lblEditDataObj_Name" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="Headerfontsize" id="addDataObjHeader">
                                        Add New OData Object</div>
                                    <div class="Headernewfontsize" id="editDataObjHeader">
                                        Edit OData Object
                                    </div>
                                    <div class="DbConnRightDiv">
                                    </div>
                                    <div class="clear">
                                    </div>
                                    <asp:UpdatePanel ID="updEditODataObject" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div id="divDataObjName" runat="server" class="DbCmdRow">
                                                <div class="DbConnLeftDiv">
                                                    Object Name
                                                </div>
                                                <div class="DbConnRightDiv">
                                                    <asp:TextBox ID="txtDataObj_Name" runat="server" class="CmdDescInputStyle1"></asp:TextBox>
                                                    <asp:HiddenField ID="hidparameter" runat="server" />
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div id="divDataObjDesc" runat="server" class="DbCmdRow">
                                                <div class="DbConnLeftDiv" style="margin-top: 12px;">
                                                    Description
                                                </div>
                                                <div class="DbConnRightDiv">
                                                    <asp:TextBox ID="txtDataObj_Desc" runat="server" TextMode="MultiLine" class="CmdDescInputStyle1"
                                                        Rows="2"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="divConnector" runat="server" class="DbCmdRow">
                                                <div class="DbConnLeftDiv">
                                                    Connector
                                                </div>
                                                <div class="DbConnRightDiv SelectDiv" style="max-width: 25%;">
                                                    <div style="float: left; max-width: 30%;">
                                                        <%--<asp:UpdatePanel ID="updConChanged" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>--%>
                                                        <asp:DropDownList ID="ddlOdataCmd_Conn" runat="server" class="UseUniformCss" AutoPostBack="true"
                                                            OnSelectedIndexChanged="ddlOdataCmd_Conn_Changed">
                                                        </asp:DropDownList>
                                                        <%--</ContentTemplate>
                                                                </asp:UpdatePanel>--%>
                                                    </div>
                                                    <div id="imgODataConnectorInfoDiv" style="margin-top: 8px; max-width: 70%; float: right;"
                                                        class="hide">
                                                        <asp:Image ID="imgODataConnectorInfo" runat="server" ImageUrl="~/css/images/info.png"
                                                            AlternateText="Connector Info" /></div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="divInputParams">
                                                <div id="btnDbcmdParaAddDiv" runat="server" class="DbCmdRow" style="margin-top: 10px;">
                                                    <div class="DbConnLeftDiv">
                                                        Input Parameters
                                                    </div>
                                                    <div class="DbConnRightDiv">
                                                        <asp:LinkButton ID="lnkAddODataPara" runat="server" OnClientClick="SubProcODataCmdAddPara(true);return false;"
                                                            CssClass="lnkNoBorder"><img src="css/images/icons/dark/pencil.png" alt="Edit" title="Edit"/></asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div id="clearDivAddParameter" runat="server" class="clear">
                                                </div>
                                                <div id="lblInputParametersDiv" class="InputStyle" runat="server" style="padding-top: 5px;
                                                    padding-bottom: 5px; margin-top: 5px; margin-left: 8px; float: left; overflow-y: scroll">
                                                    <label id="lblOdataInputPara">
                                                        No Parameters defined yet</label>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="divResponseFormat" class="DbCmdRow">
                                                <div class="DbConnLeftDiv">
                                                    Response Format
                                                </div>
                                                <div class="DbConnRightDiv SelectDiv">
                                                    <asp:DropDownList ID="ddlOdataCmd_ResFormat" runat="server" class="UseUniformCss"
                                                        onchange="ddlOdataCmd_ResFormat(this);">
                                                        <asp:ListItem Value="0">ATOM+XML</asp:ListItem>
                                                        <asp:ListItem Value="1">JSON</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="divResourceType" class="DbCmdRow">
                                                <div class="DbConnLeftDiv">
                                                    Type
                                                </div>
                                                <div class="DbConnRightDiv SelectDiv">
                                                    <asp:DropDownList ID="ddlOdataCmd_EntType" runat="server" class="UseUniformCss" onchange="resouceTypeChanged(this);">
                                                        <asp:ListItem Value="-1">Select Type</asp:ListItem>
                                                        <asp:ListItem Value="0">EntityType</asp:ListItem>
                                                        <asp:ListItem Value="1">Function</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="ODataCmd_EntityDiv" class="DbCmdRow">
                                                <div class="DbConnLeftDiv">
                                                    Entity Type
                                                </div>
                                                <div class="DbConnRightDiv SelectDiv">
                                                    <asp:DropDownList ID="ddlOdataCmd_EntityType" runat="server" class="UseUniformCss"
                                                        OnSelectedIndexChanged="ddlOdataCmd_EntityType_Changed" AutoPostBack="true">
                                                        <asp:ListItem Value="-1">Select Entity Type</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="ODataCmd_FunctionDiv" class="DbCmdRow">
                                                <div class="DbConnLeftDiv">
                                                    Function
                                                </div>
                                                <div class="DbConnRightDiv SelectDiv">
                                                    <asp:DropDownList ID="ddlOdataCmd_funcImport" runat="server" class="UseUniformCss"
                                                        OnSelectedIndexChanged="ddlOdataCmd_funcImport_Changed" AutoPostBack="true">
                                                        <asp:ListItem Value="-1">Select Function</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="ODataCmd_Function_TypeDiv" class="DbCmdRow">
                                                <div class="DbCmdRow">
                                                    <div class="DbConnLeftDiv">
                                                        HTTP Type
                                                    </div>
                                                    <div class="DbConnRightDiv" style="margin-left: 2px;">
                                                        <asp:Label ID="lblOdataCmd_FuncHType" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="DbConnLeftDiv">
                                                    Function Type
                                                </div>
                                                <div class="DbConnRightDiv" style="margin-left: 2px;">
                                                    <asp:Label ID="lblOdataCmd_FuncType" runat="server" Text=""></asp:Label>
                                                </div>
                                                <br />
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="ODataCmd_HttpTypeDiv" class="DbCmdRow">
                                                <div class="DbConnLeftDiv">
                                                    HTTP Type
                                                </div>
                                                <div class="DbConnRightDiv SelectDiv">
                                                    <asp:UpdatePanel ID="updHttpTypeChanged" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlOdataCmd_Ent_HttpTyp" runat="server" class="UseUniformCss"
                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlOdataCmd_Ent_HttpTyp_Changed">
                                                                <asp:ListItem Value="-1">Select Http Type</asp:ListItem>
                                                                <asp:ListItem Value="1">GET</asp:ListItem>
                                                                <asp:ListItem Value="2">POST</asp:ListItem>
                                                                <asp:ListItem Value="3">PUT</asp:ListItem>
                                                                <asp:ListItem Value="4">PATCH</asp:ListItem>
                                                                <asp:ListItem Value="5">MERGE</asp:ListItem>
                                                                <asp:ListItem Value="6">DELETE</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="SelectUsingKeyDiv" class="FLeft Width100">
                                                <div class="DbCmdRow">
                                                    <div class="DbConnLeftDiv">
                                                        Select Using
                                                    </div>
                                                    <div class="DbConnRightDiv SelectDiv">
                                                        <asp:DropDownList ID="ddlOdataCmd_UsingKey" runat="server" class="UseUniformCss"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlOdataCmd_UsingKey_Changed">
                                                            <asp:ListItem Value="-1">Select</asp:ListItem>
                                                            <asp:ListItem Value="0">Without Primary Key</asp:ListItem>
                                                            <asp:ListItem Value="1">Primary Key</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div id="ODataCmd_HttpType_GetDiv" class="FLeft Width100">
                                                <div class="DbCmdRow hide" id="ODataCmd_SingleValueReturnCheckDiv">
                                                    <div class="DbConnLeftDiv">
                                                        Return Single Value
                                                    </div>
                                                    <div class="DbConnRightDiv">
                                                        <asp:CheckBox ID="chkSingleValue" runat="server" onchange="chkSingleValueOnChange(this)"
                                                            Checked="false" />
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div id="ODataCmd_HttpType_SelectDiv" class="DbCmdRow hide">
                                                    <div class="DbConnLeftDiv">
                                                        Select
                                                    </div>
                                                    <div class="DbConnRightDiv">
                                                        <input id="btnAddOdataSelect" type="button" value="  Edit Properties  " class="InputStyle"
                                                            onclick="btnAddOdataSelectClick();" />
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div id="ODataCmd_HttpType_AdditionalPath" class="DbCmdRow">
                                                    <div class="DbConnLeftDiv">
                                                        Additional ResourcePath
                                                    </div>
                                                    <div class="DbConnRightDiv">
                                                        <asp:TextBox ID="txt_AdditionalResourcePath" runat="server" type="text" class="InputStyle"
                                                            Width="99%" />
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="clear">
                                                </div>
                                                <div id="ODataGetWithoutKeyDiv">
                                                    <div id="ODataCmd_HttpType_TopDiv" class="DbCmdRow">
                                                        <div class="DbConnLeftDiv">
                                                            Select Top
                                                        </div>
                                                        <div class="DbConnRightDiv">
                                                            <input id="txtOdataCmd_Top" type="text" class="InputStyle" style="width: 28%;" />
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                    <div id="ODataCmd_HttpType_SkipDiv" class="DbCmdRow">
                                                        <div class="DbConnLeftDiv">
                                                            Record To Skip
                                                        </div>
                                                        <div class="DbConnRightDiv">
                                                            <input id="txtOdataCmd_Skip" type="text" class="InputStyle" style="width: 28%;" />
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                    <div id="ODataCmd_HttpType_OrderByDiv" class="DbCmdRow">
                                                        <div class="DbConnLeftDiv">
                                                            Order By
                                                        </div>
                                                        <div class="DbConnRightDiv">
                                                            <input id="btnOrderOdataSelect" type="button" value="  Edit Properties  " class="InputStyle"
                                                                onclick="btnOrderOdataSelectClick();" />
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                    <div id="ODataCmd_HttpType_FilDiv" class="DbCmdRow">
                                                        <div class="DbConnLeftDiv">
                                                            Filter
                                                        </div>
                                                        <div class="DbConnRightDiv">
                                                            <asp:TextBox ID="txtOdataCmd_Filter" runat="server" Width="99%" class="InputStyle"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="ODataCmd_HttpType_ReturnDiv" class="DbCmdRow">
                                                <div class="DbConnLeftDiv">
                                                    Return Type
                                                </div>
                                                <div class="DbConnRightDiv SelectDiv">
                                                    <asp:DropDownList ID="ddlOdataCmd_Ent_ReturnTyp" runat="server" class="UseUniformCss"
                                                        onchange="ddlOdataCmd_Ent_ReturnTypChanged(this);">
                                                        <asp:ListItem Value="0">Single Record</asp:ListItem>
                                                        <asp:ListItem Value="1">Multiple Records</asp:ListItem>
                                                        <asp:ListItem Value="2">Single Value</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="ODataCmd_HttpType_DsPath" class="FLeft Width100">
                                                <div class="DbCmdRow">
                                                    <div class="DbConnLeftDiv">
                                                        Dataset Path(Plural)
                                                    </div>
                                                    <div class="DbConnRightDiv">
                                                        <asp:TextBox ID="txtOdataCmd_Ent_DsPath" runat="server" class="InputStyle" Width="99%"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="ODataCmd_HttpType_KeyLabelDiv" class="DbCmdRow">
                                                <div class="DbConnLeftDiv">
                                                    Keys
                                                </div>
                                                <div class="DbConnRightDiv">
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="ODataCmd_HttpType_KeyDiv" class="FLeft Width100 ODataPostBorderDiv">
                                                <div class="FLeft ODataPostOutDiv ODataPostMarginTopDiv">
                                                    <div class="ODataPostHeadDiv" align="center">
                                                        Keys</div>
                                                    <div class="ODataPostHeadDiv" align="center">
                                                        Value</div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div id="ODataCmd_HttpType_GetKeyInDiv" class="FLeft ODataPostDiv">
                                                </div>
                                                <br />
                                                <br />
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="ODataCmd_HttpType_PropDiv">
                                                <div class="DbCmdRow">
                                                    <div class="DbConnLeftDiv">
                                                        Properties
                                                    </div>
                                                    <div class="DbConnRightDiv">
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="FLeft Width100 ODataPostBorderDiv">
                                                    <div class="FLeft ODataPostOutDiv ODataPostMarginTopDiv">
                                                        <div class="ODataPostHeadDiv" align="center">
                                                            Properties</div>
                                                        <div class="ODataPostHeadDiv" align="center">
                                                            Value</div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                    <div id="ODataCmd_HttpType_PostInDiv" class="FLeft ODataPostDiv">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="divOdataFuncInputPara">
                                                <div class="DbConnLeftDiv">
                                                    Input Parameter
                                                </div>
                                                <div class="DbConnRightDiv">
                                                </div>
                                                <div class="clear">
                                                </div>
                                                <div class="FLeft Width100 ODataPostBorderDiv">
                                                    <div class="FLeft ODataPostOutDiv ODataPostMarginTopDiv">
                                                        <div class="ODataPostHeadDiv" align="center">
                                                            Parameter</div>
                                                        <div class="ODataPostHeadDiv" align="center">
                                                            Value</div>
                                                    </div>
                                                    <div class="clear">
                                                    </div>
                                                    <div id="ODataCmd_FuncType_InDiv" class="FLeft ODataPostDiv">
                                                    </div>
                                                    <br />
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                            <div id="outputPropDiv">
                                                <div class="DbConnLeftDiv">
                                                    Output Properties
                                                </div>
                                                <div class="DbConnRightDiv">
                                                    <input id="btnOutputProp" type="button" value="  Edit Properties  " class="InputStyle"
                                                        onclick="EditOdataCmdOutParam();" />
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div id="divDataCache">
                                        <div id="divDataCacheType" class="DbCmdRow">
                                            <div class="DbConnLeftDiv">
                                                Data Cache
                                            </div>
                                            <div class="DbConnRightDiv SelectDiv">
                                                <asp:DropDownList ID="ddl_DataCache_type" runat="server" class="UseUniformCss" onchange="ddl_DataCache_type_Changed(this);">
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                    <asp:ListItem Value="1">Yes (Absolute Time)</asp:ListItem>
                                                    <asp:ListItem Value="2">Yes (Relative Time)</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div id="divAbsoluteTime" class="hide">
                                            <div id="divAbsoluteCachePeriod" class="DbCmdRow">
                                                <div class="DbConnLeftDiv">
                                                    Cache Frequency
                                                </div>
                                                <div class="DbConnRightDiv SelectDiv">
                                                    <div style="float: left;">
                                                        <div id="divAbsoluteMinutes" class="hide">
                                                            <asp:DropDownList ID="ddl_AbsoluteMinutes" runat="server" class="UseUniformCss">
                                                                <asp:ListItem Value="0">0</asp:ListItem>
                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                                <asp:ListItem Value="45">45</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div id="divAbsoluteDay" class="hide">
                                                            <div style="float: left;">
                                                                <asp:DropDownList ID="ddl_AbsoluteDayHour" runat="server" class="UseUniformCss">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div style="float: left; margin-top: 10px;">
                                                                :</div>
                                                            <div style="float: left;">
                                                                <asp:DropDownList ID="ddl_AbsoluteDayMin" runat="server" class="UseUniformCss">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div id="divAbsoluteWeek" class="hide">
                                                            <asp:DropDownList ID="ddl_AbsoluteWeek" runat="server" class="UseUniformCss">
                                                                <asp:ListItem Value="1">MON</asp:ListItem>
                                                                <asp:ListItem Value="2">TUE</asp:ListItem>
                                                                <asp:ListItem Value="3">WED</asp:ListItem>
                                                                <asp:ListItem Value="4">THU</asp:ListItem>
                                                                <asp:ListItem Value="5">FRI</asp:ListItem>
                                                                <asp:ListItem Value="6">SAT</asp:ListItem>
                                                                <asp:ListItem Value="7">SUN</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div id="divAbsoluteDate" class="hide">
                                                            <asp:DropDownList ID="ddl_AbsoluteDate" runat="server" class="UseUniformCss">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div id="divAbsoluteYear" class="hide">
                                                            <div style="float: left;">
                                                                <asp:DropDownList ID="ddl_AbsoluteYear_Day" runat="server" class="UseUniformCss">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div style="float: left; margin-top: 10px;">
                                                                :</div>
                                                            <div style="float: left;">
                                                                <asp:DropDownList ID="ddl_AbsoluteYear_Month" runat="server" class="UseUniformCss">
                                                                    <asp:ListItem Value="1">January</asp:ListItem>
                                                                    <asp:ListItem Value="2">February</asp:ListItem>
                                                                    <asp:ListItem Value="3">March</asp:ListItem>
                                                                    <asp:ListItem Value="4">April</asp:ListItem>
                                                                    <asp:ListItem Value="5">May</asp:ListItem>
                                                                    <asp:ListItem Value="6">June</asp:ListItem>
                                                                    <asp:ListItem Value="7">July</asp:ListItem>
                                                                    <asp:ListItem Value="8">August</asp:ListItem>
                                                                    <asp:ListItem Value="9">September</asp:ListItem>
                                                                    <asp:ListItem Value="10">October</asp:ListItem>
                                                                    <asp:ListItem Value="11">November</asp:ListItem>
                                                                    <asp:ListItem Value="12">December</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="float: left;">
                                                        <asp:DropDownList ID="ddl_AbsoluteCachePeriod" runat="server" class="UseUniformCss"
                                                            onchange="ddl_AbsoluteCachePeriod_Changed(this);">
                                                            <asp:ListItem Value="0">Hour</asp:ListItem>
                                                            <asp:ListItem Value="1">Day(Hr./Min.)</asp:ListItem>
                                                            <asp:ListItem Value="2">Week</asp:ListItem>
                                                            <asp:ListItem Value="3">Month</asp:ListItem>
                                                            <asp:ListItem Value="4">Year(Day/Mnth)</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divRelativeTime" class="hide">
                                            <div id="divRelativeCachePeriod" class="DbCmdRow">
                                                <div class="DbConnLeftDiv">
                                                    Cache Frequency
                                                </div>
                                                <div class="DbConnRightDiv SelectDiv">
                                                    <div style="float: left;">
                                                        <div id="divRelativeMinute" class="hide">
                                                            <asp:DropDownList ID="ddl_RelativeMinute" runat="server" class="UseUniformCss">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div id="divRelativeHour" class="hide">
                                                            <asp:DropDownList ID="ddl_RelativeHour" runat="server" class="UseUniformCss">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div id="divRelativeDay" class="hide">
                                                            <asp:DropDownList ID="ddl_RelativeDay" runat="server" class="UseUniformCss">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div id="divRelativeWeek" class="hide">
                                                            <asp:DropDownList ID="ddl_RelativeWeek" runat="server" class="UseUniformCss">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div id="divRelativeMonth" class="hide">
                                                            <asp:DropDownList ID="ddl_RelativeMonth" runat="server" class="UseUniformCss">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div style="float: left;">
                                                        <asp:DropDownList ID="ddl_RelativeCachePeriod" runat="server" class="UseUniformCss"
                                                            onchange="ddl_RelativeCachePeriod_Changed(this);">
                                                            <asp:ListItem Value="0">Minute</asp:ListItem>
                                                            <asp:ListItem Value="1">Hour</asp:ListItem>
                                                            <asp:ListItem Value="2">Day</asp:ListItem>
                                                            <asp:ListItem Value="3">Week</asp:ListItem>
                                                            <asp:ListItem Value="4">Month</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                    <div class="SubProcborderDiv">
                                        <div class="SubProcBtnDiv" align="center">
                                            <div class="FLeft" style="margin-left: 40%;">
                                                <asp:UpdatePanel ID="updateSave" runat="server">
                                                    <ContentTemplate>
                                                        <asp:HiddenField ID="hdfCompleteData" runat="server" />
                                                        <asp:HiddenField ID="hdfSelectedData" runat="server" />
                                                        <asp:Button ID="btnDataObj_Save" runat="server" Text="  Save  " OnClick="btnOdataCmd_SaveClick"
                                                            OnClientClick="return CreateODataQueryUrl();" class="InputStyle" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="FLeft">
                                                <asp:Button ID="btnDataObj_Cancel" runat="server" CssClass="InputStyle hide" OnClientClick="cancelButtonClick();return false;"
                                                    Text="  Cancel  " />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div id="divCredentialBymobile" class="hide">
                            <asp:UpdatePanel ID="updatecredentialwithoutsave" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Literal ID="Literal1" Visible="false" runat="server"></asp:Literal>
                                    <div class="DbConnLeftDiv " id="div5">
                                        Username
                                    </div>
                                    <div class="DbConnRightDiv " style="padding-left: 12px" id="div6">
                                        <asp:TextBox ID="txtUsername" runat="server" Width="90%" class="InputStyle" MaxLength="50"></asp:TextBox>
                                    </div>
                                    <div class="clear">
                                    </div>
                                    <div class="DbConnLeftDiv" id="div8">
                                        Password
                                    </div>
                                    <div class="DbConnRightDiv " style="padding-left: 12px" id="div9">
                                        <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" Width="90%" class="InputStyle"
                                            MaxLength="50"></asp:TextBox>
                                    </div>
                                    <div class="clear">
                                    </div>
                                    <div class="SubProcborderDiv">
                                        <div class="SubProcBtnMrgn" align="center">
                                            <asp:Button ID="btnSaveCredential" runat="server" Text="  Next  " OnClientClick="return Credentialvalidation();"
                                                OnClick="btnSaveCredential_Click" class="InputStyle" />
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <asp:HiddenField ID="hdfDataObjectId" runat="server" />
                        <asp:HiddenField ID="hdfPreviousDataObjectId" runat="server" />
                        <asp:HiddenField ID="hdfDbCmdPara" runat="server" />
                        <asp:HiddenField ID="hdfInsertQueryPara" runat="server" />
                        <asp:HiddenField ID="hidIntellegence" runat="server" />
                        <asp:HiddenField ID="hidConnectionid" runat="server" />
                        <asp:HiddenField ID="hdfOdata_CmdId" runat="server" />
                        <asp:HiddenField ID="hdfOdata_HttpData" runat="server" />
                        <asp:HiddenField ID="hdfOdata_HttpQuery" runat="server" />
                        <asp:HiddenField ID="hdfOdata_InputParam" runat="server" />
                        <asp:HiddenField ID="hdfOdata_OutputParam" runat="server" />
                        <asp:HiddenField ID="hdfOdata_DataJson" runat="server" />
                        <asp:HiddenField ID="hdfOdata_PutKeys" runat="server" />
                        <asp:HiddenField ID="hdfOdata_FuncHType" runat="server" />
                        <asp:HiddenField ID="hdfOdata_FuncType" runat="server" />
                        <asp:HiddenField ID="ltCrd" runat="server" />
                        <asp:HiddenField ID="hidWsAuthencationMeta" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <%--</div>--%>
        </asp:Panel>
        <%--</section>--%>
    </div>
    <div>
        <asp:UpdatePanel ID="upd" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnHidden" runat="server" CssClass="hide" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="SubProcODataCmdAddPara" class="hide">
        <div>
            <div id="Div55" class="AddParameter">
                <div style="">
                    <div class="QPHeader">
                        <div class="QPTextHeader" align="center">
                            <div class="QPHeaderText">
                                Parameter
                            </div>
                        </div>
                        <div class="QPTypeHeader">
                            <div class="QPHeaderText" align="center">
                                Type
                            </div>
                        </div>
                        <div class="QPDelete">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div id="AddOdataParamDiv" class="QPContent">
                    </div>
                    <div class="clear">
                    </div>
                    <div class="QPContent">
                        <div class="QPContentRow">
                            <div class="QPContentText">
                                <input id="txtOdata_AddParam" type="text" class="txtQPOption" maxlength="30" />
                            </div>
                            <div class="QPContentType">
                                <select id="ddlOdata_AddParam" class="ddlQPOption">
                                    <option value="0">String</option>
                                    <option value="1">Decimal</option>
                                    <option value="2">Integer</option>
                                    <option value="3">Boolean</option>
                                </select>
                            </div>
                            <div class="QPContentDelete" align="center">
                                <img id="btnOdata_AddParam" alt="" src="//enterprise.mficient.com/images/add.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcAddODataSelect" class="hide">
        <div>
            <div id="Div53" class="AddParameter">
                <div style="">
                    <div id="ODataCmdHeadDiv" class="QPHeader">
                    </div>
                    <div class="clear">
                    </div>
                    <div id="ODataCmdContentDiv" class="QPContent">
                    </div>
                    <div class="clear">
                    </div>
                    <div id="ODataCmdAddDiv" class="QPContent">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcODataOutParam" class="hide">
        <div>
            <div id="Div56" runat="server" class="ProcCanvasAddDpOption" visible="true">
                <div class="AddDpOptionInner">
                    <div class="DpHeader">
                        <div class="DpTextHeader">
                            <div class="DpHeaderText">
                                Name
                            </div>
                        </div>
                        <div class="DpValueHeader">
                            <div class="DpHeaderText">
                                Path</div>
                        </div>
                        <div class="DpOptionHeaderDelete">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div id="OdataOutParamContent" class="DpContent">
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="btnSaveODataOutParam" type="button" value="  Save  " onclick="SaveOdataOutParam();"
                        class="InputStyle" />
                    <input id="btnCancelODataOutParam" type="button" value="  Cancel  " onclick="SubProcODataOutParam(false);"
                        class="InputStyle" />
                    <asp:HiddenField ID="HiddenField4" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubWsIntls" class="hide">
        <div id="SubWsInnrIntls" style="max-height: 200px; overflow-y: auto;">
        </div>
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <div id="SubProcBoxMessage">
        <div>
            <div class="MessageDiv">
                <a id="a2">Please check following points : </a>
                <br />
            </div>
            <div style="margin-top: 5px">
                <a id="aMessage"></a>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnDiv" align="center">
                    <input id="btnOkErrorMsg" type="button" value="  OK  " onclick="SubProcBoxMessage(false);"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="SubProcConfirmBoxMessage">
        <div>
            <div>
                <div class="ConfirmBoxMessage1">
                    <a id="aCFmessage"></a>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <input id="btnCnfObjectSave" type="button" value="   OK   " onclick="SubProcConfirmBoxMessage(false);"
                        class="InputStyle" />
                </div>
            </div>
        </div>
    </div>
    <div id="ODataDialogPara" class="hide">
        <div id="ShowODataCmdParaDiv">
        </div>
    </div>
    <div id="SubProcDeleteCmdConfrmation">
        <div>
            <div class="MarginT10">
            </div>
            <div class="MessageDiv">
                <a id="aMsgCmdDelCmf"></a>
            </div>
            <div class="MarginT10">
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <asp:UpdatePanel runat="server" ID="UpdDeleteCommand" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Button ID="btnDeleteObject" runat="server" Text="  Yes  " OnClientClick="SubProcDeleteCmdConfrmation(false,'');"
                                OnClick="lnkDelete_Click" CssClass="InputStyle" />
                            <input type="button" value=" No " onclick="SubProcDeleteCmdConfrmation(false,'');"
                                class="InputStyle" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <div id="dialogPara">
        <div id="ShowDnCmdParaDiv" style="max-height: 200px; overflow-y: scroll;">
        </div>
    </div>
    <div id="SubProobjectChange" class="hide">
        <asp:UpdatePanel ID="updatediscard" runat="server">
            <ContentTemplate>
                <div class="MessageDiv">
                    <a id="aFormChangeMsg" class="DefaultCursor">Discard any changes made? </a>
                </div>
                <div class="SubProcborderDiv" style="margin-top: 30px !important;">
                    <div class="SubProcBtnMrgn" align="center">
                        <input id="btnDiscard" type="button" value="  Yes  " class="InputStyle" onclick="discardChanges();" />
                        <%-- <asp:Button ID="btnDiscard" runat="server" Text="  Yes  " OnClientClick="return false;"
                            class="InputStyle" />--%>
                        <asp:Button ID="btnSaveCurrentObject" runat="server" class="InputStyle" Text="  No  "
                            OnClientClick="SubModifiction(false);" OnClick="btnSaveCurrentObject_SaveClick" />
                        <input id="btnCancelSaveCurrentForm" type="button" value="  Cancel " onclick="cancelDiscardChanges();"
                            class="InputStyle" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divODataCopyObject" class="hide" style="height: 100px">
        <div id="Div1">
            <div id="divcopysameobject" class="DbCmdRow">
                <div class="DbConnLeftDiv">
                    Object Name
                </div>
                <div class="DbConnRightDiv">
                    <asp:TextBox ID="txtOdataCopyObjectName" runat="server" class="InputStyle" Width="80%"></asp:TextBox>
                </div>
            </div>
            <div class="SubProcborderDiv">
                <div class="SubProcBtnMrgn" align="center">
                    <asp:UpdatePanel runat="server" ID="updSaveCopiedObject" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Button ID="btnSaveCopiedObject" runat="server" Text="  Yes  " OnClientClick="return validateObjectName();"
                                OnClick="btnSaveCopiedObject_Click" CssClass="InputStyle" />
                            <input type="button" value="  No  " onclick="showODataCopyObjectPopUp(false);" class="InputStyle" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <div id="dialogtestPara">
        <div style="margin-bottom: 15px !important" id="divdetails">
        </div>
    </div>
    <div id="dialogdynamictable">
        <div style="margin-bottom: 15px !important">
            <div id="Div7" class="AddParameter">
                <table id="resulttbl">
                </table>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);

        function application_init() {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>

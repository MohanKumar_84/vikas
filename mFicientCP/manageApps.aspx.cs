﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientCP
{
    public partial class manageApps : System.Web.UI.Page
    {
        string hfsValue = string.Empty;
        string hfbidValue = string.Empty;

        string strUserId = string.Empty;
        string strSessionId = string.Empty;
        string hfsPart3 = string.Empty;
        string hfsPart4 = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {

            HttpContext context = HttpContext.Current;

            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)mainMaster.FindControl("hfbid")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
                hfbidValue = ((HiddenField)previousPageForm.FindControl("hfbid")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue) || string.IsNullOrEmpty(hfbidValue)) return;

            context.Items["hfs"] = hfsValue;
            context.Items["hfbid"] = hfbidValue;

            string[] hfsParts = Utilities.DecryptString(hfsValue).Split(',');

            strUserId = hfsParts[0];
            strSessionId = hfsParts[1];
            hfsPart3 = hfsParts[2];
            hfsPart4 = hfsParts[3];

            context.Items["hfs1"] = strUserId;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = hfsPart3;
            context.Items["hfs4"] = hfsPart4;

            if (!Page.IsPostBack)
            {
                if (hfsValue != string.Empty)
                {
                    BindSubAdminsDropDown();
                    bindAppDetailsRpt(null);
                }

                Utilities.runPageStartUpScript(this.Page, "makeFieldsUniform", "$(\"input\").uniform();setOnClickEventOfRptChkBoxContSpan();");
            }
            else
            {
                if (Utilities.getPostBackControlName(this.Page).ToLower() == "lnk1".ToLower())
                {
                    if (hfsValue != string.Empty)
                    {
                        BindSubAdminsDropDown();
                        bindAppDetailsRpt(null);
                    }
                    Utilities.makeInputFieldsUniformIfMenuClickedAgain(Page, String.Empty);
                    return;
                }
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, String.Empty);
            }

        }
        protected void BindSubAdminsDropDown()
        {
            //GetSubAdminDetail getSubAdminDetails = new GetSubAdminDetail(true, false, hfsPart4, "", "", hfsPart3);
            //getSubAdminDetails.Process();
            GetSubAdminDetail getSubAdminDetails =
                new GetSubAdminDetail(hfsPart4, hfsPart3);
            getSubAdminDetails.getSubAdminWithFormModificationRole();
            if (getSubAdminDetails.StatusCode == 0)
            {
                if (getSubAdminDetails.ResultTable.Rows.Count > 0)
                {
                    ddlSubAdminList.DataSource = getSubAdminDetails.ResultTable;

                    ddlSubAdminList.DataValueField = "SUBADMIN_ID";
                    ddlSubAdminList.DataTextField = "FULL_NAME";
                    ddlSubAdminList.DataBind();
                    ddlSubAdminList.Items.Insert(0, new ListItem("All", "-1"));

                    BindPopupSubAdminsDropDown(getSubAdminDetails.ResultTable);
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, @"$.alert(" + "'" + "No sub admin added yet" + "'" + "," + "$.alert.Information" + ");showDialogImage(" + "DialogType.Info" + ");", true);
                }
            }
        }
        protected void BindPopupSubAdminsDropDown(DataTable subadminDtls)
        {
            ddlPopupSubAdminList.Items.Clear();
            ddlPopupSubAdminList.DataSource = subadminDtls;
            ddlPopupSubAdminList.DataValueField = "SUBADMIN_ID";
            ddlPopupSubAdminList.DataTextField = "FULL_NAME";
            ddlPopupSubAdminList.DataBind();
            ddlPopupSubAdminList.Items.Insert(0, new ListItem("Select", "-1"));
        }
        //protected void BindOtherSubAdminsDropDown()
        //{
        //    if (hfdSubAdminId.Value != "")
        //    {
        //        //GetSubAdminDetail getSubAdminDetails = new GetSubAdminDetail(true, false, hfsPart4, hfdSubAdminId.Value, "", hfsPart3);
        //        //getSubAdminDetails.GetOtherSubAdmins();
        //        GetSubAdminDetail getSubAdminDetails = new GetSubAdminDetail(hfsPart4, hfsPart3);
        //        getSubAdminDetails.GetOtherSubAdmins(hfdSubAdminId.Value);
        //        ddlPopupSubAdminList.Items.Clear();

        //        ddlPopupSubAdminList.DataSource = getSubAdminDetails.ResultTable;
        //        if (getSubAdminDetails.ResultTable != null)
        //        {
        //            ddlPopupSubAdminList.DataValueField = "SUBADMIN_ID";
        //            ddlPopupSubAdminList.DataTextField = "FULL_NAME";
        //            ddlPopupSubAdminList.DataBind();
        //            ddlPopupSubAdminList.Items.Insert(0, new ListItem("Select", "-1"));
        //        }
        //    }
        //}
        protected void bindAppDetailsRpt(UpdatePanel updPanel)
        {
            GetWFDetailWithVersion objWFDtlWithVersion;
            if (ddlSubAdminList.SelectedValue == "-1")
            {
                objWFDtlWithVersion = new GetWFDetailWithVersion(hfsPart3);
            }
            else
            {
                objWFDtlWithVersion = new GetWFDetailWithVersion(ddlSubAdminList.SelectedValue, hfsPart3);
            }
            objWFDtlWithVersion.Process();
            if (objWFDtlWithVersion.StatusCode == 0)
            {
                if (objWFDtlWithVersion.WfDetails.Rows.Count > 0)
                {
                    rptAppDetails.DataSource = objWFDtlWithVersion.WfDetails;
                    rptAppDetails.DataBind();
                    setTheHeaderOfRepeater("App list");
                    hideShowRepeater(false);
                    if (ddlSubAdminList.Items.Count > 2)//considering all and 1 subAdmin
                    {
                        showHideChangeAppsubAdminButton(true);
                    }
                    else
                    {
                        showHideChangeAppsubAdminButton(false);
                    }
                    showHideRepeaterContainerPanel(true);
                }
                else
                {
                    setTheHeaderOfRepeater("No app is added yet");
                    rptAppDetails.DataSource = null;
                    rptAppDetails.DataBind();
                    hideShowRepeater(true);
                    showHideChangeAppsubAdminButton(false);
                    showHideRepeaterContainerPanel(false);
                }
                
            }
            else
            {
                hideShowRepeater(false);
                showHideChangeAppsubAdminButton(false);
                showHideRepeaterContainerPanel(false);
                if (updPanel != null)
                    Utilities.showMessage("Internal server error", updPanel, DIALOG_TYPE.Error);
                else
                    Utilities.showMessageUsingPageStartUp(this.Page, "InternalError", "Internal server error", DIALOG_TYPE.Error);
            }
        }
        void hideShowRepeater(bool hide)
        {
            if (hide)
            {
                rptAppDetails.Visible = false;
            }
            else
            {
                rptAppDetails.Visible = true;
            }
        }
        protected void ddlSubAdminList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bindAppDetailsRpt(updRepeater);
            }
            catch
            {
                Utilities.showMessage("Internal server error.", updRepeater, DIALOG_TYPE.Error);
            }
        }
        void setTheHeaderOfRepeater(string headerInfo)
        {
            lblHeaderInfo.Text = "<h1>" + headerInfo + "</h1>";
        }
        protected void rptAppDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                //LinkButton lnkChangeSubadmin = (LinkButton)e.Item.FindControl("lnkChange");
                Label lblVersion = (Label)e.Item.FindControl("lblVersion");
                if (DataBinder.Eval(e.Item.DataItem, "VERSION") == DBNull.Value)
                    lblVersion.Text = "Not Published";
                else lblVersion.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "VERSION"));

                //if (ddlSubAdminList.Items.Count > 0) lnkChangeSubadmin.Visible = true;
                //else lnkChangeSubadmin.Visible = false;
            }
        }
        protected void rptAppDetails_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "change":
                    showSubAdminChangePopUp(e);
                    break;
            }
        }
        void showSubAdminChangePopUp(RepeaterCommandEventArgs e)
        {
            try
            {
                Label lblAppId = (Label)e.Item.FindControl("lblWFId");
                Label lblSubAdminId = (Label)e.Item.FindControl("lblSubAdminId");
                Label lblRptSubAdminName = (Label)e.Item.FindControl("lblSubAdmin");
                Label lblRptAppName = (Label)e.Item.FindControl("lblAppName");
                setHidAppIdForEdit(lblAppId.Text);
                setHidSubAdminIdForEdit(lblSubAdminId.Text);
                //lblSubAdminName.Text = lblRptSubAdminName.Text;
                lblAppName.Text = lblRptAppName.Text;
                //BindOtherSubAdminsDropDown();
                Utilities.showModalPopup("divModalContainer", updRepeater, "Change Subadmin", "400", false);
                updModalContainer.Update();
            }
            catch
            {
                Utilities.showMessage("Internal server error.", updRepeater, DIALOG_TYPE.Error);
            }
        }
        private void closeModalPopUp()
        {
            Utilities.closeModalPopUp("divModalContainer", updModalContainer);
        }

        protected void ClickedOne(object sender, EventArgs e)
        {
            //LinkButton lnkChange = (LinkButton)sender;
            //string str = Convert.ToString(lnkChange.CommandArgument);
            //modalPopUUpUserDetailContent.Visible = false;
            //modalPopUpSubAdminContent.Visible = true;
            //hfdSubAdminId.Value = str.Split(',')[0];
            //hfdUserId.Value = str.Split(',')[1];
            //lblUserName.Text = str.Split(',')[3];
            //lblSubAdminName.Text = str.Split(',')[2];
            //ddlSubAdmins.Items.Clear();
            //BindOtherSubAdminsDropDown();
            ////ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "ShowPopUp", @"showModalPopUp('divModalContainer','" + "Change SubAdmin" + "',400);", true);
            //Utilities.showModalPopup("divModalContainer", updRepeater, "Change Subadmin", "400", true);
        }

        protected void DetailClicked(object sender, EventArgs e)
        {
            //LinkButton lnkUser = (LinkButton)sender;
            //modalPopUUpUserDetailContent.Visible = true;
            //modalPopUpSubAdminContent.Visible = false;
            //string strCommangArg = lnkUser.CommandArgument;
            //ucUserDetails.UserId = strCommangArg.Split(',')[0];
            //ucUserDetails.UserName = strCommangArg.Split(',')[1];
            //ucUserDetails.rememberUserId();
            //ucUserDetails.getUserDetailsAndFillData(strCommangArg.Split(',')[0], hfsPart3);
            //ucUserDetails.FindControl("lnkEditUser").Visible = false;
            //ucUserDetails.FindControl("lnkBlockUser").Visible = false;
            //ucUserDetails.FindControl("lnkUnBlockUser").Visible = false;
            //Utilities.showModalPopup("divModalContainer", updRepeater, "User Detail", Convert.ToString(Utilities.getModalPopUpWidth(MODAL_POP_UP_NAME.USER_DETAIL)), true);
            //ScriptManager.RegisterClientScriptBlock(updRepeater, typeof(UpdatePanel), "HideSeparator", @"hideHTMLElements('ucSeparatorOne');", true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            closeModalPopUp();
        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<MFEWorkFlowDetail> lstSelectedApps = getWfDetailsFromRpt();
                if (ddlPopupSubAdminList.SelectedValue == "-1")
                {
                    //form comma separated list as this was filled using javascript at clints end
                    lblAppName.Text = "";
                    foreach (MFEWorkFlowDetail wfDtl in lstSelectedApps)
                    {
                        if (String.IsNullOrEmpty(lblAppName.Text))
                        {
                            lblAppName.Text += wfDtl.Name;
                        }
                        else
                        {
                            lblAppName.Text += ", " + wfDtl.Name;
                        }
                    }
                    Utilities.showAlert("Please select a subadmin", "divChangeSAPopUpError", updModalContainer, "Show Alert");
                    updModalContainer.Update();
                    return;
                }
                UpdateWFSubAdminId objUpdateWFSubAdminId = new UpdateWFSubAdminId(
                    false, hfsPart3, ddlPopupSubAdminList.SelectedValue,
                    lstSelectedApps);
                //if (rdlstAppsToChange.SelectedValue == "S")
                //{
                //    objUpdateWFSubAdminId = new UpdateWFSubAdminId(false, hfdAppId.Value,
                //                             hfsPart3, hfdSubAdminId.Value, ddlPopupSubAdminList.SelectedValue);
                //    //ChangeUserSubAdmin(hfdUserId.Value);
                //}
                //else
                //{
                //    objUpdateWFSubAdminId = new UpdateWFSubAdminId(true, String.Empty,
                //                             hfsPart3, hfdSubAdminId.Value, ddlPopupSubAdminList.SelectedValue);

                //}
                objUpdateWFSubAdminId.ProcessForMultipleAppIds();
                if (objUpdateWFSubAdminId.StatusCode == 0)
                {
                    bindAppDetailsRpt(updModalContainer);
                    Utilities.closeModalPopUp("divModalContainer", updModalContainer);
                    Utilities.showMessage("Apps subadmin changed successfully", updModalContainer, DIALOG_TYPE.Info);
                    clearHiddenAppIdForEdit();
                    clearHiddenSubAdminIdForEdit();
                    updModalContainer.Update();
                }
                else
                {
                    throw new Exception("Internal server error.");
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error.", updModalContainer, DIALOG_TYPE.Error);
            }
        }
        List<MFEWorkFlowDetail> getWfDetailsFromRpt()
        {
            List<MFEWorkFlowDetail> lstSelectedAppDtl = new List<MFEWorkFlowDetail>();
            foreach (RepeaterItem item in rptAppDetails.Items)
            {
                CheckBox chkAppSelect = (CheckBox)item.FindControl("chkSelectApp");
                Label lblAppId = (Label)item.FindControl("lblWFId");
                Label lblSubAdminId = (Label)item.FindControl("lblSubAdminId");
                Label lblAppName = (Label)item.FindControl("lblAppName");
                if (chkAppSelect.Checked)
                {
                    MFEWorkFlowDetail objAppDtl = new MFEWorkFlowDetail();
                    objAppDtl.WorkflowId = lblAppId.Text;
                    objAppDtl.SubAdminId = lblSubAdminId.Text;
                    objAppDtl.Name = lblAppName.Text;
                    lstSelectedAppDtl.Add(objAppDtl);
                }
            }
            return lstSelectedAppDtl;
        }

        void setHidSubAdminIdForEdit(string value)
        {
            hfdSubAdminId.Value = value;
        }
        void clearHiddenSubAdminIdForEdit()
        {
            setHidSubAdminIdForEdit(String.Empty);
        }
        void setHidAppIdForEdit(string value)
        {
            hfdAppId.Value = value;
        }
        void clearHiddenAppIdForEdit()
        {
            setHidAppIdForEdit(String.Empty);
        }
        void showHideChangeAppsubAdminButton(bool show)
        {
            if (show)
            {
                btnChangeSubAdmin.Visible = true;
            }
            else
            {
                btnChangeSubAdmin.Visible = false;
            }
        }
        void showHideRepeaterContainerPanel(bool show)
        {
            if (show)
            {
                pnlRptTableContainer.Visible = true;
            }
            else
            {
                pnlRptTableContainer.Visible = false;
            }
        }
    }
}
﻿<%@ Page Title="mFicient | Groups" Language="C#" MasterPageFile="~/master/Canvas.master" ClientIDMode="AutoID"
    AutoEventWireup="true" CodeBehind="groupList.aspx.cs" Inherits="mFicientCP.groupList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
  <script type="text/javascript">
      //this is used for the button used to transfer
      function removeFromGroup() {
          var divExistingUsers = $('#' + '<%=divExistingUsers.ClientID %>');
          var divOtherAvailableUsers = $('#' + '<%= divOtherAvailableUsers.ClientID%>');
          $(divExistingUsers).each(function (index) {
              $(this).children().each(function (index) {
                  var chkbox = $(this).find("input:checkbox");
                  if (chkbox.is(':checked')) {
                      var divToTransfer = this;
                      var divClearSibling = this.nextElementSibling;
                      chkbox.removeAttr('checked');
                      var chkBoxParent = $(chkbox).parent();
                      if ($(chkBoxParent).is("span")) {
                          chkBoxParent.removeAttr('class');
                      }
                      var anchorTag = $(this).find('a');
                      if (anchorTag) {
                          $(anchorTag).attr("onclick", "moveUserToExistingUser(this)");
                          $(anchorTag).attr("class", "add");
                      }
                      $(divOtherAvailableUsers).append(divToTransfer);
                      $(divOtherAvailableUsers).append(divClearSibling);
                  }
              });
          });
      }
      //this is used for the button used to transfer
      function addToGroup() {
          var divExistingUsers = $('#' + '<%=divExistingUsers.ClientID %>');
          var divOtherAvailableUsers = $('#' + '<%= divOtherAvailableUsers.ClientID%>');
          $(divOtherAvailableUsers).each(function (index) {
              $(this).children().each(function (index) {
                  var chkbox = $(this).find("input:checkbox");
                  if (chkbox.is(':checked')) {
                      chkbox.removeAttr('checked');
                      var chkBoxParent = $(chkbox).parent();
                      if ($(chkBoxParent).is("span")) {
                          chkBoxParent.removeAttr('class');
                      }
                      var anchorTag = $(this).find('a');
                      if (anchorTag) {
                          $(anchorTag).attr("onclick", "moveUserToOtherAvailableUser(this)");
                          $(anchorTag).attr("class", "remove");
                      }
                      $(divExistingUsers).append(this);
                      $(divExistingUsers).append(this.nextElementSibling);
                  }
              });
          });
      }
      //this is used for save button for final saving of the users
      function getExistingUsersForSaving() {
          var divExistingUsers = $('#' + '<%=divExistingUsers.ClientID %>');
          var hidExistingUsers = document.getElementById('<%=hidExistingUsers.ClientID %>');
          hidExistingUsers.value = "";
          $(divExistingUsers).each(function (index) {
              $(this).children().each(function (index) {
                  var anchorTag = $(this).children('a'); ;
                  if (!(anchorTag.length === 0)) {
                      if (hidExistingUsers) {
                          var aryAnchorTagValues = anchorTag.attr("id").split('|');
                          if (hidExistingUsers.value === "") {
                              hidExistingUsers.value = aryAnchorTagValues[1];
                          }
                          else {

                              hidExistingUsers.value += "|" + aryAnchorTagValues[1];
                          }
                      }
                  }
              });
          });
      }
      //this is used for save button for final saving of the users
      function getOtherAvailableUsersForSaving() {
          var divOtherAvailableUsers = $('#' + '<%= divOtherAvailableUsers.ClientID%>');
          var hidOtherExistingUsers = document.getElementById('<%=hidOtherAvailableUsers.ClientID %>');
          hidOtherExistingUsers.value = "";
          $(divOtherAvailableUsers).each(function (index) {
              $(this).children().each(function (index) {
                  var anchorTag = $(this).children('a'); ;
                  if (!(anchorTag.length === 0)) {
                      var aryAnchorTagValues = anchorTag.attr("id").split('|');
                      if (hidOtherExistingUsers) {
                          if (hidOtherExistingUsers.value === "") {

                              hidOtherExistingUsers.value = aryAnchorTagValues[1];
                          }
                          else {
                              hidOtherExistingUsers.value += "|" + aryAnchorTagValues[1];
                          }
                      }
                  }
              });
          });
      }
      //this is used in the a tag of the div
      function moveUserToOtherAvailableUser(anchorTag) {
          var divOtherAvailableUsers = $('#' + '<%= divOtherAvailableUsers.ClientID%>');
          var divToTransfer = anchorTag.parentElement;
          var divClearSibling = undefined;
          if (divToTransfer.nextElementSibling)
              divClearSibling = divToTransfer.nextElementSibling;
          $(anchorTag).attr("onclick", "moveUserToExistingUser(this)");
          $(anchorTag).attr("class", "add");
          var chkbox = $(divToTransfer).find('input:checkbox');
          var chkBoxParent = $(chkbox).parent();
          if ($(chkBoxParent).is("span")) {
              chkBoxParent.removeAttr('class');
          }
          $(divOtherAvailableUsers).append(divToTransfer);
          if (divClearSibling != undefined)
              $(divOtherAvailableUsers).append(divClearSibling);
      }
      //this is used in the a tag of the div
      function moveUserToExistingUser(anchorTag) {
          var divExistingUsers = $('#' + '<%=divExistingUsers.ClientID %>');
          var divToTransfer = anchorTag.parentElement;
          var divClearSibling = undefined;
          if (divToTransfer.nextElementSibling)
              divClearSibling = divToTransfer.nextElementSibling;
          $(anchorTag).attr("onclick", "moveUserToOtherAvailableUser(this)");
          $(anchorTag).attr("class", "remove");
          var chkbox = $(divToTransfer).find('input:checkbox');
          var chkBoxParent = $(chkbox).parent();
          if ($(chkBoxParent).is("span")) {
              chkBoxParent.removeAttr('class');
          }
          $(divExistingUsers).append(divToTransfer);
          if (divClearSibling != undefined)
              $(divExistingUsers).append(divClearSibling);
      }

      function clearGroupNameText() {
          $('#' + '<%=txtGroup.ClientID %>').val("");
      }
      function clearGroupNameText1(editBotton) {
          var v = $(editBotton).parent().parent().find("[id*='lblGroupName']");
          var v1 = $(editBotton).parent().parent().find("[id*='lblGroupId']");
          //alert($(v1).text());
          $('[id$=hidgroupname]').val($(v1).text());
          $('[id$=hidgroupid]').val($(v).text());
          $('#' + '<%=txtGroup1.ClientID %>').val($(v).text());
      }

      function processDeleteGroup() {
          var confirmReturn = confirm('Are you sure you want to delete the group');
          if (confirmReturn) {
              getExistingUsersForSaving();
              getOtherAvailableUsersForSaving();
              return true; //allow postback
          }
          else {
              return false; //prevent postback
          }
      }
      function processSaveGroup() {
          var valid = validateForSaving();
          if (valid) {
              getExistingUsersForSaving();
              getOtherAvailableUsersForSaving();
              existapps();
              return true;
          }
          else {
              return false;
          }
      }
      function validateForSaving() {
          var txtGroupName = $('#' + '<%=txtGroupName.ClientID %>');
          if ($(txtGroupName).val().trim() === "") {
              $.wl_Alert('Please enter a valid group name.</br>Min 3 Characters , No numbers are allowed.', 'info', '#divAddRemoveUsersError');
              return false;
          }
          else {
              return true;
          }
      }
      function storeSelectedTabIndex(index) {
          var hidSelectedTabIndex = $('[id$=hidTabSelected]');
          //hidSelectedTabIndex.value = '';
          $(hidSelectedTabIndex).val('');
          // hidSelectedTabIndex.value = index;
          $(hidSelectedTabIndex).val(index);
      }

      var TabEnum = {
          DEVICE: 1,
          DATA_COMMAND: 2,
          DATA_COMMAND1: 3

      }
      function makeTabAfterPostBack() {
          $('#content').find('div.tab').tabs({
              fx: {
                  opacity: 'toggle',
                  duration: 'fast'
              }
          });
      }

      function existuser() {
          var divExistingUsers = $('#' + '<%=divExistingUsers.ClientID %>');
          var myData = '';
          $(divExistingUsers).find('.spantesting').each(function (index) {
              if (myData == '') myData = $(this).html();
              else myData += ',' + $(this).html();
          });
          $('[id$=hidaddnewuseringroup]').val(myData);
      }
</script>

   <script type="text/javascript">
       function moveUserToExistingUser1(anchorTag) {
           var divExistingUsers = $('#' + '<%= divavilableapps.ClientID %>');
           var divToTransfer = anchorTag.parentElement;
           var divClearSibling = undefined;
           if (divToTransfer.nextElementSibling)
               divClearSibling = divToTransfer.nextElementSibling;
           $(anchorTag).attr("onclick", "moveUserToOtherAvailableUser1(this)");
           $(anchorTag).attr("class", "add");
           $(divExistingUsers).append(divToTransfer);
           if (divClearSibling != undefined)
               $(divExistingUsers).append(divClearSibling);
       }
       function moveUserToOtherAvailableUser1(anchorTag) {
           var divOtherAvailableUsers = $('#' + '<%= divexistingApps.ClientID%>');
           var divToTransfer = anchorTag.parentElement;
           var divClearSibling = undefined;
           if (divToTransfer.nextElementSibling)
               divClearSibling = divToTransfer.nextElementSibling;
           $(anchorTag).attr("onclick", "moveUserToExistingUser1(this)");
           $(anchorTag).attr("class", "remove");

           $(divOtherAvailableUsers).append(divToTransfer);
           if (divClearSibling != undefined)
               $(divOtherAvailableUsers).append(divClearSibling);

       }
       //this is used for save button for final saving of the users
       function getExistingAppForSaving() {
           var divExistingUsers = $('#' + '<%=divexistingApps.ClientID %>');
           var hidExistingUsers = document.getElementById('<%=hfappliedapps.ClientID %>');
           hidExistingUsers.value = "";
           $(divExistingUsers).each(function (index) {
               $(this).children().each(function (index) {
                   var anchorTag = $(this).children('a');
                   var wfId = $(this).children('[id^="@WF_ID"]').text();
                   var wfNm = $(this).children('[id^="@WF_NAME"]').text();
                   if (!(anchorTag.length === 0)) {
                       if (hidExistingUsers) {
                           var aryAnchorTagValues = anchorTag.attr("id").split('_');
                           if (hidExistingUsers.value === "") {
                               hidExistingUsers.value = wfId + "," + wfNm;
                           }
                           else {
                               hidExistingUsers.value += "|" + wfId + "," + wfNm;
                           }
                       }
                   }
               });
           });
       }
       //this is used for save button for final saving of the users
       //       function getOtherAvailableAppForSaving() {
       //           var divOtherAvailableUsers = $('#' + '<%= divavilableapps.ClientID%>');
       //           var hidOtherExistingUsers = document.getElementById('<%=hfavilableapps.ClientID %>');
       //           //existuser();
       //           hidOtherExistingUsers.value = "";
       //           $(divOtherAvailableUsers).each(function (index) {
       //               $(this).children().each(function (index) {
       //                   var anchorTag = $(this).children('a');
       //                   var wfId = $(this).children('[id^="@WF_ID"]').text();
       //                   var wfNm = $(this).children('[id^="@WF_NAME"]').text();

       //                   if (!(anchorTag.length === 0)) {
       //                       var aryAnchorTagValues = anchorTag.attr("id").split('_');
       //                       if (hidOtherExistingUsers) {
       //                           if (hidOtherExistingUsers.value === "") {
       //                               hidOtherExistingUsers.value = wfId + "," + wfNm;
       //                           }
       //                           else {
       //                               hidOtherExistingUsers.value += "|" + wfId + "," + wfNm;
       //                           }
       //                       }
       //                   }
       //               });
       //           });
       //       }

       function processSaveAppviaGroup() {
           getExistingAppForSaving();
           //getOtherAvailableAppForSaving();
           appmovesadd();
           return true;
       }

       function existapps() {
           var divExistingUsers = $('#' + '<%=divExistingUsers.ClientID %>');
           var myData = '';
           $(divExistingUsers).find('.spantesting').each(function (index) {
               if (myData == '') myData = $(this).html();
               else myData += ',' + $(this).html();
           });
           $('[id$=hidaddnewuseringroup]').val(myData);
       }

       function appmovesadd() {
           var divExistingUsers = $('#' + '<%=divexistingApps.ClientID %>');
           var myData = '';
           $(divExistingUsers).find('.spantestingdetails').each(function (index) {
               if (myData == '') myData = $(this).html();
               else myData += ',' + $(this).html();
           });
           $('[id$=hidgroupaddnewapps]').val(myData);
       }
   </script>
   <style type="text/css">
   .SubProcborderDiv{margin-top:26px;width:100%;float:left;clear:both;border-top:solid 1px #e0e0e0; padding-bottom:7px;}
		.SubProcBtnDiv{float:left;margin-top:15px; width:100%}
		.MenuLeftDiv{float:left;padding-top:3px;width:30%;margin-top:5px;cursor:default}
						.MenuCenterDiv{float:left;padding-top:5px;margin-top:5px;margin-right:5px}
		.InputStyle{
	width:99%;
	border:1px solid;
	padding:4px 2px;
	padding-bottom:4px;

	margin:0 1px;
	border-color: #bbbbbb;
	-webkit-border-radius:4px;
	-moz-border-radius:4px;
	border-radius:4px;
	background-color: #ffffff;
}
.MenuRightDiv{float:left;width:60%;margin-top:5px}
   
   </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <div style="margin: auto; width: 500px;">
                    <asp:UpdatePanel ID="updRepeater" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlGroupRptHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblGroupRptHeader" runat="server" Text="<h1>Group List </h1>"></asp:Label>
                                        </div>
                                        <div style="position: relative; top: 10px; right: 15px;">
                                            <asp:LinkButton ID="btnAddGroup" runat="server" Text="Add" CssClass="repeaterLink fr"
                                                OnClientClick="clearGroupNameText();showModalPopUp('AddEditModel','Add Group','355',false);return false;"></asp:LinkButton>
                                        </div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptGroupDtls" runat="server" Visible="true" 
                                    onitemcommand="rptGroupDtls_ItemCommand">
                                    <HeaderTemplate>
                                        <table class="repeaterTable rowClickable rptWithoutImageAndButton" style="width: 98%">
                                            <thead>
                                                <tr>
                                                    <th style="display: none">
                                                        GROUP ID
                                                    </th>
                                                    <th>
                                                        Group Name
                                                    </th>
                                                    <th>
                                                        No Of Users
                                                    </th>
                                                    <th>
                                                    
                                                    </th>

                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem" >
                                                <td style="display: none" title="id">
                                                    <asp:Label ID="lblGroupId" runat="server" Text='<%#Eval("GROUP_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblGrpName" runat="server" Text='<%# Eval("GROUP_NAME") %>'></asp:Label>
                                                    <asp:Label ID="lblSubAdminId" runat="server" Text='<%# Eval("SUBADMIN_ID") %>'></asp:Label>
                                                </td>
                                                <td onclick="rptTableCellClick(this,0);">
                                                    <asp:Label ID="lblGroupName" runat="server" Text='<%# Eval("GROUP_NAME") %>'></asp:Label>
                                                </td>
                                                <td onclick="rptTableCellClick(this,0) ;">
                                                    <asp:Label ID="lblUsersCount" runat="server" Text='<%# Eval("USER_COUNT") %>'></asp:Label>
                                                </td>
                                                <td style="width: 50px">
                                                          <a id="aedit" onclick="clearGroupNameText1(this);showModalPopUp('UpdateEditModel','Update Group','355',false);return false;"><span>
                                                      <img src="css/images/pencil16x16.png" alt="" /></span></a>  &nbsp;
                                                                                    <asp:LinkButton ID="lnkdeletecategory"  runat="server" CommandName="Delete" ToolTip="Delete Groups"
                                                                                        OnClientClick="isCookieCleanUpRequired('false');if (!confirm('Are you sure you want to sure Delete Groups?')) return false;">
                                                                                           <img src="images/Cross1.png" alt=""  />
                                                                                    </asp:LinkButton>
                                                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                        <tr class="repeaterItem">
                                                <td style="display: none" title="id">
                                                    <asp:Label ID="lblGroupId" runat="server" Text='<%#Eval("GROUP_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblGrpName" runat="server" Text='<%# Eval("GROUP_NAME") %>'></asp:Label>
                                                    <asp:Label ID="lblSubAdminId" runat="server" Text='<%# Eval("SUBADMIN_ID") %>'></asp:Label>
                                                </td>
                                                <td onclick="rptTableCellClick(this,0);">
                                                    <asp:Label ID="lblGroupName" runat="server" Text='<%# Eval("GROUP_NAME") %>'></asp:Label>
                                                </td>
                                                <td onclick="rptTableCellClick(this,0);">
                                                    <asp:Label ID="lblUsersCount" runat="server" Text='<%# Eval("USER_COUNT") %>'></asp:Label>
                                                </td>
                                                 <td style="width: 50px">
                                                    <a id="aedit" onclick="clearGroupNameText1(this);showModalPopUp('UpdateEditModel','Update Group','500',false);return false;";><span>
                                                        <img src="css/images/pencil16x16.png"  alt="" /></span></a> &nbsp;


                                                       <asp:LinkButton ID="lnkdeletecategory" runat="server" CommandName="Delete"   ToolTip="Delete Groups"
                                                                OnClientClick="isCookieCleanUpRequired('false');if (!confirm('Are you sure you want to sure Delete Groups?')) return false;">
                                                             <span>
                                                             <img src="images/Cross1.png" alt=""  />
                                                                    <span>
                                                                                    </asp:LinkButton>
                                                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                            <div class="clear">
                            </div>
                            <asp:Panel ID="pnlEditGroup" runat="server" Visible="false">
                            </asp:Panel>
                            <div id="updRepeaterHidVal">
                                <asp:HiddenField ID="hidExistingUsers" runat="server" />
                                <asp:HiddenField ID="hidOtherAvailableUsers" runat="server" />
                                <asp:HiddenField ID="hfdGroupId" runat="server" />
                                <asp:HiddenField ID="hidRowValuesUsedAfterPostBack" runat="server" />
                                <asp:HiddenField ID="hidGrpSubAdminId" runat="server" />
                                <asp:HiddenField ID="hidInitialGrpNameInEditMode" runat="server" />
                                <asp:HiddenField ID="hidAllUsersListInCmp" runat="server" />
                               <asp:HiddenField ID="hidgroupname" runat="server" />
                               <asp:HiddenField ID="hidgroupid" runat="server" />

                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </section>
    </div>
   
    <div id="AddEditModel" style="display: none;">
        <asp:UpdatePanel runat="server" ID="upAddEditModel" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divGroupAdd" class="ProcImgUpd">
                    <br />
                    <br />
                    <div>
                    <div class="MenuLeftDiv" style="padding-left: 10px;">
                        <div style="font-size: 14px">
                            Group Name</div>
                    </div>
                    <div class="MenuCenterDiv">
                        :</div>
                        <div class="MenuRightDiv">
                          
                            <asp:TextBox ID="txtGroup" runat="server" Width="100%" MaxLength="50"></asp:TextBox>

                        </div>
                       
                        <div class="SubProcborderDiv">
                        <div class="SubProcBtnDiv" align="center">
                        <asp:Button ID="btncancelgroupupdate" runat="server" Text="Cancel" OnClientClick="$('#AddEditModel').dialog('close');return false;"
                            CssClass="InputStyle" />
                       
                        <asp:Button ID="btnUpdateGroupName" CssClass="InputStyle" runat="server" Text="Update" OnClick="btnUpdateGroupName_Click" />&nbsp;&nbsp;
                        </div>
                        </div>
                    </div>
                    <br />
                    <br />
                </div>
                <div id="Div3">
                </div>
                <br />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
   
    <div id="UpdateEditModel" style="display: none;">
        <asp:UpdatePanel runat="server" ID="UpdateEditModel1" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="div1" class="ProcImgUpd">
                    <br />
                    <br />
                    <div>
                    <div class="MenuLeftDiv" style="padding-left: 10px;">
                        <div style="font-size: 14px">
                            Group Name</div>
                    </div>
                    <div class="MenuCenterDiv">
                        :</div>
						
                   
                        <div class="MenuRightDiv">
                          
                            <asp:TextBox ID="txtGroup1" runat="server" Width="100%" MaxLength="50"></asp:TextBox>

                        </div>
                        <div class="SubProcborderDiv">
                        <div class="SubProcBtnDiv" align="center">
                       <asp:Button ID="btncancelgroup" runat="server" Text="Cancel" OnClientClick="$('#UpdateEditModel').dialog('close');return false;"
                            CssClass="InputStyle" />
                           
                     <asp:Button ID="btnUpdateGroupName1" CssClass="InputStyle" runat="server" Text="Update" OnClick="btnUpdateGroupName1_Click" />&nbsp;&nbsp;
                     </div></div>
                    </div>
                    <br />
                    <br />
                </div>
                <div id="Div2">
                </div>
                <br />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
  
    
    
    <div id="divAddRemoveUsers" style="display: none;">
        <div id="divTab" class="tab">
            <ul id="tabList">
                <li id="lsttabCategory"><a id="lnktabCategory" href="#DeviceSetting">Users</a></li>
                <li id="lstTabUsersGroups"><a id="lnkDataCommandSetting" href="#DataCommandSettingsDiv">
                    Apps</a></li>
            </ul>
            <div id="DeviceSetting">
                <asp:UpdatePanel runat="server" ID="updAddRemoveUsers" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="clear" style="height: 0px;">
                        </div>
                        <div style="margin-bottom: 10px; display: none">
                            <div style="float: left; margin-right: 10px; margin-left: 30px; position: relative;
                                top: 5px;">
                                <asp:Label ID="lblGroupName" runat="server" Text="Name :"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtGroupName" runat="server" Width="250px" MaxLength="50"></asp:TextBox>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div style="margin-top: 10px; margin-left: 45px; margin-bottom: 5px;">
                            <div class="clear">
                            </div>
                            <asp:Label ID="lblUsersLabel" runat="server" Text="Select Users :"></asp:Label>
                        </div>
                        <div style="margin-bottom: 20px;">
                            <div class="comboselectbox searchable" style="height: 250px; margin-left: 30px;">
                                <div class="combowrap" style="margin-left:12px;">
                                    <div id="divOtherAvailableUsers" class="comboRepeater" runat="server">
                                        <div class="comboHeader">
                                            <h5>
                                                Other Users</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="comboselectbuttons">
                                    <div style="margin-top: 100px;">
                                    </div>
                                </div>
                                <div class="combowrap">
                                    <div id="divExistingUsers" class="comboRepeater" runat="server">
                                        <div class="comboHeader">
                                            <h5>
                                                Users in Group</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 10px">
                            <div style="text-align: center">
                              <asp:HiddenField ID="hidexistinguseringroup" runat="server" />
                             <asp:Button ID="btnCanceluseringroup" runat="server" Text="Cancel" OnClientClick="$('#divAddRemoveUsers').dialog('close');return false;" />&nbsp;&nbsp;
                                <asp:Button ID="btnSaveGroup" runat="server" Text="Apply" OnClick="btnSave_Click" OnClientClick="return processSaveGroup();" />&nbsp;&nbsp;
                               
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="DataCommandSettingsDiv">
              <div id="SubProcMenuCategory" class="hide">
       
            <asp:UpdatePanel runat="server" ID="updSubProcCat" UpdateMode="Conditional">
                <ContentTemplate>
                   
                        <div class="clear">
                        </div>
                        <div id="divAddRemoveUsersDelete" style="float: right">
                        </div>
                        <div class="clear" style="height: 0px;">
                        </div>
                        <div id="divAddRemoveUsersError">
                        </div>
                         <div style="margin-top: 10px; margin-left: 45px; margin-bottom: 5px;">
                            <div class="clear">
                            </div>
                            <asp:Label ID="Label3" runat="server" Text="Select Apps :"></asp:Label>
                        </div>
                        <div style="margin-bottom: 20px;">
                            <div class="comboselectbox searchable" style="height: 250px; margin-left: 30px;">
                                <div class="combowrap" style="margin-left:12px;">
                                    <div id="divavilableapps" class="comboRepeater" runat="server">
                                        <div class="comboHeader">
                                            <h5>
                                                Available Apps</h5> 
                                              
                                        </div>
                                    </div>
                                </div>
                                <div class="comboselectbuttons">
                                  
                                </div>
                                <div class="combowrap">
                                    <div id="divexistingApps" class="comboRepeater" runat="server">
                                        <div class="comboHeader">
                                            <h5>
                                                Existing Apps</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                        <div style="margin-bottom: 10px">
                            <div style="text-align: center">
                                <asp:Button ID="btncancelappsupdation" runat="server" Text="Cancel" OnClientClick="$('#divAddRemoveUsers').dialog('close');return false;" />&nbsp;&nbsp;
                                <asp:Button ID="btnsaveappsupdation" runat="server" Text="Apply" OnClick="btnSavegroup_Click" OnClientClick="return processSaveAppviaGroup();" />&nbsp;&nbsp;
                               <asp:HiddenField ID="hidgroupaexistapp" runat="server" />
                                <asp:HiddenField ID="hidgroupaddnewapps" runat="server" />
                               </div>
                            </div>
                        </div>
                   
                </ContentTemplate>
            </asp:UpdatePanel>
           
            <asp:HiddenField ID="hidaddnewuseringroup" runat="server" />
        </div>
  
            </div>
        </div>
        
    </div>
    <div>
        <asp:HiddenField ID="hidTabSelected" runat="server" />
        <asp:HiddenField ID="hgorupid" runat="server" />
        <asp:HiddenField ID="hfappliedgroup" runat="server" />
        <asp:HiddenField ID="hfavilablegroup" runat="server" />
        <asp:HiddenField ID="hfappliedapps" runat="server" />
        <asp:HiddenField ID="hfavilableapps" runat="server" />
        <asp:HiddenField ID="hidGrpIdInEditMode" runat="server" />
        
    </div>
    <div>
        <asp:UpdatePanel runat="server" ID="UpdatePanel11" UpdateMode="Conditional">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRowClickPostBack" EventName="Click" />
            </Triggers>
            <ContentTemplate>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    <div>
        <asp:Button ID="btnRowClickPostBack" runat="server" OnClick="btnRowClickPostBack_Click"
            Width="1px" Height="1px" Style="display: none;" />
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
            isCookieCleanUpRequired('false');
        }
        function prm_endRequest() {
            hideWaitModal();
            isCookieCleanUpRequired('true');
        }
    </script>
</asp:Content>

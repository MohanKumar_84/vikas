﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="mFicientWSTester._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>mFicient - Web Service Tester</title>
    <link rel="Stylesheet" href="~/css/main.css" type="text/css" />
</head>
<body>
    <script src='<%= Page.ResolveClientUrl("~/Scripts/encoder.js") %>' type="text/javascript"></script>
    <div id="PgCanvas">
        <div class="Sizer">
            <div class="Expander">
                <div class="Wrapper">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server" />
                        <div>
                            <h2 class="FrmHead">Web Service Tester</h2>
                            <div class="FrmFldDiv FrmFldDivNL">
                                <div class="FrmFldDiv FrmFldDivNL">
                                    <asp:UpdatePanel runat="server" ID="WebServicesPanel" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlWebServices" runat="server" AutoPostBack="true" CssClass="FrmFld" onselectedindexchanged="ddlWebServices_SelectedIndexChanged" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="FrmFldDiv FrmFldDivNL">
                                <div class="FrmFldDiv FrmFldDivNL">
                                    <asp:Label ID="lblRequest" runat="server" Text="Request XML" /><br />
                                    <asp:UpdatePanel runat="server" ID="RequestXmlPanel" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtRequest" runat="server" CssClass="FrmFld" Width="400px" Height="300px" TextMode="MultiLine" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlWebServices" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="FrmFldDiv">
                                    <asp:Label ID="lblResponse" runat="server" Text="Response XML" /><br />
                                    <asp:UpdatePanel runat="server" ID="ResponseXmlPanel" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtResponse" runat="server" CssClass="FrmFld" Width="400px" Height="300px" TextMode="MultiLine" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlWebServices" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="FrmFldDiv FrmFldDivNL">
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Button ID="btnSubmit" runat="server" Width="200px" Text="::SUBMIT::" 
                                                onclick="btnSubmit_Click" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <br />
                            <div class="FrmFldDiv FrmFldDivNL">
                                    <asp:Label ID="Label1" runat="server" Text="<b>Encrypt Data</b> : Enter Data and key in following Textbox" /><br /><br />
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtData" runat="server" CssClass="FrmFld" />
                                            <asp:TextBox ID="txtKey" runat="server" CssClass="FrmFld"  />                                            
                                            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="Button1" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    
                                </div>
                                <div class="FrmFldDiv FrmFldDivNL">
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel3" UpdateMode="Conditional">
                                        <ContentTemplate>                                        
                                            <asp:Button ID="Button1" runat="server" Width="200px" Text="::ENCRYTP::" 
                                                onclick="Button1_Click"  />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                        </div>                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.IO;
using System.Text;
using System.Net;
using System.Security.Cryptography;

namespace mFicientWSTester
{
    public partial class _Default : System.Web.UI.Page
    {
        private static string getRandomIV(int length)
        {
            string ALLOWED_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random random = new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++)
                sb.Append(ALLOWED_CHARACTERS[random.Next(ALLOWED_CHARACTERS.Length)]);
            return sb.ToString();
        }

        public  string GetMd5Hash(string _inputString)
        {
            string strMD5;
            byte[] hashedDataBytes;
            try
            {
                byte[] bs = Encoding.UTF8.GetBytes(_inputString);
                System.Security.Cryptography.MD5CryptoServiceProvider md5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
                hashedDataBytes = md5Hasher.ComputeHash(bs);
                strMD5 = BitConverter.ToString(hashedDataBytes).Replace("-", "");
            }
            catch
            {
                strMD5 = string.Empty;
            }
            return strMD5;
        }
        public  string AESEncrypt(string key, string _InputString)
        {
            try
            {
                string strKeyHash = GetMd5Hash(key).ToLower();
                byte[] ptbytes = Encoding.UTF8.GetBytes(_InputString);
                string strIV = getRandomIV(16);

                MemoryStream ms = new MemoryStream();
                AesManaged AES = new AesManaged();

                AES.KeySize = 128;
                AES.BlockSize = 128;
                AES.Mode = CipherMode.CBC;
                AES.Padding = PaddingMode.PKCS7;

                AES.Key = Encoding.UTF8.GetBytes(strKeyHash);
                AES.IV = Encoding.UTF8.GetBytes(strIV);

                CryptoStream cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write);
                cs.Write(ptbytes, 0, ptbytes.Length);
                cs.Close();

                byte[] ct = ms.ToArray();
                AES.Clear();
                return strIV + Convert.ToBase64String(ct, Base64FormattingOptions.None);
            }
            catch (Exception ex)
            {
                //throw ex;
                return "";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ddlWebServices.Items.Add(new ListItem("--Select Web Service--", ""));

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(Server.MapPath(@"~/xml/WebServiceRequests") + "\\Request.xml");

                XmlNode xmlNodeData = xmlDoc.SelectSingleNode(@"//req");
                foreach (XmlNode xmlNodeTrip in xmlNodeData.ChildNodes)
                {
                    ddlWebServices.Items.Add(new ListItem(xmlNodeTrip.Attributes["name"].Value.ToString(), xmlNodeTrip.Attributes["id"].Value.ToString()));
                }

            }
        }

        protected void ddlWebServices_SelectedIndexChanged(object sender, EventArgs e)
        {
            string xmlFunctionId = ddlWebServices.SelectedValue;
            txtRequest.Text = string.Empty;

            if (xmlFunctionId == "")
            {
                return;
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Server.MapPath(@"~/xml/WebServiceRequests") + "\\Request.xml");

            XmlNode xmlNodeData = xmlDoc.SelectSingleNode(@"//req");
            foreach (XmlNode xmlNodeTrip in xmlNodeData.ChildNodes)
            {
                if (xmlNodeTrip.Attributes["id"].Value.ToString() == xmlFunctionId)
                {
                    txtRequest.Text = xmlNodeTrip.InnerText.Trim();
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string xmlData = HttpUtility.UrlEncode(txtRequest.Text);
            HTTP oHttp = new HTTP(@"http://localhost:50995/?d=" + xmlData);
            //HTTP oHttp = new HTTP(@"https://vishnu.mficient.net/default.aspx?d=" + xmlData);
            oHttp.HttpRequestMethod = HTTP.EnumHttpMethod.HTTP_GET;

            HttpResponseStatus oResponse = oHttp.Request();

            if (oResponse.StatusCode == HttpStatusCode.OK || oResponse.StatusCode == HttpStatusCode.Accepted)
            {
                txtResponse.Text = oResponse.ResponseText;
            }
            else
            {
                txtResponse.Text = oResponse.ErrorMessage;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Literal1.Text = AESEncrypt(txtKey.Text,txtData.Text);
        }
    }
}
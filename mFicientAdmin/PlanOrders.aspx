﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="PlanOrders.aspx.cs" Inherits="mFicientAdmin.PlanOrders" %>

<%@ Register TagPrefix="cmpDetails" TagName="Details" Src="~/UserControl/UCCmpDetails.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script type="text/javascript">
        function calculatePrice(sender) {

            //got the discount
            var txtPurchaseDiscount = document.getElementById('<%=txtPurchaseDiscountAmt.ClientID %>');
            var txtAddUserDiscount = document.getElementById('<%=txtAddUserDiscountAmt.ClientID %>');
            var txtRenewDiscount = document.getElementById('<%=txtRenewDiscountAmt.ClientID %>');
            var strPriceWithoutDiscount, strPayableAmount, lblPriceWithoutDiscount, lblPayableAmount, fltDiscount, txtCommonDiscountText;
            var hidPriceAmountPayable = document.getElementById('<%=hidPriceAmountPayable.ClientID %>');
            //get the controls to work with.Price without discount and payable amount
            if (sender == txtPurchaseDiscount) {
                lblPriceWithoutDiscount = document.getElementById('<%=lblPurchaseOrgPrice.ClientID %>');
                lblPayableAmount = document.getElementById('<%=lblPurchasePayableAmount.ClientID %>');
                fltDiscount = parseFloat(txtPurchaseDiscount.value);
                txtCommonDiscountText = sender;
            }
            else if (sender == txtAddUserDiscount) {
                lblPriceWithoutDiscount = document.getElementById('<%=lblAddUserOrgPrice.ClientID %>');
                lblPayableAmount = document.getElementById('<%=lblAddUserPayableAmount.ClientID %>');
                fltDiscount = parseFloat(txtAddUserDiscount.value);
                txtCommonDiscountText = sender;
            }
            else if (sender == txtRenewDiscount) {
                lblPriceWithoutDiscount = document.getElementById('<%=lblRenewOrgPrice.ClientID %>');
                lblPayableAmount = document.getElementById('<%=lblRenewPayableAmount.ClientID %>');
                fltDiscount = parseFloat(txtRenewDiscount.value);
                txtCommonDiscountText = sender;
            }
            strPriceWithoutDiscount = lblPriceWithoutDiscount.innerHTML;
            strPayableAmount = lblPayableAmount.innerHTML;

            hidPriceAmountPayable.value = '';

            if (isNaN(fltDiscount)) {
                lblPayableAmount.innerHTML = lblPriceWithoutDiscount.innerHTML;
                hidPriceAmountPayable.value = lblPrice.innerHTML;
                txtCommonDiscountText.value = "0";
            }
            else {
                var dblPriceWithoutDiscount = parseFloat(strPriceWithoutDiscount);
                if (fltDiscount > dblPriceWithoutDiscount) {
                    $.alert("Discount amount exceeds the Price.Please enter valid discount amount", $.alert.Error); showDialogImage(DialogType.Error);
                    txtCommonDiscountText.value = "0";
                    lblPayableAmount.innerHTML = lblPriceWithoutDiscount.innerHTML;
                    return;
                }
                var dblPayableAmt = dblPriceWithoutDiscount - parseFloat(fltDiscount);
                lblPayableAmount.innerHTML = dblPayableAmt.toFixed(2);
                //set attribute did not worked properly
                //lblPrice.setAttribute('Text', value);
                hidPriceAmountPayable.value = lblPayableAmount.innerHTML;
            }

        }
        function getPayableAmount(priceWithoutDiscount, discountAmt) {
            if (isNaN(priceWithoutDiscount) || isNaN(discountAmt)) {
                return;
            }
            else {
                return (parseFloat(priceWithoutDiscount) - parseFloat(discountAmt));
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="upd" runat="server">
        <ContentTemplate>
            <div id="PageCanvasContent">
                <div id="divRepeater">
                    <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                        <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                            <div>
                                <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>Plan Orders</h1>"></asp:Label>
                            </div>
                            <div style="position: relative; top: 10px; right: 15px;">
                            </div>
                            <div style="height: 0px; clear: both">
                            </div>
                        </asp:Panel>
                        <asp:Repeater ID="rptPlanOrders" runat="server" OnItemCommand="rptPlanOrders_ItemCommand">
                            <HeaderTemplate>
                                <table class="repeaterTable">
                                    <thead>
                                        <tr>
                                            <th>
                                                COMPANY
                                            </th>
                                            <th>
                                                CURRENT PLAN
                                            </th>
                                            <th>
                                                ORDER
                                            </th>
                                        </tr>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tbody>
                                    <tr class="repeaterItem">
                                        <td>
                                            <asp:Label ID="lblCompID" runat="server" Text='<%# Eval("COMPANY_ID") %>' Visible="false"></asp:Label>
                                            <asp:LinkButton ID="lnkCompName" runat="server" Text='<%# Eval("COMPANY_NAME") %>'
                                                CommandName="CompanyDetail" ToolTip="Details"></asp:LinkButton>
                                        </td>
                                        <td>
                                            <%--<asp:Label ID="lblPlanCode" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>--%>
                                            <asp:LinkButton ID="lnkPlanName" runat="server" Text='<%# Eval("CURRENT_PLAN_NAME") %>'
                                                CommandName="PlanDetail" CommandArgument='<%#Eval("CURRENT_PLAN_ID") %>'></asp:LinkButton>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPlanCode" runat="server" Text='<%# Eval("PLAN_CODE") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblRequestType" runat="server" Text='<%# Eval("REQUEST_TYPE") %>'
                                                Visible="false"></asp:Label>
                                            <asp:LinkButton ID="lnkOrder" runat="server" Text='<%# Eval("ORDER") %>' CommandName="PlanOrder"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </tbody>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tbody>
                                    <tr class="repeaterAlternatingItem">
                                        <td>
                                            <asp:Label ID="lblCompID" runat="server" Text='<%# Eval("COMPANY_ID") %>' Visible="false"></asp:Label>
                                            <asp:LinkButton ID="lnkCompName" runat="server" Text='<%# Eval("COMPANY_NAME") %>'
                                                CommandName="CompanyDetail" ToolTip="Details"></asp:LinkButton>
                                        </td>
                                        <td>
                                            <%--<asp:Label ID="lblPlanCode" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>--%>
                                            <asp:LinkButton ID="lnkPlanName" runat="server" Text='<%# Eval("CURRENT_PLAN_NAME") %>'
                                                CommandName="PlanDetail" CommandArgument='<%#Eval("CURRENT_PLAN_ID") %>'></asp:LinkButton>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPlanCode" runat="server" Text='<%# Eval("PLAN_CODE") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblRequestType" runat="server" Text='<%# Eval("REQUEST_TYPE") %>'
                                                Visible="false"></asp:Label>
                                            <asp:LinkButton ID="lnkOrder" runat="server" Text='<%# Eval("ORDER") %>' CommandName="PlanOrder"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </tbody>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divPlanDetailBox" class="modalPopUpDetails">
        <div id="divPlanDetailsModal" class="container">
            <asp:UpdatePanel runat="server" ID="upPlanDtlsModal" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divPlanDetails" style="width: 100%;">
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label8" runat="server" Text="Plan Name"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblPlanName" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label2" runat="server" Text="Max WorkFlow"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblMaxWorkflow" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label3" runat="server" Text="Max Users"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblMaxUsers" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label4" runat="server" Text="User Charge Per Month"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblUserChargePerMonth" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label5" runat="server" Text="Charge Type"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblChargeType" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label6" runat="server" Text="Purchase Date"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblPurchaseDate" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label7" runat="server" Text="Valid Till"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblValidity" runat="server"></asp:Label></div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="divAddUserPlanBox" class="modalPopUpDetails">
        <div id="divAddUserPlanModal" class="container">
            <asp:UpdatePanel runat="server" ID="upAddUserPlanModal" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divAddUserPlan" style="width: 100%;">
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label9" runat="server" Text="Plan Code"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblAddUserPlan_Code" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label27" runat="server" Text="Plan Name"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblAddUserPlanName" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label17" runat="server" Text="Months"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblAddUserMnths" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label18" runat="server" Text="No of Users to Add"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblAddUserUsers" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label30" runat="server" Text="Validity"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblAddUserValidity" runat="server"></asp:Label></div>
                        </div>
                        <%--<div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label19" runat="server" Text="Discount"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblAddUserDiscount" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblAddUserPercentage"
                                    runat="server" Text="%"></asp:Label></div>
                        </div>--%>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label20" runat="server" Text="ChargePerMonth"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblAddUserChargeperMnth" runat="server"></asp:Label>&nbsp;<asp:Label
                                    ID="lblAddUserChrgPerMnthUnit" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label21" runat="server" Text="Price"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblAddUserOrgPrice" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblAddUserOrgPrcUnit"
                                    runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn" >
                            <div class="leftColumn" style="position:relative;top:4px;">
                                <asp:Label ID="Label22" runat="server" Text="Discount Price"></asp:Label></div>
                            <div class="middleColumn" style="position:relative;top:4px;">
                                :
                            </div>
                            <div class="lastColumn">
                                <%--<asp:Label ID="lblAddUserDiscountPrice" runat="server"></asp:Label>&nbsp;<asp:Label
                                    ID="lblAddUserDiscountUnit" runat="server"></asp:Label>--%>
                                <asp:TextBox ID="txtAddUserDiscountAmt" runat="server" onkeyup="calculatePrice(this);"></asp:TextBox></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label23" runat="server" Text="Payable Amount"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblAddUserPayableAmount" runat="server"></asp:Label>&nbsp;<asp:Label
                                    ID="lblAddUserPaybleAmntUnit" runat="server"></asp:Label></div>
                        </div>
                        <div class="action center">
                            <asp:Button ID="btnAddUser" runat="server" Text="AddUser" OnClick="btnAddUser_Click" />
                            <asp:Button ID="btnAddUserDenyRequest" runat="server" Text="Deny" OnClientClick="return confirm('Are you sure you want to deny the request');"
                                OnClick="btnDenyRequest_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="divRenewPlanBox" class="modalPopUpDetails">
        <div id="divRenewPlanModal" class="container">
            <asp:UpdatePanel runat="server" ID="upRenewPlanModal" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divRenewPlan" style="width: 100%;">
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="lblPlan" runat="server" Text="Plan Code"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblRenewPlan_Code" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="lblMn" runat="server" Text="Months"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblRenewMnths" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="lblUsr" runat="server" Text="Max Users"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblRenewUsers" runat="server"></asp:Label></div>
                        </div>
                        <%--<div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="lblForDiscount" runat="server" Text="Discount"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblRenewDiscount" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblRenewPercent"
                                    runat="server" Text="%"></asp:Label></div>
                        </div>--%>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="lblChrg" runat="server" Text="ChargePerMonth"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblRenewChargeperMnth" runat="server"></asp:Label>&nbsp;<asp:Label
                                    ID="lblRenewChrgPerMnthUnit" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="lblPrz" runat="server" Text="Price"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblRenewOrgPrice" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblRenewOrgPrcUnit"
                                    runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn" style="position:relative;top:4px;">
                                <asp:Label ID="lblForDiscountPrice" runat="server" Text="Discount Price"></asp:Label></div>
                            <div class="middleColumn" style="position:relative;top:4px;">
                                :
                            </div>
                            <div class="lastColumn">
                                <%--<asp:Label ID="lblRenewDiscountPrice" runat="server"></asp:Label>&nbsp;<asp:Label
                                    ID="lblRenewDiscountUnit" runat="server"></asp:Label>--%>
                                <asp:TextBox ID="txtRenewDiscountAmt" runat="server" onkeyup="calculatePrice(this);"></asp:TextBox></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="lblForPayableAmt" runat="server" Text="Payable Amount"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblRenewPayableAmount" runat="server"></asp:Label>&nbsp;<asp:Label
                                    ID="lblRenewPaybleAmntUnit" runat="server"></asp:Label></div>
                        </div>
                        <div class="action center">
                            <asp:Button ID="btnRenew" runat="server" Text="Renew" OnClick="btnRenew_Click" />
                            <asp:Button ID="btnRenewDenyRequest" runat="server" Text="Deny" OnClientClick="return confirm('Are you sure you want to deny the request');"
                                OnClick="btnDenyRequest_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="divPurchasePlanBox" class="modalPopUpDetails">
        <div id="divPurchasePlanModal" class="container">
            <asp:UpdatePanel runat="server" ID="upPurchasePlanModal" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divPurchasePlan" style="width: 100%;">
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label1" runat="server" Text="Plan Code"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblPurchasePlan_Code" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label10" runat="server" Text="Months"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblPurchaseMnths" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label11" runat="server" Text="Max Users"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblPurchaseUsers" runat="server"></asp:Label></div>
                        </div>
                        <%--<div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label12" runat="server" Text="Discount"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblPurchaseDiscount" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblPurchasePercent"
                                    runat="server" Text="%"></asp:Label></div>
                        </div>--%>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label13" runat="server" Text="ChargePerMonth"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblPurchaseChargeperMnth" runat="server"></asp:Label>&nbsp;<asp:Label
                                    ID="lblPurchaseChrgPerMnthUnit" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label14" runat="server" Text="Price"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblPurchaseOrgPrice" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblPurchaseOrgPrcUnit"
                                    runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn" style="position:relative;top:4px;">
                                <asp:Label ID="Label15" runat="server" Text="Discount Price"></asp:Label></div>
                            <div class="middleColumn" style="position:relative;top:4px;">
                                :
                            </div>
                            <div class="lastColumn">
                                <%--<asp:Label ID="lblPurchaseDiscountPrice" runat="server"></asp:Label>&nbsp;<asp:Label
                                    ID="lblPurchaseDiscountUnit" runat="server"></asp:Label>--%>
                                <asp:TextBox ID="txtPurchaseDiscountAmt" runat="server" onkeyup="calculatePrice(this);"></asp:TextBox></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label16" runat="server" Text="Payable Amount"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblPurchasePayableAmount" runat="server"></asp:Label>&nbsp;<asp:Label
                                    ID="lblPurchasePaybleAmntUnit" runat="server"></asp:Label></div>
                        </div>
                        <div class="action center">
                            <asp:Button ID="btnPurchase" runat="server" Text="Purchase" OnClick="btnPurchase_Click" />
                            <asp:Button ID="btnPurchaseDenyRequest" runat="server" Text="Deny" OnClientClick="return confirm('Are you sure you want to deny the request');"
                                OnClick="btnDenyRequest_Click" />
                        </div>
                    </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="divUpgradeDowngradePlanBox" class="modalPopUpDetails">
        <div id="divUpgradeDowngradePlanModal" class="container">
            <asp:UpdatePanel runat="server" ID="upUpgradeDowngradePlanModal" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divUpgradeDowngradePlan" style="width: 100%;">
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label24" runat="server" Text="Plan Code"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblUpgradeDowngradePlan_Code" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label31" runat="server" Text="Plan Name"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblUpgradeDowngradePlanName" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label25" runat="server" Text="Months"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblUpgradeDowngradeMnths" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label26" runat="server" Text="No of Users"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblUpgradeDowngradeUsers" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label29" runat="server" Text="Max WorkFlows"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblUpgradeDowngradeMaxWrkFlows" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label28" runat="server" Text="Charge per user per month"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lblUpgradeDowngradeChargeperMnth" runat="server"></asp:Label>&nbsp;<asp:Label
                                    ID="lblUpgradeDowngradeChrgPerMnthUnit" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="Label32" runat="server" Text="Payable amount"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="Label33" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblUpgradeDowngradePayableAmount"
                                    runat="server"></asp:Label>&nbsp;&nbsp;
                                <asp:Label ID="lblUpgradeDowngradePayableAmountUnit" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn" style="position:relative;top:4px;">
                                <asp:Label ID="Label34" runat="server" Text="Discount"></asp:Label></div>
                            <div class="middleColumn" style="position:relative;top:4px;">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:TextBox ID="txtUpgrdDwngrdDiscount" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="action center">
                            <asp:Button ID="btnUpgradeDowngrade" runat="server" OnClientClick="return confirm('Are you sure you want to upgrade the plan');"
                                OnClick="btnUpgradeDowngrade_Click" />
                            <asp:Button ID="btnUpgradeDowngradeDenyReq" runat="server" Text="Deny" OnClientClick="return confirm('Are you sure you want to deny the request');"
                                OnClick="btnDenyRequest_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="divCompanyDtlsModal" style="display: none;">
        <asp:UpdatePanel runat="server" ID="updCompanyDetails" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divCompanyDetailUC">
                    <cmpDetails:Details ID="ucCompanyDetails" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="divDenyRequestBox" style="display: none;" class="modalPopUpDetails">
        <div class="container">
            <asp:UpdatePanel runat="server" ID="updDenyRequest" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divDenyRequest">
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="lblSelectRemarks" runat="server" Text="Remarks"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:DropDownList ID="ddlRemarks" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="action center">
                            <asp:Button ID="btnSaveDenyRemarks" runat="server" Text="Save" OnClick="btnSaveDenyRemarks_Click" />
                            <asp:Button ID="btnCancelDenyRemarks" runat="server" Text="Cancel" OnClick="btnCancelDenyRemarks_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
        }
    </script>
    
    <asp:HiddenField ID="hidPriceAmountPayable" runat="server" />
</asp:Content>

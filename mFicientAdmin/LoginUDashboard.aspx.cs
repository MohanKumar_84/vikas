﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientAdmin
{
    public partial class LoginUDashboard : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;

            if (!IsPostBack)
            {
                Literal ltlUserType = (Literal)this.Master.Master.FindControl("ltUserType");
                /*if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else
                {
                    if (Page.PreviousPage != null)
                    {
                        string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                        prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                        ContentPlaceHolder cphPageCanvas = (ContentPlaceHolder)PreviousPage.Master.Master.FindControl("form").FindControl("MainCanvas").FindControl("PageCanvas");
                        hfs.Value = ((HiddenField)cphPageCanvas.FindControl("hfs")).Value;
                    }
                }*/
                if (string.IsNullOrEmpty(hfsValue))
                    Response.Redirect(@"Default.aspx");
                string Role = strRole.ToUpper();
                if (Role == "R")
                {
                    CheckLoginAdmin ad = new CheckLoginAdmin();
                    DataTable dt = ad.GetRlist(strUserName);
                    if (dt.Rows.Count > 0)
                    {
                        lnkdetails.Visible = true;
                        lblInfo.Text = "<h1>Enquiry Details</h1>";
                        rptUserDetails.DataSource = dt;
                        rptUserDetails.DataBind();
                    }
                    else if (dt.Rows.Count == 0)
                    {
                        lnkdetails.Visible = false;
                        lblInfo.Text = "<h1>no details of enquiries added yet</h1>";
                        rptUserDetails.Visible = false;
                    }
                    ltlUserType.Text = "Reseller";
                }
                else if (Role == "S")
                {
                    CheckLoginAdmin ad = new CheckLoginAdmin();
                    DataTable dt = ad.GetSPlist(strUserName);
                    if (dt.Rows.Count > 0)
                    {
                        lnkdetails.Visible = true;
                        lblInfo.Text = "<h1>Enquiry Details</h1>";
                        rptUserDetails.DataSource = dt;
                        rptUserDetails.DataBind();
                    }
                    else if (dt.Rows.Count == 0)
                    {
                        lnkdetails.Visible = false;
                        lblInfo.Text = "<h1>no details of enquiries added yet</h1>";
                        rptUserDetails.Visible = false;
                    }
                    ltlUserType.Text = "Sales Executive";
                }
                else if (Role == "SM")
                {
                    CheckLoginAdmin ad = new CheckLoginAdmin();
                    DataTable dt = ad.GetSMlist(strUserName);
                    if (dt.Rows.Count > 0)
                    {
                        lnkdetails.Visible = true;
                        lblInfo.Text = "<h1>Enquiry Details</h1>";
                        rptUserDetails.DataSource = dt;
                        rptUserDetails.DataBind();
                    }
                    else if (dt.Rows.Count == 0)
                    {
                        lnkdetails.Visible = false;
                        lblInfo.Text = "<h1>no details of enquiries added yet</h1>";
                        rptUserDetails.Visible = false;
                    }
                    ltlUserType.Text = "Sales Manager";
                }
                else if (Role == "SUPP")
                {
                   pnlRepeaterBoxHeader.Visible = false;
                 // lnkdetails.Visible = true;
                  ltlUserType.Text = "Support";
                }

                else if (Role == "ACC")
                {
                    pnlRepeaterBoxHeader.Visible = false;
                    ltlUserType.Text = "Accounts";
                }
                else
                {
                    ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
           LinkButton ChangePass = (LinkButton)this.Master.Master.FindControl("lbChangePwd");
            ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
        }
    }
}
﻿<%@ Page Title="mFicient" Language="C#" EnableEventValidation="true" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="ManageEnquiryDetailsSP.aspx.cs" Inherits="mFicientAdmin.ManageEnquiryDetailsSP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                     <section>
                     <div id="SMstatus" runat="server">
                        <div class="searchRow g12" style="border: 1px solid #FAFAFA">
                            <div class="g5 radioButtonList">
                                <asp:RadioButtonList ID="radListSelection" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                    RepeatLayout="Table" OnSelectedIndexChanged="radListSelection_SelectedIndexChanged1">
                                    <asp:ListItem Text="<strong>New</strong>" Value="1"  Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="<strong>Processed</strong>" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                      </div>
                    </section>
                    <section class="clear" style="margin-bottom: 5px;">
                        <div id="div1">
                        </div>
                    </section>

                        <section>
                         
                            <div id="divInformation">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>ENQUIRY DETAILS</h1>"></asp:Label>
                                        </div>
                                        
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptUserDetails" runat="server" OnItemCommand="rptUserDetails_ItemCommand">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                   <th style="display:none;">Enquiry Id</th>
                                                    <th>
                                                        COMPANY NAME
                                                    </th>
                                                    <th>
                                                   BUSINESS TYPE
                                                    </th>
                                                    <th>STATUS</th>
                                                   <th></th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                               <td style="display:none;">
                                               <asp:Label ID="lblid" runat="server" Text='<%#Eval("ENQUIRY_ID") %>'></asp:Label>
                                               </td> 
                                                <td>
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbltype" runat="server" Text='<%# Eval("TYPE_OF_COMPANY") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblstatus" runat="server" Text='<%# Eval("STATUS") %>'></asp:Label>
                                                </td>

                                                <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Info"  CssClass="repeaterLink" CommandName="Info" OnClick="lnkok_Click"></asp:LinkButton>
                                                    
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                             <tr class="repeaterItem">
                                               <td style="display:none;">
                                               <asp:Label ID="lblid" runat="server" Text='<%#Eval("ENQUIRY_ID") %>'></asp:Label>
                                               </td>  
                                                <td>
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbltype" runat="server" Text='<%# Eval("TYPE_OF_COMPANY") %>'></asp:Label>
                                                </td>
                                               <td>
                                                    <asp:Label ID="lblstatus" runat="server" Text='<%# Eval("STATUS") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Info"  CssClass="repeaterLink" CommandName="Info" OnClick="lnkok_Click"></asp:LinkButton>
                                                    
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                             
                                    <asp:HiddenField ID="hidDetailsFormMode" runat="server"  />
                                </div>
                            </asp:Panel>
                        </section>
                        <section>
                            
                        </section>
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        </div>
                        

                        </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
        <div class="modalPopUpDetails">
            <div id="divUserDtlsModal" style="display: none">
                <asp:UpdatePanel runat="server" ID="upUserDtlsModal" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="ProcImgUpd">
                            <div id="DivUserDetails" style="width: 100%;">
                                <div style="float: left; width: 75%;margin-top:2px; margin-left:40px;">
                                    <asp:Panel ID="pnlMobileUserForm" Visible="false" runat="server">
                                      <%--  <fieldset>--%>
                                            <div style="margin-bottom: 20px; display: none;">
                                                <div style="float: left; margin-right: 43px;">
                                                    <asp:Label ID="label8" Text="<strong>Enquiry Id</strong>" runat="server"></asp:Label></div>
                                                <div style="float: left;">
                                                    <asp:Label ID="lid" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div style="clear: both;">
                                            </div>
                                            <div style="margin-bottom: 20px;margin-top:-10px;">
                                                <div style="float: left; margin-right: 68px;">
                                                    <asp:Label ID="label1" Text="<strong>Company</strong>" runat="server"></asp:Label></div>
                                                <div style="float: left;">
                                                    <asp:Label ID="lname" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div style="clear: both;">
                                            </div>
                                            <div style="margin-bottom: 20px;">
                                                <div style="float: left; margin-right: 75px;margin-top:5px;">
                                                    <asp:Label ID="label2" Text="<strong>Country</strong>" runat="server"></asp:Label></div>
                                                <div style="float: left;margin-top:5px;">
                                                    <asp:Label ID="lcountry" runat="server"></asp:Label></div>
                                            </div>
                                            <div style="clear: both;">
                                            </div>
                                            <div style="margin-bottom: 20px; ">
                                                <div style="float: left; margin-right: 41px;margin-top:5px;">
                                                    <asp:Label ID="label3" Text="<strong>Business Type</strong>" runat="server"></asp:Label></div>
                                                 <div style="float: left;margin-top:5px;">
                                                <asp:Label ID="ltype" runat="server"></asp:Label></div>
                                            </div>
                                            <div style="clear: both;">
                                            </div>
                                            <div style="margin-bottom: 20px; ">
                                                <div style="float: left; margin-right: 35px;margin-top:5px;">
                                                    <asp:Label ID="label4" Text="<strong>Contact Person</strong>" runat="server"></asp:Label></div>
                                                <div style="float: left;margin-top:5px;">
                                                    <asp:Label ID="lperson" runat="server"></asp:Label></div>
                                            </div>
                                            <div style="clear: both;">
                                            </div>
                                            <div style="margin-bottom: 20px; ">
                                                <div style="float: left; margin-right: 29px;margin-top:5px;">
                                                    <asp:Label ID="label5" Text="<strong>Contact Number</strong>" runat="server"></asp:Label></div>
                                                <div style="float: left;margin-top:5px;">
                                                    <asp:Label ID="lnumber" runat="server"></asp:Label></div>
                                            </div>
                                            <div style="clear: both;">
                                            </div>
                                            <div style="margin-bottom: 20px; ">
                                                <div style="float: left; margin-right: 89px;margin-top:5px;">
                                                    <asp:Label ID="label6" Text=" <strong>Email</strong>" runat="server"></asp:Label></div>
                                                <div style="float: left;margin-top:5px;">
                                                    <asp:Label ID="lemail" runat="server"></asp:Label></div>
                                            </div>
                                            <div style="clear: both;">
                                            </div>
                                            <div style="margin-bottom: 20px; ">
                                                <div style="float: left; margin-right: 46px;margin-top:5px;">
                                                    <asp:Label ID="label7" Text="<strong>Enquiry Date</strong>" runat="server"></asp:Label></div>
                                                <div style="float: left;margin-top:5px;">
                                                    <asp:Label ID="Enquirydate" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div style="clear: both;">
                                            </div>
                                            <div style="margin-bottom: 20px;">
                                                <div style="float: left; margin-right: 88px;margin-top:5px;">
                                                    <asp:Label ID="label9" Text="<strong>Status</strong>" runat="server"></asp:Label></div>
                                                <div style="float: left;margin-top:5px;">
                                                    <asp:Label ID="lstatus" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                            <div style="clear: both;">
                                            </div>
                                            <div id="salesdiv" runat="server" style="margin-top: 7px;">
                                                <div id="Sdiv" runat="server" style="margin-bottom: 20px;">
                                                    <div style="float: left; margin-right: 14px;">
                                                        <asp:Label ID="label10" Text="<strong>Select Sales Person</strong>" runat="server"></asp:Label></div>
                                                    <div style="float: left;">
                                                        <asp:DropDownList ID="ddlsalesp" runat="server" CssClass="Fld" Width="100px" 
                                                         AutoPostBack="true">
                                                        </asp:DropDownList>  
                                                    </div>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <div style="float: right;margin-top:3px;">
                                                        <asp:LinkButton ID="lnkspallote" Text="Allote" runat="server" OnClick="lnkspallote_Click"></asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div style="clear: both;">
                                                </div>
                                                <div id="alloteddiv" runat="server" style="margin-bottom: 20px; margin-top: 24px;">
                                                    <div style="float: left; margin-right: 49px;">
                                                        <asp:Label ID="lblallotedSP" Text="<strong>Alloted User</strong>" runat="server"></asp:Label></div>
                                                    <div style="float: left;">
                                                        <asp:Label ID="lallotedSP" runat="server"></asp:Label>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnksales" runat="server" Text="Change" OnClick="lnksales_Click"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="clear: both">
                                            </div>
                                            <div id="resellerdiv" runat="server" style="margin-bottom:5px;">
                                                <div id="Rdiv" runat="server" style="margin-bottom: 20px; margin-top: -10px;">
                                                    <div style="float: left; margin-right: 20px; margin-top:7px;">
                                                        <asp:Label ID="label11" Text="<strong>Select Reseller</strong>" runat="server"></asp:Label></div>
                                                    <div style="float: left;margin-left:10px;">
                                                        <asp:DropDownList ID="ddlreseller" runat="server" CssClass="Fld" Width="100px" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <div style="float:right;margin-top:3px;">
                                                        <asp:LinkButton ID="lnkallotREseller" Text="Allote" runat="server" OnClick="lnkallotREseller_Click"></asp:LinkButton>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div style="clear: both;">
                                                </div>
                                                <div id="allotedreseller" runat="server" style="margin-bottom: 20px; margin-top: 24px;">
                                                    <div style="float: left; margin-right: 30px;">
                                                        <asp:Label ID="label12" Text="<strong>Alloted Reseller</strong>"
                                                            runat="server"></asp:Label></div>
                                                    <div style="float: left;">
                                                        <asp:Label ID="lallotedreseller" runat="server"></asp:Label>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkreseller" runat="server" Text="Change" OnClick="lnkreseller_Click"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="clear: both;">
                                            </div>
                                            <div id="divresellerlogin" style="height: 20px; margin-top: 30px;" runat="server">
                                                <asp:RadioButtonList ID="rdlist1" runat="server" AutoPostBack="true" RepeatDirection="Vertical"
                                                    RepeatLayout="Table" OnSelectedIndexChanged="rdlist1_SelectedIndexChanged">
                                                    <asp:ListItem Text="Create Company and Close Enquiries" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Close Enquiries with Remarks" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                                <div class="SubProcborderDiv">
                                                    <div class="SubProcBtnDiv" align="center">
                                                        <asp:Button ID="bttnprocess" runat="server" Width="60px" Text="Process" OnClick="bttnprocess_Click">
                                                        </asp:Button>
                                                    </div>
                                                </div>
                                                <%-- <div style="position: relative; left: 70px; bottom: 10px; top: 10px;">
                                                    <asp:LinkButton ID="lnkProcess" runat="server" Text="Process" CssClass="repeaterLink fr"
                                                        OnClick="lnkProcess_Click"></asp:LinkButton>
                                                </div>--%>
                                            </div>
                                        <%--</fieldset>--%>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="modalPopUpDetails">
            <div id="divRemark" style="display: none">
                <asp:UpdatePanel runat="server" ID="UpdatePanelRemark" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="ProcImgUpd">
                            <div id="Div2" style="width: 100%;">
                                <div style="float: left; width: 75%;">
                                    <asp:Panel ID="pnlremark" Visible="false" runat="server">
                                        <fieldset>
                                            <div style="margin-bottom: 20px; margin-top: 10px;">
                                                <div style="float: left; margin-right: 30px;width:250px;">
                                                    <asp:Label ID="label14" Text="<strong>Please select a remark to close enquiries</strong>" runat="server"></asp:Label></div>
                                                <br />
                                                <br />
                                                <div style="margin-left:60px; height:auto;width:auto;">
                                                    <asp:DropDownList ID="ddlremarks" CssClass="Fld" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div style="position: relative; left: 10px; bottom: 10px; top: 10px;">
                                                <asp:LinkButton ID="lnkSave" runat="server" Text="Save" CssClass="repeaterLink fr"
                                                    OnClick="lnkSave_Click"></asp:LinkButton>
                                            </div>
                                        </fieldset>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
  <script type="text/javascript">
      Sys.Application.add_init(application_init);
      function application_init() {
          //Sys.Debug.trace("Application.Init");
          var prm = Sys.WebForms.PageRequestManager.getInstance();
          prm.add_initializeRequest(prm_initializeRequest);
          prm.add_endRequest(prm_endRequest);
      }
      function prm_initializeRequest() {
          //$("input").uniform();
          showWaitModal();
      }
      function prm_endRequest() {
          //s$("input").uniform();
          hideWaitModal();
      }
    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientAdmin
{
    public partial class ManageCpSeller : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AddsellerDetailsAdmin ad = new AddsellerDetailsAdmin();
            DataTable dt = ad.GetSellerList();
            if (dt.Rows.Count > 0)
            {
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
            }
        }

        protected void lnkAddNewMobileUser_Click(object sender, EventArgs e)
        {

            clearControls();
            enableDisablePanels("Add");
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (hidDetailsFormMode.Value != "")
            {
                //Edit
                GetSellerDetailsAdmin sellerdetail = new GetSellerDetailsAdmin();
                sellerdetail.SellerDetails(hidDetailsFormMode.Value);

                DataTable dt = sellerdetail.ResultTable;
                if (dt.Rows.Count > 0)
                {
                    UpdateSellerDetails(hidDetailsFormMode.Value);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                }
            }
            else
            {
                //new user
                SaveSellerDetails();
                //Utilities.Insertgdserver(g);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

            clearControls();
            enableDisablePanels("");
  
        }

        protected void SaveSellerDetails()
        {
            string strMessage = ValidatePanel();
            if (!String.IsNullOrEmpty(strMessage))
            {
                showAlert(strMessage);
            }
            else
            {
                if (chkIsActive.Checked == true)
                {
                    EnableServerAdmin enable = new EnableServerAdmin();
                    enable.EnableTrueSeller(txtname.Text, txtemail.Text, true, txtnumber.Text, Convert.ToDateTime(txtdate.Text),Utilities.GetSellerId(txtname.Text,txtemail.Text));
                    enableDisablePanels("");
                    hidPasswordEntered.Value = "";

                    showStickyOrNormalMessage("Server Details added  successfully.", "sticky");
                }

                else if (chkIsActive.Checked == false)
                {
                    EnableServerAdmin enable = new EnableServerAdmin();
                    enable.EnableFalseSeller(txtname.Text, txtemail.Text, false, txtnumber.Text, Convert.ToDateTime(txtdate.Text), Utilities.GetSellerId(txtname.Text,txtemail.Text));
                    enableDisablePanels("");
                    hidPasswordEntered.Value = "";


                    showStickyOrNormalMessage("Server Details added  successfully.", "sticky");
                }

                else
                {
                    showStickyOrNormalMessage("Internal Error.Please try again", "sticky");
                }
                          

            }
        }
        void showStickyOrNormalMessage(string message, string type)
        {
            if (type.ToLower() == "sticky")
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "TestingSticky", @"$.msg('" + message + "',{sticky:true});", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "TestingNormal", @"$.msg('" + message + "');", true);
            }
        }
        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#buttons'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);

        }

        void showAlert(string message, string divIdToShowMsg)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }

        public void UpdateSellerDetails(string SellerId)
        {
            string strMessage = ValidatePanel();


            UpdateSellerDetailsAdmin updatedetail = new UpdateSellerDetailsAdmin();
            updatedetail.UpdateSeller(txtname.Text,SellerId,txtemail.Text,txtnumber.Text,Convert.ToDateTime(txtdate.Text));

            int intStatusCode = updatedetail.StatusCode;
            string strDescription = updatedetail.StatusDescription;

            if (intStatusCode == 0)
            {
                showStickyOrNormalMessage("Seller details updated successfully", "sticky");
                enableDisablePanels("");
            }
            else
            {
                showStickyOrNormalMessage("Internal Error.Please try again", "sticky");
            }
        }

        public string ValidatePanel()
        {

            string strmessage = "";
            if (!Utilities.IsValidString(txtname.Text, true, true, true, " ", 4, 20, false, false))
            {
                strmessage = strmessage + "Please Enter correct Seller Name.<br />";
            }

            if (!Utilities.IsValidEmail(txtemail.Text))
            {
                strmessage = strmessage + "Please Enter correct Email.<br />";
            }

            if (!Utilities.IsValidString(txtnumber.Text, false, false, true, "", 4, 20, false, false))
            {
                strmessage = strmessage + "Please Enter correct Contact Number.<br />";
            }

            if (!Utilities.IsValidString(txtdate.Text, false, false, true, ".", 4, 20, false, false))
            {
                strmessage = strmessage + "Please Enter correct Created Date.<br />";
            }

            if (!Utilities.IsValidString(txtsellerid.Text, true, true, true, ".", 4, 20, false, false))
            {
                strmessage = strmessage + "Please Enter correct Seller ID.<br />";
            }
            return strmessage;
        }


        void enableDisablePanels(string mode)
        {

            if (mode == "Add")
            {
                pnlRepeaterBox.Visible = false;
                pnlMobileUserForm.Visible = true;
            }
            else if (mode == "Edit")
            {
                pnlRepeaterBox.Visible = false;
                pnlMobileUserForm.Visible = true;
            }
            else
            {
                clearControls();
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = false;
            }
        }
        protected void clearControls()
        {
            txtname.Text="";
            txtemail.Text = "";
            txtnumber.Text = "";
            txtdate.Text = "";
            txtsellerid.Text = "";
            hidDetailsFormMode.Value = "";
        }


        protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item;
            //Literal lit_Name = (Literal)item.FindControl("lit_LoginName");
            Label lit_id = (Label)item.FindControl("Lblsellerid");

            if (e.CommandName == "Edit")
            {
                FillSellerDetails(lit_id.Text);
            }
        }

        protected void FillSellerDetails(string sellerId)
        {
            GetSellerDetailsAdmin sellerdetails = new GetSellerDetailsAdmin();
            sellerdetails.SellerDetails(sellerId);
            DataTable dt = sellerdetails.ResultTable;
            if (dt.Rows.Count > 0)
            {
                hidDetailsFormMode.Value = sellerId;
                txtsellerid.Text = Convert.ToString(dt.Rows[0]["SELLER_ID"]);
                txtname.Text = Convert.ToString(dt.Rows[0]["SELLER_NAME"]);
                txtemail.Text = Convert.ToString(dt.Rows[0]["EMAIL"]);
                txtnumber.Text = Convert.ToString(dt.Rows[0]["CONTACT_NUMBER"]);
                txtdate.Text = Convert.ToString(dt.Rows[0]["CREATED_ON"]);
            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }
        protected void rptUserDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Edit":
                    processEdit(e);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing2", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                    break;
              
            }
        }

        void processEdit(RepeaterCommandEventArgs e)
        {
            hidDetailsFormMode.Value = "Edit";
            Label seller_id= (Label)e.Item.FindControl("Lblsellerid");
            FillSellerDetails(seller_id.Text);
            enableDisablePanels("Edit");
        }




    }
}
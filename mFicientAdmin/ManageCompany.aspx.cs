﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Net;

namespace mFicientAdmin
{
    public partial class ManageCompany : System.Web.UI.Page
    {
        public enum ManageCompanyError
        {
            CmpBlckReasonUnbound
        }
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;

            if (!Page.IsPostBack)
            {
                if (string.IsNullOrEmpty(hfsValue))
                    Response.Redirect(@"Default.aspx");
                string Role = strRole.ToUpper();
                if (Role == "A")
                {
                    try
                    {
                        bindUnblockedRepeaterAdmin();
                    }
                    catch (MficientException ex)
                    {
                        Utilities.showMessage(ex.Message, this.Page, DIALOG_TYPE.Error);
                    }
                    catch
                    {
                        Utilities.showMessage("Internal server error.", this.Page, DIALOG_TYPE.Error);
                    }
                    try
                    {
                        bindDdlBlockCompanyReason();
                        bindDdlUnBlockCompanyReason();
                    }
                    catch
                    {
                    }
                }
                else if (Role == "R")
                {
                    BindRepeaterReseller();

                }
                else if (Role == "SM")
                {
                    BindRepeaterSalesManager();

                }
                else if (Role == "S")
                {
                    BindRepeaterSalesPerson();

                }
                else if (Role == "ACC")
                {
                    try
                    {
                        bindUnblockedRepeaterAdmin();
                    }
                    catch (MficientException ex)
                    {
                        Utilities.showMessage(ex.Message, this.Page, DIALOG_TYPE.Error);
                    }
                    catch
                    {
                        Utilities.showMessage("Internal server error.", this.Page, DIALOG_TYPE.Error);
                    }
                    try
                    {
                        bindDdlBlockCompanyReason();
                        bindDdlUnBlockCompanyReason();
                    }
                    catch
                    {
                    }
                }
                Utilities.makeInputFieldsUniform(this.Page, "MakeFormFieldsUniform", false, String.Empty);
            }
            else
            {
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "postBackScript", " $(\"select\").uniform();$(\"input\").uniform();$(\"#content\").find('div.widgets').wl_Widget();$('#aspnetForm').wl_Form();", true);//12/10/2012
                Utilities.makeInputFieldsUniformIfMenuClickedAgain(this.Page, String.Empty);
            }


        }

        protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Info":
                    processShowCompleteCmpDtl(e);
                    break;

            }
        }

        protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
        }
        void processShowCompleteCmpDtl(RepeaterCommandEventArgs e)
        {
            try
            {
                Label lblCompanyId = (Label)e.Item.FindControl("lblid");
                FillDetails(lblCompanyId.Text);
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing2", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                Utilities.showModalPopup("divUserDtlsModal",
                    upd, "Company Detail",
                    "500", true);
                updCompanyInformation.Update();
            }
            catch
            {
                Utilities.showMessage("Internal server error.", upd, DIALOG_TYPE.Error);
            }
        }

        protected void FillDetails(string companyid)
        {
            GetCompanyDetailsWithCurrentPlan objCmpDtlWithPlan = new
                GetCompanyDetailsWithCurrentPlan(companyid);
            objCmpDtlWithPlan.Process();

            GetCompanyDetailsFromCPTable objCmpDtlFromCpTbl = new
            GetCompanyDetailsFromCPTable(companyid);
            objCmpDtlFromCpTbl.Process();
            //AddCompanyDetailsAdmin get = new AddCompanyDetailsAdmin();
            //get.CompanyPlanAdminDetails(companyid);
            //DataTable dt = get.ResultTable;
            //string strDescription = get.StatusDescription;
            if (objCmpDtlWithPlan.StatusCode != 0)
            {
                Utilities.showMessage(objCmpDtlWithPlan.StatusDescription, this.Page);
            }
            else
            {
                hidCompanyIdWhenEdit.Value = companyid;

                lblCompanyName.Text = objCmpDtlWithPlan.CmpInfo.CompanyName;

                lblCompanyAddress.Text = objCmpDtlWithPlan.CmpInfo.StreetAddress1 + " " + objCmpDtlWithPlan.CmpInfo.StreetAddress2
                    + " " + objCmpDtlWithPlan.CmpInfo.StreetAddress3;

                lblCmpCity.Text = objCmpDtlWithPlan.CmpInfo.CityName;
                lblCmpState.Text = objCmpDtlWithPlan.CmpInfo.StateName;
                lblCmpEmail.Text = objCmpDtlWithPlan.CmpInfo.SupportEmail;
                lblCmpContactNo.Text = objCmpDtlWithPlan.CmpInfo.SupportContact;
                lblCmpAdminName.Text = objCmpDtlWithPlan.CmpAdmin.Firstname + " " +
                    objCmpDtlWithPlan.CmpAdmin.MiddleName + " " +
                    " " +
                    objCmpDtlWithPlan.CmpAdmin.LastName;
                lblStatus.Text = objCmpDtlWithPlan.CmpInfo.SalesManagerApproved ? "Company Working" : "Processing";
                lblAdminEmailId.Text = objCmpDtlWithPlan.CmpAdmin.EmailId;
                if (objCmpDtlWithPlan.CmpAdmin.IsTrial)
                {
                    lblCmpPlanName.Text = objCmpDtlWithPlan.TrialPlanDtl.PlanName;
                }
                else
                {
                    lblCmpPlanName.Text = objCmpDtlWithPlan.PaidPlanDtl.PlanName;
                }
                lblPlanCode.Text = objCmpDtlWithPlan.CmpCurrentPlan.PlanCode;
                lblMaxWorkflow.Text = Convert.ToString(objCmpDtlWithPlan.CmpCurrentPlan.MaxWorkFlow);
                lblMaxNoOfUsers.Text = Convert.ToString(objCmpDtlWithPlan.CmpCurrentPlan.MaxUser);
                DateTime dtValidTill = Utilities.getValidTillAsDateTime(
                    objCmpDtlWithPlan.CmpCurrentPlan.Validity,
                    objCmpDtlWithPlan.CmpCurrentPlan.PurchaseDate);
                lblValidUpto.Text = dtValidTill.ToShortDateString();
                showHideLinkBlock(false);
                showHideLinkUnBlock(false);
                if (strRole.ToUpper() == "A" || strRole.ToUpper() == "ACC")
                {
                    if (objCmpDtlWithPlan.CmpInfo.IsBlocked)
                    {
                        if (objCmpDtlFromCpTbl.StatusCode == 0 &&
                           !String.IsNullOrEmpty(objCmpDtlFromCpTbl.CompanyInfo.CompanyId))
                        {
                            showHideLinkBlock(false);
                            showHideLinkUnBlock(true);
                        }

                    }
                    else
                    {
                        if (objCmpDtlFromCpTbl.StatusCode == 0 &&
                           !String.IsNullOrEmpty(objCmpDtlFromCpTbl.CompanyInfo.CompanyId))
                        {
                            showHideLinkBlock(true);
                            showHideLinkUnBlock(false);
                        }
                    }
                    updCompanyInformation.Update();
                }
                //hidCompanyIdWhenEdit.Value = companyid;
                //lblCompanyName.Text = Convert.ToString(dt.Rows[0]["CompanyName"]);
                //lblCompanyAddress.Text = Convert.ToString(dt.Rows[0]["CompanyAddress"]);
                //lblCmpCity.Text = Convert.ToString(dt.Rows[0]["CompanyCity"]);
                //lblCmpState.Text = Convert.ToString(dt.Rows[0]["companystate"]);
                //lblCmpEmail.Text = Convert.ToString(dt.Rows[0]["companyemail"]);
                //lblCmpContactNo.Text = Convert.ToString(dt.Rows[0]["companycontact"]);
                //lblCmpAdminName.Text = Convert.ToString(dt.Rows[0]["AdminName"]);
                //lblStatus.Text = Convert.ToString(dt.Rows[0]["STATUS"]);
                //lblAdminEmailId.Text = Convert.ToString(dt.Rows[0]["EMAIL_ID"]);
                //lblCmpPlanName.Text = Convert.ToString(dt.Rows[0]["PLAN_NAME"]);
                //lblPlanCode.Text = Convert.ToString(dt.Rows[0]["PLAN_CODE"]);
                //lblMaxWorkflow.Text = Convert.ToString(dt.Rows[0]["MAX_WORKFLOW"]);
                //lblMaxNoOfUsers.Text = Convert.ToString(dt.Rows[0]["MAX_USER"]);
                //DateTime EnqDate = new DateTime(Convert.ToInt64(dt.Rows[0]["Date"]));
                //lblValidUpto.Text = EnqDate.ToShortDateString();
                //if (strRole.ToUpper() == "A" || strRole.ToUpper() == "ACC")
                //{
                //    showHideLinkBlock(true);
                //    updCompanyInformation.Update();
                //}

            }

        }

        void bindUnblockedRepeaterAdmin()
        {
            AddCompanyDetailsAdmin ad = new AddCompanyDetailsAdmin();
            DataTable dt = ad.GetCompanyListAdmin();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    lblHeaderInfo.Text = "<h1>Company Details</h1>";
                    rptUserDetails.Visible = true;
                    rptUserDetails.DataSource = dt;
                    rptUserDetails.DataBind();
                }
                else if (dt.Rows.Count == 0)
                {
                    lblHeaderInfo.Text = "<h1>No company found.</h1>";
                    rptUserDetails.Visible = false;
                }
            }
            else
            {
                throw new MficientException("Internal server error.");
            }
        }
        void bindBlockedCmpRptAdmin()
        {
            AddCompanyDetailsAdmin getBlockedCmpList = new AddCompanyDetailsAdmin();
            DataTable dt = getBlockedCmpList.getBlockedCompanyList();
            if (getBlockedCmpList.StatusCode == 0)
            {
                if (dt.Rows.Count > 0)
                {
                    lblHeaderInfo.Text = "<h1>Company Details</h1>";
                    rptUserDetails.Visible = true;
                    rptUserDetails.DataSource = dt;
                    rptUserDetails.DataBind();
                }
                else if (dt.Rows.Count == 0)
                {
                    lblHeaderInfo.Text = "<h1>No company found.</h1>";
                    rptUserDetails.Visible = false;
                }
            }
            else
            {
                throw new MficientException(getBlockedCmpList.StatusDescription);
            }

        }

        void BindRepeaterReseller()
        {
            AddCompanyDetailsAdmin ad = new AddCompanyDetailsAdmin();
            string ResellerId = ad.GetResellerId(strUserName);
            DataTable dt = ad.GetCompanyListReseller(ResellerId);
            if (dt.Rows.Count > 0)
            {
                lblHeaderInfo.Text = "<h1>Company Details</h1>";
                rptUserDetails.Visible = true;
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
            }
            else if (dt.Rows.Count == 0)
            {
                lblHeaderInfo.Text = "<h1>no details of company added yet</h1>";
                rptUserDetails.Visible = false;
            }
        }

        void BindRepeaterSalesManager()
        {
            AddCompanyDetailsAdmin ad = new AddCompanyDetailsAdmin();
            // string MgrId= ad.GetCompanyId(strUserName);
            DataTable dt = ad.GetCompanyListSalesMgr();
            if (dt.Rows.Count > 0)
            {
                lblHeaderInfo.Text = "<h1>Company Details</h1>";
                rptUserDetails.Visible = true;
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
            }
            else if (dt.Rows.Count == 0)
            {
                lblHeaderInfo.Text = "<h1>no details of company added yet</h1>";
                rptUserDetails.Visible = false;
            }
        }

        void BindRepeaterSalesPerson()
        {
            AddCompanyDetailsAdmin ad = new AddCompanyDetailsAdmin();
            string strId = ad.GetUserId(strUserName);
            DataTable dt = ad.GetCompanyListSales(strId);
            if (dt.Rows.Count > 0)
            {
                lblHeaderInfo.Text = "<h1>Company Details</h1>";
                rptUserDetails.Visible = true;
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
            }
            else if (dt.Rows.Count == 0)
            {
                lblHeaderInfo.Text = "<h1>no details of company added yet</h1>";
                rptUserDetails.Visible = false;
            }
        }

        void bindDdlBlockCompanyReason()
        {
            GetCompanyBlockReason objBlockReason =
                new GetCompanyBlockReason();
            DataTable dtblBlockReason = objBlockReason.getReasonAsDataTable();
            //should get the datatable
            if (dtblBlockReason != null && dtblBlockReason.Rows.Count > 0)
            {
                ddlReasonForBlocking.Items.Clear();
                ddlReasonForBlocking.DataSource = dtblBlockReason;
                ddlReasonForBlocking.DataTextField = "REASON";
                ddlReasonForBlocking.DataValueField = "CODE";
                ddlReasonForBlocking.DataBind();
            }
            else
            {
                throw new MficientException("Internal server error.");
            }
        }
        void bindDdlUnBlockCompanyReason()
        {
            GetCompanyUnblockReasons objUnBlockReason =
                new GetCompanyUnblockReasons();
            DataTable dtblBlockReason = objUnBlockReason.getReasonAsDataTable();
            //should get the datatable
            if (dtblBlockReason != null && dtblBlockReason.Rows.Count > 0)
            {
                ddlUnblockReason.Items.Clear();
                ddlUnblockReason.DataSource = dtblBlockReason;
                ddlUnblockReason.DataTextField = "REASON";
                ddlUnblockReason.DataValueField = "CODE";
                ddlUnblockReason.DataBind();
            }
            else
            {
                throw new MficientException("Internal server error.");
            }
        }
        protected void btnBlockSave_Click(Object sender, EventArgs e)
        {
            try
            {
                BlockUnblockCompany objBlockCmp = new
                BlockUnblockCompany(hidCompanyIdWhenEdit.Value);

                USER_TYPE eUserType = Utilities.getUserTypeFromUserCode(strRole);

                objBlockCmp.processBlockCompany(
                    Convert.ToByte(ddlReasonForBlocking.SelectedValue),
                    eUserType);
                if (objBlockCmp.StatusCode != 0)
                {
                    throw new MficientException(objBlockCmp.StatusDescription);
                }
                else
                {
                    Utilities.closeModalPopUp("divBlockCmpReason", updBlockCmp, "close reason popup");
                    Utilities.closeModalPopUp("divUserDtlsModal", updBlockCmp, "close cmp details popup");
                    Utilities.showMessage("Company blocked successfully.", updBlockCmp, DIALOG_TYPE.Info);
                    bindUnblockedRepeaterAdmin();
                }

            }
            catch (MficientException ex)
            {
                MFExceptionWithErrorInfo objMfExcption =
                    ex as MFExceptionWithErrorInfo;
                if (objMfExcption != null)
                {
                    Utilities.showAlert(objMfExcption.ErrorInfo.ErrorDescription,
                    "divBlockCmpError", updBlockCmp);
                }
                else
                {
                    Utilities.showAlert(ex.Message,
                    "divBlockCmpError", updBlockCmp);
                }
            }
            catch
            {
                Utilities.showAlert("Internal server error.",
                    "divBlockCmpError", updBlockCmp);
            }
        }
        protected void btnUnBlockSave_Click(Object sender, EventArgs e)
        {
            try
            {
                BlockUnblockCompany objBlockCmp = new
                BlockUnblockCompany(hidCompanyIdWhenEdit.Value);

                USER_TYPE eUserType = Utilities.getUserTypeFromUserCode(strRole);

                objBlockCmp.processUnBlockCompany(
                    Convert.ToByte(ddlUnblockReason.SelectedValue),
                    eUserType);
                if (objBlockCmp.StatusCode != 0)
                {
                    throw new MficientException(objBlockCmp.StatusDescription);
                }
                else
                {
                    Utilities.closeModalPopUp("divUnblockCmpReason", updBlockCmp, "close reason popup");
                    Utilities.closeModalPopUp("divUserDtlsModal", updBlockCmp, "close cmp details popup");
                    Utilities.showMessage("Company unblocked successfully.", updBlockCmp, DIALOG_TYPE.Info);
                    bindBlockedCmpRptAdmin();
                    processShowRptByChkBlockCmpStatus();
                }

            }
            catch (MficientException ex)
            {
                MFExceptionWithErrorInfo objMfExcption =
                    ex as MFExceptionWithErrorInfo;
                if (objMfExcption != null)
                {
                    Utilities.showAlert(objMfExcption.ErrorInfo.ErrorDescription,
                    "divBlockCmpError", updBlockCmp);
                }
                else
                {
                    Utilities.showAlert(ex.Message,
                    "divBlockCmpError", updBlockCmp);
                }
            }
            catch
            {
                Utilities.showAlert("Internal server error.",
                    "divBlockCmpError", updBlockCmp);
            }
        }
        protected void chkShowBlockedCompany_CheckedChanged(object sender, EventArgs e)
        {
            processShowRptByChkBlockCmpStatus();
        }
        void processShowRptByChkBlockCmpStatus()
        {
            try
            {
                if (chkShowBlockedCompany.Checked)
                {
                    bindBlockedCmpRptAdmin();
                }
                else
                {
                    bindUnblockedRepeaterAdmin();
                }
            }
            catch (MficientException ex)
            {
                Utilities.showMessage(ex.Message, upd, DIALOG_TYPE.Error);
            }
            catch
            {
                Utilities.showMessage("Internal server error.", upd, DIALOG_TYPE.Error);
            }
        }
        void showHideLinkBlock(bool show)
        {
            if (show) lnkBlockCompany.Visible = true;
            else lnkBlockCompany.Visible = false;
        }
        void showHideLinkUnBlock(bool show)
        {
            if (show) lnkUnblockCompany.Visible = true;
            else lnkUnblockCompany.Visible = false;
        }
        void checkUncheckChkShowBlockedCmp(bool check)
        {
            if (check) chkShowBlockedCompany.Checked = true;
            else chkShowBlockedCompany.Checked = false;
        }

    }
}



﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true" CodeBehind="ChangePassUsers.aspx.cs" Inherits="mFicientAdmin.ChangePassUsers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">


<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="PageCanvasContent">
                <section>
                    <asp:Panel ID="pnlChangePassword" runat="server">
                        <div class="g12">
                            <div id="formDiv">
                                <fieldset>
                                    <section>
                                        <div>
                                            <span style="font-style: italic">
                                                <asp:Label ID="lblPasswordInfo" runat="server" Text="Password should be atleast 6 character long"></asp:Label></span>
                                        </div>
                                    </section>
                                    <section>
                                        <label for="txtOldPassword">
                                            Old Password</label>
                                        <div>
                                            <asp:TextBox ID="txtOldPassword" runat="server" Width="250" TextMode="Password"></asp:TextBox>
                                        </div>
                                    </section>
                                    <section>
                                        <label for="txtNewPassword">
                                            New Password</label>
                                        <div>
                                            <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" Width="250"></asp:TextBox>
                                        </div>
                                    </section>
                                    <section>
                                        <label for="txtReEnterPassword">
                                            ReType Password</label>
                                        <div>
                                            <asp:TextBox ID="txtReEnterPassword" runat="server" TextMode="Password" Width="250"></asp:TextBox>
                                        </div>
                                    </section>
                                </fieldset>
                                <section id="buttons">
                                    <asp:Button ID="btnSave" runat="server" CssClass="aspButton" OnClick="btnSave_Click"
                                        Text="Save" />
                                </section>
                            </div>
                        </div>
                    </asp:Panel>
                </section>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
   

<script type="text/javascript">
    Sys.Application.add_init(application_init);
    function application_init() {
        //Sys.Debug.trace("Application.Init");
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_initializeRequest);
        prm.add_endRequest(prm_endRequest);
    }
    function prm_initializeRequest() {
        
        showWaitModal();
    }
    function prm_endRequest() {
        
        hideWaitModal();
    }
    </script>


</asp:Content>

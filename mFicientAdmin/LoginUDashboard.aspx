﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="LoginUDashboard.aspx.cs" Inherits="mFicientAdmin.LoginUDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    
    <div id="divRepeater">
        <asp:UpdatePanel ID="upd" runat="server">
            <ContentTemplate>
                <section>
                    <div id="divInformation">
                    </div>
                </section>
                <section>
                    <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                        <section>
                            <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                <div>
                                    <asp:Label ID="lblInfo" runat="server" Text="<h1>ENQUIRY DETAILS</h1>"></asp:Label></div>
                                <div style="margin-left: 25px; float: left;">
                                    <asp:LinkButton ID="lnkdetails" runat="server" Style="position: relative; top: 14px;font-size:11.5px;"
                                        Text="( details )" PostBackUrl="~/ManageEnquiryDetailsSP.aspx"></asp:LinkButton>
                                </div>
                            </asp:Panel>
                        </section>
                        <asp:Repeater ID="rptUserDetails" runat="server">
                            <HeaderTemplate>
                                <table class="repeaterTable">
                                    <thead>
                                        <tr>
                                            <th>
                                                COMPANY
                                            </th>
                                            <th>
                                                COUNTRY
                                            </th>
                                            <th>
                                                EXPECTED WORKFLOW
                                            </th>
                                        </tr>
                                    </thead>
                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tbody>
                                    <tr class="repeaterItem">
                                        <td>
                                            <asp:Label ID="lblid" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblname" runat="server" Text='<%#Eval("COUNTRY_NAME") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lbltype" runat="server" Text='<%# Eval("EXPECTED_WF") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </tbody>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tbody>
                                    <tr class="repeaterItem">
                                        <td>
                                            <asp:Label ID="lblid" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblname" runat="server" Text='<%#Eval("COUNTRY_NAME") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lbltype" runat="server" Text='<%# Eval("EXPECTED_WF") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </tbody>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div>
                        </div>
                        <div style="clear: both">
                            <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                            <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                        </div>
                    </asp:Panel>
                </section>
                <div>
                    <asp:HiddenField ID="hdi" runat="server" />
                    <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>

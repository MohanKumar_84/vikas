﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Collections.ObjectModel;
namespace mFicientAdmin
{
    public partial class CompanyRegistration : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        enum HIDE_SHOW_TRIAL_PLAN_CONTROLS_CONDITION
        {
            NoneSelected,
            PlanSelected,
        }
        enum HIDE_SHOW_PAYMENT_PLAN_CONTROLS_CONDITION
        {
            NoneSelected,
            PlanSelected,
        }
        enum PLAN_SELECTED_TYPE
        {
            Paid,
            Trial
        }
        //ddlChargePrice removed from UI 7/8/2012 Mohan-- No longer required
        string EnquiryId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;




            if (!Page.IsPostBack)
            {
                //int iNumber = 0;
                //companyIdNotStartingWithADigit("05B28BBB8B4C8823ADAKDSFJKLDSKLSADKDSJKDSF2723324", 3);
                Literal ltlUserType = (Literal)this.Master.Master.FindControl("ltUserType");
                Utilities.BindCountryDropDown(ddlCompanyCountry);
                /*if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else
                {
                    if (Page.PreviousPage != null)
                    {
                        string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                        prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                        ContentPlaceHolder cphPageCanvas = (ContentPlaceHolder)PreviousPage.Master.Master.FindControl("form").FindControl("MainCanvas").FindControl("PageCanvas");
                        hfs.Value = ((HiddenField)cphPageCanvas.FindControl("hfs")).Value;
                        if (prvPageName == "ManageEnquiryDetails.aspx")
                        {
                            Label process = ((Label)cphPageCanvas.FindControl("lid"));
                            EnquiryId = process.Text;
                        }
                    }
                }*/
                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "HidePaymentTab", "hidePaymentTab();", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID,"hidePaymentTab();", true);
                //enableDisableSaveButton();
                //ddlPlans.SelectedIndex = 0;
                //BindPerMonthChargeDropDown(ddlPlans.SelectedValue);
                //getPlanDetails(ddlPlans.SelectedValue);
                BindPlansDropDown();
                bindTimezoneDropDown();
                if (string.IsNullOrEmpty(hfsValue))
                    Response.Redirect(@"Default.aspx");
                string Role = strRole.ToUpper();
                if (Role == "A")
                {
                    ltlUserType.Text = "mFicient Admin";
                }
                else if (Role == "R")
                {
                    ltlUserType.Text = "Reseller";
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "hideTrialTab();makeDatePickerWithMonthYear();", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "postBackScript", " $(\"select\").uniform();$(\"#content\").find('div.widgets').wl_Widget();makeTabAfterPostBack();showSelectedTabOnPostBack();$('#aspnetForm').wl_Form();doOnDocumentReady();", true);
                if (ddlPaymentType.SelectedValue.ToLower() == "paid")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "PostBackHide", "showPaymentTab();hideTrialTab();", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "PostBackHide", "hidePaymentTab();showTrialTab();", true);
                }
                if (!String.IsNullOrEmpty(hidPasswordEntered.Value))
                {
                    fillPasswordOnPostBack();
                }
            }
        }

        void fillPasswordOnPostBack()
        {
            //the hidden field should be cleared after saving and updating the data.
            if (!String.IsNullOrEmpty(hidPasswordEntered.Value))
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "FillPasswordAfterPostBack", "fillPasswordEntered();", true);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            CompanyEmailAdmin Id = new CompanyEmailAdmin();
            //string resellerId = Id.GetResellerId(hfs.Value.Split(',')[0]);
            string resellerId = Id.GetResellerId(strUserName);
            string strErrorAdmin, strErrorCompany, strErrorPayment, strErrorTrial;
            try
            {
                string strMessage = validateForm(out strErrorAdmin, out strErrorCompany, out strErrorPayment, out strErrorTrial);
            }
            catch
            {
                Utilities.showMessage("Internal server error.Please try again.", this.Page);
                return;
            }
            if (strErrorAdmin.Contains("<li>") || strErrorCompany.Contains("<li>") || strErrorPayment.Contains("<li>") || strErrorTrial.Contains("<li>"))
            {
                showAlert("There were some errors in the form.Please see the respective tabs for detailed errors.", "error");
                if (strErrorAdmin.Contains("<li>"))
                {
                    showAlert(strErrorAdmin, "Admin", "Testing1");
                }
                if (strErrorCompany.Contains("<li>"))
                {
                    showAlert(strErrorCompany, "Company", "Testing2");
                }
                if (ddlPaymentType.SelectedValue.ToLower() != "trial")
                {
                    if (strErrorPayment.Contains("<li>"))
                    {
                        showAlert(strErrorPayment, "Payment", "Testing3");
                    }
                }
                if (ddlPaymentType.SelectedValue.ToLower() == "trial")
                {
                    if (strErrorTrial.Contains("<li>"))
                    {
                        showAlert(strErrorTrial, "Trial Plan", "TrialError");
                    }
                }
                if (!String.IsNullOrEmpty(hidPriceAmountPayable.Value))
                {
                    CalculatePayableAmount();
                }
                return;
            }
            else
            {

                //in the payment tab we have to calculate the amount using javascript.when we are setting value using javascript
                //of a label then we can only access the innerHtml of the element,the value of the innerHtml is not accessible 
                //after postBack(labelId.Text).so the set value(using javascript) is stored in the hidden field which is 
                //used later while saving.Hence we had to use hidPriceAmountPayable
                try
                {
                    CompanyRegistrationNew objCompanyRegistration;
                    string strErrorDescription = "";
                    //long lngDateOfBirth = Utilities.convertDateToTicks(txtDOB.Text, out strErrorDescription);
                    if (!String.IsNullOrEmpty(strErrorDescription))
                    {
                        showAlert(strErrorDescription, "Admin", "Testing1");
                        return;
                    }
                    #region Previous Code
                    //if (ddlPaymentType.SelectedValue.ToLower() == "paid")
                    //{
                    //    //GetPlanList objPlanList = new GetPlanList(ddlPlans.SelectedValue);
                    //    GetPlanList objPlanList = new GetPlanList(lblPaymentPlanCode.Text, ddlCurrencyType.SelectedValue, Convert.ToInt32(ddlMonths.SelectedValue));
                    //    objPlanList.Process();
                    //    DataTable dtblPlanDetail = objPlanList.PlanDetails;
                    //    //string strCurrencyType = ddlCurrencyType.SelectedValue;


                    //    if (dtblPlanDetail.Rows.Count > 0)
                    //    {

                    //        objCompanyRegistration = new CompanyRegistrationNew(txtEmail.Text, txtPassword.Text, txtFirstName.Text.Trim(), txtMiddleName.Text,
                    //                         txtLastName.Text, txtMobileNo.Text, txtCompanyAddress1.Text, txtCompanyAddress2.Text, txtCompanyAddress3.Text, txtCompanyCity.Text,
                    //                         txtCompanyState.Text, ddlCompanyCountry.SelectedValue, txtCompanyZip.Text, lngDateOfBirth.ToString(), ddlGender.SelectedValue, txtCompanyName.Text
                    //                         , "", txtCompanyAddress1.Text, txtCompanyAddress2.Text, txtCompanyAddress3.Text, txtCompanyCity.Text,
                    //                         txtCompanyState.Text, ddlCompanyCountry.SelectedValue, txtCompanyZip.Text, txtSupportEmail.Text, txtSupportContact.Text, resellerId, lblPaymentPlanCode.Text
                    //                         , lblMaxWorkflow.Text, txtMaxUsers.Text, lblPaymentCurrency.Text, lblCharge.Text, ddlMonths.SelectedValue, txtMaxUsers.Text, hidPriceAmountPayable.Value.Split(',')[0],
                    //                         hidPriceAmountPayable.Value.Split(',')[1], hidPriceAmountPayable.Value.Split(',')[2], ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_PURCHASE).ToString(),
                    //                         false, EnquiryId, Convert.ToInt32(dtblPlanDetail.Rows[0]["PUSHMESSAGE_PERDAY"]), Convert.ToInt32(dtblPlanDetail.Rows[0]["PUSHMESSAGE_PERMONTH"])
                    //                         , Convert.ToString(dtblPlanDetail.Rows[0]["PLAN_NAME"])
                    //                         , txtCompanyId.Text);
                    //    }
                    //    else
                    //    {
                    //        throw new Exception();
                    //    }
                    //}
                    //else
                    //{
                    //    objCompanyRegistration = new CompanyRegistrationNew(txtEmail.Text, txtPassword.Text, txtFirstName.Text.Trim(), txtMiddleName.Text,
                    //                     txtLastName.Text, txtMobileNo.Text, txtCompanyAddress1.Text, txtCompanyAddress2.Text, txtCompanyAddress3.Text, txtCompanyCity.Text,
                    //                     txtCompanyState.Text, ddlCompanyCountry.SelectedValue, txtCompanyZip.Text, lngDateOfBirth.ToString(), ddlGender.SelectedValue, txtCompanyName.Text
                    //                     , "", txtCompanyAddress1.Text, txtCompanyAddress2.Text, txtCompanyAddress3.Text, txtCompanyCity.Text,
                    //                     txtCompanyState.Text, ddlCompanyCountry.SelectedValue, txtCompanyZip.Text, txtSupportEmail.Text, txtSupportContact.Text, resellerId
                    //                     , lblTrialPlanCode.Text, lblTrialMaxWorkFlows.Text, lblTrialNoOfUsers.Text, "0", "0", "0", lblTrialNoOfUsers.Text, "0", "0", "0", ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.TRIAL).ToString(), true, EnquiryId,
                    //                     Convert.ToInt32(lblTrialPushMsgPerDay.Text), Convert.ToInt32(lblTrialPushMsgPerMnth.Text)
                    //                     , lblTrialPlanName.Text, txtCompanyId.Text);
                    //}
                    #endregion
                    objCompanyRegistration = new CompanyRegistrationNew(
                        getCompanyAdminFromUIControls(),
                        getCurrentPlanFromUiFields(),
                        getCompanyInfoFromUiFields(),
                        getPurchaseLogFromUiFields()
                    );
                    objCompanyRegistration.Process(this.Context);
                    int intStatusCode = objCompanyRegistration.StatusCode;
                    string strDescription = objCompanyRegistration.StatusDescription;
                    if (intStatusCode == 0)
                    {
                        //createFolder(objCompanyRegistration.CompanyId);
                        Utilities.showMessage("Company registered successfully", this.Page, "second script", DIALOG_TYPE.Info);
                        hidTabSelected.Value = "0";
                        ClearControls();
                        clearTrialControls();
                        clearPaymentControls();
                    }
                    else
                    {
                        Utilities.showMessage("Internal server error.", this.Page);

                    }
                }
                catch
                {
                    if (ddlPaymentType.SelectedValue.ToLower() == "paid")
                    {
                        CalculatePayableAmount();
                    }
                    Utilities.showMessage("Internal server error.", this.Page);
                }
            }
        }
        public string validateForm(out string errorAdminTab, out string errorCompanyTab, out string errorPaymentTab, out string errorTrialTab)
        {
            string strMessage = "";
            errorAdminTab = "<ul style=\"position:relative;right:-3px; \">Admin";
            errorCompanyTab = "<ul style=\"position:relative;right:-3px; \">Company";
            errorPaymentTab = "<ul style=\"position:relative;right:-3px; \">Payment";
            errorTrialTab = "<ul style=\"position:relative;right:-3px; \">Trial Plan";

            //admin tab
            if (String.IsNullOrEmpty(txtFirstName.Text))
            {
                errorAdminTab += "<li>Please enter first name." + "</li>";
            }
            else if (!Utilities.IsValidString(txtFirstName.Text.Trim(), true, true, false, "-", 1, 50, false, false))
            {
                errorAdminTab += "<li>Please enter correct first name." + "</li>";
            }
            if (String.IsNullOrEmpty(txtEmail.Text))
            {
                errorAdminTab += "<li>Please enter email." + "</li>";
            }
            else if (!Utilities.IsValidEmail(txtEmail.Text))
            {
                errorAdminTab += "<li>Please enter correct email." + "</li>";
            }
            if (txtPassword.Text != txtreenterPassword.Text)
            {
                errorAdminTab = "<li>Please re-enter same password." + "</li>";
            }
            //company tab
            if (String.IsNullOrEmpty(txtCompanyId.Text))
            {
                errorCompanyTab += "<li>Please enter company id." + "</li>";
            }
            else
            {
                DoesCompanyExists objCheckCompExists = new DoesCompanyExists(txtCompanyId.Text);
                objCheckCompExists.Process();
                if (objCheckCompExists.StatusCode == 0)
                {
                    if (objCheckCompExists.Exists)
                    {
                        errorCompanyTab += "<li> CompanyName already exists" + "</li>";
                    }
                }
                else
                {
                    throw new Exception();
                }
            }

            if (String.IsNullOrEmpty(txtCompanyName.Text))
            {
                errorCompanyTab += "<li>Please enter company name." + "</li>";
            }
            else if (!Utilities.IsValidString(txtCompanyName.Text, true, true, false, "- .", 1, 50, false, false))
            {
                errorCompanyTab += "<li>Please enter correct company name." + "</li>";
            }
            if (String.IsNullOrEmpty(txtCompanyAddress1.Text))
            {
                errorCompanyTab += "<li>Please enter company address no." + "</li>";
            }

            else if (!Utilities.IsValidString(txtCompanyAddress1.Text, true, true, true, "~!@#$%^&*(){}|:<>?,./=-][ ", 1, 50, false, false))
            {
                errorCompanyTab += "<li>Please enter correct company address no." + "</li>";
            }
            if (String.IsNullOrEmpty(txtCompanyCity.Text))
            {
                errorCompanyTab += "<li>Please enter city." + "</li>";
            }
            else if (!Utilities.IsValidString(txtCompanyCity.Text, true, true, false, "-.'~ ", 1, 50, false, false))
            {
                errorCompanyTab += "<li>Please enter correct city." + "</li>";
            }

            if (String.IsNullOrEmpty(txtCompanyState.Text))
            {
                errorCompanyTab += "<li>Please enter Company state." + "</li>";
            }
            else if (!Utilities.IsValidString(txtCompanyState.Text, true, true, false, "-.'~ ", 1, 50, false, false))
            {
                errorCompanyTab += "<li>Please enter correct correct state." + "</li>";
            }
            if (ddlCompanyCountry.SelectedValue == "-1")
            {
                errorCompanyTab += "<li>Please select country." + "</li>";
            }

            if (String.IsNullOrEmpty(txtCompanyZip.Text))
            {
                errorCompanyTab += "<li>Please enter zip code." + "</li>";
            }
            else if (!Utilities.IsValidString(txtCompanyZip.Text, true, true, true, ".-", 3, 20, false, false))
            {
                errorCompanyTab += "<li>Please enter correct zip code." + "</li>";
            }
            //if (String.IsNullOrEmpty(txtEmail.Text))
            //{
            //    errorCompanyTab += "<li>Please enter support email." + "</li>";
            //}
            //else if (!Utilities.IsValidEmail(txtSupportEmail.Text))
            //{
            //    errorCompanyTab += "<li>Please enter correct support email." + "</li>";
            //}

            //if (String.IsNullOrEmpty(txtSupportContact.Text))
            //{
            //    errorCompanyTab += "<li>Please enter support contact no." + "</li>";
            //}
            //else if (!Utilities.validateMobileNo(txtSupportContact.Text))
            //{
            //    errorCompanyTab += "<li>Please enter correct support contact no." + "</li>";
            //}

            //Payment Tab
            if (ddlPaymentType.SelectedValue.ToLower() != "trial")
            {
                //if (ddlPlans.SelectedValue == "-1")
                //{
                //    errorPaymentTab += "<li>Please select a plan." + "</li>";
                //}
                if (lblPaymentPlanCode.Text == "")
                {
                    errorPaymentTab += "<li>Please select a plan." + "</li>";
                }
                if (ddlMonths.SelectedValue == "-1")
                {
                    errorPaymentTab += "<li>Please select a month." + "</li>";
                }
                if (!Utilities.IsValidString(txtMaxUsers.Text, false, false, true, "", 0, 20, false, false))
                {
                    errorPaymentTab += "<li>Please enter valid max users" + "</li>";
                }
                if (!String.IsNullOrEmpty(hidPriceAmountPayable.Value) || hidPriceAmountPayable.Value.Contains(","))
                    if (Convert.ToDecimal(hidPriceAmountPayable.Value.Split(',')[0]) < Convert.ToDecimal(txtPaymentDiscount.Text))
                    {
                        errorPaymentTab += "<li>Discount amount exceeds the Price.Please enter valid discount amount." + "</li>";
                    }
            }

            //trial tab
            if (ddlPaymentType.SelectedValue.ToLower() == "trial")
            {
                if (String.IsNullOrEmpty(lblTrialPlanCode.Text))
                {
                    errorTrialTab += "<li>Please select a plan." + "</li>";
                }
            }

            //not required
            //if (!Utilities.IsValidString(txtMiddleName.Text, true, true, false, "-", 0, 50, false, false))
            //{
            //    strMessage += "Please enter correct middle name." + "<br />";
            //}

            //if (!Utilities.IsValidString(txtLastName.Text, true, true, false, "-", 0, 50, false, false))
            //{
            //    strMessage += "Please enter correct last name." + "<br />";
            //}
            errorAdminTab += "</ul>";
            errorCompanyTab += "</ul>";
            errorPaymentTab += "</ul>";
            return strMessage;
        }
        #region New Coding
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateOfBirth"></param>
        /// <returns></returns>
        /// <exception cref="System.MficientException">Thrown when there is some internal error</exception>
        MFECompanyAdmin getCompanyAdminFromUIControls()
        {

            CompanyEmailAdmin objGetResellerDtl = new CompanyEmailAdmin();
            string resellerId = objGetResellerDtl.GetResellerId(strUserName);

            if (objGetResellerDtl.StatusCode != 0)
                throw new MficientException(
                    objGetResellerDtl.StatusDescription
                    );

            MFECompanyAdmin objCmpAdmin =
                new MFECompanyAdmin();
            objCmpAdmin.AdminId = String.Empty;
            objCmpAdmin.EmailId = txtEmail.Text;
            objCmpAdmin.Password = txtPassword.Text;
            objCmpAdmin.Firstname = txtFirstName.Text;
            objCmpAdmin.LastName = txtLastName.Text;
            objCmpAdmin.StreetAddress1 = txtCompanyAddress1.Text;
            objCmpAdmin.StreetAddress2 = txtCompanyAddress2.Text;
            objCmpAdmin.StreetAddress3 = txtCompanyAddress3.Text;
            objCmpAdmin.CityName = txtCompanyCity.Text;
            objCmpAdmin.CountryCode = ddlCompanyCountry.SelectedValue;
            objCmpAdmin.Zip = txtCompanyZip.Text;
            objCmpAdmin.Dob = long.MinValue;
            objCmpAdmin.RegistrationDatetime = DateTime.UtcNow.Ticks;
            objCmpAdmin.Gender = String.Empty;
            objCmpAdmin.MiddleName = txtMiddleName.Text;
            objCmpAdmin.MobileNo = String.Empty;
            objCmpAdmin.IsTrial = isPlanSelectedPaid() ? false : true;
            objCmpAdmin.ResellerId = resellerId;

            return objCmpAdmin;
        }
        MFECompanyInfo getCompanyInfoFromUiFields()
        {
            MFECompanyInfo objCmpInfo = new MFECompanyInfo();
            objCmpInfo.CompanyId = txtCompanyId.Text;
            objCmpInfo.CompanyName = txtCompanyName.Text;
            objCmpInfo.RegisterationNo = String.Empty;
            objCmpInfo.StreetAddress1 = txtCompanyAddress1.Text;
            objCmpInfo.StreetAddress2 = txtCompanyAddress2.Text;
            objCmpInfo.StreetAddress3 = txtCompanyAddress3.Text;
            objCmpInfo.CityName = txtCompanyCity.Text;
            objCmpInfo.StateName = txtCompanyState.Text;
            objCmpInfo.CountryCode = ddlCompanyCountry.SelectedValue;
            objCmpInfo.Zip = txtCompanyZip.Text;
            objCmpInfo.AdminId = String.Empty;
            objCmpInfo.LogoImageName = String.Empty;
            objCmpInfo.SupportEmail = txtEmail.Text;
            objCmpInfo.SupportContact = "";
            objCmpInfo.UpdatedOn = DateTime.UtcNow.Ticks;
            objCmpInfo.TimezoneId = ddlCmpTimeZone.SelectedValue;
            objCmpInfo.OemReseller = chkOemrClient.Checked ? (byte)1 : (byte)0;
            objCmpInfo.OemResellerId = String.Empty;
            return objCmpInfo;
        }
        MFECurrentPlan getCurrentPlanFromUiFields()
        {
            MFECurrentPlan objCurrentPlan = new MFECurrentPlan();
            objCurrentPlan.CompanyId = txtCompanyId.Text;
            PLAN_SELECTED_TYPE ePlanSelectedType = getPlanSelectedType();

            GetPlanList objPlanList = new GetPlanList(lblPaymentPlanCode.Text, ddlCurrencyType.SelectedValue, Convert.ToInt32(ddlMonths.SelectedValue));
            objPlanList.Process();
            DataTable dtblPlanDetail = objPlanList.PlanDetails;
            if (objPlanList.StatusCode != 0) throw new MficientException("Internal server error.");
            switch (ePlanSelectedType)
            {
                case PLAN_SELECTED_TYPE.Paid:
                    objCurrentPlan.PlanCode = lblPaymentPlanCode.Text;
                    objCurrentPlan.MaxWorkFlow = Convert.ToInt32(lblMaxWorkflow.Text);
                    objCurrentPlan.MaxUser = Convert.ToInt32(txtMaxUsers.Text);
                    objCurrentPlan.UserChargePerMonth = Convert.ToDouble(lblCharge.Text);
                    objCurrentPlan.ChargeType = lblPaymentCurrency.Text;
                    objCurrentPlan.Validity = Convert.ToDouble(ddlMonths.SelectedValue);
                    objCurrentPlan.PurchaseDate = DateTime.UtcNow.Ticks;
                    objCurrentPlan.PlanChangeDate = 0;
                    objCurrentPlan.PushMsgPerDay = Convert.ToInt32(dtblPlanDetail.Rows[0]["PUSHMESSAGE_PERDAY"]);
                    objCurrentPlan.PushMsgPerMonth = Convert.ToInt32(dtblPlanDetail.Rows[0]["PUSHMESSAGE_PERMONTH"]);
                    objCurrentPlan.BalanceAmnt = 0;
                    objCurrentPlan.NextMnthPlan = Convert.ToString(dtblPlanDetail.Rows[0]["PLAN_NAME"]);
                    break;
                case PLAN_SELECTED_TYPE.Trial:
                    objCurrentPlan.PlanCode = lblTrialPlanCode.Text;
                    objCurrentPlan.MaxWorkFlow = Convert.ToInt32(lblTrialMaxWorkFlows.Text);
                    objCurrentPlan.MaxUser = Convert.ToInt32(lblTrialNoOfUsers.Text);
                    objCurrentPlan.ChargeType = "0";
                    objCurrentPlan.UserChargePerMonth = 0;
                    objCurrentPlan.Validity = 12;
                    objCurrentPlan.PushMsgPerDay = Convert.ToInt32(lblTrialPushMsgPerDay.Text);
                    objCurrentPlan.PushMsgPerMonth = Convert.ToInt32(lblTrialPushMsgPerMnth.Text);
                    objCurrentPlan.BalanceAmnt = 0;
                    objCurrentPlan.NextMnthPlan = lblTrialPlanName.Text;
                    break;

            }
            objCurrentPlan.Feature3 = 0;
            objCurrentPlan.Feature4 = 0;
            objCurrentPlan.Feature5 = 0;
            objCurrentPlan.Feature6 = 0;
            objCurrentPlan.Feature7 = 0;
            objCurrentPlan.Feature8 = 0;
            objCurrentPlan.Feature9 = 0;
            objCurrentPlan.Feature10 = 0;
            objCurrentPlan.NextMnthPlanLastUpdated = 0;
            return objCurrentPlan;
        }
        MFEPlanPurchaseLog getPurchaseLogFromUiFields()
        {
            MFEPlanPurchaseLog objPlanPurchaseLog =
                new MFEPlanPurchaseLog();
            switch (getPlanSelectedType())
            {
                case PLAN_SELECTED_TYPE.Paid:
                    objPlanPurchaseLog.PriceWithoutDiscount = Convert.ToDouble(hidPriceAmountPayable.Value.Split(',')[0]);
                    objPlanPurchaseLog.DiscountAmount = Convert.ToDouble(hidPriceAmountPayable.Value.Split(',')[1]);
                    objPlanPurchaseLog.ActualPrice = Convert.ToDouble(hidPriceAmountPayable.Value.Split(',')[2]);
                    objPlanPurchaseLog.TransactionId = String.Empty;
                    objPlanPurchaseLog.TransactionType = ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_PURCHASE).ToString();
                    break;
                case PLAN_SELECTED_TYPE.Trial:
                    objPlanPurchaseLog.PriceWithoutDiscount = 0;
                    objPlanPurchaseLog.DiscountAmount = 0;
                    objPlanPurchaseLog.ActualPrice = 0;
                    objPlanPurchaseLog.TransactionId = String.Empty;
                    objPlanPurchaseLog.TransactionType = ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.TRIAL).ToString();
                    break;
            }
            return objPlanPurchaseLog;
        }
        bool isPlanSelectedPaid()
        {
            if (ddlPaymentType.SelectedValue.ToLower() == "paid") return true;
            else return false;
        }
        PLAN_SELECTED_TYPE getPlanSelectedType()
        {
            if (ddlPaymentType.SelectedValue.ToLower() == "paid")
                return PLAN_SELECTED_TYPE.Paid;
            else
                return PLAN_SELECTED_TYPE.Trial;
        }
        #region BindTimezoneDropDown
        void bindTimezoneDropDown()
        {
            ReadOnlyCollection<TimeZoneInfo> zones = getSystemTimezones();
            ddlCmpTimeZone.DataSource = getDataTableTimeZone(zones);
            ddlCmpTimeZone.DataTextField = "TIMEZONE_DISPLAY_TEXT";
            ddlCmpTimeZone.DataValueField = "ID";
            ddlCmpTimeZone.DataBind();
        }
        ReadOnlyCollection<TimeZoneInfo> getSystemTimezones()
        {
            ReadOnlyCollection<TimeZoneInfo> zones = TimeZoneInfo.GetSystemTimeZones();
            return zones;
        }
        DataTable getDataTableTimeZone(ReadOnlyCollection<TimeZoneInfo> zones)
        {
            DataTable dtblTimezone = new DataTable();
            dtblTimezone.Columns.Add("ID", typeof(string));
            dtblTimezone.Columns.Add("TIMEZONE_DISPLAY_TEXT", typeof(string));
            foreach (TimeZoneInfo zone in zones)
            {
                DataRow row = dtblTimezone.NewRow();
                row["ID"] = zone.Id;
                row["TIMEZONE_DISPLAY_TEXT"] = zone.DisplayName;
                dtblTimezone.Rows.Add(row);
            }
            return dtblTimezone;
        }
        #endregion
        #endregion
        protected void ClearControls()
        {
            txtFirstName.Text = "";
            txtMiddleName.Text = "";
            txtLastName.Text = "";
            txtEmail.Text = "";
            txtPassword.Text = "";
            txtreenterPassword.Text = "";
            //txtMobileNo.Text = "";
            //txtDOB.Text = "";
            txtCompanyName.Text = "";
            txtCompanyAddress1.Text = "";
            txtCompanyAddress2.Text = "";
            txtCompanyAddress3.Text = "";
            txtCompanyCity.Text = "";
            txtCompanyState.Text = "";
            txtCompanyZip.Text = "";
            hidPasswordEntered.Value = "";
            txtCompanyId.Text = "";
            hidReTypePasswordEntered.Value = "";
            chkOemrClient.Checked = false;
        }
        protected void clearTrialControls()
        {
            lblTrialPlanName.Text = "Please select a plan";
            lblTrialPlanCode.Text = "";
            lblTrialPlanId.Text = "";
            lblTrialNoOfUsers.Text = "";
            lblTrialMaxWorkFlows.Text = "";
            lblTrialPushMsgPerDay.Text = "";
            lblTrialPushMsgPerMnth.Text = "";
            //pnlTrialNoOfUsers.Visible = false;
            //pnlTrialMaxWrkFlow.Visible = false;
            //pnlTrialPushMsgPerDay.Visible = false;
            //pnlTrialPushMsgPerMnth.Visible = false;
            showHideControlsInTrialPlan(HIDE_SHOW_TRIAL_PLAN_CONTROLS_CONDITION.NoneSelected);
        }
        protected void clearPaymentControls()
        {
            lblPayableAmount.Text = "";
            lblPaymentCurrency.Text = "";
            lblPaymentPlanCode.Text = "";
            lblPaymentPlanName.Text = "";
            ddlMonths.SelectedIndex = ddlMonths.Items.IndexOf(ddlMonths.Items.FindByValue("-1"));
            txtPaymentDiscount.Text = "";
            txtMaxUsers.Text = "";
            lblPrice.Text = "";
            //lblDiscountAmount.Text = "";
            lblPayableAmount.Text = "";
            setDefaultTextOfPayment();
        }

        bool imageUploadFolderExists(string uploadingFolderPath)
        {
            //DirectoryInfo dirInfo = new DirectoryInfo("~/CompanyImages");
            DirectoryInfo dirInfo = new DirectoryInfo(uploadingFolderPath);
            if (dirInfo.Exists)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        bool companyImageFolderExists(string companyFolderPath)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(companyFolderPath);
            if (dirInfo.Exists)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //void createFolder(string companyId)
        //{
        //    DirectoryInfo dirInfo;
        //    string strUploadingFolderPath = Server.MapPath("~\\CompanyImages");
        //    string strCompanyFolderPath = Server.MapPath("~\\CompanyImages\\" + companyId);
        //    if (!imageUploadFolderExists(strUploadingFolderPath))
        //    {
        //        dirInfo = new DirectoryInfo(strUploadingFolderPath);
        //        dirInfo.Create();
        //    }
        //    if (!companyImageFolderExists(strCompanyFolderPath))
        //    {
        //        dirInfo = new DirectoryInfo(strCompanyFolderPath);
        //        dirInfo.Create();
        //    }
        //}

        protected void txtMiddleName_TextChanged(object sender, EventArgs e)
        {

        }
        protected void ddlPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //fillDetialsOnPlanOrCurrencyChange("repeater");
            enableDisableSaveButton();
            if (ddlPaymentType.SelectedValue == "Paid")
            {
                //hidTabSelected.Value = "1";
                if (hidTabSelected.Value == "4")//trial tab
                {
                    hidTabSelected.Value = "3";//payment tab
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "ShowPaymentTab", "showPaymentTab();hideTrialTab();showSelectedTabOnPostBack();", true);
                    clearTrialControls();
                }
            }
            else
            {
                //hidTabSelected.Value = "1";
                if (hidTabSelected.Value == "3")//payment tab
                {
                    hidTabSelected.Value = "4";//trial tab
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "HidePaymentTab", "hidePaymentTab();showTrialTab();showSelectedTabOnPostBack();", true);
                    clearPaymentControls();
                }
            }
        }
        void enableDisableSaveButton()
        {
            //if (ddlPaymentType.SelectedValue == "Paid")
            //{
            //    pnlCompanySaveBtn.Visible = false;
            //    pnlPaymentSaveBtn.Visible = true;
            //    btnSave.Visible = false;
            //    btnPaymentSave.Visible = true;
            //}
            //else
            //{
            //    pnlCompanySaveBtn.Visible = true;
            //    pnlPaymentSaveBtn.Visible = false;
            //    btnSave.Visible = true;
            //    btnPaymentSave.Visible = true;
            //}
        }

        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#buttons'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        void showAlert(string message, string divIdToShowMsg)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        void showAlert(string message, string divIdToShowMsg, string keyForScript)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), keyForScript, @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        protected DataTable getPlanDetails(string _planCode, int months, string currency)
        {
            GetPlanList objPlanList = new GetPlanList(_planCode, currency, months);
            objPlanList.Process();
            DataTable dtblPlanDtls = objPlanList.PlanDetails;
            //if (dtblPlanDtls != null)
            //{
            //    if (dtblPlanDtls.Rows.Count > 0)
            //    {
            //        lblMaxWorkflow.Text = Convert.ToString(dtblPlanDtls.Rows[0]["MAX_WORKFLOW"]);
            //    }
            //}
            return dtblPlanDtls;
        }
        void showMaxWorkFlowAndCharge(DataTable planDtlsDTbl)
        {
            if (planDtlsDTbl != null)
            {
                lblPerMonth.Visible = true;
                lblMaxWorkflow.Text = Convert.ToString(planDtlsDTbl.Rows[0]["MAX_WORKFLOW"]);
                //if (lblPaymentCurrency.Text.ToLower() == "usd")
                //{
                //    //lblCharge.Text = Convert.ToString(planDtlsDTbl.Rows[0]["USER_CHARGE_USD_PM"])+" "+"USD";
                //    lblCharge.Text = Convert.ToString(planDtlsDTbl.Rows[0]["USER_CHARGE_USD_PM"]);
                //}
                //else if (lblPaymentCurrency.Text.ToLower() == "inr")
                //{
                //    lblCharge.Text = Convert.ToString(planDtlsDTbl.Rows[0]["USER_CHARGE_INR_PM"]);
                //    //lblCharge.Text = Convert.ToString(planDtlsDTbl.Rows[0]["USER_CHARGE_INR_PM"])+" "+"INR";
                //}
                lblCharge.Text = Convert.ToString(planDtlsDTbl.Rows[0]["PRICE"]);
            }
            else
            {
                lblCharge.Text = "0";
                lblMaxWorkflow.Text = "0";
            }
            if (hidPriceAmountPayable.Value != "")
                CalculatePayableAmount();
        }
        //protected void GetCurrentPlanDetails(out string _PlanCode, out string _MaxWorkFlow, out string _ChargeType)
        //{
        //    _MaxWorkFlow = "";
        //    _PlanCode = "";
        //    _ChargeType = "";
        //    CurrentPlanDetail currentPlan = new CurrentPlanDetail(hfs.Value.Split(',')[2]);
        //    currentPlan.Process();
        //    DataTable dtCurrentPlanDtl = currentPlan.PlanDetails;
        //    if (dtCurrentPlanDtl != null)
        //    {
        //        if (dtCurrentPlanDtl.TableName == "Plan")
        //        {
        //            _PlanCode = Convert.ToString(dtCurrentPlanDtl.Rows[0]["PLAN_CODE"]);
        //            _MaxWorkFlow = Convert.ToString(dtCurrentPlanDtl.Rows[0]["MAX_WORKFLOW"]);
        //            _ChargeType = Convert.ToString(dtCurrentPlanDtl.Rows[0]["CHARGE_TYPE"]);
        //        }
        //        else
        //        {
        //            _PlanCode = "Trial";
        //            _MaxWorkFlow = ((int)TRIAL_ACCOUNT_VALIDATION.MAX_WORKFLOW).ToString();
        //            _ChargeType = "0";
        //        }
        //    }
        //}
        protected void BindPlansDropDown()
        {
            //this part is not required.
            //if (hfdPlanCode.Value != "")
            {
                //Utilities.BindPlansDropDown(ddlPlans);//26/9/2012
            }
            //else
            //{
            //    //but this code is working properly
            //    string strMaxWorkFlow, strPlancode, strChargeType;
            //    GetCurrentPlanDetails(out strPlancode, out strMaxWorkFlow, out strChargeType);
            //    UpgradeCurrentPlan objPlanList = new UpgradeCurrentPlan();
            //    objPlanList.GetPlanList(strMaxWorkFlow);
            //    DataTable dt = objPlanList.PlanList;
            //    ddlPlans.DataSource = dt;
            //    ddlPlans.DataValueField = "PLAN_CODE";
            //    ddlPlans.DataTextField = "PLAN_NAME";
            //    ddlPlans.DataBind();
            //}
        }
        protected void BindPerMonthChargeDropDown(string _planCode)
        {
            GetPlanList objPlanList = new GetPlanList(_planCode);
            objPlanList.Process();
            DataTable dtblPlanDtls = objPlanList.PlanDetails;
            //if (hfdPlanCode.Value != "")
            //{
            //    ListItem inr = new ListItem("INR", Convert.ToString(dtblPlanDtls.Rows[0]["USER_CHARGE_INR_PM"]));
            //    ListItem usd = new ListItem("USD", Convert.ToString(dtblPlanDtls.Rows[0]["USER_CHARGE_USD_PM"]));
            //    ddlChargePrice.Items.Add(inr);
            //    ddlChargePrice.Items.Add(usd);
            //}
            //else
            //{
            //    string strMaxWorkFlow, strPlancode, strChargeType;
            //    GetCurrentPlanDetails(out strPlancode, out strMaxWorkFlow, out strChargeType);
            //    if (strChargeType == "INR")
            //    {
            //        ListItem inr = new ListItem("INR", Convert.ToString(dtblPlanDtls.Rows[0]["USER_CHARGE_INR_PM"]));
            //        ddlChargePrice.Items.Add(inr);
            //    }
            //    else if (strChargeType == "USD")
            //    {
            //        ListItem usd = new ListItem("USD", Convert.ToString(dtblPlanDtls.Rows[0]["USER_CHARGE_USD_PM"]));
            //        ddlChargePrice.Items.Add(usd);
            //    }
            //}
            //lblCharge.Text = ddlChargePrice.SelectedValue;
            //pnlChargePriceContainer.Visible = true;


            //ddlChargePrice removed from UI 7/8/2012 Mohan-- No longer required
        }
        protected void ddlChargePrice_SelectedIndexChanged(object sender, EventArgs e)
        {
            //lblCharge.Text = ddlChargePrice.SelectedValue;
            //CalculatePayableAmount();
        }
        protected void CalculatePayableAmount()
        {

            decimal decDiscount = 0, decPrice = 0, decChargePerUserPerMonth = 0;
            if (txtMaxUsers.Text == "")
            {
                txtMaxUsers.Text = "1";
            }
            //lblPrice.Text = (Convert.ToDecimal(txtMaxUsers.Text) * Convert.ToDecimal(lblCharge.Text) * Convert.ToDecimal(ddlMonths.SelectedValue)).ToString();

            try
            {
                decChargePerUserPerMonth = decimal.Parse(lblCharge.Text);
            }
            catch
            { }
            if (ddlMonths.SelectedValue != "-1")
            {
                decPrice = (Convert.ToDecimal(txtMaxUsers.Text) * Convert.ToDecimal(decChargePerUserPerMonth) * Convert.ToDecimal(ddlMonths.SelectedValue));
                lblPrice.Text = decPrice.ToString();
            }
            else
            {
                lblPrice.Text = decPrice.ToString();
            }
            try
            {
                decDiscount = decimal.Parse(txtPaymentDiscount.Text);
            }
            catch
            {
                Utilities.showMessage("Please enter valid discount amount", this.Page, "Error Discount", DIALOG_TYPE.Error);
                return;
            }
            if (decPrice < decDiscount)
            {
                Utilities.showMessage("Discount amount exceeds the Price.Please enter valid discount amount.", this.Page, "Error Message Invalid Discount", DIALOG_TYPE.Error);
                return;
            }
            //lblDiscountAmount.Text = ((Convert.ToDecimal(decDiscount) * Convert.ToDecimal(lblPrice.Text)) / 100).ToString();
            lblPayableAmount.Text = (Convert.ToDecimal(lblPrice.Text) - Convert.ToDecimal(txtPaymentDiscount.Text)).ToString();

            //return 0;

            //in the payment tab we have to calculate the amount using javascript.when we are setting value using javascript
            //of a label then we can only access the innerHtml of the element,the value of the innerHtml is not accessible 
            //after postBack(labelId.Text).so the set value(using javascript) is stored in the hidden field which is 
            //used later while saving.Hence we had to use hidPriceAmountPayable.After post back we have to set this value.
            hidPriceAmountPayable.Value = lblPrice.Text + "," + txtPaymentDiscount.Text + "," + lblPayableAmount.Text;
        }
        //protected DataTable GetDiscountPrice()
        //{
        //    // bool flag = false;
        //    GetPlanList objPlanList = new GetPlanList();
        //    objPlanList.PlanDiscounts();
        //    DataTable dtblPlanDiscounts = objPlanList.PlanDiscountList;
        //    //foreach (DataRow row in dtblPlanDiscounts.Rows)
        //    //{
        //    //    if (Convert.ToBoolean(row["ENABLED"].ToString()) == true)
        //    //    {
        //    //        string fromMonth = row["FROM_MONTH"].ToString();
        //    //        string toMonth = row["TO_MONTH"].ToString();
        //    //        if (ddlMonths.SelectedValue == fromMonth || ddlMonths.SelectedValue == toMonth)
        //    //        {
        //    //            lblDiscount.Text = row["DISCOUNT"].ToString();
        //    //            flag = true;
        //    //        }
        //    //        else
        //    //        {
        //    //            flag = false;
        //    //        }
        //    //    }
        //    //}
        //    //if (!flag)
        //    //{
        //    //    lblDiscount.Text = "0";
        //    //}
        //    return dtblPlanDiscounts;
        //}
        //void showDiscount(DataTable planDiscountDtlsDTbl)
        //{
        //    string filter = String.Format("FROM_MONTH >= '{0}' AND TO_MONTH>='{1}'", ddlMonths.SelectedValue, ddlMonths.SelectedValue);
        //    DataRow[] rows = planDiscountDtlsDTbl.Select(filter);
        //    if (rows.Length > 0)
        //    {
        //        //there should be only one row selected.
        //        lblDiscount.Text = (string)rows[0]["DISCOUNT"] + " " + "%";
        //    }
        //    else
        //    {
        //        lblDiscount.Text = "0";
        //    }
        //}
        protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlMonths.SelectedValue == "-1")
            //{
            //    clearCalculatedAmount("Months");
            //    return;
            //}
            //showDiscount(GetDiscountPrice());
            //CalculatePayableAmount();

            try
            {
                if (String.IsNullOrEmpty(lblPaymentPlanCode.Text))
                {
                    return;
                }
                else
                {
                    fillDetialsOnPlanOrCurrencyChange("dropdown", lblPaymentPlanCode.Text);
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error", this.Page, "Internal error", DIALOG_TYPE.Error);
            }
        }
        protected void ddlPlans_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ddlChargePrice.Items.Clear();
            //BindPerMonthChargeDropDown(ddlPlans.SelectedValue);
            //CalculatePayableAmount();
            //if (ddlPlans.SelectedValue == "-1")
            //{
            //    clearCalculatedAmount("Plans");
            //    return;
            //}
            //DataTable dtblPlanDetails = getPlanDetails(ddlPlans.SelectedValue);
            //showMaxWorkFlowAndCharge(dtblPlanDetails);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="changedDropDown">"Plans" "Months"</param>
        void clearCalculatedAmount(string changedDropDown)
        {
            if (changedDropDown.ToLower() == "plans")
            {
                lblMaxWorkflow.Text = "Please select a Plan to get Max workflow";
                lblCharge.Text = "Please select a plan to get related charges";
            }
            else if (changedDropDown.ToLower() == "months")
            {
                //lblDiscount.Text = "Please select a month to get discount available";
                txtPaymentDiscount.Text = "";
            }
            txtMaxUsers.Text = "";
            lblPrice.Text = "0";
            //lblDiscountAmount.Text = "0";
            lblPayableAmount.Text = "0";
            hidPriceAmountPayable.Value = "";
        }
        protected void ddlCurrencyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //26/9/2012
            //if (ddlPlans.SelectedValue != "-1")
            //{
            //    //GetPlanList objPlanList = new GetPlanList(null);
            //    //objPlanList.Process();
            //    //DataTable dt = objPlanList.PlanDetails;
            //    //string strFilter = String.Format("PLAN_CODE = '{0}'", ddlPlans.SelectedValue);
            //    //DataRow[]row= dt.Select(strFilter);
            //    //if (row.Length > 0)
            //    //{
            //    //    lblCharge
            //    //}
            //    DataTable dtblPlanDetails = getPlanDetails(ddlPlans.SelectedValue);
            //    showMaxWorkFlowAndCharge(dtblPlanDetails);
            //}
            //

            //16/10/2012
            try
            {
                if (String.IsNullOrEmpty(lblPaymentPlanCode.Text))
                {
                    return;
                }
                else
                {
                    fillDetialsOnPlanOrCurrencyChange("dropdown", lblPaymentPlanCode.Text);
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error", this.Page, "Internal error", DIALOG_TYPE.Error);
            }
        }
        protected void txtMaxUsers_TextChanged(object sender, EventArgs e)
        {
            CalculatePayableAmount();
        }
        protected void btnPurchase_Click(object sender, EventArgs e)
        {
            AddCurrentPlan();
            AddCurrentPlanPurchaseHistory();
        }
        protected void AddCurrentPlan()
        {
            //AddCurrentPlan addCurrentPlan = new AddCurrentPlan();
            //addCurrentPlan.CompanyId = hfs.Value.Split(',')[2];
            //addCurrentPlan.ChargePM = lblCharge.Text;
            //addCurrentPlan.UserChargeType = "";//ddlChargePrice.SelectedItem.Text;
            //addCurrentPlan.MaxUsers = txtMaxUsers.Text;
            //addCurrentPlan.MaxWorkFlow = lblMaxWorkflow.Text;
            //addCurrentPlan.Validity = ddlMonths.SelectedValue;
            ////addCurrentPlan.PlanCode = ddlPlans.SelectedValue;
            //addCurrentPlan.PlanCode = lblPaymentPlanCode.Text;//26/9/2012
            //addCurrentPlan.Process();
        }
        protected void AddCurrentPlanPurchaseHistory()
        {
            AddCurrentPlanPurchaseHistory addCurrentPlanPurchaseHistory = new AddCurrentPlanPurchaseHistory();
            //addCurrentPlanPurchaseHistory.CompanyId = hfs.Value.Split(',')[2];
            //addCurrentPlanPurchaseHistory.ChargePM = lblCharge.Text;
            //addCurrentPlanPurchaseHistory.UserChargeType = "";//ddlChargePrice.SelectedItem.Text;
            //addCurrentPlanPurchaseHistory.MaxUsers = txtMaxUsers.Text;
            //addCurrentPlanPurchaseHistory.MaxWorkFlow = lblMaxWorkflow.Text;
            //addCurrentPlanPurchaseHistory.Validity = ddlMonths.SelectedValue;
            ////addCurrentPlanPurchaseHistory.PlanCode = ddlPlans.SelectedValue;//26/9/2012
            //addCurrentPlanPurchaseHistory.PlanCode = lblPaymentPlanCode.Text;
            //addCurrentPlanPurchaseHistory.PriceWithoutDiscount = lblPrice.Text;
            //addCurrentPlanPurchaseHistory.DiscountPercent = txtPaymentDiscount.Text;
            //addCurrentPlanPurchaseHistory.ActualPrice = lblPayableAmount.Text;
            //addCurrentPlanPurchaseHistory.TransactionType = "Plan Purchase";
            //addCurrentPlanPurchaseHistory.AdminId = hfs.Value.Split(',')[3];
            //addCurrentPlanPurchaseHistory.TotalUsers = txtMaxUsers.Text;
            //addCurrentPlanPurchaseHistory.Process();
        }

        void showPlanList(bool isTrial)
        {
            int iBindSuccessfull;
            if (isTrial)
            {
                iBindSuccessfull = bindTrialPlanDetailsRpt();
            }
            else
            {
                iBindSuccessfull = bindPlanDetailsRpt();
            }
            if (iBindSuccessfull == 0)
            {
                if (isTrial)
                {
                    Utilities.showModalPopup("divTrialPlanModal", this.Page, "Plan Details", "600", false, false);
                    Utilities.showModalPopup("divTrialPlanModal", this.Page, "Plan Details", "650", false);
                }

                Utilities.showModalPopup("divModalContainer", this.Page, "Plan Details", "600", false, false);
                Utilities.showModalPopup("divModalContainer", this.Page, "Plan Details", "650", false);
            }
        }

        int bindPlanDetailsRpt()
        {
            int iBindSuccessfull = -1000;//error
            GetPlanList objPlanList = new GetPlanList(true);
            objPlanList.Process();
            DataTable dtblPlanDtls = objPlanList.PlanDetails;
            if (dtblPlanDtls != null)
            {
                if (dtblPlanDtls.Rows.Count > 0)
                {
                    DataView dv = dtblPlanDtls.DefaultView;
                    dv.Sort = "PLAN_NAME";
                    dtblPlanDtls = dv.ToTable();
                    rptPlanDetails.DataSource = dtblPlanDtls;
                    rptPlanDetails.DataBind();
                    iBindSuccessfull = 0;
                }
                else
                {
                    Utilities.showMessage("No Plan found.", this.Page, "Error message");
                    dtblPlanDtls = null;
                    rptPlanDetails.DataSource = dtblPlanDtls;
                    rptPlanDetails.DataBind();
                }
            }
            else
            {
                Utilities.showMessage("Internal server error.Please try again", this.Page, "Error message");
            }
            return iBindSuccessfull;
        }

        int bindTrialPlanDetailsRpt()
        {
            int iBindSuccessfull = -1000;//error
            GetTrialPlanList objPlanList = new GetTrialPlanList();
            objPlanList.Process();
            DataTable dtblPlanDtls = objPlanList.PlanDetails;
            if (dtblPlanDtls != null)
            {
                if (dtblPlanDtls.Rows.Count > 0)
                {
                    DataView dv = dtblPlanDtls.DefaultView;
                    dv.Sort = "PLAN_NAME";
                    dtblPlanDtls = dv.ToTable();
                    rptTrialPlanDetails.DataSource = dtblPlanDtls;
                    rptTrialPlanDetails.DataBind();
                    iBindSuccessfull = 0;
                }
                else
                {
                    Utilities.showMessage("No Plan found.", this.Page, "Error message");
                    dtblPlanDtls = null;
                    rptTrialPlanDetails.DataSource = dtblPlanDtls;
                    rptTrialPlanDetails.DataBind();
                }
            }
            else
            {
                Utilities.showMessage("Internal server error.Please try again", this.Page, "Error message");
            }
            return iBindSuccessfull;
        }

        protected void rptPlanDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "viewprice":
                    processShowPriceOfPlan(e);
                    break;
                case "select":
                    processNewPlanSelection(e);
                    break;
            }

        }
        void processShowPriceOfPlan(RepeaterCommandEventArgs e)
        {
            Label lblRptPlanCode = (Label)e.Item.FindControl("lblPlanCode");
            showPricingOfPlan(lblRptPlanCode.Text);
        }
        void showPricingOfPlan(string planCode)
        {
            int iError = bindPlanPriceRpt(planCode);
            if (iError == 0)
            {
                Utilities.showModalPopup("divPlanPriceDetailModal", this.Page, "Price List", "600", true, false);
            }
        }
        protected void rptPlanDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                RadioButtonList rdLstCurrency = (RadioButtonList)e.Item.FindControl("rdLstCurrency");
                Label lblCurrencyType = (Label)e.Item.FindControl("lblCurrencyType");
                //if (hidPlanChangeType.Value.ToLower() != "new")
                //{
                //    rdLstCurrency.Visible = false;
                //    lblCurrencyType.Visible = true;
                //    lblCurrencyType.Text = lblChargeType.Text;
                //}
                //else
                //{
                //    rdLstCurrency.Visible = true;
                //    lblCurrencyType.Visible = false;
                //    //lblCurrencyType.Text = lblChargeType.Text;
                //}
            }
        }

        void processNewPlanSelection(RepeaterCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "select")
            {
                try
                {
                    Label lblRptPlanCode = ((Label)e.Item.FindControl("lblPlanCode"));
                    fillDetialsOnPlanOrCurrencyChange("repeater", lblRptPlanCode.Text);
                    lblPaymentPlanCode.Text = lblRptPlanCode.Text;
                    lblPaymentPlanName.Text = ((Label)e.Item.FindControl("lblPlanName")).Text;
                }
                catch (Exception ex)
                {
                    //Utilities.showMessage("Internal server error", this.Page, "Error Message", DIALOG_TYPE.Error);
                    Utilities.showMessage(ex.Message, this.Page, "Show Error", DIALOG_TYPE.Error);
                }

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="controlWhoseValueChanged">pass repeater when change in plan or pass dropdown for currency type change</param>
        void fillDetialsOnPlanOrCurrencyChange(string controlWhoseValueChanged, string planCode)
        {
            try
            {
                if (ddlMonths.SelectedValue == "-1")
                {
                    Utilities.showMessage("Please select a month", this.Page, "Error selection Month", DIALOG_TYPE.Error);
                    throw new Exception();
                }
                DataTable dtblPlanDetails = getPlanDetails(planCode, Convert.ToInt32(ddlMonths.SelectedValue), ddlCurrencyType.SelectedValue);
                //RadioButtonList rdLstCurrency = (RadioButtonList)e.Item.FindControl("rdLstCurrency");
                //lblPaymentCurrency.Text = rdLstCurrency.SelectedValue;

                if (dtblPlanDetails != null)
                {
                    if (dtblPlanDetails.Rows.Count > 0)
                    {
                        showMaxWorkFlowAndCharge(dtblPlanDetails);
                        if (txtMaxUsers.Text != "")
                        {
                            CalculatePayableAmount();
                        }
                        if (controlWhoseValueChanged.ToLower() == "repeater")
                        {
                            Utilities.closeModalPopUp("divModalContainer", this.Page, "Close Modal");
                        }
                        showHideControlsInPaymentTab(HIDE_SHOW_PAYMENT_PLAN_CONTROLS_CONDITION.PlanSelected);
                        lblPaymentCurrency.Text = ddlCurrencyType.SelectedValue;
                    }
                    else
                    {
                        //Utilities.showMessage("Internal server error.Please try again", this.Page);
                        throw new Exception("Internal server error");
                    }
                }
                else
                {
                    //Utilities.showMessage("Internal server error.Please try again", this.Page);
                    throw new Exception("Internal server error");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void processPlanSelection(RepeaterCommandEventArgs e)
        {
            if (e.CommandName.ToLower() == "select")
            {

            }
        }

        protected void btnPlanDetModalCancel_Click(object sender, EventArgs e)
        {
            Button btnSender = (Button)sender;
            if (btnSender.CommandArgument.ToLower() == "trial")
            {
                Utilities.closeModalPopUp("divTrialPlanModal", this.Page, "CloseTrialModal");
            }
            else
            {
                Utilities.closeModalPopUp("divModalContainer", this.Page, "ClosePaymentModal");
            }
        }

        protected void lnkTrialSelectPlan_Click(object sender, EventArgs e)
        {
            showPlanList(true);
        }

        protected void rptTrialPlanDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            processTrialPlanSelect(e);
        }

        protected void rptTrialPlanDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                RadioButtonList rdLstCurrency = (RadioButtonList)e.Item.FindControl("rdLstCurrency");
                Label lblCurrencyType = (Label)e.Item.FindControl("lblCurrencyType");
                //if (hidPlanChangeType.Value.ToLower() != "new")
                //{
                //    rdLstCurrency.Visible = false;
                //    lblCurrencyType.Visible = true;
                //    lblCurrencyType.Text = lblChargeType.Text;
                //}
                //else
                //{
                //    rdLstCurrency.Visible = true;
                //    lblCurrencyType.Visible = false;
                //    //lblCurrencyType.Text = lblChargeType.Text;
                //}
            }
        }
        void processTrialPlanSelect(RepeaterCommandEventArgs e)
        {
            try
            {
                Label lblPlanCode = (Label)e.Item.FindControl("lblPlanCode");
                Label lblPlanId = (Label)e.Item.FindControl("lblPlanId");
                Label lblPlanName = (Label)e.Item.FindControl("lblPlanName");
                Label lblWorkFlow = (Label)e.Item.FindControl("lbWorkFlow");
                Label lblNoOfUsers = (Label)e.Item.FindControl("lblNoofUsers");
                Label lblPushMessagePerDay = (Label)e.Item.FindControl("lblPushMessagePerDay");
                Label lblPushMessagePerMonth = (Label)e.Item.FindControl("lblPushMessagePerMonth");
                lblTrialPlanCode.Text = lblPlanCode.Text;
                lblTrialPlanId.Text = lblPlanId.Text;
                lblTrialPlanName.Text = lblPlanName.Text;
                lblTrialMaxWorkFlows.Text = lblWorkFlow.Text;
                lblTrialPushMsgPerDay.Text = lblPushMessagePerDay.Text;
                lblTrialPushMsgPerMnth.Text = lblPushMessagePerMonth.Text;
                lblTrialNoOfUsers.Text = lblNoOfUsers.Text;
                showHideControlsInTrialPlan(HIDE_SHOW_TRIAL_PLAN_CONTROLS_CONDITION.PlanSelected);
                Utilities.closeModalPopUp("divTrialPlanModal", this.Page);
            }
            catch
            {

            }
        }

        void showHideControlsInTrialPlan(HIDE_SHOW_TRIAL_PLAN_CONTROLS_CONDITION trialPlanCondition)
        {
            switch (trialPlanCondition)
            {
                case HIDE_SHOW_TRIAL_PLAN_CONTROLS_CONDITION.NoneSelected:
                    //lblTrialPlanId.Text = "";
                    lblTrialPlanCode.Text = "";
                    lblTrialPlanName.Text = "Please select a plan";
                    pnlTrialNoOfUsers.Visible = false;
                    pnlTrialMaxWrkFlow.Visible = false;
                    pnlTrialPushMsgPerDay.Visible = false;
                    pnlTrialPushMsgPerMnth.Visible = false;
                    lnkTrialSelectPlan.Visible = true;
                    lnkTrialPlanChange.Visible = false;
                    break;
                case HIDE_SHOW_TRIAL_PLAN_CONTROLS_CONDITION.PlanSelected:
                    //lblTrialPlanId.Text = "";
                    //lblTrialPlanCode.Text = "";
                    //lblTrialPlanName.Text = "No plans selected yet";
                    pnlTrialNoOfUsers.Visible = true;
                    pnlTrialMaxWrkFlow.Visible = true;
                    pnlTrialPushMsgPerDay.Visible = true;
                    pnlTrialPushMsgPerMnth.Visible = true;
                    lnkTrialSelectPlan.Visible = false;
                    lnkTrialPlanChange.Visible = true;
                    break;
            }
        }

        void showHideControlsInPaymentTab(HIDE_SHOW_PAYMENT_PLAN_CONTROLS_CONDITION condition)
        {
            switch (condition)
            {
                case HIDE_SHOW_PAYMENT_PLAN_CONTROLS_CONDITION.NoneSelected:
                    lnkPaymentChangePlan.Visible = true;
                    lnkPaymentSelectPlan.Visible = true;
                    break;
                case HIDE_SHOW_PAYMENT_PLAN_CONTROLS_CONDITION.PlanSelected:
                    lnkPaymentSelectPlan.Visible = false;
                    lnkPaymentChangePlan.Visible = true;
                    break;
            }
        }
        protected void lnkTrialPlanChange_Click(object sender, EventArgs e)
        {
            showPlanList(true);
        }

        protected void lnkPaymentSelectPlan_Click(object sender, EventArgs e)
        {
            showPlanList(false);
        }

        protected void lnkPaymentChangePlan_Click(object sender, EventArgs e)
        {

            //CalculatePayableAmount();
            showPlanList(false);
        }

        void setDefaultTextOfPayment()
        {
            string strCommon = "Please select a plan";
            lblPaymentPlanName.Text = strCommon;
            lblPaymentCurrency.Text = strCommon;
            lblMaxWorkflow.Text = strCommon + " to get max work flow";
            lblCharge.Text = strCommon + " to get related charges";
            //lblDiscount.Text = "Please select a month to get any discount available";
            lblPrice.Text = "0";
            //lblDiscountAmount.Text = "0";
            lblPayableAmount.Text = "0";
            lblPerMonth.Visible = false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="planCode">pass null for complete list</param>
        /// <returns></returns>
        int bindPlanPriceRpt(string planCode)
        {
            int iError = -1000;//error

            GetPlanPrices objPlanPrices = null;
            if (String.IsNullOrEmpty(planCode))
            {
                objPlanPrices = new GetPlanPrices();
            }
            else
            {
                objPlanPrices = new GetPlanPrices(planCode);
            }
            objPlanPrices.Process();
            if (objPlanPrices.StatusCode == 0)
            {
                if (objPlanPrices.ResultTable.Rows.Count > 0)
                {
                    DataTable dtblModifiedTable = getModifiedPlanPriceRpt(objPlanPrices.ResultTable);
                    rptPlanPriceDtls.DataSource = dtblModifiedTable;
                    rptPlanPriceDtls.DataBind();
                    iError = 0;
                }
            }
            else
            {
                iError = -1000;
                Utilities.showMessage(objPlanPrices.StatusDescription, this.Page, "Error PlanPrice", DIALOG_TYPE.Error);
            }
            return iError;
        }

        DataTable getModifiedPlanPriceRpt(DataTable planPriceDtl)
        {
            DataTable dtblModifiedPlanPrice = new DataTable();
            dtblModifiedPlanPrice.Columns.Add("PLAN_CODE", typeof(string));
            dtblModifiedPlanPrice.Columns.Add("PLAN_NAME", typeof(string));
            dtblModifiedPlanPrice.Columns.Add("CURRENCY", typeof(string));
            dtblModifiedPlanPrice.Columns.Add("ONE_TWO", typeof(string));
            dtblModifiedPlanPrice.Columns.Add("THREE_FIVE", typeof(string));
            dtblModifiedPlanPrice.Columns.Add("SIX_TWELVE", typeof(string));
            dtblModifiedPlanPrice.Columns.Add("TWELVE_TWELVE", typeof(string));
            string strFilter = "";
            foreach (DataRow row in planPriceDtl.Rows)
            {
                strFilter = String.Format("PLAN_CODE='{0}' AND CURRENCY ='{1}'", row["PLAN_CODE"], row["CURRENCY"]);
                DataRow[] drOfModifiedTable = dtblModifiedPlanPrice.Select(strFilter);
                string strColumnName = "";
                int iFromMonth = Convert.ToInt32(row["FROM_MONTH"]);
                switch (iFromMonth)
                {
                    case 1:
                        strColumnName = "ONE_TWO";
                        break;
                    case 3:
                        strColumnName = "THREE_FIVE";
                        break;
                    case 6:
                        strColumnName = "SIX_TWELVE";
                        break;
                    case 12:
                        strColumnName = "TWELVE_TWELVE";
                        break;
                }
                if (drOfModifiedTable.Length > 0)
                {
                    drOfModifiedTable[0][strColumnName] = Convert.ToString(row["PRICE"]);
                }
                else
                {
                    DataRow newRow = dtblModifiedPlanPrice.NewRow();
                    newRow["PLAN_CODE"] = Convert.ToString(row["PLAN_CODE"]);
                    newRow["PLAN_NAME"] = Convert.ToString(row["PLAN_NAME"]);
                    newRow["CURRENCY"] = Convert.ToString(row["CURRENCY"]);
                    newRow[strColumnName] = Convert.ToString(row["PRICE"]);
                    dtblModifiedPlanPrice.Rows.Add(newRow);
                }
                dtblModifiedPlanPrice.AcceptChanges();
            }
            return dtblModifiedPlanPrice;
        }
        protected void rptPlanPriceDtls_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }
        protected void rptPlanPriceDtls_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }


    }
}
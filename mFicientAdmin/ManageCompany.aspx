﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="ManageCompany.aspx.cs" Inherits="mFicientAdmin.ManageCompany" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script type="text/javascript">
        function processConfirmBlock() {
            var confirmResult = confirm('Are you sure you want to block the company.');
            if (confirmResult) {
                showModalPopUpWithOutHeader('divBlockCmpReason', 'Reason for Blocking', '400', false);
                return false;
            }
            else {
                return confirmResult;
            }
        }
        function processConfirmUnBlock() {
            var confirmResult = confirm('Are you sure you want to unblock the company.');
            if (confirmResult) {
                showModalPopUpWithOutHeader('divUnblockCmpReason', 'Reason for unblocking', '400', false);
                return false;
            }
            else {
                return confirmResult;
            }
        }
    </script>
    <style type="text/css">
        .checker + label
        {
            float: left;
            position: relative;
            top: 1px;
            padding: 2px 0px 2px 0px;
            height: 23px;
        }
        #divUnblockCmpReason div.selector > span
        {
            width: 200px;
        }
        #divBlockCmpReason div.selector > span
        {
            width: 200px;
        }
        .reasonContainer
        {
            margin-top: 15px;
            margin-left: 25px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>COMPANY DETAILS</h1>"></asp:Label>
                                        </div>
                                        <div style="float: right; margin-right: 50px; margin-top: 10px;">
                                            <asp:CheckBox ID="chkShowBlockedCompany" Text="Show blocked Company" runat="server"
                                                OnCheckedChanged="chkShowBlockedCompany_CheckedChanged" AutoPostBack="true" />
                                        </div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptUserDetails" runat="server" OnItemCommand="rptUserDetails_ItemCommand">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        COMPANY
                                                    </th>
                                                    <th>
                                                        COMPANY ID
                                                    </th>
                                                    <th>
                                                        SERVER
                                                    </th>
                                                    <th>
                                                        MBUZZ SERVER
                                                    </th>
                                                    <th>
                                                        MPLUGIN SERVER
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td>
                                                    <asp:LinkButton ID="lnkname" runat="server" Text='<%#Eval("COMPANY_NAME") %>' CommandName="Info"></asp:LinkButton>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblid" runat="server" Text='<%#Eval("COMPANY_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblserver" runat="server" Text='<%# Eval("SERVERNAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblmbuzzserver" runat="server" Text='<%# Eval("MBUZZSERVERNAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblmpuginserver" runat="server" Text='<%# Eval("MPLUGGINSERVER") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                <td>
                                                    <asp:LinkButton ID="lnkname" runat="server" Text='<%#Eval("COMPANY_NAME") %>' CommandName="Info"></asp:LinkButton>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblid" runat="server" Text='<%#Eval("COMPANY_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblserver" runat="server" Text='<%# Eval("SERVERNAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblmbuzzserver" runat="server" Text='<%# Eval("MBUZZSERVERNAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblmpluginserver" runat="server" Text='<%# Eval("MPLUGGINSERVER") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="hidCompanyIdWhenEdit" runat="server" />
                                </div>
                            </asp:Panel>
                        </section>
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
        <div class="modalPopUpDetails">
            <div id="divUserDtlsModal" style="display: none; height: 420px;">
                <asp:UpdatePanel runat="server" ID="updCompanyInformation" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="ProcImgUpd">
                            <div id="DivUserDetails" style="width: 100%;">
                                <asp:Panel ID="pnlMobileUserForm" runat="server">
                                    <div style="float: left; width: 100%;">
                                        <div style="border-bottom: 1px solid; width: 100%; font-size: 12px;">
                                            <asp:Label ID="lblcompanyheader" runat="server" Style="font-size: 13px;" Font-Bold="true"
                                                Text="Company Details"></asp:Label>
                                            <asp:LinkButton ID="lnkBlockCompany" runat="server" OnClientClick="return processConfirmBlock();"
                                                Text="block" CssClass="fr"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkUnblockCompany" runat="server" OnClientClick="return processConfirmUnBlock();"
                                                Text="unblock" CssClass="fr"></asp:LinkButton>
                                        </div>
                                        <div style="margin-bottom: 5px; margin-top: 5px;">
                                            <div style="float: left; margin-right: 90px; margin-top: 3px;">
                                                <asp:Label ID="label11" runat="server" Text="<strong>Company Name</strong>"></asp:Label></div>
                                            <div style="float: left; margin-top: 3px;">
                                                <asp:Label ID="lblCompanyName" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px; margin-top: 5px;">
                                            <div style="float: left; margin-right: 77px; margin-top: 7px;">
                                                <asp:Label ID="label2" runat="server" Text="<strong>Company Address</strong>"></asp:Label></div>
                                            <div style="float: left; margin-top: 5px; width: 180px;">
                                                <asp:Label ID="lblCompanyAddress" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px; margin-top: 10px;">
                                            <div style="float: left; margin-right: 98px;">
                                                <asp:Label ID="label5" runat="server" Text="<strong>Company City</strong> "></asp:Label></div>
                                            <div style="float: left; width: 82px;">
                                                <asp:Label ID="lblCmpCity" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px; margin-top: 9px;">
                                            <div style="float: left; margin-right: 93px; margin-top: 3px;">
                                                <asp:Label ID="label1" runat="server" Text="<strong>Company State</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 3px;">
                                                <asp:Label ID="lblCmpState" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px; margin-top: 9px;">
                                            <div style="float: left; margin-right: 90px; margin-top: 3px;">
                                                <asp:Label ID="label3" runat="server" Text="<strong>Company Email</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 3px;">
                                                <asp:Label ID="lblCmpEmail" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px; margin-top: 9px;">
                                            <div style="float: left; margin-right: 30px; margin-top: 3px;">
                                                <asp:Label ID="label7" runat="server" Text="<strong>Company Contact Number</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 3px;">
                                                <asp:Label ID="lblCmpContactNo" runat="server"></asp:Label></div>
                                        </div>
                                    </div>
                                    <div style="float: left; width: 100%;">
                                        <div style="border-bottom: 1px solid; width: 100%; margin-top: 17px; font-size: 12px;">
                                            <asp:Label ID="Label10" runat="server" Style="font-size: 13px;" Font-Bold="true"
                                                Text="Admin Details"></asp:Label></div>
                                        <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-top: 7px;">
                                                <asp:Label ID="label6" runat="server" Text="<strong>Admin Name </strong>"></asp:Label></div>
                                            <div style="float: left; margin-top: 7px; margin-left: 106px;">
                                                <asp:Label ID="lblCmpAdminName" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-top: 12px; margin-right: 142px;">
                                                <asp:Label ID="label8" runat="server" Text="<strong>Status</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 12px;">
                                                <asp:Label ID="lblStatus" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-top: 12px; margin-right: 107px;">
                                                <asp:Label ID="label19" runat="server" Text="<strong>Admin Email</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 12px;">
                                                <asp:Label ID="lblAdminEmailId" runat="server"></asp:Label></div>
                                        </div>
                                    </div>
                                    <div style="float: left; width: 100%;">
                                        <div style="border-bottom: 1px solid; width: 100%; margin-top: 17px; font-size: 12px;">
                                            <asp:Label ID="Label4" runat="server" Style="font-size: 13px;" Font-Bold="true" Text="Plan Details"></asp:Label></div>
                                        <div style="margin-bottom: 28px;">
                                            <div style="float: left; margin-top: 7px; margin-right: 121px;">
                                                <asp:Label ID="label12" runat="server" Text="<strong>Plan Name</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 7px;">
                                                <asp:Label ID="lblCmpPlanName" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-top: 7px; margin-right: 124px;">
                                                <asp:Label ID="label9" runat="server" Text="<strong>Plan Code</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 7px;">
                                                <asp:Label ID="lblPlanCode" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-top: 12px; margin-right: 100px;">
                                                <asp:Label ID="label13" runat="server" Text="<strong>Max Workflow</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 12px;">
                                                <asp:Label ID="lblMaxWorkflow" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-top: 12px; margin-right: 124px;">
                                                <asp:Label ID="label15" runat="server" Text="<strong>Max Users</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 12px;">
                                                <asp:Label ID="lblMaxNoOfUsers" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-top: 12px; margin-right: 122px;">
                                                <asp:Label ID="label17" runat="server" Text="<strong>Valid Upto</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 12px;">
                                                <asp:Label ID="lblValidUpto" runat="server"></asp:Label></div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                        </section> </div> </div> </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="divBlockCmpReason" class="displayNone">
            <asp:UpdatePanel ID="updBlockCmp" runat="server">
                <ContentTemplate>
                    <div id="divBlockCmpError">
                    </div>
                    <div id="divBlockCompanyContainer">
                        <div class="reasonContainer">
                            <div style="float: left; margin-right: 5px; width: 60px; position: relative; top: 5px;">
                                <asp:Label ID="lblReason" runat="server" Text="Reason :"></asp:Label>
                            </div>
                            <div class="fl">
                                <asp:DropDownList ID="ddlReasonForBlocking" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="modalPopUpAction" style="margin-top: 15px !important">
                            <div id="divPopUpActionCont" class="popUpActionCont">
                                <asp:Button ID="btnBlockSave" runat="server" Text="Save" CssClass="aspButton" OnClick="btnBlockSave_Click" />
                                <asp:Button ID="btnBlockCancel" runat="server" Text="Cancel" CssClass="aspButton"
                                    OnClientClick="closeModalPopUp('divBlockCmpReason'); return false;" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="divUnblockCmpReason" class="displayNone">
            <asp:UpdatePanel ID="updUnblockCmpReason" runat="server">
                <ContentTemplate>
                    <div id="divUnBlockCmpError">
                    </div>
                    <div id="divUnBlockCompanyContainer">
                        <div class="reasonContainer">
                            <div style="float: left; margin-right: 5px; width: 60px; position: relative; top: 5px;">
                                <asp:Label ID="lblUnblockReason" runat="server" Text="Reason :"></asp:Label>
                            </div>
                            <div class="fl">
                                <asp:DropDownList ID="ddlUnblockReason" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="modalPopUpAction" style="margin-top: 15px !important">
                            <div id="div4" class="popUpActionCont">
                                <asp:Button ID="btnUnblockSave" runat="server" Text="Save" CssClass="aspButton" OnClick="btnUnBlockSave_Click" />
                                <asp:Button ID="btnCancelUnblockSave" runat="server" Text="Cancel" CssClass="aspButton"
                                    OnClientClick="closeModalPopUp('divUnblockCmpReason');return false;" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>

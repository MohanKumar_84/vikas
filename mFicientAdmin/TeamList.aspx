﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="TeamList.aspx.cs" Inherits="mFicientAdmin.TeamList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlUserTypeListing" runat="server">
                                <div class="g1">
                                    <h3>
                                        Listing :
                                    </h3>
                                </div>
                                <div style="position: relative; right: -10px; top: 2px;">
                                    <asp:DropDownList ID="ddlUserType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserType_SelectedIndexChanged">
                                        <asp:ListItem Text="Sales" Value="Sales"></asp:ListItem>
                                        <asp:ListItem Text="Reseller" Value="Reseller"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </asp:Panel>
                            <div class="clear">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlSalesMngrName" runat="server">
                                <div class="g12">
                                    <h3>
                                        Sales Manager :</h3>
                                    <asp:Label ID="lblSalesMngrName" runat="server"></asp:Label>
                                </div>
                            </asp:Panel>
                            <div class="clear">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>Sales Person Details</h1>"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptSalesPersonDtls" runat="server">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th style="display: none;">
                                                        Login Id
                                                    </th>
                                                    <th>
                                                        User Name
                                                    </th>
                                                    <th>
                                                        Contact No.
                                                    </th>
                                                    <th>
                                                        Email
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td style="display: none">
                                                    <asp:Label ID="lblLoginId" runat="server" Text='<%#Eval("LOGIN_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblUserName" runat="server" Text='<%#Eval("USER_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblContactNo" runat="server" Text='<%#Eval("CONTACT_NUMBER") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("EMAIL") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkDetails" runat="server" Text="Details" CssClass="repeaterLink"
                                                        CommandName="Details"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                <td style="display: none">
                                                    <asp:Label ID="lblLoginId" runat="server" Text='<%#Eval("LOGIN_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblUserName" runat="server" Text='<%#Eval("USER_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblContactNo" runat="server" Text='<%#Eval("CONTACT_NUMBER") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("EMAIL") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkDetails" runat="server" Text="Details" CssClass="repeaterLink"
                                                        CommandName="Details"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                            </asp:Panel>
                        </section>
                        <section>
                            <asp:Panel ID="pnlResellerRptBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="Panel2" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblResellerRptHeader" runat="server" Text="<h1>Sales Person Details</h1>"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptResellerDtls" runat="server">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th style="display: none;">
                                                        Reseller Id
                                                    </th>
                                                    <th>
                                                        User Name
                                                    </th>
                                                    <th>
                                                        Contact No.
                                                    </th>
                                                    <th>
                                                        Email
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td>
                                                    <asp:Label ID="lblResellerId" runat="server" Text='<%#Eval("RESELLER_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblUserName" runat="server" Text='<%#Eval("RESELLER_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblContactNo" runat="server" Text='<%#Eval("CONTACT_NUMBER") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("EMAIL") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkDetails" runat="server" Text="Details" CssClass="repeaterLink"
                                                        CommandName="Details"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                <td>
                                                    <asp:Label ID="lblResellerId" runat="server" Text='<%#Eval("RESELLER_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblUserName" runat="server" Text='<%#Eval("RESELLER_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblContactNo" runat="server" Text='<%#Eval("CONTACT_NUMBER") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("EMAIL") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkDetails" runat="server" Text="Details" CssClass="repeaterLink"
                                                        CommandName="Details"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                            </asp:Panel>
                        </section>
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
        <div>
            <asp:HiddenField ID="hfs" runat="server" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>

﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="ManageLoginUsers.aspx.cs" Inherits="mFicientAdmin.ManageLoginUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <asp:Panel ID="pnlUserDetails" runat="server">
                            <div id="divUser" runat="server">
                                <section>
                                    <div style="float:left;margin-top:5px;margin-left:10px; font-size:15px;">
                                   
                                        <asp:Label ID="lblSelectionFor" runat="server" Text="TYPE OF USERS:"
                                            AssociatedControlID="ddl_Users"></asp:Label>
                                    </div>
                                    <div style="margin-bottom:12px; margin-left:140px;">
                                        <asp:DropDownList ID="ddl_Users" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_Users_SelectedIndexChanged">
                                            <asp:ListItem Value="Select">--Please Select--</asp:ListItem>
                                            <asp:ListItem Value="all">ALL</asp:ListItem>
                                            <asp:ListItem Value="SalesManager">SALES </asp:ListItem>
                                            <asp:ListItem Value="SalesPerson">SALES EXECUTIVE</asp:ListItem>
                                            <asp:ListItem Value="Reseller">RESELLER</asp:ListItem>
                                            <asp:ListItem Value="Support">SUPPORT</asp:ListItem>
                                            <asp:ListItem Value="Account">ACCOUNTS</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div style="height: 0px; clear: both">
                                    </div>
                               
                                    <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                        <section>
                                            <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                                <div>
                                                    <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>USER DETAILS</h1>"></asp:Label>
                                                </div>
                                                <div style="position: relative; top: 10px; right: 15px;">
                                                    <asp:LinkButton ID="lnkAddNewUser" runat="server" Text="add" CssClass="repeaterLink fr"
                                                        OnClick="lnkAddNewUser_Click"></asp:LinkButton>
                                                </div>
                                            </asp:Panel>
                                        </section>
                                        <asp:Repeater ID="rptUserDetails" runat="server" OnItemCommand="rptUserDetails_ItemCommand"
                                            OnItemDataBound="rptUserDetails_ItemDataBound">
                                            <HeaderTemplate>
                                                <table class="repeaterTable">
                                                    <thead>
                                                        <tr>
                                                            <th style="display:none">
                                                                ID
                                                            </th>
                                                            <th >
                                                                USER NAME
                                                            </th>
<%--                                                            <th>
                                                                LOGIN NAME
                                                            </th>--%>
                                                            <th>
                                                                COUNTRY
                                                            </th>
                                                            <th>
                                                                TYPE
                                                            </th>
                                                            <th>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tbody>
                                                    <tr class="repeaterItem">
                                                        <td style="display:none">
                                                            <asp:Label ID="lblId" runat="server" Text='<%#Eval("ID") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblusername" runat="server" Text='<%#Eval("NAME") %>'></asp:Label>
                                                        </td>
<%--                                                        <td>
                                                            <asp:Label ID="lbllogin" runat="server" Text='<%#Eval("LOGIN_NAME") %>'></asp:Label>
                                                        </td>--%>
                                                        <td>
                                                            <asp:Label ID="lblcountry" runat="server" Text='<%# Eval("COUNTRY_NAME") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lbltype" runat="server" Text='<%# Eval("TYPE") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="lnkEdit" runat="server" Text="edit"  CssClass="repeaterLink" CommandName="Edit"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <tbody>
                                                    <tr class="repeaterAlternatingItem">
                                                        <td style="display:none">
                                                            <asp:Label ID="lblId" runat="server" Text='<%#Eval("ID") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblusername" runat="server" Text='<%#Eval("NAME") %>'></asp:Label>
                                                        </td>
                                                       <%-- <td>
                                                            <asp:Label ID="lbllogin" runat="server" Text='<%#Eval("LOGIN_NAME") %>'></asp:Label>
                                                        </td>--%>
                                                        <td>
                                                            <asp:Label ID="lblcountry" runat="server" Text='<%# Eval("COUNTRY_NAME") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lbltype" runat="server" Text='<%# Eval("TYPE") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="lnkEdit" runat="server" Text="edit" CssClass="repeaterLink"  CommandName="Edit"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </AlternatingItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <div>
                                        </div>
                                        <div style="clear: both">
                                            <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                            <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                                        </div>
                                    </asp:Panel>
                            
                                </section>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlLoginTypeAndForm" runat="server" Visible="false">
                            <asp:Panel ID="pnlLoginType" runat="server" Visible="true">
                                <div id="divlogin" runat="server">
                                    <section>
                                    <div id="divddl" runat="server">
                                        <div>
                                            <asp:Label ID="lblLoginType" runat="server" Text="TYPE OF LOGIN :" 
                                                AssociatedControlID="ddl_Login"></asp:Label>
                                        </div>
                                        <div style="margin-bottom:9px; margin-left:110px;">
                                            <asp:DropDownList ID="ddl_Login" runat="server" AutoPostBack="True" Visible="true"
                                                OnSelectedIndexChanged="ddl_Login_SelectedIndexChanged">
                                                <asp:ListItem Value="-1">--Please Select--</asp:ListItem>
                                                <asp:ListItem Value="SalesManager">SALES MANAGER</asp:ListItem>
                                                <asp:ListItem Value="Reseller">RESELLER</asp:ListItem>
                                                <asp:ListItem Value="SalesPerson">SALES EXECUTIVE</asp:ListItem>
                                                
                                                <asp:ListItem Value="Support">SUPPORT</asp:ListItem>
                                                <asp:ListItem Value="Account">ACCOUNTS</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        </div>

                                    </section>
                                </div>
                            </asp:Panel>
                            <%--  /* for second drop down */--%>
                            <asp:Panel ID="pnlAllForms" runat="server" Visible="true">
                                <%--end of second drop down--%>
                                <section>
                                    <div class="g11">
                                        <div id="formDiv">
                                            <div style="height: 0px; clear: both">
                                            </div>





                                            <%--/*asp:Panel*/--%>
                                            <asp:Panel ID="pnlOtherUsers" runat="server" Visible="false">
                                                <fieldset>
                                                    <label id="Usersdet" runat="server">
                                                        Details</label>
                                                        <section>
                                                        <label for="txtUserLoginName" style="margin-top:6px;">
                                                            Login Name</label>
                                                        <div>
                                                            <asp:TextBox ID="txtUserLoginName" runat="server" Width="50%" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </section>

                                                    <section>
                                                        <label for="txtUserName"  style="margin-top:6px;">
                                                            User Name</label>
                                                        <div>
                                                            <asp:TextBox ID="txtUserName" runat="server" Width="50%"></asp:TextBox>
                                                        </div>
                                                    </section>
                                                    <section>
                                                        <label for="txtUserEmail"  style="margin-top:6px;">
                                                            Email</label>
                                                        <div>
                                                            <asp:TextBox ID="txtUserEmail" runat="server" Width="50%"></asp:TextBox>
                                                        </div>
                                                    </section>
                                                    <section>
                                                        <label for="txtUserContactNo"  style="margin-top:6px;">
                                                            Contact No</label>
                                                        <div>
                                                            <asp:TextBox ID="txtUserContactNo" runat="server" Width="50%"></asp:TextBox>
                                                        </div>
                                                    </section>
                                                    <section>
                                                        <label for="txtUserCity"  style="margin-top:5px;">
                                                            City</label>
                                                        <div>
                                                            <asp:TextBox ID="txtUserCity" runat="server" Width="50%"></asp:TextBox>
                                                        </div>
                                                    </section>
                                                    <section>
                                                        <label for="ddlUsersCountry"  style="margin-top:6px;">
                                                            Country</label>
                                                        <div>
                                                            <asp:DropDownList ID="ddlUsersCountry" runat="server" AutoPostBack="True" 
                                                                onselectedindexchanged="ddlUsersCountry_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </section>
                                                    <section>
                                                        <label for="chkIsEnabled">
                                                             Enabled</label>
                                                        <div>
                                                            <asp:CheckBox ID="chkUserIsEnabled" Checked="true" runat="server" />
                                                        </div>
                                                    </section>
                                                    <asp:Panel ID="pnlServerAllocationCheckBox" runat="server" Visible="false">
                                                        <section>
                                                            <label for="chkUserServerAllocation">
                                                                Server Allocation</label>
                                                            <div>
                                                                <asp:CheckBox ID="chkUserServerAllocation" runat="server" />
                                                            </div>
                                                        </section>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlSalesMngrDropDown" runat="server" Visible="false">
                                                        <section>
                                                            <label for="ddlUserSalesManager">
                                                                Sales Manager</label>
                                                            <div>
                                                            <div id="checkusers" runat="server">
                                                                <span>Please select a country to get the list of Sales managers</span><br /></div>
                                                                <div id="ddlcheck" runat="server">
                                                                <asp:DropDownList ID="ddlUserSalesManager" runat="server" Visible="false" 
                                                                    AppendDataBoundItems="true" 
                                                                    onselectedindexchanged="ddlUserSalesManager_SelectedIndexChanged">
                                                                <asp:ListItem Text="Select" Value="-1"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </asp:Panel>
                                                    
                                                    <asp:Panel ID="pnlUserPassword" runat="server" Visible="false">
                                                    <section>
                                                        <label for="txtUserPassword">
                                                            Password</label>
                                                        <div>
                                                            <asp:TextBox ID="txtUserPassword" runat="server" Width="50%" TextMode="Password"></asp:TextBox>
                                                            <br></br>
                                                            <span>min length of password : 6</span>
                                                        </div>
                                                    </section>
                                              
                                                 
                                                    <section>
                                                        <label for="txtRepassword">
                                                           ReType Password</label>
                                                        <div>
                                                            <asp:TextBox ID="txtRepassword" TextMode="Password" runat="server" Width="50%"></asp:TextBox>
                                                            <br></br>
                                                            <span>min length of password : 6</span>
                                                        </div>
                                                    </section>

                                              
                                                 </asp:Panel>
                                                </fieldset>
                                                <section id="pnlUsersButtons">
                                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="aspButton" OnClick="btnSave_Click" />
                                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="aspButton" OnClick="btnCancel_Click" />
                                                </section>
                                            </asp:Panel>

                                            

        

                                            <%--/*asp:Panel Reseller*/--%>
                                            <asp:Panel ID="pnlreseller" Visible="false" runat="server">
                                                <fieldset>
                                                    <label id="Resellerdet" runat="server">
                                                        Details</label>
                                                         <section>
                                                        <label for="txtloginname"  style="margin-top:6px;">
                                                            Login Name</label>
                                                        <div>
                                                            <asp:TextBox ID="txtloginname" runat="server" Width="50%" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </section>

                                                    <section>
                                                        <label for="txtname"  style="margin-top:6px;">
                                                            Reseller Name</label>
                                                        <div>
                                                            <asp:TextBox ID="txtname" runat="server" Width="50%"></asp:TextBox>
                                                        </div>
                                                    </section>
                                                    <section>
                                                        <label for="txtemail"  style="margin-top:6px;">
                                                            Email</label>
                                                        <div>
                                                            <asp:TextBox ID="txtemail" runat="server" Width="50%"></asp:TextBox>
                                                        </div>
                                                    </section>
                                                    <section>
                                                        <label for="txtnumber"  style="margin-top:6px;">
                                                            Contact No</label>
                                                        <div>
                                                            <asp:TextBox ID="txtnumber" runat="server" Width="50%"></asp:TextBox>
                                                        </div>
                                                    </section>
                                                    <section>
                                                        <label for="chkIsActive"  style="margin-top:5px;">
                                                           Enabled</label>
                                                        <div>
                                                            <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" />
                                                        </div>
                                                    </section>
                                                    <section>
                                                        <label for="chkIsApproved"  style="margin-top:5px;">
                                                           Sales Approval</label>
                                                        <div>
                                                            <asp:CheckBox ID="chkIsApproved" runat="server" Checked="true" />
                                                        </div>
                                                    </section>
                                                    <section>
                                                        <label for="ddlcountry"  style="margin-top:6px;">
                                                            Country</label>
                                                        <div>
                                                            <asp:DropDownList ID="ddlcountry" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlcountry_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </section>
                                                    <section>
                                                        <label for="txtcity"  style="margin-top:6px;">
                                                            City</label>
                                                        <div>
                                                            <asp:TextBox ID="txtcity" runat="server" Width="50%"></asp:TextBox>
                                                        </div>
                                                    </section>
                                                    <section>
                                                        <label for="txtdiscount"  style="margin-top:6px;">
                                                            Discount (%)</label>
                                                        <div>
                                                            <asp:TextBox ID="txtdiscount" runat="server" Width="50%"></asp:TextBox>
                                                        </div>
                                                    </section>

                                                   <section id="panlesp" runat="server"> 
                                                        <label for="chkSalesPersons">
                                                            Sales Persons</label>
                                                        <div>
                                                        <div id="check" runat="server">
                                                          <span>Please select a country to get the list of Sales persons</span></div>                                                          <div id="chcklist" runat="server">
                                                            <asp:CheckBoxList ID="chklstSalesPersons" runat="server" RepeatColumns="4">
                                                            </asp:CheckBoxList>
                                                            </div>
                                                        </div>
                                                 </section>
                                                  
                                                    
                                                    <%--<section>
                                                    <label for="txtdate">
                                                        Created On</label>
                                                    <div>
                                                        <asp:TextBox ID="txtdate" runat="server" Width="50%"></asp:TextBox>
                                                    </div>
                                                </section>--%>
                                                    <%--<section>
                                                        <label for="txtresellerid">
                                                            Reseller Id</label>
                                                        <div>
                                                            <asp:TextBox ID="txtresellerid" runat="server" Width="50%"></asp:TextBox>
                                                        </div>
                                                    </section>--%>
                                                   
                                                    <asp:Panel ID="pnlResellerPassword" runat="server" Visible="false">
                                                    <section>
                                                        <label for="txtpassword">
                                                            Password</label>
                                                        <div>
                                                            <asp:TextBox ID="txtpassword" TextMode="Password" runat="server" Width="50%"></asp:TextBox>
                                                            <br></br>
                                                            <span>min length of password : 6</span>
                                                        </div>
                                                    </section>
                                                       <section>
                                                        <label for="txtRpwd">
                                                           ReType Password</label>
                                                        <div>
                                                            <asp:TextBox ID="txtRpwd" TextMode="Password" runat="server" Width="50%"></asp:TextBox>
                                                            <br></br>
                                                            <span>min length of password : 6</span>
                                                        </div>
                                                    </section>

                                                    </asp:Panel>
                                                </fieldset>
                                                <section id="resellerButtons">
                                                    <asp:Button ID="btnSaveReseller" runat="server" Text="Save" CssClass="aspButton"
                                                        OnClick="btnSaveReseller_Click" />
                                                    <asp:Button ID="btnCancelReseller" runat="server" Text="Cancel" CssClass="aspButton"
                                                        OnClick="btnCancelReseller_Click" />
                                                </section>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </section>
                            </asp:Panel>
                        </asp:Panel>
                        <div>
                            <asp:HiddenField ID="hdi1" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered1" runat="server" />
                            <asp:HiddenField ID="hidFormMode" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
        
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            // $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            //  $("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientAdmin
{
    public partial class PlanApprovalBySales : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;

            if (!Page.IsPostBack)
            {
                /*if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else
                {
                    if (Page.PreviousPage != null)
                    {
                        string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                        prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                        ContentPlaceHolder cphPageCanvas = (ContentPlaceHolder)PreviousPage.Master.Master.FindControl("form").FindControl("MainCanvas").FindControl("PageCanvas");
                        hfs.Value = ((HiddenField)cphPageCanvas.FindControl("hfs")).Value;
                    }
                }*/
                bindPlanOrderDetails();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "postBackScript", " $(\"select\").uniform();$(\"input\").uniform();$(\"#content\").find('div.widgets').wl_Widget();$('#aspnetForm').wl_Form();", true);//12/10/2012
            }
        }

        void bindPlanOrderDetails()
        {
            try
            {
                AddServerDetails ad = new AddServerDetails();
                DataTable dt = null;
                if (strRole.ToLower() == "acc".ToLower())
                {
                    dt = ad.GetOrderList(USER_TYPE.Account);
                }
                else if (strRole.ToLower() == "s".ToLower())
                {
                    dt = ad.GetOrderList(USER_TYPE.SalesManager);
                }
                if (dt.Rows.Count > 0)
                {
                    rptPlanOrderDetails.DataSource = dt;
                    rptPlanOrderDetails.DataBind();
                }
                else if (dt.Rows.Count == 0)
                {
                    lblHeaderInfo.Text = "<h1>No plan orders have been added yet</h1>";
                    rptPlanOrderDetails.Visible = false;
                }
                else
                {
                    dt = null;
                    rptPlanOrderDetails.DataSource = dt;
                    rptPlanOrderDetails.DataBind();
                    lblHeaderInfo.Text = "<h1>No plan requests exists</h1>";
                }
            }
            catch
            {
                Utilities.showMessage("Internal error.", this.Page, DIALOG_TYPE.Error);
            }
        }
        protected void btnok_Click(object sender, EventArgs e)
        {
            pnlMobileUserForm.Visible = false;
            pnlRepeaterBox.Visible = true;
        }

        protected void lnkok_Click(object sender, EventArgs e)
        {
            enabledisablepanels("Info");
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }

        protected void rptPlanOrder_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item;
            Label lit_id = (Label)item.FindControl("lblid");

            if (e.CommandName == "Info")
            {
                FillOrderDetails(lit_id.Text);
            }
        }

        protected void rptPlanOrderDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }

        protected void rptPlanOrderDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Info":
                    Info(e);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing2", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                    break;
                case "Approve":
                    processApprovePlanPurchase(e);
                    break;
                case "Deny":
                    processDenyRequest(e);
                    break;
            }

        }
        void processApprovePlanPurchase(RepeaterCommandEventArgs e)
        {
            Label lblCompanyId = (Label)e.Item.FindControl("lblCompanyId");
            Label lblCurrPlanId = (Label)e.Item.FindControl("lblCurrPlanId");
            Label lblPlanCodeToApprove = (Label)e.Item.FindControl("lblRequestedPlanCode");
            Label lblRequestType = (Label)e.Item.FindControl("lblRequestType");
            PurchaseRequestApprovalBySales objPurchaseReqApprovalSales = new PurchaseRequestApprovalBySales(lblCompanyId.Text, lblPlanCodeToApprove.Text, lblRequestType.Text, false);
            objPurchaseReqApprovalSales.Process();
            if (objPurchaseReqApprovalSales.StatusCode == 0)
            {
                Utilities.showMessage("Approved Successfully", this.Page, DIALOG_TYPE.Info);
                bindPlanOrderDetails();
            }
            else
            {
                Utilities.showMessage("Internal error", this.Page, DIALOG_TYPE.Error);
            }
        }
        protected void enabledisablepanels(string mode)
        {
            if (mode == "Info")
            {
                pnlRepeaterBox.Visible = false;
                pnlMobileUserForm.Visible = true;
            }
            else if (mode == "Add")
            {
                pnlRepeaterBox.Visible = false;
            }
            else
            {
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = false;
            }
        }
        protected void FillOrderDetails(string CompanyNm)
        {
            AddServerDetails add = new AddServerDetails();
            char[] Type = add.GetDetailsArray();
            foreach (char ans in Type)
            {
                if (ans == '1')
                {
                    loname.Text = "Renew Plan";
                }
                else if (ans == '2')
                {
                    loname.Text = "Add User";
                }
                else if (ans == '3')
                {
                    loname.Text = "Plan Upgrade";
                }
                else
                {
                    loname.Text = "Plan Downgrade";
                }
            }

            string Code = add.GetPlan();
            GetServerDetailsAdmin det = new GetServerDetailsAdmin();
            det.OrderDetails(CompanyNm);
            DataTable dt = det.ResultTable;
            if (dt.Rows.Count > 0)
            {
                hidDetailsFormMode.Value = CompanyNm;
                lname.Text = Convert.ToString(dt.Rows[0]["COMPANY_NAME"]);
                lplan.Text = Convert.ToString(dt.Rows[0]["PLAN_NAME"]);
                lusers.Text = Convert.ToString(dt.Rows[0]["MAX_USER"]);
                lwrflow.Text = Convert.ToString(dt.Rows[0]["MAX_WORKFLOW"]);
                ldate.Text = Convert.ToString(dt.Rows[0]["EXPIRY DATE"]);
                lcode.Text = Code;
            }

            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }
        void Info(RepeaterCommandEventArgs e)
        {
            Label company_name = (Label)e.Item.FindControl("lblname");
            FillOrderDetails(company_name.Text);
        }
        void bindDenyRequestDropDown()
        {
            GetPlanPurchaseDenyRemarks objPlanPurchaseDenyRemarks = new GetPlanPurchaseDenyRemarks();
            objPlanPurchaseDenyRemarks.Process();
            if (objPlanPurchaseDenyRemarks.StatusCode == 0)
            {
                DataTable dtblPlanPurchaseDenyRemarks = objPlanPurchaseDenyRemarks.ResultTable;
                if (dtblPlanPurchaseDenyRemarks.Rows.Count > 0)
                {
                    ddlRemarks.DataSource = dtblPlanPurchaseDenyRemarks;
                    ddlRemarks.DataTextField = "REMARK";
                    ddlRemarks.DataValueField = "REMARK_CODE";
                    ddlRemarks.DataBind();
                }
                else
                {
                    //the remarks should exist in the database table
                    throw new Exception();
                }
            }
            else
            {
                throw new Exception();
            }
        }
        protected void btnCancelDenyRemarks_Click(object sender, EventArgs e)
        {
            Utilities.closeModalPopUp("divDenyRequestBox", this.Page, "Close Remarks Div");
            btnSaveDenyRemarks.CommandArgument = "";
        }
        void showDenyRequestRemarks(string companyId, string planCode, string requestType)
        {
            try
            {
                bindDenyRequestDropDown();
                btnSaveDenyRemarks.CommandArgument = companyId + ";" + planCode + ";" + requestType;
                updDenyRequest.Update();
                Utilities.showModalPopup("divDenyRequestBox", this.Page, "Select Remarks", "300", false);
            }
            catch
            {
                throw new Exception();
            }
        }
        void processDenyRequest(RepeaterCommandEventArgs e)
        {
            try
            {
                Label lblCompanyId = (Label)e.Item.FindControl("lblCompanyId");
                Label lblPlanCode = (Label)e.Item.FindControl("lblRequestedPlanCode");
                Label lblRequestType = (Label)e.Item.FindControl("lblRequestType");
                showDenyRequestRemarks(lblCompanyId.Text, lblPlanCode.Text, lblRequestType.Text);
            }
            catch
            {
                Utilities.showMessage("Internal server error", this.Page, DIALOG_TYPE.Error);
            }
        }
        protected void btnSaveDenyRemarks_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                string strCommandArgument = btnSender.CommandArgument;//; separated values companyId,planCode,RequestType
                string[] aryCommandArgumentValues = strCommandArgument.Split(';');
                SavePlanReqDenyRemarksAndStatusByUserType objSavePlanReqDenyRemarks = new SavePlanReqDenyRemarksAndStatusByUserType(aryCommandArgumentValues[0], aryCommandArgumentValues[1]
                                                                                         , aryCommandArgumentValues[2], USER_TYPE.SalesManager, ddlRemarks.SelectedValue);
                objSavePlanReqDenyRemarks.Process();
                if (objSavePlanReqDenyRemarks.StatusCode == 0)
                {
                    Utilities.showMessage("Denied successfully", this.Page, DIALOG_TYPE.Info);
                    bindPlanOrderDetails();
                    Utilities.closeModalPopUp("divDenyRequestBox", this.Page, "Close Remarks model");
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error", this.Page, DIALOG_TYPE.Error);
            }

        }
    }
}
﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="SupportQueries.aspx.cs" Inherits="mFicientAdmin.SupportQueries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <style type="text/css">
        a.linkSelected:link
        {
            color: #8F8F97;
        }
        .modalPopUpDetails .singleColumn
        {
            width: 94%;
        }
    </style>
    <script type="text/javascript">
        function getCommentTextBox() {
            return $('#' + '<%=txtcomment.ClientID %>');
        }
        function getCommentErrorDiv()
        {
            return $('#divCommentErrorAlert');
        }
        function openCommentsPopUp() {
            showModalPopUp('divCommentByUser', 'Add comment', 450, false);
        }
        function validateCommentText() {
            var txtComment = getCommentTextBox();
            if (txtComment) {
                if ($.trim($(txtComment).val()) == "") {
                    $.wl_Alert('Please enter a comment', 'info', getCommentErrorDiv());
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="divTab" style="margin-left: 60px;">
        <br />
        <asp:LinkButton ID="lnk1" runat="server" Text="Open Queries" OnClick="lnk1_Click"></asp:LinkButton>&nbsp;&nbsp;&nbsp;|
        <asp:LinkButton ID="lnk2" runat="server" Text="Closed Queries" OnClick="lnk2_Click"></asp:LinkButton>
        &nbsp;&nbsp;
    </div>
    <br />
    <div id="PageCanvasContent">
        <div id="divRepeater">
            <asp:UpdatePanel ID="upd" runat="server">
                <ContentTemplate>
                    <section>
                        <div id="divInformation">
                        </div>
                    </section>
                    <section>
                        <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                            <section>
                                <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                    <div>
                                        <asp:Label ID="lblHeaderInfo" runat="server"> </asp:Label></div>
                                </asp:Panel>
                            </section>
                           <asp:Repeater ID="rptUserDetails" runat="server" OnItemCommand="rptUserDetails_ItemCommand"   >
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                <th style="display:none;">
                                                User Name
                                                </th>
                                                 <th>
                                                 Token Number
                                                   </th>
                                                <th>Category</th>
                                                    <th>
                                                     Subject
                                                    </th>
                                                    <th>
                                                    Posted On
                                                    </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                            <td style="display:none;">
                                            
                                             <asp:Label ID="lblid" runat="server" Text='<%#Eval("user_name") %>'></asp:Label>
                                            </td>
                                               <td >
                                                    <asp:Label ID="lblnumber" runat="server" Text='<%#Eval("TOKEN_NO") %>'></asp:Label>
                                                </td>
                                                <td >
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("QUERY_TYPE") %>'></asp:Label>
                                                </td>
                                             <td>
                                              <asp:Label ID="lblquery" runat="server" Text='<%# Eval("QUERY_SUBJECT") %>'></asp:Label>
                                                </td>
                                             
                                             <td>
                                              <asp:Label ID="lbldate" runat="server" Text='<%# Eval("PostDate") %>'></asp:Label>
                                                </td>

                                                 <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info"
                                                   OnClick="lnkok_Click" ></asp:LinkButton>
                                                </td>
                                                
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                            <td style="display:none;">
                                            
                                             <asp:Label ID="lblid" runat="server" Text='<%#Eval("user_name") %>'></asp:Label>
                                            </td>

                                                <td >
                                                    <asp:Label ID="lblnumber" runat="server" Text='<%#Eval("TOKEN_NO") %>'></asp:Label>
                                                </td>

                                               <td >
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("QUERY_TYPE") %>'></asp:Label>
                                                </td>
                                               
                                                <td>
                                              <asp:Label ID="lblquery" runat="server" Text='<%# Eval("QUERY_SUBJECT") %>'></asp:Label>
                                                </td>
                                             
                                             <td>
                                              <asp:Label ID="lbldate" runat="server" Text='<%# Eval("PostDate") %>'></asp:Label>
                                                </td>

                                                 <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info"
                                                     OnClick="lnkok_Click"></asp:LinkButton>
                                                </td>

                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            <div>
                            </div>
                            <div style="clear: both">
                                <asp:HiddenField ID="hidTokenNoSelected" runat="server" />
                                <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                            </div>
                        </asp:Panel>
                    </section>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="divUserDtlsModal" style="display: none;">
            <div class="modalPopUpDetails" style="width: 100%; display: block;">
                <asp:UpdatePanel runat="server" ID="upUserDtlsModal" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="pnlMobileUserForm" Visible="false" runat="server" class="singleSection">
                            <fieldset>
                                <div class="fieldsetLegendDiv">
                                    <section>
                                        <div class="column1">
                                            <asp:Label ID="lblForUserName" runat="server" Text="Name"></asp:Label>
                                        </div>
                                        <div class="column2">
                                            <asp:Label ID="Label1" runat="server" Text=":"></asp:Label>
                                        </div>
                                        <div class="column3">
                                            <asp:Label ID="lname" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblFullName" runat="server"></asp:Label>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="column1">
                                            <asp:Label ID="lblForSubject" runat="server" Text="Subject"></asp:Label>
                                        </div>
                                        <div class="column2">
                                            <asp:Label ID="Label5" runat="server" Text=":"></asp:Label>
                                        </div>
                                        <div class="column3">
                                            <asp:Label ID="lsubject" runat="server"></asp:Label>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="column1">
                                            <asp:Label ID="lblForQuery" runat="server" Text="Query"></asp:Label>
                                        </div>
                                        <div class="column2">
                                            <asp:Label ID="Label4" runat="server" Text=":"></asp:Label>
                                        </div>
                                        <div class="column3">
                                            <asp:Label ID="lquery" runat="server"></asp:Label>
                                        </div>
                                    </section>
                                </div>
                                <div class="fieldsetLegendDiv">
                                    <div>
                                        <asp:Label ID="lblcomments" Text="Comments" runat="server" Style="font-weight: bold;
                                            font-size: 14px;"></asp:Label></div>
                                    <div>
                                        <asp:Label ID="lblCommentsRptHeader" Style="font-size: 12px;" runat="server"></asp:Label>
                                    </div>
                                    <asp:Panel ID="pnlRptComments" runat="server" ScrollBars="Vertical" Height="300"
                                        Style="margin-top: 5px;">
                                        <asp:Repeater ID="rptComments" runat="server">
                                            <ItemTemplate>
                                                <div style="margin-bottom: 5px;">
                                                    <div>
                                                        <asp:Label ID="lblcommentby" runat="server" Text=' <%#Eval("LOGIN_NAME") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblCommentByName" runat="server" Text=' <%#Eval("LOGIN_NAME") %>'
                                                            Style="font-weight: bold;"></asp:Label>
                                                        &nbsp;<asp:Label ID="lblColon" runat="server" Text=":"></asp:Label>&nbsp;
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblcomment" runat="server" Text='<%# Eval("COMMENT") %>'></asp:Label></div>
                                                    <hr style="margin: 5px;" />
                                                </div>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </asp:Panel>
                                </div>
                                <div class="fr" style="margin-right:3px;">
                                    <asp:LinkButton ID="lnkAddComment" runat="server" style="font-size:14px;" Text="add comment" OnClientClick="openCommentsPopUp();return false;"></asp:LinkButton>
                                </div>
                                <div style="clear: both;">
                                </div>
                            </fieldset>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="divCommentByUser" style="display: none;">
            <div class="modalPopUpDetails" style="width: 100%; display: block; padding-left: 3px;">
                <asp:UpdatePanel ID="updCommentByUser" runat="server">
                <ContentTemplate>
                    <div id="divCommentErrorAlert">
                    </div>
                    <div class="fieldsetLegendDiv" style="margin-left: 15px; width: auto;">
                        <div class="singleColumn">
                            <asp:Label ID="lblcomment" Text="Comment" CssClass="lightText" runat="server"></asp:Label>
                        </div>
                        <div class="singleColumn" style="margin-top: 5px;">
                            <asp:TextBox ID="txtcomment" TextMode="MultiLine" Rows="3" runat="server" Width="370px"></asp:TextBox>
                        </div>
                        <div class="fr" style="margin-top: 10px;">
                            <asp:Button ID="bttncomment" runat="server" Text="Post" OnClientClick="return validateCommentText();"
                                OnClick="bttncomment_Click" style="margin-right:1px;"/>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div style="clear: both">
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <asp:HiddenField ID="HiddenField2" runat="server" />
    </div>
    <div>
        <asp:HiddenField ID="hidTabSelected" runat="server" />
        <asp:HiddenField ID="hidPriceAmountPayable" runat="server" />
        <asp:HiddenField ID="hidPasswordEntered" runat="server" />
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>

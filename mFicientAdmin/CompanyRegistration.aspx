﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyRegistration.aspx.cs"
    MasterPageFile="~/master/Canvas.Master" Inherits="mFicientAdmin.CompanyRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <style type="text/css">
        .hideElement
        {
            display: none;
        }
        .showElement
        {
            display: block;
        }
        .uniformCheckboxLabel
        {
            position:relative;
            top:4px;
        }
    </style>
    <script type="text/javascript">
        function hidePaymentTab() {
            var lnkPayment = $('#lnkPayment');
            var lstPayment = $('#lstPayment');
            lnkPayment.addClass("hideElement");
            lstPayment.addClass("hideElement");
        }
        function showPaymentTab() {
            var lnkPayment = $('#lnkPayment');
            var lstPayment = $('#lstPayment');
            lnkPayment.removeClass("hideElement").addClass("showElement");
            lstPayment.removeClass("hideElement").addClass("showElement");
        }

        function hideTrialTab() {
            var lnkTrial = $('#lnkTrialPlanSelection');
            var lstTrial = $('#lstTrialPlan');
            lnkTrial.addClass("hideElement");
            lstTrial.addClass("hideElement");
        }
        function showTrialTab() {
            var lnkTrial = $('#lnkTrialPlanSelection');
            var lstTrial = $('#lstTrialPlan');
            lnkTrial.removeClass("hideElement").addClass("showElement");
            lstTrial.removeClass("hideElement").addClass("showElement");
        }
        function makeTabAfterPostBack() {
            $('#content').find('div.tab').tabs({
                fx: {
                    opacity: 'toggle',
                    duration: 'fast'
                }
            });
        }
        function storeSelectedTabIndex(index) {
            var hidSelectedTabIndex = document.getElementById('<%=hidTabSelected.ClientID %>');
            hidSelectedTabIndex.value = '';
            hidSelectedTabIndex.value = index;
        }
        function showSelectedTabOnPostBack() {
            var hidSelectedTabIndex = document.getElementById('<%=hidTabSelected.ClientID %>');
            //this is the third tab and associated div
            var lstPayment = $('#lstPayment');
            var divPayment = $('#Payment');
            //this is the second tab and associated div
            var lstCompany = $('#lstCompany');
            var divCompany = $('#Company');
            //this is the first tab and associated div
            var lstAdministrator = $('#lstAdmin');
            var divAdministrator = $('#Admin');

            //this is the fourth tab and associated div
            var lstTrialPlan = $('#lstTrialPlan');
            var divTrialPlan = $('#TrialPlan');

            switch (hidSelectedTabIndex.value) {
                case '1':
                    lstPayment.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    lstCompany.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    lstAdministrator.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");
                    lstTrialPlan.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    divPayment.addClass("ui-tabs-hide");
                    divCompany.addClass("ui-tabs-hide");
                    divAdministrator.removeClass("ui-tabs-hide");
                    divTrialPlan.addClass("ui-tabs-hide");
                    break;
                case '2':
                    lstPayment.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    lstCompany.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");
                    lstAdministrator.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    divPayment.addClass("ui-tabs-hide");
                    divCompany.removeClass("ui-tabs-hide");
                    divAdministrator.addClass("ui-tabs-hide");

                    lstTrialPlan.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    divTrialPlan.addClass("ui-tabs-hide");
                    break;
                case '3':
                    lstPayment.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");
                    lstCompany.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    lstAdministrator.removeClass(" ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    divPayment.removeClass("ui-tabs-hide");
                    divCompany.addClass("ui-tabs-hide");
                    divAdministrator.addClass("ui-tabs-hide");

                    lstTrialPlan.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    divTrialPlan.addClass("ui-tabs-hide");
                    break;

                case '4':
                    lstTrialPlan.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active");
                    lstCompany.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    lstAdministrator.removeClass(" ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    divTrialPlan.removeClass("ui-tabs-hide");
                    divCompany.addClass("ui-tabs-hide");
                    divAdministrator.addClass("ui-tabs-hide");

                    lstPayment.removeClass("ui-state-default ui-corner-top ui-tabs-selected ui-state-active").addClass("ui-state-default ui-corner-top");
                    divPayment.addClass("ui-tabs-hide");
                    break;
            }
        }

        function checkPasswordMatches() {
            var txtPassword = document.getElementById('<%=txtPassword.ClientID %>');
            var txtReTypePassword = document.getElementById('<%=txtreenterPassword.ClientID %>');
            if (txtPassword.value != txtReTypePassword.value) {
                //$.msg('The password entered does not match', { sticky: true });
                //txtReTypePassword.focus();
                $.alert("Password entered does not match.", $.alert.Error); showDialogImage(DialogType.Error);
            }
        }
        function validateTabAdmin() {

        }

        function validateTabCompany() {

        }
        function validateTabPayment() {

        }
        function calculatePrice(sender) {
            var txtMaxUsers = document.getElementById('<%=txtMaxUsers.ClientID %>');
            if (CheckDecimal(txtMaxUsers)) {
                $.alert("Please enter valid no of users.", $.alert.Error); showDialogImage(DialogType.Error);
                txtNoOfUsers.value = parseInt(txtNoOfUsers.value);
                return;
            }

            var ddlMonths = document.getElementById('<%=ddlMonths.ClientID %>');
            var lblChargePerUsr = document.getElementById('<%=lblCharge.ClientID %>');
            var txtDiscount = document.getElementById('<%=txtPaymentDiscount.ClientID %>');
            var lblPrice = document.getElementById('<%= lblPrice.ClientID%>');
            var lblAmountPayable = document.getElementById('<%=lblPayableAmount.ClientID %>');
            var hidPriceAmountPayable = document.getElementById('<%=hidPriceAmountPayable.ClientID %>');

            var chargePerUsr = parseFloat(lblChargePerUsr.innerHTML);
            var maxUsers = parseInt(txtMaxUsers.value);
            var months = parseInt(ddlMonths.value);
            var discount = parseInt(txtDiscount.value);
            hidPriceAmountPayable.value = '';

            if (isNaN(chargePerUsr) || isNaN(maxUsers) || months == '-1') {

                lblPrice.innerHTML = '0';
                hidPriceAmountPayable.value = lblPrice.innerHTML;
            }
            else {
                var price = chargePerUsr * maxUsers * months;
                lblPrice.innerHTML = price.toFixed(2);
                lblAmountPayable.innerHTML = lblPrice.innerHTML;
                //set attribute did not worked properly
                //lblPrice.setAttribute('Text', value);
                hidPriceAmountPayable.value = lblPrice.innerHTML;
            }
            if (!isNaN(discount) && lblPrice.innerHTML != '0') {
                var price = parseFloat(lblPrice.innerHTML);
                //check if discount exceeds price
                if (discount > price) {
                    $.alert("Discount amount exceeds the Price.Please enter valid discount amount", $.alert.Error); showDialogImage(DialogType.Error);
                    return;
                }
                var discountValue = (price - discount);
                //this did not worked
                //lblDiscountPrice.setAttribute('Text', discountValue);
                //this worked but still could not find the text value at post back so had to use the hidden field
                //$(lblDiscountPrice).attr('Text', 'discountValue');
                lblAmountPayable.innerHTML = discountValue.toFixed(2);
                hidPriceAmountPayable.value += ',' + discount + ',' + lblAmountPayable.innerHTML;
            }
            else {
                if (isNaN(maxUsers) && sender == txtDiscount) {
                    $.alert("Please enter valid no of users.", $.alert.Error); showDialogImage(DialogType.Error);
                }
                //lblDiscountPrice.innerHTML = '0';
                //lblAmountPayable.innerHTML = '0';
            }

        }

        function rememberPasswordEntered() {
            var hidPassword = $('#' + '<%= hidPasswordEntered.ClientID %>');
            var hidReTypePasswordEntered = $('#' + '<%=hidReTypePasswordEntered.ClientID %>');
            var txtPassword = $('#' + '<%=txtPassword.ClientID %>');
            var txtRetypePassword = $('#' + '<%=txtreenterPassword.ClientID %>');
            $(hidPassword).val($(txtPassword).val());
            $(hidReTypePasswordEntered).val($(txtRetypePassword).val());
        }
        function fillPasswordEntered() {
            var hidPassword = document.getElementById('<%= hidPasswordEntered.ClientID %>');
            var hidReTypePasswordEntered = $('#' + '<%=hidReTypePasswordEntered.ClientID %>');
            var txtPassword = document.getElementById('<%=txtPassword.ClientID %>');
            var txtRetypePassword = document.getElementById('<%=txtreenterPassword.ClientID %>');
            if (hidPassword && hidReTypePasswordEntered) {
                if (hidPassword.value !== "") {
                    if (txtPassword)
                        $(txtPassword).val($(hidPassword).val());
                }
                else {
                    if (txtPassword)
                        $(txtPassword).val(""); //done for adding trimmed value.
                }
                if ($(hidReTypePasswordEntered).val() !== "") {
                    if (txtRetypePassword)
                        $(txtRetypePassword).val($(hidReTypePasswordEntered).val());
                }
                else {
                    if (txtRetypePassword)
                        $(txtRetypePassword).val(""); //done for adding trimmed value
                }
            }

        }

        function makeDatePickerWithMonthYear() {
//            
//            $(datePicker).datepicker({
//                changeMonth: true,
//                changeYear: true,
//                dateFormat: "dd-mm-yy",
//                minDate: new Date(1940, 1 - 1, 1),
//                maxDate: "+0d",
//                yearRange: "1950:" + (new Date).getFullYear()
//            });
//            //$.datepicker.formatDate('yy-mm-dd', new Date(2007, 1 - 1, 26));
//            if (datePicker) {
//                stopDefualtOfDOB(datePicker);
//            }
        }
        function stopDefualtOfDOB(txtDOB) {
            if (txtDOB) {
                txtDOB.addEventListener(
                                        'keydown', stopDefAction, false
                                    );
            }
        }

        function CheckDecimal(inputtxt) {
            var decimal = /^[0-9]+(\.[0-9]+)+$/;
            if (inputtxt.value.match(decimal)) {
                //alert('The number has a decimal part...');
                return true;
            }
            else {
                //alert('The number has no decimal part...');
                return false;
            }
        }
        function removeHtmlOfStatusField() {
            var lblMessageStatus = $('#' + '<%=lblCompanyExistsStatus.ClientID%>');
            $(lblMessageStatus).html("");
        }
        function doOnDocumentReady() {

            var lblMessageStatus = $('#' + '<%=lblCompanyExistsStatus.ClientID%>');
            var txtCompanyId = $('#' + "<%=txtCompanyId.ClientID %>");
            $(txtCompanyId).focusout(function () {
                if (($(txtCompanyId).val().toString()).length < 3) {
                    $(lblMessageStatus).html("Min length should be 3 Max length should be 10. ");
                    $(txtCompanyId).focus();
                }
            })

            $(txtCompanyId).keyup(function () {
                if (($(txtCompanyId).val().toString()).length > 3) {
                    try {
                        var request = $.ajax({
                            url: "CheckCompanyNameExists.ashx?q=" + "&" + "CmpId=" + $(txtCompanyId).val(),
                            type: "GET"
                        });

                        request.done(function (msg) {
                            if (msg && msg === "")
                                return;
                            else {

                                if (msg.toLowerCase() === "true")
                                    $(lblMessageStatus).html("CompanyId already exists.Please enter some other id");
                                else
                                    $(lblMessageStatus).html("CompanyId available");
                            }
                        });
                    }
                    catch (Error)
                        { }
                }
                else
                    return;
            });
            _makeCheckboxesUniform();

        }
        function _makeCheckboxesUniform() {
            //$.uniform.restore($('input[type="checkbox"]'));
            $('input[type="checkbox"]').uniform();
            //$.uniform.restore($('input[type="checkbox"]'));
        }
        $(document).ready(
            doOnDocumentReady
        );

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="updRegistration" runat="server">
        <ContentTemplate>
            <div id="PageCanvasContent">
                <section>
                    <div class="g3" style="width: auto;">
                        <h4>
                            <asp:Label ID="lblSelectPaymentType" runat="server" Text="Payment Type :"></asp:Label></h4>
                    </div>
                    <div class="g8">
                        <asp:DropDownList ID="ddlPaymentType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPaymentType_SelectedIndexChanged">
                            <asp:ListItem Value="Paid" Text="Paid" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="Trial" Text="Trial"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="clear">
                    </div>
                </section>
                <section>
                    <div id="error">
                    </div>
                </section>
                <section>
                    <div class="g12 widget" id="widget_tabs">
                        <h3 class="handle">
                            Please fill in the details</h3>
                        <div id="divTab" class="tab">
                            <ul id="tabList">
                                <li id="lstAdmin"><a id="lnkAdmin" href="#Admin" onclick="storeSelectedTabIndex(1);">
                                    Administrator</a></li>
                                <li id="lstCompany"><a id="lnkCompany" href="#Company" onclick="storeSelectedTabIndex(2);">
                                    Company</a></li>
                                <li id="lstPayment"><a id="lnkPayment" href="#Payment" onclick="storeSelectedTabIndex(3);">
                                    Payment</a></li>
                                <li id="lstTrialPlan"><a id="lnkTrialPlanSelection" href="#TrialPlan" onclick="storeSelectedTabIndex(4);">
                                    Trial Plan</a></li>
                            </ul>
                            <div class="formDivCls">
                                <div id="Admin">
                                    <fieldset>
                                        <label>
                                            Details <span>( All fields marked with star are required )</span>
                                            <div class="fr">
                                                <asp:CheckBox ID="chkOemrClient" runat="server" /><span class="uniformCheckboxLabel">OEM Reseller</span>
                                            </div>
                                        </label>
                                        <section>
                                            <label for='<%=txtFirstName.ClientID%>'>
                                                Name <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:TextBox ID="txtFirstName" runat="server" Width="30%"></asp:TextBox>&nbsp;
                                                <asp:TextBox ID="txtMiddleName" runat="server" Width="30%"></asp:TextBox>&nbsp;
                                                <asp:TextBox ID="txtLastName" runat="server" Width="30%"></asp:TextBox>
                                            </div>
                                        </section>
                                        <section>
                                            <label for='<%=txtEmail.ClientID%>'>
                                                Email <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:TextBox ID="txtEmail" runat="server" Width="50%"></asp:TextBox>
                                            </div>
                                        </section>
                                        <section>
                                            <label for='<%=txtPassword.ClientID%>'>
                                                Password <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="50%" onblur="rememberPasswordEntered();"></asp:TextBox>
                                            </div>
                                        </section>
                                        <section>
                                            <label for='<%=txtreenterPassword.ClientID%>'>
                                                Re-type <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:TextBox ID="txtreenterPassword" runat="server" TextMode="Password" Width="50%"
                                                    onblur="rememberPasswordEntered();checkPasswordMatches();"></asp:TextBox>
                                                <%--<asp:LinkButton ID="lnkResetPassword" runat="server" Text="reset password" OnClick="lnkResetPassword_Click"></asp:LinkButton>--%>
                                            </div>
                                        </section>
                                        <%--<section class="hidden">
                                            <label for="chkIsActive">
                                                Gender <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:DropDownList runat="server" ID="ddlGender" CssClass="Fld" Width="100px">
                                                    <asp:ListItem Value="M" Text="Male"></asp:ListItem>
                                                    <asp:ListItem Value="F" Text="Female"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </section>--%>
                                        <%--<section class="hidden">
                                            <label for="txtMobile">
                                                Mobile <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:TextBox ID="txtMobileNo" runat="server" Width="50%"></asp:TextBox>
                                                <br />
                                                <span>With country code eg.+9184736384763</span>
                                            </div>
                                        </section>--%>
                                        <%--<section class="hidden">
                                            <label for="txtDOB">
                                                Date of birth <span class="required">&nbsp;</span>
                                            </label>
                                            <div>
                                                <asp:TextBox ID="txtDOB" runat="server" Width="50%"></asp:TextBox>
                                            </div>
                                        </section>--%>
                                    </fieldset>
                                </div>
                                <div id="Company">
                                    <fieldset>
                                        <label>
                                            Details <span>( All fields marked with star are required )</span></label>
                                        <section>
                                            <label for="input">
                                                Company&nbsp;ID <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:TextBox ID="txtCompanyId" runat="server" Width="50%" MaxLength="20"></asp:TextBox>
                                                <span>
                                                    <asp:Label ID="lblCompanyExistsStatus" class="wl_formstatus" runat="server"></asp:Label></span>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="input">
                                                Company&nbsp;Name <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:TextBox ID="txtCompanyName" runat="server" Width="50%"></asp:TextBox>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="input">
                                                Street Address 1 <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:TextBox ID="txtCompanyAddress1" runat="server" Width="50%"></asp:TextBox>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="input">
                                                Street Address 2</label>
                                            <div>
                                                <asp:TextBox ID="txtCompanyAddress2" runat="server" Width="50%"></asp:TextBox>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="input">
                                                Street Address 3</label>
                                            <div>
                                                <asp:TextBox ID="txtCompanyAddress3" runat="server" Width="50%"></asp:TextBox>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="input">
                                                City <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:TextBox ID="txtCompanyCity" runat="server" Width="50%"></asp:TextBox>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="input">
                                                State <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:TextBox ID="txtCompanyState" runat="server" Width="50%"></asp:TextBox>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="input">
                                                Country <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:DropDownList runat="server" ID="ddlCompanyCountry" CssClass="Fld" />
                                            </div>
                                        </section>
                                        <section>
                                            <label for='<%= ddlCmpTimeZone.ClientID %>'>
                                                Time zone<span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:DropDownList runat="server" ID="ddlCmpTimeZone" CssClass="Fld" />
                                            </div>
                                        </section>
                                        <section>
                                            <label for="input">
                                                Zip <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:TextBox ID="txtCompanyZip" runat="server" Width="50%"></asp:TextBox>
                                            </div>
                                        </section>
                                       <%-- <section>
                                            <label for="input">
                                                Support Email <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:TextBox ID="txtSupportEmail" runat="server" Width="50%"></asp:TextBox>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="input">
                                                Support Contact <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:TextBox ID="txtSupportContact" runat="server" Width="50%"></asp:TextBox>
                                                <br />
                                                <span>With country code eg.+9184736384763</span>
                                            </div>
                                        </section>--%>
                                        <asp:Panel ID="pnlCompanySaveBtn" runat="server">
                                        </asp:Panel>
                                    </fieldset>
                                </div>
                                <div id="Payment">
                                    <fieldset>
                                        <label>
                                            Details <span>( All fields marked with star are required )</span></label>
                                        <section>
                                            <label for="ddlCurrency">
                                                Currency</label>
                                            <div>
                                                <asp:DropDownList runat="server" ID="ddlCurrencyType" CssClass="Fld" AutoPostBack="True"
                                                    OnSelectedIndexChanged="ddlCurrencyType_SelectedIndexChanged" AppendDataBoundItems="true">
                                                    <asp:ListItem Text="USD" Value="USD" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="INR" Value="INR"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="txtFirstName">
                                                Months <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:DropDownList runat="server" ID="ddlMonths" CssClass="Fld" OnSelectedIndexChanged="ddlMonths_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Value="-1" Text="Select" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                    <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                    <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                                    <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                                    <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                                    <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                                    <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                                    <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="ddlPlans">
                                                Plan <span class="required">&nbsp;</span></label>
                                            <div class="containingLabel">
                                                <%--<asp:DropDownList runat="server" ID="ddlPlans" CssClass="Fld" AutoPostBack="True"
                                                    OnSelectedIndexChanged="ddlPlans_SelectedIndexChanged" AppendDataBoundItems="true"
                                                    >
                                                    <asp:ListItem Text="Select" Value="-1"></asp:ListItem>
                                                </asp:DropDownList>--%>
                                                <asp:Label ID="lblPaymentPlanName" runat="server" Text="Please select a plan"></asp:Label>&nbsp;<asp:Label
                                                    ID="lblPaymentPlanCode" runat="server" Visible="false"></asp:Label>&nbsp;&nbsp;&nbsp;<asp:LinkButton
                                                        ID="lnkPaymentSelectPlan" runat="server" OnClick="lnkPaymentSelectPlan_Click">( select plan )</asp:LinkButton>
                                                <asp:LinkButton ID="lnkPaymentChangePlan" runat="server" OnClick="lnkPaymentChangePlan_Click"
                                                    Visible="false">( change plan )</asp:LinkButton>
                                                <%--<asp:Label ID="lblPaymentPlanId" runat="server" Visible="false"></asp:Label>--%>
                                            </div>
                                        </section>
                                        <asp:Panel ID="pnlCurrency" runat="server" Visible="false">
                                            <section>
                                                <label for="ddlCurrency">
                                                    Currency</label>
                                                <div class="containingLabel">
                                                    <%--<asp:DropDownList runat="server" ID="ddlCurrencyType" CssClass="Fld" AutoPostBack="True"
                                                    OnSelectedIndexChanged="ddlCurrencyType_SelectedIndexChanged" AppendDataBoundItems="true">
                                                    <asp:ListItem Text="USD" Value="USD" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="INR" Value="INR"></asp:ListItem>
                                                </asp:DropDownList>--%>
                                                    <asp:Label ID="lblPaymentCurrency" runat="server" Text="Please select a plan"></asp:Label>
                                                </div>
                                            </section>
                                        </asp:Panel>
                                        <section>
                                            <label for="lblMaxWorkflow">
                                                MAX Workflow <span class="required">&nbsp;</span></label>
                                            <div class="containingLabel">
                                                <asp:Label ID="lblMaxWorkflow" runat="server" Text="Please select a plan to get max workflow"></asp:Label>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="ddlChargePrice">
                                                Charge Per User<span class="required">&nbsp;</span></label>
                                            <div class="containingLabel">
                                                <span>
                                                    <asp:Label ID="lblCharge" runat="server" Text="Please select plan to get related charges"></asp:Label>&nbsp;&nbsp;<asp:Label
                                                        ID="lblPerMonth" runat="server" Text="( Per Month )" Visible="false"></asp:Label></span>
                                                <%--<asp:Panel ID="pnlChargePriceContainer" runat="server" Visible="false">
                                                    <asp:DropDownList runat="server" ID="ddlChargePrice" CssClass="Fld" OnSelectedIndexChanged="ddlChargePrice_SelectedIndexChanged"
                                                        AutoPostBack="True">
                                                    </asp:DropDownList>
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="lblCharge" runat="server" Style="position: relative; top: -10px;" Text="0"></asp:Label>
                                                </asp:Panel>--%>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="txtMaxUsers">
                                                MAX Users <span class="required">&nbsp;</span></label>
                                            <div>
                                                <asp:TextBox ID="txtMaxUsers" runat="server" Width="60px" onkeyup="calculatePrice(this);"></asp:TextBox>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="lblDiscount">
                                                Discount
                                            </label>
                                            <div class="containingLabel">
                                                <%--<asp:Label ID="lblDiscount" runat="server" Text="Please select a month to get any discount available"
                                                    ></asp:Label>--%>
                                                <asp:TextBox ID="txtPaymentDiscount" runat="server" Width="60px" onkeyup="calculatePrice(this);"></asp:TextBox>
                                            </div>
                                        </section>
                                        <section>
                                            <label for="lblPrice">
                                                Price</label>
                                            <div class="containingLabel">
                                                <asp:Label ID="lblPrice" runat="server" Text="0"></asp:Label>
                                            </div>
                                        </section>
                                        <%--<section>
                                            <label for="lblDiscountAmount">
                                                Discount Price</label>
                                            <div class="containingLabel">
                                                <asp:Label ID="lblDiscountAmount" runat="server" Text="0"></asp:Label>
                                            </div>
                                        </section>--%>
                                        <section>
                                            <label for="lblPayableAmount">
                                                Payable Amount</label>
                                            <div class="containingLabel">
                                                <asp:Label ID="lblPayableAmount" runat="server" Text="0"></asp:Label>
                                            </div>
                                        </section>
                                    </fieldset>
                                </div>
                                <div id="TrialPlan">
                                    <fieldset>
                                        <label>
                                            Details <span>( All fields marked with star are required )</span></label>
                                        <section>
                                            <label for="lblPlanName">
                                                Plan</label>
                                            <div class="containingLabel">
                                                <asp:Label ID="lblTrialPlanName" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblTrialPlanCode"
                                                    runat="server" Visible="false"></asp:Label>&nbsp;&nbsp;<asp:LinkButton ID="lnkTrialSelectPlan"
                                                        runat="server" OnClick="lnkTrialSelectPlan_Click">select plan</asp:LinkButton>
                                                <asp:LinkButton ID="lnkTrialPlanChange" runat="server" OnClick="lnkTrialPlanChange_Click"
                                                    Visible="false">change plan</asp:LinkButton>
                                                <asp:Label ID="lblTrialPlanId" runat="server" Visible="false"></asp:Label>
                                            </div>
                                        </section>
                                        <asp:Panel ID="pnlTrialNoOfUsers" runat="server" Visible="false">
                                            <section>
                                                <label for="ddlChargePrice">
                                                    No. of Users</label>
                                                <div class="containingLabel">
                                                    <span>
                                                        <asp:Label ID="lblTrialNoOfUsers" runat="server"></asp:Label></span>
                                                </div>
                                            </section>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlTrialMaxWrkFlow" runat="server" Visible="false">
                                            <section>
                                                <label for="lblMaxWorkflow">
                                                    MAX Workflow</label>
                                                <div class="containingLabel">
                                                    <asp:Label ID="lblTrialMaxWorkFlows" runat="server"></asp:Label>
                                                </div>
                                            </section>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlTrialPushMsgPerDay" runat="server" Visible="false">
                                            <section>
                                                <label for="lblMaxWorkflow">
                                                    Push Message / day</label>
                                                <div class="containingLabel">
                                                    <asp:Label ID="lblTrialPushMsgPerDay" runat="server"></asp:Label>
                                                </div>
                                            </section>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlTrialPushMsgPerMnth" runat="server" Visible="false">
                                            <section>
                                                <label for="lblMaxWorkflow">
                                                    Push Message / month</label>
                                                <div class="containingLabel">
                                                    <asp:Label ID="lblTrialPushMsgPerMnth" runat="server"></asp:Label>
                                                </div>
                                            </section>
                                        </asp:Panel>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <section>
                            <div id="divSaveBtn" class="g12 formDivCls">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="aspButton" OnClick="btnSave_Click" />
                            </div>
                        </section>
                    </div>
                    <div class="clear">
                    </div>
                </section>
                <div>
                    <asp:HiddenField ID="hidTabSelected" runat="server" />
                    <asp:HiddenField ID="hidPriceAmountPayable" runat="server" />
                    <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                    <asp:HiddenField ID="hidReTypePasswordEntered" runat="server" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divModalContainer" style="display: none">
        <div id="divPlanDetailsRpt" style="display: block">
            <asp:UpdatePanel ID="updModalContainer" runat="server">
                <ContentTemplate>
                    <div id="modalPopUpContent" style="height: 500px;">
                        <div id="divRepeater" style="margin-top: 5px;">
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server" ScrollBars="Vertical">
                                <asp:Repeater ID="rptPlanDetails" runat="server" OnItemCommand="rptPlanDetails_ItemCommand"
                                    OnItemDataBound="rptPlanDetails_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th style="display: none;">
                                                        PLAN CODE
                                                    </th>
                                                    <th>
                                                        NAME
                                                    </th>
                                                    <th>
                                                        WORKFLOW
                                                    </th>
                                                    <th>
                                                        PUSH MESSAGE *
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td style="display: none;">
                                                    <asp:Label ID="lblPlanCode" runat="server" Text='<%# Eval("PLAN_CODE") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <%--<asp:LinkButton ID="lnkPlanName" ToolTip="view details" runat="server" Text='<%# Eval("PLAN_NAME") %>'
                                                        CommandArgument='<%# Eval("PLAN_CODE") %>' CommandName="ViewDetails"></asp:LinkButton>--%>
                                                    <asp:Label ID="lblPlanName" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbWorkFlow" runat="server" Text='<%# Eval("MAX_WORKFLOW") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <%--<asp:Label ID="lblINRCharge" runat="server" Text='<%# Eval("USER_CHARGE_INR_PM") %>'></asp:Label>&nbsp;|&nbsp;
                                                    <asp:Label ID="lblUSDCharge" runat="server" Text='<%# Eval("USER_CHARGE_USD_PM") %>'></asp:Label>--%>
                                                    <asp:Label ID="lblPushMessagePerDay" runat="server" Text='<%# Eval("PUSHMESSAGE_PERDAY") %>'></asp:Label>&nbsp;|&nbsp;
                                                    <asp:Label ID="lblPushMessagePerMonth" runat="server" Text='<%# Eval("PUSHMESSAGE_PERMONTH") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkPrice" runat="server" Text="Price" ToolTip="view prices" CssClass="repeaterLink"
                                                        CommandName="ViewPrice"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkBuy" runat="server" Text="Select" CssClass="repeaterLink"
                                                        CommandName="Select"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                <td style="display: none;">
                                                    <asp:Label ID="lblPlanCode" runat="server" Text='<%# Eval("PLAN_CODE") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <%--<asp:LinkButton ID="lnkPlanName" ToolTip="view details" runat="server" Text='<%# Eval("PLAN_NAME") %>'
                                                        CommandArgument='<%# Eval("PLAN_CODE") %>' CommandName="ViewDetails"></asp:LinkButton>--%>
                                                    <asp:Label ID="lblPlanName" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbWorkFlow" runat="server" Text='<%# Eval("MAX_WORKFLOW") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <%--<asp:Label ID="lblINRCharge" runat="server" Text='<%# Eval("USER_CHARGE_INR_PM") %>'></asp:Label>&nbsp;|&nbsp;
                                                    <asp:Label ID="lblUSDCharge" runat="server" Text='<%# Eval("USER_CHARGE_USD_PM") %>'></asp:Label>--%>
                                                    <asp:Label ID="lblPushMessagePerDay" runat="server" Text='<%# Eval("PUSHMESSAGE_PERDAY") %>'></asp:Label>&nbsp;|&nbsp;
                                                    <asp:Label ID="lblPushMessagePerMonth" runat="server" Text='<%# Eval("PUSHMESSAGE_PERMONTH") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkPrice" runat="server" Text="Price" ToolTip="view prices" CssClass="repeaterLink"
                                                        CommandName="ViewPrice"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkBuy" runat="server" Text="Select" CssClass="repeaterLink"
                                                        CommandName="Select"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                        <div class="RptFooterMessage">
                                            * Per day | Per month</div>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                        </div>
                        <div id="divPlanDtlsAction" class="modalPopUpAction" style="margin-top: 3px;">
                            <asp:Button ID="btnPlanDetModalCancel" CommandArgument="Payment" runat="server" Text="Cancel"
                                OnClick="btnPlanDetModalCancel_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="divTrialPlanModal" style="display: none">
        <div id="divTrialPlanContainer" style="display: block">
            <asp:UpdatePanel ID="updTrialPlanSelect" runat="server">
                <ContentTemplate>
                    <div id="divTrialPlanRpt">
                        <div id="div4" style="margin-top: 5px;">
                            <asp:Panel ID="Panel1" CssClass="repeaterBox" runat="server" Style="height: 500px;
                                overflow: scroll;">
                                <asp:Repeater ID="rptTrialPlanDetails" runat="server" OnItemCommand="rptTrialPlanDetails_ItemCommand"
                                    OnItemDataBound="rptTrialPlanDetails_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th style="display: none;">
                                                        PLAN CODE
                                                    </th>
                                                    <th>
                                                        NAME
                                                    </th>
                                                    <th>
                                                        WORKFLOWS
                                                    </th>
                                                    <th>
                                                        NO. OF USERS
                                                    </th>
                                                    <th>
                                                        PUSH MESSAGE *
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td style="display: none;">
                                                    <asp:Label ID="lblPlanCode" runat="server" Text='<%# Eval("PLAN_CODE") %>'></asp:Label>
                                                    <asp:Label ID="lblPlanId" runat="server" Text='<%# Eval("PLAN_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <%--<asp:LinkButton ID="lnkPlanName" ToolTip="view details" runat="server" Text='<%# Eval("PLAN_NAME") %>'
                                                        CommandArgument='<%# Eval("PLAN_CODE") %>' CommandName="ViewDetails"></asp:LinkButton>--%>
                                                    <asp:Label ID="lblPlanName" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbWorkFlow" runat="server" Text='<%# Eval("MAX_WORKFLOW") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblNoofUsers" runat="server" Text='<%# Eval("MAX_USER") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPushMessagePerDay" runat="server" Text='<%# Eval("PUSHMESSAGE_PERDAY") %>'></asp:Label>&nbsp;|&nbsp;
                                                    <asp:Label ID="lblPushMessagePerMonth" runat="server" Text='<%# Eval("PUSHMESSAGE_PERMONTH") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkSelect" runat="server" Text="Select" CssClass="repeaterLink"
                                                        CommandName="Select"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                <td style="display: none;">
                                                    <asp:Label ID="lblPlanCode" runat="server" Text='<%# Eval("PLAN_CODE") %>'></asp:Label>
                                                    <asp:Label ID="lblPlanId" runat="server" Text='<%# Eval("PLAN_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <%--<asp:LinkButton ID="lnkPlanName" ToolTip="view details" runat="server" Text='<%# Eval("PLAN_NAME") %>'
                                                        CommandArgument='<%# Eval("PLAN_CODE") %>' CommandName="ViewDetails"></asp:LinkButton>--%>
                                                    <asp:Label ID="lblPlanName" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbWorkFlow" runat="server" Text='<%# Eval("MAX_WORKFLOW") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblNoofUsers" runat="server" Text='<%# Eval("MAX_USER") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPushMessagePerDay" runat="server" Text='<%# Eval("PUSHMESSAGE_PERDAY") %>'></asp:Label>&nbsp;|&nbsp;
                                                    <asp:Label ID="lblPushMessagePerMonth" runat="server" Text='<%# Eval("PUSHMESSAGE_PERMONTH") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkSelect" runat="server" Text="Select" CssClass="repeaterLink"
                                                        CommandName="Select"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                        <div class="RptFooterMessage">
                                            * Per day | Per month</div>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                        </div>
                        <div id="div5" class="modalPopUpAction" style="margin-top: 3px;">
                            <asp:Button ID="btnTrialModalCancel" CommandArgument="Trial" runat="server" Text="Cancel"
                                OnClick="btnPlanDetModalCancel_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="divPlanPriceDetailModal" style="display: none">
        <div id="divPlanPriceRptContainer" style="display: block">
            <asp:UpdatePanel ID="updPlanPriceDtlRpt" runat="server">
                <ContentTemplate>
                    <div id="divPlanPriceRpt" style="height: 300px;">
                        <div id="div6" style="margin-top: 5px;">
                            <asp:Panel ID="pnlPlanPriceRpt" CssClass="repeaterBox" runat="server" ScrollBars="Vertical">
                                <asp:Repeater ID="rptPlanPriceDtls" runat="server" OnItemCommand="rptPlanPriceDtls_ItemCommand"
                                    OnItemDataBound="rptPlanPriceDtls_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th style="display: none;">
                                                        PLAN CODE
                                                    </th>
                                                    <th>
                                                        NAME
                                                    </th>
                                                    <th>
                                                        CURRENCY
                                                    </th>
                                                    <th>
                                                        1-2 *
                                                    </th>
                                                    <th>
                                                        3-5 *
                                                    </th>
                                                    <th>
                                                        6-12 *
                                                    </th>
                                                    <th>
                                                        12-12 *
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td style="display: none;">
                                                    <asp:Label ID="lblPlanCode" runat="server" Text='<%# Eval("PLAN_CODE") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPlanName" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCurrency" runat="server" Text='<%# Eval("CURRENCY") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblOneTwo" runat="server" Text='<%# Eval("ONE_TWO") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblThreeFive" runat="server" Text='<%# Eval("THREE_FIVE") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblSixTwelve" runat="server" Text='<%# Eval("SIX_TWELVE") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblTwelveTwelve" runat="server" Text='<%# Eval("TWELVE_TWELVE") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                <td style="display: none;">
                                                    <asp:Label ID="lblPlanCode" runat="server" Text='<%# Eval("PLAN_CODE") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPlanName" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCurrency" runat="server" Text='<%# Eval("CURRENCY") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblOneTwo" runat="server" Text='<%# Eval("ONE_TWO") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblThreeFive" runat="server" Text='<%# Eval("THREE_FIVE") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblSixTwelve" runat="server" Text='<%# Eval("SIX_TWELVE") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblTwelveTwelve" runat="server" Text='<%# Eval("TWELVE_TWELVE") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                        <div class="RptFooterMessage">
                                            * Months</div>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            makeDatePickerWithMonthYear();
            $("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>

﻿<%@ Page Title="mficient" Language="C#" AutoEventWireup="true" CodeBehind="resetPassword.aspx.cs"
    Inherits="mFicientAdmin.resetPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <title>mFicient</title>
    <meta name="description" content="">
    <!-- Apple iOS and Android stuff -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon-precomposed" href="img/icon.png">
    <link rel="apple-touch-startup-image" href="img/startup.png">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
    <!-- Google Font and style definitions -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans:regular,bold" />
    <link rel="stylesheet" href="css/CanvasStyle.css" id="Link2" />
    <link rel="stylesheet" href="css/themes/CanvasTheme.css" id="Link1" />
    <link rel="stylesheet" href="css/themes/jquery-CP-Theme-ui.css" id="Link3" />
    <link rel="stylesheet" href="css/themes/jquery.uniform.css" id="Link4" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/themes/theme.css" id="themestyle" />
    <!--[if lt IE 9]>
	<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    <script src="Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script src="Scripts/login.js" type="text/javascript"></script>
    <script src="Scripts/WLJS.js" type="text/javascript"></script>
    <script src="Scripts/Scripts.js" type="text/javascript"></script>
    <script type="text/javascript">
        function clearFieldsUsedInEdit() {
            $('#' + '<%=txtNewPassword.ClientID %>').val("");
            $('#' + '<%=txtRetypeNewPassword.ClientID %>').val("");
            $('#divModalContainer').find('.alert').remove();
        }
        function rememberPasswordEntered() {
            var hidPassword = $('#' + '<%= hidPasswordEntered.ClientID %>');
            var hidReTypePasswordEntered = $('#' + '<%=hidReTypePasswordEntered.ClientID %>');
            var txtPassword = $('#' + '<%=txtNewPassword.ClientID %>');
            var txtRetypePassword = $('#' + '<%=txtRetypeNewPassword.ClientID %>');
            $(hidPassword).val($(txtPassword).val());
            $(hidReTypePasswordEntered).val($(txtRetypePassword).val());
        }
        function fillPasswordEntered() {
            var hidPassword = document.getElementById('<%= hidPasswordEntered.ClientID %>');
            var hidReTypePasswordEntered = $('#' + '<%=hidReTypePasswordEntered.ClientID %>');
            var txtPassword = document.getElementById('<%=txtNewPassword.ClientID %>');
            var txtRetypePassword = document.getElementById('<%=txtRetypeNewPassword.ClientID %>');
            if (hidPassword && hidReTypePasswordEntered) {
                if (hidPassword.value !== "") {
                    if (txtPassword)
                        $(txtPassword).val($(hidPassword).val());
                }
                else {
                    if (txtPassword)
                        $(txtPassword).val(""); //done for adding trimmed value.
                }
                if ($(hidReTypePasswordEntered).val() !== "") {
                    if (txtRetypePassword)
                        $(txtRetypePassword).val($(hidReTypePasswordEntered).val());
                }
                else {
                    if (txtRetypePassword)
                        $(txtRetypePassword).val(""); //done for adding trimmed value
                }
            }

        }
        function validateChangePwdForm() {
            var txtPassword = $('#' + '<%=txtNewPassword.ClientID %>');
            var txtRetypePassword = $('#' + '<%=txtRetypeNewPassword.ClientID %>');
            if ($(txtPassword).val() === "") {
                $.wl_Alert('Please enter a password.', 'info', '#divChangePwdError');
                return false;
            }
            else if ($(txtPassword).val() !==
            $(txtRetypePassword).val()) {
                $.wl_Alert('Password entered does not match.', 'info', '#divChangePwdError');
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</head>
<body id="login">
    <form id="form" runat="server" style="border: 0px;">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updResetPwdInfo" runat="server">
        <ContentTemplate>
            <header id="loginLogo">
                <div id="logo">
                    <a href="Default.aspx">mficient</a>
                </div>
            </header>
            <section id="content" class="loginContent">
                <div id="formDiv" style="margin-bottom: 1px;">
                    <asp:Panel ID="pnlResetPasswordInfo" runat="server" Visible="false">
                        <fieldset>
                            <section>
                                <label for="username" style="padding: 3% 0% 3% 5%;">
                                    User Name
                                </label>
                                <label for="username" style="padding: 3% 5% 3% 3%;">
                                    :</label>
                                <div style="margin-left: 20px; margin-top: 3%;">
                                    <asp:Label CssClass="resetLable" ID="lblUserName" runat="server"></asp:Label></div>
                            </section>
                            <section>
                                <label for="txtResetCode">
                                    Reset Code &nbsp;<span style="font-style: italic; font-size: x-small;">(&nbsp;As given
                                        in your email&nbsp;)</span></label>
                                <div>
                                    <input id="txtResetCode" type="text" name="resetcode" runat="server" />
                                </div>
                            </section>
                            <section>
                                <div>
                                    <asp:Button ID="btnReset1" runat="server" Text="Reset" CssClass="fr aspButton" OnClick="btnReset_Click" />
                                </div>
                            </section>
                        </fieldset>
                    </asp:Panel>
                    <%--<asp:Panel ID="pnlResetPassword" runat="server" Visible="false">
                        <fieldset>
                            <section>
                                <label for="username">
                                    Company Id</label>
                                <div>
                                    <asp:TextBox ID="txtCompanyId" runat="server"></asp:TextBox></div>
                            </section>
                            <section>
                                <label for="username">
                                    User Name</label>
                                <div>
                                    <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox></span></div>
                            </section>
                            <section>
                                <div>
                                    <asp:Button ID="btnResetPassword" runat="server" Text="Reset" CssClass="fr aspButton"
                                        OnClick="btnResetPassword_Click" />
                                </div>
                            </section>
                        </fieldset>
                    </asp:Panel>--%>
                </div>
            </section>
            <asp:HiddenField ID="hidQueryString" runat="server" />
            <asp:HiddenField ID="hidUsrIdAndType" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="TransferUpdatePanel" UpdateMode="Conditional">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:HiddenField ID="hfs" runat="server" />
            <asp:HiddenField ID="hfp" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <section>
        <div id="divModalContainer">
            <asp:UpdatePanel ID="updModalContainer" runat="server">
                <ContentTemplate>
                    <div id="divChangePwdError">
                    </div>
                    <div id="divChangePwdContainer" class="manageGroupPopUpCont">
                        <div>
                            <label for="<%=txtNewPassword.ClientID %>">
                                Password</label>
                            <div>
                                <asp:TextBox ID="txtNewPassword" TextMode="Password" onblur="rememberPasswordEntered();"
                                    runat="server" Style="width: 100%;" MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                        <div>
                            <label for="<%=txtRetypeNewPassword.ClientID %>">
                                Retype Password</label>
                            <div>
                                <asp:TextBox ID="txtRetypeNewPassword" TextMode="Password" onblur="rememberPasswordEntered();"
                                    runat="server" Style="width: 100%;" MaxLength="20"></asp:TextBox>
                            </div>
                        </div>
                        <div class="modalPopUpAction">
                            <div id="divPopUpActionCont" class="popUpActionCont">
                                <asp:Button ID="btnSavePassword" runat="server" Text="Save" CssClass="aspButton"
                                    OnClientClick="return validateChangePwdForm();" OnClick="btnSavePassword_Click" /><asp:Button
                                        ID="btnCancel" runat="server" Text="Cancel" CssClass="aspButton" OnClientClick="clearFieldsUsedInEdit();closeModalPopUp('divModalContainer');return false;" />
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                    <asp:HiddenField ID="hidReTypePasswordEntered" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </section>
    <div id="divWaitBox" class="waitModal">
        <div id="WaitAnim">
            <asp:Image ID="WaitImage" runat="server" ImageUrl="~/css/images/icons/dark/progress.gif"
                AlternateText="Please Wait" BorderWidth="0px" />
        </div>
    </div>
    </form>
</body>
<script type="text/javascript">
    Sys.Application.add_init(application_init);
    function application_init() {
        //Sys.Debug.trace("Application.Init");
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_initializeRequest);
        prm.add_endRequest(prm_endRequest);
    }
    function prm_initializeRequest() {
        //$("input").uniform();
        showWaitModal();
    }
    function prm_endRequest() {
        //$("input").uniform();
        hideWaitModal();
    }
</script>
</html>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCCompanyDetails.ascx.cs"
    Inherits="mFicientAdmin.UserControl.UCCompanyDetails" %>
<div id="divCompanyDetails" style="width: 100%;">
    <div class="modalPopUpDetails">
        <div id="divDetails" style="width: 100%;">
            <div style="float: left; width: 100%;">
                <fieldset>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForCmpId" runat="server" Text="Company Id"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label9" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblCompanyId" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForName" runat="server" Text="Name"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label2" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblName" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForAddress" runat="server" Text="Address"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label10" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblAddress" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForCity" runat="server" Text="City"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label11" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblCity" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForState" runat="server" Text="State"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label12" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblState" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForCountry" runat="server" Text="Country"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label7" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblCountry" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForContactNo" runat="server" Text="Support Contact"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label6" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblSupportContactNo" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForEmail" runat="server" Text="Support Email"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label3" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblEmail" runat="server"></asp:Label>
                        </div>
                    </section>
                </fieldset>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidUserId" runat="server" />
</div>

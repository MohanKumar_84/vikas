﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace mFicientAdmin.UserControl
{
    public partial class UCCurrentPlan : System.Web.UI.UserControl
    {
        string _companyId;

        public string CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        DataTable getCurrentPlan(string planCode)
        {
            try
            {
                GetPlanDetailsAdmin objPlanDetail = new GetPlanDetailsAdmin();
                objPlanDetail.PlanDetails(planCode);
                return objPlanDetail.ResultTable;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        void fillControls(DataTable planDetails)
        {
            try
            {
                if (planDetails.Rows.Count > 0)
                {
                    lblPlanCode.Text = Convert.ToString(planDetails.Rows[0]["PLAN_CODE"]);
                    lblPlanName.Text = Convert.ToString(planDetails.Rows[0]["PALN_NAME"]);
                    lblMaxWorkFlow.Text = Convert.ToString(planDetails.Rows[0]["MAX_WORKFLOW"]);
                    lblCharge.Text = Convert.ToString(planDetails.Rows[0]["USER_CHARGE_INR_PM"]) + " / " + Convert.ToString(planDetails.Rows[0]["USER_CHARGE_USD_PM"]);
                    lblPushMessage.Text = Convert.ToString(planDetails.Rows[0]["PUSHMESSAGE_PERDAY"]);
                    lblPushMessagePM.Text = Convert.ToString(planDetails.Rows[0]["PUSHMESSAGE_PERMONTH"]);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void processShowCompanyDetail(string companyId)
        {
            try
            {
                fillControls(getCurrentPlan(companyId));
            }
            catch
            {

            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin.UserControl
{
    public partial class ServerDetails : System.Web.UI.UserControl
    {

        public delegate void ActionClick(object sender, EventArgs e);
        public event ActionClick OnEditClick;


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lnkEditUser_Click(object sender, EventArgs e)
        {
            if (OnEditClick != null)
            {
                LinkButton lnkEdit = (LinkButton)sender;
                lnkEdit.CommandArgument = hidUserId.Value;
                OnEditClick(sender, e);
            }
        }


        public void GetServerDetails()
        {
            GetServerDetailsAdmin get = new GetServerDetailsAdmin();
            DataSet ds= get.GetSdetails();
            if (ds != null && ds.Tables.Count >= 0)
            {
                FillServerDetails(ds);   
            }
       
        }

        public void FillServerDetails(DataSet ds)
        {
            DataTable dt = ds.Tables[0];
           txtname.Text = Convert.ToString(dt.Rows[0]["SERVER_NAME"]);
            txturl.Text = Convert.ToString(dt.Rows[0]["SERVER_URL"]);
            txtip.Text = Convert.ToString(dt.Rows[0]["SERVER_IP_ADDRESS"]);
            chkIsActive.Checked = Convert.ToBoolean(dt.Rows[0]["IS_ENABLED"]);
        }

        protected void Chck_CheckedChanged(object sender, EventArgs e)
        {

        }

    }
}
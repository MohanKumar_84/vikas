﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServerDetails.ascx.cs" Inherits="mFicientAdmin.UserControl.ServerDetails" %>
<div id="divUserDetails" style="width: 100%;">
    <div class="modalPopUpDetails">
        <div id="DivUserDetails" style="width: 100%;">
            <div id="divUserDtlsHeader" class="innerHeader" style="border-bottom: 1px solid">
                <asp:Label ID="lblUserDtlsHeader" runat="server" Text="Details"></asp:Label>
                <div id="divUCActivityButtons" style="float: right; margin-bottom: 2px;">
                    <asp:LinkButton ID="lnkEditUser" runat="server" OnClick="lnkEditUser_Click">Ok</asp:LinkButton>&nbsp;<span id="ucSeparatorOne">|</span>&nbsp;
                   
                </div>
                <div class="clear">
                </div>
            </div>
            <div style="float: left; width: 55%;">
                <fieldset>
                    <section>
                                                <label for="txtname">
                                                    Server Name</label>
                                                <div>
                                                    <asp:TextBox ID="txtname" runat="server"></asp:TextBox>
                                                </div>
                                            </section>
                                            <section>
                                                <label for="txturl">
                                                    Server Url</label>
                                                <div>
                                               <asp:TextBox ID="txturl" runat="server"></asp:TextBox>
                                                </div>
                                            </section>
                                            <section>
                                                <label for="txtip">
                                                   Server IP</label>
                                                <div>
                                                    <asp:TextBox ID="txtip" runat="server"></asp:TextBox>
                                                </div>
                                            </section>
                                            
                                            <section>
                                                <label for="chkIsActive">
                                                    Enabled</label>
                                                <div>
                                                    <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" />
                                                </div>
                                            </section>


                </fieldset>
            </div>
           
    </div>
    <asp:HiddenField ID="hidUserId" runat="server" />
</div>
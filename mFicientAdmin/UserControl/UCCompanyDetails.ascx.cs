﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace mFicientAdmin.UserControl
{
    public partial class UCCompanyDetails : System.Web.UI.UserControl
    {
        string _companyId;

        public string CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        DataTable getCompanyDetails(string companyId)
        {
            AddCompanyDetailsAdmin objCompanyDetails = new AddCompanyDetailsAdmin();
            objCompanyDetails.GetDetails(companyId);
            DataTable dt = objCompanyDetails.ResultTable;
            return dt;
        }
        void fillControls(DataTable companyDetails)
        {
            if (companyDetails.Rows.Count > 0)
            {
                lblCompanyId.Text = Convert.ToString(companyDetails.Rows[0]["COMPANY_ID"]);
                lblName.Text = Convert.ToString(companyDetails.Rows[0]["COMPANY_NAME"]);
                lblCountry.Text = Convert.ToString(companyDetails.Rows[0]["COUNTRY_NAME"]);
                lblAddress.Text = Convert.ToString(companyDetails.Rows[0]["STREET_ADDRESS1"]) + " " + Convert.ToString(companyDetails.Rows[0]["STREET_ADDRESS2"]) + " " + Convert.ToString(companyDetails.Rows[0]["STREET_ADDRESS3"]);
                lblCity.Text = Convert.ToString(companyDetails.Rows[0]["CITY_NAME"]);
                lblState.Text = Convert.ToString(companyDetails.Rows[0]["STATE_NAME"]);

                lblEmail.Text = Convert.ToString(companyDetails.Rows[0]["SUPPORT_EMAIL"]);
                lblSupportContactNo.Text = Convert.ToString(companyDetails.Rows[0]["SUPPORT_CONTACT"]);
                
                //lblBusinessType.Text = Convert.ToString(companyDetails.Rows[0]["TYPE_OF_COMPANY"]);
                //lblContactPerson.Text = Convert.ToString(companyDetails.Rows[0]["CONTACT_PERSON"]);
                //lblContactNo.Text = Convert.ToString(companyDetails.Rows[0]["CONTACT_NUMBER"]);
                //lblEmail.Text = Convert.ToString(companyDetails.Rows[0]["CONTACT_EMAIL"]);
                //DateTime EnqDate = new DateTime(Convert.ToInt64(companyDetails.Rows[0]["ENQUIRY_DATETIME"]));
                //lblEnquiryDate.Text = EnqDate.ToShortDateString();

            }
        }
        public void processShowCompanyDetail(string companyId)
        {
            fillControls(getCompanyDetails(companyId));
        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCCurrentPlan.ascx.cs" Inherits="mFicientAdmin.UserControl.UCCurrentPlan" %>
<div id="divCurrentPlanDetails" style="width: 100%;">
    <div class="modalPopUpDetails">
        <div id="divDetails" style="width: 100%;">
            <div style="float: left; width: 100%;">
                <fieldset>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForPlnCode" runat="server" Text="Plan Code"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label9" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblPlanCode" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForPlanName" runat="server" Text="Name"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label2" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblPlanName" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForMaxWorkFlow" runat="server" Text="Address"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label10" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblMaxWorkFlow" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForCharge" runat="server" Text="User Charge / Month (INR / USD) "></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label11" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblCharge" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForPushMessageCount" runat="server" Text="Push Message / Day"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label12" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblPushMessage" runat="server"></asp:Label>
                        </div>
                    </section>
                    <section>
                        <div class="column1">
                            <asp:Label ID="lblForPushMessagePM" runat="server" Text="Push Message / Month"></asp:Label>
                        </div>
                        <div class="column2">
                            <asp:Label ID="Label4" runat="server" Text=":"></asp:Label>
                        </div>
                        <div class="column3">
                            <asp:Label ID="lblPushMessagePM" runat="server"></asp:Label>
                        </div>
                    </section>
                </fieldset>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidUserId" runat="server" />
</div>
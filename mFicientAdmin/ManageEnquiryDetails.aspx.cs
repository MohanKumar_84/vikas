﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public partial class ManageEnquiryDetails : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;


            if (!IsPostBack)
            {
                BindCountryDD(ddlcountry);
                LinkButton ChangePass = (LinkButton)this.Master.Master.FindControl("lbChangePwd");
                Literal ltlUserType = (Literal)this.Master.Master.FindControl("ltUserType");

                /*if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else
                {
                    if (Page.PreviousPage != null)
                    {
                        string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                        prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                        ContentPlaceHolder cphPageCanvas = (ContentPlaceHolder)PreviousPage.Master.Master.FindControl("form").FindControl("MainCanvas").FindControl("PageCanvas");
                        hfs.Value = ((HiddenField)cphPageCanvas.FindControl("hfs")).Value;
                    }
                }*/
                if (string.IsNullOrEmpty(hfsValue))
                    Response.Redirect(@"Default.aspx");
                    
                        string Role = strRole.ToUpper();
                        if (Role == "A")
                        {
                            ltlUserType.Text = "mFicient Admin";
                            lnkAddNewUser.Visible = false;
                            ChangePass.PostBackUrl = "~/ChangePassword.aspx";
                        }

                        else if (Role == "R")
                        {
                            ltlUserType.Text = "Reseller";
                            lnkAddNewUser.Visible = true;
                            ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                        }

                        else if (Role == "S")
                        {
                            ltlUserType.Text = "Sales Executive";
                            lnkAddNewUser.Visible = false;
                            ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                        }
                        else if (Role == "SM")
                        {
                            ltlUserType.Text = "Sales Manager";
                            lnkAddNewUser.Visible = false;
                            ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                        }

                        else if (Role == "SUPP")
                        {
                            ltlUserType.Text = "Support";
                            lnkAddNewUser.Visible = false;
                            ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                        }

                        else if (Role == "ACC")
                        {
                            ltlUserType.Text = "Accounts";
                            lnkAddNewUser.Visible = false;
                            ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                        }
                        AddServerDetails ad = new AddServerDetails();
                        DataTable dt = ad.GetEnquiryDetails();
                        if (dt.Rows.Count > 0)
                        {
                            lblHeaderInfo.Text = "<h1>Enquiry Details</h1>";
                            rptUserDetails.Visible = true;
                            rptUserDetails.DataSource = dt;
                            rptUserDetails.DataBind();
                        }
                        else if (dt.Rows.Count == 0)
                        {
                            lblHeaderInfo.Text = "<h1>There are no details of Enquiries added yet</h1>";
                            rptUserDetails.Visible = false;
                        }
                        Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "$(\"input\").uniform();", true);

                    }
           
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "testing", " $(\"select\").uniform();$(\"input\").uniform();", true);
            }
           
        }
        void BindCountryDD(DropDownList _ddlDropdownId)
        {
            GetCountryList countryList = new GetCountryList();
            countryList.GetCountryName();
            DataTable dt = countryList.ResultTable;
            _ddlDropdownId.DataSource = dt;
            _ddlDropdownId.DataValueField = "COUNTRY_NAME";
            _ddlDropdownId.DataTextField = "COUNTRY_NAME";
            _ddlDropdownId.DataBind();
            _ddlDropdownId.Items.Insert(0, new ListItem("Select Country", "-1"));

        }
        protected void lnkok_Click(object sender, EventArgs e)
        {
            Utilities.showModalPopup("divUserDtlsModal", this.Page, "<strong>Enquiry Details</strong>", "350", true,false);
            enabledisablepanels("Info");
            upUserDtlsModal.Update();
        }

        protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item;
            Label lit_id = (Label)item.FindControl("lblid");

            if (e.CommandName == "Info")
            {
                FillEnquiryDetails(lit_id.Text);
            }
        }
     protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
     {
         switch (e.CommandName)
         {
             case "Info":
                 Info(e);
                 ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing2", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                 break;
         }
     }

     protected void enabledisablepanels(string mode)
     {
            if (mode == "Info")
            {
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = true;
            }
      }
     
     protected void FillEnquiryDetails(string EnquiryId)
     {
         GetServerDetailsAdmin enquirydetails = new GetServerDetailsAdmin();
         enquirydetails.EnquiryDetails(EnquiryId);
         DataTable dt = enquirydetails.ResultTable;

         if (Convert.ToString(dt.Rows[0]["SALES_MNGR_ID"])=="" || Convert.ToString(dt.Rows[0]["STATUS"]) == "0")
         {
             lstatus.Text = "New";
         }
         else if (Convert.ToString(dt.Rows[0]["SALES_MNGR_ID"]) != "" && Convert.ToString(dt.Rows[0]["SALES_ID"]) == "" || Convert.ToString(dt.Rows[0]["STATUS"]) == "1")
         {
             lstatus.Text = "Processing(Sales Manager)";
         }

         else if (Convert.ToString(dt.Rows[0]["SALES_MNGR_ID"]) != "" && Convert.ToString(dt.Rows[0]["SALES_ID"]) != "" && Convert.ToString(dt.Rows[0]["RESELLER_ID"])  == "" || Convert.ToString(dt.Rows[0]["STATUS"]) == "2")
         {
             lstatus.Text = "Processing(Sales Person)";
         }
         else if (Convert.ToString(dt.Rows[0]["STATUS"]) == "3")
         {
             lstatus.Text = "Processing (Reseller)";
         }

         else if (Convert.ToString(dt.Rows[0]["STATUS"]) == "4")
         {
             lstatus.Text = "Successfully processed";
         }

         else if (Convert.ToString(dt.Rows[0]["STATUS"]) == "5")
         {
             lstatus.Text = "Processing failed";
             Remarkdiv.Visible = true;
             lblR.Text = Convert.ToString(dt.Rows[0]["REMARK_CODE"]);
         }
         
         if (dt.Rows.Count > 0)
         {
             hidDetailsFormMode.Value = EnquiryId;
             lid.Text = Convert.ToString(dt.Rows[0]["ENQUIRY_ID"]);
             lname.Text = Convert.ToString(dt.Rows[0]["COMPANY_NAME"]);
             lcountry.Text = Convert.ToString(dt.Rows[0]["COUNTRY_NAME"]);
             ltype.Text = Convert.ToString(dt.Rows[0]["TYPE_OF_COMPANY"]);
             lperson.Text = Convert.ToString(dt.Rows[0]["CONTACT_PERSON"]);
             lnumber.Text = Convert.ToString(dt.Rows[0]["CONTACT_NUMBER"]);
             lemail.Text = Convert.ToString(dt.Rows[0]["CONTACT_EMAIL"]);
             DateTime EnqDate = new DateTime(Convert.ToInt64(dt.Rows[0]["ENQUIRY_DATETIME"]));
             Enquirydate.Text = EnqDate.ToShortDateString();
             Remarkdiv.Visible = false;
         }
         else
         {
             ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
         }
     }

     void Info(RepeaterCommandEventArgs e)
     {
         Label enquiry_id = (Label)e.Item.FindControl("lblid");
         FillEnquiryDetails(enquiry_id.Text);
     }

     void StatusddlRepeater()
     {
         EnquiryDetailsAdmin get = new EnquiryDetailsAdmin();
         DataTable objdt = get.GetStatusdd(ddlstatus.SelectedValue);
         if (objdt.Rows.Count > 0)
         {
             rptUserDetails.DataSource = objdt;
             rptUserDetails.DataBind();
         }
     }

     void CountryddlRepeater()
     {
         EnquiryDetailsAdmin get = new EnquiryDetailsAdmin();
         DataTable objdt = get.GetCountrydd(ddlcountry.SelectedValue);
         if (objdt.Rows.Count > 0)
         {
             rptUserDetails.DataSource = objdt;
             rptUserDetails.DataBind();
         }
     }
     protected void bttnGet_Click(object sender, EventArgs e)
     {
         if (ddlcountry.SelectedValue != "-1")
         {
             CountryddlRepeater();
         }

         else if (ddlstatus.SelectedValue != "--Select Status--")
         {
             StatusddlRepeater();
         }
       }

     protected void lnkAddNewMobileUser_Click(object sender, EventArgs e)
     {
         pnlMobileUserForm.Visible = false;
         pnlRepeaterBox.Visible = true;
     }

     protected void Page_Init(object sender, EventArgs e)
     {
         LinkButton ChangePass = (LinkButton)this.Master.Master.FindControl("lbChangePwd");
         ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
     }

    }
}
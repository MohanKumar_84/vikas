﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;

            /*if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else
                {
                    if (Page.PreviousPage != null)
                    {
                        string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                        prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                        ContentPlaceHolder cphPageCanvas = (ContentPlaceHolder)PreviousPage.Master.Master.FindControl("form").FindControl("MainCanvas").FindControl("PageCanvas");
                        if (prvPageName == "AdminDashboard.aspx")
                        {
                            hfs.Value = ((HiddenField)cphPageCanvas.FindControl("hfs")).Value;
                        }
                    }
                }
            }*/
        }
        
        void showPasswordModalPopUp(string modalPopUpHeader)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "ShowPopUp", @"showModalPopUp('divChangePasswordContainer','" + modalPopUpHeader + "',400);", true);
        }

        //protected void BttnChangePwd_Click(object sender, EventArgs e)
        //{
        //    string strFieldBorder = "";
        //    string strMessage = Validate();
        //    if (!String.IsNullOrEmpty(strMessage))
        //    {
        //        ShowErrors(strMessage, strFieldBorder);
        //    }
        //    else
        //    {
        //        if (Utilities.GetMd5Hash(txtOldPassword.Text) == Utilities.GetMd5Hash(txtNewPassword.Text))
        //        {
        //            Utilities.showMessage("New password entered is same as that of old password.Please choose a different password", this.Page, DIALOG_TYPE.Info);
        //        }
        //        else 
        //        {
        //            ChangePasswordAdmin obj = new ChangePasswordAdmin();
        //            obj.ChangePass(txtOldPassword.Text, txtNewPassword.Text, strUserName);
        //            if (obj.StatusCode > 0)
        //            {
        //                ShowErrors("Try Again", "");
        //            }
        //            else
        //            {
        //                ShowErrors("Password Changed Successfully", "");
        //            }
        //        }
        //    }
        //}
        public void ShowErrors(string _Message, string _FieldBorder)
        {
            string strColor = @"inherit";
            string strAlign = @"left";
            string TextMessage = _Message;
            string FieldBorder = @"";
            FieldBorder += _FieldBorder;
            if (strColor == @"inherit")
            {
                strColor = @"";
            }
            string ScriptToRun = @"showWait(false);showProcessing(true, '" + TextMessage + @"', '" + strColor + @"', '" + strAlign + @"');showFormErrors('" + FieldBorder + @"');";
        }

        public string Validate()
        {
            string strMessage = "";
            if (txtNewPassword.Text.Length < 6)
            {
                strMessage += "* New password should be atleast 6 characters long " + "<br />";
            }
            else if (String.IsNullOrEmpty(txtNewPassword.Text))
            {
                strMessage += "* Please enter new password" + "<br />";
            }
            else if (!Utilities.IsValidString(txtNewPassword.Text, true, true, true, @".,#%^&*:;()[]{}?/\+=-_|~@", 6, 64, false, false))
            {
                strMessage += "* Please enter valid new password" + "<br />";
            }
            if(String.IsNullOrEmpty(txtOldPassword.Text))
            {
                strMessage += "* Please enter old password" + "<br />";
            }
            
            else if (txtNewPassword.Text != txtReEnterPassword.Text)
            {
                strMessage += "*Password provided do not match.Please re-enter the same password" + "<br />";
            }
            return strMessage;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string strMessage = Validate();
            if (!String.IsNullOrEmpty(strMessage))
            {
                showAlert(strMessage);
            }
            else
            {
                    ChangePasswordAdmin obj = new ChangePasswordAdmin();
                    obj.ChangePass(txtOldPassword.Text, txtNewPassword.Text, strUserName);
                    int intStatusCode = obj.StatusCode;
                    string strDescription = obj.StatusDescription;
                    if (strDescription != null && obj.StatusCode > 0)
                    {
                        Utilities.showMessage(strDescription, this.Page, "second script", DIALOG_TYPE.Error);
                    }
                    else
                    {
                        Utilities.showMessage("Password Changed Successfully", this.Page, "second script", DIALOG_TYPE.Info);
                    }
                }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
        }
        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#formDiv'" + ");", true);
        }
     }
}
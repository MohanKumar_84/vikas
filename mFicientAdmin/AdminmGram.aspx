﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/master/Canvas.Master"
    CodeBehind="AdminmGram.aspx.cs" Inherits="mFicientAdmin.AdminmGram" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script type="text/javascript">

        function chkSendImmediatelyClicked(sender) {
            var divSheduledTimeView = document.getElementById('<%=pnlSheduledHrsMin.ClientID %>');
            var chkSheduledHrsMin = document.getElementById('<%=chkSheduledHrsMin.ClientID %>');
            if ($(sender).is(':checked')) {
                uncheckUniformedCheckBox($(chkSheduledHrsMin));
            }
            else {
                checkUniformedCheckBox($(chkSheduledHrsMin));
            }
        }

        function checkUniformedCheckBox(checkboxObject) {
            $(checkboxObject).attr('checked', 'checked');
            var containerOfChkBox = $(checkboxObject).parent();
            if ($(containerOfChkBox).is("span")) {
                $(containerOfChkBox).attr("class", "checked");
            }
        }


        function chkSheduledHrsMinClicked(sender) {
            var divSheduledTimeView = document.getElementById('<%=pnlSheduledHrsMin.ClientID %>');
            var chkSheduledHrsMin = document.getElementById('<%=chkSheduledHrsMin.ClientID %>');
            var chkSheduleImmediate = document.getElementById('<%=chkSheduleImmediate.ClientID %>');
            if (!($(sender).is(':checked'))) {
                checkUniformedCheckBox($(chkSheduleImmediate));
            }
            else {
                uncheckUniformedCheckBox($(chkSheduleImmediate));
            }
        }


        function update_counter(counter, text_elem, max_len) {
            var counterElem = document.getElementById(counter);
            var textElem = document.getElementById('<%=txtMessage.ClientID %>');
            var len = textElem.value.length;
            //var val = max_len - textElem.value.length;
            if (checkMaxLen(textElem, max_len)) {
                var val = textElem.value.length;
                counterElem.innerHTML = val;
            }
        }


        function checkMaxLen(txt, maxLen) {
            try {
                if (txt.value.length > (maxLen - 1)) {
                    var cont = txt.value;
                    txt.value = cont.substring(0, (maxLen - 1));
                    return false;
                }
                else {
                    return true;
                };
            } catch (e) {
            }
        }



        function uncheckUniformedCheckBox(checkboxObject) {
            $(checkboxObject).removeAttr('checked');
            var containerOfChkBox = $(checkboxObject).parent();
            if ($(containerOfChkBox).is("span")) {
                $(containerOfChkBox).removeAttr("class");
            }
        }
    function changeDatePickerFormat() {
        var datePicker = $('#' + '<%=txtSendDate.ClientID %>');
        $(datePicker).datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: "+2D",
            minDate: "-0D",
            showOn: "button",
            buttonImage: "css/images/icons/dark/calendar.png",
            buttonImageOnly: true
        });
        if (datePicker) {
            preventDefaultOfDate();
        }
    }

    function changeDatePickerFormat() {
        var datePicker = $('#' + '<%=txtSendDate.ClientID %>');
        $(datePicker).datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: "+2D",
            minDate: "-0D",
            showOn: "button",
            buttonImage: "css/images/icons/dark/calendar.png",
            buttonImageOnly: true
        });
        if (datePicker) {
            preventDefaultOfDate();
        }
    }
    function makeDatePickerWithInitialDate() {
        var datePicker = $('#' + '<%=txtSendDate.ClientID %>');
        $(datePicker).datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: "+2D",
            minDate: "-0D",
            showOn: "button",
            buttonImage: "css/images/icons/dark/calendar.png",
            buttonImageOnly: true
        }).datepicker('setDate', '+1');
        if (datePicker) {
            preventDefaultOfDate();
        }
    }

    function getOffsetTimeOfClient() {
        x = new Date();
        var hidTimeOffset = document.getElementById('<%=hidTimeOffsetOfClient.ClientID %>');
        currentTimeZoneOffsetInMin = x.getTimezoneOffset();
        hidTimeOffset.value = currentTimeZoneOffsetInMin.toString();
    }
    function preventDefaultOfDate() {
        var datePicker = document.getElementById('<%=txtSendDate.ClientID %>');
        stopDefualtOfDateTxtBox(datePicker);
    }
    function processSelectDeselectAll(checkBox, idOfTemplate, chkCheckBoxList, processFor) {
        var chkCheckBoxListItems = $(chkCheckBoxList)[0].getElementsByTagName("input");
        var iNoOfCheckBoxesExceptAll = chkCheckBoxListItems.length - 1;
        var iNoOfCheckBoxesSelected = 0;
        if (!(checkBox === chkCheckBoxListItems[0])) {
            var isAllSelectedInTemplate = false;
            for (var intCounter = 1; intCounter <= chkCheckBoxListItems.length - 1; intCounter++) {
                if ($(chkCheckBoxListItems[intCounter]).is(':checked')) {
                    // Item is selected
                    iNoOfCheckBoxesSelected++;
                }
            }
            if (iNoOfCheckBoxesExceptAll == iNoOfCheckBoxesSelected) {
                isAllSelectedInTemplate = true;
            }
            if (isAllSelectedInTemplate) {
                $(chkCheckBoxListItems[0]).attr('checked', 'checked');
                var immediateSpanContainer = $(chkCheckBoxListItems[0]).parent();
                var outerSpanContainer = $(chkCheckBoxListItems[0]).parent().parent().parent();
                immediateSpanContainer.attr("class", "checked");
                outerSpanContainer.attr("class", "checked");
            }
            else {
                $(chkCheckBoxListItems[0]).removeAttr('checked');
                var immediateSpanContainer = $(chkCheckBoxListItems[0]).parent();
                var outerSpanContainer = $(chkCheckBoxListItems[intCounter]).parent().parent().parent();
                immediateSpanContainer.removeAttr("class");
                outerSpanContainer.removeAttr("class");
            }
        }
        else {
            if ($(chkCheckBoxListItems[0]).is(':checked')) {
                for (var intCounter = 1; intCounter <= chkCheckBoxListItems.length - 1; intCounter++) {

                    $(chkCheckBoxListItems[intCounter]).attr('checked', 'checked');
                    var immediateSpanContainer = $(chkCheckBoxListItems[intCounter]).parent();
                    var outerSpanContainer = $(chkCheckBoxListItems[intCounter]).parent().parent().parent();
                    immediateSpanContainer.attr("class", "checked");
                    outerSpanContainer.attr("class", "checked");
                }

            }
            else {
                for (var intCounter = 1; intCounter <= chkCheckBoxListItems.length - 1; intCounter++) {
                    if ($(chkCheckBoxListItems[intCounter]).is(':checked')) {
                        // Item is selected
                        $(chkCheckBoxListItems[intCounter]).removeAttr('checked');
                        var immediateSpanContainer = $(chkCheckBoxListItems[intCounter]).parent();
                        var outerSpanContainer = $(chkCheckBoxListItems[intCounter]).parent().parent().parent();
                        immediateSpanContainer.removeAttr("class");
                        outerSpanContainer.removeAttr("class");
                    }
                }
            }
        }
//        if (processFor == ProcessFor.DIVISION_DEPARTMENT) {
//            checkDivisionSelectAll();
//        }
//        else {
//            checkRegionSelectAll();
//        }
    }
    function stopDefualtOfDateTxtBox(txtBox) {
        if (txtBox) {
            txtBox.addEventListener(
                                        'keydown', stopDefAction, false
                                    );
        }
    }

    function uniformCheckbox() {
        $("input:not(div.checker input)").uniform();
        $("input").uniform();
    }


    $(window).load(function () {
        uniformCheckbox();
        chkSheduledHrsMinClicked(this);
    });




    function validateFormFields() {
        var errorMessage = "";
             // var Enterprise = $('select[id$=drpenterprisedetails]').val();
        var txtMessage = document.getElementById('<%=txtMessage.ClientID %>');
        var chkSheduleImmediately = $('#' + '<%=chkSheduleImmediate.ClientID %>');
        var txtSendDate = $('#' + '<%=txtSendDate.ClientID %>');
        var anyUserSelected = false;
        if ($(txtMessage).val() === "") {
            errorMessage += "Please enter a message </br>";
        }
        if (!$(chkSheduleImmediately).is(':checked')) {
            if ($(txtSendDate).val() === "") {
                errorMessage += "Please select a date to send message if not sheduled immediately. </br>";
            }
        }
        if (!(errorMessage === "")) {
            
            $('#divContainer').children('.alert').remove();
            $.wl_Alert(errorMessage, 'info', '#divContainer');
            return false;
        }
        else {
            return true;
        }
    }

</script>
 <style type="text/css">
        .pmHeaderLabel
        {
            font-weight: bold;
            font-size: 13px;
            color: #444444;
        }
        div.modalPopUp .checkboxListTbl table tr td
        {
            border: none;
            text-align: left;
            width: 150px;
        }
        div.modalPopUp .checkboxListTbl table tr td
        {
            padding: 1px 3px;
        }
        ul.token-input-list-facebook
        {
            margin: 0px 0px 0px 0px;
            width: 99%;
        }
        div.sendToSubParts
        {
            margin-left: 15px;
            padding: 3px 0;
        }
        div.sendToSubParts.withMarginTop
        {
            margin-top: 5px;
        }
        div.sendToSubParts .headerLabels
        {
            float: left;
            width: 150px;
        }
        div.sendToSubParts .headerLabelsColon
        {
            float: left;
            width: auto;
        }
        div.sendToSubParts .headerInfoSpan
        {
            float: left;
            width: 200px;
            margin-left: 10px;
        }
        div.combowrap.comboFacebookStyle
        {
            -webkit-border-radius: none !important;
            -moz-border-radius: none !important;
            border-radius: none !important;
            background: #ffffff;
        }
        .repeater.modalPopUp table tr th
        {
            border: none;
        }
        .sheduleHrs .spanLabel
        {
            float: left;
            position: relative;
            top: 6px;
        }
        .sheduleHrs div
        {
            float: left;
        }
        .sheduleHrs .divTextBox
        {
            float: left;
            position: relative;
            top: -7px;
        }
        .sheduleHrs .divDrpDwn
        {
            float: left;
            position: relative;
            top: -3px;
        }
        .sheduleHrs .ui-datepicker-trigger
        {
            position: relative;
            top: 8px;
        }
        #divMessageCategoryFormContainer .selector span
        {
            width: 200px;
        }
        #divDrpDownCategoryCont select
        {
            width: 240px !important;
        }
        #divMessageExpiresCont span.forLabel
        {
            position: relative;
            top: 9px;
        }
        #divSheduleImmediateCont .checker + label
        {
            top: 3px;
        }
        
        .sendToSubParts span.i_pencil
        {
            margin-left: 15px;
            margin-top: -3px;
        }
        div.token-input-dropdown-facebook
        {
            width: 57.2%;
        }
        .hide
        {
            display:none;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">

    <div id="PageCanvasContent">
    <asp:UpdatePanel ID="updPushMessageForm" runat="server">
        <ContentTemplate>
        <h1>mGram Notification</h1>
            <div class="">
                    <div class="customFieldset" style="width:97.5% !important" >
                        <div class="g12" id="div1">
                            <div class="fl">
                                <label class="pmHeaderLabel">
                                    Enterprise :
                                </label>
                            </div>
                            <div class="fl" id="div2" style="margin-top: -7px; margin-left:35px;">
                                <asp:DropDownList ID="drpenterprisedetails" runat="server" 
                                    onselectedindexchanged="drpenterprisedetails_SelectedIndexChanged" AutoPostBack="true">
                              
                                
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="g12" id="div3">
                            <div class="fl">
                                <label class="pmHeaderLabel">
                                    Device:
                                </label>
                            </div>
                            <div class="fl" id="div4" style="margin-top: -7px; margin-left: 60px;">
                                <asp:DropDownList ID="drpdevices" runat="server">
                              <%--  <asp:ListItem>All</asp:ListItem>
                                <asp:ListItem>Andriod</asp:ListItem>
                                <asp:ListItem>IOS</asp:ListItem>--%>
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="g12" id="div5">
                            <div class="fl">
                                <label class="pmHeaderLabel">
                                    User:
                                </label>
                            </div>
                            <div class="fl" id="div6" style="margin-top: -7px; margin-left: 70px;">
                                <asp:DropDownList ID="drpUserdetails" runat="server" Enabled="false">
                                <asp:ListItem Selected="True">ALL</asp:ListItem>
                                
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        </div>
                   
                    <div class="customFieldset" style="width:97.5% !important">
                        <div class="g12" id="divMessageCategoryFormContainer">
                            <div class="fl">
                                <label class="pmHeaderLabel">
                                    Category :
                                </label>
                            </div>
                           
                             <span class="fl forLabel">
                                    <asp:Label ID="lblmficient" style="margin-left:20px"  runat="server" Text="Mficient"></asp:Label>
                                </span>
                            
                            <div class="clear">
                            </div>
                            <section style="margin-top: 10px;">
                                <label class="pmHeaderLabel">
                                    Message text :
                                </label>
                            </section>
                            <div style="margin-top: 3px;">
                                <textarea id="txtMessage" runat="server" onkeyup="update_counter('text_counter', 'txtMessage', 1001)"></textarea><br />
                                <span id="text_counter">0</span> of 1000 characters<br />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                 
                    <div class="customFieldset" style="width:97.5% !important">
                        <div class="g12">
                            <div class="fl" id="divSheduleImmediateCont" style="width: auto; margin: 0 0 0 -2px;">
                                <asp:CheckBox ID="chkSheduleImmediate" runat="server" Text="Send Immediately" onclick="chkSendImmediatelyClicked(this)" />
                            </div>
                            <asp:Panel ID="pnlSheduledHrsMin" runat="server" CssClass="fl sheduleHrs" Style="margin-left: 30px;
                                width: auto;">
                                <asp:CheckBox ID="chkSheduledHrsMin" runat="server" onclick="chkSheduledHrsMinClicked(this);" />
                                <asp:Label ID="lblTextOn" CssClass="spanLabel" Style="top: 5px;" runat="server" Text="On"></asp:Label>
                                <div id="divDateTextBox" class="fl divTextBox" style="margin-left: 4px;">
                                    <asp:TextBox ID="txtSendDate" runat="server" Width="75%"></asp:TextBox></div>
                                <div id="divHrsContainer" class="fl divDrpDwn">
                                    <asp:DropDownList ID="ddlHours" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <asp:Label ID="lblHrs" CssClass="spanLabel" runat="server" Text="Hrs"></asp:Label>
                                <div id="divMinConntainer" class="fl divDrpDwn">
                                    <asp:DropDownList ID="ddlMinutes" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <asp:Label ID="lblMin" CssClass="spanLabel" runat="server" Text="Min"></asp:Label>
                            </asp:Panel>
                            <div class="clear">
                            </div>
                            <div id="divMessageExpiresCont" style="margin-top: 10px;">
                                <span class="fl forLabel">
                                    <label>
                                        Message expires in
                                    </label>
                                </span>
                                <div class="fl">
                                    <asp:DropDownList ID="ddlExpiryDays" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <span class="fl forLabel">
                                    <label>
                                        days
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                 
 <asp:Button ID="btnSend" runat="server" style="float: right; margin-right:20px; margin-top:10px;" Text="Send" OnClientClick="return validateFormFields();false;" OnClick="btnSend_Click"/>
          
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div>
                <asp:HiddenField ID="hidTotalLocations" runat="server" />
                <asp:HiddenField ID="hidTotalDepartments" runat="server" />
                <asp:HiddenField ID="hidTimeOffsetOfClient" runat="server" />
                <asp:HiddenField ID="hidCId" runat="server" />
                <asp:HiddenField ID="hidAutoCmpltUserIdsJson" runat="server" />
                <asp:HiddenField ID="hidAutoCmpltAllUserList" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
 
   
    <div>
        <asp:Button ID="btnScriptClickPostBack" runat="server" 
            Width="1px" Height="1px" Style="display: none;" />
    </div>

        </div>

     <script type="text/javascript">
         Sys.Application.add_init(application_init);
         function application_init() {
             //Sys.Debug.trace("Application.Init");
             var prm = Sys.WebForms.PageRequestManager.getInstance();
             prm.add_initializeRequest(prm_initializeRequest);
             prm.add_endRequest(prm_endRequest);
         }
         function prm_initializeRequest() {
           
             showWaitModal();
         }
         function prm_endRequest() {
            
             hideWaitModal();
         }
    </script>
</asp:Content>


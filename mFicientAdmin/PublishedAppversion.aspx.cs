﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientAdmin
{
    public partial class PublishedAppversion : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;


            if (!Page.IsPostBack)
            {
                
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "$(\"input\").uniform();", true);
                BindpulishedAppverstionRepeter();
            }
           
        }


        #region Record fetch  form database

        void BindpulishedAppverstionRepeter()
        {
            PublishedAppVersion addPublishDetails = new PublishedAppVersion();
            DataTable dtPublishedAppVersionList = addPublishDetails.Process();// published  version Datatable Details 15-04-2015
            if (dtPublishedAppVersionList.Rows.Count > 0)
            {
              DataTable dtrecord= CustomTable(dtPublishedAppVersionList);
              rptpublishedappversion.DataSource = dtrecord;
                rptpublishedappversion.DataBind();
              
            }
        }
        #endregion

        #region RepeterBind purpose DataSource Created
        DataTable CustomTable(DataTable dtrecord)
        {
            DataColumn MajorAppVersionDetails = dtrecord.Columns.Add("MajorAppVersionDetails", typeof(string));
            DataColumn MinorAppVersionDetails = dtrecord.Columns.Add("MinorAppVersionDetails", typeof(string));
            string strverstionminor = "";
            foreach (DataRow row in dtrecord.Rows)
            {
                strverstionminor = row["MOBILE_APP_VERSION_MINOR"].ToString();
                if (strverstionminor == "0") strverstionminor = "00";
                else if (strverstionminor.Length == 1) strverstionminor = "0" + strverstionminor;
                
                row["MajorAppVersionDetails"] = row["MOBILE_APP_VERSION_MAJOR"] + "." + strverstionminor  + "." + row["MOBILE_APP_VERSION_REV"];
                
                strverstionminor = row["MINIMUM_APP_VERSION_MINOR1"].ToString();
                if (strverstionminor == "0") strverstionminor = "00";
                else if (strverstionminor.Length == 1) strverstionminor = "0" + strverstionminor;
                 
                row["MinorAppVersionDetails"] = row["MINIMUM_APP_VERSION_MAJOR1"] + "." + strverstionminor + "." + row["MINIMUM_APP_VERSION_REV1"];
            }
            return dtrecord;
        }
        #endregion


        #region Update Version records
        protected void Updatebtn_Click(object sender, EventArgs e)
        {
            UpdateApppublishVersion updateversiondetails = new UpdateApppublishVersion(hidostype.Value, Convert.ToInt32(txtappmajorversion.Text), Convert.ToInt32(txtappmajoversionminor.Text), Convert.ToInt32(txtappversionrev.Text), Convert.ToInt32(txtappminimumversion.Text), Convert.ToInt32(txtappminimumversionminor.Text), Convert.ToInt32(txtminimumversionrev.Text));

            int intStatusCode = updateversiondetails.StatusCode;
            string strDescription = updateversiondetails.StatusDescription;

            if (intStatusCode == 0)
            {
                Utilities.showMessage("Version Detail have been updated successfully", this.Page, "second script", DIALOG_TYPE.Info);
                BindpulishedAppverstionRepeter();
                Clearallcontrol();
            }
            else
            {
                Utilities.showMessage("Internal Error,Please try again", this.Page, "second script", DIALOG_TYPE.Warning);
            }


        }
        #endregion

        #region clearallcontrols
        void Clearallcontrol()
        {
            txtappmajorversion.Text="";
       txtappmajoversionminor.Text="";
       txtappversionrev.Text="";
      txtappminimumversion.Text="";
       txtappminimumversionminor.Text="";
        txtminimumversionrev.Text = "";
        hidostype.Value = "";

        }
        #endregion


    }
}
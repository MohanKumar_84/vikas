﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="ManageTrialPlans.aspx.cs" Inherits="mFicientAdmin.ManageTrialPlans" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>Trial Plan Details</h1>"></asp:Label>
                                        </div>
                                        <div style="position: relative; top: 10px; right: 15px;">
                                            <asp:LinkButton ID="lnkAddNewMobileUser" runat="server" Text="add" CssClass="repeaterLink fr"
                                                OnClick="lnkAddNewMobileUser_Click"></asp:LinkButton>
                                        </div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptUserDetails" runat="server" OnItemCommand="rptUserDetails_ItemCommand"
                                    OnItemDataBound="rptUserDetails_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                <th style="display:none;">
                                                plan Id
                                                </th>
                                                    <th>
                                                        PLAN NAME
                                                    </th>
                                                    <th>
                                                        WORKFLOW
                                                    </th>
                                                    <th>
                                                   MAXIMUM USERS
                                                    </th>
                                                    
                                                    <th>
                                                   PUSH MESSAGE <span style="position:relative;top:-1px">*</span>

                                                    </th>
                                                   <th>
                                                   MONTH
                                                   </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                             <td style="display:none;" >
                                                    <asp:Label ID="lblplanid" runat="server" Text='<%#Eval("PLAN_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblplanname" runat="server" Text='<%#Eval("PLAN_NAME") %>'></asp:Label> ( 
                                                    <asp:Label ID="lblplancode" runat="server" Text='<%#Eval("PLAN_CODE") %>'></asp:Label> )
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblworkflow" runat="server" Text='<%# Eval("MAX_WORKFLOW") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblmaxusers" runat="server" Text='<%# Eval("MAX_USER") %>'></asp:Label>
                                                </td>
                                              
                                                    
                                                
                                                  <td>
                                             <asp:Label ID="lblmsgperday" runat="server" Text='<%# Eval("PUSHMESSAGE_PERDAY") %>'></asp:Label>/
                                              <asp:Label ID="lblmsgpermonth" runat="server" Text='<%# Eval("PUSHMESSAGE_PERMONTH") %>'></asp:Label>
                                       <asp:Label ID="lblItemDoubleAsterix" runat="server" Text="**" Visible="false" style="position:relative;top:-2px;margin-left:2px;"></asp:Label>

                                                </td>
                                              <td>
                                                    <asp:Label ID="lblmonth" runat="server" Text='<%# Eval("MONTH") %>'></asp:Label>
                                                </td>
                                               
                                                <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="edit" OnClick="lnkedit_Click" CssClass="repeaterLink" CommandName="Edit"></asp:LinkButton>
                                                    
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                            <td style="display:none;" >
                                                    <asp:Label ID="lblplanid" runat="server" Text='<%#Eval("PLAN_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblplanname" runat="server" Text='<%#Eval("PLAN_NAME") %>'></asp:Label> ( 
                                                    <asp:Label ID="lblplancode" runat="server" Text='<%#Eval("PLAN_CODE") %>'></asp:Label> )
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblworkflow" runat="server" Text='<%# Eval("MAX_WORKFLOW") %>'></asp:Label>
                                                </td>
                                   
                                                <td>
                                                    <asp:Label ID="lblmaxusers" runat="server" Text='<%# Eval("MAX_USER") %>'></asp:Label>
                                                </td>
                                                

                                                  <td>
                                                 <asp:Label ID="lblmsgperday" runat="server" Text='<%# Eval("PUSHMESSAGE_PERDAY") %>'></asp:Label>
                                                 /
                                                <asp:Label ID="lblmsgpermonth" runat="server" Text='<%# Eval("PUSHMESSAGE_PERMONTH") %>'></asp:Label>

                                        <asp:Label ID="Label3" runat="server" Text="**" Visible="false" style="position:relative;top:-2px;margin-left:2px;"></asp:Label>

                                                </td>
                                               <td>
                                                    <asp:Label ID="lblmonth" runat="server" Text='<%# Eval("MONTH") %>'></asp:Label>
                                                </td>
                                               
                                                <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="edit" CssClass="repeaterLink" OnClick="lnkedit_Click" CommandName="Edit"></asp:LinkButton>
                                                    
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                        <div style="font-style:italic;margin:2px 0 0 5px;height:auto">* Per Day/Per Month</div>
                                            <div style="font-style:italic;margin:3px 0 0 5px;height:auto">
                                                <asp:Label ID="lblDoubleAsterixInfo" runat="server" Visible="false"></asp:Label></div>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                    <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                                </div>
                            </asp:Panel>
                        </section>
                       <section></section>
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        </div>
                        </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
        
        <div class="modalPopUpDetails">
            <div id="divUserDtlsModal" style="display: none; height: 420px;">
                <asp:UpdatePanel runat="server" ID="upUserDtlsModal" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="ProcImgUpd">
                            <div id="DivUserDetails" style="width: 100%;">
                                <asp:Panel ID="pnlMobileUserForm" Visible="false" runat="server">
                                    <div style="float: left; width: 45%;">
                                        <div style="margin-bottom: 50px; margin-top: 10px;">
                                            <div style="float: left; margin-right: 70px;">
                                                <asp:Label ID="label11" runat="server" Text="<strong>Plan Code</strong>"></asp:Label></div>
                                            <div id="lbl" runat="server" style="float: left; margin-left:-25px;">
                                                <asp:Label ID="Plan" runat="server"></asp:Label>
                                            </div>
                                            <div id="txt" runat="server" style="float: left; margin-left: -50px;
                                                width: 82px;">
                                                <asp:TextBox ID="txtcode" runat="server" CssClass="textbox"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div>
                                            <div style="float: left; margin-right: 40px;margin-top:3px; ">
                                                <asp:Label ID="label2" runat="server" Text="<strong>Plan Name</strong>"></asp:Label></div>
                                            <div style="float: left; width: 82px;">
                                                <asp:TextBox ID="txtplanname" runat="server"></asp:TextBox></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-top: 20px;">
                                            <div style="float: left; margin-right: 21px;">
                                                <asp:Label ID="label5" runat="server" Text="<strong>Workflow</strong> "></asp:Label></div>
                                            <div style="float: left; width:82px;">
                                                <asp:TextBox ID="txtworkflow" runat="server" CssClass="textbox"></asp:TextBox></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-top: 10px;">
                                            <div style="float: left; margin-right: 39px; margin-top: 10px;">
                                                <asp:Label ID="label1" runat="server" Text="<strong>Month</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 8px;">
                                                <asp:TextBox ID="txtmonth" runat="server" CssClass="textbox"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div style="float: right; width: 55%;">
                                        <div style="margin-bottom: 45px;">
                                            <div style="float: left; margin-top: 13px;">
                                                <asp:Label ID="label6" runat="server" Text="<strong>Max Users </strong>"></asp:Label></div>
                                            <div style="float: left; margin-top: 8px; margin-left: 42px;">
                                                <asp:TextBox ID="txtusers" runat="server" CssClass="textbox"></asp:TextBox></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 40px; margin-top: 15px;">
                                            <div style="float: left; margin-top: 10px; margin-right: 6px;">
                                                <asp:Label ID="label8" runat="server" Text="<strong>Message Per Day</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 8px;">
                                                <asp:TextBox ID="txtmsgperday" runat="server" CssClass="textbox"></asp:TextBox></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 40px; margin-top: 15px;">
                                            <div style="float: left; margin-right: -8px; margin-top: 13px;">
                                                <asp:Label ID="label9" runat="server" Text="<strong>Message Per Month</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 8px;">
                                                <asp:TextBox ID="txtmsgpermonth" runat="server" CssClass="textbox"></asp:TextBox></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <%--   <div style="margin-bottom: 40px; margin-top: 15px;">
                                                <div style="float: left; margin-right: 66px; margin-top: 10px;">
                                                    <asp:Label ID="label1" runat="server" Text="<strong>Month</strong> "></asp:Label></div>
                                                <div style="float: left; margin-top: 8px;">
                                                    <asp:TextBox ID="txtmonth" runat="server" CssClass="textbox"></asp:TextBox></div>
                                            </div>--%>
                                    </div>
                                    <section id="buttons">
                                <div class="SubProcborderDiv" style="margin-top:15px;">
                                        <div class="SubProcBtnDiv" align="center">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                                    </div>
                                    </div>
                                    </section>
                                </asp:Panel>
                            </div>
                        </div>
                        </section> </div> </div> </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
           // $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
           // $("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>

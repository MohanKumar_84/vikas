﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public partial class SupportQueries : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;




            if (!Page.IsPostBack)
            {
                LinkButton ChangePass = (LinkButton)this.Master.Master.FindControl("lbChangePwd");
                Literal ltlUserType = (Literal)this.Master.Master.FindControl("ltUserType");
                /*if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else if (Page.PreviousPage != null)
                {
                        string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                        prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                        ContentPlaceHolder cphPageContent = (ContentPlaceHolder)Page.PreviousPage.Form.FindControl("MainCanvas").FindControl("PageCanvas");
                        hfs.Value = ((HiddenField)cphPageContent.FindControl("hfs")).Value;
                } */



                if (string.IsNullOrEmpty(hfsValue))
                {
                    Response.Redirect(@"Default.aspx");
                }
                string Role = strRole.ToUpper();
                if (Role == "A")
                {
                    ltlUserType.Text = "mFicient Admin";
                    ChangePass.PostBackUrl = "~/ChangePassword.aspx";
                }

                else if (Role == "R")
                {
                    ltlUserType.Text = "Reseller";
                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }
                else if (Role == "S")
                {
                    ltlUserType.Text = "Sales Executive";
                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }
                else if (Role == "SM")
                {
                    ltlUserType.Text = "Sales Manager";
                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }

                else if (Role == "SUPP")
                {
                    ltlUserType.Text = "Support";
                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }

                else if (Role == "ACC")
                {
                    ltlUserType.Text = "Accounts";
                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }

                lblHeaderInfo.Text = "<h1>Mobile User Queries</h1>";
                BindopenqueryRepeater();
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "$(\"input\").uniform();", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "testing", " $(\"select\").uniform();$(\"input\").uniform();", true);
            }
        }

        void changeColorOfSelectedLinkBtn(LinkButton linkSelected)
        {
            linkSelected.Attributes.Add("class", "linkSelected");
            if (linkSelected.ID == lnk1.ID)
            {
                lnk2.Attributes.Remove("class");
            }
            else if (linkSelected.ID == lnk2.ID)
            {
                lnk1.Attributes.Remove("class");
            }

        }

        protected void lnk1_Click(object sender, EventArgs e)
        {
            changeColorOfSelectedLinkBtn((LinkButton)sender);
            BindopenqueryRepeater();
        }

        protected void lnk2_Click(object sender, EventArgs e)
        {
            changeColorOfSelectedLinkBtn((LinkButton)sender);
            BindClosedRepeater();
        }

        protected void BindClosedRepeater()
        {
            SupportQueriesAdmin get = new SupportQueriesAdmin();
            DataTable dt = get.GetClosedQueries();
            if (dt.Rows.Count > 0)
            {
                lblHeaderInfo.Text = "<h1>Closed Queries</h1>";
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
                rptUserDetails.Visible = true;
            }
            else if (dt.Rows.Count == 0)
            {
                lblHeaderInfo.Text = "<h1>no closed queries added yet</h1>";
                rptUserDetails.Visible = false;
            }
        }
        protected void BindopenqueryRepeater()
        {
            SupportQueriesAdmin get = new SupportQueriesAdmin();
            // string userId = get.UserId(hfs.Value.Split(',')[0]);
            DataTable dt = get.GetOpenQueries();
            if (dt.Rows.Count > 0)
            {
                lblHeaderInfo.Text = "<h1>Open Queries</h1>";
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
                rptUserDetails.Visible = true;
            }
            else if (dt.Rows.Count == 0)
            {
                lblHeaderInfo.Text = "<h1>no open queries added yet</h1>";
                rptUserDetails.Visible = false;
            }
        }

        protected void BindCommentsRepeater(string tokenNo)
        {
            SupportQueriesAdmin queries = new SupportQueriesAdmin();
            //string userId = queries.UserId(hfs.Value.Split(',')[0]);
            string userId = queries.UserId(strUserName);
            //string TokenNumber = queries.GetTokenNo(userId);
            DataTable dt = queries.GetCommentsList(tokenNo);
            if (dt.Rows.Count > 0)
            {
                rptComments.DataSource = dt;
                rptComments.DataBind();
            }
            else
            {
                rptComments.DataSource = null;
                rptComments.DataBind();
            }
        }

        protected void lnkok_Click(object sender, EventArgs e)
        {
            
        }

        protected void bttncomment_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtcomment.Text))
            {
                SupportQueriesAdmin queries = new SupportQueriesAdmin();
                //string UserId = queries.UserId(hfs.Value.Split(',')[0]);
                string UserId = queries.UserId(strUserName);
                //string TokenNumber = queries.GetTokenNo(UserId);
                string strTokenNo = hidTokenNoSelected.Value;
                queries.AddQuery(strTokenNo, Utilities.GetMd5Hash(txtcomment.Text + DateTime.Now.Ticks.ToString()), UserId, 2, txtcomment.Text);
                if (queries.StatusCode == 0)
                {
                    txtcomment.Text = string.Empty;
                    //Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
                    BindCommentsRepeater(strTokenNo);
                    upUserDtlsModal.Update();
                    Utilities.closeModalPopUp("divCommentByUser", this.Page);
                    Utilities.showMessage("User Comments have been added successfully", this.Page, "second script", DIALOG_TYPE.Info);
                }
                else
                {
                    Utilities.showAlert("Internal server error.", "divCommentErrorAlert", this.Page);
                }
            }
            else
            {
                //((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString();
                //Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
                Utilities.showAlert("Please enter a comment.", "divCommentErrorAlert", this.Page);
            }
        }
        protected void lnkBack_Click(object sender, EventArgs e)
        {
            pnlRepeaterBox.Visible = true;
            Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
        }
        protected void enabledisablepanels(string mode)
        {
            if (mode == "Info")
            {
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = true;
            }
            else if (mode == "Add")
            {
                pnlRepeaterBox.Visible = false;
            }
            else
            {
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = false;
            }
        }

        protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item;
            Label lit_number = (Label)item.FindControl("lblnumber");

            if (e.CommandName == "Info")
            {
                FillQueryDetails(lit_number.Text);
            }
        }

        protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Info":
                    Info(e);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing2", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                    break;
            }
        }
        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#buttons'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        void showAlert(string message, string divIdToShowMsg)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }

        protected void FillQueryDetails(string TokenNumber)
        {
            SupportQueriesAdmin querydet = new SupportQueriesAdmin();
            querydet.QueryDetails(TokenNumber);
            DataTable dt = querydet.ResultTable;
            // string strId = Convert.ToString(dt.Rows[0]["USER_ID"]);
            if (dt.Rows.Count > 0)
            {
                hidDetailsFormMode.Value = TokenNumber;
                //lname.Text = hfs.Value.Split(',')[0];strUserName
                lname.Text = strUserName;
                lblFullName.Text = strUserName;
                lsubject.Text = Convert.ToString(dt.Rows[0]["QUERY_SUBJECT"]);
                lquery.Text = Convert.ToString(dt.Rows[0]["QUERY"]);
            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }

        void Info(RepeaterCommandEventArgs e)
        {
            Label TokenNumber = (Label)e.Item.FindControl("lblnumber");
            hidTokenNoSelected.Value = TokenNumber.Text;
            FillQueryDetails(TokenNumber.Text);
            BindCommentsRepeater(TokenNumber.Text);
            Utilities.showModalPopup("divUserDtlsModal", this.Page, "<strong>Query Details</strong>", "550", true, false);
            enabledisablepanels("Info");
            upUserDtlsModal.Update();
        }
    }
}
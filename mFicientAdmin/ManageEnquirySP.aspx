﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="ManageEnquirySP.aspx.cs" Inherits="mFicientAdmin.ManageEnquirySP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>ENQUIRY DETAILS</h1>"></asp:Label>
                                        </div>

                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptUserDetails" runat="server" OnItemCommand="rptUserDetails_ItemCommand"
                                    OnItemDataBound="rptUserDetails_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    
                                                    <th>
                                                        Company Name
                                                    </th>
                                                    <th>
                                                    Types Of Company
                                                    </th>
                                                   <%-- <th>
                                                       Contact Person
                                                    </th>
                                                    <th>
                                                       Contact Number
                                                    </th>
                                                   <th>
                                                   Contact Email
                                                   </th>--%>
                                                   <th></th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                  <td>
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbltype" runat="server" Text='<%# Eval("TYPE_OF_COMPANY") %>'></asp:Label>
                                                </td>
                                                <%--<td>
                                                    <asp:Label ID="lblperson" runat="server" Text='<%# Eval("CONTACT_PERSON") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblnumber" runat="server" Text='<%# Eval("CONTACT_NUMBER") %>'></asp:Label>
                                                </td>

                                                 <td>
                                                    <asp:Label ID="lblemail" runat="server" Text='<%# Eval("CONTACT_EMAIL") %>'></asp:Label>
                                                </td>--%>
                                                <td>
                                                    <asp:Label ID="lblid" runat="server"  Visible="false" Text='<%# Eval("ENQUIRY_ID") %>'></asp:Label>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info" OnClick="lnkok_Click"></asp:LinkButton>
                                                    
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                             <tr class="repeaterItem">
                                                
                                                <td>
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbltype" runat="server" Text='<%# Eval("TYPE_OF_COMPANY") %>'></asp:Label>
                                                </td>
<%--                                                <td>
                                                    <asp:Label ID="lblperson" runat="server" Text='<%# Eval("CONTACT_PERSON") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblnumber" runat="server" Text='<%# Eval("CONTACT_NUMBER") %>'></asp:Label>
                                                </td>

                                                 <td>
                                                    <asp:Label ID="lblemail" runat="server" Text='<%# Eval("CONTACT_EMAIL") %>'></asp:Label>
                                                </td>--%>
                                                <td>
                                                <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# Eval("ENQUIRY_ID") %>'></asp:Label>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info" OnClick="lnkok_Click"></asp:LinkButton>
                                                    
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                             
                                    <asp:HiddenField ID="hidDetailsFormMode" runat="server" 
                                        onvaluechanged="hidDetailsFormMode_ValueChanged" />
                                </div>
                            </asp:Panel>
                        </section>
                        <section>
                            <asp:Panel ID="pnlMobileUserForm" Visible="false" runat="server">
                                <div class="g11">
                                    <div class="formDivCls">
                                        <fieldset>
                                            <label>
                                                Details
                                           
                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Back" CssClass="repeaterLink fr"
                                                OnClick="lnkAddNewMobileUser_Click"></asp:LinkButton>
                                   
                                                
                                                
                                              </label>
                                            
                                            



                                                 
                                            <section>
                                                <label for="lname">
                                                    Company Name</label>
                                                <div>
                                     <asp:Label ID="lname" runat="server" ></asp:Label>                                             
                                                </div>
                                            </section>


                                            <section>
                                            
                                            <label for="lcountry">Country Code</label>
                                            <div>
                                            <asp:Label ID="lcountry" runat="server"></asp:Label>
                                            </div>
                                            
                                            
                                            </section>
                                            <section>
                                                <label for="ltype">
                                                   Type Of Company</label>
                                                <div>
                                       <asp:Label ID="ltype" runat="server"></asp:Label>                                         
                             </div>
                                            </section>

                                       
                                               <section>
                                                <label for="lperson">
                                                  Contact Person</label>
                                                <div>
                                                <asp:Label ID="lperson" runat="server" ></asp:Label>
                                                </div>
                                            </section>
                                            
                                             <section>
                                                <label for="lnumber">
                                                  Contact Number</label>
                                                <div>
                                                <asp:Label ID="lnumber" runat="server" ></asp:Label>
                                                </div>
                                            </section>
                                         
                                                <section>
                                                <label for="lemail">
                                                  Contact Email</label>
                                                <div>
                                               <asp:Label ID="lemail" runat="server"></asp:Label>                                      
                                               </div>
                                            </section>
                                                <section>
                                                
                                                <label for ="Enquirydate">Enquiry Date</label>
                                                
                                                <div>
                                                <asp:Label ID="Enquirydate" runat="server"></asp:Label>
                                                </div>
                                                
                                                </section>



                                             
                                            <section>
                                            <label for="lst">Allocated Resellers</label>
                                            <div>
                                <asp:Label ID="lstlbl" runat="server"></asp:Label>



                                            </div>
                                            
                                            
                                            
                                            </section>
                                            <section>
                                            <label for="ddlR">
                                        Allocate Reseller</label>
                                        <div>
                                            <asp:DropDownList runat="server" ID="ddlR" CssClass="Fld" Width="100px" OnSelectedIndexChanged="ddlSP_SelectedIndexChanged">
                                                  </asp:DropDownList>
                                                  </div>
                                                  </section>
                                                 
                                             <section id="buttons">
                                            <asp:Button ID="btnok" runat="server" Text="Save" CssClass="aspButton" OnClick="btnok_Click" />
                                        </section>

                                            
                                        
                                        
                                        
                                        </div>
                                        
                                       
                                        </fieldset>
                                                                           </div>
                                </div>
                            </asp:Panel>
                        </section>

                                           


                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
        <div>
            <asp:HiddenField ID="hfs" runat="server" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public partial class ManageDefaultServers : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;


            if (!Page.IsPostBack)
            {
                bindDdlMbuzzServer(true);
                bindDdlMpluginServer(true);
                bindDdlWebserviceServer(true);
            }
            else
            {
                Utilities.makeInputFieldsUniform(this.Page, "MakeFieldUniformOnPostBack", true, String.Empty);
            }
        }


        void bindDdlMbuzzServer(bool setDefaultServer)
        {
            GetMbuzzServerDetail objGetMbuzzServer = new
            GetMbuzzServerDetail();
            List<MFEmBuzzServer> lstMBuzzServers = objGetMbuzzServer.getAllMbuzzServers();
            if (lstMBuzzServers.Count > 0)
            {
                ddlMbuzzServers.Items.Clear();
                ddlMbuzzServers.DataSource = lstMBuzzServers;
                ddlMbuzzServers.DataTextField = "ServerName";
                ddlMbuzzServers.DataValueField = "ServerId";
                ddlMbuzzServers.DataBind();

                if (setDefaultServer)
                {
                    string strDfltServerId = String.Empty;
                    foreach (MFEmBuzzServer server in lstMBuzzServers)
                    {
                        if (server.IsDefault)
                        {
                            strDfltServerId = server.ServerId;
                            break;
                        }
                    }
                    ddlMbuzzServers.SelectedIndex =
                        ddlMbuzzServers.Items.IndexOf(
                        ddlMbuzzServers.Items.FindByValue(
                        strDfltServerId
                        )
                        );
                }
            }
        }
        void bindDdlMpluginServer(bool setDefaultServer)
        {
            GetMpluginServerDetail objMPluginServerDtl = new
            GetMpluginServerDetail();

            List<MFEmPluginServer> lstMPluginServer = objMPluginServerDtl.getAllMpluginServers();

            if (lstMPluginServer.Count > 0)
            {
                ddlMpluginServer.Items.Clear();
                ddlMpluginServer.DataSource = lstMPluginServer;
                ddlMpluginServer.DataTextField = "ServerName";
                ddlMpluginServer.DataValueField = "ServerId";
                ddlMpluginServer.DataBind();

                if (setDefaultServer)
                {
                    string strDfltServerId = String.Empty;
                    foreach (MFEmPluginServer server in lstMPluginServer)
                    {
                        if (server.IsDefault)
                        {
                            strDfltServerId = server.ServerId;
                            break;
                        }
                    }
                    ddlMpluginServer.SelectedIndex =
                        ddlMpluginServer.Items.IndexOf(
                        ddlMpluginServer.Items.FindByValue(
                        strDfltServerId
                        )
                        );
                }
            }
        }
        void bindDdlWebserviceServer(bool setDefaultServer)
        {
            GetMasterServerDetails objMasterServerDtl = new
            GetMasterServerDetails();

            List<MFEmficientServer> lstWsServers = objMasterServerDtl.getMficientServersDetails();

            if (lstWsServers.Count > 0)
            {
                ddlWebserviceServers.Items.Clear();
                ddlWebserviceServers.DataSource = lstWsServers;
                ddlWebserviceServers.DataTextField = "ServerName";
                ddlWebserviceServers.DataValueField = "ServerId";
                ddlWebserviceServers.DataBind();

                if (setDefaultServer)
                {
                    string strDfltServerId = String.Empty;
                    foreach (MFEmficientServer wsServer in lstWsServers)
                    {
                        if (wsServer.IsDefault)
                        {
                            strDfltServerId = wsServer.ServerId;
                            break;
                        }
                    }
                    ddlWebserviceServers.SelectedIndex =
                        ddlWebserviceServers.Items.IndexOf(
                        ddlWebserviceServers.Items.FindByValue(
                        strDfltServerId
                        )
                        );
                }
            }

        }

        protected void btnWSServerSave_Click(Object sender, EventArgs e)
        {
            try
            {
                bool saveSuccessfull =

                    SaveDefaultServerSetting.
                    saveWebServiceServerDefault(
                    ddlWebserviceServers.SelectedValue
                    );

                if (saveSuccessfull)
                {
                    Utilities.showMessage("Saved successfully.",
                        updManageServers, DIALOG_TYPE.Info);
                }
                else
                {
                    Utilities.showMessage("Could not save.Please try again.",
                           updManageServers, DIALOG_TYPE.Error);
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error.",
                          updManageServers, DIALOG_TYPE.Error);
            }
        }
        protected void btnMbuzzServerSave_Click(Object sender, EventArgs e)
        {
            try
            {
                bool saveSuccessfull =
                    SaveDefaultServerSetting.
                    saveMbuzzServerDefault(
                    ddlMbuzzServers.SelectedValue
                    );
                if (saveSuccessfull)
                {
                    Utilities.showMessage("Saved successfully.",
                        updManageServers, DIALOG_TYPE.Info);
                }
                else
                {
                    Utilities.showMessage("Could not save.Please try again.",
                           updManageServers, DIALOG_TYPE.Error);
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error.",
                          updManageServers, DIALOG_TYPE.Error);
            }
        }
        protected void btnMpluginServerSave_Click(Object sender, EventArgs e)
        {
            try
            {
                bool saveSuccessfull =
                    SaveDefaultServerSetting.
                    saveMPluginServerDefault(
                    ddlMpluginServer.SelectedValue
                    );
                if (saveSuccessfull)
                {
                    Utilities.showMessage("Saved successfully.",
                        updManageServers, DIALOG_TYPE.Info);
                }
                else
                {
                    Utilities.showMessage("Could not save.Please try again.",
                           updManageServers, DIALOG_TYPE.Error);
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error.",
                          updManageServers, DIALOG_TYPE.Error);
            }
        }
    }
}
﻿function ShowPopup() {
    $('#mask').show();
    $('#<%=pnlpopup.ClientID %>').show();
}
function HidePopup() {
    $('#mask').hide();
    $('#<%=pnlpopup.ClientID %>').hide();
}
$(".btnClose").live('click', function () {
    HidePopup();
});
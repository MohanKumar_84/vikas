﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public partial class PlanApprovalByAcc : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        enum PLAN_RENEW_OR_CHANGE_REQUESTS
        {
            RENEW = 1,
            USERS_ADDED = 2,
            PLAN_UPGRADE = 3,
            PLAN_DOWNGRADE = 4,
            PLAN_PURCHASE = 5,
        }
        public enum DETAILS_SHOWN_FOR_PROCESS
        {
            SALES_APPROVED_PLAN_DETAILS,
            SALES_UN_APPROVED_PLAN_DETAILS,
            NEW_COMPANY_DETAILS
        }
        enum DENY_REQUEST_TYPE
        {
            PLAN_DENY,
            COMPANY_DENY,
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;



            if (!Page.IsPostBack)
            {
                LinkButton ChangePass = (LinkButton)this.Master.Master.FindControl("lbChangePwd");
                Literal ltlUserType = (Literal)this.Master.Master.FindControl("ltUserType");
                string Role = strRole.ToUpper();
                if (Role == null)
                {
                    Response.Redirect("Default.aspx");
                }
                if (Role == "A")
                {
                    ltlUserType.Text = "mFicient Admin";

                    ChangePass.PostBackUrl = "~/ChangePassword.aspx";
                }

                else if (Role == "R")
                {
                    ltlUserType.Text = "Reseller";

                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }

                else if (Role == "S")
                {
                    ltlUserType.Text = "Sales Executive";

                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }
                else if (Role == "SM")
                {
                    ltlUserType.Text = "Sales Manager";

                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }

                else if (Role == "SUPP")
                {
                    ltlUserType.Text = "Support";

                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }

                else if (Role == "ACC")
                {
                    ltlUserType.Text = "Accounts";

                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }


                //bindSalesUnApprovedList();
                chckApproved.Checked = true;
                bindSalesApprovedPlanList();
                bindNewRegistrationRepeater();

                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "$(\"input\").uniform();", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "testing", " $(\"select\").uniform();$(\"input\").uniform();", true);
            }
        }


        public void bindSalesApprovedPlanList()
        {
            AccountApprovalAdmin get = new AccountApprovalAdmin();
            DataTable objdt = get.GetApprovedList();//Request_type = 5( for new purchase.) is not shown for this repeater.is shown in new registered repeater
            if (objdt.Rows.Count > 0)
            {
                rptPlanRequestsDetails.DataSource = objdt;
                rptPlanRequestsDetails.DataBind();
                lblHeaderInfo.Text = "<h1> PLAN CHANGE / RENEW REQUESTS</h1>";
            }
            else
            {
                objdt = null;
                rptPlanRequestsDetails.DataSource = objdt;
                rptPlanRequestsDetails.DataBind();
                lblHeaderInfo.Text = "<h1>No sales approved request pending</h1>";
            }
        }
        public void bindSalesUnApprovedList()
        {
            AccountApprovalAdmin det = new AccountApprovalAdmin();
            DataTable objdt = det.GetPendingList();//Request_type = 5( for new purchase.) is not shown for this repeater.is shown in new registered repeater
            if (objdt.Rows.Count > 0)
            {
                rptPlanRequestsDetails.DataSource = objdt;
                rptPlanRequestsDetails.DataBind();
                lblHeaderInfo.Text = "<h1>PLAN CHANGE / RENEW REQUESTS</h1>";
            }
            else
            {
                objdt = null;
                rptPlanRequestsDetails.DataSource = objdt;
                rptPlanRequestsDetails.DataBind();
                lblHeaderInfo.Text = "<h1>No unapproved request pending</h1>";
            }
            //AddServerDetails detail = new AddServerDetails();
            //DataTable objdt = detail.GetAdminList();
            //if (objdt.Rows.Count > 0)
            //{
            //    rptUserDetails.DataSource = objdt;
            //    rptUserDetails.DataBind();
            //}
        }
        public void Check()
        {
            if (chckApproved.Checked)
            {
                bindSalesApprovedPlanList();
            }
            else
            {
                bindSalesUnApprovedList();
            }
        }
        protected void lnkok_Click(object sender, EventArgs e)
        {
            //transfrred to FillCompanyDetails();
        }
        string validateForm()
        {
            string strError = String.Empty;
            try
            {
                double dblAmount = double.Parse(txtamount.Text);
                if (dblAmount < Convert.ToDouble(lblPayableAmount.Text))
                {
                    strError = "Amount is less than the payable amount";
                }
            }
            catch (FormatException e)
            {
                strError = "Please enter valid amount.";
            }
            catch (Exception e)
            {
                strError = "Please enter valid amount.";
            }
            return strError;
        }
        protected void lnkSave_Click(object sender, EventArgs e)
        {
        }
        protected void btnModalSave_Click(object source, EventArgs e)
        {
            if (txtamount.Text == "")
            {
                Utilities.showAlert("Please enter the amount received", "divUserDtlsModal", this.Page);
                return;
            }
            string strError = validateForm();
            if (String.IsNullOrEmpty(strError))
            {
                try
                {
                    DataTable dtblPlanDetails;
                    dtblPlanDetails = getPlanDtls(lblPlanCode.Text);
                    //DataTable dtblPlanDetails = getPlanDtls(lblPlanCode.Text);
                    if (dtblPlanDetails != null && dtblPlanDetails.Rows.Count > 0)
                    {
                        DataTable dtblCmpCurrentPlan = getCompanyCurrentPlanDetail(lblCompanyId.Text);
                        PurchaseRequestApprovalByAccounts objPurchaseApprovalAcc = null;
                        if (dtblCmpCurrentPlan.Rows.Count > 0)
                        {
                            DataRow rowPlanDetail = dtblPlanDetails.Rows[0];
                            DataRow rowCmpCurrentPlan = dtblCmpCurrentPlan.Rows[0];
                            if (lblRequestType.Text.ToLower() == ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_PURCHASE).ToString())
                            {
                                //for plan purchase
                                bool isPurchasePlanAfterTrial = false;
                                if (getCompanyCurrentPlanFromCPTbl(lblCompanyId.Text).Rows.Count > 0)//if it is purchase after trial then Control panel tbl will have the company details
                                {
                                    isPurchasePlanAfterTrial = true;
                                }
                                objPurchaseApprovalAcc = new PurchaseRequestApprovalByAccounts(
                                                         lblCompanyId.Text,
                                                         lblPlanCode.Text,
                                                         ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_PURCHASE).ToString(),
                                                         Convert.ToInt32(txtamount.Text),
                                                         true,
                                                         isPurchasePlanAfterTrial,
                                                         hidPlanCurrencyAndMonthsForApproval.Value.Split(';')[0],
                                                         Convert.ToInt32(hidPlanCurrencyAndMonthsForApproval.Value.Split(';')[1])
                                                         );

                                //objPurchaseApprovalAcc.Process();
                            }
                            else
                            {
                                string strNoOfMonthsRemaining = "", strChargePerMnth = lblChargePerUserPerMonth.Text;
                                getValidityOfPlan(
                                    out strNoOfMonthsRemaining,
                                    Convert.ToString(Convert.ToInt64(rowCmpCurrentPlan["PURCHASE_DATE"])),
                                    Convert.ToString((double)rowCmpCurrentPlan["VALIDITY"])
                                    );
                                //not required now
                                //if (lblCurrrencyType.Text.ToLower() == "inr")
                                //{
                                //    strChargePerMnth = Convert.ToString(rowPlanDetail["USER_CHARGE_INR_PM"]);
                                //}
                                //else
                                //{
                                //    strChargePerMnth = Convert.ToString(rowPlanDetail["USER_CHARGE_USD_PM"]);
                                //}

                                GetPlanRequestsLogDetailsForAcc objPlanRequestLog = new GetPlanRequestsLogDetailsForAcc(lblCompanyId.Text, lblPlanCode.Text, lblRequestType.Text);
                                objPlanRequestLog.Process();
                                DataTable dtblPlanRequestLog = objPlanRequestLog.PlanRequestLog;
                                if (dtblPlanRequestLog == null || dtblPlanRequestLog.Rows.Count < 0)
                                {
                                    Utilities.showMessage("Internal server error", this.Page);
                                    return;
                                }

                                string strPriceWithoutDiscount, strDiscount, strPayableAmount;
                                //not required now 30/10/2012
                                //strDiscount = getDiscount(dtblDiscounts, lblNoOfMonths.Text).ToString();
                                //strPriceWithoutDiscount = calculatePrice(lblMobileUsers.Text, lblNoOfMonths.Text, strChargePerMnth, "", out strPayableAmount);
                                //strPayableAmount = calculatePrice(lblMobileUsers.Text, lblNoOfMonths.Text, strChargePerMnth, strDiscount, out strPayableAmount);

                                strDiscount = lblDiscount.Text;
                                strPriceWithoutDiscount = lblPrice.Text;
                                strPayableAmount = lblPayableAmount.Text;

                                if (!((lblRequestType.Text.ToLower() == ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_DOWNGRADE).ToString()) && (lblRequestType.Text.ToLower() == ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.PLAN_UPGRADE).ToString())))
                                {

                                    if (!(lblRequestType.Text.ToLower() == ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.USERS_ADDED).ToString()))
                                    {
                                        //for renew
                                        //if validity is less than 0 then after renew plan the new no of months will be validity in the log table
                                        //if validity is greater than 0 then after renew plan the new validity will be validity + validity in log table
                                        if (Convert.ToDouble(strNoOfMonthsRemaining) < 0)
                                        {
                                            strNoOfMonthsRemaining = Convert.ToString(dtblPlanRequestLog.Rows[0]["VALIDITY"]);
                                        }
                                        else
                                        {
                                            strNoOfMonthsRemaining = Convert.ToString(Convert.ToDouble(dtblPlanRequestLog.Rows[0]["VALIDITY"]) + Convert.ToDouble(strNoOfMonthsRemaining));
                                        }

                                        objPurchaseApprovalAcc = new PurchaseRequestApprovalByAccounts(lblCompanyId.Text, lblPlanCode.Text,
                                        Convert.ToInt32(rowPlanDetail["MAX_WORKFLOW"]), Convert.ToInt32(lblMobileUsers.Text), Convert.ToDouble(strChargePerMnth), lblCurrrencyType.Text
                                        , Convert.ToDouble(strNoOfMonthsRemaining), Convert.ToInt32(rowPlanDetail["PUSHMESSAGE_PERDAY"]), Convert.ToInt32(rowPlanDetail["PUSHMESSAGE_PERMONTH"])
                                        , Convert.ToDouble(strPriceWithoutDiscount), Convert.ToDouble(strDiscount), Convert.ToDouble(strPayableAmount), lblRequestType.Text, (string)rowPlanDetail["PLAN_NAME"], Convert.ToInt32(txtamount.Text), false);

                                        //objPurchaseApprovalAcc.Process();
                                    }
                                    else
                                    {
                                        //validating only .check is already done in control panel
                                        if (Convert.ToDouble(strNoOfMonthsRemaining) < 0)
                                        {
                                            Utilities.showMessage("Invalid operation.Plan has expired.The plan needs to be renewed first", this.Page);
                                            return;
                                        }
                                        //for users added
                                        objPurchaseApprovalAcc = new PurchaseRequestApprovalByAccounts(lblCompanyId.Text, lblPlanCode.Text,
                                   Convert.ToInt32(rowPlanDetail["MAX_WORKFLOW"]), (Convert.ToInt32(lblMobileUsers.Text) + Convert.ToInt32(rowCmpCurrentPlan["MAX_USER"])), Convert.ToDouble(strChargePerMnth), lblCurrrencyType.Text
                                   , Convert.ToDouble(strNoOfMonthsRemaining), Convert.ToInt32(rowPlanDetail["PUSHMESSAGE_PERDAY"]), Convert.ToInt32(rowPlanDetail["PUSHMESSAGE_PERMONTH"])
                                   , Convert.ToDouble(strPriceWithoutDiscount), Convert.ToDouble(strDiscount), Convert.ToDouble(strPayableAmount), lblRequestType.Text, (string)rowPlanDetail["PLAN_NAME"], Convert.ToInt32(txtamount.Text), false);

                                        //objPurchaseApprovalAcc.Process();
                                    }
                                }
                                else
                                {
                                    //validating only .check is already done in control panel
                                    if (Convert.ToDouble(strNoOfMonthsRemaining) < 0)
                                    {
                                        Utilities.showMessage("Invalid operation.Plan has expired.The plan needs to be renewed first", this.Page);
                                        return;
                                    }
                                    //for downgrade upgrade
                                    objPurchaseApprovalAcc = new PurchaseRequestApprovalByAccounts(lblCompanyId.Text, lblPlanCode.Text,
                                            Convert.ToInt32(rowPlanDetail["MAX_WORKFLOW"]), (Convert.ToInt32(lblMobileUsers.Text) + Convert.ToInt32(rowCmpCurrentPlan["MAX_USER"])), Convert.ToDouble(strChargePerMnth), lblCurrrencyType.Text
                                            , Convert.ToDouble(strNoOfMonthsRemaining), Convert.ToInt32(rowPlanDetail["PUSHMESSAGE_PERDAY"]), Convert.ToInt32(rowPlanDetail["PUSHMESSAGE_PERMONTH"])
                                            , Convert.ToDouble(strPayableAmount), 0, Convert.ToDouble(strPayableAmount), lblRequestType.Text, (string)rowPlanDetail["PLAN_NAME"], Convert.ToInt32(txtamount.Text), false);
                                }
                            }

                            objPurchaseApprovalAcc.Process();
                            if (objPurchaseApprovalAcc.StatusCode == 0)
                            {
                                Utilities.closeModalPopUp("divCompanyModalContainer", this.Page, "CloseModalPopUp");
                                Utilities.showMessage("Request approved successfully", this.Page, DIALOG_TYPE.Info);
                                if (chckApproved.Checked)
                                {
                                    bindSalesApprovedPlanList();
                                }
                                else
                                {
                                    bindSalesUnApprovedList();
                                }
                                bindNewRegistrationRepeater();
                            }
                            else
                            {
                                Utilities.showMessage("Internal server error.", this.Page, DIALOG_TYPE.Error);
                            }
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                catch
                {
                    Utilities.showAlert("Internal server error", "divUserDtlsModal", this.Page);
                }
            }
            else
            {
                Utilities.showAlert(strError, "divUserDtlsModal", this.Page);
                return;
            }
        }
        //protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        //{
        //    RepeaterItem item = e.Item;
        //    Label lit_id = (Label)item.FindControl("lblid");

        //    if (e.CommandName == "Info")
        //    {
        //        if (chckApproved.Checked)
        //            FillCompanyDetails(lit_id.Text, e, DETAILS_SHOWN_FOR_PROCESS.SALES_APPROVED_PLAN_DETAILS);
        //        else
        //            FillCompanyDetails(lit_id.Text, e, DETAILS_SHOWN_FOR_PROCESS.SALES_UN_APPROVED_PLAN_DETAILS);
        //    }

        //}
        protected void rptPlanRequestsDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Info":
                    Info(e);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing2", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);

                    break;
            }
        }
        protected void FillCompanyDetails(string companyId, RepeaterCommandEventArgs e, DETAILS_SHOWN_FOR_PROCESS process, string planCode, string transactionType)
        {
            AccountApprovalAdmin add = new AccountApprovalAdmin();
            add.CompanyDetails(companyId);
            DataTable dt = add.ResultTable;
            GetPlanRequestsLogDetailsForAcc objPlanRequestLog = new GetPlanRequestsLogDetailsForAcc(companyId, planCode, transactionType);
            objPlanRequestLog.Process();
            DataTable dtblPlanRequestLog = objPlanRequestLog.PlanRequestLog;
            if (dt.Rows.Count > 0)
            {
                hidDetailsFormMode.Value = companyId;
                lname.Text = Convert.ToString(dt.Rows[0]["COMPANY_NAME"]);
                ladd1.Text = Convert.ToString(dt.Rows[0]["STREET_ADDRESS1"]);
                ladd2.Text = Convert.ToString(dt.Rows[0]["STREET_ADDRESS2"]);
                ladd3.Text = Convert.ToString(dt.Rows[0]["STREET_ADDRESS3"]);
                lcity.Text = Convert.ToString(dt.Rows[0]["CITY_NAME"]);
                lstate.Text = Convert.ToString(dt.Rows[0]["STATE_NAME"]);
                lcountry.Text = Convert.ToString(dt.Rows[0]["COUNTRY_NAME"]);
                lzip.Text = Convert.ToString(dt.Rows[0]["ZIP"]);
                lemail.Text = Convert.ToString(dt.Rows[0]["SUPPORT_EMAIL"]);
                lnumber.Text = Convert.ToString(dt.Rows[0]["SUPPORT_CONTACT"]);
                chckPayement.Checked = false;
                txtamount.Text = "";
                //chckApproved.Checked = false;
                lblPlanCode.Text = ((Label)e.Item.FindControl("lblcode")).Text;
                lblRequestType.Text = ((Label)e.Item.FindControl("lblRequestType")).Text;
                lblMobileUsers.Text = ((Label)e.Item.FindControl("lblMobileUsers")).Text;
                lblNoOfMonths.Text = ((Label)e.Item.FindControl("lblMonths")).Text;
                lblCurrrencyType.Text = ((Label)e.Item.FindControl("lblCurrencyType")).Text;
                lblCompanyId.Text = ((Label)e.Item.FindControl("lblid")).Text;

                if (dtblPlanRequestLog.Rows.Count > 0)
                {
                    lblPrice.Text = Convert.ToString(dtblPlanRequestLog.Rows[0]["PRICE_WITHOUT_DISCOUNT"]);
                    lblDiscount.Text = Convert.ToString(dtblPlanRequestLog.Rows[0]["DISCOUNT_PER"]);
                    lblPayableAmount.Text = Convert.ToString(dtblPlanRequestLog.Rows[0]["ACTUAL_PRICE"]);
                    lblChargePerUserPerMonth.Text = Convert.ToString(dtblPlanRequestLog.Rows[0]["USER_CHARGE_PM"]);
                }

                //earlier written in lnkok_click
                Utilities.showModalPopup("divCompanyModalContainer", this.Page, "Company Details", "500", true);
                enabledisablepanels("Info");
                switch (process)
                {
                    case DETAILS_SHOWN_FOR_PROCESS.NEW_COMPANY_DETAILS:
                        showHideActionButtonAndCntrlsByDetails(DETAILS_SHOWN_FOR_PROCESS.NEW_COMPANY_DETAILS);
                        break;
                    case DETAILS_SHOWN_FOR_PROCESS.SALES_UN_APPROVED_PLAN_DETAILS:
                    case DETAILS_SHOWN_FOR_PROCESS.SALES_APPROVED_PLAN_DETAILS:
                        if (chckApproved.Checked)
                        {
                            //btnModalSave.Visible = true;
                            //btnModalCancel.Visible = true;
                            showHideActionButtonAndCntrlsByDetails(DETAILS_SHOWN_FOR_PROCESS.SALES_APPROVED_PLAN_DETAILS);
                            pnlAmountDetails.Visible = true;
                        }
                        else
                        {
                            //btnModalCancel.Visible = false;
                            //btnModalSave.Visible = false;
                            showHideActionButtonAndCntrlsByDetails(DETAILS_SHOWN_FOR_PROCESS.SALES_UN_APPROVED_PLAN_DETAILS);
                            pnlAmountDetails.Visible = false;
                        }
                        break;
                }
                setCommandArgumentOfDenyButton(companyId, planCode, transactionType, DENY_REQUEST_TYPE.PLAN_DENY);
                upUserDtlsModal.Update();
            }

            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
                Utilities.showMessage("Internal server error", this.Page, "Error Record not found", DIALOG_TYPE.Error);
            }

        }
        void Info(RepeaterCommandEventArgs e)
        {
            Label company_id = (Label)e.Item.FindControl("lblid");
            Label lblRequestType = (Label)e.Item.FindControl("lblRequestType");
            Label lblPlanCode = (Label)e.Item.FindControl("lblcode");
            if (chckApproved.Checked)
            {
                FillCompanyDetails(company_id.Text, e, DETAILS_SHOWN_FOR_PROCESS.SALES_APPROVED_PLAN_DETAILS, lblPlanCode.Text, lblRequestType.Text);
            }
            else
            {
                FillCompanyDetails(company_id.Text, e, DETAILS_SHOWN_FOR_PROCESS.SALES_UN_APPROVED_PLAN_DETAILS, lblPlanCode.Text, lblRequestType.Text);
            }

        }
        protected void enabledisablepanels(string mode)
        {
            if (mode == "Info")
            {
                pnlRepeaterBox.Visible = true;
                // pnlMobileUserForm.Visible = true;
            }

        }
        void showHideActionButtonAndCntrlsByDetails(DETAILS_SHOWN_FOR_PROCESS process)
        {
            switch (process)
            {
                case DETAILS_SHOWN_FOR_PROCESS.NEW_COMPANY_DETAILS:
                    btnModalSave.Visible = true;
                    btnModalCancel.Visible = true;
                    btnDenyRequest.Visible = false;
                    break;
                case DETAILS_SHOWN_FOR_PROCESS.SALES_APPROVED_PLAN_DETAILS:
                    btnModalSave.Visible = true;
                    btnModalCancel.Visible = true;
                    btnDenyRequest.Visible = true;
                    break;
                case DETAILS_SHOWN_FOR_PROCESS.SALES_UN_APPROVED_PLAN_DETAILS:
                    btnModalSave.Visible = false;
                    btnModalCancel.Visible = true;
                    btnDenyRequest.Visible = false;
                    break;
            }
        }

        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#buttons'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }

        void showAlert(string message, string divIdToShowMsg)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }


        protected void chckApproved_CheckedChanged(object sender, EventArgs e)
        {
            if (chckApproved.Checked)
            {
                bindSalesApprovedPlanList();
            }
            else
            {
                bindSalesUnApprovedList();
            }
        }
        protected void rptNewCompanyDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName.ToLower())
            {
                case "info":
                    hidPlanCurrencyAndMonthsForApproval.Value = "";
                    processFillNewCompanyDetails(((Label)e.Item.FindControl("lblCompanyId")).Text, ((Label)e.Item.FindControl("lblPlanCode")).Text, ((Label)e.Item.FindControl("lblTransactionType")).Text);
                    break;
            }
        }

        void bindNewRegistrationRepeater()
        {
            GetNewRegisteredCompanyForAccApproval objNewRegisteredCompany = new GetNewRegisteredCompanyForAccApproval();
            objNewRegisteredCompany.Process();
            if (objNewRegisteredCompany.StatusCode == 0)
            {
                DataTable dtblNewRegisteredCompany = objNewRegisteredCompany.ResultTable;
                if (dtblNewRegisteredCompany.Rows.Count > 0)
                {
                    rptNewCompanyDetails.DataSource = dtblNewRegisteredCompany;
                    rptNewCompanyDetails.DataBind();
                }
                else
                {
                    dtblNewRegisteredCompany = null;
                    rptNewCompanyDetails.DataSource = dtblNewRegisteredCompany;
                    rptNewCompanyDetails.DataBind();
                    lblNewCompanyRptHeader.Text = "<h1>No new Registered company request pending</h1>";
                }
            }
            else
            {
                Utilities.showMessage("Internal server error", this.Page);
            }
        }

        void processFillNewCompanyDetails(string companyId, string planCode, string transactionType)
        {
            AccountApprovalAdmin add = new AccountApprovalAdmin();
            add.CompanyDetails(companyId);
            DataTable dtblCompanyDetail = add.ResultTable;

            GetNewRegisteredCompanyForAccApproval objNewRegisteredCompany = new GetNewRegisteredCompanyForAccApproval(companyId, planCode, transactionType);
            objNewRegisteredCompany.Process();
            DataTable dtblNewCompanyPlanDetails = objNewRegisteredCompany.ResultTable;

            if (dtblCompanyDetail.Rows.Count > 0 && dtblNewCompanyPlanDetails.Rows.Count > 0)
            {
                //hidDetailsFormMode.Value = CompanyId;
                lname.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["COMPANY_NAME"]);
                ladd1.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["STREET_ADDRESS1"]);
                ladd2.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["STREET_ADDRESS2"]);
                ladd3.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["STREET_ADDRESS3"]);
                lcity.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["CITY_NAME"]);
                lstate.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["STATE_NAME"]);
                lcountry.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["COUNTRY_NAME"]);
                lzip.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["ZIP"]);
                lemail.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["SUPPORT_EMAIL"]);
                lnumber.Text = Convert.ToString(dtblCompanyDetail.Rows[0]["SUPPORT_CONTACT"]);
                chckPayement.Checked = false;
                txtamount.Text = "";
                //chckApproved.Checked = false;
                lblPlanCode.Text = Convert.ToString(dtblNewCompanyPlanDetails.Rows[0]["PLAN_CODE"]);
                lblRequestType.Text = Convert.ToString(dtblNewCompanyPlanDetails.Rows[0]["TRANSACTION_TYPE"]);
                lblMobileUsers.Text = Convert.ToString(dtblNewCompanyPlanDetails.Rows[0]["MAX_USER"]);
                lblNoOfMonths.Text = Convert.ToString(dtblNewCompanyPlanDetails.Rows[0]["VALIDITY"]);
                lblCurrrencyType.Text = Convert.ToString(dtblNewCompanyPlanDetails.Rows[0]["CHARGE_TYPE"]);
                lblCompanyId.Text = Convert.ToString(dtblNewCompanyPlanDetails.Rows[0]["COMPANY_ID"]);
                lblChargePerUserPerMonth.Text = Convert.ToString(dtblNewCompanyPlanDetails.Rows[0]["USER_CHARGE_PM"]);
                lblPrice.Text = Convert.ToString(dtblNewCompanyPlanDetails.Rows[0]["PRICE_WITHOUT_DISCOUNT"]);
                lblDiscount.Text = Convert.ToString(dtblNewCompanyPlanDetails.Rows[0]["DISCOUNT_PER"]);
                lblPayableAmount.Text = Convert.ToString(dtblNewCompanyPlanDetails.Rows[0]["ACTUAL_PRICE"]);
                hidPlanCurrencyAndMonthsForApproval.Value = Convert.ToString(dtblNewCompanyPlanDetails.Rows[0]["CHARGE_TYPE"] + ";" + Convert.ToString(dtblNewCompanyPlanDetails.Rows[0]["VALIDITY"]));
                Utilities.showModalPopup("divCompanyModalContainer", this.Page, "Company Details", "500", false);
                enabledisablepanels("Info");
                showHideActionButtonAndCntrlsByDetails(DETAILS_SHOWN_FOR_PROCESS.NEW_COMPANY_DETAILS);
                upUserDtlsModal.Update();
                pnlAmountDetails.Visible = true;
            }
            else
            {
                Utilities.showMessage("Internal server error", this.Page);
            }
        }
        #region also used in PlanOrder.aspx
        //also used in plan order.aspx
        protected DataTable getPlanDiscounts(string validMonths)
        {
            GetPlanList objPlanList = new GetPlanList();
            objPlanList.PlanDiscounts();
            return objPlanList.PlanDiscountList;
        }
        //DataTable getPlanDtlWithDiscounts(out DataTable planDiscounts, string planCode)
        //{
        //    GetPlanList objPlanList;
        //    if (planCode == "")
        //    {
        //        objPlanList = new GetPlanList();
        //    }
        //    else
        //    {
        //        objPlanList = new GetPlanList(planCode);
        //    }
        //    objPlanList.PlanDiscounts();
        //    planDiscounts = objPlanList.PlanDiscountList;
        //    objPlanList.Process();
        //    return objPlanList.PlanDetails;
        //}
        DataTable getPlanDtls(string planCode)
        {
            GetPlanList objPlanList;
            if (planCode == "")
            {
                objPlanList = new GetPlanList();
            }
            else
            {
                objPlanList = new GetPlanList(planCode);
            }
            objPlanList.Process();
            return objPlanList.PlanDetails;
        }
        int getDiscount(DataTable planDiscountDtlsDTbl, string months)
        {
            if (planDiscountDtlsDTbl.Rows.Count > 0)
            {
                string filter = String.Format("FROM_MONTH <= '{0}' AND TO_MONTH>='{1}'", Math.Floor(Convert.ToDouble(months)), Math.Floor(Convert.ToDouble(months)));
                DataRow[] rows = planDiscountDtlsDTbl.Select(filter);
                if (rows.Length > 0)
                {

                    return Convert.ToInt32((string)rows[0]["DISCOUNT"]);
                }
                else
                {
                    return 0;
                }
            }
            else
                return 0;
        }
        string calculatePrice(string noOfUsers, string noOfMonths, string pricePerUserPerMonth, string discount, out string payableAmount)
        {
            payableAmount = "";
            double priceWithoutDiscount = (int.Parse(noOfUsers) * double.Parse(noOfMonths) * double.Parse(pricePerUserPerMonth));
            try
            {
                if (discount == "")
                {
                    payableAmount = priceWithoutDiscount.ToString();
                    return priceWithoutDiscount.ToString();
                }
                else
                {
                    double priceAfterDiscount = (int.Parse(noOfUsers) * double.Parse(noOfMonths) * double.Parse(pricePerUserPerMonth)) * int.Parse(discount) / 100;
                    payableAmount = (priceWithoutDiscount - priceAfterDiscount).ToString();
                    return priceAfterDiscount.ToString();

                }
            }
            catch
            {
                return "";
            }
        }
        DataTable getCompanyCurrentPlanDetail(string companyId)
        {
            CurrentPlanDetail objCurrentPlan = new CurrentPlanDetail(companyId, true, false);
            objCurrentPlan.Process();
            return objCurrentPlan.PlanDetails;
        }
        string getValidityOfPlan(out string monthsRemaining, string purchaseDate, string validityMonths)
        {
            DateTime dtPurchaseDate = new DateTime(Convert.ToInt64(purchaseDate));
            DateTime dtValidPeriod = dtPurchaseDate.AddMonths(Convert.ToInt32(Convert.ToDouble(validityMonths)));
            TimeSpan ts = dtValidPeriod.Subtract(DateTime.Now);
            monthsRemaining = (ts.TotalDays / 30.4368499).ToString("N2");
            return monthsRemaining;
        }
        string calculateUpgradeDowngradeAmount(double months, int noOfUsers, double currentPlanChrgPerMnth, double newPlanChrgPerMnth)
        {
            double dblAmountAlreadyPaid = months * noOfUsers * currentPlanChrgPerMnth;
            double dblAmountToPay = months * noOfUsers * newPlanChrgPerMnth;
            return (dblAmountToPay - dblAmountAlreadyPaid).ToString();
        }
        DataTable getRequestDetails()
        {
            AddServerDetails detail = new AddServerDetails();
            DataTable dtblRequestDetail = detail.GetAdminList();
            return dtblRequestDetail;
        }
        DataTable getCompanyCurrentPlanFromCPTbl(string companyId)
        {
            CurrentPlanDetail objCurrentPlan = new CurrentPlanDetail(companyId, true, true);
            objCurrentPlan.Process();
            return objCurrentPlan.PlanDetails;
        }
        #endregion

        protected void btnModalCancel_Click(object source, EventArgs e)
        {
            Utilities.closeModalPopUp("divCompanyModalContainer", this.Page);
        }


        void bindDenyRequestDropDown()
        {
            GetPlanPurchaseDenyRemarks objPlanPurchaseDenyRemarks = new GetPlanPurchaseDenyRemarks();
            objPlanPurchaseDenyRemarks.Process();
            if (objPlanPurchaseDenyRemarks.StatusCode == 0)
            {
                DataTable dtblPlanPurchaseDenyRemarks = objPlanPurchaseDenyRemarks.ResultTable;
                if (dtblPlanPurchaseDenyRemarks.Rows.Count > 0)
                {
                    ddlRemarks.DataSource = dtblPlanPurchaseDenyRemarks;
                    ddlRemarks.DataTextField = "REMARK";
                    ddlRemarks.DataValueField = "REMARK_CODE";
                    ddlRemarks.DataBind();
                }
                else
                {
                    //the remarks should exist in the database table
                    throw new Exception();
                }
            }
            else
            {
                throw new Exception();
            }
        }

        protected void btnCancelDenyRemarks_Click(object sender, EventArgs e)
        {
            Utilities.closeModalPopUp("divDenyRequestBox", this.Page, "Close Remarks Div");
            btnSaveDenyRemarks.CommandArgument = "";
        }

        protected void btnSaveDenyRemarks_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                string strCommandArgument = btnSender.CommandArgument;//; separated values companyId,planCode,RequestType
                string[] aryCommandArgumentValues = strCommandArgument.Split(';');
                SavePlanReqDenyRemarksAndStatusByUserType objSavePlanReqDenyRemarks = new SavePlanReqDenyRemarksAndStatusByUserType(aryCommandArgumentValues[0], aryCommandArgumentValues[1]
                                                                                         , aryCommandArgumentValues[2], USER_TYPE.Account, ddlRemarks.SelectedValue);
                objSavePlanReqDenyRemarks.Process();
                if (objSavePlanReqDenyRemarks.StatusCode == 0)
                {
                    Utilities.showMessage("Denied successfully", this.Page, DIALOG_TYPE.Info);
                    Utilities.closeModalPopUp("divDenyRequestBox", this.Page, "Close Deny Request Box");
                    Utilities.closeModalPopUp("divCompanyModalContainer", this.Page, "Close Plan Request Details model");
                    btnSaveDenyRemarks.CommandArgument = "";
                    if (chckApproved.Checked)
                    {
                        bindSalesApprovedPlanList();
                    }
                    else
                    {
                        bindSalesUnApprovedList();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                Utilities.showMessage("Internal server error", this.Page, DIALOG_TYPE.Error);
            }
        }

        protected void btnDenyRequest_Click(object sender, EventArgs e)
        {
            try
            {
                Button btnSender = (Button)sender;
                bindDenyRequestDropDown();
                btnSaveDenyRemarks.CommandArgument = btnSender.CommandArgument;
                updDenyRequest.Update();
                Utilities.showModalPopup("divDenyRequestBox", this.Page, "Select Remarks", "300", false);

            }
            catch
            {
                Utilities.showMessage("Internal server error", this.Page, "Show Error Message", DIALOG_TYPE.Error);
            }
        }

        void setCommandArgumentOfDenyButton(string companyId, string planCode, string requestType, DENY_REQUEST_TYPE denyRequestType)
        {
            btnDenyRequest.CommandArgument = companyId + ";" + planCode + ";" + requestType + ";" + denyRequestType;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientAdmin
{
    public partial class SalesCompanyApproval : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;


            if (!Page.IsPostBack)
            {
                LinkButton ChangePass = (LinkButton)this.Master.Master.FindControl("lbChangePwd");
                Literal ltlUserType = (Literal)this.Master.Master.FindControl("ltUserType");

                /*if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else if (Page.PreviousPage != null)
                {
                    ContentPlaceHolder cphPageContent = (ContentPlaceHolder)Page.PreviousPage.Form.FindControl("MainCanvas").FindControl("PageCanvas");
                    hfs.Value = ((HiddenField)cphPageContent.FindControl("hfs")).Value;
                }*/

                string Role = strRole.ToUpper();
                if (Role == "A")
                {
                    ltlUserType.Text = "mFicient Admin";
                    ChangePass.PostBackUrl = "~/ChangePassword.aspx";
                }

                else if (Role == "R")
                {
                    ltlUserType.Text = "Reseller";
                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }

                else if (Role == "S")
                {
                    ltlUserType.Text = "Sales Executive";
                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }
                else if (Role == "SM")
                {
                    ltlUserType.Text = "Sales Manager";
                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }

                else if (Role == "SUPP")
                {
                    ltlUserType.Text = "Support";

                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }

                else if (Role == "ACC")
                {
                    ltlUserType.Text = "Accounts";
                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }
                BindCountryPending();
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "$(\"input\").uniform();", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "testing", " $(\"select\").uniform();$(\"input\").uniform();", true);
            }
        }

        public void BindCountryApproved()
        {
            CompanyDetailsAdmin add = new CompanyDetailsAdmin();
            DataTable objdt = add.GetCompanyDetailsApproved();
            if (objdt.Rows.Count > 0)
            {
                rptUserDetails.DataSource = objdt;
                rptUserDetails.DataBind();
                lblHeaderInfo.Text = "<h1>APPROVED COMPANIES</h1>";
            }
            else
            {
                rptUserDetails.DataSource = null;
                rptUserDetails.DataBind();
                lblHeaderInfo.Text = "<h1>no company approved yet</h1>";
            }
        }


        public void BindCountryPending()
        {
            CompanyDetailsAdmin add = new CompanyDetailsAdmin();
            DataTable objdt = add.GetCompanyDetailsPending();
            if (objdt.Rows.Count > 0)
            {
                rptUserDetails.DataSource = objdt;
                rptUserDetails.DataBind();
                lblHeaderInfo.Text = "<h1>PENDING COMPANIES</h1>";
            }
            else
            {
                rptUserDetails.DataSource = null;
                rptUserDetails.DataBind();
                lblHeaderInfo.Text = "<h1>No new company exists for approval</h1>";
            }
        }


        //protected void lnkok_Click(object sender, EventArgs e)
        //{
        //    Utilities.showModalPopup("modalPopUpDetails", this.Page, "Company Details", "350", true, false);
        //    enabledisablepanels("Info");
        //    upUserDtlsModal.Update();

        //}


        protected void FillCompanyDetails(string companyid)
        {
            GetCompanyDetailsWithCurrentPlan objCmpDtlWithPlan = new
                GetCompanyDetailsWithCurrentPlan(companyid);
            objCmpDtlWithPlan.Process();

            if (objCmpDtlWithPlan.StatusCode != 0)
            {
                Utilities.showMessage(objCmpDtlWithPlan.StatusDescription, this.Page);
            }
            else
            {
                hidCmpIdWhenEdit.Value = companyid;

                lblCompanyName.Text = objCmpDtlWithPlan.CmpInfo.CompanyName;

                lblCompanyAddress.Text = objCmpDtlWithPlan.CmpInfo.StreetAddress1 + " " + objCmpDtlWithPlan.CmpInfo.StreetAddress2
                    + " " + objCmpDtlWithPlan.CmpInfo.StreetAddress3;

                lblCmpCity.Text = objCmpDtlWithPlan.CmpInfo.CityName;
                lblCmpState.Text = objCmpDtlWithPlan.CmpInfo.StateName;
                lblCmpEmail.Text = objCmpDtlWithPlan.CmpInfo.SupportEmail;
                lblCmpContactNo.Text = objCmpDtlWithPlan.CmpInfo.SupportContact;
                lblCmpAdminName.Text = objCmpDtlWithPlan.CmpAdmin.Firstname + " " +
                    objCmpDtlWithPlan.CmpAdmin.MiddleName + " " +
                    " " +
                    objCmpDtlWithPlan.CmpAdmin.LastName;
                lblStatus.Text = objCmpDtlWithPlan.CmpInfo.SalesManagerApproved ? "Company Working" : "Processing";
                lblAdminEmailId.Text = objCmpDtlWithPlan.CmpAdmin.EmailId;
                if (objCmpDtlWithPlan.CmpAdmin.IsTrial)
                {
                    lblCmpPlanName.Text = objCmpDtlWithPlan.TrialPlanDtl.PlanName;
                }
                else
                {
                    lblCmpPlanName.Text = objCmpDtlWithPlan.PaidPlanDtl.PlanName;
                }
                lblPlanCode.Text = objCmpDtlWithPlan.CmpCurrentPlan.PlanCode;
                lblMaxWorkflow.Text = Convert.ToString(objCmpDtlWithPlan.CmpCurrentPlan.MaxWorkFlow);
                lblMaxNoOfUsers.Text = Convert.ToString(objCmpDtlWithPlan.CmpCurrentPlan.MaxUser);
                DateTime dtValidTill = Utilities.getValidTillAsDateTime(
                    objCmpDtlWithPlan.CmpCurrentPlan.Validity,
                    objCmpDtlWithPlan.CmpCurrentPlan.PurchaseDate);
                lblValidUpto.Text = dtValidTill.ToShortDateString();

                Utilities.showModalPopup("divCompanyDtlsContainer", this.Page, "Company Details", "450", true, false);
                enabledisablepanels("Info");
                upUserDtlsModal.Update();
            }
            #region Previous Code
            //AddCompanyDetailsAdmin get = new AddCompanyDetailsAdmin();
            //get.CompanyPlanAdminDetails(companyid);
            //DataTable dt = get.ResultTable;
            //string strDescription = get.StatusDescription;
            //if (dt == null)
            //{
            //    Utilities.showMessage(strDescription, this.Page);
            //}
            //else if (dt.Rows.Count > 0)
            //{
            //    hidCmpIdWhenEdit.Value = companyid;
            //    lblCompanyName.Text = Convert.ToString(dt.Rows[0]["CompanyName"]);
            //    lblCompanyAddress.Text = Convert.ToString(dt.Rows[0]["CompanyAddress"]);
            //    lblCmpCity.Text = Convert.ToString(dt.Rows[0]["CompanyCity"]);
            //    lblCmpState.Text = Convert.ToString(dt.Rows[0]["companystate"]);
            //    lblCmpEmail.Text = Convert.ToString(dt.Rows[0]["companyemail"]);
            //    lblCmpContactNo.Text = Convert.ToString(dt.Rows[0]["companycontact"]);
            //    lblCmpAdminName.Text = Convert.ToString(dt.Rows[0]["AdminName"]);
            //    lblStatus.Text = Convert.ToString(dt.Rows[0]["STATUS"]);
            //    lblAdminEmailId.Text = Convert.ToString(dt.Rows[0]["EMAIL_ID"]);
            //    lblCmpPlanName.Text = Convert.ToString(dt.Rows[0]["PLAN_NAME"]);
            //    lblPlanCode.Text = Convert.ToString(dt.Rows[0]["PLAN_CODE"]);
            //    lblMaxWorkflow.Text = Convert.ToString(dt.Rows[0]["MAX_WORKFLOW"]);
            //    lblMaxNoOfUsers.Text = Convert.ToString(dt.Rows[0]["MAX_USER"]);
            //    DateTime EnqDate = new DateTime(Convert.ToInt64(dt.Rows[0]["Date"]));
            //    lbl13.Text = EnqDate.ToShortDateString();

            //}
            //else
            //{
            //    ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            //}
            #endregion
        }

        //protected void FillCompanyDetails1(string CompanyId)
        //{
        //    CompanyDetailsAdmin add = new CompanyDetailsAdmin();
        //    add.CompanyDetails(CompanyId);
        //    DataTable dt = add.ResultTable;
        //    if (dt.Rows.Count > 0)
        //    {
        //        hidDetailsFormMode.Value = CompanyId;

        //        lid.Text = CompanyId;
        //        lname.Text = Convert.ToString(dt.Rows[0]["COMPANY_NAME"]);
        //        ladd1.Text = Convert.ToString(dt.Rows[0]["STREET_ADDRESS1"]);
        //        ladd2.Text = Convert.ToString(dt.Rows[0]["STREET_ADDRESS2"]);
        //        ladd3.Text = Convert.ToString(dt.Rows[0]["STREET_ADDRESS3"]);
        //        lcity.Text = Convert.ToString(dt.Rows[0]["CITY_NAME"]);
        //        lstate.Text = Convert.ToString(dt.Rows[0]["STATE_NAME"]);
        //        lcountry.Text = Convert.ToString(dt.Rows[0]["COUNTRY_NAME"]);
        //        lzip.Text = Convert.ToString(dt.Rows[0]["ZIP"]);
        //        lemail.Text = Convert.ToString(dt.Rows[0]["SUPPORT_EMAIL"]);
        //        lnumber.Text = Convert.ToString(dt.Rows[0]["SUPPORT_CONTACT"]);
        //        Utilities.showModalPopup("divCompanyDtlsContainer", this.Page, "Company Details", "450", true,false);
        //        enabledisablepanels("Info");
        //        upUserDtlsModal.Update();
        //    }
        //    else
        //    {
        //        ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
        //    }
        //}

        void Info(RepeaterCommandEventArgs e)
        {
            Label company_id = (Label)e.Item.FindControl("lblid");
            //hidPlanCodeForApproval.Value = ((Label)e.Item.FindControl("lblcode")).Text;
            //hidIsTrial.Value = ((Label)e.Item.FindControl("lblIsTrial")).Text;
            hidCodeIsTrialTransactionType.Value = ((Label)e.Item.FindControl("lblcode")).Text + ";" + ((Label)e.Item.FindControl("lblIsTrial")).Text + ";" + ((Label)e.Item.FindControl("lblTransactionType")).Text;
            FillCompanyDetails(company_id.Text);
        }
        protected void lnkApprove_Click(object sender, EventArgs e)
        {
        }

        protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item;
            Label lit_id = (Label)item.FindControl("lblid");

            if (e.CommandName == "Info")
            {
                FillCompanyDetails(lit_id.Text);
            }
        }

        protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Info":
                    Info(e);
                    break;
            }
        }


        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#buttons'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        void showAlert(string message, string divIdToShowMsg)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        protected void enabledisablepanels(string mode)
        {
        }

        protected void radListSelection_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (radListSelection.SelectedValue == "1")
            {
                BindCountryPending();
                //lnkAddNewUser.Visible = true;
                btnApprove.Visible = true;
            }
            else
            {
                BindCountryApproved();
                //lnkAddNewUser.Visible = false;
                btnApprove.Visible = false;
            }
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            PurchaseRequestApprovalBySales objApprovalSales = null;
            if (Convert.ToBoolean(hidCodeIsTrialTransactionType.Value.Split(';')[1]))
            {
                objApprovalSales = new PurchaseRequestApprovalBySales(hidCmpIdWhenEdit.Value, hidCodeIsTrialTransactionType.Value.Split(';')[0], hidCodeIsTrialTransactionType.Value.Split(';')[2], true, true);
            }
            else
            {
                objApprovalSales = new PurchaseRequestApprovalBySales(hidCmpIdWhenEdit.Value, hidCodeIsTrialTransactionType.Value.Split(';')[0], hidCodeIsTrialTransactionType.Value.Split(';')[2], false, true);
            }
            objApprovalSales.Process();
            if (objApprovalSales.StatusCode == 0)
            {
                Utilities.closeModalPopUp("divCompanyDtlsContainer", this.Page);
                Utilities.showMessage("Company approved successfully", this.Page, "show successfull");
                clearControlsUsedForApprove();
                if (radListSelection.SelectedValue == "1")//pending
                {
                    BindCountryPending();
                }
                else
                {
                    BindCountryApproved();
                }
            }
            else
            {
                Utilities.showMessage("Internal server error", this.Page);
            }
            //clear the hidden field.

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //clear hidden field
            clearControlsUsedForApprove();
            Utilities.closeModalPopUp("divCompanyDtlsContainer", this.Page);
        }
        void clearControlsUsedForApprove()
        {
            hidCodeIsTrialTransactionType.Value = "";
        }
        protected void rptUserDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblTransactionType = (Label)e.Item.FindControl("lblTransactionType");
                if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "TRANSACTION_TYPE")) == "")
                {
                    //because when creating company then no record is inserted in the log table if the company is trial type.So only in that case we will get null value
                    lblTransactionType.Text = ((int)PLAN_RENEW_OR_CHANGE_REQUESTS.TRIAL).ToString();
                }
                else
                {
                    lblTransactionType.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "TRANSACTION_TYPE"));
                }
            }
        }
    }
}
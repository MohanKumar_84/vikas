﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Text;
using System.Threading;

namespace mFicientAdmin
{
    public partial class SupportApproval : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;




            if (!Page.IsPostBack)
            {
                LinkButton ChangePass = (LinkButton)this.Master.Master.FindControl("lbChangePwd");
                Literal ltlUserType = (Literal)this.Master.Master.FindControl("ltUserType");

                /*if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else if (Page.PreviousPage != null)
                {
                    ContentPlaceHolder cphPageContent = (ContentPlaceHolder)Page.PreviousPage.Form.FindControl("MainCanvas").FindControl("PageCanvas");
                    hfs.Value = ((HiddenField)cphPageContent.FindControl("hfs")).Value;
                }*/
               
                if (string.IsNullOrEmpty(hfsValue))
                {
                    Response.Redirect(@"Default.aspx");
                }

                string Role = strRole.ToUpper();
                if (Role == "A")
                {
                    ltlUserType.Text = "mFicient Admin";

                    ChangePass.PostBackUrl = "~/ChangePassword.aspx";
                }

                else if (Role == "R")
                {
                    ltlUserType.Text = "Reseller";

                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }

                else if (Role == "S")
                {
                    ltlUserType.Text = "Sales Executive";

                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }
                else if (Role == "SM")
                {
                    ltlUserType.Text = "Sales Manager";

                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }

                else if (Role == "SUPP")
                {
                    ltlUserType.Text = "Support";

                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }

                else if (Role == "ACC")
                {
                    ltlUserType.Text = "Accounts";

                    ChangePass.PostBackUrl = "~/ChangePassUsers.aspx";
                }
                GetServerList();
                BindServerDropDown(ddlserver);
                BindMBuzzServerDropDown(ddlmbuzz);
                BindMplugginServerDropDown(ddlmpluggin);
                Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "$(\"input\").uniform();", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "postBackScript", " $(\"select\").uniform();$(\"input\").uniform();$(\"#content\").find('div.widgets').wl_Widget();$('#aspnetForm').wl_Form();", true);//12/10/2012
            }
        }
         void BindServerDropDown(DropDownList _ddlDropdownId)
        {
            SupportApprovalAdmin get = new SupportApprovalAdmin();
            get.GetServerName();
            DataTable dt = get.ResultTable;
            _ddlDropdownId.DataSource = dt;
            _ddlDropdownId.DataTextField = "SERVER_NAME";
            _ddlDropdownId.DataValueField = "SERVER_ID";
            _ddlDropdownId.DataBind();
            _ddlDropdownId.Items.Insert(0, new ListItem("--Select-- ", "-1"));
        }


         void BindMplugginServerDropDown(DropDownList _ddlDropdownId)
        {
            SupportApprovalAdmin get = new SupportApprovalAdmin();
            get.GetMPlugginServer();
            DataTable dt = get.ResultTable;
            _ddlDropdownId.DataSource = dt;
            _ddlDropdownId.DataTextField = "MP_SERVER_NAME";
            _ddlDropdownId.DataValueField = "MP_SERVER_ID";
            _ddlDropdownId.DataBind();
            _ddlDropdownId.Items.Insert(0, new ListItem("--Select-- ", "-1"));
        }

         void BindMBuzzServerDropDown(DropDownList _ddlDropdownId)
        {
            SupportApprovalAdmin get = new SupportApprovalAdmin();
            get.GetMBuzzServer();
            DataTable dt = get.ResultTable;
            _ddlDropdownId.DataSource = dt;
            _ddlDropdownId.DataTextField = "MBUZZ_SERVER_NAME";
            _ddlDropdownId.DataValueField = "MBUZZ_SERVER_ID";
            _ddlDropdownId.DataBind();
            _ddlDropdownId.Items.Insert(0, new ListItem("--Select-- ", "-1"));
        }
         void GetServerList()
        {
                GetServerDetailsAdmin get = new GetServerDetailsAdmin();
                DataTable objdt = get.GetList();
                if (objdt.Rows.Count > 0)
                {
                    rptUserDetails.DataSource = objdt;
                    rptUserDetails.DataBind();
                }
        }

        protected void lnkchange_Click(object sender, EventArgs e)
        {
            Utilities.showModalPopup("divUserDtlsModal", this.Page, "Company Details", "400", true,false);
            enabledisablepanels("Change");
            upUserDtlsModal.Update();
        }

        protected void rptUserDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
         if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
         {
                if (((Label)e.Item.FindControl("lblserver")).Text == "Not Alloted" || ((Label)e.Item.FindControl("lblmbuzzserver")).Text == "Not Alloted" || ((Label)e.Item.FindControl("lblmplugginserver")).Text == "Not Alloted")
                    {
                        ( (LinkButton)e.Item.FindControl("lnkallote")).Visible= true;
                        ((LinkButton)e.Item.FindControl("lnkchange")).Visible = false;  
                    }
                else
                {
                    ((LinkButton)e.Item.FindControl("lnkchange")).Visible = true;
                    ((LinkButton)e.Item.FindControl("lnkallote")).Visible = false;
                }
                }
        }

        protected void FillDetails(string CompanyId)
        {
            SupportApprovalAdmin get = new SupportApprovalAdmin();
            get.GetCompany(CompanyId);
            DataTable dt = get.ResultTable;
            if (dt!=null && dt.Rows.Count > 0)
            {
                lname.Text = Convert.ToString(dt.Rows[0]["COMPANY_NAME"]);
                ddlserver.SelectedIndex = ddlserver.Items.IndexOf(ddlserver.Items.FindByText(Convert.ToString(dt.Rows[0]["SERVER_NAME"])));
                string strPreserverId = ddlserver.SelectedValue;
                hidPreviousServerId.Value = strPreserverId;
                ddlmbuzz.SelectedIndex =ddlmbuzz.Items.IndexOf(ddlmbuzz.Items.FindByText( Convert.ToString(dt.Rows[0]["MBUZZ_SERVER_NAME"])));
                string strPreMbuzzId = ddlmbuzz.SelectedValue;
                hidPreviousMbuzzId.Value = strPreMbuzzId;
                ddlmpluggin.SelectedIndex =ddlmpluggin.Items.IndexOf(ddlmpluggin.Items.FindByText( Convert.ToString(dt.Rows[0]["MP_SERVER_NAME"])));
            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }

        protected void FillAllotedDetails(string CompanyId)
        {
            AddCompanyDetailsAdmin get = new AddCompanyDetailsAdmin();
            get.GetCompanyName(CompanyId);
            DataTable dt = get.ResultTable;
           
            if (dt.Rows.Count > 0)
            {
                lname.Text = Convert.ToString(dt.Rows[0]["COMPANY_NAME"]);
            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }
        protected void lnkallote_Click(object sender, EventArgs e)
        {
            Utilities.showModalPopup("divUserDtlsModal", this.Page, "Company Details", "400", true,false);
            enabledisablepanels("Allote");
            upUserDtlsModal.Update();
        }

        protected void enabledisablepanels(string mode)
        {
            if (mode == "Allote")
            {
                //txt.Visible = true;
                lbl.Visible = false;
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = true;
                ddlserver.SelectedValue = "-1";
                ddlmbuzz.SelectedValue = "-1";
                ddlmpluggin.SelectedValue = "-1";
            }
            else if (mode == "Change")
            {
              //  txt.Visible = false;
                lbl.Visible = true;
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = true;
            }
            else
            {
               // clearControls();
                pnlRepeaterBox.Visible = false;
                pnlMobileUserForm.Visible = true;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string strErrorDescription = "";
            string strMessage = ValidatePanel();
            if (!String.IsNullOrEmpty(strMessage))
            {
                Utilities.showAlert(strMessage, "DivUserDetails", this.Page);
            }
            else
            {
                SupportApprovalAdmin supp = new SupportApprovalAdmin();
                string strNowserverid = ddlserver.SelectedValue;
                string strnowmbuzzId = ddlmbuzz.SelectedValue;
                string mplugginserverid = ddlmpluggin.SelectedValue;

                AddCompanyDetailsAdmin get = new AddCompanyDetailsAdmin();
                string CompanyName = get.GetCompanyName(hdi.Value);
                if (get.GetServerUrl(ddlserver.SelectedValue, strnowmbuzzId))
                {
                    //if ((hidPreviousServerId.Value != strNowserverid && hidPreviousServerId.Value != "" )||(hidPreviousMbuzzId.Value != strnowmbuzzId ))
                    //{
                    //    HttpResponseStatus obj = GetResponseForAddUpdate(hdi.Value, CompanyName, serverUrl, out strErrorDescription);
                    //    if (obj.StatusCode == HttpStatusCode.OK)
                    //    {
                    //        //if cd==0 and (desc=0 and hidPreviousMbuzzId!="")--->HttpResponseStatus objdel = GetResponseForDelete(hdi.Value, CompanyName, hidPreviousMbuzzId.Value, out strErrorDescription);
                    //        HttpResponseJsonParse objParsedJson = new HttpResponseJsonParse(obj.ResponseText);
                    //        if(objParsedJson.RespCode =="0" && objParsedJson.RespDesc=="0" && hidPreviousMbuzzId.Value !="" )
                    //        {
                    //            HttpResponseStatus objdel = GetResponseForDelete(hdi.Value, CompanyName, hidPreviousMbuzzId.Value, out strErrorDescription);
                    //        }
                    //    }
                    //    else
                    //        isServerUpdate = false;
                    //}
                    try
                    {
                        if (hidPreviousServerId.Value != strNowserverid || hidPreviousMbuzzId.Value != strnowmbuzzId)
                        {
                            string strTicks = Convert.ToString(DateTime.Now.Ticks);
                            if (hidPreviousServerId.Value != strNowserverid || hidPreviousMbuzzId.Value != strnowmbuzzId)
                            {
                                mBuzzAddUpdateCompany objmBuzzAddUpdateCompany = new mBuzzAddUpdateCompany(strTicks.Substring(strTicks.Length - 4, 4), hdi.Value, CompanyName, get.ServerURL);
                                mBuzzClient objmBuzzClient = new mBuzzClient(objmBuzzAddUpdateCompany.RequestJson, get.mBuzz_ServerIP, get.mBuzz_port);
                                ThreadPool.QueueUserWorkItem(new WaitCallback(objmBuzzClient.Connect), null);
                            }
                            if (hidPreviousMbuzzId.Value != strnowmbuzzId && !string.IsNullOrEmpty(hidPreviousMbuzzId.Value))
                            {
                                if (get.GetServerUrl(hidPreviousServerId.Value, hidPreviousMbuzzId.Value))
                                {
                                    mBuzzRequestJson obj = new mBuzzRequestJson();
                                    obj.enid = hdi.Value;
                                    obj.mrid = strTicks.Substring(strTicks.Length - 5, 4);
                                    obj.type = ((int)MessageCode.REMOVE_COMPANY).ToString();
                                    string json = Utilities.getRequestJson(Utilities.SerializeJson<mBuzzRequestJson>(obj));
                                    mBuzzClient objmBuzzClient = new mBuzzClient(json, get.mBuzz_ServerIP, get.mBuzz_port);
                                    //ThreadPool.QueueUserWorkItem(new WaitCallback(objmBuzzClient.Connect), null);
                                }
                            }
                        }
                    }
                    catch (Exception Ex)
                    {
                        string str = Ex.Message;
                    }
                    bool result = supp.CheckUpdateInsert(strNowserverid, strnowmbuzzId, hdi.Value, mplugginserverid);
                    if (result == true)
                    {
                        Utilities.showMessage("Server Added Successfully", this.Page, "second script", DIALOG_TYPE.Info);
                        Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
                        GetServerList();
                    }
                }
            }
        }

        //private HttpResponseStatus GetResponseForAddUpdate(string _CompanyId, string _CompanyName,string Url, out string _ErrorDescription)
        //{
        //    _ErrorDescription = "";
        //    string strUrl = "";
        //    strUrl = "https://mbuzz.mficient.net?d=" + Utilities.UrlEncode(GetServerRequestForAddUpdate(_CompanyId, _CompanyName, Url));
        //    HttpResponseStatus obj = new HttpResponseStatus(true,HttpStatusCode.NotFound, HttpStatusCode.NotFound.ToString(), string.Empty);
        //    if (strUrl.Length != 0)
        //    {
        //        obj = CallHttpWebService(strUrl, out _ErrorDescription);
        //    }
        //    return obj;
        //}
        //private string GetServerRequestForAddUpdate(string _CompanyId, string _CompanyName,string _WsUrl)
        //{
        //    string strRqst = "";
        //    UpdateServerReq objAccSett = new UpdateServerReq();
        //    string strTicks = Convert.ToString(DateTime.Now.Ticks);
        //    string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);
        //    if (objAccSett.StatusCode == 0)
        //    {
        //        //strRqst = "{\"req\":{\"func\":\"" + (int)MBUZZ_FUNCTION_CODES.ADD_UPDATE_COMPANY + "\",\"rid\":\"" + strRqtId + "\",\"sid\":\"" + hfs.Value.Split(',')[1] + "\",\"data\":\"" + "\",\"eid\":\"" + _CompanyId + "\",\"cmpname\":\"" + _CompanyName + "\",\"wsurl\":\"" + _WsUrl + "\",\"mbid\":\""+ddlmbuzz.SelectedValue+"\"}}}";
        //        strRqst = "{\"req\":{\"func\":\"" + (int)MBUZZ_FUNCTION_CODES.ADD_UPDATE_COMPANY + "\",\"rid\":\"" + strRqtId + "\",\"sid\":\"" + strSessionId + "\",\"data\":\"" + "\",\"enid\":\"" + _CompanyId + "\",\"cmpnm\":\"" + _CompanyName + "\",\"wsurl\":\"" + _WsUrl + "\",\"mbid\":\"" + ddlmbuzz.SelectedValue + "\"}}}";
        //    }
        //    return strRqst;
        //}

        //private HttpResponseStatus GetResponseForAddUpdateMbuzz(string _CompanyId, string _CompanyName, string Id, out string _ErrorDescription)
        //{
        //    _ErrorDescription = "";
        //    string strUrl = "";
        //    strUrl = "https://mbuzz.mficient.net?d=" + Utilities.UrlEncode(GetServerRequestForAddUpdateMbuzz(_CompanyId, _CompanyName, Id));
        //    HttpResponseStatus obj = new HttpResponseStatus(true, HttpStatusCode.NotFound, HttpStatusCode.NotFound.ToString(), string.Empty);
        //    if (strUrl.Length != 0)
        //    {
        //        obj = CallHttpWebService(strUrl, out _ErrorDescription);
        //    }
        //    return obj;
        //}

        //private string GetServerRequestForAddUpdateMbuzz(string _CompanyId, string _CompanyName, string _ServerId)
        //{
        //    string strRqst = "";
        //    UpdateServerReq objAccSett = new UpdateServerReq();
        //    string strTicks = Convert.ToString(DateTime.Now.Ticks);
        //    string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);
        //    if (objAccSett.StatusCode == 0)
        //    {
        //       // strRqst = "{\"req\":{\"func\":\"" + (int)MBUZZ_FUNCTION_CODES.ADD_UPDATE_COMPANY + "\",\"rid\":\"" + strRqtId + "\",\"sid\":\"" + hfs.Value.Split(',')[1] + "\",\"data\":\"" + "\",\"eid\":\"" + _CompanyId + "\",\"cmpnm\":\"" + _CompanyName + "\",\"mbid\":\"" + _ServerId + "\"}}}";
        //        strRqst = "{\"req\":{\"func\":\"" + (int)MBUZZ_FUNCTION_CODES.ADD_UPDATE_COMPANY + "\",\"rid\":\"" + strRqtId + "\",\"sid\":\"" + strSessionId + "\",\"data\":\"" + "\",\"eid\":\"" + _CompanyId + "\",\"cmpnm\":\"" + _CompanyName + "\",\"mbid\":\"" + _ServerId + "\"}}}";
        //    }
        //    return strRqst;
        //}

        //private HttpResponseStatus GetResponseForDelete(string _CompanyId, string _CompanyName,string _ServerId, out string _ErrorDescription)
        //{
        //    _ErrorDescription = "";
        //    string strUrl = "";
        //    strUrl = "https://mbuzz.mficient.net?d=" + Utilities.UrlEncode(GetServerRequestForDelete(_CompanyId, _CompanyName,_ServerId));
        //    HttpResponseStatus obj = new HttpResponseStatus(true, HttpStatusCode.NotFound, HttpStatusCode.NotFound.ToString(), string.Empty);
        //    if (strUrl.Length != 0)
        //    {
        //        obj = CallHttpWebService(strUrl, out _ErrorDescription);
        //    }
        //    return obj;
        //}

        //private string GetServerRequestForDelete(string _CompanyId, string _CompanyName,string _ServerId)
        //{
        //    string strRqst = "";
        //    RemoveServerReq objAccSett = new RemoveServerReq();
        //    string strTicks = Convert.ToString(DateTime.Now.Ticks);
        //    string strRqtId = strTicks.Substring(strTicks.Length - 4, 4);
        //    if (objAccSett.StatusCode == 0)
        //    {
        //        //strRqst = "{\"req\":{\"func\":\"" + (int)MBUZZ_FUNCTION_CODES.REMOVE_COMPANY + "\",\"rid\":\"" + strRqtId + "\",\"sid\":\"" + hfs.Value.Split(',')[1] + "\",\"data\":\"" + "\",\"eid\":\"" + _CompanyId + "\",\"serid\":\"" + hidPreviousMbuzzId.Value + "\"}}}";
        //        strRqst = "{\"req\":{\"func\":\"" + (int)MBUZZ_FUNCTION_CODES.REMOVE_COMPANY + "\",\"rid\":\"" + strRqtId + "\",\"sid\":\"" + strSessionId + "\",\"data\":\"" + "\",\"eid\":\"" + _CompanyId + "\",\"serid\":\"" + hidPreviousMbuzzId.Value + "\"}}}";
        //    }
        //    return strRqst;
        //}
        //private HttpResponseStatus CallHttpWebService(string _WebServiceURL, out string _ErrorDescription)
        //{
        //    _ErrorDescription = "";
        //    HTTP oHttp = new HTTP(_WebServiceURL);
        //    oHttp.HttpRequestMethod = HTTP.EnumHttpMethod.HTTP_GET;
        //    HttpResponseStatus oResponse = oHttp.Request();
        //    return oResponse;
        //}

        public string ValidatePanel()
        {
            string strmessage = "";
            if (ddlserver.SelectedValue == "-1")
            {
                strmessage = strmessage + "Please select mFicient server<br/>";
            }
            if (ddlmbuzz.SelectedValue == "-1")
            {
                strmessage = strmessage + "Please select mBuzz server<br/>";
            }
            if (ddlmpluggin.SelectedValue == "-1")
            {
                strmessage = strmessage + "Please select mPlugin server<br/>";
            }
              return strmessage;
        }

        protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item; 
            Label lit_id = (Label)item.FindControl("lblid");
            if (e.CommandName == "Change")
            {
                FillDetails(lit_id.Text);
            }
        }

        protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Change":
                    processEdit(e);
                   // ProcessUpdate();
                    Utilities.showModalPopup("divUserDtlsModal", this.Page, "Edit Server Details", "350", true,false);
                    enabledisablepanels("Change");
                    upUserDtlsModal.Update();
                    break;

                case "Allote":
                    ProcessAllot(e);
                    //ProcessAdd();
                    Utilities.showModalPopup("divUserDtlsModal", this.Page, "Add Server Details", "350", true,false);
                    enabledisablepanels("Change");
                    upUserDtlsModal.Update();
                    break;
            }
        }

        void ProcessAllot(RepeaterCommandEventArgs e)
        {
            Label company_id = (Label)e.Item.FindControl("lblid");
            hdi.Value = company_id.Text;
            FillAllotedDetails(company_id.Text);
            enabledisablepanels("Allote");
        }

        void processEdit(RepeaterCommandEventArgs e)
        {
            hidDetailsFormMode.Value = "Change";
            Label company_id = (Label)e.Item.FindControl("lblid");
            hdi.Value = company_id.Text;
            FillDetails(company_id.Text);
            enabledisablepanels("Change");
        }
    }
}
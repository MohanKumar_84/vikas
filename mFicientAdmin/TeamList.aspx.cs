﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Net;



namespace mFicientAdmin
{
    public partial class TeamList : System.Web.UI.Page
    {
        enum RepeaterType
        {
            SalesPerson,
            Reseller
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else
                {
                    if (Page.PreviousPage != null)
                    {
                        ContentPlaceHolder cphPageContent = (ContentPlaceHolder)Page.PreviousPage.Form.FindControl("MainCanvas").FindControl("PageCanvas");
                        hfs.Value = ((HiddenField)cphPageContent.FindControl("hfs")).Value;
                    }
                }
                if (hfs.Value != string.Empty)
                {
                    showHidePanelsByUserType();
                    GetUserDetailsAdmin objUserDtls = new GetUserDetailsAdmin();
                    objUserDtls.GetUserDetailsByNameAndUserCode(hfs.Value.Split(',')[0], hfs.Value.Split(',')[2]);
                    bindRptsByUserType(objUserDtls.ResultTable);
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "testing", " $(\"select\").uniform();", true);
            }
        }

        protected void lnkAddNewMobileUser_Click(object sender, EventArgs e)
        {
            clearControls();
            enableDisablePanels("Add");
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        public string ValidatePanel()
        {
            string strmessage = "";
            //if (!Utilities.IsValidString(txtname.Text, true, true, false, " ", 4, 20, false, false))
            //{
            //    strmessage = strmessage + "Please Enter correct Company Name.<br />";
            //}

            //if (!Utilities.IsValidString(txtid.Text, true, true, true, "", 4, 20, false, false))
            //{
            //    strmessage = strmessage + "Please Enter correct Company Id.<br />";
            //}

            //if (!Utilities.IsValidDateOfBirth(txtdate.Text))
            //{
            //    strmessage = strmessage + "Please Enter correct Date.<br />";
            //}

            return strmessage;
        }
        void enableDisablePanels(string mode)
        {

            //if (mode == "Add")
            //{
            //    pnlRepeaterBox.Visible = false;

            //    pnlMobileUserForm.Visible = true;
            //}
            //else if (mode == "Edit")
            //{
            //    pnlRepeaterBox.Visible = false;
            //    pnlMobileUserForm.Visible = true;
            //}
            //else
            //{
            //    clearControls();
            //    pnlRepeaterBox.Visible = true;
            //    pnlMobileUserForm.Visible = false;
            //}
        }
        protected void clearControls()
        {
            //txtdate.Text = "";
            //txtname.Text = "";
            //txtid.Text = "";
            //hidDetailsFormMode.Value = "";
        }
        
        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#buttons'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);

        }
        void showAlert(string message, string divIdToShowMsg)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            //if (hidDetailsFormMode.Value != "")
            //{
            //    //Edit
            //    AddCompanyDetailsAdmin details = new AddCompanyDetailsAdmin();
            //    details.GetDetails(hidDetailsFormMode.Value);

            //    DataTable dt = details.ResultTable;
            //    if (dt.Rows.Count > 0)
            //    {
            //          UpdateCompanyDetails(hidDetailsFormMode.Value);
            //        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
            //    }
            //}
            //else
            //{
            //    //new user
            //     SaveCompanyDetails();
            //    //Utilities.Insertgdserver(g);
            //    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
            //}

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearControls();
            enableDisablePanels("");

        }

        void showHidePanelsByUserType()
        {
            if (hfs.Value.Split(',')[2].ToLower() == "sm")
            {
                pnlSalesMngrName.Visible = false;
                pnlUserTypeListing.Visible = true;
                pnlRepeaterBox.Visible = true;
                rptSalesPersonDtls.Visible = true;
                pnlResellerRptBox.Visible = false;
                rptResellerDtls.Visible = false;
            }
            else
            {
                pnlSalesMngrName.Visible = true;
                pnlUserTypeListing.Visible = false;
                pnlRepeaterBox.Visible = false;
                rptSalesPersonDtls.Visible = false;
                pnlResellerRptBox.Visible = true;
                rptResellerDtls.Visible = true;
            }
        }
        void bindSalesPersonRpt(DataTable salesPersonDtls)
        {
            rptSalesPersonDtls.DataSource = salesPersonDtls;
            rptSalesPersonDtls.DataBind();
        }
        void bindResellerRpt(DataTable resellerDtls)
        {
            rptResellerDtls.DataSource = resellerDtls;
            rptResellerDtls.DataBind();
        }
        void bindRptsByUserType(DataTable loginUserDtls)
        {
            GetTeamListByUserType objTeamList = new GetTeamListByUserType(hfs.Value.Split(',')[2].ToLower(), hfs.Value.Split(',')[1]);
            objTeamList.Process();
            DataSet dsTeamList = objTeamList.TeamList;
            if (hfs.Value.Split(',')[2].ToLower() == "sm")
            {
                if (dsTeamList != null && dsTeamList.Tables.Count != 0)
                {
                    if (dsTeamList.Tables[0] != null && dsTeamList.Tables[0].Rows.Count > 0)
                    {
                        bindSalesPersonRpt(dsTeamList.Tables[0]);
                        fillRepeaterHeader(RepeaterType.SalesPerson, true);
                    }
                    else
                    {
                        fillRepeaterHeader(RepeaterType.SalesPerson, false);
                    }
                    if (dsTeamList.Tables[1] != null && dsTeamList.Tables[1].Rows.Count > 0)
                    {
                        bindResellerRpt(dsTeamList.Tables[1]);
                        fillRepeaterHeader(RepeaterType.Reseller, true);
                    }
                    else
                    {
                        fillRepeaterHeader(RepeaterType.Reseller, false);
                    }

                }
            }
            else
            {
                if (dsTeamList != null && dsTeamList.Tables.Count != 0)
                {
                    if (dsTeamList.Tables[0] != null && dsTeamList.Tables[0].Rows.Count > 0)
                    {
                        lblSalesMngrName.Text = "<h1>"+(string)dsTeamList.Tables[0].Rows[0]["USER_NAME"]+"</h1>";
                    }
                    if (dsTeamList.Tables[1] != null && dsTeamList.Tables[1].Rows.Count > 0)
                    {
                        bindResellerRpt(dsTeamList.Tables[1]);
                        fillRepeaterHeader(RepeaterType.Reseller, false);
                    }
                    else
                    {
                        fillRepeaterHeader(RepeaterType.Reseller, false);
                    }
                }
            }
        }

        void fillRepeaterHeader(RepeaterType rptType, bool isDataAvailableToShow)
        {
            switch (rptType)
            {
                case RepeaterType.SalesPerson:
                    if (isDataAvailableToShow)
                        lblHeaderInfo.Text = "<h1>Sales person details </h1>";
                    else
                        lblHeaderInfo.Text = "<h1>No Sales Person added yet</h1>";
                    break;
                case RepeaterType.Reseller:
                    if (isDataAvailableToShow)
                        lblResellerRptHeader.Text = "<h1>Reseller Details</h1>";
                    else
                        lblResellerRptHeader.Text = "<h1>No reseller added yet</h1>";
                    break;
            }
        }
        protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlUserType.SelectedValue.ToLower() == "sales")
            {
                pnlRepeaterBox.Visible = true;
                rptSalesPersonDtls.Visible = true;
                pnlResellerRptBox.Visible = false;
                rptResellerDtls.Visible = false;
                //fillRepeaterHeader(RepeaterType.SalesPerson);
            }
            else
            {
                pnlRepeaterBox.Visible = false;
                rptSalesPersonDtls.Visible = false;
                pnlResellerRptBox.Visible = true;
                rptResellerDtls.Visible = true;
            }
        }







    }
}
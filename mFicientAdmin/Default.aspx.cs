﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace mFicientAdmin
{
    public partial class Login1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Request.QueryString.Get("d1")))
                    {
                        string[] companyIdAndSubAdminName = Utilities.DecryptString(Request.QueryString.Get("d2")).Split('@');

                        txtUserName.Text = companyIdAndSubAdminName[2];
                    }
                }
                if (Request.Cookies["mfCookie"] != null)
                {
                    HttpCookie getCookie = Request.Cookies.Get("mfCookie");
                    string userName = Convert.ToString(getCookie.Values["Name"]);
                    if (!String.IsNullOrEmpty(userName))
                    {
                        txtUserName.Text = userName;
                        chkRememberUserName.Checked = true;
                    }
                }
            }
            if (Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(hfs.Value) && !string.IsNullOrEmpty(hfp.Value))
                {
                    HttpContext.Current.Items.Add("hfs", hfs.Value);
                    Server.Transfer(hfp.Value, true);
                }
            }
        }
        public string Validate()
        {
            string strMessage = "";

            if (string.IsNullOrEmpty(txtUserName.Text))
            {
                strMessage += "Please enter user name." + "<br />";
            }
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                strMessage += "Please enter password." + "<br />";
            }

            return strMessage;
        }

        protected void UserLogin(string _userName, string _password)
        {
            try
            {
                Login userlogin = new Login(_userName, _password, "web", "", "", "");
                userlogin.LoginCheck();
                if (userlogin.StatusCode <= 0)
                {
                    if (chkRememberUserName.Checked)
                    {
                        rememberUserName();
                    }
                    hfs.Value = _userName + "," + userlogin.SessionId + "," + userlogin.Role;
                    string ScriptToRun = @"xferBack();";
                    hfp.Value = @"~/LoginUDashboard.aspx";
                    ScriptManager.RegisterClientScriptBlock(TransferUpdatePanel, typeof(UpdatePanel), TransferUpdatePanel.ClientID, ScriptToRun, true);
                }
                else
                {
                    txtPassword.Text = "";
                    hideWaitModal();
                    showAlert(userlogin.StatusDescription, "content");
                }
            }
            catch
            {
            }
        }

        protected void UserLoginAdmin(string _userName, string _password, string userId)
        {
            try
            {
                UserLogin userlogin = new UserLogin(_userName, _password, "web", "", "", "");
                userlogin.LoginCheck(userId);
                if (userlogin.StatusCode <= 0)
                {
                    if (chkRememberUserName.Checked)
                    {
                        rememberUserName();
                    }
                    hfs.Value = _userName + "," + userlogin.SessionId + ",A";
                    string ScriptToRun = @"xferBack();";
                    hfp.Value = @"~/AdminDashboard.aspx";

                    ScriptManager.RegisterClientScriptBlock(TransferUpdatePanel, typeof(UpdatePanel), TransferUpdatePanel.ClientID, ScriptToRun, true);
                }
                else
                {
                    txtPassword.Text = "";
                    hideWaitModal();
                    showAlert(userlogin.StatusDescription, "content");
                }
            }
            catch
            {
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string strMessage = Validate();
            if (String.IsNullOrEmpty(strMessage))
            {
                GetServerDetailsAdmin det = new GetServerDetailsAdmin();
                string Role;
                string userId = det.GetId(txtUserName.Text, txtPassword.Text, out Role);
                if (Role == "A")
                {
                    UserLoginAdmin(txtUserName.Text, txtPassword.Text, userId);
                }
                else
                {
                    UserLogin(txtUserName.Text, txtPassword.Text);
                }
            }
            else
            {
                txtPassword.Text = "";
                hideWaitModal();
                showAlert(strMessage, "content");
            }
        }



        void closeModalPopUp()
        {
            clearControlsForForgotPassword();
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "CloseModal", @"$('#divModalContainer').dialog('close')", true);
        }

        void showModalPopUp(string modalPopUpHeader)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "ShowPopUp", @"showModalPopUp('divModalContainer','" + modalPopUpHeader + "',400);", true);
        }

        void clearControlsForForgotPassword()
        {
            txtForgotPwdUserName.Text = "";
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                string strMessage = ValidateResetPassword();
                if (String.IsNullOrEmpty(strMessage))
                {
                    GetUserTypeWithDetailsByUsername objUserDetail = new
                    GetUserTypeWithDetailsByUsername(txtForgotPwdUserName.Text);

                    objUserDetail.Process();
                    if (objUserDetail.StatusCode != 0)
                    {
                        throw new MficientException(objUserDetail.StatusDescription);
                    }

                    string strUserId, strEmailId;
                    getUserIdAndEmailIdFromUserDtl(
                        objUserDetail,
                        out strUserId, out strEmailId);

                    ProcessResetPwdDtlsSave objRestPassword = new
                    ProcessResetPwdDtlsSave(objUserDetail.EUserType,
                    strUserId,
                    strEmailId,
                    txtForgotPwdUserName.Text,
                    this.Context);
                    objRestPassword.Process();

                    if (objRestPassword.StatusCode != 0)
                        throw new MficientException(objRestPassword.StatusDescription);
                    else
                    {
                        Utilities.showMessage("Password has been reset.Please check your email for the new password",
                            this.Page, "second script", DIALOG_TYPE.Info);
                        Utilities.closeModalPopUp("divModalContainer", this.Page);
                        hideWaitModal();
                    }

                }

            }
            catch (MficientException ex)
            {
                hideWaitModal();
                Utilities.showAlert(ex.Message,
                    "divResetPwdError", this.Page);
            }
            catch
            {
                hideWaitModal();
                Utilities.showAlert(MficientConstants.INTERNAL_ERROR,
                   "divResetPwdError", this.Page);
            }
        }

        void getUserIdAndEmailIdFromUserDtl(
            GetUserTypeWithDetailsByUsername usrDetlAfterProcess,
            out string userId, out string emailId)
        {
            userId = String.Empty;
            emailId = String.Empty;
            if (usrDetlAfterProcess.StatusCode != 0)
                throw new MficientException(usrDetlAfterProcess.StatusDescription);
            switch (usrDetlAfterProcess.EUserType)
            {
                case USER_TYPE.Reseller:
                    userId = usrDetlAfterProcess.Reseller.ResellerId;
                    emailId = usrDetlAfterProcess.Reseller.Email;
                    break;
                case USER_TYPE.SalesManager:
                case USER_TYPE.Account:
                case USER_TYPE.Support:
                case USER_TYPE.SalesPerson:
                    userId = usrDetlAfterProcess.User.LoginId;
                    emailId = usrDetlAfterProcess.User.Email;
                    break;
                case USER_TYPE.Admin:
                    userId = usrDetlAfterProcess.Admin.AdminId;
                    emailId = usrDetlAfterProcess.Admin.Email;
                    break;
            }

        }

        public string ValidateResetPassword()
        {
            string strMessage = "";
            if (string.IsNullOrEmpty(txtForgotPwdUserName.Text))
            {
                strMessage += "Please enter user name" + "<br />";
            }

            return strMessage;
        }

        void rememberUserName()
        {
            HttpCookie myCookie = new HttpCookie("mfCookie");
            Response.Cookies.Remove("mfCookie");
            Response.Cookies.Add(myCookie);
            if (chkRememberUserName.Checked)
            {
                myCookie.Values.Add("Name", txtUserName.Text.ToString());
                myCookie.Values.Add("Password", txtPassword.Text.ToString());
            }
            DateTime dtxpiry = DateTime.Now.AddDays(15);
            myCookie.Expires = dtxpiry;
        }

        void showAlert(string message, string divIdToShowAlert)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowAlert + "'" + ");", true);
        }

        void hideWaitModal()
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "HideWaitModal", "hideWaitModal();", true);
        }

        protected void txtUserName_TextChanged(object sender, EventArgs e)
        {
        }
    }
}



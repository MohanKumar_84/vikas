﻿<%@ Page Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true" CodeBehind="PublishedAppversion.aspx.cs" Inherits="mFicientAdmin.PublishedAppversion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
<style type="text/css">
.ui-resizable-handle
{
    
    background-image: none !important;;
}


</style>

    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                      <div>
                                            <asp:Label ID="lblInfo" runat="server" Text="<h1>Published App Version</h1>"></asp:Label></div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptpublishedappversion" runat="server">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>

                                                <th>
                                                OS
                                                </th>

                                                 <th >
                                                  MAJOR APP VERSION
                                                    </th>
                                                   
                                                   <th >
                                              MINOR APP VERSION
                                                   </th>
                                                   <th>
                                                   
                                                   </th>
                                                 

                                     </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                            <td>
                                            <asp:Label ID="lblreseller" runat="server" Text='<%#Eval("OS") %>'></asp:Label>
 
                                            </td>
                                                <td>
                                                    <asp:Label ID="lblid" runat="server" Text='<%#Eval("MajorAppVersionDetails") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("MinorAppVersionDetails") %>' ></asp:Label>
                                                </td>
                                                 <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CssClass="repeaterLink" OnClientClick="Editablerowdetail(this);return false;"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                             <td>
                                         <asp:Label ID="lblreseller" runat="server" Text='<%#Eval("OS") %>'></asp:Label>
 
                                            </td>
                                                <td>
                                                    <asp:Label ID="lblid" runat="server" Text='<%#Eval("MajorAppVersionDetails") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("MinorAppVersionDetails") %>'></asp:Label>
                                                </td>
                                              <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CssClass="repeaterLink" OnClientClick="Editablerowdetail(this);return false;"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                                
                            </asp:Panel>
                        </section>
                        
                       
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

               <div id="divPublishAppVersionUpdateInformation" style="display:none">
                    <div class="g12" style="width:100% !important">
                      
                                <div>
                                <span class="column1">
                                    <asp:Label ID="Label3" runat="server" Text="Major App Version" class="BasicDetailHead"></asp:Label>
                                </span><span class="column2">
                                    <asp:Label ID="Label6" runat="server" Text=":"></asp:Label>
                                </span><span class="column3">
                                <asp:TextBox ID="txtappmajorversion" title="Mobile app version major" runat="server" style="width:15%; margin-left:5px;"></asp:TextBox>
                                <asp:TextBox ID="txtappmajoversionminor" title="Mobile app version minor" runat="server" style="width:15%;margin-left:5px;"></asp:TextBox>
                                <asp:TextBox ID="txtappversionrev" title="Mobile app version rev" runat="server" style="width:15%;margin-left:5px;"></asp:TextBox>
                              
                                             </span>
                                <br />
                                <br />
                                <span class="column1">
                                    <asp:Label ID="Label10" runat="server" Text="Minor App Version" class="BasicDetailHead"></asp:Label>
                                </span><span class="column2">
                                    <asp:Label ID="Label11" runat="server" Text=":"></asp:Label>
                                </span><span class="column3">
                                  <asp:TextBox ID="txtappminimumversion" title="Minimum app version major" runat="server" style="width:15%;margin-left:5px;"></asp:TextBox>
                                <asp:TextBox ID="txtappminimumversionminor" title="Minimum app version minor" runat="server" style="width:15%;margin-left:5px;"></asp:TextBox>
                                <asp:TextBox ID="txtminimumversionrev" title="Minimum app version rev" runat="server" style="width:15%;margin-left:5px;"></asp:TextBox>
                                <asp:HiddenField ID="hidostype" runat="server" />
                                </span>
                                <br /><br />
                               
                                           <div class="SubProcborderDiv">
                                <div class="SubProcBtnDiv" align="center">
                                    <asp:Button ID="btnWFSave" runat="server" Text="Save" CssClass="InputStyle" OnClientClick="openmodelpopup(false);" OnClick="Updatebtn_Click" />
                                </div>
                            </div>
                            </div>
                           
                           
           
               
               </div>
               </div>
     
                
        </section>
    </div>
    
  <script type="text/javascript">
      function Editablerowdetail(val) {
          clearversiontextbox();
          var $row = $row = jQuery(val).closest('tr');
          var versionvalue = $row.find('td:eq(1)').text();
          versionvalue = $.trim(versionvalue);
          versionvalue = versionvalue.split('.');
          $('[id$=txtappmajorversion]').val(versionvalue[0]);
          $('[id$=txtappmajoversionminor]').val(versionvalue[1]);
          $('[id$=txtappversionrev]').val(versionvalue[2]);
          versionvalue = '';
        versionvalue=  $row.find('td:eq(2)').text();
        versionvalue = $.trim(versionvalue);
        versionvalue = versionvalue.split('.');
        $('[id$=txtappminimumversion]').val(versionvalue[0]);
        $('[id$=txtappminimumversionminor]').val(versionvalue[1]);
        $('[id$=txtminimumversionrev]').val(versionvalue[2]);
        versionvalue = '';
        versionvalue = $row.find('td:eq(0)').text();
        versionvalue = $.trim(versionvalue);
        $('[id$=hidostype]').val(versionvalue);
        versionvalue = '';
        openmodelpopup(true);

      //  showModalPopUp('divPublishAppVersionUpdateInformation', 'Edit version detail', '400', true);
     

      };


      function clearversiontextbox() {
          $('[id$=txtappmajorversion]').val('');
          $('[id$=txtappmajoversionminor]').val('');
          $('[id$=txtappversionrev]').val('');
          $('[id$=txtappminimumversion]').val('');
          $('[id$=txtappminimumversionminor]').val('');
          $('[id$=txtminimumversionrev]').val('');
          versionvalue = '';
      }


      function openmodelpopup(value) {
          showModalPopUp('divPublishAppVersionUpdateInformation', 'Edit version detail', '400', value);
      }
  






   
    
  
  
  </script>
  
  
  </asp:Content>




﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="TeamList1.aspx.cs" Inherits="mFicientAdmin.TeamList1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                     <section>
                     <div id="SMstatus" runat="server" >
                        <div class="searchRow g12" style="border: 1px solid #FAFAFA">
                            <div class="g5 radioButtonList">
                                <asp:RadioButtonList ID="radListSelection" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                    RepeatLayout="Table" OnSelectedIndexChanged="radListSelection_SelectedIndexChanged1">
                                    <asp:ListItem Text="<strong>Sales Executive</strong>" Value="1"  Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="<strong>Reseller</strong>" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                      </div>
                    </section>
                    <section class="clear" style="margin-bottom: 5px;">
                        <div id="div1">
                        </div>
                    </section>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>DETAILS</h1>"></asp:Label>
                                        </div>
                                       
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptSalesDetails" runat="server">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th >
                                                  SALES EXECUTIVE
                                                    </th>
                                                        <th>
                                                          SALES EXECUTIVE EMAIL
                                                    </th>
                                                    <th>
                                                   ALLOTED RESELLER
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td>
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("LOGIN_NAME") %>'></asp:Label>
                                              </td>
                                                <td>
                                                    <asp:Label ID="lblemail" runat="server" Text='<%#Eval("EMAIL") %>'></asp:Label>
                                                </td>
                                                 <td>
                                                    <asp:Label ID="lblreseller" runat="server" Text='<%#Eval("RESELLER_NAME") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                 <td>
                                                 
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("LOGIN_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblemail" runat="server" Text='<%#Eval("EMAIL") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblreseller" runat="server" Text='<%#Eval("RESELLER_NAME") %>'></asp:Label>
                                                </td>
                                              </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>

                                 <asp:Repeater ID="rptResellerDetails" runat="server">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th >
                                                    RESELLER
                                                    </th>
                                                        <th>
                                                        EMAIL
                                                    </th>
                                                   
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td>
                                                    <asp:Label ID="lname" runat="server" Text='<%#Eval("RESELLER_NAME") %>'></asp:Label>
                                              </td>
                                                <td>
                                                    <asp:Label ID="lemail" runat="server" Text='<%#Eval("EMAIL") %>'></asp:Label>
                                                </td>
                                                
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                 <td>
                                                 
                                                    <asp:Label ID="lname" runat="server" Text='<%#Eval("RESELLER_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lemail" runat="server" Text='<%#Eval("EMAIL") %>'></asp:Label>
                                                </td>
                                        </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>

                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                    <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                                </div>
                            </asp:Panel>
                        </section>
                       
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
    </div>

    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            //$("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            //s$("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>

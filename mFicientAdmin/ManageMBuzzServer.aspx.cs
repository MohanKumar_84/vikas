﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public partial class ManageMBuzzServer : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;


            if (!Page.IsPostBack)
            {
                /*if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else
                {
                    if (Page.PreviousPage != null)
                    {
                        string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                        prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                        ContentPlaceHolder cphPageCanvas = (ContentPlaceHolder)PreviousPage.Master.Master.FindControl("form").FindControl("MainCanvas").FindControl("PageCanvas");
                        hfs.Value = ((HiddenField)cphPageCanvas.FindControl("hfs")).Value;
                    }
                }*/
                bindReapeter();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "postBackScript", " $(\"select\").uniform();$(\"input\").uniform();$(\"#content\").find('div.widgets').wl_Widget();$('#aspnetForm').wl_Form();", true);//12/10/2012
            }
        }
        void bindReapeter()
        {
            AddMBuzzServerAdmin ad = new AddMBuzzServerAdmin();
            DataTable dt = ad.GetMBuzzServerList();
            if (dt.Rows.Count > 0)
            {
                lblHeaderInfo.Text = "<h1>MBuzz server details</h1>";
                rptUserDetails.Visible = true;
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
            }
            else if (dt.Rows.Count == 0)
            {
                lblHeaderInfo.Text = "<h1>There are no details of MBuzz server added yet</h1>";
                rptUserDetails.Visible = false;
            }
        }

        protected void bttnok_Click(object sender, EventArgs e)
        {
            AddMBuzzServerAdmin delete = new AddMBuzzServerAdmin();
            delete.DeleteServer(hdi.Value);
            Utilities.showMessage("MBuzzServer Details have been deleted successfully", this.Page, "second script", DIALOG_TYPE.Info);
            Utilities.closeModalPopUp("divdelete", this.Page);
            bindReapeter();
        }

        protected void bttncancel_Click(object sender, EventArgs e)
        {
            Utilities.closeModalPopUp("divdelete", this.Page);
        }

        protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Delete":
                    processDelete(e);
                    break;
            }
        }

        void processDelete(RepeaterCommandEventArgs e)
        {
            hidDetailsFormMode.Value = "Edit";
            Label server_id = (Label)e.Item.FindControl("lblserId");
            DeleteMBuzzServer(server_id.Text);
            hdi.Value = server_id.Text;
        }

        protected void DeleteMBuzzServer(string serverid)
        {
            AddMBuzzServerAdmin get = new AddMBuzzServerAdmin();
            string CompanyId = get.GetCompanyId(serverid);
           
            if (!String.IsNullOrEmpty(CompanyId))
            {
               // Utilities.showMessage(strDesc, this.Page, DIALOG_TYPE.Error);
                Utilities.showModalPopup("divNotDelete", this.Page, "<img src=images/dialog_error.png runat=server /><strong>Error</strong>", "300", true, false);
                NotDeletePnl.Update();
            }
            else
            {
                Utilities.showModalPopup("divdelete", this.Page, "<img src=images/dialog_info.png runat=server /><strong>Delete</strong>", "300", false, false);
                DeletePanel.Update();
            }
        }

        protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
        }

        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#buttons'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        void showAlert(string message, string divIdToShowMsg)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }

    }
}
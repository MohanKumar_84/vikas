﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using mFicientAdmin;

namespace mFicientAdmin.master
{
    public partial class Canvas : System.Web.UI.MasterPage
    {
        string pageFileName = "";

        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {

            HttpContext context = HttpContext.Current;

            try
            {
                hfsValue = context.Items["hfs"].ToString();
                if (string.IsNullOrEmpty(hfsValue)) return;

                strUserName = context.Items["hfs1"].ToString();
                strSessionId = context.Items["hfs2"].ToString();
                strRole = context.Items["hfs3"].ToString();

            }
            catch (Exception ex)
            {
                return;
            }

            Literal ltlUserType = (Literal)this.Master.FindControl("ltUserType");
            if (!IsPostBack)
            {
                if (HttpContext.Current.Items.Contains("hfs"))
                {
                    pageFileName = System.IO.Path.GetFileName(HttpContext.Current.Request.CurrentExecutionFilePath).Trim();
                }
                else
                {
                    System.Web.UI.Page previousPage = Page.PreviousPage;
                    if (previousPage == null)
                    {
                        Response.End();
                        return;
                    }
                    string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                    prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                    pageFileName = System.IO.Path.GetFileName(Request.CurrentExecutionFilePath).Trim();

                }

                if (string.IsNullOrEmpty(hfsValue))
                    Response.Redirect(@"Default.aspx");

                BindRoleMenu(pageFileName);

                string Role = strRole.ToUpper();
                if (Role == "R")
                {
                    ltlUserType.Text = "Reseller";
                }
                else if (Role == "S")
                {
                    ltlUserType.Text = "Sales Executive";
                }
                else if (Role == "SM")
                {
                    ltlUserType.Text = "Sales Manager";
                }
                else if (Role == "SUPP")
                {
                    ltlUserType.Text = "Support";
                }
                else if (Role == "ACC")
                {
                    ltlUserType.Text = "Accounts";
                }
                else
                {
                    ltlUserType.Text = "Admin";
                }

                HiddenField hidMenu = (HiddenField)this.Master.FindControl("hidMenu");
                hidMenu.Value = pageFileName.Split('.')[0];


                HiddenField hidField = (HiddenField)this.Master.FindControl("hfs");
                hidField.Value = hfsValue;
            }
        }
        protected void rptMenuCats_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            RepeaterItem menuItem = e.Item;
            if (menuItem.ItemType != ListItemType.Item && menuItem.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dtItemNode = (DataRowView)menuItem.DataItem;
            LinkButton lb = (LinkButton)menuItem.FindControl("lnk1");
            if (!String.IsNullOrEmpty(dtItemNode["PAGE"].ToString().Trim()))
            {
                lb.PostBackUrl = "~/" + dtItemNode["PAGE"].ToString();
                Repeater rptItem = (Repeater)menuItem.FindControl("rptMenuItem");
                rptItem.Visible = false;
                if (pageFileName.ToLower() == dtItemNode["PAGE"].ToString().ToLower())
                {
                    lb.CssClass = "active";
                    if (String.IsNullOrEmpty(dtItemNode["HEADER"].ToString().Trim()))
                    {
                        ltHead.Visible = false;
                        ltlHelp.Visible = false;
                    }
                    else
                    {
                        ltHead.Text = dtItemNode["HEADER"].ToString();

                        if (String.IsNullOrEmpty(dtItemNode["HELP_TEXT"].ToString().Trim()))
                            ltlHelp.Visible = false;
                        else
                            ltlHelp.Text = dtItemNode["HELP_TEXT"].ToString();
                    }
                }
            }
            else
            {
                lb.OnClientClick = "return false;";
                DataSet ds = Utilities.GetSubMenuList(pageFileName, dtItemNode["id"].ToString());
                if (ds != null)
                {
                    Repeater rptItem = (Repeater)menuItem.FindControl("rptMenuItem");
                    rptItem.DataSource = ds.Tables[0];
                    rptItem.DataBind();
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        lb.CssClass = "active";
                    }
                }
            }
        }

        protected void rptMenuItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            RepeaterItem menuItem = e.Item;
            if (menuItem.ItemType != ListItemType.Item && menuItem.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dtItemNode = (DataRowView)menuItem.DataItem;
            if (!String.IsNullOrEmpty(dtItemNode["PAGE"].ToString().Trim()))
            {
                LinkButton lb = (LinkButton)menuItem.FindControl("lnk1");
                lb.PostBackUrl = "~/" + dtItemNode["PAGE"].ToString();
                if (pageFileName.ToLower() == dtItemNode["PAGE"].ToString().ToLower())
                {
                    lb.CssClass = "active";
                    if (String.IsNullOrEmpty(dtItemNode["HEADER"].ToString().Trim()))
                    {
                        ltHead.Visible = false;
                        ltlHelp.Visible = false;
                    }
                    else
                    {
                        ltHead.Text = dtItemNode["HEADER"].ToString();

                        if (String.IsNullOrEmpty(dtItemNode["HELP_TEXT"].ToString().Trim()))
                            ltlHelp.Visible = false;
                        else
                            ltlHelp.Text = dtItemNode["HELP_TEXT"].ToString();
                    }
                }
            }
        }

        protected void BindRoleMenu(string _pageFileName)
        {
            string strRoles = strRole.ToUpper();
            DataTable dt = Utilities.GetMainMenuList("'" + strRoles.Replace(",", "','") + "'");
            if (dt != null)
            {
                //dt.Rows.Add();
                //int rowIndex = dt.Rows.Count - 1;
                ////id,Link,PAGE,CLASS,HEADER,HELP_TEXT
                //dt.Rows[rowIndex]["id"] = "99";
                //dt.Rows[rowIndex]["Link"] = "Activity Log";
                //dt.Rows[rowIndex]["PAGE"] = "actyivityLog.aspx";
                //dt.Rows[rowIndex]["CLASS"] = "i_note_book";
                //dt.Rows[rowIndex]["HEADER"] = "";
                //dt.Rows[rowIndex]["HELP_TEXT"] = "";
                rptMenuCats.DataSource = dt;
                rptMenuCats.DataBind();
            }

        }

        protected void lnkLogOut_Click(object sender, EventArgs e)
        {
            UserLogOutAdmin objUserLogout = new UserLogOutAdmin();
            objUserLogout.Process(strUserName, strSessionId);
            hfs.Value = "";
            Response.Redirect("~/Default.aspx");
        }
    }
}
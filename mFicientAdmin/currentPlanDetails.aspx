﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.master" AutoEventWireup="true"
    CodeBehind="currentPlanDetails.aspx.cs" Inherits="mFicientAdmin.currentPlanDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="widget" id="CurrentPlanDetailsDiv" runat="server" style="float: left;
                width: 100%" visible="true">
                <h3 class="handle">
                    <asp:LinkButton ID="LinkButton1" runat="server">Current Plan Details</asp:LinkButton>
                </h3>
                <div>
                    <span class="BasicDetailHead">Plan Name</span>:<span class="BasicDetail"><asp:Label
                        ID="lblCurrentPlanName" runat="server"></asp:Label></span><br />
                    <br />
                    <span class="BasicDetailHead">Max Workflow</span>:<span class="BasicDetail"><asp:Label
                        ID="lblWorkflow" runat="server"></asp:Label></span><br />
                    <br />
                    <span class="BasicDetailHead">Per User/Per Month</span>:<span class="BasicDetail">
                        <asp:Label ID="lblChargePerMonth" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblChargeType" runat="server"></asp:Label>
                    </span>
                    <br />
                    <br />
                    <span class="BasicDetailHead">Max Users</span>:<span class="BasicDetail"><asp:Label
                        ID="lblMaxUsers" runat="server"></asp:Label></span><br />
                    <br />
                    <span class="BasicDetailHead">Valid Till</span>:<span class="BasicDetail"><asp:Label
                        ID="lblValidityDate" runat="server"></asp:Label></span><br />
                    <br />
                    <span class="BasicDetail">
                        <asp:Button ID="btnAddUser" runat="server" Text="Add User" OnClick="btnAddUser_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnUpgradeUser" runat="server" Text="Upgrade User" OnClick="btnUpgradeUser_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnRenewPlan" runat="server" Text="Renew Plan" 
                        PostBackUrl="~/planPurchase.aspx" onclick="btnRenewPlan_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnNewPlan" runat="server" PostBackUrl="~/subscriptionList.aspx" Text="Buy Now" />
                    </span>
                    <br />
                    <br />
                </div>
            </div>
            <div class="widget" id="AddUserDiv" runat="server" style="float: left; width: 100%"
                visible="false">
                <h3 class="handle">
                    <asp:LinkButton ID="LinkButton3" runat="server">Add User</asp:LinkButton>
                    <span style="float: right; position: relative; top: 10px;">
                        <asp:LinkButton ID="lnkBackFromAddUser" runat="server" Text="back" OnClick="lnkBack_Click"></asp:LinkButton></span>
                </h3>
                <div>
                    <span class="BasicDetailHead">Add Users</span>:<span class="BasicDetail"><asp:TextBox
                        ID="txtAddUsers" runat="server" Width="60px" AutoPostBack="True" OnTextChanged="txtAddUsers_TextChanged"></asp:TextBox></span><br />
                    <br />
                    <span class="BasicDetailHead">Months</span>:<span class="BasicDetail"><asp:Label
                        ID="lblValidMonths" runat="server"></asp:Label></span><br />
                    <br />
                    <span class="BasicDetailHead">Discount</span>:<span class="BasicDetail"><asp:Label
                        ID="lblAllowableDiscount" runat="server"></asp:Label>&nbsp;
                        <asp:Label ID="Label5" runat="server" Text="%"></asp:Label>
                    </span>
                    <br />
                    <br />
                    <span class="BasicDetailHead">Price</span>:<span class="BasicDetail"><asp:Label ID="lblPrice"
                        runat="server"></asp:Label></span><br />
                    <br />
                    <span class="BasicDetailHead">Discount Amount</span>:<span class="BasicDetail"><asp:Label
                        ID="lblDiscountAmount" runat="server"></asp:Label></span><br />
                    <br />
                    <span class="BasicDetailHead">Payable Amount</span>:<span class="BasicDetail"><asp:Label
                        ID="lblPayableAmount" runat="server"></asp:Label></span><br />
                    <br />
                    <asp:Button ID="btnAdd" runat="server" Text="Add User" OnClick="btnAdd_Click" 
                    OnClientClick="return confirm('Are you sure you want to add the plan');" />
                    </span><br />
                </div>
            </div>
            <div class="widget" id="UpgradeUserDiv" runat="server" style="float: left; width: 100%"
                visible="false">
                <h3 class="handle">
                    <asp:LinkButton ID="LinkButton2" runat="server">Upgrade User</asp:LinkButton>
                    <span style="float: right; position: relative; top: 10px;">
                        <asp:LinkButton ID="lnkBack" runat="server" Text="back" OnClick="lnkBack_Click"></asp:LinkButton></span>
                </h3>
                <div>
                    <span class="BasicDetailHead">Plan</span>:<span class="BasicDetail" style="position: relative;
                        top: 10px;"><asp:DropDownList runat="server" ID="ddlPlans" CssClass="Fld" Width="250px"
                            AutoPostBack="True" OnSelectedIndexChanged="ddlPlans_SelectedIndexChanged" />
                    </span>
                    <br />
                    <br />
                    <span class="BasicDetailHead">Max Workflow</span>:<span class="BasicDetail"><asp:Label
                        ID="lblMaxWorkflow" runat="server"></asp:Label></span><br />
                    <br />
                    <span class="BasicDetailHead">Valid Months</span>:<span class="BasicDetail"><asp:Label
                        ID="lblMonths" runat="server"></asp:Label></span><br />
                    <br />
                    <span class="BasicDetailHead">Payable Amount</span>:<span class="BasicDetail"><asp:Label
                        ID="lblAmount" runat="server"></asp:Label></span><br />
                    <br />
                    <span>
                        <asp:Button ID="btnUpgrade" runat="server" Text="Upgrade" OnClick="btnUpgrade_Click"
                            OnClientClick="return confirm('Are you sure you want to upgrade the plan');" />
                    </span>
                    <br />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hfs" runat="server" />
    <asp:HiddenField ID="hfdPlanCode" runat="server"></asp:HiddenField>


    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {

            showWaitModal();
        }
        function prm_endRequest() {

            hideWaitModal();
        }
    </script>
</asp:Content>

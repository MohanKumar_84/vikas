﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="PlanApprovalBySales.aspx.cs" Inherits="mFicientAdmin.PlanApprovalBySales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>PLAN ORDER DETAILS</h1>"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptPlanOrderDetails" runat="server" OnItemCommand="rptPlanOrderDetails_ItemCommand"
                                    OnItemDataBound="rptPlanOrderDetails_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th style="display: none;">
                                                        CompanyId
                                                    </th>
                                                    <th>
                                                        Company
                                                    </th>
                                                    <th>
                                                        Current Plan
                                                    </th>
                                                    <th>
                                                        Ordered Plan
                                                    </th>
                                                    <th>
                                                        Order Type
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td style="display: none;">
                                                    <asp:Label ID="lblCurrPlanId" runat="server" Text='<%#Eval("CURRENT_PLAN_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblCompanyId" runat="server" Text='<%#Eval("COMPANY_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblRequestType" runat="server" Text='<%#Eval("REQUEST_TYPE") %>'></asp:Label>
                                                    <asp:Label ID="lblRequestedPlanCode" runat="server" Text='<%#Eval("PLAN_CODE") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCurrentPlan" runat="server" Text='<%#Eval("CURRENT_PLAN_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPlanNameRequested" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblOrderType" runat="server" Text='<%# Eval("ORDER") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkProcess" runat="server" Text="Approve" CssClass="repeaterLink"
                                                        CommandName="Approve" OnClientClick="return confirm('Are you sure you want to approve the request.');"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkDenyRequest" runat="server" Text="Deny" CssClass="repeaterLink" style="margin-left:2px;"
                                                        CommandName="Deny" OnClientClick="return confirm('Are you sure,you want to deny the request');"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                 <td style="display: none;">
                                                    <asp:Label ID="lblCurrPlanId" runat="server" Text='<%#Eval("CURRENT_PLAN_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblCompanyId" runat="server" Text='<%#Eval("COMPANY_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblRequestType" runat="server" Text='<%#Eval("REQUEST_TYPE") %>'></asp:Label>
                                                    <asp:Label ID="lblRequestedPlanCode" runat="server" Text='<%#Eval("PLAN_CODE") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCurrentPlan" runat="server" Text='<%#Eval("CURRENT_PLAN_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPlanNameRequested" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblOrderType" runat="server" Text='<%# Eval("ORDER") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkProcess" runat="server" Text="Approve" CssClass="repeaterLink"
                                                        CommandName="Approve" OnClientClick="return confirm('Are you sure you want to approve the request.');"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkDenyRequest" runat="server" Text="Deny" CssClass="repeaterLink" style="margin-left:2px;"
                                                        CommandName="Deny" OnClientClick="return confirm('Are you sure,you want to deny the request');"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                    <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                                </div>
                            </asp:Panel>
                        </section>
                        <section>
                            <div class="modalPopUpDetails">
                                <div id="divPlanDetailsModal" style="display: none">
                                    <asp:UpdatePanel runat="server" ID="upPlanDetails" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="ProcImgUpd">
                                                <div id="DivUserDetails" style="width: 100%;">
                                                    <div style="float: left; width: 75%;">
                                                        <asp:Panel ID="pnlMobileUserForm" Visible="false" runat="server">
                                                            <div class="g11">
                                                                <fieldset>
                                                                    <label>
                                                                        Details
                                                                    </label>
                                                                    <section>
                                                                        <label for="lname">
                                                                            Company Name</label>
                                                                        <div>
                                                                            <asp:Label ID="lname" runat="server"></asp:Label>
                                                                        </div>
                                                                    </section>
                                                                    <section>
                                                                        <label for="lplan">
                                                                            Current Plan</label>
                                                                        <div>
                                                                            <asp:Label ID="lplan" runat="server"></asp:Label>
                                                                        </div>
                                                                    </section>
                                                                    <section>
                                                                        <label for="lusers">
                                                                            Number Of Users</label>
                                                                        <div>
                                                                            <asp:Label ID="lusers" runat="server"></asp:Label>
                                                                        </div>
                                                                    </section>
                                                                    <section>
                                                                        <label for="lwrflow">
                                                                            Maximum Workflow</label>
                                                                        <div>
                                                                            <asp:Label ID="lwrflow" runat="server"></asp:Label>
                                                                        </div>
                                                                    </section>
                                                                    <section>
                                                                        <label for="ldate">
                                                                            Expiry Date</label>
                                                                        <div>
                                                                            <asp:Label ID="ldate" runat="server"></asp:Label>
                                                                        </div>
                                                                    </section>
                                                                    <section>
                                                                        <label for="loname">
                                                                            Order Name</label>
                                                                        <div>
                                                                            <asp:Label ID="loname" runat="server"></asp:Label>
                                                                        </div>
                                                                    </section>
                                                                    <section>
                                                                        <label for="lcode">
                                                                            Order Code</label>
                                                                        <div>
                                                                            <asp:Label ID="lcode" runat="server"></asp:Label>
                                                                        </div>
                                                                    </section>
                                                                    <section id="buttons">
                                                                        <asp:Button ID="btnok" runat="server" Text="Ok" CssClass="aspButton" OnClick="btnok_Click" />
                                                                    </section>
                                                                </fieldset>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </section>
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
        
    </div>
    <div class="modalPopUpDetails">
        <div id="divAddUserModal" style="display: none">
            <asp:UpdatePanel runat="server" ID="upAddUserModal" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="ProcImgUpd">
                        <div class="widget" id="AddUserDiv" runat="server" style="float: left; width: 100%"
                            visible="false">
                            <h3 class="handle">
                                <%--<asp:LinkButton ID="LinkButton3" runat="server">Add User</asp:LinkButton>
                    <span style="float: right; position: relative; top: 10px;">
                        <asp:LinkButton ID="lnkBackFromAddUser" runat="server" Text="back"></asp:LinkButton></span>--%>
                            </h3>
                            <div>
                                <span class="BasicDetailHead">Add Users</span>:<span class="BasicDetail"><asp:TextBox
                                    ID="txtAddUsers" runat="server" Width="60px" AutoPostBack="True"></asp:TextBox></span><br />
                                <br />
                                <span class="BasicDetailHead">Months</span>:<span class="BasicDetail"><asp:Label
                                    ID="lblValidMonths" runat="server"></asp:Label></span><br />
                                <br />
                                <span class="BasicDetailHead">Discount</span>:<span class="BasicDetail"><asp:Label
                                    ID="lblAllowableDiscount" runat="server"></asp:Label>&nbsp;
                                    <asp:Label ID="Label5" runat="server" Text="%"></asp:Label>
                                </span>
                                <br />
                                <br />
                                <span class="BasicDetailHead">Price</span>:<span class="BasicDetail"><asp:Label ID="lblPrice"
                                    runat="server"></asp:Label></span><br />
                                <br />
                                <span class="BasicDetailHead">Discount Amount</span>:<span class="BasicDetail"><asp:Label
                                    ID="lblDiscountAmount" runat="server"></asp:Label></span><br />
                                <br />
                                <span class="BasicDetailHead">Payable Amount</span>:<span class="BasicDetail"><asp:Label
                                    ID="lblPayableAmount" runat="server"></asp:Label></span><br />
                                <br />
                                <asp:Button ID="btnAdd" runat="server" Text="Add User" OnClientClick="return confirm('Are you sure you want to add the plan');" />
                                </span><br />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div id="divDenyRequestBox" style="display: none;" class="modalPopUpDetails">
        <div class="container">
            <asp:UpdatePanel runat="server" ID="updDenyRequest" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divDenyRequest">
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="lblSelectRemarks" runat="server" Text="Remarks"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:DropDownList ID="ddlRemarks" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="action center">
                            <asp:Button ID="btnSaveDenyRemarks" runat="server" Text="Save" OnClick="btnSaveDenyRemarks_Click" />
                            <asp:Button ID="btnCancelDenyRemarks" runat="server" Text="Cancel" OnClick="btnCancelDenyRemarks_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>

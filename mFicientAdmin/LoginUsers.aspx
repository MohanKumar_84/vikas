﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginUsers.aspx.cs" Inherits="mFicientAdmin.LoginUsers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title>mficient | Login</title>
    <meta name="description" content="" />
    <meta name="author" content="revaxarts.com" />
    <!-- Apple iOS and Android stuff -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="apple-touch-icon-precomposed" href="img/icon.png" />
    <link rel="apple-touch-startup-image" href="img/startup.png" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1" />
    <!-- Google Font and style definitions -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold" />
    <link rel="stylesheet" href="css/CanvasStyle.css" id="Link2" />
    <link rel="stylesheet" href="css/light/CanvasTheme.css" id="Link1" />
    <link rel="stylesheet" href="css/style.css" />
    <!-- include the skins (change to dark if you like) -->
    <link rel="stylesheet" href="css/light/theme.css" id="themestyle" />
    <!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
    <!-- Use Google CDN for jQuery and jQuery UI -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
    <!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
    <!-- some basic functions -->
    <script src="js/functions.js" type="text/javascript"></script>
    <!-- all Third Party Plugins -->
    <script src="js/plugins.js" type="text/javascript"></script>
    <!-- Whitelabel Plugins -->
    <script src="js/wl_Alert.js" type="text/javascript"></script>
    <script src="js/wl_Dialog.js" type="text/javascript"></script>
    <script src="js/wl_Form.js" type="text/javascript"></script>
    <!-- configuration to overwrite settings -->
    <script src="js/config.js" type="text/javascript"></script>
    <!-- the script which handles all the access to plugins etc... -->
    <script src="js/login.js" type="text/javascript"></script>
    <script src="Scripts/Scripts.js" type="text/javascript"></script>
</head>
<html>
<body id="login">
    <form id="loginform" runat="server" style="border: 0px;">
    <script type="text/javascript" src='<%= Page.ResolveClientUrl("~/scripts/main.js") %>'></script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <header>
                <div id="logo">
                    <a href="Default.aspx">mficient</a>
                </div>
            </header>
            <section id="content">
                <div id="formDiv">
                    <fieldset>
                       
                        <section>
                            <label for="username">
                                Username</label>
                                <div class="rememberParticularField">
                                <asp:CheckBox ID="chkRememberUserName" runat="server" Text="remember"  />
                                </div>
                            <div>
                                <%--<input type="text" id="txtUserName" name="username" runat="server" />--%>
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox></div>
                        </section>
                        <section>
                            <label for="password">
                                Password
                            </label>
                            <div>
                                <%--<input type="password" id="txtPassword" name="password" runat="server" />--%>
                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" 
                                    ></asp:TextBox>
                            </div>
                           <div style="clear: both">
                            </div>
                        </section>
                        <section>
                            <asp:LinkButton ID="lnkForgotPassword" runat="server" OnClick="lnkForgotPassword_Click"
                                Style="font-size: 10px; height: 13px; float:left; margin-left:13px">lost password</asp:LinkButton>
                            <div>
                                <asp:Button ID="btnSubmit" runat="server" Text="login" CssClass="fr aspButton" OnClick="btnSubmit_Click" />
                            </div>
                        </section>
                </div>
            </fieldset>
            <asp:HiddenField ID="hidQueryStrFromEmail" runat="server" />
            </div> 
            </section>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="TransferUpdatePanel" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:HiddenField ID="hfs" runat="server" />
            <asp:HiddenField ID="hfp" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <section>
        <div id="divModalContainer">
            <div id="divForgotPwdContainer" style="display: block">
                <asp:UpdatePanel ID="updModalContainer" runat="server">
                    <ContentTemplate>
                        <div id="modalPopUpContent">
                            <fieldset>
                                
                                <section>
                                    <label for="txtForgotPwdUserName">
                                        User Name</label>
                                    <div style="height: 2px;">
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtForgotPwdUserName" runat="server" CssClass="required"></asp:TextBox>
                                    </div>
                                </section>
                                <div style="height: 10px;">
                                </div>
                                <section>
                                    <div>
                                        <asp:Button ID="btnReset" runat="server" Text="save" CssClass="aspButton" OnClick="btnReset_Click" />&nbsp;
                                        <asp:Button ID="btnCancel" runat="server" Text="cancel" CssClass="aspButton" OnClick="btnCancel_Click" />
                                    </div>
                                </section>
                            </fieldset>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </section>
    </form>
    <footer>Footer</footer>
</body>
<script type="text/javascript">
    Sys.Application.add_init(application_init);
    var idOfPostBackElement = "";
    function application_init() {
        //Sys.Debug.trace("Application.Init");
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_initializeRequest);
        prm.add_beginRequest(prm_beginRequest);
        prm.add_endRequest(prm_endRequest);
    }
    function prm_initializeRequest() {
        //$("input").uniform();
    }
    function prm_beginRequest(sender, args) {
        //$("input").uniform();
        idOfPostBackElement = args.get_postBackElement().id;
        //container.className = 'clear';

    }
    function prm_endRequest() {
        if (idOfPostBackElement != 'btnReset' && idOfPostBackElement != 'btnCancel') {
            $("select").uniform(); $("input").uniform();
        }
        idOfPostBackElement = "";
    }
    function txtCompanyId_onclick() {

    }

</script>
</html> 
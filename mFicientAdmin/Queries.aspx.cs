﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public partial class Queries : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
       protected void Page_Load(object sender, EventArgs e)
       {
           HttpContext context = HttpContext.Current;
           if (Page.IsPostBack)
           {
               MasterPage mainMaster = Page.Master.Master;
               hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
           }
           else
           {
               System.Web.UI.Page previousPage = Page.PreviousPage;
               if (previousPage == null)
               {
                   Response.End();
                   return;
               }

               System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
               if (previousPageForm == null) return;

               hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
           }

           if (string.IsNullOrEmpty(hfsValue)) return;

           context.Items["hfs"] = hfsValue;

           string[] hfsParts = hfsValue.Split(',');

           strUserName = hfsParts[0];
           strSessionId = hfsParts[1];
           strRole = hfsParts[2];

           context.Items["hfs1"] = strUserName;
           context.Items["hfs2"] = strSessionId;
           context.Items["hfs3"] = strRole;


           if (!Page.IsPostBack)
           {
               /*if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }

                else if (Page.PreviousPage != null)
                    {
                        string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                        prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                        ContentPlaceHolder cphPageCanvas = (ContentPlaceHolder)PreviousPage.Master.Master.FindControl("form").FindControl("MainCanvas").FindControl("PageCanvas");
                        hfs.Value = ((HiddenField)cphPageCanvas.FindControl("hfs")).Value;
                    }
                   else
                   {
                       ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "testing", " $(\"select\").uniform();$(\"input\").uniform();", true);
                   }*/
                   BindopenqueryRepeater();
                   Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID, "$(\"input\").uniform();", true);
               }
           }

       void changeColorOfSelectedLinkBtn(LinkButton linkSelected)
       {
           linkSelected.Attributes.Add("class", "linkSelected");
           if (linkSelected.ID == lnk1.ID)
           {
               lnk2.Attributes.Remove("class");
           }
           else if (linkSelected.ID == lnk2.ID)
           {
               lnk1.Attributes.Remove("class");
           }
       }

        protected void lnk1_Click(object sender, EventArgs e)
        {
            changeColorOfSelectedLinkBtn((LinkButton)sender);
            BindopenqueryRepeater();
        }

        protected void lnk2_Click(object sender, EventArgs e)
        {
            changeColorOfSelectedLinkBtn((LinkButton)sender);
            BindClosedRepeater();
        }

        protected void BindClosedRepeater()
        {
            QueriesAdmin queries = new QueriesAdmin();
            DataTable dt = queries.GetClosedQueries();
            if (dt.Rows.Count > 0)
            {
                lblHeaderInfo.Text = "<h1>closed queries</h1>";
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
                rptUserDetails.Visible = true;
            }
            else if (dt.Rows.Count == 0)
            {
                lblHeaderInfo.Text = "<h1>no closed queries added yet</h1>";
                rptUserDetails.Visible = false;
            }
        }
        protected void BindopenqueryRepeater()
        {
            QueriesAdmin queries = new QueriesAdmin();
            DataTable dt = queries.GetOpenQueries();
            if (dt.Rows.Count > 0)
            {
                lblHeaderInfo.Text = "<h1>open queries</h1>";
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
                rptUserDetails.Visible = true;
            }
            else if (dt.Rows.Count == 0)
            {
                lblHeaderInfo.Text = "<h1>no open queries added yet</h1>";
                rptUserDetails.Visible = false;
            }
        }

        protected void BindCommentsRepeater()
        {
            QueriesAdmin queries = new QueriesAdmin();
            string userId= queries.UserId(strUserName);
            string TokenNumber = queries.GetTokenNo(userId);
            DataTable dt = queries.GetCommentsList(TokenNumber);
            if (dt.Rows.Count > 0)
            {
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
        }

        protected void lnkok_Click(object sender, EventArgs e)
        {
            BindCommentsRepeater();
            Utilities.showModalPopup("divUserDtlsModal", this.Page, "<strong>Query Details</strong>", "400", true,false);
            enabledisablepanels("Info");
            upUserDtlsModal.Update();
        }

        protected void bttncomment_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtcomment.Text))
            {
                QueriesAdmin queries = new QueriesAdmin();
                string UserId = queries.UserId(strUserName);
                string TokenNumber = queries.GetTokenNo(UserId);
                queries.AddQuery(TokenNumber, Utilities.GetMd5Hash(txtcomment.Text + DateTime.Now.Ticks.ToString()), UserId, 1, txtcomment.Text);
                txtcomment.Text = string.Empty;
                Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
                Utilities.showMessage("User comments have been added successfully", this.Page, "second script", DIALOG_TYPE.Info);
            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString();
                Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
            }
        }
        protected void lnkBack_Click(object sender, EventArgs e)
        {
            pnlRepeaterBox.Visible = true;
            Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
        }
        protected void enabledisablepanels(string mode)
        {
            if (mode == "Info")
            {
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = true;
            }
            else if (mode == "Add")
            {
                pnlRepeaterBox.Visible = false;
            }
            else
            {
                pnlRepeaterBox.Visible = true;
                pnlMobileUserForm.Visible = false;
            }
        }

        protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item;
            Label token_number = (Label)item.FindControl("lblnumber");
            if (e.CommandName == "Info")
            {
                FillQueryDetails(token_number.Text);
            }
        }
        protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Info":
                    Info(e);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing2", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                    break;
            }
        }
        protected void FillQueryDetails(string TokenNumber)
        {
            QueriesAdmin querydet = new QueriesAdmin();
            querydet.QueryDetails(TokenNumber);
            DataTable dt = querydet.ResultTable;
            if (dt.Rows.Count > 0)
            {
                lname.Text = strUserName;
                lsubject.Text = Convert.ToString(dt.Rows[0]["QUERY_SUBJECT"]);
                lquery.Text = Convert.ToString(dt.Rows[0]["QUERY"]);
            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }

        void Info(RepeaterCommandEventArgs e)
        {
            Label TokenNumber = (Label)e.Item.FindControl("lblnumber");
            FillQueryDetails(TokenNumber.Text);

        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class CompanyEmailAdmin
    {
        string RId = string.Empty;
        public string GetResellerId(string loginname)
        {
            try
            {
                SqlCommand objSqlCommand = new SqlCommand(@"select RESELLER_ID from ADMIN_TBL_RESELLER_DETAIL where LOGIN_NAME=@loginname");
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@loginname", loginname);
                DataSet objDataset = Utilities.SelectDataFromSQlCommand(objSqlCommand);

                if (objDataset.Tables[0].Rows.Count > 0)//We should get a reseller id.
                {
                    RId = objDataset.Tables[0].Rows[0]["RESELLER_ID"].ToString();
                }
                else
                {
                    throw new MficientException("Reseller record not found.Please try again.");
                }
            }
            catch (MficientException ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;
            }
            catch (Exception e)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return RId;
        }

        public string ResellerId
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public int StatusCode
        {
            get;
            private set;
        }
    }
}
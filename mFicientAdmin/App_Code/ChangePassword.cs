﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace mFicientAdmin
{
    public class ChangePasswordAdmin
    {
        int intStatusCode = -1000; //for error
          //string strPassword;
        public void ChangePass(string OldAccessCode, string NewAccessCode, string Uname)
        {
            try
            {
                // to check if the value of old password entered is correct
                if (checkadmin(Utilities.GetMd5Hash(OldAccessCode)) == false)
                {
                    this.StatusDescription = "Old password entered is not correct ,hence unable to change the password";
                    this.intStatusCode = 100;
                }
                else
                {
                    SqlCommand objSqlCommand = new SqlCommand("UPDATE ADMIN_TBL_ADMIN_LOGIN SET ACCESS_CODE=@NEWACCESSCODE WHERE ACCESS_CODE=@ACCESS_CODE AND ADMIN_NAME=@adminname;");
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@ACCESS_CODE", Utilities.GetMd5Hash(OldAccessCode));
                    objSqlCommand.Parameters.AddWithValue("@adminname", Uname);
                    objSqlCommand.Parameters.AddWithValue("@NEWACCESSCODE", Utilities.GetMd5Hash(NewAccessCode));
                    int intExecuteRecordsCount = Utilities.ExecuteNonQueryRecord(objSqlCommand);
                    if (intExecuteRecordsCount > 0)
                    {
                        this.intStatusCode = 0;
                    }
                    else
                    {
                        this.intStatusCode = (int)USERLOGIN_ERRORS.PASSWORD_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    this.intStatusCode = -1000;
            }
        }

        private bool checkadmin(string accesscode)
        {
            try
            {
                SqlCommand objSqlCmd = new SqlCommand(@"SELECT * FROM ADMIN_TBL_ADMIN_LOGIN WHERE ACCESS_CODE=@accesscode");
                objSqlCmd.Parameters.AddWithValue("@accesscode", accesscode);
                //objSqlCmd.Parameters.AddWithValue("@planame", planname);
                DataSet objds = Utilities.SelectDataFromSQlCommand(objSqlCmd);
                if (objds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
            }
            return false;
          }

        private bool checkreseller(string accesscode)
        {
            try
            {
                SqlCommand objSqlCmd = new SqlCommand(@"SELECT * FROM ADMIN_TBL_RESELLER_DETAIL WHERE PASSWORD=@accesscode");
                objSqlCmd.Parameters.AddWithValue("@accesscode", accesscode);
                //objSqlCmd.Parameters.AddWithValue("@planame", planname);
                DataSet objds = Utilities.SelectDataFromSQlCommand(objSqlCmd);
                if (objds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
            }
             return false;
         }

        private bool checkuser(string accesscode)
        {
            try
            {
                SqlCommand objSqlCmd = new SqlCommand(@"SELECT * FROM ADMIN_TBL_LOGIN_USER_DETAIL WHERE PASSWORD=@accesscode");
                objSqlCmd.Parameters.AddWithValue("@accesscode", accesscode);
                //objSqlCmd.Parameters.AddWithValue("@planame", planname);
                DataSet objds = Utilities.SelectDataFromSQlCommand(objSqlCmd);
                if (objds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
            }
            return false;
        }
        public void ChangeResellerPass(string OldAccessCode, string NewAccessCode, string Uname)
        {
            try
            {
                if (checkreseller(Utilities.GetMd5Hash(OldAccessCode)) == false)
                {
                    StatusDescription = " Old password entered is not correct ,hence unable to change the password";
                    intStatusCode = 100;
                }
                else
                {
                    SqlCommand objSqlCommand = new SqlCommand("UPDATE ADMIN_TBL_RESELLER_DETAIL set PASSWORD=@NEWACCESSCODE WHERE PASSWORD=@ACCESS_CODE AND LOGIN_NAME=@loginnname");
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@ACCESS_CODE", Utilities.GetMd5Hash(OldAccessCode));
                    objSqlCommand.Parameters.AddWithValue("@loginnname", Uname);
                    objSqlCommand.Parameters.AddWithValue("@NEWACCESSCODE", Utilities.GetMd5Hash(NewAccessCode));
                    int intExecuteRecordsCount = Utilities.ExecuteNonQueryRecord(objSqlCommand);
                    if (intExecuteRecordsCount > 0)
                    {
                        this.intStatusCode = 0;
                    }
                    else
                    {
                        this.intStatusCode = (int)USERLOGIN_ERRORS.PASSWORD_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                this.intStatusCode = -1000;
            }
        }
        public void ChangeUserPass(string OldAccessCode, string NewAccessCode, string Type,string Uname)
        {
            try
            {
                if (checkuser(Utilities.GetMd5Hash(OldAccessCode)) == false)
                {
                    StatusDescription = " Old password entered is not correct,hence unable to change the password";
                    this.intStatusCode = 100;
                }
                else
                {
                    SqlCommand objSqlCommand = new SqlCommand(@"update ADMIN_TBL_LOGIN_USER_DETAIL set PASSWORD=@NEWACCESSCODE where PASSWORD=@ACCESS_CODE and TYPE=@type 
                                                            and LOGIN_NAME=@loginname");
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@ACCESS_CODE", Utilities.GetMd5Hash(OldAccessCode));
                    objSqlCommand.Parameters.AddWithValue("@type", Type);
                    objSqlCommand.Parameters.AddWithValue("@loginname", Uname);
                    objSqlCommand.Parameters.AddWithValue("@NEWACCESSCODE", Utilities.GetMd5Hash(NewAccessCode));

                    int intExecuteRecordsCount = Utilities.ExecuteNonQueryRecord(objSqlCommand);
                    if (intExecuteRecordsCount > 0)
                    {
                        this.intStatusCode = 0;
                    }
                    else
                    {
                        this.intStatusCode = (int)USERLOGIN_ERRORS.PASSWORD_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    this.intStatusCode = -1000;
            }
        }

       // string strgetPassword;
        //public string GetResellerId(string password)
        //{
        //    SqlCommand objSqlCommand = new SqlCommand(@"select RESELLER_ID from ADMIN_TBL_RESELLER_DETAIL where PASSWORD=@password");
        //    objSqlCommand.CommandType = CommandType.Text;
        //    objSqlCommand.Parameters.AddWithValue("@password", password);
        //    DataSet objDataset = Utilities.SelectDataFromSQlCommand(objSqlCommand);

        //    if (objDataset.Tables[0].Rows.Count > 0)
        //    {
        //        strgetPassword = objDataset.Tables[0].Rows[0]["RESELLER_ID"].ToString();

        //    }
        //    return strgetPassword;
        //}
      
       // string strResellerPassword;
        //public string GetPasswordReseller(string ResellerId)
        //{
        //    SqlCommand objSqlCommand = new SqlCommand(@"select PASSWORD from ADMIN_TBL_RESELLER_DETAIL where RESELLER_ID=@resellerid");
        //    objSqlCommand.CommandType = CommandType.Text;
        //    objSqlCommand.Parameters.AddWithValue("@resellerid", ResellerId);
        //    DataSet objDataset = Utilities.SelectDataFromSQlCommand(objSqlCommand);

        //    if (objDataset.Tables[0].Rows.Count > 0)
        //    {
        //        strResellerPassword = objDataset.Tables[0].Rows[0]["PASSWORD"].ToString();
                
        //    }
        //    return strResellerPassword;



        //}


        //public string GetPasswordUser(string type)
        //{
        //    SqlCommand objSqlCommand = new SqlCommand(@"select PASSWORD from ADMIN_TBL_LOGIN_USER_DETAIL where TYPE=@type");
        //    objSqlCommand.CommandType = CommandType.Text;
        //    objSqlCommand.Parameters.AddWithValue("@type", type);
        //    DataSet objDataset = Utilities.SelectDataFromSQlCommand(objSqlCommand);

        //    if (objDataset.Tables[0].Rows.Count > 0)
        //    {
        //        strPassword = objDataset.Tables[0].Rows[0]["PASSWORD"].ToString();
        //        return strPassword;
        //    }
        //    return "";

 
        //}

        public DataTable ResultTable
        {
            set;
            get;
        }


        public string UserId
        {
            get;
            set;
        }
        public string OldAccessCode
        {
            set;
            get;
        }
        public string NewAccessCode
        {
            set;
            get;
        }

        public int StatusCode
        {
            get
            {
                return intStatusCode;
            }
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
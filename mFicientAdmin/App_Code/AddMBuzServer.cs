﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class AddMBuzzServerAdmin
    {
        public string GetCompanyId(string ServerId)
        {
            string CompanyId = string.Empty; ;
            try
            {
                SqlCommand cmd = new SqlCommand(@" select COMPANY_ID from ADMIN_TBL_SERVER_MAPPING where MBUZZ_SERVER_ID=@serverid");
                cmd.Parameters.AddWithValue("@serverid", ServerId);
                DataSet objDs = Utilities.SelectDataFromSQlCommand(cmd);
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    CompanyId = Convert.ToString(objDs.Tables[0].Rows[0]["COMPANY_ID"]);
                }
                else
                {
                    CompanyId = "";
                }
            }
            catch(Exception e) 
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
              return CompanyId;
        }

        public void DeleteServer(string serverid)
        {
            try
            {
                string sql = @"delete from ADMIN_TBL_MBUZZ_SERVER where MBUZZ_SERVER_ID=@serverid";
                SqlCommand objSqlCmd = new SqlCommand(sql);
                objSqlCmd.CommandType = CommandType.Text;
                objSqlCmd.Parameters.AddWithValue("@serverid", serverid);
                if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_DELETE_ERROR;
                    this.StatusDescription = "Record Delete Error";
                }
            }
            catch(Exception e) 
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_DELETE_ERROR;
            }
        }

        public DataTable GetMBuzzServerList()
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand("select * from ADMIN_TBL_MBUZZ_SERVER order by MBUZZ_SERVER_NAME");
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
           // return ds.Tables[0];
        }
        public string Servername
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public int StatusCode
        {
            get;
            set;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class GetPlanPrices
    {
        public GetPlanPrices()
        { }
        public GetPlanPrices(string planCode)
        {
            this.PlanCode = planCode;
        }
        public void Process()
        {
            StatusCode = -1000;//for error
            try
            {
                string strQuery = "";
                if (String.IsNullOrEmpty(PlanCode))
                {
                    strQuery = @"SELECT PlanPrice.*,Plans.PLAN_NAME FROM ADMIN_TBL_PLAN_PRICE AS PlanPrice
                                INNER JOIN ADMIN_TBL_PLANS AS Plans
                                ON PlanPrice.PLAN_CODE = Plans.PLAN_CODE";
                }
                else
                {
                    strQuery = @"SELECT PlanPrice.*,Plans.PLAN_NAME FROM ADMIN_TBL_PLAN_PRICE AS PlanPrice
                                INNER JOIN ADMIN_TBL_PLANS AS Plans
                                ON PlanPrice.PLAN_CODE = Plans.PLAN_CODE
                                WHERE PlanPrice.PLAN_CODE = @PlanCode";
                }
                SqlCommand cmd = new SqlCommand(strQuery);
                if (!String.IsNullOrEmpty(PlanCode))
                {
                    cmd.Parameters.AddWithValue("@PlanCode", this.PlanCode);
                }
                DataSet dsPlanPrices = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsPlanPrices != null && dsPlanPrices.Tables.Count > 0)
                {
                    if (dsPlanPrices.Tables[0].Rows.Count > 0)
                    {
                        ResultTable = dsPlanPrices.Tables[0];
                        this.StatusCode = 0;
                        this.StatusDescription = "";
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                this.StatusDescription = "Internal Server Error";
            }
        }


        public DataTable ResultTable
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string PlanCode
        {
            set;
            get;
        }

    }
}
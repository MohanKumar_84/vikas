﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class GetPlanDetailsAdmin
    {
        public void PlanDetails(string planid)
        {
            try
            {
            SqlCommand ObjSqlCmd = new SqlCommand(@"SELECT PLAN_CODE,PLAN_NAME,MAX_WORKFLOW,MIN_USERS,PUSHMESSAGE_PERDAY,PUSHMESSAGE_PERMONTH,IS_ENABLED FROM ADMIN_TBL_PLANS  where PLAN_ID=@planid ");
            ObjSqlCmd.CommandType = CommandType.Text;
            ObjSqlCmd.Parameters.AddWithValue("@planid", planid);
            DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
            if (objDataSet.Tables[0].Rows.Count > 0)
            {
                this.StatusCode = 0;
                ResultTable = objDataSet.Tables[0];
            }
            else
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = "Record not found";
            }
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        //public DataTable PlanPriceDetails(string plancode,string FromMonth,string ToMonth,string Currency)
        //{
        //    SqlCommand ObjSqlCmd = new SqlCommand(@"select PRICE from ADMIN_TBL_PLAN_PRICE where PLAN_CODE=@plancode and FROM_MONTH=@frommonth and TO_MONTH=@tomonth and CURRENCY=@currency");
        //    ObjSqlCmd.CommandType = CommandType.Text;
        //    ObjSqlCmd.Parameters.AddWithValue("@plancode", plancode);
        //    ObjSqlCmd.Parameters.AddWithValue("@frommonth", FromMonth);
        //    ObjSqlCmd.Parameters.AddWithValue("@tomonth", ToMonth);
        //    ObjSqlCmd.Parameters.AddWithValue("@currency", Currency);
        //   DataSet ds = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
        //    return ds.Tables[0];
        //}

        public DataTable ResultTable
        {
            set;
            get;
        }

         public int StatusCode
         {
             set;
             get;
         }
         public string StatusDescription
         {
             set;
             get;
         }
        }
    }

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class GetPlanPurchaseDtlByUserType
    {
        /// <summary>
        /// Get Plan Purchase Details for user
        /// </summary>
        /// <param name="userType">pass "sales","accounts"</param>
        public GetPlanPurchaseDtlByUserType(string userType)
        {
            this.UserType = userType;
        }

        public void Process()
        {
            StatusCode = -1000;//for error
            DataSet dsPlanPurchaseDtls = getPlanPurchaseDetials();
            if(dsPlanPurchaseDtls != null)
            {
                PlanPurchaseDtl = dsPlanPurchaseDtls.Tables[0];
                StatusCode=0;
                StatusDescription ="";
            }
            else
            {
                
            }
        }

        DataSet getPlanPurchaseDetials()
        {
            try
            {
                string strQuery = getSqlQueryByType();
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                DataSet dsPlanPurchaseDtls = Utilities.SelectDataFromSQlCommand(cmd);
                return dsPlanPurchaseDtls;
            }
            catch
            {
                return null;
            }
        }

        string getSqlQueryByType()
        {
            string sqlQuery = "";
            try
            {
                if (UserType.ToLower() == "sales")
                {
                    sqlQuery = @"SELECT PurchaseLog.*,CompDtl.COMPANY_NAME,Plans.PLAN_NAME,Reseller.RESELLER_NAME
                                    FROM ADMIN_TBL_PLAN_PURCHASE_LOG  AS PurchaseLog
                                    INNER JOIN ADMIN_TBL_COMPANY_DETAIL AS CompDtl
                                    ON PurchaseLog.COMPANY_ID = CompDtl.COMPANY_ID
                                    INNER JOIN ADMIN_TBL_PLANS AS Plans
                                    ON PurchaseLog.PLAN_CODE = Plans.PLAN_CODE
                                    INNER JOIN ADMIN_TBL_RESELLER_DETAIL AS Reseller
                                    ON PurchaseLog.RESELLER_ID = Reseller.RESELLER_ID
                                    WHERE PurchaseLog.APPROVED =0";
                }
                else
                {
                    sqlQuery = @"SELECT PurchaseLog.*,CompDtl.COMPANY_NAME,Plans.PLAN_NAME,Reseller.RESELLER_NAME
                                    FROM ADMIN_TBL_PLAN_PURCHASE_LOG  AS PurchaseLog
                                    INNER JOIN ADMIN_TBL_COMPANY_DETAIL AS CompDtl
                                    ON PurchaseLog.COMPANY_ID = CompDtl.COMPANY_ID
                                    INNER JOIN ADMIN_TBL_PLANS AS Plans
                                    ON PurchaseLog.PLAN_CODE = Plans.PLAN_CODE
                                    INNER JOIN ADMIN_TBL_RESELLER_DETAIL AS Reseller
                                    ON PurchaseLog.RESELLER_ID = Reseller.RESELLER_ID
                                    WHERE PurchaseLog.GET_PAYMENT =0
                                    AND PurchaseLog.APPROVED = 1";
                }
            }
            catch 
            {
              StatusCode= (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
            return sqlQuery;
        }
        public string UserType
        {
            get;
            set;
        }
        public DataTable PlanPurchaseDtl
        {
            get;
            set;
        }
        public int StatusCode
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class GetCompanyDetailsWithCurrentPlan
    {
        MFECompanyInfo _cmpInfo;
        MFECompanyAdmin _cmpAdmin;
        MFECurrentPlan _cmpCurrentPlan;
        MFEPaidPlanDetail _paidPlanDtl;
        MFETrialPlanDetail _trialPlanDtl;

        public GetCompanyDetailsWithCurrentPlan(string companyId)
        {
            CompanyId = companyId;
        }

        public void Process()
        {
            try
            {
                string strQuery = "";
                this.StatusCode = -1000;// for error
                if (isCompanyUsingTrialPlan())
                {
                    strQuery = @"SELECT *
                                FROM ADMIN_TBL_COMPANY_DETAIL AS CmpDtl
                                INNER JOIN ADMIN_TBL_COMPANY_CURRENT_PLAN AS CmpCurrPlan
                                ON CmpDtl.COMPANY_ID = CmpCurrPlan.COMPANY_ID
                                INNER JOIN ADMIN_TBL_COMPANY_ADMINISTRATOR AS CmpAdmin
                                ON CmpAdmin.ADMIN_ID = CmpDtl.ADMIN_ID
                                INNER JOIN ADMIN_TBL_TRIAL_PLANS AS TrialPlans
                                ON CmpCurrPlan.PLAN_CODE = TrialPlans.PLAN_CODE
                                WHERE CmpDtl.COMPANY_ID = @CompanyId;";
                }
                else
                {
                    strQuery = @"SELECT *
                                FROM ADMIN_TBL_COMPANY_DETAIL AS CmpDtl
                                INNER JOIN ADMIN_TBL_COMPANY_CURRENT_PLAN AS CmpCurrPlan
                                ON CmpDtl.COMPANY_ID = CmpCurrPlan.COMPANY_ID
                                INNER JOIN ADMIN_TBL_COMPANY_ADMINISTRATOR AS CmpAdmin
                                ON CmpAdmin.ADMIN_ID = CmpDtl.ADMIN_ID
                                INNER JOIN ADMIN_TBL_PLANS AS Plans
                                ON CmpCurrPlan.PLAN_CODE = Plans.PLAN_CODE
                                WHERE CmpDtl.COMPANY_ID = @CompanyId;";
                }
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                DataSet dsCompanyInfo = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsCompanyInfo == null) throw new Exception("Internal server error.");
                if (dsCompanyInfo.Tables.Count > 0 &&
                    dsCompanyInfo.Tables[0].Rows.Count > 0)
                {
                    this.CompanyDetail = dsCompanyInfo.Tables[0];
                    this.CmpAdmin = fillAdminDtlsFromDtbl(dsCompanyInfo.Tables[0]);
                    this.CmpInfo = fillCompanyDetailFromDtbl(dsCompanyInfo.Tables[0]);
                    this.CmpCurrentPlan = fillCurrentPlanFromDtbl(dsCompanyInfo.Tables[0]);
                    if (this.CmpAdmin.IsTrial != null && this.CmpAdmin.IsTrial)
                    {
                        this.TrialPlanDtl = fillTrialPlanDtlFromDtbl(dsCompanyInfo.Tables[0]);
                    }
                    else
                    {
                        this.PaidPlanDtl = fillPaidPlanDtlFromDtbl(dsCompanyInfo.Tables[0]);
                    }
                }
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch (Exception e)
            {
                this.StatusCode = -1000;
                this.StatusDescription = e.Message;
            }
        }

        bool isCompanyUsingTrialPlan()
        {
            try
            {
                string strQuery = @"SELECT CmpAdmin.IS_TRIAL 
                                FROM ADMIN_TBL_COMPANY_DETAIL AS CmpDtl
                                INNER JOIN ADMIN_TBL_COMPANY_CURRENT_PLAN AS CmpCurrPlan
                                ON CmpDtl.COMPANY_ID = CmpCurrPlan.COMPANY_ID
                                INNER JOIN ADMIN_TBL_COMPANY_ADMINISTRATOR AS CmpAdmin
                                ON CmpAdmin.ADMIN_ID = CmpDtl.ADMIN_ID
                                WHERE CmpDtl.COMPANY_ID = @CompanyId";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                DataSet dsCompanyInfo = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsCompanyInfo.Tables.Count > 0 && dsCompanyInfo.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToInt32(dsCompanyInfo.Tables[0].Rows[0]["IS_TRIAL"]) == 1)
                    {
                        return true;
                    }
                }
                else
                {
                    throw new Exception("Internal server error");
                }
            }
            catch (Exception e)
            {
                this.StatusCode = -1000;
                this.StatusDescription = e.Message;
            }
            return false;
        }
        MFECompanyInfo fillCompanyDetailFromDtbl(DataTable companyDetail)
        {
            if (companyDetail == null) throw new ArgumentNullException();
            MFECompanyInfo objCmpInfo = new MFECompanyInfo();
            objCmpInfo.CompanyName = Convert.ToString(companyDetail.Rows[0]["COMPANY_NAME"]);
            objCmpInfo.StreetAddress1 = Convert.ToString(companyDetail.Rows[0]["STREET_ADDRESS1"]);
            objCmpInfo.StreetAddress2 = Convert.ToString(companyDetail.Rows[0]["STREET_ADDRESS2"]);
            objCmpInfo.StreetAddress3 = Convert.ToString(companyDetail.Rows[0]["STREET_ADDRESS3"]);
            objCmpInfo.CityName = Convert.ToString(companyDetail.Rows[0]["CITY_NAME"]);
            objCmpInfo.StateName = Convert.ToString(companyDetail.Rows[0]["STATE_NAME"]);
            objCmpInfo.SupportEmail = Convert.ToString(companyDetail.Rows[0]["SUPPORT_EMAIL"]);
            objCmpInfo.SupportContact = Convert.ToString(companyDetail.Rows[0]["SUPPORT_CONTACT"]);
            objCmpInfo.SalesManagerApproved = Convert.ToBoolean(companyDetail.Rows[0]["SM_APPROVED"]);
            objCmpInfo.IsBlocked = Convert.ToBoolean(companyDetail.Rows[0]["IS_BLOCKED"]);
            objCmpInfo.BlockOrUnblockId = Convert.ToString(companyDetail.Rows[0]["BLOCK_UNBLOCK_ID"]);
            return objCmpInfo;
        }
        MFECurrentPlan fillCurrentPlanFromDtbl(DataTable companyDetail)
        {
            if (companyDetail == null) throw new ArgumentNullException();
            MFECurrentPlan objCurrentPlan = new MFECurrentPlan();
            objCurrentPlan.PlanCode = Convert.ToString(companyDetail.Rows[0]["PLAN_CODE"]);
            objCurrentPlan.MaxWorkFlow = Convert.ToInt32(companyDetail.Rows[0]["MAX_WORKFLOW"]);
            objCurrentPlan.MaxUser = Convert.ToInt32(companyDetail.Rows[0]["MAX_USER"]);
            objCurrentPlan.Validity = Convert.ToDouble(companyDetail.Rows[0]["VALIDITY"]);
            objCurrentPlan.PurchaseDate = Convert.ToInt64(companyDetail.Rows[0]["PURCHASE_DATE"]);
            return objCurrentPlan;
        }
        MFECompanyAdmin fillAdminDtlsFromDtbl(DataTable companyDetail)
        {
            if (companyDetail == null) throw new ArgumentNullException();
            MFECompanyAdmin objCmpAdmin = new MFECompanyAdmin();
            objCmpAdmin.StreetAddress1 = Convert.ToString(companyDetail.Rows[0]["STREET_ADDRESS1"]);
            objCmpAdmin.StreetAddress2 = Convert.ToString(companyDetail.Rows[0]["STREET_ADDRESS2"]);
            objCmpAdmin.StreetAddress3 = Convert.ToString(companyDetail.Rows[0]["STREET_ADDRESS3"]);
            objCmpAdmin.CityName = Convert.ToString(companyDetail.Rows[0]["CITY_NAME"]);
            objCmpAdmin.EmailId = Convert.ToString(companyDetail.Rows[0]["EMAIL_ID"]);
            objCmpAdmin.IsTrial = Convert.ToBoolean(companyDetail.Rows[0]["IS_TRIAL"]);
            objCmpAdmin.Firstname = Convert.ToString(companyDetail.Rows[0]["FIRST_NAME"]);
            objCmpAdmin.MiddleName = Convert.ToString(companyDetail.Rows[0]["MIDDLE_NAME"]);
            objCmpAdmin.LastName = Convert.ToString(companyDetail.Rows[0]["LAST_NAME"]);
             return objCmpAdmin;
        }
        MFEPaidPlanDetail fillPaidPlanDtlFromDtbl(DataTable cmpDtlWithPlan)
        {
            if (cmpDtlWithPlan == null) throw new ArgumentNullException();
            MFEPaidPlanDetail objPaidPlanDtl = new MFEPaidPlanDetail();
            objPaidPlanDtl.PlanName = Convert.ToString(cmpDtlWithPlan.Rows[0]["PLAN_NAME"]);
            return objPaidPlanDtl;
        }
        MFETrialPlanDetail fillTrialPlanDtlFromDtbl(DataTable cmpDtlWithPlan)
        {
            if (cmpDtlWithPlan == null) throw new ArgumentNullException();
            MFETrialPlanDetail objTrialPlanDtl = new MFETrialPlanDetail();
            objTrialPlanDtl.PlanName = Convert.ToString(cmpDtlWithPlan.Rows[0]["PLAN_NAME"]);
            return objTrialPlanDtl;
        }
        public string CompanyId
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public DataTable CompanyDetail
        {
            set;
            get;
        }
        public MFECompanyInfo CmpInfo
        {
            get { return _cmpInfo; }
            private set { _cmpInfo = value; }
        }
        public MFECurrentPlan CmpCurrentPlan
        {
            get { return _cmpCurrentPlan; }
            private set { _cmpCurrentPlan = value; }
        }
        public MFECompanyAdmin CmpAdmin
        {
            get { return _cmpAdmin; }
            private set { _cmpAdmin = value; }
        }
        public MFEPaidPlanDetail PaidPlanDtl
        {
            get { return _paidPlanDtl; }
            private set { _paidPlanDtl = value; }
        }


        public MFETrialPlanDetail TrialPlanDtl
        {
            get { return _trialPlanDtl; }
            private set { _trialPlanDtl = value; }
        }

    }
}
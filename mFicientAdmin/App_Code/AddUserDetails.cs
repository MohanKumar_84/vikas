﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class AddUserDetailsAdmin
    {
        public AddUserDetailsAdmin()
        { }
        public AddUserDetailsAdmin(string loginName, string password, string email, byte isEnabled, string contactNo, string managerId, string countryCode, string city
                                    , string type, byte serverAllocation, string userName)
        {
            this.LoginName = loginName;
            this.Password = password;
            this.Email = email;
            this.IsEnabled = isEnabled;
            this.ContactNo = contactNo;
            this.ManagerId = managerId;
            this.CountryCode = countryCode;
            this.City = city;
            this.Type = type;
            this.ServerAllocation = serverAllocation;
            this.UserName = userName;
        }
        //not required
//        public void AddDetails(string username, string loginname, string country, string type)
//        {
//            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
//            //            string sql = @"insert into ADMIN_TBL_LOGIN_USER_DETAIL values(@loginname,@password,@email,@isenabled,@contactnumber,
//            //                            @createdon,@loginid,@managerid,
//            //                            @countrycode,@city,@type,@isserver,@username where TYPE=@type)";

//            string sql = @"INSERT INTO [ADMIN_TBL_LOGIN_USER_DETAIL]
//                                   ([LOGIN_NAME],[PASSWORD],[EMAIL]
//                                   ,[IS_ENABLED],[CONTACT_NUMBER],[CREATED_ON]
//                                   ,[LOGIN_ID],[MANAGER_ID],[COUNTRY_CODE]
//                                   ,[CITY],[TYPE],[IS_SERVER_ALLOCATION]
//                                   ,[USER_NAME])
//                             VALUES
//                                   (@LOGIN_NAME,@PASSWORD,@EMAIL
//                                   ,@IS_ENABLED,@CONTACT_NUMBER,@CREATED_ON
//                                   ,@LOGIN_ID,@MANAGER_ID,@COUNTRY_CODE
//                                   ,@CITY,@TYPE,@IS_SERVER_ALLOCATION
//                                   ,@USER_NAME))";
//            SqlCommand objSqlCmd = new SqlCommand(sql, con);

//            objSqlCmd.Parameters.AddWithValue("@username", username);
//            objSqlCmd.Parameters.AddWithValue("@loginname", loginname);
//            objSqlCmd.Parameters.AddWithValue("@country", country);
//            objSqlCmd.Parameters.AddWithValue("@type", type);



//            objSqlCmd.Parameters.AddWithValue("managerid", Utilities.GetManagerId(username));

//            try
//            {
//                con.Open();

//                objSqlCmd.CommandType = CommandType.Text;
//                Utilities.ExecuteNonQueryRecord(objSqlCmd);

//            }

//            catch (System.Data.SqlClient.SqlException ex)
//            {
//                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
//                {
//                    throw ex;
//                }
//            }


//            finally
//            {
//                Utilities.SqlConnectionClose(con);
//            }


//        }
        
        public void Process()
        {
            DataTable dtblAllLoginNames = getAllLoginNames();
            string strFilter = String.Format("LOGIN_NAME ='{0}'", this.LoginName);
            if (dtblAllLoginNames.Select(strFilter).Length > 0)
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Login name already taken";
                return;
            }
            else
            {
                try
                {
                    string strQuery = @"INSERT INTO [ADMIN_TBL_LOGIN_USER_DETAIL]
                                   ([LOGIN_NAME],[PASSWORD],[EMAIL]
                                   ,[IS_ENABLED],[CONTACT_NUMBER],[CREATED_ON]
                                   ,[LOGIN_ID],[MANAGER_ID],[COUNTRY_CODE]
                                   ,[CITY],[TYPE],[IS_SERVER_ALLOCATION]
                                   ,[USER_NAME])
                             VALUES
                                   (@LOGIN_NAME,@PASSWORD,@EMAIL
                                   ,@IS_ENABLED,@CONTACT_NUMBER,@CREATED_ON
                                   ,@LOGIN_ID,@MANAGER_ID,@COUNTRY_CODE
                                   ,@CITY,@TYPE,@IS_SERVER_ALLOCATION
                                   ,@USER_NAME)";
                    SqlCommand cmd = new SqlCommand(strQuery);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.AddWithValue("@LOGIN_NAME", this.LoginName);
                    cmd.Parameters.AddWithValue("@PASSWORD", Utilities.GetMd5Hash(this.Password));
                    cmd.Parameters.AddWithValue("@EMAIL", this.Email);
                    cmd.Parameters.AddWithValue("@IS_ENABLED", this.IsEnabled == 1 ? 1 : 0);
                    cmd.Parameters.AddWithValue("@CONTACT_NUMBER", this.ContactNo);
                    cmd.Parameters.AddWithValue("@CREATED_ON", DateTime.UtcNow);
                    cmd.Parameters.AddWithValue("@LOGIN_ID", Utilities.GetMd5Hash(loginname + DateTime.UtcNow.ToString()));
                    cmd.Parameters.AddWithValue("@MANAGER_ID", this.ManagerId);
                    cmd.Parameters.AddWithValue("@COUNTRY_CODE", this.CountryCode);
                    cmd.Parameters.AddWithValue("@CITY", this.City);
                    cmd.Parameters.AddWithValue("@TYPE", this.Type);
                    cmd.Parameters.AddWithValue("@IS_SERVER_ALLOCATION", this.ServerAllocation);
                    cmd.Parameters.AddWithValue("@USER_NAME", this.UserName);
                    if (Utilities.ExecuteNonQueryRecord(cmd) > 0)
                    {
                        this.StatusCode = 0;
                        this.StatusDescription = "";
                    }
                    else
                    {
                        this.StatusCode = -1000;
                        this.StatusDescription = "Record insert error";
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        throw ex;
                    }
                    // throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
            }
        }

        DataTable getAllLoginNames()
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand("SELECT LOGIN_NAME FROM ADMIN_TBL_RESELLER_DETAIL UNION SELECT LOGIN_NAME FROM ADMIN_TBL_LOGIN_USER_DETAIL");
                 DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
           
        }

        public string username
        {
            get;
            set;
        }
        public string loginname
        {
            get;
            set;
        }
        public string country
        {
            get;
            set;
        }
        public string type
        {
            get;
            set;

        }
        
        public int StatusCode
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }

        public string LoginName
        {
            get;
            set;
        }
        public string Password
        {
            get;
            set;
        }
        public string Email
        {
            get;
            set;
        }
        public byte IsEnabled
        {
            get;
            set;
        }
        public string ContactNo
        {
            get;
            set;
        }
        public string ManagerId
        {
            get;
            set;
        }
        public string CountryCode
        {
            get;
            set;
        }
        public string City
        {
            get;
            set;
        }
        public string Type
        {
            get;
            set;
        }
        public byte ServerAllocation
        {
            get;
            set;
        }
        public string UserName
        {
            get;
            set;
        }

    }


}




﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class GetPlanRequestsLogDetailsForAcc
    {
        public GetPlanRequestsLogDetailsForAcc(string companyId, string planCode, string transactionType)
        {
            this.CompanyId = companyId;
            this.PlanCode = planCode;
            this.TransactionType = transactionType;
        }

        public void Process()
        {
            StatusCode = -1000;//for error
            try
            {
                DataTable dtblPlanRequestLog = getPlanRequestsLogDetails();
                if (dtblPlanRequestLog != null)
                {
                    PlanRequestLog = dtblPlanRequestLog;
                    if (dtblPlanRequestLog.Rows.Count > 0)
                    {
                        this.PlanPurchaseLog = new MFEPlanPurchaseLog();
                        PlanPurchaseLog.PlanCode = Convert.ToString(dtblPlanRequestLog.Rows[0]["PLAN_CODE"]);
                        PlanPurchaseLog.MaxWorkFlow = Convert.ToInt32(dtblPlanRequestLog.Rows[0]["MAX_WORKFLOW"]);
                        PlanPurchaseLog.MaxUsers = Convert.ToInt32(dtblPlanRequestLog.Rows[0]["MAX_USER"]);
                        PlanPurchaseLog.UserChargePerMonth = Convert.ToDouble(dtblPlanRequestLog.Rows[0]["USER_CHARGE_PM"]);
                        PlanPurchaseLog.ChargeType = Convert.ToString(dtblPlanRequestLog.Rows[0]["CHARGE_TYPE"]);
                        PlanPurchaseLog.Validity = Convert.ToInt32(dtblPlanRequestLog.Rows[0]["VALIDITY"]);
                    }
                    StatusCode = 0;
                    StatusDescription = "";
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
            }
        }


        DataTable getPlanRequestsLogDetails()
        {
            try
            {
                string strQuery = @"SELECT * FROM ADMIN_TBL_PLAN_PURCHASE_LOG
                                WHERE COMPANY_ID = @CompanyId
                                AND PLAN_CODE = @PlanCode
                                AND TRANSACTION_TYPE = @TransactionType
                                AND APPROVED = 0";

                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.Parameters.AddWithValue("@PlanCode", this.PlanCode);
                cmd.Parameters.AddWithValue("@TransactionType", this.TransactionType);
                return Utilities.SelectDataFromSQlCommand(cmd).Tables[0];
            }
            catch
            {
                return null;
            }
        }

        public int StatusCode
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }

        public string CompanyId
        {
            get;
            set;
        }
        public string PlanCode
        {
            get;
            set;
        }
        public string TransactionType
        {
            get;
            set;
        }
        public DataTable PlanRequestLog
        {
            get;
            set;
        }
        public MFEPlanPurchaseLog PlanPurchaseLog
        {
            get;
            private set;
        }

    }
}
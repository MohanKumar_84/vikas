﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class SaveNewPassword
    {
        /// <summary>
        /// Pass
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <param name="companyId"></param>
        /// <param name="accessCode">Pass real access code.It is MD5'd while saving.</param>
        /// <param name="userType"></param>
        public SaveNewPassword(string userId,
            string userName,
            string accessCode,
            USER_TYPE userType,
            HttpContext context)
        {
            this.UserId = userId;
            this.UserName = userName;
            this.AccessCode = accessCode;
            this.PwdResetRequestedBy = userType;
            this.Context = context;
        }

        public void Process()
        {
            try
            {
                StatusCode = -1000;
                string strEmailId = String.Empty;
                getEmailIdOfUser();
                SqlTransaction transaction = null;
                SqlConnection con = null;
                try
                {
                    MSSqlClient.SqlConnectionOpen(out con);
                    using (con)
                    {
                        using (transaction = con.BeginTransaction())
                        {
                            int iRowsAffected = 0;
                            switch (this.PwdResetRequestedBy)
                            {
                                case USER_TYPE.Reseller:
                                    iRowsAffected = updateResellerAccessCode(transaction, con);
                                    break;
                                case USER_TYPE.SalesManager:
                                case USER_TYPE.Account:
                                case USER_TYPE.Support:
                                case USER_TYPE.SalesPerson:
                                    iRowsAffected = updateUserAccessCode(transaction, con);
                                    break;
                                case USER_TYPE.Admin:
                                    iRowsAffected = updateAdminAccessCode(transaction, con);
                                    break;
                            }

                            if (iRowsAffected > 0)
                            {
                                TimeZoneInfo tzi;
                                try
                                {
                                    tzi =
                                        TimeZoneInfo.FindSystemTimeZoneById(MficientConstants.STANDARD_TIMEZONE_ID);
                                }
                                catch (TimeZoneNotFoundException)
                                {
                                    tzi = TimeZoneInfo.Local;
                                }
                                catch (InvalidTimeZoneException)
                                {
                                    tzi = TimeZoneInfo.Local;
                                }
                                updatePwdLogIsUsedAndIsExpiredStatus(transaction, con);
                                SaveEmailInfo.changePassword(
                                    strEmailId,
                                    this.UserName,
                                    this.AccessCode,
                                    DateTime.UtcNow.Ticks,
                                    tzi,
                                    Context
                                    );
                            }
                            else
                            {
                                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                            }
                            transaction.Commit();
                            StatusCode = 0;
                        }
                    }
                }
                catch
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Internal server error";
                }
                finally
                {
                    if (con != null)
                    {
                        con.Dispose();
                    }
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
        string getEmailIdOfUser()
        {
            string strEmailId = String.Empty;
            switch (this.PwdResetRequestedBy)
            {
                case USER_TYPE.Reseller:
                    GetResellerDetailsAdmin objResellerDtl = new
                    GetResellerDetailsAdmin(this.UserId, this.UserName);
                    objResellerDtl.Process();
                    if (objResellerDtl.StatusCode == 0)
                    {
                        strEmailId = objResellerDtl.Reseller.Email;
                    }
                    else
                    {
                        throw new Exception();
                    }
                    break;
                case USER_TYPE.SalesManager:
                case USER_TYPE.Account:
                case USER_TYPE.Support:
                case USER_TYPE.SalesPerson:
                    GetUserDetailsAdmin objUserDtl = new
                    GetUserDetailsAdmin();
                    MFEAdminPanelUsers objAdminPanelUsers= objUserDtl.GetUserDetailsByNameAndUserCode(
                        this.UserName,
                        Utilities.getUserTypeCodeFromUserType(this.PwdResetRequestedBy)
                        );
                    if (objUserDtl.StatusCode == 0)
                    {
                        strEmailId = objAdminPanelUsers.Email;
                    }
                    else
                    {
                        throw new Exception();
                    }
                    break;
                case USER_TYPE.Admin:
                    GetAdminDetails objAdminDtl = new GetAdminDetails(
                        GetAdminDetails.GET_RECORDS_BY.usernameAndId,
                        this.UserName, this.UserId
                        );
                    objAdminDtl.Process();
                    if (objAdminDtl.StatusCode == 0)
                    {
                        strEmailId = objAdminDtl.Admin.Email;
                    }
                    else
                    {
                        throw new Exception();
                    }
                    break;
               
            }
            return strEmailId;
        }
        int updateAdminAccessCode(SqlTransaction transaction, SqlConnection con)
        {
            string strQuery = @"UPDATE ADMIN_TBL_ADMIN_LOGIN
                                SET ACCESS_CODE = @ACCESS_CODE
                                WHERE ADMIN_NAME = @ADMIN_NAME
                                AND ADMIN_ID =@ADMIN_ID";

            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ACCESS_CODE", Utilities.GetMd5Hash(AccessCode).ToUpper());
            cmd.Parameters.AddWithValue("@ADMIN_NAME", this.UserName);
            cmd.Parameters.AddWithValue("@ADMIN_ID", this.UserId);
            return cmd.ExecuteNonQuery();
        }
        int updateUserAccessCode(SqlTransaction transaction, SqlConnection con)
        {
            string strQuery = @"UPDATE ADMIN_TBL_LOGIN_USER_DETAIL
                                SET PASSWORD = @PASSWORD
                                WHERE LOGIN_NAME = @LOGIN_NAME
                                AND LOGIN_ID =@LOGIN_ID";

            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@PASSWORD", Utilities.GetMd5Hash(AccessCode).ToUpper());
            cmd.Parameters.AddWithValue("@LOGIN_NAME", this.UserName);
            cmd.Parameters.AddWithValue("@LOGIN_ID", this.UserId);
            return cmd.ExecuteNonQuery();
        }

        int updateResellerAccessCode(SqlTransaction transaction, SqlConnection con)
        {
            string strQuery = @"UPDATE ADMIN_TBL_RESELLER_DETAIL
                                  SET PASSWORD = @PASSWORD
                                  WHERE RESELLER_ID =@RESELLER_ID
                                  AND RESELLER_NAME =@RESELLER_NAME";

            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@PASSWORD", Utilities.GetMd5Hash(AccessCode).ToUpper());
            cmd.Parameters.AddWithValue("@RESELLER_ID", this.UserId);
            cmd.Parameters.AddWithValue("@RESELLER_NAME", this.UserName);
            return cmd.ExecuteNonQuery();
        }
        int updatePwdLogIsUsedAndIsExpiredStatus(SqlTransaction transaction, SqlConnection con)
        {
            string strQuery = @"UPDATE ADMIN_TBL_RESET_PASSWORD_LOG
                                SET IS_USED = 1
                                , IS_EXPIRED = 1
                                WHERE USER_ID = @UserId
                                AND IS_USED = 0
                                AND IS_EXPIRED = 0";

            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", UserId);
            return cmd.ExecuteNonQuery();
        }



        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string UserName
        {
            set;
            get;
        }

        public string UserId
        {
            set;
            get;
        }
        public string AccessCode
        {
            set;
            get;
        }
        public USER_TYPE PwdResetRequestedBy
        {
            get;
            private set;
        }

        public HttpContext Context
        {
            get;
            private set;
        }
    }



}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class AddCompanyDetailsAdmin
    {
        public DataTable GetCompanyListAdmin()
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(@"select distinct  c.COMPANY_NAME,c.COMPANY_ID,case when s.SERVER_NAME IS NULL then 'Not Alloted' else s.SERVER_NAME end as SERVERNAME
            ,case when mb.MBUZZ_SERVER_NAME IS NULL then 'Not Alloted' else mb.MBUZZ_SERVER_NAME end as MBUZZSERVERNAME 
            ,case when mp.MP_SERVER_NAME IS NULL then 'Not Alloted' else MP.MP_SERVER_NAME end as MPLUGGINSERVER
            from  ADMIN_TBL_COMPANY_DETAIL c left outer join ADMIN_TBL_SERVER_MAPPING m on c.COMPANY_ID=m.COMPANY_ID left outer join
            ADMIN_TBL_MST_SERVER s on m.SERVER_ID=s.SERVER_ID left outer join ADMIN_TBL_MBUZZ_SERVER mb 
            on m.MBUZZ_SERVER_ID=mb.MBUZZ_SERVER_ID left outer join 
            ADMIN_TBL_MST_MP_SERVER mp on m.MPLUGIN_SERVER_ID=mp.MP_SERVER_ID 
            WHERE c.IS_BLOCKED = 0 ORDER BY COMPANY_NAME");
                // sqlCommand.Parameters.AddWithValue("@name", name);
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }
        public DataTable getBlockedCompanyList()
        {
            DataTable dtblBlockedCmpList = new DataTable();
            try
            {

                SqlCommand sqlCommand = new SqlCommand(

                @"select distinct  c.COMPANY_NAME,c.COMPANY_ID,case when s.SERVER_NAME IS NULL then 'Not Alloted' else s.SERVER_NAME end as SERVERNAME
                ,case when mb.MBUZZ_SERVER_NAME IS NULL then 'Not Alloted' else mb.MBUZZ_SERVER_NAME end as MBUZZSERVERNAME 
                ,case when mp.MP_SERVER_NAME IS NULL then 'Not Alloted' else MP.MP_SERVER_NAME end as MPLUGGINSERVER
                from  ADMIN_TBL_COMPANY_DETAIL c left outer join ADMIN_TBL_SERVER_MAPPING m on c.COMPANY_ID=m.COMPANY_ID left outer join
                ADMIN_TBL_MST_SERVER s on m.SERVER_ID=s.SERVER_ID left outer join ADMIN_TBL_MBUZZ_SERVER mb 
                on m.MBUZZ_SERVER_ID=mb.MBUZZ_SERVER_ID left outer join 
                ADMIN_TBL_MST_MP_SERVER mp on m.MPLUGIN_SERVER_ID=mp.MP_SERVER_ID WHERE c.IS_BLOCKED = 1 ORDER BY COMPANY_NAME 
                ");
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                if (ds == null) throw new Exception();
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
                dtblBlockedCmpList = ds.Tables[0];
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return dtblBlockedCmpList;
        }
        public DataTable GetCompanyListReseller(string Id)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(@"select distinct  c.COMPANY_NAME,c.COMPANY_ID,case when s.SERVER_NAME IS NULL then 'Not Allocated' 
                                            else s.SERVER_NAME
                                                end as SERVERNAME
                                             ,case when mb.MBUZZ_SERVER_NAME IS NULL then 'Not Allocated' else
                                              mb.MBUZZ_SERVER_NAME end as MBUZZSERVERNAME ,case
                                            when mp.MP_SERVER_NAME IS NULL then 'Not Allocated'
                                            else MP.MP_SERVER_NAME end as MPLUGGINSERVER
                                            from 
                                    ADMIN_TBL_COMPANY_DETAIL c  inner join ADMIN_TBL_RESELLER_DETAIL rd on c.COUNTRY_CODE=rd.COUNTRY  
                                    left outer join ADMIN_TBL_SERVER_MAPPING m on c.COMPANY_ID=m.COMPANY_ID left outer join
                                     ADMIN_TBL_MST_SERVER s
                                    on m.SERVER_ID=s.SERVER_ID left outer join ADMIN_TBL_MBUZZ_SERVER mb 
                                    on m.MBUZZ_SERVER_ID=mb.MBUZZ_SERVER_ID left outer join 
                                     ADMIN_TBL_MST_MP_SERVER mp on m.MPLUGIN_SERVER_ID=mp.MP_SERVER_ID where rd.RESELLER_ID=@id ORDER BY COMPANY_NAME");
                sqlCommand.Parameters.AddWithValue("@id", Id);
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public DataTable GetCompanyListSalesMgr()
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(@"select distinct  c.COMPANY_NAME ,c.COMPANY_ID,
                case when s.SERVER_NAME IS NULL then 'Not Allocated' else s.SERVER_NAME end as SERVERNAME ,
                case when mb.MBUZZ_SERVER_NAME IS NULL then 'Not Allocated' else b.MBUZZ_SERVER_NAME end as MBUZZSERVERNAME ,
                case when mp.MP_SERVER_NAME IS NULL then 'Not Allocated' else MP.MP_SERVER_NAME end as MPLUGGINSERVER
                from ADMIN_TBL_COMPANY_DETAIL c inner join ADMIN_TBL_LOGIN_USER_DETAIL l on c.COUNTRY_CODE=l.COUNTRY_CODE
                left outer join ADMIN_TBL_SERVER_MAPPING m on c.COMPANY_ID=m.COMPANY_ID 
                left outer join ADMIN_TBL_MST_SERVER s on m.SERVER_ID=s.SERVER_ID 
                left outer join ADMIN_TBL_MBUZZ_SERVER mb on m.MBUZZ_SERVER_ID=mb.MBUZZ_SERVER_ID 
                left outer join ADMIN_TBL_MST_MP_SERVER mp on m.MPLUGIN_SERVER_ID=mp.MP_SERVER_ID 
                where c.SM_APPROVED=@smapproved order by COMPANY_NAME ");
                sqlCommand.Parameters.AddWithValue("@smapproved", 1);
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch
            {
                return null;
            }
        }

        public DataTable GetCompanyListSales(string LoginId)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(@"select distinct  c.COMPANY_NAME ,c.COMPANY_ID,
                case when s.SERVER_NAME IS NULL then 'Not Allocated' else s.SERVER_NAME end as SERVERNAME,
                case when mb.MBUZZ_SERVER_NAME IS NULL then 'Not Allocated' else mb.MBUZZ_SERVER_NAME end as MBUZZSERVERNAME ,
                case when mp.MP_SERVER_NAME IS NULL then 'Not Allocated' else MP.MP_SERVER_NAME end as MPLUGGINSERVER
                from ADMIN_TBL_COMPANY_DETAIL c 
                inner join ADMIN_TBL_LOGIN_USER_DETAIL l on c.COUNTRY_CODE=l.COUNTRY_CODE
                left outer join ADMIN_TBL_SERVER_MAPPING m on c.COMPANY_ID=m.COMPANY_ID 
                left outer join ADMIN_TBL_MST_SERVER s on m.SERVER_ID=s.SERVER_ID 
                left outer join ADMIN_TBL_MBUZZ_SERVER mb on m.MBUZZ_SERVER_ID=mb.MBUZZ_SERVER_ID 
                left outer join ADMIN_TBL_MST_MP_SERVER mp on m.MPLUGIN_SERVER_ID=mp.MP_SERVER_ID 
                where l.LOGIN_ID=@loginid order by COMPANY_NAME ");
                // sqlCommand.Parameters.AddWithValue("@smapproved", 1);
                sqlCommand.Parameters.AddWithValue("@loginid", LoginId);
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public void GetDetails(string companyid)
        {
            try
            {
                //changed on 19/9/2012.Mohan.inner join was added
                SqlCommand ObjSqlCmd = new SqlCommand(@"select * from ADMIN_TBL_COMPANY_DETAIL AS CmpDtl
                                                    inner join admin_tbl_mst_country AS MstCntry on CmpDtl.COUNTRY_CODE = MstCntry.COUNTRY_CODE 
                                                    where COMPANY_ID=@companyid order by COMPANY_NAME");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@companyid", companyid);

                //DataTable ResultTable;
                DataSet objdataset = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                ResultTable = objdataset.Tables[0];
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                    this.StatusDescription = ex.Message;
                }
            }
        }

        public void CompanyPlanAdminDetails(string companyid)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select distinct c.COMPANY_NAME as CompanyName,
                (c.STREET_ADDRESS1 +',' +c.STREET_ADDRESS2 +',' + c.STREET_ADDRESS3) as CompanyAddress ,
                c.CITY_NAME as CompanyCity,c.STATE_NAME as companystate,c.SUPPORT_EMAIL as companyemail,
                c.SUPPORT_CONTACT as companycontact,p.PLAN_CODE,p.MAX_WORKFLOW,p.MAX_USER,
                CONVERT(bigint, p.VALIDITY)+ PURCHASE_DATE as Date,
                (a.FIRST_NAME +a.MIDDLE_NAME+ a.LAST_NAME) as AdminName,
                (a.STREET_ADDRESS1 + a.STREET_ADDRESS2  + a.STREET_ADDRESS3) as AdminAddress,
                case when c.SM_APPROVED='1' then 'Company Working' else 'Processing' end as STATUS,
                a.CITY_NAME as AdminCity,a.STATE_NAME as AdminState,tb.PLAN_name,a.EMAIL_ID
                from ADMIN_TBL_COMPANY_CURRENT_PLAN p  
                inner join ADMIN_TBL_COMPANY_DETAIL c on c.COMPANY_ID=p.COMPANY_ID 
                inner join ADMIN_TBL_COMPANY_ADMINISTRATOR a on c.ADMIN_ID=a.ADMIN_ID 
                inner join ADMIN_TBL_PLANS tb on p.PLAN_CODE=tb.PLAN_CODE
                where c.COMPANY_ID=@companyid order by c.Company_name");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@companyid", companyid);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.StatusDescription = "";
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusDescription = "No records to display";
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                    this.StatusDescription = ex.Message;
                }
            }
        }

        public string GetCompanyName(string companyId)
        {
            string strname = string.Empty;
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select COMPANY_NAME from ADMIN_TBL_COMPANY_DETAIL d where COMPANY_ID=@companyid");
                objCmd.CommandType = CommandType.Text;
                objCmd.Parameters.AddWithValue("@companyid", companyId);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    strname = Convert.ToString(objDataSet.Tables[0].Rows[0]["COMPANY_NAME"]);
                    return strname;
                }
                else
                {
                    return strname = "";
                }
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
            return strname;
        }

        public bool GetServerUrl(string serverId, string MbuzzServerID)
        {
            string strUrl = string.Empty;
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select SERVER_URL from ADMIN_TBL_MST_SERVER where SERVER_ID=@serverid;
                select SERVER_IP,PORT_NUMBER from ADMIN_TBL_MBUZZ_SERVER where MBUZZ_SERVER_ID=@MBUZZ_serverid");
                objCmd.CommandType = CommandType.Text;
                objCmd.Parameters.AddWithValue("@serverid", serverId);
                objCmd.Parameters.AddWithValue("@MBUZZ_serverid", MbuzzServerID);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objCmd);
                if (objDataSet.Tables[0].Rows.Count > 0 && objDataSet.Tables[1].Rows.Count > 0)
                {
                    ServerURL = Convert.ToString(objDataSet.Tables[0].Rows[0]["SERVER_URL"]);
                    mBuzz_ServerIP = Convert.ToString(objDataSet.Tables[1].Rows[0]["SERVER_IP"]);
                    mBuzz_port = Convert.ToInt32(objDataSet.Tables[1].Rows[0]["PORT_NUMBER"]);
                    return true;
                }
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
            return false;
        }

        public string GetUserId(string username)
        {
            string strId = string.Empty;
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select LOGIN_ID from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@loginname");
                objCmd.CommandType = CommandType.Text;
                objCmd.Parameters.AddWithValue("@loginname", username);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    strId = Convert.ToString(objDataSet.Tables[0].Rows[0]["LOGIN_ID"]);
                    return strId;
                }
                else
                {
                    return strId = "";
                }
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
            return strId;
        }

        public string GetResellerId(string resellername)
        {
            try
            {
                SqlCommand objCmd = new SqlCommand(@" SELECT RESELLER_ID FROM ADMIN_TBL_RESELLER_DETAIL WHERE LOGIN_NAME=@resellername");
                objCmd.CommandType = CommandType.Text;
                objCmd.Parameters.AddWithValue("@resellername", resellername);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    string strId = Convert.ToString(objDataSet.Tables[0].Rows[0]["RESELLER_ID"]);
                    return strId;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
            return "";
        }

        public DataTable ResultSet
        {
            get;
            set;
        }

        public DataTable ResultTable
        {
            get;
            set;

        }
        public string ServerId
        {
            get;
            set;
        }
        public string ServerURL
        {
            get;
            set;
        }
        public string mBuzz_ServerIP
        {
            get;
            set;
        }
        public int mBuzz_port
        {
            get;
            set;
        }

        public string CompanyId
        {
            get;
            set;
        }

        public DateTime RegistrationDate
        {
            get;
            set;
        }

        public int StatusCode
        {
            get;
            set;
        }

        public string StatusDescription
        {
            get;
            set;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
namespace mFicientAdmin
{
    public class GetCompanyUnblockReasons
    {
        public enum REASON_FOR_UN_BLOCKING_COMPANY
        {
            //0 in database means not blocked.
            PaymentReceived = 1,
            SuspiciousActivityResolved = 2,
            OtherDisputeResolved = 3
        }
        public DataTable getReasonAsDataTable()
        {
            int iNoOfReasonsKnown = 3;
            int iStartingCode = 1;
            DataTable dtblReason = new DataTable();
            dtblReason.Columns.Add("REASON", typeof(string));
            dtblReason.Columns.Add("CODE", typeof(int));
            DataRow row = dtblReason.NewRow();
            for (int i = iStartingCode; i <= iNoOfReasonsKnown; i++)
            {
                addDataRowOfReasonToDTable(i, dtblReason);
            }
            return dtblReason;
        }
        void addDataRowOfReasonToDTable(int code, DataTable dtblReason)
        {
            DataRow row = dtblReason.NewRow();
            REASON_FOR_UN_BLOCKING_COMPANY eReasonForBlocking =
                (REASON_FOR_UN_BLOCKING_COMPANY)Enum.Parse(typeof(REASON_FOR_UN_BLOCKING_COMPANY),
                code.ToString());
            if (Enum.IsDefined(typeof(REASON_FOR_UN_BLOCKING_COMPANY), eReasonForBlocking))
            {
                switch (eReasonForBlocking)
                {
                    case REASON_FOR_UN_BLOCKING_COMPANY.PaymentReceived:
                        row = dtblReason.NewRow();
                        row["REASON"] = "Payment received";
                        row["CODE"] = (int)REASON_FOR_UN_BLOCKING_COMPANY.PaymentReceived;
                        dtblReason.Rows.Add(row);
                        break;
                    case REASON_FOR_UN_BLOCKING_COMPANY.SuspiciousActivityResolved:
                        row = dtblReason.NewRow();
                        row["REASON"] = "Suspicious Activity Resolved";
                        row["CODE"] = (int)REASON_FOR_UN_BLOCKING_COMPANY.SuspiciousActivityResolved;
                        dtblReason.Rows.Add(row);
                        break;
                    case REASON_FOR_UN_BLOCKING_COMPANY.OtherDisputeResolved:
                        row = dtblReason.NewRow();
                        row["REASON"] = "Other Dispute Resolved";
                        row["CODE"] = (int)REASON_FOR_UN_BLOCKING_COMPANY.OtherDisputeResolved;
                        dtblReason.Rows.Add(row);
                        break;

                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class GetAllRegisteredDevicesOfCompany
    {
        int _statusCode;
        string _companyId, _statusDescription;
        List<MFEDevice> _registeredDvc;

        


        #region Public Properties
        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }
        public List<MFEDevice> RegisteredDvc
        {
            get { return _registeredDvc; }
            private set { _registeredDvc = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }

        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }
        #endregion
        public GetAllRegisteredDevicesOfCompany(string companyId)
        {
            this.CompanyId = companyId;
        }
        public void Process()
        {
            try
            {
                SqlCommand cmd = this.sqlCmdToGetAllRegDev();
                DataSet dsRegDev = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsRegDev == null) throw new Exception();
                this.RegisteredDvc = new List<MFEDevice>();
                if (dsRegDev.Tables[0].Rows.Count > 0)
                {
                    this.RegisteredDvc = getRegisteredDevListFromDtble(dsRegDev.Tables[0]);
                }
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
            }
            catch (MficientException ex)
            {
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
        }
        string sqlQueryToGetAllRegDev()
        {
            return @"SELECT * FROM TBL_REGISTERED_DEVICE
                    WHERE COMPANY_ID = @COMPANY_ID";
        }
        SqlCommand sqlCmdToGetAllRegDev()
        {
            SqlCommand cmd = new SqlCommand(sqlQueryToGetAllRegDev());
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            return cmd;
        }
        List<MFEDevice> getRegisteredDevListFromDtble(DataTable regDevListDtbl)
        {
            if (regDevListDtbl == null) throw new ArgumentNullException();
            List<MFEDevice> lstRegDev = new List<MFEDevice>();
            foreach (DataRow row in regDevListDtbl.Rows)
            {
                lstRegDev.Add(getRegDevFromDRow(row));
            }
            return lstRegDev;
        }
        MFEDevice getRegDevFromDRow(DataRow regDevDtlRow)
        {
            if (regDevDtlRow == null) throw new ArgumentNullException();
            MFEDevice objDevice = new
            MFEDevice();
            objDevice.UserId = Convert.ToString(regDevDtlRow["USER_ID"]);
            objDevice.DeviceId = Convert.ToString(regDevDtlRow["DEVICE_ID"]);
            objDevice.DeviceType = Convert.ToString(regDevDtlRow["DEVICE_TYPE"]);
            objDevice.CompanyId = Convert.ToString(regDevDtlRow["COMPANY_ID"]);
            objDevice.DevicePushMsgId = Convert.ToString(regDevDtlRow["DEVICE_PUSH_MESSAGE_ID"]);
            return objDevice;
        }
    }
}
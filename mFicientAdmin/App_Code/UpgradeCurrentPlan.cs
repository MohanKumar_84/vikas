﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientAdmin
{
    public class UpgradeCurrentPlan
    {
        public UpgradeCurrentPlan()
        { 
        
        }

        public void Process()
        {
            try
            {
                string query = @"UPDATE ADMIN_TBL_COMPANY_CURRENT_PLAN
                                 SET PLAN_CODE = @PLAN_CODE, MAX_WORKFLOW = @MAX_WORKFLOW, PLAN_CHANGE_DATE = @PLAN_CHANGE_DATE, USER_CHARGE_PM = @USER_CHARGE_PM
                                 WHERE COMPANY_ID = @COMPANY_ID;";

                SqlCommand objSqlCommand = new SqlCommand(query);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                objSqlCommand.Parameters.AddWithValue("@PLAN_CODE", this.PlanCode);
                objSqlCommand.Parameters.AddWithValue("@MAX_WORKFLOW", Convert.ToInt32(this.MaxWorkFlow));
                objSqlCommand.Parameters.AddWithValue("@USER_CHARGE_PM", Convert.ToDouble(this.ChargePM));
                objSqlCommand.Parameters.AddWithValue("@PLAN_CHANGE_DATE",  DateTime.Now.Ticks);

                if (Utilities.ExecuteNonQueryRecord(objSqlCommand) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                    this.StatusDescription = ex.Message;
                }
            }
        }

        public void GetPlanList(string _MaxWorkFlow)
        {
            try
            {
                string strQuery = @"SELECT * FROM ADMIN_TBL_PLANS WHERE MAX_WORKFLOW > @MAX_WORKFLOW";

                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@MAX_WORKFLOW", _MaxWorkFlow);
                DataSet dsPlanList = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                if (dsPlanList != null)
                {
                    PlanList = dsPlanList.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                }
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }
        public string CompanyId
        {
            set;
            get;
        }
        public string PlanCode
        {
            set;
            get;
        }
        public DataTable PlanList
        {
            set;
            get;
        }
        public string MaxWorkFlow
        {
            set;
            get;
        }
        public string ChargePM
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
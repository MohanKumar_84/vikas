﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class MPluginnServerAdmin
    {
        public void DeleteServer(string serverid)
        {
            try
            {
                string sql = @"delete from ADMIN_TBL_MST_MP_SERVER where MP_SERVER_ID=@serverid";

                SqlCommand objSqlCmd = new SqlCommand(sql);
                objSqlCmd.CommandType = CommandType.Text;
                objSqlCmd.Parameters.AddWithValue("@serverid", serverid);
                if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_DELETE_ERROR;
                }
            }
            catch(Exception e)
            {
                this.StatusCode = ((int)DATABASE_ERRORS.RECORD_DELETE_ERROR);
                this.StatusDescription = e.Message;
            }
        }

       public string GetCompanyId(string MplugginServerId)
       {
           string CompanyId = string.Empty;
           try
           {
               SqlCommand cmd = new SqlCommand(@" select COMPANY_ID from ADMIN_TBL_SERVER_MAPPING where MPLUGIN_SERVER_ID=@serverid");
               cmd.Parameters.AddWithValue("@serverid", MplugginServerId);
               DataSet objDs = Utilities.SelectDataFromSQlCommand(cmd);
               if (objDs.Tables[0].Rows.Count > 0)
               {
                   CompanyId = Convert.ToString(objDs.Tables[0].Rows[0]["COMPANY_ID"]);
                   return CompanyId;
               }
               else
               {
                   CompanyId = "";
               }
           }
           catch(Exception e)
           {
               this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
               this.StatusDescription = e.Message;
           }
           return CompanyId;
        }

        public DataTable GetServerList()
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(@"select MP_SERVER_ID,MP_SERVER_NAME,MP_SERVER_IP_ADDRESS,MP_SERVER_PORT,IS_ENABLED,MP_HTTP_URL from ADMIN_TBL_MST_mp_server
                                                       order by MP_SERVER_NAME");
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public void MplugginServerDetails(string serverid)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select MP_SERVER_ID,MP_SERVER_NAME,MP_SERVER_IP_ADDRESS,MP_SERVER_PORT,IS_ENABLED,MP_HTTP_URL from ADMIN_TBL_MST_MP_SERVER where MP_SERVER_ID=@serverid");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@serverid", serverid);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public void UpdateServer(string serverid, string servername, string serverip, string port,bool IsEnabled,string Url)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"update ADMIN_TBL_MST_MP_SERVER set MP_SERVER_NAME=@servername,MP_SERVER_IP_ADDRESS=@serverip,MP_SERVER_PORT=@serverport,IS_ENABLED=@isenabled,MP_HTTP_URL=@url where MP_SERVER_ID=@serverid");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@serverid", serverid);
                ObjSqlCmd.Parameters.AddWithValue("@servername", servername);
                ObjSqlCmd.Parameters.AddWithValue("@serverip", serverip);
                ObjSqlCmd.Parameters.AddWithValue("@serverport", port);
                ObjSqlCmd.Parameters.AddWithValue("@isenabled", IsEnabled);
                ObjSqlCmd.Parameters.AddWithValue("@url", Url);
                if (Utilities.ExecuteNonQueryRecord(ObjSqlCmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public void AddServerDetails(string servername, string serverid, string serverip, string serverport,bool IsEnabled,string Url)
        {
            try
            {
                SqlCommand Sqlcmd = new SqlCommand(@"insert into ADMIN_TBL_MST_MP_SERVER(MP_SERVER_ID,MP_SERVER_NAME,MP_SERVER_IP_ADDRESS,MP_SERVER_PORT,IS_ENABLED,MP_HTTP_URL)
                                                values(@serverid,@servername,@serverip,@serverport,@isenabled,@url)");
                Sqlcmd.Parameters.AddWithValue("@servername", servername);
                Sqlcmd.Parameters.AddWithValue("@serverid", serverid);
                Sqlcmd.Parameters.AddWithValue("@serverip", serverip);
                Sqlcmd.Parameters.AddWithValue("@serverport", serverport);
                Sqlcmd.Parameters.AddWithValue("@isenabled", IsEnabled);
                Sqlcmd.Parameters.AddWithValue("@url", Url);

                if (Utilities.ExecuteNonQueryRecord(Sqlcmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public DataTable ResultTable
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public DataSet Resultset
        {
            get;
            set;
        }

        public string ServerId
        {
            get;
            set;
        }
    }
}
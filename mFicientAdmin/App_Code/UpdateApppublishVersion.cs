﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientAdmin
{
    public class UpdateApppublishVersion
    {

       
        public  UpdateApppublishVersion(string operatingsystem, int mobileappversionmajor, int mobileappversionminor, int mobileappversionrev, int minimummobileappversionmajor, int minimummobileappversionminor, int minimumappversionrev)
        {
            this.OperatingSystem = operatingsystem;
            this.MobileappversionMajor = mobileappversionmajor;
            this.Mobileappversionminor = mobileappversionminor;
            this.Mobileappversionrev = Mobileappversionrev;
            this.Minimummobileappversionmajor = minimummobileappversionmajor;
            this.Minimummobileappversionminor = minimummobileappversionminor;
            this.Minimumappversionrev = minimumappversionrev;
            UpdateProcess();

        }

        void UpdateProcess()
        {
            try
            {
                string sql = @"update  dbo.ADMIN_TBL_MST_MOBILE_DEVICE set MOBILE_APP_VERSION_MAJOR=@MOBILE_APP_VERSION_MAJOR,MOBILE_APP_VERSION_MINOR=@MOBILE_APP_VERSION_MINOR,
                              MOBILE_APP_VERSION_REV=@MOBILE_APP_VERSION_REV,MINIMUM_APP_VERSION_MAJOR1=@MINIMUM_APP_VERSION_MAJOR1,MINIMUM_APP_VERSION_MINOR1=@MINIMUM_APP_VERSION_MINOR1,
                           MINIMUM_APP_VERSION_REV1 =@MINIMUM_APP_VERSION_REV1 where PHONE_DEVICE_TYPE_CODE=@OS";
                SqlCommand objSqlCmd = new SqlCommand(sql);
                objSqlCmd.Parameters.AddWithValue("@OS",this.OperatingSystem);
                objSqlCmd.Parameters.AddWithValue("@MOBILE_APP_VERSION_MAJOR", this.MobileappversionMajor);
                objSqlCmd.Parameters.AddWithValue("@MOBILE_APP_VERSION_MINOR", this.Mobileappversionminor);
                objSqlCmd.Parameters.AddWithValue("@MOBILE_APP_VERSION_REV", this.Mobileappversionrev);
                objSqlCmd.Parameters.AddWithValue("@MINIMUM_APP_VERSION_MAJOR1",this.Minimummobileappversionmajor);
                objSqlCmd.Parameters.AddWithValue("@MINIMUM_APP_VERSION_MINOR1", this.Minimummobileappversionminor);
                objSqlCmd.Parameters.AddWithValue("@MINIMUM_APP_VERSION_REV1", this.Minimumappversionrev);
                objSqlCmd.CommandType = CommandType.Text;
                if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    this.StatusDescription = "Record Insert Error";
                }
            }
            catch
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                this.StatusDescription = "Record Insert Error";
            }
        }

        public string OperatingSystem
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public int MobileappversionMajor
        {
            set;
            get;
        }
        public int Mobileappversionminor
        {
            set;
            get;
        }

        public int Mobileappversionrev
        {
            get;
            set;
        }
        public int Minimummobileappversionmajor
        {
            get;
            set;
        }
        public int Minimummobileappversionminor
        {
            get;
            set;
        }

        public int Minimumappversionrev
        {
            get;
            set;
        }
        

    }
}
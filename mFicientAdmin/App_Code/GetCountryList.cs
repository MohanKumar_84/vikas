﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientAdmin
{
    public class GetCountryList
    {
        public GetCountryList()
        {
        }

        public void Process()
        {
            try
            {
                string strQuery ="select country_name,country_code,timezone,service_enabled from ADMIN_TBL_MST_COUNTRY;";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                   this.StatusDescription = "Record Not Found";
                   this.StatusCode=(int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public DataTable GetCountryName()
        {
            try
            {
                string strQuery = "select COUNTRY_NAME from ADMIN_TBL_MST_COUNTRY";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                //objSqlCommand.Parameters.AddWithValue("@loginname", loginname);
                objSqlCommand.CommandType = CommandType.Text;
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                return ResultTable = objDataSet.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
                return null;
            }
        }
     
        public DataTable ResultTable
        {
            set;
            get;
        }
        //public string DepartmentName
        //{
        //    set;
        //    get;
        //}
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }

    //public class EntityCollection : IEnumerable<Entity>
    //{ 

    //}
}
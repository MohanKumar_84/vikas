﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class UpdateplandetailsAdmin
    {
        public void UpdatePlan(string PlanId, string planname, int maxworkflow, int msgperday, int msgpermonth,bool isenabled,int MinUsers)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"update ADMIN_TBL_PLANS set  PLAN_NAME=@planname,
            MAX_WORKFLOW=@maxworkflow,PUSHMESSAGE_PERDAY=@msgperday,
                                                PUSHMESSAGE_PERMONTH=@msgpermonth,IS_ENABLED=@isenabled,MIN_USERS=@minusers
                                                where PLAN_ID=@planid");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@planid", PlanId);
                ObjSqlCmd.Parameters.AddWithValue("@planname", planname);
                ObjSqlCmd.Parameters.AddWithValue("@maxworkflow", maxworkflow);
                //ObjSqlCmd.Parameters.AddWithValue("@chargeinr", chargeinr);
                //ObjSqlCmd.Parameters.AddWithValue("@chargeusd", chargeusd);
                ObjSqlCmd.Parameters.AddWithValue("@msgperday", msgperday);
                ObjSqlCmd.Parameters.AddWithValue("@msgpermonth", msgpermonth);
                ObjSqlCmd.Parameters.AddWithValue("@isenabled", isenabled);
                ObjSqlCmd.Parameters.AddWithValue("@minusers",MinUsers);
                if (Utilities.ExecuteNonQueryRecord(ObjSqlCmd) > 0)
                {
                    StatusCode = 0;
                    StatusDescription = "";
                }
                else
                {
                    ((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString();
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public void UpdatePricePlan(decimal price, int frommonth, int tomonth, string currency, string plancode)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"update ADMIN_TBL_PLAN_PRICE set PRICE=@price where FROM_MONTH=@frommonth and TO_MONTH=@tomonth and CURRENCY=@currency and PLAN_CODE=@plancode");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@price", price);
                ObjSqlCmd.Parameters.AddWithValue("@frommonth", frommonth);
                ObjSqlCmd.Parameters.AddWithValue("@tomonth", tomonth);
                ObjSqlCmd.Parameters.AddWithValue("@currency", currency);
                ObjSqlCmd.Parameters.AddWithValue("@plancode", plancode);
                if (Utilities.ExecuteNonQueryRecord(ObjSqlCmd) > 0)
                {
                    StatusCode = 0;
                    StatusDescription = "";
                }
                else
                {
                    StatusCode = 1000;
                    StatusDescription = "Record cant be inserted";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                this.StatusDescription = e.Message;
            }
        }


        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public string PlanCode
        {
            get;
            set;
        }

        public string Planname
        {
            get;
            set;
        }

        public int maxworkflow
        {
            get;
            set;
        }

        public decimal chargeinr
        {
            get;
            set;
        }

        public decimal chargeusd
        {
            get;
            set;
        }

        public int msgperday
        {
            get;
            set;
        }

        public int msgpermonth
        {
            get;
            set;
        }


    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class SaveDefaultServerSetting
    {
        #region Save Mbuzz Server Default
        /// <summary>
        /// 
        /// </summary>
        /// <returns>was save successfull</returns>
        public static bool saveMbuzzServerDefault(string newDefaultMbuzzServerId)
        {
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                       int iReturnRowAffected =updateRemoveAllMbuzzDefaultServer(con, transaction);
                        iReturnRowAffected= updateNewMbuzzDefaultServer(newDefaultMbuzzServerId,
                            con,
                            transaction
                            );
                        if (iReturnRowAffected == 0)
                        {
                            throw new Exception();
                        }
                        transaction.Commit();
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }
        static int updateRemoveAllMbuzzDefaultServer(
            SqlConnection con,
            SqlTransaction transaction)
        {
            string strQuery = @"UPDATE ADMIN_TBL_MBUZZ_SERVER
                                SET IS_DEFAULT = 0
                                WHERE MBUZZ_SERVER_ID =(
                                SELECT MBUZZ_SERVER_ID 
                                FROM ADMIN_TBL_MBUZZ_SERVER
                                WHERE IS_DEFAULT = 1
                                AND ENABLED=1);";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            return cmd.ExecuteNonQuery();
        }
        static int updateNewMbuzzDefaultServer(
            string newDefaultServerId,
            SqlConnection con,
            SqlTransaction transaction)
        {
            string strQuery = @"UPDATE ADMIN_TBL_MBUZZ_SERVER
                                SET IS_DEFAULT = 1
                                WHERE MBUZZ_SERVER_ID = @MBUZZ_SERVER_ID
                                AND ENABLED =1";//just ensuring server is enabled.check is already done
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.Parameters.AddWithValue("@MBUZZ_SERVER_ID", newDefaultServerId);
            return cmd.ExecuteNonQuery();
        }
        #endregion

        #region Save Mplugin Server Default

        public static bool saveMPluginServerDefault(string newDefaultMPluginServerId)
        {
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        int iReturnRowAffected = updateRemoveAllMPluginDefaultServer(con, transaction);
                         iReturnRowAffected =
                            updateNewMpluginDefaultServer(
                            newDefaultMPluginServerId,
                            con,
                            transaction
                            );
                        if (iReturnRowAffected == 0)
                        {
                            throw new Exception();
                        }
                        transaction.Commit();
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }
        static int updateRemoveAllMPluginDefaultServer(
            SqlConnection con,
            SqlTransaction transaction)
        {
            string strQuery = @"UPDATE ADMIN_TBL_MST_MP_SERVER
                                SET IS_DEFAULT = 0
                                WHERE MP_SERVER_ID = (
                                SELECT MP_SERVER_ID 
                                FROM ADMIN_TBL_MST_MP_SERVER
                                WHERE IS_DEFAULT =1
                                AND IS_ENABLED =1);";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            return cmd.ExecuteNonQuery();
        }
        static int updateNewMpluginDefaultServer(
            string newDefaultServerId,
            SqlConnection con,
            SqlTransaction transaction)
        {
            string strQuery = @"UPDATE ADMIN_TBL_MST_MP_SERVER
                                SET IS_DEFAULT =1
                                WHERE MP_SERVER_ID = @MP_SERVER_ID
                                AND IS_ENABLED =1;";//just ensuring server is enabled.check is already done
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.Parameters.AddWithValue("@MP_SERVER_ID", newDefaultServerId);
            return cmd.ExecuteNonQuery();
        }
        #endregion

        #region Save Webservice Server Default
        public static bool saveWebServiceServerDefault(string newDefaultWSServerId)
        {
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                       int iReturnRowAffected= updateRemoveAllWSDefaultServer(con, transaction);
                       iReturnRowAffected = updateNewWSDefaultServer(
                            newDefaultWSServerId,
                            con,
                            transaction
                            );
                        if (iReturnRowAffected == 0)
                        {
                            throw new Exception();
                        }
                        transaction.Commit();
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }
        static int updateRemoveAllWSDefaultServer(
            SqlConnection con,
            SqlTransaction transaction)
        {
            string strQuery = @"UPDATE ADMIN_TBL_MST_SERVER
                                SET IS_DEFAULT = 0
                                WHERE SERVER_ID =
                                (SELECT SERVER_ID 
                                FROM ADMIN_TBL_MST_SERVER
                                WHERE IS_DEFAULT = 1 AND IS_ENABLED = 1
                                )";
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            return cmd.ExecuteNonQuery();
        }
        static int updateNewWSDefaultServer(
            string newDefaultServerId,
            SqlConnection con,
            SqlTransaction transaction)
        {
            string strQuery = @"UPDATE ADMIN_TBL_MST_SERVER
                                SET IS_DEFAULT =1
                                WHERE SERVER_ID = @SERVER_ID
                                AND IS_ENABLED = 1";//just ensuring server is enabled.check is already done
            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
            cmd.Parameters.AddWithValue("@SERVER_ID", newDefaultServerId);
            return cmd.ExecuteNonQuery();
        }
        #endregion
    }
}
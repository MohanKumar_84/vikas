﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class GetTeamListAdmin
    {
        public DataTable SalesPersonList(string MgrId)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select l.LOGIN_NAME,l.EMAIL,rr.RESELLER_NAME from ADMIN_TBL_LOGIN_USER_DETAIL l
                                                    inner join ADMIN_TBL_RESELLER_SALES_PERSONS r on l.LOGIN_ID=r.SELLER_ID
                                                    inner join ADMIN_TBL_RESELLER_DETAIL rr on r.RESELLER_ID=rr.RESELLER_ID
                                                    where l.MANAGER_ID=@managerid and l.IS_ENABLED=@isenabled");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@managerid", MgrId);
                ObjSqlCmd.Parameters.AddWithValue("@isenabled", 1);
                DataSet ds = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public DataTable ResellerList(string SalesId)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select LOGIN_NAME, RESELLER_NAME,EMAIL from ADMIN_TBL_RESELLER_DETAIL where IS_ENABLE=@isenabled and RESELLER_ID in
                                                        (select RESELLER_ID from 
                                                        ADMIN_TBL_RESELLER_SALES_PERSONS where SELLER_ID=@sellerid)");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@sellerid", SalesId);
                ObjSqlCmd.Parameters.AddWithValue("@isenabled", 1);
                DataSet ds = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public string GetMgrId(string loginname)
        {
            string Id = string.Empty;
            try
            {
                SqlCommand cmd = new SqlCommand(@"select LOGIN_ID from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@name");
                cmd.Parameters.AddWithValue("@name", loginname);
                DataSet objDs = Utilities.SelectDataFromSQlCommand(cmd);
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    Id = Convert.ToString(objDs.Tables[0].Rows[0]["LOGIN_ID"]);
                    return Id;
                }
                else
                {
                    return Id = "";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return Id;
        }

        
        public string GetSalesId(string loginname)
        {
            string Id = string.Empty;
            try
            {
                SqlCommand cmd = new SqlCommand(@"select LOGIN_ID from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@name");
                cmd.Parameters.AddWithValue("@name", loginname);
                DataSet objDs = Utilities.SelectDataFromSQlCommand(cmd);
                if (objDs.Tables[0].Rows.Count > 0)
                {
                   return Id = Convert.ToString(objDs.Tables[0].Rows[0]["LOGIN_ID"]);
                }
                else
                {
                    return Id="";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return Id;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
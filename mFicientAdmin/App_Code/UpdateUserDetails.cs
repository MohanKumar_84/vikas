﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class UpdateUserDetailsAdmin
    {

        public void UpdateUser(string userName,string email,byte isEnabled,string contactNo,string managerId,string countryCode,string city,string type,byte serverAllocation,string loginId)
        {
            try
            {
            string strQuery = @"UPDATE [ADMIN_TBL_LOGIN_USER_DETAIL]
                               SET [EMAIL] = @EMAIL
                                  ,[IS_ENABLED] = @IS_ENABLED
                                  ,[CONTACT_NUMBER] = @CONTACT_NUMBER
                                  ,[MANAGER_ID] = @MANAGER_ID
                                  ,[COUNTRY_CODE] = @COUNTRY_CODE
                                  ,[CITY] = @CITY
                                  ,[TYPE] = @TYPE
                                  ,[IS_SERVER_ALLOCATION] = @IS_SERVER_ALLOCATION
                                  ,[USER_NAME] = @USER_NAME
                             WHERE LOGIN_ID = @LoginID";
            //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
            //SqlCommand ObjSqlCmd = new SqlCommand(@"update ADMIN_TBL_LOGIN_USER_DETAIL set USER_NAME=@username,LOGIN_NAME=@loginname,COUNTRY_CODE=@country,TYPE=@type where LOGIN_NAME=@loginname");
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.AddWithValue("@EMAIL", email);
            cmd.Parameters.AddWithValue("@IS_ENABLED", isEnabled == 1 ? 1 : 0);
            cmd.Parameters.AddWithValue("@CONTACT_NUMBER", contactNo);
            cmd.Parameters.AddWithValue("@MANAGER_ID", managerId);
            cmd.Parameters.AddWithValue("@COUNTRY_CODE", countryCode);
            cmd.Parameters.AddWithValue("@CITY", city);
            cmd.Parameters.AddWithValue("@TYPE", type);
            cmd.Parameters.AddWithValue("@IS_SERVER_ALLOCATION", serverAllocation == 1 ? 1 : 0);
            cmd.Parameters.AddWithValue("@USER_NAME", userName);
            cmd.Parameters.AddWithValue("@LoginID", loginId);
        //    cmd.Parameters.AddWithValue("@LoginName", loginName);
           
                //con.Open();
                if (Utilities.ExecuteNonQueryRecord(cmd) > 0)
                {

                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = "Record Insert Error";
                    //throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = -1000;
                    this.StatusDescription = ex.Message;
                }
            }
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public string UserName
        {
            get;
            set;
        }
        public string LoginName
        {
            get;
            set;
        }

        public string Country
        {
            get;
            set;
        }

        public string Type
        {
            get;
            set;
        }

    }
}
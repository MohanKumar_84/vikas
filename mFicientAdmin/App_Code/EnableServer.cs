﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class EnableServerAdmin
    {
        public void AddServer(string servername, string serverid, string serverip, string serverurl, bool isenabled)
        {
            try
            {
                SqlCommand Sqlcmd = new SqlCommand(@"insert into ADMIN_TBL_MST_SERVER(SERVER_ID,SERVER_NAME,SERVER_IP_ADDRESS,SERVER_URL,IS_ENABLED)
                                                values(@serverid,@servername,@serverip,@serverurl,@isenabled)");
                Sqlcmd.Parameters.AddWithValue("@servername", servername);
                Sqlcmd.Parameters.AddWithValue("@serverid", serverid);
                Sqlcmd.Parameters.AddWithValue("@serverip", serverip);
                Sqlcmd.Parameters.AddWithValue("@serverurl", serverurl);
                Sqlcmd.Parameters.AddWithValue("@isenabled", isenabled);
                if (Utilities.ExecuteNonQueryRecord(Sqlcmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                }
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public int StatusCode
        {
            get;
            set;
        }

        public string StatusDescription
        {
            get;
            set;
        }
    }
}
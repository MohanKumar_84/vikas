﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientAdmin
{
    public enum MODAL_POP_UP_NAME
    {
        //getModalPopUpWidth is used to return the width of the pop up
        USER_DETAIL,
        DEVICE_DETAILS_REPEATER,
        SUB_ADMIN_DETAILS,
        MANAGE_GROUP_POPUPS,
        POST_BACK_CONFIRMATION,
        CHANGE_PASSWORD,
    }
    public class MficientConstants
    {
        internal const string INTERNAL_ERROR = "Internal server error.";
        internal const string ADMIN_PANEL_URL = @"https://admin.mficient.com";
        internal const int SESSION_TIMEOUT_SECONDS = 1800;
        internal const int SESSION_VALIDITY_SECONDS = 3600;
        internal const string STANDARD_TIMEZONE_ID = "India Standard Time";
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class GetMBuzzServerDetailsAdmin
    {
        public void MBuzzServerDetails(string serverId)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select MBUZZ_SERVER_NAME,MBUZZ_SERVER_ID,SERVER_IP,PORT_NUMBER,ENABLED from ADMIN_TBL_MBUZZ_SERVER Where MBUZZ_SERVER_ID=@SERVER_ID ");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@SERVER_ID", serverId);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                   this.StatusCode= (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                }
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }
         public DataTable ResultTable
        {
            set;
            get;
        }
         public int StatusCode
         {
             set;
             get;
         }
         public string StatusDescription
         {
             set;
             get;
         }

        
        }

    }

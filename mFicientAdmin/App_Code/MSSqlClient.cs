﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace mFicientAdmin
{
    public class MSSqlClient
    {
        public enum CONNECTION_STRING_FOR_DB
        {
            MFICIENT,
            MGRAM,
            ADMINDB
        }
        /// <summary>
        /// Description :Test connction ( Sql Query And Parameter )
        /// </summary>
        public static bool TestMsSQLConnection(string connectionString)
        {
            SqlConnection Conn = null;
            bool isConnected = false;
            try
            {
                Conn = new SqlConnection(connectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    if (Conn != null)
                    {
                        Conn.Dispose();
                        Conn = null;
                    }
                }
                else
                {
                    isConnected = true;
                    Conn.Close();
                }
            }
            catch
            {
            }
            return isConnected;
        }
        /// <summary>
        /// Description :Select Data According To SqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static DataSet SelectDataFromSQlCommand(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            try
            {
                SqlConnectionOpen(out objSqlConnection);
                _SqlCommand.Connection = objSqlConnection;

                DataSet ObjDataSet = new DataSet();

                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

                objSqlDataAdapter.Fill(ObjDataSet);

                //SqlConnectionClose(objSqlConnection);

                return ObjDataSet;
            }
            catch
            {
                return null;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }
        }
        public static DataSet SelectDataFromSQlCommand(string connectionString, SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            try
            {
                SqlConnectionOpen(out objSqlConnection, connectionString);
                _SqlCommand.Connection = objSqlConnection;
                DataSet ObjDataSet = new DataSet();
                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);
                objSqlDataAdapter.Fill(ObjDataSet);
                return ObjDataSet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }
        }


        /// <summary>
        /// Description :SqlConnection Open Function
        /// </summary>
        public static void SqlConnectionOpen(out SqlConnection Conn)
        {
            try
            {
                Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }
        public static void SqlConnectionOpen(out SqlConnection Conn, string ConnectionString)
        {
            try
            {
                Conn = new SqlConnection(ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }
        /// <summary>
        /// Description :SqlConnection Close Function
        /// </summary>
        public static void SqlConnectionClose(SqlConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open))
                {
                    Conn.Close();
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Description :SqlConnection Open Function
        /// </summary>
        public static void SqlConnectionOpenAdmin(out SqlConnection Conn)
        {
            try
            {
                Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }
        /// <summary>
        /// Description :Select Data According To SqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static DataSet SelectDataFromSQlCommandAdminData(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            SqlConnectionOpenAdmin(out objSqlConnection);
            _SqlCommand.Connection = objSqlConnection;

            DataSet ObjDataSet = new DataSet();

            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

            objSqlDataAdapter.Fill(ObjDataSet);

            SqlConnectionClose(objSqlConnection);

            return ObjDataSet;
        }

        /// <summary>
        /// Description :Data Process (Insert, Update, Delete ) According To SqlCommand ( Sql Query And Parameter ) Admin Database
        /// </summary>
        public static int ExecuteNonQueryRecordAdmin(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            SqlConnectionOpenAdmin(out objSqlConnection);
            _SqlCommand.Connection = objSqlConnection;

            int i = _SqlCommand.ExecuteNonQuery();

            SqlConnectionClose(objSqlConnection);

            return i;
        }

        /// <summary>
        /// Description :Data Process (Insert, Update, Delete ) According To SqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static int ExecuteNonQueryRecord(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            SqlConnectionOpen(out objSqlConnection);
            _SqlCommand.Connection = objSqlConnection;

            int i = _SqlCommand.ExecuteNonQuery();

            SqlConnectionClose(objSqlConnection);

            return i;
        }
        public static int ExecuteNonQueryRecord(SqlCommand _SqlCommand, string _connectionString)
        {
            SqlConnection objSqlConnection = null;
            SqlConnectionOpen(out objSqlConnection, _connectionString);
            _SqlCommand.Connection = objSqlConnection;
            int i = _SqlCommand.ExecuteNonQuery();

            SqlConnectionClose(objSqlConnection);

            return i;
        }
        public static DataSet SelectDataFromDifferentConnection(string ConnectionString, SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            try
            {
                SqlConnectionOpen(out objSqlConnection, ConnectionString);

                _SqlCommand.Connection = objSqlConnection;

                DataSet ObjDataSet = new DataSet();

                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

                objSqlDataAdapter.Fill(ObjDataSet);

                //SqlConnectionClose(objSqlConnection);

                return ObjDataSet;
            }
            catch
            {
                return null;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }
        }

        public static int ExecuteNonQueryRecordDotask(string ConnectionString, SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            try
            {
                SqlConnectionOpen(out objSqlConnection, ConnectionString);

                _SqlCommand.Connection = objSqlConnection;

                int i = _SqlCommand.ExecuteNonQuery();

                return i;
            }
            catch
            {
                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }
            //return 0;
        }

        public static string getConnectionStringFromWebConfig(CONNECTION_STRING_FOR_DB db)
        {
            string strConnectionString = String.Empty;
            switch (db)
            {
                case CONNECTION_STRING_FOR_DB.MFICIENT:
                case CONNECTION_STRING_FOR_DB.ADMINDB:
                    strConnectionString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
                    break;
                case CONNECTION_STRING_FOR_DB.MGRAM:
                    strConnectionString = ConfigurationManager.ConnectionStrings["ConStrmGramDB"].ConnectionString;
                    break;
            }
            return strConnectionString;
        }
    }
}
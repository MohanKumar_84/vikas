﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class GetTrialPlanList
    {
        public GetTrialPlanList(string planCode)
        {
            PlanCode = planCode;
        }
        public GetTrialPlanList()
        {

        }
        public void Process()
        {
            try
            {
                string strQuery = getSqlQuery();
                SqlCommand cmd = new SqlCommand(strQuery);
                if (!String.IsNullOrEmpty(PlanCode))
                {
                    cmd.Parameters.AddWithValue("@PlanCode", PlanCode);
                }
                cmd.CommandType = CommandType.Text;
                DataSet dsPlanDtls = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsPlanDtls != null)
                {
                    PlanDetails = dsPlanDtls.Tables[0];
                }
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }
        string getSqlQuery()
        {
            string strQuery = string.Empty ;
            try
            {
                if (String.IsNullOrEmpty(PlanCode))
                {
                    strQuery = @"SELECT * FROM ADMIN_TBL_TRIAL_PLANS";
                }
                else
                {
                    strQuery = @"SELECT * FROM ADMIN_TBL_TRIAL_PLANS WHERE PLAN_CODE = @PlanCode";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return strQuery;
        }

        public DataTable PlanDetails
        {
            set;
            get;
        }

        public DataTable PlanDiscountList
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string PlanCode
        {
            get;
            set;
        }
    }
}
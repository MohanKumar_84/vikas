﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


namespace mFicientAdmin
{
    public class ResetUsersPassAdmin
    {

         public ResetUsersPassAdmin()
        {}

         public ResetUsersPassAdmin(string userName, string subAdminId)
         {
             this.UserName=userName;
            // this.SubAdminId=SubAdminId;
         }

         public int Check(string LoginName)
         {
             try
             {
                 SqlCommand cmd = new SqlCommand(@"if exists(select LOGIN_ID from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@loginname)
                                            select 'def' as RESULT
                                            else if exists(select ADMIN_ID from ADMIN_TBL_ADMIN_LOGIN where ADMIN_NAME=@loginname)
                                            select 'abc' as RESULT
                                            else if exists(select reseller_id from ADMIN_TBL_RESELLER_DETAIL where login_name=@loginname)
                                            select 'ghi' as RESULT");
                 cmd.Parameters.AddWithValue("loginname", LoginName);
                 DataSet objDs = Utilities.SelectDataFromSQlCommand(cmd);
                 ResultTable = objDs.Tables[0];
                 string Result = Convert.ToString(ResultTable.Rows[0]["RESULT"]);
                     if (Result == "abc")
                     {
                       return intStatusCode =1;
                     }
                     else if (Result == "def")
                     {
                        return intStatusCode = 2;
                     }
                     else if (Result == "ghi")
                     {
                        return intStatusCode = 3;
                     }
             }
             catch(Exception e)
             {
                 this.StatusDescription = e.Message;
             }
             return intStatusCode;
         }


        int intStatusCode;
        public void Process(string type,string Loginname)
        {
            try
            {
                int intExecuteRecordsCount = -1;

                string NewPassword = GetPassword(Loginname);

                SqlCommand objSqlCommand = new SqlCommand(@" select LOGIN_ID,EMAIL from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@loginname and TYPE=@type;");
                objSqlCommand.CommandType = CommandType.Text;
                // objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", CompanyID);
                objSqlCommand.Parameters.AddWithValue("@loginname", Loginname);

                objSqlCommand.Parameters.AddWithValue("@type", type);

                DataTable dt = Utilities.SelectDataFromSQlCommand(objSqlCommand).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    objSqlCommand = new SqlCommand(@"INSERT INTO ADMIN_TBL_USERS_RESET_PASSWORD_LOG(USER_ID,NEW_ACCESSCODE,ACCESSCODE_RESET_DATETIME,IS_USED,IS_EXPIRED) 
                        VALUES(@USER_ID,@ACCESSCODE,@ACCESSCODE_RESET_DATETIME,@IS_USED,@IS_expired);");

                    objSqlCommand.CommandType = CommandType.Text;

                    objSqlCommand.Parameters.AddWithValue("@ACCESSCODE", NewPassword);
                    objSqlCommand.Parameters.AddWithValue("@USER_ID", dt.Rows[0]["LOGIN_ID"].ToString());
                    objSqlCommand.Parameters.AddWithValue("@ACCESSCODE_RESET_DATETIME", DateTime.Now.Ticks);
                    objSqlCommand.Parameters.AddWithValue("@IS_USED", 0);
                    objSqlCommand.Parameters.AddWithValue("@IS_expired", 0);

                    intExecuteRecordsCount = Utilities.ExecuteNonQueryRecord(objSqlCommand);
                    if (intExecuteRecordsCount > 0)
                    {
                        Utilities.SendInfoEmail(dt.Rows[0]["EMAIL"].ToString(), "You forget your password so to access your account please use " + NewPassword + " . Please ", "Forget password of biz mobile");
                        this.intStatusCode = 0;
                    }
                    else
                    {
                        this.intStatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    }
                }
                else
                {
                    this.intStatusCode = 1000001;
                }
            }
            catch 
            {
                this.intStatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
        }


        public void ProcessReseller(string LoginName)
        {
            try
            {
                int intExecuteRecordsCount = -1;

                string NewPassword = GetPassword(LoginName);

                SqlCommand objSqlCommand = new SqlCommand(@" select RESELLER_ID,EMAIL from ADMIN_TBL_RESELLER_DETAIL where LOGIN_NAME=@loginname");
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@loginname", LoginName);
                DataTable dt = Utilities.SelectDataFromSQlCommand(objSqlCommand).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    objSqlCommand = new SqlCommand(@"INSERT INTO ADMIN_TBL_USERS_RESET_PASSWORD_LOG(USER_ID,NEW_ACCESSCODE,ACCESSCODE_RESET_DATETIME,IS_USED,IS_EXPIRED) 
                        VALUES(@USER_ID,@ACCESSCODE,@ACCESSCODE_RESET_DATETIME,@IS_USED,@IS_expired);");

                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Parameters.AddWithValue("@ACCESSCODE", NewPassword);
                    objSqlCommand.Parameters.AddWithValue("@USER_ID", dt.Rows[0]["RESELLER_ID"].ToString());
                    objSqlCommand.Parameters.AddWithValue("@ACCESSCODE_RESET_DATETIME", DateTime.Now.Ticks);
                    objSqlCommand.Parameters.AddWithValue("@IS_USED", 0);
                    objSqlCommand.Parameters.AddWithValue("@IS_expired", 0);

                    intExecuteRecordsCount = Utilities.ExecuteNonQueryRecord(objSqlCommand);
                    if (intExecuteRecordsCount > 0)
                    {
                        Utilities.SendInfoEmail(dt.Rows[0]["EMAIL"].ToString(), "You forget your password so to access your account please use " + NewPassword + " . Please ", "Forget password of biz mobile");
                        this.intStatusCode = 0;
                    }
                    else
                    {
                        this.intStatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    }
                }
                else
                {
                    this.intStatusCode = 1000001;
                }
            }
            catch 
            {
                this.intStatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
        }

        //public string strResellerName, strUserName, strResellerEmail, strUsersEmail;
        //public string GetResellerName(string resellerid)
        //{
        //    SqlCommand objSqlCommand = new SqlCommand(@"select LOGIN_NAME from ADMIN_TBL_RESELLER_DETAIL where LOGIN_ID=@loginid");
        //    objSqlCommand.CommandType = CommandType.Text;
        //    objSqlCommand.Parameters.AddWithValue("@loginname", resellerid);
        //    DataSet objDataset = Utilities.SelectDataFromSQlCommand(objSqlCommand);
         
        //    if (objDataset.Tables[0].Rows.Count > 0)
        //    {
        //        strResellerName = objDataset.Tables[0].Rows[0]["LOGIN_NAME"].ToString();
        //    }
        //    return strResellerName;

        //}

        //string strResellerId;
        //public string GetResellerId(string loginname)
        //{
        //    try
        //    {
        //        SqlCommand objSqlCommand = new SqlCommand(@"select RESELLER_ID from ADMIN_TBL_RESELLER_DETAIL where LOGIN_NAME=@loginname");
        //        objSqlCommand.CommandType = CommandType.Text;
        //        objSqlCommand.Parameters.AddWithValue("@loginname", loginname);
        //        DataSet objDataset = Utilities.SelectDataFromSQlCommand(objSqlCommand);

        //        if (objDataset.Tables[0].Rows.Count > 0)
        //        {
        //            strResellerId = objDataset.Tables[0].Rows[0]["RESELLER_ID"].ToString();
        //        }
        //    }
        //    catch
        //    {
        //        this.intStatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
        //    }
        //    return strResellerId;
        //}


        //string Loginname;
        //public string Select(string Type, string userName)
        //{
        //    try
        //    {
        //        SqlCommand objSqlCommand = new SqlCommand(@"select LOGIN_NAME from ADMIN_TBL_LOGIN_USER_DETAIL where USER_NAME=@username and TYPE=@type");
        //        objSqlCommand.CommandType = CommandType.Text;
        //        objSqlCommand.Parameters.AddWithValue("@type", Type);
        //        objSqlCommand.Parameters.AddWithValue("@username", userName);
        //        DataSet objDataset = Utilities.SelectDataFromSQlCommand(objSqlCommand);
        //        if (objDataset.Tables[0].Rows.Count > 0)
        //        {
        //            Loginname = objDataset.Tables[0].Rows[0]["LOGIN_NAME"].ToString();
        //        }
        //    }
        //    catch
        //    {
        //        this.intStatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
        //    }
        //    return Loginname;
        //}

        //public string SelectByType(string type,string loginname)
        //{
        //    try
        //    {
        //    SqlCommand cmd = new SqlCommand(@"select EMAIL from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@loginname");
        //    cmd.Parameters.AddWithValue("@loginname", loginname);
        //    DataSet objDataset = Utilities.SelectDataFromSQlCommand(cmd);
        //    if (objDataset.Tables[0].Rows.Count > 0)
        //    {
        //        SqlCommand ObjSqlCmd = new SqlCommand(@" select LOGIN_NAME from ADMIN_TBL_LOGIN_USER_DETAIL  where TYPE=@type");
        //        ObjSqlCmd.CommandType = CommandType.Text;
        //        ObjSqlCmd.Parameters.AddWithValue("@type", type);
        //        DataSet ds = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            strUserName = ds.Tables[0].Rows[0]["LOGIN_NAME"].ToString();

        //        }
        //    }
        //    }
        //    catch
        //    {
        //        this.intStatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
        //    }
        //        return strUserName;
        //}
        


        //public string GetResellerEmail(string name)
        //{
        //    try
        //    {
        //        SqlCommand ObjSqlCmd = new SqlCommand(@"select email from ADMIN_TBL_RESELLER_DETAIL where LOGIN_NAME=@loginname");
        //        ObjSqlCmd.CommandType = CommandType.Text;
        //        ObjSqlCmd.Parameters.AddWithValue("@type", name);
        //        DataSet ds = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            strResellerEmail = ds.Tables[0].Rows[0]["EMAIL"].ToString();
        //            return strResellerEmail;
        //        }
        //    }
        //    catch
        //    {
        //        this.intStatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
        //    }
        //    return "";

        //}

        //public string GetUsersEmail(string UType)
        //{
        //    try
        //    {
        //        SqlCommand ObjSqlCmd = new SqlCommand(@"select email from ADMIN_TBL_LOGIN_USER_DETAIL where TYPE=@type");
        //        ObjSqlCmd.CommandType = CommandType.Text;
        //        ObjSqlCmd.Parameters.AddWithValue("@type", UType);
        //        DataSet ds = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            strUsersEmail = ds.Tables[0].Rows[0]["EMAIL"].ToString();
        //            return strUsersEmail;
        //        }
        //    }
        //    catch
        //    {
        //        this.intStatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
        //    }
        //    return "";
        //}


        public string GetType(string username)
        {
            string strType = string.Empty;
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select TYPE from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@loginname");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@loginname", username);
               // ObjSqlCmd.Parameters.AddWithValue("@type", Type);
                DataSet ds = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return strType = ds.Tables[0].Rows[0]["TYPE"].ToString();
                }
                else
                {
                    return strType = "";
                }
            }
            catch(Exception e)
            {
                this.intStatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return strType;
        }

        public string GetPassword(string _UserId)
        {
            string strGuid, strPassword;
            do
            {
                Guid objGuid = new Guid();
                objGuid = Guid.NewGuid();
                strGuid = Convert.ToString(objGuid);
                string[] stringArray = { "0", "0", "O", "o", "I", "L", "1", "l", "s", "S", "5", "9", "Q", "q", "-" };
                foreach (string strItem in stringArray)
                {
                    strGuid = strGuid.Replace(strItem, "");
                }
            }
            while (strGuid.Length < 12);

            Boolean bolHashCodeNotEmpty = false;
            strPassword = strGuid.Substring(0, 6);
            do
            {
                strPassword = Utilities.GetMd5Hash(strPassword);
                if (strPassword.Length > 0)
                    bolHashCodeNotEmpty = true;
            }
            while (bolHashCodeNotEmpty == false);
            return strPassword;
        }


        public DataTable ResultTable
        {
            set;
            get;
        }
        public int StatusCode
        {
            get
            {
                return intStatusCode;
            }
        }
        public string UserName
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }

       
        
    }
}
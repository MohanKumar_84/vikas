﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class GetPlanList
    {
        
        
        #region RemoveAfterTesting
        public GetPlanList()
        {
            
        }
        
        #endregion
        /// <summary>
        /// used in company registration where all the plan list needs to be shown
        /// </summary>
        public GetPlanList(bool completeList)
        {
            this.CompleteList = true;
        }
        public GetPlanList(string planCode)
        {
            this.PlanCode = planCode;
            this.CompleteList = false;
            this.Currency = "";
            this.Months =0;
        }
        public GetPlanList(string currency, int months)
        {
            this.Currency = currency;
            this.Months = months;
            this.CompleteList = false;
        }
        public GetPlanList(string planCode, string currency, int months)
        {
            PlanCode = planCode;
            this.Currency = currency;
            this.Months = months;
            this.CompleteList = false;
        }
        //added on 13-8-2012.to get plan list whose workflow is greater than or less than the current plan
        /// <summary>
        /// 
        /// </summary>
        /// <param name="planCode"></param>
        /// <param name="isUpgradedList">true if we want to get the list whose work flows are greater than the workflow provided</param>
        public GetPlanList(string maxWorkFlow, bool isUpgradedList, string currency, int months)
        {
            MaxWorkFlow = maxWorkFlow;
            IsUpgradedList = isUpgradedList;
            this.Currency = currency;
            this.Months = months;
            this.CompleteList = false;
        }
        public void Process()
        {
            try
            {
                StatusCode = -1000;//for error
                string strQuery = getSqlQuery();

                SqlCommand cmd = new SqlCommand(strQuery);
                if (!String.IsNullOrEmpty(PlanCode))
                {
                    cmd.Parameters.AddWithValue("@PlanCode", PlanCode);
                }
                if (!String.IsNullOrEmpty(MaxWorkFlow))
                {
                    cmd.Parameters.AddWithValue("@MAX_WORKFLOW", MaxWorkFlow);
                }
                if (!CompleteList && !String.IsNullOrEmpty(this.Currency) && this.Months!=0)
                {
                    if (this.Months <= 12)
                    {
                        cmd.Parameters.AddWithValue("@Month", this.Months);
                    }
                    cmd.Parameters.AddWithValue("@Currency", this.Currency);
                }
                cmd.CommandType = CommandType.Text;
                DataSet dsPlanDtls = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsPlanDtls != null)
                {
                    PlanDetails = dsPlanDtls.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    //throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                    this.StatusCode = -1000;
                }
            }
            catch (Exception ex)
            {

                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = -1000;
                }

                //if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                //{
                //    throw ex;
                //}
                this.StatusCode = -1000;
                this.StatusDescription = ex.Message;

            }
        }
        public void PlanDiscounts()
        {
            StatusCode = -1000;//for error
            //StatusCode = -1000;

            try
            {
                string strQuery = getDisountSqlQuery();

                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                DataSet dsPlanDiscounts = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsPlanDiscounts != null)
                {
                    PlanDiscountList = dsPlanDiscounts.Tables[0];
                    this.StatusCode = 0;
                }
                else
                {
                    this.StatusCode = -1000;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = -1000;
                }
                this.StatusCode = -1000;
            }
        }
        string getSqlQuery()
        {
            StatusCode = -1000;//for error
            string strQuery="";
           // string strQuery = "";

            //added on 13-08-2012 this first condition check
            if (!CompleteList)
            {

                if (String.IsNullOrEmpty(MaxWorkFlow))
                {
                    if (String.IsNullOrEmpty(PlanCode) && !String.IsNullOrEmpty(this.Currency) && this.Months != 0)
                    {
                        //old query changed on 25/20/2012.Tables were changed and we have to get the price from a new table.Admin_tbl_plan_price
                        //strQuery = @"SELECT * FROM ADMIN_TBL_PLANS WHERE IS_ENABLED =1";//changed on 17/9/2012 Added admin_//WHERE IS_ENABLED =1 18/9/2012
                        if (this.Months <= 12)
                        {
                            strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
                                ON Plans.PLAN_CODE=Price.PLAN_CODE
                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
                                AND Plans.IS_ENABLED =1
                                AND PRICE.CURRENCY = @Currency";
                        }
                        else
                        {
                            strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
                                ON Plans.PLAN_CODE=Price.PLAN_CODE
                                WHERE Price.TO_MONTH=12
                                AND Plans.IS_ENABLED =1
                                AND PRICE.CURRENCY = @Currency";
                        }
                    }
                    else if (!String.IsNullOrEmpty(PlanCode) && !String.IsNullOrEmpty(this.Currency) && this.Months != 0)
                    {
                        //old query changed on 25/10/2012
                        //strQuery = @"SELECT * FROM ADMIN_TBL_PLANS WHERE PLAN_CODE = @PlanCode AND IS_ENABLED =1";//changed on 17/9/2012 Added admin_//WHERE IS_ENABLED =1 18/9/2012
                        if (this.Months <= 12)
                        {
                            strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
                                ON Plans.PLAN_CODE=Price.PLAN_CODE
                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
                                AND Plans.IS_ENABLED =1
                                AND PRICE.CURRENCY = @Currency
                                AND Plans.PLAN_CODE = @PlanCode";
                        }
                        else
                        {
                            strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
                                ON Plans.PLAN_CODE=Price.PLAN_CODE
                                WHERE Price.TO_MONTH=12
                                AND Plans.IS_ENABLED =1
                                AND PRICE.CURRENCY = @Currency
                                AND Plans.PLAN_CODE = @PlanCode";
                        }
                    }
                    //gets the plan details with all the price details 1-2 months etc
                    else if (!String.IsNullOrEmpty(PlanCode) && String.IsNullOrEmpty(this.Currency) && this.Months == 0)
                    {
                        strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                                        INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
                                        ON Plans.PLAN_CODE=Price.PLAN_CODE
                                        WHERE Plans.PLAN_CODE = @PlanCode
                                        AND IS_ENABLED = 1";
                    }
                }
                else
                {
                    if (IsUpgradedList)
                    {
                        //old query
                        //strQuery = @"SELECT * FROM ADMIN_TBL_PLANS WHERE MAX_WORKFLOW > @MAX_WORKFLOW AND IS_ENABLED =1";//changed on 17/9/2012 Added admin_//AND IS_ENABLED =1 18/9/2012
                        if (this.Months <= 12)
                        {
                            strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
                                ON Plans.PLAN_CODE=Price.PLAN_CODE
                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
                                AND Plans.IS_ENABLED =1
                                AND PRICE.CURRENCY = @Currency
                                AND MAX_WORKFLOW > @MAX_WORKFLOW;";
                        }
                        else
                        {
                            strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
                                ON Plans.PLAN_CODE=Price.PLAN_CODE
                                WHERE Price.TO_MONTH=12
                                AND Plans.IS_ENABLED =1
                                AND PRICE.CURRENCY = @Currency
                                AND MAX_WORKFLOW > @MAX_WORKFLOW;";
                        }
                    }
                    else
                    {
                        //old query
                        //strQuery = @"SELECT * FROM ADMIN_TBL_PLANS WHERE MAX_WORKFLOW < @MAX_WORKFLOW AND IS_ENABLED =1";//changed on 17/9/2012 Added admin_//AND IS_ENABLED =1 18/9/2012
                        if (this.Months <= 12)
                        {
                            strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
                                ON Plans.PLAN_CODE=Price.PLAN_CODE
                                WHERE @Month BETWEEN Price.FROM_MONTH AND Price.TO_MONTH
                                AND Plans.IS_ENABLED =1
                                AND PRICE.CURRENCY = @Currency
                                AND MAX_WORKFLOW < @MAX_WORKFLOW;";
                        }
                        else
                        {
                            strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                                INNER JOIN ADMIN_TBL_PLAN_PRICE AS Price
                                ON Plans.PLAN_CODE=Price.PLAN_CODE
                                WHERE Price.TO_MONTH=12
                                AND Plans.IS_ENABLED =1
                                AND PRICE.CURRENCY = @Currency
                                AND MAX_WORKFLOW < @MAX_WORKFLOW;";
                        }
                    }
                }
            }
            else
            {
                strQuery = @"SELECT * FROM ADMIN_TBL_PLANS AS Plans
                             WHERE Plans.IS_ENABLED =1";
            }

            return strQuery;
        }

        string getDisountSqlQuery()
        {
            string strQuery=string.Empty;
            try
            {
                strQuery = @"SELECT * FROM ADMIN_TBL_PLAN_DISCOUNT WHERE ENABLED = 1";//changed on 17/9/2012 Added admin_   
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return strQuery;
        }
        public DataTable PlanDetails
        {
            set;
            get;
        }

        public DataTable PlanDiscountList
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string PlanCode
        {
            get;
            set;
        }
        public string MaxWorkFlow
        {
            get;
            set;
        }
        public bool IsUpgradedList
        {
            get;
            set;
        }
        public int Months
        {
            get;
            set;
        }
        public string Currency
        {
            get;
            set;
        }
        public bool CompleteList
        {
            get;
            set;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class AddsellerDetailsAdmin
    {
        public void AddSeller(string sellername, string email, string contactnum, DateTime CreatedOn)
        {
            SqlCommand objSqlCmd = new SqlCommand(@"INSERT INTO ADMIN_TBL_SELLER_DETAIL (SELLER_NAME, EMAIL,Is_Enable, Contact_Number,Created_On,Seller_Id) VALUES 
                        (@SELLER_NAME,@EMAIL,@Contact_Number,@Created_On,@Seller_Id)");

            objSqlCmd.Parameters.AddWithValue("@SELLER_NAME", sellername);
            objSqlCmd.Parameters.AddWithValue("@EMAIL", email);
            objSqlCmd.Parameters.AddWithValue("@Contact_Number", contactnum);

            objSqlCmd.Parameters.AddWithValue("@Created_On", DateTime.Now.Ticks);

            if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
            {
                StatusCode = 0;
                StatusDescription = "";
            }
            else
            {
                StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;

            }
        }

        public DataTable GetSellerList()
        {
            SqlCommand sqlCommand = new SqlCommand("select * from ADMIN_TBL_SELLER_DETAIL");
            DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
            return ds.Tables[0];
        }

        public string SellerId
        {
            set;
            get;
        }
        public string SellerName
        {
            set;
            get;
        }
        public string Email
        {
            set;
            get;
        }
        public string ContactNumber
        {
            get;
            set;
        }
        public DateTime CreatedOn
        {
            get;
            set;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}

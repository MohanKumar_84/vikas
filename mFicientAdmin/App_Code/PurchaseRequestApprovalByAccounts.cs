﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class PurchaseRequestApprovalByAccounts
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="planCode"></param>
        /// <param name="maxWorkFlow"></param>
        /// <param name="maxUsers"></param>
        /// <param name="userChargePerMonth"></param>
        /// <param name="currencyType"></param>
        /// <param name="validity"></param>
        /// <param name="pushMessagePerDay"></param>
        /// <param name="pushMessagePerMonth"></param>
        /// <param name="priceWithOutDiscount"></param>
        /// <param name="discountPercent"></param>
        /// <param name="actualPrice"></param>
        /// <param name="transactionType"></param>
        /// <param name="planName"></param>
        /// <param name="amountReceived"></param>
        /// <param name="isNewPurchase"></param>
        public PurchaseRequestApprovalByAccounts(
            string companyId, string planCode, int maxWorkFlow, int maxUsers,
            double userChargePerMonth, string currencyType, double validity, int pushMessagePerDay
            , int pushMessagePerMonth, double priceWithOutDiscount, double discountPercent,
            double actualPrice, string transactionType, string planName,
            int amountReceived, bool isNewPurchase)
        {
            this.CompanyId = companyId;
            this.PlanCode = planCode;
            this.MaxWorkFlow = maxWorkFlow;
            this.MaxUsers = maxUsers;
            this.UserChargePerMonth = userChargePerMonth;
            this.CurrencyType = currencyType;
            this.Validity = validity;
            this.PushMessagePerDay = pushMessagePerDay;
            this.PushMessagePerMonth = pushMessagePerMonth;
            this.PriceWithOutDiscount = priceWithOutDiscount;
            this.DiscountPercent = discountPercent;
            this.ActualPrice = actualPrice;
            this.TransactionType = transactionType;
            this.AmountReceived = amountReceived;
            this.PlanName = planName;
            this.PurchaseDatetime = DateTime.UtcNow.Ticks;
            //this.PlanChangeDate = DateTime.UtcNow.Ticks;
            this.IsNewPurchase = isNewPurchase;
        }

        /// <summary>
        /// for new purchase 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="planCode"></param>
        /// <param name="transactionType"></param>
        /// <param name="amountReceived"></param>
        public PurchaseRequestApprovalByAccounts(
            string companyId, string planCode, string transactionType,
            int amountReceived, bool isNewPurchase,
            bool isNewPurchaseAfterTrial, string currency,
            int months)
        {
            this.CompanyId = companyId;
            this.AmountReceived = amountReceived;
            this.TransactionType = transactionType;
            this.PlanCode = planCode;
            this.IsNewPurchaseAfterTrial = isNewPurchaseAfterTrial;
            this.IsNewPurchase = isNewPurchase;
            this.CurrencyType = currency;
            this.NoOfMonths = months;
        }
        public void Process()
        {
            StatusCode = -1000;//for error
            SqlTransaction sqlTransaction = null;
            SqlConnection con;
            Utilities.SqlConnectionOpen(out con);
            try
            {
                using (con)
                {
                    sqlTransaction = con.BeginTransaction();
                    if (IsNewPurchase)
                    {
                        //saveAdminCurrentPlan(con, sqlTransaction);
                        //saveCompanyPlanPurchaseHistory(con, sqlTransaction);
                        //saveCurrentPlan(con, sqlTransaction);

                        //is purchase after trial
                        //first update is trial in admin and cp table 
                        //update updatedon in administrator
                        //update admin current plan
                        //update current plan
                        if (!IsNewPurchaseAfterTrial)
                        {
                            GetPlanRequestsLogDetailsForAcc objPlanPurchaseLog = new
                                 GetPlanRequestsLogDetailsForAcc(
                                    this.CompanyId,
                                this.PlanCode,
                                this.TransactionType);
                            objPlanPurchaseLog.Process();
                            if (objPlanPurchaseLog.StatusCode == 0 &&
                                objPlanPurchaseLog.PlanPurchaseLog != null)
                            {

                                CompanyPlanChangesHelpers objPlanChangeHelpers =
                                    new CompanyPlanChangesHelpers(objPlanPurchaseLog.PlanPurchaseLog);
                                
                                double balanceAmnt = 
                                    objPlanChangeHelpers.getBalanceAmtAfterDeductingThisMonthChg(
                                    this.AmountReceived
                                    );
                                
                                long lngUpdatedOn = DateTime.UtcNow.Ticks;
                                transferCompanyDetailData(con, sqlTransaction);
                                transferCompanyAdministratorData(con, sqlTransaction, lngUpdatedOn);
                                transferCompanyCurrentPlanData(con, sqlTransaction);
                                updatePlanPurchaseLog(con, sqlTransaction, lngUpdatedOn);
                                saveDefualtCompanyAccSettings(sqlTransaction, con);
                                updateAdminCompanyCurrentPlanBalance(
                                    this.PlanCode,
                                    balanceAmnt,
                                    con,
                                    sqlTransaction);
                                updateCPCompanyCurrentPlanBalance(this.PlanCode,
                                    balanceAmnt,
                                    con,
                                    sqlTransaction
                                    );
                            }
                            else
                            {
                                throw new Exception("Could not get Purchase log");
                            }
                        }
                        else
                        {
                            processWhenTrialToPurchase(con, sqlTransaction);
                        }
                        //commit transaction
                        sqlTransaction.Commit();
                        StatusCode = 0;
                        StatusDescription = "";
                    }
                    else//in case of upgrade, downgrade ,renew,add users changes
                    {
                        updateAdminCurrentPlan(con, sqlTransaction);
                        updateCurrentPlan(con, sqlTransaction);
                        saveCompanyPlanPurchaseHistory(con, sqlTransaction);
                        //purchase log 
                        //request status changed 
                        updatePlanPurchaseLog(con, sqlTransaction, DateTime.UtcNow.Ticks);
                        updatePlanRequestStatus(con, sqlTransaction);

                        //commit transaction
                        sqlTransaction.Commit();
                        StatusCode = 0;
                        StatusDescription = "";
                    }

                }
            }
            catch (SqlException)
            {
                StatusDescription = "Internal server error";
                StatusCode = -1000;
            }
            catch (Exception)
            {
                StatusDescription = "Internal server error";
                StatusCode = -1000;
            }
        }


        void updateAdminCurrentPlan(SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                string strQuery = "";
                if ((PLAN_RENEW_OR_CHANGE_REQUESTS)Enum.Parse(typeof(PLAN_RENEW_OR_CHANGE_REQUESTS), this.TransactionType) == PLAN_RENEW_OR_CHANGE_REQUESTS.RENEW)
                {
                    strQuery = @"UPDATE [ADMIN_TBL_COMPANY_CURRENT_PLAN]
                                   SET [PLAN_CODE] = @PLAN_CODE
                                      ,[MAX_WORKFLOW] = @MAX_WORKFLOW
                                      ,[MAX_USER] = @MAX_USER
                                      ,[USER_CHARGE_PM] = @USER_CHARGE_PM
                                      ,[CHARGE_TYPE] = @CHARGE_TYPE
                                      ,[VALIDITY] = @VALIDITY
                                      ,[PURCHASE_DATE]=@PURCHASE_DATE
                                      ,[PLAN_CHANGE_DATE] = @PLAN_CHANGE_DATE
                                      ,[PUSHMESSAGE_PERDAY] = @PUSHMESSAGE_PERDAY
                                      ,[PUSHMESSAGE_PERMONTH] = @PUSHMESSAGE_PERMONTH
                                      ,[FEATURE3] = @FEATURE3
                                      ,[FEATURE4] = @FEATURE4
                                      ,[FEATURE5] = @FEATURE5
                                      ,[FEATURE6] = @FEATURE6
                                      ,[FEATURE7] = @FEATURE7
                                      ,[FEATURE8] = @FEATURE8
                                      ,[FEATURE9] = @FEATURE9
                                      ,[FEATURE10] = @FEATURE10
                                 WHERE COMPANY_ID = @CompanyId
                                ";
                }
                else
                {
                    strQuery = @"UPDATE [ADMIN_TBL_COMPANY_CURRENT_PLAN]
                                   SET [PLAN_CODE] = @PLAN_CODE
                                      ,[MAX_WORKFLOW] = @MAX_WORKFLOW
                                      ,[MAX_USER] = @MAX_USER
                                      ,[USER_CHARGE_PM] = @USER_CHARGE_PM
                                      ,[CHARGE_TYPE] = @CHARGE_TYPE
                                      ,[VALIDITY] = @VALIDITY
                                      ,[PLAN_CHANGE_DATE] = @PLAN_CHANGE_DATE
                                      ,[PUSHMESSAGE_PERDAY] = @PUSHMESSAGE_PERDAY
                                      ,[PUSHMESSAGE_PERMONTH] = @PUSHMESSAGE_PERMONTH
                                      ,[FEATURE3] = @FEATURE3
                                      ,[FEATURE4] = @FEATURE4
                                      ,[FEATURE5] = @FEATURE5
                                      ,[FEATURE6] = @FEATURE6
                                      ,[FEATURE7] = @FEATURE7
                                      ,[FEATURE8] = @FEATURE8
                                      ,[FEATURE9] = @FEATURE9
                                      ,[FEATURE10] = @FEATURE10
                                 WHERE COMPANY_ID = @CompanyId
                                ";
                }
                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("PLAN_CODE", PlanCode);
                cmd.Parameters.AddWithValue("MAX_WORKFLOW", this.MaxWorkFlow);
                cmd.Parameters.AddWithValue("MAX_USER", this.MaxUsers);
                cmd.Parameters.AddWithValue("USER_CHARGE_PM", this.UserChargePerMonth);
                cmd.Parameters.AddWithValue("CHARGE_TYPE", this.CurrencyType);
                cmd.Parameters.AddWithValue("VALIDITY", this.Validity);
                if ((PLAN_RENEW_OR_CHANGE_REQUESTS)Enum.Parse(typeof(PLAN_RENEW_OR_CHANGE_REQUESTS), this.TransactionType) == PLAN_RENEW_OR_CHANGE_REQUESTS.RENEW)
                {
                    cmd.Parameters.AddWithValue("PURCHASE_DATE", DateTime.UtcNow.Ticks);
                }

                cmd.Parameters.AddWithValue("PLAN_CHANGE_DATE", DateTime.UtcNow.Ticks);
                cmd.Parameters.AddWithValue("PUSHMESSAGE_PERDAY", this.PushMessagePerDay);
                cmd.Parameters.AddWithValue("PUSHMESSAGE_PERMONTH", this.PushMessagePerMonth);
                cmd.Parameters.AddWithValue("FEATURE3", 0);
                cmd.Parameters.AddWithValue("FEATURE4", 0);
                cmd.Parameters.AddWithValue("FEATURE5", 0);
                cmd.Parameters.AddWithValue("FEATURE6", 0);
                cmd.Parameters.AddWithValue("FEATURE7", 0);
                cmd.Parameters.AddWithValue("FEATURE8", 0);
                cmd.Parameters.AddWithValue("FEATURE9", 0);
                cmd.Parameters.AddWithValue("FEATURE10", 0);
                cmd.Parameters.AddWithValue("CompanyId", this.CompanyId);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        void updateCurrentPlan(SqlConnection con, SqlTransaction transaction)
        {
            long dateOfUpdation = DateTime.UtcNow.Ticks;
            try
            {
                string strQuery = "";
                if ((PLAN_RENEW_OR_CHANGE_REQUESTS)Enum.Parse(typeof(PLAN_RENEW_OR_CHANGE_REQUESTS), this.TransactionType) == PLAN_RENEW_OR_CHANGE_REQUESTS.RENEW)
                {
                    strQuery = @"UPDATE [TBL_COMPANY_CURRENT_PLAN]
                                   SET [PLAN_CODE] = @PLAN_CODE
                                      ,[MAX_WORKFLOW] = @MAX_WORKFLOW
                                      ,[MAX_USER] = @MAX_USER
                                      ,[USER_CHARGE_PM] = @USER_CHARGE_PM
                                      ,[CHARGE_TYPE] = @CHARGE_TYPE
                                      ,[VALIDITY] = @VALIDITY
                                      ,[PURCHASE_DATE]=@PURCHASE_DATE
                                      ,[PLAN_CHANGE_DATE] = @PLAN_CHANGE_DATE
                                      ,[PUSHMESSAGE_PERDAY] = @PUSHMESSAGE_PERDAY
                                      ,[PUSHMESSAGE_PERMONTH] = @PUSHMESSAGE_PERMONTH
                                      ,[FEATURE3] = @FEATURE3
                                      ,[FEATURE4] = @FEATURE4
                                      ,[FEATURE5] = @FEATURE5
                                      ,[FEATURE6] = @FEATURE6
                                      ,[FEATURE7] = @FEATURE7
                                      ,[FEATURE8] = @FEATURE8
                                      ,[FEATURE9] = @FEATURE9
                                      ,[FEATURE10] = @FEATURE10
                                 WHERE COMPANY_ID = @CompanyId
                                ";
                }
                else
                {
                    strQuery = @"UPDATE [TBL_COMPANY_CURRENT_PLAN]
                                   SET [PLAN_CODE] = @PLAN_CODE
                                      ,[MAX_WORKFLOW] = @MAX_WORKFLOW
                                      ,[MAX_USER] = @MAX_USER
                                      ,[USER_CHARGE_PM] = @USER_CHARGE_PM
                                      ,[CHARGE_TYPE] = @CHARGE_TYPE
                                      ,[VALIDITY] = @VALIDITY
                                      ,[PLAN_CHANGE_DATE] = @PLAN_CHANGE_DATE
                                      ,[PUSHMESSAGE_PERDAY] = @PUSHMESSAGE_PERDAY
                                      ,[PUSHMESSAGE_PERMONTH] = @PUSHMESSAGE_PERMONTH
                                      ,[FEATURE3] = @FEATURE3
                                      ,[FEATURE4] = @FEATURE4
                                      ,[FEATURE5] = @FEATURE5
                                      ,[FEATURE6] = @FEATURE6
                                      ,[FEATURE7] = @FEATURE7
                                      ,[FEATURE8] = @FEATURE8
                                      ,[FEATURE9] = @FEATURE9
                                      ,[FEATURE10] = @FEATURE10
                                 WHERE COMPANY_ID = @CompanyId
                                ";
                }
                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("PLAN_CODE", PlanCode);
                cmd.Parameters.AddWithValue("MAX_WORKFLOW", this.MaxWorkFlow);
                cmd.Parameters.AddWithValue("MAX_USER", this.MaxUsers);
                cmd.Parameters.AddWithValue("USER_CHARGE_PM", this.UserChargePerMonth);
                cmd.Parameters.AddWithValue("CHARGE_TYPE", this.CurrencyType);
                cmd.Parameters.AddWithValue("VALIDITY", this.Validity);
                if ((PLAN_RENEW_OR_CHANGE_REQUESTS)Enum.Parse(typeof(PLAN_RENEW_OR_CHANGE_REQUESTS), this.TransactionType) == PLAN_RENEW_OR_CHANGE_REQUESTS.RENEW)
                {
                    cmd.Parameters.AddWithValue("PURCHASE_DATE", dateOfUpdation);
                }
                cmd.Parameters.AddWithValue("PLAN_CHANGE_DATE", dateOfUpdation);
                cmd.Parameters.AddWithValue("PUSHMESSAGE_PERDAY", this.PushMessagePerDay);
                cmd.Parameters.AddWithValue("PUSHMESSAGE_PERMONTH", this.PushMessagePerMonth);
                cmd.Parameters.AddWithValue("FEATURE3", 0);
                cmd.Parameters.AddWithValue("FEATURE4", 0);
                cmd.Parameters.AddWithValue("FEATURE5", 0);
                cmd.Parameters.AddWithValue("FEATURE6", 0);
                cmd.Parameters.AddWithValue("FEATURE7", 0);
                cmd.Parameters.AddWithValue("FEATURE8", 0);
                cmd.Parameters.AddWithValue("FEATURE9", 0);
                cmd.Parameters.AddWithValue("FEATURE10", 0);
                cmd.Parameters.AddWithValue("CompanyId", this.CompanyId);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }

        }


        void saveCompanyPlanPurchaseHistory(SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                string strTransactionId = Utilities.GetMd5Hash(this.CompanyId + this.PurchaseDatetime + this.PlanCode);
                string strQuery = @"INSERT INTO [TBL_COMPANY_PLAN_PURCHASE_HISTORY]
                               ([COMPANY_ID]
                               ,[PLAN_CODE]
                               ,[MAX_WORKFLOW]
                               ,[MAX_USER]
                               ,[USER_CHARGE_PM]
                               ,[CHARGE_TYPE]
                               ,[VALIDITY]
                               ,[PRICE_WITHOUT_DISCOUNT]
                               ,[DISCOUNT_PER]
                               ,[ACTUAL_PRICE]
                               ,[PURCHASE_DATE]
                               ,[TRANSACTION_ID]
                               ,[TRANSACTION_TYPE]
                               ,[PLAN_NAME])
                         VALUES
                               (@COMPANY_ID
                               ,@PLAN_CODE
                               ,@MAX_WORKFLOW
                               ,@MAX_USER
                               ,@USER_CHARGE_PM
                               ,@CHARGE_TYPE
                               ,@VALIDITY
                               ,@PRICE_WITHOUT_DISCOUNT
                               ,@DISCOUNT_PER
                               ,@ACTUAL_PRICE
                               ,@PURCHASE_DATE
                               ,@TRANSACTION_ID
                               ,@TRANSACTION_TYPE
                               ,@PLAN_NAME)";
                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("COMPANY_ID", this.CompanyId);
                cmd.Parameters.AddWithValue("PLAN_CODE", this.PlanCode);
                cmd.Parameters.AddWithValue("MAX_WORKFLOW", this.MaxWorkFlow);
                cmd.Parameters.AddWithValue("MAX_USER", this.MaxUsers);
                cmd.Parameters.AddWithValue("USER_CHARGE_PM", this.UserChargePerMonth);
                cmd.Parameters.AddWithValue("CHARGE_TYPE", this.CurrencyType);
                cmd.Parameters.AddWithValue("VALIDITY", this.Validity);
                cmd.Parameters.AddWithValue("PRICE_WITHOUT_DISCOUNT", this.PriceWithOutDiscount);
                cmd.Parameters.AddWithValue("DISCOUNT_PER", this.DiscountPercent);
                cmd.Parameters.AddWithValue("ACTUAL_PRICE", this.ActualPrice);
                cmd.Parameters.AddWithValue("PURCHASE_DATE", PurchaseDatetime);
                cmd.Parameters.AddWithValue("TRANSACTION_ID", strTransactionId);
                cmd.Parameters.AddWithValue("TRANSACTION_TYPE", this.TransactionType);
                cmd.Parameters.AddWithValue("PLAN_NAME", this.PlanName);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }

        }


        void transferCompanyDetailData(SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                string strQuery = @"
                                    INSERT INTO TBL_COMPANY_ADMINISTRATOR
                                SELECT @CompanyId,[ADMIN_ID],[EMAIL_ID],[ACCESS_CODE],[FIRST_NAME]
                                      ,[LAST_NAME],[STREET_ADDRESS1],[STREET_ADDRESS2]
                                      ,[CITY_NAME],[STATE_NAME],[COUNTRY_CODE]
                                      ,[ZIP],[DATE_OF_BIRTH] ,[REGISTRATION_DATETIME]
                                      ,[GENDER],[MIDDLE_NAME],[STREET_ADDRESS3]
                                      ,[MOBILE],[IS_TRIAL]
                                 FROM ADMIN_TBL_COMPANY_ADMINISTRATOR
                                WHERE ADMIN_ID =(SELECT CmpAdmin.ADMIN_ID FROM ADMIN_TBL_COMPANY_ADMINISTRATOR CmpAdmin
		                                INNER JOIN ADMIN_TBL_COMPANY_DETAIL AS CmpDtl
		                                ON CmpAdmin.ADMIN_ID = CmpDtl.ADMIN_ID
		                                WHERE CmpDtl.COMPANY_ID = @CompanyId)";

                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                //Utilities.ExecuteNonQueryRecord(cmd);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        void transferCompanyAdministratorData(SqlConnection con, SqlTransaction transaction, long updatedOn)
        {
            try
            {
                string strQuery = @"INSERT INTO TBL_COMPANY_DETAIL([COMPANY_ID]
                                   ,[COMPANY_NAME],[REGISTRATION_NO]
                                   ,[STREET_ADDRESS1],[STREET_ADDRESS2]
                                   ,[STREET_ADDRESS3],[CITY_NAME]
                                   ,[STATE_NAME],[COUNTRY_CODE]
                                   ,[ZIP],[ADMIN_ID]
                                   ,[LOGO_IMAGE_NAME],[SUPPORT_EMAIL]
                                   ,[SUPPORT_CONTACT],[UPDATED_ON]
                                   ,[TIMEZONE_ID]
                                    ,[IS_BLOCKED]
                                    ,[BLOCK_UNBLOCK_ID])
                                    SELECT [COMPANY_ID],[COMPANY_NAME],[REGISTRATION_NO]
                                          ,[STREET_ADDRESS1],[STREET_ADDRESS2],[STREET_ADDRESS3]
                                          ,[CITY_NAME],[STATE_NAME],[COUNTRY_CODE]
                                          ,[ZIP],[ADMIN_ID],[LOGO_IMAGE_NAME]
                                          ,[SUPPORT_EMAIL],[SUPPORT_CONTACT]
                                          ,@UpdatedOn,[TIMEZONE_ID],[IS_BLOCKED],[BLOCK_UNBLOCK_ID]
                                     FROM ADMIN_TBL_COMPANY_DETAIL
                                    WHERE COMPANY_ID = @CompanyId";

                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.Parameters.AddWithValue("@UpdatedOn", updatedOn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        void transferCompanyCurrentPlanData(SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                string strQuery = @"INSERT INTO TBL_COMPANY_CURRENT_PLAN
                                    SELECT * FROM ADMIN_TBL_COMPANY_CURRENT_PLAN
                                    WHERE COMPANY_ID = @CompanyId";

                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        void updatePlanPurchaseLog(SqlConnection con, SqlTransaction transaction, long paymentDate)
        {
            try
            {
                string strQuery = @"UPDATE [ADMIN_TBL_PLAN_PURCHASE_LOG]
                                   SET [GET_PAYMENT] = @GET_PAYMENT
                                      ,[GET_PAYMENT_DATE] = @GET_PAYMENT_DATE
                                      ,[AMOUNT_RECEIVED] = @AMOUNT_RECEIVED
                                      ,[APPROVED]=1
                                      ,[APPROVED_DATE]=@ApprovedDate
                                 WHERE COMPANY_ID = @CompanyId
                                 AND PLAN_CODE = @PlanCode
                                 AND TRANSACTION_TYPE = @TransactionType";

                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("@GET_PAYMENT", 1);
                cmd.Parameters.AddWithValue("@GET_PAYMENT_DATE", paymentDate);
                cmd.Parameters.AddWithValue("@AMOUNT_RECEIVED", this.AmountReceived);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.Parameters.AddWithValue("@PlanCode", this.PlanCode);
                cmd.Parameters.AddWithValue("@TransactionType", this.TransactionType);
                cmd.Parameters.AddWithValue("@ApprovedDate", paymentDate);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void updatePlanRequestStatus(SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                string strQuery = @"UPDATE [ADMIN_TBL_PLAN_ORDER_REQUEST]
                                SET [STATUS] = @AccStatus
                                WHERE COMPANY_ID = @CompanyId AND PLAN_CODE = @PlanCode AND REQUEST_TYPE = @RequestType AND STATUS=@Status";
                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("@AccStatus", PLAN_REQUEST_STATUS.ACCOUNTS_APPROVED);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.Parameters.AddWithValue("@PlanCode", this.PlanCode);
                cmd.Parameters.AddWithValue("@RequestType", this.TransactionType);
                cmd.Parameters.AddWithValue("@Status", PLAN_REQUEST_STATUS.SALES_APPROVED);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #region Balance Amnt Update When Paid Plan is Approved
        void updateAdminCompanyCurrentPlanBalance(string planCode,
            double balanceAmount,
            SqlConnection con,
            SqlTransaction transaction)
        {
            string strQuery = @"UPDATE ADMIN_TBL_COMPANY_CURRENT_PLAN
                                SET BALANCE_AMOUNT = @BALANCE_AMOUNT
                                ,NEXT_MONTH_PLAN = @NEXT_MONTH_PLAN
                                ,NEXT_MONTH_PLAN_LAST_UPDATED = @NEXT_MONTH_PLAN_LAST_UPDATED
                                WHERE COMPANY_ID = @COMPANY_ID";
            SqlCommand cmd = new SqlCommand(strQuery,
                con,
                transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@BALANCE_AMOUNT", balanceAmount);
            cmd.Parameters.AddWithValue("@NEXT_MONTH_PLAN", planCode);
            cmd.Parameters.AddWithValue("@NEXT_MONTH_PLAN_LAST_UPDATED", 0);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            cmd.ExecuteNonQuery();
        }
        void updateCPCompanyCurrentPlanBalance(string planCode,
            double balanceAmount,
            SqlConnection con,
            SqlTransaction transaction)
        {
            string strQuery = @"UPDATE TBL_COMPANY_CURRENT_PLAN
                                SET BALANCE_AMOUNT = @BALANCE_AMOUNT
                                ,NEXT_MONTH_PLAN = @NEXT_MONTH_PLAN
                                ,NEXT_MONTH_PLAN_LAST_UPDATED = @NEXT_MONTH_PLAN_LAST_UPDATED
                                WHERE COMPANY_ID = @COMPANY_ID";
            SqlCommand cmd = new SqlCommand(strQuery,
                con,
                transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@BALANCE_AMOUNT", balanceAmount);
            cmd.Parameters.AddWithValue("@NEXT_MONTH_PLAN", planCode);
            cmd.Parameters.AddWithValue("@NEXT_MONTH_PLAN_LAST_UPDATED", 0);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            cmd.ExecuteNonQuery();
        }
        #endregion

        #region PlanChanged From trial to purchase

        void processWhenTrialToPurchase(SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                long lngPlanChangeDate = DateTime.UtcNow.Ticks;
                GetNewRegisteredCompanyForAccApproval objNewRegisteredCompany = new GetNewRegisteredCompanyForAccApproval(this.CompanyId, this.PlanCode, this.TransactionType);
                objNewRegisteredCompany.Process();
                GetPlanList objPlanList = new GetPlanList(this.PlanCode, this.CurrencyType, this.NoOfMonths);
                objPlanList.Process();
                if (objNewRegisteredCompany.StatusCode == 0 && objPlanList.StatusCode == 0)
                {
                    DataTable dtblRegisteredCompanyDtl = objNewRegisteredCompany.ResultTable;
                    DataTable dtblPlanDtl = objPlanList.PlanDetails;

                    MFEPlanPurchaseLog objPlanPurchaseLog = new MFEPlanPurchaseLog();
                    objPlanPurchaseLog.PlanCode = Convert.ToString(dtblRegisteredCompanyDtl.Rows[0]["PLAN_CODE"]);
                    objPlanPurchaseLog.MaxWorkFlow = Convert.ToInt32(dtblRegisteredCompanyDtl.Rows[0]["MAX_WORKFLOW"]);
                    objPlanPurchaseLog.MaxUsers = Convert.ToInt32(dtblRegisteredCompanyDtl.Rows[0]["MAX_USER"]);
                    objPlanPurchaseLog.UserChargePerMonth = Convert.ToDouble(dtblRegisteredCompanyDtl.Rows[0]["USER_CHARGE_PM"]);
                    objPlanPurchaseLog.ChargeType = Convert.ToString(dtblRegisteredCompanyDtl.Rows[0]["CHARGE_TYPE"]);
                    objPlanPurchaseLog.Validity = Convert.ToInt32(dtblRegisteredCompanyDtl.Rows[0]["VALIDITY"]);

                    CompanyPlanChangesHelpers objPlanChangeHelper = new CompanyPlanChangesHelpers(objPlanPurchaseLog);
                    double dblBalanceAmnt = objPlanChangeHelper.getBalanceAmtAfterDeductingThisMonthChg(this.AmountReceived);


                    if (dtblRegisteredCompanyDtl.Rows.Count > 0 && dtblPlanDtl.Rows.Count > 0)
                    {
                        updateAdminCurrentPlanWhenTrialToPurchase(
                            con, transaction,
                            dtblRegisteredCompanyDtl, dtblPlanDtl,
                            lngPlanChangeDate, dblBalanceAmnt);
                        updateCompanyCurrentPlanWhenTrialToPurchase(
                            con, transaction, dtblRegisteredCompanyDtl,
                            dtblPlanDtl, lngPlanChangeDate, dblBalanceAmnt);
                        updateAdminTblAdministratorWhenTrialToPurchase(con, transaction);
                        updateCPTblAdministratorWhenTrialToPurchase(con, transaction);
                        //changes in plan purchase log //changes in Plan Request Status
                        updatePlanPurchaseLog(con, transaction, lngPlanChangeDate);
                        updatePlanRequestStatus(con, transaction);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        void updateAdminCurrentPlanWhenTrialToPurchase(SqlConnection con, SqlTransaction transaction,
            DataTable registeredCompanyDetail, DataTable planDetails,
            long planChangeDate,
            double balanceAmount)
        {
            try
            {
                string strQuery = @"UPDATE [ADMIN_TBL_COMPANY_CURRENT_PLAN]
                                   SET[PLAN_CODE] = @PLAN_CODE
                                      ,[MAX_WORKFLOW] = @MAX_WORKFLOW
                                      ,[MAX_USER] = @MAX_USER
                                      ,[USER_CHARGE_PM] = @USER_CHARGE_PM
                                      ,[CHARGE_TYPE] = @CHARGE_TYPE
                                      ,[VALIDITY] = @VALIDITY
                                      ,[PLAN_CHANGE_DATE] = @PLAN_CHANGE_DATE
                                      ,[PUSHMESSAGE_PERDAY] = @PUSHMESSAGE_PERDAY
                                      ,[PUSHMESSAGE_PERMONTH] = @PUSHMESSAGE_PERMONTH
                                      ,BALANCE_AMOUNT=@BALANCE_AMOUNT
                                      ,NEXT_MONTH_PLAN =@NEXT_MONTH_PLAN
                                      ,NEXT_MONTH_PLAN_LAST_UPDATED=@NEXT_MONTH_PLAN_LAST_UPDATED
                                      WHERE COMPANY_ID = @CompanyId;";
                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("@PLAN_CODE", (string)registeredCompanyDetail.Rows[0]["PLAN_CODE"]);
                cmd.Parameters.AddWithValue("@MAX_WORKFLOW", (int)registeredCompanyDetail.Rows[0]["MAX_WORKFLOW"]);
                cmd.Parameters.AddWithValue("@MAX_USER", (int)registeredCompanyDetail.Rows[0]["MAX_USER"]);
                cmd.Parameters.AddWithValue("@USER_CHARGE_PM", (decimal)registeredCompanyDetail.Rows[0]["USER_CHARGE_PM"]);
                cmd.Parameters.AddWithValue("@CHARGE_TYPE", (string)registeredCompanyDetail.Rows[0]["CHARGE_TYPE"]);
                cmd.Parameters.AddWithValue("@VALIDITY", Convert.ToDouble(registeredCompanyDetail.Rows[0]["VALIDITY"]));
                cmd.Parameters.AddWithValue("@PLAN_CHANGE_DATE", planChangeDate);
                cmd.Parameters.AddWithValue("@PUSHMESSAGE_PERDAY", (int)planDetails.Rows[0]["PUSHMESSAGE_PERDAY"]);
                cmd.Parameters.AddWithValue("@PUSHMESSAGE_PERMONTH", (int)planDetails.Rows[0]["PUSHMESSAGE_PERMONTH"]);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.Parameters.AddWithValue("@BALANCE_AMOUNT", balanceAmount);
                cmd.Parameters.AddWithValue("@NEXT_MONTH_PLAN", (string)registeredCompanyDetail.Rows[0]["PLAN_CODE"]);
                cmd.Parameters.AddWithValue("@NEXT_MONTH_PLAN_LAST_UPDATED", 0);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        void updateCompanyCurrentPlanWhenTrialToPurchase(
            SqlConnection con, SqlTransaction transaction,
            DataTable registeredCompanyDetail, DataTable planDetails,
            long planChangeDate,
            double balanceAmount)
        {
            try
            {
                string strQuery = @"UPDATE [TBL_COMPANY_CURRENT_PLAN]
                                   SET[PLAN_CODE] = @PLAN_CODE
                                      ,[MAX_WORKFLOW] = @MAX_WORKFLOW
                                      ,[MAX_USER] = @MAX_USER
                                      ,[USER_CHARGE_PM] = @USER_CHARGE_PM
                                      ,[CHARGE_TYPE] = @CHARGE_TYPE
                                      ,[VALIDITY] = @VALIDITY
                                      ,[PLAN_CHANGE_DATE] = @PLAN_CHANGE_DATE
                                      ,[PUSHMESSAGE_PERDAY] = @PUSHMESSAGE_PERDAY
                                      ,[PUSHMESSAGE_PERMONTH] = @PUSHMESSAGE_PERMONTH
                                      ,BALANCE_AMOUNT=@BALANCE_AMOUNT
                                      ,NEXT_MONTH_PLAN =@NEXT_MONTH_PLAN
                                      ,NEXT_MONTH_PLAN_LAST_UPDATED=@NEXT_MONTH_PLAN_LAST_UPDATED
                                     WHERE COMPANY_ID = @CompanyId;";
                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("@PLAN_CODE", (string)registeredCompanyDetail.Rows[0]["PLAN_CODE"]);
                cmd.Parameters.AddWithValue("@MAX_WORKFLOW", (int)registeredCompanyDetail.Rows[0]["MAX_WORKFLOW"]);
                cmd.Parameters.AddWithValue("@MAX_USER", (int)registeredCompanyDetail.Rows[0]["MAX_USER"]);
                cmd.Parameters.AddWithValue("@USER_CHARGE_PM", (decimal)registeredCompanyDetail.Rows[0]["USER_CHARGE_PM"]);
                cmd.Parameters.AddWithValue("@CHARGE_TYPE", (string)registeredCompanyDetail.Rows[0]["CHARGE_TYPE"]);
                cmd.Parameters.AddWithValue("@VALIDITY", Convert.ToDouble(registeredCompanyDetail.Rows[0]["VALIDITY"]));
                cmd.Parameters.AddWithValue("@PLAN_CHANGE_DATE", planChangeDate);
                cmd.Parameters.AddWithValue("@PUSHMESSAGE_PERDAY", (int)planDetails.Rows[0]["PUSHMESSAGE_PERDAY"]);
                cmd.Parameters.AddWithValue("@PUSHMESSAGE_PERMONTH", (int)planDetails.Rows[0]["PUSHMESSAGE_PERMONTH"]);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.Parameters.AddWithValue("@BALANCE_AMOUNT", balanceAmount);
                cmd.Parameters.AddWithValue("@NEXT_MONTH_PLAN", (string)registeredCompanyDetail.Rows[0]["PLAN_CODE"]);
                cmd.Parameters.AddWithValue("@NEXT_MONTH_PLAN_LAST_UPDATED", 0);
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        void updateAdminTblAdministratorWhenTrialToPurchase(SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                string strQuery = @"UPDATE TBL_COMPANY_ADMINISTRATOR
                                SET IS_TRIAL = 0
                                WHERE ADMIN_ID = (SELECT ADMIN_ID FROM TBL_COMPANY_DETAIL
					                                WHERE COMPANY_ID = @CompanyId);";
                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);

                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        void updateCPTblAdministratorWhenTrialToPurchase(SqlConnection con, SqlTransaction transaction)
        {
            try
            {
                string strQuery = @"UPDATE ADMIN_TBL_COMPANY_ADMINISTRATOR
                                SET IS_TRIAL = 0
                                WHERE ADMIN_ID = (SELECT ADMIN_ID FROM ADMIN_TBL_COMPANY_DETAIL
					                                WHERE COMPANY_ID = @CompanyId);";
                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);

                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion
        void saveDefualtCompanyAccSettings(SqlTransaction transaction, SqlConnection con)
        {
            try
            {
                string strQuery = @"INSERT INTO [TBL_ACCOUNT_SETTINGS]
                                           ([COMPANY_ID]
                                           ,[MAX_DEVICE_PER_USER]
                                           ,[MAX_MPLUGIN_AGENTS]
                                           ,[ALLOW_DELETE_COMMAND]
                                           ,[ALLOW_ACTIVE_DIRECTORY_USER])
                                     VALUES
                                           (@COMPANY_ID
                                           ,@MAX_DEVICE_PER_USER
                                           ,@MAX_MPLUGIN_AGENTS
                                           ,@ALLOW_DELETE_COMMAND
                                           ,@ALLOW_ACTIVE_DIRECTORY_USER)";

                SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
                cmd.Parameters.AddWithValue("@MAX_DEVICE_PER_USER", 1);
                cmd.Parameters.AddWithValue("@MAX_MPLUGIN_AGENTS", 1);
                cmd.Parameters.AddWithValue("@ALLOW_DELETE_COMMAND", 0);
                cmd.Parameters.AddWithValue("@ALLOW_ACTIVE_DIRECTORY_USER", 0);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string CompanyId
        {
            get;
            set;
        }
        public string PlanCode
        {
            get;
            set;
        }
        public int MaxWorkFlow
        {
            get;
            set;
        }
        public int MaxUsers
        {
            get;
            set;
        }
        public double UserChargePerMonth
        {
            get;
            set;
        }
        public string CurrencyType
        {
            get;
            set;
        }
        public double Validity
        {
            get;
            set;
        }
        public int PushMessagePerDay
        {
            get;
            set;
        }
        public int PushMessagePerMonth
        {
            get;
            set;
        }
        public double PriceWithOutDiscount
        {
            get;
            set;
        }
        public double DiscountPercent
        {
            get;
            set;
        }
        public double ActualPrice
        {
            get;
            set;
        }
        public string TransactionType
        {
            get;
            set;
        }
        public string PlanName
        {
            get;
            set;
        }
        public long PurchaseDatetime
        {
            get;
            set;
        }
        public int StatusCode
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public bool IsNewPurchase
        {
            get;
            set;
        }
        public int AmountReceived
        {
            get;
            set;
        }
        public bool IsNewPurchaseAfterTrial
        {
            get;
            set;
        }
        public int NoOfMonths
        {
            get;
            set;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class ManageTrialPlanAdmin
    {
        public DataTable GetTrialPlanList()
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand("select * from ADMIN_TBL_TRIAL_PLANS order by PLAN_NAME");
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public void TrialPlanDetails(string planid)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select PLAN_CODE,PLAN_ID,PLAN_NAME,MAX_WORKFLOW,MAX_USER,PUSHMESSAGE_PERDAY,PUSHMESSAGE_PERMONTH,MONTH from ADMIN_TBL_TRIAL_PLANS
                                                    where PLAN_ID=@planid ORDER BY PLAN_NAME ");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@planid", planid);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public void UpdateTrialPlan(string PlanId, string planname, int maxworkflow, int users, int msgperday, int msgpermonth, int month)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"update ADMIN_TBL_TRIAL_PLANS set  PLAN_NAME=@planname,
                     MAX_WORKFLOW=@maxworkflow,MAX_USER=@maxusers,PUSHMESSAGE_PERDAY=@msgperday,
                                                PUSHMESSAGE_PERMONTH=@msgpermonth,MONTH=@month
                                                where PLAN_ID=@planid");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@planid", PlanId);
                ObjSqlCmd.Parameters.AddWithValue("@planname", planname);
                ObjSqlCmd.Parameters.AddWithValue("@maxworkflow", maxworkflow);
                ObjSqlCmd.Parameters.AddWithValue("@maxusers", users);
                ObjSqlCmd.Parameters.AddWithValue("@msgperday", msgperday);
                ObjSqlCmd.Parameters.AddWithValue("@msgpermonth", msgpermonth);
                ObjSqlCmd.Parameters.AddWithValue("@month", month);
                if (Utilities.ExecuteNonQueryRecord(ObjSqlCmd) > 0)
                {
                    StatusCode = 0;
                    StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                    this.StatusDescription = "Record Not Found";
                }
            }
            catch(Exception e) 
            {
                this.StatusCode = ((int)DATABASE_ERRORS.RECORD_INSERT_ERROR);
                this.StatusDescription = e.Message;
            }

        }

        public void AddTrialPlan(string plancode, string planId, string planame, int maxworkflow, int users, int msgperday, int msgpermonth, int month)
        {
            try
            {
                SqlCommand objSqlCmd = new SqlCommand(@"insert into ADMIN_TBL_TRIAL_PLANS (PLAN_CODE,PLAN_ID,PLAN_NAME,MAX_WORKFLOW,MAX_USER,PUSHMESSAGE_PERDAY,PUSHMESSAGE_PERMONTH,MONTH)
                VALUES(@plancode,@planid,@planname,@maxworkflow,@maxuser,@msgperday,@msgpermonth,@month)");
                objSqlCmd.Parameters.AddWithValue("@plancode", plancode);
                objSqlCmd.Parameters.AddWithValue("@planid", planId);
                objSqlCmd.Parameters.AddWithValue("@planname", planame);
                objSqlCmd.Parameters.AddWithValue("@maxworkflow", maxworkflow);
                objSqlCmd.Parameters.AddWithValue("@maxuser", users);
                objSqlCmd.Parameters.AddWithValue("@msgperday", msgperday);
                objSqlCmd.Parameters.AddWithValue("@msgpermonth", msgpermonth);
                objSqlCmd.Parameters.AddWithValue("@month", month);
                if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    this.StatusDescription = "Record Insert Error";
                }
            }
            catch (Exception e)
            {
                this.StatusCode = ((int)DATABASE_ERRORS.RECORD_INSERT_ERROR);
                this.StatusDescription = e.Message;
            }
        }
        public DataTable ResultTable
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class UpdateServerDetailAdmin
    {
        public void UpdateServer(string serverid,string servername,string serverurl,string serverip,bool isenabled)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"update ADMIN_TBL_MST_SERVER set SERVER_NAME=@servername, SERVER_URL=@serverurl,SERVER_IP_ADDRESS=@serverip,IS_ENABLED=@isenabled where SERVER_ID=@serverid");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@serverid", serverid);
                ObjSqlCmd.Parameters.AddWithValue("@servername", servername);
                ObjSqlCmd.Parameters.AddWithValue("@serverip", serverip);
                ObjSqlCmd.Parameters.AddWithValue("@serverurl", serverurl);
                ObjSqlCmd.Parameters.AddWithValue("@isenabled", isenabled);
                if (Utilities.ExecuteNonQueryRecord(ObjSqlCmd) > 0)
                {
                    StatusCode = 0;
                    StatusDescription = "";
                }
                else
                {
                    StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    StatusDescription = "Record Insert Error";
                }
            }
            catch
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                this.StatusDescription = "Record Insert Error";
            }
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        public string ServerId
        {
            get;
            set;
        }

        public string ServerName
        {
            get;
            set;
        }

        public string ServerURL
        {
            get;
            set;
        }

        public string ServerIP
        {
            get;
            set;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class GetAdminDetails
    {
        public enum GET_RECORDS_BY
        {
            loginName,
            adminId,
            usernameAndId
        }

        string _loginName;
        string _adminId;
        GET_RECORDS_BY _eGetRecordsBy;
        int _statusCode;
        string _statusDescription;
        MFEAdministrator _admin;



        #region Public Properties
        public string LoginName
        {
            get { return _loginName; }
            private set { _loginName = value; }
        }


        public string AdminId
        {
            get { return _adminId; }
            private set { _adminId = value; }
        }

        public GET_RECORDS_BY EGetRecordsBy
        {
            get { return _eGetRecordsBy; }
            private set { _eGetRecordsBy = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }


        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }

        public MFEAdministrator Admin
        {
            get { return _admin; }
            private set { _admin = value; }
        }
        #endregion
        public GetAdminDetails( GET_RECORDS_BY getRecBy,string loginName, string adminId)
        {
            this.LoginName = loginName;
            this.AdminId = adminId;
            this.EGetRecordsBy = GET_RECORDS_BY.loginName;
        }
        public void Process()
        {
            try
            {
                SqlCommand cmd = getSqlCommand();
                DataSet dsDtls = MSSqlClient.SelectDataFromSQlCommand(cmd);
                if (dsDtls == null) throw new Exception();
                if (dsDtls.Tables[0].Rows.Count > 0)
                {
                    this.Admin = getAdminFromDTable(dsDtls.Tables[0]);
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = MficientConstants.INTERNAL_ERROR;
            }
        }
        MFEAdministrator getAdminFromDTable(DataTable adminDtls)
        {
            if (adminDtls == null) throw new ArgumentNullException();
            MFEAdministrator objAdmin = new MFEAdministrator();
            if (adminDtls.Rows.Count > 0)
            {
                objAdmin.AdminName = Convert.ToString(adminDtls.Rows[0]["ADMIN_NAME"]);
                objAdmin.Password = Convert.ToString(adminDtls.Rows[0]["ACCESS_CODE"]);
                objAdmin.Email = Convert.ToString(adminDtls.Rows[0]["EMAIL"]);
                objAdmin.AdminId = Convert.ToString(adminDtls.Rows[0]["ADMIN_ID"]);
            }
            return objAdmin;
        }
        string getSqlQuery()
        {
            string strQuery = String.Empty;
            switch (this.EGetRecordsBy)
            {
                case GET_RECORDS_BY.loginName:
                    strQuery = @"SELECT * FROM ADMIN_TBL_ADMIN_LOGIN
                                WHERE ADMIN_NAME = @USER_NAME";
                    break;
                case GET_RECORDS_BY.adminId:
                    strQuery = @" SELECT * FROM ADMIN_TBL_ADMIN_LOGIN
                                WHERE ADMIN_ID = @USER_ID";
                    break;
                case GET_RECORDS_BY.usernameAndId:
                    strQuery = @"SELECT * FROM ADMIN_TBL_ADMIN_LOGIN
                                  WHERE ADMIN_NAME = @USER_NAME
                                  AND ADMIN_ID = @USER_ID";
                    break;
            }
            return strQuery;
        }
        SqlCommand getSqlCommand()
        {
            SqlCommand cmd = new SqlCommand(getSqlQuery());
            switch (this.EGetRecordsBy)
            {
                case GET_RECORDS_BY.loginName:
                    cmd.Parameters.AddWithValue("@USER_NAME", this.LoginName);
                    break;
                case GET_RECORDS_BY.adminId:
                    cmd.Parameters.AddWithValue("@USER_ID", this.AdminId);
                    break;
                case GET_RECORDS_BY.usernameAndId:
                    cmd.Parameters.AddWithValue("@USER_NAME", this.LoginName);
                    cmd.Parameters.AddWithValue("@USER_ID", this.AdminId);
                    break;

            }
            return cmd;

        }
    }
}
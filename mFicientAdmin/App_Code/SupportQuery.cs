﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class SupportQueryAdmin
    {

        public DataTable GetListbyCompany(string UserId,string CompanyName)
        {
            SqlCommand objCmd = new SqlCommand(@"select q.QUERY from ADMIN_TBL_QUERY q inner join ADMIN_TBL_COMPANY_DETAIL c on q.COMPANY_ID=c.COMPANY_ID
                                                        where c.COMPANY_NAME=@companyname and q.USER_ID=@userid ");

            objCmd.Parameters.AddWithValue("@companyname", CompanyName);
            objCmd.Parameters.AddWithValue("@userid", UserId);
            DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
            return ds.Tables[0];
        }

        public DataTable GetListbyCountry(string UserId, string CompanyName)
        {
            SqlCommand objCmd = new SqlCommand(@"select q.QUERY from ADMIN_TBL_QUERY q inner join ADMIN_TBL_COMPANY_DETAIL c on q.COMPANY_ID=c.COMPANY_ID
                                                        where c.COMPANY_NAME=@companyname and q.USER_ID=@userid ");

            objCmd.Parameters.AddWithValue("@companyname", CompanyName);
            objCmd.Parameters.AddWithValue("@userid", UserId);
            DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
            return ds.Tables[0];
        }

        public DataTable GetIsResolvedList(string UserId)
        {
            SqlCommand objCmd = new SqlCommand(@"select USER_ID,token_no,posted_on, query_subject from ADMIN_TBL_QUERY where IS_RESOLVED=@isresolved  and USER_ID=@userid ");

            objCmd.Parameters.AddWithValue("@isresolved", 1);
            objCmd.Parameters.AddWithValue("@userid", UserId);
            DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
            return ds.Tables[0];
        }


        public DataTable GetResolvedByCreatorList(string UserId)
        {
            SqlCommand objCmd = new SqlCommand(@"select USER_ID,token_no,posted_on,query_subject from ADMIN_TBL_QUERY where RESOLVED_BY_CREATOR=@isresolved  and USER_ID=@userid ");

            objCmd.Parameters.AddWithValue("@isresolved", 1);
            objCmd.Parameters.AddWithValue("@userid", UserId);
            DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
            return ds.Tables[0];
        }


        public DataTable GetForwardedToAdminList(string UserId)
        {
            SqlCommand objCmd = new SqlCommand(@"select USER_ID,token_no,posted_on,query_subject from ADMIN_TBL_QUERY where IS_FORWARD_TO_ADMIN=@isforwarded  and USER_ID=@userid ");

            objCmd.Parameters.AddWithValue("@isresolved", 1);
            objCmd.Parameters.AddWithValue("@userid", UserId);
            DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
            return ds.Tables[0];
        }


        public DataTable GetListbyToken(string UserId, string Tokennumber)
        {
            SqlCommand objCmd = new SqlCommand(@"select USER_ID,TOKEN_NO,POSTED_ON,QUERY_SUBJECT from ADMIN_TBL_QUERY where TOKEN_NO=@tokenno and USER_ID=@userid");

            objCmd.Parameters.AddWithValue("@tokenno", Tokennumber);
            objCmd.Parameters.AddWithValue("@userid", UserId);
            DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
            return ds.Tables[0];
        }


        //public string GetToken(string userid)
        //{
        //    SqlCommand objCmd = new SqlCommand(@"select TOKEN_NO from ADMIN_TBL_QUERY where USER_ID=@userid");

        //    //objCmd.Parameters.AddWithValue("@companyname", Tokennumber);
        //    objCmd.Parameters.AddWithValue("@userid", userid);
        //    DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
        //    string strtoken = Convert.ToString(ds.Tables[0].Rows[0]["TOKEN_NO"]);
        //    return strtoken;
        //}


        public DataTable GetCompanyDd(string UserId,string CompanyName)
        {
            SqlCommand objCmd = new SqlCommand(@"select user_id,posted_on,token_no,query_subject from ADMIN_TBL_QUERY q inner join 
                ADMIN_TBL_COMPANY_DETAIL c on q.COMPANY_ID=c.COMPANY_ID where q.USER_ID=@userid and COMPANY_NAME=@companyname");

            objCmd.Parameters.AddWithValue("@companyname", CompanyName);
            objCmd.Parameters.AddWithValue("@userid", UserId);
            DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
            return ds.Tables[0];
        }


        public DataTable GetCountrydd(string Country, string userid)
        {
            SqlCommand objCmd = new SqlCommand(@"select user_id,query_subject,token_no,posted_on from ADMIN_TBL_QUERY q inner join 
                                                ADMIN_TBL_COMPANY_DETAIL c
                                                on q.COMPANY_ID=c.COMPANY_ID inner join
                                                ADMIN_TBL_MST_COUNTRY cc on c.COUNTRY_CODE=cc.COUNTRY_CODE where cc.COUNTRY_NAME=@country and USER_ID=@userid");

            objCmd.Parameters.AddWithValue("@country", Country);
            objCmd.Parameters.AddWithValue("@userid", userid);
            DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
            return ds.Tables[0];
           
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class ManageLoginUsersAdmin
    {
        DataSet ds;
        public DataTable SelectReseller()
        {
            //previous query
            //select r.RESELLER_NAME as USER_NAME ,r.LOGIN_NAME ,u.COUNTRY_CODE ,u.TYPE from ADMIN_TBL_RESELLER_DETAIL r inner join  ADMIN_TBL_LOGIN_USER_DETAIL u on r.CITY=u.CITY
              //changed on 3-7-2012  
            //added [RESELLER_ID] AS ID, on 3-7-2012
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"SELECT [RESELLER_ID] AS ID, RESELLER_NAME +' ' + '(' + LOGIN_NAME +')' as NAME ,c.COUNTRY_NAME ,[IS_ENABLE],
                                                'Reseller' AS TYPE FROM [ADMIN_TBL_RESELLER_DETAIL] R INNER JOIN ADMIN_TBL_MST_COUNTRY C 
                                                  on r.COUNTRY=c.COUNTRY_CODE
                                                  order by reseller_name");
                ObjSqlCmd.CommandType = CommandType.Text;
                ds = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public DataTable SelectAll()
        {
            //added [LOGIN_ID] AS ID ,[RESELLER_ID] AS ID on 3-7-2012
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"SELECT [RESELLER_ID] AS ID, RESELLER_NAME +' ' + '(' + LOGIN_NAME +')' as NAME,
                        c1.COUNTRY_NAME  ,[IS_ENABLE],'Reseller' AS TYPE FROM [ADMIN_TBL_RESELLER_DETAIL] r inner join ADMIN_TBL_MST_COUNTRY c1
                                    on r.COUNTRY=c1.COUNTRY_CODE
                                UNION SELECT [LOGIN_ID] AS ID,USER_NAME +' ' + '(' + LOGIN_NAME +')' as NAME ,C.COUNTRY_NAME ,
                                [IS_ENABLED], case u.TYPE when 'S' then 'Sales Executive'
                                when 'SM' then 'Sales Manager' when 'SUPP' then 'Support' when 'ACC' then 'Accounts' end
                                FROM [ADMIN_TBL_LOGIN_USER_DETAIL] u INNER JOIN ADMIN_TBL_MST_COUNTRY c
                                 ON U.COUNTRY_CODE=C.COUNTRY_CODE order by NAME");
                ObjSqlCmd.CommandType = CommandType.Text;
                ds = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public DataTable SelectLoginUserBytype(string type)
        {
            //added LOGIN_TYPE AS ID on 3-7-2012
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select USER_NAME +' ' + '(' + LOGIN_NAME +')' as NAME ,c.COUNTRY_NAME,case TYPE when 'S' then 'Sales Executive'
                                                 when 'SM' then 'Sales Manager' when 'SUPP' then 'Support' when 'ACC' then 'Accounts' end as TYPE,LOGIN_ID AS ID from ADMIN_TBL_LOGIN_USER_DETAIL u inner join 
                                                  ADMIN_TBL_MST_COUNTRY c on u.COUNTRY_CODE=c.COUNTRY_CODE  where TYPE=@type  
                                                  order by USER_NAME ");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@type", type);
                DataSet ds = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public DataTable ResultTable
        {
            set;
            get;
        }

         public int StatusCode
         {
             set;
             get;
         }
         public string StatusDescription
         {
             set;
             get;
         }
        }
    }

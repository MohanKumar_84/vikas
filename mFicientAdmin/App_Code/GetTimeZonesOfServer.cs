﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.ObjectModel;
namespace mFicientAdmin
{
    public class GetTimeZonesOfServer
    {
        public static ReadOnlyCollection<TimeZoneInfo> getSystemTimezones()
        {
            ReadOnlyCollection<TimeZoneInfo> zones = TimeZoneInfo.GetSystemTimeZones();
            return zones;
        }
    }
}
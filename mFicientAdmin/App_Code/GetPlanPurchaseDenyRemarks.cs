﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class GetPlanPurchaseDenyRemarks
    {
        public GetPlanPurchaseDenyRemarks()
        { }

        public void Process()
        {
            try
            {
                StatusCode = -1000;//for error
                string strQuery = @"SELECT * FROM ADMIN_TBL_PLAN_ORDER_REMARKS;";
                SqlCommand cmd = new SqlCommand(strQuery);
                DataSet dsPlanOrderRemarks = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsPlanOrderRemarks != null)
                {
                    StatusCode = 0;
                    StatusDescription = "";
                    ResultTable = dsPlanOrderRemarks.Tables[0];
                }
                else
                {
                    StatusCode = -1000;
                    StatusDescription = "Internal server error";
                }
            }
            catch 
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
            }
        }
        public DataTable ResultTable
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public int StatusCode
        {
            get;
            set;
        }

    }
}
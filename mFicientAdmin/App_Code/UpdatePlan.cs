﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class UpdatePlanAdmin
    {
        public void UpdatePlan(string plancode, string planname, int maxworkflow, decimal chargeinr, decimal chargeusd, int msgperday, int msgpermonth)
        {
            try
            {
                string sql = @"update ADMIN_TBL_PLANS set PLAN_CODE='@plancode' ,PLAN_NAME='@planname',MAX_WORKFLOW='@maxworkflow',USER_CHARGE_INR_PM='@inr',USER_CHARGE_USD_PM='@usd',
                PUSHMESSAGE_PERDAY='@msgperday',PUSHMESSAGE_PERMONTH='@msgpermonth'
                WHERE PLAN_CODE='@plancode';";
                SqlCommand objSqlCmd = new SqlCommand(sql);
                objSqlCmd.Parameters.AddWithValue("@plancode", plancode);
                objSqlCmd.CommandType = CommandType.Text;
                if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    this.StatusDescription = "Record Insert Error";
                }
            }
            catch
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                this.StatusDescription = "Record Insert Error";
            }
        }

        public string Planname
        {
            get;
            set;
        }

        public string plancode
        {
            get;
            set;
        }

        public int maxworkflow
        {
            get;
            set;
        }


        public decimal chargeinr
        {
            get;
            set;
        }

        public decimal chargeusd
        {
            get;
            set;
        }

        public int msgperday
        {
            get;
            set;
        }

        public int msgpermonth
        {
            get;
            set;
        }


        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

        
      

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace mFicientAdmin
{
    [Serializable()]
    public class SessionUser : IXmlSerializable
    {
        [XmlIgnore]
        private string _sessionId = string.Empty;
        [XmlIgnore]
        private string _enterpriseId = string.Empty;
        [XmlIgnore]
        private string _username = string.Empty;
        [XmlIgnore]
        private string _deviceType = string.Empty;
        [XmlIgnore]
        private string _deviceId = string.Empty;
        [XmlIgnore]
        private string _userId = string.Empty;
        [XmlIgnore]
        private string _email = string.Empty;
        [XmlIgnore]
        private long _lastActivityDateTime = 0;
        [XmlIgnore]
        private long _addedDateTime = 0;


        public SessionUser()
        {

        }

        public SessionUser(string enterpriseId, string username, string deviceType, string deviceId, string userId, string email)
        {
            _enterpriseId = enterpriseId.ToUpper();
            _username = username.ToUpper();
            _deviceType = deviceType.ToUpper();
            _deviceId = deviceId.ToUpper();
            _userId = userId;
            _email = email;
            _addedDateTime = DateTime.UtcNow.Ticks;
            generateSessionId();
        }

        [XmlIgnore]
        public string SessionId
        {
            get { return _sessionId; }
            private set { _sessionId = value; }
        }

        [XmlIgnore]
        public string UserId
        {
            get { return _userId; }
            private set { _userId = value; }
        }

        [XmlIgnore]
        public string Email
        {
            get { return _email; }
            private set { _email = value; }
        }

        [XmlIgnore]
        public long AddedDateTime
        {
            get { return _addedDateTime; }
            private set { _addedDateTime = value; }
        }

        [XmlIgnore]
        public long LastActivityDateTime
        {
            get { return _lastActivityDateTime; }
            set { _lastActivityDateTime = value; }
        }

        public bool CheckSessionAndUpdateLastActivity()
        {
            if (TimeSpan.FromTicks(DateTime.UtcNow.Ticks - _lastActivityDateTime).TotalSeconds > MficientConstants.SESSION_TIMEOUT_SECONDS)
                return false;

            setLastActivityTime();
            return true;
        }

        public string Renew(string oldSessionId)
        {
            if (oldSessionId.ToUpper() != _sessionId.ToUpper()) throw new ArgumentException(@"Incorrect Old Session ID");
            generateSessionId();
            return _sessionId;
        }

        private void generateSessionId()
        {
            setLastActivityTime();
            _sessionId = Utilities.GetMd5Hash(_enterpriseId + _username + _deviceType + _deviceId + DateTime.UtcNow.Ticks.ToString()).ToUpper();
        }

        private void setLastActivityTime()
        {
            _lastActivityDateTime = DateTime.UtcNow.Ticks;
        }

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            _enterpriseId = reader.GetAttribute("eid");
            _username = reader.GetAttribute("unm");
            _userId = reader.GetAttribute("uid");
            _email = reader.GetAttribute("eml");
            _deviceId = reader.GetAttribute("did");
            _deviceType = reader.GetAttribute("dtyp");
            _sessionId = reader.GetAttribute("sid");
            _addedDateTime = Convert.ToInt64(reader.GetAttribute("adt"));
            _lastActivityDateTime = Convert.ToInt64(reader.GetAttribute("ldt"));
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("eid", _enterpriseId);
            writer.WriteAttributeString("unm", _username);
            writer.WriteAttributeString("uid", _userId);
            writer.WriteAttributeString("eml", _email);
            writer.WriteAttributeString("did", _deviceId);
            writer.WriteAttributeString("dtyp", _deviceType);
            writer.WriteAttributeString("sid", _sessionId);
            writer.WriteAttributeString("adt", _addedDateTime.ToString());
            writer.WriteAttributeString("ldt", _lastActivityDateTime.ToString());
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class BlockUnblockCompany
    {
        int _statusCode;
        string _statusDescription;
        string _companyId;
        #region Public Properties
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }


        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }
        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }
        #endregion
        public BlockUnblockCompany(string companyId)
        {
            this.CompanyId = companyId;
        }
        #region Block Company
        public void processBlockCompany(
            byte blockedReason,
            USER_TYPE userBlockingCmp)
        {
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                GetUsersOfCompany objGetUsers = new
                    GetUsersOfCompany(this.CompanyId);
                objGetUsers.Process();
                if (objGetUsers.StatusCode != 0) throw new Exception();
                GetAllRegisteredDevicesOfCompany objGetRegDev = new
                GetAllRegisteredDevicesOfCompany(this.CompanyId);
                objGetRegDev.Process();
                if (objGetRegDev.StatusCode != 0) throw new Exception();

                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        string strBlockId = Utilities.GetMd5Hash((DateTime.UtcNow.Ticks).ToString() +
                            this.CompanyId +
                            ((int)COMPANY_ACCESS_CHANGE_TYPE.Block).ToString()
                            );

                        int rowsAffected = updateCompanyDetailIsBlockedStatus(true, strBlockId, con, transaction);
                        if (rowsAffected == 0) throw new Exception();
                        rowsAffected = updateCmpDtlIsBlockedStatusInAdminTbl(true, strBlockId, con, transaction);
                        if (rowsAffected == 0) throw new Exception();
                        rowsAffected = insertCmpAccessChangedLog(COMPANY_ACCESS_CHANGE_TYPE.Block,
                            blockedReason,
                            userBlockingCmp,
                            strBlockId,
                            con,
                            transaction);
                        if (rowsAffected == 0) throw new Exception();

                        foreach (MFEMobileUser user in objGetUsers.UserList)
                        {
                            foreach (MFEDevice dev in objGetRegDev.RegisteredDvc)
                            {
                                if (dev.UserId.ToLower() == user.UserId.ToLower())
                                {
                                    SavePushMsgOutboxInMgram objSavePushMsg = new
                                    SavePushMsgOutboxInMgram(this.CompanyId,
                                    user.Username,
                                    dev.DevicePushMsgId,
                                    dev.DeviceType,
                                    SavePushMsgOutboxInMgram.NOTIFICATION_TYPE.MF_USER_BLOCK);
                                }
                            }
                        }

                        transaction.Commit();
                    }
                }
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }

        #endregion
        #region Unblock Company
        public void processUnBlockCompany(
            byte unblockedReason,
            USER_TYPE userUnblockingCmp)
        {
            SqlTransaction transaction = null;
            SqlConnection con = null;
            try
            {
                MSSqlClient.SqlConnectionOpen(out con);
                using (con)
                {
                    using (transaction = con.BeginTransaction())
                    {
                        string strBlockId = Utilities.GetMd5Hash((DateTime.UtcNow.Ticks).ToString() +
                            this.CompanyId +
                            ((int)COMPANY_ACCESS_CHANGE_TYPE.Unblock).ToString()
                            );
                        int rowsAffected = updateCompanyDetailIsBlockedStatus(false, strBlockId, con, transaction);
                        if (rowsAffected == 0) throw new Exception();
                        rowsAffected = updateCmpDtlIsBlockedStatusInAdminTbl(false, strBlockId, con, transaction);
                        if (rowsAffected == 0) throw new Exception();
                        rowsAffected = insertCmpAccessChangedLog(COMPANY_ACCESS_CHANGE_TYPE.Unblock,
                            unblockedReason,
                            userUnblockingCmp,
                            strBlockId,
                            con,
                            transaction);
                        if (rowsAffected == 0) throw new Exception();
                        transaction.Commit();
                    }
                }
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }
        }

        #endregion


        int updateCompanyDetailIsBlockedStatus(bool isBlocked,
            string blockId,
            SqlConnection con, SqlTransaction transaction)
        {
            SqlCommand cmd = sqlCmdForUpdateCmpBlock(isBlocked, blockId, con, transaction);
            return cmd.ExecuteNonQuery();
        }
        int updateCmpDtlIsBlockedStatusInAdminTbl(bool isBlocked,
            string blockId,
            SqlConnection con, SqlTransaction transaction)
        {
            SqlCommand cmd = sqlCmdForUpdateAdminCmpBlock(isBlocked, blockId, con, transaction);
            return cmd.ExecuteNonQuery();
        }
        int insertCmpAccessChangedLog(
            COMPANY_ACCESS_CHANGE_TYPE changeType,
            byte reason, USER_TYPE changedBy,
            string blockId,
            SqlConnection con, SqlTransaction transaction)
        {
            SqlCommand cmd =
                sqlCmdForInsertCmpAccessChangeLog(Convert.ToByte((int)changeType),
                reason, Convert.ToByte((int)changedBy), blockId, con, transaction);
            return cmd.ExecuteNonQuery();

        }



        string sqlQryForUpdateCompanyBlockedStatus()
        {
            return @"UPDATE TBL_COMPANY_DETAIL
                    SET IS_BLOCKED = @IS_BLOCKED,
                    BLOCK_UNBLOCK_ID = @BLOCK_UNBLOCK_ID
                    WHERE COMPANY_ID = @COMPANY_ID";
        }
        SqlCommand sqlCmdForUpdateCmpBlockedStatus(bool isBlocked, string blockedId)
        {
            SqlCommand cmd = new SqlCommand(sqlQryForUpdateCompanyBlockedStatus());
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            cmd.Parameters.AddWithValue("@IS_BLOCKED", isBlocked ? 1 : 0);
            cmd.Parameters.AddWithValue("@BLOCK_UNBLOCK_ID", blockedId);
            return cmd;
        }
        SqlCommand sqlCmdForUpdateCmpBlock(bool isBlocked, string blockedId, SqlConnection con, SqlTransaction transaction)
        {
            SqlCommand cmd = new
                SqlCommand(
                sqlQryForUpdateCompanyBlockedStatus(),
                con, transaction);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            cmd.Parameters.AddWithValue("@IS_BLOCKED", isBlocked ? 1 : 0);
            cmd.Parameters.AddWithValue("@BLOCK_UNBLOCK_ID", blockedId);
            return cmd;
        }

        string sqlQryForUpdateAdminCompanyDetail()
        {
            return @"UPDATE ADMIN_TBL_COMPANY_DETAIL
                    SET IS_BLOCKED = @IS_BLOCKED,
                    BLOCK_UNBLOCK_ID = @BLOCK_UNBLOCK_ID
                    WHERE COMPANY_ID = @COMPANY_ID";
        }
        SqlCommand sqlCmdForUpdateAdminCmpBlock(bool isBlocked, string blockedId)
        {
            SqlCommand cmd = new SqlCommand(sqlQryForUpdateAdminCompanyDetail());
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            cmd.Parameters.AddWithValue("@IS_BLOCKED", isBlocked ? 1 : 0);
            cmd.Parameters.AddWithValue("@BLOCK_UNBLOCK_ID", blockedId);
            return cmd;
        }
        SqlCommand sqlCmdForUpdateAdminCmpBlock(bool isBlocked, string blockedId, SqlConnection con, SqlTransaction transaction)
        {
            SqlCommand cmd = new
                SqlCommand(
                sqlQryForUpdateAdminCompanyDetail(),
                con, transaction);

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            cmd.Parameters.AddWithValue("@IS_BLOCKED", isBlocked ? 1 : 0);
            cmd.Parameters.AddWithValue("@BLOCK_UNBLOCK_ID", blockedId);
            return cmd;
        }

        string sqlQryInsertCmpAccessChangeLog()
        {
            return @"INSERT INTO TBL_COMPANY_ACCESS_CHANGE_LOG
                    (ACCESS_CHANGE_ID,COMPANY_ID
                    ,ACCESS_CHANGE_TYPE,ACCESS_CHANGE_REASON
                    ,ACCESS_CHANGED_BY,ACCESS_CHANGED_DATE)
                     
                    VALUES

                    (@ACCESS_CHANGE_ID
                    ,@COMPANY_ID
                    ,@ACCESS_CHANGE_TYPE
                    ,@ACCESS_CHANGE_REASON
                    ,@ACCESS_CHANGED_BY
                    ,@ACCESS_CHANGED_DATE)";
        }
        SqlCommand sqlCmdForInsertCmpAccessChangeLog(byte changeType, byte changeReason,
            byte changedBy,
            string blockedId)
        {
            SqlCommand cmd = new SqlCommand(sqlQryInsertCmpAccessChangeLog());
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ACCESS_CHANGE_ID", blockedId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            cmd.Parameters.AddWithValue("@ACCESS_CHANGE_TYPE", changeType);
            cmd.Parameters.AddWithValue("@ACCESS_CHANGE_REASON", changeReason);
            cmd.Parameters.AddWithValue("@ACCESS_CHANGED_BY", changedBy);
            cmd.Parameters.AddWithValue("@ACCESS_CHANGED_DATE", DateTime.UtcNow.Ticks);
            return cmd;
        }
        SqlCommand sqlCmdForInsertCmpAccessChangeLog(
            byte changeType,
            byte changeReason,
            byte changedBy,
            string blockedId,
            SqlConnection con,
            SqlTransaction transaction)
        {
            SqlCommand cmd = new
                SqlCommand(
                sqlQryInsertCmpAccessChangeLog(),
                con, transaction);

            cmd.Parameters.AddWithValue("@ACCESS_CHANGE_ID",
                blockedId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            cmd.Parameters.AddWithValue("@ACCESS_CHANGE_TYPE", changeType);
            cmd.Parameters.AddWithValue("@ACCESS_CHANGE_REASON", changeReason);
            cmd.Parameters.AddWithValue("@ACCESS_CHANGED_BY", changedBy);
            cmd.Parameters.AddWithValue("@ACCESS_CHANGED_DATE", DateTime.UtcNow.Ticks);
            return cmd;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class DoesCompanyExists
    {
        string _companyId;
        bool _exists;

        
        public DoesCompanyExists(string companyId)
        {
            this.CompanyId = companyId;
        }
        public void Process()
        {
            try
            {
                this.Exists= companyIDExists();
                this.StatusCode = 0;
                this.StatusDescription = "";
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error";
            }
        }
         
        bool companyIDExists()
        {
            string strQuery = @"SELECT COUNT(COMPANY_ID) AS TOTAL
                                FROM ADMIN_TBL_COMPANY_DETAIL
                                WHERE COMPANY_ID = @CompanyId";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", _companyId);
            DataSet ds = MSSqlClient.SelectDataFromSQlCommand(cmd);
            if (ds != null && ds.Tables.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0]["TOTAL"]) == 0 ? false : true;
            }
            else
            {
                throw new Exception();
            }
        }

        public bool Exists
        {
            get { return _exists; }
            private set { _exists = value; }
        }
        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }
        public int StatusCode
        {
            get;
            private set;
        }
        public string StatusDescription
        {
            get;
            private set;
        }
    }
}
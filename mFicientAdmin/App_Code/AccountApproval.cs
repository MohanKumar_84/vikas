﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class AccountApprovalAdmin
    {
        /// <summary>
        /// get list of plan order request which are not approved by Sales Executive,ie,
        /// not yet processed and are reseller approved
        /// </summary>
        /// <returns></returns>
        public DataTable GetPendingList()
        {
            StatusCode = -1000; //for error
           DataSet ds;
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select r.*, d.COMPANY_NAME,d.COMPANY_ID,p.PLAN_NAME,r.REQUEST_TYPE ,r.PLAN_CODE,case when r.REQUEST_TYPE='1' then 'RenewPlan('+ r.PLAN_CODE + ')'
                                         when r.REQUEST_TYPE='2' then 'Add User(' + r.PLAN_CODE + ')'
                                        WHEN R.REQUEST_TYPE='3' then 'Plan Upgrade(' + r.PLAN_CODE + ')'
                                         when r.REQUEST_TYPE='4' then 'Plan Downgrade(' + r.PLAN_CODE + ')'
                                         when r.REQUEST_TYPE='5' then 'Plan Purchase (' + r.PLAN_CODE + ')'
                                        end as 'ORDER' from  ADMIN_TBL_COMPANY_DETAIL d inner join
                                                            ADMIN_TBL_PLAN_ORDER_REQUEST r on
                                                r.COMPANY_ID=d.COMPANY_ID 
                                                inner join ADMIN_TBL_PLANS p on p.PLAN_CODE=r.PLAN_CODE
                                                WHERE STATUS =@StatusUnApproved OR STATUS = @StatusResellerApproved AND d.SM_APPROVED=@smapproved AND r.REQUEST_TYPE !=5 ");//
                objCmd.Parameters.AddWithValue("@smapproved", 1);//company is approved
                objCmd.Parameters.AddWithValue("@StatusUnApproved", (int)PLAN_REQUEST_STATUS.UNAPPROVED);
                objCmd.Parameters.AddWithValue("@StatusResellerApproved", (int)PLAN_REQUEST_STATUS.RESELLER_APPROVED);
                ds = Utilities.SelectDataFromSQlCommand(objCmd);
                return ds.Tables[0]; 
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
                return null;
            }
            
        }

        /// <summary>
        /// get list of plan order request which are  approved by Sales Executive
        /// </summary>
        /// <returns></returns>
        public DataTable GetApprovedList()
        {
            
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select r.*, d.COMPANY_NAME,d.COMPANY_ID,p.PLAN_NAME,r.REQUEST_TYPE ,r.PLAN_CODE,case when r.REQUEST_TYPE='1' then 'RenewPlan('+ r.PLAN_CODE + ')'
                                         when r.REQUEST_TYPE='2' then 'Add User(' + r.PLAN_CODE + ')'
                                        WHEN R.REQUEST_TYPE='3' then 'Plan Upgrade(' + r.PLAN_CODE + ')'
                                         when r.REQUEST_TYPE='4' then 'Plan Downgrade(' + r.PLAN_CODE + ')'
                                         when r.REQUEST_TYPE='5' then 'Plan Purchase (' + r.PLAN_CODE + ')'
                                        end as 'ORDER' from  ADMIN_TBL_COMPANY_DETAIL d inner join
                                                            ADMIN_TBL_PLAN_ORDER_REQUEST r on
                                                r.COMPANY_ID=d.COMPANY_ID 
                                                inner join ADMIN_TBL_PLANS p on p.PLAN_CODE=r.PLAN_CODE
                                                WHERE STATUS =@SalesApproved AND d.SM_APPROVED=@smapproved AND r.REQUEST_TYPE !=5 ");// AND r.REQUEST_TYPE !=5 Request_type = 5 is for new purchase.
                objCmd.Parameters.AddWithValue("@smapproved", 1);
                objCmd.Parameters.AddWithValue("@SalesApproved", PLAN_REQUEST_STATUS.SALES_APPROVED);
                DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
                return null;
            }
            
        }

        public void CompanyDetails(string CompanyId)
        {
            this.StatusCode = -1000; // for error
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select distinct d.COMPANY_NAME,d.STREET_ADDRESS1,d.STREET_ADDRESS2,
                                            d.STREET_ADDRESS3,d.CITY_NAME,d.STATE_NAME,c.COUNTRY_NAME,d.ZIP,d.SUPPORT_EMAIL,d.SUPPORT_CONTACT
                                            from ADMIN_TBL_COMPANY_DETAIL d inner join
                                            ADMIN_TBL_MST_COUNTRY c on d.COUNTRY_CODE=c.COUNTRY_CODE where d.COMPANY_ID=@companyid ");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@companyid", CompanyId);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    this.StatusDescription = "";
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "No records to display";
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = -1000;
                }
            }
        }

        public DataTable ResultTable
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
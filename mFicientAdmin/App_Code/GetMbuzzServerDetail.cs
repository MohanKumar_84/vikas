﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class GetMbuzzServerDetail
    {
        string _statusDescription;
        int _statusCode;
        #region Public Properties
        public string StatusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }


        public int StatusCode
        {
            get { return _statusCode; }
            set { _statusCode = value; }
        }
        #endregion
        public List<MFEmBuzzServer> getAllMbuzzServers()
        {
            List<MFEmBuzzServer> lstMbuzzServers =
                    new List<MFEmBuzzServer>();
            try
            {
                string strQuery = this.getAllMBuzzServersQuery();
                SqlCommand cmd = this.getCmdToGetAllMbuzzServer(strQuery);
                DataSet dsMbuzzServer = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsMbuzzServer == null)
                    throw new Exception(
                        ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString()
                        );
                foreach (DataRow row in dsMbuzzServer.Tables[0].Rows)
                {
                    lstMbuzzServers.Add(getMbuzzServerFromDatarow(row));
                }
            }
            catch (Exception ex)
            {
                this.StatusCode = -1000;
                if (ex.Message == ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString())
                {
                    this.StatusDescription = "Record not found.";
                }
                else
                {
                    this.StatusDescription = "Internal server error.";
                }
            }
            return lstMbuzzServers;
        }
        public DataTable getAllMbuzzServersDtbl()
        {
            DataTable dtblMbuzzServers = new DataTable();
            try
            {
                string strQuery = this.getAllMBuzzServersQuery();
                SqlCommand cmd = this.getCmdToGetAllMbuzzServer(strQuery);
                DataSet dsMbuzzServer = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsMbuzzServer == null)
                    throw new Exception(
                        ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString()
                        );
                dtblMbuzzServers = dsMbuzzServer.Tables[0];
                
            }
            catch (Exception ex)
            {
                this.StatusCode = -1000;
                if (ex.Message == ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString())
                {
                    this.StatusDescription = "Record not found.";
                }
                else
                {
                    this.StatusDescription = "Internal server error.";
                }
            }
            return dtblMbuzzServers;
        }
        public MFEmBuzzServer getDefaultMbuzzServer()
        {
            MFEmBuzzServer objMbuzzServer =
                new MFEmBuzzServer();
            try
            {
                string strQuery = this.getDefaultMbuzzServersQuery();
                SqlCommand cmd = this.getCmdTogetDefaultMbuzzServer(strQuery);
                DataSet dsMpluginServer = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsMpluginServer == null &&
                   dsMpluginServer.Tables[0].Rows.Count == 0)//should get a record
                    throw new Exception(
                        ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString()
                        );
                objMbuzzServer = this.getMbuzzServerFromDatarow(
                    dsMpluginServer.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                this.StatusCode = -1000;
                if (ex.Message == ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString())
                {
                    this.StatusDescription = "Record not found.";
                }
                else
                {
                    this.StatusDescription = "Internal server error.";
                }
            }
            return objMbuzzServer;
        }
        public MFEmBuzzServer getMbuzzServerForCompany(string companyId)
        {
            MFEmBuzzServer objMbuzzServer = new MFEmBuzzServer();
            try
            {
                string strQuery = this.getMbuzzServerForCompQuery();
                SqlCommand cmd = getCmdToGetMbuzzServerOfCmp(
                    strQuery, companyId);
                DataSet dsMbuzzServer = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsMbuzzServer == null) throw new Exception();
                if (dsMbuzzServer.Tables[0].Rows.Count > 0)
                {
                    objMbuzzServer = this.getMbuzzServerFromDatarow(
                        dsMbuzzServer.Tables[0].Rows[0]);
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return objMbuzzServer;
        }
        string getAllMBuzzServersQuery()
        {
            return @"SELECT * FROM [ADMIN_TBL_MBUZZ_SERVER]
                     WHERE ENABLED = 1 ;";
        }
        SqlCommand getCmdToGetAllMbuzzServer(string query)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            return cmd;
        }
        string getDefaultMbuzzServersQuery()
        {
            return @"SELECT * FROM [ADMIN_TBL_MBUZZ_SERVER]
                     WHERE IS_DEFAULT =@IS_DEFAULT
                    AND ENABLED = 1;";
        }
        SqlCommand getCmdTogetDefaultMbuzzServer(string query)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@IS_DEFAULT", 1);
            return cmd;
        }
        MFEmBuzzServer getMbuzzServerFromDatarow(DataRow row)
        {
            if (row == null) throw new ArgumentNullException();
            MFEmBuzzServer objMbuzzServer = new MFEmBuzzServer();
            objMbuzzServer.ServerId = Convert.ToString(row["MBUZZ_SERVER_ID"]);
            objMbuzzServer.ServerName = Convert.ToString(row["MBUZZ_SERVER_NAME"]);
            objMbuzzServer.ServerIpAddress = Convert.ToString(row["SERVER_IP"]);
            objMbuzzServer.ServerPort = Convert.ToString(row["PORT_NUMBER"]);
            objMbuzzServer.IsEnabled = Convert.ToBoolean(row["ENABLED"]);
            objMbuzzServer.IsDefault = Convert.ToBoolean(row["IS_DEFAULT"]);
            return objMbuzzServer;
        }
        string getMbuzzServerForCompQuery()
        {
            return @"SELECT * 
                                FROM 
                                ADMIN_TBL_SERVER_MAPPING AS ServerMap
                                INNER JOIN 
                                ADMIN_TBL_MBUZZ_SERVER AS MbuzzServer
                                ON ServerMap.MBUZZ_SERVER_ID = MbuzzServer.MBUZZ_SERVER_ID
                                WHERE COMPANY_ID = @COMPANY_ID"; ;
        }
        SqlCommand getCmdToGetMbuzzServerOfCmp(string query,
            string companyId)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
            return cmd;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class GetUserDetailsAdmin
    {

        public void GetUserDetails(string loginId)
        {
            try
            {
                StatusCode = -1000; // for error
                SqlCommand ObjSqlCmd = new SqlCommand(@"SELECT * FROM ADMIN_TBL_LOGIN_USER_DETAIL WHERE LOGIN_ID= @LoginId ");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@LoginId", loginId);
                // ObjSqlCmd.Parameters.AddWithValue("@LoginName", );
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet != null)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                    this.StatusDescription = "Record not found";
                }
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
        }

        //Added on 9/7/2012
        // Mohan
        /// <summary>
        /// To get user of specific type
        /// </summary>
        /// <param name="userTypeCode">code of the user type</param>
        public void GetUserDetailsByTypeAndCountry(string userTypeCode, string countryCode)
        {
            try
            {
                StatusCode = -1000; // for error
                SqlCommand ObjSqlCmd = new SqlCommand(@"SELECT USER_NAME,LOGIN_ID FROM ADMIN_TBL_LOGIN_USER_DETAIL WHERE TYPE = @Type AND COUNTRY_CODE=@CountryCode");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@Type", userTypeCode);
                ObjSqlCmd.Parameters.AddWithValue("@CountryCode", countryCode);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet != null)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                    this.StatusDescription = "Record not found";
                }
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
        }

        public MFEAdminPanelUsers GetUserDetailsByNameAndUserCode(string name, string userTypeCode)
        {
            MFEAdminPanelUsers objAdminPanelUsers = new MFEAdminPanelUsers();
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"SELECT * FROM ADMIN_TBL_LOGIN_USER_DETAIL WHERE TYPE = @Type AND LOGIN_NAME=@LoginName");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@Type", userTypeCode);
                ObjSqlCmd.Parameters.AddWithValue("@LoginName", name);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet != null)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                    ResultTable = objDataSet.Tables[0];
                    if (ResultTable.Rows.Count > 0)
                    {
                        DataRow row = ResultTable.Rows[0];

                        objAdminPanelUsers.LoginName = Convert.ToString(row["LOGIN_NAME"]);
                        objAdminPanelUsers.Password = Convert.ToString(row["PASSWORD"]);
                        objAdminPanelUsers.Email = Convert.ToString(row["EMAIL"]);
                        objAdminPanelUsers.IsEnabled = Convert.ToBoolean(row["IS_ENABLED"]);
                        objAdminPanelUsers.ContactNo = Convert.ToString(row["CONTACT_NUMBER"]);
                        objAdminPanelUsers.CreatedOn = Convert.ToDateTime(row["CREATED_ON"]);
                        objAdminPanelUsers.LoginId = Convert.ToString(row["LOGIN_ID"]);
                        objAdminPanelUsers.ManagerId = Convert.ToString(row["MANAGER_ID"]);
                        objAdminPanelUsers.CountryCode = Convert.ToString(row["COUNTRY_CODE"]);
                        objAdminPanelUsers.City = Convert.ToString(row["CITY"]);
                        objAdminPanelUsers.UserType = Convert.ToString(row["TYPE"]);
                        objAdminPanelUsers.UserName = Convert.ToString(row["USER_NAME"]);
                        objAdminPanelUsers.IsServerAllotted = Convert.ToString(row["IS_SERVER_ALLOCATION"]) == "0" ? false : true;

                    }
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                    this.StatusDescription = "Record not found";
                }
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return objAdminPanelUsers;
        }


        //public DataTable GetUserdet( string Type)
        //{
        //    SqlCommand ObjSqlCmd = new SqlCommand(@"select LOGIN_NAME from ADMIN_TBL_LOGIN_USER_DETAIL where TYPE=@type");
        //    ObjSqlCmd.CommandType = CommandType.Text;
        //    ObjSqlCmd.Parameters.AddWithValue("@type", Type);
        //    DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
        //    return objDataSet.Tables[0];

        //}

        public DataTable ResultTable
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }

        public string StatusDescription
        {
            set;
            get;
        }

    }
}
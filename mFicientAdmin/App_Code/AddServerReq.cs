﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Runtime.Serialization;

//namespace mFicientAdmin
//{
//    public class AddServerReq
//    {
//         string _sessionId, _requestId, _companyName, _companyId, _wsUrl;
//        int _functionCode;

//        public string CompanyName
//        {
//            get { return _companyName; }
//        }
//        public string CompanyId
//        {
//            get { return _companyId; }
//        }
//        public string WSUrl
//        {
//            get { return _wsUrl; }
//        }
//        public string RequestId
//        {
//            get { return _requestId; }
//        }
        
//        public int FunctionCode
//        {
//            get { return _functionCode; }
//        }


//        public AddServerReq(string requestJson)
//        {
//            RequestJsonParsing<AddServerReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<AddServerReqData>>(requestJson);
//            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
//            if (_functionCode != (int)FUNCTION_CODES.ADD_SERVER)
//            {
//                throw new Exception("Invalid Function Code");
//            }
//            _sessionId = objRequestJsonParsing.req.sid;
//            _requestId = objRequestJsonParsing.req.rid;
//            _companyName = objRequestJsonParsing.req.data.cmpnm;
//            _companyId = objRequestJsonParsing.req.data.enid;
//            _wsUrl = objRequestJsonParsing.req.data.wsurl;

//            if (string.IsNullOrEmpty(_sessionId))
//            {
//                throw new Exception();
//            }
//        }
//    }

//    [DataContract]
//    public class AddServerReqData : Data
//    {
//        public AddServerReqData()
//        { }
//         <summary>
//         Company Name
//         </summary>
//        [DataMember]
//        public string cmpnm { get; set; }

//         <summary>
//         Webservice Url
//         </summary>
//        [DataMember]
//        public string wsurl { get; set; }
//    }
//    }
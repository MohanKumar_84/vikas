﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class GetSalesPersonsByCountry
    {
        public GetSalesPersonsByCountry()
        {
        }
        public GetSalesPersonsByCountry(int countryCode)
        {
            this.CountryCode = countryCode;
        }

        public void Process()
        {
            try
            {
                string query = @"SELECT USER_NAME,LOGIN_ID FROM ADMIN_TBL_LOGIN_USER_DETAIL
                                  WHERE COUNTRY_CODE=@CountryCode
                                  AND TYPE='S'";

                SqlCommand cmd = new SqlCommand(query);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@CountryCode", this.CountryCode);
                DataSet dsSalesPersons = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsSalesPersons.Tables[0].Rows.Count > 0)
                {
                    this.StatusDescription = "";
                    ResultTable = dsSalesPersons.Tables[0];
                }
                else
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "No records to display";
                }
            }
            catch 
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = "Internal Server Error";
            }
        }

        public DataTable ResultTable
        {
            set;
            get;
        }

        public int StatusCode
        {
            set;
            get;
        }

        public string StatusDescription
        {
            set;
            get;
        }

        public int CountryCode
        {
            set;
            get;
        }
    }
}
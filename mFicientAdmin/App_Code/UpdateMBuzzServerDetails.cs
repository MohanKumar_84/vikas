﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


namespace mFicientAdmin
{
    public class UpdateMBuzzServerDetailsAdmin
    {
        public void UpdateMBuzzServer(string servername,string serverid,string serverip,int portnumber,bool isenable)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"update ADMIN_TBL_MBUZZ_SERVER 
                                    set MBUZZ_SERVER_NAME=@servername, SERVER_IP=@serverip,PORT_NUMBER=@portnumber,ENABLED=@isenabled where MBUZZ_SERVER_ID=@serverid ");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@servername", servername);
                ObjSqlCmd.Parameters.AddWithValue("@serverid", serverid);
                ObjSqlCmd.Parameters.AddWithValue("@serverip", serverip);
                ObjSqlCmd.Parameters.AddWithValue("@portnumber", portnumber);
                ObjSqlCmd.Parameters.AddWithValue("@isenabled", isenable);
                ObjSqlCmd.CommandType = CommandType.Text;
                if (Utilities.ExecuteNonQueryRecord(ObjSqlCmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR ;
                    this.StatusDescription = "Record can not be inserted";
                }
            }
            catch
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
            }
        }

        public string StatusDescription
        {
            get;
            set;
        }

        public int StatusCode
        {
            get;
            set;
        }
    }
}
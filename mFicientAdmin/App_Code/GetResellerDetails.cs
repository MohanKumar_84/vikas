﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class GetResellerDetailsAdmin
    {
        public GetResellerDetailsAdmin()
        { }
        public GetResellerDetailsAdmin(string resellerid, string resellerLoginName)
        {
            this.ResellerId = resellerid;
            this.ResellerLoginName = resellerLoginName;
        }

        public void Process()
        {
            this.Reseller = new MFEReseller();
            try
            {
                StatusCode = -1000;
                SqlCommand ObjSqlCmd = new SqlCommand(@"select RESELLER_NAME,EMAIL,CONTACT_NUMBER,IS_ENABLE,DISCOUNT,CREATED_ON,RESELLER_ID,LOGIN_NAME,PASSWORD,CITY,COUNTRY,IS_PAYMENT_APPROVAL from ADMIN_TBL_RESELLER_DETAIL
                     where RESELLER_ID=@resellerid");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@resellerid", this.ResellerId);
                ObjSqlCmd.Parameters.AddWithValue("@LoginName", this.ResellerLoginName);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet != null && objDataSet.Tables.Count > 0)
                {
                    if (objDataSet.Tables[0].Rows.Count > 0)
                    {
                        ResultTables = objDataSet;
                        DataRow row = objDataSet.Tables[0].Rows[0];
                        Reseller.Name = Convert.ToString(row["RESELLER_NAME"]);
                        Reseller.Email = Convert.ToString(row["EMAIL"]);
                        Reseller.ContactNo = Convert.ToString(row["CONTACT_NUMBER"]);
                        Reseller.IsEnabled = Convert.ToBoolean(row["IS_ENABLE"]);
                        Reseller.Discount = Convert.ToDecimal(row["DISCOUNT"]);
                        Reseller.CreatedOn = Convert.ToDateTime(row["CREATED_ON"]);
                        Reseller.ResellerId = Convert.ToString(row["RESELLER_ID"]);
                        Reseller.LoginName = Convert.ToString(row["LOGIN_NAME"]);
                        Reseller.Password = Convert.ToString(row["PASSWORD"]);
                        Reseller.City = Convert.ToString(row["CITY"]);
                        Reseller.Country = Convert.ToString(row["COUNTRY"]);
                        Reseller.IsPaymentApproved = Convert.ToBoolean(row["IS_PAYMENT_APPROVAL"]);

                        this.StatusCode = 0;
                        this.StatusDescription = "";
                    }
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                    this.StatusDescription = "Record not found";
                }
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }

        }

        public void GetList(string type)
        {
            try
            {
                string strQuery = "select USER_NAME,LOGIN_ID from ADMIN_TBL_LOGIN_USER_DETAIL where TYPE=@type;";

                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@type", type);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                }
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public void GetReseller()
        {
            try
            {
                string strQuery = "select login_name,RESELLER_ID from ADMIN_TBL_RESELLER_DETAIL";

                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;

                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                }
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public string getDirectSalesResellerId()
        {
            string strResellerId = String.Empty;
            try
            {
                string strQuery = @"SELECT * FROM ADMIN_TBL_RESELLER_DETAIL
                                    WHERE IS_DIRECT_SALES = 1 ";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                DataSet dsResellerDtl = Utilities.SelectDataFromSQlCommand(cmd);
                if (dsResellerDtl == null) throw new Exception("No record found");
                if (dsResellerDtl.Tables[0].Rows.Count > 0)
                {
                    strResellerId = Convert.ToString(dsResellerDtl.Tables[0].Rows[0]["RESELLER_ID"]);
                }
                else
                {
                    strResellerId = String.Empty;
                }
            }
            catch
            {
                strResellerId = String.Empty;
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return strResellerId;
        }


        public DataSet ResultTables
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public string ResellerId
        {
            set;
            get;
        }
        public string Country
        {
            set;
            get;
        }
        public string ResellerLoginName
        {
            set;
            get;
        }
        public MFEReseller Reseller
        {
            set;
            get;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class GetTeamListByUserType
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userType">"seller","sm"</param>
        /// <param name="userId">seller id or sales manager id</param>
        public GetTeamListByUserType(string userType, string userId)
        {
            this.UserType = userType;
            this.UserId = userId;
        }

        public void Process()
        {
            StatusCode = -1000;//for error
            try
            {
                DataSet dsTeamList = getTeamListByUserType();
                if (dsTeamList != null)
                {
                    TeamList = dsTeamList;
                    StatusCode = 0;
                    StatusDescription = "";
                }
                else
                {
                    StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                    StatusDescription = "No team list found";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        DataSet getTeamListByUserType()
        {
            try
            {
                string strQuery = getSqlQueryByType();
                SqlCommand cmd = new SqlCommand(strQuery);
                if (UserType.ToLower() == "sm")
                {
                    cmd.Parameters.AddWithValue("@ManagerId", UserId);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@SellerId", UserId);
                }
                cmd.CommandType = CommandType.Text;
                //DataSet dsTeamList = Utilities.SelectDataFromSQlCommand(cmd);
                return Utilities.SelectDataFromSQlCommand(cmd);
            }
            catch(Exception ex)
            {
                this.StatusDescription = ex.Message;
                return null;
            }
        }

        string getSqlQueryByType()
        {
            string sqlQuery = "";
            try
            {
                if (UserType.ToLower() == "sm")
                {
                    sqlQuery = @"SELECT * FROM ADMIN_TBL_LOGIN_USER_DETAIL
                                WHERE MANAGER_ID =@ManagerId
                                AND IS_ENABLED =1;


                                SELECT Reseller.* FROM ADMIN_TBL_RESELLER_SALES_PERSONS AS ResellerSales
                                INNER JOIN ADMIN_TBL_RESELLER_DETAIL AS Reseller
                                ON Reseller.RESELLER_ID = ResellerSales.RESELLER_ID
                                WHERE SELLER_ID IN (SELECT LOGIN_ID 
					                                FROM ADMIN_TBL_LOGIN_USER_DETAIL
					                                WHERE TYPE = 'S'
					                                AND MANAGER_ID =@ManagerId)";
                }
                else
                {
                    sqlQuery = @"SELECT * FROM ADMIN_TBL_LOGIN_USER_DETAIL
                             WHERE LOGIN_ID = (SELECT MANAGER_ID
					                           FROM ADMIN_TBL_LOGIN_USER_DETAIL
					                           WHERE LOGIN_ID =@SellerId);	
					
                            SELECT Reseller.* FROM ADMIN_TBL_RESELLER_SALES_PERSONS AS ResellerSales
                            INNER JOIN ADMIN_TBL_RESELLER_DETAIL AS Reseller
                            ON Reseller.RESELLER_ID = ResellerSales.RESELLER_ID
                            WHERE ResellerSales.SELLER_ID = @SellerId";
                }
            }
            catch (Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return sqlQuery;
        }
        public string UserType
        {
            get;
            set;
        }
        public string UserId
        {
            get;
            set;
        }
        public DataSet TeamList
        {
            get;
            set;
        }
        public int StatusCode
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
    }
}
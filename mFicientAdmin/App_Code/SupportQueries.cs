﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class SupportQueriesAdmin
    {
        public DataTable GetClosedQueries()
        {
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select * from ( select u.FIRST_NAME +' '+u.LAST_NAME+'( Mobile User )' as user_name, q.TOKEN_NO,q.QUERY_SUBJECT,t.QUERY_TYPE,POSTED_ON,q.RESOLVED_BY_CREATOR,q.company_id  from ADMIN_TBL_QUERY q inner join ADMIN_TBL_QUERY_TYPE t on q.QUERY_TYPE=t.QUERY_CODE
                inner join TBL_USER_DETAIL u on u.user_id=q.USER_ID and t.category=1 where  q.RESOLVED_BY_CREATOR=1 and IS_FORWARD_TO_ADMIN=1
                union 
                select u.FULL_NAME+'( Subadmin )' as user_name, q.TOKEN_NO,q.QUERY_SUBJECT,t.QUERY_TYPE,POSTED_ON,q.RESOLVED_BY_CREATOR,q.company_id  from ADMIN_TBL_QUERY q inner join ADMIN_TBL_QUERY_TYPE t on q.QUERY_TYPE=t.QUERY_CODE
                inner join TBL_SUB_ADMIN u on u.SUBADMIN_ID=q.USER_ID and t.category=2 where  q.RESOLVED_BY_CREATOR=1 )as tbl  order by POSTED_ON desc");
                //objCmd.Parameters.AddWithValue("@querytype", 1);
               // objCmd.Parameters.AddWithValue("@isresolved", 0);
                DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
                return ds.Tables[0];
            }
            catch
            {
                return null;
            }
        }

        public DataTable GetOpenQueries()
        {
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select *,'' as PostDate from ( select u.FIRST_NAME +' '+u.LAST_NAME+'( Mobile User )' as user_name, q.TOKEN_NO,q.QUERY_SUBJECT,t.QUERY_TYPE,POSTED_ON,q.RESOLVED_BY_CREATOR,q.company_id  from ADMIN_TBL_QUERY q inner join ADMIN_TBL_QUERY_TYPE t on q.QUERY_TYPE=t.QUERY_CODE
                inner join TBL_USER_DETAIL u on u.user_id=q.USER_ID and t.category=1 where  q.RESOLVED_BY_CREATOR=0 and IS_FORWARD_TO_ADMIN=1
                union 
                select u.FULL_NAME+'( Subadmin )' as user_name, q.TOKEN_NO,q.QUERY_SUBJECT,t.QUERY_TYPE,POSTED_ON,q.RESOLVED_BY_CREATOR,q.company_id  from ADMIN_TBL_QUERY q inner join ADMIN_TBL_QUERY_TYPE t on q.QUERY_TYPE=t.QUERY_CODE
                inner join TBL_SUB_ADMIN u on u.SUBADMIN_ID=q.USER_ID and t.category=2 where  q.RESOLVED_BY_CREATOR=0 )as tbl  order by POSTED_ON desc");
                //objCmd.Parameters.AddWithValue("@querytype", 0);
                // objCmd.Parameters.AddWithValue("@userid", UserId);
                //objCmd.Parameters.AddWithValue("@resolvedbycreator", 1);
                DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
                for (int i=0;i<ds.Tables[0].Rows.Count;i++)
                {
                    ds.Tables[0].Rows[i]["PostDate"] = (new DateTime(Convert.ToInt64(ds.Tables[0].Rows[i]["POSTED_ON"]))).ToLongDateString();
                }
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public void QueryDetails(string TokenNumber)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select QUERY_SUBJECT,USER_ID,QUERY from ADMIN_TBL_QUERY where TOKEN_NO=@tokennumber");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@tokennumber", TokenNumber);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                this.ResultTable = objDataSet.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public string UserName(string UserId)
        {
            string name = string.Empty;
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select LOGIN_NAME from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_ID=@loginid");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@loginid", UserId);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    name = Convert.ToString(objDataSet.Tables[0].Rows[0]["LOGIN_NAME"]);
                    return name;
                }
                else
                {
                    return name = "";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return name;
        }

        
        public string UserId(string UserName)
        {
            string strid = string.Empty;
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select LOGIN_ID from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@loginname");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@loginname", UserName);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    strid = Convert.ToString(objDataSet.Tables[0].Rows[0]["LOGIN_ID"]);
                    return strid;
                }
                else
                {
                    return strid="";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return strid;
        }

        public DataTable GetCommentsList(string tokennumber)
        {
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select s.LOGIN_NAME,t.COMMENT from ADMIN_TBL_LOGIN_USER_DETAIL s inner join ADMIN_TBL_QUERY_THREAD t
                                                on s.LOGIN_ID=t.COMMENT_BY where Token_no=@tokenno order by t.COMMENTED_ON DESC");
                objCmd.Parameters.AddWithValue("@tokenno", tokennumber);
                DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public void AddQuery(string TokenNumber, string CommentId, string CommentBy, int UserType, string Comment)
        {
            try
            {
                SqlCommand objSqlCmd = new SqlCommand(@"insert into ADMIN_TBL_QUERY_THREAD values(@tokennumber,@commentid,@commentby,@usertype,@comment,@commentedon)");
                objSqlCmd.Parameters.AddWithValue("@tokennumber", TokenNumber);
                objSqlCmd.Parameters.AddWithValue("@commentid", CommentId);
                objSqlCmd.Parameters.AddWithValue("@commentby", CommentBy);
                objSqlCmd.Parameters.AddWithValue("@usertype", UserType);
                objSqlCmd.Parameters.AddWithValue("@comment", Comment);
                objSqlCmd.Parameters.AddWithValue("@commentedon", DateTime.Now.Ticks);

                if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public void GetCompany()
        {
            try
            {
                string strQuery = "select COMPANY_NAME from ADMIN_TBL_COMPANY_DETAIL";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                ResultTable = objDataSet.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public void GetToken()
        {
            try
            {
                string strQuery = "SELECT TOKEN_NO FROM ADMIN_TBL_QUERY";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }


        public void GetCountry()
        {
            try
            {
                string strQuery = "select COUNTRY_NAME from ADMIN_TBL_MST_COUNTRY";
                SqlCommand objSqlCommand = new SqlCommand(strQuery);
                objSqlCommand.CommandType = CommandType.Text;
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                this.ResultTable = objDataSet.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public string GetTokenNo(string userid)
        {
            string name = string.Empty;
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select TOKEN_NO from ADMIN_TBL_QUERY where USER_ID=@userid");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@userid", userid);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    name = Convert.ToString(objDataSet.Tables[0].Rows[0]["TOKEN_NO"]);
                    return name;
                }
                else
                {
                    return name = "";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return name;
        }

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
        public DataTable ResultTable
        {
            set;
            get;
        }
    }
}
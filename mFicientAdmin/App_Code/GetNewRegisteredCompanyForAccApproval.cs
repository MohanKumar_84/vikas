﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    public class GetNewRegisteredCompanyForAccApproval
    {
        /// <summary>
        /// for complete list
        /// </summary>

        public GetNewRegisteredCompanyForAccApproval()
        {

        }
        /// <summary>
        /// for particular company
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="planCode"></param>
        /// <param name="transactionType"></param>

        public GetNewRegisteredCompanyForAccApproval(string companyId, string planCode, string transactionType)
        {
            this.CompanyId = companyId;
            this.PlanCode = planCode;
            this.TransactionType = transactionType;
        }
        public void Process()
        {
            StatusCode = -1000;//for error
            try
            {
                if (String.IsNullOrEmpty(CompanyId))
                {
                    ResultTable = getAllNewRegisteredCompany();
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    ResultTable = getNewRegisteredCompany();
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
            }
            catch
            {
                StatusCode = -1000;
                StatusDescription = "Internal server error";
            }
        }

        DataTable getAllNewRegisteredCompany()
        {
            try
            {
                string strQuery = @"SELECT PurchaseLog.*,CompDetl.COMPANY_NAME FROM ADMIN_TBL_PLAN_PURCHASE_LOG AS PurchaseLog
                                INNER JOIN ADMIN_TBL_COMPANY_DETAIL AS CompDetl
                                ON PurchaseLog.COMPANY_ID = CompDetl.COMPANY_ID
                                WHERE APPROVED = 0
                                AND CompDetl.SM_APPROVED=1
                                AND TRANSACTION_TYPE = '5'";//transaction type = 5 is for new purchase //see enum PLAN_RENEW_OR_CHANGE_REQUESTS

                SqlCommand cmd = new SqlCommand(strQuery);
                return Utilities.SelectDataFromSQlCommand(cmd).Tables[0];
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
                return null;
            }
        }

        DataTable getNewRegisteredCompany()
        {
            try
            {
                string strQuery = @"SELECT PurchaseLog.*,CompDetl.COMPANY_NAME FROM ADMIN_TBL_PLAN_PURCHASE_LOG AS PurchaseLog
                                INNER JOIN ADMIN_TBL_COMPANY_DETAIL AS CompDetl
                                ON PurchaseLog.COMPANY_ID = CompDetl.COMPANY_ID
                                WHERE APPROVED = 0
                                AND CompDetl.SM_APPROVED=1
                                AND PurchaseLog.COMPANY_ID = @CompanyId
                                AND PurchaseLog.PLAN_CODE = @PlanCode
                                AND PurchaseLog.TRANSACTION_TYPE = @TransactionType";//transaction type = 5 is for new purchase //see enum PLAN_RENEW_OR_CHANGE_REQUESTS

                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
                cmd.Parameters.AddWithValue("@PlanCode", this.PlanCode);
                cmd.Parameters.AddWithValue("@TransactionType", this.TransactionType);
                return Utilities.SelectDataFromSQlCommand(cmd).Tables[0];
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
                return null;
            }
        }
        public int StatusCode
        {
            get;
            set;
        }
        public string StatusDescription
        {
            get;
            set;
        }
        public DataTable ResultTable
        {
            get;
            set;
        }

        public string CompanyId
        {
            get;
            set;
        }

        public string PlanCode
        {
            get;
            set;
        }
        public string TransactionType
        {
            get;
            set;
        }
    }
}
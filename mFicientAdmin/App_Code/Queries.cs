﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class QueriesAdmin
    {
        public DataTable GetClosedQueries()
        {
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select l.ADMIN_NAME as NAME,q.USER_ID, q.TOKEN_NO,q.QUERY_SUBJECT,
                                                CONVERT(varchar(10),DATEADD(ss, (POSTED_ON / CAST(10000 AS bigint)) % 86400000,
											    DATEADD(DD, POSTED_ON / CAST(864000000000 AS bigint), 0)) - 109207, 101)  as PostedDate  from ADMIN_TBL_QUERY q 
                                                inner join ADMIN_TBL_ADMIN_LOGIN l on q.USER_ID=l.ADMIN_ID
                                                where q.QUERY_TYPE=@querytype and q.IS_RESOLVED=@isresolved ");
                objCmd.Parameters.AddWithValue("@querytype", 1);
                objCmd.Parameters.AddWithValue("@isresolved", 0);
                DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
                return ds.Tables[0];
            }
            catch
            {
               this.StatusCode=(int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
               this.StatusDescription = "Record Not Found";
               return null;
            }
        }

        public DataTable GetOpenQueries()
        {
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select l.ADMIN_NAME as NAME, q.USER_ID, q.TOKEN_NO,q.QUERY_SUBJECT,
                                           CONVERT(varchar(10),DATEADD(ss, (POSTED_ON / CAST(10000 AS bigint)) % 86400000,
													DATEADD(DD, POSTED_ON / CAST(864000000000 AS bigint), 0)) - 109207, 101) as PostedDate from ADMIN_TBL_QUERY q inner join ADMIN_TBL_ADMIN_LOGIN l
                                             on q.USER_ID=l.ADMIN_ID
                                             where QUERY_TYPE=@querytype and RESOLVED_BY_CREATOR=@resolvedbycreator");
                objCmd.Parameters.AddWithValue("@querytype", 0);
                //objCmd.Parameters.AddWithValue("@userid", UserId);
                objCmd.Parameters.AddWithValue("@resolvedbycreator", 1);
                DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
                return null;
            }
        }


        public void QueryDetails(string TokenNumber)
        {
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select QUERY_SUBJECT,USER_ID,QUERY from ADMIN_TBL_QUERY where TOKEN_NO=@tokenno");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@tokenno", TokenNumber);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    ResultTable = objDataSet.Tables[0];
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                    this.StatusDescription = "Record Not Found";
                }
            }
            catch (Exception e)
            {
             this.StatusDescription = e.Message;
             this.StatusCode=((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR);
            }
        }

       
        public string UserId(string UserName)
        {
            string strid = string.Empty;
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select ADMIN_ID from ADMIN_TBL_ADMIN_LOGIN where ADMIN_NAME=@adminnname");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@adminnname", UserName);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    strid = Convert.ToString(objDataSet.Tables[0].Rows[0]["ADMIN_ID"]);
                    return strid;
                }
                else
                {
                    return strid = "";
                }
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
            return strid;
        }

        public DataTable GetCommentsList(string tokennumber)
        {
            try
            {
                SqlCommand objCmd = new SqlCommand(@"select s.ADMIN_NAME,t.COMMENT from ADMIN_TBL_ADMIN_LOGIN s inner join ADMIN_TBL_QUERY_THREAD t on s.ADMIN_ID=t.COMMENT_BY
                                             where Token_no=@tokenno order by COMMENT");
                objCmd.Parameters.AddWithValue("@tokenno", tokennumber);
                DataSet ds = Utilities.SelectDataFromSQlCommand(objCmd);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
                return null;
            }
        }


        public void AddQuery(string TokenNumber, string CommentId, string CommentBy, int UserType, string Comment)
        {
            try
            {
                SqlCommand objSqlCmd = new SqlCommand(@"insert into ADMIN_TBL_QUERY_THREAD values(@tokennumber,@commentid,@commentby,@usertype,@comment,@commentedon)");
                objSqlCmd.Parameters.AddWithValue("@tokennumber", TokenNumber);
                objSqlCmd.Parameters.AddWithValue("@commentid", CommentId);
                objSqlCmd.Parameters.AddWithValue("@commentby", CommentBy);
                objSqlCmd.Parameters.AddWithValue("@usertype", UserType);
                objSqlCmd.Parameters.AddWithValue("@comment", Comment);
                objSqlCmd.Parameters.AddWithValue("@commentedon", DateTime.Now.Ticks);

                if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                    this.StatusDescription = "Record Insert Error";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_INSERT_ERROR;
                this.StatusDescription = e.Message;
            }
        }

        public string GetTokenNo(string UserId)
        {
            string TokenNumber = string.Empty;
            try
            {
                SqlCommand ObjSqlCmd = new SqlCommand(@"select TOKEN_NO from ADMIN_TBL_QUERY where USER_ID=@userid");
                ObjSqlCmd.CommandType = CommandType.Text;
                ObjSqlCmd.Parameters.AddWithValue("@userid", UserId);
                DataSet objDataSet = Utilities.SelectDataFromSQlCommand(ObjSqlCmd);
                if (objDataSet.Tables[0].Rows.Count > 0)
                {
                    TokenNumber = Convert.ToString(objDataSet.Tables[0].Rows[0]["TOKEN_NO"]);
                    return TokenNumber;
                }
                else
                {
                    return TokenNumber = "";
                }
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
            }
            return TokenNumber;
          }
                
            

        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }


        public DataTable ResultTable
        {
            set;
            get;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientAdmin
{
    public class CompanyPlanChangesHelpers
    {
        MFEPlanPurchaseLog _planPurchaseLog;

        public MFEPlanPurchaseLog PlanPurchaseLog
        {
            get { return _planPurchaseLog; }
            private set { _planPurchaseLog = value; }
        }
        enum Month
        {
            January = 1,
            February = 2,
            March = 3,
            April = 4,
            May = 5,
            June = 6,
            July = 7,
            August = 8,
            September = 9,
            October = 10,
            November = 11,
            December = 12
        }
        public CompanyPlanChangesHelpers(MFEPlanPurchaseLog planPurchaseLog)
        {
            this.PlanPurchaseLog = planPurchaseLog;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ///<exception cref="System.Exception">Thrown when the public property used is not set...and the plan selected is trial</exception>
        ///<exception cref="System.ArgumentException"></exception>
        public Double getUserChargeForRemainingMonth(
            out double chargePerDay)
        {
            double dblUserChargePerMonth = getUserChargeForRemainingMonth();
            chargePerDay = Convert.ToDouble(dblUserChargePerMonth / getNoOfDaysOfCurrentMonth());
            return dblUserChargePerMonth;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="noOfUsersAdded"></param>
        /// <returns></returns>
        ///<exception cref="System.Exception">Thrown when the public property used is not set...and the plan selected is trial</exception>
        ///<exception cref="System.ArgumentException"></exception>
        public Double getUserChargeForRemainingMonth()
        {
            if (this.PlanPurchaseLog.UserChargePerMonth <= 0)
                throw new Exception("Give a valid user charge per month");

            if (this.PlanPurchaseLog.MaxUsers <= 0)
                throw new Exception("Give a valid max no of users.");

            if (String.IsNullOrEmpty(this.PlanPurchaseLog.ChargeType))
                throw new Exception("Give a valid Charge Type");

            DateTime dtNow = DateTime.UtcNow;
            int iNoOfDays = getNoOfDaysOfCurrentMonth();

            DateTime dtLastDayOfMonth = new DateTime(dtNow.Year, dtNow.Month, iNoOfDays, 23, 59, 59).ToUniversalTime();

            TimeSpan tsRemaining = dtLastDayOfMonth - dtNow;
            double dblChargePerDay = Convert.ToDouble(this.PlanPurchaseLog.UserChargePerMonth / iNoOfDays);
            return Utilities.roundOfAmountByCurrencyType(
                (dblChargePerDay * tsRemaining.TotalDays * (this.PlanPurchaseLog.MaxUsers)),
                (CURRENCY_TYPE)Enum.Parse(typeof(CURRENCY_TYPE), this.PlanPurchaseLog.ChargeType)
                );
        }
        public double getBalanceAmtAfterDeductingThisMonthChg(
            double totalAmount)
        {
            return Utilities.roundOfAmountByCurrencyType(
                totalAmount - getUserChargeForRemainingMonth(),
                (CURRENCY_TYPE)Enum.Parse(typeof(CURRENCY_TYPE), this.PlanPurchaseLog.ChargeType)
                );
        }
        int getNoOfDaysOfCurrentMonth()
        {
            DateTime dtNow = DateTime.UtcNow;
            Month month = (Month)Enum.Parse(typeof(Month), Convert.ToString(dtNow.Month));
            return getNoOfDaysInMonth(month, dtNow.Year);
        }
        int getNoOfDaysInMonth(Month month, int year)
        {
            int iNoOfDays = 0;
            switch (month)
            {
                case Month.January:
                    iNoOfDays = 31;
                    break;
                case Month.February:
                    if (System.DateTime.IsLeapYear(year))
                    {
                        iNoOfDays = 29;
                    }
                    else
                    {
                        iNoOfDays = 28;
                    }
                    break;
                case Month.March:
                    iNoOfDays = 31;
                    break;
                case Month.April:
                    iNoOfDays = 30;
                    break;
                case Month.May:
                    iNoOfDays = 31;
                    break;
                case Month.June:
                    iNoOfDays = 30;
                    break;
                case Month.July:
                    iNoOfDays = 31;
                    break;
                case Month.August:
                    iNoOfDays = 31;
                    break;
                case Month.September:
                    iNoOfDays = 30;
                    break;
                case Month.October:
                    iNoOfDays = 31;
                    break;
                case Month.November:
                    iNoOfDays = 30;
                    break;
                case Month.December:
                    iNoOfDays = 31;
                    break;
            }
            return iNoOfDays;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientAdmin
{
    public class LogoutUsersAdmin
    {
        public void Process(string UserId)
        {
            SqlConnection objSqlConnection = null;
            SqlTransaction objSqlTransaction = null;
            SqlCommand objSqlCommand;
            try
            {
                Utilities.SqlConnectionOpen(out objSqlConnection);
                string query = @"DELETE FROM ADMIN_TBL_LOGIN_USER_SESSION where USER_ID=@USER_ID";
                objSqlTransaction = objSqlConnection.BeginTransaction();
                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", UserId);
                //objSqlCommand.Parameters.AddWithValue("@SessionId", SessionId);

                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    ((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString();
                }

                query = @"update ADMIN_TBL_LOGIN_USER_HISTORY set LOGOUT_DATETIME=@LOGOUT_DATETIME, LOGOUT_TYPE='normal' where user_id=@user_id and LOGIN_DEVICE_TOKEN=@login_device_token
                              and LOGIN_DEVICE_TYPE_CODE=@login_device_type_code AND LOGOUT_DATETIME=0";

                objSqlCommand = new SqlCommand(query, objSqlConnection, objSqlTransaction);
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@USER_ID", UserId);
                objSqlCommand.Parameters.AddWithValue("@login_device_type_code", "web");
                objSqlCommand.Parameters.AddWithValue("@login_device_token", "");
                objSqlCommand.Parameters.AddWithValue("@LOGOUT_DATETIME", DateTime.Now.Ticks);
                if (objSqlCommand.ExecuteNonQuery() > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    ((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString();
                }
                objSqlTransaction.Commit();
            }
            catch (Exception ex)
            {
                objSqlTransaction.Rollback();
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR;
                    this.StatusDescription = ex.Message;
                }
            }
        }

        
        public string GetUserId(string Username,string Type)
        {
            string strId=string.Empty;
            try
            {
                SqlCommand objSqlCommand = new SqlCommand(@"select LOGIN_ID from ADMIN_TBL_LOGIN_USER_DETAIL where LOGIN_NAME=@loginname and TYPE=@type");
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@loginname", Username);
                objSqlCommand.Parameters.AddWithValue("@type", Type);
                DataSet objDataset = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataset.Tables[0].Rows.Count > 0)
                {
                    strId = objDataset.Tables[0].Rows[0]["LOGIN_ID"].ToString();
                }
                else
                {
                    return strId = "";
                }
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
            return strId;
        }

       
        public string GetResellerId(string loginname)
        {
            string strResellerId=string.Empty;
            try
            {
                SqlCommand objSqlCommand = new SqlCommand(@"select RESELLER_ID from ADMIN_TBL_RESELLER_DETAIL where LOGIN_NAME=@loginname");
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Parameters.AddWithValue("@loginname", loginname);
                DataSet objDataset = Utilities.SelectDataFromSQlCommand(objSqlCommand);
                if (objDataset.Tables[0].Rows.Count > 0)
                {
                    return strResellerId = objDataset.Tables[0].Rows[0]["RESELLER_ID"].ToString();
                }
                else
                {
                    return strResellerId = "";
                }
            }
            catch (Exception e)
            {
               this.StatusDescription = e.Message;
               this.StatusCode= (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
            }
            return strResellerId;
        }

        //string strusername;
        //public string GetUserName()
        //{
        //    SqlCommand objSqlCommand = new SqlCommand(@"select ADMIN_NAME from ADMIN_TBL_ADMIN_LOGIN");
        //    objSqlCommand.CommandType = CommandType.Text;
        //  //  objSqlCommand.Parameters.AddWithValue("@loginname", loginname);
        //    DataSet objDataset = Utilities.SelectDataFromSQlCommand(objSqlCommand);

        //    if (objDataset.Tables[0].Rows.Count > 0)
        //    {
        //        strusername = objDataset.Tables[0].Rows[0]["ADMIN_NAME"].ToString();

        //    }
        //    return strusername;
        //}
        public string UserId
        {
            set;
            get;
        }
        public string SessionID
        {
            set;
            get;
        }
        public string LocationName
        {
            set;
            get;
        }
        public int StatusCode
        {
            set;
            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientAdmin
{
    public class AddPlanDetailsAdmin
    {
        public void AddPlan(string plancode, string planId, string planame, int maxworkflow, int msgperday, int msgpermonth, bool Isenabled,int MinUsers)
        {
            try
            {
                //to check if plan code and plan name are unique
                if (check(plancode, planame) == true)
                {
                    this.StatusDescription = "Please check the value entered";
                }
                else
                {
                    SqlCommand objSqlCmd = new SqlCommand(@"insert into ADMIN_TBL_PLANS (PLAN_CODE,PLAN_ID,PLAN_NAME,MAX_WORKFLOW,PUSHMESSAGE_PERDAY,PUSHMESSAGE_PERMONTH,IS_ENABLED,MIN_USERS)
                    VALUES(@plancode,@planid,@planname,@maxworkflow,@msgperday,@msgpermonth,@isenabled,@minusers)");
                    objSqlCmd.Parameters.AddWithValue("@plancode", plancode);
                    objSqlCmd.Parameters.AddWithValue("@planid", planId);
                    objSqlCmd.Parameters.AddWithValue("@planname", planame);
                    objSqlCmd.Parameters.AddWithValue("@maxworkflow", maxworkflow);
                    objSqlCmd.Parameters.AddWithValue("@msgperday", msgperday);
                    objSqlCmd.Parameters.AddWithValue("@msgpermonth", msgpermonth);
                    objSqlCmd.Parameters.AddWithValue("@isenabled", Isenabled);
                    objSqlCmd.Parameters.AddWithValue("@minusers", MinUsers);

                    if (Utilities.ExecuteNonQueryRecord(objSqlCmd) > 0)
                    {
                        this.StatusCode = 0;
                        this.StatusDescription = "";
                    }
                    else
                    {
                        this.StatusCode = 1000;
                        this.StatusDescription = "Record can't be inserted";
                    }
                }
            }

            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = 1000;
                    this.StatusDescription = ex.Message;
                }
            }
        }

        private bool check(string plancode, string planname)
        {
            try
            {
                SqlCommand objSqlCmd = new SqlCommand(@" select * from ADMIN_TBL_PLANS WHERE PLAN_CODE=@plancode OR PLAN_NAME=@planame");
                objSqlCmd.Parameters.AddWithValue("@plancode", plancode);
                objSqlCmd.Parameters.AddWithValue("@planame", planname);
                DataSet objds = Utilities.SelectDataFromSQlCommand(objSqlCmd);
                if (objds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                }
            
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
            }
            return false;
        }
        public DataTable GetPlanList()
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand("select * from ADMIN_TBL_PLANS order by PLAN_CODE");
                DataSet ds= Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch (Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                return null;
            }
        }

        public DataTable GetPlanDiscountList(string plancode)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand("select c.CURRENCY,p.TO_MONTH,p.FROM_MONTH,p.PRICE from ADMIN_TBL_PLan_currency c left Outer join ADMIN_TBL_PLAN_PRICE p on c.currency=p.CURRENCY WHERE p.PLAN_CODE=@plancode");
                sqlCommand.Parameters.AddWithValue("@plancode", plancode);
                DataSet ds= Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                this.StatusDescription = e.Message;
                return null;
            }
        }

        public DataTable GetDiscountListToAddPaln()
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand("select CURRENCY from ADMIN_TBL_PLan_currency");
                DataSet ds = Utilities.SelectDataFromSQlCommand(sqlCommand);
                return ds.Tables[0];
            }
            catch(Exception e)
            {
                this.StatusDescription = e.Message;
                this.StatusCode = (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR;
                return null;
            }
        }

        public void AddPlanPrice(int frommonth, int tomonth, decimal price, string currency, string plancode)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(@"insert into ADMIN_TBL_PLAN_PRICE(FROM_MONTH,TO_MONTH,PRICE,CURRENCY,PLAN_CODE)VALUES(@frommonth,@tomonth,@price,@currency,@plancode)");
                cmd.Parameters.AddWithValue("@frommonth", frommonth);
                cmd.Parameters.AddWithValue("@tomonth", tomonth);
                cmd.Parameters.AddWithValue("@price", price);
                cmd.Parameters.AddWithValue("@currency", currency);
                cmd.Parameters.AddWithValue("@plancode", plancode);
                if (Utilities.ExecuteNonQueryRecord(cmd) > 0)
                {
                    this.StatusCode = 0;
                    this.StatusDescription = "";
                }
                else
                {
                    this.StatusCode = 1000;
                    this.StatusDescription = "Record can't be inserted";
                }
            }
                catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    this.StatusCode = 1000;
                    this.StatusDescription = ex.Message;
                }
            }
            }

        public string Loginname
        {
            get;
            set;
        }
        public string plancode
        {
            get;
            set;

        }
        public string planname
        {
            get;
            set;
        }
        public int maxworkflow
        {
            get;
            set;
        }
        public decimal userchargeinr
        {
            get;
            set;
        }
        public decimal userchargeusd
        {
            get;
            set;
        }
        public int msgperday
        {
            get;
            set;
        }
        public int msgpermonth
        {
            get;
            set;
        }
        public int StatusCode
        {
            set;

            get;
        }
        public string StatusDescription
        {
            set;
            get;
        }
    }
}
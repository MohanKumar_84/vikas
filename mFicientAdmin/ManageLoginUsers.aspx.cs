﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace mFicientAdmin
{
    public partial class ManageLoginUsers : System.Web.UI.Page
    {
        enum PanelsAvailable
        {
            UserDetails,
            UserForm,
            ResellerForm,
            AllForm,
            SalesPersonsForm,
            SalesManager,
            SupportForm,
            AccountForm,
        }
        enum UserTypes
        {
            SalesManager,
            SalesPerson,
            Reseller,
            Support,
            Account,
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string hfsValue = string.Empty;

            string strUserName = string.Empty;
            string strSessionId = string.Empty;
            string strRole = string.Empty;

            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;

            if (!Page.IsPostBack)
            {
                Utilities.BindCountryDropDown(ddlcountry);
                Utilities.BindCountryDropDown(ddlUsersCountry);
                ddlUserSalesManager.Items.Clear();
                ddl_Users.SelectedIndex = ddl_Users.Items.IndexOf(ddl_Users.Items.FindByValue("all"));
                bindUserDetailsRepeater();
                /*if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else
                {
                    if (Page.PreviousPage != null)
                    {
                        string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                        prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                        ContentPlaceHolder cphPageCanvas = (ContentPlaceHolder)PreviousPage.Master.Master.FindControl("form").FindControl("MainCanvas").FindControl("PageCanvas");

                        hfs.Value = ((HiddenField)cphPageCanvas.FindControl("hfs")).Value;
                    }
                }*/
               
            }
            else
            {
                
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "postBackScript", " $(\"select\").uniform();$(\"input\").uniform();$(\"#content\").find('div.widgets').wl_Widget();$('#aspnetForm').wl_Form();", true);
            }
        }

        protected void lnkAddNewUser_Click(object sender, EventArgs e)
        {
            divddl.Visible = false;
            clearControls();
            switch (ddl_Users.SelectedValue)
            {
                case "all":
                    enableDisablePanel(PanelsAvailable.AllForm, false);
                    break;
                case "Reseller":
                    enableDisablePanel(PanelsAvailable.ResellerForm, false);
                    Resellerdet.InnerText = "Add Reseller";
                    if (ddlcountry.SelectedValue == "-1")
                    {
                        check.Visible = true;
                        chcklist.Visible = false;
                    }
                    break;
                case "SalesPerson":
                    enableDisablePanel(PanelsAvailable.SalesPersonsForm, false);
                    Usersdet.InnerText = "Add Sales Person";
                    if (ddl_Users.SelectedValue.ToLower() == "salesperson")
                    {
                        if (ddlUsersCountry.SelectedValue == "-1")
                        {
                            checkusers.Visible = true;
                            ddlcheck.Visible = false;
                        }
                        else if (ddlUsersCountry.SelectedValue != "-1")
                        {
                            checkusers.Visible = false;
                            bindSalesManagerDropDown();
                            ddlcheck.Visible = true;
                        }
                    }
                    break;
                case "SalesManager":
                    enableDisablePanel(PanelsAvailable.SalesManager, false);
                    Usersdet.InnerText = "Add Sales Manager";
                    break;
                case "Support":
                    enableDisablePanel(PanelsAvailable.SupportForm, false);
                    Usersdet.InnerText = "Add Supports Person";
                    break;
                case "Account":
                    enableDisablePanel(PanelsAvailable.AccountForm, false);
                    Usersdet.InnerText = "Add Accounts Person";
                    break;
            }
            //enableDisablePanel(PanelsAvailable.LoginType);
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);

        }

        /* for users*/

        void enableDisablePanelsUsers(string mode)
        {
            if (mode == "Add")
            {
                pnlRepeaterBox.Visible = false;
                //PnlUser.Visible = true;
            }
            else if (mode == "Edit")
            {
                pnlRepeaterBox.Visible = false;
                //PnlUser.Visible = true;
            }
            else
            {
                clearControls();
                pnlRepeaterBox.Visible = true;
                //PnlUser.Visible = false;
            }
        }

        /* for resellers */

        void enableDisablePanelsLogin(string mode)
        {
            if (mode == "Add")
            {
                //pnlRepeaterBox.Visible = false;
                //Pnlreseller.Visible = true;
                //pnlAllForms.Visible = true;
                pnlLoginType.Visible = true;
                pnlUserDetails.Visible = false;
            }
            else if (mode == "Edit")
            {
                pnlRepeaterBox.Visible = false;
                pnlreseller.Visible = true;
            }
            else
            {
                clearControls();
                pnlRepeaterBox.Visible = true;
                pnlreseller.Visible = false;
            }
        }
        protected void clearControls()
        {
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (hidFormMode.Value != "")
            {
                //Edit
                UpdateUserDetails(hidFormMode.Value.Split(',')[1]);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
            }
            else
            {
                //new user
                SaveUserDetails();
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
            }
        }

        protected void SaveUserDetails()
        {
            string strMessage = ValidateUsersPanel(false);
            if (!String.IsNullOrEmpty(strMessage))
            {
                showAlert(strMessage, "pnlUsersButtons");
                return;
            }
            else
            {
                string strManagerId = "";
                if (ddl_Login.SelectedValue.ToLower() == "salesperson")
                {
                    strManagerId = ddlUserSalesManager.SelectedValue;
                }
                AddUserDetailsAdmin objAddUserDtls = new AddUserDetailsAdmin(txtUserLoginName.Text, txtUserPassword.Text, txtUserEmail.Text, chkUserIsEnabled.Checked ? Convert.ToByte(1) : Convert.ToByte(0)
                                                                  , txtUserContactNo.Text, strManagerId, ddlUsersCountry.SelectedValue, txtUserCity.Text
                                                                  , getCodesForUserType((UserTypes)Enum.Parse(typeof(UserTypes), ddl_Login.SelectedValue))
                                                                  , chkUserServerAllocation.Checked ? Convert.ToByte(1) : Convert.ToByte(0)
                                                                  , txtUserName.Text);
                objAddUserDtls.Process();
                if (objAddUserDtls.StatusCode == 0)
                {
                    Utilities.showMessage(ddl_Login.SelectedValue + " added  successfully.", this.Page, "second script", DIALOG_TYPE.Info);
                    enableDisablePanel(PanelsAvailable.UserDetails);
                    clearFormFields("");

                }
                else if (objAddUserDtls.StatusCode == -1000)
                {
                    Utilities.showMessage(objAddUserDtls.StatusDescription, this.Page, "second script", DIALOG_TYPE.Info);
                }
                else
                {
                    Utilities.showMessage("Internal Error,Please try again", this.Page, "second script", DIALOG_TYPE.Warning);
                }
            }
        }

        public void UpdateUserDetails(string loginId)
        {
            string strMessage = ValidateUsersPanel(true);
            if (!String.IsNullOrEmpty(strMessage))
            {
                showAlert(strMessage,"pnlUsersButtons");
                return;
            }
            else
            {
                string strManagerId = "";
                if (ddl_Login.SelectedValue.ToLower() == "salesperson")
                {
                    strManagerId = ddlUserSalesManager.SelectedValue;
                }
                UpdateUserDetailsAdmin objUpdateUserDtls = new UpdateUserDetailsAdmin();

                objUpdateUserDtls.UpdateUser(txtUserName.Text, txtUserEmail.Text, chkUserIsEnabled.Checked ? Convert.ToByte(1) : Convert.ToByte(0)
                                            , txtUserContactNo.Text, strManagerId, ddlUsersCountry.SelectedValue, txtUserCity.Text,
                                            getCodesForUserType((UserTypes)Enum.Parse(typeof(UserTypes), ddl_Login.SelectedValue)),
                                            chkUserServerAllocation.Checked ? Convert.ToByte(1) : Convert.ToByte(0), loginId);
                if (objUpdateUserDtls.StatusCode == 0)
                {
                    Utilities.showMessage(ddl_Login.SelectedValue + " person details updated successfully", this.Page, "second script", DIALOG_TYPE.Info);

                    //enableDisablePanelsUsers("");
                    enableDisablePanel(PanelsAvailable.UserDetails);
                    clearFormFields("user");
                }
                else if (objUpdateUserDtls.StatusCode == -1000)
                {
                    Utilities.showMessage(objUpdateUserDtls.StatusDescription, this.Page, "second script", DIALOG_TYPE.Info);
                }
                else
                {
                    Utilities.showMessage("Internal Error,Please try again", this.Page, "second script", DIALOG_TYPE.Warning);
                }
            }
        }

        public string ValidateUsersPanel(bool isEdit)
        {
            string strmessage = "";
            if (String.IsNullOrEmpty(txtUserLoginName.Text))
            {
                strmessage = strmessage + "Please enter login name<br />";
            }
            else if (!Utilities.IsValidString(txtUserLoginName.Text, true, true, false, "-'", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct login name<br />";
            }

            if (String.IsNullOrEmpty(txtUserName.Text))
            {
                strmessage = strmessage + "Please enter user name<br />";
            }
            else  if (!Utilities.IsValidString(txtUserName.Text, true, true, false, " ", 4, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct user name<br />";
            }

            if (String.IsNullOrEmpty(txtUserEmail.Text))
            {
                strmessage = strmessage + "Please enter email<br />";
            }

            else  if (!Utilities.IsValidEmail(txtUserEmail.Text))
            {
                strmessage = strmessage + "Please enter correct email<br />";
            }

            if (String.IsNullOrEmpty(txtUserContactNo.Text))
            {
                strmessage = strmessage + "Please enter contact number<br />";
            }
            else if (!Utilities.IsValidString(txtUserContactNo.Text, false, false, true, "", 1, 15, false, false))
            {
                strmessage = strmessage + "Please enter correct contact number<br />";
            }

            if (String.IsNullOrEmpty(txtUserCity.Text))
            {
                strmessage = strmessage + "Please enter city<br />";
            }
            else if (!Utilities.IsValidString(txtUserCity.Text, true, true, false, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct city<br />";
            }
            if (ddlUsersCountry.SelectedValue == "-1")
            {
                strmessage = strmessage + "Please select a country<br />";
            }
            
            if (!isEdit)
            {
                if (String.IsNullOrEmpty(txtUserPassword.Text))
                {
                    strmessage = strmessage + "Please enter password<br />";
                }
                else if (!Utilities.IsValidString(txtUserPassword.Text, true, true, true, "~`!@#$%^&*()-_+={[}]:;\"'<,>.?/\\|", 6, 20, false, false))
                {
                    strmessage = strmessage + "Please enter correct password<br />";
                }

                if (txtUserPassword.Text != txtRepassword.Text)
                {
                    strmessage = strmessage + "Password provided do not match.Please re-enter the password<br />";
                }
            }
            return strmessage;
        }

        public string ValidateReseller(bool isEdit)
        {
            string strmessage = "";
            int intCountSelectedSalesPersons = 0;
            if (String.IsNullOrEmpty(txtloginname.Text))
            {
                strmessage = strmessage + "Please enter login name<br />";
            }
            else if (!Utilities.IsValidString(txtloginname.Text, true, true, false, "-'", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct login name<br />";
            }

            if (String.IsNullOrEmpty(txtname.Text))
            {
                strmessage = strmessage + "Please enter reseller name<br />";
            }
            else if (!Utilities.IsValidString(txtname.Text, true, true, false, " ", 4, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct reseller name<br />";
            }

            if (String.IsNullOrEmpty(txtemail.Text))
            {
                strmessage = strmessage + "Please enter email<br />";
            }

            else if (!Utilities.IsValidEmail(txtemail.Text))
            {
                strmessage = strmessage + "Please enter correct email<br />";
            }

            if (String.IsNullOrEmpty(txtnumber.Text))
            {
                strmessage = strmessage + "Please enter contact number<br />";
            }
            else if (!Utilities.IsValidString(txtnumber.Text, false, false, true, "", 1, 15, false, false))
            {
                strmessage = strmessage + "Please enter correct contact number<br />";
            }
            if (ddlcountry.SelectedValue == "-1")
            {
                strmessage = strmessage + "Please select a country<br/>";
            }

            if (String.IsNullOrEmpty(txtcity.Text))
            {
                strmessage = strmessage + "Please enter city<br />";
            }

            else  if (!Utilities.IsValidString(txtcity.Text, true, true, false, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct city<br />";
            }

            if (String.IsNullOrEmpty(txtdiscount.Text))
            {
                strmessage = strmessage + "Please enter discount<br />";
            }
            else  if (!Utilities.IsValidString(txtdiscount.Text, false, false, true, ".", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct discount<br />";
            }
            if (!isEdit)
            {
                if (String.IsNullOrEmpty(txtpassword.Text))
                {
                    strmessage = strmessage + "Please enter password<br />";
                }
                else if (!Utilities.IsValidString(txtpassword.Text, true, true, true, "~`!@#$%^&*()-_+={[}]:;\"'<,>.?/\\|", 6, 20, false, false))
                {
                    strmessage = strmessage + "Please enter correct password<br />";
                }

                if (txtpassword.Text != txtRpwd.Text)
                {
                    strmessage = strmessage + "Password provided did not match.Please re-enter the password<br />";
                }
            }
           
            foreach (ListItem item in chklstSalesPersons.Items)
            {
                if (item.Selected == true)
                {
                    intCountSelectedSalesPersons = 1;
                    break;
                }
            }
            if (ddlcountry.SelectedValue != "-1")
            {
                if (intCountSelectedSalesPersons == 0)
                {
                    strmessage = strmessage + "You have to select atleast one sales person";
                }
            }
            return strmessage;
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearFormFields("users");
            enableDisablePanel(PanelsAvailable.UserDetails);
        }

        protected void ddl_Users_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_Users.SelectedValue != "-1")
            {
                bindUserDetailsRepeater();
            }
           
        }

        protected void ddl_Login_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddl_Login.SelectedValue !="-1")
            {
                switch (ddl_Login.SelectedValue.ToLower())
                {
                    case "reseller":
                        enableDisablePanel(PanelsAvailable.ResellerForm, false);
                        clearFormFields("reseller");
                        break;
                    case "salesmanager":
                        enableDisablePanel(PanelsAvailable.SalesManager, false);
                        clearFormFields("salesmanager");
                        break;
                    case "salesperson":
                        enableDisablePanel(PanelsAvailable.SalesPersonsForm, false);

                        clearFormFields("salesperson");
                        break;
                    case "support":
                        enableDisablePanel(PanelsAvailable.SupportForm, false);
                        clearFormFields("support");
                        break;
                    case "account":
                        enableDisablePanel(PanelsAvailable.AccountForm, false);
                        clearFormFields("account");
                        break;
                }
            }
        }

        void bindSalesManagerDropDown()
        {
            GetUserDetailsAdmin objUserDtls = new GetUserDetailsAdmin();
            objUserDtls.GetUserDetailsByTypeAndCountry(getCodesForUserType((UserTypes)Enum.Parse(typeof(UserTypes), "SalesManager")), ddlUsersCountry.SelectedValue);
            DataTable dtblUserDtls = objUserDtls.ResultTable;
            if (dtblUserDtls != null && dtblUserDtls.Rows.Count > 0)
            {
                ddlUserSalesManager.DataSource = dtblUserDtls;
                ddlUserSalesManager.DataTextField = "USER_NAME";
                ddlUserSalesManager.DataValueField = "LOGIN_ID";
              //  ddlUserSalesManager.Items.Insert(0, new ListItem("Select", "-1"));
                ddlUserSalesManager.DataBind();
                ddlUserSalesManager.Visible = true;
            }
            else
            {
                Utilities.showMessage("There are no sales manager added yet for the country selected.Please add sales manager first", this.Page, "second script", DIALOG_TYPE.Info);
                ddlUserSalesManager.Items.Clear();
                ddlUserSalesManager.Visible = false;
            }
        }
        protected void rptUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item;
            Label lit_Name = (Label)item.FindControl("lbllogin");
            if (e.CommandName == "Edit")
            {
            }
        }

        protected void FillUserDetails(string loginId)
        {
            GetUserDetailsAdmin objGetUserDtls = new GetUserDetailsAdmin();
            objGetUserDtls.GetUserDetails(loginId);
            DataTable dtblUserDetails = objGetUserDtls.ResultTable;
            if (objGetUserDtls.StatusCode == 0)
            {
                txtUserName.Text = (string)dtblUserDetails.Rows[0]["USER_NAME"];
                txtUserEmail.Text = (string)dtblUserDetails.Rows[0]["EMAIL"];
                txtUserContactNo.Text = (string)dtblUserDetails.Rows[0]["CONTACT_NUMBER"];
                txtUserCity.Text = (string)dtblUserDetails.Rows[0]["CITY"];
                txtUserLoginName.Text = (string)dtblUserDetails.Rows[0]["LOGIN_NAME"];
             //   ddlUsersCountry.SelectedIndex = ddlUsersCountry.Items.IndexOf(ddlUsersCountry.Items.FindByValue(Convert.ToString(dtblUserDetails.Rows[0]["COUNTRY_CODE"])));
                ddlUsersCountry.SelectedValue = Convert.ToString(dtblUserDetails.Rows[0]["COUNTRY_CODE"]);
                if (ddl_Users.SelectedValue.ToLower() == "salesperson")
                {
                   // ddlUsersCountry_SelectedIndexChanged(null, null);
                    ddlUserSalesManager.SelectedIndex = ddlUserSalesManager.Items.IndexOf(ddlUserSalesManager.Items.FindByValue(Convert.ToString(dtblUserDetails.Rows[0]["MANAGER_ID"])));
                }
                if (ddl_Users.SelectedValue.ToLower() == "support")
                {
                    if ((string)dtblUserDetails.Rows[0]["IS_SERVER_ALLOCATION"] == "1")
                    {
                        chkUserServerAllocation.Checked = true;
                    }
                    else
                    {
                        chkUserServerAllocation.Checked = false;
                    }
                }
                if ((bool)dtblUserDetails.Rows[0]["IS_ENABLED"])
                {
                    chkUserIsEnabled.Checked = true;
                }
                
                    if (ddlUsersCountry.SelectedValue == "-1")
                    {
                        checkusers.Visible = true;
                        ddlcheck.Visible = false;
                    }
                    else if (ddlUsersCountry.SelectedValue != "-1")
                    {
                        checkusers.Visible = false;
                        bindSalesManagerDropDown();
                        //ddlUserSalesManager.Items.Clear();
                        ddlcheck.Visible = true;
                    }
            }
            else
            {
                Utilities.showMessage("Record Not Found", this.Page, "second script", DIALOG_TYPE.Warning);
            }
        }

        protected void FillReseller(string resellerId)
        {
            GetMBuzzServerDetailsAdmin serverdetails = new GetMBuzzServerDetailsAdmin();
            serverdetails.MBuzzServerDetails(resellerId);
            DataTable dt = serverdetails.ResultTable;
            if (dt.Rows.Count > 0)
            {
                hidDetailsFormMode.Value = resellerId;
                txtname.Text = Convert.ToString(dt.Rows[0]["RESELLER_NAME"]);
                txtemail.Text = Convert.ToString(dt.Rows[0]["EMAIL"]);
                txtnumber.Text = Convert.ToString(dt.Rows[0]["CONTACT_NUMBER"]);
                chkIsActive.Checked = Convert.ToBoolean(dt.Rows[0]["IS_ENABLE"]);
                txtdiscount.Text = Convert.ToString(dt.Rows[0]["DISCOUNT"]);
                txtloginname.Text = Convert.ToString(dt.Rows[0]["LOGIN_NAME"]);
                txtpassword.Text = Convert.ToString(dt.Rows[0]["PASSWORD"]);
                txtcity.Text = Convert.ToString(dt.Rows[0]["CITY"]);
                ddlcountry.SelectedValue = Convert.ToString(dt.Rows[0]["COUNTRY"]);
                chkIsApproved.Checked = Convert.ToBoolean(dt.Rows[0]["IS_PAYMENT_APPROVAL"]);
            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }


        protected void FillResellerDetails(string loginName, string resellerId, string countryCode)
        {
            GetResellerDetailsAdmin objResellerDetails = new GetResellerDetailsAdmin(resellerId, loginName);
            objResellerDetails.Process();
            if (objResellerDetails.StatusCode == 0)
            {
                DataSet dsResellerDetails = objResellerDetails.ResultTables;
                if (dsResellerDetails.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = dsResellerDetails.Tables[0];
                    txtname.Text = Convert.ToString(dt.Rows[0]["RESELLER_NAME"]);
                    txtemail.Text = Convert.ToString(dt.Rows[0]["EMAIL"]);
                    txtnumber.Text = Convert.ToString(dt.Rows[0]["CONTACT_NUMBER"]);
                    txtdiscount.Text = Convert.ToString(dt.Rows[0]["DISCOUNT"]);
                    
                    txtloginname.Text = Convert.ToString(dt.Rows[0]["LOGIN_NAME"]);
                    txtpassword.Text = Convert.ToString(dt.Rows[0]["PASSWORD"]);
                    txtcity.Text = Convert.ToString(dt.Rows[0]["CITY"]);
                    ddlcountry.SelectedIndex = ddlcountry.Items.IndexOf(ddlcountry.Items.FindByValue(Convert.ToString(dt.Rows[0]["COUNTRY"])));
                    //if (ddlcountry.SelectedValue != "-1")
                    //{
                    //    check.Visible = false;
                    //}
                    //else if (ddlcountry.SelectedValue == "-1")
                    //{
                    //    check.Visible = true;
                    //}

                    //bindSalesPersonsCheckListForReseller();

                    if (ddlcountry.SelectedValue == "-1")
                    {
                        check.Visible = true;
                        chcklist.Visible = false;
                    }
                    else if (ddlcountry.SelectedValue != "-1")
                    {
                        check.Visible = false;
                        bindSalesPersonsCheckListForReseller();
                        chcklist.Visible = true;
                    }
                }
            }
            else
            {
                Utilities.showMessage("Internal Error,Please try again", this.Page, "second script", DIALOG_TYPE.Warning);
            }
        }


        protected void rptUserDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lnkEdit = (LinkButton)e.Item.FindControl("lnkEdit");
                lnkEdit.CommandArgument = (string)DataBinder.Eval(e.Item.DataItem, "Type");
            }
        }

        protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            divddl.Visible = false;
            switch (e.CommandName)
            {
                case "Edit":
                    processEdit(e);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing2", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                    break;

            }
        }
        void processEdit(RepeaterCommandEventArgs e)
        {
            hidFormMode.Value = "Edit" + "," + ((Label)e.Item.FindControl("lblId")).Text + "," + ((Label)e.Item.FindControl("lblusername")).Text;
            switch (((string)e.CommandArgument))
            {
                case "Reseller":
                    Resellerdet.InnerText = "Edit Reseller Details";
                    ddl_Users.SelectedIndex = ddl_Users.Items.IndexOf(ddl_Users.Items.FindByValue("Reseller"));
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("Reseller"));
                    FillResellerDetails(((Label)e.Item.FindControl("lblusername")).Text, ((Label)e.Item.FindControl("lblId")).Text, ((Label)e.Item.FindControl("lblcountry")).Text);
                    enableDisablePanel(PanelsAvailable.ResellerForm, true);
                    break;
                case "Sales Executive":
                    Usersdet.InnerText = "Edit Sales Executive details";
                    //hidFormMode.Value = "Edit" + "," + ((Label)e.Item.FindControl("lblId")).Text + "," + ((Label)e.Item.FindControl("lbllogin")).Text;
                    //FillResellerDetails(((Label)e.Item.FindControl("lbllogin")).Text, ((Label)e.Item.FindControl("lblId")).Text, ((Label)e.Item.FindControl("lblcountry")).Text);
                    ddl_Users.SelectedIndex = ddl_Users.Items.IndexOf(ddl_Users.Items.FindByValue("SalesPerson"));
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("SalesPerson"));
                    FillUserDetails( ((Label)e.Item.FindControl("lblId")).Text);
                    enableDisablePanel(PanelsAvailable.SalesPersonsForm, true);
                    break;
                case "Sales Manager":
                    Usersdet.InnerText = "Edit Sales Manager details";
                    //hidFormMode.Value = "Edit" + "," + ((Label)e.Item.FindControl("lblId")).Text + "," + ((Label)e.Item.FindControl("lbllogin")).Text;
                    ddl_Users.SelectedIndex = ddl_Users.Items.IndexOf(ddl_Users.Items.FindByValue("SalesManager"));
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("SalesManager"));
                    FillUserDetails( ((Label)e.Item.FindControl("lblId")).Text);
                    enableDisablePanel(PanelsAvailable.SalesManager, true);
                    break;
                case "Support":
                    Usersdet.InnerText = "Edit Support person details";
                    //hidFormMode.Value = "Edit" + "," + ((Label)e.Item.FindControl("lblId")).Text + "," + ((Label)e.Item.FindControl("lbllogin")).Text;
                    ddl_Users.SelectedIndex = ddl_Users.Items.IndexOf(ddl_Users.Items.FindByValue("Support"));
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("Support"));
                    FillUserDetails( ((Label)e.Item.FindControl("lblId")).Text);
                    enableDisablePanel(PanelsAvailable.SupportForm, true);
                    break;
                case "Accounts":
                    Usersdet.InnerText = "Edit Account person details";
                    //hidFormMode.Value = "Edit" + "," + ((Label)e.Item.FindControl("lblId")).Text + "," + ((Label)e.Item.FindControl("lbllogin")).Text;
                    ddl_Users.SelectedIndex = ddl_Users.Items.IndexOf(ddl_Users.Items.FindByValue("Account"));
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("Account"));
                    FillUserDetails(((Label)e.Item.FindControl("lblId")).Text);
                    enableDisablePanel(PanelsAvailable.AccountForm, true);
                    break;
            }
        }

        protected void btnSaveReseller_Click(object sender, EventArgs e)
        {
            //using hidFormMode to see whether the form shown is in edit mode or for adding new
            //clear hidFormMode after edit process is over.
            //its value is set in the repeater command
            if (hidFormMode.Value != "")
            {
                //Edit
                UpdateResellerDetails();
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
            }
            else
            {
                //new user
                SaveResellerDetails();
                //Utilities.Insertgdserver(g);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
            }

        }
        public void UpdateResellerDetails()
        {
            //string strMessage = ValidatePanel();
            string strMessage = ValidateReseller(true);
            if (!String.IsNullOrEmpty(strMessage))
            {
                showAlert(strMessage, "resellerButtons");
                return;
            }
            else
            {
                List<string> lstResellersSalesPersons = new List<string>();
                foreach (ListItem item in chklstSalesPersons.Items)
                {
                    if (item.Selected)
                    {
                        lstResellersSalesPersons.Add(item.Value);
                    }
                }

                UpdateResellerDetailAdmin updatedetail = new UpdateResellerDetailAdmin();
                //updatedetail.UpdateReseller(txtname.Text, txtemail.Text, txtnumber.Text, txtdiscount.Text, Convert.ToDateTime(txtdate.Text), txtresellerid.Text, txtlogin.Text, txtpassword.Text);
                updatedetail.UpdateReseller(txtname.Text, txtemail.Text, txtnumber.Text, txtdiscount.Text, hidFormMode.Value.Split(',')[1], chkIsActive.Checked ? Convert.ToByte(1) : Convert.ToByte(0), chkIsApproved.Checked ? Convert.ToByte(1) : Convert.ToByte(0), lstResellersSalesPersons.ToArray(), txtcity.Text, ddlcountry.SelectedValue);
                int intStatusCode = updatedetail.StatusCode;
                string strDescription = updatedetail.StatusDescription;

                if (intStatusCode == 0)
                {
                    Utilities.showMessage("Reseller Details have been updated successfully", this.Page, "second script", DIALOG_TYPE.Info);
                    hidFormMode.Value = "";
                    enableDisablePanel(PanelsAvailable.UserDetails);
                    clearFormFields("reseller");
                }
                else
                {
                    Utilities.showMessage("Internal Error,Please try again", this.Page, "second script", DIALOG_TYPE.Warning);
                }
            }
        }
        protected void SaveResellerDetails()
        {
            string strMessage = ValidateReseller(false);
            if (!String.IsNullOrEmpty(strMessage))
            {
                showAlert(strMessage, "resellerButtons");
                return;
            }
            else
            {
                List<string> lstResellersSalesPersons = new List<string>();
                foreach (ListItem item in chklstSalesPersons.Items)
                {
                    if (item.Selected)
                    {
                        lstResellersSalesPersons.Add(item.Value);
                    }
                }
                AddresellerdetailsAdmin add = new AddresellerdetailsAdmin(txtname.Text, 
                    txtemail.Text, txtnumber.Text, txtdiscount.Text, 
                    DateTime.UtcNow, txtloginname.Text, txtpassword.Text, 
                    txtcity.Text, ddlcountry.SelectedValue,
                    chkIsApproved.Checked ? Convert.ToByte(1) : Convert.ToByte(0),
                    chkIsActive.Checked ? Convert.ToByte(1) : Convert.ToByte(0),
                    lstResellersSalesPersons.ToArray(),
                    false);
                //add.AddReseller(txtname.Text, txtemail.Text, txtnumber.Text, txtdiscount.Text, Convert.ToDateTime(txtdate.Text), txtlogin.Text, txtpassword.Text);
                add.Process();
                if (add.StatusCode == 0)
                {
                    Utilities.showMessage("Reseller Details have been Added successfully", this.Page, "second script", DIALOG_TYPE.Info);
                    //enableDisablePanelsLogin("");
                    //hidPasswordEntered.Value = "";
                    //not required changed on 16-7-2012 Mohan
                    //ddl_Users.SelectedIndex = ddl_Users.Items.IndexOf(ddl_Users.Items.FindByValue("reseller"));
                    bindUserDetailsRepeater();
                    enableDisablePanel(PanelsAvailable.UserDetails);
                    //clearFormFields("");
                    clearFormFields("reseller");
                }
                else if (add.StatusCode == -1000)
                {
                    Utilities.showMessage(add.StatusDescription, this.Page, "second script", DIALOG_TYPE.Info);
                }
                else
                {
                    Utilities.showMessage("Internal Error,Please try again", this.Page, "second script", DIALOG_TYPE.Warning);
                }
            }
        }
        protected void btnCancelReseller_Click(object sender, EventArgs e)
        {
            //clearControls();
            //enableDisablePanelsLogin("");
            enableDisablePanel(PanelsAvailable.UserDetails);
            clearFormFields("reseller");
        }

        protected void lnkAddNewMobilelOGIN_Click(object sender, EventArgs e)
        {
            clearControls();
            enableDisablePanelsLogin("Add");
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }

        void bindUserDetailsRepeater()
        {
            //copied.earlier it was in the page load function
           
            ManageLoginUsersAdmin users = new ManageLoginUsersAdmin();
            if (ddl_Users.SelectedValue.ToLower() == "all")
            {
                DataTable dt = users.SelectAll();
                if (dt.Rows.Count > 0)
                {
                    rptUserDetails.DataSource = dt;
                    rptUserDetails.DataBind();
                    rptUserDetails.Visible = true;
                    fillRptHeaderAndShowHideAdd("Users Details", false);
                }
                else
                {
                    fillRptHeaderAndShowHideAdd("No users are added yet", false);
                    rptUserDetails.Visible = false;
                }
            }

            else if (ddl_Users.SelectedValue.ToLower() == "reseller")
            {
                DataTable dt = users.SelectReseller();
                if (dt.Rows.Count > 0)
                {
                    rptUserDetails.DataSource = dt;
                    rptUserDetails.DataBind();
                    rptUserDetails.Visible = true;
                    fillRptHeaderAndShowHideAdd("Reseller Details", true);
                }
                else
                {
                    fillRptHeaderAndShowHideAdd("No resellers has been added yet", true);
                    rptUserDetails.Visible = false;
                }
            }
            else if (ddl_Users.SelectedValue.ToLower() == "Select")
            {
                Utilities.showMessage("Please select the user", this.Page, "second script", DIALOG_TYPE.Warning);
            }
            else
            {
                DataTable dt = users.SelectLoginUserBytype(getCodesForUserType((UserTypes)Enum.Parse(typeof(UserTypes), ddl_Users.SelectedValue)));
                string strUIName = "";
                switch (ddl_Users.SelectedValue.ToLower())
                {
                    case "salesmanager":
                        strUIName = "Sales Manager";
                        break;
                    case "salesperson":
                        strUIName = "Sales Persons";
                        break;
                    case "account":
                        strUIName = "Account Persons";
                        break;
                    case "support":
                        strUIName = "Support Persons";
                        break;
                }
                if (dt.Rows.Count > 0)
                {
                    rptUserDetails.DataSource = dt;
                    rptUserDetails.DataBind();
                    rptUserDetails.Visible = true;
                    //fillRptHeaderAndShowHideAdd(ddl_Users.SelectedValue + " Details", true);
                    fillRptHeaderAndShowHideAdd(strUIName + " Details", true);
                }
                else
                {
                    //fillRptHeaderAndShowHideAdd("No " + ddl_Users.SelectedValue + " persons has been added yet", true);
                    fillRptHeaderAndShowHideAdd("No" + strUIName + " has been added yet", true);
                    rptUserDetails.Visible = false;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="panelName">Panel to show
        /// </param>
        void enableDisablePanel(PanelsAvailable panelName)
        {
            //all the forms are inside the allforms panel.so every time we have to 
            //show a form pane then remember to make allforms panel visible as well
            switch (panelName)
            {
                case PanelsAvailable.UserDetails:
                    pnlUserDetails.Visible = true;
                    pnlLoginTypeAndForm.Visible = false;
                    bindUserDetailsRepeater();
                    break;
                case PanelsAvailable.ResellerForm:
                    pnlUserDetails.Visible = false;
                    pnlLoginTypeAndForm.Visible = true;
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("Reseller"));
                    //ddl_Login_SelectedIndexChanged(null, null);
                    pnlOtherUsers.Visible = false;
                    pnlreseller.Visible = true;
                    pnlSalesMngrDropDown.Visible = false;
                    pnlServerAllocationCheckBox.Visible = false;
                    break;
                case PanelsAvailable.SalesPersonsForm:
                    pnlUserDetails.Visible = false;
                    pnlLoginTypeAndForm.Visible = true;
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("SalesPerson"));
                    //ddl_Login_SelectedIndexChanged(null, null);
                    pnlOtherUsers.Visible = true;
                    pnlreseller.Visible = false;
                    pnlSalesMngrDropDown.Visible = true;
                    pnlServerAllocationCheckBox.Visible = false;
                    break;
                case PanelsAvailable.SalesManager:
                    pnlUserDetails.Visible = false;
                    pnlLoginTypeAndForm.Visible = true;
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("SalesManager"));
                    //ddl_Login_SelectedIndexChanged(null, null);
                    pnlOtherUsers.Visible = true;
                    pnlreseller.Visible = false;
                    pnlSalesMngrDropDown.Visible = false;
                    pnlServerAllocationCheckBox.Visible = false;
                    break;
                case PanelsAvailable.SupportForm:
                    pnlUserDetails.Visible = false;
                    pnlLoginTypeAndForm.Visible = true;
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("Support"));
                    //ddl_Login_SelectedIndexChanged(null, null);
                    pnlOtherUsers.Visible = true;
                    pnlreseller.Visible = false;
                    pnlSalesMngrDropDown.Visible = false;
                    pnlServerAllocationCheckBox.Visible = true;
                    break;
                case PanelsAvailable.AccountForm:
                    pnlUserDetails.Visible = false;
                    pnlLoginTypeAndForm.Visible = true;
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("Account"));
                    //ddl_Login_SelectedIndexChanged(null, null);
                    pnlOtherUsers.Visible = true;
                    pnlreseller.Visible = false;
                    pnlSalesMngrDropDown.Visible = false;
                    pnlServerAllocationCheckBox.Visible = false;
                    break;
            }
        }

        void enableDisablePanel(PanelsAvailable panelName, bool isEdit)
        {
            //all the forms are inside the allforms panel.so every time we have to 
            //show a form pane then remember to make allforms panel visible as well
            switch (panelName)
            {
                case PanelsAvailable.UserDetails:
                    pnlUserDetails.Visible = true;
                    pnlLoginTypeAndForm.Visible = false;
                    bindUserDetailsRepeater();
                    break;
                case PanelsAvailable.ResellerForm:
                    pnlUserDetails.Visible = false;
                    pnlLoginTypeAndForm.Visible = true;
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("Reseller"));
                    //ddl_Login_SelectedIndexChanged(null, null);
                    pnlOtherUsers.Visible = false;
                    pnlreseller.Visible = true;
                    pnlSalesMngrDropDown.Visible = false;
                    pnlServerAllocationCheckBox.Visible = false;
                    enableDisableLoginNameAndPasswordControl(isEdit, PanelsAvailable.ResellerForm);
                    break;
                case PanelsAvailable.SalesPersonsForm:
                    pnlUserDetails.Visible = false;
                    pnlLoginTypeAndForm.Visible = true;
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("SalesPerson"));
                    //ddl_Login_SelectedIndexChanged(null, null);
                    pnlOtherUsers.Visible = true;
                    pnlreseller.Visible = false;
                    pnlSalesMngrDropDown.Visible = true;
                    pnlServerAllocationCheckBox.Visible = false;
                    enableDisableLoginNameAndPasswordControl(isEdit, PanelsAvailable.SalesPersonsForm);
                    break;
                case PanelsAvailable.SalesManager:
                    pnlUserDetails.Visible = false;
                    pnlLoginTypeAndForm.Visible = true;
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("SalesManager"));
                    //ddl_Login_SelectedIndexChanged(null, null);
                    pnlOtherUsers.Visible = true;
                    pnlreseller.Visible = false;
                    pnlSalesMngrDropDown.Visible = false;
                    pnlServerAllocationCheckBox.Visible = false;
                    enableDisableLoginNameAndPasswordControl(isEdit, PanelsAvailable.SalesManager);
                    break;
                case PanelsAvailable.SupportForm:
                    pnlUserDetails.Visible = false;
                    pnlLoginTypeAndForm.Visible = true;
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("Support"));
                    //ddl_Login_SelectedIndexChanged(null, null);
                    pnlOtherUsers.Visible = true;
                    pnlreseller.Visible = false;
                    pnlSalesMngrDropDown.Visible = false;
                    pnlServerAllocationCheckBox.Visible = true;
                    enableDisableLoginNameAndPasswordControl(isEdit, PanelsAvailable.SupportForm);
                    break;
                case PanelsAvailable.AccountForm:
                    pnlUserDetails.Visible = false;
                    pnlLoginTypeAndForm.Visible = true;
                    ddl_Login.SelectedIndex = ddl_Login.Items.IndexOf(ddl_Login.Items.FindByValue("Account"));
                    //ddl_Login_SelectedIndexChanged(null, null);
                    pnlOtherUsers.Visible = true;
                    pnlreseller.Visible = false;
                    pnlSalesMngrDropDown.Visible = false;
                    pnlServerAllocationCheckBox.Visible = false;
                    enableDisableLoginNameAndPasswordControl(isEdit, PanelsAvailable.AccountForm);
                    break;
            }
        }
        void enableDisableLoginNameAndPasswordControl(bool isEdit, PanelsAvailable pnlInEditMode)
        {
            switch (pnlInEditMode)
            {
                case PanelsAvailable.ResellerForm:
                    if (isEdit)
                    {
                        txtloginname.Enabled = false;
                        pnlResellerPassword.Visible = false;
                    }
                    else
                    {
                        txtloginname.Enabled = true;
                        pnlResellerPassword.Visible = true;
                    }
                    break;
                case PanelsAvailable.SalesManager:
                case PanelsAvailable.SalesPersonsForm:
                case PanelsAvailable.SupportForm:
                case PanelsAvailable.AccountForm:

                    if (isEdit)
                    {
                        txtUserLoginName.Enabled = false;
                        pnlUserPassword.Visible = false;
                    }
                    else
                    {
                        txtUserLoginName.Enabled = true;
                        pnlUserPassword.Visible = true;
                    }
                    break;
            }
        }
        protected void ddlcountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (hidDetailsFormMode.Value == "")
            {
                if (ddlcountry.SelectedValue == "-1")
                {
                    check.Visible = true;
                   chcklist.Visible = false;
                }
                else if (ddlcountry.SelectedValue != "-1")
                {
                    check.Visible = false;
                    bindSalesPersonsCheckListForReseller();
                    chcklist.Visible = true;
                }
            }
          //  Check.Visible = true;
            else if (hidFormMode.Value != "")
            {
                GetResellerDetailsAdmin objGetResellerDtls = new GetResellerDetailsAdmin(hidFormMode.Value.Split(',')[1], hidFormMode.Value.Split(',')[2]);
                objGetResellerDtls.Process();
                if (objGetResellerDtls.StatusCode == 0)
                {
                    DataSet dsResellerDetails = objGetResellerDtls.ResultTables;
                    if (dsResellerDetails.Tables[0].Rows.Count > 0)
                    {
                        if (ddlcountry.SelectedValue == (string)dsResellerDetails.Tables[0].Rows[0]["COUNTRY"])
                        {
                            if (dsResellerDetails.Tables[1].Rows.Count > 0)
                            {
                                selectTheSalesPersonsForReseller(dsResellerDetails.Tables[1]);
                            }
                        }
                    }
                }
            }
        }
        void bindSalesPersonsCheckListForReseller()
        {
            GetSalesPersonsByCountry objGetSalesPersons = new GetSalesPersonsByCountry(Convert.ToInt32(ddlcountry.SelectedValue));
            objGetSalesPersons.Process();
            if (objGetSalesPersons.ResultTable != null && objGetSalesPersons.ResultTable.Rows.Count > 0)
            {
                chklstSalesPersons.DataSource = objGetSalesPersons.ResultTable;
                chklstSalesPersons.DataTextField = "USER_NAME";
                chklstSalesPersons.DataValueField = "LOGIN_ID";
                chklstSalesPersons.DataBind();
            }
            else
            {
                Utilities.showMessage("There are no sales person's corresponding to this country",this.Page,DIALOG_TYPE.Warning);
                DataTable dtbl = new DataTable();
                chklstSalesPersons.DataSource = dtbl;
                chklstSalesPersons.DataBind();
            }
        }
    
        /// <summary>
        /// Selecting the checkboxes for the sales persons saved for the reseller
        /// </summary>
        /// <param name="resellerSalesPersons"></param>
        void selectTheSalesPersonsForReseller(DataTable resellerSalesPersons)
        {
            foreach (ListItem item in chklstSalesPersons.Items)
            {
                string strFilter = String.Format("LOGIN_ID = '{0}'", item.Value);
                DataRow[] rows = resellerSalesPersons.Select(strFilter);
                if (rows.Length > 0)
                {
                    item.Selected = true;
                }
                else
                {
                    item.Selected = false;
                }
            }
        }

        string getCodesForUserType(UserTypes usrType)
        {
            string strCode = "";
            switch (usrType)
            {
                case UserTypes.Account:
                    strCode = "ACC";
                    break;
                case UserTypes.Reseller:
                    strCode = "RS";
                    break;
                case UserTypes.SalesManager:
                    strCode = "SM";
                    break;
                case UserTypes.SalesPerson:
                    strCode = "S";
                    break;
                case UserTypes.Support:
                    strCode = "SUPP";
                    break;
            }
            return strCode;
        }
       
       
        void showAlert(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#buttons'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        void showAlert(string message, string divIdToShowMsg)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowMsg + "'" + ");$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
        }
        void fillRptHeaderAndShowHideAdd(string headerMessage, bool showAddButton)
        {
            lblHeaderInfo.Text = "<h1>" + headerMessage + "</h1>";

            if (showAddButton)
            {
                lnkAddNewUser.Visible = true;
            }
            else
            {
                lnkAddNewUser.Visible = false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">pass reseller or user as value</param>
        void clearFormFields(string type)
        {
            if (type.ToLower() == "reseller")
            {
                txtname.Text = "";
                txtemail.Text = "";
                txtnumber.Text = "";
                ddlcountry.SelectedIndex = -1;
                foreach (ListItem item in chklstSalesPersons.Items)
                {
                    item.Selected = false;
                }
                DataTable dtbl = new DataTable();
                chklstSalesPersons.DataSource = dtbl;
                chklstSalesPersons.DataBind();
                //ddlcountry_SelectedIndexChanged(null, null);
                txtcity.Text = "";
                txtdiscount.Text = "";
                txtloginname.Text = "";
                txtpassword.Text = "";
                hidFormMode.Value = "";
            }
            else
            {
                txtUserName.Text = "";
                txtUserEmail.Text = "";
                txtUserContactNo.Text = "";
                txtUserCity.Text = "";
                ddlUsersCountry.SelectedIndex = -1;
                chkUserIsEnabled.Checked = false;
                chkUserServerAllocation.Checked = false;
                ddlUserSalesManager.SelectedIndex = -1;
                ddlUserSalesManager.Items.Clear();
                ddlUserSalesManager.Visible = false;
                txtUserLoginName.Text = "";
                txtUserPassword.Text = "";
                hidFormMode.Value = "";
            }
        }

        protected void ddlUsersCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_Users.SelectedValue.ToLower() == "salesperson")
            {
                if (ddlUsersCountry.SelectedValue == "-1")
                {
                    checkusers.Visible = true;
                    ddlcheck.Visible = false;
                }
                else if (ddlUsersCountry.SelectedValue != "-1")
                {
                    checkusers.Visible = false;
                    bindSalesManagerDropDown();
                    ddlcheck.Visible = true;
                }
            }
        }

        protected void ddlUserSalesManager_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

    }

}
﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="AdminDashboard.aspx.cs" Inherits="mFicientAdmin.dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                      <div>
                                            <asp:Label ID="lblInfo" runat="server" Text="<h1>ENQUIRY DETAILS</h1>"></asp:Label></div>
                                         <div style="float: left; margin-left: 25px;">
                                            <asp:LinkButton ID="lnkdetails" runat="server" style="position: relative;top: 16px; font-size:11px;" Text="( details )" PostBackUrl="~/ManageEnquiryDetails.aspx"></asp:LinkButton>
                                        </div>
                                        
                                    </asp:Panel>
                                </section>
                         
                                <asp:Repeater ID="rptUserDetails" runat="server" >
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                <th >
                                              RESELLER STATUS
                                                </th>
                                                 <th >

                                                   COMPANY
                                                    </th >
                                                   
                                                   <th >
                                                   COUNTRY
                                                   </th>
                                                   <th>
                                                   EXPECTED APPS
                                                   </th>
                                                    <%--<th>
                                                    </th>
--%>                                               

                                     </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                            <td>
                                            <asp:Label ID="lblreseller" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
 
                                            </td>
                                                <td>
                                                    <asp:Label ID="lblid" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("COUNTRY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbltype" runat="server" Text='<%# Eval("EXPECTED_WF") %>'></asp:Label>
                                                </td>
                                               
                                                
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                             <td>
                                         <asp:Label ID="lblreseller" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
 
                                            </td>
                                                <td>
                                                    <asp:Label ID="lblid" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblname" runat="server" Text='<%#Eval("COUNTRY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbltype" runat="server" Text='<%# Eval("EXPECTED_WF") %>'></asp:Label>
                                                </td>
                                               
                                               
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                    <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                                </div>
                            </asp:Panel>
                        </section>
                        
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
    </div>
    <div id="Div1">
        <section>
            <div id="div2">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                            <section>
                            <div id="divInformation1"></div>
                            
                            </section>

                        <section>
                            <asp:Panel ID="pnlBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlBoxHeader" CssClass="repeaterBox-header" runat="server">
                                      <div>
                                        
                                        <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>ORDER DETAILS</h1>"></asp:Label>
                                         <div style="float: left; margin-left: 25px;">
                                            <asp:LinkButton ID="lnkorderdet" runat="server" style="position: relative;top: 16px; font-size:11px;" Text="(Show Details)" PostBackUrl="~/PlanOrders.aspx"></asp:LinkButton>
                                        </div>
                                        </div>
                                    </asp:Panel>
                                </section>
                         
                                <asp:Repeater ID="rptUser" runat="server" OnItemCommand="rptUser_ItemCommand" >
                                    <HeaderTemplate> 
                                        <table class="repeaterTable">
                                           <thead>
                                                <tr>
                                                <th style="display:none;">
                                                COMPANY ID
                                                </th>
                                                      <th>
                                                        COMPANY
                                                    </th>
                                                    <th>
                                                   CURRENT PLAN
                                                    </th>
                                                    <th>
                                                     ORDER
                                                    </th>

                                                   
                                                </tr>
                                            </thead>
                                            
                                    </HeaderTemplate>
                                   <ItemTemplate>
                                         <tbody>
                                            <tr class="repeaterItem">
                                               <td style="display:none;">
                                                <asp:Label ID="lid" runat="server" Text='<%#Eval("COMPANY_ID") %>'></asp:Label>
                                               </td>
                                                <td>
                                                    <asp:Label ID="lname" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lplan" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                                </td>
                                               
                                                 <td>
                                                    <asp:Label ID="lorder" runat="server" Text='<%# Eval("ORDER") %>'></asp:Label>
                                                </td>
                                               
                                                <td style="display:none;">
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Process"   CssClass="repeaterLink" CommandName="ProcessRequest"></asp:LinkButton>
                                                    
                                                </td>
                                            </tr>
                                        </tbody>
                                        </ItemTemplate>
                                    <AlternatingItemTemplate>
                                       <tbody>
                                             <tr class="repeaterItem">
                                               <td style="display:none;">
                                                <asp:Label ID="lid" runat="server" Text='<%#Eval("COMPANY_ID") %>'></asp:Label>
                                               </td>
                                                <td>
                                                    <asp:Label ID="lname" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lplan" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lorder" runat="server" Text='<%# Eval("ORDER") %>'></asp:Label>
                                                </td>
                                                                                               
                                                <td style="display:none;">
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="Process"  CssClass="repeaterLink" CommandName="ProcessRequest" ></asp:LinkButton>
                                                    
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="HiddenField2" runat="server" />
                                    <asp:HiddenField ID="HiddenField3" runat="server" />
                                </div>
                           </asp:Panel>
                        </section>
                        
                        <div>
                            <asp:HiddenField ID="HiddenField7" runat="server" />
                            <asp:HiddenField ID="HiddenField8" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
        <div>
            <asp:HiddenField ID="HiddenField9" runat="server" />
        </div>
        <div style="clear: both">
            <asp:HiddenField ID="HidDetailsForm" runat="server" />
            <asp:HiddenField ID="Hiddetails" runat="server" />
        </div>
        <div class="modalPopUpDetails">
            <div id="divUserDtlsModal" style="display: none">
                <asp:UpdatePanel runat="server" ID="upUserDtlsModal" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="ProcImgUpd">
                            <div id="DivUserDetails" style="width: 85%;">
                                <asp:Panel ID="pnlMobileUserForm" Visible="false" runat="server">
                                    <fieldset>
                                        <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-right: 32px;">
                                                <asp:Label ID="label1" runat="server" Text="<strong>Name </strong>  "></asp:Label></div>
                                            <div style="float: left;">
                                                <asp:Label ID="lname" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-right: 40px;">
                                                <asp:Label ID="label2" runat="server" Text="<strong>Plan</strong>"></asp:Label></div>
                                            <div style="float: left;">
                                                <asp:Label ID="lplan" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-right: 35px;">
                                                <asp:Label ID="label3" runat="server" Text="<strong>Users</strong>"></asp:Label></div>
                                            <div style="float: left;">
                                                <asp:Label ID="lusers" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-right: 15px;">
                                                <asp:Label ID="label4" runat="server" Text="<strong>Workflow</strong>"></asp:Label></div>
                                            <div style="float: left">
                                                <asp:Label ID="lwrflow" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-right: 37px;">
                                                <asp:Label ID="label5" runat="server" Text="<strong>Date</strong> "></asp:Label></div>
                                            <div style="float: left;">
                                                <asp:Label ID="ldate" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-right: 2px;">
                                                <asp:Label ID="label6" runat="server" Text="<strong>Order Name</strong> "></asp:Label></div>
                                            <div style="float: left">
                                                <asp:Label ID="loname" runat="server"></asp:Label></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-right: 5px;">
                                                <asp:Label ID="label7" runat="server" Text="<strong>Order Code</strong> "></asp:Label></div>
                                            <div style="float: left;">
                                                <asp:Label ID="lcode" runat="server"></asp:Label></div>
                                        </div>
                                    </fieldset>
                                </asp:Panel>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="divPaymentModalContainer" style="display: none">
            <div id="divPaymentContainer" style="display: block">
                <asp:UpdatePanel ID="updPaymentModalContainer" runat="server">
                    <ContentTemplate>
                        <div id="PaymentPopUpContent">
                            <fieldset>
                                <section>
                                      <asp:CheckBox ID="chkPaymentReceived" runat="server" Text="<strong>Payment Received</strong>" Checked="true"/> 
                                
                                </section>
                                <section>
                                    <label for="input" style="margin-top:5px;">
                                     Please enter the amount</label>
                                    <div>
                                        <asp:TextBox ID="txtAmountReceived" runat="server"></asp:TextBox>
                                    </div>
                                </section>
                                <div style="height: 10px;">
                                </div>
                                <section>
                                    <div>
                                        <asp:Button ID="btnPaymentSave" runat="server" Text="save" CssClass="aspButton" OnClick="btnPaymentSave_Click"  />&nbsp;
                                        <asp:Button ID="btnPaymentCancel" runat="server" Text="cancel" CssClass="aspButton" OnClick="btnPaymentCancel_Click" />
                                    </div>
                                </section>
                            </fieldset>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            $("input").uniform();
            hideWaitModal();
        }
    </script>
    <%--<asp:HiddenField ID="hfs" runat="server" />--%>
</asp:Content>

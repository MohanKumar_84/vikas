﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="SalesCompanyApproval.aspx.cs" Inherits="mFicientAdmin.SalesCompanyApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <style type="text/css">
        div.radioButtonList table
        {
            margin: 1px;
            width: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="divInfomationForApproveDisable">
    </div>
    <div class="clear">
    </div>
    <section>
        <div id="divRepeater" style="margin: 5px;">
            <asp:UpdatePanel ID="upd" runat="server">
                <ContentTemplate>
                    <section>
                        <div class="searchRow g12" style="border: 1px solid #FAFAFA">
                            <div class="g5 radioButtonList">
                                <asp:RadioButtonList ID="radListSelection" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                    RepeatLayout="Table" OnSelectedIndexChanged="radListSelection_SelectedIndexChanged1">
                                    <asp:ListItem Text="<strong>Pending</strong>" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="<strong>Approved</strong>" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </section>
                    <section class="clear" style="margin-bottom: 5px;">
                        <div id="div1">
                        </div>
                    </section>
                    <section>
                        <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                            <section>
                                <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                    <div>
                                        <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>COMPANY DETAILS</h1>"></asp:Label>
                                    </div>
                                </asp:Panel>
                            </section>
                            <asp:Repeater ID="rptUserDetails" runat="server" OnItemCommand="rptUserDetails_ItemCommand"
                                OnItemDataBound="rptUserDetails_ItemDataBound">
                                <HeaderTemplate>
                                    <table class="repeaterTable">
                                        <thead>
                                            <tr>
                                                <th>
                                                    COMPANY ID
                                                </th>
                                                <th>
                                                    COMPANY
                                                </th>
                                                <th>
                                                    PLAN CODE
                                                </th>
                                                <th>
                                                    PLAN NAME
                                                </th>
                                                <th>
                                                </th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tbody>
                                        <tr class="repeaterItem">
                                            <td>
                                                <asp:Label ID="lblid" runat="server" Text='<%#Eval("COMPANY_ID") %>'></asp:Label>
                                                <asp:Label ID="lblIsTrial" runat="server" Text='<%#Eval("IS_TRIAL") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblTransactionType" runat="server" Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblname" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblcode" runat="server" Text='<%# Eval("Plan Code") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPlanName" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkInfo" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </tbody>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tbody>
                                        <tr class="repeaterItem">
                                            <td>
                                                <asp:Label ID="lblid" runat="server" Text='<%#Eval("COMPANY_ID") %>'></asp:Label>
                                                <asp:Label ID="lblIsTrial" runat="server" Text='<%#Eval("IS_TRIAL") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblTransactionType" runat="server" Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblname" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblcode" runat="server" Text='<%# Eval("Plan Code") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPlanName" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkInfo" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </tbody>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <div>
                            </div>
                            <div style="clear: both">
                                <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                <asp:HiddenField ID="hidCmpIdWhenEdit" runat="server" />
                            </div>
                        </asp:Panel>
                    </section>
                    <section>
                    </section>
                    <div>
                        <asp:HiddenField ID="hdi" runat="server" />
                        <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        <%--<asp:HiddenField ID="hidPlanCodeForApproval" runat="server" />
                        <asp:HiddenField ID="hidIsTrial" runat="server" />--%>
                        <asp:HiddenField ID="hidCodeIsTrialTransactionType" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </section>
    <div class="modalPopUpDetails">
        <div id="divCompanyDtlsContainer" style="display: none; height: 420px;">
            <asp:UpdatePanel runat="server" ID="upUserDtlsModal" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="ProcImgUpd">
                        <div id="DivUserDetails" style="width: 100%;">
                            <asp:Panel ID="pnlMobileUserForm" runat="server">
                                <div style="float: left; width: 100%;">
                                    <div style="border-bottom: 1px solid; width: 100%; font-size: 12px;">
                                        <asp:Label ID="lblcompanyheader" runat="server" Text="Company Details"></asp:Label></div>
                                    <div style="margin-bottom: 5px; margin-top: 5px;">
                                        <div style="float: left; margin-right: 90px; margin-top: 3px;">
                                            <asp:Label ID="label11" runat="server" Text="<strong>Company Name</strong>"></asp:Label></div>
                                        <div style="float: left; margin-top: 3px;">
                                            <asp:Label ID="lblCompanyName" runat="server"></asp:Label></div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-bottom: 20px; margin-top: 5px;">
                                        <div style="float: left; margin-right: 77px; margin-top: 7px;">
                                            <asp:Label ID="label2" runat="server" Text="<strong>Company Address</strong>"></asp:Label></div>
                                        <div style="float: left; margin-top: 5px; width: 180px;">
                                            <asp:Label ID="lblCompanyAddress" runat="server"></asp:Label></div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-bottom: 20px; margin-top: 10px;">
                                        <div style="float: left; margin-right: 98px;">
                                            <asp:Label ID="label5" runat="server" Text="<strong>Company City</strong> "></asp:Label></div>
                                        <div style="float: left; width: 82px;">
                                            <asp:Label ID="lblCmpCity" runat="server"></asp:Label></div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-bottom: 20px; margin-top: 9px;">
                                        <div style="float: left; margin-right: 93px; margin-top: 3px;">
                                            <asp:Label ID="label1" runat="server" Text="<strong>Company State</strong> "></asp:Label></div>
                                        <div style="float: left; margin-top: 3px;">
                                            <asp:Label ID="lblCmpState" runat="server"></asp:Label></div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-bottom: 20px; margin-top: 9px;">
                                        <div style="float: left; margin-right: 90px; margin-top: 3px;">
                                            <asp:Label ID="label3" runat="server" Text="<strong>Company Email</strong> "></asp:Label></div>
                                        <div style="float: left; margin-top: 3px;">
                                            <asp:Label ID="lblCmpEmail" runat="server"></asp:Label></div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-bottom: 20px; margin-top: 9px;">
                                        <div style="float: left; margin-right: 30px; margin-top: 3px;">
                                            <asp:Label ID="label7" runat="server" Text="<strong>Company Contact Number</strong> "></asp:Label></div>
                                        <div style="float: left; margin-top: 3px;">
                                            <asp:Label ID="lblCmpContactNo" runat="server"></asp:Label></div>
                                    </div>
                                </div>
                                <div style="float: left; width: 100%;">
                                    <div style="border-bottom: 1px solid; width: 100%; margin-top: 17px; font-size: 12px;">
                                        <asp:Label ID="Label10" runat="server" Text="Admin Details"></asp:Label></div>
                                    <div style="margin-bottom: 20px;">
                                        <div style="float: left; margin-top: 7px;">
                                            <asp:Label ID="label6" runat="server" Text="<strong>Admin Name </strong>"></asp:Label></div>
                                        <div style="float: left; margin-top: 7px; margin-left: 106px;">
                                            <asp:Label ID="lblCmpAdminName" runat="server"></asp:Label></div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-bottom: 20px;">
                                        <div style="float: left; margin-top: 12px; margin-right: 142px;">
                                            <asp:Label ID="label8" runat="server" Text="<strong>Status</strong> "></asp:Label></div>
                                        <div style="float: left; margin-top: 12px;">
                                            <asp:Label ID="lblStatus" runat="server"></asp:Label></div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-bottom: 20px;">
                                        <div style="float: left; margin-top: 12px; margin-right: 107px;">
                                            <asp:Label ID="label19" runat="server" Text="<strong>Admin Email</strong> "></asp:Label></div>
                                        <div style="float: left; margin-top: 12px;">
                                            <asp:Label ID="lblAdminEmailId" runat="server"></asp:Label></div>
                                    </div>
                                </div>
                                <div style="float: left; width: 100%;">
                                    <div style="border-bottom: 1px solid; width: 100%; margin-top: 17px; font-size: 12px;">
                                        <asp:Label ID="Label4" runat="server" Text="Plan Details"></asp:Label></div>
                                    <div style="margin-bottom: 28px;">
                                        <div style="float: left; margin-top: 7px; margin-right: 121px;">
                                            <asp:Label ID="label12" runat="server" Text="<strong>Plan Name</strong> "></asp:Label></div>
                                        <div style="float: left; margin-top: 7px;">
                                            <asp:Label ID="lblCmpPlanName" runat="server"></asp:Label></div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-bottom: 20px;">
                                        <div style="float: left; margin-top: 7px; margin-right: 124px;">
                                            <asp:Label ID="label9" runat="server" Text="<strong>Plan Code</strong> "></asp:Label></div>
                                        <div style="float: left; margin-top: 7px;">
                                            <asp:Label ID="lblPlanCode" runat="server"></asp:Label></div>
                                    </div>
                                    <%-- <div style="clear:both;"></div>
                               <div style="margin-bottom: 20px;">
                                            <div style="float: left; margin-top: 12px; margin-right: 6px;">
                                                <asp:Label ID="label21" runat="server" Text="<strong>Plan Name</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 8px;">
                                                <asp:Label ID="lbl11" runat="server"></asp:Label></div>
                                        </div>--%>
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-bottom: 20px;">
                                        <div style="float: left; margin-top: 12px; margin-right: 100px;">
                                            <asp:Label ID="label13" runat="server" Text="<strong>Max Workflow</strong> "></asp:Label></div>
                                        <div style="float: left; margin-top: 12px;">
                                            <asp:Label ID="lblMaxWorkflow" runat="server"></asp:Label></div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-bottom: 20px;">
                                        <div style="float: left; margin-top: 12px; margin-right: 124px;">
                                            <asp:Label ID="label15" runat="server" Text="<strong>Max Users</strong> "></asp:Label></div>
                                        <div style="float: left; margin-top: 12px;">
                                            <asp:Label ID="lblMaxNoOfUsers" runat="server"></asp:Label></div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div style="margin-bottom: 20px;">
                                        <div style="float: left; margin-top: 12px; margin-right: 122px;">
                                            <asp:Label ID="label17" runat="server" Text="<strong>Valid Upto</strong> "></asp:Label></div>
                                        <div style="float: left; margin-top: 12px;">
                                            <asp:Label ID="lblValidUpto" runat="server"></asp:Label></div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="singleColumn" style="text-align: center;">
                        <asp:Button ID="btnApprove" runat="server" Text="Approve" OnClick="btnApprove_Click" />&nbsp;&nbsp;
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click"
                            Visible="false" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            //$("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            //s$("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>

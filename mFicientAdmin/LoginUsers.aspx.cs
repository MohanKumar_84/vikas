﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;

namespace mFicientAdmin
{
    public partial class LoginUsers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                if (Request.Cookies["mCookie"] != null)
                {
                    HttpCookie getCookie = Request.Cookies.Get("mCookie");
                    string userName = Convert.ToString(getCookie.Values["UserName"]);
                    if (!String.IsNullOrEmpty(userName))
                    {
                        txtUserName.Text = userName;
                        chkRememberUserName.Checked = true;
                    }

                }
               
            }
            if (Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(hfs.Value) && !string.IsNullOrEmpty(hfp.Value))
                {
                    HttpContext.Current.Items.Add("hfs", hfs.Value);
                    Server.Transfer(hfp.Value, true);
                }
            }

           


       }


        public void showAlert(string message, string divIdToShowAlert)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$.wl_Alert(" + "'" + message + "'" + ",'info','#" + divIdToShowAlert + "'" + ");", true);
        }


        protected void UserLogin(string _userName, string _password)
        {
            string strMessage = Validate();
            if (String.IsNullOrEmpty(strMessage))
            {
                try
                {
                    Login userlogin = new Login( _userName, _password, "web", "", "", "");

                    userlogin.LoginCheck();
                
                
                    if (userlogin.StatusCode <= 0)
                    {
                        if (chkRememberUserName.Checked)
                        {
                            rememberUserName();
                        }

                        hfs.Value = _userName + "," + userlogin.SessionId + "," + userlogin.Role;
                        string ScriptToRun = @"xferBack();";
                        hfp.Value = @"~/LoginUDashboard.aspx";
                        ScriptManager.RegisterClientScriptBlock(TransferUpdatePanel, typeof(UpdatePanel), TransferUpdatePanel.ClientID, ScriptToRun, true);
                    }
                    else
                    {
                        txtPassword.Text = "";
                        showAlert(userlogin.StatusDescription,"content");
                    }

                 }
                catch (Exception ex)
                {
                    
                }
            }
            else
            {
                txtPassword .Text= "";
                showAlert(strMessage, "content");
            }

        }

      
        public string Validate()
        {
            string strMessage = "";

            if (string.IsNullOrEmpty(txtUserName.Text.Trim()))
            {
                strMessage += "Please enter user name." + "<br />";
            }
            if (string.IsNullOrEmpty(txtPassword.Text.Trim()))
            {
                strMessage += "Please enter password." + "<br />";
            }

            return strMessage;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {


            UserLogin(txtUserName.Text,txtPassword.Text);
        } 
        
        protected void lnkForgotPassword_Click(object sender, EventArgs e)
        {
            showModalPopUp("Reset password");
        }

        void closeModalPopUp()
        {
            clearControlsForForgotPassword();
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "CloseModal", @"$('#divModalContainer').dialog('close')", true);
        }
        void showModalPopUp(string modalPopUpHeader)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "ShowPopUp", @"showModalPopUp('divModalContainer','" + modalPopUpHeader + "',400);", true);
        }
        void clearControlsForForgotPassword()
        {
         //   txtForgotPwdCompanyId.Text = "";
            txtForgotPwdUserName.Text = "";
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            string strFieldBorder = "";
            string strMessage = ValidateResetPassword();
            if (String.IsNullOrEmpty(strMessage))
            {
                ResetUsersPassAdmin obj = new ResetUsersPassAdmin();
               string Salesname= obj.Select("S", txtForgotPwdUserName.Text);
               string SalesMname = obj.Select("SM", txtForgotPwdUserName.Text);
               string Support = obj.Select("SUPP", txtForgotPwdUserName.Text);
               string Accounts = obj.Select("ACC", txtForgotPwdUserName.Text);

               
                //string SalesPName = obj.SelectByType("S",txtForgotPwdUserName.Text);
                //string MnagerName = obj.SelectByType("SM", txtForgotPwdUserName.Text);
                //string Support = obj.SelectByType("SUPP", txtForgotPwdUserName.Text);
               // string AccountsName = obj.SelectByType("ACC", txtForgotPwdUserName.Text);

               if (txtForgotPwdUserName.Text == SalesMname)
                {
                    obj.Process("SM",txtForgotPwdUserName.Text);
                    showStickyOrNormalMessage("Password has been reset.Please check your email for the new password");
                }


                else if (txtForgotPwdUserName.Text == Salesname)
                {
                    obj.Process("S", txtForgotPwdUserName.Text);
                    showStickyOrNormalMessage("Password has been reset.Please check your email for the new password");

                
                }

                else if (txtForgotPwdUserName.Text == Support)
                {
                    obj.Process("SUPP", txtForgotPwdUserName.Text);
                    showStickyOrNormalMessage("Password has been reset.Please check your email for the new password");

                }

               else if (txtForgotPwdUserName.Text == Accounts)
                {
                    obj.Process("ACC", txtForgotPwdUserName.Text);
                    showStickyOrNormalMessage("Password has been reset.Please check your email for the new password");

                }
                else
                {
                    showAlert("Invalid user name", "modalPopUpContent");
                }
                                   
            }

            else
            {
                showAlert(strMessage, "modalPopUpContent");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            closeModalPopUp();
        }

        public string ValidateResetPassword()
        {
            string strMessage = "";
           
            if (txtForgotPwdUserName.Text.Trim() == "")
            {
                strMessage += "Please enter user name." + "<br />";
            }

            return strMessage;
        }

        void showStickyOrNormalMessage(string message)
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "TestingSticky", @"$.msg('" + message + "',{sticky:true});", true);
        }

        void rememberUserName()
        {
            HttpCookie myCookie = new HttpCookie("mCookie");
            Response.Cookies.Remove("mCookie");
            Response.Cookies.Add(myCookie);
            if (chkRememberUserName.Checked)
            {
                myCookie.Values.Add("UserName", txtUserName.Text.ToString());
                myCookie.Values.Add("Password", txtPassword.Text.ToString());
            }

            DateTime dtxpiry = DateTime.Now.AddDays(15);
            myCookie.Expires = dtxpiry;
        }



        public string LoginName
        {
            get
            {
                return txtUserName.Text;
            }
        }
     
    
       
        
        }
    }

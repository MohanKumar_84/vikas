﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdmin
{
    /// <summary>
    /// Summary description for CheckCompanyNameExists
    /// </summary>
    public class CheckCompanyNameExists : IHttpHandler
    {
        string _companyId,_response=String.Empty;
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                _companyId = context.Request.QueryString.Get(1);
                DoesCompanyExists objCheckCompExists = new DoesCompanyExists(_companyId);
                objCheckCompExists.Process();
                if(objCheckCompExists.StatusCode ==0)
                    _response = objCheckCompExists.Exists.ToString();
            }
            catch
            { }
            context.Response.ContentType = "text/plain";
            context.Response.Write(_response);
        }


        
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿<%@ Page Title="mFicient" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="MangePlanCp.aspx.cs" Inherits="mFicientAdmin.MangePlanCp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>Plan Details</h1>"></asp:Label>
                                        </div>
                                        <div style="position: relative; top: 10px; right: 15px;">
                                            <asp:LinkButton ID="lnkAddNewMobileUser" runat="server" Text="add" CssClass="repeaterLink fr"
                                               OnClick="lnkAddNewMobileUser_Click"  ></asp:LinkButton>
                                        </div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptUserDetails" runat="server" OnItemCommand="rptUserDetails_ItemCommand">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th style="display:none;">
                                                        plan Id
                                                    </th>
                                                    <th>
                                                       PLAN NAME
                                                    </th>
                                                    <th>
                                                        WORKFLOW
                                                    </th>
                                                    <th>
                                                        PUSH MESSAGE
                                                         <span style="position:relative;top:-1px">*</span>
                                                    </th>
                                                    <th> MINIMUM USERS</th>
                                                    <th>
                                                        ENABLED
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td style="display: none;">
                                                    <asp:Label ID="lblplanid" runat="server" Text='<%#Eval("PLAN_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblplanname" runat="server" Text='<%#Eval("PLAN_NAME") %>'></asp:Label> ( 
                                                    <asp:Label ID="lblplancode" runat="server" Text='<%#Eval("PLAN_CODE") %>'></asp:Label> )
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblworkflow" runat="server" Text='<%# Eval("MAX_WORKFLOW") %>'></asp:Label>
                                                </td>
                                                <%--  <td>
                                                    <asp:Label ID="lblinr" runat="server" Text='<%# Eval("USER_CHARGE_INR_PM") %>'></asp:Label>/
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("USER_CHARGE_USD_PM") %>'></asp:Label>
                                                </td>--%>
                                                <td>
                                                    <asp:Label ID="lblmsgperday" runat="server" Text='<%# Eval("PUSHMESSAGE_PERDAY") %>'></asp:Label>/
                                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("PUSHMESSAGE_PERMONTH") %>'></asp:Label>
                                        <asp:Label ID="lblItemDoubleAsterix" runat="server" Text="**" Visible="false" style="position:relative;top:-2px;margin-left:2px;"></asp:Label>

                                                </td>
                                                <td>
                                                    <asp:Label ID="lblminusers" runat="server" Text='<%# Eval("MIN_USERS") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Lblenabled" runat="server" Text='<%# Eval("IS_ENABLED") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="edit" OnClick="lnkedit_Click" CssClass="repeaterLink"
                                                        CommandName="Edit"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                <td style="display: none;">
                                                    <asp:Label ID="lblplanid" runat="server" Text='<%#Eval("PLAN_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblplanname" runat="server" Text='<%#Eval("PLAN_NAME") %>'></asp:Label> ( 
                                                    <asp:Label ID="lblplancode" runat="server" Text='<%#Eval("PLAN_CODE") %>'></asp:Label> )
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblworkflow" runat="server" Text='<%# Eval("MAX_WORKFLOW") %>'></asp:Label>
                                                </td>
                                                <%--  <td>
                                                    <asp:Label ID="lblinrusd" runat="server" Text='<%# Eval("USER_CHARGE_INR_PM") %>'></asp:Label>
                                                    /
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("USER_CHARGE_USD_PM") %>'></asp:Label>
                                                </td>--%>
                                                <td>
                                                    <asp:Label ID="lblmsgperday" runat="server" Text='<%# Eval("PUSHMESSAGE_PERDAY") %>'></asp:Label>
                                                    /
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("PUSHMESSAGE_PERMONTH") %>'></asp:Label>
                                           <asp:Label ID="Label8" runat="server" Text="**" Visible="false" style="position:relative;top:-2px;margin-left:2px;"></asp:Label>

                                                </td>
                                                 <td>
                                                    <asp:Label ID="lblminusers" runat="server" Text='<%# Eval("MIN_USERS") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Lblenabled" runat="server" Text='<%# Eval("IS_ENABLED") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="edit" CssClass="repeaterLink" OnClick="lnkedit_Click"
                                                        CommandName="Edit"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                         <div style="font-style:italic;margin:2px 0 0 5px;height:auto">* Per Day/Per Month</div>
                                            <div style="font-style:italic;margin:3px 0 0 5px;height:auto">
                                                <asp:Label ID="lblDoubleAsterixInfo" runat="server" Visible="false"></asp:Label></div>
                                    </FooterTemplate>

                                </asp:Repeater>
                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                    <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                                </div>
                            </asp:Panel>
                        </section>
                        <section>
                        </section>
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
        
        <div class="modalPopUpDetails">
            <div id="divUserDtlsModal" style="display: none; height: 420px;">
                <asp:UpdatePanel runat="server" ID="upUserDtlsModal" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="ProcImgUpd">
                            <div id="DivUserDetails" style="width: 100%;">
                                <asp:Panel ID="pnlMobileUserForm" runat="server">
                                    <section id="buttons1" style="float: left;">
                                        <div style="margin-bottom: 35px; margin-top: 12px;">
                                            <div style="float: left; margin-right: 45px;">
                                                <asp:Label ID="label11" runat="server" Text="<strong>Plan Code</strong>"></asp:Label></div>
                                            <div id="lbl" runat="server" style="float: left; margin-right: 34px;">
                                                <asp:Label ID="Plan" runat="server"></asp:Label>
                                            </div>
                                            <div id="txt" runat="server" style="float: left; margin-left: -27px; width: 82px;">
                                                <asp:TextBox ID="txtcode" runat="server" CssClass="textbox"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 40px; margin-top: 15px;">
                                            <div style="float: left; margin-right: 40px; margin-top: 5px;">
                                                <asp:Label ID="label2" runat="server" Text="<strong>Plan Name</strong>"></asp:Label></div>
                                            <div style="float: left; width: 82px; margin-top: 2px;">
                                                <asp:TextBox ID="txtplanname" runat="server"></asp:TextBox></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 20px;margin-top: 10px; ">
                                            <div style="float: left; margin-right: 20px; margin-top: 8px;">
                                                <asp:Label ID="label5" runat="server" Text="<strong>Workflow</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 8px;">
                                                <asp:TextBox ID="txtworkflow" runat="server" CssClass="textbox"></asp:TextBox></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 40px; margin-top: 15px;">
                                            <div style="float: left; margin-right: 53px;">
                                                <asp:Label ID="label7" runat="server" Text="<strong>Enabled</strong>"></asp:Label></div>
                                            <div style="float: left;">
                                                <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" CssClass="checkbox" />
                                            </div>
                                            </div>
                                    </section>
                                    <section id="buttons2" style="float: left; margin-left:20px;">
                                        <div style="margin-bottom: 40px; margin-top: 3px;">
                                            <div style="float: left; margin-top: 10px;margin-right:18px;">
                                                <asp:Label ID="label1" runat="server" Text="<strong>Message Per Day</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 8px;">
                                                <asp:TextBox ID="txtmsgperday" runat="server" CssClass="textbox"></asp:TextBox></div>
                                        </div>
                                        <div style="clear: both;">
                                        </div>
                                        <div style="margin-bottom: 40px; margin-top: 15px;">
                                            <div style="float: left; margin-top: 10px;margin-right:5px;">
                                                <asp:Label ID="label6" runat="server" Text="<strong>Message Per Month</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 8px;">
                                                <asp:TextBox ID="txtmsgpermonth" runat="server" CssClass="textbox"></asp:TextBox></div>
                                        </div>
                                       <div style="clear:both;"></div>
                                      <div style="margin-bottom: 40px; margin-top: 10px;">
                                            <div style="float: left; margin-top: 10px;margin-right:26px;">
                                                <asp:Label ID="label9" runat="server" Text="<strong>Minimum Users</strong> "></asp:Label></div>
                                            <div style="float: left; margin-top: 8px;">
                                                <asp:TextBox ID="txtminusers" runat="server" CssClass="textbox"></asp:TextBox></div>
                                        </div>
                                        <div style="clear:both;"></div>
                                        <%--<div style="margin-bottom: 40px; margin-top: 15px;">
                                            <div style="float: left; margin-right: 92px;">
                                                <asp:Label ID="label7" runat="server" Text="<strong>Enabled</strong>"></asp:Label></div>
                                            <div style="float: left;">
                                                <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" CssClass="checkbox" />
                                            </div>
                                            </div>--%>
                                    </section>
                                    <div style="float: left; margin-top: 15px;">
                                        <asp:Repeater ID="rptPriceDetails" runat="server">
                                            <HeaderTemplate>
                                                <table class="repeaterTable">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                <strong>CURRENCY</strong>
                                                            </th>
                                                            <th>
                                                                1-2
                                                            </th>
                                                            <th>
                                                                3-5
                                                            </th>
                                                            <th>
                                                                6-11
                                                            </th>
                                                            <th>
                                                                12-12
                                                            </th>
                                                        </tr>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tbody>
                                                    <tr class="repeaterItem">
                                                        <td>
                                                            <asp:Label ID="lblcurrency" runat="server" Text='<%#Eval("CURRENCY") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txt1" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txt2" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txt3" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txt4" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <tbody>
                                                    <tr class="repeaterItem">
                                                        <td>
                                                            <asp:Label ID="lblcurrency" runat="server" Text='<%#Eval("CURRENCY") %>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txt1" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txt2" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txt3" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txt4" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </AlternatingItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                      <div class="SubProcborderDiv">
                                        <div class="SubProcBtnDiv" align="center">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                                    </div>
                                    </div>
                                </asp:Panel>
                                </fieldset>
                            </div>
                        </div>
                        </section> </div> </div> </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            //  $("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            //$("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>

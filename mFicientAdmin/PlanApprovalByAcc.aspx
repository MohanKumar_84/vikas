﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true"
    CodeBehind="PlanApprovalByAcc.aspx.cs" Inherits="mFicientAdmin.PlanApprovalByAcc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="divInfomationForApproveDisable">
    </div>
    <div class="clear">
    </div>
    <section>
        <div id="divRepeater" style="margin: 5px;">
            <asp:UpdatePanel ID="upd" runat="server">
                <ContentTemplate>
                    <section class="clear" style="margin-bottom: 5px;">
                        <div id="div1">
                        </div>
                    </section>
                    <section>
                        <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                            <section>
                                <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                    <div>
                                        <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>PLAN CHANGE / RENEW REQUESTS</h1>"></asp:Label>
                                    </div>
                                    <div style="margin: 11px 80px 0px 300px;">
                                        <asp:CheckBox ID="chckApproved" runat="server" Text="Sales Approved" OnCheckedChanged="chckApproved_CheckedChanged"
                                            AutoPostBack="true" />
                                    </div>
                                </asp:Panel>
                            </section>
                            <asp:Repeater ID="rptPlanRequestsDetails" runat="server" OnItemCommand="rptPlanRequestsDetails_ItemCommand">
                                <HeaderTemplate>
                                    <table class="repeaterTable">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Company Id
                                                </th>
                                                <th>
                                                    Company
                                                </th>
                                                <th>
                                                    Plan Requested
                                                </th>
                                                <th>
                                                </th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tbody>
                                        <tr class="repeaterItem">
                                            <td>
                                                <asp:Label ID="lblid" runat="server" Text='<%#Eval("COMPANY_ID") %>'></asp:Label>
                                                <asp:Label ID="lblRequestType" runat="server" Text='<%# Eval("REQUEST_TYPE") %>'
                                                    Visible="false"></asp:Label>
                                                <asp:Label ID="lblMobileUsers" runat="server" Text='<%# Eval("MOBILE_USERS") %>'
                                                    Visible="false"></asp:Label>
                                                <asp:Label ID="lblMonths" runat="server" Text='<%# Eval("NUMBER_MONTH") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblCurrencyType" runat="server" Text='<%# Eval("CURRENCY_TYPE") %>'
                                                    Visible="false"></asp:Label>
                                                <asp:Label ID="lblcode" runat="server" Text='<%# Eval("PLAN_CODE") %>' Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblname" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPlanNameRequested" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info"
                                                    OnClick="lnkok_Click"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </tbody>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tbody>
                                        <tr class="repeaterItem">
                                            <td>
                                                <asp:Label ID="lblid" runat="server" Text='<%#Eval("COMPANY_ID") %>'></asp:Label>
                                                <asp:Label ID="lblRequestType" runat="server" Text='<%# Eval("REQUEST_TYPE") %>'
                                                    Visible="false"></asp:Label>
                                                <asp:Label ID="lblMobileUsers" runat="server" Text='<%# Eval("MOBILE_USERS") %>'
                                                    Visible="false"></asp:Label>
                                                <asp:Label ID="lblMonths" runat="server" Text='<%# Eval("NUMBER_MONTH") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblCurrencyType" runat="server" Text='<%# Eval("CURRENCY_TYPE") %>'
                                                    Visible="false"></asp:Label>
                                                <asp:Label ID="lblcode" runat="server" Text='<%# Eval("PLAN_CODE") %>' Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblname" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPlanNameRequested" runat="server" Text='<%# Eval("PLAN_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info"
                                                    OnClick="lnkok_Click"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </tbody>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <div>
                            </div>
                            <div style="clear: both">
                                <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                            </div>
                        </asp:Panel>
                    </section>
                    <hr />
                    <section>
                        <asp:Panel ID="pnlNewCompanyRptBox" CssClass="repeaterBox" runat="server">
                            <section>
                                <asp:Panel ID="pnlNewCompanyRptHeader" CssClass="repeaterBox-header" runat="server">
                                    <div>
                                        <asp:Label ID="lblNewCompanyRptHeader" runat="server" Text="<h1>New Company Registration</h1>"></asp:Label>
                                    </div>
                                </asp:Panel>
                            </section>
                            <asp:Repeater ID="rptNewCompanyDetails" runat="server" OnItemCommand="rptNewCompanyDetails_ItemCommand">
                                <HeaderTemplate>
                                    <table class="repeaterTable">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Company Id
                                                </th>
                                                <th>
                                                    Company
                                                </th>
                                                <th>
                                                    Plan Code
                                                </th>
                                                <th>
                                                    Plan Name
                                                </th>
                                                <th>
                                                </th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tbody>
                                        <tr class="repeaterItem">
                                            <td>
                                                <asp:Label ID="lblCompanyId" runat="server" Text='<%#Eval("COMPANY_ID") %>'></asp:Label>
                                                <asp:Label ID="lblTransactionType" runat="server" Text='<%#Eval("TRANSACTION_TYPE") %>'
                                                    Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCompanyName" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPlanCode" runat="server" Text='<%#Eval("PLAN_CODE") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPlanName" runat="server" Text='<%#Eval("PLAN_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkInfo" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </tbody>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tbody>
                                        <tr class="repeaterItem">
                                            <td>
                                                <asp:Label ID="lblCompanyId" runat="server" Text='<%#Eval("COMPANY_ID") %>'></asp:Label>
                                                <asp:Label ID="lblTransactionType" runat="server" Text='<%#Eval("TRANSACTION_TYPE") %>'
                                                    Visible="false"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCompanyName" runat="server" Text='<%#Eval("COMPANY_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPlanCode" runat="server" Text='<%#Eval("PLAN_CODE") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPlanName" runat="server" Text='<%#Eval("PLAN_NAME") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkInfo" runat="server" Text="Info" CssClass="repeaterLink" CommandName="Info"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </tbody>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <div>
                            </div>
                            <div style="clear: both">
                                <asp:HiddenField ID="hidPlanCurrencyAndMonthsForApproval" runat="server" />
                                <asp:HiddenField ID="HiddenField2" runat="server" />
                            </div>
                        </asp:Panel>
                    </section>
                    <div>
                        <asp:HiddenField ID="hdi" runat="server" />
                        <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </section>
    <div id="divCompanyModalContainer" class="modalPopUpDetails">
        <div id="divUserDtlsModal" class="container">
            <asp:UpdatePanel runat="server" ID="upUserDtlsModal" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="DivUserDetails" style="width: 100%;">
                        <div style="margin-bottom: 20px; display: none;">
                            <div>
                                <asp:Label ID="label8" Text="<strong>Company Id</strong>" runat="server"></asp:Label></div>
                            <asp:Label ID="lblCompanyId" runat="server"></asp:Label>
                            <asp:Label ID="lblPlanCode" runat="server"></asp:Label>
                            <asp:Label ID="lblRequestType" runat="server"></asp:Label>
                            <asp:Label ID="lblMobileUsers" runat="server"></asp:Label>
                            <asp:Label ID="lblNoOfMonths" runat="server"></asp:Label>
                            <asp:Label ID="lblCurrrencyType" runat="server"></asp:Label>
                            <asp:Label ID="lblChargePerUserPerMonth" runat="server"></asp:Label>
                        </div>
                        <div>
                            <asp:Label ID="lid" runat="server"></asp:Label>
                        </div>
                        <%--<div style="margin-bottom: 20px;">
                                        <div style="float: left; margin-right: 58px;">
                                            <asp:Label ID="label16" Text="<strong>Approve</strong>" runat="server"></asp:Label></div>
                                        <div style="float: left;">
                                           <asp:CheckBox ID="chckapprove" runat="server" />
                                        </div>
                                    </div>--%>
                        <div class="clear">
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label1" Text="<strong>Company</strong>" runat="server"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lname" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label2" Text="<strong>Address Line 1</strong>" runat="server"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="ladd1" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label3" Text="<strong>Address Line 2</strong>" runat="server"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="ladd2" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label4" Text="<strong>Address Line 3</strong>" runat="server"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="ladd3" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label5" Text="<strong>City</strong>" runat="server"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lcity" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label6" Text=" <strong>State</strong>" runat="server"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lstate" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label7" Text="<strong>Country</strong>" runat="server"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lcountry" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label9" Text="<strong>Zip Code</strong>" runat="server"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lzip" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label11" Text="<strong>Email</strong>" runat="server"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lemail" runat="server"></asp:Label></div>
                        </div>
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="label13" Text="<strong>Contact Number</strong>" runat="server"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:Label ID="lnumber" runat="server"></asp:Label></div>
                        </div>
                        <asp:Panel ID="pnlAmountDetails" runat="server">
                            <div class="singleColumn">
                                <div class="leftColumn">
                                    <asp:Label ID="lblForPrice" Text="<strong>Price</strong>" runat="server"></asp:Label></div>
                                <div class="middleColumn">
                                    :
                                </div>
                                <div class="lastColumn">
                                    <asp:Label ID="lblPrice" runat="server"></asp:Label></div>
                            </div>
                            <div class="singleColumn">
                                <div class="leftColumn">
                                    <asp:Label ID="lblForDiscount" Text="<strong>Discount</strong>" runat="server"></asp:Label></div>
                                <div class="middleColumn">
                                    :
                                </div>
                                <div class="lastColumn">
                                    <asp:Label ID="lblDiscount" runat="server"></asp:Label></div>
                            </div>
                            <div class="singleColumn">
                                <div class="leftColumn">
                                    <asp:Label ID="lblForPayableAmount" Text="<strong>Payable Amount</strong>" runat="server"></asp:Label></div>
                                <div class="middleColumn">
                                    :
                                </div>
                                <div class="lastColumn">
                                    <asp:Label ID="lblPayableAmount" runat="server"></asp:Label></div>
                            </div>
                            <div class="singleColumn">
                                <div class="leftColumn">
                                    <asp:Label ID="label10" Text="<strong>Received Payment</strong>" runat="server"></asp:Label></div>
                                <div class="middleColumn">
                                    :
                                </div>
                                <div class="lastColumn">
                                    <asp:CheckBox ID="chckPayement" runat="server" /></div>
                            </div>
                            <div class="singleColumn">
                                <div class="leftColumn">
                                    <asp:Label ID="label14" Text="<strong>Amount</strong>" runat="server"></asp:Label></div>
                                <div class="middleColumn">
                                    :
                                </div>
                                <div class="lastColumn">
                                    <asp:TextBox ID="txtamount" runat="server" Style="width: 50%;"></asp:TextBox></div>
                            </div>
                        </asp:Panel>
                        <div class="action center">
                            <%--<asp:LinkButton ID="lnkAddNewUser" runat="server" Text="Save" CssClass="repeaterLink"
                                OnClick="lnkSave_Click"></asp:LinkButton>--%>
                            <asp:Button ID="btnModalSave" runat="server" Text="Save" OnClick="btnModalSave_Click" />&nbsp;&nbsp;
                            <asp:Button ID="btnDenyRequest" runat="server" Text="Deny" OnClientClick="return confirm('Are you sure you want to deny the request');"
                                OnClick="btnDenyRequest_Click" />
                            <asp:Button ID="btnModalCancel" runat="server" Text="Cancel" OnClick="btnModalCancel_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="divDenyRequestBox" style="display: none;" class="modalPopUpDetails">
        <div class="container">
            <asp:UpdatePanel runat="server" ID="updDenyRequest" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divDenyRequest">
                        <div class="singleColumn">
                            <div class="leftColumn">
                                <asp:Label ID="lblSelectRemarks" runat="server" Text="Remarks"></asp:Label></div>
                            <div class="middleColumn">
                                :
                            </div>
                            <div class="lastColumn">
                                <asp:DropDownList ID="ddlRemarks" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="action center">
                            <asp:Button ID="btnSaveDenyRemarks" runat="server" Text="Save" OnClick="btnSaveDenyRemarks_Click" />
                            <asp:Button ID="btnCancelDenyRemarks" runat="server" Text="Cancel" OnClick="btnCancelDenyRemarks_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            //$("input").uniform();
            showWaitModal();
        }
        function prm_endRequest() {
            //s$("input").uniform();
            hideWaitModal();
        }
    </script>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Canvas.Master" AutoEventWireup="true" CodeBehind="ManageCpSeller.aspx.cs" Inherits="mFicientAdmin.ManageCpSeller" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageCanvas" runat="server">
    <div id="PageCanvasContent">
        <section>
            <div id="divRepeater">
                <asp:UpdatePanel ID="upd" runat="server">
                    <ContentTemplate>
                        <section>
                            <div id="divInformation">
                            </div>
                        </section>
                        <section>
                            <asp:Panel ID="pnlRepeaterBox" CssClass="repeaterBox" runat="server">
                                <section>
                                    <asp:Panel ID="pnlRepeaterBoxHeader" CssClass="repeaterBox-header" runat="server">
                                        <div>
                                            <asp:Label ID="lblHeaderInfo" runat="server" Text="<h1>SELLER DETAILS</h1>"></asp:Label>
                                        </div>
                                        <div style="position: relative; top: 10px; right: 15px;">
                                            <asp:LinkButton ID="lnkAddNewMobileUser" runat="server" Text="add" CssClass="repeaterLink fr"
                                                OnClick="lnkAddNewMobileUser_Click"></asp:LinkButton>
                                        </div>
                                    </asp:Panel>
                                </section>
                                <asp:Repeater ID="rptUserDetails" runat="server" OnItemCommand="rptUserDetails_ItemCommand"
                                    OnItemDataBound="rptUserDetails_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="repeaterTable">
                                            <thead>
                                                <tr>
                                                    <th >
                                                        Seller Name
                                                    </th>
                                                        <th>
                                                        Email
                                                    </th>
                                                    <th>
                                                        Contact Number
                                                    </th>
                                                    <th>
                                                        Created On
                                                    </th>
                                                    <th>
                                                       Enabled
                                                    </th>
                                                    <th>

                                                 Seller Id
                                                    </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tbody>
                                            <tr class="repeaterItem">
                                                <td >
                                                    <asp:Label ID="lblsellername" runat="server" Text='<%#Eval("SELLER_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblemail" runat="server" Text='<%#Eval("EMAIL") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblnumber" runat="server" Text='<%# Eval("CONTACT_NUMBER") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbldate" runat="server" Text='<%# Eval("CREATED_ON") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Lblenabled" runat="server" Text='<%# Eval("IS_ENABLE") %>'></asp:Label>
                                                </td>
                                                 <td>
                                                    <asp:Label ID="Lblsellerid" runat="server" Text='<%# Eval("SELLER_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text="edit" CssClass="repeaterLink" CommandName="Edit"></asp:LinkButton>
                                                    
                                                </td>
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tbody>
                                            <tr class="repeaterAlternatingItem">
                                                 <td >
                                                    <asp:Label ID="lblsellername" runat="server" Text='<%#Eval("SELLER_NAME") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblemail" runat="server" Text='<%#Eval("EMAIL") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblnumber" runat="server" Text='<%# Eval("CONTACT_NUMBER") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbldate" runat="server" Text='<%# Eval("CREATED_ON") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Lblenabled" runat="server" Text='<%# Eval("IS_ENABLE") %>'></asp:Label>
                                                </td>
                                                 <td>
                                                    <asp:Label ID="Lblsellerid" runat="server" Text='<%# Eval("SELLER_ID") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="edit" CssClass="repeaterLink" CommandName="Edit"></asp:LinkButton>
                                                    
                                                </td>
                                              </tr>
                                        </tbody>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <div>
                                </div>
                                <div style="clear: both">
                                    <asp:HiddenField ID="hidDocCatIdForEdit" runat="server" />
                                    <asp:HiddenField ID="hidDetailsFormMode" runat="server" />
                                </div>
                            </asp:Panel>
                        </section>
                        <section>
                            <asp:Panel ID="pnlMobileUserForm" Visible="false" runat="server">
                                <div class="g11">
                                    <div id="formDiv">
                                        <fieldset>
                                            <label>
                                                Details</label>
                                            <section>
                                                <label for="txtname">
                                                    Seller Name</label>
                                                <div>
                                                    <asp:TextBox ID="txtname" runat="server"
                                                        Width="50%"></asp:TextBox>
                                                </div>
                                            </section>
                                            <section>
                                                <label for="txtemail">
                                                    Email</label>
                                                <div>
                                                    <asp:TextBox ID="txtemail" runat="server" Width="50%" 
                                                        ></asp:TextBox>
                                                </div>
                                            </section>
                                            <section>
                                                <label for="txtnumber">
                                                 Contact Number</label>
                                                <div>
                                                    <asp:TextBox ID="txtnumber" runat="server" Width="50%"></asp:TextBox>
                                                </div>
                                            </section>

                                            <section>
                                                <label for="txtsellerid">
                                                 Seller Id</label>
                                                <div>
                                                    <asp:TextBox ID="txtsellerid" runat="server" Width="50%"></asp:TextBox>
                                                </div>
                                            </section>

                                            <section>
                                                <label for="txtdate">
                                                 Created On</label>
                                                <div>
                                                    <asp:TextBox ID="txtdate" runat="server" Width="50%"></asp:TextBox>
                                                </div>
                                            </section>

                                            <section>
                                                <label for="chkIsActive">
                                                    Is Active</label>
                                                <div>
                                                    <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" />
                                                </div>
                                            </section>
                                        </fieldset>
                                        <section id="buttons">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="aspButton" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="aspButton" OnClick="btnCancel_Click" />
                                        </section>
                                    </div>
                                </div>
                            </asp:Panel>
                        </section>
                        <div>
                            <asp:HiddenField ID="hdi" runat="server" />
                            <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </section>
        <div>
            <asp:HiddenField ID="hfs" runat="server" />
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            //Sys.Debug.trace("Application.Init");
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            $("input").uniform();
        }
        function prm_endRequest() {
            $("input").uniform();
        }
        </script>









</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace mFicientAdmin
{
    public partial class MangePlanCp : System.Web.UI.Page
    {
        string hfsValue = string.Empty;

        string strUserName = string.Empty;
        string strSessionId = string.Empty;
        string strRole = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (Page.IsPostBack)
            {
                MasterPage mainMaster = Page.Master.Master;
                hfsValue = ((HiddenField)mainMaster.FindControl("hfs")).Value;
            }
            else
            {
                System.Web.UI.Page previousPage = Page.PreviousPage;
                if (previousPage == null)
                {
                    Response.End();
                    return;
                }

                System.Web.UI.HtmlControls.HtmlControl previousPageForm = Page.PreviousPage.Form;
                if (previousPageForm == null) return;

                hfsValue = ((HiddenField)previousPageForm.FindControl("hfs")).Value;
            }

            if (string.IsNullOrEmpty(hfsValue)) return;

            context.Items["hfs"] = hfsValue;

            string[] hfsParts = hfsValue.Split(',');

            strUserName = hfsParts[0];
            strSessionId = hfsParts[1];
            strRole = hfsParts[2];

            context.Items["hfs1"] = strUserName;
            context.Items["hfs2"] = strSessionId;
            context.Items["hfs3"] = strRole;

            if (!Page.IsPostBack)
            {
                /*if (HttpContext.Current.Items.Contains("hfs"))
                {
                    hfs.Value = Convert.ToString((string)HttpContext.Current.Items["hfs"]);
                }
                else
                {
                    if (Page.PreviousPage != null)
                    {
                        string prvPageName = Page.PreviousPage.AppRelativeVirtualPath;
                        prvPageName = prvPageName.Substring(prvPageName.LastIndexOf("/") + 1);
                        ContentPlaceHolder cphPageCanvas = (ContentPlaceHolder)PreviousPage.Master.Master.FindControl("form").FindControl("MainCanvas").FindControl("PageCanvas");
                        hfs.Value = ((HiddenField)cphPageCanvas.FindControl("hfs")).Value;
                    }
                }*/
                BindRepeater();
                BindDiscountRepeaterForAddNew();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "postBackScript", " $(\"select\").uniform();$(\"input\").uniform();$(\"#content\").find('div.widgets').wl_Widget();", true);
            }
        }

        public void BindDiscountRepeaterForAddNew()
        {
            AddPlanDetailsAdmin ad = new AddPlanDetailsAdmin();
            DataTable objdt = ad.GetDiscountListToAddPaln();
            if (objdt.Rows.Count > 0)
            {
                rptPriceDetails.DataSource = objdt;
                rptPriceDetails.DataBind();
            }

        }
        void BindRepeater()
        {
            AddPlanDetailsAdmin ad = new AddPlanDetailsAdmin();
            DataTable dt = ad.GetPlanList();
            if (dt.Rows.Count > 0)
            {
                lblHeaderInfo.Text = "<h1>Plan Details</h1>";
                rptUserDetails.Visible = true;
                rptUserDetails.DataSource = dt;
                rptUserDetails.DataBind();
            }
            else if (dt.Rows.Count == 0)
            {
                lblHeaderInfo.Text = "<h1>No Details of plan are added yet</h1>";
                rptUserDetails.Visible = false;
            }
        }

        void EnableDisable(string mode)
        {
            if (mode == "Add")
            {
                txt.Visible = true;
                lbl.Visible = false;
            }
            else if (mode == "Edit")
            {
                lbl.Visible = true;
                txt.Visible = false;
            }
        }

        protected void lnkedit_Click(object sender, EventArgs e)
        {
            Utilities.showModalPopup("divUserDtlsModal", this.Page, "<strong>Edit Plan Details</strong>", "500", true, false);
            //enableDisablePanels("Edit");
            upUserDtlsModal.Update();
            EnableDisable("Edit");
        }

        protected void lnkAddNewMobileUser_Click(object sender, EventArgs e)
        {
            Utilities.showModalPopup("divUserDtlsModal", this.Page, "<strong>Add Plan Details</strong>", "500", true, false);
            //enableDisablePanels("Add");
            upUserDtlsModal.Update();
            clearControls();
            EnableDisable("Add");
            //BindDiscountRepeater();
        }

        protected void clearControls()
        {
            txtcode.Text = "";
            Plan.Text = "";
            txtplanname.Text = "";
            txtworkflow.Text = "";
            txtmsgperday.Text = "";
            txtmsgpermonth.Text = "";
            txtminusers.Text = "";
            hidDetailsFormMode.Value = "";
            for (int i = 0; i < rptPriceDetails.Items.Count; i++)
            {
                Label inr = (this.rptPriceDetails.Items[i].FindControl("lblcurrency") as Label);
                TextBox text1 = (this.rptPriceDetails.Items[i].FindControl("txt1") as TextBox);
                TextBox text2 = (this.rptPriceDetails.Items[i].FindControl("txt2") as TextBox);
                TextBox text3 = (this.rptPriceDetails.Items[i].FindControl("txt3") as TextBox);
                TextBox text4 = (this.rptPriceDetails.Items[i].FindControl("txt4") as TextBox);
                text1.Text = "";
                text2.Text = "";
                text3.Text = "";
                text4.Text = "";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (hidDetailsFormMode.Value != "")
            {
                //Edit
                GetPlanDetailsAdmin plandetail = new GetPlanDetailsAdmin();
                plandetail.PlanDetails(hidDetailsFormMode.Value);
                DataTable dt = plandetail.ResultTable;
                if (dt.Rows.Count > 0)
                {
                    UpdatePlanDetails(hidDetailsFormMode.Value);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                }
            }
            else
            {
                //new user
                SavePlanDetails();
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
            }
            BindRepeater();
            pnlRepeaterBox.Visible = true;
        }

        protected void UpdatePlanDetails(string Plancode)
        {
            EnableDisable("Edit");
            string strMessage = ValidatePanelEdit();
            if (!String.IsNullOrEmpty(strMessage))
            {
                Utilities.showAlert(strMessage, "DivUserDetails", this.Page);
            }
            else
            {
                UpdateplandetailsAdmin updatedetail = new UpdateplandetailsAdmin();
                updatedetail.UpdatePlan(Plancode, txtplanname.Text, Convert.ToInt32(txtworkflow.Text), Convert.ToInt32(txtmsgperday.Text), Convert.ToInt32(txtmsgpermonth.Text), chkIsActive.Checked,Convert.ToInt32(txtminusers.Text));
                int intStatusCode = updatedetail.StatusCode;
                string strDescription = updatedetail.StatusDescription;
                for (int i = 0; i < rptPriceDetails.Items.Count; i++)
                {
                    //Label code = (this.rptPriceDetails.Items[i].FindControl("lblcode") as Label);
                    Label inr = (this.rptPriceDetails.Items[i].FindControl("lblcurrency") as Label);
                    TextBox text1 = (this.rptPriceDetails.Items[i].FindControl("txt1") as TextBox);
                    TextBox text2 = (this.rptPriceDetails.Items[i].FindControl("txt2") as TextBox);
                    TextBox text3 = (this.rptPriceDetails.Items[i].FindControl("txt3") as TextBox);
                    TextBox text4 = (this.rptPriceDetails.Items[i].FindControl("txt4") as TextBox);
                    updatedetail.UpdatePricePlan(Convert.ToDecimal(text1.Text), Convert.ToInt32("1"), Convert.ToInt32("2"), inr.Text, Plan.Text);
                    updatedetail.UpdatePricePlan(Convert.ToDecimal(text2.Text), Convert.ToInt32("3"), Convert.ToInt32("5"), inr.Text, Plan.Text);
                    updatedetail.UpdatePricePlan(Convert.ToDecimal(text3.Text), Convert.ToInt32("6"), Convert.ToInt32("11"), inr.Text, Plan.Text);
                    updatedetail.UpdatePricePlan(Convert.ToDecimal(text4.Text), Convert.ToInt32("12"), Convert.ToInt32("12"), inr.Text, Plan.Text);
                }
                if (intStatusCode == 0)
                {
                    Utilities.showMessage("Plan Details have been Updated successfully", this.Page, "second script", DIALOG_TYPE.Info);
                    // enableDisablePanels("");
                    Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
                }
                else
                {
                    Utilities.showMessage(updatedetail.StatusDescription, this.Page, "second script", DIALOG_TYPE.Error);
                }
            }
            BindRepeater();
            EnableDisable("Edit");
        }
        protected void SavePlanDetails()
        {
            string strMessage = ValidatePanel();
            if (!String.IsNullOrEmpty(strMessage))
            {
                Utilities.showAlert(strMessage, "DivUserDetails", this.Page);
            }
            else
            {
                AddPlanDetailsAdmin addplan = new AddPlanDetailsAdmin();
                addplan.AddPlan(txtcode.Text, Utilities.GetMd5Hash(txtplanname.Text + DateTime.Now.Ticks.ToString()), txtplanname.Text, Convert.ToInt32(txtworkflow.Text), Convert.ToInt32(txtmsgperday.Text), Convert.ToInt32(txtmsgpermonth.Text), chkIsActive.Checked,Convert.ToInt32(txtminusers.Text));
                int intStatusCode = addplan.StatusCode;
                string strDescription = addplan.StatusDescription;
                if (strDescription != "")
                {
                    Utilities.showMessage(strDescription, this.Page);
                }
                 for (int i = 0; i < rptPriceDetails.Items.Count; i++)
                    {
                        Label inr = (this.rptPriceDetails.Items[i].FindControl("lblcurrency") as Label);
                        TextBox text1 = (this.rptPriceDetails.Items[i].FindControl("txt1") as TextBox);
                        TextBox text2 = (this.rptPriceDetails.Items[i].FindControl("txt2") as TextBox);
                        TextBox text3 = (this.rptPriceDetails.Items[i].FindControl("txt3") as TextBox);
                        TextBox text4 = (this.rptPriceDetails.Items[i].FindControl("txt4") as TextBox);
                        addplan.AddPlanPrice(Convert.ToInt32("1"), Convert.ToInt32("2"), Convert.ToDecimal(text1.Text), inr.Text, txtcode.Text);
                        addplan.AddPlanPrice(Convert.ToInt32("3"), Convert.ToInt32("5"), Convert.ToDecimal(text2.Text), inr.Text, txtcode.Text);
                        addplan.AddPlanPrice(Convert.ToInt32("6"), Convert.ToInt32("11"), Convert.ToDecimal(text3.Text), inr.Text, txtcode.Text);
                        addplan.AddPlanPrice(Convert.ToInt32("12"), Convert.ToInt32("12"), Convert.ToDecimal(text4.Text), inr.Text, txtcode.Text);
                    }

                    if (intStatusCode == 0)
                    {
                        Utilities.showMessage("Plan Details have been Added successfully", this.Page, "second script", DIALOG_TYPE.Info);
                        Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
                    }
                    else
                    {
                        Utilities.showMessage("Internal Error,Please Try Again", this.Page, "second script", DIALOG_TYPE.Error);
                        Utilities.closeModalPopUp("divUserDtlsModal", this.Page);
                    }
                }
                BindRepeater();
                EnableDisable("Add");
            }
        

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearControls();
        }

        public string ValidatePanel()
        {
            string strmessage = "";
            if (String.IsNullOrEmpty(txtcode.Text))
            {
                strmessage = strmessage + "Please enter plan code<br />";
            }

            else if (!Utilities.IsValidString(txtcode.Text, true, true, true, " ", 1, 100, false, false))
            {
                strmessage = strmessage + "Please enter correct plan code<br />";
            }

            if (String.IsNullOrEmpty(txtplanname.Text))
            {
                strmessage = strmessage + "Please enter plan name<br />";
            }

            else if (!Utilities.IsValidString(txtplanname.Text, true, true, false, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct plan name<br />";
            }
            if (String.IsNullOrEmpty(txtworkflow.Text))
            {
                strmessage = strmessage + "Please enter workflow<br />";
            }

            else if (!Utilities.IsValidString(txtworkflow.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct workflow<br />";
            }
            if (String.IsNullOrEmpty(txtmsgperday.Text))
            {
                strmessage = strmessage + "Please enter number of messages per day<br />";
            }

            else if (!Utilities.IsValidString(txtmsgperday.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct number of messages per day<br />";
            }

            if (String.IsNullOrEmpty(txtmsgpermonth.Text))
            {
                strmessage = strmessage + "Please enter number of messages per month<br />";
            }

            else if (!Utilities.IsValidString(txtmsgpermonth.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct number of messages per month<br />";
            }

            if (String.IsNullOrEmpty(txtminusers.Text))
            {
                strmessage = strmessage + "Please enter minimum number of users <br/>";
            }
            else if (!Utilities.IsValidString(txtminusers.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct number of minimum users<br />";
            }

            for (int i = 0; i < rptPriceDetails.Items.Count; i++)
            {
                Label currency = (this.rptPriceDetails.Items[i].FindControl("lblcurrency") as Label);
                TextBox text1 = (this.rptPriceDetails.Items[i].FindControl("txt1") as TextBox);
                TextBox text2 = (this.rptPriceDetails.Items[i].FindControl("txt2") as TextBox);
                TextBox text3 = (this.rptPriceDetails.Items[i].FindControl("txt3") as TextBox);
                TextBox text4 = (this.rptPriceDetails.Items[i].FindControl("txt4") as TextBox);

                if (currency.Text == "INR")
                {
                    if ((String.IsNullOrEmpty(text1.Text)) || (String.IsNullOrEmpty(text2.Text)) || (String.IsNullOrEmpty(text3.Text)) || (String.IsNullOrEmpty(text4.Text)))
                    {
                        strmessage = strmessage + "Please enter Price in INR<br />";
                    }
                    else if ((!Utilities.IsValidString(text2.Text, false, false, true, ".", 1, 100, false, false)) || (!Utilities.IsValidString(text3.Text, false, false, true, ".", 1, 100, false, false)) || (!Utilities.IsValidString(text3.Text, false, false, true, ".", 1, 100, false, false)) || (!Utilities.IsValidString(text3.Text, false, false, true, ".", 1, 100, false, false)))
                    {
                        strmessage = strmessage + "Please enter correct Price in INR<br />";
                    }
                }

                else if (currency.Text == "USD")
                {
                    if ((string.IsNullOrEmpty(text1.Text)) || (string.IsNullOrEmpty(text2.Text)) || (string.IsNullOrEmpty(text3.Text)) || (string.IsNullOrEmpty(text4.Text)))
                    {
                        strmessage = strmessage + "Please enter Price in USD<br />";
                    }

                    else if ((!Utilities.IsValidString(text2.Text, false, false, true, ".", 1, 100, false, false)) || (!Utilities.IsValidString(text3.Text, false, false, true, ".", 1, 100, false, false)) || (!Utilities.IsValidString(text3.Text, false, false, true, ".", 1, 100, false, false)) || (!Utilities.IsValidString(text3.Text, false, false, true, ".", 1, 100, false, false)))
                    {
                        strmessage = strmessage + "Please enter correct Price in USD<br />";
                    }
                }
            }
            return strmessage;
        }


        public string ValidatePanelEdit()
        {
            string strmessage = "";
            if (String.IsNullOrEmpty(txtplanname.Text))
            {
                strmessage = strmessage + "Please enter plan name<br />";
            }
            else if (!Utilities.IsValidString(txtplanname.Text, true, true, false, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct plan name<br />";
            }

            if (String.IsNullOrEmpty(txtworkflow.Text))
            {
                strmessage = strmessage + "Please enter workflow<br />";
            }

            else if (!Utilities.IsValidString(txtworkflow.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct Workflow<br />";
            }
            if (String.IsNullOrEmpty(txtmsgperday.Text))
            {
                strmessage = strmessage + "Please enter number of messages per day<br />";
            }
            else if (!Utilities.IsValidString(txtmsgperday.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct number of messages per day<br />";
            }

            if (String.IsNullOrEmpty(txtmsgpermonth.Text))
            {
                strmessage = strmessage + "Please enter number of messages per month<br />";
            }
            else if (!Utilities.IsValidString(txtmsgpermonth.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct number of messages per month<br />";
            }

            if (String.IsNullOrEmpty(txtminusers.Text))
            {
                strmessage = strmessage + "Please enter minimum number of users<br/>";
            }

            else if (!Utilities.IsValidString(txtminusers.Text, false, false, true, "", 1, 20, false, false))
            {
                strmessage = strmessage + "Please enter correct  minimum number of users<br/>";
            }

            for (int i = 0; i < rptPriceDetails.Items.Count; i++)
            {
                Label currency = (this.rptPriceDetails.Items[i].FindControl("lblcurrency") as Label);
                TextBox text1 = (this.rptPriceDetails.Items[i].FindControl("txt1") as TextBox);
                TextBox text2 = (this.rptPriceDetails.Items[i].FindControl("txt2") as TextBox);
                TextBox text3 = (this.rptPriceDetails.Items[i].FindControl("txt3") as TextBox);
                TextBox text4 = (this.rptPriceDetails.Items[i].FindControl("txt4") as TextBox);

                if (currency.Text == "INR")
                {
                    if ((String.IsNullOrEmpty(text1.Text)) || (String.IsNullOrEmpty(text2.Text)) || (String.IsNullOrEmpty(text3.Text)) || (String.IsNullOrEmpty(text4.Text)))
                    {
                        strmessage = strmessage + "Please enter price in INR<br />";
                    }
                    else  if ((!Utilities.IsValidString(text1.Text, false, false, true, ".", 1, 100, false, false)) || (!Utilities.IsValidString(text2.Text, false, false, true, ".", 1, 100, false, false)) || (!Utilities.IsValidString(text3.Text, false, false, true, ".", 1, 100, false, false)) || (!Utilities.IsValidString(text4.Text, false, false, true, ".", 1, 100, false, false)))
                    {
                        strmessage = strmessage + "Please enter correct price in INR<br />";
                    }
                }

                if (currency.Text == "USD")
                {
                    if ((String.IsNullOrEmpty(text1.Text)) || (String.IsNullOrEmpty(text2.Text)) || (String.IsNullOrEmpty(text3.Text)) || (String.IsNullOrEmpty(text4.Text)))
                    {
                        strmessage = strmessage + "Please enter price in USD<br />";
                    }

                    else  if ((!Utilities.IsValidString(text1.Text, false, false, true, ".", 1, 100, false, false)) || (!Utilities.IsValidString(text2.Text, false, false, true, ".", 1, 100, false, false)) || (!Utilities.IsValidString(text3.Text, false, false, true, ".", 1, 100, false, false)) || (!Utilities.IsValidString(text4.Text, false, false, true, ".", 1, 100, false, false)))
                    {
                        strmessage = strmessage + "Please enter correct price in USD<br />";
                    }
                }
            }
            return strmessage;
        }

        protected void FillPlanPriceDetails(string plancode)
        {
            if (rptPriceDetails.Items.Count > 0)
            {
                AddPlanDetailsAdmin ad = new AddPlanDetailsAdmin();
                DataTable objdt = ad.GetPlanDiscountList(plancode);
                
                for (int i = 0; i < rptPriceDetails.Items.Count; i++)
                {
                    Label inr = (this.rptPriceDetails.Items[i].FindControl("lblcurrency") as Label);
                    TextBox text1 = (this.rptPriceDetails.Items[i].FindControl("txt1") as TextBox);
                    TextBox text2 = (this.rptPriceDetails.Items[i].FindControl("txt2") as TextBox);
                    TextBox text3 = (this.rptPriceDetails.Items[i].FindControl("txt3") as TextBox);
                    TextBox text4 = (this.rptPriceDetails.Items[i].FindControl("txt4") as TextBox);
                    EnableDisable("Edit");
                    GetPlanDetailsAdmin plandetails = new GetPlanDetailsAdmin();
                    text1.Text = SearchPrice(objdt,"1", "2", inr.Text);
                    text2.Text = SearchPrice(objdt, "3", "5", inr.Text);
                    text3.Text = SearchPrice(objdt, "6", "11", inr.Text);
                    text4.Text = SearchPrice(objdt, "12", "12", inr.Text);
                }
            }
        }

        private string SearchPrice(DataTable objdt,string fromMonth,string ToMonth,string Currency)
        {
            foreach (DataRow dr in objdt.Rows)
            {
                if (dr["FROM_MONTH"].ToString() == fromMonth && dr["TO_MONTH"].ToString() == ToMonth && dr["CURRENCY"].ToString() == Currency)
                    return Convert.ToString(dr["PRICE"]);
            }
            return "";
        }

        protected void FillPlanDetails(string planid)
        {
            EnableDisable("Edit");
            GetPlanDetailsAdmin plandetails = new GetPlanDetailsAdmin();
            plandetails.PlanDetails(planid);
            DataTable dt = plandetails.ResultTable;
            if (dt.Rows.Count > 0)
            {
                hidDetailsFormMode.Value = planid;
                Plan.Text = Convert.ToString(dt.Rows[0]["PLAN_CODE"]);
                txtplanname.Text = Convert.ToString(dt.Rows[0]["PLAN_NAME"]);
                txtworkflow.Text = Convert.ToString(dt.Rows[0]["MAX_WORKFLOW"]);
                txtminusers.Text = Convert.ToString(dt.Rows[0]["MIN_USERS"]);
                txtmsgperday.Text = Convert.ToString(dt.Rows[0]["PUSHMESSAGE_PERDAY"]);
                txtmsgpermonth.Text = Convert.ToString(dt.Rows[0]["PUSHMESSAGE_PERMONTH"]);
                chkIsActive.Checked = Convert.ToBoolean(dt.Rows[0]["IS_ENABLED"]);
            }
            else
            {
                ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
            }
        }

        protected void rptUserDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Edit":
                    processEdit(e);
                    RepeaterItem item = e.Item;
                    Label lit_id = (Label)item.FindControl("lblplancode");
                    AddPlanDetailsAdmin ad = new AddPlanDetailsAdmin();
                    processPlanPriceEdit(lit_id.Text);
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Testing2", @"$('#PageCanvasContent').find('input.date, div.date').wl_Date();", true);
                    break;
            }
        }

       void processEdit(RepeaterCommandEventArgs e)
        {
            Label plan_id = (Label)e.Item.FindControl("lblplanid");
            FillPlanDetails(plan_id.Text);
            //enableDisablePanels("Edit");
        }

        void processPlanPriceEdit(string plancode)
        {
            FillPlanPriceDetails(plancode);
            // enableDisablePanels("Edit");
        }
    }
}
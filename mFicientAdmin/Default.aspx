﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="mFicientAdmin.Login1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title>mficient | Login</title>
    <meta name="description" content="" />
    <meta name="author" content="revaxarts.com" />
    <!-- Apple iOS and Android stuff -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="apple-touch-icon-precomposed" href="img/icon.png" />
    <link rel="apple-touch-startup-image" href="img/startup.png" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1" />
    <!-- Google Font and style definitions -->
    <link rel="stylesheet" href="css/CanvasStyle.css" id="Link2" />
    <link rel="stylesheet" href="css/themes/CanvasTheme.css" id="Link1" />
    <link rel="stylesheet" href="css/themes/jquery-CP-Theme-ui.css" id="Link3" />
    <%--<link rel="stylesheet" href="css/themes/jquery-ui.css" id="Link3" />--%>
    <link rel="stylesheet" href="css/themes/jquery.uniform.css" id="Link4" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/themes/theme.css" id="themestyle" />
    <!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
    <script src="Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script src="Scripts/login.js" type="text/javascript"></script>
    <script src="Scripts/WLJS.js" type="text/javascript"></script>
    <script src="Scripts/Scripts.js" type="text/javascript"></script>
    <script type="text/javascript">
        function clearResetPasswordFields() {
            $('#' + '<%=txtForgotPwdUserName.ClientID %>').val('');
        }
    </script>
</head>
<html>
<body id="login">
    <form id="loginform" runat="server" style="border: 0px;">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <header id="loginLogo">
                <div id="logo">
                    <a href="Default.aspx">mficient</a>
                </div>
            </header>
            <section id="content">
                <div id="formDiv" style="margin-bottom: 1px;">
                    <fieldset>
                        <section>
                            <label for="username" style="margin-left: 10px">
                                Username</label>
                            <div class="rememberParticularField">
                                <asp:CheckBox ID="chkRememberUserName" Checked="true" runat="server" Text="remember" />
                            </div>
                            <div style="margin-bottom: 6px;">
                                <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox></div>
                        </section>
                        <section>
                            <label for="password" style="margin-left: 10px">
                                Password
                            </label>
                            <div>
                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                            </div>
                            <div style="clear: both">
                            </div>
                        </section>
                        <section>
                            <asp:LinkButton ID="lnkForgotPassword" runat="server" Style="font-size: 12px; height: 13px;
                                padding-left: 4%;" OnClientClick="clearResetPasswordFields();showModalPopUp('divModalContainer','Reset password',350,false);return false;">
                                lost password?</asp:LinkButton>
                            <div style="float: right; width: auto; padding-top: 2%;">
                                <asp:Button ID="btnSubmit" runat="server" Text="login" CssClass="fr aspButton" OnClick="btnSubmit_Click" />
                            </div>
                        </section>
                </div>
            </fieldset>
            <asp:HiddenField ID="hidQueryStrFromEmail" runat="server" />
            </div> </section>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="TransferUpdatePanel" UpdateMode="Conditional">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <asp:HiddenField ID="hfs" runat="server" />
            <asp:HiddenField ID="hfp" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <section>
        <div id="divModalContainer">
            <div id="divForgotPwdContainer" style="display: block">
                <asp:UpdatePanel ID="updModalContainer" runat="server">
                    <ContentTemplate>
                        <div id="divResetPwdError">
                        </div>
                        <div id="divResetPwdContainer" class="modalPopupForm">
                            <div id="divResetPwdContent" class="modalPopupFormContent">
                                <div>
                                    <label for="<%=txtForgotPwdUserName.ClientID %>">
                                        Username</label>
                                    <div style="margin-top: 5px;">
                                        <asp:TextBox ID="txtForgotPwdUserName" runat="server" CssClass="required"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="modalPopUpAction">
                                    <div id="divPopUpActionCont" class="modalPopUpActionCenterTwo">
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="aspButton" OnClick="btnReset_Click" />&nbsp;
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="aspButton" OnClientClick="clearResetPasswordFields();$('#divModalContainer').dialog('close');return false;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </section>
    <div id="divWaitBox" style="background-color: transparent !important; border: 0px solid transparent !important;
        margin-left: auto; margin-right: auto; margin-top: 10%; display: none;">
        <img src="css/images/icons/dark/progress.gif" alt="Loading" />
    </div>
    </form>
</body>
<script type="text/javascript">
    Sys.Application.add_init(application_init);
    var idOfPostBackElement = "";
    function application_init() {
        //Sys.Debug.trace("Application.Init");
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(prm_initializeRequest);
        prm.add_beginRequest(prm_beginRequest);
        prm.add_endRequest(prm_endRequest);
    }
    function prm_initializeRequest() {
        //$("input").uniform();
    }
    function prm_beginRequest(sender, args) {
        idOfPostBackElement = args.get_postBackElement().id;
        showWaitModal();
    }
    function prm_endRequest() {
        if (idOfPostBackElement != 'btnReset' && idOfPostBackElement != 'btnCancel') {
            $("select").uniform(); $("input").uniform();
        }
        idOfPostBackElement = "";
    }
    function txtCompanyId_onclick() {

    }

</script>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class Get_Menu_And_Workflows_Resp
    {
        ResponseStatus _respStatus;
        Get_Menu_And_Workflows_RespData _menuCategoryList;
        string _requestId;
        public Get_Menu_And_Workflows_Resp(ResponseStatus respStatus, Get_Menu_And_Workflows_RespData menuCategoryList, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _menuCategoryList = menuCategoryList;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Menu Category List Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            GetMenuAndWorkflowsResp objMenuCatgoryListResp = new GetMenuAndWorkflowsResp();
            objMenuCatgoryListResp.func = Convert.ToString((int)FUNCTION_CODES.GET_MENU_AND_WORKFLOW_LIST);
            objMenuCatgoryListResp.rid = _requestId;
            objMenuCatgoryListResp.status = _respStatus;
            objMenuCatgoryListResp.data = _menuCategoryList;
            objMenuCatgoryListResp.data.html = Convert.ToString(MficientConstants.HTML_VERSION);
            string strJsonWithDetails = Utilities.SerializeJson<GetMenuAndWorkflowsResp>(objMenuCatgoryListResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class GetMenuAndWorkflowsResp : CommonResponse
    {
        public GetMenuAndWorkflowsResp()
        { }
        //[DataMember]
        //public List<FormListResponseData> data { get; set; }
        [DataMember]
        public Get_Menu_And_Workflows_RespData data { get; set; }
    }

    public class Get_Menu_And_Workflows_RespData
    {
        /// <summary>
        /// HTML Version
        /// </summary>
        [DataMember]
        public string html { get; set; }
        /// <summary>
        /// Menu Category Details
        /// </summary>
        [DataMember]
        public List<Menu_And_Workflows_Detail> mcat { get; set; }

        [DataMember]
        public List<GetOfflineDBTableData> offtbl { get; set; }
    }

    public class Menu_And_Workflows_Detail
    {
        /// <summary>
        ///Menu Category Id
        /// </summary>
        [DataMember]
        public string mid { get; set; }

        /// <summary>
        ///Menu Category Name
        /// </summary>
        [DataMember]
        public string mnm { get; set; }

        /// <summary>
        ///Display Index
        /// </summary>
        [DataMember]
        public string di { get; set; }

        /// <summary>
        ///Category Icon
        /// </summary>
        [DataMember]
        public string icn { get; set; }

        /// <summary>
        ///Workflow List
        /// </summary>
        [DataMember]
        public List<WorkFlowList> wf { get; set; }

    }





    public class WorkFlowList
    {
        /// <summary>
        ///WorkFlow Id
        /// </summary>
        [DataMember]
        public string wfid { get; set; }

        /// <summary>
        ///WorkFlow Name
        /// </summary>
        [DataMember]
        public string wfnm { get; set; }

        /// <summary>
        ///WorkFlow Description
        /// </summary>
        [DataMember]
        public string dsc { get; set; }

        /// <summary>
        ///WorkFlow Icon
        /// </summary>
        [DataMember]
        public string wficn { get; set; }

        /// <summary>
        ///app version
        /// </summary>
        [DataMember]
        public string appv { get; set; }

        /// <summary>
        ///type
        /// </summary>
        [DataMember]
        public string typ { get; set; }

        [DataMember]
        public string off { get; set; }
    }


    [DataContract]
    public class GetOfflineDBTableData
    {
        [DataMember]
        public string nm { get; set; }

        [DataMember]
        public string id { get; set; }

        [DataMember]
        public string mod { get; set; }

        [DataMember]
        public List<OfflinetblObject> obj { get; set; }
    }

    [DataContract]
    public class OfflinetblObject
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string mod { get; set; }
    }
}
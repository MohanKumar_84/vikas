﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Npgsql;
using System.Configuration;

namespace mFicientWS
{
    public class PostgreSqlClient
    {
        public static void OpenConnection(out NpgsqlConnection Conn, string connectionString)
        {
            try
            {
                Conn = new NpgsqlConnection(connectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch (NpgsqlException ex)
            {
                Conn = null;
                throw ex;
            }
            catch(Exception ex)
            {
                Conn = null;
                throw ex;// new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }

        public static void CloseConnection(NpgsqlConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open)) Conn.Close();
            }
            catch
            {
            }
        }

        //public static DataSet SelectDataFromCommand(NpgsqlCommand _NpgsqlCommand, string ConString)
        //{
        //    NpgsqlConnection Conn = null;
        //    try
        //    {
        //        OpenConnection(out Conn,ConString);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    DataSet ds = null;
        //    try
        //    {
        //        _NpgsqlCommand.Connection = Conn;
        //        ds = new DataSet();

        //        NpgsqlDataAdapter objSqlDataAdapter = new NpgsqlDataAdapter(_NpgsqlCommand);

        //        objSqlDataAdapter.Fill(ds);
        //    }
        //    catch (Exception ex)
        //    {
        //        ds = null;
        //        throw ex;
        //    }
        //    finally
        //    {
        //        CloseConnection(Conn);
        //    }
        //    return ds;
        //}

        public static DataSet SelectDataUsingTransaction(NpgsqlCommand _NpgsqlCommand, NpgsqlConnection connection, NpgsqlTransaction transaction)
        {
            _NpgsqlCommand.Connection = connection;
            _NpgsqlCommand.Transaction = transaction;
            DataSet ds = new DataSet();

            NpgsqlDataAdapter objSqlDataAdapter = new NpgsqlDataAdapter(_NpgsqlCommand);

            objSqlDataAdapter.Fill(ds);

            return ds;

        }

        /// <summary>
        /// Use it for transaction.It is used without any try catch block connection is not closes on error
        /// </summary>
        /// <param name="con"></param>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public static DataSet SelectDataWithOpenCon(NpgsqlCommand cmdWithConnection)
        {
            DataSet ObjDataSet = new DataSet();

            NpgsqlDataAdapter objSqlDataAdapter = new NpgsqlDataAdapter(cmdWithConnection);

            objSqlDataAdapter.Fill(ObjDataSet);

            return ObjDataSet;
        }

        /// <summary>
        /// Use it for transaction.It is used without any try catch block connection is not closes on error
        /// </summary>
        /// <param name="con"></param>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public static DataSet SelectDataWithOpenConForDoTask(NpgsqlCommand cmdWithConnection, List<QueryParameters> parameters)
        {
            //Every time add new set of parameter.It is possible that the cmd is already used
            //and already has parameters with same name.hence this is required
            cmdWithConnection.Parameters.Clear();
            DataSet ObjDataSet = new DataSet();

            NpgsqlDataAdapter objSqlDataAdapter = new NpgsqlDataAdapter(cmdWithConnection);

            foreach (QueryParameters param in parameters)
            {
                cmdWithConnection.Parameters.AddWithValue("@" + param.para, param.val);
            }

            objSqlDataAdapter.Fill(ObjDataSet);

            return ObjDataSet;
        }

        //public static int ExecuteNonQueryRecord(NpgsqlCommand cmd, string ConString)
        //{
        //    NpgsqlConnection Conn = null;
        //    try
        //    {
        //        OpenConnection(out Conn,ConString);
        //        cmd.Connection = Conn;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    try
        //    {
        //        return cmd.ExecuteNonQuery();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        CloseConnection(Conn);
        //    }
        //}

        //public static int ExecuteNonQueryUsingTransaction(NpgsqlCommand _NpgsqlCommand, NpgsqlConnection connection, NpgsqlTransaction transaction)
        //{
        //    _NpgsqlCommand.Connection = connection;
        //    _NpgsqlCommand.Transaction = transaction;
        //    try
        //    {
        //        return _NpgsqlCommand.ExecuteNonQuery();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static DataSet SelectDataFromDoTask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _password)
        {
            NpgsqlConnection conn = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId,  _userId,  _password);
                dbcon.GetConnector();
                
                try
                {
                    OpenConnection(out conn, dbcon.ConnectionString);
                }
                catch (NpgsqlException ex)
                {
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                }

                NpgsqlCommand cmd = new NpgsqlCommand(db_cmd.SqlQuery, conn);

                foreach (QueryParameters parameters in LstParameters)
                {
                    cmd.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                DataSet ObjDataSet = new DataSet();

                NpgsqlDataAdapter objSqlDataAdapter = new NpgsqlDataAdapter(cmd);

                objSqlDataAdapter.Fill(ObjDataSet);

                return ObjDataSet;
            }
            catch(Exception ex)
            {
                //return null;
                if (ex.Message.Contains("Must declare the scalar variable"))
                {
                    throw new Exception(((int)DO_TASK_ERROR.SQL_PARAMETER_ERROR).ToString());
                }
                else if (conn == null
                   && ex.Message.ToLower() == ((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString().ToLower())
                {
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                }
                else
                {
                    throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
                }
            }
            finally
            {
                CloseConnection(conn);
            }
        }

        public static int ExecuteNonQueryRecordDotask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _password)
        {
            NpgsqlConnection conn = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId,  _userId,  _password);
                
                try
                {
                    OpenConnection(out conn, dbcon.ConnectionString);
                }
                catch (NpgsqlException ex)
                {
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                }
                NpgsqlCommand cmd = new NpgsqlCommand(db_cmd.SqlQuery, conn);

                foreach (QueryParameters parameters in LstParameters)
                {
                    cmd.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                int i = cmd.ExecuteNonQuery();

                return i;
            }
            catch(Exception ex)
            {
                if (ex.Message.Contains("Must declare the scalar variable"))
                {
                    throw new Exception(((int)DO_TASK_ERROR.SQL_PARAMETER_ERROR).ToString());
                }
                else if (conn == null
                    && ex.Message.ToLower() == ((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString().ToLower())
                {
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                }
                else
                {
                    throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
                }
            }
            finally
            {
                CloseConnection(conn);
            }

        }

        public static int ExecuteNonQueryWithOpenCon(NpgsqlCommand cmdWithCon, List<QueryParameters> parameters)
        {
            //Every time add new set of parameter.It is possible that the cmd is already used
            //and already has parameters with same name.hence this is required
            cmdWithCon.Parameters.Clear();
            foreach (QueryParameters param in parameters)
            {
                cmdWithCon.Parameters.AddWithValue("@" + param.para, param.val);
            }
            return cmdWithCon.ExecuteNonQuery();
        }
    }
}
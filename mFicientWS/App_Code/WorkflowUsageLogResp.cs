﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class WorkflowUsageLogResp
    {
        ResponseStatus _respStatus;
        string _requestId;

        public WorkflowUsageLogResp(ResponseStatus respStatus,
            string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating work flow usage log Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            WorkflowUsageLogResponse objWFUsageLog = new WorkflowUsageLogResponse();
            objWFUsageLog.func = Convert.ToString((int)FUNCTION_CODES.WORK_FLOW_USAGE_LOG);
            objWFUsageLog.rid = _requestId;
            objWFUsageLog.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<WorkflowUsageLogResponse>(objWFUsageLog);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class WorkflowUsageLogResponse : CommonResponse
    {
        public WorkflowUsageLogResponse()
        { }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class RemoveContactResp
    {
        ResponseStatus _respStatus;
        string _requestId;

        public RemoveContactResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Removing Contact Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            RemoveContactResponse objRemoveContactResp = new RemoveContactResponse();
            objRemoveContactResp.func = Convert.ToString((int)FUNCTION_CODES.REMOVE_CONTACT);
            objRemoveContactResp.rid = _requestId;
            objRemoveContactResp.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<RemoveContactResponse>(objRemoveContactResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class RemoveContactResponse : CommonResponse
    {
        public RemoveContactResponse()
        {

        }
    }
}
﻿using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.IO;
using System.Web.UI;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace mFicientWS
{
    public class GetAppsHtmlHelpersNew
    {
        public static string getSingleHtmlForApp(jqueryAppClass appDetail, string uiVersion, string appName, DataSet dtObjects, int modelType)
        {
            Hashtable formActionButton = new Hashtable();
            List<string> _offlineTBL;
            string strFormsHtml = formHtmlFromDTable(appDetail.forms, dtObjects, out formActionButton, modelType, out _offlineTBL);
            StringWriter stringWriter = new StringWriter();
            AppJsonConverter objAJC = new AppJsonConverter(appDetail, dtObjects, formActionButton, _offlineTBL);
            string strAppJson = objAJC.ConvertInAppJsonForMobile();
            if (!string.IsNullOrEmpty(strAppJson))
            {
                using (HtmlTextWriter writer = new HtmlTextWriter(stringWriter, string.Empty))
                {
                    //DOC TYPE
                    writer.Write("<!DOCTYPE html>\r\n");
                    //HTML 
                    writer.RenderBeginTag(HtmlTextWriterTag.Html);

                    //HEAD SECTION
                    writer.RenderBeginTag(HtmlTextWriterTag.Head);

                    writer.RenderBeginTag(HtmlTextWriterTag.Title);
                    writer.Write("Loading...");
                    writer.RenderEndTag();

                    writer.AddAttribute(HtmlTextWriterAttribute.Name, "viewport");
                    writer.AddAttribute(HtmlTextWriterAttribute.Content, "width=300, initial-scale=1, user-scalable=no");
                    writer.RenderBeginTag(HtmlTextWriterTag.Meta);
                    writer.RenderEndTag();

                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/css");
                    writer.RenderBeginTag(HtmlTextWriterTag.Style);
                    writer.Write("body {background-color:#E9E9E9; color:#888888; font:bold 14px Helvetica,Arial,sans-serif; text-align:center; text-shadow:0 1px 0 #FFFFFF;}");
                    writer.RenderEndTag();

                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/javascript");
                    writer.RenderBeginTag(HtmlTextWriterTag.Script);
                    writer.Write("function $(p) {this.load = function() { return; }; this.ready = function() { return; }; return this; }");
                    writer.RenderEndTag();

                    writer.RenderEndTag();
                    //END HEAD SECTION

                    //BODY SECTION
                    writer.RenderBeginTag(HtmlTextWriterTag.Body);

                    //WAIT-- INSIDE BODY
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, "WAIT");
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, "width:100%;");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    writer.Write("<br/>One moment please!<br/>...preparing for first use...");
                    writer.RenderEndTag();
                    //END WAIT

                    //APP JSON --INSIDE BODY
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, "PROCFLOW");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    //SCRIPT -- INSIDE APP JSON
                    writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/javascript");
                    writer.RenderBeginTag(HtmlTextWriterTag.Script);

                    writer.Write("var APPFLOW = JSON.stringify(" + strAppJson + ");");
                    writer.RenderEndTag();
                    //END SCRIPT
                    writer.RenderEndTag();
                    //END APP JSON

                    //VIEWS---INSIDE BODY
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, "VIEWS");
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);

                    //FORM MAIN FORM HTML
                    writer.Write(strFormsHtml);//Passing writer object,we will get the changes

                    writer.RenderEndTag();
                    //END VIEWS

                    writer.RenderEndTag();
                    //END BODY

                    writer.RenderEndTag();
                    //END HTML
                }
            }
            return stringWriter.ToString();
        }

        private static string formHtmlFromDTable(List<AppForm> lstForm, DataSet dtObjects, out Hashtable formActionButton, int modelType, out List<string> offlineTBL)
        {
            //htLabels = new Hashtable();
            StringWriter stringWriter = new StringWriter();
            formActionButton = new Hashtable();
            offlineTBL = new List<string>();
            List<string> _offlineTBLsHTML = new List<string>();
            using (HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter, string.Empty))
            {
                List<AppForm> lstChildForm;
                GenerateFormHtml objJsonToHtml;
                foreach (AppForm row in lstForm)
                {
                    lstChildForm = new List<AppForm>();
                    JArray JAactionBtn = new JArray();
                    objJsonToHtml = new GenerateFormHtml(row, false, row.id, dtObjects, out lstChildForm, out JAactionBtn, modelType, out _offlineTBLsHTML);
                    if (JAactionBtn.Count > 0) formActionButton.Add(row.id.ToUpper(), JAactionBtn);
                    FormHtmlWrite(htmlWriter, objJsonToHtml, row.id);

                    foreach (string olTBL in _offlineTBLsHTML)
                        if (!offlineTBL.Contains(olTBL)) offlineTBL.Add(olTBL);

                    if (lstChildForm.Count > 0)
                    {
                        foreach (AppForm rowChild in lstChildForm)
                        {
                            lstChildForm = new List<AppForm>();
                            JAactionBtn = new JArray();
                            string strChildformId = (row.id + "-" + rowChild.name.Substring(0, 1)).ToUpper();
                            objJsonToHtml = new GenerateFormHtml(rowChild, true, strChildformId, dtObjects, out lstChildForm, out JAactionBtn, modelType, out _offlineTBLsHTML);
                            FormHtmlWrite(htmlWriter, objJsonToHtml, strChildformId);
                        }
                        offlineTBL.AddRange(_offlineTBLsHTML);
                    }
                }
            }
            return stringWriter.ToString();
        }
        private static void FormHtmlWrite(HtmlTextWriter htmlWriter, GenerateFormHtml objJsonToHtml, string Form_ID)
        {
            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Style, "display:none;");
            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, Form_ID);
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);

            //HEAD 
            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, "HEAD_" + Form_ID);//HEAD_PAGE_ID(AS ABOVE)
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
            htmlWriter.Write(Microsoft.JScript.GlobalObject.escape(objJsonToHtml.FormHtmlHead));
            htmlWriter.RenderEndTag();
            //END HEAD
            //BODY
            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Id, "BODY_" + Form_ID);//BODY_PAGE_ID(AS ABOVE)
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
            htmlWriter.Write(objJsonToHtml.FormHtmlBody);
            htmlWriter.RenderEndTag();
            //END BODY
            htmlWriter.RenderEndTag();
            //End HTML FILES 
        }
    }
}
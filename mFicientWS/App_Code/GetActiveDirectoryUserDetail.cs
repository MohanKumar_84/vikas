﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class GetActiveDirectoryUserDetail
    {
        GetActiveDirectoryUserDetailReq _getActiveDirectoryUserDetailReq;

        public GetActiveDirectoryUserDetail(GetActiveDirectoryUserDetailReq getActiveDirectoryUserDetailReq)
        {
            _getActiveDirectoryUserDetailReq = getActiveDirectoryUserDetailReq;
        }

        public GetActiveDirectoryUserDetailResp Process()
        {
            if (String.IsNullOrEmpty(_getActiveDirectoryUserDetailReq.RequestId)
                || String.IsNullOrEmpty(_getActiveDirectoryUserDetailReq.CompanyId)
                || String.IsNullOrEmpty(_getActiveDirectoryUserDetailReq.AgentName)
                || String.IsNullOrEmpty(_getActiveDirectoryUserDetailReq.AgentPassword)
                || String.IsNullOrEmpty(_getActiveDirectoryUserDetailReq.DomainName)
                || String.IsNullOrEmpty(_getActiveDirectoryUserDetailReq.Username))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
            ActiveDirectoryUser objActiveDirUserDetails = new ActiveDirectoryUser();
            try
            {
                ActiveDirectoryUser user = mPlugin.ActiveDirectoryUserDetail(_getActiveDirectoryUserDetailReq.CompanyId,
                    _getActiveDirectoryUserDetailReq.AgentName, _getActiveDirectoryUserDetailReq.AgentPassword,_getActiveDirectoryUserDetailReq.DomainName, _getActiveDirectoryUserDetailReq.Username);
                objActiveDirUserDetails = user;
                return new GetActiveDirectoryUserDetailResp(objActiveDirUserDetails, "0", "");
            }
            catch (mPlugin.mPluginException e)
            {
                objActiveDirUserDetails = null;
                return new GetActiveDirectoryUserDetailResp(objActiveDirUserDetails, "99948", e.Message);
            }
            catch (HttpException e)
            {
                throw (new Exception(Convert.ToString(e.GetHttpCode())));
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
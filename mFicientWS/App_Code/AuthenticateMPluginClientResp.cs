﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class AuthenticateMPluginClientResp
    {
        ResponseStatus _respStatus;
        string _requestId;
        public AuthenticateMPluginClientResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Authenticate MPlugin Client Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            AuthenticateMPluginClientResponse objClientAuthenticationResponse = new AuthenticateMPluginClientResponse();
            objClientAuthenticationResponse.func = Convert.ToString((int)FUNCTION_CODES.MPLUGIN_CLIENT_AUTHENTICATION);
            objClientAuthenticationResponse.rid = _requestId;
            //objClientAuthenticationResponse.status = null;
            string strJsonWithDetails = Utilities.SerializeJson<AuthenticateMPluginClientResponse>(objClientAuthenticationResponse);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class AuthenticateMPluginClientResponse : MPlugInCommonResponse
    {
        public AuthenticateMPluginClientResponse()
        { }
    }
}
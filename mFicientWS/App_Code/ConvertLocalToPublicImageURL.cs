﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class ConvertLocalToPublicImageURL
    {
        private string strMPlugInUrl = string.Empty;
        ConvertLocalToPublicImageURLReq _objConvertLocalToPublicImageURL;
        public ConvertLocalToPublicImageURL(ConvertLocalToPublicImageURLReq convertLocalToPublicImageURLReq)
        {
            _objConvertLocalToPublicImageURL = convertLocalToPublicImageURLReq;
        }
        public ConvertLocalToPublicImageURLResp Process()
        {
            ResponseStatus _respStatus = new ResponseStatus();
            string strImageData = "";
            try
            {
                string stragentPassword = string.Empty;
                string stragentName = string.Empty;
                
                _respStatus.cd = "0";
                _respStatus.desc = "";
                strMPlugInUrl = Utilities.getMPlugInServerURL(_objConvertLocalToPublicImageURL.CompanyID);

                string strQuery = "select MP_AGENT_PASSWORD,MP_AGENT_NAME from TBL_MPLUGIN_AGENT_DETAIL where COMPANY_ID=@COMPANY_ID and MP_AGENT_ID=@MP_AGENT_ID";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", _objConvertLocalToPublicImageURL.CompanyID);
                cmd.Parameters.AddWithValue("@MP_AGENT_ID", _objConvertLocalToPublicImageURL.Agentname);
                DataSet dsTable = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (dsTable != null && dsTable.Tables[0] != null)
                {
                    if (dsTable.Tables[0].Rows.Count > 0)
                    {
                        stragentPassword = Convert.ToString(dsTable.Tables[0].Rows[0]["MP_AGENT_PASSWORD"]);
                        stragentName = Convert.ToString(dsTable.Tables[0].Rows[0]["MP_AGENT_NAME"]);
                    }
                    else
                    {
                        _respStatus.cd = ((int)DO_TASK_ERROR.MPLUGIN_AGENT_NOT_FOUND).ToString();
                        _respStatus.desc = "Agent not Found";
                    }
                }
                else
                {
                    _respStatus.cd = ((int)DO_TASK_ERROR.MPLUGIN_AGENT_NOT_FOUND).ToString();
                    _respStatus.desc = "Agent not Found";
                }
                if(!string.IsNullOrEmpty(stragentName))
                    strImageData = mPlugin.GetBinaryImageData(_objConvertLocalToPublicImageURL.CompanyID, stragentName, stragentPassword, _objConvertLocalToPublicImageURL.ImageUrl, _objConvertLocalToPublicImageURL.MaxWidth);
            }
            catch (mPlugin.mPluginException mex)
            {
                try
                {
                    _respStatus = Utilities.getResponseStatus(mex);
                }
                catch
                {
                    _respStatus = Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()));
                }
            }
            catch (HttpException hex)
            {
                _respStatus = Utilities.getResponseStatus( Utilities.HttpExecptionToException(hex));
            }
            catch (Exception ex)
            {
                throw ex;
            } 
            return new ConvertLocalToPublicImageURLResp(strImageData, _respStatus, _objConvertLocalToPublicImageURL.RequestId);
            
        }

        private System.Drawing.Image ConvertStringToImageAndUpload(string strImg)
        {
            byte[] imageBytes = Encoding.Default.GetBytes(strImg);
            MemoryStream ms = new MemoryStream(imageBytes, 0,imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }

        private string GetMimeType(System.Drawing.Image img)
        {
            var imgguid = img.RawFormat.Guid;
            foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageDecoders())
            {
                if (codec.FormatID == imgguid)
                    return codec.MimeType;
            }
            return "image/unknown";
        }
    }
}
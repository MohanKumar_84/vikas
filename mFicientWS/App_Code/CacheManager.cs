﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Text;
using System.Security.Cryptography;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Xml.Serialization;
using System.IO;

namespace mFicientWS
{
    public class CacheManager
    {
        private const string SLIDING_EXPIRY = @"S";
        private const string ABSOLUTE_EXPIRY = @"A";

        internal enum CacheType
        {
            mFicientSession = 0,
            mFicientHttpResponse = 1,
            CompanyTimezone = 3,
            mFicientEmail = 4,
            mFicientDSPaging = 5,
        }

        internal static T Insert<T>(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
        {
            if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(key.Trim()) || value == null) return default(T);

            if (slidingExpiration == TimeSpan.Zero)
            {
                //ABSOLUTE EXPIRATION
                HttpRuntime.Cache.Insert(key.ToUpper().Trim(), value, null, absoluteExpiration, Cache.NoSlidingExpiration, CacheItemPriority.High, new CacheItemRemovedCallback(RemovedCallback));
                try
                {
                    if (SaveInCacheDatabase<T>(key.ToUpper().Trim(), value, ABSOLUTE_EXPIRY, absoluteExpiration.Ticks) <= 0)
                        throw new Exception("Error");
                }
                catch
                {
                    HttpRuntime.Cache.Remove(key.ToUpper().Trim());
                    return default(T);
                }
            }
            else
            {
                //SLIDING EXPIRATION
                HttpRuntime.Cache.Insert(key.ToUpper().Trim(), value, null, Cache.NoAbsoluteExpiration, slidingExpiration, CacheItemPriority.High, new CacheItemRemovedCallback(RemovedCallback));
                try
                {
                    if (SaveInCacheDatabase<T>(key.ToUpper().Trim(), value, SLIDING_EXPIRY, DateTime.UtcNow.Ticks + slidingExpiration.Ticks) <= 0)
                        throw new Exception("Error");
                }
                catch
                {
                    HttpRuntime.Cache.Remove(key.ToUpper().Trim());
                    return default(T);
                }
            }

            return (T)value;
        }

        internal static T Get<T>(string key, bool forceFromDatabase)
        {
            object value = null;
            if (!forceFromDatabase)
            {
                value = HttpRuntime.Cache[key.ToUpper()];

                if (value != null) return (T)value;
            }
            DatabaseCacheRecord<T> dcr = GetFromCacheDatabase<T>(key);
            if (dcr == null) return default(T);

            switch (dcr.CacheExpiryType)
            {
                case SLIDING_EXPIRY:
                    if (typeof(T) == typeof(SessionUser))
                    {
                        return Insert<T>(dcr.Key, dcr.Value, DateTime.UtcNow, TimeSpan.FromSeconds(MficientConstants.SESSION_VALIDITY_SECONDS));
                    }
                    else
                        return Insert<T>(dcr.Key, dcr.Value, DateTime.UtcNow, TimeSpan.FromTicks(dcr.ExpiryTime - dcr.UpdatedTime));

                case ABSOLUTE_EXPIRY:
                    return Insert<T>(dcr.Key, dcr.Value, new DateTime(dcr.ExpiryTime), TimeSpan.Zero);
            }
            return default(T);
        }


        internal static void Remove(string key)
        {
            HttpRuntime.Cache.Remove(key.ToUpper());
            RemoveInCacheDatabaseWithoutContainer(key);
        }

        internal static void RemovedCallback(String key, Object value, CacheItemRemovedReason r)
        {
            RemoveInCacheDatabase(key);
        }

        internal static string GetKey(CacheType cacheType,
            string enterpriseId,
            string user, string deviceId,
            string deviceType,
            string sessionId,
            string requestId,
            string datasetId)
        {
            try
            {
                switch (cacheType)
                {
                    case CacheType.mFicientSession:
                        enterpriseId = enterpriseId.Trim().ToUpper();
                        user = user.Trim().ToUpper();
                        deviceId = deviceId.Trim().ToUpper();
                        deviceType = deviceType.Trim().ToUpper();
                        break;

                    case CacheType.mFicientHttpResponse:
                        enterpriseId = enterpriseId.Trim().ToUpper();
                        user = user.Trim().ToUpper();
                        sessionId = sessionId.Trim().ToUpper();
                        requestId = requestId.Trim().ToUpper();
                        break;

                    case CacheType.CompanyTimezone:
                        enterpriseId = enterpriseId.Trim().ToUpper();
                        break;

                    case CacheType.mFicientEmail:
                        requestId = requestId.Trim().ToUpper();
                        break;

                    case CacheType.mFicientDSPaging:
                        enterpriseId = enterpriseId.Trim().ToUpper();
                        deviceId = deviceId.Trim().ToUpper();
                        deviceType = deviceType.Trim().ToUpper();
                        datasetId = datasetId.Trim().ToUpper();
                        break;
                }
            }
            catch
            {
                return null;
            }

            switch (cacheType)
            {
                case CacheType.mFicientSession:
                    if (string.IsNullOrEmpty(enterpriseId) || string.IsNullOrEmpty(user) || string.IsNullOrEmpty(deviceId) || string.IsNullOrEmpty(deviceType)) return null;
                    return ((int)cacheType).ToString() + @"-" + enterpriseId + @"-" + user + @"-" + deviceId + @"-" + deviceType;

                case CacheType.mFicientHttpResponse:
                    if (string.IsNullOrEmpty(enterpriseId) || string.IsNullOrEmpty(user) || string.IsNullOrEmpty(sessionId) || string.IsNullOrEmpty(requestId)) return null;
                    return ((int)cacheType).ToString() + @"-" + enterpriseId + @"-" + user + @"-" + sessionId + @"-" + requestId;

                case CacheType.CompanyTimezone:
                    if (string.IsNullOrEmpty(enterpriseId)) return null;
                    return ((int)cacheType).ToString() + @"-" + enterpriseId;

                case CacheType.mFicientEmail:
                    if (string.IsNullOrEmpty(requestId)) return null;
                    return ((int)cacheType).ToString() + @"-" + requestId;

                case CacheType.mFicientDSPaging:
                    if (string.IsNullOrEmpty(enterpriseId) ||
                        string.IsNullOrEmpty(deviceId) ||
                        string.IsNullOrEmpty(deviceType) ||
                        string.IsNullOrEmpty(datasetId)) return null;

                    return ((int)cacheType).ToString() +
                    @"-" + enterpriseId + @"-" +
                    deviceId + @"-" + deviceType + @"-" + datasetId;
            }

            return null;
        }

        public static int SaveInCacheDatabase<T>(string key, object value, string cacheExpiryType, long cacheExpiryTime)
        {

            SqlCommand sqlCommand = new SqlCommand("dbo.INSERT_IN_CACHE");
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.AddWithValue("@CACHE_KEY", key.ToUpper());
            sqlCommand.Parameters.AddWithValue("@CACHE_VALUE", SerializeObject<T>(value));
            sqlCommand.Parameters.AddWithValue("@CACHE_CONTAINER", GetMd5Hash(AppDomain.CurrentDomain.GetHashCode().ToString() + HttpRuntime.Cache.GetHashCode().ToString()));
            sqlCommand.Parameters.AddWithValue("@ADD_TIME", DateTime.UtcNow.Ticks);
            sqlCommand.Parameters.AddWithValue("@EXPIRY_TIME", cacheExpiryTime);
            sqlCommand.Parameters.AddWithValue("@CACHE_TYPE", cacheExpiryType.ToUpper());

            return MSSqlDatabaseClient.ExecuteNonQueryRecord(sqlCommand, ConfigurationManager.ConnectionStrings["CacheConString"].ConnectionString);
        }

        private static DatabaseCacheRecord<T> GetFromCacheDatabase<T>(string key)
        {
            SqlCommand sqlCommand = new SqlCommand("SELECT [CACHE_KEY],[CACHE_VALUE],[CACHE_CONTAINER],[ADD_TIME],[UPDATE_TIME],[CACHE_TYPE],[EXPIRY_TIME] FROM [TBL_CACHE] WHERE CACHE_KEY=@CACHE_KEY ");
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.Parameters.AddWithValue("@CACHE_KEY", key.ToUpper());
            string strCon = ConfigurationManager.ConnectionStrings["CacheConString"].ConnectionString;
            DataSet dataValue = MSSqlDatabaseClient.SelectDataFromSQlCommand(strCon, sqlCommand);

            if (dataValue != null && dataValue.Tables.Count > 0 && dataValue.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToInt64(dataValue.Tables[0].Rows[0]["EXPIRY_TIME"].ToString()) > DateTime.UtcNow.Ticks)
                {
                    long UPDATE_TIME = Convert.ToInt64(dataValue.Tables[0].Rows[0]["UPDATE_TIME"].ToString());
                    if (UPDATE_TIME == 0) UPDATE_TIME = DateTime.UtcNow.Ticks;
                    return new DatabaseCacheRecord<T>(dataValue.Tables[0].Rows[0]["CACHE_KEY"].ToString(), dataValue.Tables[0].Rows[0]["CACHE_VALUE"].ToString(),
                    dataValue.Tables[0].Rows[0]["CACHE_TYPE"].ToString(), UPDATE_TIME,
                    Convert.ToInt64(dataValue.Tables[0].Rows[0]["EXPIRY_TIME"].ToString()));
                }
                else
                {
                    RemoveInCacheDatabaseWithoutContainer(key);
                }
            }

            return null;
        }

        private static void RemoveInCacheDatabase(string key)
        {
            SqlCommand sqlCommand = new SqlCommand("DELETE FROM TBL_CACHE WHERE CACHE_KEY=@CACHE_KEY AND CACHE_CONTAINER = @CACHE_CONTAINER");
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.Parameters.AddWithValue("@CACHE_KEY", key.ToUpper());
            sqlCommand.Parameters.AddWithValue("@CACHE_CONTAINER", GetMd5Hash(AppDomain.CurrentDomain.GetHashCode().ToString() + HttpRuntime.Cache.GetHashCode().ToString()));
            MSSqlDatabaseClient.ExecuteNonQueryRecord(sqlCommand, ConfigurationManager.ConnectionStrings["CacheConString"].ConnectionString);
        }
        private static void RemoveInCacheDatabaseWithoutContainer(string key)
        {
            SqlCommand sqlCommand = new SqlCommand("DELETE FROM TBL_CACHE WHERE CACHE_KEY=@CACHE_KEY");
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.Parameters.AddWithValue("@CACHE_KEY", key.ToUpper());
            MSSqlDatabaseClient.ExecuteNonQueryRecord(sqlCommand, ConfigurationManager.ConnectionStrings["CacheConString"].ConnectionString);
        }

        private static string GetMd5Hash(string _inputString)
        {
            byte[] bs = Encoding.UTF8.GetBytes(_inputString);
            MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
            byte[] hashedDataBytes = md5Hasher.ComputeHash(bs);
            return BitConverter.ToString(hashedDataBytes).Replace("-", "");
        }

        private static string SerializeObject<T>(object obj)
        {
            if (typeof(T) == typeof(string)) return obj.ToString();

            StringBuilder builder = new StringBuilder();
            XmlSerializer xml = new XmlSerializer(typeof(T));
            StringWriter writer = new StringWriter(builder);
            xml.Serialize(writer, (T)obj);
            return builder.ToString();
        }

        private static T DeserializeToObject<T>(string serializedString)
        {
            try
            {
                TextReader objStringsTextReader = new StringReader(serializedString);
                XmlSerializer stringResourceSerializer = new XmlSerializer(typeof(T), string.Empty);
                return (T)stringResourceSerializer.Deserialize(objStringsTextReader);
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }

        private class DatabaseCacheRecord<T>
        {
            internal string Key;
            internal object Value;
            internal string CacheExpiryType;
            internal long UpdatedTime, ExpiryTime;

            internal DatabaseCacheRecord(string _key, string _value, string _cacheExpiryType, long _updatedTime, long _expiryTime)
            {
                Key = _key;
                CacheExpiryType = _cacheExpiryType;
                UpdatedTime = _updatedTime;
                ExpiryTime = _expiryTime;

                if (typeof(T) == typeof(string)) Value = _value;
                else Value = DeserializeToObject<T>(_value);

                if (typeof(T) == typeof(SessionUser))
                {
                    SessionUser session = (SessionUser)Value;
                    session.LastActivityDateTime = _updatedTime;
                    Value = session;
                }
            }
        }
    }
}
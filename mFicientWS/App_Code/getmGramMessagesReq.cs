﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class getmGramMessagesReq
    {
        string _requestId, _companyId, _userName;
        int _functionCode;
        long _last_Date_time;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string UserName
        {
            get { return _userName; }
        }
        public long Last_Date_time
        {
            get { return _last_Date_time; }
        }
        
        public getmGramMessagesReq(string requestJson)
        {
            RequestJsonParsing<getmGramMessagesReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<getmGramMessagesReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.MGRAM_MESSAGE_LIST_GET)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.data.enid;
            _userName = objRequestJsonParsing.req.data.unm;
            if (_userName.Contains("\\"))
            {
                _userName = _userName.Split('\\')[1];
            }
            _last_Date_time = Convert.ToInt64(objRequestJsonParsing.req.data.lstdt);
        }
    }

    [DataContract]
    public class getmGramMessagesReqData : Data
    {
        [DataMember]
        public string lstdt { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class MficientEntities
    {
    }
    public class MFECompanyAdmin
    {
        string _adminId, _emailId, _password, _firstname;

        string _lastName, _streetAddress1;

        string _streetAddress2, _cityName, _stateName;

        string _countryCode, _zip;

        long _dob, _registrationDatetime;

        string _gender, _middleName, _streetAddress3;

        string _mobileNo;

        bool _isTrial;

        string _resellerId;

        string _companyID;


        #region Public Properties
        public string Firstname
        {
            get { return _firstname; }
            set { _firstname = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public string EmailId
        {
            get { return _emailId; }
            set { _emailId = value; }
        }

        public string AdminId
        {
            get { return _adminId; }
            set { _adminId = value; }
        }

        public string StreetAddress1
        {
            get { return _streetAddress1; }
            set { _streetAddress1 = value; }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public string StateName
        {
            get { return _stateName; }
            set { _stateName = value; }
        }

        public string CityName
        {
            get { return _cityName; }
            set { _cityName = value; }
        }

        public string StreetAddress2
        {
            get { return _streetAddress2; }
            set { _streetAddress2 = value; }
        }
        public string Zip
        {
            get { return _zip; }
            set { _zip = value; }
        }

        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }

        public long RegistrationDatetime
        {
            get { return _registrationDatetime; }
            set { _registrationDatetime = value; }
        }

        public long Dob
        {
            get { return _dob; }
            set { _dob = value; }
        }

        public string StreetAddress3
        {
            get { return _streetAddress3; }
            set { _streetAddress3 = value; }
        }

        public string MiddleName
        {
            get { return _middleName; }
            set { _middleName = value; }
        }

        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }
        public string MobileNo
        {
            get { return _mobileNo; }
            set { _mobileNo = value; }
        }
        public bool IsTrial
        {
            get { return _isTrial; }
            set { _isTrial = value; }
        }
        public string ResellerId
        {
            get { return _resellerId; }
            set { _resellerId = value; }
        }

        public string CompanyID
        {
            get { return _companyID; }
            set { _companyID = value; }
        }
        #endregion
    }
    public class MFECompanyInfo
    {
        //string _companyId, _companyName, _registerationNo, _streetAddress1, _streetAddress2, _streetAddress3, _cityName, _stateName, _countryCode, _zip;
        //string _adminId, _logoImageName, _supportEmail, _supportContact, _resellerId, _enquiryId, _blockOrUnblockId, _autoLoging, _serverURL, _timezoneId;
        //long _updatedOn,_salesManagerApprovedDate;
        //bool SalesMangerApproved, _isBlocked;

        #region Public Properties
        public string RegisterationNo
        {
            get;
            set;
        }

        public string CompanyName
        {
            get;
            set;
        }

        public string CompanyId
        {
            get;
            set;
        }


        public string StreetAddress3
        {
            get;
            set;
        }

        public string StreetAddress2
        {
            get;
            set;
        }

        public string StreetAddress1
        {
            get;
            set;
        }


        public string Zip
        {
            get;
            set;
        }

        public string CountryCode
        {
            get;
            set;
        }

        public string StateName
        {
            get;
            set;
        }

        public string CityName
        {
            get;
            set;
        }


        public string SupportEmail
        {
            get;
            set;
        }

        public string LogoImageName
        {
            get;
            set;
        }

        public string AdminId
        {
            get;
            set;
        }


        public string EnquiryId
        {
            get;
            set;
        }

        public string ResellerId
        {
            get;
            set;
        }

        public string SupportContact
        {
            get;
            set;
        }


        public long UpdatedOn
        {
            get;
            set;
        }

        public bool SalesMangerApproved1
        {
            get;
            set;
        }

        public long SalesManagerApprovedDate
        {
            get;
            set;
        }

        public string TimezoneId
        {
            get;
            set;
        }
        public string BlockOrUnblockId
        {
            get;
            set;
        }
        public string AutoLogin
        {
            get;
            set;
        }
        public string ServerURL
        {
            get;
            set;
        }
        public bool IsBlocked
        {
            get;
            set;
        }
        #endregion
    }
    public class MFECurrentPlan
    {

        #region Public Properties
        public string NextMnthPlan
        {
            get;
            set;
        }

        public string ChargeType
        {
            get;
            set;
        }

        public string PlanCode
        {
            get;
            set;
        }

        public string CompanyId
        {
            get;
            set;
        }

        public int BalanceAmnt
        {
            get;
            set;
        }

        public int PushMsgPerMonth
        {
            get;
            set;
        }

        public int PushMsgPerDay
        {
            get;
            set;
        }

        public int MaxUser
        {
            get;
            set;
        }

        public int MaxWorkFlow
        {
            get;
            set;
        }

        public sbyte Feature10
        {
            get;
            set;
        }

        public sbyte Feature9
        {
            get;
            set;
        }

        public sbyte Feature8
        {
            get;
            set;
        }

        public sbyte Feature7
        {
            get;
            set;
        }

        public sbyte Feature6
        {
            get;
            set;
        }

        public sbyte Feature5
        {
            get;
            set;
        }

        public sbyte Feature4
        {
            get;
            set;
        }

        public sbyte Feature3
        {
            get;
            set;
        }

        public double Validity
        {
            get;
            set;
        }

        public double UserChargePerMonth
        {
            get;
            set;
        }

        public long NextMnthPlanLastUpdated
        {
            get;
            set;
        }

        public long PlanChangeDate
        {
            get;
            set;
        }

        public long PurchaseDate
        {
            get;
            set;
        }
        #endregion
    }
    public class MobileDetail
    {
        public string DeviceID
        {
            get;
            set;
        }
        public string DeviceType
        {
            get;
            set;
        }
        public string DevicePushNotificationId
        {
            get;
            set;
        }
    }
    public class MFEMobileUser
    {
        #region Public Properties
        public List<MobileDetail> MobileDetails
        {
            get;
            set;
        }

        public string Mobile
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public string EmailId
        {
            get;
            set;
        }

        public string UserId
        {
            get;
            set;
        }
        public string DomainId
        {
            get;
            set;
        }

        public string RequestedBy
        {
            get;
            set;
        }

        public string RegionId
        {
            get;
            set;
        }

        public string Username
        {
            get;
            set;
        }

        public string EmployeeNo
        {
            get;
            set;
        }

        public string DesignationId
        {
            get;
            set;
        }

        public string LocationId
        {
            get;
            set;
        }

        public string SubAdminId
        {
            get;
            set;
        }

        public string CompanyId
        {
            get;
            set;
        }
        public long UpdatedOn
        {
            get;
            set;
        }

        public long RegistrationDatetime
        {
            get;
            set;
        }

        public long Dob
        {
            get;
            set;
        }

        public bool DesktopMessenger
        {
            get;
            set;
        }

        public bool IsOfflineWorkAllowed
        {
            get;
            set;
        }

        public bool AllowMessenger
        {
            get;
            set;
        }

        public bool IsBlocked
        {
            get;
            set;
        }

        public bool IsActive
        {
            get;
            set;
        }

        #endregion
    }


    public class MFEDevice
    {
        public int DeviceSize
        {
            get;
            set;
        }
        public long RegistrationDate
        {
            get;
            set;
        }
        public string DevicePushMsgId
        {
            get;
            set;
        }

        public string OsVersion
        {
            get;
            set;
        }

        public string MficientKey
        {
            get;
            set;
        }
        public string DeviceModel
        {
            get;
            set;
        }

        public string SubAdminId
        {
            get;
            set;
        }

        public string DeviceId
        {
            get;
            set;
        }
        public string DeviceType
        {
            get;
            set;
        }

        public string CompanyId
        {
            get;
            set;
        }
        public string UserId
        {
            get;
            set;
        }
    }
    public class MFESubAdmin
    {

        #region Public Properties
        public string Email
        {
            get;
            set;
        }
        public bool PushMsgAllowed
        {
            get;
            set;
        }

        public bool IsBlocked
        {
            get;
            set;
        }

        public bool IsActive
        {
            get;
            set;
        }
        public string Password
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string FullName
        {
            get;
            set;
        }

        public string AdminId
        {
            get;
            set;
        }

        public string SubAdminId
        {
            get;
            set;
        }

        public string CompanyId
        {
            get;
            set;
        }
        #endregion
    }
    public class MFEPlanDetailCommon
    {
        #region Public Properties
        public string PlanName
        {
            get;
            set;
        }

        public string PlanId
        {
            get;
            set;
        }
        public string PlanCode
        {
            get;
            set;
        }
        public int PushMessagePerDay
        {
            get;
            set;
        }
        public int MaxWorkFlow
        {
            get;
            set;
        }
        public int PushMessagePerMonth
        {
            get;
            set;
        }
        #endregion
    }
    public class MFEPaidPlanDetail : MFEPlanDetailCommon
    {
        int _minUsers;
        bool _isEnabled;
        public int MinUsers
        {
            get { return _minUsers; }
            set { _minUsers = value; }
        }
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { _isEnabled = value; }
        }
    }
    public class MFETrialPlanDetail : MFEPlanDetailCommon
    {
        int _maxUsers;
        bool _isDefault;
        int _validMonths;

        public int ValidMonths
        {
            get { return _validMonths; }
            set { _validMonths = value; }
        }
        public int MaxUsers
        {
            get { return _maxUsers; }
            set { _maxUsers = value; }
        }
        public bool IsDefault
        {
            get { return _isDefault; }
            set { _isDefault = value; }
        }
    }

    public class MFEmficientServer
    {
        string _serverId, _serverName,
            _serverURL, _serverIpAddress;
        bool _isEnabled, _isDefault;
        #region Public Property
        public string ServerIpAddress
        {
            get { return _serverIpAddress; }
            set { _serverIpAddress = value; }
        }

        public string ServerURL
        {
            get { return _serverURL; }
            set { _serverURL = value; }
        }

        public string ServerName
        {
            get { return _serverName; }
            set { _serverName = value; }
        }

        public string ServerId
        {
            get { return _serverId; }
            set { _serverId = value; }
        }
        public bool IsDefault
        {
            get { return _isDefault; }
            set { _isDefault = value; }
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { _isEnabled = value; }
        }
        #endregion
    }
    public class MFEmPluginServer : MFEmficientServer
    {
        string _serverPort;
        #region Public Property
        public string ServerPort
        {
            get { return _serverPort; }
            set { _serverPort = value; }
        }
        #endregion
    }
    //public class MFEmBuzzServer : MFEmficientServer
    //{
    //    string _serverPort;
    //    #region Public Property
    //    public string ServerPort
    //    {
    //        get { return _serverPort; }
    //        set { _serverPort = value; }
    //    }
    //    #endregion
    //}
    public class MFEGroup
    {
        public MFEGroup()
        {
            this.CompanyId = "";
            this.GroupId = "";
            this.GroupName = "";
            this.SubadminId = "";
            this.CreatedOn = 0;
        }
        public string CompanyId
        {
            get;
            set;
        }
        public string GroupId
        {
            get;
            set;
        }
        public string GroupName
        {
            get;
            set;
        }
        public string SubadminId
        {
            get;
            set;
        }
        public long CreatedOn
        {
            get;
            set;
        }
    }
}
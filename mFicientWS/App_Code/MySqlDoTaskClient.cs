﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;

namespace mFicientWS
{
    public class MySqlDoTaskClient
    {
        public static void MySqlConnectionOpen(out MySqlConnection Conn)
        {
            try
            {
                Conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch (MySqlException mysqlEx)
            {
                Conn = null;
                if (mysqlEx.Message.Contains(@"Access denied") || mysqlEx.Number == 1045)
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                else
                    throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
            }
        }

        public static void MySqlConnectionOpen(out MySqlConnection Conn, string ConnectionString)
        {
            try
            {
                Conn = new MySqlConnection(ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch (MySqlException mysqlEx)
            {
                Conn = null;
                if (mysqlEx.Message.Contains(@"Access denied") || mysqlEx.Number == 1045)
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                else
                    throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
            }
        }

        public static void MySqlConnectionClose(MySqlConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open))
                {
                    Conn.Close();
                }
            }
            catch
            {
            }
        }

        public static DataSet SelectDataFromDoTask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _password)
        {
            MySqlConnection objMySqlConnection = null;
            DataSet ObjDataSet = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId, _userId, _password);
                dbcon.GetConnector();
                string strConString = "Server = " + dbcon.HostName + ";Initial Catalog=" + dbcon.DataBaseName + ";User Id=" + dbcon.UserId + ";Password =" + dbcon.Password;

                MySqlConnectionOpen(out objMySqlConnection, strConString);

                MySqlCommand _MySqlCommand = new MySqlCommand(db_cmd.SqlQuery, objMySqlConnection);

                foreach (QueryParameters parameters in LstParameters)
                {
                    _MySqlCommand.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                ObjDataSet = new DataSet();

                MySqlDataAdapter objSqlDataAdapter = new MySqlDataAdapter(_MySqlCommand);

                objSqlDataAdapter.Fill(ObjDataSet);
            }
            catch (MySqlException mySqlEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                MySqlConnectionClose(objMySqlConnection);
            }

            return ObjDataSet;
        }

        public static int ExecuteNonQueryRecordDotask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _password)
        {
            MySqlConnection objMySqlConnection = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId, _userId, _password);
                dbcon.GetConnector();

                MySqlConnectionOpen(out objMySqlConnection, dbcon.ConnectionString);

                MySqlCommand _MySqlCommand = new MySqlCommand(db_cmd.SqlQuery, objMySqlConnection);

                foreach (QueryParameters parameters in LstParameters)
                {
                    _MySqlCommand.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                int i = _MySqlCommand.ExecuteNonQuery();

                return i;
            }
            catch (MySqlException mySqlEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                MySqlConnectionClose(objMySqlConnection);
            }
            //return 0;
        }

        public static string ExecuteScalarDotask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _password)
        {
            MySqlConnection objMySqlConnection = null; ;
            string strResult = "";
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId,  _userId,  _password);
                dbcon.GetConnector();

                MySqlConnectionOpen(out objMySqlConnection, dbcon.ConnectionString);

                MySqlCommand _MySqlCommand = new MySqlCommand(db_cmd.SqlQuery, objMySqlConnection);

                foreach (QueryParameters parameters in LstParameters)
                {
                    _MySqlCommand.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                object obj = _MySqlCommand.ExecuteScalar();
                if (obj != null)
                    strResult = obj.ToString();
            }
            catch (MySqlException mySqlEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                MySqlConnectionClose(objMySqlConnection);
            }

            return strResult;
        }

        public static DataSet ExecuteStoredProcedureDoTask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _password)
        {
            MySqlConnection objMySqlConnection = null;
            DataSet ObjDataSet = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId,  _userId,  _password);
                dbcon.GetConnector();
                string strConString = "Server = " + dbcon.HostName + ";Initial Catalog=" + dbcon.DataBaseName + ";User Id=" + dbcon.UserId + ";Password =" + dbcon.Password;

                MySqlConnectionOpen(out objMySqlConnection, strConString);

                MySqlCommand _MySqlCommand = new MySqlCommand();
                _MySqlCommand.Connection = objMySqlConnection;
                _MySqlCommand.CommandType = CommandType.StoredProcedure;
                _MySqlCommand.CommandText = db_cmd.SqlQuery;

                foreach (QueryParameters parameters in LstParameters)
                {
                    _MySqlCommand.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                ObjDataSet = new DataSet();

                MySqlDataAdapter objSqlDataAdapter = new MySqlDataAdapter(_MySqlCommand);

                objSqlDataAdapter.Fill(ObjDataSet);
            }
            catch (MySqlException mySqlEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                MySqlConnectionClose(objMySqlConnection);
            }

            return ObjDataSet;
        }
    }
}
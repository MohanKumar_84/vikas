﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class DeviceRegistrationRequestResp
    {
        ResponseStatus _respStatus;
        string _requestId;

        public DeviceRegistrationRequestResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Device Registration Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            return Utilities.getCommonResponseJson(FUNCTION_CODES.DEVICE_REGISTRATION_REQUEST, _requestId,_respStatus).ToString();
            //DeviceRegistrationResponse objDeviceRegistrationResp = new DeviceRegistrationResponse();
            //objDeviceRegistrationResp.func = Convert.ToString((int)FUNCTION_CODES.DEVICE_REGISTRATION_REQUEST);
            //objDeviceRegistrationResp.rid = _requestId;
            //objDeviceRegistrationResp.status = _respStatus;
            //string strJsonWithDetails = Utilities.SerializeJson<DeviceRegistrationResponse>(objDeviceRegistrationResp);
            //return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class DeviceRegistrationResponse : CommonResponse
    {
        public DeviceRegistrationResponse()
        { }
    }
}
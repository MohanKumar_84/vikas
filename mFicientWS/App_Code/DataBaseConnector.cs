﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Npgsql;

namespace mFicientWS
{
    public class DataBaseConnector
    {
        #region Private Members

        private string enterpriseId, connectorId, connectionName, hostName, userId, password, databaseName, addString, connectionString, mPluginAgentName;
        private DatabaseType databaseType;
        private int timeout;

        #endregion

        #region Constructor

        internal DataBaseConnector(string _connectorId, string _enterpriseId, string _userId, string _Password)
        {
            connectorId = _connectorId;
            enterpriseId = _enterpriseId;
            userId = _userId;
            password = _Password;
        }

        #endregion

        #region Public Properties

        public string EnterpriseId
        {
            get
            {
                return enterpriseId;
            }
        }

        public string ConnectorId
        {
            get
            {
                return connectorId;
            }
        }

        public string ConnectionName
        {
            get
            {
                return connectionName;
            }
        }

        public string HostName
        {
            get
            {
                return hostName;
            }
        }

        public string DataBaseName
        {
            get
            {
                return databaseName;
            }
        }

        public string UserId
        {
            get
            {
                return userId;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
        }

        public string MpluginAgentName
        {
            get
            {
                return mPluginAgentName;
            }
        }

        public int Timeout
        {
            get
            {
                return timeout;
            }
        }

        public string AdditionalString
        {
            get
            {
                return addString;
            }
        }

        public DatabaseType DataBaseType
        {
            get
            {
                return databaseType;
            }
        }

        public string ConnectionString
        {
            get
            {
                return connectionString;
            }
        }

        #endregion

        #region Public Methods

        public void GetConnector()
        {
            try
            {
                string strQuery = @"SELECT * FROM TBL_DATABASE_CONNECTION  as dbcon left join TBL_MPLUGIN_AGENT_DETAIL as mplugin on dbcon.COMPANY_ID = mplugin.COMPANY_ID 
                                    AND mplugin.MP_AGENT_ID= dbcon.MPLUGIN_AGENT WHERE dbcon.COMPANY_ID = @COMPANY_ID AND dbcon.DB_CONNECTOR_ID = @DB_CONNECTOR_ID";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);
                cmd.Parameters.AddWithValue("@DB_CONNECTOR_ID", connectorId);
                DataSet ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    connectionName = ds.Tables[0].Rows[0]["CONNECTION_NAME"].ToString();
                    databaseName =EncryptionDecryption.AESDecrypt(this.enterpriseId.ToLower(), ds.Tables[0].Rows[0]["DATABASE_NAME"].ToString());//decrypt string
                    mPluginAgentName = ds.Tables[0].Rows[0]["MPLUGIN_AGENT"].ToString();
                    hostName = ds.Tables[0].Rows[0]["HOST_NAME"].ToString();                   
                    addString = ds.Tables[0].Rows[0]["ADDITIONAL_STRING"].ToString();
                    timeout = Convert.ToInt32(ds.Tables[0].Rows[0]["TIME_OUT"].ToString());
                    databaseType = (DatabaseType)Convert.ToInt32(ds.Tables[0].Rows[0]["DATABASE_TYPE"].ToString());

                    getConnectionString(userId, password);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion

        #region Private Methods

        public void getConnectionString(string _userId,string _password)
        {
            switch (databaseType)
            {
                case DatabaseType.MSSQL:
                    SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
                    connectionStringBuilder.DataSource = hostName;
                    connectionStringBuilder.InitialCatalog = databaseName;
                    connectionStringBuilder.UserID = _userId;
                    connectionStringBuilder.Password = _password;
                    if (Convert.ToInt32(timeout) > 0)
                        connectionStringBuilder.ConnectTimeout = Convert.ToInt32(timeout);

                    if (addString.Length > 0)
                        connectionString = connectionStringBuilder.ToString() + addString + ";";
                    else
                        connectionString = connectionStringBuilder.ToString();
                    break;
                case DatabaseType.MYSQL:
                    if (!string.IsNullOrEmpty(addString))
                        connectionString = "SERVER=" + hostName + ";" + "DATABASE=" + databaseName + ";" + "UID=" + userId + ";" + "PASSWORD=" + password + ";" + addString + ";";
                    else
                        connectionString = "SERVER=" + hostName + ";" + "DATABASE=" + databaseName + ";" + "UID=" + userId + ";" + "PASSWORD=" + password + ";";
                    break;
                case DatabaseType.ORACLE:
                    break;

                case DatabaseType.POSTGRESQL:
                    NpgsqlConnectionStringBuilder pgsqlConnectionString = new NpgsqlConnectionStringBuilder();
                    pgsqlConnectionString.Host = hostName;
                    pgsqlConnectionString.Database = databaseName;
                    pgsqlConnectionString.UserName = userId;
                    pgsqlConnectionString.Password = password;
                    if (Convert.ToInt32(timeout) > 0)
                        pgsqlConnectionString.CommandTimeout = Convert.ToInt32(timeout);

                    if (addString.Length > 0)
                        connectionString = pgsqlConnectionString.ToString() + addString + ";";
                    else
                        connectionString = pgsqlConnectionString.ToString();
                    break;
            }
        }

        #endregion
    }
}
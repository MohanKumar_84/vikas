﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class getmGramMessagesResp
    {
        ResponseStatus _respStatus;
        List<MgramMessage> _msgs;
        string _requestId;
        public getmGramMessagesResp(ResponseStatus respStatus, List<MgramMessage> msgs, string requestId)
        {
            _respStatus = respStatus;
            _msgs = msgs;
            _requestId = requestId;
        }
        public string GetResponseJson()
        {
            getmGramMessagesResponse objgetmGramMessagesResp = new getmGramMessagesResponse();
            objgetmGramMessagesResp.func = Convert.ToString((int)FUNCTION_CODES.MGRAM_MESSAGE_LIST_GET);
            objgetmGramMessagesResp.rid = _requestId;
            objgetmGramMessagesResp.status = _respStatus;
            objgetmGramMessagesResp.data = new getmGramMessagesRespData();
            objgetmGramMessagesResp.data.msgs = _msgs;
            string strJsonWithDetails = Utilities.SerializeJson<getmGramMessagesResponse>(objgetmGramMessagesResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class getmGramMessagesResponse : CommonResponse
    {
        public getmGramMessagesResponse()
        { }
        //[DataMember]
        //public List<FormListResponseData> data { get; set; }
        [DataMember]
        public getmGramMessagesRespData data { get; set; }
    }

    public class getmGramMessagesRespData
    {
        /// <summary>
        /// Menu Category Details
        /// </summary>
        [DataMember]
        public List<MgramMessage> msgs { get; set; }
    }
    public class MgramMessage
    {
        [DataMember]
        public string msgid { get; set; }
        [DataMember]
        public string cat { get; set; }
        [DataMember]
        public string msg { get; set; }
        [DataMember]
        public string sts { get; set; }
        [DataMember]
        public string dttm { get; set; }
        [DataMember]
        public string edt { get; set; }
    }
}
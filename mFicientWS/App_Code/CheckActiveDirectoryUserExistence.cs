﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class CheckActiveDirectoryUserExistence
    {
        CheckActiveDirectoryUserExistenceReq _checkActiveDirectoryUserExistenceReq;

        public CheckActiveDirectoryUserExistence(CheckActiveDirectoryUserExistenceReq checkActiveDirectoryUserExistenceReq)
        {
            _checkActiveDirectoryUserExistenceReq = checkActiveDirectoryUserExistenceReq;
        }

        public CheckActiveDirectoryUserExistenceResp Process()
        {
            if (String.IsNullOrEmpty(_checkActiveDirectoryUserExistenceReq.RequestId)
                || String.IsNullOrEmpty(_checkActiveDirectoryUserExistenceReq.CompanyId)
                || String.IsNullOrEmpty(_checkActiveDirectoryUserExistenceReq.AgentName)
                || String.IsNullOrEmpty(_checkActiveDirectoryUserExistenceReq.AgentPassword)
                || String.IsNullOrEmpty(_checkActiveDirectoryUserExistenceReq.DomainName)
                || String.IsNullOrEmpty(_checkActiveDirectoryUserExistenceReq.Username))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
            CheckActiveDirectoryUserExistenceField objActiveDirUserFields = new CheckActiveDirectoryUserExistenceField();
            try
            {
                bool blnDoesExist = false;
                ActiveDirectoryUser objActvDirUser = mPlugin.ActiveDirectoryUserExistence(_checkActiveDirectoryUserExistenceReq.CompanyId,
                    _checkActiveDirectoryUserExistenceReq.AgentName,
                    _checkActiveDirectoryUserExistenceReq.AgentPassword,
                    _checkActiveDirectoryUserExistenceReq.DomainName,
                    _checkActiveDirectoryUserExistenceReq.Username,
                    out blnDoesExist
                    );
                objActiveDirUserFields.result = blnDoesExist;
                objActiveDirUserFields.user = objActvDirUser;
                return new CheckActiveDirectoryUserExistenceResp(objActiveDirUserFields, "0", "");
            }
            catch (mPlugin.mPluginException e)
            {
                objActiveDirUserFields.result = false;
                return new CheckActiveDirectoryUserExistenceResp(objActiveDirUserFields, "99946", e.Message);
            }
            catch (HttpException e)
            {
                throw (new Exception(Convert.ToString(e.GetHttpCode())));
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
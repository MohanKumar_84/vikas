﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class QueryDetailWithComments
    {

       QueryDetailWithCommentsReq objReq;
         public QueryDetailWithComments(QueryDetailWithCommentsReq _objReq)
        {
            objReq = _objReq;
        }

        public QueryDetailWithCommentsResp Process()
        {
            ResponseStatus _respStatus=new ResponseStatus();
            _respStatus.cd="0";
            _respStatus.desc="";
            SqlCommand cmd = new SqlCommand("SELECT QUERY,POSTED_ON,RESOLVED_ON,T.QUERY_TYPE,T.QUERY_CODE,TOKEN_NO,RESOLVED_BY,ISNULL(S.FULL_NAME,'') AS USER_FULL_NAME FROM ADMIN_TBL_QUERY Q "+
                " INNER JOIN ADMIN_TBL_QUERY_TYPE T ON Q.QUERY_TYPE=T.QUERY_CODE "+
                " LEFT OUTER JOIN TBL_SUB_ADMIN S ON Q.RESOLVED_BY=S.SUBADMIN_ID AND Q.IS_RESOLVED='TRUE' WHERE TOKEN_NO=@TOKEN_NO;" +
                " SELECT * FROM (SELECT COMMENT,COMMENTED_ON,TOKEN_NO,'MFICIENT SUPPORT' AS COMMENTBY FROM ADMIN_TBL_QUERY_THREAD WHERE USER_TYPE=3 AND TOKEN_NO=@TOKEN_NO" +
                " UNION SELECT COMMENT,COMMENTED_ON,TOKEN_NO,S.FULL_NAME AS  COMMENTBY FROM ADMIN_TBL_QUERY_THREAD T INNER JOIN TBL_SUB_ADMIN S ON T.COMMENT_BY=S.SUBADMIN_ID  WHERE USER_TYPE=2 AND TOKEN_NO=@TOKEN_NO" +
                " UNION SELECT COMMENT,COMMENTED_ON,TOKEN_NO,'ME' AS  COMMENTBY FROM ADMIN_TBL_QUERY_THREAD WHERE USER_TYPE=1 AND TOKEN_NO=@TOKEN_NO ) AS TBL_COMMENT ORDER BY COMMENTED_ON DESC;");

            cmd.Parameters.AddWithValue("@TOKEN_NO", objReq.TokenNo);

            DataSet ds=MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (ds.Tables[0].Rows.Count <= 0)
                throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());

            Querydeatil objectQry = new Querydeatil();
            CommentList cl;
            foreach (DataRow dr in ds.Tables[0].Rows)
            {////TOKEN_NO,QUERY_TYPE,QUERY_SUBJECT,RESOLVED_On
                objectQry.qury = Convert.ToString(dr["QUERY"]);
                objectQry.pon = Convert.ToString(dr["POSTED_ON"]);
                objectQry.qtyp = Convert.ToString(dr["QUERY_TYPE"]);
                objectQry.qcd = Convert.ToString(dr["QUERY_CODE"]);
                if (Convert.ToInt64(dr["RESOLVED_ON"]) > 0)
                {
                    objectQry.ron = Convert.ToString(dr["RESOLVED_ON"]);
                    if(string.IsNullOrEmpty(Convert.ToString(dr["USER_FULL_NAME"])))
                        objectQry.rby = "Me";
                    else
                        objectQry.rby = Convert.ToString(dr["USER_FULL_NAME"]);
                }
                else
                {
                    objectQry.ron = "0";
                    objectQry.rby = "";
                }
                //POSTED_ON
            }
            List<CommentList> lstcom=new List<CommentList>();
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                cl = new CommentList();
                cl.cmt = Convert.ToString(dr["COMMENT"]);
                cl.cdt = Convert.ToString(dr["COMMENTED_ON"]);
                cl.cby = Convert.ToString(dr["Commentby"]);
                lstcom.Add(cl);
            }
            objectQry.clst=lstcom;

            return new QueryDetailWithCommentsResp(_respStatus, objectQry, objReq.RequestId);
        }
        
    }
}
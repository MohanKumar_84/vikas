﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class LogOutResp
    {
        ResponseStatus _respStatus;
        string _requestId;
        public LogOutResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Log Out Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            LogOutResponse objLogOutResp = new LogOutResponse();
            objLogOutResp.func = Convert.ToString((int)FUNCTION_CODES.LOG_OUT);
            objLogOutResp.rid = _requestId;
            objLogOutResp.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<LogOutResponse>(objLogOutResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class LogOutResponse : CommonResponse
    {
        public LogOutResponse()
        { }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class ResponseofContactRequestReq
    {
        string _requestId, _newContactName, _responseStatus, _companyId, _userName;
        int _functionCode;

        public string ContactName
        {
            get { return _newContactName; }
        }
        public string ResponseStatus
        {
            get { return _responseStatus; }
        }
        public string UserName
        {
            get { return _userName; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }


        public ResponseofContactRequestReq(string requestJson, string userID)
        {
            RequestJsonParsing<ResponseofContactRequestReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<ResponseofContactRequestReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.ADD_CONTACT_CONFIRMATION)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _newContactName = objRequestJsonParsing.req.data.cnctnm;
            _responseStatus = objRequestJsonParsing.req.data.status;
            _userName = objRequestJsonParsing.req.data.unm;
            _companyId = objRequestJsonParsing.req.data.enid;
        }
    }

    [DataContract]
    public class ResponseofContactRequestReqData : Data
    {
        public ResponseofContactRequestReqData()
        { }
        /// <summary>
        /// New Contact ID
        /// </summary>
        [DataMember]
        public string cnctnm { get; set; }

        /// <summary>
        /// Response Status
        /// </summary>
        [DataMember]
        public string status { get; set; }
     
    }
}
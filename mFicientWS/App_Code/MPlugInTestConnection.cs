﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net;
namespace mFicientWS
{
    public class MPluginTestConnection
    {
        MPluginTestConnectionReq _mPlugInTestConnectionReq;
        public MPluginTestConnection(MPluginTestConnectionReq testConnectionReq)
        {
            _mPlugInTestConnectionReq = testConnectionReq;
        }
        public MPluginTestConnectionResp Process()
        {
            if (String.IsNullOrEmpty(_mPlugInTestConnectionReq.RequestId) || String.IsNullOrEmpty(_mPlugInTestConnectionReq.RequestType)
                || String.IsNullOrEmpty(_mPlugInTestConnectionReq.ConnectionString)
                || String.IsNullOrEmpty(_mPlugInTestConnectionReq.EnterpriseId))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
            if (_mPlugInTestConnectionReq.RequestType != "2")//DB Test//
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }

            try
            {
                //bool blnMPluginResponse = mPlugin.TestDatabaseConnection(_mPlugInTestConnectionReq.EnterpriseId, "Agent1", "E10ADC3949BA59ABBE56E057F20F883E", DatabaseType.MSSQL, _mPlugInTestConnectionReq.ConnectionString);
                bool blnMPluginResponse = mPlugin.TestDatabaseConnection(_mPlugInTestConnectionReq.EnterpriseId, _mPlugInTestConnectionReq.AgentId, _mPlugInTestConnectionReq.AgentPassword
                    , (DatabaseType)Enum.Parse(typeof(DatabaseType), _mPlugInTestConnectionReq.DatabaseType), _mPlugInTestConnectionReq.ConnectionString);
                return new MPluginTestConnectionResp(blnMPluginResponse.ToString());
            }
            catch (HttpException ex)
            {
                throw ex;
            }
            catch (Exception e)
            {
                throw e;
            }

            //DataSet dsMPlugInAuthenticationDtl = getMPlugInAuthenticationDetails();
            //if (dsMPlugInAuthenticationDtl != null)
            //{
            //    if (!(dsMPlugInAuthenticationDtl.Tables[0].Rows.Count > 0) || !(Utilities.GetMd5Hash(dsMPlugInAuthenticationDtl.Tables[0].Rows[0][0].ToString()) == _mPlugInTestConnectionReq.MplugInPassword))
            //    {
            //        throw new Exception(((int)HttpStatusCode.Unauthorized).ToString());
            //    }
            //}
            //else
            //{
            //    throw new Exception();
            //}
           // DataSet dsMPlugInServerDtls = Utilities.getMPlugInServerDetail(_mPlugInTestConnectionReq.EnterpriseId);
            //if (dsMPlugInServerDtls != null)
            //{
            //    if (dsMPlugInServerDtls.Tables[0].Rows.Count > 0)
            //    {
            //        string strHttpUrl = Convert.ToString(dsMPlugInServerDtls.Tables[0].Rows[0]["MP_HTTP_URL"]);
            //        if (String.IsNullOrEmpty(strHttpUrl))
            //        {
            //            throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            //        }
            //        else
            //        {
            //            string strCompleteRequestURL = "https://" + strHttpUrl + "?d=" + Utilities.UrlEncode(_mPlugInTestConnectionReq.RequestJson);
            //            string strResponse = Utilities.callMPlugIn(strCompleteRequestURL);
            //            if (String.IsNullOrEmpty(strResponse))
            //            {
            //                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            //            }
            //            else
            //            {
            //                return new MPluginTestConnectionResp(strResponse);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            //    }
            //}
            //else
            //{
            //    throw new Exception();
            //}
            
        }

        //        DataSet getMPlugInServerDetail()
        //        {
        //            string strQuery = @"SELECT *  FROM ADMIN_TBL_MST_MP_SERVER AS MPServer
        //                                LEFT OUTER JOIN ADMIN_TBL_SERVER_MAPPING AS ServerMap
        //                                ON MPServer.MP_SERVER_ID = ServerMap.MPLUGIN_SERVER_ID
        //                                WHERE ServerMap.COMPANY_ID = @CompanyId;";
        //            SqlCommand cmd = new SqlCommand(strQuery);
        //            cmd.CommandType = CommandType.Text;
        //            cmd.Parameters.AddWithValue("@CompanyId", _mPlugInTestConnectionReq.EnterpriseId);
        //            return MSSqlClient.SelectDataFromSQlCommand(cmd);
        //        }

        //string callMPlugIn(string webServiceURL)
        //{
        //    HTTP oHttp = new HTTP(webServiceURL);

        //    oHttp.HttpRequestMethod = HTTP.EnumHttpMethod.HTTP_GET;
        //    oHttp.UserAgent = Utilities.mPluginUserAgent;
        //    HttpResponseStatus oResponse = oHttp.Request();

        //    if (oResponse.StatusCode == HttpStatusCode.OK)
        //    {
        //        return oResponse.ResponseText;
        //    }
        //    else
        //    {
        //        return "";
        //    }

        //}

        DataSet getMPlugInAuthenticationDetails()
        {
            string strQuery = @"SELECT MPLUGIN_PASSWORD FROM TBL_ACCOUNT_SETTINGS WHERE COMPANY_ID = @CompanyId";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@CompanyId", _mPlugInTestConnectionReq.EnterpriseId);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class MPGetMetaDataReq
    {
        string _requestId, _companyId, _agentName, _agentPassword, _connectorId, _metaDataType, _tableName;
        string _queryType, _connectionUserId, _connectionPassword;

        public string QueryType
        {
            get { return _queryType; }
        }
        public string TableName
        {
            get { return _tableName; }
        }

        public string RequestId
        {
            get { return _requestId; }
        }

        public string MetaDataType
        {
            get { return _metaDataType; }
            
        }

        public string ConnectorId
        {
            get { return _connectorId; }
            
        }

        public string AgentPassword
        {
            get { return _agentPassword; }
            
        }

        public string AgentName
        {
            get { return _agentName; }
            
        }

        public string CompanyId
        {
            get { return _companyId; }
            
        }

        public string ConnectionUserId
        {
            get { return _connectionUserId; }

        }

        public string ConnectionPassword
        {
            get { return _connectionPassword; }

        }

        public MPGetMetaDataReq(string requestJson)
        {
            MPlugGetMetaDataReqJsonParsing objRequestJsonParsing = Utilities.DeserialiseJson<MPlugGetMetaDataReqJsonParsing>(requestJson);
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.eid;
            _agentName = objRequestJsonParsing.req.agtid;
            _agentPassword = objRequestJsonParsing.req.agtpwd;
            _connectorId = objRequestJsonParsing.req.connid;
            _metaDataType = objRequestJsonParsing.req.mttyp;
            _tableName = objRequestJsonParsing.req.tblnm;
            _queryType = objRequestJsonParsing.req.qrytp;
            _connectionUserId = EncryptionDecryption.AESDecrypt(this.CompanyId.ToLower(), objRequestJsonParsing.req.unm);
            _connectionPassword = EncryptionDecryption.AESDecrypt(this.CompanyId.ToLower(), objRequestJsonParsing.req.pwd);

            _connectionUserId = objRequestJsonParsing.req.unm;
            _connectionPassword =  objRequestJsonParsing.req.pwd;
        }
    }


    [DataContract]
    public class MPlugGetMetaDataReqJsonParsing
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public MPlugGetMetaDataReqReqFields req { get; set; }
    }

    [DataContract]
    public class MPlugGetMetaDataReqReqFields
    {

        /// <summary>
        /// request id
        /// </summary>
        [DataMember]
        public string rid { get; set; }

        /// <summary>
        ///company id
        /// </summary>
        [DataMember]
        public string eid { get; set; }

        [DataMember]
        public string agtid { get; set; }

        [DataMember]
        public string agtpwd { get; set; }
        [DataMember]
        public string connid { get; set; }
        [DataMember]
        public string mttyp { get; set; }
        [DataMember]
        public string tblnm { get; set; }
        /// <summary>
        /// Query Type
        /// </summary>
        [DataMember]
        public string qrytp { get; set; } /// <summary>
        /// Credential
        /// </summary>
        [DataMember]
        public string unm { get; set; }

        [DataMember]
        public string pwd { get; set; }
    }
}
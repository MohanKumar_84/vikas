﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class GetQueryType
    {
        GetQueryTypeReq objReq;
        public GetQueryType(GetQueryTypeReq _objReq)
        {
            objReq = _objReq;
        }

        public GetQueryTypeResp Process()
        {
            ResponseStatus _respStatus=new ResponseStatus();
            _respStatus.cd="0";
            _respStatus.desc="";
            SqlCommand cmd = new SqlCommand("SELECT * FROM ADMIN_TBL_QUERY_TYPE WHERE CATEGORY=1 ORDER BY QUERY_TYPE ");
            DataSet ds=MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            List<QueryTypeList> qlst = new List<QueryTypeList>();
            if (ds != null)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    QueryTypeList objectQry = new QueryTypeList();
                    objectQry.qtyp = Convert.ToString(dr["QUERY_TYPE"]);
                    objectQry.qcd = Convert.ToString(dr["QUERY_CODE"]);
                    qlst.Add(objectQry);
                }
            }
            return new GetQueryTypeResp(_respStatus, qlst, objReq.RequestId);
        }
    }
}
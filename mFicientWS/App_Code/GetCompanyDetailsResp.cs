﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class GetCompanyDetailsResp
    {
        ResponseStatus _respStatus;
        string _requestId;
        CompanyDtlsResponseData _objCompDtlsData;
        public GetCompanyDetailsResp(ResponseStatus respStatus, string requestId, CompanyDtlsResponseData objCompDtlsData)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
                _objCompDtlsData = objCompDtlsData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Company Details Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            CompanyDtlsResponse objCompDtlsResp = new CompanyDtlsResponse();
            objCompDtlsResp.func = Convert.ToString((int)FUNCTION_CODES.COMPANY_DETAIL);
            objCompDtlsResp.rid = _requestId;
            objCompDtlsResp.status = _respStatus;
            objCompDtlsResp.data = _objCompDtlsData;

            string strJsonWithDetails = Utilities.SerializeJson<CompanyDtlsResponse>(objCompDtlsResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class CompanyDtlsResponse : CommonResponse
    {
        public CompanyDtlsResponse()
        { }
        [DataMember]
        public CompanyDtlsResponseData data { get; set; }
    }

    public class CompanyDtlsResponseData
    {
      

        /// <summary>
        /// Company Name
        /// </summary>
        [DataMember]
        public string nm{ get; set; }

        

        /// <summary>
        /// Address
        /// </summary>
        [DataMember]
        public string add { get; set; }

        /// <summary>
        /// City
        /// </summary>
        [DataMember]
        public string cty { get; set; }

        /// <summary>
        /// State
        /// </summary>
        [DataMember]
        public string st { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        [DataMember]
        public string cnt { get; set; }

        /// <summary>
        /// Country Name
        /// </summary>
        [DataMember]
        public string cnm { get; set; }

        /// <summary>
        /// Zip
        /// </summary>
        [DataMember]
        public string zip { get; set; }

        

        /// <summary>
        ///Logo Name
        /// </summary>
        [DataMember]
        public string logo { get; set; }

        /// <summary>
        /// Support Email
        /// </summary>
        [DataMember]
        public string seml { get; set; }

        /// <summary>
        /// Support Contact
        /// </summary>
        [DataMember]
        public string spcon { get; set; }

        ///// <summary>
        ///// Company Id
        ///// </summary>
        //[DataMember]
        //public string enid { get; set; }

        ///// <summary>
        ///// Registration No
        ///// </summary>
        //[DataMember]
        //public string regno { get; set; }
    }
}
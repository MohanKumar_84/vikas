﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Data;

namespace mFicientWS
{
    public class UserAuthenticationResp
    {
        ResponseStatus _respStatus;
        string _requestId, _sessionId;
        List<UserData> _lstUsersDataDtls;

        public UserAuthenticationResp(ResponseStatus respStatus, string requestId, string sessionId, List<UserData> lstUsersDataDtls)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
                _sessionId = sessionId;
                _lstUsersDataDtls = lstUsersDataDtls;

            }
            catch (Exception ex)
            {
                throw new Exception("Error creating User Authentication Detail Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            UserAuthenticationResponse objUserAuthenticationDtlsResp = new UserAuthenticationResponse();
            objUserAuthenticationDtlsResp.func = Convert.ToString((int)FUNCTION_CODES.USER_AUTHENTICATION);
            objUserAuthenticationDtlsResp.rid = _requestId;
            objUserAuthenticationDtlsResp.status = _respStatus;
            UserAuthenticationResponseData objResponseData = new UserAuthenticationResponseData();
            
            objResponseData.sid = _sessionId;
            objResponseData.users = _lstUsersDataDtls;
            objUserAuthenticationDtlsResp.data = objResponseData;
            string strJsonWithDetails = Utilities.SerializeJson<UserAuthenticationResponse>(objUserAuthenticationDtlsResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class UserAuthenticationResponse : CommonResponse
    {
        public UserAuthenticationResponse()
        {
        }
        [DataMember]
        public UserAuthenticationResponseData data { get; set; }
    }
    public class UserAuthenticationResponseData
    {
        /// <summary>
        /// session id
        /// </summary>
        [DataMember]
        public string sid { get; set; } /// <summary>
                                        
        /// Users Data
        /// </summary>
        [DataMember]
        public List<UserData> users { get; set; }
    }

    public class Users
    {
        /// <summary>
        /// UserName
        /// </summary>
        [DataMember]
        public string unm { get; set; }
    }

    public class UserData
    {
        public UserData()
        {
        }
        /// <summary>
        /// UserName
        /// </summary>
        [DataMember]
        public string unm { get; set; }

        /// <summary>
        /// FullName
        /// </summary>
        [DataMember]
        public string fnm { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [DataMember]
        public string em { get; set; }

        /// <summary>
        /// Designation Name
        /// </summary>
        [DataMember]
        public string des { get; set; }

        /// <summary>
        /// Department Name
        /// </summary>
        [DataMember]
        public string dep { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [DataMember]
        public string status { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [DataMember]
        public string istop { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class CheckAccountAndDeviceResp
    {
        ResponseStatus _respStatus;
        string _requestId;

        public CheckAccountAndDeviceResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Device Registration Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            CheckAccountAndDeviceResponse objCheckAccountAndDeviceResp = new CheckAccountAndDeviceResponse();
            objCheckAccountAndDeviceResp.func = Convert.ToString((int)FUNCTION_CODES.CHECK_ACCOUNT_AND_DEVICE);
            objCheckAccountAndDeviceResp.rid = _requestId;
            objCheckAccountAndDeviceResp.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<CheckAccountAndDeviceResponse>(objCheckAccountAndDeviceResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class CheckAccountAndDeviceResponse : CommonResponse
    {
        public CheckAccountAndDeviceResponse()
        { }
    }
}
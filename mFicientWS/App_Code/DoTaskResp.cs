﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class DoTaskResp
    {
        public ResponseStatus _respStatus;
        string _requestId, _controlId, _datasetId, _totalPage, _cache, _cachefrequecy, _cacheCondition,_mplugin;
        public object _lstData;
        List<List<string>> _imageColumns;
        public DoTaskResp()
        { 
            this._respStatus = new ResponseStatus();
            this._respStatus.cd = "-1";
        }
        public DoTaskResp(ResponseStatus respStatus,
            string requestId,
            string datasetId,
            string totalPage,
            object lstData, string controlId,
            string cache,
            string cachefrequecy,
            string cacheCondition,
            string mplugin, List<List<string>> imageColumns
            )
        {
            try
            {
                this._respStatus = respStatus;
                this._requestId = requestId;
                this._lstData = lstData;
                this._controlId = controlId;
                this._datasetId = datasetId;
                this._totalPage = totalPage;
                this._cache = cache;
                this._cachefrequecy = cachefrequecy;
                this._cacheCondition = cacheCondition;
                this._mplugin = mplugin;
                this._imageColumns = imageColumns;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Form Details Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            string strJsonWithDetails = String.Empty;
            doTaskResponse<List<Seleced_cmd_Tables>> objCompleteTblSelectResp = new doTaskResponse<List<Seleced_cmd_Tables>>();
            objCompleteTblSelectResp.func = Convert.ToString((int)FUNCTION_CODES.DO_TASK);
            objCompleteTblSelectResp.rid = _requestId;
            objCompleteTblSelectResp.status = _respStatus;

            dotaskData<List<Seleced_cmd_Tables>> objCompleteTblSelectData = new dotaskData<List<Seleced_cmd_Tables>>();
            List<Seleced_cmd_Tables> tablelst = new List<Seleced_cmd_Tables>();
            DataSet dsTableLstData = (DataSet)_lstData;
            /**
              * Even if a dataset contains more than one table
              * we have to return only the data of 
              * the first table only.
              * **/
            if (dsTableLstData != null && dsTableLstData.Tables != null && dsTableLstData.Tables.Count > 0)
            {
                DataTable dt = dsTableLstData.Tables[0];
                foreach (DataColumn dc in dt.Columns)
                {
                    Seleced_cmd_Tables tbl = new Seleced_cmd_Tables();
                    tbl.cnm = dc.ColumnName;
                    //add column values
                    List<string> strList = new List<string>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        strList.Add(dr[dc.ColumnName].ToString());
                    }
                    tbl.val = strList;
                    tablelst.Add(tbl);
                }
            }
            objCompleteTblSelectData.dt = tablelst;
            objCompleteTblSelectData.ctrlid = this._controlId;
            objCompleteTblSelectData.dsid = this._datasetId;
            objCompleteTblSelectData.ttlpg = this._totalPage;
            objCompleteTblSelectData.cache = this._cache;
            objCompleteTblSelectData.efrq = this._cachefrequecy;
            objCompleteTblSelectData.econ = this._cacheCondition;
            objCompleteTblSelectData.rtfn = 0.ToString();
            objCompleteTblSelectResp.data = objCompleteTblSelectData;
            objCompleteTblSelectData.mpl = this._mplugin;
            foreach(List<string> icol in this._imageColumns)
                icol[1] =  Utilities.convertColumnImagetype(Convert.ToInt32(icol[1].ToString())).ToString();

            objCompleteTblSelectData.icol = this._imageColumns;
           
            strJsonWithDetails = Utilities.SerializeJson<doTaskResponse<List<Seleced_cmd_Tables>>>(objCompleteTblSelectResp);

            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }
    public class doTaskResponse<T> : CommonResponse
    {
        public doTaskResponse()
        { }
        [DataMember]
        public dotaskData<T> data { get; set; }
    }
    public class dotaskData<T>
    {
        [DataMember]
        public T dt { get; set; }

        [DataMember]
        public string ctrlid { get; set; }

        [DataMember]
        public string rtfn { get; set; }

        [DataMember]
        public string dsid { get; set; }

        [DataMember]
        public string ttlpg { get; set; }

        [DataMember]
        public string cache { get; set; }

        [DataMember]
        public string efrq { get; set; }

        [DataMember]
        public string econ { get; set; }

        [DataMember]
        public string mpl { get; set; }

        [DataMember]
        public List<List<string>> icol { get; set; }
    }
    public class FormControlData
    {
        [DataMember]
        public string t { get; set; }
    }
    public class Pie_ddl_Data
    {
        [DataMember]
        public string t { get; set; }
        [DataMember]
        public string v { get; set; }
    }
    public class Bar_Line_Data
    {
        [DataMember]
        public string t { get; set; }
        [DataMember]
        public string v1 { get; set; }
        [DataMember]
        public string v2 { get; set; }
        [DataMember]
        public string v3 { get; set; }
        [DataMember]
        public string v4 { get; set; }
        [DataMember]
        public string v5 { get; set; }
    }
    public class rptData
    {
        [DataMember]
        public string i { get; set; }
        [DataMember]
        public string b { get; set; }
        [DataMember]
        public string s { get; set; }
        [DataMember]
        public string r { get; set; }
        [DataMember]
        public string c { get; set; }
        [DataMember]
        public string n { get; set; }
        [DataMember]
        public string an { get; set; }
    }

    [DataContract]
    public class Seleced_cmd_Tables
    {
        [DataMember]
        public string cnm { get; set; }
        [DataMember]
        public List<string> val { get; set; }
    }
    #region Editable List Resp Fields
    public class EditableListRespFields
    {
        [DataMember]
        public List<EditabletListItem> rows { get; set; }
    }
    public class EditabletListItem
    {
        [DataMember]
        public List<EditableListItemFields> item { get; set; }
    }
    public class EditableListItemFields
    {
        [DataMember]
        public string pnam { get; set; }
        [DataMember]
        public string pval { get; set; }
    }
    #endregion
}
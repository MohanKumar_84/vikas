﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class DoTaskRespForBatchProcess
    {
        const string KEY_RPT_DATA = "rptdt",
                    KEY_BAR_LINE_DATA = "barlinedt",
                    KEY_DDL_PIE_DATA = "pieddldt",
                    KEY_FINAL_TAG = "dt",
                    KEY_TABLE_SELECTED_DATA = "tabledt",
                    KEY_TBL_MANIPULATION_DATA = "tblmanipulation";
        
        public ResponseStatus _respStatus;
        string _requestId;
        //int _return_fn;
        //object _lstData;
        List<RespDataForCommand> _respDataForCmd;
        public DoTaskRespForBatchProcess(ResponseStatus respStatus, string requestId, List<RespDataForCommand> respDataForCmd)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
                _respDataForCmd = respDataForCmd;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Form Details Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            DoTaskResponseNew objDoTaskResp = new DoTaskResponseNew();
            objDoTaskResp.data = new List<FinalCommandResponse>();
            List<object> lstFinalCmdResp = new List<object>();
            FinalCommandResponse objFinalCmdResp;
            foreach (RespDataForCommand objRespDataForCmd in _respDataForCmd)
            {
                objFinalCmdResp = new FinalCommandResponse();
                objFinalCmdResp.err = objRespDataForCmd.CmdErrorDtl;
                objFinalCmdResp.idx = objRespDataForCmd.ProcessId;
                objFinalCmdResp.ctrlid = objRespDataForCmd.ControlId;
                objFinalCmdResp.rtfn= objRespDataForCmd.ReturnFunction.ToString();
                if(objRespDataForCmd.LstData==null)
                    objFinalCmdResp.dt = new List<Seleced_cmd_Tables>();
                else
                    objFinalCmdResp.dt = (List<Seleced_cmd_Tables>)objRespDataForCmd.LstData;
                objDoTaskResp.data.Add(objFinalCmdResp);
            }

            objDoTaskResp.func = Convert.ToString((int)FUNCTION_CODES.DO_TASK_FOR_BATCH_PROCESS);
            objDoTaskResp.rid = _requestId;
            objDoTaskResp.status = _respStatus;
            //return jsonAfterReplacingNull(Utilities.getFinalJson(Utilities.SerializeJson<DoTaskResponseNew>(objDoTaskResp)));
            return Utilities.getFinalJson(Utilities.SerializeJson<DoTaskResponseNew>(objDoTaskResp));
        }

    //    string jsonAfterReplacingNull(string json)
    //    {
    //        object objJson = JSONParser.JsonDecode(json);
    //        System.Collections.ArrayList listData = ((System.Collections.ArrayList)(((System.Collections.Hashtable)((System.Collections.Hashtable)objJson)["resp"]))["data"]);
    //        object dtValue = null;
    //        string finalKeyToRemove = null;
    //        foreach (object obj in listData)
    //        {
    //            System.Collections.Hashtable hshData = ((System.Collections.Hashtable)obj);
    //            if (hshData[KEY_BAR_LINE_DATA] == null) hshData.Remove(KEY_BAR_LINE_DATA);
    //            else
    //            {
    //                dtValue = hshData[KEY_BAR_LINE_DATA];
    //                finalKeyToRemove = KEY_BAR_LINE_DATA;
    //            }

    //            if (hshData[KEY_DDL_PIE_DATA] == null) hshData.Remove(KEY_DDL_PIE_DATA);
    //            else
    //            {
    //                dtValue = hshData[KEY_DDL_PIE_DATA];
    //                finalKeyToRemove = KEY_BAR_LINE_DATA;
    //            }

    //            if (hshData[KEY_RPT_DATA] == null) hshData.Remove(KEY_RPT_DATA);
    //            else
    //            {
    //                dtValue = hshData[KEY_RPT_DATA];
    //                finalKeyToRemove = KEY_RPT_DATA;
    //            }

    //            if (hshData[KEY_TABLE_SELECTED_DATA] == null) hshData.Remove(KEY_TABLE_SELECTED_DATA);
    //            else
    //            {
    //                dtValue = hshData[KEY_TABLE_SELECTED_DATA];
    //                finalKeyToRemove = KEY_TABLE_SELECTED_DATA;
    //            }

    //            if (hshData[KEY_TBL_MANIPULATION_DATA] == null) hshData.Remove(KEY_TBL_MANIPULATION_DATA);
    //            else
    //            {
    //                dtValue =new System.Collections.ArrayList();//don't send any value for this
    //                finalKeyToRemove = KEY_TBL_MANIPULATION_DATA;
    //            }
    //            hshData.Add(KEY_FINAL_TAG, dtValue);
    //            if (!String.IsNullOrEmpty(finalKeyToRemove))
    //                hshData.Remove(finalKeyToRemove);
    //        }
    //        return JSONParser.JsonEncode(objJson);

    //    }
    }
    public class DoTaskResponseNew : CommonResponse
    {
        public DoTaskResponseNew()
        {

        }
        [DataMember]
        public List<FinalCommandResponse> data { get; set; }
    }
    //public class DotaskFinalDataNew<T>
    //{
    //    [DataMember]
    //    public List<T> dt { get; set; }
    //}
    public class FinalCommandResponse
    {

        /// <summary>
        /// Process Id
        /// </summary>
        [DataMember]
        public string idx { get; set; }

        /// <summary>
        /// Control Id
        /// </summary>
        [DataMember]
        public string ctrlid { get; set; }

        /// <summary>
        ///Return Function
        /// </summary>
        [DataMember]
        public string rtfn { get; set; }

        /// <summary>
        /// error code
        /// </summary>
        [DataMember]
        public CommandErrorDetail err { get; set; }

        /// <summary>
        /// Complete Table Details
        /// </summary>
        [DataMember]
        public List<Seleced_cmd_Tables> dt { get; set; }

        /// <summary>
        /// Insert Update Delete (DB Manipulation) Data
        /// </summary>
        //[DataMember]
        //public List<DbManipulationCmdData> tblmanipulation { get; set; }
    }

    //public class DbManipulationCmdData
    //{
    //    [DataMember]
    //    public string rtv { get; set; }
    //}
    public class CommandErrorDetail
    {
        /// <summary>
        /// Error Code
        /// </summary>
        [DataMember]
        public string cd { get; set; }

        /// <summary>
        /// Error Description
        /// </summary>
        [DataMember]
        public string desc { get; set; }
    }

    public class RespDataForCommand
    {
        string _commandId, _controlId, _ProcessId;
        int _returnFunction;
        object _lstData;
        CommandErrorDetail _cmdErrorDtl;
        bool _isRetrunTypeForSelectAll;

        public RespDataForCommand(string commandId, string controlId, string processId,
                                  object lstData, int returnFunction,
                                  CommandErrorDetail errorDtl,
                                  bool isReturnTypeForSelectAll
                                )//Select all and tblManipulation both has same return function coming from mobile end
        {
            this.CommandId = commandId;
            this.ControlId = controlId;
            ProcessId = processId;
            this.LstData = lstData;
            this.ReturnFunction = returnFunction;
            this.CmdErrorDtl = errorDtl;
            this.IsRetrunTypeForSelectAll = isReturnTypeForSelectAll;
        }
        public object LstData
        {
            get { return _lstData; }
            private set { _lstData = value; }
        }
        public string ControlId
        {
            get { return _controlId; }
            private set { _controlId = value; }
        }
        public string ProcessId
        {
            get { return _ProcessId; }
            private set { _ProcessId = value; }
        }
        public string CommandId
        {
            get { return _commandId; }
            private set { _commandId = value; }
        }
        public int ReturnFunction
        {
            get { return _returnFunction; }
            private set { _returnFunction = value; }
        }
        public CommandErrorDetail CmdErrorDtl
        {
            get { return _cmdErrorDtl; }
            private set { _cmdErrorDtl = value; }
        }

        public bool IsRetrunTypeForSelectAll
        {
            get { return _isRetrunTypeForSelectAll; }
            private set { _isRetrunTypeForSelectAll = value; }
        }

    }

}
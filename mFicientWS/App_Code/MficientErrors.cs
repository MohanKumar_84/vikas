﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public static class MFErrors
    {
        public enum AddDevicePushMsgIdError
        {
            RecordInsertError = 130061,
        }
        public enum CompanyRegistrationError
        {
            CompanyIdExists = 110051,
            ValidationError = 110052,
            AdminEmailIdExists = 110053,
        }
        public enum GetAllTimezonesError
        {
            NotFound = 110071,
        }
        public enum UserLoginErrors
        {
            UserDoesNotExist = 1300101,
        }
    }
    public interface ImFicientErrors
    {
        string getErrorMsgFromErrorCode(int errorCode);
        object getErrorEnumValue(int errorCode);
    }

    public class AddDevicePushMsgIdError : ImFicientErrors
    {
        public string getErrorMsgFromErrorCode(int errorCode)
        {
            string strErrorMsg = String.Empty;

            MFErrors.AddDevicePushMsgIdError eErrorCode =
                (MFErrors.AddDevicePushMsgIdError)this.getErrorEnumValue(errorCode);

            if (
                Enum.IsDefined(
                typeof(MFErrors.AddDevicePushMsgIdError),
                eErrorCode
                )
              )
            {
                switch (eErrorCode)
                {
                    case MFErrors.AddDevicePushMsgIdError.RecordInsertError:
                        strErrorMsg = "Record insertion error.";
                        break;
                }
            }
            else
            {
                throw new MficientException("Error code not a part of defined error code");
            }
            return strErrorMsg;
        }

        public object getErrorEnumValue(int errorCode)
        {
            return Enum.Parse(
                typeof(MFErrors.AddDevicePushMsgIdError),
                errorCode.ToString()
                );
        }
    }
    public class CompanyRegistrationError : ImFicientErrors
    {
        public string getErrorMsgFromErrorCode(int errorCode)
        {
            string strErrorMsg = String.Empty;

            MFErrors.CompanyRegistrationError eErrorCode =
                (MFErrors.CompanyRegistrationError)this.getErrorEnumValue(errorCode);

            if (
                Enum.IsDefined(
                typeof(MFErrors.CompanyRegistrationError),
                eErrorCode
                )
              )
            {
                switch (eErrorCode)
                {
                    case MFErrors.CompanyRegistrationError.CompanyIdExists:
                        strErrorMsg = "CompanyId already exist.";
                        break;
                    case MFErrors.CompanyRegistrationError.ValidationError:
                        break;
                }
            }
            else
            {
                throw new MficientException("Error code not a part of defined error code");
            }
            return strErrorMsg;
        }

        public object getErrorEnumValue(int errorCode)
        {
            return Enum.Parse(
                typeof(MFErrors.CompanyRegistrationError),
                errorCode.ToString()
                );
        }
    }
    public class GetAllTimezonesError : ImFicientErrors
    {
        public string getErrorMsgFromErrorCode(int errorCode)
        {
            string strErrorMsg = String.Empty;

            MFErrors.GetAllTimezonesError eErrorCode =
                (MFErrors.GetAllTimezonesError)this.getErrorEnumValue(errorCode);

            if (
                Enum.IsDefined(
                typeof(MFErrors.GetAllTimezonesError),
                eErrorCode
                )
              )
            {
                switch (eErrorCode)
                {
                    case MFErrors.GetAllTimezonesError.NotFound:
                        strErrorMsg = "Timezone ids not found.";
                        break;
                }
            }
            else
            {
                throw new MficientException("Error code not a part of defined error code");
            }
            return strErrorMsg;
        }

        public object getErrorEnumValue(int errorCode)
        {
            return Enum.Parse(
                typeof(MFErrors.GetAllTimezonesError),
                errorCode.ToString()
                );
        }
    }
    public class UserLoginErrors : ImFicientErrors
    {
        public string getErrorMsgFromErrorCode(int errorCode)
        {
            string strErrorMsg = String.Empty;

            MFErrors.UserLoginErrors eErrorCode =
                (MFErrors.UserLoginErrors)this.getErrorEnumValue(errorCode);

            if (
                Enum.IsDefined(
                typeof(MFErrors.UserLoginErrors),
                eErrorCode
                )
              )
            {
                switch (eErrorCode)
                {
                    case MFErrors.UserLoginErrors.UserDoesNotExist:
                        strErrorMsg = "Login failed.";
                        break;
                }
            }
            else
            {
                throw new MficientException("Error code not a part of defined error code");
            }
            return strErrorMsg;
        }

        public object getErrorEnumValue(int errorCode)
        {
            return Enum.Parse(
                typeof(MFErrors.UserLoginErrors),
                errorCode.ToString()
                );
        }
    }
}
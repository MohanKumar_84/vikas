﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class mBuzzCompanyUserListReq
    {
        string _requestId, _companyId;
        int _functionCode;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }


        public mBuzzCompanyUserListReq(string requestJson)
        {
            RequestJsonParsing<mBuzzCompanyUsersListReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<mBuzzCompanyUsersListReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.MBUZZ_COMPANY_USER_LIST)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.data.enid;
        }
    }

    [DataContract]
    public class mBuzzCompanyUsersListReqData 
    {
        public mBuzzCompanyUsersListReqData()
        { }

        /// <summary>
        /// Company Id
        /// </summary>
        [DataMember]
        public string enid { get; set; }
    }
}
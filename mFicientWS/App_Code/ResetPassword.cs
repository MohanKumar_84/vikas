﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientWS
{
    public class ResetPassword
    {
        ResetPasswordReq _resetPasswordReq;
        public ResetPassword(ResetPasswordReq resetPasswordReq)
        {
            _resetPasswordReq = resetPasswordReq;
        }
        public ResetPasswordResp Process(HttpContext context)
        {
            if (isResetRequestTimeValid())
            {
                SqlTransaction transaction = null;
                SqlConnection con;
                MSSqlDatabaseClient.SqlConnectionOpen(out con);
                try
                {
                    using (con)
                    {
                        using (transaction = con.BeginTransaction())
                        {
                            string strResetCode;
                            updateIsExpiredStatus(con, transaction);
                            saveResetPassword(con, transaction, out strResetCode);

                            bool blnEmailInfoSaved = SaveEmailInfo.resetPassword(_resetPasswordReq.UserName,
                                _resetPasswordReq.Email,
                                _resetPasswordReq.CompanyId,
                                ((int)RESET_PASSWORD_REQUEST_BY.User).ToString(),
                                strResetCode,
                                context);
                            if (!blnEmailInfoSaved) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                            transaction.Commit();
                            //make response status
                            ResponseStatus objResponseStatus = new ResponseStatus();
                            objResponseStatus.cd = "0";
                            objResponseStatus.desc = "";
                            return new ResetPasswordResp(objResponseStatus, _resetPasswordReq.RequestId);
                        }
                    }
                }
                catch (SqlException e)
                {
                    if (transaction != null)
                        transaction.Rollback();
                    if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                    }
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                catch (Exception e)
                {
                    if (transaction != null)
                        transaction.Rollback();
                    if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                    {
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                    }
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                finally
                {
                    if (con != null)
                    {
                        con.Dispose();
                    }
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }
                }
            }
            else
            {
                ResponseStatus objResponseStatus = new ResponseStatus();
                objResponseStatus.cd = Convert.ToString((int)RESET_PASSWORD_ERROR.REQUEST_SEND_BEFORE_STIPULATED_TIME);
                objResponseStatus.desc = "Request send before stipulated time period of 48 hrs.";
                return new ResetPasswordResp(objResponseStatus, _resetPasswordReq.RequestId);
            }
        }

        /// <summary>
        /// Returns reset code for Reset password Saving.(MD5).
        /// </summary>
        /// <param name="resetCode">original reset code which was md5'd</param>
        /// <returns></returns>
        private string getMD5ResetCode(out string originalResetCode)
        {
            string strGuid;
            do
            {
                Guid objGuid = new Guid();
                objGuid = Guid.NewGuid();
                strGuid = Convert.ToString(objGuid);
                string[] stringArray = { "0", "0", "O", "o", "I", "L", "1", "l", "s", "S", "5", "9", "Q", "q", "-" };
                foreach (string strItem in stringArray)
                {
                    strGuid = strGuid.Replace(strItem, "");
                }
            }
            while (strGuid.Length < 12);
            originalResetCode = strGuid.Substring(0, 6);
            return Utilities.GetMd5Hash(originalResetCode);
            //do
            //{

            //    if (strMd5ResetCode.Length > 0)
            //        bolHashCodeNotEmpty = true;
            //}
            //while (bolHashCodeNotEmpty == false);
            //return strMd5ResetCode;
        }

        void saveResetPassword(SqlConnection sqlConnection, SqlTransaction sqlTransaction, out string resetCode)
        {
            string strMD5ResetCode;
            strMD5ResetCode = getMD5ResetCode(out resetCode);
            string strQuery = @"INSERT INTO TBL_RESET_PASSWORD_LOG(USER_ID,NEW_ACCESSCODE,ACCESSCODE_RESET_DATETIME,IS_USED,IS_EXPIRED,COMPANY_ID) 
                                VALUES(@USER_ID,@ACCESS_CODE,@ACCESSCODE_RESET_DATETIME,0,0,@COMPANY_ID);";
            SqlCommand cmd = new SqlCommand(strQuery, sqlConnection, sqlTransaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@USER_ID", _resetPasswordReq.UserId);
            cmd.Parameters.AddWithValue("@ACCESS_CODE", strMD5ResetCode);
            cmd.Parameters.AddWithValue("@ACCESSCODE_RESET_DATETIME", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _resetPasswordReq.CompanyId);
            cmd.ExecuteNonQuery();
        }
        //        void saveInfoInEmailOutbox(string password)
        //        {
        //            SqlConnection sqlConnection ;
        //            MSSqlClient.SqlConnectionOpen(out sqlConnection, MficientConstants.strmGramConection);
        //            string strQuery = @"INSERT INTO [ADMIN_TBL_EMAIL_OUTBOX]
        //                                ([EMAIL_ID]
        //                                ,[EMAIL]
        //                                ,[SUBJECT]
        //                                ,[BODY]
        //                                ,[POST_TIME]
        //                                ,[SEND_TIME]
        //                                ,[STATUS],[EMAIL_TYPE])
        //                            VALUES
        //                                (@EMAIL_ID
        //                                ,@EMAIL
        //                                ,@SUBJECT
        //                                ,@BODY
        //                                ,@POST_TIME
        //                                ,@SEND_TIME
        //                                ,@STATUS,0);";
        //            SqlCommand cmd = new SqlCommand(strQuery, sqlConnection);
        //            cmd.CommandType = CommandType.Text;
        //            cmd.Parameters.AddWithValue("@EMAIL_ID", Utilities.GetMd5Hash((_resetPasswordReq.Email + DateTime.UtcNow.Ticks.ToString())));
        //            cmd.Parameters.AddWithValue("@EMAIL", _resetPasswordReq.Email);
        //            cmd.Parameters.AddWithValue("@SUBJECT", "mFicient Credentials");
        //            cmd.Parameters.AddWithValue("@BODY", "Reset Code is " + password + ". To Change Your Password Click on link http://enterprise.mficient.com/resetPassword.aspx?d=" + Utilities.EncryptString(_resetPasswordReq.UserName + "~" + _resetPasswordReq.CompanyId));
        //            cmd.Parameters.AddWithValue("@POST_TIME", DateTime.UtcNow.Ticks);
        //            cmd.Parameters.AddWithValue("@SEND_TIME", 0);
        //            cmd.Parameters.AddWithValue("@STATUS", 0);
        //            cmd.ExecuteNonQuery();
        //        }

        void updateIsExpiredStatus(SqlConnection sqlConnection, SqlTransaction sqlTransaction)
        {
            string strQuery = @"UPDATE TBL_RESET_PASSWORD_LOG
                                SET IS_EXPIRED = 1
                                WHERE USER_ID = @UserId
                                AND COMPANY_ID=@COMPANY_ID";
            SqlCommand cmd = new SqlCommand(strQuery, sqlConnection, sqlTransaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", _resetPasswordReq.UserId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _resetPasswordReq.CompanyId);
            cmd.ExecuteNonQuery();
        }

        bool isResetRequestTimeValid()
        {
            string strQuery = @"SELECT TOP 1 * 
                                FROM TBL_RESET_PASSWORD_LOG
                                WHERE USER_ID = @UserId
                                AND  COMPANY_ID=@COMPANY_ID
                                AND IS_EXPIRED=0
                                AND IS_USED =0
                                ORDER BY ACCESSCODE_RESET_DATETIME DESC";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", _resetPasswordReq.UserId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _resetPasswordReq.CompanyId);
            DataSet ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        long lngPreviousResetTime = (long)ds.Tables[0].Rows[0]["ACCESSCODE_RESET_DATETIME"];
                        DateTime dtPreviousResetTime = new DateTime(lngPreviousResetTime);
                        TimeSpan tsTimeIntervalBtwnReqAndUsage = DateTime.UtcNow - dtPreviousResetTime;
                        if (tsTimeIntervalBtwnReqAndUsage.TotalHours > 48)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        //if there are no records,then there was no previous reset request,hence we can save this request
                        //hence returning true
                        return true;
                    }
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
            }
            else
            {

                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class MPluginGetDomainListReq
    {
        string _requestId, _companyId, _agentName, _agentPassword;

        public string AgentPassword
        {
            get { return _agentPassword; }
        }

        public string AgentName
        {
            get { return _agentName; }
        }

        public string CompanyId
        {
            get { return _companyId; }
        }

        public string MPlugInPassword
        {
            get { return _agentPassword; }
        }
        
        public string RequestId
        {
            get { return _requestId; }
        }

        public MPluginGetDomainListReq(string requestJson)
        {
            GetDomainListReqJsonParsing objRequestJsonParsing = Utilities.DeserialiseJson<GetDomainListReqJsonParsing>(requestJson);
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.eid;
            _agentName = objRequestJsonParsing.req.agtnm;
            _agentPassword = objRequestJsonParsing.req.agtpwd;
        }
    }

    [DataContract]
    public class GetDomainListReqJsonParsing
    {
        [DataMember]
        public GetDomainListReqFields req { get; set; }
    }

    [DataContract]
    public class GetDomainListReqFields
    {
        [DataMember]
        public string rid { get; set; }

        [DataMember]
        public string eid { get; set; }

        [DataMember]
        public string agtnm { get; set; }

        [DataMember]
        public string agtpwd { get; set; }
    }
}
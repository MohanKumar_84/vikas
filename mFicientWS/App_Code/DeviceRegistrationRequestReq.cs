﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class DeviceRegistrationRequestReq
    {

        string _userId,_AppVersion, _requestId, _deviceId, _deviceType, _companyId, _registrationReqType, _Model, _OSVersion, _userName, _deviceSize, _pushMessageId;
        int _functionCode;

        public string DeviceSize
        {
            get { return _deviceSize; }
        }

        public string UserName
        {
            get { return _userName; }
        }
        public string RegistrationReqType
        {
            get { return _registrationReqType; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }

        public string UserId
        {
            get { return _userId; }
        }

        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string Model
        {
            get { return _Model; }
        }

        public string OSVersion
        {
            get { return _OSVersion; }
        }//_OSVersion

        public string AppVersion
        {
            get { return _AppVersion; }
        }//_OSVersion

        public string PushMessageId
        {
            get { return _pushMessageId; }
        }
        public DeviceRegistrationRequestReq(string requestJson)
        {
            RequestJsonParsing<DeviceRegistrationRequestData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<DeviceRegistrationRequestData>>(requestJson);
            //_email = objRequestJsonParsing.req.data.em;
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);

            if (_functionCode != (int)FUNCTION_CODES.DEVICE_REGISTRATION_REQUEST)
            {
                throw new Exception("Invalid Function Code");
            }
            //_sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _registrationReqType = objRequestJsonParsing.req.data.rqtp;
            _Model = objRequestJsonParsing.req.data.mdl;
            _deviceSize = objRequestJsonParsing.req.data.dsz;
            _OSVersion = objRequestJsonParsing.req.data.ver;
            _AppVersion = objRequestJsonParsing.req.data.mfv == null ? "" : objRequestJsonParsing.req.data.mfv;

            //string decryptedUserName = objRequestJsonParsing.req.data.unm;
            string decryptedUserName = EncryptionDecryption.AESDecrypt(_companyId, objRequestJsonParsing.req.data.unm);
            if (decryptedUserName.Contains("\\"))
            {
                _userName = decryptedUserName.Split('\\')[1];
            }
            else
            {
                _userName = decryptedUserName;
            }

            _pushMessageId = objRequestJsonParsing.req.data.pmid;
            DEVICE_ICON_SIZE deviceSize = (DEVICE_ICON_SIZE)Enum.Parse(typeof(DEVICE_ICON_SIZE), _deviceSize);
            if (string.IsNullOrEmpty(_registrationReqType) || string.IsNullOrEmpty(_Model) || string.IsNullOrEmpty(_deviceSize)
                || !Enum.IsDefined(typeof(DEVICE_ICON_SIZE), deviceSize) || (_pushMessageId==null))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
            _userId = Utilities.GetUserID(_companyId, _userName);
            if (_Model.Length > 50)
                _Model = _Model.Substring(0, 50);
        }
    }

    [DataContract]
    public class DeviceRegistrationRequestData : Data
    {
        public DeviceRegistrationRequestData()
        { }
        /// <summary>
        /// Request Type
        /// </summary>
        [DataMember]
        public string rqtp { get; set; }

        /// <summary>
        /// Device Model
        /// </summary>
        [DataMember]
        public string mdl { get; set; }

        /// <summary>
        /// Device size
        /// </summary>
        [DataMember]
        public string dsz { get; set; }

        /// <summary>
        /// os version
        /// </summary>
        [DataMember]
        public string ver { get; set; }

        /// <summary>
        /// Push message Id
        /// </summary>
        [DataMember]
        public string pmid { get; set; }

        /// <summary>
        /// Push message Id
        /// </summary>
        [DataMember]
        public string mfv { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientWS
{
    public class WSCGetRegisteredDevForUser
    {
        public WSCGetRegisteredDevForUser(
            string companyId, string userId)
        {
            CompanyId = companyId;
            UserId = userId;
        }

        public List<MFEDevice> getRegisteredDevices()
        {
            List<MFEDevice> lstRegisteredDevices = new List<MFEDevice>();
            try
            {
                StatusCode = -1000;
                DataSet dsRegisteredDevice = this.getRegisteredDevicesDS();
                if (dsRegisteredDevice != null)
                {
                    if (dsRegisteredDevice.Tables[0] != null)
                    {
                        lstRegisteredDevices =
                            getRegisteredDeviceFromDTbl(dsRegisteredDevice.Tables[0]);
                    }
                    StatusCode = 0;
                    StatusDescription = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch
            {
                StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return lstRegisteredDevices;
        }
        public List<string> getCommaSepRegisteredDevIdAndDevType()
        {
            List<string> lstCommaSepRegisteredDevIdAndDevType = new List<string>();
            try
            {
                List<MFEDevice> lstDevices = getRegisteredDevices();
                if (this.StatusCode != 0) throw new Exception();
                lstCommaSepRegisteredDevIdAndDevType =
                    getCommaSeparatedDevIdAndDevType(lstDevices);
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
                return lstCommaSepRegisteredDevIdAndDevType;
            }
            catch 
            {
                StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return lstCommaSepRegisteredDevIdAndDevType;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="device">Pass DeviceId,DevType,DevModel and Devsize.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">If parameter value is not found</exception>
        public MFEDevice getRegisteredDeviceByDevIdTypeModelAndSize(MFEDevice device)
        {
            if (device.DeviceId == null ||
                device.DeviceType == null ||
                device.DeviceModel == null )throw new ArgumentNullException();

            
            //try
            //{
            //    StatusCode = -1000;
            //    DataSet dsRegisteredDevice = this.getRegisteredDevicesDS();
            //    if (dsRegisteredDevice != null)
            //    {
            //        if (dsRegisteredDevice.Tables[0] != null)
            //        {
            //            lstRegisteredDevices =
            //                getRegisteredDeviceFromDTbl(dsRegisteredDevice.Tables[0]);
            //        }
            //        StatusCode = 0;
            //        StatusDescription = "";
            //    }
            //    else
            //    {
            //        throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            //    }
            //}
            //catch (Exception ex)
            //{
            //    StatusCode = -1000;
            //    this.StatusDescription = "Internal server error.";
            //}
            return new MFEDevice();
        }
        DataSet getRegisteredDevicesDS()
        {
            string strQuery = getSqlQueryByUserId();
            SqlCommand cmd = getCommandToRun(strQuery);
            DataSet objDataSet = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            return objDataSet;
        }
        string getSqlQueryByUserId()
        {
            return @" SELECT device.*,usr.USER_ID,usr.USER_NAME,(usr.FIRST_NAME +' '+usr.LAST_NAME)AS FULL_NAME 
                      FROM TBL_REGISTERED_DEVICE device
                      INNER JOIN TBL_USER_DETAIL as usr
                      ON usr.USER_ID = device.USER_ID
                      WHERE device.COMPANY_ID =@CompanyId
                      AND usr.USER_ID = @UserID;";
        }
        string getSqlQueryByUserIdAndDevDetl()
        {
            return @" SELECT device.*,usr.USER_ID,usr.USER_NAME,(usr.FIRST_NAME +' '+usr.LAST_NAME)AS FULL_NAME 
                      FROM TBL_REGISTERED_DEVICE device
                      INNER JOIN TBL_USER_DETAIL as usr
                      ON usr.USER_ID = device.USER_ID
                      WHERE device.COMPANY_ID =@CompanyId
                      AND usr.USER_ID = @UserID
                      AND device.DEVICE_TYPE = @DeviceType
                     AND device.DEVICE_ID = @DeviceId
                     AND device.DEVICE_MODEL =@DeviceModel
                     AND device.DEVICE_SIZE = @DEVICE_SIZE;";
        }
        SqlCommand getCommandToRun(string query)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            cmd.Parameters.AddWithValue("@UserID", this.UserId);
            return cmd;
        }
        List<MFEDevice> getRegisteredDeviceFromDTbl(DataTable deviceDtls)
        {
            if (deviceDtls == null) throw new ArgumentNullException();
            List<MFEDevice> lstDevices = new List<MFEDevice>();
            foreach (DataRow row in deviceDtls.Rows)
            {
                lstDevices.Add(getRegisteredDeviceFromDatarow(row));
            }
            return lstDevices;
        }
        MFEDevice getRegisteredDeviceFromDatarow(DataRow row)
        {
            if (row == null) throw new ArgumentNullException();
            MFEDevice objDevice = new MFEDevice();
            objDevice.UserId = Convert.ToString(row["USER_ID"]);
            objDevice.CompanyId = Convert.ToString(row["COMPANY_ID"]);
            objDevice.DeviceType = Convert.ToString(row["DEVICE_TYPE"]);
            objDevice.DeviceId = Convert.ToString(row["DEVICE_ID"]);
            objDevice.RegistrationDate = Convert.ToInt64(row["REGISTRATION_DATE"]);
            objDevice.SubAdminId = Convert.ToString(row["SUBADMIN_ID"]);
            objDevice.DeviceModel = Convert.ToString(row["DEVICE_MODEL"]);
            objDevice.MficientKey = Convert.ToString(row["MFICIENT_KEY"]);
            objDevice.OsVersion = Convert.ToString(row["OS_VERSION"]);
            objDevice.DeviceSize = Convert.ToInt32(row["DEVICE_SIZE"]);
            objDevice.DevicePushMsgId = Convert.ToString(row["DEVICE_PUSH_MESSAGE_ID"]);
            return objDevice;
        }
        List<string> getCommaSeparatedDevIdAndDevType(DataTable deviceDtls)
        {
            List<string> lstCommaSepDevIdAndDevType = new List<string>();
            foreach (DataRow row in deviceDtls.Rows)
            {
                lstCommaSepDevIdAndDevType.Add(row["DEVICE_ID"] + "," + row["DEVICE_TYPE"]);
            }
            return lstCommaSepDevIdAndDevType;
        }
        List<string> getCommaSeparatedDevIdAndDevType(List<MFEDevice> registeredDevices)
        {
            List<string> lstCommaSepDevIdAndDevType = new List<string>();
            foreach (MFEDevice device in registeredDevices)
            {
                lstCommaSepDevIdAndDevType.Add(device.DeviceId + "," + device.DeviceType);
            }
            return lstCommaSepDevIdAndDevType;
        }
        #region Public Properties
        //public DataSet ResultTables
        //{
        //    private set;
        //     get;
        //}
        public string UserId
        {
            get;
            set;
        }
        public int StatusCode
        {
            private set;
            get;
        }
        public string StatusDescription
        {
            private set;
            get;
        }
        public string SubAdminId
        {
            private set;
            get;
        }
        public string AdminId
        {
            private set;
            get;
        }
        public string CompanyId
        {
            private set;
            get;
        }
        //public List<string> CommaSeparatedDevIdAndDevType
        //{
        //    get;
        //    private set;
        //}
        //public List<MFEDevice> DevicesRegistered
        //{
        //    get;
        //    private set;
        //}
        #endregion
    }
}
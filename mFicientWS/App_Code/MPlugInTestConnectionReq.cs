﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class MPluginTestConnectionReq
    {
        string _requestId, _enterpriseId, _mplugInPassword, _requestType, _connectionString, _databaseType, _requestJson, _agentId, _agentPassword;

        public string AgentPassword
        {
            get { return _agentPassword; }
           
        }

        public string AgentId
        {
            get { return _agentId; }
            
        }

        public string RequestJson
        {
            get { return _requestJson; }
        }

        public string DatabaseType
        {
            get { return _databaseType; }
        }

        public string ConnectionString
        {
            get { return _connectionString; }
           
        }

        public string RequestType
        {
            get { return _requestType; }
            
        }

        public string MplugInPassword
        {
            get { return _mplugInPassword; }
            
        }

        public string EnterpriseId
        {
            get { return _enterpriseId; }
            
        }

        public string RequestId
        {
            get { return _requestId; }
            
        }

        public MPluginTestConnectionReq(string requestJson)
        {
            TestConnectionRequestJsonParsing<TestConnectionRequestData> objRequestJsonParsing = Utilities.DeserialiseJson<TestConnectionRequestJsonParsing<TestConnectionRequestData>>(requestJson);
            //_functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            //if (_functionCode != (int)FUNCTION_CODES.COMPANY_DETAIL)
            //{
            //    throw new Exception("Invalid Function Code");
            //}
            _requestId = objRequestJsonParsing.req.rid;
            _enterpriseId = objRequestJsonParsing.req.eid;
            _mplugInPassword = objRequestJsonParsing.req.mpwd;
            _requestType = objRequestJsonParsing.req.data.rtyp;
            _connectionString = objRequestJsonParsing.req.data.con;
            _databaseType = objRequestJsonParsing.req.data.dbtyp;
            _agentId = objRequestJsonParsing.req.data.agtid;
            _agentPassword = objRequestJsonParsing.req.data.agtpwd;
            _requestJson = requestJson;
        }
    }

    [DataContract]
    public class TestConnectionRequestJsonParsing<T>
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public TestConnectionRequestFields<T> req { get; set; }
    }


    [DataContract]
    public class TestConnectionRequestFields<T>
    {

        [DataMember]
        public string rid { get; set; }

        [DataMember]
        public string eid { get; set; }

        [DataMember]
        public string mpwd { get; set; }

        /// <summary>
        /// data from request
        /// </summary>
        [DataMember]
        public T data { get; set; }
    }


    [DataContract]
    public class TestConnectionRequestData 
    {
        [DataMember]
        public string rtyp { get; set; }

        [DataMember]
        public string con { get; set; }

        [DataMember]
        public string dbtyp { get; set; }

        [DataMember]
        public string agtid { get; set; }

        [DataMember]
        public string agtpwd { get; set; }
    }
}
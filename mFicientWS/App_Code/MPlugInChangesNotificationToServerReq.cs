﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class MPlugInChangesNotificationToServerReq
    {
        string _requestId,  _companyId, _agentName, _agentPassword;

        public string AgentPassword
        {
            get { return _agentPassword; }
        }

        public string AgentName
        {
            get { return _agentName; }
        }

        public string CompanyId
        {
            get { return _companyId; }
        }
        //int _functionCode;

        public string MPlugInPassword
        {
            get { return _agentPassword; }
        }
        
        public string RequestId
        {
            get { return _requestId; }
        }
        //public int FunctionCode
        //{
        //    get { return _functionCode; }
        //}

        public MPlugInChangesNotificationToServerReq(string requestJson)
        {
            MPlugInChangeNotificationReqJsonParsing objRequestJsonParsing = Utilities.DeserialiseJson<MPlugInChangeNotificationReqJsonParsing>(requestJson);
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.eid;
            //_requestType = objRequestJsonParsing.req.rtyp;
            //_mPlugInPassword = objRequestJsonParsing.req.mpwd;
            _agentName = objRequestJsonParsing.req.agtid;
            _agentPassword = objRequestJsonParsing.req.agtpwd;
        }
    }

    [DataContract]
    public class MPlugInChangeNotificationReqJsonParsing
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public MPlugInChngNotificationReqFields req { get; set; }
    }

    [DataContract]
    public class MPlugInChngNotificationReqFields
    {

        /// <summary>
        /// request id
        /// </summary>
        [DataMember]
        public string rid { get; set; }
        
        /// <summary>
        /// function code
        /// </summary>
        //[DataMember]
        //public string func { get; set; }

        /// <summary>
        ///mPlugIn Password
        /// </summary>
        //[DataMember]
        //public string mpwd { get; set; }

        /// <summary>
        ///company id
        /// </summary>
        [DataMember]
        public string eid { get; set; }

        [DataMember]
        public string agtid { get; set; }

        [DataMember]
        public string agtpwd { get; set; }
    }
}
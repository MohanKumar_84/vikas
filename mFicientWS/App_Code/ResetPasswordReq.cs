﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class ResetPasswordReq
    {
        string _email, _userId, _sessionId, _requestId, _deviceId, _deviceType, _userName, _companyId;
        int _functionCode;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string UserName
        {
            get { return _userName; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string SessionId
        {
            get { return _sessionId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string Email
        {
            get { return _email; }
        }

        public ResetPasswordReq(string requestJson)
        {
            //session id is not required.
            RequestJsonParsing<ResetPasswordReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<ResetPasswordReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.RESET_PASSWORD)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            //string decryptedUserName = objRequestJsonParsing.req.data.unm;

            _companyId = objRequestJsonParsing.req.data.enid;

            string decryptedUserName = EncryptionDecryption.AESDecrypt(_companyId, objRequestJsonParsing.req.data.unm);
            if (decryptedUserName.Contains("\\"))
            {
                _userName = decryptedUserName.Split('\\')[1];
            }
            else
            {
                _userName = decryptedUserName;
            }
            _userId = Utilities.GetUserID(_companyId, _userName, out _email);
        }

    }
    [DataContract]
    public class ResetPasswordReqData : Data
    {
        public ResetPasswordReqData()
        { }
    }
}
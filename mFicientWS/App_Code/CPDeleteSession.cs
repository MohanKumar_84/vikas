﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class CPDeleteSession
    {
        CPDeleteSessionReq _cpDeleteSessionReq;
        public CPDeleteSession(CPDeleteSessionReq cpDeleteSessionReq)
        {
            _cpDeleteSessionReq = cpDeleteSessionReq;
        }
        public void Process()
        {
            List<DeviceDetail> lstDeviceToDelete = _cpDeleteSessionReq.DeviceToDelete;
            string strUserName = _cpDeleteSessionReq.UserName;
            string strCompanyId = _cpDeleteSessionReq.CompanyId;
            foreach (DeviceDetail deviceDetail in lstDeviceToDelete)
            {
                //CacheHelper.removeFromCache(CacheHelper.getKeyForCacheSesssion(strUserName, deviceDetail.dtyp, deviceDetail.did, strCompanyId));
                CacheManager.Remove(CacheManager.GetKey(CacheManager.CacheType.mFicientSession,
                    strCompanyId, strUserName,
                    deviceDetail.did, deviceDetail.dtyp,
                    "","",String.Empty));
            }
        }
    }
}
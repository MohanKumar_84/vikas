﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json.Linq;

namespace mFicientWS
{
    public class DataBaseCommand
    {
        #region Private Members

        private string commandId, connectorId, enterpriseId, sqlQuery, returnType, commandName, tableName, mPluginAgent, mPluginAgentPwd, outputColName, userId, password, cacheType, cacheFrequency,imageColumn, cacheCondition;
        private DatabaseCommandType dbCommandType;
        private DataBaseConnector dbConnector;
        private int dbConnectorType;
        private DoTaskReq _doTaskRequest;
        ResponseStatus objRespStatus = null;
        DataSet dsDataForResponse = null;
        string strQuerytResult = string.Empty;
        string strOutputColumnName = string.Empty;
        List<List<string>> imageCols;
        #endregion

        #region Constructor
        public bool isExist = false;
        internal DataBaseCommand(string _commandId, string _enterpriseId, string _userId, string _password)
        {
            commandId = _commandId;
            enterpriseId = _enterpriseId;
            userId = _userId;
            password = _password;
            objRespStatus = new ResponseStatus();
            getCommandDetails();
        }

        #endregion

        #region Public Properties

        public string CommandId
        {
            get
            {
                return commandId;
            }
        }
        public string ProcessId
        {
            get;
            set;
        }
        public string ConnectorId
        {
            get
            {
                return connectorId;
            }
        }
        public string UserId
        {
            get
            {
                return userId;
            }
        }
        public string Password
        {
            get
            {
                return password;
            }
        }

        public DataBaseConnector DbConnector
        {
            get
            {
                try
                {
                    dbConnector = new DataBaseConnector(connectorId, enterpriseId, UserId, Password);
                    dbConnector.GetConnector();
                }
                catch
                {
                }
                return dbConnector;
            }
        }
        public int DBConnectorType
        {
            get
            {
                return dbConnectorType;
            }
        }
        public DatabaseCommandType DataBaseCommandType
        {
            get
            {
                return dbCommandType;
            }
        }

        public string SqlQuery
        {
            get
            {
                return sqlQuery;
            }
        }

        public string ReturnType
        {
            get
            {
                return returnType;
            }
        }

        public string EnterpriseId
        {
            get
            {
                return enterpriseId;
            }
        }

        public string CommandName
        {
            get
            {
                return commandName;
            }
        }

        public string TableName
        {
            get
            {
                return tableName;
            }
        }

        public string OutputColumnName
        {
            get
            {
                return outputColName;
            }

        }
        //public string DataTranslationJson
        //{
        //    get
        //    {
        //        return dataTranslationJson;
        //    }
        //}
        public string MPluginAgent
        {
            get
            {
                return mPluginAgent;
            }
        }
        public string MPluginAgentPwd
        {
            get
            {
                return mPluginAgentPwd;
            }
        }
        public string ImageColumn
        {
            get
            {
                return imageColumn;
            }
        }
        #endregion

        #region Private Methods

        private void getCommandDetails()
        {
            string strQuery = @"SELECT cmd.*,isnull(ag.MP_AGENT_PASSWORD, '') as MP_AGENT_PASSWORD,con.MPLUGIN_AGENT,con.DATABASE_TYPE  FROM TBL_DATABASE_COMMAND as cmd inner join TBL_database_CONNECTION as con on cmd.db_CONNECTOR_ID=con.db_CONNECTOR_ID and cmd.COMPANY_ID=con.COMPANY_ID 
            left outer join TBL_MPLUGIN_AGENT_DETAIL as ag on ag.MP_AGENT_NAME=con.MPLUGIN_AGENT and ag.COMPANY_ID=con.COMPANY_ID WHERE cmd.COMPANY_ID = @COMPANY_ID ";

            if (commandId.StartsWith("name://"))
            {
                strQuery += " and DB_COMMAND_NAME = @DB_COMMAND_ID;";
                commandId = commandId.Substring(7);
            }
            else strQuery += " and DB_COMMAND_ID = @DB_COMMAND_ID;";
            SqlCommand cmd = new SqlCommand(strQuery);

            try
            {
                cmd.Parameters.AddWithValue("@DB_COMMAND_ID", commandId);
                cmd.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);
                DataSet ds = MSSqlDoTaskClient.SelectDataFromSqlCommand(cmd);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    connectorId = Convert.ToString(ds.Tables[0].Rows[0]["DB_CONNECTOR_ID"]);
                    mPluginAgentPwd = Convert.ToString(ds.Tables[0].Rows[0]["MP_AGENT_PASSWORD"]);
                    mPluginAgent = Convert.ToString(ds.Tables[0].Rows[0]["MPLUGIN_AGENT"]);
                    dbConnectorType = Convert.ToInt32(ds.Tables[0].Rows[0]["DATABASE_TYPE"]);
                    commandName = Convert.ToString(ds.Tables[0].Rows[0]["DB_COMMAND_NAME"]);
                    returnType = Convert.ToString(ds.Tables[0].Rows[0]["RETURN_TYPE"]);
                    dbCommandType = (DatabaseCommandType)Convert.ToInt32(ds.Tables[0].Rows[0]["DB_COMMAND_TYPE"]);
                    tableName = Convert.ToString(ds.Tables[0].Rows[0]["TABLE_NAME"]);
                    sqlQuery = Convert.ToString(ds.Tables[0].Rows[0]["SQL_QUERY"]);
                    outputColName = Convert.ToString(ds.Tables[0].Rows[0]["COLUMNS"]);
                    commandId = Convert.ToString(ds.Tables[0].Rows[0]["DB_COMMAND_ID"]);
                    cacheType = Convert.ToString(ds.Tables[0].Rows[0]["CACHE"]);
                    cacheFrequency = Convert.ToString(ds.Tables[0].Rows[0]["EXPIRY_FREQUENCY"]);
                    cacheCondition = Convert.ToString(ds.Tables[0].Rows[0]["EXPIRY_CONDITION"]);
                    imageColumn = Convert.ToString(ds.Tables[0].Rows[0]["IMAGE_COLUMN"]);
                    imageCols = new List<List<string>>();
                    if (!string.IsNullOrEmpty(imageColumn) && imageColumn.Trim().Length > 0)
                        imageCols = Utilities.DeserialiseJson<List<List<string>>>(imageColumn);
                    isExist = true;
                }
            }
            catch 
            {

            }
        }

        DoTaskResp processDBCommand()
        {
            if (!String.IsNullOrEmpty(connectorId))
            {
                try
                {
                    if (!String.IsNullOrEmpty(mPluginAgent.Trim()))
                    {
                        return processMPlugInDatabaseCommand();
                    }
                    else
                    {
                        _doTaskRequest.DataCredential.pwd = EncryptionDecryption.AESDecrypt(_doTaskRequest.CompanyId, _doTaskRequest.DataCredential.pwd);
                        _doTaskRequest.DataCredential.unm = EncryptionDecryption.AESDecrypt(_doTaskRequest.CompanyId, _doTaskRequest.DataCredential.unm);
                        switch ((DatabaseType)this.dbConnectorType)
                        {
                            case DatabaseType.MSSQL:
                                return MsSqlDtTask();
                            case DatabaseType.MYSQL:
                                return MySqlDtTask();
                            case DatabaseType.POSTGRESQL:
                                return PostgreSqlDtTask();
                            case DatabaseType.ORACLE:
                                return OracleDtTask();
                        }
                    }
                }
                catch (Exception e)
                {
                    objRespStatus = Utilities.getResponseStatus(e);
                }
            }
            else
            {
                objRespStatus = Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.INVALID_COMMAND).ToString()));
            }
            return new DoTaskResp(objRespStatus, _doTaskRequest.RequestId, "", "0", null, _doTaskRequest.ControlId, cacheType, cacheFrequency, cacheCondition, mPluginAgent, imageCols);
        }

        DoTaskResp processMPlugInDatabaseCommand()
        {
            ResponseStatus objRespStatus = new ResponseStatus();
            objRespStatus.cd = "0";
            objRespStatus.desc = "";
            string strMPlugInUrl = Utilities.getMPlugInServerURL(_doTaskRequest.CompanyId);
            if (!String.IsNullOrEmpty(strMPlugInUrl))
            {
                mPlugin.QueryType mPlugInQueryType = mPlugin.QueryType.SELECT;
                if (Convert.ToInt32(dbCommandType) == 1)//select query
                {
                    mPlugInQueryType = mPlugin.QueryType.SELECT;
                }
                else if (Convert.ToInt32(dbCommandType) == 2)//insert query
                {
                    mPlugInQueryType = mPlugin.QueryType.INSERT;
                }
                else if (Convert.ToInt32(dbCommandType) == 3)//update query
                {
                    mPlugInQueryType = mPlugin.QueryType.UPDATE;
                }
                else if (Convert.ToInt32(dbCommandType) == 4)//delete query
                {
                    mPlugInQueryType = mPlugin.QueryType.DELETE;
                }
                else if (Convert.ToInt32(dbCommandType) == 5)//stored procedure
                {
                    mPlugInQueryType = mPlugin.QueryType.STORED_PROCEDURE;
                }

                mPlugin.SqlParameters parameters = new mPlugin.SqlParameters();
                foreach (QueryParameters parameter in _doTaskRequest.Parameters)
                {
                    parameters.Add(parameter.para, parameter.val);
                }
                try
                {
                    switch (mPlugInQueryType)
                    {
                        case mPlugin.QueryType.SELECT:
                        case mPlugin.QueryType.STORED_PROCEDURE:
                            if (_doTaskRequest.RcdPagingCount > 0)
                            {
                                string strDatasetId, strTotalPages;
                                dsDataForResponse = mPlugin.RunSelectSqlQuery(
                                    enterpriseId,
                                    mPluginAgent, mPluginAgentPwd,
                                    connectorId, sqlQuery, parameters,
                                    this.dbConnectorType != (int)DatabaseType.ODBC ? mPlugin.HttpRequestType.DATABASE_QUERY : mPlugin.HttpRequestType.DSN_QUERY
                                    ,
                                    true, _doTaskRequest.PageNumber, _doTaskRequest.RcdPagingCount, mPlugInQueryType,
                                    out strDatasetId,
                                    out strTotalPages, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd, _doTaskRequest.UserName, "");

                                if (dsDataForResponse != null)
                                {
                                    this.savePagedDataInCache(
                                        enterpriseId,
                                        _doTaskRequest.UserId,
                                        _doTaskRequest.DeviceId, _doTaskRequest.DeviceType,
                                        strDatasetId, mPluginAgent
                                        );
                                }
                                return getDBProcessResponse(
                                    dsDataForResponse,
                                    objRespStatus,
                                    strDatasetId,
                                    strTotalPages
                                    );
                            }
                            else
                            {
                                dsDataForResponse = mPlugin.RunSelectSqlQuery(
                                    enterpriseId,
                                    mPluginAgent, mPluginAgentPwd,
                                    connectorId, sqlQuery, parameters, this.dbConnectorType != (int)DatabaseType.ODBC ? mPlugin.HttpRequestType.DATABASE_QUERY : mPlugin.HttpRequestType.DSN_QUERY
                                    , _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd, _doTaskRequest.UserName, "");

                                return getDBProcessResponse(dsDataForResponse, objRespStatus,
                                    String.Empty,
                                    String.Empty
                                    );
                            }
                        case mPlugin.QueryType.INSERT:
                            dsDataForResponse = new DataSet();
                            strOutputColumnName = string.Empty;
                            if (!string.IsNullOrEmpty(outputColName) && (DatabaseType)this.dbConnectorType == DatabaseType.ORACLE)
                                strOutputColumnName = outputColName.Split(',')[0];
                            strQuerytResult = mPlugin.RunInsertSqlQuery(enterpriseId, mPluginAgent, mPluginAgentPwd, connectorId, sqlQuery, parameters,
                                this.dbConnectorType != (int)DatabaseType.ODBC ? mPlugin.HttpRequestType.DATABASE_QUERY : mPlugin.HttpRequestType.DSN_QUERY
                                , _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd, strOutputColumnName, _doTaskRequest.UserName, "");
                            DataTable dtMsSql = new DataTable();

                            if (!string.IsNullOrEmpty(strOutputColumnName))
                            {
                                strOutputColumnName = outputColName.Split(',')[0];
                                dtMsSql.Columns.Add(strOutputColumnName);
                                DataRow row = dtMsSql.NewRow();
                                if (!string.IsNullOrEmpty(strOutputColumnName))
                                {
                                    row[strOutputColumnName] = strQuerytResult;
                                    dtMsSql.Rows.Add(row);
                                    dsDataForResponse.Tables.Add(dtMsSql);
                                }
                            }

                            return getDBProcessResponse(dsDataForResponse, objRespStatus,
                                    String.Empty,
                                    String.Empty);

                        case mPlugin.QueryType.UPDATE:
                            dsDataForResponse = new DataSet();
                            strOutputColumnName = string.Empty;
                            if (!string.IsNullOrEmpty(outputColName) && (DatabaseType)this.dbConnectorType == DatabaseType.ORACLE)
                                strOutputColumnName = outputColName.Split(',')[0];
                            strQuerytResult = mPlugin.RunUpdateSqlQuery(enterpriseId, mPluginAgent, mPluginAgentPwd, connectorId, sqlQuery, parameters,
                                this.dbConnectorType != (int)DatabaseType.ODBC ? mPlugin.HttpRequestType.DATABASE_QUERY : mPlugin.HttpRequestType.DSN_QUERY
                                , _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd, strOutputColumnName, _doTaskRequest.UserName, "");

                            DataTable dtMsSqlUpdate = new DataTable();

                            if (!string.IsNullOrEmpty(outputColName))
                            {
                                strOutputColumnName = outputColName.Split(',')[0];
                                dtMsSqlUpdate.Columns.Add(strOutputColumnName);
                                DataRow row = dtMsSqlUpdate.NewRow();
                                if (!string.IsNullOrEmpty(strOutputColumnName))
                                {
                                    row[strOutputColumnName] = strQuerytResult;
                                    dtMsSqlUpdate.Rows.Add(row);
                                    dsDataForResponse.Tables.Add(dtMsSqlUpdate);
                                }
                            }

                            return getDBProcessResponse(dsDataForResponse, objRespStatus,
                                    String.Empty,
                                    String.Empty);
                        case mPlugin.QueryType.DELETE:
                            dsDataForResponse = new DataSet();
                            strOutputColumnName = string.Empty;
                            if (!string.IsNullOrEmpty(outputColName) && (DatabaseType)this.dbConnectorType == DatabaseType.ORACLE)
                                strOutputColumnName = outputColName.Split(',')[0];
                            strQuerytResult = mPlugin.RunDeleteSqlQuery(enterpriseId, mPluginAgent, mPluginAgentPwd, connectorId, sqlQuery, parameters,
                                this.dbConnectorType != (int)DatabaseType.ODBC ? mPlugin.HttpRequestType.DATABASE_QUERY : mPlugin.HttpRequestType.DSN_QUERY
                                , _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd, strOutputColumnName, _doTaskRequest.UserName, "");

                            DataTable dtMsSqlDelete = new DataTable();

                            if (!string.IsNullOrEmpty(outputColName))
                            {
                                strOutputColumnName = outputColName.Split(',')[0];
                                dtMsSqlDelete.Columns.Add(strOutputColumnName);
                                DataRow row = dtMsSqlDelete.NewRow();
                                if (!string.IsNullOrEmpty(strOutputColumnName))
                                {
                                    row[strOutputColumnName] = strQuerytResult;
                                    dtMsSqlDelete.Rows.Add(row);
                                    dsDataForResponse.Tables.Add(dtMsSqlDelete);
                                }
                            }

                            return getDBProcessResponse(dsDataForResponse, objRespStatus,
                                    String.Empty,
                                    String.Empty);
                    }
                }
                catch (mPlugin.mPluginException mex)
                {
                    try
                    {
                        objRespStatus = Utilities.getResponseStatus(mex);
                    }
                    catch
                    {
                        objRespStatus = Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString()));
                    }
                }
                catch (HttpException hex)
                {
                    throw Utilities.HttpExecptionToException(hex);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new Exception();
            }
            //reaches this point if there ia an error,or there is an exception thrown.
            return new DoTaskResp(objRespStatus, _doTaskRequest.RequestId, "", "0", null, _doTaskRequest.ControlId, cacheType, cacheFrequency, cacheCondition, mPluginAgent,imageCols);
        }

        #endregion

        #region Public Method

        public DoTaskResp processDBCommand(DoTaskReq doTaskRequest)
        {
            _doTaskRequest = doTaskRequest;
            if (commandName == null)
            {
                return new DoTaskResp(Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.INVALID_COMMAND).ToString())),
               _doTaskRequest.RequestId, "", "0", null, _doTaskRequest.ControlId, cacheType, cacheFrequency, cacheCondition, mPluginAgent, imageCols);
            }
            else
            {
                return processDBCommand();
            }
        }

        #endregion

        #region Without mPluginDB

        DoTaskResp MsSqlDtTask()
        {
            switch (dbCommandType)
            {
                case DatabaseCommandType.SELECT:
                    {
                        dsDataForResponse = MSSqlDoTaskClient.SelectDataFromDoTask(this, _doTaskRequest.Parameters, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd);
                        string strDatasetId = string.Empty;
                        int totalBlocks = 0;
                        if (_doTaskRequest.RcdPagingCount > 0)
                        {
                            try
                            {
                                strDatasetId = getDatasetIdForpaging(_doTaskRequest.UserId,
                                        _doTaskRequest.DeviceId,
                                        _doTaskRequest.DeviceType);

                                this.savePagedDataInCache(
                                       _doTaskRequest.CompanyId,
                                       _doTaskRequest.UserId,
                                       _doTaskRequest.DeviceId,
                                       _doTaskRequest.DeviceType,
                                       strDatasetId,
                                       dsDataForResponse
                                       );

                                totalBlocks = dsDataForResponse.Tables[0].Rows.Count / _doTaskRequest.RcdPagingCount;
                                if (dsDataForResponse.Tables[0].Rows.Count % _doTaskRequest.RcdPagingCount != 0)
                                    totalBlocks += 1;
                                string key = CacheManager.GetKey(CacheManager.CacheType.mFicientDSPaging, _doTaskRequest.CompanyId,
                                         _doTaskRequest.UserId, _doTaskRequest.DeviceId, _doTaskRequest.DeviceType, String.Empty, String.Empty, strDatasetId);
                                dsDataForResponse = ProcessPagingHelpers.processGetResponseDS(key, 1, _doTaskRequest.RcdPagingCount);

                            }
                            catch (MficientException ex)
                            {
                                dsDataForResponse = null;
                            }

                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                strDatasetId, (totalBlocks).ToString());
                        }
                        else
                        {
                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                String.Empty,
                                String.Empty);
                        }

                    }
                case DatabaseCommandType.INSERT:
                case DatabaseCommandType.UPDATE:
                case DatabaseCommandType.DELETE:
                    {
                        dsDataForResponse = new DataSet();
                        strOutputColumnName = string.Empty;

                        strQuerytResult = MSSqlDoTaskClient.ExecuteScalarDotask(this, _doTaskRequest.Parameters, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd);

                        DataTable dtMsSql = new DataTable();

                        if (!string.IsNullOrEmpty(outputColName))
                        {
                            strOutputColumnName = outputColName.Split(',')[0];
                            dtMsSql.Columns.Add(strOutputColumnName);
                            DataRow row = dtMsSql.NewRow();
                            if (!string.IsNullOrEmpty(strOutputColumnName))
                            {
                                row[strOutputColumnName] = strQuerytResult;
                                dtMsSql.Rows.Add(row);
                                dsDataForResponse.Tables.Add(dtMsSql);
                            }
                        }

                        return getDBProcessResponse(dsDataForResponse, objRespStatus,
                            String.Empty,
                            String.Empty);
                    }
                case DatabaseCommandType.STORED_PROCEDURE:
                    {
                        dsDataForResponse = MSSqlDoTaskClient.ExecuteStoredProcedureDoTask(this, _doTaskRequest.Parameters, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd);
                        string strDatasetId = string.Empty;
                        int totalBlocks = 0;
                        if (_doTaskRequest.RcdPagingCount > 0)
                        {
                            try
                            {
                                strDatasetId = getDatasetIdForpaging(_doTaskRequest.UserId,
                                        _doTaskRequest.DeviceId,
                                        _doTaskRequest.DeviceType);

                                this.savePagedDataInCache(
                                       _doTaskRequest.CompanyId,
                                       _doTaskRequest.UserId,
                                       _doTaskRequest.DeviceId,
                                       _doTaskRequest.DeviceType,
                                       strDatasetId,
                                       dsDataForResponse
                                       );

                                totalBlocks = dsDataForResponse.Tables[0].Rows.Count / _doTaskRequest.RcdPagingCount;
                                if (dsDataForResponse.Tables[0].Rows.Count % _doTaskRequest.RcdPagingCount != 0)
                                    totalBlocks += 1;
                                string key = CacheManager.GetKey(CacheManager.CacheType.mFicientDSPaging, _doTaskRequest.CompanyId,
                                        _doTaskRequest.UserId, _doTaskRequest.DeviceId, _doTaskRequest.DeviceType, String.Empty, String.Empty, strDatasetId);
                                dsDataForResponse = ProcessPagingHelpers.processGetResponseDS(key, 1, _doTaskRequest.RcdPagingCount);

                            }
                            catch (MficientException ex)
                            {
                                dsDataForResponse = null;
                            }

                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                strDatasetId, (totalBlocks).ToString());
                        }
                        else
                        {
                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                String.Empty,
                                String.Empty);
                        }

                    }
            }
            return null;

        }

        DoTaskResp PostgreSqlDtTask()
        {
            switch (dbCommandType)
            {
                case DatabaseCommandType.SELECT:
                    {
                        DataSet dsDataForResponse = PostGreDoTaskClient.SelectDataFromDoTask(
                            this, _doTaskRequest.Parameters, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd
                            );

                        string strDatasetId = string.Empty;
                        int totalBlocks = 0;
                        if (_doTaskRequest.RcdPagingCount > 0)
                        {
                            try
                            {
                                strDatasetId = getDatasetIdForpaging(_doTaskRequest.UserId,
                                        _doTaskRequest.DeviceId,
                                        _doTaskRequest.DeviceType);

                                this.savePagedDataInCache(
                                       _doTaskRequest.CompanyId,
                                       _doTaskRequest.UserId,
                                       _doTaskRequest.DeviceId,
                                       _doTaskRequest.DeviceType,
                                       strDatasetId,
                                       dsDataForResponse
                                       );

                                totalBlocks = dsDataForResponse.Tables[0].Rows.Count / _doTaskRequest.RcdPagingCount;
                                if (dsDataForResponse.Tables[0].Rows.Count % _doTaskRequest.RcdPagingCount != 0)
                                    totalBlocks += 1;
                                string key = CacheManager.GetKey(CacheManager.CacheType.mFicientDSPaging, _doTaskRequest.CompanyId,
                                       _doTaskRequest.UserId, _doTaskRequest.DeviceId, _doTaskRequest.DeviceType, String.Empty, String.Empty, strDatasetId);
                                dsDataForResponse = ProcessPagingHelpers.processGetResponseDS(key, 1, _doTaskRequest.RcdPagingCount);

                            }
                            catch (MficientException ex)
                            {
                                dsDataForResponse = null;
                            }

                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                strDatasetId, (totalBlocks).ToString());
                        }
                        else
                        {
                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                String.Empty,
                                String.Empty);
                        }
                    }
                case DatabaseCommandType.INSERT:
                case DatabaseCommandType.UPDATE:
                case DatabaseCommandType.DELETE:
                    {
                        dsDataForResponse = new DataSet();
                        strOutputColumnName = string.Empty;

                        strQuerytResult = PostGreDoTaskClient.ExecuteScalarDotask(this, _doTaskRequest.Parameters, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd);

                        DataTable dtMsSql = new DataTable();

                        if (!string.IsNullOrEmpty(outputColName))
                        {
                            strOutputColumnName = outputColName.Split(',')[0];
                            dtMsSql.Columns.Add(strOutputColumnName);
                            DataRow row = dtMsSql.NewRow();
                            if (!string.IsNullOrEmpty(strOutputColumnName))
                            {
                                row[strOutputColumnName] = strQuerytResult;
                                dtMsSql.Rows.Add(row);
                                dsDataForResponse.Tables.Add(dtMsSql);
                            }
                        }

                        return getDBProcessResponse(dsDataForResponse, objRespStatus,
                            String.Empty,
                            String.Empty);
                    }
                case DatabaseCommandType.STORED_PROCEDURE:
                    {
                        DataSet dsDataForResponse = PostGreDoTaskClient.ExecuteStoredProcedureDoTask(
                            this, _doTaskRequest.Parameters, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd
                            );

                        string strDatasetId = string.Empty;
                        int totalBlocks = 0;
                        if (_doTaskRequest.RcdPagingCount > 0)
                        {
                            try
                            {
                                strDatasetId = getDatasetIdForpaging(_doTaskRequest.UserId,
                                        _doTaskRequest.DeviceId,
                                        _doTaskRequest.DeviceType);

                                this.savePagedDataInCache(
                                       _doTaskRequest.CompanyId,
                                       _doTaskRequest.UserId,
                                       _doTaskRequest.DeviceId,
                                       _doTaskRequest.DeviceType,
                                       strDatasetId,
                                       dsDataForResponse
                                       );

                                totalBlocks = dsDataForResponse.Tables[0].Rows.Count / _doTaskRequest.RcdPagingCount;
                                if (dsDataForResponse.Tables[0].Rows.Count % _doTaskRequest.RcdPagingCount != 0)
                                    totalBlocks += 1;

                                string key = CacheManager.GetKey(CacheManager.CacheType.mFicientDSPaging, _doTaskRequest.CompanyId,
                                        _doTaskRequest.UserId, _doTaskRequest.DeviceId, _doTaskRequest.DeviceType, String.Empty, String.Empty, strDatasetId);
                                dsDataForResponse = ProcessPagingHelpers.processGetResponseDS(key, 1, _doTaskRequest.RcdPagingCount);

                            }
                            catch (MficientException ex)
                            {
                                dsDataForResponse = null;
                            }

                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                strDatasetId, (totalBlocks).ToString());
                        }
                        else
                        {
                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                String.Empty,
                                String.Empty);
                        }
                    }
            }
            return null;

        }

        DoTaskResp MySqlDtTask()
        {
            switch (dbCommandType)
            {
                case DatabaseCommandType.SELECT:
                    {
                        dsDataForResponse = MySqlDoTaskClient.SelectDataFromDoTask(
                                               this, _doTaskRequest.Parameters, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd
                                               );
                        string strDatasetId = string.Empty;
                        int totalBlocks = 0;
                        if (_doTaskRequest.RcdPagingCount > 0)
                        {
                            try
                            {
                                strDatasetId = getDatasetIdForpaging(_doTaskRequest.UserId,
                                        _doTaskRequest.DeviceId,
                                        _doTaskRequest.DeviceType);

                                this.savePagedDataInCache(
                                       _doTaskRequest.CompanyId,
                                       _doTaskRequest.UserId,
                                       _doTaskRequest.DeviceId,
                                       _doTaskRequest.DeviceType,
                                       strDatasetId,
                                       dsDataForResponse
                                       );

                                totalBlocks = dsDataForResponse.Tables[0].Rows.Count / _doTaskRequest.RcdPagingCount;
                                if (dsDataForResponse.Tables[0].Rows.Count % _doTaskRequest.RcdPagingCount != 0)
                                    totalBlocks += 1;
                                string key = CacheManager.GetKey(CacheManager.CacheType.mFicientDSPaging, _doTaskRequest.CompanyId,
                                                                       _doTaskRequest.UserId, _doTaskRequest.DeviceId, _doTaskRequest.DeviceType, String.Empty, String.Empty, strDatasetId);
                                dsDataForResponse = ProcessPagingHelpers.processGetResponseDS(key, 1, _doTaskRequest.RcdPagingCount);

                            }
                            catch (MficientException ex)
                            {
                                dsDataForResponse = null;
                            }

                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                strDatasetId, (totalBlocks).ToString());
                        }
                        else
                        {
                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                String.Empty,
                                String.Empty);
                        }
                    }
                case DatabaseCommandType.INSERT:
                case DatabaseCommandType.UPDATE:
                case DatabaseCommandType.DELETE:
                    {
                        dsDataForResponse = new DataSet();
                        strOutputColumnName = string.Empty;

                        strQuerytResult = MySqlDoTaskClient.ExecuteScalarDotask(this, _doTaskRequest.Parameters, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd);

                        DataTable dtMsSql = new DataTable();

                        if (!string.IsNullOrEmpty(outputColName))
                        {
                            strOutputColumnName = outputColName.Split(',')[0];
                            dtMsSql.Columns.Add(strOutputColumnName);
                            DataRow row = dtMsSql.NewRow();
                            if (!string.IsNullOrEmpty(strOutputColumnName))
                            {
                                row[strOutputColumnName] = strQuerytResult;
                                dtMsSql.Rows.Add(row);
                                dsDataForResponse.Tables.Add(dtMsSql);
                            }
                        }

                        return getDBProcessResponse(dsDataForResponse, objRespStatus,
                            String.Empty,
                            String.Empty);
                    }
                case DatabaseCommandType.STORED_PROCEDURE:
                    {
                        dsDataForResponse = MySqlDoTaskClient.ExecuteStoredProcedureDoTask(
                                               this, _doTaskRequest.Parameters, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd
                                               );
                        string strDatasetId = string.Empty;
                        int totalBlocks = 0;
                        if (_doTaskRequest.RcdPagingCount > 0)
                        {
                            try
                            {
                                strDatasetId = getDatasetIdForpaging(_doTaskRequest.UserId,
                                        _doTaskRequest.DeviceId,
                                        _doTaskRequest.DeviceType);

                                this.savePagedDataInCache(
                                       _doTaskRequest.CompanyId,
                                       _doTaskRequest.UserId,
                                       _doTaskRequest.DeviceId,
                                       _doTaskRequest.DeviceType,
                                       strDatasetId,
                                       dsDataForResponse
                                       );

                                totalBlocks = dsDataForResponse.Tables[0].Rows.Count / _doTaskRequest.RcdPagingCount;
                                if (dsDataForResponse.Tables[0].Rows.Count % _doTaskRequest.RcdPagingCount != 0)
                                    totalBlocks += 1;
                                string key = CacheManager.GetKey(CacheManager.CacheType.mFicientDSPaging, _doTaskRequest.CompanyId,
                                       _doTaskRequest.UserId, _doTaskRequest.DeviceId, _doTaskRequest.DeviceType, String.Empty, String.Empty, strDatasetId);
                                dsDataForResponse = ProcessPagingHelpers.processGetResponseDS(key, 1, _doTaskRequest.RcdPagingCount);

                            }
                            catch (MficientException ex)
                            {
                                dsDataForResponse = null;
                            }

                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                strDatasetId, (totalBlocks).ToString());
                        }
                        else
                        {
                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                String.Empty,
                                String.Empty);
                        }
                    }
            }
            return null;

        }

        DoTaskResp OracleDtTask()
        {
            switch (dbCommandType)
            {
                case DatabaseCommandType.SELECT:
                    {
                        dsDataForResponse = OracleDoTaskClient.SelectDataFromDoTask(
                                               this, _doTaskRequest.Parameters, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd
                                               );
                        string strDatasetId = string.Empty;
                        int totalBlocks = 0;
                        if (_doTaskRequest.RcdPagingCount > 0)
                        {
                            try
                            {
                                strDatasetId = getDatasetIdForpaging(_doTaskRequest.UserId,
                                        _doTaskRequest.DeviceId,
                                        _doTaskRequest.DeviceType);

                                this.savePagedDataInCache(
                                       _doTaskRequest.CompanyId,
                                       _doTaskRequest.UserId,
                                       _doTaskRequest.DeviceId,
                                       _doTaskRequest.DeviceType,
                                       strDatasetId,
                                       dsDataForResponse
                                       );

                                totalBlocks = dsDataForResponse.Tables[0].Rows.Count / _doTaskRequest.RcdPagingCount;
                                if (dsDataForResponse.Tables[0].Rows.Count % _doTaskRequest.RcdPagingCount != 0)
                                    totalBlocks += 1;
                                string key = CacheManager.GetKey(CacheManager.CacheType.mFicientDSPaging, _doTaskRequest.CompanyId,
                                       _doTaskRequest.UserId, _doTaskRequest.DeviceId, _doTaskRequest.DeviceType, String.Empty, String.Empty, strDatasetId);
                                dsDataForResponse = ProcessPagingHelpers.processGetResponseDS(key, 1, _doTaskRequest.RcdPagingCount);

                            }
                            catch (MficientException ex)
                            {
                                dsDataForResponse = null;
                            }

                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                strDatasetId, (totalBlocks).ToString());
                        }
                        else
                        {
                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                String.Empty,
                                String.Empty);
                        }
                    }
                case DatabaseCommandType.INSERT:
                case DatabaseCommandType.UPDATE:
                case DatabaseCommandType.DELETE:
                    {
                        dsDataForResponse = new DataSet();
                        strOutputColumnName = string.Empty;


                        strQuerytResult = OracleDoTaskClient.ExecuteScalarDotask(this, _doTaskRequest.Parameters, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd);

                        DataTable dtMsSql = new DataTable();

                        if (!string.IsNullOrEmpty(outputColName))
                        {
                            strOutputColumnName = outputColName.Split(',')[0];
                            dtMsSql.Columns.Add(strOutputColumnName);
                            DataRow row = dtMsSql.NewRow();
                            if (!string.IsNullOrEmpty(strOutputColumnName))
                            {
                                row[strOutputColumnName] = strQuerytResult;
                                dtMsSql.Rows.Add(row);
                                dsDataForResponse.Tables.Add(dtMsSql);
                            }
                        }

                        return getDBProcessResponse(dsDataForResponse, objRespStatus,
                            String.Empty,
                            String.Empty);
                    }
                case DatabaseCommandType.STORED_PROCEDURE:
                    {
                        dsDataForResponse = OracleDoTaskClient.ExecuteStoredProcedureDoTask(
                                               this, _doTaskRequest.Parameters, _doTaskRequest.DataCredential.unm, _doTaskRequest.DataCredential.pwd
                                               );
                        string strDatasetId = string.Empty;
                        int totalBlocks = 0;
                        if (_doTaskRequest.RcdPagingCount > 0)
                        {
                            try
                            {
                                strDatasetId = getDatasetIdForpaging(_doTaskRequest.UserId,
                                        _doTaskRequest.DeviceId,
                                        _doTaskRequest.DeviceType);

                                this.savePagedDataInCache(
                                       _doTaskRequest.CompanyId,
                                       _doTaskRequest.UserId,
                                       _doTaskRequest.DeviceId,
                                       _doTaskRequest.DeviceType,
                                       strDatasetId,
                                       dsDataForResponse
                                       );

                                totalBlocks = dsDataForResponse.Tables[0].Rows.Count / _doTaskRequest.RcdPagingCount;
                                if (dsDataForResponse.Tables[0].Rows.Count % _doTaskRequest.RcdPagingCount != 0)
                                    totalBlocks += 1;
                                string key = CacheManager.GetKey(CacheManager.CacheType.mFicientDSPaging, _doTaskRequest.CompanyId,
                                                                       _doTaskRequest.UserId, _doTaskRequest.DeviceId, _doTaskRequest.DeviceType, String.Empty, String.Empty, strDatasetId);
                                dsDataForResponse = ProcessPagingHelpers.processGetResponseDS(key, 1, _doTaskRequest.RcdPagingCount);

                            }
                            catch (MficientException ex)
                            {
                                dsDataForResponse = null;
                            }

                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                strDatasetId, (totalBlocks).ToString());
                        }
                        else
                        {
                            return getDBProcessResponse(dsDataForResponse,
                                objRespStatus,
                                String.Empty,
                                String.Empty);
                        }
                    }
            }
            return null;

        }

        string getDatasetIdForpaging(string userId, string deviceId, string deviceTYpe)
        {
            return Utilities.GetMd5Hash(
                  DateTime.UtcNow.Ticks +
                  _doTaskRequest.UserId +
                  _doTaskRequest.DeviceId +
                  _doTaskRequest.DeviceType
                                  ).Substring(0, 16);
        }

        DoTaskResp getDBProcessResponse(DataSet ds, ResponseStatus respsts, string datasetId, string totalPages)
        {
            ResponseStatus objRespStatus = respsts;
            objRespStatus.cd = "0";
            objRespStatus.desc = String.Empty;
            try
            {
                return new DoTaskResp(objRespStatus, _doTaskRequest.RequestId, datasetId, totalPages,
                        ds, _doTaskRequest.ControlId, cacheType, cacheFrequency, cacheCondition, mPluginAgent, imageCols);
            }
            catch
            {
                throw new Exception(((int)DO_TASK_ERROR.INVALID_COLUMN_IN_DATA_COMMAND).ToString());
            }
        }

        #endregion

        #region Save the dataset in cache

        void savePagedDataInCache(string companyId, string userId, string deviceId,
            string deviceType, string datasetId, object value)
        {
            string strCacheKey =
                CacheManager.GetKey(CacheManager.CacheType.mFicientDSPaging,
                companyId, userId,
                deviceId, deviceType,
                String.Empty, String.Empty,
                datasetId);

            if (value.GetType() == typeof(String))
            {
                CacheManager.Insert<string>(strCacheKey,
                    value, DateTime.UtcNow,
                    TimeSpan.FromSeconds(MficientConstants.DATASET_PAGING_VALIDITY_SECONDS));
            }
            else
            {
                CacheManager.Insert<DataSet>(strCacheKey,
                    value, DateTime.UtcNow,
                    TimeSpan.FromSeconds(MficientConstants.DATASET_PAGING_VALIDITY_SECONDS));
            }
        }

        #endregion
    }
}
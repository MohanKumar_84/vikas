﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientWS
{
    public class MbuzzPushMessageSave
    {
        MbuzzPushMessageSaveReq pushMessageforContactStatusReq;
        //DataTable dtUsersList;

        public MbuzzPushMessageSave(MbuzzPushMessageSaveReq _pushMessageforContactStatusReq)
        {
            pushMessageforContactStatusReq = _pushMessageforContactStatusReq;
        }

        public MbuzzPushMessageSaveResp Process()
        {
            SqlConnection con = new SqlConnection();
            ResponseStatus objRespStatus = new ResponseStatus();
            try
            {
                SqlCommand cmd = new SqlCommand(@"select DEVICE_PUSH_MESSAGE_ID,DEVICE_TYPE,DEVICE_ID from TBL_REGISTERED_DEVICE
                inner join TBL_USER_DETAIL on TBL_REGISTERED_DEVICE.USER_ID=TBL_USER_DETAIL.USER_ID where TBL_REGISTERED_DEVICE.COMPANY_ID=@COMPANY_ID and TBL_USER_DETAIL.USER_NAME=@Username;
                select FIRST_NAME as FROM_USER_NAME from TBL_USER_DETAIL where COMPANY_ID=@COMPANY_ID and USER_NAME=@FROM_USER_NAME;");
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", pushMessageforContactStatusReq.CompanyId);
                cmd.Parameters.AddWithValue("@Username", pushMessageforContactStatusReq.Username);
                cmd.Parameters.AddWithValue("@FROM_USER_NAME", pushMessageforContactStatusReq.FromUser);
                DataSet DdsDeviceDetail = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);

                if (DdsDeviceDetail != null && DdsDeviceDetail.Tables[0].Rows.Count > 0)
                {
                    string FromUser = DdsDeviceDetail.Tables[1].Rows[0]["FROM_USER_NAME"].ToString();
                    foreach (DataRow dr in DdsDeviceDetail.Tables[0].Rows)
                    {
                        MSSqlDatabaseClient.SqlConnectionOpen(out con, MSSqlDatabaseClient.getConnectionStringFromWebConfig(
                        MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MGRAM));

                        mFicientCommonProcess.SendNotification.SaveNotificationTable(con, null,pushMessageforContactStatusReq.CompanyId, pushMessageforContactStatusReq.Username,
                        pushMessageforContactStatusReq.FromUser,pushMessageforContactStatusReq.Source,(mFicientCommonProcess.NOTIFICATION_TYPE)pushMessageforContactStatusReq.PushMessageType,
                        Convert.ToString(dr["DEVICE_PUSH_MESSAGE_ID"]), Convert.ToString(dr["DEVICE_TYPE"]), Convert.ToString(dr["DEVICE_ID"]), "", pushMessageforContactStatusReq.BadgeCount);
                    }
                    //                        if (!string.IsNullOrEmpty(dr["DEVICE_PUSH_MESSAGE_ID"].ToString()))
                    //                        {
                    //                            cmd = new SqlCommand(@"IF EXISTS (
                    //                                                    SELECT * FROM TBL_PUSHMESSAGE_OUTBOX
                    //			                                        WHERE USERNAME = @USERNAME
                    //			                                        AND FROM_USER_FIRSTNAME = @FROM_USER_FIRSTNAME
                    //			                                        AND [SOURCE]=@SOURCE
                    //			                                        AND ENTERPRISE_ID = @ENTERPRISE_ID 
                    //                                                    AND NOTIFICATION_TYPE = @NOTIFICATION_TYPE)
                    //
                    //                                                    UPDATE TBL_PUSHMESSAGE_OUTBOX 
                    //                                                    SET BADGE_COUNT = @BADGE_COUNT,
                    //                                                    PUSH_DATETIME	 = 	@PUSH_DATETIME
                    //                                                    WHERE  USERNAME = @USERNAME
                    //                                                    AND FROM_USER_FIRSTNAME = @FROM_USER_FIRSTNAME
                    //                                                    AND [SOURCE]=@SOURCE
                    //                                                    AND ENTERPRISE_ID = @ENTERPRISE_ID 
                    //                                                    AND NOTIFICATION_TYPE = @NOTIFICATION_TYPE
                    //                                                    AND [DEVICE_TOKEN_ID]=@DEVICE_TOKEN_ID
                    //                                                    AND DEVICE_TYPE = @DEVICE_TYPE 
                    //                                                    AND DEVICE_ID = @DEVICE_ID
                    //                                                    
                    //                                                    ELSE
                    //
                    //                                                    INSERT INTO TBL_PUSHMESSAGE_OUTBOX 
                    //                                                    ([PUSH_NOTIFICATION_ID] ,[ENTERPRISE_ID] ,[SOURCE],
                    //                                                    [BADGE_COUNT],[NOTIFICATION_TYPE],[FROM_USER_FIRSTNAME],
                    //                                                    [USERNAME],[PUSH_DATETIME],[DEVICE_TOKEN_ID],[DEVICE_TYPE],[DEVICE_ID],[INFO])
                    //                                                    VALUES(@PUSH_NOTIFICATION_ID,@ENTERPRISE_ID, 
                    //                                                    @SOURCE ,@BADGE_COUNT, @NOTIFICATION_TYPE, 
                    //                                                    @FROM_USER_FIRSTNAME,@USERNAME,@PUSH_DATETIME,@DEVICE_TOKEN_ID,@DEVICE_TYPE,@DEVICE_ID,'')");
                    //                            cmd.CommandType = CommandType.Text;

                    //                            cmd.Parameters.AddWithValue("@PUSH_NOTIFICATION_ID", Utilities.GetMd5Hash(pushMessageforContactStatusReq.CompanyId + pushMessageforContactStatusReq.Username + dr["DEVICE_PUSH_MESSAGE_ID"] + DateTime.UtcNow.Ticks.ToString()));
                    //                            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", pushMessageforContactStatusReq.CompanyId);
                    //                            cmd.Parameters.AddWithValue("@SOURCE", pushMessageforContactStatusReq.Source);
                    //                            cmd.Parameters.AddWithValue("@BADGE_COUNT", pushMessageforContactStatusReq.BadgeCount);
                    //                            cmd.Parameters.AddWithValue("@USERNAME", pushMessageforContactStatusReq.Username);
                    //                            cmd.Parameters.AddWithValue("@NOTIFICATION_TYPE", pushMessageforContactStatusReq.PushMessageType);
                    //                            cmd.Parameters.AddWithValue("@FROM_USER_FIRSTNAME", FromUser);
                    //                            cmd.Parameters.AddWithValue("@PUSH_DATETIME", DateTime.UtcNow.Ticks);
                    //                            cmd.Parameters.AddWithValue("@DEVICE_TOKEN_ID", Convert.ToString(dr["DEVICE_PUSH_MESSAGE_ID"]));
                    //                            cmd.Parameters.AddWithValue("@DEVICE_TYPE", Convert.ToString(dr["DEVICE_TYPE"]));
                    //                            cmd.Parameters.AddWithValue("@DEVICE_ID", Convert.ToString(dr["DEVICE_ID"]));

                    //                            MSSqlDatabaseClient.ExecuteNonQueryRecord(cmd, MSSqlDatabaseClient.getConnectionStringFromWebConfig(MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MGRAM));
                    //                            break;
                    //                        }
                    //                    }
                }
                objRespStatus.cd = "0";
                objRespStatus.desc = "";
                //PushMessage(dtUsersList);
            }
            catch (Exception ex)
            {
                objRespStatus.cd = "2000201";
                objRespStatus.desc = "Error";
                if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            }
            finally
            {
                MSSqlDatabaseClient.SqlConnectionClose(con);
            }
            return new MbuzzPushMessageSaveResp(objRespStatus,pushMessageforContactStatusReq.RequestId);
        }

        //public bool PushMessage(DataTable usersLists)
        //{ 
        //    //call webservice from pushmessage server
        //    return true;
        //}
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientWS
{
    public class MSSqlDatabaseClient
    {
        public enum CONNECTION_STRING_FOR_DB
        {
            MFICIENT,
            MGRAM,
            ADMINDB
        }
        /// <summary>
        /// Description :Select Data According To SqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static DataSet SelectDataFromSqlCommand(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            try
            {
                SqlConnectionOpen(out objSqlConnection);
                _SqlCommand.Connection = objSqlConnection;

                DataSet ObjDataSet = new DataSet();

                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

                objSqlDataAdapter.Fill(ObjDataSet);

                //SqlConnectionClose(objSqlConnection);

                return ObjDataSet;
            }
            catch(Exception ex)
            {
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }
        }
        public static DataSet SelectDataFromSQlCommand(string connectionString, SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            try
            {
                SqlConnectionOpen(out objSqlConnection, connectionString);
                _SqlCommand.Connection = objSqlConnection;
                DataSet ObjDataSet = new DataSet();
                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);
                objSqlDataAdapter.Fill(ObjDataSet);
                return ObjDataSet;
            }
            catch
            {
                return null;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }

        }
        public static DataSet SelectDataFromSQlCommand(SqlConnection  con, SqlCommand _SqlCommand)
        {
            try
            {
                _SqlCommand.Connection = con;
                DataSet ObjDataSet = new DataSet();
                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);
                objSqlDataAdapter.Fill(ObjDataSet);
                return ObjDataSet;
            }
            catch
            {
                return null;
            }
            finally
            {
                //SqlConnectionClose(objSqlConnection);
            }

        }
        
        /// <summary>
        /// Use it for transaction.It is used without any try catch block connection is not closes on error
        /// </summary>
        /// <param name="con"></param>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public static DataSet SelectDataWithOpenCon(SqlCommand cmdWithConnection)
        {
            DataSet ObjDataSet = new DataSet();

            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(cmdWithConnection);

            objSqlDataAdapter.Fill(ObjDataSet);

            return ObjDataSet;
        }

        /// <summary>
        /// Use it for transaction.It is used without any try catch block connection is not closes on error
        /// </summary>
        /// <param name="con"></param>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public static DataSet SelectDataWithOpenConForDoTask(SqlCommand cmdWithConnection, List<QueryParameters> parameters)
        {
            //Every time add new set of parameter.It is possible that the cmd is already used
            //and already has parameters with same name.hence this is required
            cmdWithConnection.Parameters.Clear();
            DataSet ObjDataSet = new DataSet();

            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(cmdWithConnection);

            foreach (QueryParameters param in parameters)
            {
                cmdWithConnection.Parameters.AddWithValue("@" + param.para, param.val);
            }

            objSqlDataAdapter.Fill(ObjDataSet);

            return ObjDataSet;
        }


        /// <summary>
        /// Description :SqlConnection Open Function
        /// </summary>
        public static void SqlConnectionOpen(out SqlConnection Conn)
        {
            try
            {
                Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }

        public static void SqlConnectionOpen(out SqlConnection Conn, string ConnectionString)
        {
            try
            {
                Conn = new SqlConnection(ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            } catch (SqlException qslex)
            {
                Conn = null;
                throw qslex;
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }

        /// <summary>
        /// Description :SqlConnection Close Function
        /// </summary>
        public static void SqlConnectionClose(SqlConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open))
                {
                    Conn.Close();
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Description :SqlConnection Open Function
        /// </summary>
        public static void SqlConnectionOpenAdmin(out SqlConnection Conn)
        {
            try
            {
                Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }


        /// <summary>
        /// Description :Select Data According To SqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static DataSet SelectDataFromSQlCommandAdminData(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            SqlConnectionOpenAdmin(out objSqlConnection);
            _SqlCommand.Connection = objSqlConnection;

            DataSet ObjDataSet = new DataSet();

            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

            objSqlDataAdapter.Fill(ObjDataSet);

            SqlConnectionClose(objSqlConnection);

            return ObjDataSet;
        }

        /// <summary>
        /// Description :Data Process (Insert, Update, Delete ) According To SqlCommand ( Sql Query And Parameter ) Admin Database
        /// </summary>
        public static int ExecuteNonQueryRecordAdmin(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            int i = -1;
            try
            {
                SqlConnectionOpenAdmin(out objSqlConnection);

                _SqlCommand.Connection = objSqlConnection;

                i = _SqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }

            return i;
        }

        public static int ExecuteNonQueryRecord(SqlCommand _SqlCommand, string _connectionString)
        {
            SqlConnection objSqlConnection = null;
            int i = -1;
            try
            {
                SqlConnectionOpen(out objSqlConnection, _connectionString);
                _SqlCommand.Connection = objSqlConnection;
                i = _SqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }

            return i;
        }

        /// <summary>
        /// Description :Data Process (Insert, Update, Delete ) According To SqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static int ExecuteNonQueryRecord(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            int i = -1;
            try
            {
                SqlConnectionOpen(out objSqlConnection);
                _SqlCommand.Connection = objSqlConnection;

                 i = _SqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }

            return i;
        }


        public static int ExecuteNonQueryWithOpenCon(SqlCommand cmdWithCon, List<QueryParameters> parameters)
        {
            //Every time add new set of parameter.It is possible that the cmd is already used
            //and already has parameters with same name.hence this is required
            cmdWithCon.Parameters.Clear();
            foreach (QueryParameters param in parameters)
            {
                cmdWithCon.Parameters.AddWithValue("@" + param.para, param.val);
            }
            return cmdWithCon.ExecuteNonQuery();
        }
       

        public static string getConnectionStringFromWebConfig(CONNECTION_STRING_FOR_DB db)
        {
            string strConnectionString = String.Empty;
            switch (db)
            {
                case CONNECTION_STRING_FOR_DB.MFICIENT:
                case CONNECTION_STRING_FOR_DB.ADMINDB:
                    strConnectionString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
                    break;
                case CONNECTION_STRING_FOR_DB.MGRAM:
                    strConnectionString = ConfigurationManager.ConnectionStrings["ConStrmGramDB"].ConnectionString;
                    break;
                //case CONNECTION_STRING_FOR_DB.ADMINDB:
                //    strConnectionString = ConfigurationManager.ConnectionStrings["ServerDtlsConString"].ConnectionString;
                //    break;
            }
            return strConnectionString;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class GetQueryTypeResp
    {
        ResponseStatus _respStatus;
        List<QueryTypeList> _qlst;
        string _requestId;
        public GetQueryTypeResp(ResponseStatus respStatus, List<QueryTypeList> qlst,string requestId)
        {
            _respStatus = respStatus;
            _qlst = qlst;
            _requestId = requestId;
        }
        public string GetResponseJson()
        {
            GetQueryTypeResponse objGetQueryTypeResp = new GetQueryTypeResponse();
            objGetQueryTypeResp.func = Convert.ToString((int)FUNCTION_CODES.GET_QUERY_TYPE);
            objGetQueryTypeResp.rid = _requestId;
            objGetQueryTypeResp.status = _respStatus;
            GetQueryTypeRespData data = new GetQueryTypeRespData();
            data.qlst = _qlst;
            objGetQueryTypeResp.data = data;
            string strJsonWithDetails = Utilities.SerializeJson<GetQueryTypeResponse>(objGetQueryTypeResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }
    public class GetQueryTypeResponse : CommonResponse
    {
        public GetQueryTypeResponse()
        { }
        //[DataMember]
        //public List<FormListResponseData> data { get; set; }
        [DataMember]
        public GetQueryTypeRespData data { get; set; }
    }

    public class GetQueryTypeRespData
    {
        /// <summary>
        /// Menu Category Details
        /// </summary>
        [DataMember]
        public List<QueryTypeList> qlst { get; set; }
    }
    public class QueryTypeList
    {
        [DataMember]
        public string qtyp { get; set; }

        [DataMember]
        public string qcd { get; set; }
    }
}
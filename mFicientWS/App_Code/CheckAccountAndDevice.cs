﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class CheckAccountAndDevice
    {
        CheckAccountAndDeviceReq objReq;
        public CheckAccountAndDevice(CheckAccountAndDeviceReq _objReq)
        {
            objReq = _objReq;
        }
        public CheckAccountAndDeviceResp Process()
        {
            DataSet dsUserDtls = getLoginDtlsOfUserByUserName();
            ResponseStatus objRespStatus = new ResponseStatus();

            if (Convert.ToBoolean(dsUserDtls.Tables[0].Rows[0]["IS_BLOCKED"]))
            {
                objRespStatus.cd = "1200401";
                objRespStatus.desc = "Account is Blocked.";
            }
            else if (String.IsNullOrEmpty(Convert.ToString(dsUserDtls.Tables[0].Rows[0]["MFICIENT_KEY"])))
            {
                if (Convert.ToInt64(dsUserDtls.Tables[0].Rows[0]["request_Datetime"]) > 0)
                {
                    objRespStatus.cd = "1200402";
                    objRespStatus.desc = "Registration request is pending.";
                }
                else if (Convert.ToInt64(dsUserDtls.Tables[0].Rows[0]["DENIED_DATETIME"]) > 0)
                {
                    objRespStatus.cd = "1200403";
                    objRespStatus.desc = "Registration request is Denied.";
                }
                else
                {
                    objRespStatus.cd = "1200404";
                    objRespStatus.desc = "Device is not registered. ";
                }
            }
            else
            {
                objRespStatus.cd = "0";
                objRespStatus.desc = "";
            }
            return new CheckAccountAndDeviceResp(objRespStatus,objReq.RequestId);
        }

        DataSet getLoginDtlsOfUserByUserName()
        {
            string strQuery = @"SELECT  usr.*,rd.MFICIENT_KEY,rdr.request_Datetime,drd.DENIED_DATETIME
                            FROM TBL_USER_DETAIL AS usr LEFT OUTER JOIN TBL_REGISTERED_DEVICE AS rd
                            ON usr.USER_ID = rd.USER_ID and rd.DEVICE_ID=@DEVICE_ID and rd.DEVICE_TYPE=@DEVICE_TYPE
                            LEFT OUTER JOIN TBL_DEVICE_REGISTRATION_REQUEST AS rdr
                            ON usr.USER_ID = rdr.USER_ID and rdr.DEVICE_ID=@DEVICE_ID and rdr.DEVICE_TYPE=@DEVICE_TYPE
                            LEFT OUTER JOIN  TBL_DEVICE_REGISTRATION_REQ_DENIED as drd
                            ON usr.USER_ID = drd.USER_ID and drd.DEVICE_ID=@DEVICE_ID and drd.DEVICE_TYPE=@DEVICE_TYPE
                            WHERE usr.USER_ID =@USER_ID AND usr.COMPANY_ID =  @COMPANY_ID AND usr.IS_ACTIVE = 1 ";

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", objReq.CompanyId);
            cmd.Parameters.AddWithValue("@USER_ID", objReq.UserId);
            cmd.Parameters.AddWithValue("@DEVICE_ID", objReq.DeviceId);
            cmd.Parameters.AddWithValue("@DEVICE_TYPE", objReq.DeviceType);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class Get_Menu_And_Workflows_Req
    {
        string _userId, _sessionId, _requestId, _deviceId, _deviceType, _companyId, _modelType;
        int _functionCode;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string SessionId
        {
            get { return _sessionId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        int FunctionCode
        {
            get { return _functionCode; }
        }
        public string ModelType
        {
            get { return _modelType; }
        }
        public Get_Menu_And_Workflows_Req(string requestJson, string userId)
        {
            RequestJsonParsing<GetMenuCategoryListRequestData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<GetMenuCategoryListRequestData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            _userId = userId;
            if (_functionCode != (int)FUNCTION_CODES.GET_MENU_AND_WORKFLOW_LIST && _functionCode != (int)FUNCTION_CODES.GET_MENU_AND_WORKFLOW_LIST_FOR_TESTER)
            {
                throw new Exception("Invalid Function Code");
            }
            _sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _modelType = objRequestJsonParsing.req.data.mtyp == null ? "0" : objRequestJsonParsing.req.data.mtyp;
            try
            {

                DEVICE_MODEL_TYPE eModelType = (DEVICE_MODEL_TYPE)Enum.Parse(typeof(DEVICE_MODEL_TYPE), _modelType);
                if (!Enum.IsDefined(typeof(DEVICE_MODEL_TYPE), eModelType))
                {
                    throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
                }
            }
            catch
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
        }
    }

    [DataContract]
    public class GetMenuCategoryListRequestData : Data
    {
        /// <summary>
        /// Model Type
        /// </summary>
        [DataMember]
        public string mtyp { get; set; }
    }


}
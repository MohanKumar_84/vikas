﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Threading;

namespace mFicientWS
{
    public class Block_Unblock
    {
        Block_UnblockReq _objBlock_UnblockReq;

        public Block_Unblock(Block_UnblockReq objBlock_UnblockReq)
        {
            _objBlock_UnblockReq = objBlock_UnblockReq;
        }


        public Block_UnblockResp Process()
        {
            ResponseStatus objRespStatus = new ResponseStatus();
            SqlConnection con;
            MSSqlDatabaseClient.SqlConnectionOpen(out con);
            try
            {
                SqlCommand cmd = new SqlCommand(@"update  TBL_USER_DETAIL set IS_BLOCKED =@Block WHERE USER_ID=@USERID and COMPANY_ID=@COMPANY_ID;", con);
                cmd.Parameters.AddWithValue("@USERID", _objBlock_UnblockReq.Mobileuserid);
                cmd.Parameters.AddWithValue("@Block", _objBlock_UnblockReq.Type);
                cmd.Parameters.AddWithValue("@COMPANY_ID", _objBlock_UnblockReq.CompanyId);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();

                mFicientCommonProcess.getUserDetail obj=new mFicientCommonProcess.getUserDetail(_objBlock_UnblockReq.Mobileuserid,_objBlock_UnblockReq.CompanyId,con,true);
                DataSet dsUserDetails = obj.Process();
                
                //GetMbuzzServerDetail objGetMbuzzServerDtl = new GetMbuzzServerDetail();
                //MFEmBuzzServer objMbuzzServer = objGetMbuzzServerDtl.getMbuzzServerForCompany(
                //_objBlock_UnblockReq.CompanyId, con);
                MSSqlDatabaseClient.SqlConnectionClose(con);
                //if (objGetMbuzzServerDtl.StatusCode != 0) throw new Exception();
                //if (!String.IsNullOrEmpty(objMbuzzServer.ServerIpAddress))
                //{
                //    if (_objBlock_UnblockReq.Type == 1)
                //    {
                //        //UserDeletedorBlockedRequest userDeletedorBlocked = new UserDeletedorBlockedRequest(Utilities.GetMsgRefID(), Convert.ToString(dsUserDetails.Tables[0].Rows[0]["USER_NAME"]), ((int)MessageCode.CONTACT_BLOCKED).ToString(), hfsPart4);
                //        string strReq = mFicientCommonProcess.Utility.UserDeletedorBlockedRequest(Convert.ToString(dsUserDetails.Tables[0].Rows[0]["USER_NAME"]), _objBlock_UnblockReq.Type.ToString(), _objBlock_UnblockReq.CompanyId);
                //        mBuzzClient objClient = new mBuzzClient(strReq, objMbuzzServer.ServerIpAddress, Convert.ToInt32(objMbuzzServer.ServerPort));
                //        ThreadPool.QueueUserWorkItem(new WaitCallback(objClient.Connect), null);
                //    }
                //    else if (_objBlock_UnblockReq.Type == 0)
                //    {
                //        string requestJson = mFicientCommonProcess.Utility.AddNewContactRequest(Utilities.GetMsgRefID(), dsUserDetails, _objBlock_UnblockReq.CompanyId, GetUserContactsForSendRequest(Convert.ToString(dsUserDetails.Tables[0].Rows[0]["REQUESTED_BY"])), _objBlock_UnblockReq.Type.ToString());
                //        mBuzzClient objClient = new mBuzzClient(requestJson, objMbuzzServer.ServerIpAddress, Convert.ToInt32(objMbuzzServer.ServerPort));
                //        ThreadPool.QueueUserWorkItem(new WaitCallback(objClient.Connect), null);
                //    }
                //}
                if (_objBlock_UnblockReq.Type == 1)
                {
                    SqlConnection mGramcon;
                    MSSqlDatabaseClient.SqlConnectionOpen(out mGramcon, MSSqlDatabaseClient.getConnectionStringFromWebConfig(
                        MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MGRAM));
                    try
                    {
                        foreach (DataRow dev in dsUserDetails.Tables[1].Rows)
                            mFicientCommonProcess.SendNotification.SaveNotificationTable(con, null, _objBlock_UnblockReq.CompanyId, Convert.ToString(dsUserDetails.Tables[0].Rows[0]["USER_NAME"]),"",0, mFicientCommonProcess.NOTIFICATION_TYPE.MF_USER_BLOCK, Convert.ToString(dev["DEVICE_PUSH_MESSAGE_ID"]), Convert.ToString(dev["DEVICE_TYPE"]), Convert.ToString(dev["DEVICE_ID"]), "",1);
                    }
                    catch
                    {

                    }
                    finally
                    {
                        MSSqlDatabaseClient.SqlConnectionClose(mGramcon);
                    }
                }
                objRespStatus.cd = "0";
                objRespStatus.desc = "";
            }
            catch (Exception ex)
            {
                objRespStatus.cd = "";
                objRespStatus.desc = "Error";
                if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    MSSqlDatabaseClient.SqlConnectionClose(con);
            }
            return new Block_UnblockResp(objRespStatus, _objBlock_UnblockReq.RequestId);

        }
        public string GetUserContactsForSendRequest(string RequestedBy)
        {
            string strQuery = @"select user_name from  tbl_user_detail a inner join tbl_User_group_link b on a.user_id=b.user_id where b.Group_id in ('" + RequestedBy.Replace(",", "','") + "')";
            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;

            DataSet objDataSet = MSSqlDatabaseClient.SelectDataFromSqlCommand(objSqlCommand);
            string Result = string.Empty;
            if (objDataSet != null && objDataSet.Tables.Count > 0 && objDataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in objDataSet.Tables[0].Rows)
                {
                    if (!string.IsNullOrEmpty(Result)) Result += ",";
                    Result += dr[0].ToString();
                }
            }
            return Result;

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class UserContactsResp
    {
        ResponseStatus _respStatus;
        string _requestId;
        List<UserData> _lstUsersDataDtls;

        public UserContactsResp(ResponseStatus respStatus, string requestId, List<UserData> lstUsersDataDtls)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
                _lstUsersDataDtls = lstUsersDataDtls;

            }
            catch (Exception ex)
            {
                throw new Exception("Error creating User Authentication Detail Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            UserContactsResponse objUserContactsDtlsResp = new UserContactsResponse();
            objUserContactsDtlsResp.func = Convert.ToString((int)FUNCTION_CODES.USER_CONTACTS);
            objUserContactsDtlsResp.rid = _requestId;
            objUserContactsDtlsResp.status = _respStatus;
            UserContactsResponseData objResponseData = new UserContactsResponseData();            
            objResponseData.users = _lstUsersDataDtls;
            objUserContactsDtlsResp.data = objResponseData;
            string strJsonWithDetails = Utilities.SerializeJson<UserContactsResponse>(objUserContactsDtlsResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class UserContactsResponse : CommonResponse
    {
        public UserContactsResponse()
        {
        }
        [DataMember]
        public UserContactsResponseData data { get; set; }
    }
    public class UserContactsResponseData
    {
        /// Users Data
        /// </summary>
        [DataMember]
        public List<UserData> users { get; set; }
    }

}
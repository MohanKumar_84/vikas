﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
namespace mFicientWS
{
    public class GetMPlugInConnectionDetails
    {
        GetMPlugInConnectionDetailsReq _getMPlugInConnDtlsReq;
        public GetMPlugInConnectionDetails(GetMPlugInConnectionDetailsReq getMPlugInConnDtlsReq)
        {
            _getMPlugInConnDtlsReq = getMPlugInConnDtlsReq;
        }
        public GetMPlugInConnectionDetailsResp Process()
        {
            try
            {
                DataSet dsMPlugInConnectionDetails = getMPlugInConnectionDetails();
                Newtonsoft.Json.Linq.JObject s3Bucket = new Newtonsoft.Json.Linq.JObject();
                if (dsMPlugInConnectionDetails == null
                    || dsMPlugInConnectionDetails.Tables[0] == null
                    || dsMPlugInConnectionDetails.Tables[1] == null || dsMPlugInConnectionDetails.Tables[2] == null)
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());

                List<MPlugInDatabaseConnection> lstDbConnectionDetails = new List<MPlugInDatabaseConnection>();
                foreach (DataRow row in dsMPlugInConnectionDetails.Tables[0].Rows)
                {
                    MPlugInDatabaseConnection objMPlugInDBConn = new MPlugInDatabaseConnection();
                    objMPlugInDBConn.connid = Convert.ToString(row["DB_CONNECTOR_ID"]);
                    objMPlugInDBConn.dbtyp = Convert.ToString(row["DATABASE_TYPE"]);
                    objMPlugInDBConn.host = Convert.ToString(row["HOST_NAME"]);
                    objMPlugInDBConn.dbname = Convert.ToString(row["DATABASE_NAME"]);//decrypt string
                    objMPlugInDBConn.uid = Convert.ToString(row["USER_ID"]);//decrypt string
                    objMPlugInDBConn.pwd = Convert.ToString(row["PASSWORD"]);//decrypt string
                    objMPlugInDBConn.timeout = Convert.ToString(row["TIME_OUT"]);
                    objMPlugInDBConn.addstring = Convert.ToString(row["ADDITIONAL_STRING"]);
                    lstDbConnectionDetails.Add(objMPlugInDBConn);
                }
                List<MPlugInWSConnection> lstWSConnectionDetails = new List<MPlugInWSConnection>();
                foreach (DataRow row in dsMPlugInConnectionDetails.Tables[1].Rows)
                {
                    MPlugInWSConnection objMPlugInWSConn = new MPlugInWSConnection();
                    objMPlugInWSConn.connid = Convert.ToString(row["WS_CONNECTOR_ID"]);

                    if (Convert.ToString(row["WEBSERVICE_TYPE"]).ToString().ToUpper() == "HTTP")
                        objMPlugInWSConn.wstyp = "1";
                    else if (Convert.ToString(row["WEBSERVICE_TYPE"]).ToString().ToUpper() == "WSDL")
                        objMPlugInWSConn.wstyp = "3";
                    else
                        objMPlugInWSConn.wstyp = "4";

                    objMPlugInWSConn.url = Convert.ToString(row["WEBSERVICE_URL"]);
                    objMPlugInWSConn.uid = Convert.ToString(row["USER_NAME"]);
                    objMPlugInWSConn.pwd = Convert.ToString(row["PASSWORD"]);
                    objMPlugInWSConn.authtype = Convert.ToString(row["AUTHENTICATION_TYPE"]);
                    lstWSConnectionDetails.Add(objMPlugInWSConn);
                }
                List<MPlugInODataConnection> lstODataConnectionDetails = new List<MPlugInODataConnection>();
                foreach (DataRow row in dsMPlugInConnectionDetails.Tables[2].Rows)
                {
                    MPlugInODataConnection objMPlugInODataConn = new MPlugInODataConnection();
                    objMPlugInODataConn.connid = Convert.ToString(row["ODATA_CONNECTOR_ID"]);
                    objMPlugInODataConn.endpnt = Convert.ToString(row["ODATA_ENDPOINT"]);
                    objMPlugInODataConn.uid = Convert.ToString(row["USER_NAME"]);
                    objMPlugInODataConn.pwd = Convert.ToString(row["PASSWORD"]);
                    objMPlugInODataConn.ver = Convert.ToString(row["VERSION"]);
                    objMPlugInODataConn.authtype = Convert.ToString(row["AUTHENTICATION_TYPE"]);
                    lstODataConnectionDetails.Add(objMPlugInODataConn);
                }
                try
                {
                    DataSet dsBucketInfo = Utilities.GetS3BucketDetails(BucketType.MPLUGIN_AGENT);
                    if (dsBucketInfo != null && dsBucketInfo.Tables.Count > 0)
                    {
                        if (dsBucketInfo.Tables[0] != null && dsBucketInfo.Tables[0].Rows.Count > 0)
                        {
                            s3Bucket.Add("aid", dsBucketInfo.Tables[0].Rows[0]["USER_NAME"].ToString());
                            s3Bucket.Add("ak", dsBucketInfo.Tables[0].Rows[0]["ACCESS_KEY"].ToString());
                            s3Bucket.Add("sak", dsBucketInfo.Tables[0].Rows[0]["SECRET_ACCESS_KEY"].ToString());
                        }
                    }
                }
                catch (Exception ex)
                {                
                }

                return new GetMPlugInConnectionDetailsResp(null, _getMPlugInConnDtlsReq.RequestId, lstDbConnectionDetails, lstWSConnectionDetails, lstODataConnectionDetails, s3Bucket);
                //if (dsMPlugInConnectionDetails != null && dsMPlugInConnectionDetails.Tables[0] != null 
                //    && dsMPlugInConnectionDetails.Tables[1] != null && dsMPlugInConnectionDetails.Tables.Count > 0)
                //{
                 //}
                //else
                //{
                //    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                //}
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        DataSet getMPlugInConnectionDetails()
        {
            string strQuery = @"SELECT DBConn.*,DBConn.COMPANY_ID FROM TBL_DATABASE_CONNECTION AS DBConn
                                WHERE DBConn.COMPANY_ID = @CompanyId and  DBConn.Mplugin_AGENT=@MP_AGENT_NAME;

                                SELECT WSConn.*,WSConn.COMPANY_ID FROM TBL_WEBSERVICE_CONNECTION AS WSConn
                                WHERE WSConn.COMPANY_ID = @CompanyId and  WSConn.Mplugin_AGENT=@MP_AGENT_NAME;

                                SELECT ODataConn.*,ODataConn.COMPANY_ID FROM TBL_ODATA_CONNECTION AS ODataConn
                                WHERE ODataConn.COMPANY_ID = @CompanyId and  ODataConn.MPLUGIN_AGENT=@MP_AGENT_NAME;";

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@CompanyId", _getMPlugInConnDtlsReq.CompanyId);
            cmd.Parameters.AddWithValue("@MP_AGENT_NAME", _getMPlugInConnDtlsReq.MpluginName);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }

    }
}
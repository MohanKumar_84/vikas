﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class ChangePasswordReq
    {
        string _oldPassword, _newPassword, _userId, _sessionId, _requestId, _deviceId, _deviceType, _companyId, _domainName;
        int _functionCode;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string SessionId
        {
            get { return _sessionId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string NewPassword
        {
            get { return _newPassword; }
        }
        public string OldPassword
        {
            get { return _oldPassword; }
        }
        public string DomainName
        {
            get { return _domainName; }
        }
        public ChangePasswordReq(string requestJson, string userId ,string domainName)
        {
            RequestJsonParsing<ChangePasswordReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<ChangePasswordReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.CHANGE_PASSWORD)
            {
                throw new Exception("Invalid Function Code");
            }
            _companyId = objRequestJsonParsing.req.data.enid;
            _domainName = domainName;
            _oldPassword = EncryptionDecryption.AESDecrypt(_companyId, objRequestJsonParsing.req.data.pwd);
            _newPassword = EncryptionDecryption.AESDecrypt(_companyId, objRequestJsonParsing.req.data.npwd);
            _userId = userId;
            _sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            if (String.IsNullOrEmpty(_oldPassword) || String.IsNullOrEmpty(_newPassword))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }

        }

    }

    [DataContract]
    public class ChangePasswordReqData : Data
    {
        /// <summary>
        /// Old Password
        /// </summary>
        [DataMember]
        public string pwd { get; set; }

        /// <summary>
        /// New Password
        /// </summary>
        [DataMember]
        public string npwd { get; set; }

    }
}
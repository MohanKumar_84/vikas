﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientWS
{
    public class Get_Offline_Data__Objects
    {
        Get_Offline_Data__Objects_Req objofflinereq;
        public Get_Offline_Data__Objects(Get_Offline_Data__Objects_Req _objofflinereq)
        {
            objofflinereq = _objofflinereq;
        }

        public Get_Offline_Data__Objects_Resp Process()
        {

            Get_Offline_Data_ObjectsData objoffline = new Get_Offline_Data_ObjectsData();
            List<OfflineObjectDetails> objectoffline = new List<OfflineObjectDetails>();
            ResponseStatus objRespStatus = new ResponseStatus();
            objRespStatus.cd = "0";
            objRespStatus.desc = "";
            foreach (string obj in objofflinereq.Object)
            {
                DataSet dsofflinetblDtls = getOfflineListByCompanyAndTblId(obj);
                if (dsofflinetblDtls.Tables.Count != 0 && dsofflinetblDtls.Tables[0].Rows.Count > 0)
                {

                    if (dsofflinetblDtls.Tables[0].Rows.Count != 0)
                    {

                        foreach (DataRow row in dsofflinetblDtls.Tables[0].Rows)
                        {
                            OfflineObjectDetails objMenuCatListDtls = new OfflineObjectDetails();
                            objMenuCatListDtls.nm = Convert.ToString(row["OFFLINE_OBJECT_NAME"]);
                            objMenuCatListDtls.id = Convert.ToString(row["OFFLINE_OBJECT_ID"]);
                            objMenuCatListDtls.tbl = Convert.ToString(row["TABLE_NAME"]);
                            objMenuCatListDtls.typ = Convert.ToString(row["QUERY_TYPE"]);
                            objMenuCatListDtls.where = Convert.ToString(row["QUERY"]);
                            objMenuCatListDtls.input = new List<string>();
                            string input = Convert.ToString(row["INPUT_PARAMETERS"]);
                            List<OfflineObjectInput> inp = new List<OfflineObjectInput>();
                            if (!string.IsNullOrEmpty(input))
                            {
                                inp = Utilities.DeserialiseJson<List<OfflineObjectInput>>(input);
                                foreach (OfflineObjectInput f in inp)
                                    objMenuCatListDtls.input.Add(f.para);
                            }
                            string strOutput = Convert.ToString(row["OUTPUTS"]);
                            if (strOutput.Trim().Length > 0)
                                objMenuCatListDtls.col = Utilities.DeserialiseJson<List<OfflineObjectColumn>>(strOutput);
                            else
                                objMenuCatListDtls.col = new List<OfflineObjectColumn>();

                            foreach(OfflineObjectColumn off in objMenuCatListDtls.col )
                            {
                                off.para = off.para.Trim();
                            }


                            objMenuCatListDtls.error = Convert.ToString(row["ERROR_MESSAGE"]);
                            objMenuCatListDtls.exit = (Convert.ToInt32(row["EXIT_ON_ERROR"])).ToString();
                            objMenuCatListDtls.rtry = (Convert.ToInt32(row["ALLOW_RETRY"])).ToString();

                            objectoffline.Add(objMenuCatListDtls);
                        }
                    }
                }
            }
            objoffline.obj = objectoffline;
            return new Get_Offline_Data__Objects_Resp(objRespStatus, objoffline, objofflinereq.RequestId);
        }


        DataSet getOfflineListByCompanyAndTblId(string objectid)
        {
            string strQuery = @" Select o.*,t.TABLE_NAME from  dbo.TBL_OFFLINE_OBJECTS as o inner join TBL_OFFLINE_DATA_TABLE as t on o.OFFLINE_TABLES=t.TABLE_ID where o.COMPANY_ID=@CompanyId and OFFLINE_OBJECT_ID =@Objid;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", objofflinereq.CompanyId);
            cmd.Parameters.AddWithValue("@Objid", objectid);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }

    }

}
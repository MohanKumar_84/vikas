﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
namespace mFicientWS
{
    public enum FUNCTION_CODES
    {
        COMPANY_DETAIL = 11002,
        GET_ENTERPRISE_LOGO_AS_ZIPPED = 11004,

        USER_DETAIL = 12001,
        CHANGE_PASSWORD = 12002,
        RESET_PASSWORD = 12003,
        CHECK_ACCOUNT_AND_DEVICE = 12004,

        MOBILE_USER_LOGIN = 13001,
        DEVICE_REGISTRATION_REQUEST = 13002,
        RENEW_SESSION = 13003,
        LOG_OUT = 13004,
        ADD_DEVICE_PUSH_MSG_ID = 13006,

        GET_MENU_AND_WORKFLOW_LIST = 14004,
        UPDATION_COMPLETE = 14006,
        GET_APP_FILES_AS_ZIPPED = 14007,
        GET_HTML_FILES_OF_APP_AS_SINGLE_HTML = 14008,
        WORK_FLOW_USAGE_LOG = 14009,
        GET_OFFLINE_ALL_DBTABLES_PROP = 14013,
        GET_MENU_AND_WORKFLOW_LIST_FOR_TESTER = 14012,
       GET_OFFLINE_DATA_TABLE_META=14010,
       GET_OFFLINE_DATA_OBJECTS=14011,


        DO_TASK = 15001,
        DO_TASK_FOR_BATCH_PROCESS=15002,
        GET_PAGING_DATA = 15003,
        //Download Online Database  Offline Table
        DOWNLOAD_ONLINE_DATABASE_OFFLINE = 15004,
        //Upload Offline Database to Online Database
        UPLOAD_OFFLINE_DATABASE_TO_ONLINE_DATABASE = 15005,
        CONVERT_LOCAL_TO_PUBLIC_IMAGEURL=15006,
        SAVE_PUSH_MSG_FROM_USER = 15007,

        SUPPORT_QUERY = 16001,
        USER_QUERY_LIST = 16002,
        USER_QUERY_DETAIL = 16003,
        COMMENT_AND_UPDATE_QUERY = 16004,
        GET_QUERY_TYPE = 16005,

        MGRAM_MESSAGE_LIST_GET = 17001,
        MGRAM_MESSAGE_GET = 17002,
        MGRAM_MESSAGE_STATUS_UPDATE = 17003,

        //mBuzz Server function
        USER_AUTHENTICATION = 20001,
        MBUZZ_PUSHMESSAGE_SAVE = 20002,
        MBUZZ_COMPANY_USER_LIST = 20003,
        ADD_CONTACT_REQUEST = 20004,
        ADD_CONTACT_CONFIRMATION = 20005,
        REMOVE_CONTACT = 20006,
        REMOVE_PENDING_REQUEST = 20007,
        DESKTOP_USER_LOGIN = 20008,
        USER_CONTACTS = 20009,
        //MBUZZ_FILE_DOWNLOAD=18001,

        //mPlugin function
        MPLUGIN_CLIENT_AUTHENTICATION = 41001,
        GET_MPLUGIN_CONNECTION_DETAILS = 41002,

        //mGram Function
        MGRAM_ENTERPRISE_DETAIL = 51001,
        // Get Userlist for registration
        GET_LIST_OF_USERS_REGISTERED_DEVICE = 18001,
        //User Group list and users list
        USER_GROUP_LIST = 18002,
        // webservice User List
        Webservice_User_List = 18003,
        //Get UserDetails
        Get_User_Detail = 18004,
        // USER FOR GROUP
        USER_FOR_GROUP = 18006,
        // USER bLOCK OR UN BLOCK
        BLOCK_UNBLOCK_USER=18005, 

        // MODIFLY USER GROUP DETAILS
        MODIFY_USER_GROUP=18006,

        // DEVICE APPROVAL DENY DELETE OPERATION 
        APPROVE_DENY_DELETE=18007,
    }

    public enum SUBADMIN_ACTIVITY_LOG
    {
        ALL = 0,
        MOBILE_USER_ADD = 1,
        MOBILE_USER_UPDATE = 2,
        MOBILE_USER_DELETE = 3,
        MOBILE_DEVICE_APPROVE = 4,
        MOBILE_DEVICE_DELETE = 5,
        MOBILE_DEVICE_DENY = 6,
        MOBILE_DEVICE_DELETE_APPROVE = 12,
        ADD_GROUP = 7,
        UPDATE_GROUP = 8,
        ADD_MPLUGIN = 9,
        UPDATE_MPLUGIN = 10,
        PUBLISH_APP = 11,
        DELETE_GROUP = 13,
    }
    public enum RETURN_FUNCTION_TYPE_DOTASK
    {
        COMPLETE_TBL_CLMS_AND_ROWS_SELECT = 99,
        COMPLETE_TBL_RETURN_ZERO = 0,
        DROP_DOWN_DATA = 103,
        REPEATER_DATA = 105,
        BAR_DATA = 106,
        LINE_DATA = 107,
        PIE_DATA = 108,
        TBL_TABLE = 109,
        TBL_EDITABLE = 112,
        TBL_BIND_FORM_CONTROLS = 116,
    }

    public enum COMPANY_USER_STATUS
    {
        COMPANY_USER = 0,
        CONTACT = 1,
        PENDING_SENT_REQUEST = 2,
        PENDING_RECEIVED_REQUEST = 3,
    }

    public enum MGRAM_MESSAGE_STATUS
    {
        PENDING = 0,
        DELIVERED = 1,
        READ = 2,
        EXPIRED = -1,
    }

    public enum MOB_UPDATE_TYPE
    {
        COMPANY_LOGO_UPDATE = 1,
        USER_DETAILS_UPDATE = 2,
        MENU_CATEGORY_UPDATE = 3,
        All = 4,
    }

    public enum CONSTANT_VARIABLES
    {
        REQUEST_TIMEOUT = 30,
        SESSION_TIMEOUT = 30,
        RESET_ACCESSCODE_TIMEOUT = 1440,
    }
    public enum AUTHENTICATION_RESULT
    {
        CORRECT = 0,
        USER_NOT_EXISTS = 1,
        LOGGED_OUT = 2,
        SESSION_NOT_EXISTS = 3,
        SESSION_TIMEOUT = 4,
        COMPANY_NOT_EXISTS = 5
    }
    public enum DATABASE_ERRORS//9000000
    {
        DATABASE_CONNECTION_ERROR = 9000001,
        RECORD_NOT_FOUND_ERROR = 9000002,
        RECORD_INSERT_ERROR = 9000003,
        RECORD_DELETE_ERROR = 9000004,
    }

    public enum FileType
    {
        JPEG = 0,
        JPG = 1,
        PNG = 2,
        TXT = 3,
        XLS = 4,
        PDF = 5
    }
    

    public enum SYSTEM_ERRORS//9100000
    {
        VALIDATION_ERROR = 9100000
    }

    public enum USERLOGIN_ERRORS
    {
        USER_ALREADY_LOGGED_ERROR = 1100201,
        EMAIL_OR_PASSWORD_ERROR = 1100202,
        PASSWORD_ERROR = 1100203,
    }

    public enum RESET_PASSWORD_ERROR
    {
        REQUEST_SEND_BEFORE_STIPULATED_TIME = 1200301,
    }

    public enum DO_TASK_ERROR
    {
        NONE = 0,
        CONNECTION_ERROR = 1500101,
        SQL_QUERY_ERROR = 1500102,
        SQL_PARAMETER_ERROR = 1500103,
        WEB_SERVICE_PARAMETERS_ERROR = 1500104,
        INVALID_COLUMN_IN_DATA_COMMAND = 1500105,
        INVALID_TAGS_IN_JSON_XML = 1500106,
        MPLUGIN_ERROR = 1500107,
        INVALID_TAG_PATH_PROVIDED = 1500108,
        MPLUGIN_SERVER_NOT_FOUND = 1500109,
        INVALID_COMMAND_TYPE = 1500110,
        XML_RPC_FAULT = 1500111,
        INVALID_COMMAND=1500112,
        TRANSACTION_ERROR = 1500113,
        MPLUGIN_SERVER_NOT_REACHABLE = 1500114,
        MPLUGIN_SERVER_UNAUTHORIZED = 1500115,
        MPLUGIN_AGENT_NOT_FOUND = 1500116,
        WEBSERVICE_NOT_REACHABLE = 1500117,
        WEBSERVICE_ERROR = 1500118,
        COMMAND_CREDENTIAL_ERROR=1500119,
        //NO_RECORD = 1500119,
        CACHE_LIST_IS_EMPTY = 1500317,
        DATA_IN_CACHE_EXPIRED = 1500318,
        MPLUGIN_UNKNOWN_ERROR=1500120
    }

    public enum MPlugInChangesRequestType
    {
        REFRESH_CONNECTORS = 4,
        CREDENTIALS_CHANGED = 5
    }

    public enum DatabaseType
    {
        OTHER = 0,
        MSSQL = 1,
        ORACLE = 2,
        MYSQL = 3,
        POSTGRESQL = 4,
        ODBC = 5
    }

    public enum DatabaseCommandType
    {
        SELECT = 1,
        INSERT = 2,
        UPDATE = 3,
        DELETE = 4,
        STORED_PROCEDURE = 5
    }

    public enum WebserviceType
    {
        HTTP = 1,
        WSDL_SOAP = 2,
        RPC_XML = 3
    }

    public enum ResponseType
    {
        OTHER = -1,
        XML = 0,
        JSON = 1,
        NONE = 2
    }

    public enum HttpType
    {
        GET = 1,
        POST = 2,
        PUT = 3,
        PATCH = 4,
        MERGE = 5,
        DELETE = 6
    }

    public enum IMAGE_ZIPPER_TYPE
    {
        COMPANY_LOGO,
    }
    public enum DEVICE_ICON_SIZE
    {
        SMALL = 0,
        MEDIUM = 1,
        LARGE = 2,
    }

    public enum XML_RPC_Parameter_Type
    {
        I4 = 0,
        BASE64 = 1,
        BOOLEAN = 2,
        DATE_TIME = 3,
        DOUBLE = 4,
        INTEGER = 5,
        STRING = 6,
        STRUCT = 7,
        ARRAY = 8,
        NOT_DEFINED=9,
    }

    public enum ODATA_INPUT_TYPE
    {
        String = 0,
        Decimal = 1,
        Integer = 2,
        Boolean = 3
    }

    public enum DEVICE_MODEL_TYPE
    {
        PHONE = 0,
        TABLET = 1,
    }
    public enum MFICIENT_USER_TYPE
    { 
        CloudUser,
        ActiveDirectoryUser
    }
    public enum MbuzzDataSendStatus
    {
        PENDING = 0,
        SENT = 1,
        FAILED = 2
    }
    public enum MbuzzMessageCode
    {
        NEW_CONTACT_ADDED_OR_UNBLOCKED = 124,
        CONTACT_BLOCKED = 125,
        CONTACT_DELETED = 126,
        CONTACT_NO_DEVICE = 127,
        DEVICE_DELETED = 128,
        DESKTOP_MESSENGER_DELETED = 129, 
        ADD_UPDATE_COMPANY = 130,
        REMOVE_COMPANY = 131,
    }
    public enum MOBILE_UPDATE_TYPE
    {
        COMPANY_LOGO_UPDATE = 1,
        USER_DETAILS_UPDATE = 2,
        MENU_CATEGORY_UPDATE = 3,
    }
    public enum RESET_PASSWORD_REQUEST_BY
    {
        SubAdmin,
        Admin,
        User
    }

    public enum RequestCommandType
    {
        Database = 1,
        WebService = 2,
        WebServiceSoap = 3,
        WebServiceRPCXML = 4,
        ODATA = 5,
    }

    public enum BucketType
    {
        MFICIENT_WEBSERVICE = 1,
        MPLUGIN_AGENT = 2,
        MBUZZ_DESKTOP = 3,
        MFICIENT_ANDROID = 4,
        IOS_DEVICE = 5,
    }

    public class MficientConstants
    {
        public static int[] DatabaseErrors = { 90001, 9000002, 9000003, 9000004 };
        public static string strServerConection = WebConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
        public static string strmGramConection = WebConfigurationManager.ConnectionStrings["ConStrmGramDB"].ConnectionString;
        //public static string strmBuzzConection = WebConfigurationManager.ConnectionStrings["ConMBuzz"].ConnectionString;
        public static int INVALID_REQUEST_JSON = 91001;
        public static string mPluginUserAgent = @"mFicient-WebServices";
        internal const int SESSION_TIMEOUT_SECONDS = 1800;
        internal const int SESSION_VALIDITY_SECONDS = 3600;
        internal const int DATASET_PAGING_VALIDITY_SECONDS =900;
        internal const int RESPONSE_KEY_VALIDITY_SECONDS = 300;
        public static double HTML_VERSION = 1.090;

        internal const string ENTERPRISE_URL = @"https://enterprise.mficient.com";
        internal static string IconUrl = @"https://enterprise.mficient.com/images/icon";
        /// <summary>
        /// This is without the companyId
        /// </summary>
        internal static string CompanyImagesUrl = @"https://enterprise.mficient.com/CompanyImages/";

        internal static string IconUrlSmall = @"https://enterprise.mficient.com/images/icon/small";
        internal static string IconUrlMedium = @"https://enterprise.mficient.com/images/icon/medium";
        internal static string IconUrlLarge = @"https://enterprise.mficient.com/images/icon/large";
        internal static string IosDeviceTypeFromMobile = "ios";
        internal static string AndroidDevTypeFromMobile = "android";
        public static string FilePathOfDefaultPage = "/default.aspx";
        public static string FilePathOfObject = "/execobject.aspx";

        //internal static string SubAdminIdForTestCompany = "C70A65E7C7A4F268517F1B97F4AEA956";
        //internal static string TestCompanyName = "democompany";
        //internal static string TestUserName = "demouser";
        internal const string HTML_LINK_FILES = @"[{""ver"":""3.0"",""files"":[
        {""idx"":1,""typ"":""com"",""pf"":""DEVICE_device.js""},
        {""idx"":2,""typ"":""com"",""pf"":""JQM_default-theme.min.css""},
        {""idx"":3,""typ"":""com"",""pf"":""JQM_jquery.mobile.icons.min.css""},
        {""idx"":4,""typ"":""com"",""pf"":""JQM_jquery.mobile.structure-1.4.5.min.css""},
        {""idx"":5,""typ"":""com"",""pf"":""JQ_jquery-2.1.3.min.js""},
        {""idx"":6,""typ"":""com"",""pf"":""JQM_jquery.mobile-1.4.5.min.js""},
        {""idx"":7,""typ"":""com"",""pf"":""MFICIENT_mficient-3.0.min.css""},
        {""idx"":8,""typ"":""tbl"",""pf"":""JQU_jquery.fixheadertable.min.css""},
        {""idx"":9,""typ"":""com"",""pf"":""DEVICE_platform.js""},
        {""idx"":10,""typ"":""jqp"",""pf"":""JQP_amcharts.js""},
        {""idx"":11,""typ"":""jqp"",""pf"":""JQP_gauge.js""},
        {""idx"":12,""typ"":""jqp"",""pf"":""JQP_pie.js""},
        {""idx"":13,""typ"":""jqp"",""pf"":""JQP_serial.js""},
        {""idx"":14,""typ"":""tbl"",""pf"":""JQU_jqbrowser.js""},
        {""idx"":15,""typ"":""tbl"",""pf"":""JQU_jquery.fixheadertable.min.js""},
        {""idx"":16,""typ"":""map"",""pf"":""JQU_jquery.ui.map.min.js""},
        {""idx"":17,""typ"":""com"",""pf"":""MFICIENT_mficient-3.0.min.js""}]}]";
    }
}
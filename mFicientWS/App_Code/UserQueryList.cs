﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class UserQueryList
    {
         UserQueryListReq objReq;
         public UserQueryList(UserQueryListReq _objReq)
        {
            objReq = _objReq;
        }

        public UserQueryListResp Process()
        {
            ResponseStatus _respStatus=new ResponseStatus();
            _respStatus.cd="0";
            _respStatus.desc="";
            SqlCommand cmd = new SqlCommand("SELECT TOKEN_NO,QUERY_TYPE,QUERY_SUBJECT,POSTED_ON,case when RESOLVED_BY_CREATOR = 1  then 1 else 0 end as Qstatus FROM ADMIN_TBL_QUERY where COMPANY_ID=@COMPANY_ID and USER_ID=@USER_ID ORDER BY POSTED_ON desc ");
            cmd.Parameters.AddWithValue("@COMPANY_ID", objReq.CompanyId);
            cmd.Parameters.AddWithValue("@USER_ID", objReq.UserID);

            DataSet ds=MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (ds.Tables[0].Rows.Count <= 0)
                throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());

            List<QueryList> qlst = new List<QueryList>();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {////TOKEN_NO,QUERY_TYPE,QUERY_SUBJECT,RESOLVED_On
                QueryList objectQry = new QueryList();
                objectQry.tn = Convert.ToString(dr["TOKEN_NO"]);
                objectQry.qtyp = Convert.ToString(dr["QUERY_TYPE"]);
                objectQry.pon = Convert.ToString(dr["POSTED_ON"]);
                objectQry.qsub = Convert.ToString(dr["QUERY_SUBJECT"]);
                objectQry.sts = Convert.ToString(dr["Qstatus"]);
                qlst.Add(objectQry);
            }

            return new UserQueryListResp(_respStatus, qlst, objReq.RequestId);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Oracle.ManagedDataAccess.Client;
using System.Data;

namespace mFicientWS
{
    public class OracleDoTaskClient
    {
        public static void OracleConnectionOpen(out OracleConnection Conn, string ConnectionString)
        {
            try
            {
                Conn = new OracleConnection(ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
            }
        }

        public static void OracleConnectionClose(OracleConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open))
                {
                    Conn.Close();
                }
            }
            catch
            {
            }
        }

        public static DataSet SelectDataFromDoTask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _uid, string _pwd)
        {
            OracleConnection objOracleConnection = null;
            DataSet ObjDataSet = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId, _uid, _pwd);
                dbcon.GetConnector();
                //string strConString = "Server = " + dbcon.HostName + ";Initial Catalog=" + dbcon.DataBaseName + ";User Id=" + _uid + ";Password =" + _pwd;

                string connectionString = "";
                //Use System.Data.OracleClient as database provider
                Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder builder = new Oracle.ManagedDataAccess.Client.OracleConnectionStringBuilder();
                builder.DataSource = dbcon.HostName + "/" + dbcon.DataBaseName;
                builder.UserID = _uid;
                builder.Password = _pwd;
                connectionString = builder.ToString();
                if (dbcon.AdditionalString.Length > 0)
                    connectionString = connectionString + dbcon.AdditionalString + ";";

                OracleConnectionOpen(out objOracleConnection, connectionString);

                OracleCommand _OracleCommand = new OracleCommand(db_cmd.SqlQuery, objOracleConnection);
                _OracleCommand.BindByName = true;
                foreach (QueryParameters parameters in LstParameters)
                {
                    _OracleCommand.Parameters.Add(parameters.para, parameters.val);
                }

                ObjDataSet = new DataSet();

                OracleDataAdapter objSqlDataAdapter = new OracleDataAdapter(_OracleCommand);

                objSqlDataAdapter.Fill(ObjDataSet);
            }
            catch (OracleException oracleEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                OracleConnectionClose(objOracleConnection);
            }

            return ObjDataSet;
        }

        public static DataSet SelectDataFromOpenConnectionDoTask(OracleCommand _OracleCommand, List<QueryParameters> LstParameters)
        {
            OracleConnection objOracleConnection = null;
            DataSet ObjDataSet = null;
            try
            {
                _OracleCommand.BindByName = true;
                foreach (QueryParameters parameters in LstParameters)
                {
                    _OracleCommand.Parameters.Add(parameters.para, parameters.val);
                }

                ObjDataSet = new DataSet();

                OracleDataAdapter objSqlDataAdapter = new OracleDataAdapter(_OracleCommand);

                objSqlDataAdapter.Fill(ObjDataSet);
            }
            catch (OracleException oracleEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                OracleConnectionClose(objOracleConnection);
            }

            return ObjDataSet;
        }
        public static int ExecuteNonQueryWithOpenCon(OracleCommand _OracleCommand, List<QueryParameters> LstParameters)
        {
            OracleConnection objOracleConnection = null;
            try
            {
                _OracleCommand.BindByName = true;
                foreach (QueryParameters parameters in LstParameters)
                {
                    _OracleCommand.Parameters.Add(parameters.para, parameters.val);
                }

                int i = _OracleCommand.ExecuteNonQuery();

                return i;
            }
            catch (OracleException oracleEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                OracleConnectionClose(objOracleConnection);
            }
            //return 0;
        }
        public static int ExecuteNonQueryRecordDotask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _uid, string _pwd)
        {
            OracleConnection objOracleConnection = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId, _uid, _pwd);
                dbcon.GetConnector();

                OracleConnectionOpen(out objOracleConnection, dbcon.ConnectionString);

                OracleCommand _OracleCommand = new OracleCommand(db_cmd.SqlQuery, objOracleConnection);
                _OracleCommand.BindByName = true;
                foreach (QueryParameters parameters in LstParameters)
                {
                    _OracleCommand.Parameters.Add(parameters.para, parameters.val);
                }

                int i = _OracleCommand.ExecuteNonQuery();

                return i;
            }
            catch (OracleException oracleEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                OracleConnectionClose(objOracleConnection);
            }
            //return 0;
        }

        public static string ExecuteScalarDotask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _uid, string _pwd)
        {
            OracleConnection objOracleConnection = null; ;
            string strResult = "";
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId, _uid, _pwd);
                dbcon.GetConnector();

                OracleConnectionOpen(out objOracleConnection, dbcon.ConnectionString);

                OracleCommand _OracleCommand = new OracleCommand(db_cmd.SqlQuery, objOracleConnection);
                _OracleCommand.BindByName = true;
                foreach (QueryParameters parameters in LstParameters)
                {
                    _OracleCommand.Parameters.Add(parameters.para, parameters.val);
                }

                object obj = _OracleCommand.ExecuteScalar();
                if (obj != null)
                    strResult = obj.ToString();
            }
            catch (OracleException oracleEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                OracleConnectionClose(objOracleConnection);
            }

            return strResult;
        }

        public static DataSet ExecuteStoredProcedureDoTask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _uid, string _pwd)
        {
            OracleConnection objOracleConnection = null;
            DataSet ObjDataSet = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId, _uid, _pwd);
                dbcon.GetConnector();
                string strConString = "Server = " + dbcon.HostName + ";Initial Catalog=" + dbcon.DataBaseName + ";User Id=" + dbcon.UserId + ";Password =" + dbcon.Password;

                OracleConnectionOpen(out objOracleConnection, strConString);

                OracleCommand _OracleCommand = new OracleCommand();
                _OracleCommand.Connection = objOracleConnection;
                _OracleCommand.CommandType = CommandType.StoredProcedure;
                _OracleCommand.CommandText = db_cmd.SqlQuery;
                _OracleCommand.BindByName = true;
                foreach (QueryParameters parameters in LstParameters)
                {
                    _OracleCommand.Parameters.Add(parameters.para, parameters.val);
                }

                ObjDataSet = new DataSet();

                OracleDataAdapter objOracleDataAdapter = new OracleDataAdapter(_OracleCommand);

                objOracleDataAdapter.Fill(ObjDataSet);
            }
            catch (OracleException oracleEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                OracleConnectionClose(objOracleConnection);
            }

            return ObjDataSet;
        }
    }
}
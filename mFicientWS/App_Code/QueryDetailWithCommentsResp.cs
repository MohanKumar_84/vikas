﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Data;

namespace mFicientWS
{
    public class QueryDetailWithCommentsResp
    { 
        ResponseStatus _respStatus;
        string _requestId;
        Querydeatil _Qrydetail;
        //CommentList _lstComment;
        public QueryDetailWithCommentsResp(ResponseStatus respStatus, Querydeatil Qrydetail, string requestId)
        {
            _respStatus = respStatus;
            _Qrydetail = Qrydetail;
            //_lstComment = lstComment;
            _requestId = requestId;
        }
        public string GetResponseJson()
        {
           QueryDetailWithCommentsResponse objCheckAccountAndDeviceResp = new QueryDetailWithCommentsResponse();
           objCheckAccountAndDeviceResp.func = Convert.ToString((int)FUNCTION_CODES.USER_QUERY_DETAIL);
            objCheckAccountAndDeviceResp.rid = _requestId;
            objCheckAccountAndDeviceResp.status = _respStatus;
            QueryDetailWithCommentsRespData data = new QueryDetailWithCommentsRespData();
            data.qdtl = _Qrydetail;
            objCheckAccountAndDeviceResp.data = data;
            string strJsonWithDetails = Utilities.SerializeJson<QueryDetailWithCommentsResponse>(objCheckAccountAndDeviceResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }
    public class QueryDetailWithCommentsResponse : CommonResponse
    {
        public QueryDetailWithCommentsResponse()
        { }
        //[DataMember]
        //public List<FormListResponseData> data { get; set; }
        [DataMember]
        public QueryDetailWithCommentsRespData data { get; set; }
    }

    public class QueryDetailWithCommentsRespData
    {
        /// <summary>
        /// Menu Category Details
        /// </summary>
        [DataMember]
        public Querydeatil qdtl { get; set; }
    }
    public class Querydeatil
    {
        [DataMember]
        public string qury { get; set; }

        [DataMember]
        public string pon { get; set; }
        
        [DataMember]
        public string qtyp { get; set; }

        [DataMember]
        public string qcd { get; set; }

        [DataMember]
        public string ron { get; set; }

        [DataMember]
        public string rby { get; set; }

        [DataMember]
        public List<CommentList> clst { get; set; }
    }

    public class CommentList
    {//COMMENT,COMMENTED_ON,Commentby,TOKEN_NO
        [DataMember]
        public string cmt { get; set; }

        [DataMember]
        public string cdt { get; set; }

        [DataMember]
        public string cby { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class MPluginGetDomainListResp
    {
        string _statusCode, _statusDescription;
        List<string> _data;

        public MPluginGetDomainListResp(List<string> domainLst, string statusCode, string statusDescription)
        {
            try
            {
                _data = domainLst;
                _statusCode = statusCode;
                _statusDescription = statusDescription;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating MPluginGetDomains Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            MPluginGetDomainListRespFields objDomainListRespFields = new MPluginGetDomainListRespFields();
            objDomainListRespFields.cd = _statusCode;
            objDomainListRespFields.desc = _statusDescription;
            objDomainListRespFields.data = _data;
            string strJsonWithDetails = Utilities.SerializeJson<MPluginGetDomainListRespFields>(objDomainListRespFields);
            return Utilities.getFinalJson(strJsonWithDetails);
        }

        public string StatusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }

        public string StatusCode
        {
            get { return _statusCode; }
            set { _statusCode = value; }
        }

        public List<string> Data
        {
            get { return _data; }
            set { _data = value; }
        }
    }

    [DataContract]
    public class MPluginGetDomainListRespFields
    {
        [DataMember]
        public string cd { get; set; }

        [DataMember]
        public string desc { get; set; }

        [DataMember]
        public List<string> data { get; set; }
    }
}
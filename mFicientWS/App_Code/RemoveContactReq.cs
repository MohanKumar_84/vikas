﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class RemoveContactReq
    {
        string _requestId, _ContactName, _companyId, _userName;
        int _functionCode;

        public string ContactName
        {
            get { return _ContactName; }
        }
        public string UserName
        {
            get { return _userName; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }


        public RemoveContactReq(string requestJson)
        {
            RequestJsonParsing<RemoveContactReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<RemoveContactReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.REMOVE_CONTACT)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _ContactName = objRequestJsonParsing.req.data.cnctnm;
            _userName = objRequestJsonParsing.req.data.unm;
            _companyId = objRequestJsonParsing.req.data.enid;
        }
    }

    [DataContract]
    public class RemoveContactReqData : Data
    {
        public RemoveContactReqData()
        { }
        /// <summary>
        /// New Contact ID
        /// </summary>
        [DataMember]
        public string cnctnm { get; set; }
    }
}
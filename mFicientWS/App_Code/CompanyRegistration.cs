﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
namespace mFicientWS
{
//    public class CompanyRegistration
//    {
//        CompanyRegistrationReq _cmpRegistrationReq;

//        public CompanyRegistration(CompanyRegistrationReq cmpRegistrationReq)
//        {
//            _cmpRegistrationReq = cmpRegistrationReq;
//        }
//        public CompanyRegistrationResp Process(HttpContext context)
//        {
//            ResponseStatus objRespStatus = new ResponseStatus();
//            objRespStatus = GetResponseStatus.getRespStatus(null, _cmpRegistrationReq.FunctionCode);
//            try
//            {
//                validateCompanyIdExistense();
//                validateAdminIdExistense();
//                validateReqFields();

//                MFETrialPlanDetail objTrialPlanDetail = getDefaultTrialPlan();
//                MFEmficientServer objDefaultMfServer = this.getDefaultMfServerDtl();
//                MFEmPluginServer objDefaultMpluginServer = this.getDefaultMpluginServerDtl();
//                MFEmBuzzServer objDefaultMbuzzServer = this.getDefaultMbuzzServerDtl();

//                runProcessInTransaction(objTrialPlanDetail,
//                    objDefaultMfServer,
//                    objDefaultMpluginServer,
//                    objDefaultMbuzzServer);

//                notifyMbuzzAboutNewCmpAddition(objDefaultMbuzzServer, objDefaultMfServer.ServerURL);
//                SaveEmailInfo.companyRegistration(
//                    _cmpRegistrationReq.CompanyAdmin.EmailId,

//                    _cmpRegistrationReq.CompanyAdmin.Firstname + " " +
//                    _cmpRegistrationReq.CompanyAdmin.MiddleName + " " +
//                    _cmpRegistrationReq.CompanyAdmin.LastName,

//                    _cmpRegistrationReq.CompanyAdmin.EmailId,
//                    _cmpRegistrationReq.CompanyInfo.CompanyId,
//                    context);
//            }
//            catch (MficientException ex)
//            {
//                objRespStatus = GetResponseStatus.getRespStatus(ex,
//                    _cmpRegistrationReq.FunctionCode);
//            }
//            return new CompanyRegistrationResp(objRespStatus, _cmpRegistrationReq.RequestId);
//        }
//        void runProcessInTransaction(
//            MFETrialPlanDetail trialPlan,
//            MFEmficientServer mfServer,
//            MFEmPluginServer mPluginServer,
//            MFEmBuzzServer mbuzzServer)
//        {
//            if (trialPlan == null ||
//                mfServer == null ||
//                mPluginServer == null ||
//                mbuzzServer == null) throw new ArgumentNullException();

//            long registrationDateTime = DateTime.UtcNow.Ticks;
//            string strUniqueAdminID = getUniqueAdminId(registrationDateTime);
//            //For purchase Log
//            string strTransactionId = getUniqueTransactionId();
//            SqlTransaction transaction = null;
//            SqlConnection con;
//            MSSqlClient.SqlConnectionOpen(out con);
//            try
//            {
//                using (con)
//                {
//                    using (transaction = con.BeginTransaction())
//                    {
//                        saveCompanyAdministrator(
//                            transaction,
//                            con,
//                            strUniqueAdminID,
//                            registrationDateTime
//                            );

//                        saveCompanyDetails(transaction,
//                            con,
//                            strUniqueAdminID,
//                            registrationDateTime);

//                        saveCompanyCurrentPlan(transaction,
//                            con,
//                            registrationDateTime,
//                            trialPlan);

//                        transferCompanyAdministratorData(con,
//                            transaction);

//                        transferCompanyCurrentPlanData(con, transaction);

//                        transferCompanyDetailData(con,
//                            transaction,
//                            registrationDateTime);

//                        saveDefualtCompanyAccSettings(transaction, con);
//                        saveCmpServerMappingData(transaction,
//                            con,
//                            mfServer,
//                            mPluginServer,
//                            mbuzzServer,
//                            registrationDateTime);
//                        transaction.Commit();
//                    }
//                }
//            }
//            catch 
//            {
//                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
//            }
//            finally
//            {
//                if (con != null)
//                {
//                    con.Dispose();
//                }
//                if (transaction != null)
//                {
//                    transaction.Dispose();
//                }
//            }
//        }
//        void notifyMbuzzAboutNewCmpAddition(MFEmBuzzServer mBuzzServer, string ServerUrl)
//        {
//            try
//            {
//                ReqJsonToAddNewCmpToMBuzz objReqToAddNewCmpToMbuzz = new ReqJsonToAddNewCmpToMBuzz();
//                objReqToAddNewCmpToMbuzz.enid = _cmpRegistrationReq.CompanyInfo.CompanyId;
//                objReqToAddNewCmpToMbuzz.mrid = (DateTime.UtcNow.Ticks).ToString().Substring(0, 4);
//                objReqToAddNewCmpToMbuzz.type = ((int)MbuzzMessageCode.ADD_UPDATE_COMPANY).ToString();
//                objReqToAddNewCmpToMbuzz.data = new ReqJsonToAddNewCmpToMBuzzData();
//                objReqToAddNewCmpToMbuzz.data.ennm = _cmpRegistrationReq.CompanyInfo.CompanyName;
//                objReqToAddNewCmpToMbuzz.data.surl = ServerUrl;
//                string json = "{\"req\":" + Utilities.SerializeJson<ReqJsonToAddNewCmpToMBuzz>(objReqToAddNewCmpToMbuzz) + "}";
//                mBuzzClient objmBuzzClient =
//                    new mBuzzClient(
//                        json,
//                        mBuzzServer.ServerIpAddress,
//                        Convert.ToInt32(mBuzzServer.ServerPort)
//                        );
//                ThreadPool.QueueUserWorkItem(new WaitCallback(objmBuzzClient.Connect), null);
//            }
//            catch
//            { }
//        }
//        void saveEmailForAdmin()
//        {

//        }
//        void validateReqFields()
//        {
//            StringBuilder sbError = new StringBuilder();
//            if (String.IsNullOrEmpty(_cmpRegistrationReq.CompanyInfo.CompanyName))
//            {
//                appendErrorMsg(sbError, "Please enter enterprise / organization name.");
//            }
//            else if (!Utilities.IsValidString(_cmpRegistrationReq.CompanyInfo.CompanyName, true, true, true, "- .", 1, 50, false, false))
//            {
//                appendErrorMsg(sbError, "Please enter correct enterprise / organization name.");
//            }

//            if (String.IsNullOrEmpty(_cmpRegistrationReq.CompanyInfo.CompanyId))
//            {
//                appendErrorMsg(sbError, "Please enter enterprise / organization id.");
//            }
//            else
//            {
//                try
//                {
//                    validateCompanyId();
//                }
//                catch (MficientException ex)
//                {
//                    appendErrorMsg(sbError, ex.Message);
//                }
//            }
//            if (String.IsNullOrEmpty(_cmpRegistrationReq.CompanyAdmin.Firstname))
//            {
//                appendErrorMsg(sbError, "Please enter administrator name.");
//            }
//            else if (!Utilities.IsValidString(_cmpRegistrationReq.CompanyAdmin.Firstname.Trim(), true, true, true, "- '", 1, 50, false, false))
//            {
//                appendErrorMsg(sbError, "Please enter correct administrator name.");
//            }
//            if (String.IsNullOrEmpty(_cmpRegistrationReq.CompanyAdmin.EmailId))
//            {
//                appendErrorMsg(sbError, "Please enter email.");
//            }
//            else if (!Utilities.IsValidEmail(_cmpRegistrationReq.CompanyAdmin.EmailId))
//            {
//                appendErrorMsg(sbError, "Please enter correct email.");
//            }
//            if (String.IsNullOrEmpty(_cmpRegistrationReq.CompanyAdmin.Password))
//            {
//                appendErrorMsg(sbError, "Please enter password.");
//            }
//            else if (!Utilities.IsValidString(_cmpRegistrationReq.CompanyAdmin.Password, true, true, true, @".,#%^&*:;()[]{}?/\+=-_|~", 6, 20, false, false))
//            {
//                appendErrorMsg(sbError, "Please enter valid password.Min 6 characters and max 20 characters.");
//            }
//            if (String.IsNullOrEmpty(_cmpRegistrationReq.CompanyAdmin.MobileNo))
//            {
//                appendErrorMsg(sbError, "Please enter a contact no.");
//            }
//            else
//            {
//                try
//                {
//                    validateMobileNo(_cmpRegistrationReq.CompanyAdmin.MobileNo);
//                }
//                catch (MficientException ex)
//                {
//                    appendErrorMsg(sbError, ex.Message);
//                }
//            }
//            //Company Details
//            if (String.IsNullOrEmpty(_cmpRegistrationReq.CompanyInfo.StreetAddress1))
//            {
//                appendErrorMsg(sbError, "Please enter address in Street address 1.");
//            }
//            else if (!Utilities.IsValidString(_cmpRegistrationReq.CompanyInfo.StreetAddress1, true, true, true, "~!@#$%^&*(){}|:<>?,./=-][ ", 1, 50, false, false))
//            {
//                appendErrorMsg(sbError, "Please enter correct address in Street address 1.Min 1 character and max 50 characters.");
//            }
//            if (!String.IsNullOrEmpty(_cmpRegistrationReq.CompanyInfo.StreetAddress2))
//            {
//                if (!Utilities.IsValidString(_cmpRegistrationReq.CompanyInfo.StreetAddress2, true, true, true, "~!@#$%^&*(){}|:<>?,./=-][ ", 0, 50, false, false))
//                {
//                    appendErrorMsg(sbError, "Please enter correct address in Street address 2.Max 50 characters.");
//                }
//            }
//            if (!String.IsNullOrEmpty(_cmpRegistrationReq.CompanyInfo.StreetAddress3))
//            {
//                if (!Utilities.IsValidString(_cmpRegistrationReq.CompanyInfo.StreetAddress3, true, true, true, "~!@#$%^&*(){}|:<>?,./=-][ ", 0, 50, false, false))
//                {
//                    appendErrorMsg(sbError, "Please enter correct address in Street address 3.Max 50 characters.");
//                }
//            }
//            if (String.IsNullOrEmpty(_cmpRegistrationReq.CompanyInfo.CityName))
//            {
//                appendErrorMsg(sbError, "Please enter city.");
//            }
//            else if (!Utilities.IsValidString(_cmpRegistrationReq.CompanyInfo.CityName, true, true, true, "-.'~ ", 1, 50, false, false))
//            {
//                appendErrorMsg(sbError, "Please enter correct city.");
//            }

//            if (String.IsNullOrEmpty(_cmpRegistrationReq.CompanyInfo.StateName))
//            {
//                appendErrorMsg(sbError, "Please enter state.");
//            }
//            else if (!Utilities.IsValidString(_cmpRegistrationReq.CompanyInfo.StateName, true, true, true, "-.'~ ", 1, 50, false, false))
//            {
//                appendErrorMsg(sbError, "Please enter correct state.");
//            }
//            if (String.IsNullOrEmpty(_cmpRegistrationReq.CompanyInfo.Zip))
//            {
//                appendErrorMsg(sbError, "Please enter zip code.");
//            }
//            else if (!Utilities.IsValidString(_cmpRegistrationReq.CompanyInfo.Zip, true, true, true, ".-", 3, 10, false, false))
//            {
//                appendErrorMsg(sbError, "Please enter correct zip code.");
//            }
//            if (String.IsNullOrEmpty(_cmpRegistrationReq.CompanyInfo.CountryCode))
//            {
//                appendErrorMsg(sbError, "Please select country.");
//            }
//            if (String.IsNullOrEmpty(_cmpRegistrationReq.CompanyInfo.TimezoneId))
//            {
//                appendErrorMsg(sbError, "Please select a timezone.");
//            }
//            if (sbError.Length > 0)
//            {
//                throw new MficientException(((int)MFErrors.CompanyRegistrationError.ValidationError).ToString(),
//                        new Exception(sbError.ToString()));
//            }
//        }
//        void appendErrorMsg(StringBuilder errorMsg, string errorToAdd)
//        {
//            if (errorMsg.Length == 0)
//            {
//                errorMsg.Append(errorToAdd);
//            }
//            else
//            {
//                errorMsg.Append("," + errorToAdd);
//            }
//        }
//        void validateCompanyId()
//        {
//            StringBuilder sbError = new StringBuilder();
//            string strCompanyId = _cmpRegistrationReq.CompanyInfo.CompanyId;
//            if (String.IsNullOrEmpty(strCompanyId))
//            {
//                sbError.Append("Please enter enterprise / organization id.");
//            }
//            else
//            {
//                int iFirstAlphabetOfCompanyId;
//                string strFirstAlphabetOfCmpId = strCompanyId.Substring(0, 1);
//                if (Int32.TryParse(strFirstAlphabetOfCmpId, out iFirstAlphabetOfCompanyId))
//                {
//                    sbError.Append("Please enter valid enterprise / organization id.It should begin with an alphabet.");
//                }
//                else if (!Utilities.IsValidString(strCompanyId, true, true, true, "", 5, 20, false, false))
//                {
//                    sbError.Append("Please enter valid enterprise / organization id.Min 5 characters and Max 20 characters.");
//                }
//            }
//            if (sbError.Length > 0)
//            {
//                throw new MficientException(sbError.ToString());
//            }
//        }
//        void validateCompanyIdExistense()
//        {
//            StringBuilder sbError = new StringBuilder();
//            if (doesCompanyIdExist(_cmpRegistrationReq.CompanyInfo.CompanyId))
//            {
//                sbError.Append("Company Id already exist.");
//            }
//            if (sbError.Length > 0)
//            {
//                throw new MficientException(
//                        ((int)MFErrors.CompanyRegistrationError.CompanyIdExists).ToString(),
//                        new Exception(sbError.ToString()));
//            }
//        }
//        void validateAdminIdExistense()
//        {
//            StringBuilder sbError = new StringBuilder();
//            if (doesAdminIdExists(_cmpRegistrationReq.CompanyAdmin.EmailId))
//            {
//                sbError.Append("Admin email already taken.");
//            }
//            if (sbError.Length > 0)
//            {
//                throw new MficientException(
//                        ((int)MFErrors.CompanyRegistrationError.AdminEmailIdExists).ToString(),
//                        new Exception(sbError.ToString()));
//            }
//        }
//        void validateMobileNo(string mobileNo)
//        {
//            string strErrorMsg = "Invalid contact no.";
//            if (String.IsNullOrEmpty(mobileNo)) throw new MficientException("Please enter a contact no.");
//            if (mobileNo.Length < 7 || mobileNo.Length > 20) throw new MficientException(strErrorMsg);
//            char chrFirstChar = Convert.ToChar(mobileNo.Substring(0, 1));
//            if (chrFirstChar != '+') throw new MficientException(strErrorMsg);
//            char chrSecondChar = Convert.ToChar(mobileNo.Substring(1, 1));
//            int iSecondChar = 0;
//            try
//            {
//                //check if it is a number.
//                iSecondChar = int.Parse(chrSecondChar.ToString());
//            }
//            catch
//            {
//                throw new MficientException(strErrorMsg);
//            }
//            if (iSecondChar == 0) throw new MficientException(strErrorMsg);
//            string strNoFromThirdChar = mobileNo.Substring(2, mobileNo.Length - 2);
//            try
//            {
//                ulong.Parse(strNoFromThirdChar);
//            }
//            catch
//            {
//                throw new MficientException(strErrorMsg);
//            }
//        }
//        bool doesCompanyIdExist(string companyId)
//        {
//            CompanyDetail objCmpDtl =
//                new CompanyDetail(companyId);
//            objCmpDtl.Process();
//            if (objCmpDtl.StatusCode == 0)
//            {
//                return objCmpDtl.CompanyExists;
//            }
//            else
//            {
//                throw new Exception();
//            }
//        }
//        bool doesAdminIdExists(string emailId)
//        {
//            WSCGetAdminDetails objGetAdminDtls = new WSCGetAdminDetails();
//            if (objGetAdminDtls.StatusCode == 0)
//                return objGetAdminDtls.doesAdminExists(emailId);
//            else throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
//        }
//        string getUniqueAdminId(long registrationDatetime)
//        {
//            return Utilities.GetMd5Hash(
//                          _cmpRegistrationReq.CompanyAdmin.EmailId +
//                          _cmpRegistrationReq.CompanyAdmin.MobileNo +
//                          _cmpRegistrationReq.CompanyAdmin.Firstname +
//                          _cmpRegistrationReq.CompanyAdmin.MiddleName +
//                          _cmpRegistrationReq.CompanyAdmin.LastName +
//                          registrationDatetime.ToString()).
//                          Substring(0, 16);
//        }
//        string getUniqueTransactionId()
//        {
//            return Utilities.GetMd5Hash(
//                     _cmpRegistrationReq.CompanyAdmin.EmailId +
//                     _cmpRegistrationReq.CompanyAdmin.MobileNo +
//                     _cmpRegistrationReq.CompanyAdmin.Firstname +
//                     _cmpRegistrationReq.CompanyAdmin.MiddleName +
//                     _cmpRegistrationReq.CompanyAdmin.LastName
//                     ).
//                     Substring(0, 16);
//        }


//        void saveCompanyAdministrator(
//            SqlTransaction transaction,
//            SqlConnection con,
//            string adminId,
//            long registrationDatetime)
//        {
//            string query = @"INSERT INTO ADMIN_TBL_COMPANY_ADMINISTRATOR(ADMIN_ID,EMAIL_ID,ACCESS_CODE,FIRST_NAME,LAST_NAME,MOBILE,STREET_ADDRESS1
//                        ,STREET_ADDRESS2,STREET_ADDRESS3,CITY_NAME,STATE_NAME,COUNTRY_CODE,ZIP,DATE_OF_BIRTH,
//                        REGISTRATION_DATETIME,GENDER,MIDDLE_NAME,RESELLER_ID,IS_TRIAL)
//                        VALUES(@ADMIN_ID,@EMAIL_ID,@ACCESSCODE,@FIRSTNAME,@LASTNAME,@MOBILE,@STREETADDRESS1
//                        ,@STREETADDRESS2,@STREETADDRESS3,@CITYNAME,@STATENAME,@COUNTRYCODE,@ZIP,@DATEOFBIRTH,
//                         @REGISTRATIONDATETIME,@GENDER,@MIDDLENAME,@RESELLER_ID,@IsTrial)";
//            SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
//            objSqlCommand.CommandType = CommandType.Text;

//            objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", adminId);
//            objSqlCommand.Parameters.AddWithValue("@EMAIL_ID", _cmpRegistrationReq.CompanyAdmin.EmailId);
//            objSqlCommand.Parameters.AddWithValue("@ACCESSCODE", Utilities.GetMd5Hash(_cmpRegistrationReq.CompanyAdmin.Password));
//            objSqlCommand.Parameters.AddWithValue("@FIRSTNAME", _cmpRegistrationReq.CompanyAdmin.Firstname);
//            objSqlCommand.Parameters.AddWithValue("@LASTNAME", _cmpRegistrationReq.CompanyAdmin.LastName);
//            objSqlCommand.Parameters.AddWithValue("@MOBILE", _cmpRegistrationReq.CompanyAdmin.MobileNo);
//            objSqlCommand.Parameters.AddWithValue("@STREETADDRESS1", _cmpRegistrationReq.CompanyInfo.StreetAddress1);
//            objSqlCommand.Parameters.AddWithValue("@STREETADDRESS2", _cmpRegistrationReq.CompanyInfo.StreetAddress2);
//            objSqlCommand.Parameters.AddWithValue("@STREETADDRESS3", _cmpRegistrationReq.CompanyInfo.StreetAddress3);
//            objSqlCommand.Parameters.AddWithValue("@CITYNAME", _cmpRegistrationReq.CompanyAdmin.CityName);
//            objSqlCommand.Parameters.AddWithValue("@STATENAME", _cmpRegistrationReq.CompanyAdmin.StateName);
//            objSqlCommand.Parameters.AddWithValue("@COUNTRYCODE", _cmpRegistrationReq.CompanyAdmin.CountryCode);
//            objSqlCommand.Parameters.AddWithValue("@ZIP", _cmpRegistrationReq.CompanyAdmin.Zip);
//            objSqlCommand.Parameters.AddWithValue("@DATEOFBIRTH", 0);
//            objSqlCommand.Parameters.AddWithValue("@REGISTRATIONDATETIME", registrationDatetime);
//            objSqlCommand.Parameters.AddWithValue("@GENDER", String.Empty);
//            objSqlCommand.Parameters.AddWithValue("@MIDDLENAME", _cmpRegistrationReq.CompanyAdmin.MiddleName);
//            objSqlCommand.Parameters.AddWithValue("@RESELLER_ID", String.Empty);
//            objSqlCommand.Parameters.AddWithValue("@IsTrial", 1);
//            objSqlCommand.ExecuteNonQuery();
//        }

//        void saveCompanyDetails(
//            SqlTransaction transaction,
//            SqlConnection con,
//            string adminId,
//            long registrationDatetime)
//        {
//            string strQuery = @"INSERT INTO ADMIN_TBL_COMPANY_DETAIL(COMPANY_ID,ADMIN_ID,COMPANY_NAME,REGISTRATION_NO,STREET_ADDRESS1,
//					                STREET_ADDRESS2,STREET_ADDRESS3,CITY_NAME,STATE_NAME,COUNTRY_CODE,ZIP,SUPPORT_EMAIL,SUPPORT_CONTACT,LOGO_IMAGE_NAME,RESELLER_ID
//                                    ,UPDATED_ON,ENQUIRY_ID,SM_APPROVED,SM_APPROVED_DATE,TIMEZONE_ID)
//		                            VALUES(@COMPANY_ID,@ADMIN_ID,@COMPANY_NAME,@REGISTRATION_NO,@STREET_ADDRESS1,
//				                     @STREET_ADDRESS2,@STREET_ADDRESS3,@CITY_NAME,@STATE_NAME,@COUNTRY_CODE,@ZIP,@SupportEmail,@SupportContact,@LOGO_IMAGE_NAME,@RESELLER_ID
//                                     ,@UpdatedOn,@EnquiryId,@SMApproved,@SMApprovedDate,@TIMEZONE_ID);";

//            SqlCommand objSqlCommand = new SqlCommand(strQuery, con, transaction);
//            objSqlCommand.CommandType = CommandType.Text;
//            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _cmpRegistrationReq.CompanyInfo.CompanyId);
//            objSqlCommand.Parameters.AddWithValue("@ADMIN_ID", adminId);
//            objSqlCommand.Parameters.AddWithValue("@COMPANY_NAME", _cmpRegistrationReq.CompanyInfo.CompanyName);
//            objSqlCommand.Parameters.AddWithValue("@REGISTRATION_NO", String.Empty);
//            objSqlCommand.Parameters.AddWithValue("@STREET_ADDRESS1", _cmpRegistrationReq.CompanyInfo.StreetAddress1);
//            objSqlCommand.Parameters.AddWithValue("@STREET_ADDRESS2", _cmpRegistrationReq.CompanyInfo.StreetAddress2);
//            objSqlCommand.Parameters.AddWithValue("@STREET_ADDRESS3", _cmpRegistrationReq.CompanyInfo.StreetAddress3);
//            objSqlCommand.Parameters.AddWithValue("@CITY_NAME", _cmpRegistrationReq.CompanyInfo.CityName);
//            objSqlCommand.Parameters.AddWithValue("@STATE_NAME", _cmpRegistrationReq.CompanyInfo.StateName);
//            objSqlCommand.Parameters.AddWithValue("@COUNTRY_CODE", _cmpRegistrationReq.CompanyInfo.CountryCode);
//            objSqlCommand.Parameters.AddWithValue("@ZIP", _cmpRegistrationReq.CompanyInfo.Zip);
//            objSqlCommand.Parameters.AddWithValue("@SupportEmail", String.Empty);
//            objSqlCommand.Parameters.AddWithValue("@SupportContact", 0);
//            objSqlCommand.Parameters.AddWithValue("@LOGO_IMAGE_NAME", String.Empty);
//            objSqlCommand.Parameters.AddWithValue("@RESELLER_ID", String.Empty);
//            objSqlCommand.Parameters.AddWithValue("@UpdatedOn", registrationDatetime);
//            objSqlCommand.Parameters.AddWithValue("@EnquiryId", String.Empty);
//            objSqlCommand.Parameters.AddWithValue("@SMApproved", 1);
//            objSqlCommand.Parameters.AddWithValue("@SMApprovedDate", registrationDatetime);
//            objSqlCommand.Parameters.AddWithValue("@TIMEZONE_ID", _cmpRegistrationReq.CompanyInfo.TimezoneId);
//            objSqlCommand.ExecuteNonQuery();

//        }

//        void saveCompanyCurrentPlan(SqlTransaction transaction,
//            SqlConnection con,
//            long registrationDatetime,
//            MFETrialPlanDetail trialPlanDetail)
//        {

//            string query = @"INSERT INTO ADMIN_TBL_COMPANY_CURRENT_PLAN(COMPANY_ID,PLAN_CODE,MAX_WORKFLOW,MAX_USER,USER_CHARGE_PM,CHARGE_TYPE,VALIDITY
//                            ,PURCHASE_DATE,PLAN_CHANGE_DATE,PUSHMESSAGE_PERDAY,PUSHMESSAGE_PERMONTH,FEATURE3,FEATURE4,FEATURE5,FEATURE6,FEATURE7,FEATURE8,FEATURE9,FEATURE10,BALANCE_AMOUNT,NEXT_MONTH_PLAN,NEXT_MONTH_PLAN_LAST_UPDATED)
//                            VALUES(@COMPANY_ID,@PLAN_CODE,@MAX_WORKFLOW,@MAX_USER,@USER_CHARGE_PM,@CHARGE_TYPE,@VALIDITY,@PURCHASE_DATE,@PLAN_CHANGE_DATE
//                            ,@PUSHMESSAGE_PERDAY,@PUSHMESSAGE_PERMONTH,@FEATURE3,@FEATURE4,@FEATURE5,@FEATURE6,@FEATURE7,@FEATURE8,@FEATURE9,@FEATURE10,@BALANCE_AMOUNT,@NEXT_MONTH_PLAN,@NEXT_MONTH_PLAN_LAST_UPDATED);";

//            SqlCommand objSqlCommand = new SqlCommand(query, con, transaction);
//            objSqlCommand.CommandType = CommandType.Text;
//            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _cmpRegistrationReq.CompanyInfo.CompanyId);
//            objSqlCommand.Parameters.AddWithValue("@PLAN_CODE", trialPlanDetail.PlanCode);
//            objSqlCommand.Parameters.AddWithValue("@MAX_WORKFLOW", trialPlanDetail.MaxWorkFlow);
//            objSqlCommand.Parameters.AddWithValue("@MAX_USER", trialPlanDetail.MaxUsers);
//            objSqlCommand.Parameters.AddWithValue("@USER_CHARGE_PM", 0);
//            objSqlCommand.Parameters.AddWithValue("@CHARGE_TYPE", String.Empty);
//            objSqlCommand.Parameters.AddWithValue("@VALIDITY", trialPlanDetail.ValidMonths);
//            objSqlCommand.Parameters.AddWithValue("@PURCHASE_DATE", registrationDatetime);
//            objSqlCommand.Parameters.AddWithValue("@PLAN_CHANGE_DATE", 0);
//            objSqlCommand.Parameters.AddWithValue("@PUSHMESSAGE_PERDAY", trialPlanDetail.PushMessagePerDay);
//            objSqlCommand.Parameters.AddWithValue("@PUSHMESSAGE_PERMONTH", trialPlanDetail.PushMessagePerMonth);
//            objSqlCommand.Parameters.AddWithValue("@FEATURE3", 0);
//            objSqlCommand.Parameters.AddWithValue("@FEATURE4", 0);
//            objSqlCommand.Parameters.AddWithValue("@FEATURE5", 0);
//            objSqlCommand.Parameters.AddWithValue("@FEATURE6", 0);
//            objSqlCommand.Parameters.AddWithValue("@FEATURE7", 0);
//            objSqlCommand.Parameters.AddWithValue("@FEATURE8", 0);
//            objSqlCommand.Parameters.AddWithValue("@FEATURE9", 0);
//            objSqlCommand.Parameters.AddWithValue("@FEATURE10", 0);
//            objSqlCommand.Parameters.AddWithValue("@BALANCE_AMOUNT", 0);
//            objSqlCommand.Parameters.AddWithValue("@NEXT_MONTH_PLAN", trialPlanDetail.PlanCode);
//            objSqlCommand.Parameters.AddWithValue("@NEXT_MONTH_PLAN_LAST_UPDATED", 0);

//            objSqlCommand.ExecuteNonQuery();

//        }

//        void saveDefualtCompanyAccSettings(SqlTransaction transaction, SqlConnection con)
//        {
//            string strQuery = @"INSERT INTO [TBL_ACCOUNT_SETTINGS]
//                                           ([COMPANY_ID]
//                                           ,[MAX_DEVICE_PER_USER]
//                                           ,[MAX_MPLUGIN_AGENTS]
//                                           ,[ALLOW_DELETE_COMMAND]
//                                           ,[ALLOW_ACTIVE_DIRECTORY_USER])
//                                     VALUES
//                                           (@COMPANY_ID
//                                           ,@MAX_DEVICE_PER_USER
//                                           ,@MAX_MPLUGIN_AGENTS
//                                           ,@ALLOW_DELETE_COMMAND
//                                           ,@ALLOW_ACTIVE_DIRECTORY_USER);";

//            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@COMPANY_ID", _cmpRegistrationReq.CompanyInfo.CompanyId);
//            cmd.Parameters.AddWithValue("@MAX_DEVICE_PER_USER", 1);
//            cmd.Parameters.AddWithValue("@MAX_MPLUGIN_AGENTS", 1);
//            cmd.Parameters.AddWithValue("@ALLOW_DELETE_COMMAND", 0);
//            cmd.Parameters.AddWithValue("@ALLOW_ACTIVE_DIRECTORY_USER", 0);
//            cmd.ExecuteNonQuery();

//        }
//        void saveCmpServerMappingData(
//            SqlTransaction transaction,
//            SqlConnection con,
//            MFEmficientServer mfServer,
//            MFEmPluginServer mPluginServer,
//            MFEmBuzzServer mbuzzServer,
//            long registrationDatetime)
//        {
//            if (mfServer == null ||
//                mPluginServer == null ||
//                mbuzzServer == null) throw new ArgumentNullException();

//            string strQuery = @"INSERT INTO [ADMIN_TBL_SERVER_MAPPING]
//                                       ([COMPANY_ID],[SERVER_ID]
//                                       ,[SERVER_ADDED_ON],[MBUZZ_SERVER_ID]
//                                       ,[MBUZZ_ADDED_ON],[MPLUGIN_SERVER_ID]
//                                       ,[MPLUGIN_ADDED_ON])
//                                 VALUES
//                                       (@COMPANY_ID,@SERVER_ID
//                                       ,@SERVER_ADDED_ON,@MBUZZ_SERVER_ID
//                                       ,@MBUZZ_ADDED_ON,@MPLUGIN_SERVER_ID
//                                       ,@MPLUGIN_ADDED_ON);";

//            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@COMPANY_ID", _cmpRegistrationReq.CompanyInfo.CompanyId);
//            cmd.Parameters.AddWithValue("@SERVER_ID", mfServer.ServerId);
//            cmd.Parameters.AddWithValue("@SERVER_ADDED_ON", registrationDatetime);
//            cmd.Parameters.AddWithValue("@MBUZZ_SERVER_ID", mbuzzServer.ServerId);
//            cmd.Parameters.AddWithValue("@MBUZZ_ADDED_ON", registrationDatetime);
//            cmd.Parameters.AddWithValue("@MPLUGIN_SERVER_ID", mPluginServer.ServerId);
//            cmd.Parameters.AddWithValue("@MPLUGIN_ADDED_ON", registrationDatetime);
//            cmd.ExecuteNonQuery();

//        }
//        void transferCompanyAdministratorData(SqlConnection con, SqlTransaction transaction)
//        {
//            string strQuery = @"INSERT INTO TBL_COMPANY_ADMINISTRATOR
//                                SELECT @CompanyId,[ADMIN_ID],[EMAIL_ID],[ACCESS_CODE],[FIRST_NAME]
//                                      ,[LAST_NAME],[STREET_ADDRESS1],[STREET_ADDRESS2]
//                                      ,[CITY_NAME],[STATE_NAME],[COUNTRY_CODE]
//                                      ,[ZIP],[DATE_OF_BIRTH] ,[REGISTRATION_DATETIME]
//                                      ,[GENDER],[MIDDLE_NAME],[STREET_ADDRESS3]
//                                      ,[MOBILE],[IS_TRIAL]
//                                 FROM ADMIN_TBL_COMPANY_ADMINISTRATOR
//                                WHERE ADMIN_ID =(SELECT CmpAdmin.ADMIN_ID FROM ADMIN_TBL_COMPANY_ADMINISTRATOR CmpAdmin
//		                                INNER JOIN ADMIN_TBL_COMPANY_DETAIL AS CmpDtl
//		                                ON CmpAdmin.ADMIN_ID = CmpDtl.ADMIN_ID
//		                                WHERE CmpDtl.COMPANY_ID = @CompanyId)";

//            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
//            cmd.Parameters.AddWithValue("@CompanyId", _cmpRegistrationReq.CompanyInfo.CompanyId);
//            cmd.ExecuteNonQuery();
//        }

//        void transferCompanyDetailData(SqlConnection con,
//            SqlTransaction transaction,
//            long updatedOn)
//        {
//            string strQuery = @"INSERT INTO TBL_COMPANY_DETAIL([COMPANY_ID]
//                                   ,[COMPANY_NAME],[REGISTRATION_NO]
//                                   ,[STREET_ADDRESS1],[STREET_ADDRESS2]
//                                   ,[STREET_ADDRESS3],[CITY_NAME]
//                                   ,[STATE_NAME],[COUNTRY_CODE]
//                                   ,[ZIP],[ADMIN_ID]
//                                   ,[LOGO_IMAGE_NAME],[SUPPORT_EMAIL]
//                                   ,[SUPPORT_CONTACT],[UPDATED_ON]
//                                   ,[TIMEZONE_ID])
//                                    SELECT [COMPANY_ID],[COMPANY_NAME],[REGISTRATION_NO]
//                                          ,[STREET_ADDRESS1],[STREET_ADDRESS2],[STREET_ADDRESS3]
//                                          ,[CITY_NAME],[STATE_NAME],[COUNTRY_CODE]
//                                          ,[ZIP],[ADMIN_ID],[LOGO_IMAGE_NAME]
//                                          ,[SUPPORT_EMAIL],[SUPPORT_CONTACT]
//                                          ,@UpdatedOn,[TIMEZONE_ID]
//                                     FROM ADMIN_TBL_COMPANY_DETAIL
//                                    WHERE COMPANY_ID = @CompanyId";

//            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
//            cmd.Parameters.AddWithValue("@CompanyId", _cmpRegistrationReq.CompanyInfo.CompanyId);
//            cmd.Parameters.AddWithValue("@UpdatedOn", updatedOn);
//            cmd.ExecuteNonQuery();
//        }
//        void transferCompanyCurrentPlanData(SqlConnection con, SqlTransaction transaction)
//        {
//            string strQuery = @"INSERT INTO TBL_COMPANY_CURRENT_PLAN
//                                    SELECT * FROM ADMIN_TBL_COMPANY_CURRENT_PLAN
//                                    WHERE COMPANY_ID = @CompanyId";

//            SqlCommand cmd = new SqlCommand(strQuery, con, transaction);
//            cmd.Parameters.AddWithValue("@CompanyId", _cmpRegistrationReq.CompanyInfo.CompanyId);
//            cmd.ExecuteNonQuery();
//        }


//        MFETrialPlanDetail getDefaultTrialPlan()
//        {
//            MFETrialPlanDetail objTrialPlanDtl =
//                new MFETrialPlanDetail();
//            GetAvailablePlanList objPlanList =
//                new GetAvailablePlanList(GetAvailablePlanList.PLAN_TYPE.Trial);
//            objPlanList.Process();
//            if (objPlanList.StatusCode == 0 &&
//                objPlanList.PlanListDtbl.Rows.Count > 0)
//            {
//                string filter = String.Format("IS_DEFAULT = {0}", 1);
//                DataRow[] rows = objPlanList.PlanListDtbl.Select(filter);
//                if (rows.Length > 0)
//                {
//                    objTrialPlanDtl.PlanCode = Convert.ToString(rows[0]["PLAN_CODE"]);
//                    objTrialPlanDtl.PlanId = Convert.ToString(rows[0]["PLAN_ID"]);
//                    objTrialPlanDtl.PlanName = Convert.ToString(rows[0]["PLAN_NAME"]);
//                    objTrialPlanDtl.MaxWorkFlow = Convert.ToInt32(rows[0]["MAX_WORKFLOW"]);
//                    objTrialPlanDtl.MaxUsers = Convert.ToInt32(rows[0]["MAX_USER"]);
//                    objTrialPlanDtl.PushMessagePerDay = Convert.ToInt32(rows[0]["PUSHMESSAGE_PERDAY"]);
//                    objTrialPlanDtl.PushMessagePerMonth = Convert.ToInt32(rows[0]["PUSHMESSAGE_PERMONTH"]);
//                    objTrialPlanDtl.ValidMonths = Convert.ToInt32(rows[0]["MONTH"]);
//                    objTrialPlanDtl.IsDefault = Convert.ToBoolean(rows[0]["IS_DEFAULT"]);
//                    return objTrialPlanDtl;
//                }
//                else
//                {
//                    //we should get a record.
//                    throw new Exception(
//                        (((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString()));
//                }
//            }
//            else
//            {
//                //we should get a record.
//                throw new Exception(
//                    (((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString()));
//            }
//        }
//        MFEmficientServer getDefaultMfServerDtl()
//        {
//            MFEmficientServer objDefaultMfServer =
//                    new MFEmficientServer();
//            GetMasterServerDetails objServerDtl = new GetMasterServerDetails();
//            objDefaultMfServer = objServerDtl.getDefaultMficientServer();
//            if (objServerDtl.StatusCode != 0)
//            {
//                throw new Exception(objServerDtl.StatusDescription);
//            }
//            else
//            {
//                return objDefaultMfServer;
//            }
//        }
//        MFEmPluginServer getDefaultMpluginServerDtl()
//        {
//            MFEmPluginServer objDefaultMPluginServer =
//                    new MFEmPluginServer();
//            GetMpluginServerDetail objServerDtl = new GetMpluginServerDetail();
//            objDefaultMPluginServer = objServerDtl.getDefaultMpluginServer();
//            if (objServerDtl.StatusCode != 0)
//            {
//                throw new Exception(objServerDtl.StatusDescription);
//            }
//            else
//            {
//                return objDefaultMPluginServer;
//            }
//        }
//        MFEmBuzzServer getDefaultMbuzzServerDtl()
//        {
//            MFEmBuzzServer objDefaultMbuzzServer =
//                    new MFEmBuzzServer();
//            GetMbuzzServerDetail objServerDtl = new GetMbuzzServerDetail();
//            objDefaultMbuzzServer = objServerDtl.getDefaultMbuzzServer();
//            if (objServerDtl.StatusCode != 0)
//            {
//                throw new Exception(objServerDtl.StatusDescription);
//            }
//            else
//            {
//                return objDefaultMbuzzServer;
//            }
//        }
//    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class mGramGetEnterpriseAndMPAgentDetailReq
    {
        string _requestId, _companyId;
        int _functionCode;

        public string RequestId
        {
            get { return _requestId; }
        }

        public string CompanyId
        {
            get { return _companyId; }
        }

        int FunctionCode
        {
            get { return _functionCode; }
        }

        public mGramGetEnterpriseAndMPAgentDetailReq(string requestJson)
        {
            try
            {
                RequestJsonParsing<mGramGetEnterpriseAndMPAgentDetailReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<mGramGetEnterpriseAndMPAgentDetailReqData>>(requestJson);
                _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
                if (_functionCode != (int)FUNCTION_CODES.MGRAM_ENTERPRISE_DETAIL)
                {
                    throw new Exception("Invalid Function Code");
                }
                _requestId = objRequestJsonParsing.req.rid;
                _companyId = objRequestJsonParsing.req.data.enid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    [DataContract]
    public class mGramGetEnterpriseAndMPAgentDetailReqData : Data
    {
    }
}
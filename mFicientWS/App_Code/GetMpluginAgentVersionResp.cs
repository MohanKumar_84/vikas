﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Data;

namespace mFicientWS
{
    public class GetMpluginAgentVersionResp
    {
        private ResponseStatus _respStatus;
        private string _requestId, _CompanyId;
        string _minVersion,_majorVersion;
        public GetMpluginAgentVersionResp(ResponseStatus respstatus, string requestId, string CompanyId,string minVersion,string majorVersion)
        {
            try
            {
                _respStatus = respstatus;
                _requestId = requestId;
                _CompanyId = CompanyId;
                _minVersion = minVersion;
                _majorVersion = majorVersion;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Get  Mplugin Agent  Version  Response[" + ex.Message + "]");
            }
        }

        public string GetAgentVersionRespJson()
        {
            GetMpluginAgentVersionResponse objgetMpluginAgentVersionRespJson = new GetMpluginAgentVersionResponse();
            objgetMpluginAgentVersionRespJson.rid = _requestId;
            objgetMpluginAgentVersionRespJson.enid = _CompanyId;
            objgetMpluginAgentVersionRespJson.data = new GetMpluginAgentVersionData();
            objgetMpluginAgentVersionRespJson.data.minver = _minVersion;
            objgetMpluginAgentVersionRespJson.data.majver = _majorVersion;
            string strJson = Utilities.SerializeJson<GetMpluginAgentVersionResponse>(objgetMpluginAgentVersionRespJson);
            return Utilities.getFinalJson(strJson); 
        }
        [DataContract]
        public class GetMpluginAgentVersionResponse : CommonResponse
        {
            [DataMember]
            public GetMpluginAgentVersionData data { get; set; }

            [DataMember]
            public string enid { get; set; }
        }
        [DataContract]
        public class GetMpluginAgentVersionData
        {
            [DataMember]
            public string minver { get; set; }
            [DataMember]
            public string majver { get; set; }
        }
    }
}
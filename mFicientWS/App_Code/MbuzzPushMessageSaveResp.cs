﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class MbuzzPushMessageSaveResp
    {
        ResponseStatus _respStatus;
        string _requestId;

        public MbuzzPushMessageSaveResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting contact status Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            PushMessageforContactStatusResponse objPushMessageforContactStatusResp = new PushMessageforContactStatusResponse();
            objPushMessageforContactStatusResp.func = Convert.ToString((int)FUNCTION_CODES.MBUZZ_PUSHMESSAGE_SAVE);
            objPushMessageforContactStatusResp.rid = _requestId;
            objPushMessageforContactStatusResp.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<PushMessageforContactStatusResponse>(objPushMessageforContactStatusResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class PushMessageforContactStatusResponse : CommonResponse
    {
        public PushMessageforContactStatusResponse()
        {

        }
    }
}
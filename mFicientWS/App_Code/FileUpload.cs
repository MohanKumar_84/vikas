﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Text;
//using System.IO;
//using System.Net;
//using System.Data;
//using System.Data.SqlClient;

//namespace mFicientWS
//{
//    public class FileUpload
//    {
//        string bucketName = "mficient";
//        FileUploadReq fileUploadReq;

//        public FileUpload(FileUploadReq _fileUploadReq)
//        {
//            fileUploadReq = _fileUploadReq;
//        }

//        public FileUploadResp Process()
//        {
//            ResponseStatus objRespStatus = new ResponseStatus();
//            string bucketUserName = string.Empty, accessKey = string.Empty, secretAccessKey = string.Empty;
//            try
//            {
//                DataSet dsS3ClientDetails = Utilities.GetS3BucketDetails(BucketType.MFICIENT_WEBSERVICE);
//                if (dsS3ClientDetails != null && dsS3ClientDetails.Tables.Count > 0)
//                {
//                    bucketUserName = dsS3ClientDetails.Tables[0].Rows[0]["USER_NAME"].ToString();
//                    accessKey = dsS3ClientDetails.Tables[0].Rows[0]["ACCESS_KEY"].ToString();
//                    secretAccessKey = dsS3ClientDetails.Tables[0].Rows[0]["SECRET_ACCESS_KEY"].ToString();
//                }
//            }
//            catch (Exception ex)
//            {

//            }
//            if (!string.IsNullOrEmpty(fileUploadReq.MpluginAgentName))
//            {
//                try
//                {
//                    S3BucketFileManager s3BucketManager = new S3BucketFileManager(bucketName, accessKey, secretAccessKey);
//                    s3BucketManager.Download(fileUploadReq.S3Url, fileUploadReq.DestinationUrl);
//                    objRespStatus.cd = "0";
//                    objRespStatus.desc = "";
//                }
//                catch (Exception ex)
//                {
//                    objRespStatus.cd = "1500801";
//                    objRespStatus.desc = "Failed to upload file";
//                }
//            }
//            else
//            {
//                try
//                {
//                    string strAgentPassword = string.Empty;
//                    DataSet dsmPluginAgentDetails = getmPluginAgentPAssword();
//                    if (dsmPluginAgentDetails != null && dsmPluginAgentDetails.Tables[0] != null)
//                    {
//                        if (dsmPluginAgentDetails.Tables[0].Rows.Count > 0)
//                        {
//                            strAgentPassword = Convert.ToString(dsmPluginAgentDetails.Tables[0].Rows[0]["MP_AGENT_PASSWORD"]);
//                        }
//                        else
//                        {
//                            throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
//                        }
//                    }
//                    else
//                    {
//                        throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
//                    }
//                    mPlugin.UploadFileUsingS3(fileUploadReq.CompanyId, fileUploadReq.MpluginAgentName, strAgentPassword, fileUploadReq.S3Url, fileUploadReq.DestinationUrl);
//                }
//                catch (mPlugin.mPluginException mpEx)
//                {
//                    if (mpEx.Message == "1500801")
//                    {
//                        objRespStatus.cd = "1500801";
//                        objRespStatus.desc = "Failed to upload file";
//                    }
//                }
//                catch (HttpException httpEx)
//                {

//                }
//                catch (Exception ex)
//                {
//                    objRespStatus.cd = "1500801";
//                    objRespStatus.desc = "Failed to upload file";
//                }
//            }

//            return new FileUploadResp(objRespStatus, fileUploadReq.RequestId);
//        }

//        private DataSet getmPluginAgentPAssword()
//        {
//            string strQuery = "select MP_AGENT_PASSWORD from TBL_MPLUGIN_AGENT_DETAIL where COMPANY_ID=@COMPANY_ID and MP_AGENT_NAME=@MP_AGENT_NAME";
//            SqlCommand cmd = new SqlCommand(strQuery);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@COMPANY_ID", fileUploadReq.CompanyId);
//            cmd.Parameters.AddWithValue("@MP_AGENT_NAME", fileUploadReq.MpluginAgentName);
//            try
//            {
//                return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }
//    }
//}
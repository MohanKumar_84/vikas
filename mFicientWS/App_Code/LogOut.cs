﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientWS
{
    public class LogOut
    {
        LogOutReq _logOutReq;
        System.Web.Caching.Cache _appCache;
        public LogOut(LogOutReq logOutReq, System.Web.Caching.Cache cache)
        {
            _logOutReq = logOutReq;
            _appCache = cache;
        }
        public LogOutResp Process()
        {
            try
            {
                //updateLoginHistory();
                CacheManager.Remove(CacheManager.GetKey(
                    CacheManager.CacheType.mFicientSession, _logOutReq.CompanyId,
                    _logOutReq.UserName, _logOutReq.DeviceId,
                    _logOutReq.DeviceType,
                    String.Empty, String.Empty,
                    String.Empty
                    )
                    );
                ResponseStatus objResponseStatus = new ResponseStatus();
                objResponseStatus.cd = "0";
                objResponseStatus.desc = "";

                SqlConnection con = new SqlConnection();
                try
                {
                    MSSqlDatabaseClient.SqlConnectionOpen(out con, MSSqlDatabaseClient.getConnectionStringFromWebConfig(
                        MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MGRAM));
                    //mFicientCommonProcess.SendNotification.ResetUserNotificationBadgeCount(con, null, _logOutReq.UserName, _logOutReq.CompanyId);
                    string cacheKey = CacheManager.GetKey(  CacheManager.CacheType.mFicientSession, _logOutReq.CompanyId, _logOutReq.UserName, _logOutReq.DeviceId, _logOutReq.DeviceType,"", "", String.Empty );
                    SessionUser _session = CacheManager.Get<SessionUser>(cacheKey, false);

                    if (_session != null)
                    {
                        Utilities.saveActivityLog(con, mFicientCommonProcess.ACTIVITYENUM.USER_LOGOUT, _logOutReq.CompanyId, "-1", _logOutReq.UserName,
                               _session.FullName, _logOutReq.DeviceId, _logOutReq.DeviceType, _logOutReq.SessionId, _session.Model, "", "", null);
                    }
                }
                catch { }
                finally
                {
                    MSSqlDatabaseClient.SqlConnectionClose(con);
                }


                return new LogOutResp(objResponseStatus, _logOutReq.RequestId);
            }
            catch
            {
                throw new Exception();
            }
        }

        int updateLoginHistory()
        {

            string strQuery = @"UPDATE TBL_MOBILE_USER_LOGIN_HISTORY
                                                SET LOGOUT_DATETIME = @LogOutDateTime,
                                                LOGOUT_TYPE = 'NORMAL'
                                                WHERE USER_ID = @UserId
                                                AND LOGOUT_DATETIME = 0
                                                AND LOGIN_DEVICE_TOKEN =@LoginDeviceToken
                                                AND LOGIN_DEVICE_TYPE_CODE =@LoginDeviceTypeCode
                                                AND  COMPANY_ID=@COMPANY_ID;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@UserId", _logOutReq.UserId);
            cmd.Parameters.AddWithValue("@LogOutDateTime", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@LoginDeviceToken", _logOutReq.DeviceId);
            cmd.Parameters.AddWithValue("@LoginDeviceTypeCode", _logOutReq.DeviceType);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _logOutReq.CompanyId);
            return MSSqlDatabaseClient.ExecuteNonQueryRecord(cmd);
        }
    }
}
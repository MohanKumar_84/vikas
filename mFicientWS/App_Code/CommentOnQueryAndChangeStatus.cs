﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class CommentOnQueryAndChangeStatus
    {
        CommentOnQueryAndChangeStatusReq _commentOnQueryReq;
        public CommentOnQueryAndChangeStatus(CommentOnQueryAndChangeStatusReq commentOnQueryReq)
        {
            _commentOnQueryReq = commentOnQueryReq;
        }

        public CommentOnQueryAndChangeStatusResp Process()
        {
            ResponseStatus objResponseStatus = new ResponseStatus();
            objResponseStatus.cd = "0";
            objResponseStatus.desc = "";
            int iReturnValAfterExecutingQuery = 0;
            SqlCommand cmd = new SqlCommand("dbo.InsertSupportQueryComment");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TOKEN_NO", _commentOnQueryReq.TokenNo);
            cmd.Parameters.AddWithValue("@COMMENT_ID", Utilities.GetMd5Hash(_commentOnQueryReq.TokenNo + System.DateTime.UtcNow.Ticks.ToString() + _commentOnQueryReq.UserId));
            cmd.Parameters.AddWithValue("@COMMENT_BY", _commentOnQueryReq.UserId);
            cmd.Parameters.AddWithValue("@USER_TYPE", 1);
            cmd.Parameters.AddWithValue("@COMMENT", _commentOnQueryReq.Comment);
            cmd.Parameters.AddWithValue("@COMMENTED_ON", System.DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@IS_RESOLVED_BY_CREATOR", _commentOnQueryReq.IsResolve ? 1 : 0);
            cmd.Parameters.AddWithValue("@RESOLVED_ON", System.DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _commentOnQueryReq.CompanyId);
            cmd.Parameters.AddWithValue("@RESOLVED_BY", _commentOnQueryReq.UserId);
            cmd.Parameters.AddWithValue("@USER_ID", _commentOnQueryReq.UserId);
            cmd.Parameters.Add("@ReturnVal", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            MSSqlDatabaseClient.ExecuteNonQueryRecord(
               cmd,
               MSSqlDatabaseClient.getConnectionStringFromWebConfig(MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MFICIENT)
               );
            iReturnValAfterExecutingQuery = Convert.ToInt32(cmd.Parameters["@ReturnVal"].Value);
            if (iReturnValAfterExecutingQuery < 0)//returning -1,-2,-3 for different condition and if acceptted.
            {
                return new CommentOnQueryAndChangeStatusResp(objResponseStatus,
                    _commentOnQueryReq.RequestId,
                    true);
            }
            else
            {
                return new CommentOnQueryAndChangeStatusResp(
                    objResponseStatus,
                    _commentOnQueryReq.RequestId,
                    false);
            }
        }
        #region Previous Code
        //public CommentOnQueryAndChangeStatusResp Process()
        //{
        //    ResponseStatus objResponseStatus = new ResponseStatus();
        //    objResponseStatus.cd = "0";
        //    objResponseStatus.desc = "";
        //    SqlCommand cmd = null;
        //    if (!string.IsNullOrEmpty(_renewSessionReq.Comment))
        //    {
        //        cmd = new SqlCommand("INSERT INTO [ADMIN_TBL_QUERY_THREAD]([TOKEN_NO],[COMMENT_ID],[COMMENT_BY],[USER_TYPE],[COMMENT],[COMMENTED_ON])" +
        //        "VALUES (@TOKEN_NO,@COMMENT_ID,@COMMENT_BY,@USER_TYPE,@COMMENT,@COMMENTED_ON)");
        //        cmd.CommandType = CommandType.Text;

        //        cmd.Parameters.AddWithValue("@TOKEN_NO", _renewSessionReq.TokenNo);
        //        cmd.Parameters.AddWithValue("@COMMENT_ID", Utilities.GetMd5Hash(_renewSessionReq.TokenNo + System.DateTime.UtcNow.Ticks.ToString() + _renewSessionReq.UserId));
        //        cmd.Parameters.AddWithValue("@COMMENT_BY", _renewSessionReq.UserId);
        //        cmd.Parameters.AddWithValue("@USER_TYPE", 1);
        //        cmd.Parameters.AddWithValue("@COMMENT", _renewSessionReq.Comment);
        //        cmd.Parameters.AddWithValue("@COMMENTED_ON", System.DateTime.UtcNow.Ticks);
        //        MSSqlClient.ExecuteNonQueryRecord(cmd);
        //    }
        //    if (_renewSessionReq.IsResolve)
        //    {
        //        cmd = new SqlCommand("update  ADMIN_TBL_QUERY set RESOLVED_BY_CREATOR='true',RESOLVED_ON=@RESOLVED_ON,RESOLVED_BY=@RESOLVED_BY where TOKEN_NO=@TOKEN_NO AND  COMPANY_ID=@COMPANY_ID");
        //        cmd.CommandType = CommandType.Text;

        //        cmd.Parameters.AddWithValue("@TOKEN_NO", _renewSessionReq.TokenNo);
        //        cmd.Parameters.AddWithValue("@RESOLVED_ON", System.DateTime.UtcNow.Ticks);
        //        cmd.Parameters.AddWithValue("@COMPANY_ID", _renewSessionReq.CompanyId);
        //        cmd.Parameters.AddWithValue("@RESOLVED_BY", _renewSessionReq.UserId);
        //        MSSqlClient.ExecuteNonQueryRecord(cmd);
        //    }

        //    return new CommentOnQueryAndChangeStatusResp(objResponseStatus, _renewSessionReq.RequestId);
        //}
        #endregion
    }
}
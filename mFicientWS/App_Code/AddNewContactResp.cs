﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class AddNewContactResp
    {
        ResponseStatus _respStatus;
        string _requestId;
        UserData _objUserData;

        public AddNewContactResp(ResponseStatus respStatus, string requestId, UserData objUserData)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
                _objUserData = objUserData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Adding New Contact Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            AddNewContactResponse objAddNewContactResp = new AddNewContactResponse();
            AddNewContactResponsedata objAddNewContactResponsedata = new AddNewContactResponsedata();
            objAddNewContactResp.func = Convert.ToString((int)FUNCTION_CODES.ADD_CONTACT_REQUEST);
            objAddNewContactResp.rid = _requestId;
            objAddNewContactResp.status = _respStatus;
            objAddNewContactResponsedata.user = _objUserData;
            objAddNewContactResp.data = objAddNewContactResponsedata;
            string strJsonWithDetails = Utilities.SerializeJson<AddNewContactResponse>(objAddNewContactResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class AddNewContactResponse : CommonResponse
    {
        public AddNewContactResponse()
        {
            
        }
        [DataMember]
        public AddNewContactResponsedata data { get; set; }
    }
    public class AddNewContactResponsedata 
    {
        public AddNewContactResponsedata()
        {

        }
        [DataMember]
        public UserData user { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientWS
{
    public class GetMPluginAgentDtl
    {
        enum RUN_QUERY_BY
        {
            CompanyId,
            CompanyAndAgentName,
        }
        string _companyId, _mpAgentId, _mpAgentName, _mpAgentPassword;
        int _statusCode;
        string _statusDescription;
        RUN_QUERY_BY _eRunQueryBy;


        public GetMPluginAgentDtl(string companyId)
        {
            this.CompanyId = companyId;
            this.ERunQueryBy = RUN_QUERY_BY.CompanyId;
        }
        public GetMPluginAgentDtl(string companyId, string agentName)
        {
            this.CompanyId = companyId;
            this.MpAgentName = agentName;
            this.ERunQueryBy = RUN_QUERY_BY.CompanyAndAgentName;
        }
        public void Process()
        {
            try
            {
                DataTable dtblAgentDtl = null;
                switch (ERunQueryBy)
                {
                    case RUN_QUERY_BY.CompanyId:
                        dtblAgentDtl = getMpluginAgentDtl(this.CompanyId);
                        break;
                    case RUN_QUERY_BY.CompanyAndAgentName:
                        dtblAgentDtl = getMpluginAgentDtl(
                            this.CompanyId,
                            this.MpAgentName
                            );
                        break;

                }

                this.MpAgentId = Convert.ToString(dtblAgentDtl.Rows[0]["MP_AGENT_ID"]);
                this.MpAgentName = Convert.ToString(dtblAgentDtl.Rows[0]["MP_AGENT_NAME"]);
                this.MpAgentPassword = Convert.ToString(dtblAgentDtl.Rows[0]["MP_AGENT_PASSWORD"]);
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
            }
            catch (Exception ex)
            {
                this.StatusCode = -1000;
                if (ex.Message == ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString())
                    this.StatusDescription = ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
                else
                    this.StatusDescription = "Internal server error.";

            }
        }
        DataTable getMpluginAgentDtl(string companyId)
        {
            string strQuery = getSqlQuery(ERunQueryBy);
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            DataSet dsAgentDtl = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (dsAgentDtl == null) throw new Exception("Internal server error.");
            if (dsAgentDtl.Tables[0].Rows.Count == 0)
                throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            return dsAgentDtl.Tables[0];
        }
        DataTable getMpluginAgentDtl(string companyId,
            string agentName)
        {
            string strQuery = getSqlQuery(ERunQueryBy);
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            cmd.Parameters.AddWithValue("@MP_AGENT_NAME", this.MpAgentName);
            DataSet dsAgentDtl = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (dsAgentDtl == null) throw new Exception("Internal server error.");
            if (dsAgentDtl.Tables[0].Rows.Count == 0)
                throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            return dsAgentDtl.Tables[0];
        }
        string getSqlQuery(RUN_QUERY_BY runQueryBy)
        {
            string strQuery = String.Empty;
            switch (runQueryBy)
            {
                case RUN_QUERY_BY.CompanyId:
                    strQuery = @"SELECT *
                        FROM TBL_MPLUGIN_AGENT_DETAIL
                        WHERE COMPANY_ID = @CompanyId";
                    break;
                case RUN_QUERY_BY.CompanyAndAgentName:
                    strQuery = @"SELECT *
                        FROM TBL_MPLUGIN_AGENT_DETAIL
                        WHERE COMPANY_ID = @CompanyId
                        AND MP_AGENT_NAME = @MP_AGENT_NAME";
                    break;
            }
            return strQuery;
        }
        #region Public Properties
        public string MpAgentId
        {
            get { return _mpAgentId; }
            private set { _mpAgentId = value; }
        }
        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }

        public string MpAgentName
        {
            get { return _mpAgentName; }
            private set { _mpAgentName = value; }
        }

        public string MpAgentPassword
        {
            get { return _mpAgentPassword; }
            private set { _mpAgentPassword = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }

        private RUN_QUERY_BY ERunQueryBy
        {
            get { return _eRunQueryBy; }
            set { _eRunQueryBy = value; }
        }
        #endregion

    }
}
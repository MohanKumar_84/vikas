﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientWS
{
    public class CompanyTimezone
    {

        static string  _timeZoneID;
        static TimeZoneInfo _cmpTimezoneInfo;

        public static TimeZoneInfo CmpTimezoneInfo
        {
            get { return CompanyTimezone._cmpTimezoneInfo; }
            private set { CompanyTimezone._cmpTimezoneInfo = value; }
        }
        public static string TimeZoneID
        {
            get { return CompanyTimezone._timeZoneID; }
            private set { CompanyTimezone._timeZoneID = value; }
        }
        private CompanyTimezone()
        {
        }

        #region Get Company's Local date Now
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns>Time in ticks</returns>
        ///// exception cref="System.Exception"></exception>
        //public static long getCompanyLocalDateNowTicks(int timeZoneOffset)
        //{
        //    try
        //    {
        //        DateTime dtInUTC = DateTime.UtcNow;
        //        int iTimeOffsetHrs = (timeZoneOffset / 60);
        //        int iTimeOffsetMin = (timeZoneOffset % 60);//getTimezoneOffset() returns -330 for india.Time zone for india is GMT+5:30
        //        return dtInUTC.AddHours(iTimeOffsetHrs).AddMinutes(iTimeOffsetMin).Ticks;
        //    }
        //    catch
        //    {
        //        throw new Exception();
        //    }
        //}
        public static long getCompanyLocalDateTicks(long unixTimeInSec,TimeZoneInfo tzi)
        {
            try
            {
                DateTime dtFromEpoch = timeFromEpochInUtc(unixTimeInSec);
                return TimeZoneInfo.ConvertTimeFromUtc(dtFromEpoch,
                    tzi).Ticks;
            }
            catch
            {
                throw new Exception();
            }
        }
        public static DateTime timeFromEpochInUtc(long unixSeconds)
        {
            DateTime dtEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return dtEpoch.AddSeconds(unixSeconds);
            //return dtEpoch.AddMilliseconds(unixSeconds);
        }
        public static long getEpoch(long dateToConvert)
        {
            return Convert.ToInt64((dateToConvert - 621355968000000000) / 10000000);
        }
        #endregion

        #region Get company time zone
        public static string getTimezoneIdString(string companyId)
        {
            CompanyDetail objCmpDtls = new
            CompanyDetail(companyId);
            objCmpDtls.Process();

            if (objCmpDtls.StatusCode == 0)
            {
                return objCmpDtls.TimeZoneId;
            }
            else
            {
                return String.Empty;
            }
        }
        /// <summary>
        /// Returns the timezone info of the company.
        /// returns TimeZoneInfo.Local if the saved Timezone Id is not found.
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static TimeZoneInfo getTimezoneInfo(string companyId)
        {
            string strTimezoneIdString = getTimezoneIdString(companyId);
            TimeZoneID = strTimezoneIdString;
            TimeZoneInfo tzi = getTimezoneInfoById(strTimezoneIdString);
            CmpTimezoneInfo = tzi;
            return tzi;
        }
        public static TimeZoneInfo getTimezoneInfoById(string timeZoneID)
        {
            TimeZoneInfo tzi;
            try
            {
                tzi = TimeZoneInfo.FindSystemTimeZoneById(timeZoneID);
            }
            catch (TimeZoneNotFoundException)
            {
                tzi = TimeZoneInfo.Local;
            }
            catch (InvalidTimeZoneException)
            {
                tzi = TimeZoneInfo.Local;
            }
            return tzi;
        }
        //        /// <summary>
        //        /// 
        //        /// </summary>
        //        /// <param name="companyId"></param>
        //        public static int getCompanyTimeZoneOffset(string companyId)
        //        {
        //            return getTimezoneOffset((string)getCompanyCountryCode(companyId).Rows[0]["COUNTRY_CODE"]);
        //        }
        //        static DataTable getCompanyCountryCode(string companyId)
        //        {
        //            try
        //            {
        //                string strQuery = @"SELECT COUNTRY_CODE FROM ADMIN_TBL_COMPANY_DETAIL
        //                                    WHERE COMPANY_ID = @CompanyId;";
        //                SqlCommand objSqlCommand = new SqlCommand(strQuery);
        //                objSqlCommand.CommandType = CommandType.Text;
        //                objSqlCommand.Parameters.AddWithValue("@CompanyId", companyId);
        //                DataSet dsCountryCode = MSSqlClient.SelectDataFromSqlCommand(objSqlCommand);
        //                if (dsCountryCode.Tables[0].Rows.Count > 0)
        //                {
        //                    return dsCountryCode.Tables[0];
        //                }
        //                else
        //                {
        //                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }
        //        }
        //        static string getTimezoneString(string countryCode)
        //        {
        //            try
        //            {
        //                string strQuery = @"SELECT * FROM ADMIN_TBL_MST_COUNTRY
        //                                    WHERE COUNTRY_CODE = @CountryCode;";
        //                SqlCommand objSqlCommand = new SqlCommand(strQuery);
        //                objSqlCommand.CommandType = CommandType.Text;
        //                objSqlCommand.Parameters.AddWithValue("@CountryCode", countryCode);
        //                DataSet dsCountryMaster = MSSqlClient.SelectDataFromSqlCommand(objSqlCommand);
        //                if (dsCountryMaster.Tables[0].Rows.Count > 0)
        //                {
        //                    return (string)dsCountryMaster.Tables[0].Rows[0]["TIMEZONE_STRING"];
        //                }
        //                else
        //                {
        //                    throw new Exception("Country code does not exist");
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }
        //        }
        //        static int getTimezoneOffset(string countryCode)
        //        {
        //            try
        //            {
        //                string strQuery = @"SELECT * FROM ADMIN_TBL_MST_COUNTRY
        //                                    WHERE COUNTRY_CODE = @CountryCode;";
        //                SqlCommand objSqlCommand = new SqlCommand(strQuery);
        //                objSqlCommand.CommandType = CommandType.Text;
        //                objSqlCommand.Parameters.AddWithValue("@CountryCode", countryCode);
        //                DataSet dsCountryMaster = MSSqlClient.SelectDataFromSqlCommand(objSqlCommand);
        //                if (dsCountryMaster.Tables[0].Rows.Count > 0)
        //                {
        //                    return Convert.ToInt32(dsCountryMaster.Tables[0].Rows[0]["TIMEZONE_OFFSET"]);
        //                }
        //                else
        //                {
        //                    throw new Exception("Country code does not exist");
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }
        //        }
        #endregion
    }
}
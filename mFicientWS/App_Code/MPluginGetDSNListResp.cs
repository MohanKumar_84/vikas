﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class MPluginGetDSNListResp
    {
        string  _statusCode, _statusDescription;
        MPluginGetDSNDsnField _dsnFields;
        public MPluginGetDSNListResp(MPluginGetDSNDsnField dsnLst, string statusCode, string statusDescription)
        {
            try
            {
                _dsnFields = dsnLst;
                _statusCode = statusCode;
                _statusDescription = statusDescription;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating MPluginGetDSN Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            MPluginGetDSNListRespFields objDSNListRespFields = new MPluginGetDSNListRespFields();
            objDSNListRespFields.cd = _statusCode;
            objDSNListRespFields.desc = _statusDescription;
            objDSNListRespFields.data = _dsnFields;
            string strJsonWithDetails = Utilities.SerializeJson<MPluginGetDSNListRespFields>(objDSNListRespFields);
            return Utilities.getFinalJson(strJsonWithDetails);
        }

        public string StatusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }

        public string StatusCode
        {
            get { return _statusCode; }
            set { _statusCode = value; }
        }

        public MPluginGetDSNDsnField Data
        {
            get { return _dsnFields; }
            set { _dsnFields = value; }
        }
    }

    public class MPluginGetDSNListRespFields
    {
        [DataMember]
        public string cd { get; set; }

        [DataMember]
        public string desc { get; set; }

        [DataMember]
        public MPluginGetDSNDsnField data { get; set; }
    }
    public class MPluginGetDSNDsnField
    {
        [DataMember]
        public List<DSN> dsn { get; set; }
    }

}
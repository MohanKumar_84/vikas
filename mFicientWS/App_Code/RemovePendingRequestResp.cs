﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class RemovePendingRequestResp
    {
        ResponseStatus _respStatus;
        string _requestId;

        public RemovePendingRequestResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Removing Pending Request Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            RemovePendingRequestResponse objRemovePendingRequestResp = new RemovePendingRequestResponse();
            objRemovePendingRequestResp.func = Convert.ToString((int)FUNCTION_CODES.REMOVE_PENDING_REQUEST);
            objRemovePendingRequestResp.rid = _requestId;
            objRemovePendingRequestResp.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<RemovePendingRequestResponse>(objRemovePendingRequestResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class RemovePendingRequestResponse : CommonResponse
    {
        public RemovePendingRequestResponse()
        {

        }
    }
}
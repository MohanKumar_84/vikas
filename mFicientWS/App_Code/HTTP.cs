﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Threading;
using System.Net;

//using System.Web.Security;
/// <summary>
/// Summary description for clsHTTP
/// </summary>
/// 
namespace mFicientWS
{
    public class HTTP
    {
        internal string strURL = string.Empty;
        private string UseURL = string.Empty;
        internal int Retry = 0, Timeout = 30000;
        private string strPostData = string.Empty, strParamJoiner = string.Empty;
        private int intPostAttempts = 0;
        internal int RetryDelay = 50;
        internal string ResponseText = string.Empty, ValidResponse = string.Empty;
        internal string InvalidResponse = string.Empty;
        internal Boolean blnRequestSuccess = false;
        internal string ErrorMessage = string.Empty;
        private Byte[] ContentData;
        private string strUsername = string.Empty, strPassword = string.Empty, strHttpRequestMethod = string.Empty,contantType="";
        private Boolean blnAuthenticationRequired = false;
        private Boolean blnWaitForResponse = true;
        private string strUserAgent = string.Empty;
        private EnumHttpAuthentication httpAuthentication = EnumHttpAuthentication.Basic;

        public HTTP(String URL)
        {
            int intPostQstnMark = 0;
            strURL = URL;
            try
            {
                intPostQstnMark = strURL.IndexOf("?");
                if (intPostQstnMark > 0)
                {
                    UseURL = strURL.Substring(0, intPostQstnMark);
                    strPostData = strURL.Substring(intPostQstnMark + 1, strURL.Length - UseURL.Length - 1);
                    strParamJoiner = "&";
                }
                else
                {
                    UseURL = strURL;
                }
            }
            catch
            {
                UseURL = strURL;
            }
        }
        internal void SetContantType(String _contantType)
        {
            contantType = _contantType;
        }
        public HTTP(string URL, Boolean _blnWaitForResponse)
        {
            int intPostQstnMark = 0;
            strURL = URL;
            blnWaitForResponse = _blnWaitForResponse;
            try
            {
                intPostQstnMark = strURL.IndexOf("?");
                if (intPostQstnMark > 0)
                {
                    UseURL = strURL.Substring(0, intPostQstnMark);
                    strPostData = strURL.Substring(intPostQstnMark + 1, strURL.Length - UseURL.Length - 1);
                    strParamJoiner = "&";
                }
                else
                {
                    UseURL = strURL;
                }
            }
            catch
            {
                UseURL = strURL;
            }
        }
        internal string URL
        {
            get
            {
                URL = strURL;
                return URL;
            }
            set
            {
                int intPostQstnMark = 0;
                strURL = value;
                try
                {
                    intPostQstnMark = strURL.IndexOf("?");
                    if (intPostQstnMark > 0)
                    {
                        UseURL = strURL.Substring(0, intPostQstnMark);
                        strPostData = strURL.Substring(intPostQstnMark + 1, strURL.Length - UseURL.Length - 1);
                        strParamJoiner = "&";
                    }
                    else
                    {
                        UseURL = strURL;
                    }
                }
                catch
                {
                    UseURL = strURL;
                }
            }
        }
        internal string UserAgent
        {
            set
            {
                strUserAgent = value;
            }
        }
        internal Boolean HttpAuthenticationRequired
        {
            get
            {
                HttpAuthenticationRequired = blnAuthenticationRequired;
                return HttpAuthenticationRequired;
            }
            set
            {
                blnAuthenticationRequired = value;
            }
        }
        internal void SetHttpCredentials(String UID, String PWD)
        {
            strUsername = UID;
            strPassword = PWD;
        }
        internal void AddRequestData(String VarName, String VarValue)
        {
            if (VarName.Trim().Length <= 0)
            {
                return;
            }
            strPostData = strPostData + strParamJoiner + VarName + "=" + System.Web.HttpUtility.UrlEncode(VarValue);
            strParamJoiner = "&";
        }
        internal void ClearRequestData()
        {
            strPostData = "";
            strParamJoiner = "";
        }

        internal string HttpRequestMethod
        {
            get
            {
                return strHttpRequestMethod;
            }
            set
            {
                strHttpRequestMethod = value;
            }
        }

        internal enum EnumHttpAuthentication
        {
            Basic = 0,
            Digest = 1,
            Ntlm = 2,
            Negotiate = 3,
            Kerberos = 4
        };

        internal EnumHttpAuthentication HttpAuthentication
        {
            get
            {
                return httpAuthentication;
            }
            set
            {
                httpAuthentication = value;
            }
        }

        public HttpResponseStatus Request()
        {
            HttpStatusCode StatusCode = HttpStatusCode.NotFound;
            blnRequestSuccess = false;
            try
            {
                int i = 0;
                intPostAttempts = 0;
                ResponseText = "";
                ErrorMessage = "";

                if (UseURL.Trim().Length <= 0) return new HttpResponseStatus(blnRequestSuccess, HttpStatusCode.BadRequest, @"URL not defined for HTTP request", string.Empty);

                try
                {
                    if (strHttpRequestMethod == WebRequestMethods.Http.Post) ContentData = System.Text.Encoding.UTF8.GetBytes(strPostData);
                }
                catch (Exception ex)
                {
                    ErrorMessage = ex.Message;
                }

                if (ErrorMessage.Trim().Length > 0) return new HttpResponseStatus(blnRequestSuccess, HttpStatusCode.BadRequest, ErrorMessage, string.Empty);

                HttpWebRequest myHttpWebRequest = null;
                HttpWebResponse myHttpWebResponse = null;
                StreamReader myHttpWebReader = null;
                string myHttpWebResult = "";
                Stream myHttpRequestStream = null;

                for (i = 0; i <= Retry; i++)
                {
                    ErrorMessage = "";
                    ResponseText = "";
                    try
                    {
                        //Creates an HttpWebRequest with the specified URL.
                        switch (strHttpRequestMethod)
                        {
                            case WebRequestMethods.Http.Post:
                                myHttpWebRequest = (HttpWebRequest)WebRequest.Create(UseURL);
                                break;

                            case WebRequestMethods.Http.Get:
                                if (strPostData.Trim().Length > 0) myHttpWebRequest = (HttpWebRequest)WebRequest.Create(UseURL + "?" + strPostData);
                                else myHttpWebRequest = (HttpWebRequest)WebRequest.Create(UseURL);
                                break;
                        }

                        myHttpWebRequest.Timeout = Timeout;
                        myHttpWebRequest.AllowAutoRedirect = true;
                        myHttpWebRequest.MaximumAutomaticRedirections = 10;
                        myHttpWebRequest.KeepAlive = true;
                        myHttpWebRequest.Method = strHttpRequestMethod;
                        myHttpWebRequest.AllowWriteStreamBuffering = true;
                        myHttpWebRequest.SendChunked = false;
                        myHttpWebRequest.ServicePoint.Expect100Continue = false;

                        if (!string.IsNullOrEmpty(strUserAgent)) myHttpWebRequest.UserAgent = strUserAgent;

                        if (strHttpRequestMethod == WebRequestMethods.Http.Post)
                        {
                            myHttpWebRequest.ContentType = contantType;
                            myHttpWebRequest.ContentLength = ContentData.Length;
                            myHttpRequestStream = myHttpWebRequest.GetRequestStream();
                            myHttpRequestStream.Write(ContentData, 0, ContentData.Length);
                            myHttpRequestStream.Close();
                        }

                        if (strUsername.Trim().Length > 0 || strPassword.Trim().Length > 0)
                        {
                            try
                            {
                                Uri uri = new Uri(strURL);
                                string domainName = uri.Host;

                                CredentialCache myCredentialCache = new CredentialCache();
                                myCredentialCache.Add(uri, httpAuthentication.ToString(), new NetworkCredential(strUsername.Trim(), strPassword.Trim(), domainName));
                                myHttpWebRequest.Credentials = myCredentialCache;
                            }
                            catch (Exception ex)
                            {
                                ErrorMessage = "Credential Error";
                            }

                            if (ErrorMessage.Trim().Length > 0) return new HttpResponseStatus(blnRequestSuccess, HttpStatusCode.Unauthorized, ErrorMessage, string.Empty);
                        }

                        myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                        StatusCode = myHttpWebResponse.StatusCode;
                        // wait for Response 
                        if (blnWaitForResponse)
                        {
                            if (StatusCode == HttpStatusCode.OK)
                            {
                                myHttpWebReader = new StreamReader(myHttpWebResponse.GetResponseStream());
                                myHttpWebResult = myHttpWebReader.ReadToEnd();
                                ResponseText = myHttpWebResult;
                                blnRequestSuccess = true;
                            }
                            else
                            {
                                try
                                {
                                    myHttpWebReader = new StreamReader(myHttpWebResponse.GetResponseStream());
                                    myHttpWebResult = myHttpWebReader.ReadToEnd();
                                    ResponseText = myHttpWebResult;
                                }
                                catch
                                {
                                }
                                if (ResponseText.Trim().Length <= 0)
                                {
                                    ResponseText = myHttpWebResponse.StatusDescription;
                                }
                                if (ResponseText.Trim().Length <= 0)
                                    ResponseText = "ERROR " + myHttpWebResponse.StatusCode.ToString();
                                ErrorMessage = ResponseText;
                            }
                        }
                    }
                    catch (WebException wex)
                    {
                        StatusCode = ((System.Net.HttpWebResponse)wex.Response).StatusCode;
                        ErrorMessage = ((System.Net.HttpWebResponse)wex.Response).StatusDescription;
                    }
                    catch (Exception ex)
                    {
                        StatusCode = HttpStatusCode.BadRequest;
                        ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        if (myHttpRequestStream != null)
                        {
                            myHttpRequestStream.Close();
                            myHttpRequestStream = null;
                        }
                        if (myHttpWebReader != null)
                        {
                            myHttpWebReader.Close();
                            myHttpWebReader = null;
                        }
                        if (myHttpWebResponse != null)
                        {
                            myHttpWebResponse.Close();
                            myHttpWebResponse = null;
                        }
                        if (myHttpWebRequest != null) myHttpWebRequest = null;
                    }

                    if (blnRequestSuccess || !blnWaitForResponse) break;

                    if (i < Retry) Thread.Sleep(RetryDelay);
                    Thread.Sleep(1);
                }
                if (!blnRequestSuccess && ErrorMessage.Trim().Length <= 0)
                {
                    ErrorMessage = "HTTP Request Failed";
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }

            return new HttpResponseStatus(blnRequestSuccess, StatusCode, ErrorMessage, ResponseText);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class SavePushMsgFromUserResp
    {
        ResponseStatus _respStatus;
        string _requestId;

        public SavePushMsgFromUserResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Save push message from user Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            SavePushMsgFromUserResponse objSavePushMsgFromUserResp = new SavePushMsgFromUserResponse();
            objSavePushMsgFromUserResp.func = Convert.ToString((int)FUNCTION_CODES.SAVE_PUSH_MSG_FROM_USER);
            objSavePushMsgFromUserResp.rid = _requestId;
            objSavePushMsgFromUserResp.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<SavePushMsgFromUserResponse>(objSavePushMsgFromUserResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class SavePushMsgFromUserResponse : CommonResponse
    {
        public SavePushMsgFromUserResponse()
        { }
    }
}
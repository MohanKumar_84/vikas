﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class GetWebserviceUserList
    {
        GetWebservice_User_ListReq objwebserviceuserlistReq;


        public GetWebserviceUserList(GetWebservice_User_ListReq getlistuserdetails)
        {
            objwebserviceuserlistReq = getlistuserdetails;
        }
        public GetWebservice_User_ListResp Process()
        {
            ResponseStatus objRespStatus = new ResponseStatus();
            GetWebservice_User_ListRespMetaData objlist=new GetWebservice_User_ListRespMetaData();
            DataSet dsresult=new DataSet();
            try
            {
                string stquery = "";
                if (objwebserviceuserlistReq.GroupId == null || objwebserviceuserlistReq.GroupId == "") stquery = "select USER_ID,USER_NAME from TBL_USER_DETAIL WHERE COMPANY_ID=@CompanyId;";
                else stquery = @"select u.USER_ID,u.USER_NAME,g.GROUP_ID,g.COMPANY_ID from TBL_USER_DETAIL  as u inner join dbo.TBL_USER_GROUP_LINK as g
                                 on g.USER_ID=u.USER_ID where g.Group_ID=@GroupId  and  g.COMPANY_id=@CompanyId group by u.USER_ID,u.USER_NAME,g.GROUP_ID,g.COMPANY_ID;";
                SqlCommand cmd = new SqlCommand(stquery);
                cmd.Parameters.AddWithValue("@CompanyId", objwebserviceuserlistReq.CompanyId);
                cmd.Parameters.AddWithValue("@GroupId", objwebserviceuserlistReq.GroupId);
                cmd.CommandType = CommandType.Text;
                dsresult = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                List<Userss> listuser = new List<Userss>();
                foreach (DataRow row in dsresult.Tables[0].Rows)
                {
                    objRespStatus.cd = "0";
                    objRespStatus.desc = "";
                    Userss objregisteruser = new Userss();
                    objregisteruser.nm = Convert.ToString(row["USER_NAME"]);
                    objregisteruser.id = Convert.ToString(row["USER_ID"]);
                    listuser.Add(objregisteruser);
                }
                objlist.usr = listuser;
            }
           
            catch (Exception ex)
            {
                objRespStatus.cd = "0140101";
                objRespStatus.desc = "Error";
                if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            }
            return new GetWebservice_User_ListResp(objRespStatus, objlist ,objwebserviceuserlistReq.RequestId);
        }
        }
    }
   
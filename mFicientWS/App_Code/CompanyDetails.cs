﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientWS
{
    public class CompanyDetail
    {
        #region Private Variables
        string _enterpriseId, _companyName, _registrationNo;
        string _adminId, _logoImageName, _supportEmail, _supportContact;
        string _streetAddress1, _streetAddress2, _streetAddress3;
        string _cityName, _stateName, _countryCode, _zip;
        int _statusCode;
        string _statusDescription, _timeZoneId;
        bool _companyExists = false;

        

        #endregion
        #region Public Properties
        public string RegistrationNo
        {
            get { return _registrationNo; }
        }
        public string CompanyName
        {
            get { return _companyName; }
        }
        public string EnterpriseId
        {
            get { return _enterpriseId; }
        }
        public string StreetAddress3
        {
            get { return _streetAddress3; }
        }
        public string StreetAddress2
        {
            get { return _streetAddress2; }
        }
        public string StreetAddress1
        {
            get { return _streetAddress1; }
        }
        public string Zip
        {
            get { return _zip; }
        }
        public string CountryCode
        {
            get { return _countryCode; }
        }
        public string StateName
        {
            get { return _stateName; }
        }
        public string CityName
        {
            get { return _cityName; }
        }
        public string SupportContact
        {
            get { return _supportContact; }
        }
        public string SupportEmail
        {
            get { return _supportEmail; }
        }
        public string LogoImageName
        {
            get { return _logoImageName; }
        }
        public string AdminId
        {
            get { return _adminId; }
        }

        public int StatusCode
        {
            get { return _statusCode; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
        }
        public string TimeZoneId
        {
            get { return _timeZoneId; }
           private set { _timeZoneId = value; }
        }
        public bool CompanyExists
        {
            get { return _companyExists; }
            private set { _companyExists = value; }
        }
        #endregion

        public CompanyDetail(string enterpriseId)
        {
            _enterpriseId = enterpriseId;
        }

        public void Process()
        {
            try
            {
                string strQuery = @"SELECT * FROM TBL_COMPANY_DETAIL
                                WHERE COMPANY_ID = @CompanyId";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@CompanyId", _enterpriseId);
                DataSet dsCompanyDetails = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (dsCompanyDetails != null)
                {
                    if (dsCompanyDetails.Tables[0].Rows.Count > 0)
                    {
                        DataRow drCompanyDtl = dsCompanyDetails.Tables[0].Rows[0];
                        _companyName = Convert.ToString(drCompanyDtl["COMPANY_NAME"]);
                        _registrationNo = Convert.ToString(drCompanyDtl["REGISTRATION_NO"]);
                        _adminId = Convert.ToString(drCompanyDtl["ADMIN_ID"]);
                        _logoImageName = Convert.ToString(drCompanyDtl["LOGO_IMAGE_NAME"]);
                        _supportEmail = Convert.ToString(drCompanyDtl["SUPPORT_EMAIL"]);
                        _supportContact = Convert.ToString(drCompanyDtl["SUPPORT_CONTACT"]);
                        _streetAddress1 = Convert.ToString(drCompanyDtl["STREET_ADDRESS1"]);
                        _streetAddress2 = Convert.ToString(drCompanyDtl["STREET_ADDRESS2"]);
                        _streetAddress3 = Convert.ToString(drCompanyDtl["STREET_ADDRESS3"]);
                        _cityName = Convert.ToString(drCompanyDtl["CITY_NAME"]);
                        _stateName = Convert.ToString(drCompanyDtl["STATE_NAME"]);
                        _countryCode = Convert.ToString(drCompanyDtl["COUNTRY_CODE"]);
                        _zip = Convert.ToString(drCompanyDtl["ZIP"]);
                        _timeZoneId = Convert.ToString(drCompanyDtl["TIMEZONE_ID"]);
                        _statusCode = 0;
                        _statusDescription = "";
                        _companyExists = true;
                    }
                    else 
                    {
                        _companyExists = false;
                        _statusCode = 0;
                        _statusDescription = "";
                    }
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch(Exception ex)
            {
                _statusCode = -1000;
                _statusDescription = ex.Message;
            }
        }
    }
}
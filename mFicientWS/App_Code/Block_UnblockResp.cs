﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class Block_UnblockResp
    {
        ResponseStatus _respStatus;
        string _requestId;

        public Block_UnblockResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting contact status Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            Block_UnblockRespences objblockunblockresponse = new Block_UnblockRespences();
            objblockunblockresponse.rid = _requestId;
            objblockunblockresponse.func = Convert.ToString((int)FUNCTION_CODES.BLOCK_UNBLOCK_USER);
            objblockunblockresponse.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<Block_UnblockRespences>(objblockunblockresponse);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }
    public class Block_UnblockRespences : CommonResponse
    {
        public Block_UnblockRespences()
        { }
    }
}

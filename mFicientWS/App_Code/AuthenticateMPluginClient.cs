﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net;
namespace mFicientWS
{
    public class AuthenticateMPluginClient
    {
        AuthenticateMPluginClientReq _authenticateMPlugInClientReq;
        public AuthenticateMPluginClient(AuthenticateMPluginClientReq authenticateMPlugInClientReq)
        {
            _authenticateMPlugInClientReq = authenticateMPlugInClientReq;
        }

        public AuthenticateMPluginClientResp Process()
        {
            try
            {
                DataSet dsMPlugInClientDetails = getMPlugInClientDetails();
                if (dsMPlugInClientDetails != null && dsMPlugInClientDetails.Tables[0].Rows.Count > 0)
                {
                    Utilities.saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.MPLUGIN_CONNECTED, _authenticateMPlugInClientReq.CompanyId, "-1", _authenticateMPlugInClientReq.mpluginAgentName,
                           "", "", "", "", "", "", "",null);
                    return new AuthenticateMPluginClientResp(null, _authenticateMPlugInClientReq.RequestId);
                }
                else
                {
                    throw new Exception(((int)HttpStatusCode.Unauthorized).ToString()); ;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        DataSet getMPlugInClientDetails()
        {
            string strQuery = @"SELECT * FROM TBL_MPLUGIN_AGENT_DETAIL WHERE COMPANY_ID = @COMPANY_ID AND MP_AGENT_NAME = @MP_AGENT_NAME AND MP_AGENT_PASSWORD = @MP_AGENT_PASSWORD";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _authenticateMPlugInClientReq.CompanyId);
            cmd.Parameters.AddWithValue("@MP_AGENT_NAME", _authenticateMPlugInClientReq.mpluginAgentName);
            cmd.Parameters.AddWithValue("@MP_AGENT_PASSWORD", _authenticateMPlugInClientReq.Password);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }
    }
}
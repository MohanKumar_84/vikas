﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class MficientBase
    {
        string _statusDescription;
        int _statusCode;
        public string StatusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            set { _statusCode = value; }
        }
        public MficientBase()
        {
            this.StatusCode = 0;
            this.StatusDescription = String.Empty;
        }
    }
}
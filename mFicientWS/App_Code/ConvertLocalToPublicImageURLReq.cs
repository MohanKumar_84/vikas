﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class ConvertLocalToPublicImageURLReq
    {
        private int _functioncode;
        private string _requestId, _lasturl, _agentname, _companyId,_maxWidth;
        
        public string Agentname
        {
            get { return _agentname; }
        }
        public string ImageUrl
        {
            get { return _lasturl; }
        }
        public string CompanyID
        {
            get { return _companyId; }
        }
        public int FuntionCode
        {
            get { return _functioncode; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string MaxWidth
        {
            get { return _maxWidth; }
        }

        public ConvertLocalToPublicImageURLReq(string requestJson, string userId)
        {
            RequestJsonParsing<ConvertLocalToPublicImageURLData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<ConvertLocalToPublicImageURLData>>(requestJson);
            _functioncode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functioncode != (int)FUNCTION_CODES.CONVERT_LOCAL_TO_PUBLIC_IMAGEURL)
            {
                throw new Exception("Invalid Function Code");
            }
            //_sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            //_deviceId = objRequestJsonParsing.req.did;
            //_deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            //_username = objRequestJsonParsing.req.data.unm;
            _lasturl = objRequestJsonParsing.req.data.furl;
            _agentname = objRequestJsonParsing.req.data.mpn;
            _maxWidth = objRequestJsonParsing.req.data.mwd == null ? "" : objRequestJsonParsing.req.data.mwd;
            if (string.IsNullOrEmpty(_agentname)) throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            //_userId = userId;
        }
    }

    [DataContract]
    public class ConvertLocalToPublicImageURLData : Data
    {
        ///<summary>
        /// Image Url
        /// <summary>
        [DataMember]
        public string furl { get; set; }

        ///<summary>
        ///Agent Name
        ///</summary>
        [DataMember]
        public string mpn { get; set; }
        
        ///<summary>
        ///max Width
        ///</summary>
        [DataMember]
        public string mwd { get; set; }
    }
    
}
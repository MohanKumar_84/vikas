﻿using System;
using System.Security.Cryptography;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using System.Web.Configuration;
using System.Web;
using System.Net;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;

namespace mFicientWS
{

    public class Utilities
    {
        public static int convertColumnImagetype(int type)
        {
            return type == 1 ? 5 : (type == 2 ? 6 : (type == 3 ? 7 : 0));
        }
        public static void saveActivityLog(SqlConnection objSqlConnection, mFicientCommonProcess.ACTIVITYENUM type, string CompanyId, string SubAdminid, string para1, string para2, string para3, string para4, string para5, string para6, string para7, string para8, SqlTransaction trn)
        {
            try
            {
                if (objSqlConnection == null)
                    MSSqlDatabaseClient.SqlConnectionOpen(out objSqlConnection);

                List<mFicientCommonProcess.Activity> objActivity = new List<mFicientCommonProcess.Activity>();
                objActivity.Add(new mFicientCommonProcess.Activity(type, CompanyId, DateTime.UtcNow.Ticks, SubAdminid, para1, para2, para3, para4, para5, para6, para7, para8, mFicientCommonProcess.SOURCE.WEBSERVICE));
                mFicientCommonProcess.SaveActivityLog.ActivityPerform(objSqlConnection, objActivity, trn);
            }
            catch { }
        }
        public static string SerializeJson<T>(object obj)
        {
            T objResponse = (T)obj;
            MemoryStream stream = new MemoryStream();
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(objResponse.GetType());
            serializer.WriteObject(stream, objResponse);
            stream.Position = 0;
            StreamReader streamReader = new StreamReader(stream);
            return streamReader.ReadToEnd();
        }
        public static string getFinalJson(string jsonWithDetails)
        {
            string strFinalJson = "{\"resp\":" + jsonWithDetails + "}";
            return strFinalJson;
        }
        public static string getFinalRequestJson(string jsonWithDetails)
        {
            string strFinalJson = "{\"req\":" + jsonWithDetails + "}";
            return strFinalJson;
        }
        public static JObject getCommonResponseJson(FUNCTION_CODES fncd, string _requestId, ResponseStatus _respStatus)
        {
            JObject jobj = new JObject();
            jobj.Add("func", Convert.ToString((int)fncd));
            jobj.Add("rid", _requestId);

            JObject josts = new JObject();
            josts.Add("cd", _respStatus.cd);
            josts.Add("desc", _respStatus.desc);
            jobj.Add("status", josts);

            JObject jobjRes = new JObject();
            jobjRes.Add("resp", jobj);
            return jobjRes;
        }
        public static string GetMsgRefID()
        {
            Random random = new Random();
            string strMsgRefID = random.Next(989, 9999999).ToString() + DateTime.Now.Ticks.ToString();
            return strMsgRefID;
        }
        public static T DeserialiseJson<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer serialiser = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serialiser.ReadObject(ms);
            ms.Close();
            return obj;
        }
        public static string GetMd5Hash(string _inputString)
        {
            string strMD5;
            byte[] hashedDataBytes;
            try
            {
                byte[] bs = Encoding.UTF8.GetBytes(_inputString);
                System.Security.Cryptography.MD5CryptoServiceProvider md5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
                hashedDataBytes = md5Hasher.ComputeHash(bs);
                strMD5 = BitConverter.ToString(hashedDataBytes).Replace("-", "");
            }
            catch
            {
                strMD5 = string.Empty;
            }
            return strMD5;
        }
        public static bool VerifyMd5hash(string _input, string _hash)
        {
            if (StringComparer.OrdinalIgnoreCase.Compare(GetMd5Hash(_input), _hash) == 0)
            {
                return true;
            }
            return false;
        }
        #region Datetime Helper Functions
        public static long getEpoch(long dateToConvert)
        {
            return Convert.ToInt64((dateToConvert - 621355968000000000) / 10000000);
        }
        public static string getFullCompanyLocalFormattedDate(TimeZoneInfo tzi,
            long ticksInUTC)
        {
            DateTime dtInLocalTimezone = TimeZoneInfo.ConvertTimeFromUtc(
                 new DateTime(ticksInUTC, DateTimeKind.Utc),
                 tzi
                 );
            return getFullFormattedDateForUI(dtInLocalTimezone);
        }
        public static string getFormattedDateForUI(DateTime date)
        {
            return date.ToString("dd-MMM-yyyy");
        }
        public static string getFullFormattedDateForUI(DateTime date)
        {
            return date.ToString("F",
                  System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
        }
        /// <summary>
        /// Interprets year, month, and day as a year, month, and day in the Gregorian calendar
        /// Gives the date in dd-MMM-yyyy format.
        /// </summary>
        /// <param name="date">The format of date should be dd-mm-yyyy</param>
        /// <param name="separator">eg. For dd-mm-yyyy. separator is -</param>
        /// <returns></returns>
        public static string getFormattedDateForUI(
            string dateString,
            char separator)
        {
            string[] aryDateSelected = dateString.Split(separator);
            DateTime dtSelected =
                new DateTime(
                Convert.ToInt32(aryDateSelected[2]),
                Convert.ToInt32(aryDateSelected[1]),
                Convert.ToInt32(aryDateSelected[0])
                );
            return getFormattedDateForUI(dtSelected);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateString">The date format should be dd-mm-yyyy</param>
        /// <param name="min"></param>
        /// <param name="sec"></param>
        /// <param name="millisec"></param>
        /// <param name="separator">eg. For dd-mm-yyyy. separator is -</param>
        /// <returns></returns>
        public static DateTime getTimeInUtc(string dateString,
            int hrs,
            int min,
            int sec,
            char separator)
        {
            string[] aryDateSelected = dateString.Split(separator);
            return new DateTime(
                Convert.ToInt32(aryDateSelected[2]),
                Convert.ToInt32(aryDateSelected[1]),
                Convert.ToInt32(aryDateSelected[0]),
                hrs,
                min,
                sec
                ).ToUniversalTime();
        }
        #endregion
        public static Exception HttpExecptionToException(HttpException hex)
        {
            DO_TASK_ERROR dtError = DO_TASK_ERROR.NONE;
            switch (hex.GetHttpCode())
            {
                case (int)HttpStatusCode.ServiceUnavailable:
                    dtError = DO_TASK_ERROR.MPLUGIN_AGENT_NOT_FOUND;
                    break;
                case (int)HttpStatusCode.Unauthorized:
                    dtError = DO_TASK_ERROR.MPLUGIN_SERVER_UNAUTHORIZED;
                    break;
                case (int)HttpStatusCode.GatewayTimeout:
                case (int)HttpStatusCode.RequestTimeout:
                case (int)HttpStatusCode.InternalServerError:
                case (int)HttpStatusCode.NotFound:
                    dtError = DO_TASK_ERROR.MPLUGIN_SERVER_NOT_REACHABLE;
                    break;
                default:
                    dtError = DO_TASK_ERROR.MPLUGIN_UNKNOWN_ERROR;
                    break;
            }
            return new Exception(Convert.ToString(((int)dtError)), hex);
        }
        public static ResponseStatus HttpExecptionToResponseStatus(HttpException hex)
        {
            DO_TASK_ERROR dtError = DO_TASK_ERROR.NONE;
            switch (hex.GetHttpCode())
            {
                case (int)HttpStatusCode.ServiceUnavailable:
                    dtError = DO_TASK_ERROR.MPLUGIN_AGENT_NOT_FOUND;
                    break;
                case (int)HttpStatusCode.Unauthorized:
                    dtError = DO_TASK_ERROR.MPLUGIN_SERVER_UNAUTHORIZED;
                    break;
                case (int)HttpStatusCode.GatewayTimeout:
                case (int)HttpStatusCode.RequestTimeout:
                case (int)HttpStatusCode.InternalServerError:
                case (int)HttpStatusCode.NotFound:
                    dtError = DO_TASK_ERROR.MPLUGIN_SERVER_NOT_REACHABLE;
                    break;
            }
            return getResponseStatus(new Exception(Convert.ToString(((int)dtError)), hex));
        }
        public static ResponseStatus getResponseStatus(Exception e)
        {
            /**
             * get response status Returns response status
             * if we have defined the Error 
             * other wise it will throw the exception
             * passed to it  so that it is caught at mficient module.
             * **/

            ResponseStatus objRespStatus = new ResponseStatus();
            if (e != null)
            {
                DO_TASK_ERROR doTaskError = (DO_TASK_ERROR)Enum.Parse(typeof(DO_TASK_ERROR), e.Message);
                if (Enum.IsDefined(typeof(DO_TASK_ERROR), doTaskError))
                {
                    objRespStatus.cd = ((int)doTaskError).ToString();
                    switch (doTaskError)
                    {
                        case DO_TASK_ERROR.CONNECTION_ERROR:
                            objRespStatus.desc = "Error in connection.";
                            break;
                        case DO_TASK_ERROR.SQL_QUERY_ERROR:
                            objRespStatus.desc = "Query was incorrect";
                            break;
                        case DO_TASK_ERROR.SQL_PARAMETER_ERROR:
                            objRespStatus.desc = "Parameters provided incorrect";
                            break;
                        case DO_TASK_ERROR.WEB_SERVICE_PARAMETERS_ERROR:
                            objRespStatus.desc = "Web service url incorrect";
                            break;
                        case DO_TASK_ERROR.INVALID_COLUMN_IN_DATA_COMMAND:
                            objRespStatus.desc = "Invalid data command: column invalid";
                            break;
                        case DO_TASK_ERROR.INVALID_TAGS_IN_JSON_XML:
                            objRespStatus.desc = "Invalid tags provided";
                            break;
                        case DO_TASK_ERROR.MPLUGIN_ERROR:
                            objRespStatus.desc = e.InnerException.Message;
                            break;
                        case DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED:
                            objRespStatus.desc = "Tag path provided was incorrect";
                            break;
                        case DO_TASK_ERROR.MPLUGIN_SERVER_NOT_FOUND:
                            objRespStatus.cd = (((int)DO_TASK_ERROR.MPLUGIN_SERVER_NOT_FOUND)).ToString();
                            objRespStatus.desc = "Server not found";
                            break;
                        case DO_TASK_ERROR.INVALID_COMMAND_TYPE:
                            objRespStatus.desc = "Invalid command type";
                            break;
                        case DO_TASK_ERROR.INVALID_COMMAND:
                            objRespStatus.desc = "Invalid command";
                            break;
                        case DO_TASK_ERROR.XML_RPC_FAULT:
                            objRespStatus.desc = "Error executing the method.Method response returned a fault.";
                            break;
                        case DO_TASK_ERROR.MPLUGIN_AGENT_NOT_FOUND:
                            objRespStatus.desc = "Mplugin agent not found";
                            break;
                        case DO_TASK_ERROR.MPLUGIN_SERVER_NOT_REACHABLE:
                            objRespStatus.desc = "Mplugin server not reachable";
                            break;
                        case DO_TASK_ERROR.MPLUGIN_SERVER_UNAUTHORIZED:
                            objRespStatus.desc = "Mplugin server unautherized";
                            break;
                        case DO_TASK_ERROR.CACHE_LIST_IS_EMPTY:
                            objRespStatus.cd = (((int)DO_TASK_ERROR.DATA_IN_CACHE_EXPIRED)).ToString();
                            objRespStatus.desc = "Cache List Is Empty.";
                            break;
                        case DO_TASK_ERROR.DATA_IN_CACHE_EXPIRED:
                            objRespStatus.cd = (((int)DO_TASK_ERROR.DATA_IN_CACHE_EXPIRED)).ToString();
                            objRespStatus.desc = "No record found.";
                            break;
                        case DO_TASK_ERROR.TRANSACTION_ERROR:
                            objRespStatus.cd = (((int)DO_TASK_ERROR.DATA_IN_CACHE_EXPIRED)).ToString();
                            objRespStatus.desc = "Transaction Error.";
                            break;
                        case DO_TASK_ERROR.WEBSERVICE_NOT_REACHABLE:
                            objRespStatus.desc = "Webservice Not Reachable";
                            break;
                        case DO_TASK_ERROR.WEBSERVICE_ERROR:
                            objRespStatus.desc = "Webservice http Error";
                            break;
                        case DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR:
                            objRespStatus.desc = "Command Credential Error";
                            break;
                        default:
                            objRespStatus.desc = doTaskError.ToString();
                            break;
                    }
                }
                else
                {
                    throw e;
                }
            }
            else
            {
                objRespStatus.cd = ((int)DO_TASK_ERROR.NONE).ToString();
                objRespStatus.desc = " ";
            }
            return objRespStatus;
        }

        #region Do functions Of webservice
        public static string DoUserLogin(object _RequestObject, HttpContext _Context)
        {
            UserLoginReq objUserLoginReq = (UserLoginReq)_RequestObject;
            UserLogin objUserLogin = new UserLogin((UserLoginReq)_RequestObject);
            UserLoginResp objUserLoginResp = objUserLogin.Process(_Context);

            string sessionKey = CacheManager.GetKey(CacheManager.CacheType.mFicientSession,
                objUserLoginReq.CompanyId, objUserLoginReq.UserName,
                objUserLoginReq.DeviceId, objUserLoginReq.DeviceType,
                "", "", String.Empty);

            string companyTimezoneKey = CacheManager.GetKey(CacheManager.CacheType.CompanyTimezone,
                objUserLoginReq.CompanyId, String.Empty, String.Empty,
                String.Empty, String.Empty,
                String.Empty, String.Empty);

            if (objUserLoginResp.StatusCode == 0)
            {
                SessionUser session = new SessionUser(objUserLoginResp.CompanyId, objUserLoginResp.UserName, objUserLoginResp.DeviceType, objUserLoginResp.DeviceId, objUserLoginResp.UserId
                                                , objUserLoginResp.EmailId, objUserLoginResp.ModelType, objUserLoginResp.FullName);
                if (CacheManager.Insert<SessionUser>(sessionKey, session, DateTime.UtcNow, TimeSpan.FromSeconds(MficientConstants.SESSION_VALIDITY_SECONDS)) == null)
                    throw new Exception(((int)HttpStatusCode.BadRequest).ToString());

                objUserLoginResp.SessionId = session.SessionId;
                try
                {
                    saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.USER_LOGIN, objUserLoginResp.CompanyId, "", objUserLoginResp.UserName,
                            session.FullName, objUserLoginResp.DeviceId, objUserLoginResp.DeviceType, "0", objUserLoginResp.SessionId, objUserLoginResp.ModelType, "", null);
                    string strTimezoneID = CacheManager.Get<string>(companyTimezoneKey, false);
                    if (String.IsNullOrEmpty(strTimezoneID))
                    {
                        CacheManager.Insert<string>(
                            companyTimezoneKey,
                            Convert.ToString(
                            CompanyTimezone.getTimezoneIdString(objUserLoginResp.CompanyId)
                            ),
                            DateTime.UtcNow,
                            TimeSpan.FromSeconds(MficientConstants.SESSION_VALIDITY_SECONDS)
                            );
                    }
                }
                catch
                { }
            }
            else
            {
                saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.USER_LOGIN, objUserLoginReq.CompanyId, "", objUserLoginReq.UserName,
                           objUserLoginResp.UserId, objUserLoginReq.DeviceId, objUserLoginReq.DeviceType, "1", objUserLoginResp.StatusCode.ToString(), "", "", null);
                objUserLoginResp.SessionId = "";
                CacheManager.Remove(sessionKey);
            }

            return objUserLoginResp.GetResponseJson();
        }
        public static string DoResetPassword(object _RequestObject, HttpContext context)
        {
            ResetPassword objResetPassword = new ResetPassword((ResetPasswordReq)_RequestObject);
            ResetPasswordResp objResetPasswordResp = objResetPassword.Process(context);
            return objResetPasswordResp.GetResponseJson();
        }
        public static string DoChangePassword(object _RequestObject, HttpContext context)
        {
            ChangePassword objChangePassword = new ChangePassword((ChangePasswordReq)_RequestObject);
            ChangePasswordResp objChangePasswordResp = objChangePassword.Process(context);
            return objChangePasswordResp.GetResponseJson();
        }
        public static string DoDesktopUserLogin(object _RequestObject)
        {
            DesktopUserLogin objDesktopUserLogin = new DesktopUserLogin((DesktopUserLoginReq)_RequestObject);
            DesktopUserLoginResp objDesktopUserLoginResp = objDesktopUserLogin.Process();
            return objDesktopUserLoginResp.GetResponseJson();
        }
        public static string DoUserDetail(object _RequestObject)
        {
            UserDetail objUserDtl = new UserDetail((UserDetailReq)_RequestObject);
            UserDetailResp objUserDtlResp = objUserDtl.Process();
            return objUserDtlResp.GetResponseJson();
        }
        public static string DoGetOfflineTableMeta(object _RequestObject)
        {
            Get_Offline_Data_Table_Meta objUserDtl = new Get_Offline_Data_Table_Meta((Get_Offline_Datatable_Meta_Req)_RequestObject);
            return objUserDtl.Process();
        }
        public static string DoGetOfflineObjectMeta(object _RequestObject)
        {
            Get_Offline_Data__Objects objUserDtl = new Get_Offline_Data__Objects((Get_Offline_Data__Objects_Req)_RequestObject);
            Get_Offline_Data__Objects_Resp objUserDtlResp = objUserDtl.Process();
            return objUserDtlResp.GetResponseJson();
        }
        public static string DoRenewSession(object _RequestObject, System.Web.Caching.Cache cache)
        {
            ResponseStatus objRespStatus = new ResponseStatus();
            RenewSessionResp objRenewSessionResp;
            try
            {
                RenewSessionReq objRequest = (RenewSessionReq)_RequestObject;

                string sessionKey = CacheManager.GetKey(
                    CacheManager.CacheType.mFicientSession,
                    objRequest.CompanyId, objRequest.UserName,
                    objRequest.DeviceId, objRequest.DeviceType,
                    String.Empty, String.Empty, String.Empty
                    );

                SessionUser session = CacheManager.Get<SessionUser>(sessionKey, false);
                session.Renew(objRequest.SessionId);

                CacheManager.Insert<SessionUser>(sessionKey, session, DateTime.UtcNow, TimeSpan.FromSeconds(MficientConstants.SESSION_VALIDITY_SECONDS));

                objRespStatus.cd = "0";
                objRespStatus.desc = "";
                objRenewSessionResp = new RenewSessionResp(objRespStatus, objRequest.RequestId, session.SessionId);

                saveActivityLog(null, mFicientCommonProcess.ACTIVITYENUM.SESSTION_RENEW, objRequest.CompanyId, "-1", objRequest.UserName,
                          objRequest.UserId, objRequest.DeviceId, objRequest.DeviceType, session.Model, objRequest.SessionId, session.SessionId, "", null);
                return objRenewSessionResp.GetResponseJson();
            }
            catch (ArgumentException e)
            {
                throw new Exception(((int)HttpStatusCode.Unauthorized).ToString());
            }
        }
        public static string DoLogOut(object _RequestObject, System.Web.Caching.Cache cache)
        {
            LogOut objLogOut = new LogOut((LogOutReq)_RequestObject, cache);
            LogOutResp objLogOutResp = objLogOut.Process();
            return objLogOutResp.GetResponseJson();
        }
        public static string DoDeviceRegistrationRequest(object _RequestObject, HttpContext context)
        {
            DeviceRegistrationRequest objDeviceRegistrationReq = new DeviceRegistrationRequest((DeviceRegistrationRequestReq)_RequestObject);
            DeviceRegistrationRequestResp objDeviceRegistrationResp = objDeviceRegistrationReq.Process(context);
            return objDeviceRegistrationResp.GetResponseJson();
        }
        public static string DoDoTaskRequest(object _RequestObject)
        {
            DoTask objDoTask = new DoTask((DoTaskReq)_RequestObject);
            DoTaskResp objDoTaskResp = objDoTask.Process();
            return objDoTaskResp.GetResponseJson();
        }
        public static string DoDoTaskRequestForBatchProcess(object _RequestObject)
        {
            DoTaskWebServiceForBatchProcess objDoTask = new DoTaskWebServiceForBatchProcess((DoTaskReqForBatchProcess)_RequestObject);
            DoTaskRespForBatchProcess objDoTaskResp = objDoTask.Process(true);
            return objDoTaskResp.GetResponseJson();
        }
        public static string DoCompanyDetail(object _RequestObject)
        {
            GetCompanyDetails objCompanyDtls = new GetCompanyDetails((GetCompanyDetailsReq)_RequestObject);
            GetCompanyDetailsResp objCompanyDtlsResp = objCompanyDtls.Process();
            return objCompanyDtlsResp.GetResponseJson();
        }
        public static string DoUserAuthentication(object _RequestObject)
        {
            UserAuthentication objUserAuthentication = new UserAuthentication((UserAuthenticationReq)_RequestObject);
            UserAuthenticationResp objUserAuthenticationResp = objUserAuthentication.Process();
            return objUserAuthenticationResp.GetResponseJson();
        }
        public static string DoUserContacts(object _RequestObject, System.Web.Caching.Cache cache)
        {
            UserContacts objUserContacts = new UserContacts((UserContactsReq)_RequestObject, cache);
            UserContactsResp objUserContactsResp = objUserContacts.Process();
            return objUserContactsResp.GetResponseJson();
        }
        public static string DoAddNewContact(object _RequestObject)
        {
            AddNewContact objAddNewContact = new AddNewContact((AddNewContactReq)_RequestObject);
            AddNewContactResp objAddNewContactResp = objAddNewContact.Process();
            return objAddNewContactResp.GetResponseJson();
        }
        public static string DoRemoveContact(object _RequestObject, System.Web.Caching.Cache cache)
        {
            RemoveContact objRemoveContact = new RemoveContact((RemoveContactReq)_RequestObject, cache);
            RemoveContactResp objRemoveContactResp = objRemoveContact.Process();
            return objRemoveContactResp.GetResponseJson();
        }
        public static string DoRemovePendingRequest(object _RequestObject)
        {
            RemovePendingRequest objRemovePendingRequest = new RemovePendingRequest((RemovePendingRequestReq)_RequestObject);
            RemovePendingRequestResp objRemovePendingRequestResp = objRemovePendingRequest.Process();
            return objRemovePendingRequestResp.GetResponseJson();
        }
        public static string DomBuzzCompanyUserList(object _RequestObject)
        {
            mBuzzCompanyUserList objmBuzzCompanyUserList = new mBuzzCompanyUserList((mBuzzCompanyUserListReq)_RequestObject);
            mBuzzCompanyUserListResp objmBuzzCompanyUserListResp = objmBuzzCompanyUserList.Process();
            return objmBuzzCompanyUserListResp.GetResponseJson();
        }
        public static string DoConfirmMobUpdation(object _RequestObject)
        {
            ConfirmMobUpdation objConfirmMobUpdation = new ConfirmMobUpdation((ConfirmMobUpdationReq)_RequestObject);
            ConfirmMobUpdationResp objConfirmMobUpdationResp = objConfirmMobUpdation.Process();
            return objConfirmMobUpdationResp.GetResponseJson();
        }
        public static string DoResponseofContactRequest(object _RequestObject, System.Web.Caching.Cache cache)
        {
            ResponseofContactRequest objResponseofContactRequest = new ResponseofContactRequest((ResponseofContactRequestReq)_RequestObject, cache);
            ResponseofContactRequestResp objResponseofContactRequestResp = objResponseofContactRequest.Process();
            return objResponseofContactRequestResp.GetResponseJson();
        }
        public static string DoPushMessageforContactStatus(object _RequestObject)
        {
            MbuzzPushMessageSave objPushMessageforContactStatus = new MbuzzPushMessageSave((MbuzzPushMessageSaveReq)_RequestObject);
            MbuzzPushMessageSaveResp objPushMessageforContactStatusResp = objPushMessageforContactStatus.Process();
            return objPushMessageforContactStatusResp.GetResponseJson();
        }
        public static string DoGetMenuAndWorkFlow(object _RequestObject)
        {
            Get_Menu_And_Workflows objMenuAndWorkFlow = new Get_Menu_And_Workflows((Get_Menu_And_Workflows_Req)_RequestObject);
            Get_Menu_And_Workflows_Resp objMenuAndWorkFlowResp = objMenuAndWorkFlow.Process();
            return objMenuAndWorkFlowResp.GetResponseJson();
        }
        public static string DoCheckAccountAndDevice(object _RequestObject)
        {
            CheckAccountAndDevice objCheckAccountAndDevice = new CheckAccountAndDevice((CheckAccountAndDeviceReq)_RequestObject);
            CheckAccountAndDeviceResp objCheckAccountAndDeviceResp = objCheckAccountAndDevice.Process();
            return objCheckAccountAndDeviceResp.GetResponseJson();
        }
        public static string DoGetQueryType(object _RequestObject)
        {
            GetQueryType objGetQueryType = new GetQueryType((GetQueryTypeReq)_RequestObject);
            GetQueryTypeResp objGetQueryTypeResp = objGetQueryType.Process();
            return objGetQueryTypeResp.GetResponseJson();
        }
        public static string DoUserQueryList(object _RequestObject)
        {
            UserQueryList objUserQueryList = new UserQueryList((UserQueryListReq)_RequestObject);
            UserQueryListResp objUserQueryListResp = objUserQueryList.Process();
            return objUserQueryListResp.GetResponseJson();
        }
        public static string DoSupportQuery(object _RequestObject, HttpContext context)
        {
            SupportQuery objSupportQuery = new SupportQuery((SupportQueryReq)_RequestObject);
            SupportQueryResp objSupportQueryResp = objSupportQuery.Process();
            return objSupportQueryResp.GetResponseJson();
        }
        public static string DoQueryDetailWithComments(object _RequestObject)
        {
            QueryDetailWithComments objQueryDetailWithComments = new QueryDetailWithComments((QueryDetailWithCommentsReq)_RequestObject);
            QueryDetailWithCommentsResp objQueryDetailWithCommentsResp = objQueryDetailWithComments.Process();
            return objQueryDetailWithCommentsResp.GetResponseJson();
        }
        public static string DoCommentOnQueryAndChangeStatus(object _RequestObject)
        {
            CommentOnQueryAndChangeStatus objCommentOnQueryAndChangeStatus = new CommentOnQueryAndChangeStatus((CommentOnQueryAndChangeStatusReq)_RequestObject);
            CommentOnQueryAndChangeStatusResp objCommentOnQueryAndChangeStatusResp = objCommentOnQueryAndChangeStatus.Process();
            return objCommentOnQueryAndChangeStatusResp.GetResponseJson();
        }
        public static string DoMPlugInClientAuthentication(object _RequestObject)
        {
            AuthenticateMPluginClient objAuthenticateMPlugInClient = new AuthenticateMPluginClient((AuthenticateMPluginClientReq)_RequestObject);
            AuthenticateMPluginClientResp objAuthenticateMPlugInClientResp = objAuthenticateMPlugInClient.Process();
            return objAuthenticateMPlugInClientResp.GetResponseJson();
        }
        public static string DoGetMPlugInConnectionDetails(object _RequestObject)
        {
            GetMPlugInConnectionDetails objGetMPlugInConnDetail = new GetMPlugInConnectionDetails((GetMPlugInConnectionDetailsReq)_RequestObject);
            GetMPlugInConnectionDetailsResp objGetMPlugInConnDetailResp = objGetMPlugInConnDetail.Process();
            return objGetMPlugInConnDetailResp.GetResponseJson();
        }
        public static string DoWFUsageLog(object _RequestObject)
        {
            WorkflowUsageLog objWFUsageLog = new WorkflowUsageLog((WorkflowUsageLogReq)_RequestObject);
            WorkflowUsageLogResp objWorkflowUsageLogResp = objWFUsageLog.Process();
            return objWorkflowUsageLogResp.GetResponseJson();
        }
        public static string DoAddDevPushMsgId(object _RequestObject)
        {
            AddDevicePushMsgID objAddPushMsgId = new AddDevicePushMsgID((AddDevicePushMsgIDreq)_RequestObject);
            AddDevicePushMsgIDResp objAddDevicePushMsgIDResp = objAddPushMsgId.Process();
            return objAddDevicePushMsgIDResp.GetResponseJson();
        }
        public static string DogetmGramMessages(object _RequestObject)
        {
            getmGramMessages objgetmGramMessages = new getmGramMessages((getmGramMessagesReq)_RequestObject);
            getmGramMessagesResp objgetmGramMessagesResp = objgetmGramMessages.Process();
            return objgetmGramMessagesResp.GetResponseJson();
        }
        public static string DogetmGramCompleteMessage(object _RequestObject)
        {
            getmGramCompleteMessages objgetmGramMessage = new getmGramCompleteMessages((getmGramCompleteMessagesReq)_RequestObject);
            getmGramCompleteMessagesResp objgetmGramMessageResp = objgetmGramMessage.Process();
            return objgetmGramMessageResp.GetResponseJson();
        }
        public static string DomGramMessageStatusUpdate(object _RequestObject)
        {
            mGramMessageStatusUpdate objmGramMessageStatusUpdate = new mGramMessageStatusUpdate((mGramMessageStatusUpdateReq)_RequestObject);
            mGramMessageStatusUpdateResp objmGramMessageStatusUpdateResp = objmGramMessageStatusUpdate.Process();
            return objmGramMessageStatusUpdateResp.GetResponseJson();
        }
        public static string DomGramEnterpriseAndMPAgentDetail(object _RequestObject)
        {
            mGramGetEnterpriseAndMPAgentDetail objmGramGetEnterpriseAndMPAgentDetail = new mGramGetEnterpriseAndMPAgentDetail((mGramGetEnterpriseAndMPAgentDetailReq)_RequestObject);
            mGramGetEnterpriseAndMPAgentDetailResp objmGramGetEnterpriseAndMPAgentDetailResp = objmGramGetEnterpriseAndMPAgentDetail.Process();
            return objmGramGetEnterpriseAndMPAgentDetailResp.GetResponseJson();
        }
        public static string DoGetPagingData(object _RequestObject)
        {
            GetPagingData objGetPagingData = new GetPagingData((GetPagingDataReq)_RequestObject);
            GetPagingDataResp objGetPagingDataResp = objGetPagingData.Process();
            return objGetPagingDataResp.GetResponseJson();
        }
        public static string DoGetHtmlFilesForAppAsSingleHTML(object _RequestObject)
        {
            HtmlFilesForAppAsHTML objGetHtmlFilesForApp =
                new HtmlFilesForAppAsHTML((GetHtmlFilesOfAppAsHTMLReq)_RequestObject);
            return objGetHtmlFilesForApp.Process();
        }
        public static string DoSavePushMsgFromUser(object _RequestObject)
        {
            SavePushMsgFromUser objSavePushMsgFromUser =
                new SavePushMsgFromUser((SavePushMsgFromUserReq)_RequestObject);
            SavePushMsgFromUserResp objSavePushMsgFromUserResp = objSavePushMsgFromUser.Process();
            return objSavePushMsgFromUserResp.GetResponseJson();
        }
        public static string DoDownloadDBOnlineForOfflineTable(object _RequestObject)
        {
            DownloadOnlineDatabaseOfflineTable objSavePushMsgFromUser =
                new DownloadOnlineDatabaseOfflineTable((DownloadDBOnlineForOfflineTableReq)_RequestObject);
            DownloadDBOnlineForOfflineTableResp objSavePushMsgFromUserResp = objSavePushMsgFromUser.Process();
            return objSavePushMsgFromUserResp.GetResponseJson();
        }
        public static string DoUploadOfflineTable(object _RequestObject)
        {
            UploadOfflineDatabaseOnlineDatabase objSavePushMsgFromUser =
                new UploadOfflineDatabaseOnlineDatabase((UploadtoOnlineDatabaseReq)_RequestObject);
            CommonResponse objSavePushMsgFromUserResp = objSavePushMsgFromUser.Process();
            return getFinalJson(SerializeJson<CommonResponse>(objSavePushMsgFromUserResp));
        }
        public static string DoConvertLocalToPublicImageURL(object _RequestObject)
        {
            ConvertLocalToPublicImageURL objConvertLocalToPublicImageURL =
                new ConvertLocalToPublicImageURL((ConvertLocalToPublicImageURLReq)_RequestObject);
            ConvertLocalToPublicImageURLResp objConvertLocalToPublicImageURLResp = objConvertLocalToPublicImageURL.Process();
            return objConvertLocalToPublicImageURLResp.GetResponseJson();
        }


        public static string GetUserDetail(object _RequestObject)
        {
            GetUserDetails objGetUserDetails =  new GetUserDetails((GetUserDetailsReq)_RequestObject);
            GetUserDetailResp objGetUserDetailsResp = objGetUserDetails.Process();
            return objGetUserDetailsResp.GetResponseJson();
        }
        public static string GetWebserviceUserList(object _RequestObject)
        {

            GetWebserviceUserList objGetUserDetails =
                 new GetWebserviceUserList((GetWebservice_User_ListReq)_RequestObject);
            GetWebservice_User_ListResp objGetUserDetailsResp = objGetUserDetails.Process();
            return objGetUserDetailsResp.GetResponseJson();
        }
        public static string UserGrouplist(object _RequestObject)
        {

            UserGrouplist objGetUserDetails =
                 new UserGrouplist((User_Group_ListReq)_RequestObject);
            User_Group_ListResp objGetUserDetailsResp = objGetUserDetails.Process();
            return objGetUserDetailsResp.GetResponseJson();
        }

        public static string Block_Unblock(object _RequestObject)
        {
            Block_Unblock objBlockUnblock =
                 new Block_Unblock((Block_UnblockReq)_RequestObject);
            Block_UnblockResp objGetUserDetailsResp = objBlockUnblock.Process();
            return objGetUserDetailsResp.GetResponseJson();

        }
        public static string ModifyUserforGroup(object _RequestObject)
        {
            ModifyUserforGroup objModifyUserforGroup =
                 new ModifyUserforGroup((ModifyUserfromGroupReq)_RequestObject);
            ModifyUserforGroupResp objGetUserDetailsResp = objModifyUserforGroup.Process();
            return objGetUserDetailsResp.GetResponseJson();
        }

        public static string GetListofUserRegistration(object _RequestObject)
        {
            GetListofUserRegistration objGetListofUserRegistrationp =
                 new GetListofUserRegistration((GetListofUserRegistrationReq)_RequestObject);
            GetListofUserRegistrationResp objGetUserDetailsResp = objGetListofUserRegistrationp.Process();
            return objGetUserDetailsResp.GetResponseJson();
        }

        //public static string Approve_Deny_Delete(object _RequestObject)
        //{
        //    Approve_Deny_Delete objapprval =
        //         new Approve_Deny_Delete((Approve_Deny_DeleteReq)_RequestObject);
        //    Approve_Deny_DeleteResp objGetUserDetailsResp = objapprval.Process();
        //    return objGetUserDetailsResp.GetResponseJson();

        //}











        //public static string DoGetCountryList(object _RequestObject)
        //{
        //    GetCountryList objGetCountryList = new GetCountryList((GetCountryListReq)_RequestObject);
        //    GetCountryListResp objGetCountryListResp = objGetCountryList.Process();
        //    return objGetCountryListResp.GetResponseJson();
        //}
        //public static string DoCompanyRegistration(object _RequestObject,HttpContext context)
        //{
        //    CompanyRegistration objCmpRegistration = new CompanyRegistration((CompanyRegistrationReq)_RequestObject);
        //    CompanyRegistrationResp objCmpRegistrationResp = objCmpRegistration.Process(context);
        //    return objCmpRegistrationResp.GetResponseJson();
        //}
        //public static string DoGetTimezoneList(object _RequestObject)
        //{
        //    GetAllTimezones objGetAllTimezone = new GetAllTimezones((GetAllTimezonesReq)_RequestObject);
        //    GetAllTimezonesResp objGetAllTimezonesResp = objGetAllTimezone.Process();
        //    return objGetAllTimezonesResp.GetResponseJson();
        //}
        #endregion

        #region Data Encription

        public static string EncryptString(string s)
        {
            string strEnc = "";
            try
            {
                if (string.IsNullOrEmpty(s))
                {
                    return strEnc;
                }

                int key1 = (DateTime.UtcNow.Second % 10) + 2;
                int key2 = ((key1 + DateTime.UtcNow.Minute) % 10) + 4;
                string strThisAscii = "";

                for (int i = 0; i < s.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        strThisAscii = Hex(Asc(s.Substring(i, 1)) + key1 - key2);
                    }
                    else
                    {
                        strThisAscii = Hex(Asc(s.Substring(i, 1)) + key2 - key1);
                    }

                    do
                    {
                        strThisAscii = "0" + strThisAscii;

                    } while (strThisAscii.Length < 4);

                    strEnc = strThisAscii + strEnc;
                }

                strEnc = (Hex(key1 * 10) + strEnc + Hex(key2 * 10)).ToLower();

                strEnc = strEnc.Replace("a", "g").Replace("b", "i").Replace("c", "k").Replace("d", "m").Replace("e", "o").Replace("f", "q").Replace("1", "s").Replace("2", "u");
                strEnc = strEnc.Replace("3", "v").Replace("4", "t").Replace("5", "r").Replace("6", "p").Replace("7", "n").Replace("8", "l").Replace("9", "j").Replace("0", "h");
            }
            catch
            {
                strEnc = "";
            }
            return strEnc;
        }

        public static string DecryptString(string s)
        {
            string strDec = "";
            try
            {
                if (string.IsNullOrEmpty(s))
                {
                    return strDec;
                }

                s = s.Replace("h", "0").Replace("j", "9").Replace("l", "8").Replace("n", "7").Replace("p", "6").Replace("r", "5").Replace("t", "4").Replace("v", "3");
                s = s.Replace("u", "2").Replace("s", "1").Replace("q", "f").Replace("o", "e").Replace("m", "d").Replace("k", "c").Replace("i", "b").Replace("g", "a");

                int key1 = (int)HexToDec(s.Substring(0, 2)) / 10;
                int key2 = (int)HexToDec(s.Substring(s.Length - 2, 2)) / 10;

                s = s.Substring(2, s.Length - 4);

                string strThisChar = "";
                bool oddChar = true;

                for (int i = s.Length - 4; i >= 0; i -= 4)
                {
                    if (oddChar)
                    {
                        strThisChar = Chr(HexToDec(s.Substring(i, 4)) - key1 + key2);
                        oddChar = false;
                    }
                    else
                    {
                        strThisChar = Chr(HexToDec(s.Substring(i, 4)) - key2 + key1);
                        oddChar = true;
                    }

                    strDec += strThisChar;
                }

                return strDec;
            }
            catch
            {
            }
            return "";
        }

        public static string Hex(int n)
        {
            try
            {
                return n.ToString("X");
            }
            catch
            {
            }
            return "";
        }

        public static int HexToDec(string s)
        {
            int intResult;
            try
            {
                intResult = Convert.ToInt32(s, 16);
            }
            catch
            {
                intResult = 0;
            }
            return intResult;
        }

        public static string StringReverse(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }
            StringBuilder rs = new StringBuilder(s.Length);
            for (int i = s.Length - 1; i >= 0; i--)
            {
                rs.Append(s.Substring(i, 1));
            }
            return rs.ToString();
        }

        private static short Asc(string String)
        {
            return Encoding.Default.GetBytes(String)[0];
        }

        private static string Chr(int CharCode)
        {
            if (CharCode > 255)
            {
                return "";
            }
            return Encoding.Default.GetString(new[] { (byte)CharCode });
        }

        #endregion

        #region mBuzzServer Processes
        public static bool IsmBuzzServerIDExist(string serverID)
        {
            try
            {
                SqlConnection Conn = new SqlConnection(MficientConstants.strServerConection);
                SqlCommand cmd = new SqlCommand(@"SELECT * FROM ADMIN_TBL_MBUZZ_SERVER WHERE MBUZZ_SERVER_ID = @MBUZZ_SERVER_ID");
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@MBUZZ_SERVER_ID", serverID);
                DataTable dtmBuzzServerDtls = new DataTable();
                using (Conn)
                {
                    Conn.Open();
                    cmd.Connection = Conn;
                    dtmBuzzServerDtls = new DataTable();
                    SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(cmd);
                    objSqlDataAdapter.Fill(dtmBuzzServerDtls);
                    Conn.Close();
                    if (dtmBuzzServerDtls != null && dtmBuzzServerDtls.Rows.Count > 0) return true;
                }
            }
            catch
            {

            }
            return false;
        }
        internal static string GetUserID(string companyID, string username)
        {
            string strUserId = "";
            SqlCommand cmd = new SqlCommand(@"SELECT * FROM TBL_USER_DETAIL WHERE COMPANY_ID = @COMPANY_ID AND USER_NAME = @USER_NAME");
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", companyID);
            cmd.Parameters.AddWithValue("@USER_NAME", username);

            DataTable dt = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd).Tables[0];

            if (dt != null && dt.Rows.Count > 0)
                strUserId = dt.Rows[0]["USER_ID"].ToString();
            return strUserId;
        }
        internal static string GetUserID(string companyID, string username, out string Email)
        {
            string strUserId = "";
            Email = string.Empty;
            SqlCommand cmd = new SqlCommand(@"SELECT * FROM TBL_USER_DETAIL WHERE COMPANY_ID = @COMPANY_ID AND USER_NAME = @USER_NAME");
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@COMPANY_ID", companyID);
            cmd.Parameters.AddWithValue("@USER_NAME", username);

            DataTable dt = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd).Tables[0];

            if (dt != null && dt.Rows.Count > 0)
            {
                strUserId = dt.Rows[0]["USER_ID"].ToString();
                Email = dt.Rows[0]["EMAIL_ID"].ToString();
            }
            return strUserId;
        }
        #endregion

        public static string UrlEncode(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return HttpUtility.UrlEncode(s).Replace(@"'", @"%27");
        }

        internal static string getMPlugInServerURL(string companyId)
        {
            string strQuery = @"SELECT *  FROM ADMIN_TBL_MST_MP_SERVER AS MPServer
                                LEFT OUTER JOIN ADMIN_TBL_SERVER_MAPPING AS ServerMap
                                ON MPServer.MP_SERVER_ID = ServerMap.MPLUGIN_SERVER_ID
                                WHERE ServerMap.COMPANY_ID = @CompanyId;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", companyId);
            DataSet dsMPluginServerDetails = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (dsMPluginServerDetails != null && dsMPluginServerDetails.Tables[0] != null && dsMPluginServerDetails.Tables[0].Rows.Count > 0)
            {
                return Convert.ToString(dsMPluginServerDetails.Tables[0].Rows[0]["MP_HTTP_URL"]);
            }
            else
            {
                return String.Empty;
            }
        }
        #region For WS CALL LOG
        //        public static void insertWSCallLog(string reqJson,string serverIp)
        //        {
        //            string strQuery = @"INSERT INTO TBL_WS_CALL_LOG
        //                                (
        //                                WS_REQUEST_DATETIME, REQ_JSON,SERVER_IP
        //                                )
        //                                VALUES
        //                                (
        //                                @WS_REQUEST_DATETIME,@REQ_JSON,@SERVER_IP
        //                                )";
        //            SqlCommand cmd = new SqlCommand(strQuery);
        //            cmd.CommandType = CommandType.Text;
        //            cmd.Parameters.AddWithValue("@WS_REQUEST_DATETIME", DateTime.UtcNow.Ticks);
        //            cmd.Parameters.AddWithValue("@REQ_JSON", reqJson);
        //            cmd.Parameters.AddWithValue("@SERVER_IP", serverIp);

        //            MSSqlClient.ExecuteNonQueryRecord(cmd);
        //        }
        #endregion

        internal static int saveSubAdminActivityLog(string userRole, string subAdminId, string userIdPara1, string companyId, SUBADMIN_ACTIVITY_LOG processType, long dateTimeTicks
            , string deviceIdPara2, string deviceTypePara3, string groupIdPara4, string appIdPara5, SqlConnection con, SqlTransaction transaction)
        {
            //checkCreateSingleton();

            SqlCommand cmd = new SqlCommand(@"INSERT INTO TBL_ACTIVITY_LOG(USER_ROLE,USER_ID,COMPANY_ID,PROCESS_TYPE,PROCESS_ID,ACTIVITY_DATETIME,PARA1,PARA2,PARA3,PARA4,PARA5)
                            VALUES(@USER_ROLE,@USER_ID,@COMPANY_ID,@PROCCESS_TYPE,@PROCCESS_ID,@ACTIVITY_DATETIME,@PARA1,@PARA2,@PARA3,@PARA4,@PARA5)", con, transaction);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@USER_ROLE", userRole);
            cmd.Parameters.AddWithValue("@USER_ID", subAdminId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
            cmd.Parameters.AddWithValue("@PROCCESS_TYPE", (int)processType);
            cmd.Parameters.AddWithValue("@PROCCESS_ID", Utilities.GetMd5Hash(userIdPara1 + companyId + DateTime.UtcNow.Ticks.ToString()));
            cmd.Parameters.AddWithValue("@PARA1", userIdPara1);
            cmd.Parameters.AddWithValue("@PARA2", deviceIdPara2);
            cmd.Parameters.AddWithValue("@PARA3", deviceTypePara3);
            cmd.Parameters.AddWithValue("@PARA4", groupIdPara4);
            cmd.Parameters.AddWithValue("@PARA5", appIdPara5);
            cmd.Parameters.AddWithValue("@ACTIVITY_DATETIME", dateTimeTicks);
            return cmd.ExecuteNonQuery();

        }

        #region S3 Bucket

        public static DataSet GetS3BucketDetails(BucketType bucketType)
        {
            string strQuery = @"SELECT * FROM TBL_BUCKET_MASTER WHERE ID = @ID";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ID", (Int16)bucketType);
            try
            {
                return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region
        public static string getCompanyLocalFormattedDate(TimeZoneInfo tzi,
           long ticksInUTC)
        {
            DateTime dtInLocalTimezone = TimeZoneInfo.ConvertTimeFromUtc(
                 new DateTime(ticksInUTC, DateTimeKind.Utc),
                 tzi
                 );
            return getFormattedDateForUI(dtInLocalTimezone);
        }
        #endregion
        public static string Base64Decode(string base64EncodedData)
        {
            if (!string.IsNullOrEmpty(base64EncodedData))
            {
                var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
                return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
            return "";
        }
        public static int processHttpStatusCode(int _StatusCode)
        {
            if (((int)HttpStatusCode.NotFound == _StatusCode) || ((int)HttpStatusCode.GatewayTimeout == _StatusCode) || ((int)HttpStatusCode.RequestTimeout == _StatusCode))
                return (int)DO_TASK_ERROR.WEBSERVICE_NOT_REACHABLE;
            else if ((int)HttpStatusCode.Unauthorized == _StatusCode)
                return (int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR;
            else
                return (int)DO_TASK_ERROR.WEBSERVICE_ERROR;
        }


    }
}
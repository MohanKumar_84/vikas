﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class SupportQueryReq
    {
        string _requestId, _userID, _companyId, _query, _SubjectQuery,
            _username;
        int _functionCode, _Querytype;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string UserID
        {
            get { return _userID; }
        }
        public string SubjectQuery
        {
            get { return _SubjectQuery; }
        }
        public int Querytype
        {
            get { return _Querytype; }
        }
        public string Query
        {
            get { return _query; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string Username
        {
            get { return _username; }
        }
        public SupportQueryReq(string requestJson,string userId)
        {
            RequestJsonParsing<SupportQueryReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<SupportQueryReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.SUPPORT_QUERY)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _userID = userId;
            _query = objRequestJsonParsing.req.data.qury;
            _SubjectQuery = objRequestJsonParsing.req.data.sqry;
            _SubjectQuery = objRequestJsonParsing.req.data.sqry;
            _companyId = objRequestJsonParsing.req.data.enid;
            _Querytype =Convert.ToInt32( objRequestJsonParsing.req.data.qtyp);
            _username = objRequestJsonParsing.req.data.unm;
        }
    }

    [DataContract]
    public class SupportQueryReqData : Data
    {
        public SupportQueryReqData()
        { }
        /// <summary>
        /// query
        /// </summary>
        [DataMember]
        public string qury { get; set; }

        /// <summary>
        /// subject
        /// </summary>
        [DataMember]
        public string sqry { get; set; }

        /// <summary>
        /// type
        /// </summary>
        [DataMember]
        public string qtyp { get; set; }
    }
}
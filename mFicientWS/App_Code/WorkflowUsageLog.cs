﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Net;
namespace mFicientWS
{
    public class WorkflowUsageLog
    {
        WorkflowUsageLogReq _wfUsageLogReq;
        
        public WorkflowUsageLog(WorkflowUsageLogReq wfUsageLogReq)
        {
            _wfUsageLogReq = wfUsageLogReq;
        }
        public WorkflowUsageLogResp Process()
        {
            string strCacheKey = CacheManager.GetKey(
                CacheManager.CacheType.CompanyTimezone,
                _wfUsageLogReq.CompanyId,
                String.Empty, String.Empty,
                String.Empty, String.Empty,
                String.Empty, String.Empty);
            
            ResponseStatus objResponseStatus = new ResponseStatus();
            objResponseStatus.cd = "0";
            objResponseStatus.desc = String.Empty;

            string strTimezoneID = CacheManager.Get<string>(strCacheKey,false);
            TimeZoneInfo tzi;
            if (String.IsNullOrEmpty(strTimezoneID))
            {
                tzi = CompanyTimezone.getTimezoneInfo(_wfUsageLogReq.CompanyId);
            }
            else
            {
                tzi = CompanyTimezone.getTimezoneInfoById(strTimezoneID);
            }
            if(tzi == null)throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            try
            {
                if (_wfUsageLogReq.AppDetails.Count > 0)
                {

                    foreach (WFUsageLogAppDetails appDtl in _wfUsageLogReq.AppDetails)
                    {
                        saveWfUsageLog(appDtl.appid,
                            Convert.ToByte(appDtl.ntyp),
                            Convert.ToInt64(appDtl.udt),
                            tzi
                            );
                    }
                }
                
            }
            catch
            { 
                //we have to return OK Status in any case
                //whether the insert command worked properly or not
            }
            return new WorkflowUsageLogResp(
                objResponseStatus,
                _wfUsageLogReq.RequestId);
            #region Previous Code
            //string strCmpTimeOffset = CacheManager.Get<string>(strCacheKey);
            //if (String.IsNullOrEmpty(strCmpTimeOffset))
            //{
            //    strCmpTimeOffset =Convert.ToString( CompanyTimezone.getCompanyTimeZoneOffset(_wfUsageLogReq.CompanyId));
            //}
            //if(String.IsNullOrEmpty(strCmpTimeOffset))
            //{
            //    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            //}
            //if (_wfUsageLogReq.AppDetails.Count > 0)
            //{
                
            //    foreach (WFUsageLogAppDetails appDtl in _wfUsageLogReq.AppDetails)
            //    {
            //        saveWfUsageLog(appDtl.appid,Convert.ToByte(appDtl.ntyp),Convert.ToInt64(appDtl.udt),Convert.ToInt32(strCmpTimeOffset));
            //    }
            //}
            //return ((int)HttpStatusCode.OK).ToString();
            #endregion
        }

        void saveWfUsageLog(
            string appId,
            byte networkType,
            long unixTime,
            TimeZoneInfo tzi)
        {
            
            string strQuery = @"INSERT INTO [TBL_WORKFLOW_USAGE_LOG]
                               ([COMPANY_ID],[WF_ID],[USER_ID]
                               ,[DEVICE_ID],[DEVICE_TYPE],[PROCESS_DATETIME]
                               ,[NETWORK_TYPE])
                         VALUES
                               (@COMPANY_ID,@WF_ID,@USER_ID
                               ,@DEVICE_ID,@DEVICE_TYPE,@PROCESS_DATETIME
                               ,@NETWORK_TYPE)";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _wfUsageLogReq.CompanyId);
            cmd.Parameters.AddWithValue("@WF_ID", appId);
            cmd.Parameters.AddWithValue("@USER_ID", _wfUsageLogReq.UserID);
            cmd.Parameters.AddWithValue("@DEVICE_ID", _wfUsageLogReq.DeviceId);
            cmd.Parameters.AddWithValue("@DEVICE_TYPE", _wfUsageLogReq.DeviceType);
            
            cmd.Parameters.AddWithValue("@PROCESS_DATETIME",
                CompanyTimezone.getCompanyLocalDateTicks(unixTime, tzi));

            cmd.Parameters.AddWithValue("@NETWORK_TYPE", networkType);
            MSSqlDatabaseClient.ExecuteNonQueryRecord(cmd);
        }
    }
}
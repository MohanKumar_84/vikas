﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class SavePushMsgFromUserReq
    {

        string _userId, _requestId, _deviceId, _deviceType, _companyId,_fromUsername,_message,_sendToUsername,_sendToGrpName;
        int _functionCode;

        public string FromUsername
        {
            get { return _fromUsername; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }

        public string UserId
        {
            get { return _userId; }
        }

        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string Message
        {
            get { return _message; }
        }
        public string SendToUsername
        {
            get { return _sendToUsername; }
        }
        public string SendToGroupName
        {
            get { return _sendToGrpName; }
        }
        public SavePushMsgFromUserReq(string requestJson,string userId)
        {
            RequestJsonParsing<SavePushMsgFromUserReqData> objRequestJsonParsing = 
                    Utilities.DeserialiseJson<RequestJsonParsing<SavePushMsgFromUserReqData>>(requestJson);
            
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);

            if (_functionCode != (int)FUNCTION_CODES.SAVE_PUSH_MSG_FROM_USER)
            {
                throw new Exception("Invalid Function Code");
            }
            //_sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _fromUsername = objRequestJsonParsing.req.data.unm;
            _message = objRequestJsonParsing.req.data.msg;
            _sendToUsername = objRequestJsonParsing.req.data.rusr;
            _sendToGrpName = objRequestJsonParsing.req.data.rgrp;
            _userId = userId;            
        }
    }

    [DataContract]
    public class SavePushMsgFromUserReqData : Data
    {
        public SavePushMsgFromUserReqData()
        { }
        /// <summary>
        /// message
        /// </summary>
        [DataMember]
        public string msg { get; set; }

        /// <summary>
        /// sendTo username
        /// </summary>
        [DataMember]
        public string rusr { get; set; }

        /// <summary>
        /// send to group name
        /// </summary>
        [DataMember]
        public string rgrp { get; set; }
    }
}
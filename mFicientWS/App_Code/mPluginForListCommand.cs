﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Web.Caching;

namespace mFicientWS
{
    internal class mPluginForListCommand
    {
        #region Private Members

        private static volatile mPluginForListCommand obj;
        // private HttpRequestJson<RequestData> httpRequest;
        private HttpRequestJson<RequestCmdsData> httpCmdRequest;
        // private HttpResponseJson<ResponseCommandsData> objResponse;
        private HttpResponseJson<List<ResponseCommandsData>> objCmdResponse;

        private string response;

        #endregion

        #region Enumerations

        public enum HttpRequestType
        {
            DATABASE_QUERY = 1,
            DBTEST_CONNECTION = 2,
            WEBSERVICE_QUERY = 3,
            REFRESH_CONNECTORS = 4,
            CHANGE_PASSWORD = 5,
            WSDL_WEBSERVICE_QUERY = 6,
            LIST_OF_COMMANDS = 7,
            LIST_OF_COMMANDS_WITHOUT_TRAN = 20,
        }

        public enum QueryType
        {
            SELECT = 1,
            INSERT = 2,
            UPDATE = 3,
            GET = 4,
            POST = 5,
            WSDL = 6,
            SOAP = 7,
            GET_METADATA = 8
        }

        public enum MetadataType
        {
            ALL = 0,
            TABLES = 1,
            COLUMNS = 2
        }

        public enum Status
        {
            FALSE = 0,
            TRUE = 1
        }

        #endregion

        #region Constructor

        private mPluginForListCommand()
        {
            //httpRequest = new HttpRequestJson<RequestData>();
            httpCmdRequest = new HttpRequestJson<RequestCmdsData>();
            //objResponse = new HttpResponseJson<ResponseCommandsData>();
            objCmdResponse = new HttpResponseJson<List<ResponseCommandsData>>();
            response = string.Empty;
        }

        #endregion

        #region Public Methods

        public static List<ResponseCommandsData> RunCommandsAndGetResponse(string enterpriseId, string agentName, string agentPassword, List<CommandsList> objCmd, bool IsBatch,bool isTransaction)
        {
            checkCreateSingleton();
            obj.response = string.Empty;
            obj.objCmdResponse = new HttpResponseJson<List<ResponseCommandsData>>();

            obj.httpCmdRequest = new HttpRequestJson<RequestCmdsData>();
            obj.httpCmdRequest.req.rid = obj.getRequestID();
            obj.httpCmdRequest.req.eid = enterpriseId;
            obj.httpCmdRequest.req.mpnm = agentName;
            obj.httpCmdRequest.req.mpwd = agentPassword;
            obj.httpCmdRequest.req.rtyp = ((int)HttpRequestType.LIST_OF_COMMANDS).ToString();

            RequestCmdsData objData = new RequestCmdsData();
            objData.IsBatch = IsBatch ? "1" : "0";
            if (isTransaction)
                objData.rtyp = ((int)HttpRequestType.LIST_OF_COMMANDS).ToString();
            else
                objData.rtyp = ((int)HttpRequestType.LIST_OF_COMMANDS_WITHOUT_TRAN).ToString();
            objData.cmds = objCmd;
            obj.httpCmdRequest.req.data = objData;

            string requestJson = obj.serializeJson<HttpRequestJson<RequestCmdsData>>(obj.httpCmdRequest);

            HttpResponseStatus responseStatus = obj.callmPlugin(enterpriseId, requestJson, out obj.response);
            if (responseStatus.StatusCode == HttpStatusCode.OK)
            {
                obj.objCmdResponse = Utilities.DeserialiseJson<HttpResponseJson<List<ResponseCommandsData>>>(obj.response.Trim());
                if ((Status)(Convert.ToInt32(obj.objCmdResponse.resp.data.error)) == Status.FALSE)
                {
                    if(obj.objCmdResponse.resp.cmdata!=null)
                    foreach (ResponseCommandsData cmdResp in obj.objCmdResponse.resp.cmdata)
                    {
                        foreach (CommandsList cmdReq in objCmd)
                        {
                            if (cmdReq.idx == cmdResp.idx)
                            {
                                if ((Status)(Convert.ToInt32(cmdResp.error)) == Status.FALSE)
                                {
                                    if (cmdResp.records != null && cmdResp.records.Length > 0)
                                    {
                                        if (Convert.ToInt32(cmdReq.querytype) == (int)QueryType.SELECT)
                                        {
                                            byte[] byteArray = Encoding.UTF8.GetBytes(cmdResp.records);
                                            MemoryStream stream = new MemoryStream(byteArray);
                                            DataSet ds = new DataSet();
                                            ds.ReadXml(stream);
                                            cmdResp.recordDataSet = ds;
                                        }
                                    }
                                }
                                else if ((Status)(Convert.ToInt32(cmdResp.error)) == Status.TRUE)
                                {
                                    if ((Status)(Convert.ToInt32(cmdResp.inerror)) == Status.TRUE)
                                        throw new mPlugin.mPluginException(cmdResp.errormsg, new Exception(cmdResp.inerrormsg));
                                    else if ((Status)(Convert.ToInt32(cmdResp.inerror)) == Status.FALSE)
                                        throw new mPlugin.mPluginException(cmdResp.errormsg);
                                }
                                break;
                            }
                        }
                    }

                }
                else 
                {
                    if ((Status)(Convert.ToInt32(obj.objCmdResponse.resp.data.inerror)) == Status.TRUE)
                        throw new mPlugin.mPluginException(obj.objCmdResponse.resp.data.errormsg, new Exception(obj.objCmdResponse.resp.data.inerrormsg));
                    else if ((Status)(Convert.ToInt32(obj.objCmdResponse.resp.data.inerror)) == Status.FALSE)
                        throw new mPlugin.mPluginException(obj.objCmdResponse.resp.data.errormsg);
                }

            }
            else
            {
                throw new HttpException((int)responseStatus.StatusCode, obj.response);
            }

            return obj.objCmdResponse.resp.cmdata;
        }
        #endregion

        #region Private Methods

        private static void checkCreateSingleton()
        {
            if (obj == null)
                obj = new mPluginForListCommand();
        }

        private string getRequestID()
        {
            Random random = new Random();
            string strRequestID = random.Next(99, 9999).ToString() + DateTime.UtcNow.Ticks.ToString().Substring(0, 16);
            return strRequestID;
        }

        private HttpResponseStatus callmPlugin(string enterpriseId, string requestJson, out string responseJson)
        {
            responseJson = "";
            HttpResponseStatus oResponse = new HttpResponseStatus(true, HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString(), string.Empty);
            string jsonData = UrlEncode(requestJson);
            try
            {
                string Url = getmPluginUrl(enterpriseId);
                HTTP oHttp = new HTTP("https://" + Url + "?d=" + jsonData);
                oHttp.HttpRequestMethod = WebRequestMethods.Http.Get;
                oHttp.UserAgent = @"mFicient-WebServices";
                oHttp.Timeout = 30000;
                oHttp.Retry = 3;
                oResponse = oHttp.Request();

                if (oResponse.StatusCode == HttpStatusCode.OK || oResponse.StatusCode == HttpStatusCode.Accepted)
                {
                    responseJson = oResponse.ResponseText;
                }
                else
                {
                    responseJson = oHttp.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                oResponse.StatusCode = HttpStatusCode.InternalServerError;
                responseJson = ex.Message;
            }
            return oResponse;
        }

        private string getmPluginUrl(string enterpriseId)
        {
            try
            {
                string strQuery = @"SELECT *  FROM ADMIN_TBL_MST_MP_SERVER AS MPServer
                                LEFT OUTER JOIN ADMIN_TBL_SERVER_MAPPING AS ServerMap
                                ON MPServer.MP_SERVER_ID = ServerMap.MPLUGIN_SERVER_ID
                                WHERE ServerMap.COMPANY_ID = @COMPANY_ID";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);
                DataSet ds = mFicientWS.MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0]["MP_HTTP_URL"].ToString();
                }
            }
            catch
            {

            }
            return string.Empty;
        }

        private string serializeJson<T>(object obj)
        {
            T objRequest = (T)obj;
            MemoryStream stream = new MemoryStream();
            StreamReader streamReader = null;
            try
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(objRequest.GetType());
                serializer.WriteObject(stream, objRequest);
                stream.Position = 0;
                streamReader = new StreamReader(stream);
            }
            catch
            {

            }
            return streamReader.ReadToEnd();
        }

        private T deserialiseJson<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            try
            {
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
                DataContractJsonSerializer serialiser = new DataContractJsonSerializer(obj.GetType());
                obj = (T)serialiser.ReadObject(ms);
                ms.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(@"Error in deserialising Json [ " + ex.Message + " ]");
            }
            return obj;
        }

        #endregion

        #region Http Utility

        public static string UrlEncode(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return HttpUtility.UrlEncode(s).Replace(@"'", @"%27");
        }

        #endregion

        #region Private Classes

        #region HTTP REQUEST CLASS

        [DataContract]
        private class HttpRequestJson<T>
        {
            public HttpRequestJson()
            {
                this.req = new HttpRequestData<T>();
            }

            [DataMember]
            public HttpRequestData<T> req { get; set; }
        }

        [DataContract]
        private class HttpRequestData<T>
        {
            [DataMember]
            public string rid { get; set; }

            [DataMember]
            public string eid { get; set; }

            [DataMember]
            public string mpwd { get; set; }

            [DataMember]
            public string mpnm { get; set; }

            [DataMember]
            public string rtyp { get; set; }

            [DataMember]
            public T data { get; set; }
        }

        [DataContract]
        private class RequestData
        {

            [DataMember]
            public string rtyp { get; set; }

            [DataMember]
            public string connid { get; set; }

            [DataMember]
            public string dbtyp { get; set; }

            [DataMember]
            public string con { get; set; }

            [DataMember]
            public string url { get; set; }

            [DataMember]
            public string actn { get; set; }

            [DataMember]
            public string uid { get; set; }

            [DataMember]
            public string pwd { get; set; }

            [DataMember]
            public string querytype { get; set; }

            [DataMember]
            public string sql { get; set; }

            [DataMember]
            public List<SqlParameter> param { get; set; }

            [DataMember]
            public string httpdata { get; set; }
        }

        [DataContract]
        private class RequestCmdsData
        {
            public RequestCmdsData()
            {
                this.cmds = new List<CommandsList>();
            }

            [DataMember]
            public string IsBatch { get; set; }

            [DataMember]
            public string rtyp { get; set; }

            [DataMember]
            public List<CommandsList> cmds { get; set; }
        }

        [DataContract]
        public class CommandsList
        {
            public CommandsList()
            {
                this.cmdlp = new List<CmdParamsList>();
            }

            [DataMember]
            public string idx { get; set; }

            [DataMember]
            public string rtyp { get; set; }

            [DataMember]
            public string connid { get; set; }

            [DataMember]
            public string dbtyp { get; set; }

            [DataMember]
            public string con { get; set; }

            [DataMember]
            public string url { get; set; }

            [DataMember]
            public string actn { get; set; }

            [DataMember]
            public string uid { get; set; }

            [DataMember]
            public string pwd { get; set; }

            [DataMember]
            public string querytype { get; set; }

            [DataMember]
            public string sql { get; set; }

            [DataMember]
            public List<CmdParamsList> cmdlp { get; set; }
            [DataMember]
            public string httpdata { get; set; }
        }
        [DataContract]
        public class CmdParamsList
        {
            [DataMember]
            public List<SqlParameter> param { get; set; }
        }
        #endregion

        #region HTTP RESPONSE CLASS

        [DataContract]
        private class HttpResponseJson<T>
        {
            public HttpResponseJson()
            {
                this.resp = new HttpResponseData<T>();
            }

            [DataMember]
            public HttpResponseData<T> resp { get; set; }
        }

        [DataContract]
        private class HttpResponseData<T>
        {
            [DataMember]
            public string rid { get; set; }

            [DataMember]
            public string eid { get; set; }

            [DataMember]
            public string mpnm { get; set; }

            [DataMember]
            public string rtyp { get; set; }

            [DataMember]
            public T cmdata { get; set; }

            [DataMember]
            public ResponseCommandsData data { get; set; }

        }

        //[DataContract]
        //private class ResponseData
        //{
        //    [DataMember]
        //    public List<ResponseCommandsData> cmds { get; set; }
        //}

        [DataContract]
        public class ResponseCommandsData
        {
            [DataMember]
            public string idx { get; set; }

            [DataMember]
            public string error { get; set; }

            [DataMember]
            public string errormsg { get; set; }

            [DataMember]
            public string inerror { get; set; }

            [DataMember]
            public string inerrormsg { get; set; }

            [DataMember]
            public string reccount { get; set; }

            [DataMember]
            public string records { get; set; }

            [DataMember]
            public DataSet recordDataSet { get; set; }

            [DataMember]
            public string status { get; set; }

            [DataMember]
            public string httpresponse { get; set; }

            [DataMember]
            public string result { get; set; }
        }

        #endregion

        #endregion

        [DataContract]
        public class SqlParameters : IEnumerable<SqlParameter>
        {
            private List<SqlParameter> parameters;

            public SqlParameters()
            {
                parameters = new List<SqlParameter>();
            }

            public void Add(string paramName, string paramValue)
            {
                if (string.IsNullOrEmpty(paramName))
                    throw new ArgumentNullException();
                parameters.Add(new SqlParameter(paramName, paramValue));
            }

            public int Count()
            {
                return parameters.Count;
            }

            [DataMember]
            public List<SqlParameter> Parameters
            {
                get
                {
                    return parameters;
                }
            }

            #region IEnumerable<SqlParameter> Members

            public IEnumerator<SqlParameter> GetEnumerator()
            {
                return this.parameters.GetEnumerator();
            }

            #endregion

            #region IEnumerable Members

            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }

            #endregion
        }

        [DataContract]
        public class SqlParameter
        {
            private string paramName, paramValue;

            public SqlParameter(string parameterName, string parameterValue)
            {
                paramName = parameterName;
                paramValue = parameterValue;
            }

            [DataMember]
            public string pname
            {
                get
                {
                    return paramName;
                }
                set
                {
                    paramName = value;
                }
            }

            [DataMember]
            public string pvalue
            {
                get
                {
                    return paramValue;
                }
                set
                {
                    paramValue = value;
                }
            }
        }
    }
}
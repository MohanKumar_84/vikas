﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientWS
{
    public class WSCGetUserDetail
    {
        string _userId, _companyId, _deviceId,
            _statusDescription;
        int _statusCode;
        //MFEMobileUser _mobUser;

        #region Public Properties
        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
            private set { _deviceId = value; }
        }
        public string UserId
        {
            get { return _userId; }
            private set { _userId = value; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }
        //public MFEMobileUser MobUser
        //{
        //    get { return _mobUser; }
        //    private set { _mobUser = value; }
        //}
        #endregion
        #region Public Properties
        public WSCGetUserDetail(string userId,
            string companyId, string deviceId)
        {
            this.UserId = userId;
            this.CompanyId = companyId;
            this.DeviceId = deviceId;
            this.StatusCode = 0;
            this.StatusDescription = String.Empty;
        }
        public MFEMobileUser getUserDetail()
        {
            MFEMobileUser objMobileUser =
                new MFEMobileUser();
            try
            {
                string strQuery = getSqlQueryForUserDetail();
                SqlCommand cmd = getCmdForUserDetail(strQuery);
                DataSet dsUserDetail = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (dsUserDetail == null) throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                if (dsUserDetail.Tables[0].Rows.Count > 0)
                {
                    DataTable dtblUserDtl = dsUserDetail.Tables[0];
                    DataRow rowUserDtl = dtblUserDtl.Rows[0];
                    objMobileUser = getMobileUserFromDataRow(rowUserDtl,null);
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return objMobileUser;
        }
       
        public string getUserContactsFromRequestedBy(string RequestedBy)
        {
            string strResult = string.Empty;
            try
            {
                string strQuery = @"select user_name from  tbl_user_detail a 
                                inner join tbl_User_group_link b 
                                on a.user_id=b.user_id where b.Group_id in 
                                ('" + RequestedBy.Replace(",", "','") + "')";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                DataSet objDataSet = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (objDataSet != null)
                {
                    foreach (DataRow dr in objDataSet.Tables[0].Rows)
                    {
                        if (!string.IsNullOrEmpty(strResult)) strResult += ",";
                        strResult += dr[0].ToString();
                    }
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return strResult;
        }
        public MFEMobileUser getUserDetail(string password)
        {
            MFEMobileUser objMobileUser =
                 new MFEMobileUser();
            try
            {
                string strQuery = getSqlQueryForUserDetailWithPwd(true);
                SqlCommand cmd = getCmdForUserDtlWithPwd(strQuery, password);
                DataSet dsUserDetail = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (dsUserDetail == null) throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                if (dsUserDetail.Tables[0].Rows.Count > 0)
                {
                    List<MobileDetail> objMobilesDetail = new List<MobileDetail>();
                    MobileDetail objMobileDetail = new MobileDetail();
                    foreach (DataRow dr in dsUserDetail.Tables[1].Rows)
                    {
                        objMobileDetail = new MobileDetail();
                        objMobileDetail.DeviceID = Convert.ToString(dr["DEVICE_ID"]);
                        objMobileDetail.DeviceType = Convert.ToString( dr["DEVICE_TYPE"]);
                        objMobileDetail.DevicePushNotificationId = Convert.ToString( dr["DEVICE_PUSH_MESSAGE_ID"]);
                        objMobilesDetail.Add(objMobileDetail);

                    }
                    //dsUserDetail.Tables[1]
                    objMobileUser = getMobileUserFromDataRow(dsUserDetail.Tables[0].Rows[0], objMobilesDetail);
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return objMobileUser;
        }
        public MFESubAdmin getSubAdminOfUser()
        {
            MFESubAdmin objSubAdminDtl =
                new MFESubAdmin();
            try
            {
                string strQuery = getSqlQueryForSubAdminOfUser();
                SqlCommand cmd = getCmdForSubAdminOfUser(strQuery);
                DataSet dsSubAdminDetail = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (dsSubAdminDetail == null) throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                if (dsSubAdminDetail.Tables[0].Rows.Count > 0)
                {
                    DataRow rowSubadminDtl = dsSubAdminDetail.Tables[0].Rows[0];
                    objSubAdminDtl = getSubAdminFromDataRow(rowSubadminDtl);
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return objSubAdminDtl;
        }
        #endregion
        #region Sql Query
        string getSqlQueryForUserDetail()
        {
            return @"SELECT * FROM TBL_USER_DETAIL
                        WHERE USER_ID = @USER_ID
                        AND COMPANY_ID = @COMPANY_ID;";
        }
       
        string getSqlQueryForUserDetailWithPwd(bool DeviceDetail)
        {
            string strQuery="";
            if (DeviceDetail)
            {
                strQuery = getSqlQueryForUserMobileDevice();
            }
            return @"SELECT * FROM TBL_USER_DETAIL
                    WHERE USER_ID = @USER_ID
                    AND ACCESS_CODE = @ACCESS_CODE
                    AND COMPANY_ID = @COMPANY_ID;" + strQuery;

            
        }
        string getSqlQueryForUserMobileDevice()
        {
            return @"SELECT * FROM TBL_REGISTERED_DEVICE
                    WHERE USER_ID = @USER_ID
                    AND COMPANY_ID = @COMPANY_ID;";
        }
        string getSqlQueryForSubAdminOfUser()
        {
            return @"SELECT sadmin.* FROM TBL_USER_DETAIL AS usr
                    INNER JOIN
                    TBL_SUB_ADMIN AS sadmin
                    ON usr.SUBADMIN_ID = sadmin.SUBADMIN_ID
                    WHERE usr.USER_ID = @USER_ID
                    AND usr.COMPANY_ID =@COMPANY_ID";
        }
        #endregion
        #region Sql Commands
        SqlCommand getCmdForUserDetail(string query)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            cmd.Parameters.AddWithValue("@USER_ID", this.UserId);
            return cmd;
        }
        SqlCommand getCmdForUserDesignation(string query)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@USER_ID", this.UserId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            return cmd;
        }
        SqlCommand getCmdForUserDtlWithPwd(string query, string password)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@USER_ID", this.UserId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            cmd.Parameters.AddWithValue("@ACCESS_CODE", password);
            return cmd;
        }
        SqlCommand getCmdForSubAdminOfUser(string query)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@USER_ID", this.UserId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            return cmd;
        }
        #endregion

        MFEMobileUser getMobileUserFromDataRow(DataRow rowUserDtl,List<MobileDetail> objmobsDetail)
        {
            if (rowUserDtl == null) throw new ArgumentNullException();
            MFEMobileUser objMobileUser = new MFEMobileUser();
            objMobileUser.UserId = Convert.ToString(rowUserDtl["USER_ID"]);
            objMobileUser.EmailId = Convert.ToString(rowUserDtl["EMAIL_ID"]);
            objMobileUser.Password = Convert.ToString(rowUserDtl["ACCESS_CODE"]);
            objMobileUser.FirstName = Convert.ToString(rowUserDtl["FIRST_NAME"]);
            objMobileUser.LastName = Convert.ToString(rowUserDtl["LAST_NAME"]);
            objMobileUser.Mobile = Convert.ToString(rowUserDtl["MOBILE"]);
            objMobileUser.CompanyId = Convert.ToString(rowUserDtl["COMPANY_ID"]);
            objMobileUser.Dob = Convert.ToInt64(rowUserDtl["DATE_OF_BIRTH"]);
            objMobileUser.RegistrationDatetime = Convert.ToInt64(rowUserDtl["REGISTRATION_DATETIME"]);
            objMobileUser.SubAdminId = Convert.ToString(rowUserDtl["SUBADMIN_ID"]);
            objMobileUser.LocationId = "";
            objMobileUser.DesignationId ="";
            objMobileUser.EmployeeNo = Convert.ToString(rowUserDtl["EMPLOYEE_NO"]);
            objMobileUser.Username = Convert.ToString(rowUserDtl["USER_NAME"]);
            objMobileUser.IsActive = Convert.ToBoolean(rowUserDtl["IS_ACTIVE"]);
            objMobileUser.RegionId = "";
            objMobileUser.IsBlocked = Convert.ToBoolean(rowUserDtl["IS_BLOCKED"]);
            objMobileUser.AllowMessenger = Convert.ToBoolean(rowUserDtl["ALLOW_MESSENGER"]);
            objMobileUser.IsOfflineWorkAllowed = Convert.ToBoolean(rowUserDtl["IS_OFFLINE_WORK"]);
            objMobileUser.UpdatedOn = Convert.ToInt64(rowUserDtl["UPDATE_ON"]);
            objMobileUser.DesktopMessenger = Convert.ToBoolean(rowUserDtl["DESKTOP_MESSENGER"]);
            objMobileUser.RequestedBy = Convert.ToString(rowUserDtl["REQUESTED_BY"]);
            objMobileUser.DomainId = Convert.ToString(rowUserDtl["DOMAIN_ID"]);
            objMobileUser.MobileDetails = objmobsDetail;
            return objMobileUser;
        }
        
        MFESubAdmin getSubAdminFromDataRow(DataRow rowSubAdminDtl)
        {
            if (rowSubAdminDtl == null) throw new ArgumentNullException();
            MFESubAdmin objSubAdmin = new MFESubAdmin();
            objSubAdmin.CompanyId = Convert.ToString(rowSubAdminDtl["COMPANY_ID"]);
            objSubAdmin.SubAdminId = Convert.ToString(rowSubAdminDtl["SUBADMIN_ID"]);
            objSubAdmin.AdminId = Convert.ToString(rowSubAdminDtl["ADMIN_ID"]);
            objSubAdmin.FullName = Convert.ToString(rowSubAdminDtl["FULL_NAME"]);
            objSubAdmin.UserName = Convert.ToString(rowSubAdminDtl["USER_NAME"]);
            objSubAdmin.Password = Convert.ToString(rowSubAdminDtl["ACCESS_CODE"]);
            objSubAdmin.Email = Convert.ToString(rowSubAdminDtl["EMAIL"]);
            objSubAdmin.IsActive = Convert.ToBoolean(rowSubAdminDtl["ISACTIVE"]);
            objSubAdmin.IsBlocked = Convert.ToBoolean(rowSubAdminDtl["IS_BLOCKED"]);
            objSubAdmin.PushMsgAllowed = Convert.ToBoolean(rowSubAdminDtl["PUSH_MESSAGE"]);
            return objSubAdmin;
        }
    }
}
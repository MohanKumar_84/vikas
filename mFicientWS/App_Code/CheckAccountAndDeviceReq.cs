﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class CheckAccountAndDeviceReq
    {
        string _requestId, _deviceId, _deviceType, _companyId, _UserId;
        int _functionCode;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string UserId
        {
            get { return _UserId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }

        public CheckAccountAndDeviceReq(string requestJson,string UserID)
        {
            RequestJsonParsing<CheckAccountAndDeviceReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<CheckAccountAndDeviceReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.COMPANY_DETAIL)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _UserId = UserID;
        }
    }
    [DataContract]
    public class CheckAccountAndDeviceReqData : Data
    {

    }
}
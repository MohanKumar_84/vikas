﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class CheckActiveDirectoryUserExistenceResp
    {
        string  _statusCode, _statusDescription;
        CheckActiveDirectoryUserExistenceField _userFields;

        public CheckActiveDirectoryUserExistenceResp(CheckActiveDirectoryUserExistenceField userFields, string statusCode, string statusDescription)
        {
            try
            {
                _userFields = userFields;
                _statusCode = statusCode;
                _statusDescription = statusDescription;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating MPluginCheckUserExistence Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            CheckActiveDirectoryUserExistenceRespFields objCheckUserRespFields = new CheckActiveDirectoryUserExistenceRespFields();
            objCheckUserRespFields.cd = _statusCode;
            objCheckUserRespFields.desc = _statusDescription;
            objCheckUserRespFields.data = _userFields;
            string strJsonWithDetails = Utilities.SerializeJson<CheckActiveDirectoryUserExistenceRespFields>(objCheckUserRespFields);
            return Utilities.getFinalJson(strJsonWithDetails);
        }

        public string StatusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }

        public string StatusCode
        {
            get { return _statusCode; }
            set { _statusCode = value; }
        }

        public CheckActiveDirectoryUserExistenceField Data
        {
            get { return _userFields; }
            set { _userFields = value; }
        }
    }

    [DataContract]
    public class CheckActiveDirectoryUserExistenceRespFields
    {
        [DataMember]
        public string cd { get; set; }

        [DataMember]
        public string desc { get; set; }

        [DataMember]
        public CheckActiveDirectoryUserExistenceField data { get; set; }
    }

    [DataContract]
    public class CheckActiveDirectoryUserExistenceField
    {
        [DataMember]
        public bool result { get; set; }

        [DataMember]
        public ActiveDirectoryUser user { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class UserContactsReq
    {
        string _requestId,  _companyId, _userName;
        int _functionCode;

        public string Username
        {
            get { return _userName; }
        }

        public string CompanyId
        {
            get { return _companyId; }
        }

        public string RequestId
        {
            get { return _requestId; }
        }

        public int FunctionCode
        {
            get { return _functionCode; }
        }

        public UserContactsReq(string requestJson)
        {
            RequestJsonParsing<UserContactsReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<UserContactsReqData>>(requestJson);
            _userName = objRequestJsonParsing.req.data.unm;
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.USER_CONTACTS)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.data.enid;
        }
    }


    [DataContract]
    public class UserContactsReqData : Data
    {
    }
}
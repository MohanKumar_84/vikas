﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net;
namespace mFicientWS
{
    public class MPlugInChangesNotificationToServer
    {

        MPlugInChangesNotificationToServerReq _mPlugInChangeNotificationReq;

        public MPlugInChangesNotificationToServer(MPlugInChangesNotificationToServerReq mPlugInChangeNotificationReq)
        {
            _mPlugInChangeNotificationReq = mPlugInChangeNotificationReq;
        }

        public void Process(MPlugInChangesRequestType requestType)
        {
            if (String.IsNullOrEmpty(_mPlugInChangeNotificationReq.RequestId) 
                 || String.IsNullOrEmpty(_mPlugInChangeNotificationReq.CompanyId) )
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
               
            }

            try
            {
                switch (requestType)
                {
                    case MPlugInChangesRequestType.REFRESH_CONNECTORS:
                        mPlugin.RefreshConnectors(_mPlugInChangeNotificationReq.CompanyId, _mPlugInChangeNotificationReq.AgentName, _mPlugInChangeNotificationReq.AgentPassword);
                        break;
                    case MPlugInChangesRequestType.CREDENTIALS_CHANGED:
                        mPlugin.CredentialsChanged(_mPlugInChangeNotificationReq.CompanyId, _mPlugInChangeNotificationReq.AgentName, _mPlugInChangeNotificationReq.AgentPassword);
                        break;
                }
            }
            catch (HttpException e)
            {
                throw (new Exception(Convert.ToString(e.GetHttpCode())));
            }
            catch
            {
                throw new Exception();
            }


            //DataSet dsMPlugInAuthenticationDtl = getMPlugInAuthenticationDetails();
            //if (dsMPlugInAuthenticationDtl != null)
            //{
            //    if (!(dsMPlugInAuthenticationDtl.Tables[0].Rows.Count > 0) || !(Utilities.GetMd5Hash(dsMPlugInAuthenticationDtl.Tables[0].Rows[0][0].ToString()) == _mPlugInChangeNotificationReq.MPlugInPassword))
            //    {
            //        throw new Exception(((int)HttpStatusCode.Unauthorized).ToString());
            //    }
            //}
            //else
            //{
            //    throw new Exception();
            //}

            //DataSet dsMPlugInServerDtls = Utilities.getMPlugInServerDetail(_mPlugInChangeNotificationReq.CompanyId);
            //if (dsMPlugInServerDtls != null)
            //{
            //    if (dsMPlugInServerDtls.Tables[0].Rows.Count > 0)
            //    {
            //        string strHttpUrl = Convert.ToString(dsMPlugInServerDtls.Tables[0].Rows[0]["MP_HTTP_URL"]);
            //        if (String.IsNullOrEmpty(strHttpUrl))
            //        {
            //            throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            //        }
            //        else
            //        {
            //            string strCompleteRequestURL = "https://" + strHttpUrl + "?d=" + Utilities.UrlEncode(createMPlugInRequest());
            //            string strResponse = "sucessfull";
            //            if (!Utilities.mPlugInCallSuccessfull(strCompleteRequestURL))
            //            {
            //                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            //            }
            //            else
            //            {
            //                return new MPlugInChangesNotificationToServerResp(strResponse);
            //            }
            //        }
            //       // mPlugin.RefreshConnectors(
            //    }
            //    else
            //    {
            //        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            //    }
            //}
            //else
            //{
            //    throw new Exception();
            //}
        }


        //DataSet getMPlugInAuthenticationDetails()
        //{
        //    string strQuery = @"SELECT MPLUGIN_PASSWORD FROM TBL_ACCOUNT_SETTINGS WHERE COMPANY_ID = @CompanyId";
        //    SqlCommand cmd = new SqlCommand(strQuery);
        //    cmd.Parameters.AddWithValue("@CompanyId", _mPlugInChangeNotificationReq.CompanyId);
        //    return MSSqlClient.SelectDataFromSQlCommand(cmd);
        //}

        //string createMPlugInRequest()
        //{
        //    string strRequestJson = "";
        //    MPlugInChangeNotificationRequest objMPlugInDBReq = new MPlugInChangeNotificationRequest();
        //    objMPlugInDBReq.eid = _mPlugInChangeNotificationReq.CompanyId;
        //    objMPlugInDBReq.mpwd = _mPlugInChangeNotificationReq.MPlugInPassword;
        //    objMPlugInDBReq.rid = _mPlugInChangeNotificationReq.RequestId;

        //    MPlugInChangeNotificationData objReqData = new MPlugInChangeNotificationData();
        //    objReqData.rtyp = _mPlugInChangeNotificationReq.RequestType;
        //    objMPlugInDBReq.data = objReqData;

        //    strRequestJson = Utilities.SerializeJson<MPlugInChangeNotificationRequest>(objMPlugInDBReq);

        //    return "{\"req\":" + strRequestJson + "}";
        //}
    }
}
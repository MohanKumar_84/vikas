﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class ResponseofContactRequestResp
    {
        ResponseStatus _respStatus;
        string _requestId;

        public ResponseofContactRequestResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in rResponse of Contact Request Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            ResponseofContactRequestResponse objResponseofContactRequestResp = new ResponseofContactRequestResponse();
            objResponseofContactRequestResp.func = Convert.ToString((int)FUNCTION_CODES.ADD_CONTACT_CONFIRMATION);
            objResponseofContactRequestResp.rid = _requestId;
            objResponseofContactRequestResp.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<ResponseofContactRequestResponse>(objResponseofContactRequestResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class ResponseofContactRequestResponse : CommonResponse
    {
        public ResponseofContactRequestResponse()
        {

        }
    }
}
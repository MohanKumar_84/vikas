﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class UserLoginReq
    {
        string _password, _requestId, _deviceId, _deviceType, _companyId, _DomainName, _userName, _ipAddress, _hostName, _osVersion, _appVersion;
        int _functionCode;

        public string HostName
        {
            get { return _hostName; }
        }
        public string DomainName
        {
            get { return _DomainName; }
        }
        public string IpAddress
        {
            get { return _ipAddress; }
        }
        public string UserName
        {
            get { return _userName; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string Password
        {
            get { return _password; }
        }
        public string OSVersion
        {
            get { return _osVersion; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string AppVersion
        {
            get { return _appVersion; }
        }

        public UserLoginReq(string requestJson, string hostName, string ipAddress)
        {
            RequestJsonParsing<UserLoginReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<UserLoginReqData>>(requestJson);
            
            _companyId = objRequestJsonParsing.req.data.enid;
            string decryptedUserName = EncryptionDecryption.AESDecrypt(_companyId, objRequestJsonParsing.req.data.unm);
            if (decryptedUserName.Contains("\\"))
            {
                _DomainName = decryptedUserName.Split('\\')[0];
                _userName = decryptedUserName.Split('\\')[1];
            }
            else
            {
                _DomainName = String.Empty;
                _userName = decryptedUserName;
            }

            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.MOBILE_USER_LOGIN)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _password = EncryptionDecryption.AESDecrypt(_companyId, objRequestJsonParsing.req.data.pwd);
            _ipAddress = ipAddress;
            _hostName = hostName;
            _osVersion = objRequestJsonParsing.req.data.ver;
            _appVersion = objRequestJsonParsing.req.data.mfv;            
        }
    }

    [DataContract]
    public class UserLoginReqData : Data
    {
        /// <summary>
        /// Password
        /// </summary>
        [DataMember]
        public string pwd { get; set; }

        /// <summary>
        /// Os Version
        /// </summary>
        [DataMember]
        public string ver { get; set; }
        /// <summary>
        /// Os Version
        /// </summary>
        [DataMember]
        public string mfv { get; set; }
    }
}
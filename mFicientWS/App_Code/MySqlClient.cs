﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
using System.Configuration;

namespace mFicientWS
{
    public class MySqlClient
    {

        /// <summary>
        /// Use it for transaction.It is used without any try catch block connection is not closes on error
        /// </summary>
        /// <param name="con"></param>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public static DataSet SelectDataWithOpenConForDoTask(MySqlCommand cmdWithConnection, List<QueryParameters> parameters)
        {
            //Every time add new set of parameter.It is possible that the cmd is already used
            //and already has parameters with same name.hence this is required
            cmdWithConnection.Parameters.Clear();
            DataSet ObjDataSet = new DataSet();

            MySqlDataAdapter objSqlDataAdapter = new MySqlDataAdapter(cmdWithConnection);

            foreach (QueryParameters param in parameters)
            {
                cmdWithConnection.Parameters.AddWithValue("@" + param.para, param.val);
            }

            objSqlDataAdapter.Fill(ObjDataSet);

            return ObjDataSet;
        }

        public static DataSet SelectDataFromDoTask(string db_cmd, List<QueryParameters> lstParameters, string userId, string password, string endPoint, string dbName)
        {
            MySqlConnection objSqlConnection = null;
            try
            {
                string strConString = "Server = " + endPoint + ";Initial Catalog=" + dbName + ";User Id=" + userId + ";Password =" + password;

                OpenConnection(out objSqlConnection, strConString);

                MySqlCommand _SqlCommand = new MySqlCommand(db_cmd, objSqlConnection);

                foreach (QueryParameters parameters in lstParameters)
                {
                    _SqlCommand.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                DataSet ObjDataSet = new DataSet();

                MySqlDataAdapter objSqlDataAdapter = new MySqlDataAdapter(_SqlCommand);

                objSqlDataAdapter.Fill(ObjDataSet);

                return ObjDataSet;
            }
            catch (MySqlException ex)
            {
                throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Must declare the scalar variable"))
                {
                    throw new Exception(((int)DO_TASK_ERROR.SQL_PARAMETER_ERROR).ToString());
                }
                if (ex.Message.Contains("Login failed for user"))
                {
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                }
                if (ex.Message.Contains("A network-related or instance-specific error occurred"))
                {
                    throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
                }
                else
                {
                    throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
                }
            }
            finally
            {
                CloseConnection(objSqlConnection);
            }
        }

        /// <summary>
        /// Description :MySqlConnection Open Function
        /// </summary>
        public static void OpenConnection(out MySqlConnection Conn, string connectionString)
        {
            try
            {
                Conn = new MySqlConnection(connectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch (MySqlException sqlex)
            {
                Conn = null;
                throw sqlex;
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }

        /// <summary>
        /// Description :MySqlConnection Close Function
        /// </summary>
        public static void CloseConnection(MySqlConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open))
                {
                    Conn.Close();
                }
            }
            catch
            {
            }
        }

        public static int ExecuteNonQueryUsingTransaction(MySqlCommand _SqlCommand, MySqlConnection connection, MySqlTransaction transaction)
        {
            _SqlCommand.Connection = connection;
            _SqlCommand.Transaction = transaction;
            return _SqlCommand.ExecuteNonQuery();
        }

        public static DataSet SelectDataFromDoTask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _password)
        {
            MySqlConnection objSqlConnection = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId, _userId, _password);
                dbcon.GetConnector();
                string strConString = "Server = " + dbcon.HostName + ";Initial Catalog=" + dbcon.DataBaseName + ";User Id=" + dbcon.UserId + ";Password =" + dbcon.Password;

                OpenConnection(out objSqlConnection, strConString);

                MySqlCommand _SqlCommand = new MySqlCommand(db_cmd.SqlQuery, objSqlConnection);

                foreach (QueryParameters parameters in LstParameters)
                {
                    _SqlCommand.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                DataSet ObjDataSet = new DataSet();

                MySqlDataAdapter objSqlDataAdapter = new MySqlDataAdapter(_SqlCommand);

                objSqlDataAdapter.Fill(ObjDataSet);

                return ObjDataSet;
            }
            catch (MySqlException ex)
            {
                throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Must declare the scalar variable"))
                {
                    throw new Exception(((int)DO_TASK_ERROR.SQL_PARAMETER_ERROR).ToString());
                }
                if (ex.Message.Contains("Login failed for user"))
                {
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                }
                if (ex.Message.Contains("A network-related or instance-specific error occurred"))
                {
                    throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
                }
                else
                {
                    throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
                }
            }
            finally
            {
                CloseConnection(objSqlConnection);
            }
        }

        /// <summary>
        /// Description :Data Process (Insert, Update, Delete ) According To SqlCommand ( Sql Query And Parameter )
        /// </summary>
        public static int ExecuteNonQueryRecordDotask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _password)
        {
            MySqlConnection objSqlConnection = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId, _userId, _password);


                OpenConnection(out objSqlConnection, dbcon.ConnectionString);

                MySqlCommand _SqlCommand = new MySqlCommand(db_cmd.SqlQuery, objSqlConnection);

                foreach (QueryParameters parameters in LstParameters)
                {
                    _SqlCommand.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                int i = _SqlCommand.ExecuteNonQuery();

                return i;
            }

            catch (MySqlException ex)
            {
                throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Must declare the scalar variable"))
                {
                    throw new Exception(((int)DO_TASK_ERROR.SQL_PARAMETER_ERROR).ToString());
                }
                if (ex.Message.Contains("Login failed for user"))
                {
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                }
                if (ex.Message.Contains("A network-related or instance-specific error occurred"))
                {
                    throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
                }
                else
                {
                    throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
                }
            }
            finally
            {
                CloseConnection(objSqlConnection);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmdWithCon"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static int ExecuteNonQueryWithOpenCon(MySqlCommand cmdWithCon, List<QueryParameters> parameters)
        {
            //Every time add new set of parameter.It is possible that the cmd is already used
            //and already has parameters with same name.hence this is required
            cmdWithCon.Parameters.Clear();
            foreach (QueryParameters param in parameters)
            {
                cmdWithCon.Parameters.AddWithValue("@" + param.para, param.val);
            }
            return cmdWithCon.ExecuteNonQuery();
        }
    }
}
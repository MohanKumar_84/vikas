﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Collections;

namespace mFicientWS
{
    //public class ReadHTTPXMLResponseForBatchProcess
    //{
    //    DoTaskReqForBatchProcess _doTaskRequest;
    //    string _responseXML, _recordPath, _datasetPath;
    //    DoTaskCmdDetails _cmdDetails;
    //    public ReadHTTPXMLResponseForBatchProcess(DoTaskReqForBatchProcess doTaskRequest, string responseXML, string recordPath, string datasetPath, DoTaskCmdDetails cmdDetails)
    //    {
    //        _doTaskRequest = doTaskRequest;
    //        _responseXML = responseXML;
    //        _recordPath = recordPath;
    //        _datasetPath = datasetPath;
    //        _cmdDetails = cmdDetails;
    //    }
    //    public Object Process()
    //    {
    //        XmlDocument respXML = new XmlDocument();
    //        respXML.LoadXml(_responseXML);
    //        object objToReturn=null;
    //        try
    //        {
    //            XmlNode xmlNodeData = respXML.SelectSingleNode("//*[local-name()='" + _datasetPath + "']");
    //            switch ((DO_TASK_RETURN_FUNCTION)Enum.Parse(typeof(DO_TASK_RETURN_FUNCTION), _cmdDetails.rtfn.ToString()))
    //            {
    //                case DO_TASK_RETURN_FUNCTION.COMPLETE_DATA:
    //                    break;
    //                case DO_TASK_RETURN_FUNCTION.DROP_DOWN_DATA:
    //                case DO_TASK_RETURN_FUNCTION.PIE_DATA:
    //                    objToReturn= getListOfDropDownDataForHTTPXML(respXML, _datasetPath, _recordPath);
    //                    break;
    //                case DO_TASK_RETURN_FUNCTION.REPEATER_DATA:
    //                    objToReturn= getListOfRepeaterDataForHTTPXML(respXML, _datasetPath, _recordPath);
    //                    break;
    //                case DO_TASK_RETURN_FUNCTION.BAR_LINE_DATA:
    //                case DO_TASK_RETURN_FUNCTION.BAR_LINE:
    //                    objToReturn= getListOfBarLineDataForHTTPXML(respXML, _datasetPath, _recordPath);
    //                    break;

    //            }

    //            return objToReturn;
    //        }
    //        catch
    //        {
    //            return null;
    //        }
            
    //    }

    //    List<rptData> getListOfRepeaterDataForHTTPXML(XmlDocument document, string datasetPath, string recordpath)
    //    {
    //        //Algorithm
    //        //get modified path of each element
    //        //find count of the node list.if throws an exception then throw the exception that tag not found.
    //        //the one with the greatest count ,means that many object are to be formed.
    //        //if the tag is not part of an array,then the valie goes to all the objects.
    //        //if the tag path is empty then also empty is returned to all objects.

    //        int iCountOfLoop = 0;
    //        rptData objRptData = null;
    //        Hashtable hshTblOfObjectFormed = new Hashtable();
    //        Hashtable hashOfTagsWithSingleValue = new Hashtable();
    //        List<rptData> lstRptData = new List<rptData>();

    //        int iLargestCountOfTags = 0;
    //        XmlNodeList nodeListI = null, nodeListB = null, nodeListS = null, nodeListR = null, nodeListC = null;
    //        string tagPath = "";
    //        //get modified path of each element
    //        //find count of the node list.if throws and exption then thorw the exception that tag not found.

    //        tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordpath,_cmdDetails.rt.i);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListI = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = nodeListI.Count;
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("i", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordpath, _cmdDetails.rt.b);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListB = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListB.Count ? nodeListB.Count : iLargestCountOfTags;
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("b", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordpath, _cmdDetails.rt.s);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListS = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListS.Count ? nodeListS.Count : iLargestCountOfTags;
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("s", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }

    //        tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordpath, _cmdDetails.rt.r);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListR = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListR.Count ? nodeListR.Count : iLargestCountOfTags;
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("r", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }

    //        tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordpath, _cmdDetails.rt.c);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListC = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListC.Count ? nodeListC.Count : iLargestCountOfTags;
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("c", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        //the one with the greatest count ,means that many object are to be formed.
    //        for (int i = 0; i <= iLargestCountOfTags - 1; i++)
    //        {
    //            objRptData = new rptData();
    //            //fill object with default vlue as string.Empty
    //            objRptData.i = "";
    //            objRptData.b = "";
    //            objRptData.r = "";
    //            objRptData.s = "";
    //            objRptData.c = "";
    //            hshTblOfObjectFormed.Add(i, objRptData);
    //        }
    //        //if the tag is not part of an array,then the valie goes to all the objects.
    //        foreach (DictionaryEntry rptObject in hshTblOfObjectFormed)
    //        {
    //            objRptData = (rptData)rptObject.Value;
    //            foreach (DictionaryEntry tag in hashOfTagsWithSingleValue)
    //            {
    //                if (tag.Key.ToString().ToLower() == "i")
    //                {
    //                    objRptData.i = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "b")
    //                {
    //                    objRptData.b = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "s")
    //                {
    //                    objRptData.s = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "r")
    //                {
    //                    objRptData.r = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "c")
    //                {
    //                    objRptData.c = Convert.ToString(tag.Value);
    //                }
    //            }
    //        }
    //        //get i
    //        if (nodeListI != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListI)
    //            {
    //                objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objRptData.i = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get b
    //        if (nodeListB != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListB)
    //            {
    //                objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objRptData.b = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get s
    //        if (nodeListS != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListS)
    //            {
    //                objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objRptData.s = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get r
    //        if (nodeListR != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListR)
    //            {
    //                objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objRptData.r = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get c
    //        if (nodeListR != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListC)
    //            {
    //                objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objRptData.c = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }

    //        //form the list and return
    //        foreach (Object obj in hshTblOfObjectFormed.Values)
    //        {
    //            objRptData = (rptData)obj;
    //            lstRptData.Add(objRptData);
    //        }
    //        return lstRptData;
    //    }

    //    List<Pie_ddl_Data> getListOfDropDownDataForHTTPXML(XmlDocument document, string datasetPath, string recordpath)
    //    {
    //        //Algorithm
    //        //get modified path of each element
    //        //find count of the node list.if throws and exption then thorw the exception that tag not found.
    //        //the one with the greatest count ,means that many object are to be formed.
    //        //if the tag is not part of an array,then the valie goes to all the objects.
    //        //if the tag path is empty then also empty is returned to all objects.

    //        int iCountOfLoop = 0;
    //        Pie_ddl_Data objDropDownData = null;
    //        Hashtable hshTblOfObjectFormed = new Hashtable();
    //        Hashtable hashOfTagsWithSingleValue = new Hashtable();
    //        List<Pie_ddl_Data> lstDropDownData = new List<Pie_ddl_Data>();

    //        int iLargestCountOfTags = 0;
    //        XmlNodeList nodeListT = null, nodeListV = null;

    //        //get modified path of each element
    //        //find count of the node list.if throws and exption then thorw the exception that tag not found.
    //        string tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordpath, _cmdDetails.rt.t);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListT = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = nodeListT.Count;

    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("t", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordpath, _cmdDetails.rt.v);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListV = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListV.Count ? nodeListV.Count : iLargestCountOfTags;
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("v", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }

    //        //the one with the greatest count ,means that many object are to be formed.
    //        for (int i = 0; i <= iLargestCountOfTags - 1; i++)
    //        {
    //            objDropDownData = new Pie_ddl_Data();
    //            //fill object with default vlue as string.Empty
    //            objDropDownData.t = "";
    //            objDropDownData.v = "";
    //            hshTblOfObjectFormed.Add(i, objDropDownData);
    //        }
    //        //if the tag is not part of an array,then the valie goes to all the objects.
    //        foreach (DictionaryEntry ddlObject in hshTblOfObjectFormed)
    //        {
    //            objDropDownData = (Pie_ddl_Data)ddlObject.Value;
    //            foreach (DictionaryEntry tag in hashOfTagsWithSingleValue)
    //            {
    //                if (tag.Key.ToString().ToLower() == "t")
    //                {
    //                    objDropDownData.t = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "v")
    //                {
    //                    objDropDownData.v = Convert.ToString(tag.Value);
    //                }
    //            }
    //        }
    //        //get t
    //        if (nodeListT != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListT)
    //            {
    //                objDropDownData = (Pie_ddl_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objDropDownData.t = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get v
    //        if (nodeListV != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListV)
    //            {
    //                objDropDownData = (Pie_ddl_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objDropDownData.v = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }

    //        //form the list and return
    //        foreach (Object obj in hshTblOfObjectFormed.Values)
    //        {
    //            objDropDownData = (Pie_ddl_Data)obj;
    //            lstDropDownData.Add(objDropDownData);
    //        }
    //        return lstDropDownData;
    //    }

    //    List<Bar_Line_Data> getListOfBarLineDataForHTTPXML(XmlDocument document, string datasetPath, string recordPath)
    //    {
    //        //Algorithm
    //        //get modified path of each element
    //        //find count of the node list.if throws and exption then thorw the exception that tag not found.
    //        //the one with the greatest count ,means that many object are to be formed.
    //        //if the tag is not part of an array,then the valie goes to all the objects.
    //        //if the tag path is empty then also empty is returned to all objects.

    //        int iCountOfLoop = 0;
    //        Bar_Line_Data objBarLineData = null;
    //        Hashtable hshTblOfObjectFormed = new Hashtable();
    //        Hashtable hashOfTagsWithSingleValue = new Hashtable();
    //        List<Bar_Line_Data> lstBarLineData = new List<Bar_Line_Data>();

    //        int iLargestCountOfTags = 0;
    //        XmlNodeList nodeListT = null, nodeListV1 = null, nodeListV2 = null, nodeListV3 = null, nodeListV4 = null, nodeListV5 = null;


    //        //get modified path of each element
    //        //find count of the node list.if throws and exption then thorw the exception that tag not found.
    //        string tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordPath, _cmdDetails.rt.t);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListT = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = nodeListT.Count;
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("t", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordPath, _cmdDetails.rt.v1);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListV1 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListV1.Count ? nodeListV1.Count : iLargestCountOfTags;
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("v1", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordPath, _cmdDetails.rt.v2);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListV2 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListV2.Count ? nodeListV2.Count : iLargestCountOfTags;
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("v2", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }

    //        tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordPath, _cmdDetails.rt.v3);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListV3 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListV3.Count ? nodeListV3.Count : iLargestCountOfTags;
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("v3", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }

    //        tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordPath, _cmdDetails.rt.v4);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListV4 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListV4.Count ? nodeListV4.Count : iLargestCountOfTags;
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("v4", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }

    //        tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordPath, _cmdDetails.rt.v5);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListV5 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListV5.Count ? nodeListV5.Count : iLargestCountOfTags;
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("v5", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        //the one with the greatest count ,means that many object are to be formed.
    //        for (int i = 0; i <= iLargestCountOfTags - 1; i++)
    //        {
    //            objBarLineData = new Bar_Line_Data();
    //            //fill object with default vlue as string.Empty
    //            objBarLineData.t = "";
    //            objBarLineData.v1 = "";
    //            objBarLineData.v2 = "";
    //            objBarLineData.v3 = "";
    //            objBarLineData.v4 = "";
    //            objBarLineData.v5 = "";
    //            hshTblOfObjectFormed.Add(i, objBarLineData);
    //        }
    //        //if the tag is not part of an array,then the valie goes to all the objects.
    //        foreach (DictionaryEntry barLineObject in hshTblOfObjectFormed)
    //        {
    //            objBarLineData = (Bar_Line_Data)barLineObject.Value;
    //            foreach (DictionaryEntry tag in hashOfTagsWithSingleValue)
    //            {
    //                if (tag.Key.ToString().ToLower() == "t".ToLower())
    //                {
    //                    objBarLineData.t = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "v1".ToLower())
    //                {
    //                    objBarLineData.v1 = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "v2".ToLower())
    //                {
    //                    objBarLineData.v2 = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "v3".ToLower())
    //                {
    //                    objBarLineData.v3 = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "v4".ToLower())
    //                {
    //                    objBarLineData.v4 = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "v5".ToLower())
    //                {
    //                    objBarLineData.v5 = Convert.ToString(tag.Value);
    //                }
    //            }
    //        }
    //        //get t
    //        if (nodeListT != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListT)
    //            {
    //                objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objBarLineData.t = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get v1
    //        if (nodeListV1 != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListV1)
    //            {
    //                objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objBarLineData.v1 = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get v2
    //        if (nodeListV2 != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListV2)
    //            {
    //                objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objBarLineData.v2 = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get v3
    //        if (nodeListV3 != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListV3)
    //            {
    //                objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objBarLineData.v3 = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }

    //        //get v4
    //        if (nodeListV4 != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListV4)
    //            {
    //                objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objBarLineData.v4 = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }

    //        //get v5
    //        if (nodeListV5 != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListV5)
    //            {
    //                objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objBarLineData.v5 = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }

    //        //form the list and return
    //        foreach (Object obj in hshTblOfObjectFormed.Values)
    //        {
    //            objBarLineData = (Bar_Line_Data)obj;
    //            lstBarLineData.Add(objBarLineData);
    //        }
    //        return lstBarLineData;
    //    }

    //    List<Pie_ddl_Data> getListOfPieChartDataForHTTPXML(XmlDocument document, string datasetPath, string recordPath)
    //    {
    //        //Algorithm
    //        //get modified path of each element
    //        //find count of the node list.if throws and exption then thorw the exception that tag not found.
    //        //the one with the greatest count ,means that many object are to be formed.
    //        //if the tag is not part of an array,then the valie goes to all the objects.
    //        //if the tag path is empty then also empty is returned to all objects.

    //        int iCountOfLoop = 0;
    //        Pie_ddl_Data objPieChartData = null;
    //        Hashtable hshTblOfObjectFormed = new Hashtable();
    //        Hashtable hashOfTagsWithSingleValue = new Hashtable();
    //        List<Pie_ddl_Data> lstPieChartData = new List<Pie_ddl_Data>();

    //        int iLargestCountOfTags = 0;
    //        XmlNodeList nodeListT = null, nodeListV = null;

    //        //get modified path of each element
    //        //find count of the node list.if throws and exption then thorw the exception that tag not found.
    //        string tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordPath, _cmdDetails.rt.t);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListT = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = nodeListT.Count;
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("t", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        tagPath = getModifiedPathOfTagForHTTPXMLResp(datasetPath, recordPath, _cmdDetails.rt.v);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListV = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListV.Count ? nodeListV.Count : iLargestCountOfTags;
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("v", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }

    //        //the one with the greatest count ,means that many object are to be formed.
    //        for (int i = 0; i <= iLargestCountOfTags - 1; i++)
    //        {
    //            objPieChartData = new Pie_ddl_Data();
    //            //fill object with default vlue as string.Empty
    //            objPieChartData.t = "";
    //            objPieChartData.v = "";
    //            hshTblOfObjectFormed.Add(i, objPieChartData);
    //        }
    //        //if the tag is not part of an array,then the valie goes to all the objects.
    //        foreach (DictionaryEntry pieDataObject in hshTblOfObjectFormed)
    //        {
    //            objPieChartData = (Pie_ddl_Data)pieDataObject.Value;
    //            foreach (DictionaryEntry tag in hashOfTagsWithSingleValue)
    //            {
    //                if (tag.Key.ToString().ToLower() == "t")
    //                {
    //                    objPieChartData.t = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "v")
    //                {
    //                    objPieChartData.v = Convert.ToString(tag.Value);
    //                }
    //            }
    //        }
    //        //get t
    //        if (nodeListT != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListT)
    //            {
    //                objPieChartData = (Pie_ddl_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objPieChartData.t = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get v
    //        if (nodeListV != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListV)
    //            {
    //                objPieChartData = (Pie_ddl_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objPieChartData.v = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }

    //        //form the list and return
    //        foreach (Object obj in hshTblOfObjectFormed.Values)
    //        {
    //            objPieChartData = (Pie_ddl_Data)obj;
    //            lstPieChartData.Add(objPieChartData);
    //        }
    //        return lstPieChartData;
    //    }

    //    string getModifiedPathOfTagForHTTPXMLResp(string datasetPath, string recordPath, string pathOfElement)
    //    {
    //        string strModifiedTagPath = "";
    //        //isPartOfAnArray = false;
    //        if (!String.IsNullOrEmpty(pathOfElement))
    //        {
    //            string strLocalnamePrefix = "/*[local-name()='";
    //            string strLocalNameSuffix = "']";
    //            string[] aryPath = datasetPath.Split('/');
    //            for (int i = 0; i <= aryPath.Length - 1; i++)
    //            {
    //                if (!String.IsNullOrEmpty(aryPath[i]))
    //                {
    //                    if (strModifiedTagPath.Length == 0)
    //                    {
    //                        strModifiedTagPath += "//*[local-name()='" + aryPath[i] + strLocalNameSuffix;
    //                    }
    //                    else
    //                    {
    //                        strModifiedTagPath += strLocalnamePrefix + aryPath[i] + strLocalNameSuffix;
    //                    }
    //                }
    //            }
    //            if (!(datasetPath.Trim().ToLower() == recordPath.Trim().ToLower()))//if it is multiple then dataset path and record path does not match
    //            {
    //                aryPath = recordPath.Split('/');
    //                for (int i = 0; i <= aryPath.Length - 1; i++)
    //                {
    //                    if (!String.IsNullOrEmpty(aryPath[i]))
    //                    {
    //                        strModifiedTagPath += strLocalnamePrefix + aryPath[i] + strLocalNameSuffix;
    //                    }
    //                }
    //            }
    //            aryPath = pathOfElement.Split('/');
    //            for (int i = 0; i <= aryPath.Length - 1; i++)
    //            {
    //                if (!String.IsNullOrEmpty(aryPath[i]))
    //                {
    //                    strModifiedTagPath += strLocalnamePrefix + aryPath[i] + strLocalNameSuffix;
    //                }
    //            }
    //        }
    //        return strModifiedTagPath;
    //    }
    //}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientWS
{
    public class WSCCompanyDetails : MficientBase
    {
        string _enterpriseId;
        MFECompanyInfo _companyInfo;

        public WSCCompanyDetails(string enterpriseId)
        {
            _enterpriseId = enterpriseId;
        }
        public void Process()
        {
            string strQuery = @"SELECT s.SERVER_URL,c.*,a.AUTO_LOGIN FROM TBL_COMPANY_DETAIL as c 
                                    Left outer join TBL_ACCOUNT_SETTINGS as a on c.company_id=a.company_id
                                    left outer join [dbo].[ADMIN_TBL_SERVER_MAPPING] as m on c.company_id=m.company_id
                                    left outer join [dbo].[ADMIN_TBL_MST_SERVER] as s on m.server_ID =s.server_id 
                                    WHERE c.COMPANY_ID = @CompanyId;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", _enterpriseId);
            DataSet dsCompanyDetails = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (dsCompanyDetails != null)
            {
                if (dsCompanyDetails.Tables[0].Rows.Count > 0)
                {
                    DataRow drCompanyDtl = dsCompanyDetails.Tables[0].Rows[0];
                    this.CompanyInfo = fillCompanyInfoFromDRow(drCompanyDtl);
                }
                this.StatusCode = 0;
                this.StatusDescription = String.Empty;
            }
            else
            {
                throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            }
        }
        MFECompanyInfo fillCompanyInfoFromDRow(DataRow cmpDtlRow)
        {
            if (cmpDtlRow == null) throw new ArgumentException();
            MFECompanyInfo objCmpInfo = new MFECompanyInfo();
            objCmpInfo.CompanyId = Convert.ToString(cmpDtlRow["COMPANY_ID"]);
            objCmpInfo.CompanyName = Convert.ToString(cmpDtlRow["COMPANY_NAME"]);
            objCmpInfo.RegisterationNo = Convert.ToString(cmpDtlRow["REGISTRATION_NO"]);
            objCmpInfo.AdminId = Convert.ToString(cmpDtlRow["ADMIN_ID"]);
            objCmpInfo.LogoImageName = Convert.ToString(cmpDtlRow["LOGO_IMAGE_NAME"]);
            objCmpInfo.SupportEmail = Convert.ToString(cmpDtlRow["SUPPORT_EMAIL"]);
            objCmpInfo.SupportContact = Convert.ToString(cmpDtlRow["SUPPORT_CONTACT"]);
            objCmpInfo.StreetAddress1 = Convert.ToString(cmpDtlRow["STREET_ADDRESS1"]);
            objCmpInfo.StreetAddress2 = Convert.ToString(cmpDtlRow["STREET_ADDRESS2"]);
            objCmpInfo.StreetAddress3 = Convert.ToString(cmpDtlRow["STREET_ADDRESS3"]);
            objCmpInfo.CityName = Convert.ToString(cmpDtlRow["CITY_NAME"]);
            objCmpInfo.StateName = Convert.ToString(cmpDtlRow["STATE_NAME"]);
            objCmpInfo.CountryCode = Convert.ToString(cmpDtlRow["COUNTRY_CODE"]);
            objCmpInfo.Zip = Convert.ToString(cmpDtlRow["ZIP"]);
            objCmpInfo.TimezoneId = Convert.ToString(cmpDtlRow["TIMEZONE_ID"]);
            objCmpInfo.IsBlocked = Convert.ToBoolean(cmpDtlRow["IS_BLOCKED"]);
            objCmpInfo.BlockOrUnblockId = Convert.ToString(cmpDtlRow["BLOCK_UNBLOCK_ID"]);
            objCmpInfo.AutoLogin = Convert.ToBoolean(cmpDtlRow["AUTO_LOGIN"]) ? "1" : "0";
            objCmpInfo.ServerURL = Convert.ToString(cmpDtlRow["SERVER_URL"]);
            return objCmpInfo;
        }

        #region Public Properties
        public string EnterpriseId
        {
            get { return _enterpriseId; }
            private set { _enterpriseId = value; }
        }
        public MFECompanyInfo CompanyInfo
        {
            get { return _companyInfo; }
            private set { _companyInfo = value; }
        }
        #endregion
    }
}
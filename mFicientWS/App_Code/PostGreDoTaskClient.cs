﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;
using System.Configuration;
using System.Data;

namespace mFicientWS
{
    public class PostGreDoTaskClient
    {
        public static void NpgsqlConnectionOpen(out NpgsqlConnection Conn)
        {
            try
            {
                Conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch (NpgsqlException npgEx)
            {
                Conn = null;
                if (npgEx.Message.Contains(@"password authentication failed") || npgEx.Code == "28P01" || npgEx.Code == "28000")
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                else
                    throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
            }
        }

        public static void NpgsqlConnectionOpen(out NpgsqlConnection Conn, string ConnectionString)
        {
            try
            {
                Conn = new NpgsqlConnection(ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch (NpgsqlException npgEx)
            {
                Conn = null;
                if (npgEx.Message.Contains(@"password authentication failed") || npgEx.Code == "28P01" || npgEx.Code == "28000")
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                else
                    throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
            }
        }

        public static void NpgsqlConnectionClose(NpgsqlConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open))
                {
                    Conn.Close();
                }
            }
            catch
            {
            }
        }

        public static DataSet SelectDataFromDoTask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _password)
        {
            NpgsqlConnection conn = null;
            DataSet ObjDataSet = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId, _userId, _password);
                dbcon.GetConnector();

                try
                {
                    NpgsqlConnectionOpen(out conn, dbcon.ConnectionString);
                }
                catch (NpgsqlException ex)
                {
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                }

                NpgsqlCommand cmd = new NpgsqlCommand(db_cmd.SqlQuery, conn);

                foreach (QueryParameters parameters in LstParameters)
                {
                    cmd.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                ObjDataSet = new DataSet();

                NpgsqlDataAdapter objSqlDataAdapter = new NpgsqlDataAdapter(cmd);

                objSqlDataAdapter.Fill(ObjDataSet);
            }
            catch (NpgsqlException mySqlEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                NpgsqlConnectionClose(conn);
            }

            return ObjDataSet;
        }

        public static int ExecuteNonQueryRecordDotask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _password)
        {
            NpgsqlConnection objNpgsqlConnection = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId,  _userId,  _password);
                dbcon.GetConnector();

                NpgsqlConnectionOpen(out objNpgsqlConnection, dbcon.ConnectionString);

                NpgsqlCommand _NpgsqlCommand = new NpgsqlCommand(db_cmd.SqlQuery, objNpgsqlConnection);

                foreach (QueryParameters parameters in LstParameters)
                {
                    _NpgsqlCommand.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                int i = _NpgsqlCommand.ExecuteNonQuery();

                return i;
            }
            catch (NpgsqlException npgSqlEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                NpgsqlConnectionClose(objNpgsqlConnection);
            }
            //return 0;
        }

        public static string ExecuteScalarDotask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _password)
        {
            NpgsqlConnection objNpgsqlConnection = null;
            string strResult = "";
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId,  _userId,  _password);
                dbcon.GetConnector();

                NpgsqlConnectionOpen(out objNpgsqlConnection, dbcon.ConnectionString);

                NpgsqlCommand _NpgsqlCommand = new NpgsqlCommand(db_cmd.SqlQuery, objNpgsqlConnection);

                foreach (QueryParameters parameters in LstParameters)
                {
                    _NpgsqlCommand.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                object obj = _NpgsqlCommand.ExecuteScalar();
                if (obj != null)
                    strResult = obj.ToString();
            }
            catch (NpgsqlException npgSqlEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                NpgsqlConnectionClose(objNpgsqlConnection);
            }

            return strResult;
        }

        public static DataSet ExecuteStoredProcedureDoTask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _password)
        {
            NpgsqlConnection conn = null;
            DataSet ObjDataSet = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId,  _userId,  _password);
                dbcon.GetConnector();

                try
                {
                    NpgsqlConnectionOpen(out conn, dbcon.ConnectionString);
                }
                catch (NpgsqlException ex)
                {
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                }

                NpgsqlCommand cmd = new NpgsqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = db_cmd.SqlQuery;

                foreach (QueryParameters parameters in LstParameters)
                {
                    cmd.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                ObjDataSet = new DataSet();

                NpgsqlDataAdapter objSqlDataAdapter = new NpgsqlDataAdapter(cmd);

                objSqlDataAdapter.Fill(ObjDataSet);
            }
            catch (NpgsqlException npgSqlEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                NpgsqlConnectionClose(conn);
            }

            return ObjDataSet;
        }
    }
}
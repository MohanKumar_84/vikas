﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class UserContacts
    {
        UserContactsReq UserContactsReq;
        System.Web.Caching.Cache _appCache;
        public UserContacts(UserContactsReq _UserContactsReq, System.Web.Caching.Cache cache)
        {
            UserContactsReq = _UserContactsReq;
            _appCache = cache;
        }
        public UserContactsResp Process()
        {
            ResponseStatus objRespStatus = new ResponseStatus();
            try
            {                
                GetUserContactList userContacts = new GetUserContactList(Utilities.GetUserID(UserContactsReq.CompanyId, UserContactsReq.Username), UserContactsReq.CompanyId);
                List<UserData> lstUsersDataDtls = userContacts.GetContacts();
                objRespStatus.cd = "0";
                objRespStatus.desc = "";

                return new UserContactsResp(objRespStatus, UserContactsReq.RequestId, lstUsersDataDtls);
            }
            catch (Exception ex)
            {
                if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                throw ex;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class UserDetailReq
    {
        string  _userId, _sessionId, _requestId, _deviceId, _deviceType, _companyId;
        int _functionCode;
        long _updatedOn;
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string SessionId
        {
            get { return _sessionId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        //public string Email
        //{
        //    get { return _email; }
        //}
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public long UpdatedOn
        {
            get { return _updatedOn; }
        }

        public UserDetailReq(string requestJson, string userId)
        {
            RequestJsonParsing<UserDetailReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<UserDetailReqData>>(requestJson);
            //_email = objRequestJsonParsing.req.data.em;
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            _userId = userId;
            if (_functionCode != (int)FUNCTION_CODES.USER_DETAIL)
            {
                throw new Exception("Invalid Function Code");
            }
            _sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _updatedOn = Convert.ToInt64(objRequestJsonParsing.req.data.udton);
        }
    }

    [DataContract]
    public class UserDetailReqData : Data
    {
        /// <summary>
        /// Updated On
        /// </summary>
        [DataMember]
        public string udton { get; set; }
    }
}
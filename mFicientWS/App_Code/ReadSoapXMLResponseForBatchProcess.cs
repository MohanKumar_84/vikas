﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Collections;

namespace mFicientWS
{
    //public class ReadSoapXMLResponseForBatchProcess
    //{
    //    DoTaskReqForBatchProcess _doTaskRequest;
    //    string _responseXML;
    //    DoTaskCmdDetails _cmdDetails;
    //    public ReadSoapXMLResponseForBatchProcess(DoTaskReqForBatchProcess doTaskRequest, string responseXML, DoTaskCmdDetails cmdDetails)
    //    {
    //        _doTaskRequest = doTaskRequest;
    //        _responseXML = responseXML;
    //        _cmdDetails = cmdDetails;
    //    }
    //    public Object Process()
    //    {
    //        XmlDocument document = new XmlDocument();
    //        document.LoadXml(_responseXML);
    //        Hashtable hshTblOfObjectFormed = new Hashtable();
    //        object objToReturn = null;
    //        switch ((RETURN_FUNCTION_TYPE_DOTASK)Enum.Parse(typeof(RETURN_FUNCTION_TYPE_DOTASK), _cmdDetails.rtfn.ToString()))
    //        {
    //            case RETURN_FUNCTION_TYPE_DOTASK.COMPLETE_TBL_CLMS_AND_ROWS_SELECT:
    //                break;
    //            case RETURN_FUNCTION_TYPE_DOTASK.DROP_DOWN_DATA:
    //            case RETURN_FUNCTION_TYPE_DOTASK.PIE_DATA:
    //                objToReturn= getListOfDropDownOrPieDataForSoapXML(document);
    //                break;
    //            case RETURN_FUNCTION_TYPE_DOTASK.REPEATER_DATA:
    //                objToReturn= getListOfRepeaterDataForSoapXML(document);
    //                break;
    //            case RETURN_FUNCTION_TYPE_DOTASK.LINE_DATA:
    //            case RETURN_FUNCTION_TYPE_DOTASK.BAR_DATA:
    //                objToReturn= getListOfBarLineDataForSoapXML(document);
    //                break;

    //        }
    //        return objToReturn;

    //    }
    //    List<rptData> getListOfRepeaterDataForSoapXML(XmlDocument document)
    //    {
    //        //Algorithm
    //        //get modified path of each element
    //        //find count of the node list.if throws an exception then throw the exception that tag not found.
    //        //the one with the greatest count ,means that many object are to be formed.
    //        //if the tag is not part of an array,then the valie goes to all the objects.
    //        //if the tag path is empty then also empty is returned to all objects.

    //        int iCountOfLoop = 0;
    //        rptData objRptData = null;
    //        Hashtable hshTblOfObjectFormed = new Hashtable();
    //        Hashtable hashOfTagsWithSingleValue = new Hashtable();
    //        List<rptData> lstRptData = new List<rptData>();

    //        int iLargestCountOfTags = 0;
    //        XmlNodeList nodeListI = null, nodeListB = null, nodeListS = null, nodeListR = null;
    //        bool blnIsPartOfAnArray;
    //        string tagPath = "";
    //        //get modified path of each element
    //        //find count of the node list.if throws and exption then thorw the exception that tag not found.

    //        tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_cmdDetails.rt.i, out blnIsPartOfAnArray);

    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                // nodeListI = document.SelectNodes("//*[local-name()='" + methodPartName + "']" + tagPath, new XmlNamespaceManager(document.NameTable));
    //                nodeListI = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = nodeListI.Count;
    //                if (!blnIsPartOfAnArray)
    //                {
    //                    hashOfTagsWithSingleValue.Add("i", nodeListI[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
    //                    nodeListI = null;
    //                }
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("i", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_cmdDetails.rt.b, out blnIsPartOfAnArray);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListB = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListB.Count ? nodeListB.Count : iLargestCountOfTags;
    //                if (!blnIsPartOfAnArray)
    //                {
    //                    hashOfTagsWithSingleValue.Add("b", nodeListB[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
    //                    nodeListB = null;
    //                }
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("b", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_cmdDetails.rt.s, out blnIsPartOfAnArray);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListS = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListS.Count ? nodeListS.Count : iLargestCountOfTags;
    //                if (!blnIsPartOfAnArray)
    //                {
    //                    hashOfTagsWithSingleValue.Add("s", nodeListS[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
    //                    nodeListS = null;
    //                }
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("s", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }

    //        tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_cmdDetails.rt.r, out blnIsPartOfAnArray);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListR = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListR.Count ? nodeListR.Count : iLargestCountOfTags;
    //                if (!blnIsPartOfAnArray)
    //                {
    //                    hashOfTagsWithSingleValue.Add("r", nodeListR[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
    //                    nodeListR = null;
    //                }
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("r", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        //the one with the greatest count ,means that many object are to be formed.
    //        for (int i = 0; i <= iLargestCountOfTags - 1; i++)
    //        {
    //            objRptData = new rptData();
    //            //fill object with default vlue as string.Empty
    //            objRptData.i = "";
    //            objRptData.b = "";
    //            objRptData.r = "";
    //            objRptData.s = "";
    //            hshTblOfObjectFormed.Add(i, objRptData);
    //        }
    //        //if the tag is not part of an array,then the valie goes to all the objects.
    //        foreach (DictionaryEntry rptObject in hshTblOfObjectFormed)
    //        {
    //            objRptData = (rptData)rptObject.Value;
    //            foreach (DictionaryEntry tag in hashOfTagsWithSingleValue)
    //            {
    //                if (tag.Key.ToString().ToLower() == "i")
    //                {
    //                    objRptData.i = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "b")
    //                {
    //                    objRptData.b = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "s")
    //                {
    //                    objRptData.s = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "r")
    //                {
    //                    objRptData.r = Convert.ToString(tag.Value);
    //                }
    //            }
    //        }
    //        //get i
    //        if (nodeListI != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListI)
    //            {
    //                objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objRptData.i = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get b
    //        if (nodeListB != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListB)
    //            {
    //                objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objRptData.b = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get s
    //        if (nodeListS != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListS)
    //            {
    //                objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objRptData.s = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get r
    //        if (nodeListR != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListR)
    //            {
    //                objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objRptData.r = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }


    //        //form the list and return
    //        foreach (Object obj in hshTblOfObjectFormed.Values)
    //        {
    //            objRptData = (rptData)obj;
    //            lstRptData.Add(objRptData);
    //        }
    //        return lstRptData;
    //    }

    //    List<Pie_ddl_Data> getListOfDropDownOrPieDataForSoapXML(XmlDocument document)
    //    {
    //        //Algorithm
    //        //get modified path of each element
    //        //find count of the node list.if throws and exption then thorw the exception that tag not found.
    //        //the one with the greatest count ,means that many object are to be formed.
    //        //if the tag is not part of an array,then the valie goes to all the objects.
    //        //if the tag path is empty then also empty is returned to all objects.

    //        int iCountOfLoop = 0;
    //        Pie_ddl_Data objDropDownData = null;
    //        Hashtable hshTblOfObjectFormed = new Hashtable();
    //        Hashtable hashOfTagsWithSingleValue = new Hashtable();
    //        List<Pie_ddl_Data> lstDropDownData = new List<Pie_ddl_Data>();

    //        int iLargestCountOfTags = 0;
    //        XmlNodeList nodeListT = null, nodeListV = null;
    //        bool blnIsPartOfAnArray;

    //        //get modified path of each element
    //        //find count of the node list.if throws and exption then thorw the exception that tag not found.
    //        string tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_cmdDetails.rt.t, out blnIsPartOfAnArray);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListT = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = nodeListT.Count;
    //                if (!blnIsPartOfAnArray)
    //                {
    //                    hashOfTagsWithSingleValue.Add("t", nodeListT[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
    //                    nodeListT = null;
    //                }
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("t", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_cmdDetails.rt.v, out blnIsPartOfAnArray);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListV = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListV.Count ? nodeListV.Count : iLargestCountOfTags;
    //                if (!blnIsPartOfAnArray)
    //                {
    //                    hashOfTagsWithSingleValue.Add("v", nodeListV[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
    //                    nodeListV = null;
    //                }
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("v", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }

    //        //the one with the greatest count ,means that many object are to be formed.
    //        for (int i = 0; i <= iLargestCountOfTags - 1; i++)
    //        {
    //            objDropDownData = new Pie_ddl_Data();
    //            //fill object with default vlue as string.Empty
    //            objDropDownData.t = "";
    //            objDropDownData.v = "";
    //            hshTblOfObjectFormed.Add(i, objDropDownData);
    //        }
    //        //if the tag is not part of an array,then the valie goes to all the objects.
    //        foreach (DictionaryEntry ddlObject in hshTblOfObjectFormed)
    //        {
    //            objDropDownData = (Pie_ddl_Data)ddlObject.Value;
    //            foreach (DictionaryEntry tag in hashOfTagsWithSingleValue)
    //            {
    //                if (tag.Key.ToString().ToLower() == "t")
    //                {
    //                    objDropDownData.t = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "v")
    //                {
    //                    objDropDownData.v = Convert.ToString(tag.Value);
    //                }
    //            }
    //        }
    //        //get t
    //        if (nodeListT != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListT)
    //            {
    //                objDropDownData = (Pie_ddl_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objDropDownData.t = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get v
    //        if (nodeListV != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListV)
    //            {
    //                objDropDownData = (Pie_ddl_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objDropDownData.v = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }

    //        //form the list and return
    //        foreach (Object obj in hshTblOfObjectFormed.Values)
    //        {
    //            objDropDownData = (Pie_ddl_Data)obj;
    //            lstDropDownData.Add(objDropDownData);
    //        }
    //        return lstDropDownData;
    //    }

    //    List<Bar_Line_Data> getListOfBarLineDataForSoapXML(XmlDocument document)
    //    {
    //        //Algorithm
    //        //get modified path of each element
    //        //find count of the node list.if throws and exption then thorw the exception that tag not found.
    //        //the one with the greatest count ,means that many object are to be formed.
    //        //if the tag is not part of an array,then the valie goes to all the objects.
    //        //if the tag path is empty then also empty is returned to all objects.

    //        int iCountOfLoop = 0;
    //        Bar_Line_Data objBarLineData = null;
    //        Hashtable hshTblOfObjectFormed = new Hashtable();
    //        Hashtable hashOfTagsWithSingleValue = new Hashtable();
    //        List<Bar_Line_Data> lstBarLineData = new List<Bar_Line_Data>();

    //        int iLargestCountOfTags = 0;
    //        XmlNodeList nodeListT = null, nodeListV1 = null, nodeListV2 = null, nodeListV3 = null, nodeListV4 = null, nodeListV5 = null;
    //        bool blnIsPartOfAnArray;

    //        //get modified path of each element
    //        //find count of the node list.if throws and exption then thorw the exception that tag not found.
    //        string tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_cmdDetails.rt.t, out blnIsPartOfAnArray);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListT = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = nodeListT.Count;
    //                if (!blnIsPartOfAnArray)
    //                {
    //                    hashOfTagsWithSingleValue.Add("t", nodeListT[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
    //                    nodeListT = null;
    //                }
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("t", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_cmdDetails.rt.v1, out blnIsPartOfAnArray);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListV1 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListV1.Count ? nodeListV1.Count : iLargestCountOfTags;
    //                if (!blnIsPartOfAnArray)
    //                {
    //                    hashOfTagsWithSingleValue.Add("v1", nodeListV1[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
    //                    nodeListV1 = null;
    //                }
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("v1", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_cmdDetails.rt.v2, out blnIsPartOfAnArray);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListV2 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListV2.Count ? nodeListV2.Count : iLargestCountOfTags;
    //                if (!blnIsPartOfAnArray)
    //                {
    //                    hashOfTagsWithSingleValue.Add("v2", nodeListV2[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
    //                    nodeListV2 = null;
    //                }
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("v2", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }

    //        tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_cmdDetails.rt.v3, out blnIsPartOfAnArray);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListV3 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListV3.Count ? nodeListV3.Count : iLargestCountOfTags;
    //                if (!blnIsPartOfAnArray)
    //                {
    //                    hashOfTagsWithSingleValue.Add("v3", nodeListV3[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
    //                    nodeListV3 = null;
    //                }
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("v3", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }

    //        tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_cmdDetails.rt.v4, out blnIsPartOfAnArray);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListV4 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListV4.Count ? nodeListV4.Count : iLargestCountOfTags;
    //                if (!blnIsPartOfAnArray)
    //                {
    //                    hashOfTagsWithSingleValue.Add("v4", nodeListV4[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
    //                    nodeListV4 = null;
    //                }
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("v4", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }

    //        tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_cmdDetails.rt.v5, out blnIsPartOfAnArray);
    //        try
    //        {
    //            if (!String.IsNullOrEmpty(tagPath))
    //            {
    //                nodeListV5 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
    //                iLargestCountOfTags = iLargestCountOfTags < nodeListV5.Count ? nodeListV5.Count : iLargestCountOfTags;
    //                if (!blnIsPartOfAnArray)
    //                {
    //                    hashOfTagsWithSingleValue.Add("v5", nodeListV5[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
    //                    nodeListV5 = null;
    //                }
    //            }
    //            else
    //            {
    //                hashOfTagsWithSingleValue.Add("v5", "");
    //            }
    //        }
    //        catch
    //        {
    //            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
    //        }
    //        //the one with the greatest count ,means that many object are to be formed.
    //        for (int i = 0; i <= iLargestCountOfTags - 1; i++)
    //        {
    //            objBarLineData = new Bar_Line_Data();
    //            //fill object with default vlue as string.Empty
    //            objBarLineData.t = "";
    //            objBarLineData.v1 = "";
    //            objBarLineData.v2 = "";
    //            objBarLineData.v3 = "";
    //            objBarLineData.v4 = "";
    //            objBarLineData.v5 = "";
    //            hshTblOfObjectFormed.Add(i, objBarLineData);
    //        }
    //        //if the tag is not part of an array,then the valie goes to all the objects.
    //        foreach (DictionaryEntry barLineObject in hshTblOfObjectFormed)
    //        {
    //            objBarLineData = (Bar_Line_Data)barLineObject.Value;
    //            foreach (DictionaryEntry tag in hashOfTagsWithSingleValue)
    //            {
    //                if (tag.Key.ToString().ToLower() == "t".ToLower())
    //                {
    //                    objBarLineData.t = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "v1".ToLower())
    //                {
    //                    objBarLineData.v1 = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "v2".ToLower())
    //                {
    //                    objBarLineData.v2 = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "v3".ToLower())
    //                {
    //                    objBarLineData.v3 = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "v4".ToLower())
    //                {
    //                    objBarLineData.v4 = Convert.ToString(tag.Value);
    //                }
    //                else if (tag.Key.ToString().ToLower() == "v5".ToLower())
    //                {
    //                    objBarLineData.v5 = Convert.ToString(tag.Value);
    //                }
    //            }
    //        }
    //        //get t
    //        if (nodeListT != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListT)
    //            {
    //                objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objBarLineData.t = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get v1
    //        if (nodeListV1 != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListV1)
    //            {
    //                objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objBarLineData.v1 = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get v2
    //        if (nodeListV2 != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListV2)
    //            {
    //                objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objBarLineData.v2 = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }
    //        //get v3
    //        if (nodeListV3 != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListV3)
    //            {
    //                objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objBarLineData.v3 = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }

    //        //get v4
    //        if (nodeListV4 != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListV4)
    //            {
    //                objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objBarLineData.v4 = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }

    //        //get v5
    //        if (nodeListV5 != null)
    //        {
    //            iCountOfLoop = 0;
    //            foreach (XmlNode node in nodeListV5)
    //            {
    //                objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
    //                objBarLineData.v5 = node.InnerText.Trim();
    //                iCountOfLoop++;
    //            }
    //        }

    //        //form the list and return
    //        foreach (Object obj in hshTblOfObjectFormed.Values)
    //        {
    //            objBarLineData = (Bar_Line_Data)obj;
    //            lstBarLineData.Add(objBarLineData);
    //        }
    //        return lstBarLineData;
    //    }

    //    string getModifiedPathOfTagForSoapAndXMLRPC(string originalTagPath, out bool isPartOfAnArray)
    //    {
    //        string strModifiedTagPath = "";
    //        isPartOfAnArray = false;
    //        string strLocalnamePrefix = "/*[local-name()='";
    //        string strLocalNameSuffix = "']";
    //        if (!String.IsNullOrEmpty(originalTagPath))
    //        {
    //            string[] arySplitTagPath = originalTagPath.Split('.');
    //            Hashtable hashOfTagsFromed = new Hashtable();
    //            for (int i = 0; i < arySplitTagPath.Length; i++)
    //            {
    //                if (arySplitTagPath[i].ToLower().StartsWith("arrayof"))//if its an array of then also we will get that tag after dot.so no need to process it here.But it shows that tag is an array.
    //                {
    //                    isPartOfAnArray = true;
    //                    continue;
    //                }
    //                if (hashOfTagsFromed.ContainsKey(arySplitTagPath[i]))
    //                {
    //                    continue;
    //                }
    //                if (strModifiedTagPath.Length == 0)
    //                {
    //                    strModifiedTagPath += "//*[local-name()='" + arySplitTagPath[i] + strLocalNameSuffix;
    //                }
    //                else
    //                {
    //                    strModifiedTagPath += strLocalnamePrefix + arySplitTagPath[i] + strLocalNameSuffix;
    //                }
    //                hashOfTagsFromed.Add(arySplitTagPath[i], arySplitTagPath[i]);
    //            }
    //        }
    //        return strModifiedTagPath;
    //    }
    //}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Data;

namespace mFicientWS
{
    public class GetListofUserRegistrationResp
    {
        ResponseStatus _respStatus;
      
        string _requestId;
        GetListMetaData _getmetadata;
        public GetListofUserRegistrationResp(ResponseStatus respStatus,GetListMetaData getlist , string requestId)
        {
            _respStatus = respStatus;
            _getmetadata = getlist;
            _requestId = requestId;
        }
        public string GetResponseJson()
        {
            GetListofUserRegistrationRespJson objgetRegistrationResp = new GetListofUserRegistrationRespJson();
            objgetRegistrationResp.func = Convert.ToString((int)FUNCTION_CODES.GET_LIST_OF_USERS_REGISTERED_DEVICE);
            objgetRegistrationResp.rid = _requestId;
            objgetRegistrationResp.status = _respStatus;
            objgetRegistrationResp.data = _getmetadata;
            string strJsonWithDetails = Utilities.SerializeJson<GetListofUserRegistrationRespJson>(objgetRegistrationResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
         [DataContract]
        public class GetListofUserRegistrationRespJson : CommonResponse
        {
             public GetListofUserRegistrationRespJson()
                      { }
              [DataMember]
             public GetListMetaData data { get; set; }
           
        }
        
    }
    [DataContract]
    public class Registeruserdevices
    {
        [DataMember]
        public string dunm { get; set; }
        [DataMember]
        public string uid { get; set; }
        [DataMember]
        public string did { get; set; }
        [DataMember]
        public string dtyp { get; set; }
        [DataMember]
        public string os { get; set; }
        [DataMember]
        public string mdl { get; set; }
        [DataMember]
        public string aver { get; set; }
        [DataMember]
        public string ron { get; set; }
    }
    [DataContract]
    public class Pendinguserdevices
    {
        [DataMember]
        public string dunm { get; set; }
        [DataMember]
        public string uid { get; set; }
        [DataMember]
        public string os { get; set; }
        [DataMember]
        public string mdl { get; set; }
        [DataMember]
        public string aver { get; set; }
        [DataMember]
        public string ron { get; set; }
    }
    [DataContract]
    public class GetListMetaData
    {
        [DataMember]
        public List<Registeruserdevices> reg { get; set; }
        [DataMember]
        public List<Pendinguserdevices> pnd { get; set; }
    }
}
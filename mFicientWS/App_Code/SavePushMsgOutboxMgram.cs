﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Data.SqlClient;
//using System.Data;

//namespace mFicientWS
//{
//    public class SavePushMsgOutboxMgram
//    {
//        string _enterpriseId, _deviceId, _deviceType, _userName, _devicePushMsgId;
//        int _statusCode;
//        string _statusDescription;
//        public SavePushMsgOutboxMgram(string companyId,
//            string deviceId, string deviceType,
//            string userName, string devcPushMsgId)
//        {
//            this.EnterpriseId = companyId;
//            this.DeviceId = deviceId;
//            this.DeviceType = deviceType;
//            this.UserName = userName;
//            this.DevicePushMsgId = devcPushMsgId;
//        }
//        public void Process()
//        {
//            try
//            {
//                saveDeviceDtls();
//            }
//            catch (Exception ex)
//            {
//                this.StatusCode = -1000;
//                if (ex.Message == (((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR)).ToString())
//                {
//                    this.StatusDescription = ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
//                }
//                this.StatusDescription = "Internal server error.";
//            }
//        }
//        void saveDeviceDtls()
//        {
//            string strConnectionString = MSSqlDatabaseClient.getConnectionStringFromWebConfig(
//                MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MGRAM);
//            string strQuery = getQuery();
//            SqlCommand cmd = new SqlCommand(strQuery);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@EnterpriseId", this.EnterpriseId);
//            cmd.Parameters.AddWithValue("@DeviceId", this.DeviceId);
//            cmd.Parameters.AddWithValue("@DeviceType", this.DeviceType);
//            cmd.Parameters.AddWithValue("@UserName", this.UserName);
//            cmd.Parameters.AddWithValue("@DevicePushMsgId", this.DevicePushMsgId);
//            int iRowsEffected = MSSqlDatabaseClient.ExecuteNonQueryRecord(cmd, strConnectionString);
//            if (iRowsEffected == 0) throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
//        }

//        string getQuery()
//        {
//            return @"IF EXISTS (SELECT USERNAME 
//                    FROM TBL_USER_DEVICE 
//                    WHERE ENTERPRISE_ID = @EnterpriseId
//                    AND DEVICE_ID = @DeviceId
//                    AND DEVICE_TYPE = @DeviceType
//                    AND USERNAME = @UserName)
//
//                    UPDATE TBL_USER_DEVICE
//                    SET DEVICE_PUSH_MESSAGE_ID = @DevicePushMsgId
//                    WHERE ENTERPRISE_ID =@EnterpriseID 
//                    AND DEVICE_ID = @DeviceId
//                    AND DEVICE_TYPE = @DeviceType
//                    AND USERNAME = @UserName
//
//                    ELSE
//
//                    INSERT INTO TBL_USER_DEVICE
//                    VALUES(@EnterpriseId,@DeviceId,@DeviceType,@UserName,@DevicePushMsgId);";

//        }
//        #region Public Properties
//        public string EnterpriseId
//        {
//            get { return _enterpriseId; }
//            set { _enterpriseId = value; }
//        }
//        public string DeviceId
//        {
//            get { return _deviceId; }
//            set { _deviceId = value; }
//        }
//        public string DeviceType
//        {
//            get { return _deviceType; }
//            set { _deviceType = value; }
//        }
//        public string UserName
//        {
//            get { return _userName; }
//            set { _userName = value; }
//        }
//        public int StatusCode
//        {
//            get { return _statusCode; }
//            set { _statusCode = value; }
//        }
//        public string StatusDescription
//        {
//            get { return _statusDescription; }
//            set { _statusDescription = value; }
//        }
//        public string DevicePushMsgId
//        {
//            get { return _devicePushMsgId; }
//            private set { _devicePushMsgId = value; }
//        }
//        #endregion
//    }
//}
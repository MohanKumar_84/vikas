﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class CommentOnQueryAndChangeStatusReq
    {
        string _requestId, _companyId, _userId, _tokenNo, _Comment;
        int _functionCode; bool _IsResolve;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string TokenNo
        {
            get { return _tokenNo; }
        }
        public string Comment
        {
            get { return _Comment; }
        }
        public bool IsResolve
        {
            get { return _IsResolve; }
        }

        public CommentOnQueryAndChangeStatusReq(string requestJson, string userId)
        {
            RequestJsonParsing<CommentOnQueryAndChangeStatusReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<CommentOnQueryAndChangeStatusReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.COMMENT_AND_UPDATE_QUERY)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.data.enid;
            _userId = userId;
            _tokenNo = objRequestJsonParsing.req.data.tn;
            _Comment = objRequestJsonParsing.req.data.cmt;
            if (_Comment==null)
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
            _Comment = _Comment.Trim();
            _IsResolve = objRequestJsonParsing.req.data.irlv == "1" ? true : false;
            if (_Comment == string.Empty && !_IsResolve)
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
            //if(String .IsNullOrEmpty(_Comment) )
            //    _IsResolve = objRequestJsonParsing.req.data.irlv == "1" ? true : false;
        }
    }

    [DataContract]
    public class CommentOnQueryAndChangeStatusReqData : Data
    {
        /// <summary>
        /// Query token no.
        /// </summary>
        [DataMember]
        public string tn { get; set; }

        /// <summary>
        /// Comment
        /// </summary>
        [DataMember]
        public string cmt { get; set; }

        /// <summary>
        /// is Resolve
        /// </summary>
        [DataMember]
        public string irlv { get; set; }
    }
}
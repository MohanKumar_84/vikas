﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Net;

namespace mFicientWS
{
    public class ConfirmMobUpdation
    {
        ConfirmMobUpdationReq _ConfirmMobUpdationReq;
        public ConfirmMobUpdation(ConfirmMobUpdationReq ConfirmMobUpdationReq)
        {
            _ConfirmMobUpdationReq = ConfirmMobUpdationReq;
        }
        public ConfirmMobUpdationResp Process()
        {
            ResponseStatus objResponseStatus = new ResponseStatus();
            try
            {
                string query = "DELETE FROM TBL_UPDATION_REQUIRED_DETAIL WHERE USER_ID=@USER_ID and DEVICE_ID=@DEVICE_ID and DEVICE_TYPE=@DEVICE_TYPE and COMPANY_ID=@COMPANY_ID";
                if (_ConfirmMobUpdationReq.UpadationType != (int)MOB_UPDATE_TYPE.All)
                {
                    query = query + " and UPDATION_TYPE=@UPDATION_TYPE";
                }

                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@USER_ID", _ConfirmMobUpdationReq.UserId);
                cmd.Parameters.AddWithValue("@DEVICE_ID", _ConfirmMobUpdationReq.DeviceId);
                cmd.Parameters.AddWithValue("@DEVICE_TYPE", _ConfirmMobUpdationReq.DeviceType);
                cmd.Parameters.AddWithValue("@COMPANY_ID", _ConfirmMobUpdationReq.CompanyId);

                if (_ConfirmMobUpdationReq.UpadationType != (int)MOB_UPDATE_TYPE.All)
                {
                    cmd.Parameters.AddWithValue("@UPDATION_TYPE", _ConfirmMobUpdationReq.UpadationType);
                }
                objResponseStatus.cd = "0";
                objResponseStatus.desc = "";
                MSSqlDatabaseClient.ExecuteNonQueryRecord(cmd);
            }
            catch (Exception ex)
            {
                if (ex.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            return new ConfirmMobUpdationResp(objResponseStatus,_ConfirmMobUpdationReq.RequestId);
        }
    }
}
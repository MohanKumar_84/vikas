﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Runtime.Serialization;

//namespace mFicientWS
//{
//    public class FileUploadReq
//    {
//        string _s3url, _sessionId, _requestId, _deviceId, _deviceType, _companyId, _destUrl, _mPluginAgentName;
//        int _functionCode;
//        string _userName;

//        public string UserName
//        {
//            get { return _userName; }
//        }
//        public string CompanyId
//        {
//            get { return _companyId; }
//        }
//        public string DeviceType
//        {
//            get { return _deviceType; }
//        }
//        public string DeviceId
//        {
//            get { return _deviceId; }
//        }
//        public string RequestId
//        {
//            get { return _requestId; }
//        }
//        public string SessionId
//        {
//            get { return _sessionId; }
//        }
//        public string S3Url
//        {
//            get { return _s3url; }
//        }
//        public string DestinationUrl
//        {
//            get { return _destUrl; }
//        }
//        public string MpluginAgentName
//        {
//            get { return _mPluginAgentName; }
//        }
//        public int FunctionCode
//        {
//            get { return _functionCode; }
//        }

//        public FileUploadReq(string requestJson)
//        {
//            RequestJsonParsing<FileUploadReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<FileUploadReqData>>(requestJson);
//            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
//            if (_functionCode != (int)FUNCTION_CODES.CONVERT_LOCAL_TO_PUBLIC_IMAGEURL)
//            {
//                throw new Exception("Invalid Function Code");
//            }
//            _sessionId = objRequestJsonParsing.req.sid;
//            _requestId = objRequestJsonParsing.req.rid;
//            _deviceId = objRequestJsonParsing.req.did;
//            _deviceType = objRequestJsonParsing.req.dtyp;
//            _companyId = objRequestJsonParsing.req.data.enid;
//            _userName = objRequestJsonParsing.req.data.unm;
//            _s3url = objRequestJsonParsing.req.data.s3url;
//            _destUrl = objRequestJsonParsing.req.data.durl;
//            _mPluginAgentName = objRequestJsonParsing.req.data.mpnm;
//        }
//    }

//    [DataContract]
//    public class FileUploadReqData : Data
//    {
//        /// <summary>
//        /// S3 Bucket Url
//        /// </summary>
//        [DataMember]
//        public string s3url { get; set; }

//        /// <summary>
//        /// Destination Path
//        /// </summary>
//        [DataMember]
//        public string durl { get; set; }

//        /// <summary>
//        /// mPlugin Agent Name
//        /// </summary>
//        [DataMember]
//        public string mpnm { get; set; }
//    }
//}
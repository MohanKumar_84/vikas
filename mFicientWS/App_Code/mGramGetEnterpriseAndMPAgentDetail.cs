﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientWS
{
    public class mGramGetEnterpriseAndMPAgentDetail
    {
        mGramGetEnterpriseAndMPAgentDetailReq _mGramGetEnterpriseAndMPAgentDetailReq;

        public mGramGetEnterpriseAndMPAgentDetail(mGramGetEnterpriseAndMPAgentDetailReq mGramGetEnterpriseAndMPAgentDetailReq)
        {
            _mGramGetEnterpriseAndMPAgentDetailReq = mGramGetEnterpriseAndMPAgentDetailReq;
        }

        public mGramGetEnterpriseAndMPAgentDetailResp Process()
        {
            DataSet dsCompanyDtls = getmGramCompanyDetail();
            if (dsCompanyDtls != null && dsCompanyDtls.Tables[0].Rows.Count > 0)
            {
                ResponseStatus objRespStatus = new ResponseStatus();
                objRespStatus.cd = "0";
                objRespStatus.desc = "";

                mGramEnterpriseAndMPAgentDetailRespRespData objmGramEnterpriseDetailRespData = new mGramEnterpriseAndMPAgentDetailRespRespData();
                objmGramEnterpriseDetailRespData.eid = Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["COMPANY_ID"]);
                objmGramEnterpriseDetailRespData.connm = Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["CONNECTION_NAME"]);
                objmGramEnterpriseDetailRespData.dbnm = Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["DATABASE_NAME"]);
                objmGramEnterpriseDetailRespData.host = Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["HOST_NAME"]);
                objmGramEnterpriseDetailRespData.unm = Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["USER_NAME"]);
                objmGramEnterpriseDetailRespData.pwd = Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["PASSWORD"]);
                objmGramEnterpriseDetailRespData.tblnm = Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["TABLE_NAME"]);

                if (!string.IsNullOrEmpty(Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["MP_AGENT_NAME"])))
                    objmGramEnterpriseDetailRespData.mpnm = Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["MP_AGENT_NAME"]);
                else
                    objmGramEnterpriseDetailRespData.mpnm = string.Empty;

                if (!string.IsNullOrEmpty(Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["MP_AGENT_PASSWORD"])))
                    objmGramEnterpriseDetailRespData.mpwd = Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["MP_AGENT_PASSWORD"]);
                else
                    objmGramEnterpriseDetailRespData.mpwd = string.Empty;

                objmGramEnterpriseDetailRespData.dylmt = Convert.ToInt32(dsCompanyDtls.Tables[0].Rows[0]["PUSHMESSAGE_PERDAY"].ToString());
                objmGramEnterpriseDetailRespData.mnlmt = Convert.ToInt32(dsCompanyDtls.Tables[0].Rows[0]["PUSHMESSAGE_PERMONTH"].ToString());
                objmGramEnterpriseDetailRespData.dbtyp = Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["DATABASE_TYPE"]);
                objmGramEnterpriseDetailRespData.adstrg = Convert.ToString(dsCompanyDtls.Tables[0].Rows[0]["ADDITIONAL"]);

                return new mGramGetEnterpriseAndMPAgentDetailResp(objRespStatus, objmGramEnterpriseDetailRespData, _mGramGetEnterpriseAndMPAgentDetailReq.RequestId);

            }
            throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
        }

        DataSet getmGramCompanyDetail()
        {
            string strQuery = @"Select a.*, mpAgent.*,compPlan.PUSHMESSAGE_PERDAY, compPlan.PUSHMESSAGE_PERMONTH from TBL_PULL_DATA_SETTINGS as a
                        LEFT JOIN TBL_MPLUGIN_AGENT_DETAIL AS mpAgent ON mpAgent.MP_AGENT_NAME = a.MPLUGIN_AGENT
                        INNER JOIN TBL_COMPANY_CURRENT_PLAN AS compPlan ON compPlan.COMPANY_ID = a.COMPANY_ID 
                        WHERE a.COMPANY_ID = @CompanyId";

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", _mGramGetEnterpriseAndMPAgentDetailReq.CompanyId);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }
    }
}
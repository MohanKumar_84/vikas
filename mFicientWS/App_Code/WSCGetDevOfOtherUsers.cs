﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientWS
{
    public class WSCGetDevOfOtherUsers
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId">User we have to exclude from search.</param>
        /// <param name="companyId"></param>
        public WSCGetDevOfOtherUsers(
            string userId,
            string companyId)
        {
            this.UserId = userId;
            this.CompanyId = companyId;
        }

        public List<MFEDevice> getRegisteredDevices()
        {
            List<MFEDevice> lstRegisteredDevices = new List<MFEDevice>();
            //try
            //{
            //    StatusCode = -1000;
            //    DataSet dsRegisteredDevice = this.getRegisteredDevicesDS();
            //    if (dsRegisteredDevice != null)
            //    {
            //        if (dsRegisteredDevice.Tables[0] != null)
            //        {
            //            //lstRegisteredDevices =
            //            //    getRegisteredDeviceFromDTbl(dsRegisteredDevice.Tables[0]);
            //        }
            //        StatusCode = 0;
            //        StatusDescription = "";
            //    }
            //    else
            //    {
            //        throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            //    }
            //}
            //catch (Exception ex)
            //{
            //    StatusCode = -1000;
            //    this.StatusDescription = "Internal server error.";
            //}
            return lstRegisteredDevices;
        }

        //DataSet getRegisteredDevicesDS()
        //{
        //    //string strQuery = getSqlQueryByUserId();
        //    //SqlCommand cmd = getCommandToRun(strQuery);
        //    //DataSet objDataSet = MSSqlClient.SelectDataFromSqlCommand(cmd);
        //    //return objDataSet;
        //}
        string getSqlQueryByUserIdAndDevDetl()
        {
            return @" SELECT device.*,usr.USER_ID,usr.USER_NAME,(usr.FIRST_NAME +' '+usr.LAST_NAME)AS FULL_NAME 
                      FROM TBL_REGISTERED_DEVICE device
                      INNER JOIN TBL_USER_DETAIL as usr
                      ON usr.USER_ID != device.USER_ID
                      WHERE device.COMPANY_ID =@CompanyId
                      AND usr.USER_ID = @UserID
                      AND device.DEVICE_TYPE = @DeviceType
                     AND device.DEVICE_ID = @DeviceId
                     AND device.DEVICE_MODEL =@DeviceModel
                     AND device.DEVICE_SIZE = @DEVICE_SIZE;";
        }
        #region Public Properties
        //public DataSet ResultTables
        //{
        //    private set;
        //     get;
        //}
        public string UserId
        {
            get;
            set;
        }
        public int StatusCode
        {
            private set;
            get;
        }
        public string StatusDescription
        {
            private set;
            get;
        }
        public string SubAdminId
        {
            private set;
            get;
        }
        public string AdminId
        {
            private set;
            get;
        }
        public string CompanyId
        {
            private set;
            get;
        }

        #endregion
    }
}
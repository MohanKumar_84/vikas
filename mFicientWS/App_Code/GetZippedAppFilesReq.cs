﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class GetZippedAppFilesReq
    {
        string _email, _userId, _sessionId, _requestId, _deviceId, _deviceType, _companyId, _appId, _uiVersion, _modelType;
        int _functionCode;

        public string ModelType
        {
            get { return _modelType; }
        }
        public string UiVersion
        {
            get { return _uiVersion; }
        }
        public string AppId
        {
            get { return _appId; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string SessionId
        {
            get { return _sessionId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string Email
        {
            get { return _email; }
        }

        public GetZippedAppFilesReq(string requestJson, string userId)
        {
            RequestJsonParsing<GetZippedAppFilesReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<GetZippedAppFilesReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.GET_APP_FILES_AS_ZIPPED)
            {
                throw new Exception("Invalid Function Code");
            }
            _email = objRequestJsonParsing.req.data.em;
            _userId = userId;
            _sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _appId = objRequestJsonParsing.req.data.appid;
            _uiVersion = objRequestJsonParsing.req.data.uiver;
            _modelType = objRequestJsonParsing.req.data.mtyp == null ? "0" : objRequestJsonParsing.req.data.mtyp;
            if (String.IsNullOrEmpty(_appId) || String.IsNullOrEmpty(_uiVersion))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
            try
            {
                DEVICE_MODEL_TYPE eModelType = (DEVICE_MODEL_TYPE)Enum.Parse(typeof(DEVICE_MODEL_TYPE), _modelType);
                if (!Enum.IsDefined(typeof(DEVICE_MODEL_TYPE), eModelType))
                {
                    throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
                }
            }
            catch
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
        }
    }
    [DataContract]
    public class GetZippedAppFilesReqData : Data
    {
        /// <summary>
        /// app id
        /// </summary>
        [DataMember]
        public string appid { get; set; }

        /// <summary>
        /// UI Version
        /// </summary>
        [DataMember]
        public string uiver { get; set; }

        /// <summary>
        /// Model Type
        /// </summary>
        [DataMember]
        public string mtyp { get; set; }

    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    
    public class ConfirmMobUpdationReq
    {
        string _userId, _sessionId, _requestId, _deviceId, _deviceType, _companyId;
        int _functionCode, _UpadationType;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string SessionId
        {
            get { return _sessionId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public int UpadationType
        {
            get { return _UpadationType; }
        }

        public ConfirmMobUpdationReq(string requestJson, string userId)
        {
            RequestJsonParsing<ConfirmMobUpdationReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<ConfirmMobUpdationReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.UPDATION_COMPLETE)
            {
                throw new Exception("Invalid Function Code");
            }
            _UpadationType = int.Parse(objRequestJsonParsing.req.data.utyp);
            _userId = userId;
            _sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            if (_UpadationType<=0)
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
        }
        

        [DataContract]
        public class ConfirmMobUpdationReqData : Data
        {
            /// <summary>
            /// updation Type
            /// </summary>
            [DataMember]
            public string utyp { get; set; }

        }
    }
}
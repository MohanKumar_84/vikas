﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Newtonsoft;
using Newtonsoft.Json.Linq;

namespace mFicientWS
{
    public class Get_Offline_Data_Table_Meta
    {
        Get_Offline_Datatable_Meta_Req _offlinereq;
        public Get_Offline_Data_Table_Meta(Get_Offline_Datatable_Meta_Req offlinereq)
        {
            _offlinereq = offlinereq;
        }

        public string Process()
        {
            JObject Resp = new JObject();
            Resp.Add("func", Convert.ToString((int)FUNCTION_CODES.GET_OFFLINE_DATA_TABLE_META));
            Resp.Add("rid", _offlinereq.RequestId);

            Resp.Add("", "");

            JArray offtblsjson = new JArray();
            string cd = "0", desc = "";
            foreach (string offtbl in _offlinereq.Tableid)
            {
                DataSet dsofflinetblDtls = getOfflineListByCompanyAndTblId(offtbl);
                if (dsofflinetblDtls.Tables.Count != 0 && dsofflinetblDtls.Tables[0].Rows.Count > 0)
                {
                    if (dsofflinetblDtls.Tables[0].Rows.Count != 0)
                    {

                        if (dsofflinetblDtls.Tables[0].Rows.Count != 0)
                        {
                            JObject offtbljson = new JObject();
                            foreach (DataRow row in dsofflinetblDtls.Tables[0].Rows)
                            {
                                JObject tbl = JObject.Parse(JObject.Parse(Convert.ToString(row["TABLE_JSON"]))["tbl"].ToString());

                                offtbljson.Add("nm", tbl["nm"].ToString());
                                offtbljson.Add("id", tbl["id"].ToString());
                                offtbljson.Add("type", tbl["typ"].ToString());
                                offtbljson.Add("sync",  tbl["sync"].ToString());
                                offtbljson.Add("prmt", tbl["prmt"].ToString());

                                JObject sync = (JObject)JObject.Parse(Convert.ToString(row["SYNC_JSON"]))["sync"];
                                foreach (DataRow row1 in dsofflinetblDtls.Tables[1].Rows)
                                {
                                    if (sync["conVal"].ToString() == row1["db_connector_id"].ToString())
                                    {
                                        offtbljson.Add("cr", Convert.ToString(row1["credential_property"]));
                                        break;
                                    }
                                }
                                var JSONupload = sync["upload"];
                                if (JSONupload != null && JSONupload.ToString().Length > 0)
                                {
                                    JObject upload = JObject.Parse(JSONupload.ToString());
                                    JObject upJson = new JObject();
                                    upJson.Add("seq", tbl["seq"].ToString());
                                    upJson.Add("nor", tbl["nor"].ToString());
                                    upJson.Add("rem", upload["deleteRecordAfterUpload"].ToString());
                                    upJson.Add("newlp", JArray.Parse(JObject.Parse(upload["newRecord"].ToString())["params"].ToString()));
                                    if (upload["modifiedSameAsNewRecord"].ToString() == "1")
                                        upJson.Add("uplp", new JArray());
                                    else
                                        upJson.Add("uplp", JArray.Parse(JObject.Parse(upload["modifiedRecord"].ToString())["params"].ToString()));
                                    //--------------------------------------------------------
                                    var uploadCriteria = upload["uploadCriteria"];
                                    if (uploadCriteria != null)
                                        upJson.Add("uwhr", uploadCriteria.ToString());
                                    else upJson.Add("uwhr", "");
                                    var uploadWithIn = upload["uploadWithIn"];
                                    if (uploadWithIn != null)//in days
                                    {
                                        int daysInhours = Convert.ToInt32(uploadWithIn);
                                        upJson.Add("uhrs", daysInhours * 24);
                                    }
                                    else upJson.Add("uhrs", "84");
                                    offtbljson.Add("up", upJson);
                                }
                                
                                var JSONdownload = sync["download"];
                                JArray imageCols = new JArray();
                                JArray linkCols = new JArray();
                                if (JSONdownload != null && JSONdownload.ToString().Length > 0)
                                {
                                    string mplugin = string.Empty;
                                    string strCmdId = JObject.Parse(sync["download"].ToString())["command"].ToString();
                                    if (Convert.ToInt32(sync["cmdType"].ToString()) == (int)RequestCommandType.Database)
                                    {
                                        DataBaseCommand db = new DataBaseCommand(strCmdId, _offlinereq.CompanyId, "", "");
                                        if (!string.IsNullOrEmpty(db.ImageColumn)) imageCols = JArray.Parse(db.ImageColumn);
                                        mplugin = db.MPluginAgent;
                                    }                                    
                                    JObject download = JObject.Parse(JSONdownload.ToString());
                                    JObject dwJson = new JObject();
                                    linkCols=  JArray.Parse(download["outputParam"].ToString());
                                    dwJson.Add("cln", download["cleanRecord"].ToString());
                                    dwJson.Add("mpl", mplugin);
                                    dwJson.Add("disa", download["disallowAfterExpiry"].ToString());
                                    JObject expiry = JObject.Parse(download["expiry"].ToString());
                                    dwJson.Add("epr", expiry["type"].ToString());
                                    dwJson.Add("efr", expiry["frequency"].ToString());
                                    dwJson.Add("econ", expiry["condition"].ToString());
                                    dwJson.Add("lp", JArray.Parse(download["inputParam"].ToString()));
                                    offtbljson.Add("down", dwJson);
                                }

                                JArray jaCols = JArray.Parse(tbl["col"].ToString());

                                foreach (JArray imgCol in imageCols)
                                {
                                    foreach (JObject lnkCol in linkCols)
                                        if (lnkCol["para"].ToString().ToLower() == imgCol[0].ToString().ToLower())
                                        {
                                            foreach (JObject jooffCol in jaCols)
                                                if (lnkCol["val"].ToString().Trim().Length > 2 && lnkCol["val"].ToString().Substring(2).ToLower() == jooffCol["cnm"].ToString().ToLower())
                                                {
                                                    jooffCol["ftyp"] = Utilities.convertColumnImagetype(Convert.ToInt32(imgCol[1].ToString())).ToString(); 
                                                    break;
                                                }
                                            break;
                                        }
                                }

                                offtbljson.Add("col", jaCols);


                                offtblsjson.Add(offtbljson);
                            }
                        }
                    }
                }
            }
            JObject status = new JObject();
            status.Add("cd", cd);
            status.Add("desc", desc);
            Resp.Add("status", status);
            JObject data = new JObject();
            data.Add("offtbl", offtblsjson);
            Resp.Add("data", data);
            JObject finalJSON = new JObject();
            finalJSON.Add("resp", Resp);
            return finalJSON.ToString();
        }

        DataSet getOfflineListByCompanyAndTblId(string tblID)
        {
            string strQuery = @" Select * from  dbo.TBL_OFFLINE_DATA_TABLE  where  COMPANY_ID=@CompanyId and TABLE_ID =@tblid;
             SELECT db_connector_id,credential_property FROM  dbo.TBL_DATABASE_CONNECTION  WHERE COMPANY_ID=@CompanyId;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", _offlinereq.CompanyId);
            cmd.Parameters.AddWithValue("@tblid", tblID);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }
    }
}

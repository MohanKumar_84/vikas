﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Ionic.Zip;
using System.Text;
using System.IO;
using System.Net;
namespace mFicientWS
{
    public class EnterpriseLogoZipper
    {
        EnterpriseLogoReq _getZippedImagesReq;
        HttpContext _appContext;
        internal EnterpriseLogoZipper(EnterpriseLogoReq getZippedImagesReq, HttpContext appContext)
        {
            _getZippedImagesReq = getZippedImagesReq;
            _appContext = appContext;
        }

        internal ZipFile ZippedUIFiles()
        {
            try
            {
                CompanyDetail objCmpDtls = new CompanyDetail(_getZippedImagesReq.CompanyId);
                objCmpDtls.Process();
                if (objCmpDtls.StatusCode == 0 &&
                   ! String.IsNullOrEmpty(objCmpDtls.LogoImageName) )
                {
                    string strImageName = objCmpDtls.LogoImageName;
                    ZipFile zip = new ZipFile();
                    string strUrl = MficientConstants.CompanyImagesUrl + _getZippedImagesReq.CompanyId + @"/" + "logo";
                    using (zip)
                    {
                        try
                        {
                            zip.AddEntry(Convert.ToString(strImageName.Trim()), downloadRemoteImageFile(strUrl + @"/" + strImageName));
                        }
                        catch
                        { }
                    }
                    if (zip.Count == 0)
                    {
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                    }
                    _appContext.Items.Add("ZIP_FILE_NAME", "ENTERPRISELOGO");

                    return zip;
                }
                else
                {
                    //throw new Exception("Company Logo Name not found");
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private Stream downloadRemoteImageFile(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception)
            {
                throw new Exception();
            }

            // Check that the remote file was found. The ContentType
            // check is performed since a request for a non-existent
            // image file might be redirected to a 404-page, which would
            // yield the StatusCode "OK", even though the image was not
            // found.
            if ((response.StatusCode == HttpStatusCode.OK ||
                response.StatusCode == HttpStatusCode.Moved ||
                response.StatusCode == HttpStatusCode.Redirect) &&
                response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
            {
                return response.GetResponseStream();

            }
            else
                return null;
        }
    }
}
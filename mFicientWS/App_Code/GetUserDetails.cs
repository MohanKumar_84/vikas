﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class GetUserDetails
    {
        GetUserDetailsReq _getuserlist;


        public GetUserDetails(GetUserDetailsReq getlistuserdetails)
        {
            _getuserlist = getlistuserdetails;
        }

        public GetUserDetailResp Process()
        {
            ResponseStatus objRespStatus = new ResponseStatus();
                   objRespStatus.cd = "0";
                   objRespStatus.desc = "";
            UserDetail obj=new UserDetail(_getuserlist.MobileUserId,_getuserlist.CompanyId,_getuserlist.RequestId);
            return new GetUserDetailResp(objRespStatus, obj.GetUserDetail(), _getuserlist.RequestId);
        }
     


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientWS
{
    public class UserDeviceDetails
    {
        string _userId, _deviceId, _deviceType, _companyId, _deviceModel, _mficientKey, _osVersion, _deviceSize;
        DataTable _deviceDetails;

        public UserDeviceDetails(string userId, string companyId, string deviceId, string deviceType)
        {
            _userId = userId;
            _deviceId = deviceId;
            _deviceType = deviceType;
            _companyId = companyId;
        }

        public void Process()
        {
            string strQuery = @"SELECT * FROM TBL_REGISTERED_DEVICE
                                WHERE DEVICE_ID = @DEVICE_ID
                                AND DEVICE_TYPE = @DeviceType
                                AND USER_ID= @UserId
                                AND COMPANY_ID = @CompanyId;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@DEVICE_ID", this.DeviceId);
            cmd.Parameters.AddWithValue("@DeviceType", this.DeviceType);
            cmd.Parameters.AddWithValue("@UserId", this.UserId);
            cmd.Parameters.AddWithValue("@CompanyId", this.CompanyId);
            DataSet dsDeviceDetails = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (dsDeviceDetails != null && dsDeviceDetails.Tables[0] != null && dsDeviceDetails.Tables[0].Rows.Count > 0)
            {
                _deviceDetails = dsDeviceDetails.Tables[0];
                _deviceModel = Convert.ToString(_deviceDetails.Rows[0]["DEVICE_MODEL"]);
                _osVersion = Convert.ToString(_deviceDetails.Rows[0]["OS_VERSION"]);
                _mficientKey = Convert.ToString(_deviceDetails.Rows[0]["MFICIENT_KEY"]);
                _deviceSize = Convert.ToString(_deviceDetails.Rows[0]["DEVICE_SIZE"]);
            }

        }


        public string DeviceType
        {
            get { return _deviceType; }
        }

        public string DeviceId
        {
            get { return _deviceId; }
        }

        public string UserId
        {
            get { return _userId; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public DataTable DeviceDetails
        {
            get { return _deviceDetails; }
        }

        public string DeviceModel
        {
            get { return _deviceModel; }
        }

        public string OsVersion
        {
            get { return _osVersion; }
        }

        public string MficientKey
        {
            get { return _mficientKey; }
        }
        public string DeviceSize
        {
            get { return _deviceSize; }
        }
    }
}
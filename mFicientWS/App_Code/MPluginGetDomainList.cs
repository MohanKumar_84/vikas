﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class MPluginGetDomainList
    {
        MPluginGetDomainListReq _mPluginGetDomainListReq;

        public MPluginGetDomainList(MPluginGetDomainListReq getDomainListReq)
        {
            _mPluginGetDomainListReq = getDomainListReq;
        }

        public MPluginGetDomainListResp Process()
        {
            if (String.IsNullOrEmpty(_mPluginGetDomainListReq.RequestId)
                || String.IsNullOrEmpty(_mPluginGetDomainListReq.CompanyId)
                || String.IsNullOrEmpty(_mPluginGetDomainListReq.AgentName)
                || String.IsNullOrEmpty(_mPluginGetDomainListReq.AgentPassword))
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
            List<string> objDsnFields = new List<string>();
            try
            {
                List<string> lstDSN = mPlugin.GetDomainList(_mPluginGetDomainListReq.CompanyId,
                    _mPluginGetDomainListReq.AgentName, _mPluginGetDomainListReq.AgentPassword);
                objDsnFields = lstDSN;
                return new MPluginGetDomainListResp(objDsnFields, "0", "");
            }
            catch (mPlugin.mPluginException e)
            {
                objDsnFields = new List<string>();
                return new MPluginGetDomainListResp(objDsnFields, "99945", e.Message);
            }
            catch (HttpException e)
            {
                throw (new Exception(Convert.ToString(e.GetHttpCode())));
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
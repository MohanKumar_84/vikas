﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class MPGetWSDLInfoResp
    {
        string _data,_statusCode,_description;
        public MPGetWSDLInfoResp(string data, string statusCode, string description)
        {
            try
            {
                _data = data;
                _statusCode = statusCode;
                _description = description;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating MPlugInGetMetaDataResp Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
           //string str= "{resp:{\"cd\":\"" + _statusCode + "\",\"desc\":\"" + _description + "\",\"data\":\"" + _data + "\"}}";
           //return str;

            MPGetWSDLInformationResponse objGetWSDLInfoResp = new MPGetWSDLInformationResponse();
            objGetWSDLInfoResp.cd = _statusCode;
            objGetWSDLInfoResp.desc = _description;
            objGetWSDLInfoResp.data = _data;
            string strJsonWithDetails = Utilities.SerializeJson<MPGetWSDLInformationResponse>(objGetWSDLInfoResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class MPGetWSDLInformationResponse 
    {
        
        [DataMember]
        public string cd { get; set; }

        [DataMember]
        public string desc { get; set; }

        [DataMember]
        public string data { get; set; }
    }
}
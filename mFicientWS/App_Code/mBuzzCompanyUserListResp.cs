﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class mBuzzCompanyUserListResp
    {
        ResponseStatus respStatus;
        List<Users> lstUsersDtls;
        string requestId;

        public mBuzzCompanyUserListResp(ResponseStatus _respStatus, List<Users> _lstUsersDtls, string _requestId)
        {
            try
            {
                respStatus = _respStatus;
                lstUsersDtls = _lstUsersDtls;
                requestId = _requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting Company Users List Detail Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            mBuzzCompanyUsersListResponse objmBuzzCompanyUsersListResp = new mBuzzCompanyUsersListResponse();
            objmBuzzCompanyUsersListResp.func = Convert.ToString((int)FUNCTION_CODES.MBUZZ_COMPANY_USER_LIST);
            objmBuzzCompanyUsersListResp.rid = requestId;
            objmBuzzCompanyUsersListResp.status = respStatus;

            mBuzzCompanyUsersListResponseData objmBuzzCompanyUsersListRespData = new mBuzzCompanyUsersListResponseData();
            objmBuzzCompanyUsersListRespData.users = lstUsersDtls;
            objmBuzzCompanyUsersListResp.data = objmBuzzCompanyUsersListRespData;
            string strJsonWithDetails = Utilities.SerializeJson<mBuzzCompanyUsersListResponse>(objmBuzzCompanyUsersListResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }
    public class mBuzzCompanyUsersListResponse : CommonResponse
    {
        public mBuzzCompanyUsersListResponse()
        { }

        [DataMember]
        public mBuzzCompanyUsersListResponseData data { get; set; }
    }

    public class mBuzzCompanyUsersListResponseData
    {
        /// <summary>
        /// Users Data
        /// </summary>
        [DataMember]
        public List<Users> users { get; set; }
    }
}
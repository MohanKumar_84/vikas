﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Data;
//using System.Data.SqlClient;
//namespace mFicientWS
//{
//    public class AddNewCompanyOnServer
//    {
//        AddNewCompanyOnServerReq _addNewCmpOnServerReq;
//        public AddNewCompanyOnServer(AddNewCompanyOnServerReq addNewCmpOnServerReq)
//        {
//            _addNewCmpOnServerReq = addNewCmpOnServerReq;
//        }

//        public AddNewCompanyOnServerResp Process()
//        {
//            SqlTransaction transaction = null;
//            SqlConnection con;
//            MSSqlClient.SqlConnectionOpen(out con);
//            ResponseStatus objRespStatus = new ResponseStatus();
//            try
//            {
//                using (con)
//                {
//                    using (transaction = con.BeginTransaction())
//                    {
//                        insertCompanyAdminDtls(con, transaction);
//                        insertCompanyCurrentPlanDtls(con, transaction);
//                        insertCompanyDtls(con, transaction);
//                        transaction.Commit();
//                        //make the response status
//                        objRespStatus.cd = "0";
//                        objRespStatus.desc = "";
//                    }
//                }

//            }
//            catch (SqlException e)
//            {
//                //transaction.Rollback();
//                if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
//                {
//                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
//                }
//                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
//            }
//            catch (Exception e)
//            {
//                //transaction.Rollback();
//                if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
//                {
//                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
//                }
//                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
//            }
//            finally
//            {
//                if (con != null)
//                {
//                    con.Dispose();
//                }
//                if (transaction != null)
//                {
//                    transaction.Dispose();
//                }
//            }

//            return new AddNewCompanyOnServerResp(objRespStatus,_addNewCmpOnServerReq.RequestId);

//        }




//        void insertCompanyAdminDtls(SqlConnection sqlConnection, SqlTransaction transaction)
//        {
//            string strQuery = @"INSERT INTO [TBL_COMPANY_ADMINISTRATOR]
//                               ([ADMIN_ID],[EMAIL_ID],[ACCESS_CODE]
//                               ,[FIRST_NAME],[LAST_NAME],[STREET_ADDRESS1]
//                               ,[STREET_ADDRESS2],[CITY_NAME],[STATE_NAME]
//                               ,[COUNTRY_CODE],[ZIP],[DATE_OF_BIRTH]
//                               ,[REGISTRATION_DATETIME],[GENDER],[MIDDLE_NAME]
//                               ,[STREET_ADDRESS3],[MOBILE],[IS_TRIAL],COMPANY_ID)
//                         VALUES
//                               (@ADMIN_ID,@EMAIL_ID,@ACCESS_CODE
//                               ,@FIRST_NAME,@LAST_NAME,@STREET_ADDRESS1
//                               ,@STREET_ADDRESS2,@CITY_NAME,@STATE_NAME
//                               ,@COUNTRY_CODE,@ZIP,@DATE_OF_BIRTH
//                               ,@REGISTRATION_DATETIME,@GENDER,@MIDDLE_NAME
//                               ,@STREET_ADDRESS3,@MOBILE,@IS_TRIAL,@COMPANY_ID)";

//            SqlCommand cmd = new SqlCommand(strQuery, sqlConnection, transaction);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@ADMIN_ID", _addNewCmpOnServerReq.CmpAdminDtls.admid);
//            cmd.Parameters.AddWithValue("@EMAIL_ID", _addNewCmpOnServerReq.CmpAdminDtls.emid);
//            cmd.Parameters.AddWithValue("@ACCESS_CODE", _addNewCmpOnServerReq.CmpAdminDtls.acccd);
//            cmd.Parameters.AddWithValue("@FIRST_NAME", _addNewCmpOnServerReq.CmpAdminDtls.frtnm);
//            cmd.Parameters.AddWithValue("@LAST_NAME", _addNewCmpOnServerReq.CmpAdminDtls.lstnm);
//            cmd.Parameters.AddWithValue("@STREET_ADDRESS1", _addNewCmpOnServerReq.CmpAdminDtls.stadd1);
//            cmd.Parameters.AddWithValue("@STREET_ADDRESS2", _addNewCmpOnServerReq.CmpAdminDtls.stadd2);
//            cmd.Parameters.AddWithValue("@CITY_NAME", _addNewCmpOnServerReq.CmpAdminDtls.ctnm);
//            cmd.Parameters.AddWithValue("@STATE_NAME", _addNewCmpOnServerReq.CmpAdminDtls.stnm);
//            cmd.Parameters.AddWithValue("@COUNTRY_CODE", _addNewCmpOnServerReq.CmpAdminDtls.cntcd);
//            cmd.Parameters.AddWithValue("@ZIP", _addNewCmpOnServerReq.CmpAdminDtls.zip);
//            cmd.Parameters.AddWithValue("@DATE_OF_BIRTH", Convert.ToInt64(_addNewCmpOnServerReq.CmpAdminDtls.dob));
//            cmd.Parameters.AddWithValue("@REGISTRATION_DATETIME", Convert.ToInt64(_addNewCmpOnServerReq.CmpAdminDtls.regdt));
//            cmd.Parameters.AddWithValue("@GENDER", _addNewCmpOnServerReq.CmpAdminDtls.gnd);
//            cmd.Parameters.AddWithValue("@MIDDLE_NAME", _addNewCmpOnServerReq.CmpAdminDtls.midnm);
//            cmd.Parameters.AddWithValue("@STREET_ADDRESS3", _addNewCmpOnServerReq.CmpAdminDtls.stadd3);
//            cmd.Parameters.AddWithValue("@MOBILE", _addNewCmpOnServerReq.CmpAdminDtls.mob);
//            cmd.Parameters.AddWithValue("@IS_TRIAL", 0);
//            cmd.Parameters.AddWithValue("@COMPANY_ID", _addNewCmpOnServerReq.CmpDetails.enid);//3/12/2012
//            //MSSqlClient.ExecuteNonQueryRecord(cmd);
//            cmd.ExecuteNonQuery();
//        }

//        void insertCompanyCurrentPlanDtls(SqlConnection sqlConnection, SqlTransaction transaction)
//        {
//            string strQuery = @"INSERT INTO [TBL_COMPANY_CURRENT_PLAN]
//                               ([COMPANY_ID],[PLAN_CODE],[MAX_WORKFLOW]
//                               ,[MAX_USER],[USER_CHARGE_PM] ,[CHARGE_TYPE]
//                               ,[VALIDITY],[PURCHASE_DATE],[PLAN_CHANGE_DATE]
//                               ,[PUSHMESSAGE_PERDAY],[PUSHMESSAGE_PERMONTH],[FEATURE3]
//                               ,[FEATURE4],[FEATURE5],[FEATURE6]
//                               ,[FEATURE7],[FEATURE8],[FEATURE9]
//                               ,[FEATURE10])
//                         VALUES
//                               (@COMPANY_ID,@PLAN_CODE ,@MAX_WORKFLOW
//                               ,@MAX_USER,@USER_CHARGE_PM,@CHARGE_TYPE
//                               ,@VALIDITY,@PURCHASE_DATE,@PLAN_CHANGE_DATE
//                               ,@PUSHMESSAGE_PERDAY,@PUSHMESSAGE_PERMONTH,@FEATURE3
//                               ,@FEATURE4,@FEATURE5,@FEATURE6
//                               ,@FEATURE7,@FEATURE8,@FEATURE9
//                               ,@FEATURE10)";

//            SqlCommand cmd = new SqlCommand(strQuery, sqlConnection, transaction);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@COMPANY_ID", _addNewCmpOnServerReq.CmpCurrentPlan.enid);
//            cmd.Parameters.AddWithValue("@PLAN_CODE", _addNewCmpOnServerReq.CmpCurrentPlan.plncd);
//            cmd.Parameters.AddWithValue("@MAX_WORKFLOW", Convert.ToInt32(_addNewCmpOnServerReq.CmpCurrentPlan.maxwf));
//            cmd.Parameters.AddWithValue("@MAX_USER", Convert.ToInt32(_addNewCmpOnServerReq.CmpCurrentPlan.maxusr));
//            cmd.Parameters.AddWithValue("@USER_CHARGE_PM", Convert.ToDecimal(_addNewCmpOnServerReq.CmpCurrentPlan.chgpm));
//            cmd.Parameters.AddWithValue("@CHARGE_TYPE", _addNewCmpOnServerReq.CmpCurrentPlan.chgtp);
//            cmd.Parameters.AddWithValue("@VALIDITY", Convert.ToInt32(_addNewCmpOnServerReq.CmpCurrentPlan.val));
//            cmd.Parameters.AddWithValue("@PURCHASE_DATE",Convert.ToInt64( _addNewCmpOnServerReq.CmpCurrentPlan.purdt));
//            cmd.Parameters.AddWithValue("@PLAN_CHANGE_DATE", Convert.ToInt64(_addNewCmpOnServerReq.CmpCurrentPlan.plnchgdt));
//            cmd.Parameters.AddWithValue("@PUSHMESSAGE_PERDAY", Convert.ToInt32(_addNewCmpOnServerReq.CmpCurrentPlan.pshmgpd));
//            cmd.Parameters.AddWithValue("@PUSHMESSAGE_PERMONTH", Convert.ToInt32(_addNewCmpOnServerReq.CmpCurrentPlan.pshmgpm));
//            cmd.Parameters.AddWithValue("@FEATURE3",_addNewCmpOnServerReq.CmpCurrentPlan.ftr3=="0"?0:1);
//            cmd.Parameters.AddWithValue("@FEATURE4", _addNewCmpOnServerReq.CmpCurrentPlan.ftr4=="0"?0:1);
//            cmd.Parameters.AddWithValue("@FEATURE5",_addNewCmpOnServerReq.CmpCurrentPlan.ftr5=="0"?0:1);
//            cmd.Parameters.AddWithValue("@FEATURE6",_addNewCmpOnServerReq.CmpCurrentPlan.ftr6=="0"?0:1);
//            cmd.Parameters.AddWithValue("@FEATURE7", _addNewCmpOnServerReq.CmpCurrentPlan.ftr7=="0"?0:1);
//            cmd.Parameters.AddWithValue("@FEATURE8",_addNewCmpOnServerReq.CmpCurrentPlan.ftr8=="0"?0:1);
//            cmd.Parameters.AddWithValue("@FEATURE9",_addNewCmpOnServerReq.CmpCurrentPlan.ftr9=="0"?0:1);
//            cmd.Parameters.AddWithValue("@FEATURE10",_addNewCmpOnServerReq.CmpCurrentPlan.ftr10=="0"?0:1);

//            //MSSqlClient.ExecuteNonQueryRecord(cmd);
//            cmd.ExecuteNonQuery();
//        }

//        void insertCompanyDtls(SqlConnection sqlConnection, SqlTransaction transaction)
//        {
//            string strQuery = @"INSERT INTO [TBL_COMPANY_DETAIL]
//                                ([COMPANY_ID],[COMPANY_NAME],[REGISTRATION_NO]
//                                ,[STREET_ADDRESS1],[STREET_ADDRESS2],[STREET_ADDRESS3]
//                                ,[CITY_NAME],[STATE_NAME],[COUNTRY_CODE]
//                                ,[ZIP],[ADMIN_ID],[LOGO_IMAGE_NAME]
//                                ,[SUPPORT_EMAIL],[SUPPORT_CONTACT],[UPDATED_ON])
//                            VALUES
//                                (@COMPANY_ID,@COMPANY_NAME,@REGISTRATION_NO
//                                ,@STREET_ADDRESS1,@STREET_ADDRESS2,@STREET_ADDRESS3
//                                ,@CITY_NAME,@STATE_NAME,@COUNTRY_CODE
//                                ,@ZIP,@ADMIN_ID,@LOGO_IMAGE_NAME
//                                ,@SUPPORT_EMAIL,@SUPPORT_CONTACT,@UPDATED_ON)";

//            SqlCommand cmd = new SqlCommand(strQuery, sqlConnection, transaction);
//            cmd.CommandType = CommandType.Text;
//            cmd.Parameters.AddWithValue("@COMPANY_ID", _addNewCmpOnServerReq.CmpDetails.enid);
//            cmd.Parameters.AddWithValue("@COMPANY_NAME", _addNewCmpOnServerReq.CmpDetails.cmpnm);
//            cmd.Parameters.AddWithValue("@REGISTRATION_NO", _addNewCmpOnServerReq.CmpDetails.regno);
//            cmd.Parameters.AddWithValue("@STREET_ADDRESS1", _addNewCmpOnServerReq.CmpDetails.stadd1);
//            cmd.Parameters.AddWithValue("@STREET_ADDRESS2", _addNewCmpOnServerReq.CmpDetails.stadd2);
//            cmd.Parameters.AddWithValue("@STREET_ADDRESS3", _addNewCmpOnServerReq.CmpDetails.stadd3);
//            cmd.Parameters.AddWithValue("@CITY_NAME", _addNewCmpOnServerReq.CmpDetails.ctnm);
//            cmd.Parameters.AddWithValue("@STATE_NAME", _addNewCmpOnServerReq.CmpDetails.stnm);
//            cmd.Parameters.AddWithValue("@COUNTRY_CODE", _addNewCmpOnServerReq.CmpDetails.cntcd);
//            cmd.Parameters.AddWithValue("@ZIP", _addNewCmpOnServerReq.CmpDetails.zip);
//            cmd.Parameters.AddWithValue("@ADMIN_ID", _addNewCmpOnServerReq.CmpDetails.admid);
//            cmd.Parameters.AddWithValue("@LOGO_IMAGE_NAME", _addNewCmpOnServerReq.CmpDetails.imgnm);
//            cmd.Parameters.AddWithValue("@SUPPORT_EMAIL", _addNewCmpOnServerReq.CmpDetails.suppem);
//            cmd.Parameters.AddWithValue("@SUPPORT_CONTACT", _addNewCmpOnServerReq.CmpDetails.suppcnt);
//            cmd.Parameters.AddWithValue("@UPDATED_ON",DateTime.UtcNow.Ticks);

//            //MSSqlClient.ExecuteNonQueryRecord(cmd);
//            cmd.ExecuteNonQuery();
//        }
//    }
//}
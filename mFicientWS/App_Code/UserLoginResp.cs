﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;
namespace mFicientWS
{
    public class UserLoginResp
    {
        ResponseStatus _respStatus;
        string _requestId, _sessionId, _mobileUpdateRequired, _companyLogoUpdated, _userDtlsUpdated, _Mficien_Key, _subAdmin, _autologin, _traceDevice;
        string _deviceId, _deviceType, _companyId, _userName, _userId, _emailId;
        JArray _lst;
        JObject _s3Amazon;
        JObject _s3Bucket;

        public string EmailId
        {
            get { return _emailId; }
        }

        public string UserId
        {
            get { return _userId; }
        }

        public string UserName
        {
            get { return _userName; }

        }

        public string CompanyId
        {
            get { return _companyId; }

        }

        public string DeviceType
        {
            get { return _deviceType; }

        }

        public string DeviceId
        {
            get { return _deviceId; }

        }
        public JArray crd
        {
            get { return _lst; }
        }
        public JObject S3BucketCredentials
        {
            get { return _s3Bucket; }
        }
        public UserLoginResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
                _mobileUpdateRequired = "0";
                _companyLogoUpdated = "0";
                _userDtlsUpdated = "0";
                _Mficien_Key = "";
                _deviceId = "";
                _deviceType = "";
                _companyId = "";
                _userName = "";
                _userId = "";
                _lst = new JArray();
                _emailId = "";
                _s3Bucket = new JObject();
                _s3Amazon = new JObject();
                _subAdmin = "0";
                _autologin = "0";
                _traceDevice="{}";
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating User Login Detail Response[" + ex.Message + "]");
            }
        }

        public UserLoginResp(ResponseStatus respStatus, string requestId, string mobileUpdateRequired, string companyLogoUpdated, string userDtlUpdated, string Mficien_Key, string deviceId, string deviceType, string companyId, string userName, string userId, string email, JArray lst, JObject s3Bucket, JObject s3Amazon, string SubAdmin, string autologin, string modelType, string fullname, string traceDevice)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
                _mobileUpdateRequired = mobileUpdateRequired;
                _companyLogoUpdated = companyLogoUpdated;
                _userDtlsUpdated = userDtlUpdated;
                _Mficien_Key = Mficien_Key;
                _deviceId = deviceId;
                _deviceType = deviceType;
                _companyId = companyId;
                _userName = userName;
                _userId = userId;
                _lst = lst;
                _emailId = email;
                _s3Bucket = s3Bucket;
                _s3Amazon = s3Amazon;
                _subAdmin = SubAdmin;
                _autologin = autologin;
                ModelType = modelType;
                FullName = fullname;
                _traceDevice = traceDevice;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating User Login Detail Response[" + ex.Message + "]");
            }
        }

        public int StatusCode
        {
            get { return Convert.ToInt32(_respStatus.cd); }
        }

        public string SessionId
        {
            get;
            set;
        }
        public string ModelType
        {
            get;
            set;
        }
        public string FullName
        {
            get;
            set;
        }

        public void SetKeyindesc(string key)
        {
            _respStatus.desc = key;
        }
        public string GetResponseJson()
        {
            JObject jo = new JObject();
            jo.Add("func", Convert.ToString((int)FUNCTION_CODES.MOBILE_USER_LOGIN));
            jo.Add("rid", _requestId);
            JObject josts = new JObject();
            josts.Add("cd", _respStatus.cd);
            josts.Add("desc", _respStatus.desc);
            jo.Add("status", josts);
            JObject jodata = new JObject();
            jodata.Add("sid", SessionId);
            jodata.Add("mup", _mobileUpdateRequired);
            jodata.Add("cup", _companyLogoUpdated);
            jodata.Add("uup", _userDtlsUpdated);
            jodata.Add("mfk", _Mficien_Key);
            jodata.Add("adm", _subAdmin);
            jodata.Add("alin", _autologin);
            jodata.Add("crd", _lst);
            jodata.Add("s3", _s3Bucket);
            jodata.Add("acr", _s3Amazon["acr"]);
            jodata.Add("acp",_s3Amazon["acp"]);
            if (_traceDevice.Length > 0)
            {
                jodata.Add("tloc", JObject.Parse(_traceDevice));
            }
            jo.Add("data", jodata);

            return Utilities.getFinalJson(jo.ToString());
        }
    }

}
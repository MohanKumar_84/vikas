﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class UserQueryListResp
    { 
        ResponseStatus _respStatus;
        List<QueryList> _objQueryList;
        string _requestId;
        public UserQueryListResp(ResponseStatus respStatus, List<QueryList> objQueryList, string requestId)
        {
            _respStatus = respStatus;
            _objQueryList = objQueryList;
            _requestId = requestId;
        }
        public string GetResponseJson()
        {
            UserQueryListResponse objCheckAccountAndDeviceResp = new UserQueryListResponse();
            objCheckAccountAndDeviceResp.func = Convert.ToString((int)FUNCTION_CODES.USER_QUERY_LIST);
            objCheckAccountAndDeviceResp.rid = _requestId;
            objCheckAccountAndDeviceResp.status = _respStatus;
            UserQueryListRespData data = new UserQueryListRespData();
            data.qrs = _objQueryList;
            objCheckAccountAndDeviceResp.data = data;
            string strJsonWithDetails = Utilities.SerializeJson<UserQueryListResponse>(objCheckAccountAndDeviceResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }
    public class UserQueryListResponse : CommonResponse
    {
        public UserQueryListResponse()
        { }
        //[DataMember]
        //public List<FormListResponseData> data { get; set; }
        [DataMember]
        public UserQueryListRespData data { get; set; }
    }

    public class UserQueryListRespData
    {
        /// <summary>
        /// Menu Category Details
        /// </summary>
        [DataMember]
        public List<QueryList> qrs { get; set; }
    }//TOKEN_NO,QUERY_TYPE,QUERY_SUBJECT,RESOLVED_On
    public class QueryList
    {
        [DataMember]
        public string tn { get; set; }

        [DataMember]
        public string qtyp { get; set; }

        [DataMember]
        public string pon { get; set; }

        [DataMember]
        public string qsub { get; set; }

        [DataMember]
        public string sts { get; set; }
    }
}
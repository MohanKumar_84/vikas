﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class DesktopUserLogin
    {
        DesktopUserLoginReq _desktopUserLoginReq;

        public DesktopUserLogin(DesktopUserLoginReq desktopUserLoginReq)
        {
            _desktopUserLoginReq = desktopUserLoginReq;
        }

        public DesktopUserLoginResp Process()
        {
            string strUserFullName = string.Empty, strBucketUsername = string.Empty, strAccessKey = string.Empty, strSecretAccessKey = string.Empty;
            DataSet dsUserLoginDtls = getLoginDtlsOfUserByUserName();
            ResponseStatus objRespStatus = new ResponseStatus();
            if (dsUserLoginDtls.Tables.Count == 0 || dsUserLoginDtls.Tables[0].Rows.Count == 0)
            {
                objRespStatus.cd = "200081";
                objRespStatus.desc = "Login Failed";
                return new DesktopUserLoginResp(objRespStatus, _desktopUserLoginReq.RequestId, "", strUserFullName, strBucketUsername, strAccessKey, strSecretAccessKey);
            }

            DataTable dtblUserLoginDtl = dsUserLoginDtls.Tables[0];
            string sessionId = "";
            if (_desktopUserLoginReq.Password.ToUpper() != Convert.ToString(dtblUserLoginDtl.Rows[0]["ACCESS_CODE"]).ToUpper())
            {
                objRespStatus.cd = "200082";
                objRespStatus.desc = "Login Failed";
                return new DesktopUserLoginResp(objRespStatus, _desktopUserLoginReq.RequestId, "", strUserFullName, strBucketUsername, strAccessKey, strSecretAccessKey);
            }
            if (Convert.ToBoolean(dtblUserLoginDtl.Rows[0]["IS_BLOCKED"]))
            {
                objRespStatus.cd = "200083";
                objRespStatus.desc = "Account is blocked. Please contact to company support person. ";
                return new DesktopUserLoginResp(objRespStatus, _desktopUserLoginReq.RequestId, "", strUserFullName, strBucketUsername, strAccessKey, strSecretAccessKey);
            }
            if (!Convert.ToBoolean(dtblUserLoginDtl.Rows[0]["DESKTOP_MESSENGER"]))
            {
                objRespStatus.cd = "200084";
                objRespStatus.desc = "Desktop Messenger is not allowed. ";
                return new DesktopUserLoginResp(objRespStatus, _desktopUserLoginReq.RequestId, "", strUserFullName, strBucketUsername, strAccessKey, strSecretAccessKey);
            }
            else
            {
                objRespStatus.cd = "0";
                objRespStatus.desc = "";
                try
                {
                    DataSet dsBucketInfo = Utilities.GetS3BucketDetails(BucketType.MBUZZ_DESKTOP);
                    if (dsBucketInfo != null && dsBucketInfo.Tables.Count > 0)
                    {
                        if (dsBucketInfo.Tables[0] != null && dsBucketInfo.Tables[0].Rows.Count > 0)
                        {
                            strBucketUsername = dsBucketInfo.Tables[0].Rows[0]["USER_NAME"].ToString();
                            strAccessKey = dsBucketInfo.Tables[0].Rows[0]["ACCESS_KEY"].ToString();
                            strSecretAccessKey = dsBucketInfo.Tables[0].Rows[0]["SECRET_ACCESS_KEY"].ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                strUserFullName = dsUserLoginDtls.Tables[0].Rows[0]["FIRST_NAME"].ToString() + " " + dsUserLoginDtls.Tables[0].Rows[0]["LAST_NAME"].ToString();
            }

            return new DesktopUserLoginResp(objRespStatus, _desktopUserLoginReq.RequestId, sessionId, strUserFullName, strBucketUsername, strAccessKey, strSecretAccessKey);
        }

        DataSet getLoginDtlsOfUserByUserName()
        {
            string strQuery = @"SELECT * FROM TBL_USER_DETAIL WHERE USER_NAME =@UserName AND COMPANY_ID =  @CompanyId AND IS_ACTIVE = 1";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", _desktopUserLoginReq.CompanyId);
            cmd.Parameters.AddWithValue("@UserName", _desktopUserLoginReq.UserName);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }
    }
}
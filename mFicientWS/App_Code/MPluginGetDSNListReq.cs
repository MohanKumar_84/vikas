﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class MPluginGetDSNListReq
    {
        string _requestId, _enterpriseId;
        string _agentName, _agentPassword;

        public string AgentPassword
        {
            get { return _agentPassword; }

        }
        public string AgentName
        {
            get { return _agentName; }

        }
        public string EnterpriseId
        {
            get { return _enterpriseId; }

        }

        public string RequestId
        {
            get { return _requestId; }

        }

        public MPluginGetDSNListReq(string requestJson)
        {
            MPlugGetDSNListReqJsonParsing objRequestJsonParsing =
                Utilities.DeserialiseJson<MPlugGetDSNListReqJsonParsing>(requestJson);
            _requestId = objRequestJsonParsing.req.rid;
            _enterpriseId = objRequestJsonParsing.req.eid;
            _agentName = objRequestJsonParsing.req.agtnm;
            _agentPassword = objRequestJsonParsing.req.agtpwd;
        }
    }

    [DataContract]
    public class MPlugGetDSNListReqJsonParsing
    {
        /// <summary>
        /// Request
        /// </summary>
        [DataMember]
        public MPlugGetDSNListReqFields req { get; set; }
    }

    [DataContract]
    public class MPlugGetDSNListReqFields
    {

        /// <summary>
        /// request id
        /// </summary>
        [DataMember]
        public string rid { get; set; }

        /// <summary>
        ///company id
        /// </summary>
        [DataMember]
        public string eid { get; set; }

        [DataMember]
        public string agtnm { get; set; }

        [DataMember]
        public string agtpwd { get; set; }

    }
}
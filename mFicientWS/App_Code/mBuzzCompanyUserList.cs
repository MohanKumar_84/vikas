﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientWS
{
    public class mBuzzCompanyUserList
    {
        mBuzzCompanyUserListReq objmBuzzCompanyUserListReq;
        DataTable dtUsersList;

        public mBuzzCompanyUserList(mBuzzCompanyUserListReq _mBuzzCompanyUserListReq)
        {
            objmBuzzCompanyUserListReq = _mBuzzCompanyUserListReq;
        }

        public mBuzzCompanyUserListResp Process()
        {
            ResponseStatus objRespStatus = new ResponseStatus();
            List<Users> lstUsersDtls = new List<Users>();
            try
            {
                SqlCommand cmd = new SqlCommand(@"SELECT u.USER_NAME as UserName FROM TBL_USER_DETAIL u WHERE u.COMPANY_ID = @COMPANY_ID");
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@COMPANY_ID", objmBuzzCompanyUserListReq.CompanyId);

                dtUsersList = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd).Tables[0];
                if (dtUsersList != null && dtUsersList.Rows.Count > 0)
                {
                    foreach (DataRow row in dtUsersList.Rows)
                    {
                        Users objUsersData = new Users();
                        objUsersData.unm = row["UserName"].ToString();
                        lstUsersDtls.Add(objUsersData);
                    }
                    objRespStatus.cd = "0";
                    objRespStatus.desc = "";
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            }
            return new mBuzzCompanyUserListResp(objRespStatus, lstUsersDtls, objmBuzzCompanyUserListReq.RequestId);
        }
    }
}
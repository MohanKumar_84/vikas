﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace mFicientWS
{
    public class MSSqlDoTaskClient
    {
        public static void SqlConnectionOpen(out SqlConnection Conn)
        {
            try
            {
                Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch (SqlException ex)
            {
                Conn = null;
                if (ex.Number == 18456)
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                else
                    throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
            }
        }

        public static void SqlConnectionOpen(out SqlConnection Conn, string ConnectionString)
        {
            try
            {
                Conn = new SqlConnection(ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch (SqlException ex)
            {
                Conn = null;
                if (ex.Number == 18456)
                    throw new Exception(((int)DO_TASK_ERROR.COMMAND_CREDENTIAL_ERROR).ToString());
                else
                    throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DO_TASK_ERROR.CONNECTION_ERROR).ToString());
            }
        }

        public static void SqlConnectionClose(SqlConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open))
                {
                    Conn.Close();
                }
            }
            catch
            {
            }
        }

        public static DataSet SelectDataFromSqlCommand(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            DataSet ObjDataSet = null;
            try
            {
                SqlConnectionOpen(out objSqlConnection);
                _SqlCommand.Connection = objSqlConnection;

                ObjDataSet = new DataSet();

                SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

                objSqlDataAdapter.Fill(ObjDataSet);
            }
            catch (SqlException ex)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }
            return ObjDataSet;
        }

        public static DataSet SelectDataFromDoTask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _Password)
        {
            SqlConnection objSqlConnection = null;

            DataSet objDataSet = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId, _userId, _Password);
                dbcon.GetConnector();

                SqlConnectionOpen(out objSqlConnection, dbcon.ConnectionString);
                if (objSqlConnection.State == ConnectionState.Open)
                {
                    SqlCommand _SqlCommand = new SqlCommand(db_cmd.SqlQuery, objSqlConnection);

                    foreach (QueryParameters parameters in LstParameters)
                    {
                        _SqlCommand.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                    }

                    objDataSet = new DataSet();

                    SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

                    objSqlDataAdapter.Fill(objDataSet);
                }
            }
            catch (SqlException ex)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }

            return objDataSet;
        }

        public static int ExecuteNonQueryRecordDotask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters,string _userId,string _password)
        {
            SqlConnection objSqlConnection = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId, _userId, _password);
                dbcon.GetConnector();

                SqlConnectionOpen(out objSqlConnection, dbcon.ConnectionString);

                SqlCommand _SqlCommand = new SqlCommand(db_cmd.SqlQuery, objSqlConnection);

                foreach (QueryParameters parameters in LstParameters)
                {
                    _SqlCommand.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                int i = _SqlCommand.ExecuteNonQuery();

                return i;
            }
            catch (SqlException msSqlEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }
            //return 0;
        }

        public static string ExecuteScalarDotask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _password)
        {
            SqlConnection objSqlConnection = null;
            string strResult = "";
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId, _userId, _password);
                dbcon.GetConnector();

                SqlConnectionOpen(out objSqlConnection, dbcon.ConnectionString);

                SqlCommand _SqlCommand = new SqlCommand(db_cmd.SqlQuery, objSqlConnection);

                foreach (QueryParameters parameters in LstParameters)
                {
                    _SqlCommand.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                }

                object obj = _SqlCommand.ExecuteScalar();
                if (obj != null)
                    strResult = obj.ToString();
            }
            catch (SqlException msSqlEx)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }

            return strResult;
        }

        public static DataSet ExecuteStoredProcedureDoTask(DataBaseCommand db_cmd, List<QueryParameters> LstParameters, string _userId, string _password)
        {
            SqlConnection objSqlConnection = null;

            DataSet objDataSet = null;
            try
            {
                DataBaseConnector dbcon = new DataBaseConnector(db_cmd.ConnectorId, db_cmd.EnterpriseId, _userId,  _password);
                dbcon.GetConnector();

                SqlConnectionOpen(out objSqlConnection, dbcon.ConnectionString);
                if (objSqlConnection.State == ConnectionState.Open)
                {
                    SqlCommand _SqlCommand = new SqlCommand();
                    _SqlCommand.Connection = objSqlConnection;
                    _SqlCommand.CommandType = CommandType.StoredProcedure;
                    _SqlCommand.CommandText = db_cmd.SqlQuery;

                    foreach (QueryParameters parameters in LstParameters)
                    {
                        _SqlCommand.Parameters.AddWithValue("@" + parameters.para, parameters.val);
                    }

                    objDataSet = new DataSet();

                    SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

                    objSqlDataAdapter.Fill(objDataSet);
                }
            }
            catch (SqlException ex)
            {
                throw new Exception(((int)DO_TASK_ERROR.SQL_QUERY_ERROR).ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                SqlConnectionClose(objSqlConnection);
            }

            return objDataSet;
        }
    }
}
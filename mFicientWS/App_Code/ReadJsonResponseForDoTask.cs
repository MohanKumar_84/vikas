﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Data;
using System.Xml;
namespace mFicientWS
{
    public class ReadJsonResponseForDoTask
    {
        string _responseJson;
        WebServiceCommand _wsCmd;
        OdataCommand _odataCommand;
        char _pathSplitChar = '/';
        DoTaskReq _doTaskReq;
        bool _isForReturnFunctionZero = false;

        public bool IsForReturnFunctionZero
        {
            get { return _isForReturnFunctionZero; }
            set { _isForReturnFunctionZero = value; }
        }
        //public enum RETURN_FUNCTION_TYPE
        //{
        //    COMPLETE_TBL_CLMS_AND_ROWS = 0,
        //    DROP_DOWN_DATA = 103,
        //    REPEATER_DATA = 105,
        //    BAR_DATA = 106,
        //    BAR_LINE_DATA = 107,
        //    PIE_DATA =108,
        //    TBL_MANIPULATION = 9
        //}

        public ReadJsonResponseForDoTask(string responseJson
            , WebServiceCommand wscmd,
            DoTaskReq doTaskReq)
        {
            _responseJson = responseJson;
            _wsCmd = wscmd;
            _doTaskReq = doTaskReq;
            _odataCommand = null;
            _pathSplitChar = '/';
        }
        public ReadJsonResponseForDoTask(string responseJson
            , OdataCommand odataCmd,
            DoTaskReq doTaskReq)
        {
            _responseJson = responseJson;
            _odataCommand = odataCmd;
            _doTaskReq = doTaskReq;
            _wsCmd = null;
            _pathSplitChar = '.';
        }
        /// <summary>
        /// Use this for Return function 0
        /// </summary>
        /// <param name="responseXML"></param>
        /// <param name="wsCmd"></param>
        public ReadJsonResponseForDoTask(string responseJson,
            WebServiceCommand wsCmd)
        {
            _isForReturnFunctionZero = true;
            _doTaskReq = null;
            _responseJson = responseJson;
            _wsCmd = wsCmd;
            _odataCommand = null;
            _pathSplitChar = '/';
        }
        /// <summary>
        /// Use this for Return function 0 With OData Command
        /// </summary>
        /// <param name="responseXML"></param>
        /// <param name="wsCmd"></param>
        public ReadJsonResponseForDoTask(string responseJson,
            OdataCommand odataCommand)
        {
            _isForReturnFunctionZero = true;
            _doTaskReq = null;
            _responseJson = responseJson;
            _odataCommand = odataCommand;
            _wsCmd = null;
            _pathSplitChar = '.';
        }
        public Object Process()
        {
            object objAfterReadingDatasetPath = new object();
            try
            {
                //Hashtable hashJson;
                if (this.ResponseJson.StartsWith("{"))
                    objAfterReadingDatasetPath = (Hashtable)JSONParser.JsonDecode(this.ResponseJson);
                else if (this.ResponseJson.StartsWith("["))
                {
                    ArrayList arr = (ArrayList)JSONParser.JsonDecode(this.ResponseJson);
                    objAfterReadingDatasetPath = arr[0];
                }
            }
            catch
            {
                throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
            }
            int iCountOfLoop = 0;
            if (!String.IsNullOrEmpty(this.WsCmd.DataSetPath))
            {
                iCountOfLoop = (this.WsCmd.DataSetPath.Split('/')).Length;

                for (int i = 0; i < iCountOfLoop; i++)
                {
                    if (objAfterReadingDatasetPath.GetType() == typeof(Hashtable))
                    {
                        objAfterReadingDatasetPath = ((Hashtable)objAfterReadingDatasetPath)[this.WsCmd.DataSetPath.Split('/')[i]];
                        if (objAfterReadingDatasetPath == null)
                            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                    }
                    else if (objAfterReadingDatasetPath.GetType() == typeof(ArrayList))
                    {
                        //if a dataset path is provided only the end result should be
                        //array list any array list in between is invalid tag path provided
                        if (i < iCountOfLoop)
                            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                    }
                }
            }
            return getDataForReturnFunctionZero(this.ResponseJson,
                this.WsCmd,
                objAfterReadingDatasetPath);
            
        }
        public Object ProcessForOData()
        {
            if (this.OdataCommand == null) throw new ArgumentNullException();
            Hashtable hashJson = (Hashtable)JSONParser.JsonDecode(this.ResponseJson);
            object objAfterReadingDatasetPath = hashJson;

            int iCountOfLoop = 0;
            string strDataSetPath = this.OdataCommand.DataSetPath;

            //Expecting somePath[] always (if dataset path is present)
            if (!String.IsNullOrEmpty(strDataSetPath))
            {
                if (!strDataSetPath.Trim().EndsWith("[]")) throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                strDataSetPath = strDataSetPath.Replace("[]", " ").Trim();
            }

            List<IdeWsParam> lstOutPutTagPath = this.OdataCommand.OutputTagPath;
            if (!String.IsNullOrEmpty(strDataSetPath))
            {
                iCountOfLoop = (strDataSetPath.Split('.')).Length;

                for (int i = 0; i < iCountOfLoop; i++)
                {
                    if (objAfterReadingDatasetPath.GetType() == typeof(Hashtable))
                    {
                        objAfterReadingDatasetPath = ((Hashtable)objAfterReadingDatasetPath)[strDataSetPath.Split('.')[i]];
                        if (objAfterReadingDatasetPath == null)
                            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                    }
                    else if (objAfterReadingDatasetPath.GetType() == typeof(ArrayList))
                    {
                        //if a dataset path is provided only the end result should be
                        //array list any array list in between is invalid tag path provided
                        if (i < iCountOfLoop)
                            throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                    }
                }
            }
            return getDataForReturnFunctionZero(
                this.ResponseJson,
                lstOutPutTagPath,
                strDataSetPath,
                objAfterReadingDatasetPath
                );
            
        }

        #region Get Data For Return Function Zero
        DataTable getDataForReturnFunctionZero(string responseJson,
            WebServiceCommand wscmd,
            object objAfterReadingDatasetPath)
        {
            DataTable dtblForReturning = new DataTable();
            foreach (IdeWsParam outputTag in wscmd.OutPutTagPaths)
            {
                dtblForReturning.Columns.Add(outputTag.name, typeof(string));
            }

            if (!String.IsNullOrEmpty(wscmd.DataSetPath))
            {
                ArrayList lstData = new ArrayList();
                try
                {
                    /**
                     * if data set path is provided then the end result should be an array list
                     * other wise the path provided is incorrect.
                     * **/
                    lstData = (ArrayList)objAfterReadingDatasetPath;
                }
                catch
                {
                    throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                }
                foreach (object objDataFromJson in lstData)
                {

                    dtblForReturning.Rows.Add(getDataRow(dtblForReturning,
                                                 wscmd.OutPutTagPaths, objDataFromJson));
                }
            }
            else
            {
                dtblForReturning.Rows.Add(getDataRow(dtblForReturning,
                                                wscmd.OutPutTagPaths, objAfterReadingDatasetPath));
            }


            return dtblForReturning;
        }
        DataTable getDataForReturnFunctionZero(string responseJson,
            List<IdeWsParam> outputTagPath,
            string datasetPath,
            object objAfterReadingDatasetPath)
        {
            DataTable dtblForReturning = new DataTable();
            if (outputTagPath != null)
            {
                foreach (IdeWsParam outputTag in outputTagPath)
                {
                    dtblForReturning.Columns.Add(outputTag.name, typeof(string));
                }

                if (!String.IsNullOrEmpty(datasetPath))
                {
                    ArrayList lstData = new ArrayList();
                    try
                    {
                        /**
                         * if data set path is provided then the end result should be an array list
                         * other wise the path provided is incorrect.
                         * **/
                        lstData = (ArrayList)objAfterReadingDatasetPath;
                    }
                    catch
                    {
                        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                    }
                    foreach (object objDataFromJson in lstData)
                    {
                        if (this.WsCmd != null)
                        {
                            dtblForReturning.Rows.Add(getDataRow(dtblForReturning,
                                                     outputTagPath, objDataFromJson));
                        }
                        else if (this.OdataCommand != null)
                        {
                            dtblForReturning.Rows.Add(getDataRowForODATA(dtblForReturning,
                                                     outputTagPath, objDataFromJson));
                        }

                    }
                }
                else
                {
                    if (this.WsCmd != null)
                    {
                        dtblForReturning.Rows.Add(getDataRow(dtblForReturning,
                                                    outputTagPath, objAfterReadingDatasetPath));
                    }
                    else if (this.OdataCommand != null)
                    {
                        dtblForReturning.Rows.Add(getDataRowForODATA(dtblForReturning,
                                                    outputTagPath, objAfterReadingDatasetPath));
                    }

                }
            }
            return dtblForReturning;
        }
        DataRow getDataRow(DataTable dtbl,
            List<IdeWsParam> outputPathsLst,
            object objAfterReadingDatasetPath)
        {
            DataRow row = dtbl.NewRow();
            int iCountOfColLoop = 0;
            foreach (DataColumn col in dtbl.Columns)
            {

                string strRecordTagPath = getOutputPathOfTagFromPathList(col.ColumnName, outputPathsLst);
                int iCountOfLoop = 0;
                object objAfterReadingFinalTagPath;


                try
                { iCountOfLoop = (strRecordTagPath.Split('/')).Length; }
                catch
                { throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString()); }


                objAfterReadingFinalTagPath = objAfterReadingDatasetPath;
                for (int i = 0; i < iCountOfLoop; i++)
                {
                    if (objAfterReadingFinalTagPath.GetType() == typeof(Hashtable))
                    {
                        string strPathAfterSplitByIndexOfLoop = strRecordTagPath.Split('/')[i];
                        if (!String.IsNullOrEmpty(strPathAfterSplitByIndexOfLoop))
                        {
                            if (objAfterReadingFinalTagPath.GetType() == typeof(Hashtable))
                            {
                                objAfterReadingFinalTagPath = ((Hashtable)objAfterReadingFinalTagPath)[strPathAfterSplitByIndexOfLoop];
                                if (objAfterReadingFinalTagPath == null)
                                    throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                            }
                            else if (objAfterReadingFinalTagPath.GetType() == typeof(ArrayList))
                            {
                                if (i < iCountOfLoop)
                                {
                                    //if we get array list in between the loop 
                                    //take only the 1 record.
                                    //try
                                    //{
                                    //    objAfterReadingFinalTagPath = ((ArrayList)objAfterReadingDatasetPath)[i];
                                    //}
                                    //catch
                                    //{
                                    //    continue;
                                    //}
                                    objAfterReadingFinalTagPath = ((ArrayList)objAfterReadingDatasetPath)[0];
                                    if (objAfterReadingFinalTagPath == null)
                                        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());

                                    if (objAfterReadingFinalTagPath.GetType() == typeof(Hashtable))
                                    {
                                        objAfterReadingFinalTagPath = ((Hashtable)objAfterReadingFinalTagPath)[strPathAfterSplitByIndexOfLoop];
                                    }
                                    else
                                    {
                                        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                                    }
                                }
                                else
                                {
                                    //the final path should not be an array list
                                    //then we will not know what is to return
                                    //the array list may contain another object,and simple types as well.
                                    throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                                }
                            }
                        }
                        else
                        {
                            objAfterReadingFinalTagPath = String.Empty;
                        }
                    }
                }
                row[col.ColumnName] = Convert.ToString(objAfterReadingFinalTagPath);

                iCountOfColLoop++;
            }
            return row;
        }

        DataRow getDataRowForODATA(DataTable dtbl,
           List<IdeWsParam> outputPathsLst,
           object objAfterReadingDatasetPath)
        {
            DataRow row = dtbl.NewRow();
            int iCountOfColLoop = 0;
            foreach (DataColumn col in dtbl.Columns)
            {

                string strRecordTagPath = getOutputPathOfTagFromPathList(col.ColumnName, outputPathsLst);
                int iCountOfLoop = 0;
                object objAfterReadingFinalTagPath;


                try
                { iCountOfLoop = (strRecordTagPath.Split(this.PathSplitChar)).Length; }
                catch
                { throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString()); }


                objAfterReadingFinalTagPath = objAfterReadingDatasetPath;
                for (int i = 0; i < iCountOfLoop; i++)
                {
                    if (objAfterReadingFinalTagPath.GetType() == typeof(Hashtable))
                    {
                        string strPathAfterSplitByIndexOfLoop = strRecordTagPath.Split(this.PathSplitChar)[i];
                        if (!String.IsNullOrEmpty(strPathAfterSplitByIndexOfLoop))
                        {
                            if (objAfterReadingFinalTagPath.GetType() == typeof(Hashtable))
                            {
                                objAfterReadingFinalTagPath = ((Hashtable)objAfterReadingFinalTagPath)[strPathAfterSplitByIndexOfLoop];
                                if (objAfterReadingFinalTagPath == null)
                                    throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                            }
                            else if (objAfterReadingFinalTagPath.GetType() == typeof(ArrayList))
                            {
                                if (i < iCountOfLoop)
                                {
                                    //if we get array list in between the loop 
                                    //take only the 1 record.
                                    //try
                                    //{
                                    //    objAfterReadingFinalTagPath = ((ArrayList)objAfterReadingDatasetPath)[i];
                                    //}
                                    //catch
                                    //{
                                    //    continue;
                                    //}
                                    objAfterReadingFinalTagPath = ((ArrayList)objAfterReadingDatasetPath)[0];
                                    if (objAfterReadingFinalTagPath == null)
                                        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());

                                    if (objAfterReadingFinalTagPath.GetType() == typeof(Hashtable))
                                    {
                                        objAfterReadingFinalTagPath = ((Hashtable)objAfterReadingFinalTagPath)[strPathAfterSplitByIndexOfLoop];
                                    }
                                    else
                                    {
                                        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                                    }
                                }
                                else
                                {
                                    //the final path should not be an array list
                                    //then we will not know what is to return
                                    //the array list may contain another object,and simple types as well.
                                    throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
                                }
                            }
                        }
                        else
                        {
                            objAfterReadingFinalTagPath = String.Empty;
                        }
                    }
                }
                row[col.ColumnName] = Convert.ToString(objAfterReadingFinalTagPath);

                iCountOfColLoop++;
            }
            return row;
        }
        #endregion
        #region private methods
        string getOutputPathOfTagFromPathList(string nameOfOutputTag, List<IdeWsParam> outputPathsLst)
        {
            string strOutputTagPath = String.Empty;
            foreach (IdeWsParam outputTagAndPath in outputPathsLst)
            {
                //Comparing the string as it is sent from the mobile end.Not converting toLower in case there is an error
                //from mobile end.
                if (outputTagAndPath.name == nameOfOutputTag)
                {
                    strOutputTagPath = outputTagAndPath.path;
                    break;
                }
                else
                    continue;
            }
            return strOutputTagPath;
        }
        List<string> getCommaSeparatedCntrlAndOutputTags(DoTaskReq doTaskReq)
        {
            List<string> lstCommaSepCntrlAndOutputTags = new List<string>();
            string strTagValue = String.Empty;
            RETURN_FUNCTION_TYPE_DOTASK eCntrlType = getReturnFunctionCntrlType((int)RETURN_FUNCTION_TYPE_DOTASK.COMPLETE_TBL_CLMS_AND_ROWS_SELECT);
            switch (eCntrlType)
            {
                case RETURN_FUNCTION_TYPE_DOTASK.COMPLETE_TBL_CLMS_AND_ROWS_SELECT:
                   
                    break;
                //case RETURN_FUNCTION_TYPE_DOTASK.DROP_DOWN_DATA:
                //case RETURN_FUNCTION_TYPE_DOTASK.PIE_DATA:
                //    strTagValue = "t," + doTaskReq.returnType.t;
                //    lstCommaSepCntrlAndOutputTags.Add(strTagValue);

                //    strTagValue = "v," + doTaskReq.returnType.v;
                //    lstCommaSepCntrlAndOutputTags.Add(strTagValue);

                //    break;
                //case RETURN_FUNCTION_TYPE_DOTASK.REPEATER_DATA:
                //    strTagValue = "b," + doTaskReq.returnType.b;
                //    lstCommaSepCntrlAndOutputTags.Add(strTagValue);

                //    strTagValue = "c," + doTaskReq.returnType.c;
                //    lstCommaSepCntrlAndOutputTags.Add(strTagValue);

                //    strTagValue = "i," + doTaskReq.returnType.i;
                //    lstCommaSepCntrlAndOutputTags.Add(strTagValue);

                //    strTagValue = "r," + doTaskReq.returnType.r;
                //    lstCommaSepCntrlAndOutputTags.Add(strTagValue);

                //    strTagValue = "s," + doTaskReq.returnType.s;
                //    lstCommaSepCntrlAndOutputTags.Add(strTagValue);

                //    strTagValue = "n," + doTaskReq.returnType.n;
                //    lstCommaSepCntrlAndOutputTags.Add(strTagValue);

                //    strTagValue = "an," + doTaskReq.returnType.an;
                //    lstCommaSepCntrlAndOutputTags.Add(strTagValue);
                //    break;
                //case RETURN_FUNCTION_TYPE_DOTASK.BAR_DATA:
                //case RETURN_FUNCTION_TYPE_DOTASK.LINE_DATA:
                //    strTagValue = "t," + doTaskReq.returnType.t;
                //    lstCommaSepCntrlAndOutputTags.Add(strTagValue);

                //    strTagValue = "v1," + doTaskReq.returnType.v1;
                //    lstCommaSepCntrlAndOutputTags.Add(strTagValue);

                //    strTagValue = "v2," + doTaskReq.returnType.v2;
                //    lstCommaSepCntrlAndOutputTags.Add(strTagValue);

                //    strTagValue = "v3," + doTaskReq.returnType.v3;
                //    lstCommaSepCntrlAndOutputTags.Add(strTagValue);

                //    strTagValue = "v4," + doTaskReq.returnType.v4;
                //    lstCommaSepCntrlAndOutputTags.Add(strTagValue);
                //    break;
                default:
                    break;
            }
            return lstCommaSepCntrlAndOutputTags;
        }
        RETURN_FUNCTION_TYPE_DOTASK getReturnFunctionCntrlType(int returnFunctionValue)
        {
            RETURN_FUNCTION_TYPE_DOTASK eCntrlType = RETURN_FUNCTION_TYPE_DOTASK.REPEATER_DATA;
            switch ((RETURN_FUNCTION_TYPE_DOTASK)Enum.Parse(typeof(RETURN_FUNCTION_TYPE_DOTASK), returnFunctionValue.ToString()))
            {
                case RETURN_FUNCTION_TYPE_DOTASK.COMPLETE_TBL_CLMS_AND_ROWS_SELECT:
                    eCntrlType = RETURN_FUNCTION_TYPE_DOTASK.COMPLETE_TBL_CLMS_AND_ROWS_SELECT;
                    break;
                case RETURN_FUNCTION_TYPE_DOTASK.DROP_DOWN_DATA:
                    eCntrlType = RETURN_FUNCTION_TYPE_DOTASK.DROP_DOWN_DATA;
                    break;
                case RETURN_FUNCTION_TYPE_DOTASK.REPEATER_DATA:
                    eCntrlType = RETURN_FUNCTION_TYPE_DOTASK.REPEATER_DATA;

                    break;
                case RETURN_FUNCTION_TYPE_DOTASK.BAR_DATA:
                    eCntrlType = RETURN_FUNCTION_TYPE_DOTASK.BAR_DATA;
                    break;
                case RETURN_FUNCTION_TYPE_DOTASK.LINE_DATA:
                    eCntrlType = RETURN_FUNCTION_TYPE_DOTASK.LINE_DATA;
                    break;
                case RETURN_FUNCTION_TYPE_DOTASK.PIE_DATA:
                    eCntrlType = RETURN_FUNCTION_TYPE_DOTASK.PIE_DATA;
                    break;
                case RETURN_FUNCTION_TYPE_DOTASK.TBL_EDITABLE:
                    eCntrlType = RETURN_FUNCTION_TYPE_DOTASK.TBL_TABLE;
                    break;
                case RETURN_FUNCTION_TYPE_DOTASK.TBL_TABLE:
                    eCntrlType = RETURN_FUNCTION_TYPE_DOTASK.TBL_TABLE;
                    break;
            }
            return eCntrlType;
        }
        #endregion
        #region Public Properties
        public DoTaskReq DoTaskReq
        {
            get { return _doTaskReq; }
            private set { _doTaskReq = value; }
        }
        public WebServiceCommand WsCmd
        {
            get { return _wsCmd; }
            private set { _wsCmd = value; }
        }
        public string ResponseJson
        {
            get { return _responseJson; }
            private set { _responseJson = value; }
        }
        public OdataCommand OdataCommand
        {
            get { return _odataCommand; }
            private set { _odataCommand = value; }
        }

        public char PathSplitChar
        {
            get { return _pathSplitChar; }
            private set { _pathSplitChar = value; }
        }
        #endregion
    }
}
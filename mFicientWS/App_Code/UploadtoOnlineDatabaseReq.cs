﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace mFicientWS
{
    public class UploadtoOnlineDatabaseReq
    {
        private string _sessionid, _requestId, _deviceId, _deviceType, _companyId, _username, _tableId, _userId, _model, _appId;
        private int _functioncode;
        DataConnCredential _dataCredential;
        public List<string> _colname { get; set; }
        public List<List<string>> _val { get; set; }
       
        public string TableId
        {
            get { return _tableId; }
        }
       
        public string SessionId
        {
            get { return _sessionid; }
        }
        public List<string> colname
        {
            get { return _colname; }
        }

        public List<List<string>> valdetails
        {
            get { return _val; }
        }
        public int functioncode
        {
            get { return _functioncode; }

        }

        public string RequestId
        {
            get { return _requestId; }

        }

        public string DeviceId
        {
            get { return _deviceId; }

        }
        public string DeviceType
        {
            get { return _deviceType; }
        }

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        public string Model
        {
            get { return _model; }
        }
        public string AppID
        {
            get { return _appId; }
        }
        public string Username
        {
            get { return _username; }
        }
        public DataConnCredential DataCredential
        {
            get { return _dataCredential; }
        }
        public UploadtoOnlineDatabaseReq(string requestJson, string userId, string model)
        {
            RequestJsonParsing<UploadtoOnlineDatabaseData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<UploadtoOnlineDatabaseData>>(requestJson);
            _functioncode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functioncode != (int)FUNCTION_CODES.UPLOAD_OFFLINE_DATABASE_TO_ONLINE_DATABASE)
            {
                throw new Exception("Invalid Function Code");
            }
            _sessionid = objRequestJsonParsing.req.sid;
            _deviceId = objRequestJsonParsing.req.did;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _username = objRequestJsonParsing.req.data.unm;
            _tableId = objRequestJsonParsing.req.data.tbl;
            _companyId = objRequestJsonParsing.req.data.enid;
            _dataCredential = objRequestJsonParsing.req.data.cr;
            _colname = objRequestJsonParsing.req.data.col;
            _val = objRequestJsonParsing.req.data.val;
            _userId = userId;
            _model = model;
            _appId = objRequestJsonParsing.req.data.appId;
            _appId = _appId == null ? "" : _appId;
            
            if (_dataCredential != null)
            {
                _dataCredential.pwd = _dataCredential.pwd;// EncryptionDecryption.AESDecrypt(this.CompanyId.ToUpper(), _dataCredential.pwd);
                _dataCredential.unm = _dataCredential.unm;// EncryptionDecryption.AESDecrypt(this.CompanyId.ToUpper(), _dataCredential.unm);
            }
            else
            {
                _dataCredential = new DataConnCredential();
                _dataCredential.pwd = "";
                _dataCredential.unm = "";
            }
            if (string.IsNullOrEmpty(_tableId) || _colname == null || _val == null) throw new Exception();
        }
    }

    [DataContract]
    public class UploadtoOnlineDatabaseData : Data
    {
        [DataMember]
        public string appId { get; set; }
        [DataMember]
        public string tbl { get; set; }

        [DataMember]
        public DataConnCredential cr { get; set; }


        [DataMember]
        public List<List<string>> val { get; set; }


        [DataMember]
        public List<string> col { get; set; }

    }

}
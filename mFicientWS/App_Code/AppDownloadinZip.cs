﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ionic.Zip;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Xml;
using System.Collections;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;

namespace mFicientWS
{
    public class AppDownloadinZip
    {
        GetZippedAppFilesReq _getZippedAppFilesReq;
        HttpContext _appContext;
        Hashtable formActionButton;

        internal AppDownloadinZip(GetZippedAppFilesReq getZippedAppFilesReq, HttpContext appContext)
        {
            _getZippedAppFilesReq = getZippedAppFilesReq;
            if (!isUserAuthorisedToDownload())
            {
                throw new Exception(HttpStatusCode.Unauthorized.ToString());
            }

            formActionButton = new Hashtable();
            _appContext = appContext;
        }

        internal ZipFile ZippedUIFiles()
        {
            try
            {
                ZipFile zip = new ZipFile();
                StringBuilder listOfFiles = new StringBuilder();
                StringBuilder listOfImages = new StringBuilder();
                DataSet dtObjects;
                appFlowDetailAndForm objapp = new appFlowDetailAndForm(_getZippedAppFilesReq.UserId, _getZippedAppFilesReq.CompanyId, Convert.ToInt32(_getZippedAppFilesReq.ModelType));
                jqueryAppClass appDetail = objapp.GetNewAppJson(_getZippedAppFilesReq.AppId, out dtObjects);
                string strImageName, strExtension;
                if (appDetail == null || appDetail.startUpViewId == null)//if there is a app id then there will be a html for app id.
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }

                //for collecting form list and child form list
                StringBuilder sbFileList = new StringBuilder(String.Empty);


                //get form/child form list and add to zip
                getFormsHtmlCollection(appDetail.forms, out sbFileList);

                // Add the filename to listOfFiles (separated by comma)
                listOfFiles = listOfFiles.Length == 0 ? listOfFiles.Append(sbFileList) : listOfFiles.Append(',' + sbFileList.ToString());

                using (zip)
                {
                    zip.AddEntry("LIST.txt", listOfFiles.ToString());
                }
                //add icon image
                using (zip)
                {
                    try
                    {
                        strImageName = appDetail.icon;
                        strExtension = strImageName.Substring(strImageName.LastIndexOf('.'));
                        zip.AddEntry("ICON" + strExtension, downloadRemoteImageFile(MficientConstants.IconUrl + @"/" + appDetail.icon));
                    }
                    catch { }
                }

                //add Images List
                using (zip)
                {
                    zip.AddEntry("IMAGES.txt", listOfImages.ToString());
                }
                _appContext.Items.Add("ZIP_FILE_NAME", _getZippedAppFilesReq.AppId);

                return zip;
            }
            catch (Exception e)
            {
                if (e.Message == ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString())
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
                throw new Exception();
            }
        }
        bool isUserAuthorisedToDownload()
        {
            SqlCommand cmd = new SqlCommand(@"SELECT WfCat.* FROM TBL_WORKFLOW_AND_CATEGORY_LINK as WfCat
                WHERE WfCat.COMPANY_ID = @CompanyId AND WfCat.WORKFLOW_ID = @WorkflowId;

                SELECT * FROM TBL_WORKFLOW_AND_GROUP_LINK AS AppGroupLink INNER JOIN TBL_USER_GROUP_LINK AS UsrGrpLnk
                ON AppGroupLink.GROUP_ID = UsrGrpLnk.GROUP_ID WHERE AppGroupLink.COMPANY_ID = @CompanyId
                AND UsrGrpLnk.USER_ID = @UserId AND AppGroupLink.WORKFLOW_ID = @WorkflowId");

            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", _getZippedAppFilesReq.CompanyId);
            cmd.Parameters.AddWithValue("@WorkflowId", _getZippedAppFilesReq.AppId);
            cmd.Parameters.AddWithValue("@UserId", _getZippedAppFilesReq.UserId);
            DataSet dsUserAppDetail = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (dsUserAppDetail != null)
            {
                if (dsUserAppDetail.Tables[0].Rows.Count > 0)
                {
                    if (dsUserAppDetail.Tables[1].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            else
            {
                throw new Exception();
            }
        }

        void getFormsHtmlCollection(List<AppForm> lstForm, out StringBuilder listOfFiles)
        {
            listOfFiles = new StringBuilder();
            List<AppForm> childForms;

            foreach (AppForm row in lstForm)
            {
                childForms = new List<AppForm>();
                foreach (sectionRowPanels fromRow in row.rowPanels)
                {
                    foreach (SectionColPanels fromCol in fromRow.colmnPanels)
                    {
                        foreach (Controls ctrls in fromCol.controls)
                        {
                            if (ctrls.type.idPrefix == "ELT") childForms = ctrls.childForms;
                        }
                    }
                }
                if (listOfFiles.Length == 0)
                {
                    listOfFiles.Append(row.id.ToUpper() + ".html");
                }
                else
                {
                    listOfFiles.Append("," + row.id.ToUpper() + ".html");
                }
                if (childForms != null && childForms.Count > 0)
                {
                    foreach (AppForm frmChildApp in childForms)
                    {
                        string strChildformId = (row.id + "-" + frmChildApp.name.Substring(0, 1)).ToUpper();
                        if (listOfFiles.Length == 0)
                        {
                            listOfFiles.Append(strChildformId + ".html");
                        }
                        else
                        {
                            listOfFiles.Append("," + strChildformId + ".html");
                        }
                    }
                }
            }
        }

        private Stream downloadRemoteImageFile(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception)
            {
                throw new Exception();
            }

            // Check that the remote file was found. The ContentType
            // check is performed since a request for a non-existent
            // image file might be redirected to a 404-page, which would
            // yield the StatusCode "OK", even though the image was not
            // found.
            if ((response.StatusCode == HttpStatusCode.OK ||
                response.StatusCode == HttpStatusCode.Moved ||
                response.StatusCode == HttpStatusCode.Redirect) &&
                response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
            {
                return response.GetResponseStream();
            }
            else
                return null;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class RemoveContact
    {
        RemoveContactReq removeContactReq;
        System.Web.Caching.Cache appCache;

        public RemoveContact(RemoveContactReq _removeContactReq,System.Web.Caching.Cache _appCache)
        {
            removeContactReq = _removeContactReq;
            appCache = _appCache;
        }

        public RemoveContactResp Process()
        {
            SqlCommand cmd;
            string strContactId, strUserId;
            strContactId = Utilities.GetUserID(removeContactReq.CompanyId, removeContactReq.ContactName);
            strUserId = Utilities.GetUserID(removeContactReq.CompanyId, removeContactReq.UserName);
            ResponseStatus objRespStatus = new ResponseStatus();

            if (strContactId.Length > 0)
            {
                try
                {
                    cmd = new SqlCommand(@"select * from TBL_USER_CONTACT_FOR_MBUZZ WHERE ( USER_ID= @USER_ID1 or USER_ID= @USER_ID2)  AND COMPANY_ID= @COMPANY_ID");
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@USER_ID1", strUserId);
                    cmd.Parameters.AddWithValue("@USER_ID2", strContactId);
                    cmd.Parameters.AddWithValue("@COMPANY_ID", removeContactReq.CompanyId);
                    DataSet ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                    //----------------------------------------------------------------------------
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        UpdateUser(strUserId, strContactId, removeContactReq.CompanyId, ds);
                        UpdateUser(strContactId, strUserId, removeContactReq.CompanyId, ds);
                    }
                    objRespStatus.cd = "0";
                    objRespStatus.desc = "";
                    return new RemoveContactResp(objRespStatus, removeContactReq.RequestId);
                }
                catch (Exception ex)
                {
                    if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
            }
            objRespStatus.cd = "2000601";
            objRespStatus.desc = "User Not Found";
            return new RemoveContactResp(objRespStatus, removeContactReq.RequestId);
        }

        void UpdateUser(string userId, string contactId, string CompanyId, DataSet ds)
        {
            DataRow[] dr = ds.Tables[0].Select("USER_ID='" + userId + "'");
            if (dr.Length > 0)
            {
                string contacts = dr[0]["contacts"].ToString();
                contacts = (contacts.Replace(contactId, "")).Replace(",,", ",");

                SqlCommand cmd = new SqlCommand(@"UPDATE dbo.TBL_USER_CONTACT_FOR_MBUZZ SET CONTACTS = @CONTACTS WHERE USER_ID= @USER_ID AND COMPANY_ID= @COMPANY_ID");
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@USER_ID", userId);
                cmd.Parameters.AddWithValue("@COMPANY_ID", CompanyId);
                cmd.Parameters.AddWithValue("@CONTACTS", contacts);
                if (MSSqlDatabaseClient.ExecuteNonQueryRecord(cmd) <= 0)
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Net;
namespace mFicientWS
{
    public class SupportQueryResp
    {
        ResponseStatus _respStatus;
        string _requestId, _tokenId;
        bool _isSameReqBefore5Min;
        public SupportQueryResp(ResponseStatus respStatus, string requestId, string tokenId, bool isSameReqBefore5Min)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
                _tokenId = tokenId;
                _isSameReqBefore5Min = isSameReqBefore5Min;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Support Query Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            if (!_isSameReqBefore5Min)
            {
                SupportQueryResponse objSupportQueryResp = new SupportQueryResponse();
                objSupportQueryResp.func = Convert.ToString((int)FUNCTION_CODES.SUPPORT_QUERY);
                objSupportQueryResp.rid = _requestId;
                objSupportQueryResp.status = _respStatus;
                SupportQueryResponseData objResponseData = new SupportQueryResponseData();
                objResponseData.tn = _tokenId;
                objSupportQueryResp.data = objResponseData;
                string strJsonWithDetails = Utilities.SerializeJson<SupportQueryResponse>(objSupportQueryResp);
                return Utilities.getFinalJson(strJsonWithDetails);
            }
            else
            {
                return ((int)HttpStatusCode.Accepted).ToString();
            }
        }
    }

    public class SupportQueryResponse : CommonResponse
    {
        public SupportQueryResponse()
        {

        }
        [DataMember]
        public SupportQueryResponseData data { get; set; }
    }
    public class SupportQueryResponseData
    {
        /// <summary>
        /// session id
        /// </summary>
        [DataMember]
        public string tn { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class AddNewContact
    {
        AddNewContactReq addNewContactReq;

        public AddNewContact(AddNewContactReq _addNewContactReq)
        {
            addNewContactReq = _addNewContactReq;
        }

        public AddNewContactResp Process()
        {
            string strNewContactId, strUserId;
            strNewContactId = Utilities.GetUserID(addNewContactReq.CompanyId, addNewContactReq.NewContactName);
            strUserId = Utilities.GetUserID(addNewContactReq.CompanyId, addNewContactReq.UserName);
            ResponseStatus objRespStatus = new ResponseStatus();
            UserData objUserData = new UserData();
            if (strNewContactId.Length > 0 && strUserId.Length > 0)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(@"SELECT userDtl.* FROM tbl_user_detail as userDtl WHERE userDtl.user_id=@USER_ID and userDtl.COMPANY_ID=@COMPANY_ID;
                        select a.USER_NAME,b.GROUP_ID from TBL_USER_DETAIL a inner join TBL_USER_GROUP_LINK b on a.USER_ID=b.USER_ID and a.COMPANY_ID=b.COMPANY_ID where a.USER_ID=@TO_USER_ID and a.COMPANY_ID=@COMPANY_ID;");
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@USER_ID", strUserId);
                    cmd.Parameters.AddWithValue("@COMPANY_ID", addNewContactReq.CompanyId);
                    cmd.Parameters.AddWithValue("@TO_USER_ID", strNewContactId);
                    DataSet dsUserDetails = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                    if (dsUserDetails != null)
                    {
                         cmd = new SqlCommand(@"INSERT INTO TBL_ADD_CONTACT_REQUEST (  FROM_USER_ID, COMPANY_ID,TO_USER_ID,REQUEST_DATE_TIME)
                                                            VALUES(  @FROM_USER_ID, @COMPANY_ID, @TO_USER_ID, @REQUEST_DATE_TIME);");
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@FROM_USER_ID", strUserId);
                        cmd.Parameters.AddWithValue("@COMPANY_ID", addNewContactReq.CompanyId);
                        cmd.Parameters.AddWithValue("@TO_USER_ID", strNewContactId);
                        cmd.Parameters.AddWithValue("@REQUEST_DATE_TIME", DateTime.UtcNow.Ticks);

                        if (MSSqlDatabaseClient.ExecuteNonQueryRecord(cmd) > 0)
                        {                  
                            objUserData.istop = "1";
                            foreach (DataRow dr1 in dsUserDetails.Tables[1].Rows)
                            {
                                if (dsUserDetails.Tables[0].Rows[0]["REQUESTED_BY"].ToString().Contains(dr1["GROUP_ID"].ToString()))
                                {
                                    objUserData.istop = "0"; break;
                                }
                            }
                            objUserData.unm = dsUserDetails.Tables[0].Rows[0]["USER_NAME"].ToString();
                            objUserData.fnm = dsUserDetails.Tables[0].Rows[0]["FIRST_NAME"].ToString() + " " + dsUserDetails.Tables[0].Rows[0]["LAST_NAME"].ToString();
                            objUserData.dep = "";
                            objUserData.des = "";
                            objUserData.em = dsUserDetails.Tables[0].Rows[0]["EMAIL_ID"].ToString();
                            objUserData.status = "3";
                            objRespStatus.cd = "0";
                            objRespStatus.desc = "";
                            return new AddNewContactResp(objRespStatus, addNewContactReq.RequestId, objUserData);
                        }
                        else
                        {
                            throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                        }
                    }
                    else
                    {
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                    }
                }
                catch (Exception ex)
                {
                    if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
            }
            objRespStatus.cd = "2000401";
            objRespStatus.desc = "Request Not Saved";
            return new AddNewContactResp(objRespStatus, addNewContactReq.RequestId, null);
        }
    }
}
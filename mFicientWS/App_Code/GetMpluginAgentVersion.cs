﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
namespace mFicientWS
{
    public class GetMpluginAgentVersion
    {
        GetMpluginAgentVersionReq _objgetMpluginAgentVersionReq;
        public GetMpluginAgentVersion(GetMpluginAgentVersionReq objgetmpluginagentversion)
        {
            _objgetMpluginAgentVersionReq=objgetmpluginagentversion;
        }
        public GetMpluginAgentVersionResp Process()
        {
            ResponseStatus _respStatus = new ResponseStatus();
            _respStatus.cd = "0";
            _respStatus.desc = "";
            try
            {
                string appVersion = mPlugin.GetmPluginAgentVersionDetailsData(_objgetMpluginAgentVersionReq.CompanyId, _objgetMpluginAgentVersionReq.AgentName, _objgetMpluginAgentVersionReq.AgentPassword);
                return new GetMpluginAgentVersionResp(_respStatus, _objgetMpluginAgentVersionReq.RequestId, _objgetMpluginAgentVersionReq.CompanyId, appVersion.Split('.')[0], appVersion.Split('.')[1]);
            }
            catch (mPlugin.mPluginException e)
            {
                _respStatus.cd = "1";
                _respStatus.desc = e.Message;
              return new GetMpluginAgentVersionResp(_respStatus, _objgetMpluginAgentVersionReq.RequestId, _objgetMpluginAgentVersionReq.CompanyId,"","");
            }
            catch (HttpException e)
            {
                throw (new Exception(Convert.ToString(e.GetHttpCode())));
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
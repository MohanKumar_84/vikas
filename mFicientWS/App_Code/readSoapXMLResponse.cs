﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Collections;
using System.Xml.XPath;
namespace mFicientWS
{
    public class readSoapXMLResponse
    {
        DoTaskReq _doTaskRequest;
        string _responseXML;
        List<IdeWsParam> _outputTagPaths;
        WebServiceCommand _wsCmd;
        public readSoapXMLResponse(DoTaskReq doTaskRequest,
                    string responseXML,
                    WebServiceCommand wsCmd)
        {
            _doTaskRequest = doTaskRequest;
            _responseXML = responseXML;
            _outputTagPaths = wsCmd.OutPutTagPaths;
            _wsCmd = wsCmd;
        }
        //public Object Process()
        //{
        //    XmlDocument document = new XmlDocument();
        //    document.LoadXml(_responseXML);
        //    Hashtable hshTblOfObjectFormed = new Hashtable();
        //    if (_doTaskRequest.ReturnFunction == (int)RETURN_FUNCTION_TYPE_DOTASK.REPEATER_DATA)
        //    {
        //        return getListOfRepeaterDataForSoapXML(document,
        //           this.WsCmd.DataSetPath,
        //           this.OutputTagPaths
        //           );
        //    }
        //    else if (_doTaskRequest.ReturnFunction == (int)RETURN_FUNCTION_TYPE_DOTASK.DROP_DOWN_DATA || _doTaskRequest.ReturnFunction == (int)RETURN_FUNCTION_TYPE_DOTASK.PIE_DATA)
        //    {
        //        return getListOfDropDownOrPieDataForSoapXML(document,
        //           this.WsCmd.DataSetPath,
        //           this.OutputTagPaths
        //           );
        //    }
        //    else if (_doTaskRequest.ReturnFunction == (int)RETURN_FUNCTION_TYPE_DOTASK.BAR_DATA || _doTaskRequest.ReturnFunction == (int)RETURN_FUNCTION_TYPE_DOTASK.LINE_DATA)
        //    {
        //        return getListOfBarLineDataForSoapXML(document,
        //           this.WsCmd.DataSetPath,
        //           this.OutputTagPaths
        //           );
        //    }
        //    return null;
        //}

        //List<rptData> getListOfRepeaterDataForSoapXML(XmlDocument document,
        //    string datasetPath,
        //    List<IdeWsParam> outputPathsLst
        //    )
        //{
        //    //Algorithm
        //    //get modified path of each element
        //    //find count of the node list.if throws an exception then throw the exception that tag not found.
        //    //the one with the greatest count ,means that many object are to be formed.
        //    //if the tag is not part of an array,then the valie goes to all the objects.
        //    //if the tag path is empty then also empty is returned to all objects.

        //    int iCountOfLoop = 0;
        //    string strCompletePath = String.Empty;
        //    rptData objRptData = null;
        //    Hashtable hshTblOfObjectFormed = new Hashtable();
        //    Hashtable hashOfTagsWithSingleValue = new Hashtable();
        //    List<rptData> lstRptData = new List<rptData>();

        //    int iLargestCountOfTags = 0;
        //    XmlNodeList nodeListI = null, nodeListB = null, nodeListS = null, nodeListR = null;
        //    string tagPath = String.Empty;

        //    //get modified path of each element
        //    //find count of the node list.if throws and exption then thorw the exception that tag not found.
        //    //for i
        //    strCompletePath = getCompleteOutputTagPath(datasetPath, _doTaskRequest.returnType.i, outputPathsLst);
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(strCompletePath);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListI = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = nodeListI.Count;
        //            if (nodeListI.Count == 1)//only a single tag is returned
        //            {
        //                hashOfTagsWithSingleValue.Add("i", nodeListI[0].InnerText);
        //                nodeListI = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("i", "");
        //        }
        //    }
        //    catch (XPathException ex)
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    //for b
        //    strCompletePath = getCompleteOutputTagPath(datasetPath, _doTaskRequest.returnType.b, outputPathsLst);
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(strCompletePath);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListB = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListB.Count ? nodeListB.Count : iLargestCountOfTags;
        //            if (nodeListB.Count == 1)//only a single tag is returned
        //            {
        //                hashOfTagsWithSingleValue.Add("b", nodeListB[0].InnerText);
        //                nodeListB = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("b", "");
        //        }
        //    }
        //    catch (XPathException ex)
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    //for s
        //    strCompletePath = getCompleteOutputTagPath(datasetPath, _doTaskRequest.returnType.s, outputPathsLst);
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(strCompletePath);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListS = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListS.Count ? nodeListS.Count : iLargestCountOfTags;
        //            if (nodeListS.Count == 1)//only a single tag is returned
        //            {
        //                hashOfTagsWithSingleValue.Add("s", nodeListS[0].InnerText);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListS = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("s", "");
        //        }
        //    }
        //    catch (XPathException ex)
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    //for r
        //    strCompletePath = getCompleteOutputTagPath(datasetPath, _doTaskRequest.returnType.r, outputPathsLst);
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(strCompletePath);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListR = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListR.Count ? nodeListR.Count : iLargestCountOfTags;
        //            if (nodeListR.Count == 1)//only a single tag is returned
        //            {
        //                hashOfTagsWithSingleValue.Add("r", nodeListR[0].InnerText);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListR = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("r", "");
        //        }
        //    }
        //    catch (XPathException ex)
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    //the one with the greatest count ,means that many object are to be formed.
        //    for (int i = 0; i <= iLargestCountOfTags - 1; i++)
        //    {
        //        objRptData = new rptData();
        //        //fill object with default vlue as string.Empty
        //        objRptData.i = "";
        //        objRptData.b = "";
        //        objRptData.r = "";
        //        objRptData.s = "";
        //        hshTblOfObjectFormed.Add(i, objRptData);
        //    }
        //    //if the tag is not part of an array,then the valie goes to all the objects.
        //    foreach (DictionaryEntry rptObject in hshTblOfObjectFormed)
        //    {
        //        objRptData = (rptData)rptObject.Value;
        //        foreach (DictionaryEntry tag in hashOfTagsWithSingleValue)
        //        {
        //            if (tag.Key.ToString().ToLower() == "i")
        //            {
        //                objRptData.i = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "b")
        //            {
        //                objRptData.b = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "s")
        //            {
        //                objRptData.s = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "r")
        //            {
        //                objRptData.r = Convert.ToString(tag.Value);
        //            }
        //        }
        //    }
        //    //get i
        //    if (nodeListI != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListI)
        //        {
        //            objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objRptData.i = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }
        //    //get b
        //    if (nodeListB != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListB)
        //        {
        //            objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objRptData.b = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }
        //    //get s
        //    if (nodeListS != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListS)
        //        {
        //            objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objRptData.s = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }
        //    //get r
        //    if (nodeListR != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListR)
        //        {
        //            objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objRptData.r = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }


        //    //form the list and return
        //    foreach (Object obj in hshTblOfObjectFormed.Values)
        //    {
        //        objRptData = (rptData)obj;
        //        lstRptData.Add(objRptData);
        //    }
        //    return lstRptData;
        //}
        //List<Pie_ddl_Data> getListOfDropDownOrPieDataForSoapXML(XmlDocument document,
        //    string datasetPath,
        //    List<IdeWsParam> outputPathsLst)
        //{
        //    //Algorithm
        //    //get modified path of each element
        //    //find count of the node list.if throws and exption then thorw the exception that tag not found.
        //    //the one with the greatest count ,means that many object are to be formed.
        //    //if the tag is not part of an array,then the valie goes to all the objects.
        //    //if the tag path is empty then also empty is returned to all objects.

        //    int iCountOfLoop = 0;
        //    string strCompletePath = String.Empty;
        //    string tagPath = String.Empty;
        //    Pie_ddl_Data objDropDownData = null;
        //    Hashtable hshTblOfObjectFormed = new Hashtable();
        //    Hashtable hashOfTagsWithSingleValue = new Hashtable();
        //    List<Pie_ddl_Data> lstDropDownData = new List<Pie_ddl_Data>();

        //    int iLargestCountOfTags = 0;
        //    XmlNodeList nodeListT = null, nodeListV = null;

        //    //get modified path of each element
        //    //find count of the node list.if throws and exption then thorw the exception that tag not found.
        //    //for t
        //    strCompletePath = getCompleteOutputTagPath(datasetPath, _doTaskRequest.returnType.t, outputPathsLst);
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(strCompletePath);
        //    //string tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.t, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListT = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = nodeListT.Count;
        //            if (nodeListT.Count == 1)
        //            {
        //                hashOfTagsWithSingleValue.Add("t", nodeListT[0].InnerText);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListT = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("t", "");
        //        }
        //    }
        //    catch (XPathException ex)
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    //for v
        //    strCompletePath = getCompleteOutputTagPath(datasetPath, _doTaskRequest.returnType.v, outputPathsLst);
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(strCompletePath);
        //    //tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.v, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListV = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListV.Count ? nodeListV.Count : iLargestCountOfTags;
        //            if (nodeListV.Count == 1)
        //            {
        //                hashOfTagsWithSingleValue.Add("v", nodeListV[0].InnerText);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListV = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("v", "");
        //        }
        //    }
        //    catch (XPathException ex)
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    catch
        //    {
        //        throw;
        //    }

        //    //the one with the greatest count ,means that many object are to be formed.
        //    for (int i = 0; i <= iLargestCountOfTags - 1; i++)
        //    {
        //        objDropDownData = new Pie_ddl_Data();
        //        //fill object with default vlue as string.Empty
        //        objDropDownData.t = "";
        //        objDropDownData.v = "";
        //        hshTblOfObjectFormed.Add(i, objDropDownData);
        //    }
        //    //if the tag is not part of an array,then the valie goes to all the objects.
        //    foreach (DictionaryEntry ddlObject in hshTblOfObjectFormed)
        //    {
        //        objDropDownData = (Pie_ddl_Data)ddlObject.Value;
        //        foreach (DictionaryEntry tag in hashOfTagsWithSingleValue)
        //        {
        //            if (tag.Key.ToString().ToLower() == "t")
        //            {
        //                objDropDownData.t = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "v")
        //            {
        //                objDropDownData.v = Convert.ToString(tag.Value);
        //            }
        //        }
        //    }
        //    //get t
        //    if (nodeListT != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListT)
        //        {
        //            objDropDownData = (Pie_ddl_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objDropDownData.t = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }
        //    //get v
        //    if (nodeListV != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListV)
        //        {
        //            objDropDownData = (Pie_ddl_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objDropDownData.v = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }

        //    //form the list and return
        //    foreach (Object obj in hshTblOfObjectFormed.Values)
        //    {
        //        objDropDownData = (Pie_ddl_Data)obj;
        //        lstDropDownData.Add(objDropDownData);
        //    }
        //    return lstDropDownData;
        //}

        //List<Bar_Line_Data> getListOfBarLineDataForSoapXML(XmlDocument document,
        //    string datasetPath,
        //    List<IdeWsParam> outputPathsLst)
        //{
        //    //Algorithm
        //    //get modified path of each element
        //    //find count of the node list.if throws and exption then thorw the exception that tag not found.
        //    //the one with the greatest count ,means that many object are to be formed.
        //    //if the tag is not part of an array,then the valie goes to all the objects.
        //    //if the tag path is empty then also empty is returned to all objects.

        //    int iCountOfLoop = 0;
        //    string tagPath = String.Empty, strCompletePath = String.Empty;
        //    Bar_Line_Data objBarLineData = null;
        //    Hashtable hshTblOfObjectFormed = new Hashtable();
        //    Hashtable hashOfTagsWithSingleValue = new Hashtable();
        //    List<Bar_Line_Data> lstBarLineData = new List<Bar_Line_Data>();

        //    int iLargestCountOfTags = 0;
        //    XmlNodeList nodeListT = null, nodeListV1 = null, nodeListV2 = null, nodeListV3 = null, nodeListV4 = null, nodeListV5 = null;

        //    //get modified path of each element
        //    //find count of the node list.if throws and exption then thorw the exception that tag not found.
        //    //for t
        //    strCompletePath = getCompleteOutputTagPath(datasetPath, _doTaskRequest.returnType.t, outputPathsLst);
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(strCompletePath);
        //    //string tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.t, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListT = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = nodeListT.Count;
        //            if (nodeListT.Count == 1)
        //            {
        //                hashOfTagsWithSingleValue.Add("t", nodeListT[0].InnerText);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListT = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("t", "");
        //        }
        //    }
        //    catch (XPathException ex)
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    //for v1
        //    strCompletePath = getCompleteOutputTagPath(datasetPath, _doTaskRequest.returnType.v1, outputPathsLst);
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(strCompletePath);
        //    //tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.v1, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListV1 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListV1.Count ? nodeListV1.Count : iLargestCountOfTags;
        //            if (nodeListV1.Count == 1)
        //            {
        //                hashOfTagsWithSingleValue.Add("v1", nodeListV1[0].InnerText);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListV1 = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("v1", "");
        //        }
        //    }
        //    catch (XPathException ex)
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    //for v2
        //    strCompletePath = getCompleteOutputTagPath(datasetPath, _doTaskRequest.returnType.v2, outputPathsLst);
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(strCompletePath);
        //    //tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.v2, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListV2 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListV2.Count ? nodeListV2.Count : iLargestCountOfTags;
        //            if (nodeListV2.Count == 1)
        //            {
        //                hashOfTagsWithSingleValue.Add("v2", nodeListV2[0].InnerText);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListV2 = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("v2", "");
        //        }
        //    }
        //    catch (XPathException ex)
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    //for v3
        //    strCompletePath = getCompleteOutputTagPath(datasetPath, _doTaskRequest.returnType.v3, outputPathsLst);
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(strCompletePath);
        //    //tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.v3, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListV3 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListV3.Count ? nodeListV3.Count : iLargestCountOfTags;
        //            if (nodeListV3.Count == 1)
        //            {
        //                hashOfTagsWithSingleValue.Add("v3", nodeListV3[0].InnerText);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListV3 = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("v3", "");
        //        }
        //    }
        //    catch (XPathException ex)
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    //for v4
        //    strCompletePath = getCompleteOutputTagPath(datasetPath, _doTaskRequest.returnType.v4, outputPathsLst);
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(strCompletePath);
        //    //tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.v4, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListV4 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListV4.Count ? nodeListV4.Count : iLargestCountOfTags;
        //            if (nodeListV4.Count == 1)
        //            {
        //                hashOfTagsWithSingleValue.Add("v4", nodeListV4[0].InnerText);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListV4 = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("v4", "");
        //        }
        //    }
        //    catch (XPathException ex)
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    //for v5
        //    strCompletePath = getCompleteOutputTagPath(datasetPath, _doTaskRequest.returnType.v5, outputPathsLst);
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(strCompletePath);
        //    //tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.v5, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListV5 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListV5.Count ? nodeListV5.Count : iLargestCountOfTags;
        //            if (nodeListV5.Count == 1)
        //            {
        //                hashOfTagsWithSingleValue.Add("v5", nodeListV5[0].InnerText);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListV5 = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("v5", "");
        //        }
        //    }
        //    catch (XPathException ex)
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    //the one with the greatest count ,means that many object are to be formed.
        //    for (int i = 0; i <= iLargestCountOfTags - 1; i++)
        //    {
        //        objBarLineData = new Bar_Line_Data();
        //        //fill object with default vlue as string.Empty
        //        objBarLineData.t = "";
        //        objBarLineData.v1 = "";
        //        objBarLineData.v2 = "";
        //        objBarLineData.v3 = "";
        //        objBarLineData.v4 = "";
        //        objBarLineData.v5 = "";
        //        hshTblOfObjectFormed.Add(i, objBarLineData);
        //    }
        //    //if the tag is not part of an array,then the valie goes to all the objects.
        //    foreach (DictionaryEntry barLineObject in hshTblOfObjectFormed)
        //    {
        //        objBarLineData = (Bar_Line_Data)barLineObject.Value;
        //        foreach (DictionaryEntry tag in hashOfTagsWithSingleValue)
        //        {
        //            if (tag.Key.ToString().ToLower() == "t".ToLower())
        //            {
        //                objBarLineData.t = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "v1".ToLower())
        //            {
        //                objBarLineData.v1 = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "v2".ToLower())
        //            {
        //                objBarLineData.v2 = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "v3".ToLower())
        //            {
        //                objBarLineData.v3 = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "v4".ToLower())
        //            {
        //                objBarLineData.v4 = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "v5".ToLower())
        //            {
        //                objBarLineData.v5 = Convert.ToString(tag.Value);
        //            }
        //        }
        //    }
        //    //get t
        //    if (nodeListT != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListT)
        //        {
        //            objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objBarLineData.t = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }
        //    //get v1
        //    if (nodeListV1 != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListV1)
        //        {
        //            objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objBarLineData.v1 = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }
        //    //get v2
        //    if (nodeListV2 != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListV2)
        //        {
        //            objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objBarLineData.v2 = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }
        //    //get v3
        //    if (nodeListV3 != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListV3)
        //        {
        //            objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objBarLineData.v3 = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }

        //    //get v4
        //    if (nodeListV4 != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListV4)
        //        {
        //            objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objBarLineData.v4 = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }

        //    //get v5
        //    if (nodeListV5 != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListV5)
        //        {
        //            objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objBarLineData.v5 = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }

        //    //form the list and return
        //    foreach (Object obj in hshTblOfObjectFormed.Values)
        //    {
        //        objBarLineData = (Bar_Line_Data)obj;
        //        lstBarLineData.Add(objBarLineData);
        //    }
        //    return lstBarLineData;
        //}
        //List<rptData> getListOfRepeaterDataForSoapXML(XmlDocument document)
        //{
        //    //Algorithm
        //    //get modified path of each element
        //    //find count of the node list.if throws an exception then throw the exception that tag not found.
        //    //the one with the greatest count ,means that many object are to be formed.
        //    //if the tag is not part of an array,then the valie goes to all the objects.
        //    //if the tag path is empty then also empty is returned to all objects.

        //    int iCountOfLoop = 0;
        //    rptData objRptData = null;
        //    Hashtable hshTblOfObjectFormed = new Hashtable();
        //    Hashtable hashOfTagsWithSingleValue = new Hashtable();
        //    List<rptData> lstRptData = new List<rptData>();

        //    int iLargestCountOfTags = 0;
        //    XmlNodeList nodeListI = null, nodeListB = null, nodeListS = null, nodeListR = null;
        //    bool blnIsPartOfAnArray;
        //    string tagPath = "";

        //    //get modified path of each element
        //    //find count of the node list.if throws and exption then thorw the exception that tag not found.

        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.i, out blnIsPartOfAnArray);

        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            //nodeListI = document.SelectNodes("//*[local-name()='" + methodPartName + "']" + tagPath, new XmlNamespaceManager(document.NameTable));
        //            nodeListI = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = nodeListI.Count;
        //            if (!blnIsPartOfAnArray)
        //            {
        //                hashOfTagsWithSingleValue.Add("i", nodeListI[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListI = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("i", "");
        //        }
        //    }
        //    catch
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.b, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListB = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListB.Count ? nodeListB.Count : iLargestCountOfTags;
        //            if (!blnIsPartOfAnArray)
        //            {
        //                hashOfTagsWithSingleValue.Add("b", nodeListB[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListB = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("b", "");
        //        }
        //    }
        //    catch
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.s, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListS = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListS.Count ? nodeListS.Count : iLargestCountOfTags;
        //            if (!blnIsPartOfAnArray)
        //            {
        //                hashOfTagsWithSingleValue.Add("s", nodeListS[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListS = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("s", "");
        //        }
        //    }
        //    catch
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }

        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.r, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListR = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListR.Count ? nodeListR.Count : iLargestCountOfTags;
        //            if (!blnIsPartOfAnArray)
        //            {
        //                hashOfTagsWithSingleValue.Add("r", nodeListR[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListR = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("r", "");
        //        }
        //    }
        //    catch
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    //the one with the greatest count ,means that many object are to be formed.
        //    for (int i = 0; i <= iLargestCountOfTags - 1; i++)
        //    {
        //        objRptData = new rptData();
        //        //fill object with default vlue as string.Empty
        //        objRptData.i = "";
        //        objRptData.b = "";
        //        objRptData.r = "";
        //        objRptData.s = "";
        //        hshTblOfObjectFormed.Add(i, objRptData);
        //    }
        //    //if the tag is not part of an array,then the valie goes to all the objects.
        //    foreach (DictionaryEntry rptObject in hshTblOfObjectFormed)
        //    {
        //        objRptData = (rptData)rptObject.Value;
        //        foreach (DictionaryEntry tag in hashOfTagsWithSingleValue)
        //        {
        //            if (tag.Key.ToString().ToLower() == "i")
        //            {
        //                objRptData.i = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "b")
        //            {
        //                objRptData.b = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "s")
        //            {
        //                objRptData.s = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "r")
        //            {
        //                objRptData.r = Convert.ToString(tag.Value);
        //            }
        //        }
        //    }
        //    //get i
        //    if (nodeListI != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListI)
        //        {
        //            objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objRptData.i = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }
        //    //get b
        //    if (nodeListB != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListB)
        //        {
        //            objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objRptData.b = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }
        //    //get s
        //    if (nodeListS != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListS)
        //        {
        //            objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objRptData.s = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }
        //    //get r
        //    if (nodeListR != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListR)
        //        {
        //            objRptData = (rptData)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objRptData.r = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }


        //    //form the list and return
        //    foreach (Object obj in hshTblOfObjectFormed.Values)
        //    {
        //        objRptData = (rptData)obj;
        //        lstRptData.Add(objRptData);
        //    }
        //    return lstRptData;
        //}

        //List<Pie_ddl_Data> getListOfDropDownOrPieDataForSoapXML(XmlDocument document)
        //{
        //    //Algorithm
        //    //get modified path of each element
        //    //find count of the node list.if throws and exption then thorw the exception that tag not found.
        //    //the one with the greatest count ,means that many object are to be formed.
        //    //if the tag is not part of an array,then the valie goes to all the objects.
        //    //if the tag path is empty then also empty is returned to all objects.

        //    int iCountOfLoop = 0;
        //    Pie_ddl_Data objDropDownData = null;
        //    Hashtable hshTblOfObjectFormed = new Hashtable();
        //    Hashtable hashOfTagsWithSingleValue = new Hashtable();
        //    List<Pie_ddl_Data> lstDropDownData = new List<Pie_ddl_Data>();

        //    int iLargestCountOfTags = 0;
        //    XmlNodeList nodeListT = null, nodeListV = null;
        //    bool blnIsPartOfAnArray;

        //    //get modified path of each element
        //    //find count of the node list.if throws and exption then thorw the exception that tag not found.
        //    string tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.t, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListT = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = nodeListT.Count;
        //            if (!blnIsPartOfAnArray)
        //            {
        //                hashOfTagsWithSingleValue.Add("t", nodeListT[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListT = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("t", "");
        //        }
        //    }
        //    catch
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.v, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListV = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListV.Count ? nodeListV.Count : iLargestCountOfTags;
        //            if (!blnIsPartOfAnArray)
        //            {
        //                hashOfTagsWithSingleValue.Add("v", nodeListV[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListV = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("v", "");
        //        }
        //    }
        //    catch
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }

        //    //the one with the greatest count ,means that many object are to be formed.
        //    for (int i = 0; i <= iLargestCountOfTags - 1; i++)
        //    {
        //        objDropDownData = new Pie_ddl_Data();
        //        //fill object with default vlue as string.Empty
        //        objDropDownData.t = "";
        //        objDropDownData.v = "";
        //        hshTblOfObjectFormed.Add(i, objDropDownData);
        //    }
        //    //if the tag is not part of an array,then the valie goes to all the objects.
        //    foreach (DictionaryEntry ddlObject in hshTblOfObjectFormed)
        //    {
        //        objDropDownData = (Pie_ddl_Data)ddlObject.Value;
        //        foreach (DictionaryEntry tag in hashOfTagsWithSingleValue)
        //        {
        //            if (tag.Key.ToString().ToLower() == "t")
        //            {
        //                objDropDownData.t = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "v")
        //            {
        //                objDropDownData.v = Convert.ToString(tag.Value);
        //            }
        //        }
        //    }
        //    //get t
        //    if (nodeListT != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListT)
        //        {
        //            objDropDownData = (Pie_ddl_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objDropDownData.t = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }
        //    //get v
        //    if (nodeListV != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListV)
        //        {
        //            objDropDownData = (Pie_ddl_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objDropDownData.v = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }

        //    //form the list and return
        //    foreach (Object obj in hshTblOfObjectFormed.Values)
        //    {
        //        objDropDownData = (Pie_ddl_Data)obj;
        //        lstDropDownData.Add(objDropDownData);
        //    }
        //    return lstDropDownData;
        //}

        //List<Bar_Line_Data> getListOfBarLineDataForSoapXML(XmlDocument document)
        //{
        //    //Algorithm
        //    //get modified path of each element
        //    //find count of the node list.if throws and exption then thorw the exception that tag not found.
        //    //the one with the greatest count ,means that many object are to be formed.
        //    //if the tag is not part of an array,then the valie goes to all the objects.
        //    //if the tag path is empty then also empty is returned to all objects.

        //    int iCountOfLoop = 0;
        //    Bar_Line_Data objBarLineData = null;
        //    Hashtable hshTblOfObjectFormed = new Hashtable();
        //    Hashtable hashOfTagsWithSingleValue = new Hashtable();
        //    List<Bar_Line_Data> lstBarLineData = new List<Bar_Line_Data>();

        //    int iLargestCountOfTags = 0;
        //    XmlNodeList nodeListT = null, nodeListV1 = null, nodeListV2 = null, nodeListV3 = null, nodeListV4 = null, nodeListV5 = null;
        //    bool blnIsPartOfAnArray;

        //    //get modified path of each element
        //    //find count of the node list.if throws and exption then thorw the exception that tag not found.
        //    string tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.t, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListT = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = nodeListT.Count;
        //            if (!blnIsPartOfAnArray)
        //            {
        //                hashOfTagsWithSingleValue.Add("t", nodeListT[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListT = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("t", "");
        //        }
        //    }
        //    catch
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.v1, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListV1 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListV1.Count ? nodeListV1.Count : iLargestCountOfTags;
        //            if (!blnIsPartOfAnArray)
        //            {
        //                hashOfTagsWithSingleValue.Add("v1", nodeListV1[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListV1 = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("v1", "");
        //        }
        //    }
        //    catch
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.v2, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListV2 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListV2.Count ? nodeListV2.Count : iLargestCountOfTags;
        //            if (!blnIsPartOfAnArray)
        //            {
        //                hashOfTagsWithSingleValue.Add("v2", nodeListV2[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListV2 = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("v2", "");
        //        }
        //    }
        //    catch
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }

        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.v3, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListV3 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListV3.Count ? nodeListV3.Count : iLargestCountOfTags;
        //            if (!blnIsPartOfAnArray)
        //            {
        //                hashOfTagsWithSingleValue.Add("v3", nodeListV3[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListV3 = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("v3", "");
        //        }
        //    }
        //    catch
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }

        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.v4, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListV4 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListV4.Count ? nodeListV4.Count : iLargestCountOfTags;
        //            if (!blnIsPartOfAnArray)
        //            {
        //                hashOfTagsWithSingleValue.Add("v4", nodeListV4[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListV4 = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("v4", "");
        //        }
        //    }
        //    catch
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }

        //    tagPath = getModifiedPathOfTagForSoapAndXMLRPC(_doTaskRequest.returnType.v5, out blnIsPartOfAnArray);
        //    try
        //    {
        //        if (!String.IsNullOrEmpty(tagPath))
        //        {
        //            nodeListV5 = document.SelectNodes(tagPath, new XmlNamespaceManager(document.NameTable));
        //            iLargestCountOfTags = iLargestCountOfTags < nodeListV5.Count ? nodeListV5.Count : iLargestCountOfTags;
        //            if (!blnIsPartOfAnArray)
        //            {
        //                hashOfTagsWithSingleValue.Add("v5", nodeListV5[0].Value);//if it is not part of an aarray then there will be only one value (ie only one node)
        //                nodeListV5 = null;
        //            }
        //        }
        //        else
        //        {
        //            hashOfTagsWithSingleValue.Add("v5", "");
        //        }
        //    }
        //    catch
        //    {
        //        throw new Exception(((int)DO_TASK_ERROR.INVALID_TAG_PATH_PROVIDED).ToString());
        //    }
        //    //the one with the greatest count ,means that many object are to be formed.
        //    for (int i = 0; i <= iLargestCountOfTags - 1; i++)
        //    {
        //        objBarLineData = new Bar_Line_Data();
        //        //fill object with default vlue as string.Empty
        //        objBarLineData.t = "";
        //        objBarLineData.v1 = "";
        //        objBarLineData.v2 = "";
        //        objBarLineData.v3 = "";
        //        objBarLineData.v4 = "";
        //        objBarLineData.v5 = "";
        //        hshTblOfObjectFormed.Add(i, objBarLineData);
        //    }
        //    //if the tag is not part of an array,then the valie goes to all the objects.
        //    foreach (DictionaryEntry barLineObject in hshTblOfObjectFormed)
        //    {
        //        objBarLineData = (Bar_Line_Data)barLineObject.Value;
        //        foreach (DictionaryEntry tag in hashOfTagsWithSingleValue)
        //        {
        //            if (tag.Key.ToString().ToLower() == "t".ToLower())
        //            {
        //                objBarLineData.t = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "v1".ToLower())
        //            {
        //                objBarLineData.v1 = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "v2".ToLower())
        //            {
        //                objBarLineData.v2 = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "v3".ToLower())
        //            {
        //                objBarLineData.v3 = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "v4".ToLower())
        //            {
        //                objBarLineData.v4 = Convert.ToString(tag.Value);
        //            }
        //            else if (tag.Key.ToString().ToLower() == "v5".ToLower())
        //            {
        //                objBarLineData.v5 = Convert.ToString(tag.Value);
        //            }
        //        }
        //    }
        //    //get t
        //    if (nodeListT != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListT)
        //        {
        //            objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objBarLineData.t = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }
        //    //get v1
        //    if (nodeListV1 != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListV1)
        //        {
        //            objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objBarLineData.v1 = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }
        //    //get v2
        //    if (nodeListV2 != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListV2)
        //        {
        //            objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objBarLineData.v2 = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }
        //    //get v3
        //    if (nodeListV3 != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListV3)
        //        {
        //            objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objBarLineData.v3 = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }

        //    //get v4
        //    if (nodeListV4 != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListV4)
        //        {
        //            objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objBarLineData.v4 = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }

        //    //get v5
        //    if (nodeListV5 != null)
        //    {
        //        iCountOfLoop = 0;
        //        foreach (XmlNode node in nodeListV5)
        //        {
        //            objBarLineData = (Bar_Line_Data)hshTblOfObjectFormed[(int)iCountOfLoop];
        //            objBarLineData.v5 = node.InnerText.Trim();
        //            iCountOfLoop++;
        //        }
        //    }

        //    //form the list and return
        //    foreach (Object obj in hshTblOfObjectFormed.Values)
        //    {
        //        objBarLineData = (Bar_Line_Data)obj;
        //        lstBarLineData.Add(objBarLineData);
        //    }
        //    return lstBarLineData;
        //}

        //string getModifiedPathOfTagForSoapAndXMLRPC(string originalTagPath, out bool isPartOfAnArray)
        //{
        //    string strModifiedTagPath = "";
        //    isPartOfAnArray = false;
        //    string strLocalnamePrefix = "/*[local-name()='";
        //    string strLocalNameSuffix = "']";
        //    if (!String.IsNullOrEmpty(originalTagPath))
        //    {
        //        string[] arySplitTagPath = originalTagPath.Split('.');
        //        Hashtable hashOfTagsFromed = new Hashtable();
        //        for (int i = 0; i < arySplitTagPath.Length; i++)
        //        {
        //            if (arySplitTagPath[i].ToLower().StartsWith("arrayof"))//if its an array of then also we will get that tag after dot.so no need to process it here.But it shows that tag is an array.
        //            {
        //                isPartOfAnArray = true;
        //                continue;
        //            }
        //            if (hashOfTagsFromed.ContainsKey(arySplitTagPath[i]))
        //            {
        //                continue;
        //            }
        //            if (strModifiedTagPath.Length == 0)
        //            {
        //                strModifiedTagPath += "//*[local-name()='" + arySplitTagPath[i] + strLocalNameSuffix;
        //            }
        //            else
        //            {
        //                strModifiedTagPath += strLocalnamePrefix + arySplitTagPath[i] + strLocalNameSuffix;
        //            }
        //            hashOfTagsFromed.Add(arySplitTagPath[i], arySplitTagPath[i]);
        //        }
        //    }
        //    return strModifiedTagPath;
        //}

        string getModifiedPathOfTagForSoapAndXMLRPC(string completeTagPath)
        {
            string strModifiedTagPath = String.Empty;
            string strLocalnamePrefix = "/*[local-name()='";
            string strLocalNameSuffix = "']";
            if (!String.IsNullOrEmpty(completeTagPath))
            {
                string[] arySplitTagPath = completeTagPath.Split('/');
                Hashtable hashOfTagsFromed = new Hashtable();
                for (int i = 0; i < arySplitTagPath.Length; i++)
                {
                    if (strModifiedTagPath.Length == 0)
                    {
                        strModifiedTagPath += "//*[local-name()='" + arySplitTagPath[i] + strLocalNameSuffix;
                    }
                    else
                    {
                        strModifiedTagPath += strLocalnamePrefix + arySplitTagPath[i] + strLocalNameSuffix;
                    }
                    hashOfTagsFromed.Add(arySplitTagPath[i], arySplitTagPath[i]);
                }
            }
            return strModifiedTagPath;
        }


        string getCompleteOutputTagPath(string datasetPath,
            string nameOfOutputTag
            , List<IdeWsParam> outputPathsLst)
        {
            if (!String.IsNullOrEmpty(datasetPath))
            {
                return getTheDatasetPathWithoutTheFinalArrayTag(datasetPath) + "/" + getOutputPathOfTagFromPathList(nameOfOutputTag, outputPathsLst);
            }
            else
            {
                return getOutputPathOfTagFromPathList(nameOfOutputTag, outputPathsLst);
            }
        }
        string getTheDatasetPathWithoutTheFinalArrayTag(string datasetPath)
        {
            /****
             * If Dataset Path is present then the last tag provided will be an array ending as Tagname[].
             * Eg: GetProductsResponse.GetProductsResult.Distributor[]
             * Result Path:Distributor.Name
             * Removing the last Distributor[] tag.
             * ****/
            if (String.IsNullOrEmpty(datasetPath)) throw new ArgumentNullException();
            //string strDataSetPath = datasetPath.Replace('.', '/');
            //   strDataSetPath=  strDataSetPath.Substring(0,
            //    strDataSetPath.Length - (strDataSetPath.Substring(strDataSetPath.LastIndexOf('/')).Length + 1));
            string strDataSetPath = String.Empty;
            string[] aryStrSplitValues = datasetPath.Replace('.', '/').Split('/');
            int iLengthOfAryAfterSplit = aryStrSplitValues.Length;
            //Normally we should not get this case
            if (iLengthOfAryAfterSplit == 1) return strDataSetPath;
            if (aryStrSplitValues.Length > 1)
            {
                for (int i = 0; i <= iLengthOfAryAfterSplit - 2; i++)
                {
                    if (String.IsNullOrEmpty(strDataSetPath))
                    {
                        strDataSetPath += aryStrSplitValues[i];
                    }
                    else
                    {
                        strDataSetPath += "/" + aryStrSplitValues[i];
                    }
                }
            }
            else
            {
                strDataSetPath = String.Empty;
            }
            return strDataSetPath;
        }
        string getOutputPathOfTagFromPathList(string nameOfOutputTag, List<IdeWsParam> outputPathsLst)
        {
            string strOutputTagPath = String.Empty;
            foreach (IdeWsParam outputTagAndPath in outputPathsLst)
            {
                //Comparing the string as it is sent from the mobile end.Not converting toLower in case there is an error
                //from mobile end.
                if (outputTagAndPath.name == nameOfOutputTag)
                {
                    strOutputTagPath =
                        String.IsNullOrEmpty(outputTagAndPath.path) ?
                        String.Empty :
                        outputTagAndPath.path.Replace('.', '/');
                    ;

                    break;
                }
                else
                    continue;
            }
            return strOutputTagPath;
        }


        #region Public Properties
        public List<IdeWsParam> OutputTagPaths
        {
            get { return _outputTagPaths; }
            private set { _outputTagPaths = value; }
        }

        public WebServiceCommand WsCmd
        {
            get { return _wsCmd; }
            private set { _wsCmd = value; }
        }
        #endregion

    }
}
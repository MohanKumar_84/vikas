﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace mFicientWS
{
    [Serializable]
    public class XMLRPC
    {
        private SimpleDataCollection simpleTypes;
        private ArrayCollection arrays;
        private StructCollection structs;
        public XMLRPC()
        {
            simpleTypes = new SimpleDataCollection();
            arrays = new ArrayCollection();
            structs = new StructCollection();
        }

        [Serializable]
        public class SimpleData
        {
            private string strName, strDataType, strValue, strParentName;
            public SimpleData()
            {
            }
            public SimpleData(string _name, string _dataType)
            {
                strName = _name;
                strDataType = _dataType;
            }
            public SimpleData(string name, string type, string value)
            {
                strName = name;
                strDataType = type;
                strValue = value;
            }
            public SimpleData(string name, string type, string value, string parentName)
            {
                strName = name;
                strDataType = type;
                strValue = value;
                strParentName = parentName;
            }
            public string Name
            {
                get
                {
                    return strName;
                }
                set
                {
                    strName = value;
                }
            }
            public string DataType
            {
                get
                {
                    return strDataType;
                }
                set
                {
                    strDataType = value;
                }
            }
            public string Value
            {
                get
                {
                    return strValue;
                }
                set
                {
                    strValue = value;
                }
            }
            public string ParentName
            {
                get
                {
                    return strParentName;
                }
                set
                {
                    strParentName = value;
                }
            }
        }

        [Serializable]
        public class Array
        {
            private string strName, strDataType, strParentName;
            private SimpleDataCollection simpleTypes;
            private ArrayCollection arrays;
            private StructCollection structs;
            public Array()
            {
            }
            public Array(string _name, string _dataType)
            {
                strName = _name;
                strDataType = _dataType;
            }
            public Array(string _name)
            {
                strName = _name;
            }

            public Array(string _name, string _dataType,string _parentName)
            {
                strName = _name;
                strDataType = _dataType;
                strParentName = _parentName;
            }
            //public Array(string _name,string _parentName)
            //{
            //    strName = _name;
            //    strParentName = _parentName;
            //}

            public string ParentName
            {
                get
                {
                    return strParentName;
                }
                set
                {
                    strParentName = value;
                }
            }
            public string Name
            {
                get { return strName; }
                set
                {
                    strName = value;
                }
            }
            public string DataType
            {
                get { return strDataType; }
                set
                {
                    strDataType = value;
                }
            }
            public SimpleDataCollection SimpleTypes
            {
                get
                {
                    return simpleTypes;
                }
                set
                {
                    simpleTypes = value;
                }
            }
            public ArrayCollection Arrays
            {
                get
                {
                    return arrays;
                }
                set
                {
                    arrays = value;
                }
            }
            public StructCollection Structs
            {
                get
                {
                    return structs;
                }
                set
                {
                    structs = value;
                }
            }
        }

        [Serializable]
        public class Struct
        {
            private string strName, strParentName;
            private SimpleDataCollection simpleTypes;
            private ArrayCollection arrays;
            private StructCollection structs;
            public Struct()
            {
            }
            public Struct(string _name)
            {
                strName = _name;
            }

            public Struct(string _name,string _parentName)
            {
                strName = _name;
                strParentName = _parentName;
            }


            public string Name
            {
                get { return strName; }
                set
                {
                    strName = value;
                }
            }
            public string ParentName
            {
                get
                {
                    return strParentName;
                }
                set
                {
                    strParentName = value;
                }
            }
            public SimpleDataCollection SimpleTypes
            {
                get
                {
                    return simpleTypes;
                }
                set
                {
                    simpleTypes = value;
                }
            }
            public ArrayCollection Arrays
            {
                get
                {
                    return arrays;
                }
                set
                {
                    arrays = value;
                }
            }
            public StructCollection Structs
            {
                get
                {
                    return structs;
                }
                set
                {
                    structs = value;
                }
            }
        }

        [Serializable]
        public class SimpleDataCollection : CollectionBase, IEnumerable
        {
            private Hashtable _values = new Hashtable();
            private Hashtable _keys = new Hashtable();

            public SimpleDataCollection()
            {
            }

            public SimpleData Add(SimpleData data)
            {
                if (string.IsNullOrEmpty(data.Name)) return null;
                if (findKey(data.Name) != null) Remove(data.Name);
                return Add(data.Name, data);
            }

            private SimpleData Add(string key, SimpleData data)
            {
                Guid gid = Guid.NewGuid();
                _values.Add(gid, data);
                _keys.Add(gid, key);
                List.Add(gid);
                return data;
            }

            public void Remove(string key)
            {
                RemoveData(key);
            }

            public void Remove(SimpleData data)
            {
                RemoveData(data);
            }

            private void RemoveData(object data)
            {
                if (data.GetType().Equals(typeof(string)))
                {
                    List<Guid> guidList = getGuid((string)data);
                    while (guidList.Count > 0)
                    {
                        foreach (Guid gid in guidList)
                        {
                            removeGuid(gid);
                            break;
                        }
                        guidList = getGuid((string)data);
                    }
                }
                else
                {
                    foreach (Guid gid in _values.Keys)
                    {
                        if (object.ReferenceEquals(_values[gid], data))
                        {
                            removeGuid(gid);
                            break;
                        }
                    }
                }
            }

            public bool Contains(string key)
            {
                if (findKey(key) == null) return false;
                return true;
            }

            public SimpleData findKey(string key)
            {
                List<object> objectList = findListFromKey(key);
                if (objectList.Count <= 0) return null;
                return (SimpleData)objectList[0];
            }

            private List<object> findListFromKey(string key)
            {
                List<object> objectList = new List<object>();
                List<Guid> guidList = getGuid(key);
                if (guidList.Count <= 0) return objectList;
                foreach (Guid gid in guidList)
                {
                    object obj = findGuid(gid);
                    if (obj != null) objectList.Add(obj);
                }
                return objectList;
            }

            private object findGuid(Guid gid)
            {
                if (_values.ContainsKey(gid)) return _values[gid];
                return null;
            }

            private void removeGuid(Guid gid)
            {
                _values.Remove(gid);
                _keys.Remove(gid);
                if (List.Contains(gid)) List.Remove(gid);
            }

            private List<Guid> getGuid(string key)
            {
                List<Guid> guidList = new List<Guid>();
                foreach (Guid gid in _keys.Keys)
                {
                    if (_keys[gid].ToString() == key) guidList.Add(gid);
                }
                return guidList;
            }

            public SimpleData this[int index]
            {
                get
                {
                    return (SimpleData)findGuid((Guid)List[index]);
                }
            }

            public new SimpleDataEnumerator GetEnumerator()
            {
                return new SimpleDataEnumerator(this);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public class SimpleDataEnumerator : IEnumerator
            {
                private SimpleDataCollection sd;
                private int index;

                public SimpleDataEnumerator(SimpleDataCollection _sd)
                {
                    sd = _sd;
                    index = -1;
                }
                public SimpleData Current
                {
                    get
                    {
                        return sd[index];
                    }
                }
                public bool MoveNext()
                {
                    index++;
                    return (index < sd.Count);
                }
                public void Reset()
                {
                    index = -1;
                }
                object IEnumerator.Current
                {
                    get
                    {
                        return this.Current;
                    }
                }
            }
        }

        [Serializable]
        public class ArrayCollection : CollectionBase, IEnumerable
        {
            private Hashtable _values = new Hashtable();
            private Hashtable _keys = new Hashtable();

            public ArrayCollection()
            {
            }

            public Array Add(Array data)
            {
                if (string.IsNullOrEmpty(data.Name)) return null;
                if (findKey(data.Name) != null) Remove(data.Name);
                return Add(data.Name, data);
            }

            private Array Add(string key, Array data)
            {
                Guid gid = Guid.NewGuid();
                _values.Add(gid, data);
                _keys.Add(gid, key);
                List.Add(gid);
                return data;
            }

            public void Remove(string key)
            {
                RemoveData(key);
            }

            public void Remove(Array data)
            {
                RemoveData(data);
            }

            private void RemoveData(object data)
            {
                if (data.GetType().Equals(typeof(string)))
                {
                    List<Guid> guidList = getGuid((string)data);
                    while (guidList.Count > 0)
                    {
                        foreach (Guid gid in guidList)
                        {
                            removeGuid(gid);
                            break;
                        }
                        guidList = getGuid((string)data);
                    }
                }
                else
                {
                    foreach (Guid gid in _values.Keys)
                    {
                        if (object.ReferenceEquals(_values[gid], data))
                        {
                            removeGuid(gid);
                            break;
                        }
                    }
                }
            }

            public bool Contains(string key)
            {
                if (findKey(key) == null) return false;
                return true;
            }

            public Array findKey(string key)
            {
                List<object> objectList = findListFromKey(key);
                if (objectList.Count <= 0) return null;
                return (Array)objectList[0];
            }

            private List<object> findListFromKey(string key)
            {
                List<object> objectList = new List<object>();
                List<Guid> guidList = getGuid(key);
                if (guidList.Count <= 0) return objectList;
                foreach (Guid gid in guidList)
                {
                    object obj = findGuid(gid);
                    if (obj != null) objectList.Add(obj);
                }
                return objectList;
            }

            private object findGuid(Guid gid)
            {
                if (_values.ContainsKey(gid)) return _values[gid];
                return null;
            }

            private void removeGuid(Guid gid)
            {
                _values.Remove(gid);
                _keys.Remove(gid);
                if (List.Contains(gid)) List.Remove(gid);
            }

            private List<Guid> getGuid(string key)
            {
                List<Guid> guidList = new List<Guid>();
                foreach (Guid gid in _keys.Keys)
                {
                    if (_keys[gid].ToString() == key) guidList.Add(gid);
                }
                return guidList;
            }

            public Array this[int index]
            {
                get
                {
                    return (Array)findGuid((Guid)List[index]);
                }
            }

            public new ArrayEnumerator GetEnumerator()
            {
                return new ArrayEnumerator(this);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public class ArrayEnumerator : IEnumerator
            {
                private ArrayCollection sd;
                private int index;

                public ArrayEnumerator(ArrayCollection _sd)
                {
                    sd = _sd;
                    index = -1;
                }
                public Array Current
                {
                    get
                    {
                        return sd[index];
                    }
                }
                public bool MoveNext()
                {
                    index++;
                    return (index < sd.Count);
                }
                public void Reset()
                {
                    index = -1;
                }
                object IEnumerator.Current
                {
                    get
                    {
                        return this.Current;
                    }
                }
            }
        }

        [Serializable]
        public class StructCollection : CollectionBase, IEnumerable
        {
            private Hashtable _values = new Hashtable();
            private Hashtable _keys = new Hashtable();

            public StructCollection()
            {
            }

            public Struct Add(Struct data)
            {
                if (string.IsNullOrEmpty(data.Name)) return null;
                if (findKey(data.Name) != null) Remove(data.Name);
                return Add(data.Name, data);
            }

            private Struct Add(string key, Struct data)
            {
                Guid gid = Guid.NewGuid();
                _values.Add(gid, data);
                _keys.Add(gid, key);
                List.Add(gid);
                return data;
            }

            public void Remove(string key)
            {
                RemoveData(key);
            }

            public void Remove(Array data)
            {
                RemoveData(data);
            }

            private void RemoveData(object data)
            {
                if (data.GetType().Equals(typeof(string)))
                {
                    List<Guid> guidList = getGuid((string)data);
                    while (guidList.Count > 0)
                    {
                        foreach (Guid gid in guidList)
                        {
                            removeGuid(gid);
                            break;
                        }
                        guidList = getGuid((string)data);
                    }
                }
                else
                {
                    foreach (Guid gid in _values.Keys)
                    {
                        if (object.ReferenceEquals(_values[gid], data))
                        {
                            removeGuid(gid);
                            break;
                        }
                    }
                }
            }

            public bool Contains(string key)
            {
                if (findKey(key) == null) return false;
                return true;
            }

            public Struct findKey(string key)
            {
                List<object> objectList = findListFromKey(key);
                if (objectList.Count <= 0) return null;
                return (Struct)objectList[0];
            }

            private List<object> findListFromKey(string key)
            {
                List<object> objectList = new List<object>();
                List<Guid> guidList = getGuid(key);
                if (guidList.Count <= 0) return objectList;
                foreach (Guid gid in guidList)
                {
                    object obj = findGuid(gid);
                    if (obj != null) objectList.Add(obj);
                }
                return objectList;
            }

            private object findGuid(Guid gid)
            {
                if (_values.ContainsKey(gid)) return _values[gid];
                return null;
            }

            private void removeGuid(Guid gid)
            {
                _values.Remove(gid);
                _keys.Remove(gid);
                if (List.Contains(gid)) List.Remove(gid);
            }

            private List<Guid> getGuid(string key)
            {
                List<Guid> guidList = new List<Guid>();
                foreach (Guid gid in _keys.Keys)
                {
                    if (_keys[gid].ToString() == key) guidList.Add(gid);
                }
                return guidList;
            }

            public Struct this[int index]
            {
                get
                {
                    return (Struct)findGuid((Guid)List[index]);
                }
            }

            public new StructEnumerator GetEnumerator()
            {
                return new StructEnumerator(this);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public class StructEnumerator : IEnumerator
            {
                private StructCollection sd;
                private int index;

                public StructEnumerator(StructCollection _sd)
                {
                    sd = _sd;
                    index = -1;
                }
                public Struct Current
                {
                    get
                    {
                        return sd[index];
                    }
                }
                public bool MoveNext()
                {
                    index++;
                    return (index < sd.Count);
                }
                public void Reset()
                {
                    index = -1;
                }
                object IEnumerator.Current
                {
                    get
                    {
                        return this.Current;
                    }
                }
            }
        }

        public SimpleDataCollection SimpleTypes
        {
            get
            {
                return simpleTypes;
            }
            set
            {
                simpleTypes = value;
            }
        }

        public ArrayCollection Arrays
        {
            get
            {
                return arrays;
            }
            set
            {
                arrays = value;
            }
        }

        public StructCollection Structs
        {
            get
            {
                return structs;
            }
            set
            {
                structs = value;
            }
        }
    }

}
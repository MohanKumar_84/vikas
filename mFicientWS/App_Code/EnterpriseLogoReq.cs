﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class EnterpriseLogoReq
    {

        string _requestId, _companyId;
        int _functionCode;
        
        public string CompanyId
        {
            get { return _companyId; }
        }
        
        public EnterpriseLogoReq(string requestJson, string userId)
        {
            RequestJsonParsing<GetZippedImagesReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<GetZippedImagesReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.GET_ENTERPRISE_LOGO_AS_ZIPPED)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.data.enid;
            
        }
    }
    [DataContract]
    public class GetZippedImagesReqData : Data
    {
        
    }

}
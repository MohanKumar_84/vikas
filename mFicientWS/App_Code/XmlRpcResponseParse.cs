﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Collections;
using System.Runtime.Serialization;
using System.IO;
using System.Text;
using System.Data;
namespace mFicientWS
{
    public class XmlRpcResponseParse
    {

        string _responseInString;
        XmlDocument _responseInXMLDoc = new XmlDocument();
        XMLRPC _xmlRpcResponseColl;
        RETURN_FUNCTION_TYPE_DOTASK _returnFunction;
        //QueryReturnValue _returnValues;
        RpcParamaters _rpcParam = new RpcParamaters();
        string a1 = "", a2 = "", a3 = "", a4 = "", a5 = "", a6 = "", a7 = "";
        const string ArrayPrefix = "Array", StructPrefix = "Struct", SimpleTypePrefix = "Simple", XmlRpcPrefix = "XmlRpc";
        List<rptData> _rptData = new List<rptData>();
        List<Pie_ddl_Data> _ddlData = new List<Pie_ddl_Data>();
        List<Bar_Line_Data> _barLineData = new List<Bar_Line_Data>();
        List<Pie_ddl_Data> _pieData = new List<Pie_ddl_Data>();
        //List<tblData> _tblData = new List<tblData>();



        enum XML_RPC_COLL_DATA_TYPE
        {
            SIMPLE_TYPE = 1,
            ARRAY = 2,
            STRUCT_TYPE = 3,
            NOT_DEFINED = 4,
        }

        #region Properties

        public string ResponseInString
        {
            get { return _responseInString; }
        }
        public XmlDocument ResponseInXMLDoc
        {
            get { return _responseInXMLDoc; }
        }
        public XMLRPC XmlRpcResponse
        {
            get { return _xmlRpcResponseColl; }
        }
        internal RETURN_FUNCTION_TYPE_DOTASK ReturnFunction
        {
            get { return _returnFunction; }
        }
        //public QueryReturnValue ReturnValues
        //{
        //    get { return _returnValues; }
        //}
        public RpcParamaters RpcParam
        {
            get { return _rpcParam; }
        }
        public List<rptData> RptData
        {
            get { return _rptData; }
        }
        public List<Pie_ddl_Data> DdlData
        {
            get { return _ddlData; }
        }
        public List<Bar_Line_Data> BarLineData
        {
            get { return _barLineData; }
        }
        public List<Pie_ddl_Data> PieData
        {
            get { return _pieData; }
        }
        
        #endregion
        #region Constructor
        //TODO:Set the constructor
        public XmlRpcResponseParse(string response, RpcParamaters rpcParam, RETURN_FUNCTION_TYPE_DOTASK returnFunction)
        {
            _responseInString = response;
            _responseInXMLDoc = new XmlDocument();
            _responseInXMLDoc.LoadXml(_responseInString);
            _rpcParam = rpcParam;
            _returnFunction = returnFunction;
        }
        public XmlRpcResponseParse(XmlDocument response, RpcParamaters rpcParam, RETURN_FUNCTION_TYPE_DOTASK returnFunction)
        {
            _responseInString = null;
            _responseInXMLDoc = response;
            _rpcParam = rpcParam;
            _returnFunction = returnFunction;
        }
        #endregion
        public void Process()
        {
             _responseInXMLDoc.LoadXml(getTestResponseString());
                parseResponse();
                
        }

        public void OutputParmeterReadAccourdingREturntype(string OutputName, string value)
        {
            //switch (_returnFunction)
            //{
                //case RETURN_FUNCTION_TYPE_DOTASK.BAR_DATA:
                //case RETURN_FUNCTION_TYPE_DOTASK.LINE_DATA:
                //    if (!string.IsNullOrEmpty(_returnValues.t) && OutputName.ToUpper() == _returnValues.t.ToUpper().Substring(_returnValues.t.LastIndexOf('.') + 1))
                //        a1 = value;
                //    if (!string.IsNullOrEmpty(_returnValues.v1) && OutputName.ToUpper() == _returnValues.v1.ToUpper().Substring(_returnValues.v1.LastIndexOf('.') + 1))
                //        a2 = value;
                //    if (!string.IsNullOrEmpty(_returnValues.v2) && OutputName.ToUpper() == _returnValues.v2.ToUpper().Substring(_returnValues.v2.LastIndexOf('.') + 1))
                //        a3 = value;
                //    if (!string.IsNullOrEmpty(_returnValues.v3) && OutputName.ToUpper() == _returnValues.v3.ToUpper().Substring(_returnValues.v3.LastIndexOf('.') + 1))
                //        a4 = value;
                //    if (!string.IsNullOrEmpty(_returnValues.v4) && OutputName.ToUpper() == _returnValues.v4.ToUpper().Substring(_returnValues.v4.LastIndexOf('.') + 1))
                //        a5 = value;
                //    if (!string.IsNullOrEmpty(_returnValues.v5) && OutputName.ToUpper() == _returnValues.v5.ToUpper().Substring(_returnValues.v5.LastIndexOf('.') + 1))
                //        a6 = value;
                //    break;
                //case RETURN_FUNCTION_TYPE_DOTASK.DROP_DOWN_DATA:
                //case RETURN_FUNCTION_TYPE_DOTASK.PIE_DATA:
                //    if (!string.IsNullOrEmpty(_returnValues.t) && OutputName.ToUpper() == _returnValues.t.ToUpper().Substring(_returnValues.t.LastIndexOf('.') + 1))
                //        a1 = value;
                //    if (!string.IsNullOrEmpty(_returnValues.v) && OutputName.ToUpper() == _returnValues.v.ToUpper().Substring(_returnValues.v.LastIndexOf('.') + 1))
                //        a2 = value;
                //    break;
                //case RETURN_FUNCTION_TYPE_DOTASK.REPEATER_DATA:
                //    if (!string.IsNullOrEmpty(_returnValues.r) && OutputName.ToUpper() == _returnValues.r.ToUpper().Substring(_returnValues.r.LastIndexOf('.') + 1))
                //        a1 = value;
                //    if (!string.IsNullOrEmpty(_returnValues.b) && OutputName.ToUpper() == _returnValues.b.ToUpper().Substring(_returnValues.b.LastIndexOf('.') + 1))
                //        a2 = value;
                //    if (!string.IsNullOrEmpty(_returnValues.s) && OutputName.ToUpper() == _returnValues.s.ToUpper().Substring(_returnValues.s.LastIndexOf('.') + 1))
                //        a3 = value;
                //    if (!string.IsNullOrEmpty(_returnValues.i) && OutputName.ToUpper() == _returnValues.i.ToUpper().Substring(_returnValues.i.LastIndexOf('.') + 1))
                //        a4 = value;
                //    if (!string.IsNullOrEmpty(_returnValues.c) && OutputName.ToUpper() == _returnValues.c.ToUpper().Substring(_returnValues.c.LastIndexOf('.') + 1))
                //        a5 = value;
                //    if (!string.IsNullOrEmpty(_returnValues.n) && OutputName.ToUpper() == _returnValues.n.ToUpper().Substring(_returnValues.n.LastIndexOf('.') + 1))
                //        a6 = value;
                //    if (!string.IsNullOrEmpty(_returnValues.an) && OutputName.ToUpper() == _returnValues.an.ToUpper().Substring(_returnValues.an.LastIndexOf('.') + 1))
                //        a7 = value;
                //    break;
                //TODO:COmplete Data
            //}
        }

        string createOutputList()
        {
            List<rptData> rptdata = new List<rptData>();
            List<Pie_ddl_Data> ddlData = new List<Pie_ddl_Data>();
            List<Bar_Line_Data> barLineData = new List<Bar_Line_Data>();
            List<Pie_ddl_Data> pieData = new List<Pie_ddl_Data>();
            //List<tblData> tblData = new List<tblData>();

            string strReturn = "";
            switch (_returnFunction)
            {
                case RETURN_FUNCTION_TYPE_DOTASK.BAR_DATA:
                case RETURN_FUNCTION_TYPE_DOTASK.LINE_DATA:
                    Bar_Line_Data obj = new Bar_Line_Data();
                    obj.t = a1;
                    obj.v1 = a2;
                    obj.v2 = a3;
                    obj.v3 = a4;
                    obj.v4 = a5;
                    obj.v5 = a6;
                    barLineData.Add(obj);
                    this.BarLineData.Add(obj);
                    //strReturn = "t=" + a1 + ",v1=" + a2 + ",v2=" + a3 + ",v3=" + a4 + ",v4=" + a5 + ",v5=" + a6;
                    break;
                case RETURN_FUNCTION_TYPE_DOTASK.DROP_DOWN_DATA:
                case RETURN_FUNCTION_TYPE_DOTASK.PIE_DATA:
                    strReturn = "t=" + a1 + ",v=" + a2;
                    break;
                case RETURN_FUNCTION_TYPE_DOTASK.REPEATER_DATA:
                    rptData objRptData = new rptData();
                    objRptData.r = a1;
                    objRptData.b = a2;
                    objRptData.s = a3;
                    objRptData.i = a4;
                    objRptData.c = a5;
                    objRptData.n = a6;
                    objRptData.an = a7;
                    //strReturn = "r=" + a1 + ",b=" + a2 + ",s=" + a3 + ",i=" + a4 + ",c=" + a5;
                    this.RptData.Add(objRptData);
                    break;
                //TODO:COmplete Data
            }
            return strReturn;
        }

        bool tag_matched(string type, XML_RPC_COLL_DATA_TYPE datatype)
        {
            if (datatype == XML_RPC_COLL_DATA_TYPE.STRUCT_TYPE && Convert.ToInt32(type) == (int)XML_RPC_Parameter_Type.STRUCT)
                return true;
            if (datatype == XML_RPC_COLL_DATA_TYPE.ARRAY && Convert.ToInt32(type) == (int)XML_RPC_Parameter_Type.ARRAY)
                return true;
            if (datatype == XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE && Convert.ToInt32(type) <= (int)XML_RPC_Parameter_Type.STRING)
                return true;
            return false;
        }

        bool tag_Contains(List<param> lstParam, XML_RPC_COLL_DATA_TYPE datatype, out int index, string name)
        {
            index = 0;
            foreach (param pr in lstParam)
            {
                if (pr.name.ToUpper() == name.ToUpper())
                {
                    if (datatype == XML_RPC_COLL_DATA_TYPE.STRUCT_TYPE && Convert.ToInt32(pr.typ) == (int)XML_RPC_Parameter_Type.STRUCT)
                        return true;
                    if (datatype == XML_RPC_COLL_DATA_TYPE.ARRAY && Convert.ToInt32(pr.typ) == (int)XML_RPC_Parameter_Type.ARRAY)
                        return true;
                    if (datatype == XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE && Convert.ToInt32(pr.typ) <= (int)XML_RPC_Parameter_Type.STRING)
                        return true;
                }
                index++;
            }
            return false;
        }

        #region Parsing
        private void parseResponse()
        {
            /*
            Check if root nodes exist.
            Throw exception if it doesn't.
            
            Check if response if Fault or Normal.
            If Fault - Throw Exception.
            
            Read contents of first Value tag.
            
            Switch for Contents of Value tag (to find type of Value returned.
            

             * If Struct:
             * Parse contents of Struct (Call parseStruct)
             
             * If Array:
             * Parse contents of Array (call parseArray)
             
             * If Primitive Type:
                Parse primitive type
             
            */
            XmlNode methodResponseNode = SelectSingleNode(_responseInXMLDoc, "methodResponse");
            if (methodResponseNode == null)
            {
                throw new Exception("Not a valid response.Method Response missing");
            }
            XmlNode faultNode = SelectSingleNode(methodResponseNode, "fault");
            if (faultNode != null)
            {
                throw new Exception(((int)DO_TASK_ERROR.XML_RPC_FAULT).ToString());
            }
            XmlNode paramsNode = SelectSingleNode(methodResponseNode, "params");
            if (paramsNode == null)
            {
                throw new Exception("Not a valid response.params tag missing");
            }
            XmlNode paramNode = SelectSingleNode(paramsNode, "param");
            if (paramNode == null)
            {
                throw new Exception("Not a valid response.param tag missing");
            }
            XmlNode valueNode = SelectSingleNode(paramNode, "value");
            if (valueNode == null)
            {
                throw new Exception("Not a valid response.value tag missing");
            }
            XmlNode node = SelectValueNode(valueNode);
            //loopThroughXmlRpcObject(node);
            XML_RPC_COLL_DATA_TYPE eTypeOfNode = getDataTypeOfNode(node);
            if (tag_matched(RpcParam.param[0].typ, eTypeOfNode))
            {
                XMLRPC objXmlRpcRespColl = new XMLRPC();
                switch (eTypeOfNode)
                {
                    case XML_RPC_COLL_DATA_TYPE.ARRAY:
                        XMLRPC.Array objXmlRpcArray = objXmlRpcRespColl.Arrays.Add(new XMLRPC.Array(RpcParam.param[0].name + objXmlRpcRespColl.Arrays.Count.ToString(), "0", XmlRpcPrefix));
                        parseArrayType(node, objXmlRpcArray, RpcParam.param[0].inparam);
                        break;
                    case XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE:
                        parseSimpleType(node, objXmlRpcRespColl, RpcParam.param[0].name);
                        break;
                    case XML_RPC_COLL_DATA_TYPE.STRUCT_TYPE:
                        XMLRPC.Struct objXmlRpcStruct = objXmlRpcRespColl.Structs.Add(new XMLRPC.Struct(RpcParam.param[0].name + objXmlRpcRespColl.Structs.Count.ToString(), XmlRpcPrefix));
                        parseStruct(node, objXmlRpcStruct, RpcParam.param[0].inparam);
                        break;
                }
                _xmlRpcResponseColl = objXmlRpcRespColl;
            }
            else
            {

            }
        }


        void loopThroughOutputPath()
        {
        }

        XmlNode[] getArrayValueNodes(XmlNode arrayNode)
        {
            XmlNode dataNode = SelectSingleNode(arrayNode, "data");
            XmlNode[] members = SelectNodes(dataNode, "value");
            return members;
        }
        XmlNode[] getStructMemberNodes(XmlNode structNode)
        {
            //XmlNodeList nodeList = this.ResponseInXMLDoc.GetElementsByTagName("struct");

            XmlNode[] members = SelectNodes(structNode, "member");
            return members;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="container"></param>
        /// <param name="name">Pass null or empty if not struct type.</param>
        /// <exception cref="System.Exception">Thrown when name is not provided in the struct type</exception>
        void parseSimpleType(XmlNode node, object container, string name)
        {
            XMLRPC objXmlRpc = container as XMLRPC;
            XMLRPC.Array objXmlRpcArray = container as XMLRPC.Array;
            XMLRPC.Struct objXmlRpcStruct = container as XMLRPC.Struct;
            int iCountOfColl = 0;
            string strDataType = node.Name;
            string strValue = SelectValueNode(node).Value;
            XMLRPC.SimpleDataCollection objSimpleColl;
            if (objXmlRpc != null)
            {
                iCountOfColl = objXmlRpc.SimpleTypes != null ? objXmlRpc.SimpleTypes.Count : 0;
                if (objXmlRpc.SimpleTypes != null)
                {
                    objSimpleColl = objXmlRpc.SimpleTypes;
                }
                else
                {
                    objSimpleColl = new XMLRPC.SimpleDataCollection();
                }
                objSimpleColl.Add(new XMLRPC.SimpleData(name + (iCountOfColl).ToString(), strDataType, strValue, XmlRpcPrefix));
                OutputParmeterReadAccourdingREturntype(name, strValue);
                objXmlRpc.SimpleTypes = objSimpleColl;
                createOutputList();
            }
            else if (objXmlRpcArray != null)
            {
                iCountOfColl = objXmlRpcArray.SimpleTypes != null ? objXmlRpcArray.SimpleTypes.Count : 0;
                if (objXmlRpcArray.SimpleTypes != null)
                {
                    objSimpleColl = objXmlRpcArray.SimpleTypes;
                }
                else
                {
                    objSimpleColl = new XMLRPC.SimpleDataCollection();
                }
                objSimpleColl.Add(new XMLRPC.SimpleData(name + (iCountOfColl).ToString(), strDataType, strValue, objXmlRpcArray.Name));
                OutputParmeterReadAccourdingREturntype(name, strValue);
                objXmlRpcArray.SimpleTypes = objSimpleColl;
                createOutputList();
            }
            else if (objXmlRpcStruct != null)
            {
                if (String.IsNullOrEmpty(name))
                {
                    throw new Exception();
                }
                iCountOfColl = objXmlRpcStruct.SimpleTypes != null ? objXmlRpcStruct.SimpleTypes.Count : 0;
                if (objXmlRpcStruct.SimpleTypes != null)
                {
                    objSimpleColl = objXmlRpcStruct.SimpleTypes;
                }
                else
                {
                    objSimpleColl = new XMLRPC.SimpleDataCollection();
                }
                objSimpleColl.Add(new XMLRPC.SimpleData(name, strDataType, strValue, objXmlRpcStruct.Name));
                OutputParmeterReadAccourdingREturntype(name, strValue);
                objXmlRpcStruct.SimpleTypes = objSimpleColl;
            }

            //hshOfMembers.Add(name, node);
        }
        void parseArrayType(XmlNode arrayNode, XMLRPC.Array containerArray, List<param> innerParam)
        {
            XmlNode[] arrayMembers = getArrayValueNodes(arrayNode);
            XMLRPC.ArrayCollection objXmlRpcArrayColl;
            XMLRPC.StructCollection objXmlRpxStructColl;
            foreach (XmlNode member in arrayMembers)
            {
                //string name = (SelectValueNode(member)).Name;
                //XmlNode node = SelectValueNode((SelectValueNode(member)));
                XmlNode memberNode = SelectValueNode(member);
                XML_RPC_COLL_DATA_TYPE eDataTypeOfNode = getDataTypeOfNode(memberNode);
                if (tag_matched(innerParam[0].typ, eDataTypeOfNode))
                {
                    switch (eDataTypeOfNode)
                    {
                        case XML_RPC_COLL_DATA_TYPE.ARRAY:
                            if (containerArray.Arrays != null)
                            {
                                objXmlRpcArrayColl = containerArray.Arrays;
                            }
                            else
                            {
                                objXmlRpcArrayColl = new XMLRPC.ArrayCollection();
                            }
                            XMLRPC.Array objInnerArray = objXmlRpcArrayColl.Add(new XMLRPC.Array(innerParam[0].name + "0", "0", containerArray.Name));
                            containerArray.Arrays = objXmlRpcArrayColl;
                            parseArrayType(memberNode, objInnerArray, innerParam[0].inparam);
                            break;
                        case XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE:
                            parseSimpleType(memberNode, containerArray, innerParam[0].name);
                            break;
                        case XML_RPC_COLL_DATA_TYPE.STRUCT_TYPE:
                            if (containerArray.Structs != null)
                            {
                                objXmlRpxStructColl = containerArray.Structs;
                            }
                            else
                            {
                                objXmlRpxStructColl = new XMLRPC.StructCollection();
                            }
                            XMLRPC.Struct objXmlRpcStruct = objXmlRpxStructColl.Add(new XMLRPC.Struct(innerParam[0].name + "0", containerArray.Name));
                            containerArray.Structs = objXmlRpxStructColl;
                            parseStruct(memberNode, objXmlRpcStruct, innerParam[0].inparam);
                            break;
                    }
                }
                else
                {
                }
            }
        }
        private void parseStruct(XmlNode structNode, XMLRPC.Struct containerStruct, List<param> innerParam)
        {
            XmlNode[] structMembers = getStructMemberNodes(structNode);
            XMLRPC.StructCollection objStructColl;
            XMLRPC.ArrayCollection objArrayColl;
            foreach (XmlNode member in structMembers)
            {
                XmlNode nameNode;
                XmlNode valueNode;
                selectNameAndValueNode(member, "name", out nameNode, "value",
                  out valueNode);

                string name = nameNode.FirstChild.Value;
                XmlNode vvNode = SelectValueNode(valueNode);
                XML_RPC_COLL_DATA_TYPE eDataTypeOfNode = getDataTypeOfNode(vvNode);
                int index = 0;
                if (tag_Contains(innerParam, eDataTypeOfNode, out index, name))
                {
                    switch (eDataTypeOfNode)
                    {
                        case XML_RPC_COLL_DATA_TYPE.ARRAY:
                            if (containerStruct.Arrays != null)
                            {
                                objArrayColl = containerStruct.Arrays;
                            }
                            else
                            {
                                objArrayColl = new XMLRPC.ArrayCollection();
                            }
                            XMLRPC.Array objXmlRpcArray = objArrayColl.Add(new XMLRPC.Array(name, "0", containerStruct.Name));
                            containerStruct.Arrays = objArrayColl;
                            parseArrayType(vvNode, objXmlRpcArray, innerParam[index].inparam);
                            break;
                        case XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE:
                            parseSimpleType(vvNode, containerStruct, name);
                            //                            if(inner
                            break;
                        case XML_RPC_COLL_DATA_TYPE.STRUCT_TYPE:
                            if (containerStruct.Structs != null)
                            {
                                objStructColl = containerStruct.Structs;
                            }
                            else
                            {
                                objStructColl = new XMLRPC.StructCollection();
                            }
                            XMLRPC.Struct objXmlRpcStruct = objStructColl.Add(new XMLRPC.Struct(name, containerStruct.Name));
                            containerStruct.Structs = objStructColl;
                            parseStruct(vvNode, objXmlRpcStruct, innerParam[index].inparam);
                            break;
                    }
                }
                else
                {

                }
            }
        }
        XML_RPC_COLL_DATA_TYPE getDataTypeOfNode(XmlNode node)
        {
            XML_RPC_COLL_DATA_TYPE eNodeType = XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE;
            switch (node.Name.ToLower())
            {
                case "struct":
                    eNodeType = XML_RPC_COLL_DATA_TYPE.STRUCT_TYPE;
                    break;
                case "array":
                    eNodeType = XML_RPC_COLL_DATA_TYPE.ARRAY;
                    break;
                default:
                    eNodeType = XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE;
                    break;
            }
            return eNodeType;
        }

        #endregion

        #region Get the Final Data

        XMLRPC.SimpleDataCollection getSimpleDataFromCollection(string outputPath)
        {
            if (!String.IsNullOrEmpty(outputPath))
            {
                string[] aryOutputPath = outputPath.Split('.');
                object objAfterSearch = null;
                for (int i = 0; i <= aryOutputPath.Length - 1; i++)
                {
                    if (i == 0)
                    {
                        getContainingObjectFromCollection(this.XmlRpcResponse, aryOutputPath[i], i, XML_RPC_Parameter_Type.NOT_DEFINED, out objAfterSearch);
                    }
                    else
                    {
                        XML_RPC_Parameter_Type eCollTypeToSearch;
                        try
                        {
                            eCollTypeToSearch = (XML_RPC_Parameter_Type)Enum.Parse(typeof(XML_RPC_Parameter_Type), aryOutputPath[i]);
                        }
                        catch
                        {

                            string strType = aryOutputPath[i].Substring(aryOutputPath[i].IndexOf('(') + 1, (aryOutputPath[i].LastIndexOf(')') - (aryOutputPath[i].IndexOf('(') + 1))); ;
                            eCollTypeToSearch = (XML_RPC_Parameter_Type)Enum.Parse(typeof(XML_RPC_Parameter_Type), strType);
                        }
                        getContainingObjectFromCollection(objAfterSearch, aryOutputPath[i].Split('(')[0], i, eCollTypeToSearch, out objAfterSearch);
                    }
                }
                return (XMLRPC.SimpleDataCollection)objAfterSearch;
            }
            else
                return null;
        }
        void getContainingObjectFromCollection(object objToSearch, string nameToSearch, int iterationCount, XML_RPC_Parameter_Type collTypeToSearch, out object objAfterSearch)
        {
            objAfterSearch = null;
            XMLRPC objXmlRpc = objToSearch as XMLRPC;
            XMLRPC.Array objXmpRpcArray = objToSearch as XMLRPC.Array;
            XMLRPC.Struct objXmlRpcStruct = objToSearch as XMLRPC.Struct;

            XML_RPC_COLL_DATA_TYPE eCollType;
            switch (collTypeToSearch)
            {
                case XML_RPC_Parameter_Type.ARRAY:
                    eCollType = XML_RPC_COLL_DATA_TYPE.ARRAY;
                    break;
                case XML_RPC_Parameter_Type.STRUCT:
                    eCollType = XML_RPC_COLL_DATA_TYPE.STRUCT_TYPE;
                    break;
                default:
                    eCollType = XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE;
                    break;
            }

            if (objXmlRpc != null)
            {
                if (iterationCount == 0)
                {
                    XML_RPC_Parameter_Type eNameType = (XML_RPC_Parameter_Type)Enum.Parse(typeof(XML_RPC_Parameter_Type), nameToSearch);
                    switch (eNameType)
                    {
                        case XML_RPC_Parameter_Type.ARRAY:
                            eCollType = XML_RPC_COLL_DATA_TYPE.ARRAY;
                            break;
                        case XML_RPC_Parameter_Type.STRUCT:
                            eCollType = XML_RPC_COLL_DATA_TYPE.STRUCT_TYPE;
                            break;
                        default:
                            eCollType = XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE;
                            break;
                    }

                    switch (eCollType)
                    {
                        case XML_RPC_COLL_DATA_TYPE.STRUCT_TYPE:
                            objAfterSearch = objXmlRpc.Structs[0];
                            break;
                        case XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE:
                            objAfterSearch = objXmlRpc.SimpleTypes[0];
                            break;
                        case XML_RPC_COLL_DATA_TYPE.ARRAY:
                            objAfterSearch = objXmlRpc.Arrays[0];
                            break;
                    }
                }
            }
            else if (objXmlRpcStruct != null)
            {
                if (!String.IsNullOrEmpty(nameToSearch))
                {
                    //The request to find should be in a struct.Not Array or SimpleType
                    int iCountOfLoop = 0;
                    switch (eCollType)
                    {
                        case XML_RPC_COLL_DATA_TYPE.STRUCT_TYPE:
                            XMLRPC.StructCollection objStructColl = new XMLRPC.StructCollection();
                            XMLRPC.Struct objStruct = new XMLRPC.Struct();

                            foreach (XMLRPC.Struct rpcStruct in objXmlRpcStruct.Structs)
                            {
                                objStruct = rpcStruct.Structs.findKey(nameToSearch);
                                objStruct.Name = nameToSearch + iCountOfLoop;
                                objStruct.ParentName = rpcStruct.Name;
                                objStructColl.Add(objStruct);
                                iCountOfLoop++;
                            }
                            objStruct = new XMLRPC.Struct();
                            objStruct.Structs = objStructColl;
                            objAfterSearch = objStruct;
                            //objAfterSearch = objXmlRpcStruct.Structs.findKey(nameToSearch);
                            break;
                        case XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE:
                            XMLRPC.SimpleData objSimpleData = new XMLRPC.SimpleData();
                            XMLRPC.SimpleDataCollection objSimpleColl = new XMLRPC.SimpleDataCollection();
                            foreach (XMLRPC.Struct rpcStruct in objXmlRpcStruct.Structs)
                            {
                                objSimpleData = rpcStruct.SimpleTypes.findKey(nameToSearch);
                                objSimpleData.Name = nameToSearch + " " + iCountOfLoop;
                                objSimpleData.ParentName = rpcStruct.Name;
                                objSimpleColl.Add(objSimpleData);
                                iCountOfLoop++;
                            }
                            objAfterSearch = objSimpleColl;
                            break;
                        case XML_RPC_COLL_DATA_TYPE.ARRAY:
                            XMLRPC.ArrayCollection objArrayColl = new XMLRPC.ArrayCollection();
                            XMLRPC.Array objArray = new XMLRPC.Array();
                            foreach (XMLRPC.Struct rpcStruct in objXmlRpcStruct.Structs)
                            {
                                objArray = rpcStruct.Arrays.findKey(nameToSearch);
                                objArray.Name = nameToSearch + iCountOfLoop;
                                objArray.ParentName = rpcStruct.Name;
                                objArrayColl.Add(objArray);
                                iCountOfLoop++;
                            }
                            objArray = new XMLRPC.Array();
                            objArray.Arrays = objArrayColl;
                            objAfterSearch = objArray;
                            break;
                    }
                }
                else
                {
                    throw new Exception("Name should be provided");
                }
            }
            else if (objXmpRpcArray != null)
            {
                int iCountOfLoop = 0;
                switch (eCollType)
                {
                    case XML_RPC_COLL_DATA_TYPE.STRUCT_TYPE:
                        XMLRPC.Struct objStruct = new XMLRPC.Struct();
                        objStruct.Structs = objXmpRpcArray.Structs;
                        objAfterSearch = objStruct;
                        break;
                    case XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE:
                        XMLRPC.SimpleData objSimpleData = new XMLRPC.SimpleData();
                        XMLRPC.SimpleDataCollection objSimpleColl = new XMLRPC.SimpleDataCollection();
                        foreach (XMLRPC.Array rpcArray in objXmpRpcArray.Arrays)
                        {
                            //objSimpleData = rpcArray.SimpleTypes.findKey(nameToSearch);
                            //objSimpleData.Name = nameToSearch + " " + iCountOfLoop;
                            //objSimpleColl.Add(objSimpleData);
                            //iCountOfLoop++;
                            string strTypeToFind = getParamTypeInString(nameToSearch);
                            foreach (XMLRPC.SimpleData simpleData in rpcArray.SimpleTypes)
                            {
                                if (strTypeToFind.ToLower() == simpleData.DataType)
                                {
                                    objSimpleData.Name = iCountOfLoop.ToString();
                                    objSimpleData.Value = simpleData.Value;
                                    objSimpleData.ParentName = simpleData.ParentName;
                                    objSimpleColl.Add(objSimpleData);
                                    iCountOfLoop++;
                                }
                            }

                        }
                        objAfterSearch = objSimpleColl;
                        break;
                    case XML_RPC_COLL_DATA_TYPE.ARRAY:
                        XMLRPC.Array objArray = new XMLRPC.Array();
                        objArray.Arrays = objXmpRpcArray.Arrays;
                        objAfterSearch = objArray;
                        break;
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlRpcType"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Thrown when xmlRpcTypePassed is not defined</exception>
        string getParamTypeInString(string xmlRpcType)
        {
            string strTypeInString = "";
            try
            {
                XML_RPC_Parameter_Type xmlRpcParamType = (XML_RPC_Parameter_Type)Enum.Parse(typeof(XML_RPC_Parameter_Type), xmlRpcType);
                if (Enum.IsDefined(typeof(XML_RPC_Parameter_Type), xmlRpcParamType))
                {
                    switch (xmlRpcParamType)
                    {
                        case XML_RPC_Parameter_Type.BASE64:
                            strTypeInString = "base64";
                            break;
                        case XML_RPC_Parameter_Type.BOOLEAN:
                            strTypeInString = "boolean";
                            break;
                        case XML_RPC_Parameter_Type.DATE_TIME:
                            strTypeInString = "dateTime.iso8601";
                            break;
                        case XML_RPC_Parameter_Type.DOUBLE:
                            strTypeInString = "double";
                            break;
                        case XML_RPC_Parameter_Type.INTEGER:
                            strTypeInString = "int";
                            break;
                        case XML_RPC_Parameter_Type.STRING:
                            strTypeInString = "string";
                            break;
                        case XML_RPC_Parameter_Type.I4:
                            strTypeInString = "i4";
                            break;
                    }
                }
            }
            catch
            {
                throw new ArgumentException("Parameter type not defined");
            }

            return strTypeInString;

        }
        #endregion

        #region Select Nodes from XML
        void selectNameAndValueNode(XmlNode node, string name1, out XmlNode node1, string name2, out XmlNode node2)
        {
            node1 = node2 = null;
            foreach (XmlNode selnode in node.ChildNodes)
            {
                if (selnode.Name == name1)
                {
                    if (node1 == null)
                        node1 = selnode;

                }
                else if (selnode.Name == name2)
                {
                    if (node2 == null)
                        node2 = selnode;

                }
            }
        }

        XmlNode SelectSingleNode(XmlNode node, string name)
        {
            return node.SelectSingleNode(name);
        }

        XmlNode[] SelectNodes(XmlNode node, string name)
        {
            ArrayList list = new ArrayList();
            foreach (XmlNode selnode in node.ChildNodes)
            {
                if (selnode.Name == name)
                    list.Add(selnode);
            }
            return (XmlNode[])list.ToArray(typeof(XmlNode));
        }

        XmlNode SelectValueNode(XmlNode valueNode)
        {
            // an XML-RPC value is either held as the child node of a <value> element
            // or is just the text of the value node as an implicit string value
            XmlNode vNode = SelectSingleNode(valueNode, "*");
            if (vNode == null)
                vNode = valueNode.FirstChild;
            return vNode;
        }
        #endregion

        #region Get The type of Object
        XML_RPC_COLL_DATA_TYPE getCollectionType(object obj)
        {
            if (obj.GetType() == typeof(XMLRPC.Struct))
            {
                return XML_RPC_COLL_DATA_TYPE.STRUCT_TYPE;
            }
            else if (obj.GetType() == typeof(XMLRPC.Array))
            {
                return XML_RPC_COLL_DATA_TYPE.ARRAY;
            }
            else if (obj.GetType() == typeof(XMLRPC.SimpleData))
            {
                return XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE;
            }
            else throw new Exception("Not defined");
        }
        #endregion

        #region Datatable Formation
        DataTable getRepeaterDtblStructure()
        {
            DataTable dtblRpt = new DataTable();
            dtblRpt.Columns.Add("i", typeof(string));
            dtblRpt.Columns.Add("b", typeof(string));
            dtblRpt.Columns.Add("r", typeof(string));
            dtblRpt.Columns.Add("s", typeof(string));
            dtblRpt.Columns.Add("c", typeof(string));
            return dtblRpt;
        }
        DataTable getDropDownDtblStructure()
        {
            DataTable dtblDdl = new DataTable();
            dtblDdl.Columns.Add("t", typeof(string));
            dtblDdl.Columns.Add("v", typeof(string));
            return dtblDdl;
        }
        DataTable getBarLineDtblStructure()
        {
            DataTable dtblBarLine = new DataTable();
            dtblBarLine.Columns.Add("t", typeof(string));
            dtblBarLine.Columns.Add("v1", typeof(string));
            dtblBarLine.Columns.Add("v2", typeof(string));
            dtblBarLine.Columns.Add("v3", typeof(string));
            dtblBarLine.Columns.Add("v4", typeof(string));
            dtblBarLine.Columns.Add("v5", typeof(string));
            return dtblBarLine;
        }
        DataTable getPieDtblStructure()
        {
            DataTable dtblPie = new DataTable();
            dtblPie.Columns.Add("t", typeof(string));
            dtblPie.Columns.Add("v", typeof(string));
            return dtblPie;
        }

        DataTable getDtblByType()
        {
            DataTable dtbl = null;
            switch (this.ReturnFunction)
            {
                case RETURN_FUNCTION_TYPE_DOTASK.REPEATER_DATA:
                    dtbl = getRepeaterDtblStructure();
                    break;
                case RETURN_FUNCTION_TYPE_DOTASK.DROP_DOWN_DATA:
                    dtbl = getDropDownDtblStructure();
                    break;
                case RETURN_FUNCTION_TYPE_DOTASK.BAR_DATA:
                case RETURN_FUNCTION_TYPE_DOTASK.LINE_DATA:
                    dtbl = getBarLineDtblStructure();
                    break;
                case RETURN_FUNCTION_TYPE_DOTASK.PIE_DATA:
                    dtbl = getPieDtblStructure();
                    break;
            }
            return dtbl;
        }

        DataTable getRptFinalDtbl(XMLRPC.SimpleDataCollection i, XMLRPC.SimpleDataCollection b, XMLRPC.SimpleDataCollection r, XMLRPC.SimpleDataCollection s, XMLRPC.SimpleDataCollection c)
        {
            DataTable dtblRpt = getRepeaterDtblStructure();
            int iMaxCount = 0;
            iMaxCount = i != null ? i.Count : 0;
            iMaxCount = iMaxCount < b.Count ? b.Count : iMaxCount;//should be present
            iMaxCount = iMaxCount < r.Count ? r.Count : iMaxCount;//should be present
            iMaxCount = s != null ? iMaxCount < s.Count ? s.Count : iMaxCount : 0;
            iMaxCount = c != null ? iMaxCount < c.Count ? c.Count : iMaxCount : 0;
            for (int j = 0; j <= iMaxCount - 1; j++)
            {
                //add i
                if (i.Count > j)
                    dtblRpt.Rows[j]["i"] = i[j].Value;
                else
                    dtblRpt.Rows[j]["i"] = String.Empty;
                //add b
                if (b.Count > j)
                    dtblRpt.Rows[j]["b"] = b[j].Value;
                else
                    dtblRpt.Rows[j]["b"] = String.Empty;
                //add r
                if (r.Count > j)
                    dtblRpt.Rows[j]["r"] = r[j].Value;
                else
                    dtblRpt.Rows[j]["r"] = String.Empty;
                //add s
                if (s.Count > j)
                    dtblRpt.Rows[j]["s"] = s[j].Value;
                else
                    dtblRpt.Rows[j]["s"] = String.Empty;
                //add c
                if (c.Count > j)
                    dtblRpt.Rows[j]["c"] = c[j].Value;
                else
                    dtblRpt.Rows[j]["c"] = String.Empty;
            }
            return dtblRpt;
        }
        DataTable getDropDownFinalDtbl(XMLRPC.SimpleDataCollection v, XMLRPC.SimpleDataCollection t)
        {
            DataTable dtblRpt = getDropDownDtblStructure();
            int iMaxCount = 0;
            iMaxCount = v.Count;
            iMaxCount = iMaxCount < t.Count ? t.Count : iMaxCount;//should be present
            DataRow row;
            for (int j = 0; j <= iMaxCount - 1; j++)
            {
                row = dtblRpt.NewRow();
                //add i
                if (v.Count > j)
                {
                    row["v"] = v[j].Value;
                }
                else
                {
                    row["v"] = String.Empty;
                }
                //add t
                if (t.Count > j)
                {
                    row["t"] = t[j].Value;
                }
                else
                {
                    row["t"] = String.Empty;
                }
                dtblRpt.Rows.Add(row);

            }
            return dtblRpt;
        }
        #endregion


        #region Testing Strings
        string getTestResponseString()
        {
            //return @"<?xml version=""1.0"" encoding=""UTF-8""?>
            //<methodResponse><params><param><value><struct><member><name>sum</name><value><i4>300</i4></value></member><member><name>difference</name><value><i4>-100</i4></value></member><member><name>inner struct</name><value><struct><member><name>sum1</name><value><i4>300</i4></value></member><member><name>difference1</name><value><i4>-100</i4></value></member></struct></value></member><member><name>inner array</name><value><array><data><value><i4>-100</i4></value><value><i4>-100</i4></value><value><i4>-100</i4></value><value><i4>-100</i4></value><value><struct><member><name>sum2</name><value><i4>300</i4></value></member><member><name>difference2</name><value><i4>-100</i4></value></member></struct></value></data></array></value></member></struct></value></param></params></methodResponse>";


            //return @"<?xml version=""1.0"" encoding=""UTF-8""?>
            //<methodResponse><params><param><value><array><data><value><int>100</int></value><value><int>100</int></value><value><int>100</int></value><value><int>100</int></value><value><int>100</int></value><value><int>100</int></value><value><string>Test</string></value><value><struct><member><name>sum</name><value><i4>300</i4></value></member><member><name>difference</name><value><i4>-100</i4></value></member></struct></value><value><array><data><value><i4>-100</i4></value><value><i4>-100</i4></value><value><i4>-100</i4></value><value><i4>-100</i4></value><value><struct><member><name>sum2</name><value><i4>300</i4></value></member><member><name>difference2</name><value><i4>-100</i4></value></member></struct></value></data></array></value></data></array></value></param></params></methodResponse>";

            return @"<?xml version=""1.0"" encoding=""UTF-8""?>
<methodResponse><params><param><value><array><data><value><struct><member><name>ProductCode</name><value><i4>1</i4></value></member><member><name>ProductName</name><value><string>Nokia</string></value></member><member><name>SubProduct</name><value><array><data><value><string>Nokia1</string></value><value><string>Nokia2</string></value><value><string>Nokia3</string></value><value><string>Nokia4</string></value><value><string>Nokia5</string></value><value><string>Nokia6</string></value></data></array></value></member></struct></value><value><struct><member><name>ProductCode</name><value><i4>2</i4></value></member><member><name>ProductName</name><value><string>Samsung</string></value></member><member><name>SubProduct</name><value><array><data><value><string>Samsung1</string></value><value><string>Samsung2</string></value><value><string>Samsung3</string></value><value><string>Samsung4</string></value><value><string>Samsung5</string></value></data></array></value></member></struct></value><value><struct><member><name>ProductCode</name><value><i4>3</i4></value></member><member><name>ProductName</name><value><string>HTC</string></value></member><member><name>SubProduct</name><value><array><data><value><string>HTC1</string></value><value><string>HTC2</string></value><value><string>HTC3</string></value><value><string>HTC4</string></value><value><string>HTC5</string></value></data></array></value></member></struct></value></data></array></value></param></params></methodResponse>";
        }

        string getOutputPathStruct()
        {
            string strOutputPath = @"add.inner struct.difference1";
            ////string[] aryOutputPath = strOutputPath.Split('.');
            ////string strPathForXML;
            ////if (aryOutputPath.Length == 1)
            ////{
            ////    return XmlRpc.SimpleTypes.findKey(aryOutputPath[0]).DataType;
            ////}
            ////for (int i = 0; i <= aryOutputPath.Length - 1; i++)
            ////{
            ////    object obj = XmlRpc.Arrays.findKey(aryOutputPath[i]);
            ////    obj = XmlRpc.Structs.findKey(aryOutputPath[i]);
            ////    //XML_RPC_COLLECTION_TYPE collType = getCollectionType(obj);
            ////    //if (obj.GetType() == typeof(XMLRPC.Struct))
            ////    //{ 
            ////    //    parseStructType(
            ////    //}
            ////}
            return strOutputPath;
        }

        //string getOutputPathStruct(object objToSearch, string itemToSearch, out string finalPath, XML_RPC_COLLECTION_TYPE collType)
        //{
        //    string strOutputPath = @"add.inner.difference1";
        //    string[] aryOutputPath = strOutputPath.Split('.');
        //    string strPathForXML;
        //    for (int i = 0; i <= aryOutputPath.Length - 1; i++)
        //    {

        //    }


        //    //XmlRpc.SimpleTypes.findKey(
        //    return "";
        //}
        //string getTheStructIndexAndMemberName(string outputPath,out int index)
        //{
        //    index = 0;
        //    string[] path = outputPath.Split('.');

        //}
        string getOutputPathArray()
        {
            //return @"testarray.innerTestArray";
            //return @"ar1.ar2.ar3.ar4.pc";
            return @"ar1.str1.SubProduct.pc";
        }
        string getOutputPathArrayInStruct()
        {
            return "";
        }

        string getOutputJson()
        {
            //return @"{""param"":[{""name"":""ar1"",""typ"":""8"",""inparam"":[{""name"":""ar2"",""typ"":""8"",""inparam"":[{""name"":""ar3"",""typ"":""8"",""inparam"":[{""name"":""ar4"",""typ"":""8"",""inparam"":[{""name"":""pc"",""typ"":""1"",""inparam"":""""}]}]}]}]}]}";
            //return @"{""param"":[{""name"":""ar1"",""typ"":""8"",""inparam"":[{""name"":""str1"",""typ"":""7"",""inparam"":[{""name"":""SubProduct"",""typ"":""8"",""inparam"":[{""name"":""pc"",""typ"":""6""}]}]}]}]}";
            //return @"{""param"":[{""name"":""ar1"",""typ"":""8"",""inparam"":[{""name"":""str1"",""typ"":""7"",""inparam"":[{""name"":""ProductCode"",""typ"":""0""},{""name"":""ProductName"",""typ"":""6""}]}]}]}";
            return @"{""param"":[{""name"":""ar1"",""typ"":""8"",""inparam"":[{""name"":""str1"",""typ"":""7"",""inparam"":[{""name"":""ProductCode"",""typ"":""0""},{""name"":""ProductName"",""typ"":""6""},{""name"":""SubProduct"",""typ"":""8"",""inparam"":[{""name"":""pc"",""typ"":""6""}]}]}]}]}";
        }
        #endregion
        #region Modified Path

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rpcParamFromJson"></param>
        /// <exception cref="System.Exception"></exception>
        string getModifiedPath(RpcParamaters rpcParamFromJson, string outputPath)
        {
            string strModifiedPath = "";
            if (!String.IsNullOrEmpty(outputPath))
            {
                string[] aryOutputPath = outputPath.Split('.');
                List<param> lstParamBeforeIteration = new List<param>();
                List<param> lstParamAfterIteration;
                XML_RPC_Parameter_Type eParamType;
                //if root type is Struct then in the response collection we will have name value pair.So the modified path will be 7.Name.Name
                //But in any other case we do not get the name value collection hence modified path will be 8.7.8.5;
                XML_RPC_COLL_DATA_TYPE eRootType = XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE;//does'nt matter if its array or simple type
                for (int i = 0; i <= aryOutputPath.Length - 1; i++)
                {
                    if (i == 0)
                    {
                        iterateThroughParam(aryOutputPath[i], i, rpcParamFromJson, null, out lstParamAfterIteration, out eParamType);
                    }
                    else
                    {
                        iterateThroughParam(aryOutputPath[i], i, rpcParamFromJson, lstParamBeforeIteration, out lstParamAfterIteration, out eParamType);
                    }
                    lstParamBeforeIteration = lstParamAfterIteration;
                    if (i < aryOutputPath.Length - 1 && (lstParamBeforeIteration == null || lstParamBeforeIteration.Count == 0))
                    {
                        throw new Exception("Complete path not found");
                    }
                    if (String.IsNullOrEmpty(strModifiedPath))
                    {
                        strModifiedPath = ((int)eParamType).ToString();
                    }
                    else
                    {
                        switch (eRootType)
                        {
                            case XML_RPC_COLL_DATA_TYPE.STRUCT_TYPE:
                                strModifiedPath += "." + aryOutputPath[i] + "(" + ((int)eParamType).ToString() + ")";
                                break;
                            default:
                                strModifiedPath += "." + ((int)eParamType).ToString();
                                break;
                        }

                    }
                    //make the new Param as Root
                    switch (eParamType)
                    {
                        case XML_RPC_Parameter_Type.STRUCT:
                            eRootType = XML_RPC_COLL_DATA_TYPE.STRUCT_TYPE;
                            break;
                        case XML_RPC_Parameter_Type.ARRAY:
                            eRootType = XML_RPC_COLL_DATA_TYPE.ARRAY;
                            break;
                        default:
                            eRootType = XML_RPC_COLL_DATA_TYPE.SIMPLE_TYPE;
                            break;
                    }
                }
            }
            return strModifiedPath;
            //TODO:remove this call from here.

        }
        void iterateThroughParam(string nameToFind, int iterationLevel, RpcParamaters rpcParamFromJson
            , List<param> paramsBeforeIteration
            , out List<param> paramsAfterIteration, out XML_RPC_Parameter_Type paramType)
        {
            if (iterationLevel == 0)
            {
                List<param> lstParams = rpcParamFromJson.param.FindAll
                    (
                    delegate(param parameter)
                    { return parameter.name == nameToFind; }
                    );
                if (lstParams.Count == 0)
                {
                    throw new Exception("Params not found");
                }
                else if (lstParams.Count > 1)
                {
                    throw new Exception("Duplicate Params");
                }
                paramType = getParamType(lstParams[0].typ);
                paramsAfterIteration = lstParams[0].inparam;
            }
            else
            {
                List<param> lstParams = paramsBeforeIteration.FindAll
                    (
                    delegate(param parameter)
                    { return parameter.name == nameToFind; }
                    );

                if (lstParams.Count > 1)
                {
                    throw new Exception("Duplicate Params");
                }
                if (lstParams.Count == 0)
                {
                    throw new Exception("Params not found");
                }
                else if (lstParams.Count > 1)
                {
                    throw new Exception("Duplicate Params");
                }
                paramType = getParamType(lstParams[0].typ);
                paramsAfterIteration = lstParams[0].inparam;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException"></exception>
        XML_RPC_Parameter_Type getParamType(string value)
        {
            try
            {
                XML_RPC_Parameter_Type eXmlRpcParamType = (XML_RPC_Parameter_Type)Enum.Parse(typeof(XML_RPC_Parameter_Type), value);
                if (Enum.IsDefined(typeof(XML_RPC_Parameter_Type), eXmlRpcParamType))
                {
                    return eXmlRpcParamType;
                }
                else
                {
                    throw new ArgumentException("Parameter type not defined");
                }
            }
            catch
            {
                throw new ArgumentException("Parameter type not defined");
            }
        }
        #endregion
        #region Deserialise Output Json
        RpcParamaters getRpcOutputParamAfterDeserialisation(string json)
        {
            //TODO :Remove this test json
            //json = getOutputJson();
            RpcParamaters objRpcParam = new RpcParamaters();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(objRpcParam.GetType());
            objRpcParam = serializer.ReadObject(ms) as RpcParamaters;
            ms.Close();
            return objRpcParam;
        }

        #endregion
        #region Rpc Param Class For Json Derialization


        #endregion
    }
    [DataContract]
    public class RpcParamaters
    {
        [DataMember]
        public List<param> param { get; set; }
    }
    [DataContract]
    public class param
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string typ { get; set; }
        [DataMember]
        public List<param> inparam { get; set; }
    }
}
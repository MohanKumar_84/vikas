﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace mFicientWS
{
    public class SupportQuery
    {
        SupportQueryReq _supportQueryReq;

        public SupportQuery(SupportQueryReq supportQueryReq)
        {
            _supportQueryReq = supportQueryReq;
        }

        public SupportQueryResp Process()
        {
            ResponseStatus objResponseStatus = new ResponseStatus();
            objResponseStatus.cd = "0";
            objResponseStatus.desc = "";
            int iReturnValAfterExecutingQUery = 0;
            string strTokenNo = "";

            SqlCommand cmd = new SqlCommand("dbo.InsertSupportQuery");
            cmd.CommandType = CommandType.StoredProcedure;
            strTokenNo = getTokenID();
            cmd.Parameters.AddWithValue("@TOKEN_NO", strTokenNo);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _supportQueryReq.CompanyId);
            cmd.Parameters.AddWithValue("@USER_ID", _supportQueryReq.UserID);
            cmd.Parameters.AddWithValue("@POSTED_ON", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@QUERY_SUBJECT", _supportQueryReq.SubjectQuery);
            cmd.Parameters.AddWithValue("@QUERY_TYPE", _supportQueryReq.Querytype);
            cmd.Parameters.AddWithValue("@QUERY", _supportQueryReq.Query);
            cmd.Parameters.AddWithValue("@IS_RESOLVED", false);
            cmd.Parameters.AddWithValue("@RESOLVED_BY_CREATOR", false);
            cmd.Parameters.AddWithValue("@RESOLVED_ON", 0);
            cmd.Parameters.AddWithValue("@IS_FORWARD_TO_ADMIN", false);
            cmd.Parameters.AddWithValue("@RESOLVED_BY", String.Empty);
            cmd.Parameters.Add("@ReturnVal", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@AlreadySavedTokenNo", SqlDbType.NVarChar,20).Direction = ParameterDirection.Output;
             MSSqlDatabaseClient.ExecuteNonQueryRecord(
                cmd,
                MSSqlDatabaseClient.getConnectionStringFromWebConfig(MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MFICIENT)
                );
             iReturnValAfterExecutingQUery = Convert.ToInt32(cmd.Parameters["@ReturnVal"].Value);
             if (iReturnValAfterExecutingQUery == 1)
             {
                 return new SupportQueryResp(
                     objResponseStatus,
                     _supportQueryReq.RequestId,
                     strTokenNo,
                     false);
             }
             else if (iReturnValAfterExecutingQUery == -1)
             {
                 return new SupportQueryResp(
                     objResponseStatus,
                     _supportQueryReq.RequestId,
                     Convert.ToString(cmd.Parameters["@AlreadySavedTokenNo"].Value),
                     true);
             }
             else
             {
                 throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
             }
        }
        #region Previous Code
        //public SupportQueryResp Process()
        //{
        //    ResponseStatus objResponseStatus = new ResponseStatus();
        //    objResponseStatus.cd = "0";
        //    objResponseStatus.desc = "";

        //    string strTokenNo = "";

        //    SqlCommand cmd = new SqlCommand("INSERT INTO ADMIN_TBL_QUERY([TOKEN_NO],[COMPANY_ID],[USER_ID],[QUERY_TYPE],[QUERY_SUBJECT],[QUERY],[POSTED_ON],[IS_RESOLVED],[RESOLVED_BY_CREATOR],[RESOLVED_ON],IS_FORWARD_TO_ADMIN)" +
        //    "VALUES (@TOKEN_NO,@COMPANY_ID,@USER_ID,@QUERY_TYPE,@QUERY_SUBJECT,@QUERY,@POSTED_ON,@IS_RESOLVED,@RESOLVED_BY_CREATOR,@RESOLVED_ON,'false')");
        //    cmd.CommandType = CommandType.Text;

        //    strTokenNo = getTokenID();
        //    cmd.Parameters.AddWithValue("@TOKEN_NO", strTokenNo);
        //    cmd.Parameters.AddWithValue("@COMPANY_ID", _supportQueryReq.CompanyId);
        //    cmd.Parameters.AddWithValue("@USER_ID", _supportQueryReq.UserID);
        //    cmd.Parameters.AddWithValue("@QUERY_TYPE", _supportQueryReq.Querytype);
        //    cmd.Parameters.AddWithValue("@POSTED_ON", System.DateTime.UtcNow.Ticks);
        //    cmd.Parameters.AddWithValue("@QUERY_SUBJECT", _supportQueryReq.SubjectQuery);

        //    cmd.Parameters.AddWithValue("@QUERY", _supportQueryReq.Query);

        //    cmd.Parameters.AddWithValue("@IS_RESOLVED", false);
        //    cmd.Parameters.AddWithValue("@RESOLVED_BY_CREATOR", false);
        //    cmd.Parameters.AddWithValue("@RESOLVED_ON", 0);
        //    return new SupportQueryResp(objResponseStatus, _supportQueryReq.RequestId, strTokenNo);
        //}
        #endregion
        private string getTokenID()
        {
            string strTokenNo = Utilities.GetMd5Hash(_supportQueryReq.CompanyId + _supportQueryReq.UserID + "1" + System.DateTime.UtcNow.Ticks);
            int index = 0;
            Boolean blnIsSessionIdValid = false;
            string strNewTokenId = string.Empty;
            do
            {
                strNewTokenId = strTokenNo.Substring(index, 10);
                blnIsSessionIdValid = isTokenValid(strNewTokenId);
                index = index + 1;
            }
            while (blnIsSessionIdValid == false);

            return strNewTokenId;
        }

        private Boolean isTokenValid(string strTokenNo)
        {
            Boolean blnIsSessionIdValid = true;

            SqlCommand objSqlCommand = new SqlCommand("select token_no from ADMIN_TBL_QUERY where token_no=@TOKEN_NO AND  COMPANY_ID=@COMPANY_ID");
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@TOKEN_NO", strTokenNo);
            objSqlCommand.Parameters.AddWithValue("@COMPANY_ID", _supportQueryReq.CompanyId);
            DataSet objDataSet = MSSqlDatabaseClient.SelectDataFromSqlCommand(objSqlCommand);
            if (objDataSet.Tables[0].Rows.Count > 0)
                blnIsSessionIdValid = false;
            return blnIsSessionIdValid;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class UserDetail
    {
        string userId, companyId, RequestId;
       public UserDetail(UserDetailReq userDtlsReq)
        {
            userId = userDtlsReq.UserId;
            companyId = userDtlsReq.CompanyId;
            RequestId = userDtlsReq.RequestId;
        }
       public UserDetail(string _userId, string _companyId, string _requestId)
       {
           userId = _userId;
           companyId = _companyId;
           RequestId = _requestId;
       }

       public UserDetailResp Process()
       {
           DataSet dsUserDtls = getUserDtlsByUserId();
           if (dsUserDtls != null && dsUserDtls.Tables.Count != 0)
           {
               if (dsUserDtls.Tables[0].Rows.Count != 0)
               {
                   ResponseStatus objRespStatus = new ResponseStatus();
                   objRespStatus.cd = "0";
                   objRespStatus.desc = "";
                   return new UserDetailResp(objRespStatus, GetData(dsUserDtls), RequestId);
               }
               else
                   throw new Exception(((int)HttpStatusCode.Unauthorized).ToString());
           }
           else
               throw new Exception(((int)HttpStatusCode.Unauthorized).ToString());
       }

       public UserDetailResponseData GetUserDetail()
       {
           DataSet dsUserDtls = getUserDtlsByUserId();
           if (dsUserDtls != null && dsUserDtls.Tables.Count != 0)
           {
               if (dsUserDtls.Tables[0].Rows.Count != 0)
               {
                   ResponseStatus objRespStatus = new ResponseStatus();
                   objRespStatus.cd = "0";
                   objRespStatus.desc = "";
                   return GetData(dsUserDtls);
               }
           }
           return null;
       }

       UserDetailResponseData GetData(DataSet dsUserDtls)
       {          
           UserDetailResponseData objUserDtlsRespData = new UserDetailResponseData();

           DataRow row = dsUserDtls.Tables[0].Rows[0];
           objUserDtlsRespData.ufnm = Convert.ToString(row["FIRST_NAME"]);
           objUserDtlsRespData.ulnm = Convert.ToString(row["LAST_NAME"]);
           objUserDtlsRespData.mob = Convert.ToString(row["MOBILE"]);
           objUserDtlsRespData.dob = Convert.ToString(row["DATE_OF_BIRTH"]);
           objUserDtlsRespData.em = Convert.ToString(row["EMAIL_ID"]);
           objUserDtlsRespData.dsgc = "";
           objUserDtlsRespData.des = "";
           objUserDtlsRespData.locc = "";
           objUserDtlsRespData.loc = "";
           objUserDtlsRespData.regc ="";
           objUserDtlsRespData.reg = "";
           objUserDtlsRespData.msgr = Convert.ToBoolean(row["ALLOW_MESSENGER"]) ? "1" : "0";
           objUserDtlsRespData.olw = Convert.ToBoolean(row["IS_OFFLINE_WORK"]) ? "1" : "0";
           objUserDtlsRespData.div = "";
           objUserDtlsRespData.divc = "";
           objUserDtlsRespData.dep = "";
           objUserDtlsRespData.depc ="";
           objUserDtlsRespData.empid ="";
           objUserDtlsRespData.admin = "";
           string strcustom = Convert.ToString(row["CUSTOM_PROPERTIES"]);
           objUserDtlsRespData.cust = new List<CustomProperty>();
           if (strcustom != "")
           {
               objUserDtlsRespData.cust = Utilities.DeserialiseJson<List<CustomProperty>>(strcustom);
           }
           return objUserDtlsRespData;
       }

       DataSet getUserDtlsByUserId()
       {
           string strQuery = @"SELECT userDtl.*,ISNULL(usrCompany.COMPANY_NAME,'') as COMPANY_NAME,ISNULL(sadmin.[SUBADMIN_ID],'') as SUBADMIN_ID
                             FROM tbl_user_detail as userDtl inner JOIN tbl_company_detail as usrCompany ON usrCompany.company_id = userDtl.company_id
                             left outer join tbl_sub_admin as sadmin on sadmin.MOBILE_USER = userDtl.user_ID
                             WHERE userDtl.user_id = @UserId AND  userDtl.COMPANY_ID=@COMPANY_ID;";
           SqlCommand cmd = new SqlCommand(strQuery);
           cmd.CommandType = CommandType.Text;
           cmd.Parameters.AddWithValue("@UserId", userId);
           cmd.Parameters.AddWithValue("@COMPANY_ID", companyId);
           return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
       }
      public List<CustomProperty> lst { get; set; }
    }

    public class CustomProperty
    {
        /// <summary>
        ///  Name 
        /// </summary>
        [DataMember]
        public string name { get; set; }
        /// <summary>
        ///  Name 
        /// </summary>
        [DataMember]
        public string value { get; set; }

    }
}
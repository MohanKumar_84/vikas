﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
namespace mFicientWS
{
    public class SavePushMsgFromUser
    {
        SavePushMsgFromUserReq _savePushMsgFromUserReq;
        //int _notificationType = 0;
        public SavePushMsgFromUser(SavePushMsgFromUserReq savePushMsgFromUserReq)
        {
            _savePushMsgFromUserReq = savePushMsgFromUserReq;
        }

        public SavePushMsgFromUserResp Process()
        {
            try
            {
                if (!String.IsNullOrEmpty(_savePushMsgFromUserReq.SendToUsername))
                {
                    processForUsername();
                }
                if (!String.IsNullOrEmpty(_savePushMsgFromUserReq.SendToGroupName))
                {
                    processForGroupName();
                }
            }
            catch (MficientException ex)
            {
            }
            ResponseStatus objRespStatus =  new ResponseStatus();
            objRespStatus.cd = "0";
            objRespStatus.desc="";
            return new SavePushMsgFromUserResp(objRespStatus, _savePushMsgFromUserReq.RequestId);
        }
        void processForUsername()
        {
            List<mFicientCommonProcess.mGarmUserdetails> users = new List<mFicientCommonProcess.mGarmUserdetails>();
            users.Add(new mFicientCommonProcess.mGarmUserdetails(_savePushMsgFromUserReq.SendToUsername, _savePushMsgFromUserReq.CompanyId, "", getUserDevice()));
            SqlConnection con ;
            MSSqlDatabaseClient.SqlConnectionOpen(out con,MSSqlDatabaseClient.getConnectionStringFromWebConfig(
                MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MGRAM));
            try
            {
                mFicientCommonProcess.SendNotification.SaveMgramMessage(users, con, null, "App Notification", _savePushMsgFromUserReq.Message, DateTime.UtcNow.Ticks,7);
            }
            catch
            {

            }
            finally
            {
                MSSqlDatabaseClient.SqlConnectionClose(con);
            }
        }
        List<List<string>> getUserDevice()
        {
            List<List<string>> obj = new List<List<string>>();
            string strQuery = "select * from tbl_registered_device as r inner join tbl_user_detail as u on r.user_id=u.user_id  and r.company_id=u.Company_id   where user_name=@user_name and r.COMPANY_ID=@COMPANY_ID;";
            SqlCommand sqlCommand = new SqlCommand(strQuery);
            sqlCommand.Parameters.AddWithValue("@COMPANY_ID", _savePushMsgFromUserReq.CompanyId);
            sqlCommand.Parameters.AddWithValue("@user_name", _savePushMsgFromUserReq.SendToUsername);
            DataSet ds = MSSqlDatabaseClient.SelectDataFromSQlCommand(MSSqlDatabaseClient.getConnectionStringFromWebConfig(
                MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MFICIENT), sqlCommand);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                List<string> item = new List<string>();
                item.Add(Convert.ToString(dr["DEVICE_PUSH_MESSAGE_ID"]));
                item.Add(Convert.ToString(dr["DEVICE_TYPE"]));
                item.Add(Convert.ToString(dr["DEVICE_ID"]));
                obj.Add(item);
            }
            return obj;
        }
        long getExpirationDateInTicks(long SendDatetime)
        {

            DateTime dtSendTime = DateTime.UtcNow;
            DateTime dtExpiryTime = DateTime.UtcNow;
            long lngSendTime = dtSendTime.Ticks;
            if (SendDatetime != 0)
            {
                dtSendTime = new DateTime(SendDatetime, DateTimeKind.Utc);
            }
            dtExpiryTime = dtSendTime.AddDays(7);
            return dtExpiryTime.Ticks;
        }
        void processForGroupName()
        {
            string strQuery = @"SELECT userDetail.USER_NAME FROM TBL_USER_GROUP_LINK userGroup INNER JOIN
                TBL_USER_DETAIL userDetail ON userGroup.USER_ID = userDetail.USER_ID
                INNER JOIN TBL_USER_GROUP groups ON groups.group_id = userGroup.group_id WHERE groups.GROUP_NAME  =@GROUP_NAME and userGroup.COMPANY_ID=@COMPANY_ID;
                select * from tbl_registered_device as r inner join tbl_user_detail as u on r.user_id=u.user_id INNER JOIN TBL_USER_GROUP_LINK groups ON groups.user_id = u.user_id
                inner join TBL_USER_GROUP userGroup on userGroup.GROUP_ID=groups.GROUP_ID and r.company_id=u.Company_id  where  r.COMPANY_ID=@COMPANY_ID;";
            
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@GROUP_NAME", _savePushMsgFromUserReq.SendToGroupName);
            cmd.Parameters.AddWithValue("@COMPANY_ID", _savePushMsgFromUserReq.CompanyId);

            cmd.CommandType = CommandType.Text;
            DataSet dsGroupUsersDtls = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            List<mFicientCommonProcess.mGarmUserdetails> users = new List<mFicientCommonProcess.mGarmUserdetails>();
            List<List<string>> objDevive;        
            if (dsGroupUsersDtls != null)
            {
                foreach (DataRow row in dsGroupUsersDtls.Tables[0].Rows)
                {
                    objDevive = new List<List<string>>();
                    DataRow[] drows = dsGroupUsersDtls.Tables[1].Select();
                    foreach (DataRow drow in drows)
                    {
                        List<string> obj = new List<string>();
                        obj.Add(Convert.ToString(drow["DEVICE_PUSH_MESSAGE_ID"]));
                        obj.Add(Convert.ToString(drow["DEVICE_TYPE"]));
                        obj.Add(Convert.ToString(drow["DEVICE_ID"]));
                        objDevive.Add(obj);
                    }
                    users.Add(new mFicientCommonProcess.mGarmUserdetails(Convert.ToString(row["USER_NAME"]), _savePushMsgFromUserReq.CompanyId, "", objDevive));
                }                
            }
            
            SqlConnection con;
            MSSqlDatabaseClient.SqlConnectionOpen(out con, MSSqlDatabaseClient.getConnectionStringFromWebConfig(
                MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MGRAM));
            try
            {
                mFicientCommonProcess.SendNotification.SaveMgramMessage(users, con, null, "App Notification", _savePushMsgFromUserReq.Message, DateTime.UtcNow.Ticks, 7);
            }
            catch
            {

            }
            finally
            {
                MSSqlDatabaseClient.SqlConnectionClose(con);
            }
        }        
    }
}
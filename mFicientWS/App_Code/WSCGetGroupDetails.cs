﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientWS
{
    public class WSCGetGroupDetail
    {
        string  _companyId,
            _statusDescription;
        int _statusCode;
        //MFEMobileUser _mobUser;

        #region Public Properties
        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }
       
        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }
        //public MFEMobileUser MobUser
        //{
        //    get { return _mobUser; }
        //    private set { _mobUser = value; }
        //}
        #endregion
        #region Public Methods
        public WSCGetGroupDetail(string companyId)
        {
            this.CompanyId = companyId;
            this.StatusCode = 0;
            this.StatusDescription = String.Empty;
        }
        public MFEGroup getGroupDetailByName(string groupName)
        {
            MFEGroup objGroup =new MFEGroup();
            try
            {
                string strQuery = getSqlQueryForGroupByName();
                SqlCommand cmd = getCmdForGroupByName(strQuery,groupName);
                DataSet dsGroupDetail = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (dsGroupDetail == null) throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                if (dsGroupDetail.Tables[0].Rows.Count > 0)
                {
                    DataTable dtblGroupDtl = dsGroupDetail.Tables[0];
                    DataRow rowGroupDtl = dtblGroupDtl.Rows[0];
                    objGroup = getGroupDtlFromDataRow(rowGroupDtl);
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return objGroup;
        }
        #endregion
        #region Sql Query
        
        string getSqlQueryForGroupByName()
        {
            return @"SELECT * FROM TBL_USER_GROUP
                    WHERE GROUP_NAME = @GROUP_NAME
                    AND COMPANY_ID =@COMPANY_ID";
        }
        #endregion
        #region Sql Commands
        
        SqlCommand getCmdForGroupByName(string query,string groupName)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@GROUP_NAME", groupName);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            return cmd;
        }
        #endregion

        
        MFEGroup getGroupDtlFromDataRow(DataRow rowCmpGroupDtl)
        {
            if (rowCmpGroupDtl == null) throw new ArgumentNullException();
            MFEGroup objGroup = new MFEGroup();
            objGroup.CompanyId = Convert.ToString(rowCmpGroupDtl["COMPANY_ID"]);
            objGroup.GroupId = Convert.ToString(rowCmpGroupDtl["GROUP_ID"]);
            objGroup.GroupName = Convert.ToString(rowCmpGroupDtl["GROUP_NAME"]);
            objGroup.SubadminId = Convert.ToString(rowCmpGroupDtl["SUBADMIN_ID"]);
            objGroup.CreatedOn = Convert.ToInt64(rowCmpGroupDtl["CREATED_ON"]);
            
            return objGroup;
        }
    }
}
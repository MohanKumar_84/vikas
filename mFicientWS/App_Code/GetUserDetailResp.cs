﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class GetUserDetailResp
    {
         ResponseStatus _respStatus;

        string _requestId;
        UserDetailResponseData _getmetadata;
        public GetUserDetailResp(ResponseStatus respStatus, UserDetailResponseData getlist, string requestId)
        {
            _respStatus = respStatus;
            _getmetadata = getlist;
            _requestId = requestId;
        }
        public string GetResponseJson()
        {
            GetUserDetailRespJson objgetRegistrationResp = new GetUserDetailRespJson();
            objgetRegistrationResp.func = Convert.ToString((int)FUNCTION_CODES.Get_User_Detail);
            objgetRegistrationResp.rid = _requestId;
            objgetRegistrationResp.status = _respStatus;
           
            objgetRegistrationResp.data = _getmetadata;
            string strJsonWithDetails = Utilities.SerializeJson<GetUserDetailRespJson>(objgetRegistrationResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
        [DataContract]
        public class GetUserDetailRespJson : CommonResponse
        {
            public GetUserDetailRespJson()
            { }
            [DataMember]
            public UserDetailResponseData data { get; set; }
            
        }
    }    
}
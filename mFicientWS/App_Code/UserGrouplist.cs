﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class UserGrouplist
    {
        User_Group_ListReq _getusergroup;

        public UserGrouplist(User_Group_ListReq getlistgroupuser)
        {
            _getusergroup = getlistgroupuser;
        }
        public User_Group_ListResp Process()
        {
            ResponseStatus _respStatus = new ResponseStatus();
            _respStatus.cd = "0";
            _respStatus.desc = "";
            User_Group_ListMetaData getdevicedata = new User_Group_ListMetaData();
            string strQuery = @"select * from  dbo.TBL_USER_GROUP where COMPANY_ID=@ENTERPRISE_ID;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@ENTERPRISE_ID", _getusergroup.CompanyId);
            DataSet ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (ds.Tables[0].Rows.Count > 0)
            {
               
                List<Group> listregistegroup = new List<Group>();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Group objregisteruser = new Group();
                    ResponseStatus objRespStatus = new ResponseStatus();
                    objRespStatus.cd = "0";
                    objRespStatus.desc = "";
                    objregisteruser.id = Convert.ToString(row["GROUP_ID"]);
                    objregisteruser.nm = Convert.ToString(row["GROUP_NAME"]);
                    listregistegroup.Add(objregisteruser);
                } 
             getdevicedata.grp=listregistegroup;
            }
            else
                throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            return new User_Group_ListResp(_respStatus, getdevicedata, _getusergroup.RequestId);
           
        }
    }
}
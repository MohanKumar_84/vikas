﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;

namespace mFicientWS
{
    public class AddDevicePushMsgIDreq
    {
        string _userId, _sessionId, _requestId, _deviceId, _deviceType, _companyId, _pushMsgId;
        int _functionCode;
        string _userName;

        public string UserName
        {
            get { return _userName; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public string SessionId
        {
            get { return _sessionId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string PushMsgId
        {
            get { return _pushMsgId; }
        }
        public AddDevicePushMsgIDreq(string requestJson, string userId)
        {
            //RequestJsonParsing<AddDevicePushMsgIDreqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<AddDevicePushMsgIDreqData>>(requestJson);
            JObject objRequestJsonParsing = new JObject();
            objRequestJsonParsing = (JObject)objRequestJsonParsing["req"];
            _functionCode = Convert.ToInt32(objRequestJsonParsing["func"]);
            if (_functionCode != (int)FUNCTION_CODES.ADD_DEVICE_PUSH_MSG_ID)
            {
                throw new Exception("Invalid Function Code");
            }
            _userId = userId;
            _sessionId = Convert.ToString(objRequestJsonParsing["sid"]);
            _requestId = Convert.ToString(objRequestJsonParsing["rid"]);
            _deviceId = Convert.ToString(objRequestJsonParsing["did"]);
            _deviceType = Convert.ToString(objRequestJsonParsing["dtyp"]);
            JObject objRequestData = (JObject)objRequestJsonParsing["data"];
            _companyId =Convert.ToString(objRequestData["enid"]);
            _pushMsgId = Convert.ToString(objRequestData["pmid"]).Trim();
            _userName = Convert.ToString(objRequestData["unm"]);
            if (_userName.Contains("\\"))
                _userName = _userName.Split('\\')[1];
        }
    }
}

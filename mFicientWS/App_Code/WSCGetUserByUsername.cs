﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientWS
{
    public class WSCGetUserByUsername
    {
        string _companyId,
            _statusDescription;
        int _statusCode;
        

        #region Public Properties
        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }
        
        #endregion
        #region Public Methods
        public WSCGetUserByUsername(string companyId)
        {
            this.CompanyId = companyId;
            this.StatusCode = 0;
            this.StatusDescription = String.Empty;
        }
        public MFEMobileUser getUserDetail(string username)
        {
            MFEMobileUser objMobileUser =
                new MFEMobileUser();
            try
            {
                string strQuery = getSqlQueryForUserDetail();
                SqlCommand cmd = getCmdForUserDetail(strQuery,username);
                DataSet dsUserDetail = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (dsUserDetail == null) throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                if (dsUserDetail.Tables[0].Rows.Count > 0)
                {
                    DataTable dtblUserDtl = dsUserDetail.Tables[0];
                    DataRow rowUserDtl = dtblUserDtl.Rows[0];
                    objMobileUser = getMobileUserFromDataRow(rowUserDtl,null);
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return objMobileUser;
        }
        #endregion
        #region Sql Query
        string getSqlQueryForUserDetail()
        {
            return @"SELECT * FROM TBL_USER_DETAIL
                        WHERE USER_NAME = @USER_NAME
                        AND COMPANY_ID = @COMPANY_ID;";
        }
        
        #endregion
        #region Sql Commands
        SqlCommand getCmdForUserDetail(string query,string username)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.Parameters.AddWithValue("@COMPANY_ID", this.CompanyId);
            cmd.Parameters.AddWithValue("@USER_NAME", username);
            return cmd;
        }
        
        #endregion

        MFEMobileUser getMobileUserFromDataRow(DataRow rowUserDtl,List<MobileDetail> objmobsDetail)
        {
            if (rowUserDtl == null) throw new ArgumentNullException();
            MFEMobileUser objMobileUser = new MFEMobileUser();
            objMobileUser.UserId = Convert.ToString(rowUserDtl["USER_ID"]);
            objMobileUser.EmailId = Convert.ToString(rowUserDtl["EMAIL_ID"]);
            objMobileUser.Password = Convert.ToString(rowUserDtl["ACCESS_CODE"]);
            objMobileUser.FirstName = Convert.ToString(rowUserDtl["FIRST_NAME"]);
            objMobileUser.LastName = Convert.ToString(rowUserDtl["LAST_NAME"]);
            objMobileUser.Mobile = Convert.ToString(rowUserDtl["MOBILE"]);
            objMobileUser.CompanyId = Convert.ToString(rowUserDtl["COMPANY_ID"]);
            objMobileUser.Dob = Convert.ToInt64(rowUserDtl["DATE_OF_BIRTH"]);
            objMobileUser.RegistrationDatetime = Convert.ToInt64(rowUserDtl["REGISTRATION_DATETIME"]);
            objMobileUser.SubAdminId = Convert.ToString(rowUserDtl["SUBADMIN_ID"]);
            objMobileUser.LocationId = Convert.ToString(rowUserDtl["LOCATION_ID"]);
            objMobileUser.DesignationId = Convert.ToString(rowUserDtl["DESIGNATION_ID"]);
            objMobileUser.EmployeeNo = Convert.ToString(rowUserDtl["EMPLOYEE_NO"]);
            objMobileUser.Username = Convert.ToString(rowUserDtl["USER_NAME"]);
            objMobileUser.IsActive = Convert.ToBoolean(rowUserDtl["IS_ACTIVE"]);
            objMobileUser.RegionId = Convert.ToString(rowUserDtl["REGION_ID"]);
            objMobileUser.IsBlocked = Convert.ToBoolean(rowUserDtl["IS_BLOCKED"]);
            objMobileUser.AllowMessenger = Convert.ToBoolean(rowUserDtl["ALLOW_MESSENGER"]);
            objMobileUser.IsOfflineWorkAllowed = Convert.ToBoolean(rowUserDtl["IS_OFFLINE_WORK"]);
            objMobileUser.UpdatedOn = Convert.ToInt64(rowUserDtl["UPDATE_ON"]);
            objMobileUser.DesktopMessenger = Convert.ToBoolean(rowUserDtl["DESKTOP_MESSENGER"]);
            objMobileUser.RequestedBy = Convert.ToString(rowUserDtl["REQUESTED_BY"]);
            objMobileUser.DomainId = Convert.ToString(rowUserDtl["DOMAIN_ID"]);
            objMobileUser.MobileDetails = objmobsDetail;
            return objMobileUser;
        }
        
    }
}
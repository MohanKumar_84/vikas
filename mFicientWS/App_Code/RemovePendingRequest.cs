﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class RemovePendingRequest
    {
        RemovePendingRequestReq removePendingRequestReq;

        public RemovePendingRequest(RemovePendingRequestReq _removePendingRequestReq)
        {
            removePendingRequestReq = _removePendingRequestReq;
        }

        public RemovePendingRequestResp Process()
        {
            string strContactId, strUserId;
            strContactId = Utilities.GetUserID(removePendingRequestReq.CompanyId, removePendingRequestReq.ContactName);
            strUserId = Utilities.GetUserID(removePendingRequestReq.CompanyId, removePendingRequestReq.UserName);
            ResponseStatus objRespStatus = new ResponseStatus();

            if (strContactId.Length > 0 && strUserId.Length > 0)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(@"DELETE FROM TBL_ADD_CONTACT_REQUEST WHERE FROM_USER_ID = @FROM_USER_ID AND
                                                     TO_USER_ID= @TO_USER_ID AND COMPANY_ID= @COMPANY_ID");
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@TO_USER_ID", strContactId);
                    cmd.Parameters.AddWithValue("@COMPANY_ID", removePendingRequestReq.CompanyId);
                    cmd.Parameters.AddWithValue("@FROM_USER_ID", strUserId);

                    if (MSSqlDatabaseClient.ExecuteNonQueryRecord(cmd) > 0)
                    {
                        objRespStatus.cd = "0";
                        objRespStatus.desc = "";
                        return new RemovePendingRequestResp(objRespStatus, removePendingRequestReq.RequestId);
                    }
                    else
                    {
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_DELETE_ERROR).ToString());
                    }
                }
                catch (Exception ex)
                {
                    if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
            }
            objRespStatus.cd = "2000701";
            objRespStatus.desc = "User Not Found";
            return new RemovePendingRequestResp(objRespStatus, removePendingRequestReq.RequestId);
        }
    }
}
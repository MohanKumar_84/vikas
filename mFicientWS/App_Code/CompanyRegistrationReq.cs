﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    //public class CompanyRegistrationReq
    //{
    //    //string _firstName, _middleName, _lastName;
    //    //string _email, _password, _mobile, _dateOfBirth;
    //    //string _companyId, _companyName;
    //    //string _streetAddress1, _streetAddress2, _streetAddress3;
    //    //string _city, _state, _country, _timezoneID, _zip;

    //    int _functionCode;
    //    string _requestId;

    //    MFECompanyAdmin _companyAdmin = new MFECompanyAdmin();
    //    MFECompanyInfo _companyInfo = new MFECompanyInfo();

    //    #region Public Properties
    //    public MFECompanyAdmin CompanyAdmin
    //    {
    //        get { return _companyAdmin; }
    //    }

    //    public MFECompanyInfo CompanyInfo
    //    {
    //        get { return _companyInfo; }
    //    }

    //    public int FunctionCode
    //    {
    //        get { return _functionCode; }
    //    }
    //    public string RequestId
    //    {
    //        get { return _requestId; }
    //    }
    //    #endregion

    //    public CompanyRegistrationReq(string requestJson)
    //    {
    //        RequestJsonParsing<CompanyRegistrationReqData> objRequestJsonParsing =
    //            Utilities.DeserialiseJson<RequestJsonParsing<CompanyRegistrationReqData>>(requestJson);
    //        _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
    //        if (_functionCode != (int)FUNCTION_CODES.COMPANY_REGISTRATION)
    //        {
    //            throw new Exception("Invalid Function Code");
    //        }
    //        _requestId = objRequestJsonParsing.req.rid;
    //        CompanyAdmin.Firstname = objRequestJsonParsing.req.data.fnm;
    //        CompanyAdmin.MiddleName = objRequestJsonParsing.req.data.mnm;
    //        CompanyAdmin.LastName = objRequestJsonParsing.req.data.lnm;
    //        CompanyAdmin.EmailId = objRequestJsonParsing.req.data.em;
    //        CompanyAdmin.Password = objRequestJsonParsing.req.data.pwd;
    //        CompanyAdmin.MobileNo = objRequestJsonParsing.req.data.mob;
    //        CompanyAdmin.CityName = objRequestJsonParsing.req.data.city;
    //        CompanyAdmin.StateName = objRequestJsonParsing.req.data.state;
    //        CompanyAdmin.CountryCode = objRequestJsonParsing.req.data.cntry;

    //        CompanyInfo.CompanyId = objRequestJsonParsing.req.data.enid;
    //        CompanyInfo.CompanyName = objRequestJsonParsing.req.data.ennm;
    //        CompanyInfo.StreetAddress1 = objRequestJsonParsing.req.data.add1;
    //        CompanyInfo.StreetAddress2 = objRequestJsonParsing.req.data.add2;
    //        CompanyInfo.StreetAddress3 = objRequestJsonParsing.req.data.add3;
    //        CompanyInfo.StateName = objRequestJsonParsing.req.data.state;
    //        CompanyInfo.CityName = objRequestJsonParsing.req.data.city;
    //        CompanyInfo.CountryCode = objRequestJsonParsing.req.data.cntry;
    //        CompanyInfo.TimezoneId = objRequestJsonParsing.req.data.tzn;
    //        CompanyInfo.Zip = objRequestJsonParsing.req.data.zip;
    //        CompanyAdmin.Zip = objRequestJsonParsing.req.data.zip;
    //    }
    //}

    //[DataContract]
    //public class CompanyRegistrationReqData : Data
    //{
    //    public CompanyRegistrationReqData()
    //    { }
    //    /// <summary>
    //    /// First name
    //    /// </summary>
    //    [DataMember]
    //    public string fnm { get; set; }

    //    /// <summary>
    //    /// Middle name
    //    /// </summary>
    //    [DataMember]
    //    public string mnm { get; set; }

    //    /// <summary>
    //    /// Last name
    //    /// </summary>
    //    [DataMember]
    //    public string lnm { get; set; }

    //    /// <summary>
    //    /// Password
    //    /// </summary>
    //    [DataMember]
    //    public string pwd { get; set; }

    //    /// <summary>
    //    /// Mobile No
    //    /// </summary>
    //    [DataMember]
    //    public string mob { get; set; }

    //    /// <summary>
    //    /// Enterprise name
    //    /// </summary>
    //    [DataMember]
    //    public string ennm { get; set; }

    //    /// <summary>
    //    /// Street address 1
    //    /// </summary>
    //    [DataMember]
    //    public string add1 { get; set; }

    //    /// <summary>
    //    /// Street address 2
    //    /// </summary>
    //    [DataMember]
    //    public string add2 { get; set; }

    //    /// <summary>
    //    /// Street add 3
    //    /// </summary>
    //    [DataMember]
    //    public string add3 { get; set; }

    //    /// <summary>
    //    /// City
    //    /// </summary>
    //    [DataMember]
    //    public string city { get; set; }

    //    /// <summary>
    //    ///State
    //    /// </summary>
    //    [DataMember]
    //    public string state { get; set; }

    //    /// <summary>
    //    /// Country
    //    /// </summary>
    //    [DataMember]
    //    public string cntry { get; set; }

    //    /// <summary>
    //    /// Timezone ID
    //    /// </summary>
    //    [DataMember]
    //    public string tzn { get; set; }

    //    /// <summary>
    //    /// zip
    //    /// </summary>
    //    [DataMember]
    //    public string zip { get; set; }
    //}
}
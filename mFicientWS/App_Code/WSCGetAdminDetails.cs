﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientWS
{
    public class WSCGetAdminDetails
    {
        string _userId, _companyId,
            _statusDescription;
        int _statusCode;


        #region Public Properties
        public string CompanyId
        {
            get { return _companyId; }
            private set { _companyId = value; }
        }

        public string UserId
        {
            get { return _userId; }
            private set { _userId = value; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }

        #endregion
        public WSCGetAdminDetails()
        {
            this.StatusCode = 0;
            this.StatusDescription = String.Empty;
        }
        public MFECompanyAdmin getAdminDetailByEmail(string emailId)
        {
            MFECompanyAdmin objCmpAdmin =
                new MFECompanyAdmin();
            try
            {
                string strQuery = getSqlQueryForAdminByEmail();
                SqlCommand cmd = getCmdForAdminByEmail(strQuery, emailId);
                DataSet dsAdminDetail = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (dsAdminDetail == null) throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                if (dsAdminDetail.Tables[0].Rows.Count > 0)
                {
                    DataRow rowUserDtl = dsAdminDetail.Tables[0].Rows[0];
                    objCmpAdmin = getAdminFromDataRow(rowUserDtl);
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return objCmpAdmin;
        }
         MFECompanyAdmin getAdminDetailByEmail(string emailId,out bool adminExists)
        {
            adminExists = false;
            MFECompanyAdmin objCmpAdmin =
               new MFECompanyAdmin();
            try
            {
                string strQuery = getSqlQueryForAdminByEmail();
                SqlCommand cmd = getCmdForAdminByEmail(strQuery, emailId);
                DataSet dsAdminDetail = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (dsAdminDetail == null) throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                if (dsAdminDetail.Tables[0].Rows.Count > 0)
                {
                    DataRow rowUserDtl = dsAdminDetail.Tables[0].Rows[0];
                    objCmpAdmin = getAdminFromDataRow(rowUserDtl);
                    adminExists = true;
                }
                else
                {
                    adminExists = false;
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "Internal server error.";
            }
            return objCmpAdmin;
        }
        public bool doesAdminExists(string emailId)
        {
            bool blnAdminExists = false;
            //status code is set in the function getAdminDetailByEmail.
            MFECompanyAdmin objCmpAdmin = getAdminDetailByEmail(emailId, out blnAdminExists);
            return blnAdminExists;
        }
        string getSqlQueryForAdminByEmail()
        {
            return @"SELECT *
                    FROM TBL_COMPANY_ADMINISTRATOR
                    WHERE EMAIL_ID = @EMAIL_ID;";
        }

        SqlCommand getCmdForAdminByEmail(string query, string emailId)
        {
            SqlCommand cmd = new SqlCommand(query);
            cmd.Parameters.AddWithValue("@EMAIL_ID", emailId);
            return cmd;
        }
        MFECompanyAdmin getAdminFromDataRow(DataRow rowAdminDtl)
        {
            if (rowAdminDtl == null) throw new ArgumentNullException();
            MFECompanyAdmin objAdmin = new MFECompanyAdmin();
            objAdmin.CompanyID = Convert.ToString(rowAdminDtl["COMPANY_ID"]);
            objAdmin.AdminId = Convert.ToString(rowAdminDtl["ADMIN_ID"]);
            objAdmin.EmailId= Convert.ToString(rowAdminDtl["EMAIL_ID"]);
            objAdmin.Password = Convert.ToString(rowAdminDtl["ACCESS_CODE"]);
            objAdmin.Firstname = Convert.ToString(rowAdminDtl["FIRST_NAME"]);
            objAdmin.LastName = Convert.ToString(rowAdminDtl["LAST_NAME"]);
            objAdmin.StreetAddress1 = Convert.ToString(rowAdminDtl["STREET_ADDRESS1"]);
            objAdmin.StreetAddress2 = Convert.ToString(rowAdminDtl["STREET_ADDRESS2"]);
            objAdmin.CityName = Convert.ToString(rowAdminDtl["CITY_NAME"]);
            objAdmin.StateName= Convert.ToString(rowAdminDtl["STATE_NAME"]);
            objAdmin.CountryCode = Convert.ToString(rowAdminDtl["COUNTRY_CODE"]);
            objAdmin.Zip = Convert.ToString(rowAdminDtl["ZIP"]);
            objAdmin.Dob= Convert.ToInt64(rowAdminDtl["DATE_OF_BIRTH"]);
            objAdmin.RegistrationDatetime = Convert.ToInt64(rowAdminDtl["REGISTRATION_DATETIME"]);
            objAdmin.Gender = Convert.ToString(rowAdminDtl["GENDER"]);
            objAdmin.MiddleName = Convert.ToString(rowAdminDtl["MIDDLE_NAME"]);
            objAdmin.StreetAddress3 = Convert.ToString(rowAdminDtl["STREET_ADDRESS3"]);
            objAdmin.MobileNo= Convert.ToString(rowAdminDtl["MOBILE"]);
            objAdmin.IsTrial = Convert.ToBoolean(rowAdminDtl["IS_TRIAL"]);
            return objAdmin;
        }
    }
}
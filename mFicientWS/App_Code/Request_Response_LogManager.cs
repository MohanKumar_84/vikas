﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class Request_Response_LogManager
    {
        public string _requestId, _request, _response, _requestdatetime, _responsedatetime, _statusDescription;
        public int _responsecode, _statusCode;
        
        #region Public Properties
        public string RequestId
        {
            get { return _requestId; }
            set { _requestId = value; }
        }
        public string Request
        {
            get { return _request; }
            set { _request = value; }
        }

        public string Response
        {
            get { return _response; }
            set { _response = value; }
        }
        public string Requestdatetime
        {
            get { return _requestdatetime; }
            set { _requestdatetime = value; }
        }
        public string Responsedatetime
        {
            get { return _responsedatetime; }
            set { _responsedatetime = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            set { _statusCode = value; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }
        public int ResponseCode
        {
            get{ return _responsecode; }
            set { _responsecode = value; }
        }
      
        #endregion

        public void  SaveRequest_ResponseLogmanager(string requestId,string  request,string requestdatetime)
        {
            this.RequestId=requestId;
            this.Request=request;
            this.Requestdatetime=requestdatetime;
        }
        public void SaveRequest_ResponseLogmanager(string requestId, string response, int responsecode, string responsedatetime)
        {
            this.RequestId = requestId;
            this.Response = response;
            this.ResponseCode = responsecode;
            this.Responsedatetime = responsedatetime;
        }

        public void Process()
        {
            try
            {
                  InsertRequestLogManager();
            }
            catch (Exception ex)
            {
                this.StatusCode = -1000;
                if (ex.Message == (((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR)).ToString())
                {
                    this.StatusDescription = ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
                }
                this.StatusDescription = "Internal server error.";
            }
        }

        public void Process1()
        {
            try
            {
                UpdateLogManager();
            }
            catch (Exception ex)
            {
                this.StatusCode = -1000;
                if (ex.Message == (((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR)).ToString())
                {
                    this.StatusDescription = ((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString();
                }
                this.StatusDescription = "Internal server error.";
            }
        }




        void InsretRescords(SqlConnection sqlConnection)
        {
            string strQuery = @"insert into TBL_WEBSERVICE_REQUEST_RESPONSE_LOG(REQUEST_ID,REQUEST,RESPONSE,RESPONSE_CODE,REQUEST_DATETIME,RESPONSE_DATETIME)
                               values(@REQUEST_ID,@REQUEST,@RESPONSE,@RESPONSE_CODE,@REQUEST_DATETIME,@RESPONSE_DATETIME);";

            SqlCommand cmd = new SqlCommand(strQuery, sqlConnection);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@REQUEST_ID", this.RequestId);
            cmd.Parameters.AddWithValue("@REQUEST", this.Request);
            cmd.Parameters.AddWithValue("@RESPONSE", "");
            cmd.Parameters.AddWithValue("@RESPONSE_CODE", "");
            cmd.Parameters.AddWithValue("@REQUEST_DATETIME", this.Requestdatetime);
            cmd.Parameters.AddWithValue("@RESPONSE_DATETIME", "");
            cmd.ExecuteNonQuery();
        }
        private void InsertRequestLogManager()
        {
            SqlConnection con;
            MSSqlDatabaseClient.SqlConnectionOpen(out con);
            try
            {
                using (con)
                {
                    InsretRescords(con);
                }
            }
            catch (SqlException e)
            {
                if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            catch (Exception e)
            {
              
                if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            finally
                {
                    if (con != null)
                    {
                        con.Dispose();
                    }
                   
                }
            }




        private void UpdateLogManager()
        {

            SqlConnection con;
            MSSqlDatabaseClient.SqlConnectionOpen(out con);
            try
            {
                using (con)
                {
                    UpdateRecords(con);
                }
            }
            catch (SqlException e)
            {
                if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            catch (Exception e)
            {

                if (e.Message == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString())
                {
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                }
                throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
            }
            finally
            {
                if (con != null)
                {
                    con.Dispose();
                }

            }
        }
        void UpdateRecords(SqlConnection sqlConnection)
        {

            string strQuery = @"update dbo.TBL_WEBSERVICE_REQUEST_RESPONSE_LOG set  RESPONSE=@RESPONSE, RESPONSE_CODE=@RESPONSE_CODE, RESPONSE_DATETIME=@RESPONSE_DATETIME where  REQUEST_ID=@REQUEST_ID";

            SqlCommand cmd = new SqlCommand(strQuery, sqlConnection);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@REQUEST_ID", this.RequestId);
            cmd.Parameters.AddWithValue("@RESPONSE", this.Response);
            cmd.Parameters.AddWithValue("@RESPONSE_CODE",this.ResponseCode);
            cmd.Parameters.AddWithValue("@RESPONSE_DATETIME", this.Requestdatetime);
            cmd.ExecuteNonQuery();
        }
    }
  

}
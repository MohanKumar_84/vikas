﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class Approve_Deny_DeleteResp
    {
        ResponseStatus _respStatus;
        string _requestId;

        public Approve_Deny_DeleteResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting contact status Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            Approve_Deny_DeleteRespData objblockunblockresponse = new Approve_Deny_DeleteRespData();
            objblockunblockresponse.rid = _requestId;
            objblockunblockresponse.func = Convert.ToString((int)FUNCTION_CODES.APPROVE_DENY_DELETE);
            objblockunblockresponse.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<Approve_Deny_DeleteRespData>(objblockunblockresponse);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
        public class Approve_Deny_DeleteRespData : CommonResponse
        {
            public Approve_Deny_DeleteRespData()
            { }
        }




    }
}
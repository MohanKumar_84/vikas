﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.IO;
using System.Net;
using System.Text;
namespace mFicientWS
{
    public class XmlRpcRequestXmlForBatchProcess
    {

        RpcParamaters _rpcPara;
        string _methodName, _url;

        DoTaskReqForBatchProcess _doTaskRequest;
        DoTaskCmdDetails _cmdDetails;

        public DoTaskCmdDetails CmdDetails
        {
            get { return _cmdDetails; }
        }
        public string MethodName
        {
            get { return _methodName; }
        }
        public RpcParamaters RpcParam
        {
            get { return _rpcPara; }
        }
        public DoTaskReqForBatchProcess DoTaskRequest
        {
            get { return _doTaskRequest; }
        }
        public string Url
        {
            get { return _url; }
        }
        public XmlRpcRequestXmlForBatchProcess(RpcParamaters xmlRpc, string methodName,
                                   DoTaskReqForBatchProcess doTaskReq, string url,
                                   DoTaskCmdDetails cmdDetails
                                  )
        {
            _rpcPara = xmlRpc;
            _methodName = methodName.Trim();
            _doTaskRequest = doTaskReq;
            _url = url;
            _cmdDetails = cmdDetails;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlRpcType"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Thrown when xmlRpcTypePassed is not defined</exception>
        string getParamTypeInString(string xmlRpcType)
        {
            try
            {
                XML_RPC_Parameter_Type xmlRpcParamType = (XML_RPC_Parameter_Type)Enum.Parse(typeof(XML_RPC_Parameter_Type), xmlRpcType);
                if (Enum.IsDefined(typeof(XML_RPC_Parameter_Type), xmlRpcParamType))
                {
                    switch (xmlRpcParamType)
                    {
                        case XML_RPC_Parameter_Type.BASE64:
                            return "base64";
                        case XML_RPC_Parameter_Type.BOOLEAN:
                            return "boolean";
                        case XML_RPC_Parameter_Type.DATE_TIME:
                            return "dateTime.iso8601";
                        case XML_RPC_Parameter_Type.DOUBLE:
                            return "double";
                        case XML_RPC_Parameter_Type.INTEGER:
                            return "int";
                        case XML_RPC_Parameter_Type.STRING:
                            return "string";
                    }
                }
            }
            catch
            {
                throw new ArgumentException("Parameter type not defined");
            }

            return "";

        }
        public XmlDocument getRequestXML(out Stream requestStream)
        {
            Stream stream = new MemoryStream(2000);
            XmlTextWriter xtw = new XmlTextWriter(stream, System.Text.Encoding.UTF8);
            xtw.WriteStartDocument();

            xtw.WriteStartElement("", "methodCall", "");

            xtw.WriteElementString("methodName", this.MethodName);

            xtw.WriteStartElement("", "params", "");

            foreach (param para in _rpcPara.param)
            {
                xtw.WriteStartElement("", "param", "");

                GetInnerparams(xtw, para, false);

                xtw.WriteEndElement();//param
            }

            //addSimpleTypeParams(xtw, this.XmlRpc);
            //addArrayTypeParams(xtw);
            //addStructTypeParams(xtw);

            xtw.WriteEndElement();//params
            xtw.WriteEndElement();//method call
            xtw.WriteEndDocument();//end document

            xtw.Flush();

            // Make sure you flush or you only get half the text
            xtw.Flush();

            requestStream = stream;
            StreamReader reader = null;
            try
            {
                reader = new StreamReader(stream, Encoding.UTF8, true);
                stream.Seek(0, SeekOrigin.Begin);
                string result = reader.ReadToEnd();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(result);
                return doc;
            }
            catch
            {
                if (reader != null)
                    reader.Close();
                if (requestStream != null)
                    requestStream.Close();
                return null;

            }
        }
        public XmlDocument getRequestXML()
        {
            Stream stream = new MemoryStream(2000);
            XmlTextWriter xtw = new XmlTextWriter(stream, System.Text.Encoding.UTF8);
            xtw.WriteStartDocument();

            xtw.WriteStartElement("", "methodCall", "");

            xtw.WriteElementString("methodName", this.MethodName);

            xtw.WriteStartElement("", "params", "");

            foreach (param para in _rpcPara.param)
            {
                xtw.WriteStartElement("", "param", "");

                GetInnerparams(xtw, para, false);

                xtw.WriteEndElement();//param
            }

            //addSimpleTypeParams(xtw, this.XmlRpc);
            //addArrayTypeParams(xtw);
            //addStructTypeParams(xtw);

            xtw.WriteEndElement();//params
            xtw.WriteEndElement();//method call
            xtw.WriteEndDocument();//end document

            xtw.Flush();

            // Make sure you flush or you only get half the text
            xtw.Flush();
            using (stream)
            {
                StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                stream.Seek(0, SeekOrigin.Begin);
                string result = reader.ReadToEnd();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(result);
                return doc;
            }
        }
        public string callXMLRpcWebService()
        {
            string strResponse;
            try
            {
                Stream stream;
                getRequestXML(out stream);
                using (stream)
                {
                    // Use a StreamReader to get the byte order correct
                    //StreamReader reader = new StreamReader(stream, Encoding.UTF8, true);
                    //stream.Seek(0, SeekOrigin.Begin);
                    //string result = reader.ReadToEnd();
                    strResponse = processCall(stream);
                }
                return strResponse;
            }
            catch (InvalidOperationException ex)
            {
                throw ex;
            }
            catch
            {
                throw;
            }
        }

        void GetInnerparams(XmlTextWriter xtw, param para, bool isStruct)
        {
            if (isStruct)
            {
                xtw.WriteElementString("name", para.name);
            }
            if (para != null)
            {
                if (Int64.Parse(para.typ) < 7)
                    addSimpleType(xtw, para);
                else if (Int64.Parse(para.typ) == 7)
                    addStructTypeParams(xtw, para);
                else if (Int64.Parse(para.typ) > 7)
                    addArrayTypeParams(xtw, para);
            }
        }
        void addArrayTypeParams(XmlTextWriter xmlTxtWriter, param para)
        {
            foreach (param objpara in para.inparam)
            {
                addArrayType(xmlTxtWriter, objpara);
            }
        }
        void addStructTypeParams(XmlTextWriter xmlTxtWriter, param para)
        {
            xmlTxtWriter.WriteStartElement("", "value", "");
            xmlTxtWriter.WriteStartElement("", "struct", "");
            foreach (param objpara in para.inparam)
            {
                addStructType(xmlTxtWriter, objpara);
            }
            xmlTxtWriter.WriteEndElement();
            xmlTxtWriter.WriteEndElement();
        }

        void addStructType(XmlTextWriter xmlTxtWriter, param para)
        {
            xmlTxtWriter.WriteStartElement("", "member", "");
            GetInnerparams(xmlTxtWriter, para, true);
            xmlTxtWriter.WriteEndElement();
            xmlTxtWriter.WriteEndElement();

        }

        void addSimpleType(XmlTextWriter xmlTxtWriter, param para)
        {
            xmlTxtWriter.WriteStartElement("", "value", "");
            string strValue = "";
            foreach (DoTaskCmdParameters cmdParams in _cmdDetails.cmdlp)
            {
                foreach (QueryParameters parameter in cmdParams.lp)
                {
                    if (parameter.para == para.name)
                    {
                        strValue = parameter.val;
                    }
                }
            }
            //xmlTxtWriter.WriteElementString(getParamTypeInString(para.typ), "", _doTaskRequest.Parameters.Single(name => name.para == para.name).val);
            xmlTxtWriter.WriteElementString(getParamTypeInString(para.typ), "", strValue);
            xmlTxtWriter.WriteEndElement();//value
        }

        void addArrayType(XmlTextWriter xmlTxtWriter, param rpcArrayType)
        {
            xmlTxtWriter.WriteStartElement("", "value", "");
            xmlTxtWriter.WriteStartElement("", "array", "");
            xmlTxtWriter.WriteStartElement("", "data", "");

            GetInnerparams(xmlTxtWriter, rpcArrayType, false);

            xmlTxtWriter.WriteEndElement();//data
            xmlTxtWriter.WriteEndElement();//array
            xmlTxtWriter.WriteEndElement();//value
        }

        string processCall(Stream reqStream)
        {
            byte[] requestData = GetStreamBytes(reqStream);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(this.Url);
            request.Method = "POST";
            request.ContentType = "text/xml";
            request.UserAgent = @"mFicient-WebServices";
            request.ContentLength = requestData.Length;

            using (Stream requestStream = request.GetRequestStream())
                requestStream.Write(requestData, 0, requestData.Length);

            string result = null;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                        result = reader.ReadToEnd();
                }
            }
            return result;
        }
        public static byte[] GetStreamBytes(Stream stream)
        {
            try
            {
                stream.Position = 0;
            }
            catch
            {
            }

            byte[] readBuffer = new byte[1024];
            List<byte> outputBytes = new List<byte>();

            int offset = 0;

            while (true)
            {
                int bytesRead = stream.Read(readBuffer, 0, readBuffer.Length);

                if (bytesRead == 0)
                {
                    break;
                }
                else if (bytesRead == readBuffer.Length)
                {
                    outputBytes.AddRange(readBuffer);
                }
                else
                {
                    byte[] tempBuf = new byte[bytesRead];

                    Array.Copy(readBuffer, tempBuf, bytesRead);

                    outputBytes.AddRange(tempBuf);

                    break;
                }

                offset += bytesRead;
            }

            return outputBytes.ToArray();
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class DownloadDBOnlineForOfflineTableReq
    {
        private string _sessionId, _requestId, _deviceId, _deviceType, _companyId, _username, _tableId, _userId, _model, _appId;
        private List<QueryParameters> _lstQueryParameter;
        private int _functionCode;
        private string strMPlugInUrl = string.Empty;
        DataConnCredential _dataCredential;
        public List<QueryParameters> QueryParameters
        {
            get { return _lstQueryParameter; }
        }

        public string DeviceType
        {
            get { return _deviceType; }
        }

        public string DeviceId
        {
            get { return _deviceId; }
        }

        public string RequestId
        {
            get { return _requestId; }
        }

        public string SessionId
        {
            get { return _sessionId; }
        }

        public string CompanyId
        {
            get { return _companyId; }
        }

        public string Username
        {
            get { return _username; }
        }

        public string TableId
        {
            get { return _tableId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        public string Model
        {
            get { return _model; }
        }
        public string AppID
        {
            get { return _appId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public DataConnCredential DataCredential
        {
            get { return _dataCredential; }
        }
        public DownloadDBOnlineForOfflineTableReq(string requestJson, string userId, string model)
        {
            RequestJsonParsing<DownloadOnlineDatabaseOfflineTableReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<DownloadOnlineDatabaseOfflineTableReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.DOWNLOAD_ONLINE_DATABASE_OFFLINE)
            {
                throw new Exception("Invalid Function Code");
            }
            _sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _username = objRequestJsonParsing.req.data.unm;
            _tableId = objRequestJsonParsing.req.data.tbl;
            _lstQueryParameter = objRequestJsonParsing.req.data.lp;
            _dataCredential = objRequestJsonParsing.req.data.cr;
            _userId = userId;
            _model = model;
            _appId = objRequestJsonParsing.req.data.appId;
            _appId = _appId == null ? "" : _appId;
        }

    }

    [DataContract]
    public class DownloadOnlineDatabaseOfflineTableReqData : Data
    {
        [DataMember]
        public string appId { get; set; }
        /// <summary>
        /// Table Id
        /// </summary>
        [DataMember]
        public string tbl { get; set; }
        /// <summary>
        /// Latest Parameter
        /// </summary>
        /// 
        [DataMember]
        public List<QueryParameters> lp { get; set; }

        /// <summary>
        /// Credential
        /// </summary>
        [DataMember]
        public DataConnCredential cr { get; set; }
    }

}
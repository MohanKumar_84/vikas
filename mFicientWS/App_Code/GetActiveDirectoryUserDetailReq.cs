﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class GetActiveDirectoryUserDetailReq
    {
        string _requestId, _companyId, _agentName, _agentPassword, _domainName, _username;

        public string AgentPassword
        {
            get { return _agentPassword; }
        }

        public string AgentName
        {
            get { return _agentName; }
        }

        public string CompanyId
        {
            get { return _companyId; }
        }

        public string MPlugInPassword
        {
            get { return _agentPassword; }
        }
        
        public string RequestId
        {
            get { return _requestId; }
        }
        
        public string DomainName
        {
            get { return _domainName; }
        }
        
        public string Username
        {
            get { return _username; }
        }

        public GetActiveDirectoryUserDetailReq(string requestJson)
        {
            GetActiveDirectoryUserDetailReqJson objRequestJsonParsing = Utilities.DeserialiseJson<GetActiveDirectoryUserDetailReqJson>(requestJson);
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.eid;
            _agentName = objRequestJsonParsing.req.agtnm;
            _agentPassword = objRequestJsonParsing.req.agtpwd;
            _domainName = objRequestJsonParsing.req.dmnm;
            _username = objRequestJsonParsing.req.unm;
        }
    }

    [DataContract]
    public class GetActiveDirectoryUserDetailReqJson
    {
        [DataMember]
        public GetActiveDirectoryUserDetailReqFields req { get; set; }
    }

    [DataContract]
    public class GetActiveDirectoryUserDetailReqFields
    {
        [DataMember]
        public string rid { get; set; }

        [DataMember]
        public string eid { get; set; }

        [DataMember]
        public string agtnm { get; set; }

        [DataMember]
        public string agtpwd { get; set; }

        [DataMember]
        public string dmnm { get; set; }

        [DataMember]
        public string unm { get; set; }
    }
}
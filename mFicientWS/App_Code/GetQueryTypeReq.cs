﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class GetQueryTypeReq
    {
        string _requestId, _companyId;
        int _functionCode;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }

        public GetQueryTypeReq(string requestJson)
        {
            RequestJsonParsing<GetQueryTypeReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<GetQueryTypeReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.GET_QUERY_TYPE)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.data.enid;
        }
    }
    [DataContract]
    public class GetQueryTypeReqData : Data
    {
    }
}
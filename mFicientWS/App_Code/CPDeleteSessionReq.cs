﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientWS
{
    public class CPDeleteSessionReq
    {
        string _requestId, _companyId, _userName;
        CPDeleteSessionReqData _data;
        List<DeviceDetail> _deviceToDelete;

        public List<DeviceDetail> DeviceToDelete
        {
            get { return _deviceToDelete; }
        }

        public CPDeleteSessionReqData Data
        {
            get { return _data; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }

        public string RequestId
        {
            get { return _requestId; }
        }
        public string UserName
        {
            get { return _userName; }
        }

        public CPDeleteSessionReq(string requestJson)
        {
            CPDeleteSessionReqJson objCPDeleteSessionReq = Utilities.DeserialiseJson<CPDeleteSessionReqJson>(requestJson);
            _requestId = objCPDeleteSessionReq.req.rid;
            _data = objCPDeleteSessionReq.req.data;
            _userName = objCPDeleteSessionReq.req.data.unm;
            _companyId = objCPDeleteSessionReq.req.data.enid;
            _deviceToDelete = objCPDeleteSessionReq.req.data.devc;
        }
    }

    public class CPDeleteSessionReqJson
    {
        [DataMember]
        public CPDeleteSessionReqJsonFields req { get; set; }
    }

    public class CPDeleteSessionReqJsonFields
    {
        /// <summary>
        /// request id
        /// </summary>
        [DataMember]
        public string rid { get; set; }

        /// <summary>
        /// data
        /// </summary>
        [DataMember]
        public CPDeleteSessionReqData data { get; set; }
    }
    public class CPDeleteSessionReqData
    {
        [DataMember]
        public string unm { get; set; }

        [DataMember]
        public string enid { get; set; }

        [DataMember]
        public List<DeviceDetail> devc { get; set; }
    }
    
    public class DeviceDetail
    {
        /// <summary>
        /// DeviceId
        /// </summary>
        [DataMember]
        public string did { get; set; }

        /// <summary>
        /// Device Type
        /// </summary>
        [DataMember]
        public string dtyp { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class MPGetOdataMetadata
    {
        MPGetOdataMetadataReq _mpGetOdataMetadata;

        public MPGetOdataMetadata(MPGetOdataMetadataReq mpGetOdataMetadata)
        {
            _mpGetOdataMetadata = mpGetOdataMetadata;
        }

        public MPGetOdataMetadataResp Process()
        {
            try
            {
                string strMPluginResponse = "";
                
                strMPluginResponse = mPlugin.GetODataCSDL(
                    _mpGetOdataMetadata.CompanyId,
                    _mpGetOdataMetadata.AgentName,
                    _mpGetOdataMetadata.AgentPassword,
                    _mpGetOdataMetadata.WsURL,
                    _mpGetOdataMetadata.HttpUserName,
                    _mpGetOdataMetadata.HttpUserPassword, _mpGetOdataMetadata.HttpAuthenticationType
                    );

                return new MPGetOdataMetadataResp(strMPluginResponse, "0", "");

            }
            catch (mPlugin.mPluginException e)
            {
                return new MPGetOdataMetadataResp("", "1", e.Message);
            }
            catch (HttpException e)
            {
                throw (new Exception(Convert.ToString(e.GetHttpCode())));
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
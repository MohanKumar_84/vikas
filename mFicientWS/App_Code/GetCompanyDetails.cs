﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net;
namespace mFicientWS
{
    public class GetCompanyDetails
    {

        GetCompanyDetailsReq _companyDtlsReq;
        public GetCompanyDetails(GetCompanyDetailsReq companyDtlsReq)
        {
            _companyDtlsReq = companyDtlsReq;
        }

        public GetCompanyDetailsResp Process()
        {
            DataSet dsCompanyDetails = getCompanyDtlsByCompanyId();
            ResponseStatus objResponseStatus = new ResponseStatus();
            CompanyDtlsResponseData objCompDtlsData = new CompanyDtlsResponseData();
            if (dsCompanyDetails != null && dsCompanyDetails.Tables.Count > 0)
            {
                if (dsCompanyDetails.Tables[0].Rows.Count != 0)
                {
                    objCompDtlsData.cty = (string)dsCompanyDetails.Tables[0].Rows[0]["CITY_NAME"];
                    objCompDtlsData.nm = (string)dsCompanyDetails.Tables[0].Rows[0]["COMPANY_NAME"];
                    objCompDtlsData.cnt = (string)dsCompanyDetails.Tables[0].Rows[0]["COUNTRY_CODE"];
                    objCompDtlsData.cnm = Convert.ToString(dsCompanyDetails.Tables[0].Rows[0]["COUNTRY_NAME"]);
                    objCompDtlsData.logo = Convert.ToString(dsCompanyDetails.Tables[0].Rows[0]["LOGO_IMAGE_NAME"]);
                    objCompDtlsData.st = (string)dsCompanyDetails.Tables[0].Rows[0]["STATE_NAME"];

                    string strAddress = "";
                    if (!String.IsNullOrEmpty((string)dsCompanyDetails.Tables[0].Rows[0]["STREET_ADDRESS1"]))
                    {
                        strAddress += (string)dsCompanyDetails.Tables[0].Rows[0]["STREET_ADDRESS1"];
                    }
                    if (!String.IsNullOrEmpty((string)dsCompanyDetails.Tables[0].Rows[0]["STREET_ADDRESS2"]))
                    {
                        strAddress += "," + (string)dsCompanyDetails.Tables[0].Rows[0]["STREET_ADDRESS2"];
                    }
                    if (!String.IsNullOrEmpty((string)dsCompanyDetails.Tables[0].Rows[0]["STREET_ADDRESS3"]))
                    {
                        strAddress += "," + (string)dsCompanyDetails.Tables[0].Rows[0]["STREET_ADDRESS3"];
                    }
                    objCompDtlsData.add = strAddress;
                    objCompDtlsData.spcon = (string)dsCompanyDetails.Tables[0].Rows[0]["SUPPORT_CONTACT"];
                    objCompDtlsData.seml = (string)dsCompanyDetails.Tables[0].Rows[0]["SUPPORT_EMAIL"];
                    objCompDtlsData.zip = (string)dsCompanyDetails.Tables[0].Rows[0]["ZIP"];
                    objResponseStatus.cd = "0";
                    objResponseStatus.desc = "";
                }
                else
                {
                    //record not found error
                    throw new Exception(((int)HttpStatusCode.Unauthorized).ToString());
                }
            }
            else
            {
                //record not found error
                throw new Exception(((int)HttpStatusCode.Unauthorized).ToString());
            }
            return new GetCompanyDetailsResp(objResponseStatus, _companyDtlsReq.RequestId, objCompDtlsData);
        }

        DataSet getCompanyDtlsByCompanyId()
        {
            string strQuery = @"SELECT CompDtl.*,Cntry.COUNTRY_NAME
                                FROM TBL_COMPANY_DETAIL AS CompDtl
                                INNER JOIN ADMIN_TBL_MST_COUNTRY AS Cntry
                                ON CompDtl.COUNTRY_CODE = Cntry.COUNTRY_CODE
                                WHERE COMPANY_ID = @CompanyId;";
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@CompanyId", _companyDtlsReq.CompanyId);
            return MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
        }
    }
}
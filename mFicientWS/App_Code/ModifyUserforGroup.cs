﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientWS
{
    public class ModifyUserforGroup
    {
        ModifyUserfromGroupReq _objModifyUserfromGroupReq;

        public ModifyUserforGroup(ModifyUserfromGroupReq objModifyUserfromGroupReq)
        {
            _objModifyUserfromGroupReq = objModifyUserfromGroupReq;
        }

        public ModifyUserforGroupResp Process()
        {

            ResponseStatus objRespStatus = new ResponseStatus();
            try
            {
                    string strquery="";
                    if (_objModifyUserfromGroupReq.Type == 0) strquery = @"Delete from dbo.TBL_USER_GROUP_LINK where  USER_ID=@USERID and GROUP_ID=@GROUPID;";
                    else strquery = @"insert into  dbo.TBL_USER_GROUP_LINK(COMPANY_ID,GROUP_ID,USER_ID,CREATED_ON) VALUES(@COMPANY_ID,@GROUPID,@USERID,@CREATED_ON);";
                    SqlCommand cmdcommanddetails = new SqlCommand(strquery);
                    cmdcommanddetails.Parameters.AddWithValue("@USERID", _objModifyUserfromGroupReq.UserIds);
                    cmdcommanddetails.Parameters.AddWithValue("@GROUPID", _objModifyUserfromGroupReq.GroupId);
                    cmdcommanddetails.Parameters.AddWithValue("@COMPANY_ID", _objModifyUserfromGroupReq.CompanyId);
                    cmdcommanddetails.Parameters.AddWithValue("@CREATED_ON", DateTime.UtcNow.Ticks);
                    cmdcommanddetails.CommandType = CommandType.Text;
                    MSSqlDatabaseClient.ExecuteNonQueryRecord(cmdcommanddetails);
                     objRespStatus.cd = "0";
                     objRespStatus.desc = "";
                      }
                     catch (Exception ex)
                     {
                         objRespStatus.cd = "";
                         objRespStatus.desc = "Error";
                         if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                             throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                     }
            return new ModifyUserforGroupResp(objRespStatus, _objModifyUserfromGroupReq.RequestId);

        }
    }
}
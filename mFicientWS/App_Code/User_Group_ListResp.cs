﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class User_Group_ListResp
    {
        ResponseStatus _respStatus;

        string _requestId;
        User_Group_ListMetaData _getmetadata;
        public User_Group_ListResp(ResponseStatus respStatus, User_Group_ListMetaData getlist, string requestId)
        {
            _respStatus = respStatus;
            _getmetadata = getlist;
            _requestId = requestId;
        }
        public string GetResponseJson()
        {
            User_Group_ListRespJson objgetRegistrationResp = new User_Group_ListRespJson();
            objgetRegistrationResp.func = Convert.ToString((int)FUNCTION_CODES.USER_GROUP_LIST);
            objgetRegistrationResp.rid = _requestId;
            objgetRegistrationResp.status = _respStatus;
            objgetRegistrationResp.data = _getmetadata;
            string strJsonWithDetails = Utilities.SerializeJson<User_Group_ListRespJson>(objgetRegistrationResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
        [DataContract]
        public class User_Group_ListRespJson : CommonResponse
        {
            public User_Group_ListRespJson()
            { }
            [DataMember]
            public User_Group_ListMetaData data { get; set; }

        }
    }
     [DataContract]
        public class Group
        {
            [DataMember]
            public string nm { get; set; }
             [DataMember]
            public string id { get; set; }
        }
        

        [DataContract]
        public class User_Group_ListMetaData
        {
            [DataMember]
            public List<Group> grp { get; set; }
        }


}
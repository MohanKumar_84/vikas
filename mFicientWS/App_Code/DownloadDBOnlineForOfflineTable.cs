﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using System.IO;
using System.Text;
using System.Xml;
using System.Net;
using System.Collections;
using Newtonsoft.Json.Linq;

namespace mFicientWS
{

    public class DownloadOnlineDatabaseOfflineTable
    {
        DownloadDBOnlineForOfflineTableReq _objDownloadOffline;
        private List<QueryParameters> _lstOutputjson;
        private List<Seleced_cmd_Tables> _lstSelectedTableValues;
        //private ColoumData data;
        private string strMPlugInUrl = string.Empty;
        public DownloadOnlineDatabaseOfflineTable(DownloadDBOnlineForOfflineTableReq downloadOnlineDatabaseOfflineTableReq)
        {
            _objDownloadOffline = downloadOnlineDatabaseOfflineTableReq;
        }

        public DownloadDBOnlineForOfflineTableResp Process()
        {
            ResponseStatus _respStatus = new ResponseStatus();
            _lstOutputjson = new List<QueryParameters>();
            _respStatus.cd = "0";
            _respStatus.desc = "";
            object data = new object();
            string strCmdType = string.Empty, strCmdId = string.Empty, strOutputJson = string.Empty;
            Hashtable cols = new Hashtable();
            try
            {
                //strMPlugInUrl = Utilities.getMPlugInServerURL(_objDownloadOnlineDatabaseOffline.CompanyId);

                string strQuery = "SELECT * FROM TBL_OFFLINE_DATA_TABLE WHERE TABLE_ID=@TABLE_ID";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@TABLE_ID", _objDownloadOffline.TableId);
                DataSet dsTable = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);

                if (dsTable != null && dsTable.Tables[0] != null)
                {
                    if (dsTable.Tables[0].Rows.Count > 0)
                    {
                        JObject joSync = JObject.Parse(JObject.Parse(Convert.ToString(dsTable.Tables[0].Rows[0]["SYNC_JSON"]))["sync"].ToString());
                        strCmdType = joSync["cmdType"].ToString();
                        strCmdId = JObject.Parse(joSync["download"].ToString())["command"].ToString();
                        strOutputJson = JObject.Parse(joSync["download"].ToString())["outputParam"].ToString();

                        _lstOutputjson = Utilities.DeserialiseJson<List<QueryParameters>>(strOutputJson);
                        DoTask dotsk = new DoTask(new DoTaskReq(_objDownloadOffline.RequestId,_objDownloadOffline.CompanyId, strCmdId, strCmdType,_objDownloadOffline.QueryParameters,
                            _objDownloadOffline.DataCredential, _objDownloadOffline.UserId, _objDownloadOffline.Model, _objDownloadOffline.AppID, _objDownloadOffline.Username, _objDownloadOffline.DeviceId));
                        DoTaskResp resp = dotsk.Process();

                        _respStatus.cd = resp._respStatus.cd;
                        _respStatus.desc = resp._respStatus.desc;
                        data = resp._lstData;

                        JArray jaCols = JArray.Parse(JObject.Parse(JObject.Parse(Convert.ToString(dsTable.Tables[0].Rows[0]["TABLE_JSON"]))["tbl"].ToString())["col"].ToString());
                        foreach (JObject joCol in jaCols)
                        {
                            cols.Add(joCol["cnm"].ToString().ToLower(), joCol);
                        }
                    }
                    else
                    {
                        _respStatus.cd = "016005";
                        _respStatus.desc = "Record not Found";
                    }
                }
                else
                {
                    _respStatus.cd = "016005";
                    _respStatus.desc = "Record not Found";
                }

            }
            catch (Exception ex)
            {
                _respStatus.cd = "016005";
                _respStatus.desc = "Record not Found";
                throw ex;
            }

            return new DownloadDBOnlineForOfflineTableResp(_respStatus, data, _objDownloadOffline.RequestId, _lstOutputjson, cols);
        }
    }

    [DataContract]
    public class SelectedCmdTableValues
    {
        [DataMember]
        public List<Seleced_cmd_Tables> dt { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.Data;
using MySql.Data.MySqlClient;
using Npgsql;
using System.Xml;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;

namespace mFicientWS
{

    public class UploadOfflineDatabaseOnlineDatabase
    {
        UploadtoOnlineDatabaseReq _objuploadofflineReq;
        //private string strMPlugInUrl = string.Empty;
        ResponseStatus _respStatus = new ResponseStatus();
        public UploadOfflineDatabaseOnlineDatabase(UploadtoOnlineDatabaseReq uploadtoOnlineDatabaseReq)
        {
            _objuploadofflineReq = uploadtoOnlineDatabaseReq;
        }

        public CommonResponse Process()
        {

            _respStatus.cd = "0";
            _respStatus.desc = "";

            CommonResponse resp = new CommonResponse();
            string strCmdType, strNewCmdId = string.Empty, strUpdateCmdId = string.Empty;
            try
            {
                //strMPlugInUrl = Utilities.getMPlugInServerURL(_objuploadtoOnlineDatabaseReq.CompanyId);

                string strQuery = "SELECT * FROM TBL_OFFLINE_DATA_TABLE WHERE TABLE_ID=@TABLE_ID";
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@TABLE_ID", _objuploadofflineReq.TableId);
                DataSet dsTable = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
                if (dsTable != null && dsTable.Tables[0] != null)
                {
                    if (dsTable.Tables[0].Rows.Count > 0)
                    {
                        List<QueryParameters> CommandParametersListNew = new List<QueryParameters>(), CommandParametersListUpdate = new List<QueryParameters>();
                        JObject joSync = JObject.Parse(JObject.Parse(Convert.ToString(dsTable.Tables[0].Rows[0]["SYNC_JSON"]))["sync"].ToString());
                        strCmdType = joSync["cmdType"].ToString();
                        
                        var varUpload=joSync["upload"];
                        if (varUpload != null)
                        {
                            JObject joUpload=JObject.Parse(varUpload.ToString());
                            // new---------------------------------------------------------------------
                            JObject joUploadNew = JObject.Parse(joUpload["newRecord"].ToString());
                            strNewCmdId = joUploadNew["command"].ToString();
                            CommandParametersListNew = Utilities.DeserialiseJson<List<QueryParameters>>(joUploadNew["params"].ToString());
                            //-------------------------------------------------------------------------------
                            var varUploadmodified= joUpload["modifiedRecord"];
                            if (varUploadmodified != null && varUploadmodified.ToString().Length > 0)
                            {
                                JObject joUploadmodified = JObject.Parse(varUploadmodified.ToString());
                                strUpdateCmdId = joUploadmodified["command"].ToString();
                                CommandParametersListUpdate = Utilities.DeserialiseJson<List<QueryParameters>>(joUploadmodified["params"].ToString());
                            }
                            processCommand(strCmdType, strNewCmdId, strUpdateCmdId, CommandParametersListNew, CommandParametersListUpdate);
                        }
                    }
                    else
                    {
                        _respStatus.cd = "015005";
                        _respStatus.desc = "Record not Found";
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                    }
                }
                else
                {
                    _respStatus.cd = "015005";
                    _respStatus.desc = "Record not Found";
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch (Exception ex)
            {
                _respStatus.cd = "015005";
                _respStatus.desc = "Record not Found";
                throw ex;
            }
            resp.func = _objuploadofflineReq.functioncode.ToString();
            resp.rid = _objuploadofflineReq.RequestId;
            resp.status = _respStatus;
            return resp;
        }
        private void processCommand(string cmd_Type, string _cmdIdNew, string _cmdIdUpdate, List<QueryParameters> CommandParametersListNew, List<QueryParameters> CommandParametersListUpdate)
        {
            foreach (List<string> rowdata in _objuploadofflineReq.valdetails)
            {
                DataConnCredential dataCredential = new DataConnCredential();
                dataCredential.pwd = _objuploadofflineReq.DataCredential.pwd;
                dataCredential.unm = _objuploadofflineReq.DataCredential.unm;
                DoTask dotsk = new DoTask(new DoTaskReq(_objuploadofflineReq.RequestId, _objuploadofflineReq.CompanyId,(rowdata[0] == "1" ? _cmdIdNew : _cmdIdUpdate),
                               cmd_Type, GetLP(CommandParametersListNew, rowdata), dataCredential, _objuploadofflineReq.UserId, _objuploadofflineReq.Model,
                               _objuploadofflineReq.AppID, _objuploadofflineReq.Username, _objuploadofflineReq.DeviceId));
                DoTaskResp resp = dotsk.Process();
                _respStatus.cd = resp._respStatus.cd;
                _respStatus.desc = resp._respStatus.desc;
            }           
        }
        //private void processCommand(string cmd_Type, string _cmdIdNew, string _cmdIdUpdate, List<QueryParameters> CommandParametersListNew, List<QueryParameters> CommandParametersListUpdate)
        //{
        //    List<DoTaskCmdParameters> newRcd = new List<DoTaskCmdParameters>();
        //    List<DoTaskCmdParameters> updateRcd = new List<DoTaskCmdParameters>();

        //    foreach (List<string> rowdata in _objuploadtoOnlineDatabaseReq.valdetails)
        //    {
        //        if (rowdata[0] == "0")  newRcd.Add(new DoTaskCmdParameters(GetLP(CommandParametersListNew, rowdata)));
        //        else updateRcd.Add(new DoTaskCmdParameters(GetLP(CommandParametersListUpdate, rowdata)));
        //    }

        //    List<DoTaskCmdDetails> _commands = new List<DoTaskCmdDetails>();
        //    if (newRcd.Count > 0)
        //        _commands.Add(new DoTaskCmdDetails(0, _cmdIdNew, Convert.ToInt32(cmd_Type), newRcd, _objuploadtoOnlineDatabaseReq.DataCredential));
        //    if (updateRcd.Count > 0)
        //        _commands.Add(new DoTaskCmdDetails(1, _cmdIdUpdate, Convert.ToInt32(cmd_Type), updateRcd, _objuploadtoOnlineDatabaseReq.DataCredential));

        //    DoTaskWebServiceForBatchProcess dotsk = new DoTaskWebServiceForBatchProcess(new DoTaskReqForBatchProcess(_objuploadtoOnlineDatabaseReq.RequestId, _objuploadtoOnlineDatabaseReq.CompanyId, "", Convert.ToInt32(cmd_Type), _commands, _objuploadtoOnlineDatabaseReq.DataCredential, _objuploadtoOnlineDatabaseReq.Username));
        //    DoTaskRespForBatchProcess resp = dotsk.Process(true);

        //    _respStatus.cd = resp._respStatus.cd;
        //    _respStatus.desc = resp._respStatus.desc;
        //}
        List<QueryParameters> GetLP(List<QueryParameters> commandLP, List<string> rowdata)
        {
            List<QueryParameters> lp = new List<QueryParameters>();
            foreach (QueryParameters newpara in commandLP)
            {
                for (int i = 0; i < _objuploadofflineReq.colname.Count; i++)
                {
                    if (newpara.val.Trim().Length>2 &&  newpara.val.Substring(2) == _objuploadofflineReq.colname[i])
                    {
                        lp.Add(new QueryParameters(newpara.para, rowdata[i + 1])); break;
                    }
                }
            }
            return lp;
        }
    }

}
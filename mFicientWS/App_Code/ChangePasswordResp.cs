﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class ChangePasswordResp
    {
        ResponseStatus _respStatus;
        string _requestId;
        public ChangePasswordResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Change Password Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            ChangePasswordResponse objChangePasswordResp = new ChangePasswordResponse();
            objChangePasswordResp.func = Convert.ToString((int)FUNCTION_CODES.CHANGE_PASSWORD);
            objChangePasswordResp.rid = _requestId;
            objChangePasswordResp.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<ChangePasswordResponse>(objChangePasswordResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class ChangePasswordResponse : CommonResponse
    {
        public ChangePasswordResponse()
        { }
    }
}
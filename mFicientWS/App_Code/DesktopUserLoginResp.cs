﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class DesktopUserLoginResp
    {
        ResponseStatus _respStatus;
        string _requestId, _sessionId, _userFullName, _bucketUsername, _accessKey, _secretKey;
        public DesktopUserLoginResp(ResponseStatus respStatus, string requestId, string sessionId, string userFullName, string bucketUsername, string accessKey, string secretKey)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
                _sessionId = sessionId;
                _userFullName = userFullName;
                _bucketUsername = bucketUsername;
                _accessKey = accessKey;
                _secretKey = secretKey;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating User Login Detail Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            DesktopUserLoginDtlsResponse objUserLoginDtlsResp = new DesktopUserLoginDtlsResponse();
            objUserLoginDtlsResp.func = Convert.ToString((int)FUNCTION_CODES.DESKTOP_USER_LOGIN);
            objUserLoginDtlsResp.rid = _requestId;
            objUserLoginDtlsResp.status = _respStatus;
            DesktopUserLoginDtlResponseData objResponseData = new DesktopUserLoginDtlResponseData();
            objResponseData.sid = _sessionId;
            objUserLoginDtlsResp.data = objResponseData;
            objUserLoginDtlsResp.data.fnm = _userFullName;
            //S3Bucket s3Bucket = new S3Bucket();
            //s3Bucket.aid = _bucketUsername;
            //s3Bucket.ak = _accessKey;
            //s3Bucket.sak = _secretKey;
            //objResponseData.s3 = s3Bucket;
            string strJsonWithDetails = Utilities.SerializeJson<DesktopUserLoginDtlsResponse>(objUserLoginDtlsResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class DesktopUserLoginDtlsResponse : CommonResponse
    {
        public DesktopUserLoginDtlsResponse()
        {

        }
        [DataMember]
        public DesktopUserLoginDtlResponseData data { get; set; }
    }
    public class DesktopUserLoginDtlResponseData
    {
        public DesktopUserLoginDtlResponseData()
        {
            //s3 = new S3Bucket();
        }

        /// <summary>
        /// session id
        /// </summary>
        [DataMember]
        public string sid { get; set; }

        /// <summary>
        /// FullName
        /// </summary>
        [DataMember]
        public string fnm { get; set; }

        /// <summary>
        /// S3Bucket
        /// </summary>
        //[DataMember]
        //public S3Bucket s3 { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
namespace mFicientWS
{
    public enum EMAIL_FOR
    {
        COMPANY_REGISTRATION,
        CP_PASSWORD_CHANGED,
        CP_RESET_PASSWORD,
        CP_PLAN_ORDER_REQUEST,
        CP_USER_CREATED,
        CP_QUERY_PROCESS,
        USER_DEVICE_REGISTRATION,
        USER_DEVICE_APPROVAL,
        USER_PASSWORD_CHANGED,
        USER_RESET_PASSWORD,
        USER_QUERY_PROCESS,
        MBUZZ_CONTACT_REQ_PROCESS,
        ADMIN_CHANGE_PASSWORD,
        ADMIN_RESET_PASSWORD,
        ADMIN_QUERY_PROCESS,
    }
    public class SaveEmailInfo
    {
        #region member variables

        //static bool _useConnection;
        //static SqlConnection _con;
        #endregion
        #region Public Properties

        //public static bool UseConnection
        //{
        //    get { return _useConnection; }
        //}
        //public static SqlConnection Con
        //{
        //    get { return _con; }
        //}
        //public static SqlTransaction Transaction
        //{
        //    get { return _transaction; }
        //}
        #endregion

        public enum RESET_PWD_USER_SEARCH_TYPE
        {
            UserName,
            UserId
        }
        public enum MGRAM_EMAIL_TYPE
        {
            NoReply = 0,
        }
        static string getEmailTemplateName(EMAIL_FOR emailFor)
        {
            string strEmailTemplateName = "";
            switch (emailFor)
            {
                case EMAIL_FOR.USER_DEVICE_REGISTRATION:
                    strEmailTemplateName = "DeviceRegistration";
                    break;
                case EMAIL_FOR.USER_PASSWORD_CHANGED:
                    strEmailTemplateName = "ChangePassword";
                    break;
                case EMAIL_FOR.USER_RESET_PASSWORD:
                    strEmailTemplateName = "ResetPassword";
                    break;
                case EMAIL_FOR.USER_QUERY_PROCESS:
                    strEmailTemplateName = "SupportQuery";
                    break;
                case EMAIL_FOR.COMPANY_REGISTRATION:
                    strEmailTemplateName = "CompanyRegistration";
                    break;
            }
            return strEmailTemplateName;
        }
        static string getEmailSubject(EMAIL_FOR emailFor)
        {
            string strEmailSubject = "";
            switch (emailFor)
            {

                case EMAIL_FOR.USER_DEVICE_REGISTRATION:
                    strEmailSubject = "Device registration";
                    break;
                case EMAIL_FOR.USER_PASSWORD_CHANGED:
                    strEmailSubject = "Password changed";
                    break;
                case EMAIL_FOR.USER_RESET_PASSWORD:
                    strEmailSubject = "Password reset";
                    break;
                case EMAIL_FOR.USER_QUERY_PROCESS:
                    strEmailSubject = "User query";
                    break;
                case EMAIL_FOR.COMPANY_REGISTRATION:
                    strEmailSubject = "Company Registration";
                    break;

            }
            return strEmailSubject;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailIdOfUser"></param>
        /// <param name="emailSubject"></param>
        /// <param name="parametersToReplace"></param>
        /// <returns>True if successfull</returns>
        static bool saveEmailInfo(string emailIdOfUser,
            string emailSubject,
            Hashtable parametersToReplace, EMAIL_FOR emailFor,
            HttpContext appContext)
        {
            string strEmailBody = "";
            string strEmailDBUniqueID = Utilities.GetMd5Hash(emailIdOfUser + DateTime.UtcNow.Ticks.ToString());
            string strEmailTemplateKey = getEmailTemplateKey(emailFor);
            string strfileContent = getFileContentFromCache(strEmailTemplateKey);
            if (String.IsNullOrEmpty(strfileContent))
            {
                strfileContent = getFileContentFromTextFile(emailFor, appContext);
                if (!String.IsNullOrEmpty(strfileContent))
                    saveTemplateInCache(strEmailTemplateKey, strfileContent);
            }
            if (!String.IsNullOrEmpty(strfileContent))
            {
                strEmailBody = getEmailBodyAfterReplacingParams(parametersToReplace, strfileContent);
                int iRowsAffected = insertInMgramTable(strEmailDBUniqueID,
                    emailIdOfUser, emailSubject, strEmailBody);
                if (iRowsAffected > 0)
                {
                    return true;
                }
                return false;
            }
            else
            {
                return false;
            }
        }
        static void saveTemplateInCache(string key, string value)
        {
            CacheManager.Insert<string>(key, value, DateTime.UtcNow, TimeSpan.FromSeconds(MficientConstants.SESSION_VALIDITY_SECONDS));
        }
        static string getEmailTemplateKey(EMAIL_FOR emailFor)
        {
            return CacheManager.GetKey(
                 CacheManager.CacheType.mFicientEmail,
                 String.Empty, String.Empty,
                 String.Empty, String.Empty,
                 String.Empty, ((int)emailFor).ToString(),
                 String.Empty
                 );
        }
        static string getFileContentFromCache(string key)
        {
            return CacheManager.Get<string>(key,false);
        }
        static string getFileContentFromTextFile(EMAIL_FOR emailFor, HttpContext appContext)
        {
            string strFileContent = String.Empty;
            try
            {
                string strPath = appContext.Server.MapPath("EmailTemplates\\") + getEmailTemplateName(emailFor) + ".txt";
                FileInfo fileEmailTemplate = new FileInfo(strPath);
                if (fileEmailTemplate != null)
                {
                    using (StreamReader sreader = fileEmailTemplate.OpenText())
                    {
                        // Use the StreamReader object...
                        strFileContent = sreader.ReadToEnd();
                    }
                }
            }
            catch
            { }
            return strFileContent;
        }
        static string getEmailBodyAfterReplacingParams(Hashtable parametersToReplace,
            string fileContent)
        {
            string strEmailBody = String.Empty;
            Hashtable hashKeyPositions = new Hashtable();
            foreach (DictionaryEntry hashEntry in parametersToReplace)
            {
                int iIndexOFParam = getIndexOfParamToReplace(fileContent, Convert.ToString(hashEntry.Key));
                EmailTemplateParameterNameValue objParameterNameValue =
                    getValueForParam(parametersToReplace,
                        Convert.ToString(hashEntry.Key),
                        Convert.ToString(hashEntry.Value)
                     );
                fileContent = fileContent.Insert(iIndexOFParam, Convert.ToString(objParameterNameValue.ParameterValue));
                iIndexOFParam = getIndexOfParamToReplace(fileContent, Convert.ToString(hashEntry.Key));
                fileContent = fileContent.Remove(iIndexOFParam, objParameterNameValue.ParameterName.Length);
            }
            //if for some reason some of the parameters are not replaced then find that pattern and replace it with empty string.
            //string strPattern = @"\b<%[A-Za-z0-9 ]%>\b";
            string strPattern = @"<%[A-Za-z0-9 ]*%>";
            bool isLoopCompleted = false;
            do
            {
                foreach (DictionaryEntry hashEntry in hashKeyPositions)
                {
                    MatchCollection matches = System.Text.RegularExpressions.Regex.Matches(fileContent, strPattern);
                    EmailTemplateParameterNameValue parameterNameValue = (EmailTemplateParameterNameValue)hashEntry.Value;
                    foreach (Match match in matches)
                    {
                        if (parameterNameValue.ParameterName != match.Value)
                        {
                            fileContent = fileContent.Remove(match.Index, match.Value.Length);//because when thee characters are deleted then others position changes.
                            break;
                        }
                    }
                }
                int iCountOfMatchesAfterReplacing = 0;
                foreach (DictionaryEntry hashEntry in hashKeyPositions)
                {
                    MatchCollection matches = System.Text.RegularExpressions.Regex.Matches(fileContent, strPattern);
                    iCountOfMatchesAfterReplacing = iCountOfMatchesAfterReplacing + matches.Count;
                }
                if (iCountOfMatchesAfterReplacing > 0)
                {
                    isLoopCompleted = false;
                }
                else
                {
                    isLoopCompleted = true;
                }
            } while (isLoopCompleted == false);

            strEmailBody = fileContent;
            return strEmailBody;
        }
        static int getIndexOfParamToReplace(string fileContent, string paramName)
        {
            return fileContent.IndexOf("<%" + paramName + "%>");
        }
        static EmailTemplateParameterNameValue getValueForParam(Hashtable paramsWithValue, string paramName, string value)
        {
            EmailTemplateParameterNameValue objParameterNameValue = new EmailTemplateParameterNameValue();
            objParameterNameValue.ParameterName = "<%" + paramName + "%>";
            objParameterNameValue.ParameterValue = Convert.ToString(paramsWithValue[paramName]);
            return objParameterNameValue;
        }
        static int insertInMgramTable(string uniqueId,
            string emailId,
            string emailSubject,
            string emailBody)
        {
            SqlConnection con = new SqlConnection();
            try
            {
                MSSqlDatabaseClient.SqlConnectionOpen(
                                out con,
                                MSSqlDatabaseClient.getConnectionStringFromWebConfig(MSSqlDatabaseClient.CONNECTION_STRING_FOR_DB.MGRAM));
           
                SqlCommand cmd = getSqlCommand(con, uniqueId, emailId, emailSubject, emailBody);

                return cmd.ExecuteNonQuery();
            }
            catch {
            }
            finally
            {
                if (con != null)
                {
                    MSSqlDatabaseClient.SqlConnectionClose(con);
                }
            }
            return 0;
        }

        static string getSqlQuery()
        {
            return @"INSERT INTO ADMIN_TBL_EMAIL_OUTBOX
                   ([EMAIL_ID],[EMAIL]
                   ,[SUBJECT],[BODY]
                   ,[POST_TIME],[SEND_TIME]
                   ,[STATUS],[EMAIL_TYPE]
                   ,[NO_OF_ATTEMPTS],[NEXT_ATTEMPT_TIME])
             VALUES
                   (@EMAIL_ID,@EMAIL
                   ,@SUBJECT,@BODY
                   ,@POST_TIME,@SEND_TIME
                   ,@STATUS,@EMAIL_TYPE
                   ,@NO_OF_ATTEMPTS ,@NEXT_ATTEMPT_TIME)";
        }
        static SqlCommand getSqlCommand(SqlConnection con, string uniqueID,
            string emailID, string subject, string body)
        {
            SqlCommand cmd = new SqlCommand(getSqlQuery(), con);
            cmd.Parameters.AddWithValue("@EMAIL_ID", uniqueID);
            cmd.Parameters.AddWithValue("@EMAIL", emailID);
            cmd.Parameters.AddWithValue("@SUBJECT", subject);
            cmd.Parameters.AddWithValue("@BODY", body);
            cmd.Parameters.AddWithValue("@POST_TIME", DateTime.UtcNow.Ticks);
            cmd.Parameters.AddWithValue("@SEND_TIME", 0);
            cmd.Parameters.AddWithValue("@STATUS", 0);
            cmd.Parameters.AddWithValue("@EMAIL_TYPE", ((int)MGRAM_EMAIL_TYPE.NoReply));//No Reply
            cmd.Parameters.AddWithValue("@NO_OF_ATTEMPTS", 0);
            cmd.Parameters.AddWithValue("@NEXT_ATTEMPT_TIME", 0);
            return cmd;
        }


        public static bool deviceRegistration(string subAdminEmailId, string saUsername, string companyId, string userId, HttpContext appContext)
        {
            Hashtable hashParamToReplace = new Hashtable();
            string strLinkValue = MficientConstants.ENTERPRISE_URL + @"/default.aspx?d1=" +
                Utilities.EncryptString(userId + "@" +
                companyId + "@" + saUsername) +
                "&d2=registerDevice";

            hashParamToReplace.Add("Username", saUsername);
            hashParamToReplace.Add("Link", strLinkValue);
            return saveEmailInfo(
                subAdminEmailId,
                getEmailSubject(EMAIL_FOR.USER_DEVICE_REGISTRATION),
                hashParamToReplace,
                EMAIL_FOR.USER_DEVICE_REGISTRATION, appContext);
        }
        public static void companyRegistration(string emailId, string fullName, string username, string companyID, HttpContext appContext)
        {
            Hashtable hashParamToReplace = new Hashtable();
            hashParamToReplace.Add("FullName", fullName);
            hashParamToReplace.Add("Username", username);
            hashParamToReplace.Add("CompanyID", companyID);
            saveEmailInfo(emailId, getEmailSubject(EMAIL_FOR.COMPANY_REGISTRATION), hashParamToReplace, EMAIL_FOR.COMPANY_REGISTRATION, appContext);
        }
        public static bool changePassword(
                    string emailId,
                    string username, string password,
                    long utcDateInTicks, TimeZoneInfo tzInfo,
                    HttpContext context)
        {
            Hashtable hashParamToReplace = new Hashtable();
            hashParamToReplace.Add("username", username);

            hashParamToReplace.Add("ChangeDate",
                Utilities.getFullCompanyLocalFormattedDate(tzInfo, utcDateInTicks));

            //hashParamToReplace.Add("Password", password);

            return saveEmailInfo(
                 emailId,
                 getEmailSubject(EMAIL_FOR.USER_PASSWORD_CHANGED),
                 hashParamToReplace,
                 EMAIL_FOR.USER_PASSWORD_CHANGED, context
                 );
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="emailId"></param>
        /// <param name="companyId"></param>
        /// <param name="userType"></param>
        /// <param name="resetCode"></param>
        /// <param name="context"></param>
        /// <returns>True if process was successful</returns>
        public static bool resetPassword(
            string userName, string emailId,
            string companyId, string userType,
            string resetCode, HttpContext context)
        {
            Hashtable hashParamToReplace = new Hashtable();
            hashParamToReplace.Add("Username", userName);
            hashParamToReplace.Add("ResetCode", resetCode);
            hashParamToReplace.Add("Link",
                MficientConstants.ENTERPRISE_URL +
                "/resetPassword.aspx?d=" +
                Utilities.EncryptString(userName + "~" + companyId + "~" + userType));

            return saveEmailInfo(
                 emailId,
                 getEmailSubject(EMAIL_FOR.USER_RESET_PASSWORD),
                 hashParamToReplace,
                 EMAIL_FOR.USER_RESET_PASSWORD, context
                 );
        }

        public static bool supportQuery(
            string subAdminUsername, string emailId,
            string companyId, string usernameOfUser,
            string tokenNo,
            HttpContext context)
        {
            Hashtable hashParamToReplace = new Hashtable();
            hashParamToReplace.Add("SAusername", subAdminUsername);
            hashParamToReplace.Add("username", usernameOfUser);
            hashParamToReplace.Add("tokenNo", tokenNo);
            return saveEmailInfo(
                 emailId,
                 getEmailSubject(EMAIL_FOR.USER_QUERY_PROCESS),
                 hashParamToReplace,
                 EMAIL_FOR.USER_QUERY_PROCESS, context
                 );
        }

        //public static void userChangePassword(string emailId, string param1, string param2, string param3, string param4
        //                                    , bool useConnection, SqlTransaction transaction, SqlConnection con, HttpContext appContext)
        //{
        //    Hashtable hashParamToReplace = new Hashtable();
        //    hashParamToReplace.Add("param1", param1);
        //    hashParamToReplace.Add("param2", param2);
        //    hashParamToReplace.Add("param3", param3);
        //    hashParamToReplace.Add("param4", param4);
        //    _useConnection = useConnection;
        //    //_transaction = transaction;
        //    _con = con;
        //    saveEmailInfo(emailId, getEmailSubject(EMAIL_FOR.USER_PASSWORD_CHANGED), hashParamToReplace, EMAIL_FOR.USER_PASSWORD_CHANGED, appContext);
        //}
        //public static void userResetPassword(string emailId, string param1, string param2, string param3, string param4
        //                                    , bool useConnection, SqlTransaction transaction, SqlConnection con, HttpContext appContext)
        //{
        //    Hashtable hashParamToReplace = new Hashtable();
        //    hashParamToReplace.Add("param1", param1);
        //    hashParamToReplace.Add("param2", param2);
        //    hashParamToReplace.Add("param3", param3);
        //    hashParamToReplace.Add("param4", param4);
        //    _useConnection = useConnection;
        //    //_transaction = transaction;
        //    _con = con;
        //    saveEmailInfo(emailId, getEmailSubject(EMAIL_FOR.USER_RESET_PASSWORD), hashParamToReplace, EMAIL_FOR.USER_RESET_PASSWORD, appContext);
        //}
    }


}
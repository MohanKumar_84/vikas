﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class AddDevicePushMsgIDResp
    {
        ResponseStatus _respStatus;
        string _requestId;

        public AddDevicePushMsgIDResp(ResponseStatus respStatus,
            string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Add device push msgid Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            AddDevicePushMsgIDResponse objblockunblockresponse = new AddDevicePushMsgIDResponse();
            objblockunblockresponse.rid = _requestId;
            objblockunblockresponse.func = Convert.ToString((int)FUNCTION_CODES.ADD_DEVICE_PUSH_MSG_ID);
            objblockunblockresponse.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<AddDevicePushMsgIDResponse>(objblockunblockresponse);
            return Utilities.getFinalJson(strJsonWithDetails);
            //return Utilities.getCommonResponseJson(FUNCTION_CODES.ADD_DEVICE_PUSH_MSG_ID, _requestId, _respStatus);
        }
    }

    public class AddDevicePushMsgIDResponse : CommonResponse
    {
        public AddDevicePushMsgIDResponse()
        { }
    }
}
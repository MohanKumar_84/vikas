﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class DoTaskReqForBatchProcess
    {
        string _userId, _sessionId, _requestId, _deviceId, _deviceType, _companyId;
        int _functionCode;

        List<DoTaskCmdDetails> _commands;
        
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        
        public string SessionId
        {
            get { return _sessionId; }
        }
        public string UserId
        {
            get { return _userId; }
        }

        public List<DoTaskCmdDetails> Commands
        {
            get { return _commands; }
            set { _commands = value; }
        }
        public DoTaskReqForBatchProcess(string requestJson, string userId)
        {
            RequestJsonParsing<DoTaskReqDataNew> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<DoTaskReqDataNew>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.DO_TASK_FOR_BATCH_PROCESS)
            {
                throw new Exception("Invalid Function Code");
            }
            _userId = userId;
            _sessionId = objRequestJsonParsing.req.sid;
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            _commands = objRequestJsonParsing.req.data.cmds;
            if (_commands.Count == 0)
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
        }
        public DoTaskReqForBatchProcess(string rid, string enid, string cmd, int ctyp, List<DoTaskCmdDetails> lp, DataConnCredential cr, string userId)
        {
            _userId = userId;
            _requestId = rid;
            _companyId = enid;
            _commands = lp;
            if (_commands.Count == 0)
            {
                throw new Exception(((int)MficientConstants.INVALID_REQUEST_JSON).ToString());
            }
        }
    }

    [DataContract]
    public class DoTaskReqDataNew : Data
    {
        /// <summary>
        /// Command Details
        /// </summary>
        [DataMember]
        public List<DoTaskCmdDetails> cmds { get; set; }
    }
    [DataContract]
    public class DoTaskCmdDetails
    {
        public DoTaskCmdDetails() { }
        public DoTaskCmdDetails(int _index, string _cmd, int _ctyp, List<DoTaskCmdParameters> _cmdlp, DataConnCredential _cr)
        {
            idx = _index.ToString();
            index = _index;
            ctrlid = "";
            cmd = _cmd;
            ctyp = _ctyp;
            cmdlp = _cmdlp;
            rtfn = 0;
            cr = _cr;
        }
        ///// <summary>
        ///// Process Id                    
        ///// </summary>
        //[DataMember]
        //public string pid { get; set; }

        /// <summary>
        /// Process Id                    
        /// </summary>
        [DataMember]
        public string idx { get; set; }

        /// <summary>
        /// Index We do not get this from
        /// Mobile request.
        /// It is used only for sorting process
        /// in Batch Process
        /// </summary>
        [DataMember]
        public int index { get; set; }

        /// <summary>
        /// Control Id                    
        /// </summary>
        [DataMember]
        public string ctrlid { get; set; }

        /// <summary>
        /// Command Id
        /// </summary>
        [DataMember]
        public string cmd { get; set; }

        /// <summary>
        /// Command Type
        /// </summary>
        [DataMember]
        public int ctyp { get; set; }

        /// <summary>
        /// Parameters for command 
        /// </summary>
        [DataMember]
        public List<DoTaskCmdParameters> cmdlp { get; set; }
        

        /// <summary>
        /// Return Function
        /// </summary>
        [DataMember]
        public int rtfn { get; set; }

        /// <summary>
        /// Credential
        /// </summary>
        [DataMember]
        public DataConnCredential cr { get; set; }
    }
    [DataContract]
    public class DoTaskCmdParameters
    {
        public DoTaskCmdParameters(List<QueryParameters> _lp)
        {
            lp = _lp;
        }
        /// <summary>
        /// Parameters for query
        /// </summary>
        [DataMember]
        public List<QueryParameters> lp { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class QueryDetailWithCommentsReq
    {
        string _requestId, _companyId, _userId,_tokenNo;
        int _functionCode;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string UserId
        {
            get { return _userId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public string TokenNo
        {
            get { return _tokenNo; }
        }

        public QueryDetailWithCommentsReq(string requestJson, string userId)
        {
            RequestJsonParsing<QueryDetailWithCommentsReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<QueryDetailWithCommentsReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.USER_QUERY_DETAIL)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.data.enid;
            _userId = userId;
            _tokenNo = objRequestJsonParsing.req.data.tn;
        }
    }

    [DataContract]
    public class QueryDetailWithCommentsReqData : Data
    {
        /// <summary>
        /// Query token no.
        /// </summary>
        [DataMember]
        public string tn { get; set; }
    }
}
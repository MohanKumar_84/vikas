﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class mGramMessageStatusUpdateResp
    {
        ResponseStatus _respStatus;
        string _requestId;
        public mGramMessageStatusUpdateResp(ResponseStatus respStatus, string requestId)
        {
            _respStatus = respStatus;
            _requestId = requestId;
        }
        public string GetResponseJson()
        {
            mGramMessageStatusUpdateResponse objgetmGramMessagesResp = new mGramMessageStatusUpdateResponse();
            objgetmGramMessagesResp.func = Convert.ToString((int)FUNCTION_CODES.MGRAM_MESSAGE_STATUS_UPDATE);
            objgetmGramMessagesResp.rid = _requestId;
            objgetmGramMessagesResp.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<mGramMessageStatusUpdateResponse>(objgetmGramMessagesResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }
    public class mGramMessageStatusUpdateResponse : CommonResponse
    {
        public mGramMessageStatusUpdateResponse()
        { }
    }
}
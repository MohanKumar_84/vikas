﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientWS
{
    public class UserAuthenticationReq
    {
        string _requestId, _deviceId, _deviceType, _companyId, _userName, _password;
        int _functionCode;

        public string UserName
        {
            get { return _userName; }
        }
        public string Password
        {
            get { return _password; }
        }
        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }

        public UserAuthenticationReq(string requestJson)
        {
            RequestJsonParsing<UserAuthenticationReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<UserAuthenticationReqData>>(requestJson);
            string decryptedUserName = objRequestJsonParsing.req.data.unm;
            //string decryptedUserName = EncryptionDecryption.AESDecrypt(_companyId, objRequestJsonParsing.req.data.unm);
            if (decryptedUserName.Contains("\\"))
            {
                _userName = decryptedUserName.Split('\\')[1];
            }
            else
            {
                _userName = decryptedUserName;
            }

            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.USER_AUTHENTICATION)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
            //_password = EncryptionDecryption.AESDecrypt(_companyId, objRequestJsonParsing.req.data.pwd);
            _password = objRequestJsonParsing.req.data.pwd;
        }
    }

    [DataContract]
    public class UserAuthenticationReqData : Data
    {
        /// <summary>
        /// Password
        /// </summary>
        [DataMember]
        public string pwd { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientWS
{
    public class DoTaskWSHelperFunctions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="returnFunction"></param>
        /// <returns></returns>
        ///<exception cref="MficientException">Thrown if the return function is not defined</exception>
        public static RETURN_FUNCTION_TYPE_DOTASK getRtrnFnType(int returnFunction)
        {
            try
            {
                RETURN_FUNCTION_TYPE_DOTASK eRtrnFncType = (RETURN_FUNCTION_TYPE_DOTASK)Enum.Parse(typeof(RETURN_FUNCTION_TYPE_DOTASK), returnFunction.ToString());
                if (Enum.IsDefined(typeof(RETURN_FUNCTION_TYPE_DOTASK), eRtrnFncType))
                {
                    return eRtrnFncType;
                }
                else
                {
                    throw new MficientException("Return function not defined");
                }
            }
            catch (ArgumentException)
            {
                throw new MficientException("Return function not defined");
            }
            catch
            {
                throw new MficientException("Return function not defined");
            }
        }
    }
}
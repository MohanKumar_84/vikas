﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Data;
namespace mFicientWS
{
    public class GetPagingDataResp
    {
        ResponseStatus _respStatus;
        int _returnFuction;
        DataSet _lstData;
        string _requestId, _controlId;



        public GetPagingDataResp(ResponseStatus respStatus,
            string requestId, int returnFunction, DataSet lstData)
        {
            try
            {
                this._respStatus = respStatus;
                this._requestId = requestId;
                this._returnFuction = returnFunction;
                this._lstData = lstData;
                //this._controlId = controlId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Get Paging Data Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            string strResponse = String.Empty;
            GetPagingDataResponse<List<Seleced_cmd_Tables>> objFormDtlResp = new GetPagingDataResponse<List<Seleced_cmd_Tables>>();
                    objFormDtlResp.func = Convert.ToString((int)FUNCTION_CODES.GET_PAGING_DATA);
                    objFormDtlResp.rid = _requestId;
                    objFormDtlResp.status = _respStatus;

                    dotaskData<List<Seleced_cmd_Tables>> objCompleteTblSelectData = new dotaskData<List<Seleced_cmd_Tables>>();
                    List<Seleced_cmd_Tables> tablelst = new List<Seleced_cmd_Tables>();
                    DataSet dsTableLstData = (DataSet)_lstData;
                    /**
                      * Even if a dataset contains more than one table
                      * we have to return only the data of 
                      * the first table only.
                      * **/
                    if (dsTableLstData != null && dsTableLstData.Tables != null && dsTableLstData.Tables.Count > 0)
                    {
                        DataTable dt = dsTableLstData.Tables[0];
                        foreach (DataColumn dc in dt.Columns)
                        {
                            Seleced_cmd_Tables tbl = new Seleced_cmd_Tables();
                            tbl.cnm = dc.ColumnName;
                            //add column values
                            List<string> strList = new List<string>();
                            foreach (DataRow dr in dt.Rows)
                            {
                                strList.Add(dr[dc.ColumnName].ToString());
                            }
                            tbl.val = strList;
                            tablelst.Add(tbl);
                        }
                    }
                    objCompleteTblSelectData.dt = tablelst;
                    //objCompleteTblSelectData.ctrlid = this._controlId;
                    objCompleteTblSelectData.rtfn = 0.ToString();

                    objFormDtlResp.data = objCompleteTblSelectData;


                    strResponse = Utilities.SerializeJson<GetPagingDataResponse<List<Seleced_cmd_Tables>>>(objFormDtlResp);

                    return Utilities.getFinalJson(strResponse);
        }
        RETURN_FUNCTION_TYPE_DOTASK getDoTaskDataFor(
            int returnFunction)
        {
            return (RETURN_FUNCTION_TYPE_DOTASK)Enum.Parse(typeof(RETURN_FUNCTION_TYPE_DOTASK),
                returnFunction.ToString());
        }
    }

    public class GetPagingDataResponse<T> : CommonResponse
    {
        public GetPagingDataResponse()
        { }
        [DataMember]
        public dotaskData<T> data { get; set; }
    }
}
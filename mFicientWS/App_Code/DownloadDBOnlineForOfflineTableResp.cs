﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Data;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace mFicientWS
{

    public class DownloadDBOnlineForOfflineTableResp
    {
        private ResponseStatus respStatus;
        private string requestId;
        List<QueryParameters> _lstOutputjson;
        Hashtable jaCols;
        object objData;

        public DownloadDBOnlineForOfflineTableResp(ResponseStatus _respStatus, object _lstSelectedTablevalues, string _requestId, List<QueryParameters> lstOutputjson, Hashtable _jaCols)
        {
            try
            {
                respStatus = _respStatus;
                requestId = _requestId;
                objData = _lstSelectedTablevalues;
                jaCols = _jaCols;
                this._lstOutputjson = lstOutputjson;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Download Online Database For Offline Table Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            DownloadOnlineDatabaseOfflineTableRespData objdownloadonlinedatabaseofflineTable = new DownloadOnlineDatabaseOfflineTableRespData();
            objdownloadonlinedatabaseofflineTable.func = Convert.ToString((int)FUNCTION_CODES.DOWNLOAD_ONLINE_DATABASE_OFFLINE);
            objdownloadonlinedatabaseofflineTable.rid = requestId;
            objdownloadonlinedatabaseofflineTable.status = respStatus;
            DataSet dsTableLstData = (DataSet)objData;
            List<Seleced_cmd_Tables> tablelst = new List<Seleced_cmd_Tables>();
            /**
              * Even if a dataset contains more than one table
              * we have to return only the data of 
              * the first table only.
              * **/
            if (respStatus.cd == "0" && dsTableLstData != null && dsTableLstData.Tables != null && dsTableLstData.Tables.Count > 0)
            {
                DataTable dt = dsTableLstData.Tables[0];
                foreach (DataColumn dc in dt.Columns)
                {
                    foreach (QueryParameters outcol in _lstOutputjson)
                    {                       
                        if (outcol.para.ToLower() == dc.ColumnName.ToLower() && outcol.val.Length > 0)
                        {
                            string colname = outcol.val.Substring(2).ToLower();
                            if (jaCols.Contains(colname))
                            {
                                JObject colMeta = (JObject)jaCols[colname];
                                Seleced_cmd_Tables tbl = new Seleced_cmd_Tables();
                                tbl.cnm = outcol.val.Substring(2);
                                //add column values
                                List<string> strList = new List<string>();
                                foreach (DataRow dr in dt.Rows)
                                {
                                    string colValue = dr[dc.ColumnName].ToString();
                                    decimal testDecimal = 0;
                                    if ((colMeta["ctyp"].ToString() == "1" || colMeta["ctyp"].ToString() == "2") && !decimal.TryParse(colValue, out testDecimal))
                                    {
                                        colValue = colMeta["dval"].ToString();
                                        if (string.IsNullOrEmpty(colValue.Trim())) colValue = "0";
                                        strList.Add(colMeta["dval"].ToString());
                                    }
                                    else
                                    {
                                        strList.Add(colValue);
                                    }
                                }
                                tbl.val = strList;
                                tablelst.Add(tbl);
                                break;
                            }
                        }
                    }
                }
            }
            objdownloadonlinedatabaseofflineTable.data = new doDownloadData();
            objdownloadonlinedatabaseofflineTable.data.dt = tablelst;
            string strJson = Utilities.SerializeJson<DownloadOnlineDatabaseOfflineTableRespData>(objdownloadonlinedatabaseofflineTable);
            return Utilities.getFinalJson(strJson);
        }
    }

    [DataContract]
    public class DownloadOnlineDatabaseOfflineTableRespData : CommonResponse
    {
        public DownloadOnlineDatabaseOfflineTableRespData()
        {
        }

        [DataMember]
        public doDownloadData data { get; set; }
    }

    public class doDownloadData
    {
        [DataMember]
        public List<Seleced_cmd_Tables> dt { get; set; }
    }




}

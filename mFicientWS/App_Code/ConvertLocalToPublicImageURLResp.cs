﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace mFicientWS
{
    
    public class ConvertLocalToPublicImageURLResp
    {
        private string imageBase64, comapnyId, userName;
        private ResponseStatus respStatus;
        private string requestId;

        public ConvertLocalToPublicImageURLResp(string _imageBase64, ResponseStatus _respStatus, string _requestId)
        {
            try
            {
                imageBase64 = _imageBase64;
                respStatus = _respStatus;
                requestId = _requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Local to Public Image URL Response[" + ex.Message + "]");
            }

        }
        public string GetResponseJson()
        {
            ConvertLocalToPublicRespData objconvertLocaltoPublicData = new ConvertLocalToPublicRespData();
            objconvertLocaltoPublicData.func = Convert.ToString((int)FUNCTION_CODES.CONVERT_LOCAL_TO_PUBLIC_IMAGEURL);
            objconvertLocaltoPublicData.rid = requestId;
            objconvertLocaltoPublicData.status = respStatus;
            ConvertImageURL objData = new ConvertImageURL();
            objData.img = imageBase64;
            objconvertLocaltoPublicData.data = objData;
            string strjson = Utilities.SerializeJson<ConvertLocalToPublicRespData>(objconvertLocaltoPublicData);
            return Utilities.getFinalJson(strjson);
        }
    }
    [DataContract]
    public class ConvertLocalToPublicRespData : CommonResponse
    {
        [DataMember]
        public ConvertImageURL data { get; set; }
    }

    [DataContract]
    public class ConvertImageURL
    {
        /// <summary>
        /// Public URL
        /// </summary>
        [DataMember]
        public string img { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using System.IO;
using System.Text;
using System.Net;
namespace mFicientWS
{
    public class OdataCommand
    {
        #region Private Members

        private string commandId, connectorId, endPoint, enterpriseId, resourcePath, singleValueResourcePath, httpData, returnType, datasetPath, commandName, connectionName, dataServiceVersion,
            queryOptions, inputParaJson, outputParaJson, mPluginAgent, mPluginAgentPwd, userName, password, cacheType, cacheFrequency, cacheCondition;
        private ResponseType responseType;
        private HttpType httpType;
        //private ODataConnector oDataConnector;
        private FunctionType functionType;
        private ODataRequestType resourceType;
        List<IdeWsParam> _outputTagPath;
        List<QueryParametersType> lstInputParameters;
        ResponseStatus objRespStatus = new ResponseStatus();

        DoTaskReq _doTaskRequest;
        #endregion

        #region Constructor
        public bool isExist = false;
        public OdataCommand(string _commandId, string _enterpriseId)
        {
            commandId = _commandId;
            enterpriseId = _enterpriseId;
            _outputTagPath = new List<IdeWsParam>();
            lstInputParameters = new List<QueryParametersType>();
            GetCommandDetails();
        }

        #endregion

        #region Public Properties

        public string ConnectorId
        {
            get
            {
                return connectorId;
            }
        }

        public string CommandId
        {
            get
            {
                return commandId;
            }
        }

        public string EndPoint
        {
            get
            {
                return endPoint;
            }
        }

        public string EnterpriseId
        {
            get
            {
                return enterpriseId;
            }
        }

        public string DataServiceVersion
        {
            get
            {
                return dataServiceVersion;
            }
        }

        public string ResourcePath
        {
            get
            {
                return resourcePath;
            }
        }

        public string SingleValueResourcePath
        {
            get
            {
                return singleValueResourcePath;
            }
        }

        public string HttpData
        {
            get
            {
                return httpData;
            }
        }

        public string CommandName
        {
            get
            {
                return commandName;
            }
        }

        public string ConnectionName
        {
            get
            {
                return connectionName;
            }
        }

        public string QueryOptions
        {
            get
            {
                return queryOptions;
            }
        }

        public List<QueryParametersType> InputParaTypes
        {
            get
            {
                return lstInputParameters;
            }
        }

        public string OutputParaJson
        {
            get
            {
                return outputParaJson;
            }
        }

        public string MPluginAgent
        {
            get
            {
                return mPluginAgent;
            }
        }

        public string MPluginAgentPwd
        {
            get
            {
                return mPluginAgentPwd;
            }
        }

        public string UserName
        {
            get
            {
                return userName;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
        }

        public ResponseType Responsetype
        {
            get
            {
                return responseType;
            }
        }

        public HttpType Httptype
        {
            get
            {
                return httpType;
            }
        }

        //public ODataConnector OdataConnector
        //{
        //    get
        //    {
        //        return oDataConnector;
        //    }
        //}

        public FunctionType Functiontype
        {
            get
            {
                return functionType;
            }
        }

        public ODataRequestType Resourcetype
        {
            get
            {
                return resourceType;
            }
        }

        public string ReturnType
        {
            get
            {
                return returnType;
            }
        }

        public string DataSetPath
        {
            get
            {
                return datasetPath;
            }
        }
        public List<IdeWsParam> OutputTagPath
        {
            get { return _outputTagPath; }
            private set { _outputTagPath = value; }
        }
        #endregion

        #region Public Methods

        public DoTaskResp processODataCommand(DoTaskReq doTaskRequest)
        {
            try
            {
                _doTaskRequest = doTaskRequest;
                if (String.IsNullOrEmpty(this.CommandName))
                {
                    return new DoTaskResp(Utilities.getResponseStatus(new Exception(((int)DO_TASK_ERROR.INVALID_COMMAND).ToString())),
                   _doTaskRequest.RequestId, "", "0", null, _doTaskRequest.ControlId, cacheType, cacheFrequency, cacheCondition,"",new List<List<string>>());
                }
                if (!String.IsNullOrEmpty(this.MPluginAgent.Trim()))
                {
                    return processODataWithMplugin();
                }
                else
                {
                    _doTaskRequest.DataCredential.pwd = EncryptionDecryption.AESDecrypt(_doTaskRequest.CompanyId, _doTaskRequest.DataCredential.pwd);
                    _doTaskRequest.DataCredential.unm = EncryptionDecryption.AESDecrypt(_doTaskRequest.CompanyId, _doTaskRequest.DataCredential.unm);

                    return processODataWithoutMplugin();
                }
            }
            catch (Exception e)
            {
                objRespStatus = Utilities.getResponseStatus(e);
            }
            return new DoTaskResp(objRespStatus, _doTaskRequest.RequestId, "", "0", null, _doTaskRequest.ControlId, cacheType, cacheFrequency, cacheCondition, "", new List<List<string>>());
        }

        DoTaskResp processODataWithMplugin()
        {
            ODataConnector objOdataCon = new ODataConnector(this.ConnectorId, this.EnterpriseId);
            objOdataCon.GetConnector();
            ResponseStatus objRespStatus = new ResponseStatus();
            objRespStatus.cd = "0";
            objRespStatus.desc = "";
            string strMPluginUrl = Utilities.getMPlugInServerURL(_doTaskRequest.CompanyId);
            string strResponse = "";
            Object objRespData = null;

            try
            {
                if (!String.IsNullOrEmpty(strMPluginUrl))
                {
                    string strHttpUrl = strMPluginUrl;
                    if (String.IsNullOrEmpty(strHttpUrl))
                    {
                        throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
                    }
                    else
                    {
                        string strOdataUrlAfterReplacingParams = String.Empty;
                        string strBodyAfterReplacingParams = String.Empty;
                        if (!String.IsNullOrEmpty(this.ResourcePath.Trim()))
                        {
                            strOdataUrlAfterReplacingParams += this.ResourcePath;
                            if (!string.IsNullOrEmpty(this.SingleValueResourcePath))
                                strOdataUrlAfterReplacingParams += this.SingleValueResourcePath;
                            if (this.Httptype != HttpType.POST)
                            {
                                foreach (QueryParameters parameters in _doTaskRequest.Parameters)
                                {
                                    if (getParameterType(parameters.para, this.InputParaTypes) == ODATA_INPUT_TYPE.String)
                                        strOdataUrlAfterReplacingParams = strOdataUrlAfterReplacingParams.Replace("@@" + parameters.para + "@@", "'" + parameters.val + "'");
                                    else
                                        strOdataUrlAfterReplacingParams = strOdataUrlAfterReplacingParams.Replace("@@" + parameters.para + "@@", parameters.val);
                                }
                            }
                        }
                        if (!String.IsNullOrEmpty(this.QueryOptions.Trim()))
                        {
                            strOdataUrlAfterReplacingParams += "?" + this.QueryOptions;
                            foreach (QueryParameters parameters in _doTaskRequest.Parameters)
                            {
                                strOdataUrlAfterReplacingParams = strOdataUrlAfterReplacingParams.Replace("@@" + parameters.para + "@@", parameters.val);
                            }
                        }
                        if (this.Httptype != HttpType.GET ||
                          this.Httptype != HttpType.DELETE)
                        {
                            strBodyAfterReplacingParams += this.HttpData;
                            foreach (QueryParameters parameters in _doTaskRequest.Parameters)
                            {
                                strBodyAfterReplacingParams = strBodyAfterReplacingParams.Replace("@@" + parameters.para + "@@", parameters.val);
                            }
                        }
                        mPlugin.QueryType eQueryType = mPlugin.QueryType.POST;
                        switch (this.Httptype)
                        {
                            case HttpType.GET:
                                eQueryType = mPlugin.QueryType.GET;
                                break;
                            case HttpType.POST:
                                eQueryType = mPlugin.QueryType.POST;
                                break;
                            case HttpType.PUT:
                                eQueryType = mPlugin.QueryType.HTTP_PUT;
                                break;
                            case HttpType.PATCH:
                                eQueryType = mPlugin.QueryType.HTTP_PATCH;
                                break;
                            case HttpType.MERGE:
                                eQueryType = mPlugin.QueryType.HTTP_MERGE;
                                break;
                            case HttpType.DELETE:
                                eQueryType = mPlugin.QueryType.HTTP_DELETE;
                                break;

                        }
                        try
                        {
                            if (this.Httptype == HttpType.GET)
                            {

                                strResponse = mPlugin.RunODataWebservice(this.EnterpriseId, this.MPluginAgent, this.MPluginAgentPwd, this.ConnectorId,
                                eQueryType, String.Empty, this.Resourcetype, this.Responsetype, strOdataUrlAfterReplacingParams, this.Functiontype, objOdataCon.HttpAuthenticationType, _doTaskRequest.UserId);

                                if (!String.IsNullOrEmpty(strResponse))
                                {
                                    if (this.Responsetype == ResponseType.NONE)
                                    {
                                        List<string> lstOdataSingleResp = new List<string>();
                                        lstOdataSingleResp.Add(strResponse);
                                        objRespData = lstOdataSingleResp;
                                    }
                                    else
                                    {
                                        objRespData = readResponseByType(this.Responsetype, strResponse, this);
                                    }
                                }
                                else if (!String.IsNullOrEmpty(strResponse))
                                {
                                    return responseForReturnFunctionZero(this.Responsetype, strResponse, this);
                                }
                            }
                            else
                            {
                                strResponse = mPlugin.RunODataWebservice(this.EnterpriseId, this.MPluginAgent, this.MPluginAgentPwd, this.ConnectorId,
                                eQueryType, strBodyAfterReplacingParams, this.Resourcetype, this.Responsetype, strOdataUrlAfterReplacingParams, this.Functiontype, objOdataCon.HttpAuthenticationType, _doTaskRequest.UserName);
                            }
                        }
                        catch (HttpException hex)
                        {
                            if (hex.Message == HttpStatusCode.NotFound.ToString())
                            {
                                throw new Exception(((int)DO_TASK_ERROR.MPLUGIN_SERVER_NOT_FOUND).ToString());
                            }
                            else
                            {
                                throw Utilities.HttpExecptionToException(hex);
                            }
                        }
                        catch (mPlugin.mPluginException mex)
                        {
                            int errorCode = Utilities.processHttpStatusCode(Convert.ToInt32(mex.Message));
                            throw new Exception(errorCode.ToString());
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return new DoTaskResp(objRespStatus, _doTaskRequest.RequestId, "", "0", objRespData, _doTaskRequest.ControlId, cacheType, cacheFrequency, cacheCondition, "", new List<List<string>>());
        }
        DoTaskResp responseForReturnFunctionZero(ResponseType responseType, string response, OdataCommand odataCommand)
        {
            if (odataCommand == null) throw new ArgumentException();
            DataTable dtblData = new DataTable();
            DataSet ds = new DataSet();
            if (responseType == ResponseType.XML)
            {
                readHTTPXMLResponse objReadXMLResp = new readHTTPXMLResponse(response,
                    odataCommand);
                dtblData = (DataTable)objReadXMLResp.ProcessForOdata();
            }
            else
            {

                ReadJsonResponseForDoTask objReadJson =
                   new ReadJsonResponseForDoTask(response, odataCommand);
                dtblData = (DataTable)objReadJson.ProcessForOData();
            }
            ds.Tables.Add(dtblData);
            return new DoTaskResp(objRespStatus, _doTaskRequest.RequestId, "", "0", ds, _doTaskRequest.ControlId, cacheType, cacheFrequency, cacheCondition, "", new List<List<string>>());

        }
        private ODATA_INPUT_TYPE getParameterType(string paraName, List<QueryParametersType> lstParametersList)
        {
            foreach (QueryParametersType objPara in lstParametersList)
            {
                if (objPara.para.Equals(paraName, StringComparison.CurrentCultureIgnoreCase))
                {
                    return (ODATA_INPUT_TYPE)Convert.ToInt32(objPara.typ);
                }
            }
            return ODATA_INPUT_TYPE.String;
        }

        DoTaskResp processODataWithoutMplugin()
        {
            ODataConnector objOdataCon = new ODataConnector(this.ConnectorId, this.EnterpriseId);
            objOdataCon.GetConnector();
            ResponseStatus objRespStatus = new ResponseStatus();
            objRespStatus.cd = "0";
            objRespStatus.desc = "";
            string strResponse = "";
            Object objRespData = null;
            string strOdataUrlAfterReplacingParams = String.Empty;
            string strBodyAfterReplacingParams = String.Empty;

            try
            {
                if (!String.IsNullOrEmpty(this.ResourcePath.Trim()))
                {
                    strOdataUrlAfterReplacingParams += this.ResourcePath;
                    if (!string.IsNullOrEmpty(this.SingleValueResourcePath))
                        strOdataUrlAfterReplacingParams += this.SingleValueResourcePath;
                    if (this.Httptype != HttpType.POST)
                    {
                        foreach (QueryParameters parameters in _doTaskRequest.Parameters)
                        {
                            if (getParameterType(parameters.para, this.InputParaTypes) == ODATA_INPUT_TYPE.String)
                                strOdataUrlAfterReplacingParams = strOdataUrlAfterReplacingParams.Replace("@@" + parameters.para + "@@", "'" + parameters.val + "'");
                            else
                                strOdataUrlAfterReplacingParams = strOdataUrlAfterReplacingParams.Replace("@@" + parameters.para + "@@", parameters.val);
                        }
                    }
                }
                if (!String.IsNullOrEmpty(this.QueryOptions.Trim()))
                {
                    strOdataUrlAfterReplacingParams += "?" + this.QueryOptions;
                    foreach (QueryParameters parameters in _doTaskRequest.Parameters)
                    {
                        strOdataUrlAfterReplacingParams = strOdataUrlAfterReplacingParams.Replace("@@" + parameters.para + "@@", parameters.val);
                    }
                }
                if (this.Httptype != HttpType.GET ||
                  this.Httptype != HttpType.DELETE)
                {
                    strBodyAfterReplacingParams += this.HttpData;
                    foreach (QueryParameters parameters in _doTaskRequest.Parameters)
                    {
                        strBodyAfterReplacingParams = strBodyAfterReplacingParams.Replace("@@" + parameters.para + "@@", parameters.val);
                    }
                }
                HttpResponseStatus response = processODataServiceQuery(this.EndPoint, strOdataUrlAfterReplacingParams, this.UserName, this.Password, this.Httptype,
                                         this.DataServiceVersion, this.Responsetype, this.Resourcetype, this.Functiontype, strBodyAfterReplacingParams, (ODataHTTP.EnumHttpAuthentication)Convert.ToInt32(objOdataCon.HttpAuthenticationType));

                strResponse = response.ResponseText;
                if (this.Httptype == HttpType.GET)
                {
                    if (response.StatusCode == HttpStatusCode.OK)// || response.StatusCode == HttpStatusCode.Accepted
                    {
                        if (!String.IsNullOrEmpty(strResponse))
                        {
                            if (this.Responsetype == ResponseType.NONE)
                            {
                                List<string> lstOdataSingleResp = new List<string>();
                                lstOdataSingleResp.Add(strResponse);
                                objRespData = lstOdataSingleResp;
                            }
                            else
                            {
                                objRespData = readResponseByType(
                                    this.Responsetype,
                                    strResponse,
                                    this
                                    );
                            }
                        }
                        else if (!String.IsNullOrEmpty(strResponse))
                        {
                            return responseForReturnFunctionZero(this.Responsetype, strResponse, this);
                        }
                    }
                    else
                    {
                        int errorCode = Utilities.processHttpStatusCode(Convert.ToInt32(response.StatusCode));
                        throw new Exception(errorCode.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return new DoTaskResp(objRespStatus, _doTaskRequest.RequestId, "", "0", objRespData, _doTaskRequest.ControlId, cacheType, cacheFrequency, cacheCondition, "", new List<List<string>>());
        }

        private HttpResponseStatus processODataServiceQuery(string endPoint, string odataResourceUrl, string username, string password, HttpType httpType, string dataServiceVersion, ResponseType responseType, ODataRequestType odataRequestType, FunctionType odataFunctionType, string httpData, ODataHTTP.EnumHttpAuthentication httpAuthentication)
        {
            string responseJson = string.Empty;
            HttpResponseStatus oResponse = null;
            ODataHTTP oDataHttp = new ODataHTTP(endPoint + odataResourceUrl);
            oDataHttp.SetHttpCredentials(username, password);
            oDataHttp.MaxDataServiceVersion = dataServiceVersion;
            oDataHttp.RequestFormat = responseType;
            oDataHttp.ODataRequesttype = odataRequestType;
            oDataHttp.Functiontype = odataFunctionType;
            oDataHttp.HttpAuthentication = httpAuthentication;
            switch (httpType)
            {
                case HttpType.GET:
                    oDataHttp.HttpRequestMethod = ODataHTTP.EnumHttpMethod.HTTP_GET;
                    break;

                case HttpType.POST:
                    oDataHttp.HttpRequestMethod = ODataHTTP.EnumHttpMethod.HTTP_POST;
                    oDataHttp.PostString = httpData;
                    break;
                case HttpType.PUT:
                    oDataHttp.HttpRequestMethod = ODataHTTP.EnumHttpMethod.HTTP_PUT;
                    oDataHttp.PostString = httpData;
                    break;

                case HttpType.PATCH:
                    oDataHttp.HttpRequestMethod = ODataHTTP.EnumHttpMethod.HTTP_PATCH;
                    oDataHttp.PostString = httpData;
                    break;

                case HttpType.MERGE:
                    oDataHttp.HttpRequestMethod = ODataHTTP.EnumHttpMethod.HTTP_MERGE;
                    oDataHttp.PostString = httpData;
                    break;

                case HttpType.DELETE:
                    oDataHttp.HttpRequestMethod = ODataHTTP.EnumHttpMethod.HTTP_DELETE;
                    break;
            }
            try
            {
                oResponse = oDataHttp.Request();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return oResponse;
        }


        private void GetCommandDetails()
        {
            string strQuery = @"SELECT  cmd.*,con.*,isnull(ag.MP_AGENT_PASSWORD, '') as MP_AGENT_PASSWORD,isnull(con.MPLUGIN_AGENT,'') as MPLUGIN_AGENT FROM TBL_ODATA_COMMAND as cmd inner join TBL_ODATA_CONNECTION as con on cmd.ODATA_CONNECTOR_ID=con.ODATA_CONNECTOR_ID and cmd.COMPANY_ID=con.COMPANY_ID 
            left outer join TBL_MPLUGIN_AGENT_DETAIL as ag on ag.MP_AGENT_NAME=con.MPLUGIN_AGENT and ag.COMPANY_ID=con.COMPANY_ID 
            WHERE con.COMPANY_ID = @COMPANY_ID";

            if (commandId.StartsWith("name:////"))
            {
                strQuery += " and ODATA_COMMAND_NAME = @ODATA_COMMAND_ID;";
                commandId = commandId.Substring(6);
            }
            else strQuery += " ODATA_COMMAND_ID = @ODATA_COMMAND_ID ;";

            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.Parameters.AddWithValue("@ODATA_COMMAND_ID", commandId);
            cmd.Parameters.AddWithValue("@COMPANY_ID", enterpriseId);
            DataSet ds = MSSqlDatabaseClient.SelectDataFromSqlCommand(cmd);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                connectorId = ds.Tables[0].Rows[0]["ODATA_CONNECTOR_ID"].ToString();
                mPluginAgentPwd = ds.Tables[0].Rows[0]["MP_AGENT_PASSWORD"].ToString();
                mPluginAgent = ds.Tables[0].Rows[0]["MPLUGIN_AGENT"].ToString();
                endPoint = ds.Tables[0].Rows[0]["ODATA_ENDPOINT"].ToString();
                dataServiceVersion = ds.Tables[0].Rows[0]["VERSION"].ToString();
                resourcePath = ds.Tables[0].Rows[0]["RESOURCE_PATH"].ToString();
                singleValueResourcePath = ds.Tables[0].Rows[0]["ADDITIONAL_RESOURCE_PATH"].ToString();
                httpType = (HttpType)Convert.ToInt32(ds.Tables[0].Rows[0]["HTTP_TYPE"].ToString().Trim().Split('_')[0]);
                resourceType = (ODataRequestType)Convert.ToInt32(ds.Tables[0].Rows[0]["RESOURCE_TYPE"].ToString().Trim());
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["FUNCTION_TYPE"].ToString().Trim())) functionType = (FunctionType)Convert.ToInt32(ds.Tables[0].Rows[0]["FUNCTION_TYPE"].ToString());
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["RESPONSE_FORMAT"].ToString().Trim())) responseType = (ResponseType)Convert.ToInt32(ds.Tables[0].Rows[0]["RESPONSE_FORMAT"].ToString());
                commandName = ds.Tables[0].Rows[0]["ODATA_COMMAND_NAME"].ToString();
                connectionName = ds.Tables[0].Rows[0]["CONNECTION_NAME"].ToString();
                httpData = ds.Tables[0].Rows[0]["HTTP_DATA"].ToString();
                queryOptions = ds.Tables[0].Rows[0]["QUERY_OPTION"].ToString();
                inputParaJson = ds.Tables[0].Rows[0]["INPUT_PARAMETER_JSON"].ToString();
                outputParaJson = ds.Tables[0].Rows[0]["OUTPUT_PARAMETER_JSON"].ToString();
                userName = ds.Tables[0].Rows[0]["USER_NAME"].ToString();
                cacheType = Convert.ToString(ds.Tables[0].Rows[0]["CACHE"]);
                cacheFrequency = Convert.ToString(ds.Tables[0].Rows[0]["EXPIRY_FREQUENCY"]);
                cacheCondition = Convert.ToString(ds.Tables[0].Rows[0]["EXPIRY_CONDITION"]);
                if (!string.IsNullOrEmpty(userName))
                    userName = EncryptionDecryption.AESDecrypt(enterpriseId, ds.Tables[0].Rows[0]["USER_NAME"].ToString());
                password = ds.Tables[0].Rows[0]["PASSWORD"].ToString();
                if (!string.IsNullOrEmpty(password))
                    password = EncryptionDecryption.AESDecrypt(enterpriseId, ds.Tables[0].Rows[0]["PASSWORD"].ToString());
                returnType = ds.Tables[0].Rows[0]["RETURN_TYPE"].ToString();
                datasetPath = ds.Tables[0].Rows[0]["DATASET_PATH"].ToString();

                if (!string.IsNullOrEmpty(outputParaJson.Trim()))
                {
                    MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(outputParaJson));
                    System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(OutputTagPath.GetType());
                    OutputTagPath = serializer.ReadObject(ms) as List<IdeWsParam>;
                }

                if (!string.IsNullOrEmpty(inputParaJson.Trim()))
                {
                    List<QueryParametersType> objRequestParaJsonParsing = Utilities.DeserialiseJson<List<QueryParametersType>>(inputParaJson);
                    lstInputParameters = objRequestParaJsonParsing;
                }
            }
        }

        #endregion
        #region ODATA RESPONSE READ
        Object readResponseByType(ResponseType responseType, string response, OdataCommand odataCmd)
        {
            if (responseType == ResponseType.XML)
            {
                return readXmlResponse(response, odataCmd);

            }
            else if (responseType == ResponseType.JSON)
            {
                return readResponseJson(response, odataCmd, _doTaskRequest);
            }
            else
            {
                return null;
            }
        }

        Object readResponseJson(string responseJson, OdataCommand odataCmd, DoTaskReq doTaskReq)
        {
            ReadJsonResponseForDoTask objReadJson = new ReadJsonResponseForDoTask(responseJson, odataCmd, doTaskReq);
            return objReadJson.ProcessForOData();
        }

        Object readXmlResponse(string responseXML, OdataCommand odataCmd)
        {
            readHTTPXMLResponse obj = new readHTTPXMLResponse(_doTaskRequest, responseXML, odataCmd);
            return obj.ProcessForOdata();
        }

        Object readRawResponse(string responseJson, OdataCommand odataCmd, DoTaskReq doTaskReq)
        {
            ReadJsonResponseForDoTask objReadJson = new ReadJsonResponseForDoTask(responseJson, odataCmd, doTaskReq);
            return objReadJson.ProcessForOData();
        }
        #endregion
    }
}
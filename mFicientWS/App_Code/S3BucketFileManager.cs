﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Amazon.S3.Model;
using Amazon.S3;
using System.IO;

namespace mFicientWS
{
    public class S3BucketFileManager
    {
        private IAmazonS3 s3Client;
        private string bucketName, accessKey, secretAccessKey;

        #region Constructor

        public S3BucketFileManager(string _bucketName, string _accessKey, string _secretAccessKey)
        {
            bucketName = _bucketName;
            accessKey = _accessKey;
            secretAccessKey = _secretAccessKey;
            s3Client = new AmazonS3Client(accessKey, secretAccessKey, Amazon.RegionEndpoint.APSoutheast1);
        }

        #endregion

        #region Public Methods

        public void UploadFile(string key, string filePath)
        {
            try
            {
                PutObjectRequest request = new PutObjectRequest();
                request.BucketName = bucketName;
                request.Key = key;
                request.FilePath = filePath;
                s3Client.PutObject(request);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new Exception("Invalid AWS Credentials.");
                }
                else
                {
                    throw new Exception(@"Error occurred when writing an object", amazonS3Exception);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UploadFile(string key, Stream inputStream)
        {
            try
            {
                PutObjectRequest request = new PutObjectRequest();
                request.BucketName = bucketName;
                request.Key = key;
                request.InputStream = inputStream;
                s3Client.PutObject(request);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new Exception("Invalid AWS Credentials.");
                }
                else
                {
                    throw new Exception(@"Error occurred when writing an object", amazonS3Exception);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Stream Download(string sourceFileKey)
        {
            try
            {
                GetObjectRequest request = new GetObjectRequest()
                {
                    BucketName = bucketName,
                    Key = sourceFileKey.Substring(8)
                };

                using (GetObjectResponse response = s3Client.GetObject(request))
                {
                    return response.ResponseStream;
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new Exception("Invalid AWS Credentials.");
                }
                else
                {
                    throw new Exception(@"Failed to download File", amazonS3Exception);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Download(string sourceFileKey, string destinationPath)
        {
            try
            {
                GetObjectRequest request = new GetObjectRequest()
                {
                    BucketName = bucketName,
                    Key = sourceFileKey.Substring(8)
                };

                using (GetObjectResponse response = s3Client.GetObject(request))
                {
                    response.WriteResponseStreamToFile(destinationPath);
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new Exception("Invalid AWS Credentials.");
                }
                else
                {
                    throw new Exception(@"Failed to download File", amazonS3Exception);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CopyingFile(string sourceKey, string destinationKey)
        {
            try
            {
                CopyObjectRequest request = new CopyObjectRequest
                {
                    SourceBucket = bucketName,
                    SourceKey = sourceKey,
                    DestinationBucket = bucketName,
                    DestinationKey = destinationKey,
                };
                s3Client.CopyObject(request);
            }
            catch (AmazonS3Exception s3Exception)
            {
                throw new Exception(s3Exception.Message + @"[ " + s3Exception.InnerException + " ]");
            }
        }

        public void DeleteFile(string key)
        {
            try
            {
                DeleteObjectRequest request = new DeleteObjectRequest()
                {
                    BucketName = bucketName,
                    Key = key
                };

                s3Client.DeleteObject(request);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new Exception("Invalid AWS Credentials.");
                }
                else
                {
                    throw new Exception(string.Format("An error occurred with the message '{0}' when deleting an object", amazonS3Exception.Message));
                }
            }
        }

        public bool CheckIfObjectExist(string key)
        {
            bool exist = false;
            try
            {
                ListObjectsRequest request = new ListObjectsRequest
                {
                    BucketName = bucketName
                };
                ListObjectsResponse response;

                do
                {
                    response = s3Client.ListObjects(request);
                    foreach (S3Object entry in response.S3Objects)
                    {
                        if (entry.Key == key)
                        {
                            exist = true;
                            break;
                        }
                    }
                    request.Marker = response.NextMarker;
                } while (response.IsTruncated);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new Exception("Invalid Credentials.");
                }
                else
                {
                    throw new Exception("Error occurred when listing objects", amazonS3Exception);
                }
            }
            return exist;
        }

        public List<string> GetObjectList()
        {
            List<string> lstKeys = new List<string>();
            try
            {
                ListObjectsRequest request = new ListObjectsRequest
                {
                    BucketName = bucketName
                };
                ListObjectsResponse response;

                do
                {
                    response = s3Client.ListObjects(request);
                    foreach (S3Object entry in response.S3Objects)
                    {
                        if (!lstKeys.Contains(entry.Key))
                            lstKeys.Add(entry.Key);
                    }
                    request.Marker = response.NextMarker;
                } while (response.IsTruncated);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new Exception("Invalid Credentials.");
                }
                else
                {
                    throw new Exception("Error occurred when listing objects", amazonS3Exception);
                }
            }
            return lstKeys;
        }

        #endregion
    }
}
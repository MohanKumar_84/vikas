﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
namespace mFicientWS
{
    public partial class TestConnection : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //query string get
            int intHttpStatusCode = (int)HttpStatusCode.OK;
            int intFunctionCode = 9999;
            string strResponse="",strRequest="";
            try
            {
                if (Request.HttpMethod == "GET")
                {
                    strRequest = Request.QueryString.Get("d");
                }
                else if (Request.HttpMethod == "POST")
                {
                    strRequest = Request.Form.Get("d");
                }
                else
                {
                    intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    return;
                }
                if (string.IsNullOrEmpty(strRequest))
                {
                    intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    return;
                }
                //string _RequestObject = "{\"req\":{\"rid\":\"9554\",\"eid\":\"12345\",\"data\":{\"rtyp\":\"2\",\"con\":\"Data Source=tcp:192.168.1.16\\\\LOGIXDEV,14335;Initial Catalog=MFICIENT_MPLUGIN_DB;User ID=mfdbuser;Password=mfdbuser9876\",\"dbtyp\":\"1\",\"agtid\":\"agent1\",\"agtpwd\":\"E10ADC3949BA59ABBE56E057F20F883E\"}}}";
                MPluginTestConnectionReq objTestConnReq = new MPluginTestConnectionReq(strRequest);
                MPluginTestConnection objMPluginTestConn = new MPluginTestConnection(objTestConnReq);
                MPluginTestConnectionResp objMPluginTestConnResp = objMPluginTestConn.Process();
                strResponse = objMPluginTestConnResp.GetResponseJson();
            }
            catch (HttpException ex)
            {
                if (ex.Message == HttpStatusCode.ServiceUnavailable.ToString())
                {
                    intHttpStatusCode = (int)HttpStatusCode.ServiceUnavailable;
                }
            }
            catch (Exception ex)
            {
                int intParseResult = 0;
                if (int.TryParse(ex.Message, out intParseResult))
                {
                    if (intParseResult == (int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR)
                    {
                        intHttpStatusCode = (int)HttpStatusCode.ServiceUnavailable;
                    }
                    else if (intParseResult == (int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR)
                    {
                        intHttpStatusCode = (int)HttpStatusCode.NoContent;
                    }
                    else if (intParseResult == MficientConstants.INVALID_REQUEST_JSON)
                    {
                        intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    }
                    else if (intParseResult == (int)HttpStatusCode.Unauthorized)
                    {
                        intHttpStatusCode = (int)HttpStatusCode.Unauthorized;
                    }
                    else
                    {
                        intHttpStatusCode = (int)HttpStatusCode.InternalServerError;
                    }
                }
                else
                {
                    if (ex.Source == "System.Runtime.Serialization")
                    {
                        intHttpStatusCode = (int)HttpStatusCode.BadRequest;
                    }
                    else
                    {
                        intHttpStatusCode = (int)HttpStatusCode.InternalServerError;
                    }
                }
            }
            finally
            {
                HttpContext.Current.Items.Add("FUNCTION_CODE", intFunctionCode);
                HttpContext.Current.Items.Add("HTTP_STATUS_CODE",intHttpStatusCode);
                HttpContext.Current.Items.Add("RESPONSE_XML", strResponse);
                HttpContext.Current.Items.Add("REQUEST_ID", "TestConnectionID");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataQueryParser
{
    #region SelectTag

    [TagType(JoinTag.cTagName)]
    [MatchJoinTag]
    internal class JoinTag : SimpleOneWordTag
    {
        #region Consts

        /// <summary>
        /// The name of the tag (its identifier).
        /// </summary>
        public const string cTagName = "JOIN";

        #endregion

        #region Properties

        /// <summary>
        /// Gets the name of the tag (its identifier and sql text)
        /// </summary>
        protected override string Name
        {
            get
            {
                return cTagName;
            }
        }

        #endregion
    }

    #endregion

    #region MatchSelectTagAttribute

    internal class MatchJoinTagAttribute : MatchSimpleOneWordTagAttribute
    {
        #region Properties

        /// <summary>
        /// Gets the name of the tag (its identifier and sql text)
        /// </summary>
        protected override string Name
        {
            get
            {
                return JoinTag.cTagName;
            }
        }

        #endregion
    }

    #endregion
}

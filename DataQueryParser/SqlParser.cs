﻿using System;
using System.Text;
using System.Xml;

namespace DataQueryParser
{
	#region SqlParser

    public class SqlParser : ParserBase
	{
		#region Base class implementation

		#region Fields

		/// <summary>
		/// Stores the types of all the possible tags.
		/// </summary>
		Type[] fTags;

		#endregion

		#region Properties

		/// <summary>
		/// Indicates whether the white space is a non-valueable character.
		/// </summary>
		protected override bool IsSkipWhiteSpace
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// Returns the list of all the available tags.
		/// </summary>
		protected override Type[] Tags
		{
			get
			{
				if (fTags == null)
				{
					fTags = new Type[] { 
						typeof(SelectTag),
                        typeof(FromTag),
                        typeof(JoinTag),
                        //typeof(WhereTag),
                        //typeof(OrderByTag),
						typeof(BracesTag),
						typeof(SquareBracketTag),
                        typeof(StringLiteralTag),
                        //typeof(ForUpdateTag),
                        //typeof(StartWith),
                        //typeof(GroupByTag)
                        //remove " type to parse postgre sql query
                        typeof(QuotedIdentifierTag)
					};
				}

				return fTags;
			}
		}

		#endregion

		#endregion

		#region Common

		#region Methods

		/// <summary>
		/// Returns the xml node which corresponds to the From tag.
		/// If this node does not exist, this method generates an
		/// exception.
		/// </summary>
        private XmlNode GetFromTagXmlNode()
        {
            XmlNode myFromNode = ParsedDocument.SelectSingleNode(string.Format(@"{0}/{1}[@{2}='{3}']", cRootXmlNodeName, cTagXmlNodeName, cTagTypeXmlAttributeName, FromTag.cTagName));
            if (myFromNode == null)
            {
                //throw new Exception(ToText());
            }

            return myFromNode;
        }

		/// <summary>
		/// Checks whether there is a tag in the text at the specified position, and returns its tag.
		/// </summary>
		internal new Type IsTag(string text, int position)
		{
			return base.IsTag(text, position);
		}

		#endregion

		#endregion
	}

	#endregion
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicient
{
    public class AddEnquiry
    {
        int statusCode;
        public AddEnquiry(string companyName,string country, string typeofCompany, int expectedUsers, int expectedWF, string contactPerson, string contactNo, string contactEmail)
        {
            try
            {
                string strEnquiryID = Utilities.GetMd5Hash(companyName + typeofCompany + DateTime.Now.Ticks.ToString()).Substring(0, 8).ToUpper();
                SqlCommand cmd = new SqlCommand(@"INSERT INTO ADMIN_TBL_ENQUIRY_DETAIL (  ENQUIRY_ID, COMPANY_NAME,COUNTRY_CODE,TYPE_OF_COMPANY,EXPECTED_USERS,EXPECTED_WF,CONTACT_PERSON,CONTACT_NUMBER,
                                            CONTACT_EMAIL,ENQUIRY_DATETIME,SALES_MNGR_ID,APPROCH_DATE,SALES_ID,ALLOTED_SALES_DATE,RESELLER_ID,ALLOTED_RESALLER_DATE,STATUS,REMARK_CODE)
                                                VALUES(  @ENQUIRY_ID, @COMPANY_NAME, @COUNTRY_CODE, @TYPE_OF_COMPANY,@EXPECTED_USERS,@EXPECTED_WF,@CONTACT_PERSON,@CONTACT_NUMBER,
                                            @CONTACT_EMAIL,@ENQUIRY_DATETIME,@SALES_MNGR_ID,@APPROCH_DATE,@SALES_ID,@ALLOTED_SALES_DATE,@RESELLER_ID,@ALLOTED_RESALLER_DATE,@STATUS,@REMARK_CODE);");
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@ENQUIRY_ID", strEnquiryID);
                cmd.Parameters.AddWithValue("@COMPANY_NAME", companyName);
                cmd.Parameters.AddWithValue("@COUNTRY_CODE", country);
                cmd.Parameters.AddWithValue("@TYPE_OF_COMPANY", typeofCompany);
                cmd.Parameters.AddWithValue("@EXPECTED_USERS", expectedUsers);
                cmd.Parameters.AddWithValue("@EXPECTED_WF", expectedWF);
                cmd.Parameters.AddWithValue("@CONTACT_PERSON", contactPerson);
                cmd.Parameters.AddWithValue("@CONTACT_NUMBER", contactNo);
                cmd.Parameters.AddWithValue("@CONTACT_EMAIL", contactEmail);
                cmd.Parameters.AddWithValue("@ENQUIRY_DATETIME", DateTime.Now.Ticks);
                cmd.Parameters.AddWithValue("@SALES_MNGR_ID", "");
                cmd.Parameters.AddWithValue("@APPROCH_DATE", 0);
                cmd.Parameters.AddWithValue("@SALES_ID", "");
                cmd.Parameters.AddWithValue("@ALLOTED_SALES_DATE", 0);
                cmd.Parameters.AddWithValue("@RESELLER_ID", "");
                cmd.Parameters.AddWithValue("@ALLOTED_RESALLER_DATE", 0);
                cmd.Parameters.AddWithValue("@STATUS", "");
                cmd.Parameters.AddWithValue("@REMARK_CODE", "");

                if (Utilities.ExecuteNonQueryRecord(cmd) < 0)
                {
                    statusCode = 0;
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                }
                else
                {
                    statusCode = 1;
                }
            }
            catch (Exception ex)
            {
                if (Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                    throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }

        public int StatusCode
        {
            get
            {
                return statusCode;
            }
        }
    }
}
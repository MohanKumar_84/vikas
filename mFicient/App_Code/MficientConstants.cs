﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicient
{
    public enum FUNCTION_CODES
    {
        COMPANY_REGISTRATION = 11005,
        COUNTRY_LIST = 11006,
        TIMEZONE_LIST = 11007,
    }
    public enum DIALOG_TYPE
    {
        None,
        Error,
        Warning,
        Info
    }
    public enum MODAL_POP_UP_NAME
    {
        //getModalPopUpWidth is used to return the width of the pop up
        USER_DETAIL,
        DEVICE_DETAILS_REPEATER,
        SUB_ADMIN_DETAILS,
        MANAGE_GROUP_POPUPS,
        POST_BACK_CONFIRMATION,
    }
    public class MficientConstants
    {
        internal const string MFICIENT_WS_URL = @"https://brahma.mficient.net";
    }
}
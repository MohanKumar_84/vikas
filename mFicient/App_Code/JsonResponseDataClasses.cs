﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicient
{
    public class JsonResponseDataClasses
    {
    }
    public class JsonRespGetAllTimezonesData
    {
        /// <summary>
        /// Timezone List
        /// </summary>
        [DataMember]
        public List<JsonRespTimezoneInformation> tzs { get; set; }
    }
    public class JsonRespTimezoneInformation
    {
        [DataMember]
        public string tzid { get; set; }

        [DataMember]
        public string tztxt { get; set; }
    }

    public class JsonRespGetCountryCodesData
    {
        /// <summary>
        /// Country list
        /// </summary>
        [DataMember]
        public List<JsonRespCountryList> cntrys { get; set; }
    }
    public class JsonRespCountryList
    {
        /// <summary>
        /// Country text
        /// </summary>
        [DataMember]
        public string cntry { get; set; }

        /// <summary>
        /// Country Code
        /// </summary>
        [DataMember]
        public string ccd { get; set; }
    }

    public class JsonRespCompanyRegistrationData
    {
    }
}
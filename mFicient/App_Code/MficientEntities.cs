﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicient
{
    public class MficientEntities
    {
    }
    public class MFECompanyAdmin
    {
        string _adminId, _emailId, _password, _firstname;

        string _lastName, _streetAddress1;

        string _streetAddress2, _cityName, _stateName;

        string _countryCode, _zip;

        long _dob, _registrationDatetime;

        string _gender, _middleName, _streetAddress3;

        string _mobileNo;

        bool _isTrial;

        string _resellerId;

        #region Public Properties
        public string Firstname
        {
            get { return _firstname; }
            set { _firstname = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public string EmailId
        {
            get { return _emailId; }
            set { _emailId = value; }
        }

        public string AdminId
        {
            get { return _adminId; }
            set { _adminId = value; }
        }

        public string StreetAddress1
        {
            get { return _streetAddress1; }
            set { _streetAddress1 = value; }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public string StateName
        {
            get { return _stateName; }
            set { _stateName = value; }
        }

        public string CityName
        {
            get { return _cityName; }
            set { _cityName = value; }
        }

        public string StreetAddress2
        {
            get { return _streetAddress2; }
            set { _streetAddress2 = value; }
        }
        public string Zip
        {
            get { return _zip; }
            set { _zip = value; }
        }

        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }

        public long RegistrationDatetime
        {
            get { return _registrationDatetime; }
            set { _registrationDatetime = value; }
        }

        public long Dob
        {
            get { return _dob; }
            set { _dob = value; }
        }

        public string StreetAddress3
        {
            get { return _streetAddress3; }
            set { _streetAddress3 = value; }
        }

        public string MiddleName
        {
            get { return _middleName; }
            set { _middleName = value; }
        }

        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }
        public string MobileNo
        {
            get { return _mobileNo; }
            set { _mobileNo = value; }
        }
        public bool IsTrial
        {
            get { return _isTrial; }
            set { _isTrial = value; }
        }
        public string ResellerId
        {
            get { return _resellerId; }
            set { _resellerId = value; }
        }
        #endregion
    }
    public class MFECompanyInfo
    {
        string _companyId, _companyName, _registerationNo;
        string _streetAddress1, _streetAddress2, _streetAddress3;
        string _cityName, _stateName, _countryCode, _zip;
        string _adminId, _logoImageName, _supportEmail;
        string _supportContact, _resellerId, _enquiryId;
        long _updatedOn;
        string _timezoneId;
        bool SalesMangerApproved;
        long _salesManagerApprovedDate;

        #region Public Properties
        public string RegisterationNo
        {
            get { return _registerationNo; }
            set { _registerationNo = value; }
        }

        public string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        public string CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }


        public string StreetAddress3
        {
            get { return _streetAddress3; }
            set { _streetAddress3 = value; }
        }

        public string StreetAddress2
        {
            get { return _streetAddress2; }
            set { _streetAddress2 = value; }
        }

        public string StreetAddress1
        {
            get { return _streetAddress1; }
            set { _streetAddress1 = value; }
        }


        public string Zip
        {
            get { return _zip; }
            set { _zip = value; }
        }

        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }

        public string StateName
        {
            get { return _stateName; }
            set { _stateName = value; }
        }

        public string CityName
        {
            get { return _cityName; }
            set { _cityName = value; }
        }


        public string SupportEmail
        {
            get { return _supportEmail; }
            set { _supportEmail = value; }
        }

        public string LogoImageName
        {
            get { return _logoImageName; }
            set { _logoImageName = value; }
        }

        public string AdminId
        {
            get { return _adminId; }
            set { _adminId = value; }
        }


        public string EnquiryId
        {
            get { return _enquiryId; }
            set { _enquiryId = value; }
        }

        public string ResellerId
        {
            get { return _resellerId; }
            set { _resellerId = value; }
        }

        public string SupportContact
        {
            get { return _supportContact; }
            set { _supportContact = value; }
        }


        public long UpdatedOn
        {
            get { return _updatedOn; }
            set { _updatedOn = value; }
        }


        public bool SalesMangerApproved1
        {
            get { return SalesMangerApproved; }
            set { SalesMangerApproved = value; }
        }


        public long SalesManagerApprovedDate
        {
            get { return _salesManagerApprovedDate; }
            set { _salesManagerApprovedDate = value; }
        }


        public string TimezoneId
        {
            get { return _timezoneId; }
            set { _timezoneId = value; }
        }
        #endregion
    }
    public class MFECurrentPlan
    {
        string _companyId, _planCode,
            _chargeType, _nextMnthPlan;

        int _maxWorkFlow, _maxUser, _pushMsgPerDay,
            _pushMsgPerMonth, _balanceAmnt;

        sbyte _feature3, _feature4, _feature5,
            _feature6, _feature7, _feature8, _feature9,
            _feature10;

        double _userChargePerMonth, _validity;

        long _purchaseDate, _planChangeDate,
                    _nextMnthPlanLastUpdated;

        #region Public Properties
        public string NextMnthPlan
        {
            get { return _nextMnthPlan; }
            set { _nextMnthPlan = value; }
        }

        public string ChargeType
        {
            get { return _chargeType; }
            set { _chargeType = value; }
        }

        public string PlanCode
        {
            get { return _planCode; }
            set { _planCode = value; }
        }

        public string CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }

        public int BalanceAmnt
        {
            get { return _balanceAmnt; }
            set { _balanceAmnt = value; }
        }

        public int PushMsgPerMonth
        {
            get { return _pushMsgPerMonth; }
            set { _pushMsgPerMonth = value; }
        }

        public int PushMsgPerDay
        {
            get { return _pushMsgPerDay; }
            set { _pushMsgPerDay = value; }
        }

        public int MaxUser
        {
            get { return _maxUser; }
            set { _maxUser = value; }
        }

        public int MaxWorkFlow
        {
            get { return _maxWorkFlow; }
            set { _maxWorkFlow = value; }
        }

        public sbyte Feature10
        {
            get { return _feature10; }
            set { _feature10 = value; }
        }

        public sbyte Feature9
        {
            get { return _feature9; }
            set { _feature9 = value; }
        }

        public sbyte Feature8
        {
            get { return _feature8; }
            set { _feature8 = value; }
        }

        public sbyte Feature7
        {
            get { return _feature7; }
            set { _feature7 = value; }
        }

        public sbyte Feature6
        {
            get { return _feature6; }
            set { _feature6 = value; }
        }

        public sbyte Feature5
        {
            get { return _feature5; }
            set { _feature5 = value; }
        }

        public sbyte Feature4
        {
            get { return _feature4; }
            set { _feature4 = value; }
        }

        public sbyte Feature3
        {
            get { return _feature3; }
            set { _feature3 = value; }
        }

        public double Validity
        {
            get { return _validity; }
            set { _validity = value; }
        }

        public double UserChargePerMonth
        {
            get { return _userChargePerMonth; }
            set { _userChargePerMonth = value; }
        }

        public long NextMnthPlanLastUpdated
        {
            get { return _nextMnthPlanLastUpdated; }
            set { _nextMnthPlanLastUpdated = value; }
        }

        public long PlanChangeDate
        {
            get { return _planChangeDate; }
            set { _planChangeDate = value; }
        }

        public long PurchaseDate
        {
            get { return _purchaseDate; }
            set { _purchaseDate = value; }
        }
        #endregion
    }
    public class MFEMobileUser
    {
        string _userId, _emailId, _password,
            _firstName, _lastName, _mobile,
            _companyId, _subAdminId, _locationId,
            _designationId, _employeeNo,
            _username, _regionId, _requestedBy, _domainId;

        long _dob, _registrationDatetime, _updatedOn;

        bool _isActive, _isBlocked, _allowMessenger,
            _isOfflineWorkAllowed,
            _desktopMessenger;


        #region Public Properties
        public string Mobile
        {
            get { return _mobile; }
            set { _mobile = value; }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public string EmailId
        {
            get { return _emailId; }
            set { _emailId = value; }
        }

        public string UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }
        public string DomainId
        {
            get { return _domainId; }
            set { _domainId = value; }
        }

        public string RequestedBy
        {
            get { return _requestedBy; }
            set { _requestedBy = value; }
        }

        public string RegionId
        {
            get { return _regionId; }
            set { _regionId = value; }
        }

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        public string EmployeeNo
        {
            get { return _employeeNo; }
            set { _employeeNo = value; }
        }

        public string DesignationId
        {
            get { return _designationId; }
            set { _designationId = value; }
        }

        public string LocationId
        {
            get { return _locationId; }
            set { _locationId = value; }
        }

        public string SubAdminId
        {
            get { return _subAdminId; }
            set { _subAdminId = value; }
        }

        public string CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }
        public long UpdatedOn
        {
            get { return _updatedOn; }
            set { _updatedOn = value; }
        }

        public long RegistrationDatetime
        {
            get { return _registrationDatetime; }
            set { _registrationDatetime = value; }
        }

        public long Dob
        {
            get { return _dob; }
            set { _dob = value; }
        }

        public bool DesktopMessenger
        {
            get { return _desktopMessenger; }
            set { _desktopMessenger = value; }
        }

        public bool IsOfflineWorkAllowed
        {
            get { return _isOfflineWorkAllowed; }
            set { _isOfflineWorkAllowed = value; }
        }

        public bool AllowMessenger
        {
            get { return _allowMessenger; }
            set { _allowMessenger = value; }
        }

        public bool IsBlocked
        {
            get { return _isBlocked; }
            set { _isBlocked = value; }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        #endregion
    }
    public class MFEDepartment
    {
        string _companyId, _departmentId,
            _divisionId, _deparmentName,
            _departmentCode;

        public string DepartmentCode
        {
            get { return _departmentCode; }
            set { _departmentCode = value; }
        }

        public string DeparmentName
        {
            get { return _deparmentName; }
            set { _deparmentName = value; }
        }

        public string DivisionId
        {
            get { return _divisionId; }
            set { _divisionId = value; }
        }

        public string DepartmentId
        {
            get { return _departmentId; }
            set { _departmentId = value; }
        }

        public string CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }
    }
    public class MFEDivision
    {
        string _companyId, _divisionId,
            _adminId, _divisionName,
            _divisionCode;

        public string DivisionCode
        {
            get { return _divisionCode; }
            set { _divisionCode = value; }
        }

        public string DivisionName
        {
            get { return _divisionName; }
            set { _divisionName = value; }
        }

        public string DivisionId
        {
            get { return _divisionId; }
            set { _divisionId = value; }
        }

        public string AdminId
        {
            get { return _adminId; }
            set { _adminId = value; }
        }

        public string CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }
    }
    public class MFEDevice
    {
        string _userId, _companyId,
            _deviceType, _deviceId,
            _subAdminId, _deviceModel,
            _mficientKey,
            _OsVersion, _devicePushMsgId;
        long _registrationDate;
        int _deviceSize;

        public int DeviceSize
        {
            get { return _deviceSize; }
            set { _deviceSize = value; }
        }
        public long RegistrationDate
        {
            get { return _registrationDate; }
            set { _registrationDate = value; }
        }
        public string DevicePushMsgId
        {
            get { return _devicePushMsgId; }
            set { _devicePushMsgId = value; }
        }

        public string OsVersion
        {
            get { return _OsVersion; }
            set { _OsVersion = value; }
        }

        public string MficientKey
        {
            get { return _mficientKey; }
            set { _mficientKey = value; }
        }

        public string DeviceModel
        {
            get { return _deviceModel; }
            set { _deviceModel = value; }
        }

        public string SubAdminId
        {
            get { return _subAdminId; }
            set { _subAdminId = value; }
        }

        public string DeviceId
        {
            get { return _deviceId; }
            set { _deviceId = value; }
        }

        public string DeviceType
        {
            get { return _deviceType; }
            set { _deviceType = value; }
        }

        public string CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }

        public string UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }


    }
    public class MFEUserDesignation
    {
        string _companyId, _designationId,
            _adminId, _designationName,
            _designationCode;
        long _seniorityLevel;

        public string DesignationCode
        {
            get { return _designationCode; }
            set { _designationCode = value; }
        }

        public string DesignationName
        {
            get { return _designationName; }
            set { _designationName = value; }
        }

        public string AdminId
        {
            get { return _adminId; }
            set { _adminId = value; }
        }

        public string DesignationId
        {
            get { return _designationId; }
            set { _designationId = value; }
        }

        public string CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }


        public long SeniorityLevel
        {
            get { return _seniorityLevel; }
            set { _seniorityLevel = value; }
        }
    }

    public class MFEPlanDetailCommon
    {
        string _planCode, _planId, _planName;
        int _maxWorkFlow, _pushMessagePerDay;
        int _pushMessagePerMonth;


        #region Public Properties
        public string PlanName
        {
            get { return _planName; }
            set { _planName = value; }
        }

        public string PlanId
        {
            get { return _planId; }
            set { _planId = value; }
        }
        public string PlanCode
        {
            get { return _planCode; }
            set { _planCode = value; }
        }
        public int PushMessagePerDay
        {
            get { return _pushMessagePerDay; }
            set { _pushMessagePerDay = value; }
        }
        public int MaxWorkFlow
        {
            get { return _maxWorkFlow; }
            set { _maxWorkFlow = value; }
        }
        public int PushMessagePerMonth
        {
            get { return _pushMessagePerMonth; }
            set { _pushMessagePerMonth = value; }
        }
        #endregion
    }
    public class MFEPaidPlanDetail : MFEPlanDetailCommon
    {
        int _minUsers;
        bool _isEnabled;
        public int MinUsers
        {
            get { return _minUsers; }
            set { _minUsers = value; }
        }
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { _isEnabled = value; }
        }
    }
    public class MFETrialPlanDetail : MFEPlanDetailCommon
    {
        int _maxUsers;
        bool _isDefault;
        int _validMonths;

        public int ValidMonths
        {
            get { return _validMonths; }
            set { _validMonths = value; }
        }
        public int MaxUsers
        {
            get { return _maxUsers; }
            set { _maxUsers = value; }
        }
        public bool IsDefault
        {
            get { return _isDefault; }
            set { _isDefault = value; }
        }
    }

    public class MFEmficientServer
    {
        string _serverId, _serverName,
            _serverURL, _serverIpAddress;
        bool _isEnabled, _isDefault;
        #region Public Property
        public string ServerIpAddress
        {
            get { return _serverIpAddress; }
            set { _serverIpAddress = value; }
        }

        public string ServerURL
        {
            get { return _serverURL; }
            set { _serverURL = value; }
        }

        public string ServerName
        {
            get { return _serverName; }
            set { _serverName = value; }
        }

        public string ServerId
        {
            get { return _serverId; }
            set { _serverId = value; }
        }
        public bool IsDefault
        {
            get { return _isDefault; }
            set { _isDefault = value; }
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { _isEnabled = value; }
        }
        #endregion
    }
    public class MFEmPluginServer : MFEmficientServer
    {
        string _serverPort;
        #region Public Property
        public string ServerPort
        {
            get { return _serverPort; }
            set { _serverPort = value; }
        }
        #endregion
    }
    public class MFEmBuzzServer : MFEmficientServer
    {
        string _serverPort;
        #region Public Property
        public string ServerPort
        {
            get { return _serverPort; }
            set { _serverPort = value; }
        }
        #endregion
    }
}
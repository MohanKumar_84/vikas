﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicient
{
    public static class GetRequestJson
    {
        private static string getFinalReqJson(string jsonWithDetails)
        {
            string strFinalJson = "{\"req\":" + jsonWithDetails + "}";
            return strFinalJson;
        }
        private static string generateRequestId(string uniqueString)
        { 
            Random newRandom = new Random();
           return Utilities.GetMd5Hash(DateTime.UtcNow.Ticks.ToString() + newRandom.ToString() + uniqueString).Substring(0, 16);
        }
        public static string RegisterCompanyReq(MFECompanyInfo companyInfo, MFECompanyAdmin admin)
        {
            JsonReqCompanyRegistration objCmpRegistrationReq = new JsonReqCompanyRegistration();
            objCmpRegistrationReq.func = Convert.ToString((int)FUNCTION_CODES.COMPANY_REGISTRATION);
            objCmpRegistrationReq.rid = generateRequestId(admin.Firstname+companyInfo.CompanyId);

            JsonReqCompanyRegistrationData objCmpRegistrationData =
                new JsonReqCompanyRegistrationData();

            objCmpRegistrationData.fnm = admin.Firstname;
            objCmpRegistrationData.mnm = admin.MiddleName;
            objCmpRegistrationData.lnm = admin.LastName;
            objCmpRegistrationData.pwd = admin.Password;
            objCmpRegistrationData.mob = admin.MobileNo;
            objCmpRegistrationData.em = admin.EmailId;
            objCmpRegistrationData.enid = companyInfo.CompanyId;
            objCmpRegistrationData.ennm = companyInfo.CompanyName;
            objCmpRegistrationData.add1 = companyInfo.StreetAddress1;
            objCmpRegistrationData.add2 = String.IsNullOrEmpty(companyInfo.StreetAddress2) ? String.Empty : companyInfo.StreetAddress2;
            objCmpRegistrationData.add3 = String.IsNullOrEmpty(companyInfo.StreetAddress3) ? String.Empty : companyInfo.StreetAddress3;
            objCmpRegistrationData.city = companyInfo.CityName;
            objCmpRegistrationData.state = companyInfo.StateName;
            objCmpRegistrationData.cntry = companyInfo.CountryCode;
            objCmpRegistrationData.tzn = companyInfo.TimezoneId;
            objCmpRegistrationData.zip = companyInfo.Zip;

            objCmpRegistrationReq.data = objCmpRegistrationData;
            string strJsonWithDetails = Utilities.SerializeJson<JsonReqCompanyRegistration>(objCmpRegistrationReq);
            return getFinalReqJson(strJsonWithDetails);
        }
        public static string GetCountryCodes()
        {
            JsonReqGetCountryCodes objGetCountryCodeReq = new JsonReqGetCountryCodes();
            objGetCountryCodeReq.func = Convert.ToString((int)FUNCTION_CODES.COUNTRY_LIST);
            objGetCountryCodeReq.rid = generateRequestId(String.Empty);
            JsonReqGetCountryCodesData objGetCountryCodeData = new JsonReqGetCountryCodesData();
            objGetCountryCodeData.enid = String.Empty;
            objGetCountryCodeReq.data = objGetCountryCodeData;
            string strJsonWithDetails = Utilities.SerializeJson<JsonReqGetCountryCodes>(objGetCountryCodeReq);
            return getFinalReqJson(strJsonWithDetails);
        }
        public static string GetTimezones()
        {
            JsonReqGetTimezoneList objGetTimezoneReq = new JsonReqGetTimezoneList();
            objGetTimezoneReq.func = Convert.ToString((int)FUNCTION_CODES.TIMEZONE_LIST);
            objGetTimezoneReq.rid = generateRequestId(String.Empty);
            JsonReqGetTimezoneListData objGetTZData = new JsonReqGetTimezoneListData();
            objGetTZData.enid = String.Empty;
            objGetTimezoneReq.data = objGetTZData;
            string strJsonWithDetails = Utilities.SerializeJson<JsonReqGetTimezoneList>(objGetTimezoneReq);
            return getFinalReqJson(strJsonWithDetails);
        }
    }
}
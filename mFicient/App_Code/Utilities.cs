﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Globalization;

namespace mFicient
{
    public enum DATABASE_ERRORS//9000000
    {
        DATABASE_CONNECTION_ERROR = 9000001,
        RECORD_NOT_FOUND_ERROR = 9000002,
        RECORD_INSERT_ERROR = 9000003,
        RECORD_DELETE_ERROR = 9000004,
        USER_ISNOT_ADMIN = 9000005,
    }
    public class Utilities
    {
        public static string SerializeJson<T>(object obj)
        {
            T objResponse = (T)obj;
            MemoryStream stream = new MemoryStream();
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(objResponse.GetType());
            serializer.WriteObject(stream, objResponse);
            stream.Position = 0;
            StreamReader streamReader = new StreamReader(stream);
            return streamReader.ReadToEnd();
        }

        public static T DeserialiseJson<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer serialiser = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serialiser.ReadObject(ms);
            ms.Close();
            return obj;
        }

        public static bool isRespFromMficientWSWithoutError(ResponseStatus respStatus)
        {
            if (respStatus.cd == "0") return true;
            else return false;
        }

        /// <summary>

        /// Description :Select Data According To SqlCommand ( Sql Query And Parameter )
        /// Created By: Shalini Dwivedi
        /// Created On :6/06/2012

        /// </summary>
        public static DataSet SelectDataFromSQlCommand(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;

            SqlConnectionOpen(out objSqlConnection);
            _SqlCommand.Connection = objSqlConnection;

            DataSet ObjDataSet = new DataSet();

            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

            objSqlDataAdapter.Fill(ObjDataSet);

            SqlConnectionClose(objSqlConnection);

            return ObjDataSet;
        }

        public static void SqlConnectionOpen(out SqlConnection Conn)
        {
            try
            {
                Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }

        public static void BindCountryDropDown(DropDownList _ddlDropdownId)
        {
            CountryList countryList = new CountryList();
            countryList.Process();
            DataTable dt = countryList.ResultTable;
            _ddlDropdownId.DataSource = dt;
            _ddlDropdownId.DataValueField = "COUNTRY_CODE";
            _ddlDropdownId.DataTextField = "COUNTRY_NAME";
            _ddlDropdownId.DataBind();
            _ddlDropdownId.Items.Insert(0, new ListItem("Select Country", "-1"));

        }

        public static void SqlConnectionClose(SqlConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open))
                {
                    Conn.Close();
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Description :Data Process (Insert, Update, Delete ) According To SqlCommand ( Sql Query And Parameter )
        /// Created By: Shalini Dwivedi
        /// Created On :6/06/2012
        /// </summary>
        public static int ExecuteNonQueryRecord(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            SqlConnectionOpen(out objSqlConnection);
            _SqlCommand.Connection = objSqlConnection;

            int i = _SqlCommand.ExecuteNonQuery();

            SqlConnectionClose(objSqlConnection);

            return i;
        }

        public static string GetMd5Hash(string _inputString)
        {
            string strMD5;
            byte[] hashedDataBytes;
            try
            {
                byte[] bs = Encoding.UTF8.GetBytes(_inputString);
                System.Security.Cryptography.MD5CryptoServiceProvider md5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
                hashedDataBytes = md5Hasher.ComputeHash(bs);
                strMD5 = BitConverter.ToString(hashedDataBytes).Replace("-", "");
            }
            catch
            {
                strMD5 = string.Empty;
            }
            return strMD5;
        }

        public static bool IsValidEmail(string _email)
        {
            if (!string.IsNullOrEmpty(_email))
            {
                string strRegex = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                                    + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				                [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                                    + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				                [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                    + @"([a-zA-Z\-]+\.)+[a-zA-Z]{2,4})$";

                Regex re = new Regex(strRegex);
                if (re.IsMatch(_email))
                    return (true);
                else
                    return (false);
            }
            else
                return false;
        }

        public static bool IsValidString(string _InputString, bool _a_z, bool _A_Z, bool _0_9, string _SpecialChars, int _MinLength, int _MaxLength, bool _StartSpaceAllowed, bool _EndSpaceAllowed)
        {
            try
            {
                if ((!_StartSpaceAllowed && _InputString.StartsWith(" ")) || (!_EndSpaceAllowed && _InputString.EndsWith(" ")))
                {
                    return false;
                }

                if ((_MaxLength < _MinLength) || (_MinLength >= 0 && _InputString.Length < _MinLength) || (_MaxLength >= 0 && _InputString.Length > _MaxLength))
                {
                    return false;
                }

                /* Create a regex expression based on _a-z and _A_Z and _0_9 and DisallowedCharacters */

                if (string.IsNullOrEmpty(_InputString))
                {
                    return true;
                }

                string strRegex = "";

                if (_a_z)
                {
                    strRegex += "a-z";
                }

                if (_A_Z)
                {
                    strRegex += "A-Z";
                }

                if (_0_9)
                {
                    strRegex += "0-9";
                }

                string strSpecialCharacters = _SpecialChars.Replace(@"\", @"\\");

                if (strSpecialCharacters.Contains("]"))
                {
                    strRegex = "]" + strRegex;
                    strSpecialCharacters = strSpecialCharacters.Replace("]", "");
                }

                if (strSpecialCharacters.Contains("^"))
                {
                    strRegex += "^";
                    strSpecialCharacters = strSpecialCharacters.Replace("^", "");
                }

                if (strSpecialCharacters.Contains("-"))
                {
                    strRegex = "^[" + strRegex + strSpecialCharacters.Replace("-", "") + "-" + "]+$";
                }
                else
                {
                    strRegex = "^[" + strRegex + strSpecialCharacters + "]+$";
                }

                Regex re = new Regex(strRegex);
                if (re.IsMatch(_InputString))
                {
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        #region Input Uniform After PostBack
        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="key"></param>
        /// <param name="isPostBack"></param>
        /// <param name="otherScripts">pass semicolon separated scripts</param>
        public static void makeInputFieldsUniform(System.Web.UI.Page page, string key, bool isPostBack, string otherScripts)
        {
            if (isPostBack)
            {
                ScriptManager.RegisterStartupScript(page, typeof(Page), key, "$(\"div:not('.selector')>select\").uniform();$(\"input:not(.checker input,.uploader input)\").uniform();" + otherScripts, true);
            }
            else
            {
                page.ClientScript.RegisterStartupScript(page.GetType(), key, "$(\"div:not('.selector')>select\").uniform();$(\"input:not(.checker input,.uploader input)\").uniform();" + otherScripts, true);
            }
        }

        #endregion
        
        #region ModelpopUps Functions
        
        public static void showMessage(string message, System.Web.UI.UpdatePanel updPanel, DIALOG_TYPE dialogType)
        {
            if (updPanel == null) return;
            string showDialogImageType = "";
            string strOptionForScript = "";
            switch (dialogType)
            {
                case DIALOG_TYPE.Error:
                    showDialogImageType = "DialogType.Error";
                    strOptionForScript = "$.alert.Error";
                    break;
                case DIALOG_TYPE.Info:
                    showDialogImageType = "DialogType.Info";
                    strOptionForScript = "$.alert.Information";
                    break;
            }
            ScriptManager.RegisterStartupScript(updPanel, typeof(UpdatePanel), "Message", @"$.alert(" + "'" + message + "'" + "," + strOptionForScript + ");showDialogImage(" + showDialogImageType + ");", true);
        }
        
        #endregion

        #region Http Call
        internal static string callHttpWebService(string webServiceURLWithQueryString
             , HTTP.EnumHttpMethod httpType,
             out HttpStatusCode statusCode)
        {
            HTTP oHttp = new HTTP(webServiceURLWithQueryString);

            oHttp.HttpRequestMethod = httpType;

            HttpResponseStatus oResponse = oHttp.Request();
            statusCode = oResponse.StatusCode;
            if (statusCode == HttpStatusCode.OK)
            {
                return oResponse.ResponseText;
            }
            else
            {
                return oResponse.ErrorMessage; ;
            }

        }
        public static string UrlEncode(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return HttpUtility.UrlEncode(s).Replace(@"'", @"%27");
        }
        public static string getCompleteUrlWithQueryString(
            string webServiceUrl, string queryString)
        {
            return webServiceUrl + "/?d=" + Utilities.UrlEncode(queryString);
        }
        #endregion
    }




}
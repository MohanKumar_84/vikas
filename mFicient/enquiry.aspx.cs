﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mFicient
{
    public partial class enquiry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
                Utilities.BindCountryDropDown(ddlCountry);
            txtExpUsers.Attributes.Add("onkeyPress", "return allownumbers(event)");
            txtExpWF.Attributes.Add("onkeyPress", "return allownumbers(event)");
            txtContactNo.Attributes.Add("onkeyPress", "return allownumbers(event)");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!Utilities.IsValidString(txtCompName.Text, true, true, false, "-., ", 2, 200, false, false))
            {
                lblMsg.Text = "Please enter correct company name." + "<br />";
                return;
            }

            if (ddlCountry.SelectedValue == "-1")
            {

                lblMsg.Text = "Please select country." + "<br />";
                return;
            }

            if (!Utilities.IsValidString(txtCompType.Text, true, true, false, "-, .", 2, 50, false, false))
            {
                lblMsg.Text = "Please enter correct company type." + "<br />";
                return;
            }

            if (!Utilities.IsValidString(txtExpUsers.Text, false, false, true, "", 1, 100, false, false))
            {
                lblMsg.Text = "Please enter correct expected users." + "<br />";
                return;
            }

            if (!Utilities.IsValidString(txtExpWF.Text, false, false, true, "", 1, 100, false, false))
            {
                lblMsg.Text = "Please enter correct expected WF." + "<br />";
                return;
            }

            if (!Utilities.IsValidString(txtContactPerson.Text.Trim(), true, true, false, "-. ", 1, 100, false, false))
            {
                lblMsg.Text = "Please enter correct conatct person name." + "<br />";
                return;
            }
            if (!Utilities.IsValidString(txtContactNo.Text, false, false, true, "", 4, 20, false, false))
            {
                lblMsg.Text = "Please enter correct contact no." + "<br />";
                return;
            }

            if (!Utilities.IsValidEmail(txtContactEmail.Text))
            {
                lblMsg.Text = "Please enter correct email." + "<br />";
                return;
            }

            if (String.IsNullOrEmpty(lblMsg.Text))
            {
                AddEnquiry addEnquiry = new AddEnquiry(txtCompName.Text, ddlCountry.SelectedValue, txtCompType.Text, Convert.ToInt32(txtExpUsers.Text), Convert.ToInt32(txtExpWF.Text), txtContactPerson.Text, txtContactNo.Text, txtContactEmail.Text);
                if (addEnquiry.StatusCode > 0)
                {
                    txtCompName.Text = "";
                    txtCompType.Text = "";
                    txtContactEmail.Text = "";
                    txtContactNo.Text = "";
                    txtContactPerson.Text = "";
                    txtExpUsers.Text = "";
                    txtExpWF.Text = "";
                    ddlCountry.SelectedIndex = -1;
                }
            }
        }
    }
}
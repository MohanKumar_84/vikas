﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
namespace mFicient
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                bindTimezoneDropDown();
                bindCountryCodeDropDown();
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesign", false,
                    "");
            }
            else
            {
                Utilities.makeInputFieldsUniform(this.Page, "SetUniformDesignAfterPostback", true, "");
                fillPasswordOnPostBack();
            }

        }
        void fillPasswordOnPostBack()
        {
            ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "FillPasswordAfterPostBack", "fillPasswordEntered();", true);
        }
        void bindTimezoneDropDown()
        {
            ddlTimezone.Items.Clear();
            System.Net.HttpStatusCode statusCode;
            string strResponse = getTimezonesUsingWS(out statusCode);
            DataTable dtblTzs = new DataTable();
            dtblTzs.Columns.Add("TZ_ID", typeof(string));
            dtblTzs.Columns.Add("TZ_TEXT", typeof(string));
            if (statusCode == System.Net.HttpStatusCode.OK)
            {
                dtblTzs = getTzFromWsResponse(strResponse);
            }
            ddlTimezone.DataSource = dtblTzs;
            ddlTimezone.DataTextField = "TZ_TEXT";
            ddlTimezone.DataValueField = "TZ_ID";
            ddlTimezone.DataBind();
            ddlTimezone.Items.Insert(0, new ListItem("Select", "-1"));
        }
        string getTimezonesUsingWS(out System.Net.HttpStatusCode statusCode)
        {
            string strRequest = GetRequestJson.GetTimezones();
            statusCode = System.Net.HttpStatusCode.BadRequest;
            string strWSQueryString = Utilities.getCompleteUrlWithQueryString(MficientConstants.MFICIENT_WS_URL, strRequest);
            return Utilities.callHttpWebService(
                strWSQueryString,
                HTTP.EnumHttpMethod.HTTP_GET,
                out statusCode);
        }
        DataTable getTzFromWsResponse(string response)
        {
            DataTable dtblTzs = new DataTable();
            dtblTzs.Columns.Add("TZ_ID", typeof(string));
            dtblTzs.Columns.Add("TZ_TEXT", typeof(string));
            JsonResponseParser<JsonRespGetAllTimezonesData> objRespJsonParsed =
                Utilities.DeserialiseJson<JsonResponseParser<JsonRespGetAllTimezonesData>>(response);
            if (Utilities.isRespFromMficientWSWithoutError(objRespJsonParsed.resp.status))
            {
                foreach (JsonRespTimezoneInformation tzi in objRespJsonParsed.resp.data.tzs)
                {
                    DataRow row = dtblTzs.NewRow();
                    row["TZ_ID"] = tzi.tzid;
                    row["TZ_TEXT"] = tzi.tztxt;
                    dtblTzs.Rows.Add(row);
                }
            }
            return dtblTzs;
        }
        void bindCountryCodeDropDown()
        {
            ddlCountry.Items.Clear();
            System.Net.HttpStatusCode statusCode;
            string strResponse = getCountryCodeUsingWS(out statusCode);
            DataTable dtblCountryCodes = new DataTable();
            dtblCountryCodes.Columns.Add("CountryCode_ID", typeof(string));
            dtblCountryCodes.Columns.Add("Country_TEXT", typeof(string));
            if (statusCode == System.Net.HttpStatusCode.OK)
            {
                dtblCountryCodes = getCountryCodesFromWsResponse(strResponse);
            }
            ddlCountry.DataSource = dtblCountryCodes;
            ddlCountry.DataTextField = "Country_TEXT";
            ddlCountry.DataValueField = "CountryCode_ID";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("Select", "-1"));
        }
        string getCountryCodeUsingWS(out System.Net.HttpStatusCode statusCode)
        {
            string strRequest = GetRequestJson.GetCountryCodes();
            statusCode = System.Net.HttpStatusCode.BadRequest;
            string strWSQueryString = Utilities.getCompleteUrlWithQueryString(MficientConstants.MFICIENT_WS_URL, strRequest);
            return Utilities.callHttpWebService(
                strWSQueryString,
                HTTP.EnumHttpMethod.HTTP_GET,
                out statusCode);
        }
        DataTable getCountryCodesFromWsResponse(string response)
        {
            DataTable dtblCountryCode = new DataTable();
            dtblCountryCode.Columns.Add("CountryCode_ID", typeof(string));
            dtblCountryCode.Columns.Add("Country_TEXT", typeof(string));
            JsonResponseParser<JsonRespGetCountryCodesData> objRespJsonParsed =
                Utilities.DeserialiseJson<JsonResponseParser<JsonRespGetCountryCodesData>>(response);
            if (Utilities.isRespFromMficientWSWithoutError(objRespJsonParsed.resp.status))
            {
                foreach (JsonRespCountryList cntry in objRespJsonParsed.resp.data.cntrys)
                {
                    DataRow row = dtblCountryCode.NewRow();
                    row["CountryCode_ID"] = cntry.ccd;
                    row["Country_TEXT"] = cntry.cntry;
                    dtblCountryCode.Rows.Add(row);
                }
            }
            return dtblCountryCode;
        }

        void validateReqFields()
        {
            StringBuilder sbError = new StringBuilder();
            startUnorderedListTag(sbError);
            if (String.IsNullOrEmpty(txtCompanyName.Text))
            {
                appendErrorMsg(sbError, "Please enter enterprise / organization name.");
            }
            else if (!Utilities.IsValidString(txtCompanyName.Text, true, true, true, "- .'", 1, 50, false, false))
            {
                appendErrorMsg(sbError, "Please enter correct enterprise / organization name.");
            }

            if (String.IsNullOrEmpty(txtCompanyId.Text))
            {
                appendErrorMsg(sbError, "Please enter enterprise / organization id.");
            }
            else
            {
                try
                {
                    validateCompanyId();
                }
                catch (MficientException ex)
                {
                    appendErrorMsg(sbError, ex.Message);
                }
            }

            if (String.IsNullOrEmpty(txtAdminName.Text))
            {
                appendErrorMsg(sbError, "Please enter administrator name.");
            }
            else if (!Utilities.IsValidString(txtAdminName.Text.Trim(), true, true, true, "- '", 1, 100, false, false))
            {
                appendErrorMsg(sbError, "Please enter correct administrator name.");
            }
            if (String.IsNullOrEmpty(txtAdminEmail.Text))
            {
                appendErrorMsg(sbError, "Please enter email.");
            }
            else if (!Utilities.IsValidEmail(txtAdminEmail.Text))
            {
                appendErrorMsg(sbError, "Please enter correct email.");
            }
            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                appendErrorMsg(sbError, "Please enter password.");
            }
            else if (!Utilities.IsValidString(txtPassword.Text, true, true, true, @".,#%^&*:;()[]{}?/\+=-_|~", 6, 20, false, false))
            {
                appendErrorMsg(sbError, "Please enter valid password.Min 6 characters and max 20 characters.");
            }
            if (txtPassword.Text != txtRetypePassword.Text)
            {
                appendErrorMsg(sbError, "Password entered does not match.");
            }
            if (String.IsNullOrEmpty(txtContactNo.Text))
            {
                appendErrorMsg(sbError, "Please enter a contact no.");
            }
            else
            {
                try
                {
                    validateMobileNo(txtContactNo.Text);
                }
                catch (MficientException ex)
                {
                    appendErrorMsg(sbError, ex.Message);
                }
            }
            //Company Details
            if (String.IsNullOrEmpty(txtAddress1.Text))
            {
                appendErrorMsg(sbError, "Please enter address in Street address 1.");
            }
            else if (!Utilities.IsValidString(txtAddress1.Text, true, true, true, "~!@#$%^&*(){}|:<>?,./=-][ ", 1, 50, false, false))
            {
                appendErrorMsg(sbError, "Please enter correct address in Street address 1.Min 1 character and max 50 characters.");
            }
            if (!String.IsNullOrEmpty(txtAddress2.Text))
            {
                if (!Utilities.IsValidString(txtAddress2.Text, true, true, true, "~!@#$%^&*(){}|:<>?,./=-][ ", 0, 50, false, false))
                {
                    appendErrorMsg(sbError, "Please enter correct address in Street address 2.Max 50 characters.");
                }
            }
            if (!String.IsNullOrEmpty(txtAddress3.Text))
            {
                if (!Utilities.IsValidString(txtAddress3.Text, true, true, true, "~!@#$%^&*(){}|:<>?,./=-][ ", 0, 50, false, false))
                {
                    appendErrorMsg(sbError, "Please enter correct address in Street address 3.Max 50 characters.");
                }
            }
            if (String.IsNullOrEmpty(txtCity.Text))
            {
                appendErrorMsg(sbError, "Please enter city.");
            }
            else if (!Utilities.IsValidString(txtCity.Text, true, true, true, "-.'~ ", 1, 50, false, false))
            {
                appendErrorMsg(sbError, "Please enter correct city.");
            }

            if (String.IsNullOrEmpty(txtState.Text))
            {
                appendErrorMsg(sbError, "Please enter state.");
            }
            else if (!Utilities.IsValidString(txtState.Text, true, true, true, "-.'~ ", 1, 50, false, false))
            {
                appendErrorMsg(sbError, "Please enter correct state.");
            }
            if (String.IsNullOrEmpty(txtZip.Text))
            {
                appendErrorMsg(sbError, "Please enter zip code.");
            }
            else if (!Utilities.IsValidString(txtZip.Text, true, true, true, ".-", 3, 10, false, false))
            {
                appendErrorMsg(sbError, "Please enter correct zip code.");
            }
            if (ddlCountry.Items.Count > 0)
            {
                if (ddlCountry.SelectedValue == "-1")
                {
                    appendErrorMsg(sbError, "Please select a country.");
                }
            }
            else
            {
                appendErrorMsg(sbError, "Please select a country.");
            }
            if (ddlTimezone.Items.Count > 0)
            {
                if (ddlTimezone.SelectedValue == "-1")
                {
                    appendErrorMsg(sbError, "Please select a timezone.");
                }
            }
            else
            {
                appendErrorMsg(sbError, "Please select a timezone.");
            }

            endUnorderedListTag(sbError);
            if (sbError.ToString().Contains("<li>"))
            {
                throw new MficientException(sbError.ToString());
            }
        }
        void startUnorderedListTag(StringBuilder sb)
        {
            sb.Append("<ul style=\"position:relative;right:-10px; \">");
        }
        void endUnorderedListTag(StringBuilder sb)
        {
            sb.Append("</ul>");
        }
        void appendErrorMsg(StringBuilder errorMsg, string errorToAdd)
        {
            errorMsg.Append("<li>" + errorToAdd + "</li>");
        }
        void validateCompanyId()
        {
            StringBuilder sbError = new StringBuilder();
            string strCompanyId = txtCompanyId.Text;
            if (String.IsNullOrEmpty(strCompanyId))
            {
                sbError.Append("Please enter enterprise / organization id.");
            }
            else
            {
                int iFirstAlphabetOfCompanyId;
                string strFirstAlphabetOfCmpId = strCompanyId.Substring(0, 1);
                if (Int32.TryParse(strFirstAlphabetOfCmpId, out iFirstAlphabetOfCompanyId))
                {
                    sbError.Append("Please enter valid enterprise / organization id.It should begin with an alphabet.");
                }
                else if (!Utilities.IsValidString(strCompanyId, true, true, true, "", 5, 20, false, false))
                {
                    sbError.Append("Please enter valid enterprise / organization id.Min 5 characters and Max 20 characters.");
                }
            }
            if (sbError.Length > 0)
            {
                throw new MficientException(sbError.ToString());
            }
        }
        void validateMobileNo(string mobileNo)
        {
            string strErrorMsg = "Invalid contact no.";
            if (String.IsNullOrEmpty(mobileNo)) throw new MficientException("Please enter contact no.");
            if (mobileNo.Length < 7 || mobileNo.Length > 20) throw new MficientException(strErrorMsg);
            char chrFirstChar = Convert.ToChar(mobileNo.Substring(0, 1));
            if (chrFirstChar != '+') throw new MficientException(strErrorMsg);
            char chrSecondChar = Convert.ToChar(mobileNo.Substring(1, 1));
            int iSecondChar = 0;
            try
            {
                //check if it is a number.
                iSecondChar = int.Parse(chrSecondChar.ToString());
            }
            catch
            {
                throw new MficientException(strErrorMsg);
            }
            if (iSecondChar == 0) throw new MficientException(strErrorMsg);
            string strNoFromThirdChar = mobileNo.Substring(2, mobileNo.Length - 2);
            try
            {
                ulong.Parse(strNoFromThirdChar);
            }
            catch
            {
                throw new MficientException(strErrorMsg);
            }
        }
        string getErrorMsgFromWsDesc(string message)
        {
            if (!String.IsNullOrEmpty(message))
            {
                string[] aryMessages = message.Split(',');
                StringBuilder sbErrorMsg = new StringBuilder();
                startUnorderedListTag(sbErrorMsg);
                foreach (string error in aryMessages)
                {
                    appendErrorMsg(sbErrorMsg, error);
                }
                endUnorderedListTag(sbErrorMsg);
                return sbErrorMsg.ToString();
            }
            else
            {
                return String.Empty;
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //System.Threading.Thread.Sleep(2000);
                validateReqFields();
                System.Net.HttpStatusCode statusCode;
                string strResponse = addCompanyByCallingWS(out statusCode);
                if (statusCode == System.Net.HttpStatusCode.OK)
                {
                    JsonResponseParser<JsonRespCompanyRegistrationData> objRespJsonParsed =
                    Utilities.DeserialiseJson<JsonResponseParser<JsonRespCompanyRegistrationData>>(strResponse);
                    if (!Utilities.isRespFromMficientWSWithoutError(objRespJsonParsed.resp.status))
                    {
                        throw new MficientException(getErrorMsgFromWsDesc(objRespJsonParsed.resp.status.desc));
                    }
                    else
                    {
                        clearAllControlFields();
                        Utilities.showMessage("Registration Successful!<br/><br/>An email containing further instructions has been sent to you.<br/>In case of any further queries, please contact support@mficient.com",
                            updRegisterCompany, DIALOG_TYPE.Info);
                        setValueOfHidPasswordEntered(String.Empty);
                        setValueOfHidRetypePassword(String.Empty);
                    }
                }
                else
                {
                    throw new MficientException("Error encountered while saving.Please try again.");
                }
            }
            catch (MficientException ex)
            {
                Utilities.showMessage(
                    ex.Message,
                    updRegisterCompany,
                    DIALOG_TYPE.Error);

            }
            catch
            {
                Utilities.showMessage("Internal server error.",
                    updRegisterCompany,
                    DIALOG_TYPE.Error);
            }
        }
        string addCompanyByCallingWS(out System.Net.HttpStatusCode statusCode)
        {
            string strRequest = getRequestForCompanyRegistration();
            statusCode = System.Net.HttpStatusCode.BadRequest;
            string strWSQueryString = Utilities.getCompleteUrlWithQueryString(MficientConstants.MFICIENT_WS_URL, strRequest);
            return Utilities.callHttpWebService(
               strWSQueryString,
               HTTP.EnumHttpMethod.HTTP_GET,
               out statusCode);
        }
        string getRequestForCompanyRegistration()
        {
            MFECompanyInfo objCmpIngo = getCmpInfoAfterFillingFromFormElements();
            MFECompanyAdmin objCmpAdmin = getCmpAdminAfterFillingFromFormElements();
            return GetRequestJson.RegisterCompanyReq(objCmpIngo, objCmpAdmin);
        }
        MFECompanyAdmin getCmpAdminAfterFillingFromFormElements()
        {
            MFECompanyAdmin objCmpAdmin = new MFECompanyAdmin();
            setAdminNameFromFormElement(objCmpAdmin);
            objCmpAdmin.Password = txtPassword.Text.Trim();
            objCmpAdmin.MobileNo = txtContactNo.Text.Trim();
            objCmpAdmin.EmailId = txtAdminEmail.Text.Trim();
            return objCmpAdmin;
        }
        MFECompanyInfo getCmpInfoAfterFillingFromFormElements()
        {
            MFECompanyInfo objMFECmpInfo = new MFECompanyInfo();
            objMFECmpInfo.CompanyId = txtCompanyId.Text.Trim();
            objMFECmpInfo.CompanyName = txtCompanyName.Text.Trim();
            objMFECmpInfo.StreetAddress1 = txtAddress1.Text.Trim();
            objMFECmpInfo.StreetAddress2 = txtAddress2.Text.Trim();
            objMFECmpInfo.StreetAddress3 = txtAddress3.Text.Trim();
            objMFECmpInfo.CityName = txtCity.Text.Trim();
            objMFECmpInfo.StateName = txtState.Text.Trim();
            objMFECmpInfo.CountryCode = ddlCountry.SelectedValue;
            objMFECmpInfo.TimezoneId = ddlTimezone.SelectedValue;
            objMFECmpInfo.Zip = txtZip.Text;
            return objMFECmpInfo;
        }
        /// <summary>
        /// Set the First Middle And last name in the object passed.
        /// </summary>
        /// <param name="companyAdmin"></param>
        void setAdminNameFromFormElement(MFECompanyAdmin companyAdmin)
        {
            companyAdmin.Firstname = String.Empty;
            companyAdmin.MiddleName = String.Empty;
            companyAdmin.LastName = String.Empty;
            string[] aryName = txtAdminName.Text.Trim().Split(' ');
            int iNameLengthAfterSplit = aryName.Length;
            if (iNameLengthAfterSplit == 0) throw new MficientException("Please enter a name");
            companyAdmin.Firstname = aryName[0].Length > 50 ? aryName[0].Substring(0, 50) : aryName[0];
            if (iNameLengthAfterSplit > 1)
            {
                if (iNameLengthAfterSplit == 2)
                {
                    companyAdmin.LastName = aryName[1].Length > 50 ? aryName[1].Substring(0, 50) : aryName[1];
                }
                else
                {
                    companyAdmin.LastName = aryName[iNameLengthAfterSplit - 1].Length > 50 ?
                        aryName[iNameLengthAfterSplit - 1].Substring(0, 50) : aryName[iNameLengthAfterSplit - 1];

                    for (int i = 1; i <= iNameLengthAfterSplit - 2; i++)
                    {
                        if (String.IsNullOrEmpty(companyAdmin.MiddleName))
                            companyAdmin.MiddleName += aryName[i];
                        else
                            companyAdmin.MiddleName += " " + aryName[i];
                    }
                    companyAdmin.MiddleName = companyAdmin.MiddleName.Length > 50 ?
                        companyAdmin.MiddleName.Substring(0, 50) : companyAdmin.MiddleName;
                }
            }
        }
        void clearAllControlFields()
        {
            txtCompanyName.Text = "";
            txtCompanyId.Text = "";
            txtAdminName.Text = "";
            txtAdminEmail.Text = "";
            txtPassword.Text = "";
            txtRetypePassword.Text = "";
            txtContactNo.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtAddress3.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue("-1"));
            ddlTimezone.SelectedIndex = ddlTimezone.Items.IndexOf(ddlTimezone.Items.FindByValue("-1"));
            txtZip.Text = "";
            hidPasswordEntered.Value = "";
            hidReTypePasswordEntered.Value = "";
        }
        #region Set Value of Hid fields
        void setValueOfHidPasswordEntered(string value)
        {
            hidPasswordEntered.Value = value;
        }
        void setValueOfHidRetypePassword(string value)
        {
            hidReTypePasswordEntered.Value = value;
        }
        #endregion
    }
}
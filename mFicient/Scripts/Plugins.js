﻿(function ($) {
    $.uniform = {
        options: {
            selectClass: 'selector',
            radioClass: 'radio',
            checkboxClass: 'checker',
            fileClass: 'uploader',
            filenameClass: 'filename',
            fileBtnClass: 'action',
            fileDefaultText: 'No file selected',
            fileBtnText: 'Choose File',
            checkedClass: 'checked',
            focusClass: 'focus',
            disabledClass: 'disabled',
            buttonClass: 'button',
            activeClass: 'active',
            hoverClass: 'hover',
            useID: true,
            idPrefix: 'uniform',
            resetSelector: false,
            autoHide: true
        },
        elements: []
    };

    if ($.browser.msie && $.browser.version < 7) {
        $.support.selectOpacity = false;
    } else {
        $.support.selectOpacity = true;
    }

    $.fn.uniform = function (options) {

        options = $.extend($.uniform.options, options);

        var el = this;
        //code for specifying a reset button
        if (options.resetSelector != false) {
            $(options.resetSelector).mouseup(function () {
                function resetThis() {
                    $.uniform.update(el);
                }
                setTimeout(resetThis, 10);
            });
        }

        function doInput(elem) {
            $el = $(elem);
            $el.addClass($el.attr("type"));
            storeElement(elem);
        }

        function doTextarea(elem) {
            $(elem).addClass("uniform");
            storeElement(elem);
        }

        function doButton(elem) {
            var $el = $(elem);
            if (!$el.is(":submit")) {

                var divTag = $("<div>"),
          spanTag = $("<span>");

                divTag.addClass(options.buttonClass);

                if (options.useID && $el.attr("id") != "") divTag.attr("id", options.idPrefix + "-" + $el.attr("id"));

                var btnText;

                if ($el.is("a") || $el.is("button")) {
                    btnText = $el.text();
                } else if ($el.is(":submit") || $el.is(":reset") || $el.is("input[type=button]")) {
                    btnText = $el.attr("value");
                }

                btnText = btnText == "" ? $el.is(":reset") ? "Reset" : "Submit" : btnText;

                spanTag.html(btnText);

                $el.css("opacity", 0);
                $el.wrap(divTag);
                $el.wrap(spanTag);

                //redefine variables
                divTag = $el.closest("div");
                spanTag = $el.closest("span");

                if ($el.is(":disabled")) divTag.addClass(options.disabledClass);

                divTag.bind({
                    "mouseenter.uniform": function () {
                        divTag.addClass(options.hoverClass);
                    },
                    "mouseleave.uniform": function () {
                        divTag.removeClass(options.hoverClass);
                        divTag.removeClass(options.activeClass);
                    },
                    "mousedown.uniform touchbegin.uniform": function () {
                        divTag.addClass(options.activeClass);
                    },
                    "mouseup.uniform touchend.uniform": function () {
                        divTag.removeClass(options.activeClass);
                    },
                    "click.uniform touchend.uniform": function (e) {
                        if ($(e.target).is("span") || $(e.target).is("div")) {
                            if (elem[0].dispatchEvent) {
                                var ev = document.createEvent('MouseEvents');
                                ev.initEvent('click', true, true);
                                elem[0].dispatchEvent(ev);
                            } else {
                                elem[0].click();
                            }
                        }
                    }
                });

                elem.bind({
                    "focus.uniform": function () {
                        divTag.addClass(options.focusClass);
                    },
                    "blur.uniform": function () {
                        divTag.removeClass(options.focusClass);
                    }
                });

                $.uniform.noSelect(divTag);
                storeElement(elem);
            }
        }

        function doSelect(elem) {
            var $el = $(elem);

            var divTag = $('<div />'),
        spanTag = $('<span />');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.selectClass);

            if (options.useID && elem.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
            }

            var selected = elem.find(":selected:first");
            if (selected.length == 0) {
                selected = elem.find("option:first");
            }
            spanTag.html(selected.html());

            elem.css('opacity', 0);
            elem.wrap(divTag);
            elem.before(spanTag);

            //redefine variables
            divTag = elem.parent("div");
            spanTag = elem.siblings("span");
            //5/9/2012
            //elem.width(divTag.outerWidth(false));
//            var elemWidth = 0; //9/11/2012
//            if (divTag.outerWidth(false) == 10) //width of the div tag if elem initial width is zero
//            {
//                elemWidth = divTag.outerWidth(false) + spanTag.outerWidth(false) + 25 + 2 + 10;
//                //where 25 and 2 is the padding provided to the span tag in jquery.uniform.css,10 is the padding provided to wrapperDiv in css
//                //there is an additional 10px area due to the uniform image//adding that as well to work consistently
//            } else {
//                elemWidth = spanTag.outerWidth(false) + 25 + 2 + 10; //where 25,2 is the padding provided to the span tag in jquery.uniform.css
//            }
//            elem.width(elemWidth);
            elem.bind({
                "change.uniform": function () {
                    spanTag.text(elem.find(":selected").html());
                    divTag.removeClass(options.activeClass);
                },
                "focus.uniform": function () {
                    divTag.addClass(options.focusClass);
                },
                "blur.uniform": function () {
                    divTag.removeClass(options.focusClass);
                    divTag.removeClass(options.activeClass);
                },
                "mousedown.uniform touchbegin.uniform": function () {
                    divTag.addClass(options.activeClass);
                },
                "mouseup.uniform touchend.uniform": function () {
                    divTag.removeClass(options.activeClass);
                },
                "click.uniform touchend.uniform": function () {
                    divTag.removeClass(options.activeClass);
                },
                "mouseenter.uniform": function () {
                    divTag.addClass(options.hoverClass);
                },
                "mouseleave.uniform": function () {
                    divTag.removeClass(options.hoverClass);
                    divTag.removeClass(options.activeClass);
                },
                "keyup.uniform": function () {
                    spanTag.text(elem.find(":selected").html());
                }
            });

            //handle disabled state
            if ($(elem).attr("disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }
            $.uniform.noSelect(spanTag);

            storeElement(elem);
            //added on 19/12/2012 to set th width of the drop down according to the width of the selected text
//            $(elem).change(function () {
//                setDropDownListWidthOnChange($(elem));
//            })
//            $(divTag).hover(
//            function () {
//                var elemSiblingSpan = $(elem).siblings("span");
//                if ($(elem).outerWidth() < $(elemSiblingSpan).outerWidth()) {
//                    setDropDownListWidthOnChange($(elem));
//                }
//            })

        }

        function doCheckbox(elem) {
            var $el = $(elem);
            var divTag = $('<div />'),
        spanTag = $('<span />');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.checkboxClass);

            //assign the id of the element
            if (options.useID && elem.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
            }

            //wrap with the proper elements
            $(elem).wrap(divTag);
            $(elem).wrap(spanTag);

            //redefine variables
            spanTag = elem.parent();
            divTag = spanTag.parent();

            //hide normal input and add focus classes
            $(elem)
        .css("opacity", 0)
        .bind({
            "focus.uniform": function () {
                divTag.addClass(options.focusClass);
            },
            "blur.uniform": function () {
                divTag.removeClass(options.focusClass);
            },
            "click.uniform touchend.uniform": function () {
                if (!$(elem).attr("checked")) {
                    //box was just unchecked, uncheck span
                    spanTag.removeClass(options.checkedClass);
                } else {
                    //box was just checked, check span.
                    spanTag.addClass(options.checkedClass);
                }
            },
            "mousedown.uniform touchbegin.uniform": function () {
                divTag.addClass(options.activeClass);
            },
            "mouseup.uniform touchend.uniform": function () {
                divTag.removeClass(options.activeClass);
            },
            "mouseenter.uniform": function () {
                divTag.addClass(options.hoverClass);
            },
            "mouseleave.uniform": function () {
                divTag.removeClass(options.hoverClass);
                divTag.removeClass(options.activeClass);
            }
        });

            //handle defaults
            if ($(elem).attr("checked")) {
                //box is checked by default, check our box
                spanTag.addClass(options.checkedClass);
            }

            //handle disabled state
            if ($(elem).attr("disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }

            storeElement(elem);
        }

        function doRadio(elem) {
            var $el = $(elem);

            var divTag = $('<div />'),
        spanTag = $('<span />');

            if (!$el.css("display") == "none" && options.autoHide) {
                divTag.hide();
            }

            divTag.addClass(options.radioClass);

            if (options.useID && elem.attr("id") != "") {
                divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
            }

            //wrap with the proper elements
            $(elem).wrap(divTag);
            $(elem).wrap(spanTag);

            //redefine variables
            spanTag = elem.parent();
            divTag = spanTag.parent();

            //hide normal input and add focus classes
            $(elem)
        .css("opacity", 0)
        .bind({
            "focus.uniform": function () {
                divTag.addClass(options.focusClass);
            },
            "blur.uniform": function () {
                divTag.removeClass(options.focusClass);
            },
            "click.uniform touchend.uniform": function () {
                if (!$(elem).attr("checked")) {
                    //box was just unchecked, uncheck span
                    spanTag.removeClass(options.checkedClass);
                } else {
                    //box was just checked, check span
                    var classes = options.radioClass.split(" ")[0];
                    $("." + classes + " span." + options.checkedClass + ":has([name='" + $(elem).attr('name') + "'])").removeClass(options.checkedClass);
                    spanTag.addClass(options.checkedClass);
                }
            },
            "mousedown.uniform touchend.uniform": function () {
                if (!$(elem).is(":disabled")) {
                    divTag.addClass(options.activeClass);
                }
            },
            "mouseup.uniform touchbegin.uniform": function () {
                divTag.removeClass(options.activeClass);
            },
            "mouseenter.uniform touchend.uniform": function () {
                divTag.addClass(options.hoverClass);
            },
            "mouseleave.uniform": function () {
                divTag.removeClass(options.hoverClass);
                divTag.removeClass(options.activeClass);
            }
        });

            //handle defaults
            if ($(elem).attr("checked")) {
                //box is checked by default, check span
                spanTag.addClass(options.checkedClass);
            }
            //handle disabled state
            if ($(elem).attr("disabled")) {
                //box is checked by default, check our box
                divTag.addClass(options.disabledClass);
            }

            storeElement(elem);

        }

        function doFile(elem) {
            //sanitize input
            var $el = $(elem);
            if ($el && $($el.parent()[0]).attr('class') !== "uploader") {//8/5/2013 Mohan

                var divTag = $('<div />'),
        filenameTag = $('<span>' + options.fileDefaultText + '</span>'),
        btnTag = $('<span>' + options.fileBtnText + '</span>');

                if (!$el.css("display") == "none" && options.autoHide) {
                    divTag.hide();
                }

                divTag.addClass(options.fileClass);
                filenameTag.addClass(options.filenameClass);
                btnTag.addClass(options.fileBtnClass);

                if (options.useID && $el.attr("id") != "") {
                    divTag.attr("id", options.idPrefix + "-" + $el.attr("id"));
                }

                //wrap with the proper elements
                $el.wrap(divTag);
                $el.after(btnTag);
                $el.after(filenameTag);

                //redefine variables
                divTag = $el.closest("div");
                filenameTag = $el.siblings("." + options.filenameClass);
                btnTag = $el.siblings("." + options.fileBtnClass);

                //set the size
                if (!$el.attr("size")) {
                    var divWidth = divTag.width();
                    //$el.css("width", divWidth);
                    $el.attr("size", divWidth / 10);
                }

                //actions
                var setFilename = function () {
                    var filename = $el.val();
                    if (filename === '') {
                        filename = options.fileDefaultText;
                    } else {
                        filename = filename.split(/[\/\\]+/);
                        filename = filename[(filename.length - 1)];
                    }
                    filenameTag.text(filename);
                };

                // Account for input saved across refreshes
                setFilename();

                $el.css("opacity", 0)
        .bind({
            "focus.uniform": function () {
                divTag.addClass(options.focusClass);
            },
            "blur.uniform": function () {
                divTag.removeClass(options.focusClass);
            },
            "mousedown.uniform": function () {
                if (!$(elem).is(":disabled")) {
                    divTag.addClass(options.activeClass);
                }
            },
            "mouseup.uniform": function () {
                divTag.removeClass(options.activeClass);
            },
            "mouseenter.uniform": function () {
                divTag.addClass(options.hoverClass);
            },
            "mouseleave.uniform": function () {
                divTag.removeClass(options.hoverClass);
                divTag.removeClass(options.activeClass);
            }
        });

                // IE7 doesn't fire onChange until blur or second fire.
                if ($.browser.msie) {
                    // IE considers browser chrome blocking I/O, so it
                    // suspends tiemouts until after the file has been selected.
                    $el.bind('click.uniform.ie7', function () {
                        setTimeout(setFilename, 0);
                    });
                } else {
                    // All other browsers behave properly
                    $el.bind('change.uniform', setFilename);
                }

                //handle defaults
                if ($el.attr("disabled")) {
                    //box is checked by default, check our box
                    divTag.addClass(options.disabledClass);
                }

                $.uniform.noSelect(filenameTag);
                $.uniform.noSelect(btnTag);

                storeElement(elem);
            }



        }

        $.uniform.restore = function (elem) {
            if (elem == undefined) {
                elem = $($.uniform.elements);
            }

            $(elem).each(function () {
                if ($(this).is(":checkbox")) {
                    //unwrap from span and div
                    $(this).unwrap().unwrap();
                } else if ($(this).is("select")) {
                    //remove sibling span
                    $(this).siblings("span").remove();
                    //unwrap parent div
                    $(this).unwrap();
                } else if ($(this).is(":radio")) {
                    //unwrap from span and div
                    $(this).unwrap().unwrap();
                } else if ($(this).is(":file")) {
                    //remove sibling spans
                    $(this).siblings("span").remove();
                    //unwrap parent div
                    $(this).unwrap();
                } else if ($(this).is("button, :submit, :reset, a, input[type='button']")) {
                    //unwrap from span and div
                    $(this).unwrap().unwrap();
                }

                //unbind events
                $(this).unbind(".uniform");

                //reset inline style
                $(this).css("opacity", "1");

                //remove item from list of uniformed elements
                var index = $.inArray($(elem), $.uniform.elements);
                $.uniform.elements.splice(index, 1);
            });
        };

        function storeElement(elem) {
            //store this element in our global array
            elem = $(elem).get();
            if (elem.length > 1) {
                $.each(elem, function (i, val) {
                    $.uniform.elements.push(val);
                });
            } else {
                $.uniform.elements.push(elem);
            }
        }

        //noSelect v1.0
        $.uniform.noSelect = function (elem) {
            function f() {
                return false;
            };
            $(elem).each(function () {
                this.onselectstart = this.ondragstart = f; // Webkit & IE
                $(this)
          .mousedown(f) // Webkit & Opera
        .css({
            MozUserSelect: 'none'
        }); // Firefox
            });
        };

        $.uniform.update = function (elem) {
            if (elem == undefined) {
                elem = $($.uniform.elements);
            }
            //sanitize input
            elem = $(elem);

            elem.each(function () {
                //do to each item in the selector
                //function to reset all classes
                var $e = $(this);

                if ($e.is("select")) {
                    //element is a select
                    var spanTag = $e.siblings("span");
                    var divTag = $e.parent("div");

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);

                    //reset current selected text
                    spanTag.html($e.find(":selected").html());

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }

                } else if ($e.is(":checkbox")) {
                    //element is a checkbox
                    var spanTag = $e.closest("span");
                    var divTag = $e.closest("div");

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);
                    spanTag.removeClass(options.checkedClass);

                    if ($e.is(":checked")) {
                        spanTag.addClass(options.checkedClass);
                    }
                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }

                } else if ($e.is(":radio")) {
                    //element is a radio
                    var spanTag = $e.closest("span");
                    var divTag = $e.closest("div");

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);
                    spanTag.removeClass(options.checkedClass);

                    if ($e.is(":checked")) {
                        spanTag.addClass(options.checkedClass);
                    }

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }
                } else if ($e.is(":file")) {
                    var divTag = $e.parent("div");
                    var filenameTag = $e.siblings(options.filenameClass);
                    btnTag = $e.siblings(options.fileBtnClass);

                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);

                    filenameTag.text($e.val());

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }
                } else if ($e.is(":submit") || $e.is(":reset") || $e.is("button") || $e.is("a") || elem.is("input[type=button]")) {
                    var divTag = $e.closest("div");
                    divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);

                    if ($e.is(":disabled")) {
                        divTag.addClass(options.disabledClass);
                    } else {
                        divTag.removeClass(options.disabledClass);
                    }

                }

            });
        };

        return this.each(function () {
            if ($.support.selectOpacity) {
                var elem = $(this);

                if (elem.is("select")) {
                    //element is a select
                    if (elem.attr("multiple") != true) {
                        //element is not a multi-select
                        if (elem.attr("size") == undefined || elem.attr("size") <= 1) {
                            doSelect(elem);
                        }
                    }
                } else if (elem.is(":checkbox")) {
                    //element is a checkbox
                    doCheckbox(elem);
                } else if (elem.is(":radio")) {
                    //element is a radio
                    doRadio(elem);
                } else if (elem.is(":file")) {
                    //element is a file upload
                    doFile(elem);
                } else if (elem.is(":text, :password, input[type='email']")) {
                    doInput(elem);
                } else if (elem.is("textarea")) {
                    doTextarea(elem);
                } else if (elem.is("a") || elem.is(":submit") || elem.is(":reset") || elem.is("button") || elem.is("input[type=button]")) {
                    doButton(elem);
                }

            }
        });
    };
})(jQuery);


(function ($) {
    /**
    * This is a simple jQuery plugin that works with the jQuery UI
    * dialog. This plugin makes the jQuery UI dialog append to the
    * first form on the page (i.e. the asp.net form) so that
    * forms in the dialog will post back to the server.
    *
    * This plugin is merely used to open dialogs. Use the normal
    * $.fn.dialog() function to close dialogs programatically.
    */
    $.fn.aspdialog = function () {
        if (typeof $.fn.dialog !== "function") return;

        var dlg = {};

        if ((arguments.length == 0) || (arguments[0] instanceof String)) {
            // If we just want to open it without any options
            // we do it this way.
            dlg = this.dialog({
                "autoOpen": false,
                modal: true,
                beforeClose: function (event, ui) {
                    //removeDialogImage();
                    //console.log('Event Fire');
                }
            });
            dlg.parent().appendTo('form:first');
            dlg.dialog('open');
        } else {
            var options = arguments[0];
            options.autoOpen = false;
            options.bgiframe = true;

            dlg = this.dialog(options);
            dlg.parent().appendTo('form:first');
            dlg.dialog('open');
        }
    };
})(jQuery);

/*-----------------------------------------------------------------------*/
/*My custom script*/
/*-----------------------------------------------------------------------*/

function setDropDownListWidthOnChange(dropdown) {
    var elem = dropdown;
    var divTag = elem.parent("div");
    var spanTag = elem.siblings("span");
    var selected = elem.find(":selected:first");
    if (selected.length == 0) {
        selected = elem.find("option:first");
    }
    spanTag.html(selected.html());



    var elemWidth = 0;
    if (divTag.outerWidth(false) == 10) //width of the div tag if elem initial width is zero
    {
        elemWidth = divTag.outerWidth(false) + spanTag.outerWidth(false) + 25 + 2 + 10;

    } else {
        elemWidth = spanTag.outerWidth(false) + 25 + 2 + 10;
    }
    elem.width(elemWidth);
}

/*----------------------------------------------------------------------*/
/* Alert Dialog
/* like the native alert method
/*----------------------------------------------------------------------*/

$.alert = function (content, options) {
    //if no options it is a normal dialog
    if (!options) {
        var options = $.extend(true, {}, {
            buttons: [{
                text: $.alert.defaults.text.ok,
                click: function () {
                    $(this).dialog('close');
                    //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove();
                    $('#wl_dialog_alert').prev().find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
                    $('#wl_dialog_alert').remove();
                }
            }],
            open: function () { // open event handler
                $(this) // the element being dialogged
        .parent() // get the dialog widget element
        .find(".ui-dialog-titlebar-close") // find the close button for this dialog
        .hide(); // hide it
            },
            beforeClose: function (event, ui) {
                removeDialogImage();
                //console.log('Event Fire');
            }
        }, $.alert.defaults);
    }

    //nativ behaviour
    if (options.nativ) {
        alert(content);
        return;
    }

    //create a container
    var container = $('<div/>', {
        id: 'wl_dialog_alert'
    }).appendTo('body'); //wl_dialog 11/20/2012.Alert will have an image aginst the header.so to make this unique,making this change.

    //set a header
    if (options.text.header) {
        container.attr('title', options.text.header);
    }

    //fill the container
    container.html(content.replace(/\n/g, '<br>'));
    //display the dialog
    container.dialog(options);

    return {
        close: function (callback) {
            container.dialog('close');
            container.remove();
            if ($.isFunction(callback)) callback.call(this);
        },
        setHeader: function (text) {
            this.set('title', text);
        },
        setBody: function (html) {
            container.html(html);
        },
        set: function (option, value) {
            container.dialog("option", option, value);
        }
    }


};


$.alert.defaults = {
    nativ: false,
    resizable: false,
    modal: true,
    text: {
        header: 'Notification',
        ok: 'OK'
    }
};


$.alert.Error = {
    nativ: false,
    resizable: false,
    modal: true,
    text: {
        header: 'Error',
        ok: 'OK'
    },
    buttons: [{
        text: $.alert.defaults.text.ok,
        click: function () {
            $(this).dialog('close');
            //$('.ui-dialog-titlebar').find($('#dialogImageId')).remove();
            $('#wl_dialog_alert').prev().find($('#dialogImageId')).remove(); //10-10-2012 Removing the Dialog Image
            $('#wl_dialog_alert').remove();
        }
    }],
    open: function () { // open event handler
        $(this) // the element being dialogged
    .parent() // get the dialog widget element
    .find(".ui-dialog-titlebar-close") // find the close button for this dialog
    .hide(); // hide it
    },
    beforeClose: function (event, ui) {
        removeDialogImage();
        //console.log('Event Fire');
    }
};

$.alert.Information = {
    nativ: false,
    resizable: false,
    modal: true,
    text: {
        header: 'Notification',
        ok: 'OK'
    },
    buttons: [{
        text: $.alert.defaults.text.ok,
        click: function () {
            $(this).dialog('close');
            $('#wl_dialog_alert').prev().find($('#dialogImageId')).remove();
            $('#wl_dialog_alert').remove();
        }
    }],
    open: function () { // open event handler
        $(this) // the element being dialogged
    .parent() // get the dialog widget element
    .find(".ui-dialog-titlebar-close") // find the close button for this dialog
    .hide(); // hide it
    },
    beforeClose: function (event, ui) {
        removeDialogImage();
    }
};

$(document).ready(function () {


    //for Caching
    var $content = $('#content');

    /*----------------------------------------------------------------------*/
    /* preload images
    /*----------------------------------------------------------------------*/

    //$.preload();

    //$("select, input[type=file]").not('select[multiple]').uniform();

});
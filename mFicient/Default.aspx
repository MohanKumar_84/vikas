﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="mFicient._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Register Company</title>
    <link rel="stylesheet" href="css/style.css" id="Link1" />
    <link rel="stylesheet" href="css/themes/jquery-ui.css" id="Link2" />
    <link rel="stylesheet" href="css/themes/jquery.uniform.css" id="Link3" />
    <script src="Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script src="Scripts/Plugins.js" type="text/javascript"></script>
    <script src="Scripts/Scripts.js" type="text/javascript"></script>
    <script type="text/javascript">
        function rememberPasswordEntered() {
            var hidPassword = $('#' + '<%= hidPasswordEntered.ClientID %>');
            var hidReTypePasswordEntered = $('#' + '<%=hidReTypePasswordEntered.ClientID %>');
            var txtPassword = $('#' + '<%=txtPassword.ClientID %>');
            var txtRetypePassword = $('#' + '<%=txtRetypePassword.ClientID %>');
            $(hidPassword).val($(txtPassword).val());
            $(hidReTypePasswordEntered).val($(txtRetypePassword).val());
        }
        function fillPasswordEntered() {
            var hidPassword = document.getElementById('<%= hidPasswordEntered.ClientID %>');
            var hidReTypePasswordEntered = $('#' + '<%=hidReTypePasswordEntered.ClientID %>');
            var txtPassword = document.getElementById('<%=txtPassword.ClientID %>');
            var txtRetypePassword = document.getElementById('<%=txtRetypePassword.ClientID %>');
            if (hidPassword && hidReTypePasswordEntered) {
                if (hidPassword.value !== "") {
                    if (txtPassword)
                        $(txtPassword).val($(hidPassword).val());
                }
                else {
                    if (txtPassword)
                        $(txtPassword).val("");
                }
                if ($(hidReTypePasswordEntered).val() !== "") {
                    if (txtRetypePassword)
                        $(txtRetypePassword).val($(hidReTypePasswordEntered).val());
                }
                else {
                    if (txtRetypePassword)
                        $(txtRetypePassword).val("");
                }
            }

        }
        function setWidthOfDropDown() {
            //            var ddlTimezone = $('#' + '<%=ddlTimezone.ClientID %>');
            //            var ddlCountry = $('#' + '<%=ddlCountry.ClientID %>');
            //            if (ddlTimezone) {
            //                $(ddlTimezone).siblings('span').width('100%');
            //            }
            //            if (ddlCountry) {
            //                $(ddlCountry).siblings('span').width('100%');
            //            }
        }
    </script>
</head>
<body>
    <div id="cmpRegisterForm">
        <div id="cmpRegisterFormInner">
            <div class="clearfix" id="header">
                <div style="width: 150; height: 75; float: left;">
                    <img alt="mFicient" src="css/themes/images/mficientLogo150x75.png" />
                </div>
                <div id="headerText">
                    New User Registration
                </div>
            </div>
            <form id="frmRegisterCompany" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            <asp:UpdatePanel ID="updRegisterCompany" runat="server">
                <ContentTemplate>
                    <div>
                        <label for="<%=txtCompanyName.ClientID %>">
                            Enterprise / Organization Name
                        </label>
                        <asp:TextBox ID="txtCompanyName" runat="server" MaxLength="50"></asp:TextBox>
                    </div>
                    <div>
                        <label for="<%=txtCompanyId.ClientID %>">
                            Enterprise / Organization ID</label>
                        <asp:TextBox ID="txtCompanyId" runat="server"></asp:TextBox>
                    </div>
                    <div>
                        <label for="<%=txtAdminName.ClientID %>">
                            Administrator Name</label>
                        <asp:TextBox ID="txtAdminName" runat="server" MaxLength="100"></asp:TextBox>
                    </div>
                    <div>
                        <label for="<%=txtAdminEmail.ClientID %>">
                            Email</label>
                        <asp:TextBox ID="txtAdminEmail" runat="server"></asp:TextBox>
                    </div>
                    <%--<div class="fl" style="width:48%;">
                        <label for="<%=txtPassword.ClientID %>">
                            Password</label>
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="20" onblur="rememberPasswordEntered();"></asp:TextBox>
                    </div>
                    <div class="fl" style="margin-left:18px;width:48%">
                        <label for="<%=txtRetypePassword.ClientID %>">
                            Retype Password</label>
                        <asp:TextBox ID="txtRetypePassword" runat="server" TextMode="Password" MaxLength="20" onblur="rememberPasswordEntered();"></asp:TextBox>
                    </div>--%>
                    <div>
                        <label for="<%=txtPassword.ClientID %>">
                            Password</label>
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="20" onblur="rememberPasswordEntered();"></asp:TextBox>
                    </div>
                    <div>
                        <label for="<%=txtRetypePassword.ClientID %>">
                            Retype Password</label>
                        <asp:TextBox ID="txtRetypePassword" runat="server" TextMode="Password" MaxLength="20"
                            onblur="rememberPasswordEntered();"></asp:TextBox>
                    </div>
                    <%--<div class="clear"></div>--%>
                    <div>
                        <label for="<%=txtContactNo.ClientID %>">
                            Contact No</label>
                        <asp:TextBox ID="txtContactNo" runat="server"></asp:TextBox>
                    </div>
                    <div>
                        <label for="<%=txtAddress1.ClientID %>">
                            Street Address 1</label>
                        <asp:TextBox ID="txtAddress1" runat="server" MaxLength="50"></asp:TextBox>
                    </div>
                    <div>
                        <label for="<%=txtAddress2.ClientID %>">
                            Street Address 2</label>
                        <asp:TextBox ID="txtAddress2" runat="server" MaxLength="50"></asp:TextBox>
                    </div>
                    <div>
                        <label for="<%=txtAddress3.ClientID %>">
                            Street Address 3</label>
                        <asp:TextBox ID="txtAddress3" runat="server" MaxLength="50"></asp:TextBox>
                    </div>
                    <div>
                        <label for="<%=txtCity.ClientID %>">
                            City</label>
                        <asp:TextBox ID="txtCity" runat="server" MaxLength="50"></asp:TextBox>
                    </div>
                    <div>
                        <label for="<%=txtState.ClientID %>">
                            State</label>
                        <asp:TextBox ID="txtState" runat="server" MaxLength="50"></asp:TextBox>
                    </div>
                    <div>
                        <label for="<%=txtZip.ClientID %>">
                            Zip</label>
                        <asp:TextBox ID="txtZip" runat="server" MaxLength="10"></asp:TextBox>
                    </div>
                    <div>
                        <label for="<%=ddlCountry.ClientID %>">
                            Country</label>
                        <asp:DropDownList ID="ddlCountry" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div>
                        <label for="<%=ddlTimezone.ClientID %>">
                            Timezone</label>
                        <asp:DropDownList ID="ddlTimezone" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="submitContainer">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                    </div>
                    <div>
                        <asp:HiddenField ID="hidPasswordEntered" runat="server" />
                        <asp:HiddenField ID="hidReTypePasswordEntered" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div id="divWaitBox">
                <img src="css/themes/images/progress.gif" alt="Loading" />
            </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        Sys.Application.add_init(application_init);
        function application_init() {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(prm_initializeRequest);
            prm.add_endRequest(prm_endRequest);
        }
        function prm_initializeRequest() {
            showWaitModal();
        }
        function prm_endRequest() {
            hideWaitModal();
        }
    </script>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="enquiry.aspx.cs" Inherits="mFicient.enquiry" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <script type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8) {
                keychar = String.fromCharCode(key);
            }
            if (key == 13) {
                key = 8;
                keychar = String.fromCharCode(key);
            }
            return reg.test(keychar);
        }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
    <ContentTemplate>
        <div>
        <div>
            <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Size="Smaller"></asp:Label>
        </div>
        <br />
        <div style="width: 200px;float:left;">
            <div style="height: 30px; vertical-align: middle">
            COMPANY NAME
            </div>
            <div style="height: 30px; vertical-align: middle">
            COUNTRY
            </div>
            <div style="height: 30px; vertical-align: middle">
            TYPE OF COMPANY
            </div>
            <div style="height: 30px; vertical-align: middle">
            EXPECTED USERS
            </div>
            <div style="height: 30px; vertical-align: middle">
            EXPECTED WF
            </div>
            <div style="height: 30px; vertical-align: middle">
            CONTACT PERSON
            </div>
            <div style="height: 30px; vertical-align: middle">
            CONTACT NUMBER
            </div>
            <div style="height: 30px; vertical-align: middle">
            CONTACT EMAIL
            </div>
        </div>
        <div style="width: 300px;float:left;">
            <div style="height: 30px; vertical-align: middle">
            <asp:TextBox ID="txtCompName" runat="server"></asp:TextBox>
            </div>
            <div style="height: 30px; vertical-align: middle">
                <asp:DropDownList ID="ddlCountry" runat="server">
                </asp:DropDownList>
            </div>
            <div style="height: 30px; vertical-align: middle">
            <asp:TextBox ID="txtCompType" runat="server"></asp:TextBox>
            </div>
            <div style="height: 30px; vertical-align: middle">
            <asp:TextBox ID="txtExpUsers" runat="server"></asp:TextBox>
            </div>
            <div style="height: 30px; vertical-align: middle">
                <asp:TextBox ID="txtExpWF" runat="server"></asp:TextBox>
            </div>
            <div style="height: 30px; vertical-align: middle">
            <asp:TextBox ID="txtContactPerson" runat="server"></asp:TextBox>
            </div>
            <div style="height: 30px; vertical-align: middle">
            <asp:TextBox ID="txtContactNo" runat="server"></asp:TextBox>
            </div>
            <div style="height: 30px; vertical-align: middle">
            <asp:TextBox ID="txtContactEmail" runat="server"></asp:TextBox>
            </div>
        </div>
       
    </div>
        <div style="width: 500px; clear:both;">
            <div style="height: 30px; vertical-align: middle">
                <div style="float:right;margin-right:250px;">
                        <asp:UpdatePanel runat="server" ID="ButtonUpdatePanel" UpdateMode="Conditional">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click"/>
                            </Triggers>
                            <ContentTemplate>
                               <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>

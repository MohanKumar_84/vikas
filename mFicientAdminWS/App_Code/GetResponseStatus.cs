﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientAdminWS
{
    public class GetResponseStatus
    {
        /// <summary>
        /// To get the response status with error.
        /// The response status for error have 2 fields Code and description.
        /// So to get the response status Pass the exception with error code as message.
        ///To pass error message,pass it as inner exception message.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.MficientException">Thrown when Error code does not exists.
        /// And the error is not a code.</exception>
        public static ResponseStatus getRespStatus(MficientException ex,
            int fnCode)
        {
            FUNCTION_CODES eFnCode = getFunctionCodeType(fnCode);
            ResponseStatus objResponseStatus = new ResponseStatus();
            if (Enum.IsDefined(typeof(FUNCTION_CODES), eFnCode))
            {
                if (ex != null)
                {
                    int iErrorCode = 0;
                    bool isErrorACode = isErrorMsgAnIntErrorCode(
                        ex.Message, out iErrorCode);
                    if (isErrorACode)
                    {
                        objResponseStatus.cd = iErrorCode.ToString();
                        if (ex.InnerException == null)
                        {
                            objResponseStatus.desc =
                                getErrorMsgForCodeByFunctionCode(
                                iErrorCode,
                                eFnCode);
                        }
                        else
                        {
                            objResponseStatus.desc =
                                ex.InnerException.Message;
                        }
                    }
                    else
                        throw new MficientException("The error message is not an error code.");
                }
                else
                {
                    objResponseStatus.cd = "0";
                    objResponseStatus.desc = String.Empty;
                }
            }
            else
            {
                throw new MficientException("Invalid function code.");
            }
            return objResponseStatus;
        }
        static bool isErrorMsgAnIntErrorCode(string errorMsg, out int errorCode)
        {
            errorCode = 0;
            return Int32.TryParse(errorMsg, out errorCode);
        }
        static string getErrorMsgForCodeByFunctionCode(int errorCode,
            FUNCTION_CODES fnCode)
        {
            string strErrorMsg = String.Empty;
            ImFicientErrors objError = null;
            switch (fnCode)
            {
                case FUNCTION_CODES.COMPANY_SERVER_DETAIL:
                    break;
                case FUNCTION_CODES.MPLUGIN_SERVER_DETAIL:
                    break;
                case FUNCTION_CODES.GET_LATEST_APP_VERSION:
                    break;
                case FUNCTION_CODES.COMPANY_REGISTRATION:
                    objError = new CompanyRegistrationError();
                    break;
                case FUNCTION_CODES.COUNTRY_LIST:
                    break;
                case FUNCTION_CODES.TIMEZONE_LIST:
                    objError = new GetAllTimezonesError();
                    break;
                case FUNCTION_CODES.ADD_MBUZZ_SERVER:
                    break;
                case FUNCTION_CODES.UPDATE_MBUZZ_SERVER:
                    break;
                default:
                    break;
            }
            
            strErrorMsg = objError.getErrorMsgFromErrorCode(errorCode);
            return strErrorMsg;
        }
        static FUNCTION_CODES getFunctionCodeType(int functionCode)
        {
            return (FUNCTION_CODES)Enum.Parse(
                typeof(FUNCTION_CODES),
                functionCode.ToString()
                );
        }
    }
}
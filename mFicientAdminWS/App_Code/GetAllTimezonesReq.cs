﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientAdminWS
{
    public class GetAllTimezonesReq
    {
        string _requestId, _companyId;
        int _functionCode;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }
        public GetAllTimezonesReq(string requestJson)
        {
            RequestJsonParsing<GetAllTimezonesReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<GetAllTimezonesReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.TIMEZONE_LIST)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            
        }
    }
    [DataContract]
    public class GetAllTimezonesReqData : Data
    {
    }
}
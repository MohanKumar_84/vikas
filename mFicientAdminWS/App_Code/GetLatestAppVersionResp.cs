﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientAdminWS
{
    public class GetLatestAppVersionResp
    {
        ResponseStatus objRespStatus;
        string strRequestId;
        //private string strRequestID, strStatusDescription;
        private int intStatusCode, intAppVersionMajor, intAppVersionMinor, intAppVersionRevision, intMinAppVersionMajor, intMinAppVersionMinor, intMinAppVersionRevision,
        intDbAppVersionMajor, intDbAppVersionMinor, intDbAppVersionRevision;

        public GetLatestAppVersionResp(ResponseStatus _responseStatus, string requestId, int _AppVersionMajor, int _AppVersionMinor, int _AppVersionRevision,
           int _AppMinVersionMajor, int _AppMinVersionMinor, int _AppMinVersionRevision, int _DatabaseVersionMajor, int _DatabaseVersionMinor, int _DatabaseVersionRevision)
        {
            try
            {
                objRespStatus = _responseStatus;
                strRequestId = requestId;
                //strStatusDescription = _statusDescription.Trim();
                intAppVersionMajor = _AppVersionMajor;
                intAppVersionMinor = _AppVersionMinor;
                intAppVersionRevision = _AppVersionRevision;
                intMinAppVersionMajor = _AppMinVersionMajor;
                intMinAppVersionMinor = _AppMinVersionMinor;
                intMinAppVersionRevision = _AppMinVersionRevision;
                intDbAppVersionMajor = _DatabaseVersionMajor;
                intDbAppVersionMinor = _DatabaseVersionMinor;
                intDbAppVersionRevision = _DatabaseVersionRevision;
                //Utilities.ValidateResponseParams(strRequestID, intStatusCode, strStatusDescription);
            }
            catch (Exception ex)
            {
                throw new Exception("Error Creating GetLatestAppVersionResp Instance [" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            GetLatestAppVersionResponse objGetLatestVersionResp = new GetLatestAppVersionResponse();
            objGetLatestVersionResp.func = Convert.ToString((int)FUNCTION_CODES.GET_LATEST_APP_VERSION);
            objGetLatestVersionResp.rid = strRequestId;
            objGetLatestVersionResp.status = objRespStatus;
            GetLatestAppVersionResponseData objGetLatestVersionData = new GetLatestAppVersionResponseData();
            objGetLatestVersionData.vmjr = intAppVersionMajor;
            objGetLatestVersionData.vmin = intAppVersionMinor;
            objGetLatestVersionData.vrev = intAppVersionRevision;
            objGetLatestVersionData.mmjr = intMinAppVersionMajor;
            objGetLatestVersionData.mmin = intMinAppVersionMinor;
            objGetLatestVersionData.mrev = intMinAppVersionRevision;
            //objGetLatestVersionData.dbmjr = intDbAppVersionMajor;
            //objGetLatestVersionData.dbmnr = intDbAppVersionMinor;
            //objGetLatestVersionData.dbrev = intDbAppVersionRevision;
            objGetLatestVersionResp.data = objGetLatestVersionData;

            string strJsonWithDetails = Utilities.SerializeJson<GetLatestAppVersionResponse>(objGetLatestVersionResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class GetLatestAppVersionResponse : CommonResponse
    {
        public GetLatestAppVersionResponse()
        { }
        [DataMember]
        public GetLatestAppVersionResponseData data { get; set; }
    }

    public class GetLatestAppVersionResponseData
    {
        /// <summary>
        ///App Version Major
        /// </summary>
        [DataMember]
        public int vmjr { get; set; }

        /// <summary>
        ///App Version Minor
        /// </summary>
        [DataMember]
        public int vmin { get; set; }

        /// <summary>
        ///App Version Revision
        /// </summary>
        [DataMember]
        public int vrev { get; set; }

        /// <summary>
        ///Minimum App Version Major
        /// </summary>
        [DataMember]
        public int mmjr { get; set; }

        /// <summary>
        /// Minimum App Version Minor
        /// </summary>
        [DataMember]
        public int mmin { get; set; }


        /// <summary>
        /// Minimum App Version Revision
        /// </summary>
        [DataMember]
        public int mrev { get; set; }
    }
}
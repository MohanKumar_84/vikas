﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdminWS
{
    public class GetLatestAppVersion
    {
        private GetLatestAppVersionReq objRequest;
        //private int intVersionMajor, intVersionMinor, intVersionRevision, intAppVersionMajor, intAppVersionMinor, intAppVersionRevision;
        //private int intDbVersionMajor, intDbVersionMinor, intDbVersionRevision;

        public GetLatestAppVersion(GetLatestAppVersionReq _Request)
        {
            objRequest = _Request;
        }


        public GetLatestAppVersionResp Process()
        {
            GetLatestAppVersionResp objResp;
            ResponseStatus objRespStatus = new ResponseStatus();
            //SqlConnection con;
            //Utilities.SqlConnectionOpen(out con);

            string strQuery = @"select d.MOBILE_APP_VERSION_MAJOR,d.MOBILE_APP_VERSION_MINOR,d.MOBILE_APP_VERSION_REV,d.MINIMUM_APP_VERSION_MAJOR1,
            d.MINIMUM_APP_VERSION_MINOR1,d.MINIMUM_APP_VERSION_REV1 from ADMIN_TBL_MST_MOBILE_DEVICE d where PHONE_DEVICE_TYPE_CODE='" + objRequest.DeviceType + "' " +
            " select top 1 v.DATABASE_VERSION_MAJOR,v.DATABASE_VERSION_MINOR,v.DATABASE_VERSION_REV from ADMIN_TBL_MST_SYSTEM_DATABASE_VERSION v order by v.DATABASE_VERSION_DATE";
            SqlCommand objSqlCommand = new SqlCommand(strQuery);
            objSqlCommand.CommandType = CommandType.Text;
            objSqlCommand.Parameters.AddWithValue("@PHONE_DEVICE_TYPE_CODE", objRequest.DeviceType);

            DataSet objDataset = Utilities.SelectDataFromSQlCommand(objSqlCommand);
            //DataSet objDataset = Utilities.SelectDataFromSQlCommand(objSqlCommand);
            if (objDataset!= null && objDataset.Tables[0].Rows.Count > 0 && objDataset.Tables[1].Rows.Count > 0)
            {
                objRespStatus.cd = "0"; objRespStatus.desc = "";
                objResp = new GetLatestAppVersionResp(objRespStatus, objRequest.RequestId, Convert.ToInt32(objDataset.Tables[0].Rows[0]["MOBILE_APP_VERSION_MAJOR"]),
                    Convert.ToInt32(objDataset.Tables[0].Rows[0]["MOBILE_APP_VERSION_MINOR"]), Convert.ToInt32(objDataset.Tables[0].Rows[0]["MOBILE_APP_VERSION_REV"])
                    ,  Convert.ToInt32(objDataset.Tables[0].Rows[0]["MINIMUM_APP_VERSION_MAJOR1"]),  Convert.ToInt32(objDataset.Tables[0].Rows[0]["MINIMUM_APP_VERSION_MINOR1"]),
                     Convert.ToInt32(objDataset.Tables[0].Rows[0]["MINIMUM_APP_VERSION_REV1"]), Convert.ToInt32(objDataset.Tables[1].Rows[0]["DATABASE_VERSION_MAJOR"]),
                      Convert.ToInt32(objDataset.Tables[1].Rows[0]["DATABASE_VERSION_MINOR"]),  Convert.ToInt32(objDataset.Tables[1].Rows[0]["DATABASE_VERSION_REV"]));
            }
            else
            {
                throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            }
            
            return objResp;
        }
    }
}
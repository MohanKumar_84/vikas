﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientAdminWS
{
    public class GetmPluginServerDetailsResp
    {
        ResponseStatus _respStatus;
        string _requestId, _ServerIP, _ServerPort;
        //DataTable _companyServerDtls;
        public GetmPluginServerDetailsResp(ResponseStatus respStatus, string requestId, string ServerIP, string ServerPort)
        {
            try
            {
                _respStatus = respStatus;
                _ServerIP = ServerIP;
                _ServerPort = ServerPort;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Company Server Details Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            GetmPluginServerDetailsResponse objCompServerDtlsResp = new GetmPluginServerDetailsResponse();
            objCompServerDtlsResp.func = Convert.ToString((int)FUNCTION_CODES.MPLUGIN_SERVER_DETAIL);
            objCompServerDtlsResp.rid = _requestId;
            objCompServerDtlsResp.status = _respStatus;
            GetmPluginServerDetailsResponseData objCompServerDtlsData = new GetmPluginServerDetailsResponseData();
            objCompServerDtlsData.mpip = _ServerIP;
            objCompServerDtlsData.mppt = Convert.ToInt32( _ServerPort);
            objCompServerDtlsResp.data = objCompServerDtlsData;

            string strJsonWithDetails = Utilities.SerializeJson<GetmPluginServerDetailsResponse>(objCompServerDtlsResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class GetmPluginServerDetailsResponse : CommonResponse
    {
        public GetmPluginServerDetailsResponse()
        { }
        [DataMember]
        public GetmPluginServerDetailsResponseData data { get; set; }
    }

    public class GetmPluginServerDetailsResponseData
    {

        /// <summary>
        /// Server IP Address
        /// </summary>
        [DataMember]
        public string mpip { get; set; }

        /// <summary>
        /// Server Port
        /// </summary>
        [DataMember]
        public Int32 mppt { get; set; }
    }
}
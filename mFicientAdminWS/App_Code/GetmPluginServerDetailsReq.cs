﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientAdminWS
{
    public class GetmPluginServerDetailsReq
    {

        string _requestId, _companyId;
        int _functionCode;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }

        public GetmPluginServerDetailsReq(string requestJson)
        {
            RequestJsonParsing<GetmPluginServerDetailsReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<GetmPluginServerDetailsReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.MPLUGIN_SERVER_DETAIL)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _companyId = objRequestJsonParsing.req.data.enid;
        }


    }
    [DataContract]
    public class GetmPluginServerDetailsReqData : Data
    {

    }

}
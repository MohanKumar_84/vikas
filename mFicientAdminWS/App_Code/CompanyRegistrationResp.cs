﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientAdminWS
{
    public class CompanyRegistrationResp
    {
        ResponseStatus _respStatus;
        string _requestId;
        public CompanyRegistrationResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Company Details Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            CompanyRegistrationResponse objCompRegistrationResp = new CompanyRegistrationResponse();
            objCompRegistrationResp.func = Convert.ToString((int)FUNCTION_CODES.COMPANY_REGISTRATION);
            objCompRegistrationResp.rid = _requestId;
            objCompRegistrationResp.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<CompanyRegistrationResponse>(objCompRegistrationResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class CompanyRegistrationResponse : CommonResponse
    {
        public CompanyRegistrationResponse()
        { }
    }
}
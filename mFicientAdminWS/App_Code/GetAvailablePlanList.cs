﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace mFicientAdminWS
{
    public class GetAvailablePlanList
    {
        public enum PLAN_TYPE
        {
            Trial,
            Paid
        }
        PLAN_TYPE _ePlanType;
        string _statusDescription;
        int _statusCode;
        DataTable _planListDtbl;
        #region Public Properties
        public PLAN_TYPE EPlanType
        {
            get { return _ePlanType; }
            private set { _ePlanType = value; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
            private set { _statusDescription = value; }
        }
        public int StatusCode
        {
            get { return _statusCode; }
            private set { _statusCode = value; }
        }
        public DataTable PlanListDtbl
        {
            get { return _planListDtbl; }
            private set { _planListDtbl = value; }
        }
        #endregion
        public GetAvailablePlanList(PLAN_TYPE planType)
        {
            _ePlanType = planType;
        }
        public void Process()
        {
            try
            {
                SqlCommand cmd = getCommand();
                DataSet dsPlans = MSSqlClient.SelectDataFromSqlCommand(cmd);
                if (dsPlans != null)
                {
                    this.PlanListDtbl = dsPlans.Tables[0];
                    this.StatusCode = 0;
                    this.StatusDescription = String.Empty;
                }
                else
                {
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            catch
            {
                this.StatusCode = -1000;
                this.StatusDescription = "";
            }
        }
        string getQuery()
        {
            string strQuery = String.Empty;
            switch (this.EPlanType)
            {
                case PLAN_TYPE.Trial:
                    strQuery = @"SELECT *
                                FROM ADMIN_TBL_TRIAL_PLANS";
                    break;
                case PLAN_TYPE.Paid:
                    strQuery = @"SELECT *
                                FROM ADMIN_TBL_PLANS";
                    break;
            }
            return strQuery;
        }
        SqlCommand getCommand()
        {
            string strQuery = getQuery();
            SqlCommand cmd = new SqlCommand(strQuery);
            cmd.CommandType = CommandType.Text;
            switch (this.EPlanType)
            {
                case PLAN_TYPE.Trial:
                    break;
                case PLAN_TYPE.Paid:
                    break;
            }
            return cmd;
        }
    }
}
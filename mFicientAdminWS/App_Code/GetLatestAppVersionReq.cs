﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientAdminWS
{
    public class GetLatestAppVersionReq
    {
        string _requestId, _deviceType;
        int _functionCode;

        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }

        public GetLatestAppVersionReq(string requestJson)
        {
            RequestJsonParsing<GetLatestAppVersionReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<GetLatestAppVersionReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.GET_LATEST_APP_VERSION)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
           _deviceType = objRequestJsonParsing.req.dtyp;
           if (_deviceType == null)
           {
               throw new Exception(((int)Utilities.INVALID_REQUEST_JSON).ToString());
           }
        }
    }

    [DataContract]
    public class GetLatestAppVersionReqData : Data
    {
        
    }
}
﻿using System;
using System.Security.Cryptography;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Configuration;
namespace mFicientAdminWS
{
   
    public enum FUNCTION_CODES
    {
        COMPANY_SERVER_DETAIL = 11001,
        MPLUGIN_SERVER_DETAIL = 11002,
        GET_LATEST_APP_VERSION = 11003,

        COMPANY_REGISTRATION = 11005,
        COUNTRY_LIST = 11006,
        TIMEZONE_LIST = 11007,

        ADD_MBUZZ_SERVER = 21001,
        UPDATE_MBUZZ_SERVER = 21002,
    }
    public enum CONSTANT_VARIABLES
    {
        REQUEST_TIMEOUT = 30,
    }
    
    public enum DATABASE_ERRORS//9000000
    {
        DATABASE_CONNECTION_ERROR = 9000001,
        RECORD_NOT_FOUND_ERROR = 9000002,
        RECORD_INSERT_ERROR = 9000003,
        RECORD_DELETE_ERROR = 9000004,
    }

    public enum SYSTEM_ERRORS//9100000
    {
        VALIDATION_ERROR = 9100000
    }
    public enum MbuzzDataSendStatus
    {
        PENDING = 0,
        SENT = 1,
        FAILED = 2
    }
    public enum MbuzzMessageCode
    {
        NEW_CONTACT_ADDED_OR_UNBLOCKED = 124,
        CONTACT_BLOCKED = 125,
        CONTACT_DELETED = 126,
        CONTACT_NO_DEVICE = 127,
        DEVICE_DELETED = 128,
        DESKTOP_MESSENGER_DELETED = 129,
        ADD_UPDATE_COMPANY = 130,
        REMOVE_COMPANY = 131,
    }
    public class Utilities
    {
        public static int[] DatabaseErrors = { 90001, 9000002, 9000003, 9000004 };
        public static int INVALID_REQUEST_JSON = 91001;
        /// <summary>

        /// Description :SqlConnection Open Function
        /// Created By:Shalini Dwivedi 
        /// Created On :28/01/2012

        /// </summary>

        public static void SqlConnectionOpen(out SqlConnection Conn)
        {
            try
            {
                Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString);
                Conn.Open();
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Dispose();
                    Conn = null;
                    throw new Exception();
                }
            }
            catch
            {
                Conn = null;
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
        }


        /// <summary>

        /// Description :SqlConnection Close Function
        /// Created By:Shalini Dwivedi 
        /// Created On :28/01/2012

        /// </summary>

        public static void SqlConnectionClose(SqlConnection Conn)
        {
            try
            {
                if ((Conn != null) && (Conn.State == ConnectionState.Open))
                {
                    Conn.Close();
                }
            }
            catch
            {
            }
        }
        /// <summary>

        /// Description :Select Data According To SqlCommand ( Sql Query And Parameter )
        /// Created By: Rahul Chaturvedi
        /// Created On :23/01/2011

        /// </summary>

        public static DataSet SelectDataFromSQlCommand(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;

            SqlConnectionOpen(out objSqlConnection);
            _SqlCommand.Connection = objSqlConnection;

            DataSet ObjDataSet = new DataSet();

            SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(_SqlCommand);

            objSqlDataAdapter.Fill(ObjDataSet);

            SqlConnectionClose(objSqlConnection);

            return ObjDataSet;
        }


        /// <summary>
        /// Description :Data Process (Insert, Update, Delete ) According To SqlCommand ( Sql Query And Parameter )
        /// Created By: Rahul Chaturvedi
        /// Created On :23/01/2011
        /// </summary>
        public static int ExecuteNonQueryRecord(SqlCommand _SqlCommand)
        {
            SqlConnection objSqlConnection = null;
            SqlConnectionOpen(out objSqlConnection);
            _SqlCommand.Connection = objSqlConnection;

            int i = _SqlCommand.ExecuteNonQuery();

            SqlConnectionClose(objSqlConnection);

            return i;
        }

        //Utilities.GetMd5Hash(strAccessCode)
        public static bool CheckAuthenticationCode(string accessCode)
        {
            SqlCommand cmd = new SqlCommand("select * from ADMIN_TBL_WEBSERVICE_AUTHENTICATION where AUTHENTICATION_CODE=@AUTHENTICATION_CODE");
            cmd.Parameters.AddWithValue("@AUTHENTICATION_CODE",GetMd5Hash(accessCode));
            DataSet ds=SelectDataFromSQlCommand(cmd);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
                return true;
            else return false;
        }

        public static string SerializeJson<T>(object obj)
        {
            T objResponse = (T)obj;
            MemoryStream stream = new MemoryStream();
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(objResponse.GetType());
            serializer.WriteObject(stream, objResponse);
            stream.Position = 0;
            StreamReader streamReader = new StreamReader(stream);
            return streamReader.ReadToEnd();
        }
        public static string getFinalJson(string jsonWithDetails)
        {
            string strFinalJson = "{\"resp\":" + jsonWithDetails + "}";
            return strFinalJson;
        }
        public static T DeserialiseJson<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer serialiser = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serialiser.ReadObject(ms);
            ms.Close();
            return obj;
        }

        /// <summary>

        /// Description :Check user is already login 
        /// Created By: Rahul Chaturvedi
        /// Created On :28/01/2012

        /// </summary>
        public static bool IsValidEmail(string _email)
        {
            if (!string.IsNullOrEmpty(_email))
            {
                string strRegex = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
                                    + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				                [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
                                    + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				                [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                                    + @"([a-zA-Z\-]+\.)+[a-zA-Z]{2,4})$";

                Regex re = new Regex(strRegex);
                if (re.IsMatch(_email))
                    return (true);
                else
                    return (false);
            }
            else
                return false;
        }

        // Default Value Parameter Was Giving Compile Error So Removed 
        public static bool IsValidString(string _InputString, bool _a_z, bool _A_Z, bool _0_9, string _SpecialChars, int _MinLength, int _MaxLength, bool _StartSpaceAllowed, bool _EndSpaceAllowed)
        {
            try
            {
                if ((!_StartSpaceAllowed && _InputString.StartsWith(" ")) || (!_EndSpaceAllowed && _InputString.EndsWith(" ")))
                {
                    return false;
                }

                if ((_MaxLength < _MinLength) || (_MinLength >= 0 && _InputString.Length < _MinLength) || (_MaxLength >= 0 && _InputString.Length > _MaxLength))
                {
                    return false;
                }

                /* Create a regex expression based on _a-z and _A_Z and _0_9 and DisallowedCharacters */

                if (string.IsNullOrEmpty(_InputString))
                {
                    return true;
                }

                string strRegex = "";

                if (_a_z)
                {
                    strRegex += "a-z";
                }

                if (_A_Z)
                {
                    strRegex += "A-Z";
                }

                if (_0_9)
                {
                    strRegex += "0-9";
                }

                string strSpecialCharacters = _SpecialChars.Replace(@"\", @"\\");

                if (strSpecialCharacters.Contains("]"))
                {
                    strRegex = "]" + strRegex;
                    strSpecialCharacters = strSpecialCharacters.Replace("]", "");
                }

                if (strSpecialCharacters.Contains("^"))
                {
                    strRegex += "^";
                    strSpecialCharacters = strSpecialCharacters.Replace("^", "");
                }

                if (strSpecialCharacters.Contains("-"))
                {
                    strRegex = "^[" + strRegex + strSpecialCharacters.Replace("-", "") + "-" + "]+$";
                }
                else
                {
                    strRegex = "^[" + strRegex + strSpecialCharacters + "]+$";
                }

                Regex re = new Regex(strRegex);
                if (re.IsMatch(_InputString))
                {
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        /// <summary>

        /// Description :Validation On Request ID, Status Code, Status Description For Responce Xml
        /// Created By: ----
        /// Created On :12/09/2011

        /// </summary>

        public static void ValidateResponseParams(string _RequestID, int _StatusCode, string _StatusDescription)
        {
            if (string.IsNullOrEmpty(_RequestID))
            {
                throw new Exception("Invalid Request ID");
            }

            if (_StatusCode < 0)
            {
                throw new Exception("Invalid Status Code");
            }

            if ((_StatusCode > 0) && string.IsNullOrEmpty(_StatusDescription))
            {
                throw new Exception("Invalid Status Description");
            }
        }

        /// <summary>

        /// Description :Create 32 bit Hash Code
        /// Created By: ------
        /// Created On :---

        /// </summary>

        public static string GetMd5Hash(string _inputString)
        {
            string strMD5;
            byte[] hashedDataBytes;
            try
            {
                byte[] bs = Encoding.UTF8.GetBytes(_inputString);
                System.Security.Cryptography.MD5CryptoServiceProvider md5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
                hashedDataBytes = md5Hasher.ComputeHash(bs);
                strMD5 = BitConverter.ToString(hashedDataBytes).Replace("-", "");
            }
            catch
            {
                strMD5 = string.Empty;
            }
            return strMD5;
        }

        public static bool VerifyMd5hash(string _input, string _hash)
        {
            if (StringComparer.OrdinalIgnoreCase.Compare(GetMd5Hash(_input), _hash) == 0)
            {
                return true;
            }
            return false;
        }

        public static long getEpoch(long dateToConvert)
        {
           return Convert.ToInt64((dateToConvert - 621355968000000000) / 10000000);
        }

        #region Do functions Of webservice

        public static string DoCompanyServerDetail(object _RequestObject)
        {
            GetCompanyServerDetails objCompanyServerDtls = new GetCompanyServerDetails((GetCompanyServerDetailsReq)_RequestObject);
            GetCompanyServerDetailsResp objCompanyServerDtlsResp = objCompanyServerDtls.Process();
            return objCompanyServerDtlsResp.GetResponseJson();
        }

        public static string DoGetmPluginServerDetails(object _RequestObject)
        {
            GetmPluginServerDetails objGetmPluginServerDetails = new GetmPluginServerDetails((GetmPluginServerDetailsReq)_RequestObject);
            GetmPluginServerDetailsResp objGetmPluginServerDetailsResp = objGetmPluginServerDetails.Process();
            return objGetmPluginServerDetailsResp.GetResponseJson();
        }
        
        public static string DoGetLatestAppVersion(object _RequestObject)
        {
            GetLatestAppVersion objLatestAppVersion = new GetLatestAppVersion((GetLatestAppVersionReq)_RequestObject);
            GetLatestAppVersionResp objLatestAppVersionResp = objLatestAppVersion.Process();
            return objLatestAppVersionResp.GetResponseJson();
        }

        public static string DoAddmBuzzServer(object _RequestObject)
        {
            AddmBuzzServer objAddmBuzzServer = new AddmBuzzServer((AddmBuzzServerReq)_RequestObject);
            AddmBuzzServerResp objAddmBuzzServerResp = objAddmBuzzServer.Process();
            return objAddmBuzzServerResp.GetResponseJson();
        }

        public static string DoUpdatemBuzzServer(object _RequestObject)
        {
            UpdatemBuzzServer objUpdatemBuzzServer = new UpdatemBuzzServer((UpdatemBuzzServerReq)_RequestObject);
            UpdatemBuzzServerResp objUpdatemBuzzServerResp = objUpdatemBuzzServer.Process();
            return objUpdatemBuzzServerResp.GetResponseJson();
        }
        public static string DoGetCountryList(object _RequestObject)
        {
            GetCountryList objGetCountryList = new GetCountryList((GetCountryListReq)_RequestObject);
            GetCountryListResp objGetCountryListResp = objGetCountryList.Process();
            return objGetCountryListResp.GetResponseJson();
        }
        public static string DoCompanyRegistration(object _RequestObject, HttpContext context)
        {
            CompanyRegistration objCmpRegistration = new CompanyRegistration((CompanyRegistrationReq)_RequestObject);
            CompanyRegistrationResp objCmpRegistrationResp = objCmpRegistration.Process(context);
            return objCmpRegistrationResp.GetResponseJson();
        }
        public static string DoGetTimezoneList(object _RequestObject)
        {
            GetAllTimezones objGetAllTimezone = new GetAllTimezones((GetAllTimezonesReq)_RequestObject);
            GetAllTimezonesResp objGetAllTimezonesResp = objGetAllTimezone.Process();
            return objGetAllTimezonesResp.GetResponseJson();
        }
        #endregion

       
        #region Data Encription

        public static string EncryptString(string s)
        {
            string strEnc = "";
            try
            {
                if (string.IsNullOrEmpty(s))
                {
                    return strEnc;
                }

                int key1 = (DateTime.Now.Second % 10) + 2;
                int key2 = ((key1 + DateTime.Now.Minute) % 10) + 4;
                string strThisAscii = "";

                for (int i = 0; i < s.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        strThisAscii = Hex(Asc(s.Substring(i, 1)) + key1 - key2);
                    }
                    else
                    {
                        strThisAscii = Hex(Asc(s.Substring(i, 1)) + key2 - key1);
                    }

                    do
                    {
                        strThisAscii = "0" + strThisAscii;

                    } while (strThisAscii.Length < 4);

                    strEnc = strThisAscii + strEnc;
                }

                strEnc = (Hex(key1 * 10) + strEnc + Hex(key2 * 10)).ToLower();

                strEnc = strEnc.Replace("a", "g").Replace("b", "i").Replace("c", "k").Replace("d", "m").Replace("e", "o").Replace("f", "q").Replace("1", "s").Replace("2", "u");
                strEnc = strEnc.Replace("3", "v").Replace("4", "t").Replace("5", "r").Replace("6", "p").Replace("7", "n").Replace("8", "l").Replace("9", "j").Replace("0", "h");
            }
            catch
            {
                strEnc = "";
            }
            return strEnc;
        }

        public static string DecryptString(string s)
        {
            string strDec = "";
            try
            {
                if (string.IsNullOrEmpty(s))
                {
                    return strDec;
                }

                s = s.Replace("h", "0").Replace("j", "9").Replace("l", "8").Replace("n", "7").Replace("p", "6").Replace("r", "5").Replace("t", "4").Replace("v", "3");
                s = s.Replace("u", "2").Replace("s", "1").Replace("q", "f").Replace("o", "e").Replace("m", "d").Replace("k", "c").Replace("i", "b").Replace("g", "a");

                int key1 = (int)HexToDec(s.Substring(0, 2)) / 10;
                int key2 = (int)HexToDec(s.Substring(s.Length - 2, 2)) / 10;

                s = s.Substring(2, s.Length - 4);

                string strThisChar = "";
                bool oddChar = true;

                for (int i = s.Length - 4; i >= 0; i -= 4)
                {
                    if (oddChar)
                    {
                        strThisChar = Chr(HexToDec(s.Substring(i, 4)) - key1 + key2);
                        oddChar = false;
                    }
                    else
                    {
                        strThisChar = Chr(HexToDec(s.Substring(i, 4)) - key2 + key1);
                        oddChar = true;
                    }

                    strDec += strThisChar;
                }

                return strDec;
            }
            catch
            {
            }
            return "";
        }

        public static string Hex(int n)
        {
            try
            {
                return n.ToString("X");
            }
            catch
            {
            }
            return "";
        }

        public static int HexToDec(string s)
        {
            int intResult;
            try
            {
                intResult = Convert.ToInt32(s, 16);
            }
            catch
            {
                intResult = 0;
            }
            return intResult;
        }

        public static string StringReverse(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }
            StringBuilder rs = new StringBuilder(s.Length);
            for (int i = s.Length - 1; i >= 0; i--)
            {
                rs.Append(s.Substring(i, 1));
            }
            return rs.ToString();
        }

        private static short Asc(string String)
        {
            return Encoding.Default.GetBytes(String)[0];
        }

        private static string Chr(int CharCode)
        {
            if (CharCode > 255)
            {
                return "";
            }
            return Encoding.Default.GetString(new[] { (byte)CharCode });
        }

        #endregion

    }
}
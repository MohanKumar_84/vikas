﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientAdminWS
{

    [Serializable]
    public class MficientException : ApplicationException
    {
        public MficientException() { }
        public MficientException(string message) : base(message) { }
        public MficientException(string message, Exception inner) : base(message, inner) { }
        protected MficientException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
    public class MficientExceptionWithErrorInfo : MficientException
    {
        MficientErrorInfo _errorInfo;

        public MficientErrorInfo ErrorInfo
        {
            get { return _errorInfo; }
            private set { _errorInfo = value; }
        }
        public MficientExceptionWithErrorInfo(
            int errorCode,
            int functionCode,
            string errorDescription)
        {
            ErrorInfo.ErrorCode = errorCode;
            ErrorInfo.FunctionCode = functionCode;
            ErrorInfo.ErrorDescription = errorDescription;
        }
    }
    public class MficientErrorInfo
    {
        int _errorCode, _functionCode;

        public int FunctionCode
        {
            get { return _functionCode; }
            set { _functionCode = value; }
        }

        public int ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }
        string _errorDescription;

        public string ErrorDescription
        {
            get { return _errorDescription; }
            set { _errorDescription = value; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.ObjectModel;
namespace mFicientAdminWS
{
    public class GetAllTimezones
    {
        GetAllTimezonesReq _getTZReq;
        public GetAllTimezones(GetAllTimezonesReq getTZReq)
        {
            _getTZReq = getTZReq;
        }

        public GetAllTimezonesResp Process()
        {
            ResponseStatus objResponseStatus = new ResponseStatus();
            List<TimezoneInformation> lstTimezones = new List<TimezoneInformation>();
            try
            {
                ReadOnlyCollection<TimeZoneInfo> zones = WSCGetSystemTimezones.getSystemTimezones();
                foreach (TimeZoneInfo tz in zones)
                {
                    TimezoneInformation objTzInfo = new TimezoneInformation();
                    objTzInfo.tzid = tz.Id;
                    objTzInfo.tztxt = tz.DisplayName;
                    lstTimezones.Add(objTzInfo);
                }
                objResponseStatus.cd = "0";
                objResponseStatus.desc=String.Empty;
            }
            catch
            {
                objResponseStatus.cd = "1100701";
                objResponseStatus.desc = " Timezone ids not found.";
            }
            return new GetAllTimezonesResp(objResponseStatus, lstTimezones, _getTZReq.RequestId);
        }

    }
}
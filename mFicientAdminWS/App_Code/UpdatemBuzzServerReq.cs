﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientAdminWS
{
    public class UpdatemBuzzServerReq
    { 
        string _requestId, _serverName, _serverIP, _password, _port, _serverId;
        int _functionCode;

        public string ServerName
        {
            get { return _serverName; }
        }
        public string ServerIP
        {
            get { return _serverIP; }
        }
        public string ServerId
        {
            get { return _serverId; }
        }
        public string Password
        {
            get { return _password; }
        }
        public string ServerPort
        {
            get { return _port; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }


        public UpdatemBuzzServerReq(string requestJson)
        {
            RequestJsonParsing<UpdatemBuzzServerReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<UpdatemBuzzServerReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.UPDATE_MBUZZ_SERVER)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _serverName = objRequestJsonParsing.req.data.srvnm;
            _serverIP = objRequestJsonParsing.req.data.srvip;
            _port = objRequestJsonParsing.req.data.srvprt;
            _password = objRequestJsonParsing.req.pwd;
            _serverId = objRequestJsonParsing.req.data.srvid;
        }
    }

    [DataContract]
    public class UpdatemBuzzServerReqData : Data
    {
        public UpdatemBuzzServerReqData()
        { }
        /// <summary>
        /// Server Name
        /// </summary>
        [DataMember]
        public string srvnm { get; set; }

        /// <summary>
        /// Server IP
        /// </summary>
        [DataMember]
        public string srvip { get; set; }

        /// <summary>
        /// Server IP
        /// </summary>
        [DataMember]
        public string srvprt { get; set; }

        /// <summary>
        /// Server ID
        /// </summary>
        [DataMember]
        public string srvid { get; set; }
    }
}
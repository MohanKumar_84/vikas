﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace mFicientAdminWS
{
    public class GetCompanyServerDetailsReq
    {

        string _requestId, _deviceId, _deviceType, _companyId;
        int _functionCode;

        public string CompanyId
        {
            get { return _companyId; }
        }
        public string DeviceType
        {
            get { return _deviceType; }
        }
        public string DeviceId
        {
            get { return _deviceId; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }

        public GetCompanyServerDetailsReq(string requestJson)
        {
            RequestJsonParsing<GetCompanyServerDetailsReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<GetCompanyServerDetailsReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.COMPANY_SERVER_DETAIL)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _deviceId = objRequestJsonParsing.req.did;
            _deviceType = objRequestJsonParsing.req.dtyp;
            _companyId = objRequestJsonParsing.req.data.enid;
        }


    }
    [DataContract]
    public class GetCompanyServerDetailsReqData : Data
    {

    }

}
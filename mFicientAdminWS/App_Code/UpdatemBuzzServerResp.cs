﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mFicientAdminWS
{
    public class UpdatemBuzzServerResp
    {
        ResponseStatus _respStatus;
        string _requestId;

        public UpdatemBuzzServerResp(ResponseStatus respStatus, string requestId)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Updating Company Response[" + ex.Message + "]");
            }
        }
        public string GetResponseJson()
        {
            UpdatemBuzzServerResponse objUpdateCompanyResp = new UpdatemBuzzServerResponse();
            objUpdateCompanyResp.func = Convert.ToString((int)FUNCTION_CODES.UPDATE_MBUZZ_SERVER);
            objUpdateCompanyResp.rid = _requestId;
            objUpdateCompanyResp.status = _respStatus;
            string strJsonWithDetails = Utilities.SerializeJson<UpdatemBuzzServerResponse>(objUpdateCompanyResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class UpdatemBuzzServerResponse : CommonResponse
    {
        public UpdatemBuzzServerResponse()
        {

        }
    }
}
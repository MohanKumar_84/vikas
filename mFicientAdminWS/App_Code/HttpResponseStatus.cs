﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace mFicientAdminWS
{
    public class HttpResponseStatus
    {
        public Boolean Response;
        public HttpStatusCode StatusCode;
        public String ErrorMessage = "", ResponseText = "";

        public HttpResponseStatus(Boolean _Response, HttpStatusCode _StatusCode, String _ErrorMessage, String _ResponseText)
        {
            Response = _Response;
            StatusCode = _StatusCode;
            ErrorMessage = _ErrorMessage;
            ResponseText = _ResponseText;
        }
    }
}
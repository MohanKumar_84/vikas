﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Runtime.Serialization;
namespace mFicientAdminWS
{
    public class GetCompanyServerDetailsResp
    {
        ResponseStatus _respStatus;
        string _requestId,_Serverurl, _ServerIP, _ServerPort;
        public GetCompanyServerDetailsResp(ResponseStatus respStatus, string requestId, string Serverurl, string ServerIP, string ServerPort)
        {
            try
            {
                _respStatus = respStatus;
                _requestId = requestId;
                _Serverurl = Serverurl;
                _ServerIP = ServerIP;
                _ServerPort = ServerPort;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Company Server Details Response[" + ex.Message + "]");
            }
        }

        public string GetResponseJson()
        {
            CompanyServerDtlsResponse objCompServerDtlsResp = new CompanyServerDtlsResponse();
            objCompServerDtlsResp.func = Convert.ToString((int)FUNCTION_CODES.COMPANY_SERVER_DETAIL);
            objCompServerDtlsResp.rid = _requestId;
            objCompServerDtlsResp.status = _respStatus;
            CompanyServerDtlsResponseData objCompServerDtlsData = new CompanyServerDtlsResponseData();
            objCompServerDtlsData.mfur = _Serverurl;// (string)_companyServerDtls.Rows[0]["SERVER_URL"];//cp web service
            objCompServerDtlsData.mbip = _ServerIP;// (string)_companyServerDtls.Rows[0]["SERVER_IP"];
            objCompServerDtlsData.mbpt = Convert.ToInt32(_ServerPort);// Convert.ToInt32(_companyServerDtls.Rows[0]["PORT_NUMBER"].ToString());
            objCompServerDtlsResp.data = objCompServerDtlsData;

            string strJsonWithDetails = Utilities.SerializeJson<CompanyServerDtlsResponse>(objCompServerDtlsResp);
            return Utilities.getFinalJson(strJsonWithDetails);
        }
    }

    public class CompanyServerDtlsResponse : CommonResponse
    {
        public CompanyServerDtlsResponse()
        { }
        [DataMember]
        public CompanyServerDtlsResponseData data { get; set; }
    }

    public class CompanyServerDtlsResponseData
    {
        /// <summary>
        /// Server URL
        /// </summary>
        [DataMember]
        public string mfur { get; set; }

        ///// <summary>
        ///// Server IP Address
        ///// </summary>       
        [DataMember]
        public string mbip { get; set; }

        /// <summary>
        /// Server port
        /// </summary>
        [DataMember]
        public Int32 mbpt { get; set; }
    }
}
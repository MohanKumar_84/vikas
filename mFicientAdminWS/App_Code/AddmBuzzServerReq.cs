﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace mFicientAdminWS
{
    public class AddmBuzzServerReq
    {
        string _requestId, _serverName, _serverIP, _password, _port;
        int _functionCode;

        public string ServerName
        {
            get { return _serverName; }
        }
        public string ServerIP
        {
            get { return _serverIP; }
        }
        public string Password
        {
            get { return _password; }
        }
        public string ServerPort
        {
            get { return _port; }
        }
        public string RequestId
        {
            get { return _requestId; }
        }
        public int FunctionCode
        {
            get { return _functionCode; }
        }


        public AddmBuzzServerReq(string requestJson)
        {
            RequestJsonParsing<AddmBuzzServerReqData> objRequestJsonParsing = Utilities.DeserialiseJson<RequestJsonParsing<AddmBuzzServerReqData>>(requestJson);
            _functionCode = Convert.ToInt32(objRequestJsonParsing.req.func);
            if (_functionCode != (int)FUNCTION_CODES.ADD_MBUZZ_SERVER)
            {
                throw new Exception("Invalid Function Code");
            }
            _requestId = objRequestJsonParsing.req.rid;
            _serverName = objRequestJsonParsing.req.data.srvnm;
            _serverIP = objRequestJsonParsing.req.data.srvip;
            _port = objRequestJsonParsing.req.data.srvprt;
            _password = objRequestJsonParsing.req.pwd;
        }
    }

    [DataContract]
    public class AddmBuzzServerReqData : Data
    {
        /// <summary>
        /// Server Name
        /// </summary>
        [DataMember]
        public string srvnm { get; set; }

        /// <summary>
        /// Server IP
        /// </summary>
        [DataMember]
        public string srvip { get; set; }

        /// <summary>
        /// Server IP
        /// </summary>
        [DataMember]
        public string srvprt { get; set; }

    }
}
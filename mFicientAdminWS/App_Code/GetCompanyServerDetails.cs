﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
namespace mFicientAdminWS
{
    public class GetCompanyServerDetails
    {
        GetCompanyServerDetailsReq _companyServerDtlsReq;
        public GetCompanyServerDetails(GetCompanyServerDetailsReq companyServerDtlsReq)
        {
            _companyServerDtlsReq = companyServerDtlsReq;
        }

        public GetCompanyServerDetailsResp Process()
        {
            DataSet dsServerDetails = getServerDetailsByCompanyId();
            ResponseStatus objResponseStatus = new ResponseStatus();
            if (dsServerDetails != null && dsServerDetails.Tables.Count > 0)
            {
                if (dsServerDetails.Tables[0].Rows.Count != 0)
                {
                    objResponseStatus.cd = "0";
                    objResponseStatus.desc = "";
                }
                else
                {
                    //record not found error
                    throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
                }
            }
            else
            {
                //record not found error
                throw new Exception(((int)DATABASE_ERRORS.RECORD_NOT_FOUND_ERROR).ToString());
            }
            return new GetCompanyServerDetailsResp(objResponseStatus, _companyServerDtlsReq.RequestId, (string)dsServerDetails.Tables[0].Rows[0]["SERVER_URL"], (string)dsServerDetails.Tables[0].Rows[0]["SERVER_IP"], dsServerDetails.Tables[0].Rows[0]["PORT_NUMBER"].ToString());
        }

        DataSet getServerDetailsByCompanyId()
        {
            //open a connection for this as this will be using a different database.
            //don't use the utilities function
            //SqlConnection con;
            //Utilities.SqlConnectionOpen(out con);
            SqlCommand cmd = new SqlCommand(@"SELECT * FROM ADMIN_TBL_SERVER_MAPPING  AS a INNER JOIN ADMIN_TBL_MST_SERVER AS b
                                            ON a.SERVER_ID = b.SERVER_ID INNER JOIN ADMIN_TBL_MBUZZ_SERVER AS c
                                            ON a.MBUZZ_SERVER_ID = c.MBUZZ_SERVER_ID WHERE COMPANY_ID = @CompanyId");

            cmd.Parameters.AddWithValue("@CompanyId", _companyServerDtlsReq.CompanyId);
            cmd.CommandType = CommandType.Text;
            DataSet dsServerDtls = Utilities.SelectDataFromSQlCommand(cmd);
            //using (con)
            //{
            //    //con.Open();
            //    dsServerDtls = new DataSet();
            //    cmd.Connection = con;
            //    SqlDataAdapter objSqlDataAdapter = new SqlDataAdapter(cmd);
            //    objSqlDataAdapter.Fill(dsServerDtls);
            //}

            return dsServerDtls;
        }
    }
}
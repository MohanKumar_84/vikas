﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace mFicientAdminWS
{
    public class AddmBuzzServer
    {
        AddmBuzzServerReq addmBuzzServerReq;

        public AddmBuzzServer(AddmBuzzServerReq _addmBuzzServerReq)
        {
            addmBuzzServerReq = _addmBuzzServerReq;
        }

        public AddmBuzzServerResp Process()
        {
            SqlCommand cmd = null;
            ResponseStatus objRespStatus = new ResponseStatus();
            string serverId = "";
            try
            {
                cmd = new SqlCommand(@"SELECT * FROM ADMIN_TBL_MBUZZ_SERVER WHERE MBUZZ_SERVER_NAME = @MBUZZ_SERVER_NAME;SELECT * FROM ADMIN_TBL_MBUZZ_SERVER WHERE SERVER_IP = @SERVER_IP");

                cmd.Parameters.AddWithValue("@MBUZZ_SERVER_NAME", addmBuzzServerReq.ServerName);
                cmd.Parameters.AddWithValue("@SERVER_IP", addmBuzzServerReq.ServerIP);

                DataSet dsServerDtls = Utilities.SelectDataFromSQlCommand(cmd);

                if (dsServerDtls != null && dsServerDtls.Tables[0].Rows.Count > 0)
                {
                    objRespStatus.cd = "210012";
                    objRespStatus.desc = "Server Name Already Exist.";
                }
                else if (dsServerDtls != null && dsServerDtls.Tables[1].Rows.Count > 0)
                {
                    objRespStatus.cd = "210011";
                    objRespStatus.desc = "Server IP Already Exist";

                }
                else
                {
                    serverId = Utilities.GetMd5Hash(addmBuzzServerReq.ServerName + addmBuzzServerReq.ServerIP + DateTime.Now.Ticks.ToString()).Substring(0, 16);
                    cmd = new SqlCommand(@"INSERT INTO ADMIN_TBL_MBUZZ_SERVER (  MBUZZ_SERVER_ID, MBUZZ_SERVER_NAME,SERVER_IP,PORT_NUMBER,ENABLED)
                                                    VALUES(  @MBUZZ_SERVER_ID, @MBUZZ_SERVER_NAME, @SERVER_IP,@PORT_NUMBER,@ENABLED);");
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@MBUZZ_SERVER_ID", serverId);
                    cmd.Parameters.AddWithValue("@MBUZZ_SERVER_NAME", addmBuzzServerReq.ServerName);
                    cmd.Parameters.AddWithValue("@SERVER_IP", addmBuzzServerReq.ServerIP);
                    cmd.Parameters.AddWithValue("@PORT_NUMBER", addmBuzzServerReq.ServerPort);
                    cmd.Parameters.AddWithValue("@ENABLED", true);

                    objRespStatus.cd = "0";
                    objRespStatus.desc = "";

                    if (Utilities.ExecuteNonQueryRecord(cmd) < 0)
                    {
                        throw new Exception(((int)DATABASE_ERRORS.RECORD_INSERT_ERROR).ToString());
                    }
                }
                        
            }
            catch(Exception ex)
            {
                if(Convert.ToInt32(ex.Message) == ((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR))
                throw new Exception(((int)DATABASE_ERRORS.DATABASE_CONNECTION_ERROR).ToString());
            }
            return new AddmBuzzServerResp(objRespStatus, addmBuzzServerReq.RequestId, serverId);
        }
    }
}